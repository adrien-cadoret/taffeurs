/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name qualityCtrl
 * @requires $scope
 * @requires 'usersService'
 * @requires 'qualityService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $window
 * @requires 'auth'
 * @requires 'adminService'
 * @description Interaction de la page qualité
 */

app.controller('qualityCtrl', ['$scope', 'usersService','qualityService', '$pageTitle', '$filter',  '$window', 'auth', 'adminService',
  function($scope, usersService, qualityService, $pageTitle, $filter, $window, auth, adminService) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('QUALITY'));

  //Initialisation des variables
  $scope.log.login = false;//On met la variable log.login à false pour l'apparence de la page
  var userid = auth.userName;//On récupère l'identifiant de l'utilisateur actuellement connecté
  var saveRow = {};//Sauvegarde de l'écart sélectionné
  $scope.selectedRow = null;//Enregistrer l'écart sélectionné
  $scope.sortType = 'date';//Type de tri de la table des écarts
  $scope.sortReverse = false;//Ordre de tri dans la table des écarts
  $scope.search = {};//Variable de recherche
  $scope.search.gap = '';
  $scope.pagination = {};//Variables pour la pagination
  $scope.pagination.current = 1;//Page actuelle
  $scope.pagination.size = 10;//Nombre d'éléments par page
  $scope.validAddBtn = false;//Gestion de la validation du formulaire d'ajout
  $scope.addAction = false;//Gestion de la vision ou de l'ajout d'un écart
  $scope.differences_all = [];//Initialisation du tableau de tous les écarts
  $scope.differences = [];//Initialisation du tableau des écarts de l'utilisateur
  $scope.modifDone = false;//Repérer si des modifications ont été réalisées sur l'écart
  $scope.date = {};//Variables de date pour les datepickers
  $scope.date.begin = new Date();//Début de l'intervalle
  $scope.date.begin.setMonth($scope.date.begin.getMonth()-1); //Just for the demo
  $scope.date.end = new Date();//Fin de l'intervalle
  $scope.date.add = new Date();//Date de l'ajout
  $scope.successExport = false;//Afficher ou masquer la boite de dialogue pour informer du succès de l'exportation

  //Initialisation du premier jour de la semaine pour les datepickers (1 - Lundi, 0 - Dimanche)
  $scope.dateOptions1 = {
    startingDay: 1
  };

  /**
   * @description Récupèration des droits utilisateur
   */
  adminService.getData(function(data) {
    $scope.rights = data;
  });

  /**
   * @description Récupèration des données de l'utilisateur
   */
  usersService.getUsers(function(data) {
    $scope.users = data;
    angular.forEach(data, function(user) {
      if(user.company == userid) {
        $scope.user = user;

        //Récupération des écarts liés à l'utilisateur
        qualityService.getData(function(data) {
          $scope.differences_all = data;
          angular.forEach($scope.differences_all, function(elt) {
            //On transforme la date de l'écart en un objet date
            elt.date = new Date(elt.date);
            $scope.differences.push(elt);
          });
        });
      }
    });
  });

  /**
   * @description Détecte quand la fenêtre est redimensionnée et ferme les datepickers
   */
  angular.element($window).bind('resize', function () {
    $scope.openedEnd = false;
    $scope.openedBegin = false;
    $scope.openedAdd = false;
    $scope.$apply();
  });

  /**
   * @description On observe si la variable date.begin change de valeur
   * Si c'est le cas, et si la nouvelle valeur est supérieur à la date de fin, on
   * met à jour la date de fin.
   */
  $scope.$watch('date.begin', function (newValue) {
    $scope.date.end = (newValue > $scope.date.end) ? newValue : $scope.date.end;
  });

  /**
   * @description On observe si la variable date.end change de valeur
   * Si c'est le cas, et si la nouvelle valeur est inférieure à la date de début, on
   * met à jour la date de début.
   */
  $scope.$watch('date.end', function (newValue) {
    $scope.date.begin = (newValue < $scope.date.begin) ? newValue : $scope.date.begin;
  });

  //Initialisation des fonctions
  $scope.showModalDeleteGap = showModalDeleteGap;
  $scope.modalSuccessExport = modalSuccessExport;
  $scope.deleteGap = deleteGap;
  $scope.setSelectedRow = setSelectedRow;
  $scope.saveGap = saveGap;
  $scope.addGap = addGap;
  $scope.openAdd = openAdd;
  $scope.openBegin = openBegin;
  $scope.openEnd = openEnd;
  $scope.openView = openView;
  $scope.viewAddGap = viewAddGap;
  $scope.closeAddGap = closeAddGap;

  /**
   * @ngdoc function
   * @name openView
   * @description Ouvre la boite de dialogue pour modifier ou visionner un écart
   */
  function openView() {
    $scope.showGap = true;
    $scope.date.add  = angular.copy($scope.selectedRow.date);
    saveRow = angular.copy($scope.selectedRow);
    $scope.newGap = $scope.selectedRow;
    $scope.validAddBtn = false;
  }

  /**
   * @ngdoc function
   * @name viewAddGap
   * @description Ouvre la boite de dialogue pour ajouter un écart
   */
  function viewAddGap() {
    $scope.showGap = true;
    $scope.addAction = true;
    $scope.date.add = new Date();
    $scope.newGap = {};
    $scope.selectedRow = null;
    $scope.newGap.subco = $scope.fil.subcoFilter;
    $scope.validAddBtn = false;
  }

  /**
   * @ngdoc function
   * @name closeAddGap
   * @description Ferme la boite de dialogue pour ajouter un nouvel écart
   */
  function closeAddGap (){
    var index = $scope.differences.indexOf($scope.selectedRow);
    if($scope.addAction)
      $scope.newGap = null;
    else
      $scope.differences[index] = angular.copy(saveRow);

    $scope.addAction = false;
    $scope.showGap = false;
    $scope.modifDone = false;
  }

  /**
   * @ngdoc function
   * @name addGap
   * @description Ajoute un nouvel écart
   * @params {boolean} isValid - vrai si le formulaire d'ajout est valide, faux sinon
   */
  function addGap(isValid) {
    if (isValid) {
      $scope.newGap.date = $scope.date.add;
      if($scope.user.isSubco) {
        $scope.newGap.subco = $scope.user.company;
        $scope.newGap.iddo = $scope.user.contacts[0];
      } else {
        $scope.newGap.iddo = $scope.user.company;
      }
      var newkey = 1;
      if($scope.differences_all.length > 0)
        newkey = $scope.differences_all[$scope.differences_all.length-1].key + 1;
      $scope.newGap.key = newkey;
      $scope.differences.push($scope.newGap);
      $scope.differences_all.push($scope.newGap);
      qualityService.insertData($scope.differences);
      qualityService.insertData($scope.differences_all);

      var notif = {"type":"gap_new", "date":new Date(), "title" : $scope.newGap.command, "key":newkey, "active" : "active"};
      angular.forEach($scope.users, function(elt) {
          if($scope.user.isSubco && elt.company == $scope.newGap.iddo) {
            elt.notifications.push(notif);
            usersService.insertData($scope.users);
          } else if(!$scope.user.isSubco && elt.company == $scope.newGap.subco) {
            elt.notifications.push(notif);
            usersService.insertData($scope.users);
          }
      });
      $scope.closeAddGap();
      $scope.modifDone = false;
      $scope.validAddBtn = false;
    } else {
      $scope.validAddBtn = true;
    }
  }

  /**
   * @ngdoc function
   * @name openAdd
   * @description Ouvre le datepicker d'ajout ou le ferme s'il était déjà ouvert
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openAdd($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedAdd = !$scope.openedAdd;
    $scope.date.add = new Date( $scope.date.add);
  }

  /**
   * @ngdoc function
   * @name openBegin
   * @description Ouvre le premier datepicker ou le ferme s'il l'était déjà
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openBegin($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedBegin = !$scope.openedBegin;
    $scope.openedEnd = false;
    $scope.date.begin = new Date($scope.date.begin);
    $scope.date.end = new Date($scope.date.end);
  }

  /**
   * @ngdoc function
   * @name openEnd
   * @description Ouvre le second datepicker ou le ferme s'il l'était déjà
   * @param {Event} $event - L'évenement d'ouverture
   */
  function openEnd($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedEnd = !$scope.openedEnd;
    $scope.openedBegin = false;
    $scope.date.begin = new Date($scope.date.begin);
    $scope.date.end = new Date( $scope.date.end);
  }

  /**
   * @ngdoc function
   * @name setSelectedRow
   * @description Enregistre l'écart qui est sélectionnéd dans la table des écarts
   * @param {Object} gap - L'écart à enregistrer
   */
  function setSelectedRow(gap) {
    $scope.selectedRow = ($scope.selectedRow != gap)? gap : null;
  }

  /**
   * @ngdoc function
   * @name saveGap
   * @description Enregistre les modifications de l'utilisateur sur l'écart sélectionné
   * @param {boolean} isValid - vrai si le formulaire est valide, faux sinon
   */
  function saveGap(isValid) {
    if(isValid) {
      $scope.newGap.date = angular.copy($scope.date.add);
      $scope.selectedRow = $scope.newGap;
      var notif = {
        "type": "gap_modify",
        "date": new Date(),
        "title": $scope.newGap.command,
        "key": $scope.newGap.key,
        "active": "active"
      };
      angular.forEach($scope.users, function (elt) {
        if (($scope.user.isSubco && elt.company == $scope.newGap.iddo) || (!$scope.user.isSubco && elt.company == $scope.newGap.subco)) {
          elt.notifications.push(notif);
          usersService.insertData($scope.users);
        }
      });

      qualityService.insertData($scope.differences);
      $scope.showGap = false;
      $scope.validAddBtn = false;
    } else {
      $scope.validAddBtn = true;
    }
  }

  /**
   * @ngdoc function
   * @name deleteGap
   * @description Supprime l'écart de la base de données
   */
  function deleteGap() {
    var index = $scope.differences.indexOf($scope.selectedRow);
    $scope.differences.splice(index, 1);
    var notif = {
      "type": "gap_delete",
      "date": new Date(),
      "title": $scope.selectedRow.command,
      "key": "",
      "active": "active"
    };
    angular.forEach($scope.users, function (elt) {
      if ($scope.user.isSubco && elt.company == $scope.selectedRow.iddo) {
        elt.notifications.push(notif);
        usersService.insertData($scope.users);
      } else if (!$scope.user.isSubco && elt.company == $scope.selectedRow.subco) {
        elt.notifications.push(notif);
        usersService.insertData($scope.users);
      }
    });
    qualityService.insertData($scope.differences);
    $scope.selectedRow = null;
    $scope.showGap = false;
    $scope.showModalDeleteGap(false);
  }

  /**
   * @ngdoc function
   * @name modalSuccessExport
   * @description Ouvre ou ferme la boite de dialogue pour informer l'utilisateur du succès de l'exportation
   * @param {boolean} boolean - vrai pour ouvrir la boite de dialogue, faux sinon
   */
  function modalSuccessExport(boolean) {
    $scope.successExport = boolean;
  }

  /**
   * @ngdoc function
   * @name showModalDeleteGap
   * @description Ouvre ou ferme la boite de dialogue pour confirmer la suppression d'un écart
   * @param {boolean} boolean - vrai pour ouvrir la boite de dialogue, faux sinon
   */
 function showModalDeleteGap(boolean) {
    $scope.modalDeleteGap = boolean;
  }

}]);

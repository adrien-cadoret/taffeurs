/**
 * VIF - 06/2015
 * @ngdoc factory
 * @name qualityService
 * @requires $http
 * @description Récupère et insère des données liées à la qualité
 */

app.factory('qualityService',['$http',  function($http) {
  var qualityService = {};

  //Récupération de la liste des écarts
  qualityService.getData = function (callback) {
    return $http.get('../../json/quality.json').success(callback);
  };

  //Insertion ou modification des données dans la liste des écarts
  qualityService.insertData = function (data) {
    return $http({
      url: "../../json/quality.json",
      method: "POST",
      data: data,
      headers: {'Content-Type': 'application/json'}});
  };

  return qualityService;
}]);

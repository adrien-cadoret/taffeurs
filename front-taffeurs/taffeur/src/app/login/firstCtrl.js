/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name firstCtrl
 * @requires $scope
 * @requires 'auth'
 * @requires 'usersService'
 * @requires 'globalService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $location
 * @requires $translate
 * @description Intéraction de la page de première connexion
 */

app.controller('firstCtrl',['$scope', 'auth', 'usersService', 'globalService','$pageTitle', '$filter','$location','$translate',
  function($scope, auth, usersService, globalService, $pageTitle, $filter,$location,$translate) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('FIRST_CO'));

  //On met la variable log.login à true pour l'apparence de la page
  $scope.log.login = true;

  //On récupère le langage actuel
  $scope.language = $translate.use();

  //On récupère l'identifiant de l'utilisateur actuellement connecté
  var userid = auth.userName;

  //Initialisation des variables pour changer d'étape
  $scope.step1 = true;
  $scope.step2 = false;
  $scope.step3 = false;

  //Initialisation des varibles pour controler la validité des formulaires
  $scope.pressBtn1 = false;
  $scope.pressBtn2 = false;

  //Récupération des chemins des images utilisateurs
  $scope.absolutePath = globalService.absolutePath;

  //Initialisation du mot de passe
  $scope.newPassword = '';

  //Initialisation des fonctions
  $scope.saveFile = saveFile;
  $scope.lastStep= lastStep;
  $scope.setPreviousStep = setPreviousStep;
  $scope.setNextStep = setNextStep;
  $scope.setStep = setStep;

  /**
   * @description Récupération de l'utilisateur
   */
  usersService.getUsers(function(data) {
    $scope.users = data;
    angular.forEach($scope.users, function(user) {
      if (user.company == userid) {
        $scope.user = user;
        if($scope.user.activation)
          $location.url("/");
      }
    });
  });

  /**
   * @ngdoc function
   * @name setStep
   * @description Passage d'une étape à l'autre
   * @param {boolean} isValid1 - true si le formulaire de l'étape 3 est valide, false sinon
   * @param {boolean} isValid2 - true si le formulaire de l'étape 2 est valide, false sinon
   * @param {int} num - Le numéro de l'étape à laquelle on essaye d'accéder
   */
  function setStep(isValid1, isValid2, num) {
    switch(num) {
      case 1:
        $scope.step1 = true;
        $scope.step2 = false;
        $scope.step3 = false;
        break;
      case 2:
        if(isValid1) {
          $scope.step1 = false;
          $scope.step2 = true;
          $scope.step3 = false;
          $scope.pressBtn1 = false;
        } else {
          $scope.pressBtn1 = true;
        }
        break;
      case 3:
        if(isValid1 && isValid2) {
          $scope.step1 = false;
          $scope.step2 = false;
          $scope.step3 = true;
          $scope.pressBtn2 = false;
          $scope.pressBtn1 = false;
        } else if (!isValid1) {
          $scope.setStep(null, null, 1);
          $scope.pressBtn1 = true;
        } else if (!isValid2) {
          $scope.setStep(true, null, 2);
          $scope.pressBtn2 = true;
        }

        break;
    }
  }

  /**
   * @ngdoc function
   * @name setNextStep
   * @description Passage à l'étape suivante
   * @param {boolean} isValid - true si le formulaire de l'étape précédente est valide, false sinon
   */
  function setNextStep(isValid) {
    if ($scope.step1)
      $scope.setStep(isValid, null, 2);
    else if ($scope.step2)
      $scope.setStep(true, isValid, 3);
  }

  /**
   * @ngdoc function
   * @name setPreviousStep
   * @description Retour à l'étape précédente
   */
  function setPreviousStep() {
    if($scope.step3)
      $scope.setStep(true, null, 2);
    else if($scope.step2)
      $scope.setStep(null, null, 1);
  }

  /**
   * @ngdoc function
   * @name lastStep
   * @description Dernière étape, sauvegarde des données
   */
  function lastStep() {
    $scope.user.password = $scope.newPassword;
    $scope.user.activation = true;
    usersService.insertData($scope.users);
  }

  /**
   * @ngdoc function
   * @name saveFile
   * @description Enregistre la nouvelle image de l'utilisateur
   * @params {Object} element - La nouvelle image
   */
  function saveFile(element) {
    $scope.user.img = element.files[0].name;
    var formData = new FormData();
    formData.append("file", element.files[0]);
    usersService.uploadImage(formData);
  }
}]);


/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name resetCtrl
 * @requires $scope
 * @requires 'usersService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $translate
 * @requires 'authenticationSvc'
 * @description Intéraction de la page de réinitialisation du mot de passe
 */

app.controller('resetCtrl',['$scope', 'usersService', '$pageTitle', '$filter','$translate', 'authenticationSvc',
  function($scope, usersService, $pageTitle, $filter, $translate, authenticationSvc) {

  //On interdit l'accès à la page de login si un utilisateur est déjà connecté
  if(authenticationSvc.getUserInfo() != null)
    $location.url('/');

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('RESET'));

  //On met la variable log.login à true pour l'apparence de la page
  $scope.log.login = true;

  //On récupère le langage actuel
  $scope.language = $translate.use();

  //Initialisation de la variable de réinitialisation
  $scope.reset = {};

  //Initialisation des variables de validatio
  $scope.oneReset = false;
  $scope.validMail = false;
  $scope.validUsername = false;

  //Initialisation des fonctions
  $scope.cancelReset = cancelReset;
  $scope.resetPass = resetPass;

  /**
   * @description Récupération de tous les utilisateurs
   */
  usersService.getUsers(function(data) {
    $scope.users = data;
  });

  /**
   * @ngdoc function
   * @name resetPass
   * @description Réinitialise le mot de passe
   * @param {boolean} isValid - true si le formulaire de réinitialisation est valide, false sinon
   */
  function resetPass(isValid) {
    $scope.oneReset = true;
    $scope.validUsername = false;
    angular.forEach($scope.users, function(user) {
      if(user.company == $scope.reset.username) {
        $scope.validUsername = true;
        if(user.mail == $scope.reset.mail) {
          $scope.validMail = true;
          $scope.oneReset = false;
          var newPass = randomPassword(10);
          user.password = newPass;
          usersService.insertData($scope.users);
          usersService.passwordReset({
            'userName': $scope.reset.username,
            'mail': $scope.reset.mail,
            'newPass': newPass
          });
          $scope.showModalSend = true;
        } else {
          $scope.validMail = false;
        }
      }
    });
  };
    /**
     * @ngdoc function
     * @name cancelReset
     * @description Annule la réinitialisation du mot de passe
     */
  function cancelReset() {
    $scope.oneReset = false;
    $scope.validMail = false;
    $scope.validUsername = false;
    $scope.reset.mail = '';
    $scope.reset.username= '';
  }

    /**
     * @ngdoc function
     * @name randomPassword
     * @description Créée un nouveau mot de passe aléatoirement
     * @params {int} length - Longueur du mot de passe
     */
  function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
      var i = Math.floor(Math.random() * chars.length);
      pass += chars.charAt(i);
    }
    return pass;
  }


}]);

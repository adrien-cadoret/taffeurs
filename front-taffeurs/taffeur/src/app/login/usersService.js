/**
 * VIF - 06/2015
 * @ngdoc factory
 * @name usersService
 * @requires $http
 * @description Récupère et insère les données liées aux utilisateurs
 */

app.factory('usersService',['$http',  function($http) {
  var usersService = {};

  //Récupère les utilisateurs
  usersService.getUsers = function (callback) {
    return $http.get('../../json/users.json').success(callback);
  };

  //Insère des données liées aux utilisateurs
  usersService.insertData = function (data) {
    return $http({
      url: "../../json/users.json",
      method: "POST",
      data: data,
      headers: {'Content-Type': 'application/json'}});
  };

  //Télécharge une image sur le serveur
  usersService.uploadImage = function (data) {
    return $http({
      method: 'post',
      url: "/upload",
      data: data,
      params: {},
      transformRequest: angular.identity,
      headers: {'Content-Type': undefined}
    });
  };

  //Permet l'envoi du mail lors de la réinitialisation du mot de passe (ne fonctionne pas côté serveur)
  usersService.passwordReset = function (data) {
    return $http({
      method: 'post',
      url: "/api/passwordReset",
      data: data
    });
  };

  return usersService;
}]);

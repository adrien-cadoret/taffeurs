/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name loginCtrl
 * @requires $scope
 * @requires 'usersService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $location
 * @requires 'authenticationSvc'
 * @requires $translate
 * @description Intéraction de la page de connexion
 */

app.controller('loginCtrl',['$scope', 'usersService', '$pageTitle', '$filter', '$location', 'authenticationSvc', '$translate',
  function($scope, usersService, $pageTitle, $filter, $location, authenticationSvc,$translate) {

  //On interdit l'accès à la page de login si un utilisateur est déjà connecté
  if(authenticationSvc.getUserInfo() != null)
    $location.url('/');

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('LOG_IN'));

  //On met la variable log.login à true pour l'apparence de la page
  $scope.log.login = true;

  //On récupère le langage actuel
  $scope.language = $translate.use();

  //Initialisation de la variable de connexion
  $scope.connexion = {};

  //Initialisation des variables de validation
  $scope.validUser = false;
  $scope.validPassword = false;
  $scope.oneClic = false;
  $scope.firstTime = false;
  $scope.userInfo = null;

  //Initialiasation des fonctions
  $scope.login = login;
  $scope.submitForm = submitForm;
  $scope.reset = reset;

  /**
   * @description Récupération de tous les utilisateurs
   */
  usersService.getUsers(function(data) {
    $scope.users = data;
  });

  /**
   * @ngdoc function
   * @name submitForm
   * @description Regarde la validité des identifiants et si ils sont corrects, connecte l'utilisateur
   * @param {boolean} isValid - true si le formulaire de connexion est valide, false sinon
   */
  function submitForm(isValid) {
    $scope.oneClic = true;
    if(isValid) {
      angular.forEach($scope.users, function(user) {
        if(user.company == $scope.connexion.id) {
          $scope.validUser = true;
          if(user.password == $scope.connexion.mdp) {
            $scope.validPassword = true;
            $scope.login(user.activation);
            $scope.oneClic = false;
          } else {
            $scope.validPassword = false;
          }
        }
      });
    }
  }

  /**
   * @ngdoc function
   * @name reset
   * @description Réinitialise la variable de connexion
   */
  function reset() {
    $scope.connexion = {};
  }

  /**
   * @ngdoc function
   * @name login
   * @description Connecte l'utilisateur et le redirige vers la page d'accueil
   * @param {boolean} boolean - true si le compte de l'utilisateur a déjà été activé, false sinon
   */
  function login(boolean) {
    authenticationSvc.login($scope.connexion.id, $scope.connexion.mdp)
      .then(function (result) {
        $scope.userInfo = result;
        if(boolean)
          $location.path("/");
        else
          $location.url("/firstTime");
      }, function (error) {;
        console.log(error);
      });
  }


}]);

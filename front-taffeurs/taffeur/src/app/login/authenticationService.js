/**
 * VIF - 06/2015
 * @ngdoc factory
 * @name authenticationSvc
 * @requires $http
 * @requires $q
 * @requires $window
 * @description Gère l'identification des utilisateurs
 */
app.factory("authenticationSvc", ["$http","$q","$window",function ($http, $q, $window) {
  var userInfo;

  //Connexion d'un utilisateur
  function login(userName, password) {
    var deferred = $q.defer();
    $http.post("/api/login", { userName: userName, password: password })
      .then(function (result) {
        userInfo = {
          accessToken: result.data.access_token,
          userName: result.data.userName
        };
        $window.sessionStorage["userInfo"] = JSON.stringify(userInfo);
        deferred.resolve(userInfo);
      }, function (error) {
        deferred.reject(error);
      });

    return deferred.promise;
  }

  //Déconnexion d'un utilisateur
  function logout() {
    var deferred = $q.defer();

    $http({
      method: "POST",
      url: "/api/logout",
      headers: {
        "access_token": userInfo.accessToken
      }
    }).then(function (result) {
      userInfo = null;
      $window.sessionStorage["userInfo"] = null;
      deferred.resolve(result);
    }, function (error) {
      deferred.reject(error);
    });

    return deferred.promise;
  }

  //Récupération des informations de l'utilisateur
  function getUserInfo() {
    return userInfo;
  }

  //Initialisation de la session
  function init() {
    if ($window.sessionStorage["userInfo"]) {
      userInfo = JSON.parse($window.sessionStorage["userInfo"]);
    }
  }
  init();

  return {
    login: login,
    logout: logout,
    getUserInfo: getUserInfo
  };
}]);

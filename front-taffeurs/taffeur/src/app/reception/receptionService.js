/**
 * Created by VIF - 06/2015
 * @ngdoc factory
 * @name receptionService
 * @requires $http
 * @description Service to recover the reception data.
 */

app.factory('receptionService',['$http',  function($http) {
  var receptionService = {};

  //Récupération de la liste des réceptions
  receptionService.getData = function (callback) {
    return $http.get('../../json/receptions.json').success(callback);
  };

  //Insertion ou modification des données de la liste des réceptions
  receptionService.insertData = function (data) {
    return $http({
      url: "../../json/receptions.json",
      method: "POST",
      data: data,
      headers: {'Content-Type': 'application/json'}});
  };

  return receptionService;
}]);

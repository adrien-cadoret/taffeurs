/**
 * VIF - 07/2015
 * @ngdoc controller
 * @name receptionCtrl
 * @requires $scope
 * @requires 'usersService'
 * @requires 'receptionService'
 * @requires 'globalService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $window
 * @requires 'auth'
 * @requires 'adminService'
 * @description Interaction de la page des réceptions
 */
app.controller('receptionCtrl', ['$scope', 'usersService','receptionService', '$pageTitle', '$filter', '$window','auth', 'adminService',
  function($scope, usersService, receptionService, $pageTitle, $filter, $window, auth, adminService) {

  //Set the page title
  $pageTitle.set($filter('translate')('RECEPTION'));

  //Initialisation des variables
  $scope.log.login = false;//On met la variable log.login à false pour l'apparence de la page
  var userid = auth.userName;//On récupère l'identifiant de l'utilisateur actuellement connecté
  $scope.selectedReception = null;////Variable pour enregistrer la réception sélectionnée
  $scope.sortTypeRecep = 'date';//Type de tri de la table des réception
  $scope.sortReverseRecep = false;//Ordre de tri de la table des réception
  $scope.search = {};//Variable de recherche
  $scope.search.receps = '';
  $scope.pagination = {};//Variables pour la pagination
  $scope.pagination.current = 1;//Page actuelle
  $scope.pagination.size = 10; //Nombre d'éléments par page
  var currentDo = $scope.fil.doFilter;//Récupération du donneur d'ordres actuellement sélectionné
  var currentSub = $scope.fil.subcoFilter;//Récupération du sous-traitant actuellement sélectionné
  $scope.successExport = false;//Afficher ou masquer la boite de dialogue pour informer du succès de l'exportation
  $scope.validAddBtn = false;//Controle de la validation du formulaire d'ajout d'un écart
  $scope.addAction = false;//Afficher ou masquer la boite de dialogue pour ajouter une réception
  $scope.receptions = [];//Initialisation du tableau des réceptions de l'utilisateur
  $scope.users = [];//Initialisation du tableau des utilisateurs
  $scope.date = {};//Variables de date
  $scope.date.begin = new Date();//Début de l'intervalle
  $scope.date.begin.setMonth($scope.date.begin.getMonth()-1);
  $scope.date.end = new Date();//Fin de l'intervalle
  $scope.date.add = new Date;//Date pour l'ajout d'une réception

  //Initialisation du premier jour de la semaine pour les datepickers (1 - Lundi, 0 - Dimanche)
  $scope.dateOptions1 = {
    startingDay: 1
  };

  /**
   * @description Récupération des droits utilisateurs
   */
  adminService.getData(function(data) {
    $scope.rights = data;
  });

  /**
   * @description Récupération de l'utilisateur
   */
  usersService.getUsers(function(data) {
    $scope.users = data;
    angular.forEach(data, function(user) {
      if(user.company == userid) {
        $scope.user = user;

        //Récupération de réceptions de l'utilisateur
        receptionService.getData(function(data) {
          $scope.receptions_all = data;
          angular.forEach($scope.receptions_all, function(recep) {
            //On transforme la date enregistrée en un objet date
            recep.date = new Date(recep.date);
            $scope.receptions.push(recep);
          })
        });
      }
    });
  });

    /**
     * @description On détecte quand la fenêtre est redimensionnée et on ferme les datepickers
     */
  angular.element($window).bind('resize', function () {
    $scope.openedEndRecep = false;
    $scope.openedBeginRecep = false;
    $scope.openedAddRecep = false;
    $scope.$apply();
  });

  /**
   * @description On observe si la variable date.begin change de valeur
   * Si c'est le cas, et si la nouvelle valeur est supérieur à la date de fin, on
   * met à jour la date de fin.
   */
  $scope.$watch('date.begin', function (newValue) {
    $scope.date.end = (newValue > $scope.date.end) ? newValue : $scope.date.end;
  });

  /**
   * @description On observe si la variable date.end change de valeur
   * Si c'est le cas, et si la nouvelle valeur est inférieure à la date de début, on
   * met à jour la date de début.
   */
  $scope.$watch('date.end', function (newValue) {
    $scope.date.begin = (newValue < $scope.date.begin) ? newValue : $scope.date.begin;
  });

  //Initialisation des fonctions
  $scope.modalSuccessExport =modalSuccessExport;
  $scope.setSelectedReception =setSelectedReception;
  $scope.openEndRecep =openEndRecep;
  $scope.openBeginRecep =openBeginRecep;
  $scope.openAdd = openAdd;
  $scope.addRec = addRec;
  $scope.closeAddRec = closeAddRec;
  $scope.viewAddRec = viewAddRec;

  /**
   * @ngdoc function
   * @name viewAddRec
   * @description Ouvre la boite de dialogue pour ajouter une réception
   */
  function viewAddRec() {
    $scope.showRec = true;
    $scope.addAction = true;
    $scope.date.add = new Date();
    $scope.selectedReception = {};
    $scope.selectedReception.subco = currentSub;
    $scope.selectedReception.iddo = currentDo;
    console.log($scope.selectedReception.iddo);
    $scope.validAddBtn = false;
  }

 /**
  * @ngdoc function
  * @name closeAddRec
  * @description Ferme la boite de dialogue pour ajouter une réception
  */
 function closeAddRec(){
    $scope.showRec = false;
    if($scope.addAction)
      $scope.selectedReception = null;
    $scope.addAction = false;
 }

  /**
   * @ngdoc function
   * @name addRec
   * @description Ajoute une nouvelle réception à la base de données
   * @params {boolean} isValid - vrai si le formulaire pour ajouter une réception est valide, faux sinon
   */
 function addRec(isValid){
    if(isValid) {
      $scope.selectedReception.date = $scope.date.add;
      $scope.selectedReception.data = [];
      $scope.selectedReception.validated = false;
      if($scope.user.isSubco)
        $scope.selectedReception.subco = $scope.user.company;
      else
        $scope.selectedReception.iddo = $scope.user.company;

      var newkey = 1;
      if($scope.receptions_all.length > 0)
        newkey = $scope.receptions_all[$scope.receptions_all.length-1].key + 1;
      $scope.selectedReception.key = newkey;
      var notif = {"type":"reception_new", "date":new Date(), "title" : $scope.selectedReception.command, "key":$scope.selectedReception.key, "active" : "active"};
      angular.forEach($scope.users, function(elt) {
        if(elt.company == $scope.selectedReception.iddo) {
          elt.notifications.push(notif);
          usersService.insertData($scope.users);
        }
      });
      $scope.receptions.push($scope.selectedReception);
      $scope.receptions_all.push($scope.selectedReception);
      receptionService.insertData($scope.receptions_all);

      $scope.closeAddRec();
      $scope.validAddBtn = false;
    } else {
      $scope.validAddBtn = true;
    }
 }

 /**
  * @ngdoc function
  * @name openAdd
  * @description Ouvre le datepicker d'ajout ou le ferme s'il était déjà ouvert
  * @param {Event} $event - L'évènement d'ouverture
  */
 function openAdd($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedAdd = !$scope.openedAdd;
    $scope.date.add= new Date( $scope.date.add);
 }

 /**
  * @ngdoc function
  * @name openBeginRecep
  * @description Ouvre le premier datepicker ou le ferme s'il était déjà ouvert
  * @param {Event} $event - L'évènement d'ouverture
  */
 function openBeginRecep($event) {
   $event.preventDefault();
   $event.stopPropagation();
   $scope.openedBeginRecep = !$scope.openedBeginRecep;
   $scope.openedEndRecep = false;
   $scope.date.begin = new Date($scope.date.begin);
   $scope.date.end = new Date($scope.date.end);
 }


 /**
  * @ngdoc function
  * @name openEndRecep
  * @description Ouvre le second datepicker ou le ferme s'il était déjà ouvert
  * @param {Event} $event - L'évènement d'ouverture
  */
 function openEndRecep($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedEndRecep = !$scope.openedEndRecep;
    $scope.openedBeginRecep = false;
    $scope.date.begin = new Date($scope.date.begin);
    $scope.date.end = new Date( $scope.date.end);
 }

 /**
  * @ngdoc function
  * @name setSelectedReception
  * @description Enregistre la réception qui est sélectionnée dans la table des réceptions
  * @param {Object} reception - La réception à enregistrer
  */
 function setSelectedReception(reception) {
   $scope.selectedReception =  ($scope.selectedReception != reception)? reception : null;
 }

 /**
  * @ngdoc function
  * @name modalSuccessExport
  * @description Ouvre ou ferme la boite de dialogue pour informer l'utilisateur du succès de l'exportation
  * @param {boolean} boolean - vrai pour ouvrir la boite de dialogue, faux sinon
  */
 function modalSuccessExport(boolean) {
   $scope.successExport = boolean;
 }
}]);

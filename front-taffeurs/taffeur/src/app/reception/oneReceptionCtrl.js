/**
 * VIF - 07/2015
 * @ngdoc controller
 * @name oneReceptionCtrl
 * @requires $scope
 * @requires $routeParams
 * @requires 'usersService'
 * @requires 'receptionService'
 * @requires 'globalService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $window
 * @requires 'auth'
 * @requires 'adminService'
 * @requires $location
 * @description Interaction de la page de visualisation d'une réception
 */

app.controller('oneReceptionCtrl', ['$scope',  '$routeParams', 'usersService','receptionService', 'globalService', '$pageTitle', '$filter', '$window', 'auth', 'adminService', '$location',
  function($scope, $routeParams, usersService, receptionService,  globalService, $pageTitle, $filter, $window, auth, adminService, $location) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('RECEPTION'));

  //On met la variable log.login à false pour l'apparence de la page
  $scope.log.login = false;

  //On récupére l'identifiant de l'utilisateur connecté
  var userid =auth.userName;

  //Initialisation des variables de tri
  $scope.sortType = 'lot';
  $scope.sortReverse = false;

  //Initialisation des variables de pagination
  $scope.pagination = {};
  $scope.pagination.current = 1;
  $scope.pagination.size = 10;

  //Initialisation des tableaux pour stocker les réceptions
  $scope.reception = {};
  $scope.receptions = [];

  //Initialisation des variables de recherche
  $scope.search = {};
  $scope.search.recep = '';
  $scope.changeRecep = false;

  //Initialisation de la variable pour afficher ou masquer la boite de dialogue sur l'exportation
  $scope.successExport = false;

  //Initialisation du premier jour de la semaine pour les datepickers (1 - Lundi, 0 - Dimanche)
  $scope.dateOptions1 = {
    startingDay: 1
  };

  //Initialisation des variables pour modifier la réception
  $scope.change = false;
  $scope.changeDone = false;
  $scope.saveBtn = false;

  //Initialisation des fonctions
  $scope.showModalDeleteLine = showModalDeleteLine;
  $scope.showModalDeleteRecep = showModalDeleteRecep;
  $scope.exportRec = exportRec;
  $scope.getBootstroPlacement =getBootstroPlacement;
  $scope.validateReception = validateReception;
  $scope.deleteReception = deleteReception;
  $scope.setChange = setChange;
  $scope.hideChangeDone = hideChangeDone;
  $scope.saveChange = saveChange;
  $scope.openDate = openDate;
  $scope.removeData = removeData;
  $scope.saveData = saveData;
  $scope.addData = addData;

  /**
   * @description Détecte quand la fenêtre est redimensionnée et ferme le datepicker
   */
  angular.element($window).bind('resize', function () {
    $scope.openedAdd = false;
    $scope.$apply();
  });

  /**
   * @description Récupération des droits utilisateur
   */
  adminService.getData(function(data) {
    $scope.rights = data;
  });

  /**
   * @description Récupération de l'utilisateur et de la réception
   */
  usersService.getUsers(function(data) {
    angular.forEach(data, function(user) {
      if(user.company == userid) {
        $scope.user = user;

        /**
         * @description Recovery of the reception
         */
        receptionService.getData(function(data) {
          angular.forEach(data, function(reception) {
            reception.date = new Date(reception.date);
            if(($scope.user.isSubco && reception.subco == $scope.user.company) || (!$scope.user.isSubco && reception.iddo == $scope.user.company)) {
              if (reception.key == parseInt($routeParams.numRec)) {
                $scope.reception = reception;
              }
            }
            $scope.receptions.push(reception);
          });
          if($scope.reception == null)
            $location.url("/reception");
        });

      }
    });
  });

  /**
   * @ngdoc function
   * @name addData
   * @description Ajoute des données à la réception
   */
  function addData(){
    var unit1 = $scope.reception.unit;
    var unit2 = $scope.reception.unit;
    if($scope.reception.data.length > 0) {
      unit1 = $scope.reception.data[$scope.reception.data.length - 1].unit1;
      unit2 = $scope.reception.data[$scope.reception.data.length - 1].unit2;
    }
    $scope.inserted = {"lot":"","quantity1":0,"unit1":unit1,"quantity2":0
      ,"unit2":unit2};
    $scope.reception.data.push($scope.inserted);
    receptionService.insertData($scope.receptions);
  }

  /**
   * @ngdoc function
   * @name removeData
   * @description Supprime des données dans la réception
   */
 function removeData() {
    $scope.reception.data.splice($scope.saveIndex, 1);
    receptionService.insertData($scope.receptions);
    $scope.showModalDeleteLine(false, null);
  }

  /**
   * @ngdoc function
   * @name saveData
   * @description Sauvegarde les modifications sur les données de la table
   */
  function saveData() {
    receptionService.insertData($scope.receptions);
  }

  /**
   * @ngdoc function
   * @name openDate
   * @description Ouvre le  datepicker ou le ferme s'il était déjà ouvert
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openDate($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedAdd = !$scope.openedAdd;
    $scope.reception.date = new Date($scope.reception.date);
  }

      /**
   * @ngdoc function
   * @name saveChange
   * @description Sauvegarde les changements sur la réception
   * @param {boolean} isValid - true si le formulaire de modification est valide, false sinon.
   */
  function saveChange(isValid) {
    if(isValid) {
      receptionService.insertData($scope.receptions);
      var notif = {
        "type": "reception_edit",
        "date": new Date(),
        "title": $scope.reception.command,
        "key": $scope.reception.key,
        "active": "active"
      };
      angular.forEach($scope.users, function (elt) {
        if (($scope.user.isSubco && (elt.company == $scope.reception.iddo))
          || (!$scope.user.isSubco && (elt.company == $scope.reception.subco))) {
          elt.notifications.push(notif);
          usersService.insertData($scope.users);
        }
      });
      $scope.change = false;
      $scope.changeDone = true;
      $scope.saveBtn = false;
    } else {
      $scope.saveBtn = true;
    }
  }

  /**
   * @ngdoc function
   * @name hideChangeDone
   * @description Masque le message informant l'utilisateur qu'un changement à été réalisé
   */
  function hideChangeDone() {
    $scope.changeDone = false;
  }

  /**
   * @ngdoc function
   * @name setChange
   * @description Une modification a été réalisée sur la réception
   */
  function setChange() {
    $scope.change = true;
    $scope.changeDone = false;
  }

      /**
   * @ngdoc function
   * @name deleteReception
   * @description Supprime la réception dans la base de données
   */
  function deleteReception() {
    var index = $scope.receptions.indexOf($scope.reception);
    $scope.receptions.splice(index, 1);
    var notif = {"type":"reception_delete", "date":new Date(), "title" : $scope.reception.command, "key":"", "active" : "active"};
    angular.forEach($scope.users, function(elt) {
      if($scope.user.isSubco && elt.company == $scope.reception.iddo) {
        elt.notifications.push(notif);
        usersService.insertData($scope.users);
      } else if(!$scope.user.isSubco && elt.company == $scope.reception.subco) {
        elt.notifications.push(notif);
        usersService.insertData($scope.users);
      }
    });
    receptionService.insertData($scope.receptions);
    $scope.showModalDeleteRecep(false);
  }

  /**
   * @ngdoc function
   * @name saveChange
   * @description Valide la réception
   */
  function validateReception() {
    $scope.reception.validated = true;
    $scope.saveChange();
    receptionService.insertData($scope.receptions);
    var notif = {"type":"reception_close", "date":new Date(), "title" : $scope.reception.command, "key":$scope.reception.key, "active" : "active"};
    angular.forEach($scope.users, function(elt) {
      if(elt.company == $scope.reception.iddo) {
        elt.notifications.push(notif);
        usersService.insertData($scope.users);
      }
    });
    $scope.showModalValidation = false;
  }

  /**
   * @ngdoc function
   * @name getBootstroPlacement
   * @description Détermine l'emplacement de la bulle d'aide par rapport à la taille de la fenêtre
   * @return {string} L'emplacement de la bulle d'aide
   */
  function getBootstroPlacement(){
    return ($window.innerWidth  < 1200)? 'bottom' : 'right';
  }

  /**
   * @ngdoc function
   * @name exportRec
   * @description Affiche ou masque la boite de dialogue d'exportation
   * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon
   */
 function exportRec(boolean) {
    $scope.successExport = boolean;
  }

  /**
   * @ngdoc function
   * @name showModalDeleteRecep
   * @description Affiche ou masque la boite de dialogue de suppression de la réception
   * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon
   */
  function showModalDeleteRecep(boolean) {
    $scope.showModalDelete = boolean;
  }

  /**
   * @ngdoc function
   * @name showModalDeleteRecep
   * @description Affiche ou masque la boite de dialogue de suppression d'une ligne de l'inventaire
   * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon
   * @param {int} index - L'index de la ligne à supprimer
   */
    function showModalDeleteLine(boolean, index) {
      $scope.saveIndex = index;
      $scope.modalDeleteLine = boolean;
    }
}]);

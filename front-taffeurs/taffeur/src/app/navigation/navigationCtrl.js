/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name navigationCtrl
 * @requires $scope
 * @requires $location
 * @requires $window
 * @requires 'globalService'
 * @requires 'usersService'
 * @requires $route
 * @requires $timeout
 * @requires $translate
 * @requires 'tmhDynamicLocale'
 * @requires $routeParams
 * @requires $filter
 * @requires 'authenticationSvc'
 * @requires $anchorScroll
 * @description Interaction des barres de navigation
 */

app.controller("navigationCtrl", ['$scope', '$location','$window', 'globalService', 'usersService', '$route', '$timeout', '$translate',
  'tmhDynamicLocale', '$routeParams', '$filter', 'authenticationSvc', '$anchorScroll',
  function ($scope, $location, $window, globalService, usersService,$route, $timeout, $translate,
            tmhDynamicLocale,$routeParams,$filter, authenticationSvc,$anchorScroll) {

  //Initialisation des variables pour l'apparence de la page
  $scope.log.login = false;
  $scope.log.admin = false;

  //Initialisation d'une variable pour l'apparence des barres de menu
  $scope.petit = false;

  //Récupération de l'utilisateur connecté
  if(authenticationSvc.getUserInfo() != null)
    $scope.company = authenticationSvc.getUserInfo().userName;

  //Récupération du chemin vers l'image de l'utilisateur
  $scope.absolutePath= globalService.absolutePath;

  //Récupération de la liste des unités
  $scope.units = globalService.getUnit;

  //Initialisation des varibles pour afficher et masquer les sous-menu
  $scope.stockMenu = false; //menu Stock interface classique
  $scope.openRights = false; //menu Droits interface administrateur

  //Initialisation des variables pour l'interface administrateur
  $scope.adm = {};
  $scope.adm.language = $translate.use();

  //Initialisation de l'aide
  bootstro.start();
  bootstro.stop();

 //Initialisation des fonctions
  $scope.saveFixed = saveFixed;
  $scope.saveColor = saveColor;
  $scope.changeLanguage = changeLanguage;
  $scope.goToTop = goToTop;
  $scope.logout = logout;
  $scope.setRead_All = setRead_All;
  $scope.setRead_OneType = setRead_OneType;
  $scope.setRead_OneNot = setRead_OneNot;
  $scope.majNot = majNot;
  $scope.menuClass = menuClass;
  $scope.switchLanguage = switchLanguage;
  $scope.startHelp = startHelp;
  $scope.reload = reload;

  /**
   * @description Récupération de l'utilisateur
   */
  usersService.getUsers(function (data) {
    $scope.users = data;
    angular.forEach($scope.users, function (user) {
      if (user.company == $scope.company) {
        $scope.user = user;
        if(!$scope.user.activation) {
          $location.url("/firstTime");
        }
        if($scope.user.company == 'Administrateur') {
          if($location.path() != '/rights/custo' && $location.path() != '/rights/subco' && $location.path() != '/traduction')
            $location.url("/users");
          $scope.log.admin = true;
          $scope.changeLanguage(user.settings.language);
        }
        $scope.settings = user.settings;
        $scope.color.header = $scope.settings.horMenu;
        $scope.color.leftbar = $scope.settings.vertMenu;
        $scope.changeColor($scope.color.header, $scope.color.leftbar);
        $scope.header.fixed = $scope.settings.fixed;
        $scope.notifs = user.notifications;
        if ($scope.user.isSubco)
          $scope.fil.subcoFilter = $scope.user.company;
        else
          $scope.fil.doFilter = $scope.user.company;
        $scope.switchLanguage($scope.settings.language);
        $scope.majNot();
      }
    });
  });

  /**
   * @ngdoc function
   * @name switchLanguage
   * @description Change le langage courrant du portail
   * @param {string} key - La clé du langage
   */
  function switchLanguage(key) {
    $translate.use(key);
    tmhDynamicLocale.set(key);
    moment.locale(key);
    $scope.majNot();
  }

  function menuClass(page) {
    var current = $location.path().substring(1);
    var currentSplit = current.split('/');
    var classe = "";
    angular.forEach(currentSplit, function (chemin) {
      if (chemin == page) {
        classe = "active";
      }
    });
    return classe;
  }

  /**
   * @ngdoc function
   * @name majNot
   * @description Met à jour le calcul et l'affichage des notifications
   */
  function majNot() {
    $scope.notifications = [];
    $scope.nonRead = 0;
    $scope.notOrder = 0;
    $scope.notStock = 0;
    $scope.notQuality = 0;
    $scope.notReception = 0;
    $scope.notEvent = 0;
    $scope.notContact = 0;
    angular.forEach($scope.notifs, function (notification) {
      notification.from = moment(notification.date).fromNow(true);
      if ((moment().diff(new Date(notification.date), 'days') > (parseInt($scope.settings.numOfDays)-1)) ||
        ((notification.type == 'order' || notification.type == 'entry' || notification.type == 'output') && $scope.settings.orders == '0') ||
        ((notification.type == 'inventory_close' ||notification.type == 'inventory_new') && $scope.settings.stock == '0') ||
        (notification.type == 'gap' && $scope.settings.quality == '0') ||
        (notification.type == 'reception' && $scope.settings.reception == '0') ||
        (notification.type == 'event' && $scope.settings.calendar == '0')) {
        notification.active = 'out';
      } else {
        if (notification.type == 'order_new') {
          if (notification.active == 'active')
            $scope.notOrder++;
          notification.text = $filter('translate')('NEW_PO_NOT');
          notification.icon = 'fa-file-text';
          notification.href = '#/orders/' + notification.key;
          notification.class = 'order';
        } else if (notification.type == 'order_delete') {
          if (notification.active == 'active')
            $scope.notOrder++;
          notification.text = $filter('translate')('DELETE_PO',{ number: notification.title});
          notification.icon = 'fa-file-text';
          notification.href = '#/orders' ;
          notification.class = 'order';
        } else if (notification.type == 'order_edit') {
          if (notification.active == 'active')
            $scope.notOrder++;
          notification.text = $filter('translate')('EDIT_MO',{ number: notification.title});
          notification.icon = 'fa-file-text';
          notification.href = '#/orders/' + notification.key;
          notification.class = 'order';
        }else if (notification.type == 'entry') {
          if (notification.active == 'active')
            $scope.notOrder++;
          notification.text = $filter('translate')('ENTRY_VALIDATED',{ number: notification.title});
          notification.icon = 'fa-sign-in';
          notification.href = '#/orders/' +notification.key;
          notification.class = 'order';
        } else if (notification.type == 'output') {
          if (notification.active == 'active')
            $scope.notOrder++;
          notification.text = $filter('translate')('OUTPUT_VALIDATED',{ number: notification.title});
          notification.icon = 'fa-sign-out';
          notification.href = '#/orders/' +notification.key;
          notification.class = 'order';
        } else if (notification.type == 'inventory_close') {
          if (notification.active == 'active')
            $scope.notStock++;
          notification.text = $filter('translate')('INVENTORY_CLOSE',{ number: notification.title});
          notification.icon = 'fa-tasks';
          notification.href = '#/stock/inventories/' +notification.key;
          notification.class = 'warning';
        } else if (notification.type == 'inventory_new') {
          if (notification.active == 'active')
            $scope.notStock++;
          notification.text = $filter('translate')('NEW_INVENTORY_NOT',{ title: notification.title});
          notification.icon = 'fa-tasks';
          notification.href = '#/stock/inventories/' + notification.key;
          notification.class = 'warning';
        } else if (notification.type == 'inventory_delete') {
          if (notification.active == 'active')
            $scope.notStock++;
          notification.text = $filter('translate')('DELETE_INVENTORY_NOT',{ title: notification.title});
          notification.icon = 'fa-tasks';
          notification.href = '#/stock/inventories/' + notification.key;
          notification.class = 'warning';
        }else if (notification.type == 'inventory_edit') {
          if (notification.active == 'active')
            $scope.notStock++;
          notification.text = $filter('translate')('INVENTORY_EDIT',{ title: notification.title});
          notification.icon = 'fa-tasks';
          notification.href = '#/stock/inventories/' + notification.key;
          notification.class = 'warning';
        } else if (notification.type == 'gap_new') {
          if (notification.active == 'active')
            $scope.notQuality++;
          notification.text = $filter('translate')('GAP_DECLARED');
          notification.icon = 'fa-thumbs-up';
          notification.href = '#/quality';
          notification.class = 'danger';
        } else if (notification.type == 'gap_modify') {
          if (notification.active == 'active')
            $scope.notQuality++;
          notification.text = $filter('translate')('GAP_MODIFY');
          notification.icon = 'fa-thumbs-up';
          notification.href = '#/quality';
          notification.class = 'danger';
        } else if (notification.type == 'gap_delete') {
          if (notification.active == 'active')
            $scope.notQuality++;
          notification.text = $filter('translate')('GAP_DELETE');
          notification.icon = 'fa-thumbs-up';
          notification.href = '#/quality';
          notification.class = 'danger';
        } else if (notification.type == 'reception_new') {
          if (notification.active == 'active')
            $scope.notReception++;
          notification.text = $filter('translate')('NEW_RECEPTION',{ number: notification.title});
          notification.icon = 'fa-truck';
          notification.href = '#/reception/' +notification.key;
          notification.class = 'success';
        } else if (notification.type == 'reception_delete') {
          if (notification.active == 'active')
            $scope.notReception++;
          notification.text = $filter('translate')('RECEPTION_DELETED',{ number: notification.title});
          notification.icon = 'fa-truck';
          notification.href = '#/reception/' +notification.key;
          notification.class = 'success';
        }else if (notification.type == 'reception_edit') {
          if (notification.active == 'active')
            $scope.notReception++;
          notification.text = $filter('translate')('RECEPTION_EDIT',{ number: notification.title});
          notification.icon = 'fa-truck';
          notification.href = '#/reception/' +notification.key;
          notification.class = 'success';
        } else if (notification.type == 'reception_close') {
          if (notification.active == 'active')
            $scope.notReception++;
          notification.text = $filter('translate')('RECEPTION_VALIDATED',{ number: notification.title});
          notification.icon = 'fa-truck';
          notification.href = '#/reception/' +notification.key;
          notification.class = 'success';
        } else if (notification.type == 'event_new') {
          if (notification.active == 'active')
            $scope.notEvent++;
          notification.text = $filter('translate')('EVENT_NEW',{ title: notification.title});
          notification.icon = 'fa-calendar-o';
          notification.href = '#/calendar';
          notification.class = 'calendar';
        } else if (notification.type == 'event_modify') {
        if (notification.active == 'active')
          $scope.notEvent++;
        notification.text = $filter('translate')('EVENT_MODIFY',{ title: notification.title});
        notification.icon = 'fa-calendar-o';
        notification.href = '#/calendar';
        notification.class = 'calendar';
        } else if (notification.type == 'event_delete') {
        if (notification.active == 'active')
          $scope.notEvent++;
        notification.text = $filter('translate')('EVENT_DELETE',{ title: notification.title});
        notification.icon = 'fa-calendar-o';
        notification.href = '#/calendar';
        notification.class = 'calendar';
        } else if (notification.type == 'contact_new') {
          if (notification.active == 'active')
            $scope.notContact++;
          notification.text = $filter('translate')('NEW_CONTACT_NOT',{ name: notification.title});
          notification.icon = 'fa-user';
          notification.href = '#/account';
          notification.class = 'contact';
        } else if (notification.type == 'contact_edit') {
          if (notification.active == 'active')
            $scope.notContact++;
          notification.text = $filter('translate')('EDIT_CONTACT_NOT',{ name: notification.title});
          notification.icon = 'fa-user';
          notification.href = '#/account';
          notification.class = 'contact';
        } else if (notification.type == 'contact_delete') {
          if (notification.active == 'active')
            $scope.notContact++;
          notification.text = $filter('translate')('DELETE_CONTACT_NOT',{ name: notification.title});
          notification.icon = 'fa-user';
          notification.href = '#/account';
          notification.class = 'contact';
        }
        $scope.notifications.push(notification);
      }
    });
    $scope.nonRead = $scope.notOrder + $scope.notStock + $scope.notQuality + $scope.notReception + $scope.notEvent + $scope.notContact;
  }

  /**
   * @ngdoc function
   * @name setRead_OneNot
   * @description Marque une notification comme lue et met à jour les calculs
   * @param {Object} notification - La notification à marquer comme vue
   */
  function setRead_OneNot(notification) {
    notification.active = '';
    $scope.nonRead--;
    if (notification.type == 'order' || notification.type=='entry' || notification.type=='output'  || notification.type=='order_new'  || notification.type=='order_delete' || notification.type=='order_edit')
      $scope.notOrder--;
    else if (notification.type == 'inventory_close' || notification.type == 'inventory_new'  || notification.type=='inventory_delete' || notification.type=='inventory_edit' )
      $scope.notStock--;
    else if (notification.type == 'gap_new' || notification.type == 'gap_modify' || notification.type == 'gap_delete')
      $scope.notQuality--;
    else if (notification.type == 'reception_close' || notification.type == 'reception_new' || notification.type == 'reception_delete' || notification.type == 'reception_edit')
      $scope.notReception--;
    else if (notification.type == 'event_new' || notification.type == 'event_modify' || notification.type == 'event_delete')
      $scope.notEvent--;
    else if (notification.type == 'contact_new' || notification.type == 'contact_edit' || notification.type == 'contact_delete')
      $scope.notContact--;
    usersService.insertData($scope.users);
  }

  /**
   * @ngdoc function
   * @name setRead_OneType
   * @description Marque un type de notification comme vu et met à jour les calculs
   * @param {Object} type - Le type de notification à marquer comme vu
   */
  function setRead_OneType(type) {
    if (type == 'orders') {
      $scope.nonRead = $scope.nonRead - $scope.notOrder;
      $scope.notOrder = 0;
    } else if (type == 'inventories')  {
      $scope.nonRead = $scope.nonRead - $scope.notStock;
      $scope.notStock = 0;
    } else if (type == 'quality') {
      $scope.nonRead = $scope.nonRead - $scope.notQuality;
      $scope.notQuality = 0;
    } else if (type == 'reception') {
      $scope.nonRead = $scope.nonRead - $scope.notReception;
      $scope.notReception = 0;
    } else if (type == 'calendar') {
      $scope.nonRead = $scope.nonRead - $scope.notEvent;
      $scope.notEvent = 0;
    } else if (type == 'account') {
      $scope.nonRead = $scope.nonRead - $scope.notContact;
      $scope.notContact = 0;
    }
    angular.forEach($scope.notifications, function(notification) {
      if(notification.type == type
        || (type=='orders' && (notification.type=='entry' || notification.type=='output' || notification.type=='order_new' || notification.type=='order_delete' || notification.type=='order_edit'))
        || (type=='reception' && (notification.type=='reception_close' || notification.type=='reception_new' || notification.type=='reception_delete' || notification.type == 'reception_edit'))
        || (type=='inventories' && (notification.type=='inventory_close' || notification.type=='inventory_new' || notification.type=='inventory_delete' || notification.type=='inventory_edit'))
        || (type=='quality' && (notification.type == 'gap_new' || notification.type == 'gap_modify' || notification.type == 'gap_delete'))
        || (type=='account' && (notification.type == 'contact_new' || notification.type == 'contact_edit' || notification.type == 'contact_delete'))
        || (type=='calendar' && (notification.type == 'event_new' || notification.type == 'event_modify' || notification.type == 'event_delete'))) {
        notification.active = '';
      }
    });
    usersService.insertData($scope.users);
  }

  /**
   * @ngdoc function
   * @name setRead_All
   * @description Marque toutes les notifications comme vues et met à jour les calculs
   */
  function setRead_All() {
    angular.forEach($scope.notifications, function(notification) {
      notification.active = '';
    });
    $scope.nonRead = 0;
    $scope.notOrder = 0;
    $scope.notStock = 0;
    $scope.notQuality = 0;
    $scope.notReception = 0;
    $scope.notEvent = 0;
    $scope.notContact = 0;
    usersService.insertData($scope.users);
  }

  /**
   * @ngdoc function
   * @name reload
   * @description Recharge la page courante
   */
  function reload() {
    $timeout(function () {
      $route.reload();
    }, 1);
  }

  /**
   * @ngdoc function
   * @name logout
   * @description Déconnecte l'utilisateur et redirige vers la page de connexion
   */
  function logout() {
    $scope.changeColor('normal','normal');
    authenticationSvc.logout()
      .then(function () {
        $scope.userInfo = null;
        $location.url("/login");
      }, function (error) {
        $location.url("/login"); //Provisoire à cause du problème de déconnexion en https
        console.log(error);
      });
  }

  /**
   * @ngdoc function
   * @name startHelp
   * @description Lance l'aide à l'utilisation sur la page courrante
   */
  function startHelp() {
    bootstro.start();
  }

  /**
   * @ngdoc function
   * @name goToTop
   * @description Redirige vers le haut de la page
   */
  function goToTop() {
    $location.hash('page-heading');
    $anchorScroll();
  }

  /**
   * @ngdoc function
   * @name changeLanguage
   * @description Enregistre le changement de langage de l'administrateur
   * @param {string} key - La clé du nouveau langage
   */
  function changeLanguage(key) {
    if(key != "en-gb")
      $scope.adm.language = key;
    else
      $scope.adm.language = "gb";
    $scope.user.settings.language = key;
    usersService.insertData($scope.users);
  }

  /**
   * @ngdoc function
   * @name saveColor
   * @description Enregistre le changement de couleur pour l'administrateur
   * @param {string} header - La couleur de la barre horizontale
   * @param {string} leftbar - La couleur de la barre verticale
   */
  function saveColor(header, leftbar) {
    $scope.settings.vertMenu = leftbar;
    $scope.settings.horMenu = header;
    $scope.color.leftbar = leftbar;
    $scope.color.header = header;
    usersService.insertData($scope.users);
    $scope.changeColor(header, leftbar);
  }

  /**
   * @ngdoc function
   * @name saveFixed
   * @description Enregistre le changement de paramètre pour la fixation du menu pour l'administrateur
   */
  function saveFixed() {
    $scope.settings.fixed = $scope.header.fixed;
    usersService.insertData($scope.users);
  }
}]);

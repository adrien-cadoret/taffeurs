/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name scheduleCtrl
 * @requires $scope
 * @requires 'scheduleService'
 * @requires 'usersService'
 * @requires $window
 * @requires $pageTitle
 * @requires $filter
 * @requires 'auth'
 * @requires $timeout
 * @requires $interval
 * @requires 'adminService'
 * @description S'occupe de l'interaction de la page du planning des ordres de fabrication
 */

app.controller('scheduleCtrl', ['$scope', 'scheduleService', 'usersService', '$window', '$pageTitle', '$filter', 'auth', '$timeout', '$interval', 'adminService',
  function($scope, scheduleService, usersService, $window,  $pageTitle, $filter, auth,$timeout,$interval,adminService) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('ORDERS'));

  //Initialisation des variables
  $scope.log.login = false;  //On met la variable log.login à false pour l'apparence de la page
  var userid = auth.userName; //On récupère l'identifiant de l'utilisateur actuellement connecté
  $scope.date = {}; //Variables de date pour les datepickers
  $scope.date.begin = new Date(); //Début de l'intervalle
  $scope.date.begin.setMonth($scope.date.begin.getMonth()-1);
  $scope.date.end = new Date(); //Fin de l'intervalle
  $scope.pagination = {}; //Variables pour la pagination
  $scope.pagination.current = 1; //Page actuelle
  $scope.pagination.size = 10; //Nombre d'éléments par page
  $scope.ordres_all = []; //Tous les ordres de fabrication
  $scope.ordres = []; //Seulement les ordres de fabrication de l'utilisateur
  $scope.import = []; //Les ordres de fabrication à importer de l'utilisateur
  $scope.search = {}; //Variable de recherche
  $scope.search.orders = '';
  $scope.sortType = 'date'; //Type de tri de la table des ordres de fabrication
  $scope.sortReverse = false; //Ordre de tri dans la table des ordres de fabrication
  $scope.selectedOrder = null;//Enregistrer l'ordre sélectionné
  $scope.importPO = false;//Afficher ou masquer la boite de dialogue d'importation des OF
  $scope.successExport = false;//Afficher ou masquer la boite de dialogue pour informer du succès de l'exportation
  $scope.nothingToExport = false;//Afficher ou masquer la boite de dialogue pour informer qu'il n'y a rien à exporter
  $scope.noImportData = false;//Initialisation de la variable pour afficher ou masquer le message comme quoi il n'y a pas de données à exporter

  //Initialisation du premier jour de la semaine pour les datepickers (1 - Lundi, 0 - Dimanche)
  $scope.dateOptions1 = {
    startingDay: 1
  };

  /**
   * @description Récupération des droits utilisateurs
   */
  adminService.getData(function(data) {
    $scope.rights = data;
  });

  /**
   * @description Récupération de l'utilisateur
   */
  usersService.getUsers(function(data) {
    $scope.users = data;
    angular.forEach(data, function(user) {
      if(user.company == userid) {
        $scope.user = user;

        //Récupération des ordres de fabrication lié à l'utilisateur
        scheduleService.getData(function(data) {
          $scope.ordres_all = data;
          angular.forEach($scope.ordres_all, function(ordre) {
            //On transforme la date enregistrée en un objet date
            ordre.date = new Date(ordre.date);
            $scope.ordres.push(ordre);
          });
        });

      }
    });
  });

  /**
   * @description On détecte quand la fenêtre est redimensionnée et on ferme les datepickers
   */
  angular.element($window).bind('resize', function () {
    $scope.openedEnd = false;
    $scope.openedBegin = false;
    $scope.$apply();
  });

  /**
   * @description On observe si la variable date.begin change de valeur
   * Si c'est le cas, et si la nouvelle valeur est supérieur à la date de fin, on
   * met à jour la date de fin.
   */
  $scope.$watch('date.begin', function (newValue) {
    $scope.date.end = (newValue > $scope.date.end) ? newValue : $scope.date.end;
  });

  /**
   * @description On observe si la variable date.end change de valeur
   * Si c'est le cas, et si la nouvelle valeur est inférieure à la date de début, on
   * met à jour la date de début.
   */
  $scope.$watch('date.end', function (newValue) {
    $scope.date.begin = (newValue < $scope.date.begin) ? newValue : $scope.date.begin;
  });

  //Initialisation des fonctions
  $scope.setSelectedOrder = setSelectedOrder;
  $scope.openBegin = openBegin;
  $scope.openEnd = openEnd;
  $scope.successExportModal = successExportModal;
  $scope.nothingNewModal = nothingNewModal;
  $scope.hideImportPO = hideImportPO;
  $scope.exportPO = exportPO;
  $scope.openImport = openImport;
  $scope.addImportPO = addImportPO;


  /**
   * @ngdoc function
   * @name setSelectedOrder
   * @description On enregistre l'ordre de fabrication qui est sélectionné dans la table
   * @param {Object} order - L'ordre de fabrication à enregistrer
   */
  function setSelectedOrder(order) {
    $scope.selectedOrder =  ($scope.selectedOrder != order)? order : null;
  }

  /**
   * @ngdoc function
   * @name openBegin
   * @description Ouvre le premier datepicker ou le ferme s'il était déjà ouvert
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openBegin($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedBegin = !$scope.openedBegin;
    $scope.openedEnd = false;
    $scope.date.begin = new Date($scope.date.begin);
    $scope.date.end = new Date($scope.date.end);
  }

  /**
   * @ngdoc function
   * @name openEnd
   * @description Ouvre le second datepicker ou le ferme s'il était déjà ouvert
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openEnd($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedEnd = !$scope.openedEnd;
    $scope.openedBegin = false;
    $scope.date.begin = new Date($scope.date.begin);
    $scope.date.end = new Date( $scope.date.end);
  }

  /**
   * @ngdoc function
   * @name openImport
   * @description Récupère les ordres de fabrication à importer et ouvre la boite de dialogue pour importer les OF
   */
  function openImport() {
    $scope.importPO = true;
    $scope.endProgress = false;

        var cleanTab = []; //Permet de vider le fichier import.json d'une fois à l'autre des OF déjà importés

    //Appel au serveur pour récupérer le fichier .csv avec le nom de l'utilisateur et remplir le fichier json des
    //ordres de fabrication à importer
    scheduleService.makeJSON({'userName' : $scope.user.company});
    $scope.progress = 0;
    var chronos = [];
    var exist = false;

    //Mise en place d'un compteur pour faire avancer la barre de progression
    var interval = $interval(function() {
      $scope.progress = $scope.progress + 10;
    }, 100);

    //L'utilisateur attend deux secondes durant lesquelles on récupère les ordres de fabrication à importer
    $timeout(function() {
      scheduleService.getImportData(function(data) {
        $scope.import_all = data;
        angular.forEach($scope.import_all, function(elt) {
          exist = false;
          //On regarde si l'OF n'a pas déjà été importé => impossible de modifier l'OF dans VIF et de le réimporter
          angular.forEach($scope.ordres, function(ordre) {
            if(ordre.num == elt.chrono) {
              exist = true;
            }
          });
          //Si l'OF n'a pas déjà été importé on regarde s'il appartient bien à l'utilisateur et on l'ajoute au tableau
          //des OF à importer
          if(!exist) {
            cleanTab.push(elt);
            if (chronos.indexOf(elt.chrono) < 0) {
              if (elt.iddo == $scope.user.company) {
                //On initialise le sous-traitant de l'OF à nul
                elt.subco = '';
                $scope.import.push(elt);
                chronos.push(elt.chrono);
              }
            }
          }
          $scope.noImportData = ($scope.import.length == 0);
        });

        //On supprime du fichier import.json les OF déjà importés
        $scope.import_all = cleanTab;
        scheduleService.cleanImportData($scope.import_all);
      });
      //On cache la barre de progression pour afficher le tableau des OF à importer
      $scope.endProgress = true;

      $interval.cancel(interval);
    }, 2000);
  }

  /**
   * @ngdoc function
   * @name addImportPO
   * @description Importe les nouveaux ordres de fabrication qui ont été assignés à un sous-traitant
   */
  function addImportPO() {
    var newImport = [];
    var newKey = -1;
    var newOrder = {};
    var newEntOut = {};
    var newData = {};
    var rangCpt = 0;
    var saveArticle = [];
    var getUnit2 = [];

    //Pour chaque ordre de fabrication du tableau des OF à importer
    angular.forEach($scope.import, function(elt) {
      newOrder = {};
      saveArticle = [];

      //Si l'utilisateur à renseigné un sous-traitant
      if(elt.subco != "") {
        //On commence par enregistrer les informations d'ordre général sur l'OF
        newKey = $scope.ordres_all[$scope.ordres_all.length-1].key + 1;
        newOrder.key = newKey;
        newOrder.iddo = elt.iddo;
        newOrder.subco = elt.subco;
        var date = elt.date.split("/");
        //Format français : la date du fichier à importer doit toujours être de la forme JJ/MM/AAAA
        newOrder.date = new Date(date[2],date[1]-1,date[0]);
        newOrder.priority = elt.priority;
        newOrder.num = elt.chrono;
        newOrder.article = elt.article;
        newOrder.lib = elt.label;
        newOrder.quantity = parseFloat(elt.quantity1);
        newOrder.unit = $filter('uppercase')(elt.unit1);
        newOrder.quantity2 = parseFloat(elt.quantity2);
        getUnit2 = elt.unit2.split('/');
        newOrder.unit2 = $filter('uppercase')(getUnit2[0]);
        newOrder.entries_valid = false;
        newOrder.outputs_valid = false;
        newOrder.entries = [];
        newOrder.outputs = [];

        //On re-parcourt tous les ordres de fabrication, pour renseigner les entrées et les sorties
        angular.forEach($scope.import_all, function(elt2) {
          newEntOut = {};
          rangCpt = 0;
          if((elt2.chrono == elt.chrono) && (saveArticle.indexOf(elt2.rang) < 0)) {
            newEntOut.rang = elt2.rang;
            newEntOut.lib = elt2.labelArticle;
            newEntOut.article = elt2.code;
            saveArticle.push(elt2.rang);
            newEntOut.quantity1 = 0;
            newEntOut.quantity2 = 0;
            newEntOut.data= [];

            //On parcourt une troisième fois tous les ordres de fabrication pour renseigner tous les lots de chaque entrée/sortie
            angular.forEach($scope.import_all, function(elt3) {
              if((elt3.chrono == elt2.chrono) && (elt3.code == elt2.code)) {
                newEntOut.quantity1 = newEntOut.quantity1 + parseFloat(elt3.quantity1_art);
                newEntOut.unit1 = $filter('uppercase')(elt3.unit1_art);
                newEntOut.quantity2 = newEntOut.quantity2 + parseFloat(elt3.quantity2_art);
                if(newEntOut.quantity2 > 0) {
                  getUnit2 = elt3.unit2.split('/');
                  newEntOut.unit2 = $filter('uppercase')(getUnit2[0]);
                } else {
                  newEntOut.unit2 = "";
                }
                newData = {};
                rangCpt = rangCpt + 1;
                newData.rang = rangCpt;
                newData.lot = elt3.lot;
                newData.quantity1 = parseFloat(elt3.quantity1_art);
                newData.unit1 = $filter('uppercase')(elt3.unit1_art);
                newData.quantity2 = parseFloat(elt3.quantity2_art);
                if(newData.quantity2 > 0) {
                  getUnit2 = elt3.unit2.split('/');
                  newData.unit2 = $filter('uppercase')(getUnit2[0]);
                } else {
                  newData.unit2 = "";
                }
                newEntOut.data.push(newData);
              }
            });
            if(elt2.type == "ENT") {
              newOrder.entries.push(newEntOut);
            } else {
              newOrder.outputs.push(newEntOut);
            }
          }
        });
        //On ajoute l'OF à la liste de l'utilisateur
        $scope.ordres.push(newOrder);
        $scope.ordres_all.push(newOrder);
        scheduleService.insertData($scope.ordres_all);

        //On créée une notification pour le sous-traitant de l'OF
        var notif = {"type":"order_new", "date":new Date(), "title": newOrder.num, "key" : newOrder.key,  "active" : "active"};
        //On cherche le sous-traitant dans la liste des utilisateurs et on lui ajoute la notification
        angular.forEach($scope.users, function(elt) {
          if(elt.company == newOrder.subco) {
            elt.notifications.push(notif);
            usersService.insertData($scope.users);
          }
        });
      } else {
        //On conserve l'OF même si aucun sous-traitant en lui a été assigné
        newImport.push(elt);
      }
    });
    $scope.import = newImport; //Mise à jour du tableau des éléments à importer (on supprime les éléments importés)
    //On ferme la boite de dialogue s'il ne reste plus aucun OF à importer, sinon on la laisse ouverte.
    $scope.importPO = $scope.import.length != 0;
  }

  /**
   * @ngdoc function
   * @name exportPO
   * @description Exporte les ordres de fabrication qui ont les entrées et les sorties validées
   */
  function exportPO() {
    $scope.nothingToExport = true;
    var exportArray = [];
    var exportObject = [];
    exportObject = ['société','établissement','prechro','chrono','type article','n° article','code article prévu','code article réel','lot réel','quantité 1 déclarée','unité 1 déclarée','quantité 2 déclarée','unité 2 déclarée'];
    exportArray.push(exportObject);

    //On parcourt le tableau de tous les ordres de fabrication
    angular.forEach($scope.ordres, function(ordre) {

      //Si l'ordre de fabrication n'a pas déjà été exporté et si ses entrées et sorties ont été validées
      if(!ordre.export && ordre.entries_valid && ordre.outputs_valid) {

        //On créée une ligne pour chaque lot de chaque entrée
        angular.forEach(ordre.entries, function(entry){
          angular.forEach(entry.data, function(data) {
            exportObject = [$scope.user.society, $scope.user.establishment,'1OF', ordre.num, 'ENT',entry.rang, entry.article,entry.article,entry.lib, data.lot, data.quantity1, data.unit1, data.quantity2, data.unit2];
            exportArray.push(exportObject);
            $scope.nothingToExport = false;
          });
        });

        //Puis une ligne pour chaque lot de chaque sortie
        angular.forEach(ordre.outputs, function(output){
          angular.forEach(output.data, function(data) {
            exportObject = [$scope.user.society, $scope.user.establishment,'1OF', ordre.num, 'SOR',output.rang, output.article,output.article,output.lib, data.lot, data.quantity1, data.unit1, data.quantity2, data.unit2];
            exportArray.push(exportObject);
            $scope.nothingToExport = false;
          });
        });

        ordre.export = true;
      }
    });
    //On enregistre l'ordre de fabrication pour ne pas le ré-exporter de nouveau
    scheduleService.insertData($scope.ordres_all);

    if(!$scope.nothingToExport) {
      //On appelle le service pour exporter les données
      scheduleService.exportData({array: exportArray, date: $filter('date')(new Date(), 'dd-MM-yyyy HH:mm:ss')});
      //On affiche la boite de dialogue pour indiquer à l'utilisateur que l'exportation s'est bien déroulée
      $scope.successExportModal(true);
    }
  }

  /**
   * @ngdoc function
   * @name nothingNewModal
   * @description Ouvre ou ferme la boite de dialogue pour informer l'utilisateur qu'il n'y a rien à exporter
   * @param {boolean} boolean - vrai si on veut ouvrir la boite de dialogue, faux sinon
   */
  function nothingNewModal(boolean) {
    $scope.nothingToExport = boolean;
  }

  /**
   * @ngdoc function
   * @name nothingNewModal
   * @description Ouvre ou ferme la boite de dialogue pour informer l'utilisateur que l'exportation s'est bien déroulée
   * @param {boolean} boolean - vrai si on veut ouvrir la boite de dialogue, faux sinon
   */
  function successExportModal(boolean) {
    $scope.successExport = boolean;
  }

  /**
   * @ngdoc function
   * @name hideImportPO
   * @description Ferme la boite de dialogue pour importer les ordres de fabrication
   */
  function hideImportPO() {
    $scope.importPO = false;
    //Resetting the table of manufacturing orders to be imported
    $scope.import = [];
  }


}]);

/**
 * VIF - 06/2015
 * @ngdoc factory
 * @name scheduleService
 * @description Récupère et insère des données liées aux ordres de fabrication
 */

app.factory('scheduleService', ['$http', function($http) {
  var scheduleService = {};

  //Récupèration des ordres de fabrication
  scheduleService.getData = function (callback) {
    return $http.get('../../json/orders.json').success(callback);
  };

  //Appel à la création d'un fichier json avec les données à importer
  scheduleService.makeJSON = function(data) {
    return $http({
      url: "/api/import",
      method: "POST",
      data: data});
  };

  //Récupération des données à importer
  scheduleService.getImportData = function (callback) {
    return $http.get('../../json/import.json').success(callback);
  };

  //Modification du fichier des importations pour supprimer les OF qui ont déjà été importés
  scheduleService.cleanImportData = function (data) {
    return $http({
      url: "../../json/import.json",
      method: "POST",
      data: data,
      headers: {'Content-Type': 'application/json'}});
  };

  //Insertion (ou modification) de données de la liste des OF
  scheduleService.insertData = function (data) {
    return $http({
      url: "../../json/orders.json",
      method: "POST",
      data: data,
      headers: {'Content-Type': 'application/json'}});
  };

  //Exportation des ordres de fabrication
  scheduleService.exportData = function (data) {
    return $http({
      url: "../../json/exportOrd",
      method: "POST",
      data: data});
  };

  return scheduleService;
}]);

/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name orderCtrl
 * @requires $scope
 * @requires $routeParams
 * @requires 'scheduleService'
 * @requires 'usersService'
 * @requires 'globalService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $window
 * @requires 'auth'
 * @requires 'adminService'
 * @requires $location
 * @description Interaction de la page de visualisation d'un ordre de fabrication
 */

app.controller('orderCtrl', ['$scope', '$routeParams', 'scheduleService', 'usersService', 'globalService', '$pageTitle', '$filter', '$window', 'auth', 'adminService', '$location',
  function($scope, $routeParams, scheduleService, usersService, globalService,$pageTitle, $filter, $window, auth, adminService,$location ) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('PO'));

  //On met la variable log.login à false pour l'apparence de la page
  $scope.log.login = false;

  //Récupération de l'identifiant de l'utilisateur actuel
  var userid = auth.userName;

  //Variables pour naviguer entre les deux onglets
  $scope.entriesTab = true;
  $scope.outputsTab = false;

  //Initialisation des variables de recherche
  $scope.search = {};
  $scope.search.entries = '';
  $scope.search.outputs = '';

  //Initialisation des variables de tri
  $scope.sortType_Entry = 'rang';
  $scope.sortReverse_Entry = false;
  $scope.sortType_Output = 'rang';
  $scope.sortReverse_Output = false;

  $scope.validEntriesBtn = false;
  $scope.validOutputBtn = false;
  $scope.nothingToExport = false;
  $scope.change = false;
  $scope.changeDone = false;
  $scope.saveBtn = false;
  $scope.order = null;
  $scope.user = {};
  $scope.settings= {};
  $scope.ordres = [];

  //Initialisation du premier jour de la semaine pour les datepickers (1 - Lundi, 0 - Dimanche)
  $scope.dateOptions1 = {
    startingDay: 1
  };

  //Initialisation des fonctions
  $scope.classEntry = classEntry;
  $scope.classOutput = classOutput;
  $scope.openDate = openDate;
  $scope.setSelectedEntry = setSelectedEntry;
  $scope.setSelectedOutput = setSelectedOutput;
  $scope.addEntry = addEntry;
  $scope.addOutput = addOutput;
  $scope.saveTheOrder = saveTheOrder;
  $scope.hideChangeDone = hideChangeDone;
  $scope.saveEntry = saveEntry;
  $scope.saveOutput = saveOutput;
  $scope.deletePO = deletePO;
  $scope.deleteEntry = deleteEntry;
  $scope.deleteOutput = deleteOutput;
  $scope.setChange = setChange;
  $scope.validateEntries = validateEntries;
  $scope.validateOutputs = validateOutputs;
  $scope.quantitySum = quantitySum;
  $scope.addDataEntry = addDataEntry;
  $scope.saveDataEntry = saveDataEntry;
  $scope.removeDataEntry = removeDataEntry;
  $scope.addDataOutput = addDataOutput;
  $scope.saveDataOutput = saveDataOutput;
  $scope.removeDataOutput = removeDataOutput;
  $scope.getBootstroPlacement = getBootstroPlacement;
  $scope.exportOrd = exportOrd;
  $scope.closeNothingNew = closeNothingNew;
  $scope.setShowModalPO = setShowModalPO;
  $scope.showModalValidEntries = showModalValidEntries;
  $scope.showModalValidOutputs = showModalValidOutputs;
  $scope.showModalDeleteEntry = showModalDeleteEntry;
  $scope.showModalDeleteOutput = showModalDeleteOutput;
  $scope.showModalDeleteDataEntry = showModalDeleteDataEntry;
  $scope.showModalDeleteDataOutput = showModalDeleteDataOutput;

  /**
   * @description Récupération des droits
   */
  adminService.getData(function(data) {
    $scope.rights = data;
  });

  /**
   * @description Détecte quand la fenêtre est redimensionnée et ferme le datepicker s'il était ouvert
   */
  angular.element($window).bind('resize', function () {
    $scope.openedDate = false;
    $scope.$apply();
  });

  /**
   * @description Récupération de l'utilisateur et de l'ordre de fabrication à visualiser
   */
  usersService.getUsers(function(data) {
    $scope.users = data;
    angular.forEach(data, function(user) {
      if(user.company == userid) {
        $scope.user = user;
        $scope.settings = user.settings;

        scheduleService.getData(function(data) {
          angular.forEach(data, function(ordre) {
            ordre.date = new Date(ordre.date);
            if(($scope.user.isSubco && ordre.subco == $scope.user.company) || (!$scope.user.isSubco && ordre.iddo == $scope.user.company)) {
              if (ordre.key == parseInt($routeParams.nof)) {
                $scope.order = ordre;
                $scope.saveOrder = angular.copy(ordre);
                angular.forEach($scope.order.entries, function (entry) {
                  entry.error = false;
                  angular.forEach(entry.data, function(dat){
                    dat.quantity1 = parseFloat(dat.quantity1);
                    dat.quantity2 = parseFloat(dat.quantity2);
                  });
                });
                if ($scope.order.entries.length > 0)
                  $scope.selectedEntry = $scope.order.entries[0];
                else
                  $scope.selectedEntry = null;
                $scope.newEntry = $scope.selectedEntry;
                angular.forEach($scope.order.outputs, function (output) {
                  output.error = false;
                  angular.forEach(output.data, function(dat){
                    dat.quantity1 = parseFloat(dat.quantity1);
                    dat.quantity2 = parseFloat(dat.quantity2);
                  });
                });
                if ($scope.order.outputs.length > 0)
                  $scope.selectedOutput = $scope.order.outputs[0];
                else
                  $scope.selectedOutput = null;
                $scope.newOutput = $scope.selectedOutput;
              }
            }
            $scope.ordres.push(ordre);
          });
          //Si l'utilisateur essaye d'accéder à un ordre de fabrication qui n'existe pas ou qui ne lui appartient pas,
          // on le redirige vers la page des ordres de fabrication
          if($scope.order == null)
            $location.url("/orders");
        });
      }
    });
  });

  /**
   * @ngdoc function
   * @name classEntry
   * @description Détermine la classe de l'entrée
   * @param {Object} entry - L'entrée concernée
   * @returns {String} - Le nom de la classe
   */
  function classEntry(entry) {
    if($scope.selectedEntry == entry) {
      //l'entrée est sélectionnée
      return 'tiles-inverse';
    } else if (entry.error==true) {
      //il y a une erreur sur l'entrée
      return 'tiles-danger';
    } else {
      return 'tiles-success';
    }
  }

  /**
   * @ngdoc classOutput
   * @name classOutput
   * @description Détermine la classe de la sortie
   * @param {Object} output - La sortie concernée
   * @returns {String} - Le nom de la classe
   */
  function classOutput(output) {
    if($scope.selectedOutput == output) {
      //La sortie est sélectionnée
      return 'tiles-inverse';
    } else if (output.error==true) {
      //il y a une erreur sur la sortie
      return 'tiles-danger';
    } else {
      return 'tiles-success';
    }
  }

  /**
   * @ngdoc function
   * @name openDate
   * @description Ouvre le datepicker ou le ferme s'il était déjà ouvert
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openDate($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedDate = !$scope.openedDate;
    $scope.order.date = new Date($scope.order.date);
  }

  /**
   * @ngdoc function
   * @name setSelectedEntry
   * @description Enregistre l'entrée sélectionnée
   * @param {Object} entry - L'entrée à enregistrer
   */
  function setSelectedEntry(entry) {
    $scope.selectedEntry = ($scope.selectedEntry != entry)? entry : null;
    $scope.newEntry = $scope.selectedEntry;
  }

  /**
   * @ngdoc function
   * @name setSelectedOutput
   * @description Enregistre la sortie sélectionnée
   * @param {Object} output - La sortie à enregistrer
   */
  function setSelectedOutput(output) {
    $scope.selectedOutput = ($scope.selectedOutput != output)? output : null;
    $scope.newOutput = $scope.selectedOutput;
  }

  /**
   * @ngdoc function
   * @name addEntry
   * @description Ajouter une entrée
   * @param {boolean} isValid - true si le formulaire pour ajouter une entrée est valide, false sinon
   */
  function addEntry(isValid) {
    if(isValid) {
      $scope.validEntriesBtn=false;
      $scope.newEntry.data = [];
      $scope.order.entries.push($scope.newEntry);
      scheduleService.insertData($scope.ordres);
      $scope.selectedEntry = $scope.newEntry;
      $scope.addDataEntry();
    } else {
      $scope.validEntriesBtn = true;
    }
  }

    /**
     * @ngdoc function
     * @name addOutput
     * @description Ajouter une sortie
     * @param {boolean} isValid - true si le formulaire pour ajouter une sortie est valide, false sinon
     */
    function addOutput(isValid) {
      if(isValid) {
        $scope.validOutputBtn=false;
        $scope.newOutput.data = [];
        $scope.order.outputs.push($scope.newOutput);
        scheduleService.insertData($scope.ordres);
        $scope.selectedOutput = $scope.newOutput;
        $scope.addDataOutput();
      } else {
        $scope.validOutputBtn=true;
      }
    }

  /**
   * @ngdoc function
   * @name saveTheOrder
   * @description Enregistre les modifications sur l'ordre de fabrication
   * @param {boolean} isValid - true si le formulaire pour modifier l'ordre est valide, false sinon
   */
  function saveTheOrder(isValid) {
    if(isValid) {
      scheduleService.insertData($scope.ordres);
      var notif = {
        "type": "order_edit",
        "date": new Date(),
        "title": $scope.order.num,
        "key": $scope.order.key,
        "active": "active"
      };
      angular.forEach($scope.users, function (elt) {
        if (($scope.user.isSubco && elt.company == $scope.order.iddo)
          || (!$scope.user.isSubco && elt.company == $scope.order.subco)) {
          elt.notifications.push(notif);
          usersService.insertData($scope.users);
        }
      });

      $scope.change = false;
      $scope.changeDone = true;
      $scope.saveBtn = false;
    } else {
      $scope.saveBtn = true;
    }
  }

  /**
   * @ngdoc function
   * @name hideChangeDone
   * @description Masque le message informant l'utilisateur qu'un changement à été réalisé
   */
  function hideChangeDone() {
    $scope.changeDone = false;
  }

  /**
   * @ngdoc function
   * @name saveEntry
   * @description Enregistre les données de l'entrée
   */
  function saveEntry() {
    if($scope.selectedEntry != null)
      scheduleService.insertData($scope.ordres);
  }

  /**
   * @ngdoc function
   * @name saveOutput
   * @description Enregistre les données de la sortie
   */
  function saveOutput() {
    if($scope.selectedOutput != null)
      scheduleService.insertData($scope.ordres);
  }

  /**
   * @ngdoc function
   * @name deletePO
   * @description Supprime l'ordre de fabrication
   */
  function deletePO(){
    var newindex = $scope.ordres.indexOf($scope.order);
    var notif = {"type":"order_delete", "date":new Date(), "title" : $scope.order.num,"key" :"",  "active" : "active"};
    angular.forEach($scope.users, function(elt) {
      if(elt.company == $scope.order.subco) {
        elt.notifications.push(notif);
        usersService.insertData($scope.users);
      }
    });

    if(newindex > -1) {
      $scope.ordres.splice(newindex, 1);
    }
    scheduleService.insertData($scope.ordres);
  }

  /**
   * @ngdoc function
   * @name deleteEntry
   * @description Supprime l'entrée sélectionnée
   */
  function deleteEntry() {
    var indexe = $scope.order.entries.indexOf($scope.selectedEntry);
    $scope.order.entries.splice(indexe,1);
    var numerotation = 1;
    angular.forEach($scope.order.entries, function(data) {
      data.rang = numerotation;
      numerotation += 1;
    });
    scheduleService.insertData($scope.ordres);
    $scope.newEntry = null;
    $scope.selectedEntry = null;
    $scope.showModalDeleteEntry(false);
  }

  /**
   * @ngdoc function
   * @name deleteOutput
   * @description Supprime la sortie sélectionnée
   */
  function deleteOutput() {
    var indexo = $scope.order.outputs.indexOf($scope.selectedOutput);
    $scope.order.outputs.splice(indexo,1);
    var numerotation = 1;
    angular.forEach($scope.order.outputs, function(data) {
      data.rang = numerotation;
      numerotation += 1;
    });
    scheduleService.insertData($scope.ordres);
    $scope.newOutput = null;
    $scope.selectedOutput = null;
    $scope.showModalDeleteOutput(false);
  }

  /**
   * @ngdoc function
   * @name setChange
   * @description Une modification a été réalisée sur l'ordre de fabrication
   */
  function setChange() {
      $scope.change = true;
      $scope.changeDone = false;
   }

  /**
   * @ngdoc function
   * @name validateEntries
   * @description Valide les entrées de l'ordre
   */
  function validateEntries() {
    $scope.showModalEntries = false;
    $scope.order.entries_valid = true;
    scheduleService.insertData($scope.ordres);
    var notif = {"type":"entry", "date":new Date(),  "title":$scope.order.num, "key" : $scope.order.key,  "active" : "active"};
    angular.forEach($scope.users, function(elt) {
      if(elt.company == $scope.order.iddo) {
        elt.notifications.push(notif);
        usersService.insertData($scope.users);
      }
    });
    $scope.showModalValidEntries(false);
  }

   /**
   * @ngdoc function
   * @name validateOutputs
   * @description Valide les sorties de l'ordre
   */
  function validateOutputs() {
    $scope.showModalOutputs = false;
    $scope.order.outputs_valid = true;
    scheduleService.insertData($scope.ordres);
    var notif = {"type":"output", "date":new Date(), "title":$scope.order.num, "key" : $scope.order.key,  "active" : "active"};
    angular.forEach($scope.users, function(elt) {
      if(elt.company == $scope.order.iddo) {
        elt.notifications.push(notif);
        usersService.insertData($scope.users);
      }
    });
    $scope.showModalValidOutputs(false);
  }

  /**
   * @ngdoc function
   * @name quantitySum
   * @description Calcule la somme des quantité déclarées
   * @param {Object} element - L'entrée ou la sortie dont on a besoin de calculer la somme des quantités
   * @returns {int} La somme calculée
   */
  function quantitySum(element) {
    var sum = 0;
    angular.forEach(element.data, function(data) {
      sum = parseFloat(sum) + parseFloat(data.quantity1);
    });
    return sum;
  }

  /**
   * @ngdoc function
   * @name addDataEntry
   * @description Ajoute une donnée à l'entrée sélectionnée
   */
  function addDataEntry(){
    var newRang = 1;
    var unit1 = $scope.selectedEntry.unit;
    var unit2 = $scope.selectedEntry.unit;
    if($scope.selectedEntry.data.length > 0) {
      newRang = $scope.selectedEntry.data[$scope.selectedEntry.data.length - 1].rang + 1;
      unit1 = $scope.selectedEntry.data[$scope.selectedEntry.data.length - 1].unit1;
      unit2 = $scope.selectedEntry.data[$scope.selectedEntry.data.length - 1].unit2;
    }
    $scope.inserted = {"rang" : newRang, "lot":"","quantity1":0,"unit1":unit1,"quantity2":0,"unit2":unit2};
    $scope.selectedEntry.data.push($scope.inserted);
  }

  /**
   * @ngdoc function
   * @name saveDataEntry
   * @description Enregistre les modifications effectuées sur une donnée de l'entrée
   * @param {int} data - La donnée à enregistrer
   */
  function saveDataEntry(data) {
    $scope.selectedEntry.error = (data.lot == '' || data.quantity1 == '' || data.quantity1 == 0 || data.quantity1 == null);
    scheduleService.insertData($scope.ordres);
  }

  /**
   * @ngdoc function
   * @name removeDataEntry
   * @description Supprime la donnée qui est sélectionnée de l'entrée
   */
  function removeDataEntry() {
    $scope.selectedEntry.data.splice($scope.saveIndexEntry, 1);
    var numerotation = 1;
    angular.forEach($scope.selectedEntry.data, function(data) {
      data.rang = numerotation;
      numerotation += 1;
    });
    scheduleService.insertData($scope.ordres);
    $scope.showModalDeleteDataEntry(false, null);
  }

  /**
   * @ngdoc function
   * @name addDataOutput
   * @description Ajoute une donnée à la sortie sélectionnée
   */
  function addDataOutput(){
    var newRang = 1;
    var unit1 = $scope.selectedEntry.unit;
    var unit2 = $scope.selectedEntry.unit;
    if($scope.selectedOutput.data.length > 0) {
      newRang = $scope.selectedOutput.data[$scope.selectedOutput.data.length - 1].rang + 1;
      unit1 = $scope.selectedOutput.data[$scope.selectedOutput.data.length - 1].unit1;
      unit2 = $scope.selectedOutput.data[$scope.selectedOutput.data.length - 1].unit2;
    }
    $scope.inserted = {"rang" : newRang, "lot":"","quantity1":0,"unit1":unit1,"quantity2":0,"unit2":unit2, "contenant":"", "contenantPere":""};
    $scope.selectedOutput.data.push($scope.inserted);
  }

  /**
   * @ngdoc function
   * @name saveDataOutput
   * @description Enregistre les modifications de la donnée de la sortie
   * @param {int} data - La donnée à enregistrer
   */
  function saveDataOutput(data) {
    $scope.selectedOutput.error = (data.lot == '' || data.quantity1 <= 0 || data.quantity1 == null);
    scheduleService.insertData($scope.ordres);
  }

  /**
   * @ngdoc function
   * @name removeDataOutput
   * @description Supprime la donnée sélectionnée de la sortie
   */
  function removeDataOutput() {
    $scope.selectedOutput.data.splice($scope.saveIndexOutput, 1);
    var numerotation = 1;
    angular.forEach($scope.selectedOutput.data, function(data) {
      data.rang = numerotation;
      numerotation += 1;
    });
    scheduleService.insertData($scope.ordres);
    $scope.showModalDeleteDataOutput(false, null);
  }

  /**
   * @ngdoc function
   * @name getBootstroPlacement
   * @description Détermine l'emplacement de la bulle d'aide en fonction de la taille de la fenêtre
   * @return {string} L'emplacement de la bulle d'aide
   */
  function getBootstroPlacement(){
    return ($window.innerWidth  < 1200)? 'bottom' : 'right';
  }

  /**
   * @ngdoc function
   * @name exportOrd
   * @description Exporte l'ordre de fabrication
   */
  function exportOrd() {
    $scope.nothingToExport = true;
    var exportArray = [];
    var exportObject = [];
    exportObject = ['société','établissement','prechro','chrono','type article','n° article','code article prévu','code article réel','lot réel','quantité 1 déclarée','unité 1 déclarée','quantité 2 déclarée','unité 2 déclarée'];
    exportArray.push(exportObject);

    if(!$scope.order.export) {
      angular.forEach($scope.order.entries, function(entry){
        angular.forEach(entry.data, function(data) {
          exportObject = [$scope.user.society, $scope.user.establishment,'1OF', $scope.order.num, 'ENT',entry.rang, entry.article,entry.article,entry.lib, data.lot, data.quantity1, data.unit1, data.quantity2, data.unit2];
          exportArray.push(exportObject);
          $scope.nothingToExport = false;
        });
      });
      angular.forEach($scope.order.outputs, function(output){
        angular.forEach(output.data, function(data) {
          exportObject = [$scope.user.society, $scope.user.establishment,'1OF', $scope.order.num, 'SOR',output.rang, output.article,output.article,output.lib, data.lot, data.quantity1, data.unit1, data.quantity2, data.unit2];
          exportArray.push(exportObject);
          $scope.nothingToExport = false;
        });
      });
      $scope.order.export = true;
    }

    scheduleService.insertData($scope.ordres_all);
    if(!$scope.nothingToExport)
      scheduleService.exportData({array : exportArray,date: $filter('date')(new Date(), 'dd-MM-yyyy HH:mm:ss')});
  }

  /**
   * @ngdoc function
   * @name closeNothingNew
   * @description Masque la boite de dialogue qui informe l'utilisateur qu'il n'y a rien de nouveau à exporter
   */
  function closeNothingNew() {
    $scope.nothingToExport = false;
  }

  /**
   * @ngdoc function
   * @name setShowModalPO
   * @description Affiche ou masque la boite de dialogue pour supprimer l'ordre de fabrication
   * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon
   */
 function setShowModalPO(boolean) {
    $scope.showModalPO = boolean;
  }

  /**
   * @ngdoc function
   * @name showModalValidEntries
   * @description Affiche ou masque la boite de dialogue pour valider les entrées
   * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon save
   */
 function showModalValidEntries(boolean) {
    $scope.showModalEntries = boolean;
 }
    /**
     * @ngdoc function
     * @name setSelectedEntry
     * @description Affiche ou masque la boite de dialogue pour valider les sorties
     * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon save
     */
  function showModalValidOutputs(boolean) {
    $scope.showModalOutputs = boolean;
  }
    /**
     * @ngdoc function
     * @name setSelectedEntry
     * @description Affiche ou masque la boite de dialogue pour supprimer une entrée
     * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon save
     */
  function showModalDeleteEntry(boolean) {
    $scope.modalDeleteEntry = boolean;
  }
    /**
     * @ngdoc function
     * @name setSelectedEntry
     * @description Affiche ou masque la boite de dialogue pour supprimer une sortie
     * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon save
     */
  function showModalDeleteOutput(boolean) {
    $scope.modalDeleteOutput = boolean;
  }
    /**
     * @ngdoc function
     * @name setSelectedEntry
     * @description Affiche ou masque la boite de dialogue pour supprimer une donnée de l'entrée
     * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon save
     * @param {int} index - l'index de la donnée à supprimer

     */
  function showModalDeleteDataEntry(boolean, index) {
    $scope.saveIndexEntry = index;
    $scope.modalDeleteDataEntry = boolean;
  }

  /**
   * @ngdoc function
   * @name setSelectedEntry
   * @description Affiche ou masque la boite de dialogue pour supprimer une donnée de la sortie
   * @param {boolean} boolean - true pour afficher la boite de dialogue, false sinon save
   * @param {int} index - l'index de la donnée à supprimer
   */
  function showModalDeleteDataOutput(boolean, index) {
    $scope.saveIndexOutput = index;
    $scope.modalDeleteDataOutput = boolean;
  }

}]);



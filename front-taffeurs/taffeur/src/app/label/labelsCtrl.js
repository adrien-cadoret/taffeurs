/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name labelsCtrl
 * @requires $scope
 * @requires 'usersService'
 * @requires 'auth'
 * @requires $pageTitle
 * @requires $filter
 * @description Interaction de la page sur les étiquettes
 */

app.controller('labelsCtrl', ['$scope', 'usersService', 'auth', '$pageTitle', '$filter',
  function($scope, usersService, auth, $pageTitle, $filter) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('ETIQUETTES'));

  //On met la variable log.login à false pour l'apparence de la page
  $scope.log.login = false;

  //On récupère l'identifiant de l'utilisateur actuellement connecté
  var userid =  auth.userName;


  /**
   * @description Récupération des données de l'utilisateur
   */
  usersService.getUsers(function(data) {
    angular.forEach(data, function(user) {
      if (user.company == userid)
        $scope.user = user;
    });
  });

}]);

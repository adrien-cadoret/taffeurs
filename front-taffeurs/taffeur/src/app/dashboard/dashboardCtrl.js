/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name dashboardCtrl
 * @requires $scope
 * @requires 'ordersService'
 * @requires 'scheduleService'
 * @requires 'usersService'
 * @requires 'globalService'
 * @requires 'inventoriesService'
 * @requires 'qualityService'
 * @requires 'receptionService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $window
 * @requires 'auth'
 * @description Interaction du tableau de bord
 */

app.controller('dashboardCtrl', ['$scope', 'scheduleService', 'usersService', 'globalService', 'inventoriesService', 'qualityService', 'receptionService',
  '$pageTitle', '$filter', '$window','auth',
  function($scope, scheduleService, usersService, globalService, inventoriesService, qualityService, receptionService, $pageTitle, $filter, $window, auth) {

  //On définit le titre de la page
  $pageTitle.set('VIF');

  //On met la variable log.login à false pour l'apparence de la page
  $scope.log.login = false;

  //Récupération de l'identifiant de l'utilisateur connecté
  var userid =  auth.userName;

  //Initialisation  des variables de date
  $scope.date={};
  $scope.date.begin = $filter('date')(new Date(), 'shortDate');
  $scope.date.end = new Date();

  //Initialisation des variables pour la pagination
  $scope.pagination = {};
  $scope.pagination.currentOrders = 1;
  $scope.pagination.currentRecep = 1;
  $scope.pagination.currentInv = 1;
  $scope.pagination.currentQua = 1;
  $scope.pagination.size = 5;

  //Initialisation des tableaux de donnée
  $scope.filteredPO = [];
  $scope.filteredInv = [];
  $scope.filteredQua = [];
  $scope.filteredRec = [];
  $scope.nbCons1 = [];
  $scope.nbFab1 = [];
  $scope.nbCons2 = [];
  $scope.nbFab2 = [];
  $scope.orders = [];
  $scope.inventories = [];
  $scope.differences = [];
  $scope.receptions = [];
  $scope.notifs = [];

  //Initialisation des variables pour trier les tables
  $scope.sortType = 'date';
  $scope.sortReverse = false;
  $scope.sortTypeInventories = 'date';
  $scope.sortReverseInventories = false;
  $scope.sortTypeQua = 'date';
  $scope.sortReverseQua = false;
  $scope.sortTypeRecep = 'date';
  $scope.sortReverseRecep = false;

  //Initialisation du premier jour de la semaine pour les datepickers (1 - Lundi, 0 - Dimanche)
  $scope.dateOptions1 = {
    startingDay: 1
  };

  //Initialisation des fonctions
  $scope.cancel = cancel;
  $scope.open = open;
  $scope.openBegin = openBegin;
  $scope.openEnd = openEnd;
  $scope.setDate = setDate;

  /**
   * @description Récupération de l'utilisateur et de ses données
   */
  usersService.getUsers(function(data) {
    angular.forEach(data, function(user) {
      if(user.company == userid) {
        $scope.user = user;
        $scope.settings = user.settings;
        $scope.notifs = user.notifications;
        if(user.isSubco)
          $scope.fil.subcoFilter = user.company;
        else
          $scope.fil.doFilter = user.company;

        $scope.setDate(parseInt($scope.settings.format));

        /**
         * @description Récupértion des ordres de fabrications
         */
        scheduleService.getData(function(data) {
          angular.forEach(data, function(ordre) {
              ordre.date = new Date(ordre.date);
              $scope.orders.push(ordre);
          });
        });

        /**
         * @description Récupération des inventaires non cloturés
         */
        inventoriesService.getData(function(data) {
          angular.forEach(data, function(inv) {
            if(inv.clos == 'Non') {
                inv.date = new Date(inv.date);
                $scope.inventories.push(inv);
            }
          });
        });

        /**
         * @description Récupération des écarts
         */
        qualityService.getData(function(data) {
          angular.forEach(data, function(elt) {
              elt.date = new Date(elt.date);
              $scope.differences.push(elt);
          });
        });

        /**
         * @description Récupération des réceptions
         */
        receptionService.getData(function(data) {
          angular.forEach(data, function(recep) {
            recep.date = new Date(recep.date);
            $scope.receptions.push(recep);
          });
        });
      }
    });
  });

  /**
   * @description On détecte quand la fenêtre est redimensionnée et on ferme les datepickers
   */
  angular.element($window).bind('resize', function () {
    $scope.dateRange = false;
    $scope.$apply();
  });

  /**
   * @ngdoc function
   * @name openBegin
   * @description Ouvre le premier datepicker ou le ferme s'il était déjà ouvert
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openBegin($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedBegin = !$scope.openedBegin;
    $scope.openedEnd = false;
    $scope.date.begin = new Date($scope.date.begin);
    $scope.date.end = new Date($scope.date.end);
  }

  /**
   * @ngdoc function
   * @name openEnd
   * @description Ouvre le second datepicker ou le ferme s'il était déjà ouvert
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openEnd($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedEnd = !$scope.openedEnd;
    $scope.openedBegin = false;
    $scope.date.begin = new Date($scope.date.begin);
    $scope.date.end = new Date( $scope.date.end);
  }

  /**
   * @ngdoc function
   * @name setDate
   * @description Modifie l'intervalle de date pour afficher les informations sur le tableau de bord
   * @param {int} format - L'identifiant du format de l'intervalle sélectionné par l'utilisateur
   */
  function setDate(format) {
    $scope.format = format;
    var today = moment();
    var end =  moment();
    switch (format) {
      case 0:
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(today);
        $scope.dateRange = false;
        break;
      case 1:
        today = today.subtract(1, 'days');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(today);
        $scope.dateRange = false;
        break;
      case 2:
        today = today.subtract(6, 'days');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(end);
        $scope.dateRange = false;
        break;
      case 3:
        today = today.subtract(29, 'days');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(end);
        $scope.dateRange = false;
        break;
      case 4:
        today = today.startOf('month');
        end = end.endOf('month');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(end);
        $scope.dateRange = false;
        break;
      case 5:
        today = today.subtract(1,'month').startOf('month');
        end =  end.subtract(1,'month').endOf('month');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(end);
        $scope.dateRange = false;
        break;
      case 6:
        if($scope.settings.format == 6) {
          $scope.date.begin = new Date($scope.settings.startsAt);
          $scope.date.end = new Date($scope.settings.endsAt);
        }
        break;
      case 7:
        $scope.dateRange = false;
        break;
    }
  }

 /**
  * @ngdoc function
  * @name open
  * @description Ouvre la liste déroulante pour choisir l'intervalle de date
  */
 function open() {
    $scope.saveFormat = $scope.format;
    $scope.dateRange = !$scope.dateRange;
    $scope.saveBegin = $scope.date.begin;
    $scope.saveEnd = $scope.date.end;
 }

  /**
   * @ngdoc function
   * @name cancel
   * @description Ferme la liste déroulante pour choisir l'intervalle de date
   */
  function cancel() {
    $scope.dateRange = false;
    $scope.date.begin = $scope.saveBegin;
    $scope.date.end = $scope.saveEnd;
    $scope.format = $scope.saveFormat;
    $scope.setDate($scope.format);
  }

  /**
   * @description On observe si la variable date.begin change de valeur
   * Si c'est le cas, et si la nouvelle valeur est supérieur à la date de fin, on
   * met à jour la date de fin.
   */
  $scope.$watch('date.begin', function (newValue) {
    $scope.date.end = (newValue > $scope.date.end) ? newValue : $scope.date.end;
  });

  /**
   * @description On observe si la variable date.end change de valeur
   * Si c'est le cas, et si la nouvelle valeur est inférieure à la date de début, on
   * met à jour la date de début.
   */
  $scope.$watch('date.end', function (newValue) {
    $scope.date.begin = (newValue < $scope.date.begin) ? newValue : $scope.date.begin;
  });
}]);

'use strict';

/**
 * @ngdoc overview
 * @name vif.taffeur
 * @description
 * # vif.taffeur
 *
 * Module principal de l'application
 */
var app = angular.module('vif.taffeur',
  [
  'ngAnimate',
  'ngAria',
  'ngCookies',
  'ngMessages',
  'ngResource',
  'ngRoute',
  'ngSanitize',
  'ngTouch',
  'ui.bootstrap',
  'ngPasswordStrength',
  'toggle-switch',
  'mwl.calendar',
  'tmh.dynamicLocale',
  'xeditable',
  'ngPageTitle',
  'pascalprecht.translate',
    'angular.css.injector'
  ]);


app.run(['$rootScope', '$location', 'editableOptions', function ($rootScope, $location, editableOptions) {
  //thème des tableaux éditables
  editableOptions.theme = 'bs3';

  //Redirection de l'utilisateur s'il n'est pas identifié
  $rootScope.$on("$routeChangeError", function (event, current, previous, eventObj) {
    if (eventObj.authenticated === false)
      $location.path("/login");
  });
}]);

app.config(['$routeProvider',  '$locationProvider', '$httpProvider', 'tmhDynamicLocaleProvider', '$translateProvider',
  function($routeProvider,$locationProvider, $httpProvider,tmhDynamicLocaleProvider,$translateProvider) {

  //Vérifie que l'utilisateur est connecté
  var checkLog = ['$q', 'authenticationSvc',function ($q, authenticationSvc) {
    var userInfo = authenticationSvc.getUserInfo();
    if (userInfo) {
      return $q.when(userInfo);
    } else {
      return $q.reject({ authenticated: false });
    }
  }];

  //Mise en place des éléments de traduction
  $translateProvider.useStaticFilesLoader({
    prefix: '../json/translation/translation_',
    suffix: '.json'
  });
  $translateProvider.use('fr');
  $translateProvider.useSanitizeValueStrategy('escape');
  $translateProvider.preferredLanguage('fr');
  $translateProvider.fallbackLanguage('fr');
  $translateProvider.useLocalStorage();
  tmhDynamicLocaleProvider.localeLocationPattern('../bower_components/angular-i18n/angular-locale_{{locale}}.js');

  //Routage
  $routeProvider
    .when('/', {
      templateUrl: 'app/dashboard/dashboard.html',
      controller: 'dashboardCtrl',
      resolve: {auth: checkLog}
    })
    .when('/login', {
      templateUrl: 'app/login/login.html',
      controller: 'loginCtrl'
    })
    .when('/password', {
      templateUrl: 'app/login/reset.html',
      controller: 'resetCtrl'
    })
    .when('/firstTime', {
      templateUrl: 'app/login/firstLog.html',
      controller: 'firstCtrl',
      resolve: {auth: checkLog}
    })
    .when('/orders', {
      templateUrl: 'app/orders/schedule.html',
      controller: 'scheduleCtrl',
      resolve: {auth: checkLog}
    })
    .when('/orders/:nof', {
      templateUrl: 'app/orders/order.html',
      controller:'orderCtrl',
      resolve: {auth: checkLog}
    })
    .when('/stock/inventories/:numInv', {
      templateUrl: 'app/stock/inventory.html',
      controller: 'inventoryCtrl',
      resolve: {auth: checkLog}
    })
    .when('/stock/inventories', {
      templateUrl: 'app/stock/inventories.html',
      controller: 'inventoriesCtrl',
      resolve: {auth: checkLog}
    })
    .when('/stock/status', {
      templateUrl: 'app/stock/status.html',
      controller: 'statusCtrl',
      resolve: {auth: checkLog}
    })
    .when('/quality', {
      templateUrl: 'app/quality/quality.html',
      controller:'qualityCtrl',
      resolve: {auth: checkLog}
    })
    .when('/reception', {
      templateUrl: 'app/reception/reception.html',
      controller: 'receptionCtrl',
      resolve: {auth: checkLog}
    })
    .when('/reception/:numRec', {
      templateUrl: 'app/reception/oneReception.html',
      controller: 'oneReceptionCtrl',
      resolve: {auth: checkLog}
    })
    .when('/calendar', {
      templateUrl: 'app/calendar/calendar.html',
      controller:'calendarCtrl',
      resolve: {auth: checkLog}
    })
    .when('/account', {
      templateUrl: 'app/account/account.html',
      controller: 'accountCtrl',
      resolve: {auth: checkLog}
    })
    .when('/settings', {
      templateUrl: 'app/settings/settings.html',
      controller: 'settingsCtrl',
      resolve: {auth: checkLog}
    })
    .when('/labels', {
      templateUrl: 'app/label/labels.html',
      controller: 'labelsCtrl',
      resolve: {auth: checkLog}
    })
    .when('/users', {
      templateUrl: 'app/admin/adminPage.html',
      controller: 'adminCtrl',
      resolve: {auth: checkLog}
    })
    .when('/rights/custo', {
      templateUrl: 'app/admin/rightsCusto.html',
      controller: 'rightsCtrl',
      resolve: {auth: checkLog}
    })
    .when('/rights/subco', {
      templateUrl: 'app/admin/rightsSubco.html',
      controller: 'rightsCtrl',
      resolve: {auth: checkLog}
    })
    .when('/traduction', {
      templateUrl: 'app/admin/adminTrad.html',
      controller: 'adminCtrl',
      resolve: {auth: checkLog}
    })
    .when('/404', {
      templateUrl: '404.html',
      controller:'404Ctrl'
    })
    .otherwise({
      redirectTo: '/404'
  });
}

]);

/**
 * @ngdoc controller
 * @name hubCtrl
 * @requires $scope
 * @requires $location
 * @requires $window
 * @requires 'globalService'
 * @requires 'usersService'
 * @requires $route
 * @requires $timeout
 * @requires $translate
 * @requires 'tmhDynamicLocale'
 * @requires 'cssInjector'
 * @description Interaction globale de l'application
 */
app.controller("hubCtrl", ['$scope', '$location','$window', 'globalService', 'usersService', '$route', '$timeout', '$translate', 'tmhDynamicLocale', 'cssInjector',
  function ($scope, $location, $window, globalService, usersService,$route, $timeout, $translate, tmhDynamicLocale, cssInjector) {

  //Initialisation des variables pour l'apparence de la page
  $scope.log = {login: true, admin:false}; //afficher ou non les barres de menu et admin pour afficher ou non les menus administrateur
  $scope.fil = {subcoFilter: '', doFilter: ''}; //Filtre sous-traitant et donneur d'ordres
  $scope.disp = {barre: true}; //Ouvrir ou fermer le mennu vertical

  //Initialisation des variables pour la couleur des barres de navigation
  $scope.color = {};
  $scope.color.header = 'normal';
  $scope.color.leftbar = 'normal';

  //Initialisation de la variable pour fixer le menu horizontal
  $scope.header = {};
  $scope.header.fixed = true;

  /**
   * @ngdoc function
   * @name switchLanguage
   * @description Change le langage courrant du portail
   * @param {string} key - La clé du langage
   */
  $scope.switchLanguage = function(key) {
    $translate.use(key);
    tmhDynamicLocale.set(key);
    moment.locale(key);
  };

  /**
   * @ngdoc function
   * @name changementBarre
   * @description Ouvre ou ferme le menu vertical
   */
  $scope.changementBarre = function() {
    $scope.disp.barre = !$scope.disp.barre;
  };

  /**
   * @ngdoc function
   * @name changeBarre
   * @description Détermine la classe du menu vertical en fonction de la tailel de la fenêtre
   */
  $scope.changeBarre = function() {
    if($scope.log.login) {
      return "focusedform";
    }else {
      if (!$scope.disp.barre && $window.innerWidth < 767) {
        $scope.petit = true;
        return "show-leftbar";
      } else if (!$scope.disp.barre) {
        $scope.petit = false;
        return "collapse-leftbar";
      } else {
        $scope.petit = false;
        return "";
      }
    }
  };

  /**
   * @ngdoc function
   * @name changeColor
   * @description Modifie la couleur des barres de menu
   * @param {string} header - Couleur de la barre horizontale
   * @param {string} leftbar - Couleur de la barre verticale
   */
  $scope.changeColor = function(header, leftbar) {
    $scope.color.header = header;
    $scope.color.leftbar = leftbar;
    cssInjector.removeAll();

    if($scope.color.header != 'normal') {
      var path = 'styles/header-' + $scope.color.header + '.css';
      cssInjector.add(path);
    }

    if($scope.color.leftbar != 'normal') {
      path = 'styles/slidebar-' + $scope.color.leftbar + '.css';
      cssInjector.add(path);
    }
  };

}]);



/**
 * Created by VIF - 06/2015
 * @ngdoc factory
 * @name adminService
 * @requires $http
 * @description Service pour récupérer et insérer des données dans le fichier des droits utilisateur
 */

app.factory('adminService', ['$http', function($http) {
  var adminService = {};

  //Récupération des droits
  adminService.getData = function (callback) {
    return $http.get('../../json/rights.json').success(callback);
  };

  //Modification des droits
  adminService.insertData = function (data) {
    return $http({
      url: "../../json/rights.json",
      method: "POST",
      data: data,
      headers: {'Content-Type': 'application/json'}});
  };

  return adminService;
}]);

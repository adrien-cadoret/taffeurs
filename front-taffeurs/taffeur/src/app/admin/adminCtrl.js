/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name adminCtrl
 * @requires $scope
 * @requires 'usersService'
 * @requires $location
 * @requires 'globalService'
 * @requires 'auth'
 * @requires 'scheduleService'
 * @requires 'inventoriesService'
 * @requires 'qualityService'
 * @requires 'receptionService'
 * @requires 'calendarService'
 * @requires 'statusService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $window
 * @description Interaction de la page administrateur de gestion des utilisateurs
 */

app.controller('adminCtrl', ['$scope', 'usersService', '$location', 'globalService', 'auth', 'scheduleService', 'inventoriesService', 'qualityService', 'receptionService',
  'calendarService', 'statusService', '$pageTitle', '$filter', '$window',
  function($scope, usersService, $location,globalService,auth, scheduleService, inventoriesService, qualityService, receptionService, calendarService,
           statusService,$pageTitle,$filter,$window) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('USER_MANAGEMENT'));

  //On met la variable log.login à false et la variable log.admin à true pour l'apparence de la page
  $scope.log.login = false; //On active les barres horizontales et verticales
  $scope.log.admin = true; //On  active les menus de l'administrateur

  //Récupération de l'utilisateur actuellement connecté
  var userid =  auth.userName;

  //Récupération du chemin vers les images des utilisateurs
  $scope.absolutePath = globalService.absolutePath;

  //Initialisation de la variable pour sélectionner un utilisateur
  $scope.selectedUser = null;

  //Initialisation des tableaux d'utilisateurs
  $scope.filteredUsers = [];
  $scope.users = [];

  //Initialisation des variables de recherche
  $scope.search = {};
  $scope.search.user = '';
  $scope.search.contact = '';

  //Initialisation des variables de pagination
  $scope.pagination= {};
  $scope.pagination.current = 1;
  $scope.pagination.size = 16;
  $scope.pagination.currentC = 1;
  $scope.pagination.sizeC = 16;

  //Initialisation des variables pour filtrer les catégories d'utilisateur
  $scope.filter = {};
  $scope.filter.do = true;
  $scope.filter.subco = true;

  //Initialisation des fonctions
  $scope.setSelectedUser = setSelectedUser;
  $scope.setForm = setForm;
  $scope.addNewContact = addNewContact;
  $scope.cancelNewContact = cancelNewContact;
  $scope.deleteUser = deleteUser;
  $scope.cancelViewUser = cancelViewUser;
  $scope.reloadPage = reloadPage;
  $scope.displayItem = displayItem;

  /**
   * @description Récupération de la liste des utilisateurs et de l'utilisateur actuel
   */
  usersService.getUsers(function(data) {
    $scope.users_all = data;
    angular.forEach($scope.users_all, function(user) {
      if (user.company == userid) {
        $scope.user = user;

        if ($scope.user.company != 'Administrateur')
          $location.url('/');
      } else {
        $scope.users.push(user);
      }
    });
  });

  /**
   * @ngdoc function
   * @name setSelectedUser
   * @description Enregistre l'utilisateur sélectionné
   * @param {Object} user - L'utilisateur à enregistrer
   */
  function setSelectedUser(user) {
    $scope.selectedUser = ($scope.selectedUser != user) ? user : null;
  }

  /**
   * @ngdoc function
   * @name setForm
   * @description Permet au model de connaitre le formulaire d'ajout d'un contact
   * @params {Object} arg - Le formulaire d'ajout
   */
  function setForm(arg) {
    $scope.form = arg;
  }

  /**
   * @ngdoc function
   * @name addNewContact
   * @description Ajoute un nouvel utilisateur
   */
  function addNewContact() {
    angular.forEach($scope.users, function (elt) {
      if (elt.company == $scope.newContact.company) {
        $scope.sameCompany = true;
        $scope.form.addContactForm.$valid = false;
      }
    });
    if ($scope.form.addContactForm.$valid) {
      $scope.newContact.img = "default.png";
      $scope.user.contacts.push($scope.newContact.company);
      $scope.newContact.password = randomPassword(10);
      $scope.newContact.name="";
      $scope.newContact.firstName="";
      $scope.newContact.function="";
      $scope.newContact.isSubco =  ($scope.newContact.isSubco == 'true');
      $scope.newContact.contacts = [];
      $scope.newContact.notifications = [];
      $scope.newContact.settings = {
        "format": 4,
        "language": "fr",
        "subDefault": "",
        "orders": true,
        "stock": true,
        "quality": true,
        "reception": true,
        "calendar": true,
        "dashboard": true,
        "numOfDays": 3,
        "vertical": true,
        "horizontal": true,
        "view": "month",
        "startsAt": "2015-01-01T22:00:00.000Z",
        "endsAt": "2015-12-31T22:00:00.000Z",
        "vertMenu" : "normal",
        "horMenu" :"normal",
        "fixed" :true
      };

      $scope.newContact.activation = false;
      $scope.users.push($scope.newContact);
      //Envoi mail au nouvel utilisateur
      usersService.insertData($scope.users);
      $scope.cancelNewContact();
    } else {
      $scope.addContact = true;
      $scope.pressBtn = true;
    }
  }

  /**
   * @ngdoc function
   * @name cancelNewContact
   * @description Annule l'action d'ajouter un nouvel utilisateur
   */
  function cancelNewContact() {
    $scope.addContact = false;
    $scope.sameCompany = false;
    $scope.newContact = {};
    $scope.pressBtn = false;
  }

  /**
   * @ngdoc function
   * @name randomPassword
   * @description Créée un nouveau mot de passe aléatoirement
   * @params {int} length - Longueur du mot de passe
   */
  function randomPassword(length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
      var i = Math.floor(Math.random() * chars.length);
      pass += chars.charAt(i);
    }
    return pass;
  }

  /**
   * @ngdoc function
   * @name cancelViewUser
   * @description Ferme la boite de dialogue pour voir un utilisateur
   */
  function cancelViewUser() {
    $scope.viewUser = false;
  }

  /**
   * @ngdoc function
   * @name reloadPage
   * @description Rafraichit la page
   */
  function reloadPage() {
    $window.location.reload();
  }

  /**
   * @ngdoc function
   * @name displayItem
   * @description Fitlrer pour déterminer si un utilisateur peut être affiché ou non
   * @returns {boolean} - true si l'élément peut être affiché, faux sinon
   */
  function displayItem(item) {
    return ((item.isSubco && $scope.filter.subco) || (!item.isSubco && $scope.filter.do));
  }

  /**
   * @ngdoc function
   * @name deleteUser
   * @description Supprime l'utilisateur qui est sélectionné et toutes ses données
   */
 function deleteUser(){
    var company = $scope.selectedUser.company;

    var newTab = [];
    var newContacts = [];
    //Suppresion de l'utilisateur
    angular.forEach($scope.users, function(elt){
      newContacts = [];
      if(elt.company != company) {
        angular.forEach(elt.contacts, function(contact) {
          if(contact != company) {
            newContacts.push(contact);
          } else {
            var notif = {"type":"contact_delete", "date":new Date(), "key" : "", "title":company,  "active" : "active"};
            elt.notifications.push(notif);
          }
        });
        elt.contacts = newContacts;
        newTab.push(elt);
      }
    });
    $scope.users = angular.copy(newTab);
    newTab.push($scope.user);
    usersService.insertData(newTab);

    //Suppresion des données ordres
    scheduleService.getData(function(data) {
      var newTabOrdres = [];
      angular.forEach(data, function(elt) {
        if(elt.subco != company && elt.iddo != company)
          newTabOrdres.push(elt);
      });
      scheduleService.insertData(newTabOrdres);
    });

    //Suppresion des données inventaires
    inventoriesService.getData(function(data) {
      var newTabInv = [];
      angular.forEach(data, function(elt) {
        if(elt.subco != company && elt.iddo != company)
          newTabInv.push(elt);
      });
      inventoriesService.insertData(newTabInv);
    });


    //Suppresion des données état
    statusService.getData(function(data) {
      var newTabSta = [];
      angular.forEach(data, function(elt) {
        if(elt.subco != company && elt.iddo != company)
          newTabSta.push(elt);
      });
      statusService.insertData(newTabSta);
    });

    //Suppresion des données qualité
    qualityService.getData(function(data) {
      var newTabGap = [];
      angular.forEach(data, function(elt) {
        if(elt.subco != company && elt.iddo != company)
          newTabGap.push(elt);
      });
      qualityService.insertData(newTabGap);
    });

    //Suppresion des données réception
    receptionService.getData(function(data) {
      var newTabRec = [];
      angular.forEach(data, function(elt) {
        if(elt.subco != company && elt.iddo != company)
          newTabRec.push(elt);
      });
      receptionService.insertData(newTabRec);
    });

    //Suppresion des données calendrier
    calendarService.getData(function(data) {
      var newTabCal = [];
      angular.forEach(data, function(elt) {
        if(elt.subco != company && elt.iddo != company)
          newTabCal.push(elt);
      });
      calendarService.insertData(newTabCal);
    });

    $scope.selectedUser = null;
    $scope.showModalDelete = false;
  }
}]);


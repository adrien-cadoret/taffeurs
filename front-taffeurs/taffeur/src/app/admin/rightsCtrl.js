/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name rightsCtrl
 * @requires $scope
 * @requires 'adminService'
 * @requires $pageTitle
 * @requires $filter
 * @description Interaction de la page administrateur de gestion des droits
 */
app.controller('rightsCtrl', ['$scope', 'adminService','$pageTitle', '$filter',
  function($scope,adminService,$pageTitle, $filter) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('RIGHTS_MANAGEMENT'));

  //On met la variable log.login à false et la variable log.admin à true pour l'apparence de la page
  $scope.log.login = false; //On active les barres horizontales et verticales
  $scope.log.admin = true; //On  active les menus de l'administrateur

  //Initialisation des variables pour afficher ou masquer une catégorie de droits
  $scope.orders = true;
  $scope.stock = true;
  $scope.quality = true;
  $scope.reception = true;
  $scope.labels = true;
  $scope.calendar = true;
  $scope.account = true;

  //Initialisation des fonctions
  $scope.saveRights = saveRights;
  $scope.setTab = setTab;

  /**
   * @description Récupération des droits
   */
  adminService.getData(function(data) {
    $scope.rights = data;
  });

  /**
   * @ngdoc function
   * @name setTab
   * @description Affiche ou masque une catégorie de droit
   * @param {string} tabName - La catégorie à afficher ou masquer
   */
  function setTab(tabName) {
    switch(tabName) {
      case 'orders' :
        $scope.orders = !$scope.orders;
        break;
      case 'stock' :
        $scope.stock = !$scope.stock;
        break;
      case 'quality' :
        $scope.quality = !$scope.quality;
        break;
      case 'reception' :
        $scope.reception = !$scope.reception;
        break;
      case 'labels' :
        $scope.labels = !$scope.labels;
        break;
      case 'calendar' :
        $scope.calendar = !$scope.calendar;
        break;
      case 'account' :
        $scope.account = !$scope.account;
        break;
    }
  }

  /**
   * @ngdoc function
   * @name saveRights
   * @description Enregistre les droits dans la base de données
   */
  function saveRights() {
    adminService.insertData($scope.rights);
  }

}]);


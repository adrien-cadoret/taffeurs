/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name accountCtrl
 * @requires $scope
 * @requires 'usersService'
 * @requires 'globalService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $window
 * @requires 'auth'
 * @requires 'scheduleService'
 * @requires 'inventoriesService'
 * @requires 'statusService'
 * @requires 'receptionService'
 * @requires 'qualityService'
 * @requires 'calendarService'
 * @requires $http
 * @requires $timeout
 * @requires $route
 * @requires 'adminService'
 * @description Interaction de la page sur le compte utilisateur
 */

app.controller('accountCtrl',['$scope', 'usersService', 'globalService', '$pageTitle', '$filter', '$window', 'auth',
  'scheduleService', 'inventoriesService', 'statusService', 'receptionService', 'qualityService', 'calendarService', '$http', '$timeout', '$route', 'adminService',
  function($scope, usersService, globalService, $pageTitle, $filter, $window, auth,scheduleService,inventoriesService,statusService,receptionService,qualityService,
           calendarService,$http,$timeout,$route,adminService) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('ACCOUNT'));

  //On met la variable log.login à false pour l'apparence de la page
  $scope.log.login = false;

  //On récupère l'identifiant de l'utilisateur connecté
  var userid = auth.userName;

  //On récupère le chemin pour charger les images des utilisateurs
  $scope.absolutePath = globalService.absolutePath;

  //On initialise les variables pour enregistrer les contacts
  $scope.selectedContact = null;
  $scope.selectedSubco = null;

  //Initialisation des variables pour changer d'onglet
  $scope.data = true;
  $scope.security = false;

  //Initialisation des variables pour modifier les données de l'utilisateur
  $scope.password = {};
  $scope.password.oldPass = null;
  $scope.password.newPass = null;
  $scope.password.repeatPass= null;
  $scope.passwordUpd = false;
  $scope.change = false;
  $scope.errorPass = false;

  //Initialisation des variables de recherche
  $scope.search = {};
  $scope.search.contact = '';
  $scope.add = {};
  $scope.add.subcoSearch = '';

  //Initialisation des boutons pour ajouter un contact
  $scope.addContact = false;
  $scope.pressBtn = false;
  $scope.btnValid = false;

  //Initialization of the pagination variables
  $scope.pagination = {};
  $scope.pagination.current = 1;
  $scope.pagination.size = 16;

  //Initialisation des variables pour ajouter un contact
  $scope.filteredContacts = [];
  $scope.contacts = [];
  $scope.newContact = {};
  $scope.usersNames = [];
  $scope.choice=true;
  $scope.contactAdded = false;

  /**
   * @description Récupération des droits utilisateur
   */
  adminService.getData(function(data) {
    $scope.rights = data;
  });

  /**
   * @description Récupération des données de l'utilisateur et des données de ses contacts
   */
  usersService.getUsers(function(data) {
    $scope.users = data;
    angular.forEach($scope.users, function(user) {
      if(user.company == userid) {
        //On récupère l'utilisateur
        $scope.user = user;
        //Pour chacun de ses contacts, on récupère les données
        angular.forEach(user.contacts, function(contact) {
          angular.forEach($scope.users, function(elt){
            if(elt.company == contact) {
              if(elt.activation) {
                $scope.contacts.push({
                  "company": elt.company,
                  "img": elt.img,
                  "name": elt.name,
                  "firstName": elt.firstName,
                  "function": elt.function,
                  "phone_1": elt.phone_1,
                  "phone_2": elt.phone_2,
                  "mail": elt.mail
                })
              } else {
                $scope.contacts.push({
                  "company": elt.company,
                  "img": elt.img,
                  "name": $filter('translate')('WAIT_ACTIVATION')
                })
              }
            }
          });
        });
        angular.forEach($scope.users, function(user) {
          if (user.isSubco && $scope.user.contacts.indexOf(user.company) < 0)
            $scope.usersNames.push(user.company);
        });
      }
    });
  });

  //Initialisation des fonctions
  $scope.passwordUpdate = passwordUpdate;
  $scope.cancelUpdate = cancelUpdate;
  $scope.setForm = setForm;
  $scope.addNewContact = addNewContact;
  $scope.showAddContact = showAddContact;
  $scope.showAdd = showAdd;
  $scope.cancelNewContact = cancelNewContact;
  $scope.getPlacementBootstro_Info = getPlacementBootstro_Info;
  $scope.saveChange = saveChange;
  $scope.setSelectedContact = setSelectedContact;
  $scope.setSelectedSubco = setSelectedSubco;
  $scope.deleteContact = deleteContact;
  $scope.getPlacementBootstro_Contact =getPlacementBootstro_Contact;
  $scope.saveFile =saveFile;
  $scope.showModalDelete = showModalDelete;
  $scope.setChange = setChange;

  /**
   * @ngdoc function
   * @name saveChange
   * @description Enregistre les changements sur les données de l'utilisateur
   * @params {boolean} boolean - true si la page doit être rafraichie, false sinon
   */
  function saveChange(boolean) {
    usersService.insertData($scope.users);
    var notif = {"type":"contact_edit", "date":new Date(), "key" : "", "title":$scope.user.company,  "active" : "active"};

    angular.forEach($scope.user.contacts, function(contactName) {
      angular.forEach($scope.users, function(elt) {
       if(elt.company == contactName)
         elt.notifications.push(notif);
      })
    });
    if(boolean)
      $window.location.reload();
  }

  /**
   * @ngdoc function
   * @name setSelectedContact
   * @description Enregistre le contact qui est sélectionné
   * @param {Object} contact - Le contact à enregistrer
   */
  function setSelectedContact(contact) {
    $scope.selectedContact = ($scope.selectedContact != contact) ? contact : null;
  }

  /**
   * @ngdoc function
   * @name setSelectedSubco
   * @description Enregistre le contact qui est sélectionné dans la boite de dialogue d'ajout
   * @param {Object} subco - Le contact à enregistrer
   */
  function setSelectedSubco(subco) {
    $scope.selectedSubco = ($scope.selectedSubco != subco) ? subco : null;
  }

  /**
   * @ngdoc function
   * @name deleteContact
   * @description On supprime le contact sélectionné et les données échangées entre l'utilisateur et le contact
   */
  function deleteContact(){
    var company = $scope.selectedContact.company;

    //On supprime le contact de la liste des contacts de l'utilisateur
    var index = $scope.contacts.indexOf($scope.selectedContact);
    $scope.contacts.splice(index, 1);

    index = $scope.user.contacts.indexOf(company);
    $scope.user.contacts.splice(index, 1);

    //On informe le contact qu'un de ses contacts l'a supprimé de sa liste et on supprime aussi l'utilisateur de la liste
    //des contacts du contact
    var notif = {"type":"contact_delete", "date":new Date(), "key" : "", "title":$scope.user.company,  "active" : "active"};
    angular.forEach($scope.users, function(elt) {
      if(elt.company == company) {
        index = elt.contacts.indexOf($scope.user.company);
        elt.contacts.splice(index, 1);
        elt.notifications.push(notif);
      }
    });
    //On enregistre les données des utilisateurs dans la base de données pour prendre en compte les modifications
    usersService.insertData($scope.users);

    //On parcourt tous les autres fichiers pour supprimer les données relatives à l'utilisateur et son contact

    //Suppresion des ordres de fabrication
    scheduleService.getData(function(data) {
      var newTabOrdres = [];
      angular.forEach(data, function(elt) {
        if(elt.iddo = $scope.user.company) {
          if (elt.subco != company)
            newTabOrdres.push(elt);
        }
      });
      scheduleService.insertData(newTabOrdres);
    });

    //Suppresion des données inventaires
    inventoriesService.getData(function(data) {
      var newTabInv = [];
      angular.forEach(data, function(elt) {
        if(elt.iddo = $scope.user.company) {
          if (elt.subco != company)
            newTabInv.push(elt);
        }
      });
      inventoriesService.insertData(newTabInv);
    });


    //Suppresion des données état
    statusService.getData(function(data) {
      var newTabSta = [];
      angular.forEach(data, function(elt) {
        if(elt.iddo = $scope.user.company) {
          if (elt.subco != company)
            newTabSta.push(elt);
        }
      });
      statusService.insertData(newTabSta);
    });

    //Suppresion des données qualité
    qualityService.getData(function(data) {
      var newTabGap = [];
      angular.forEach(data, function(elt) {
        if(elt.iddo = $scope.user.company) {
          if (elt.subco != company)
            newTabGap.push(elt);
        }
      });
      qualityService.insertData(newTabGap);
    });

    //Suppresion des données réception
    receptionService.getData(function(data) {
      var newTabRec = [];
      angular.forEach(data, function(elt) {
        if(elt.iddo = $scope.user.company) {
          if (elt.subco != company)
            newTabRec.push(elt);
        }
      });
      receptionService.insertData(newTabRec);
    });

    //Suppresion des données calendrier
    calendarService.getData(function(data) {
      var newTabCal = [];
      angular.forEach(data, function(elt) {
        if(elt.iddo = $scope.user.company) {
          if (elt.subco != company)
            newTabCal.push(elt);
        }
      });
      calendarService.insertData(newTabCal);
    });

    $scope.selectedContact = null;
    $scope.showModalDelete(false);
  }

  /**
   * @ngdoc function
   * @name passwordUpdate
   * @description Met à jour le mot de passe de l'utilisateur
   * @params {boolean} isValid - true si le formulaire de mise à jour du mot de passe est valide, faux sinon
   */
  function passwordUpdate(isValid) {
    if(isValid) {
      if($scope.password.oldPass != $scope.user.password) {
        //Si l'ancien mot de passe n'est pas le bon
        $scope.btnValid = true;
        $scope.errorPass = true;
      } else {
        //Si tout est bon, on enregistre le nouveau mot de passe dans la base de données
        $scope.errorPass = false;
        $scope.btnValid = false;
        $scope.user.password = $scope.password.newPass;
        $scope.passwordUpd = true;
        $scope.saveChange(false);
        $scope.password.newPass = '';
        $scope.password.oldPass = '';
        $scope.password.repeatPass = '';
      }
    } else {
      $scope.btnValid = true;
    }
  }

  /**
   * @ngdoc function
   * @name cancelUpdate
   * @description On annule la mise à jour du mot de passe
   */
  function cancelUpdate() {
    $scope.password.newPass = '';
    $scope.password.oldPass = '';
    $scope.password.repeatPass = '';
  }

  /**
   * @ngdoc function
   * @name setForm
   * @description Permet au model de connaitre le formulaire d'ajout d'un contact
   * @params {Object} arg - Le formulaire d'ajout
   */
  function setForm(arg) {
    $scope.form = arg;
  }

  /**
   * @ngdoc function
   * @name newContact
   * @description Ajoute un nouveau contact dans la liste des contacts et dans la liste des utilisateurs
   * si il n'existait pas avant
   */
  function addNewContact() {
    if($scope.new) {
      //Si l'utilisateur a choisit d'ajouter un contact qui n'existe pas
      //On commence par vérifier que l'identifiant choisit n'est pas déjà utilisé
      angular.forEach($scope.users, function (elt) {
        if (elt.company == $scope.newContact.company) {
          $scope.sameCompany = true;
          $scope.form.addContactForm.$valid = false;
        }
      });
      //Puis on vérifie si les informations renseignées sont valides
      if ($scope.form.addContactForm.$valid) {
        //On crée l'utilisateur
        $scope.newContact.img = "default.png";
        $scope.user.contacts.push($scope.newContact.company);
        $scope.newContact.notifications = [];
        $scope.newContact.password = randomPassword(10);
        $scope.newContact.name="";
        $scope.newContact.firstName="";
        $scope.newContact.function="";
        $scope.newContact.isSubco = true;
        $scope.newContact.contacts = [];
        $scope.newContact.contacts.push($scope.user.company);
        $scope.newContact.settings = {
          "format": 4,
          "language": "fr",
          "subDefault": "",
          "orders": true,
          "stock": true,
          "quality": true,
          "reception": true,
          "calendar": true,
          "dashboard": true,
          "numOfDays": 3,
          "vertical": true,
          "horizontal": true,
          "view": "month",
          "startsAt": "2015-01-01T22:00:00.000Z",
          "endsAt": "2015-12-31T22:00:00.000Z",
          "vertMenu" : "normal",
          "horMenu" :"normal",
          "fixed" :true
        };
        $scope.newContact.activation = false;
        $scope.contacts.push($scope.newContact);
        //On met une notification au nouvel utilisateur pour l'informer qu'il a un nouveau contact
        var notif = {"type":"contact_new", "date":new Date(), "key" : "", "title":$scope.user.company,  "active" : "active"};
        $scope.newContact.notifications.push(notif);
        $scope.users.push($scope.newContact);
        //Envoi mail nouvel utilisateur (non implémenté) pour le prévenir de ses identifiants de connexion
        $scope.saveChange(false);
        $scope.contactAdded = true;
      } else {
        $scope.addContact = true;
        $scope.pressBtn = true;
      }
    } else {
      //Si le contact existait déjà dans la liste des utilisateurs, on l'informe simplement de l'arrivée d'un nouveau contact
      angular.forEach($scope.users, function(elt) {
        if(elt.company == $scope.selectedSubco) {
          $scope.user.contacts.push(elt.company);
          elt.contacts.push($scope.user.company);
          var notif = {"type":"contact_new", "date":new Date(), "key" : "", "title":$scope.user.company,  "active" : "active"};
          elt.notifications.push(notif);
          $scope.saveChange(false);
          $scope.reload();
        }
      });
      $scope.addContact = false;
      $scope.newContact = {};
      $scope.choice = true;
      $scope.pressBtn = false;
    }
  }

  /**
   * @ngdoc function
   * @name showAdd
   * @description Annule l'action d'ajouter un nouveau contact
   */
  function showAddContact() {
    $scope.addContact = true;
    $scope.new = true;
    $scope.newContact = {};
    $scope.choice = true;
  }

  /**
   * @ngdoc function
   * @name showAdd
   * @description Redirige l'utilisateur dans la boite de dialogue d'ajout
   * @params {boolean} boolean - true si on veut ajouter un nouveau contact, faux si on veut rechercher un contact existant
   */
  function showAdd(boolean) {
    $scope.choice = false;
    $scope.new = boolean;
  }

  /**
   * @ngdoc function
   * @name cancelNewContact
   * @description Annule l'action d'ajouter un nouveau contact
   */
  function cancelNewContact() {
    $scope.addContact = false;
    $scope.sameCompany = false;
    $scope.newContact = {};
    $scope.pressBtn = false;
    $scope.choice = true;
    $scope.contactAdded = false;
    $scope.reload();
  }

  /**
   * @ngdoc function
   * @name getPlacementBootstro_Info
   * @description Détermine le placement des bulles d'aide d'informartion en fonction de la taille de la fênetre
   * @return {string} L'emplacement de la bulle d'aide
   */
  function getPlacementBootstro_Info(){
    return ($window.innerWidth  < 1200)? 'bottom' : 'right';
  }

  /**
   * @ngdoc function
   * @name getPlacementBootstro_Contact
   * @description Détermine le placement des bulles d'aide sur les contacts en fonction de la taille de la fênetre
   * @return {string} L'emplacement de la bulle d'aide
   */
  function getPlacementBootstro_Contact(){
    return ($window.innerWidth  < 1200)? 'top' : 'left';
  }

  /**
   * @ngdoc function
   * @name randomPassword
   * @description Créée un nouveau mot de passe aléatoirement
   * @params {int} length - Longueur du mot de passe
   */
  function randomPassword (length) {
    var chars = "abcdefghijklmnopqrstuvwxyz!@#$%^&*()-+<>ABCDEFGHIJKLMNOP1234567890";
    var pass = "";
    for (var x = 0; x < length; x++) {
      var i = Math.floor(Math.random() * chars.length);
      pass += chars.charAt(i);
    }
    return pass;
  }

  /**
   * @ngdoc function
   * @name saveFile
   * @description Enregistre la nouvelle image de l'utilisateur
   * @params {Object} element - La nouvelle image
   */
  function saveFile(element) {
    $scope.change = true;
    $scope.user.img = element.files[0].name;
    var formData = new FormData();
    formData.append("file", element.files[0]);
    usersService.uploadImage(formData);
  }

  /**
   * @ngdoc function
   * @name showModalDelete
   * @description Ouvre ou ferme la boite de dialogue pour confirmer la suppression du contact
   * @params {boolean} boolean - true si on veut fermer la boite de dialogue, faux sinon
   */
  function showModalDelete(boolean) {
    $scope.modalDelete = boolean;
  }

  /**
   * @ngdoc function
   * @name setChange
   * @description On enregistre que des modifications ont été effectuées sur les données de l'utilisateur
   */
  function setChange() {
    $scope.change = true;
  }

}]);

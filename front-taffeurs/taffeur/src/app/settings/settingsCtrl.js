/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name settingsCtrl
 * @requires $scope
 * @requires $routeParams
 * @requires $pageTitle
 * @requires 'usersService'
 * @requires $filter
 * @requires $window
 * @requires 'auth'
 * @requires 'adminService'
 * @description Interaction de la page des paramètres
 */

app.controller('settingsCtrl', ['$scope', '$routeParams', '$pageTitle', 'usersService','$filter', '$window','auth', 'adminService',
  function($scope,$routeParams, $pageTitle, usersService, $filter, $window, auth, adminService) {

  //On définit le titre de la page
  $pageTitle.set($filter('translate')('SETTINGS'));

  //Initialisation des variables
  $scope.log.login = false;//On met la variable log.login à false pour l'apparence de la page
  var userid = auth.userName;//On récupère l'identifiant de l'utilisateur actuellement connecté
  $scope.date={};//Variable de date
  $scope.date.begin = new Date();//Début de l'intervalle
  $scope.date.end = new Date();//Fin de l'intervakke
  $scope.users = [];//Initialisation du tableau des utilisateurs

  //Initialisation du premier jour de la semaine pour les datepickers (1 - Lundi, 0 - Dimanche)
  $scope.dateOptions1 = {
    startingDay: 1
  };

  /**
   * @description Récupération des droits utilisateurs
   */
  adminService.getData(function(data) {
    $scope.rights = data;
  });

  /**
     * @description Récupération de l'utilisateur
     */
  usersService.getUsers(function(data){
    $scope.users = data;
    angular.forEach(data, function(user) {
      if(user.company == userid) {
        $scope.user = user;
        $scope.settings = user.settings;

        //On appelle la fonction setDate à l'ouverture pour afficher le choix de l'utilisateur par rapport à l'intervalle
        //de date à l'ouverture du tableau de bord
        $scope.setDate($scope.settings.format);
      }
    });
  });

  /**
   * @description On détecte si la fenêtre est redimensionnée et on ferme le datepicker
   */
  angular.element($window).bind('resize', function () {
    $scope.dateRange=false;
    $scope.$apply();
  });

  //Initialisation des fonctions
  $scope.openDP = openDP;
  $scope.cancelDP = cancelDP;
  $scope.openBegin = openBegin;
  $scope.openEnd = openEnd;
  $scope.save_language = save_language;
  $scope.save_view = save_view;
  $scope.setDate = setDate;
  $scope.colorSet =colorSet;
  $scope.changeFixed =changeFixed;
  $scope.reloadPage =reloadPage;
  $scope.saveSettings =saveSettings;
  $scope.reset =reset;

  /**
  * @ngdoc function
  * @name openDB
  * @description Ouvre le datepicker
  */
 function openDP() {
    $scope.saveFormat = $scope.format;
    $scope.dateRange = !$scope.dateRange;
    $scope.saveBegin = $scope.date.begin;
    $scope.saveEnd = $scope.date.end;
 }

 /**
  * @ngdoc function
  * @name cancelDP
  * @description Ferme le datepicker
  */
 function cancelDP() {
   $scope.dateRange = false;
   $scope.date.begin = $scope.saveBegin;
   $scope.date.end = $scope.saveEnd;
   $scope.format = $scope.saveFormat;
   $scope.setDate($scope.format);
 }

 /**
  * @ngdoc function
  * @name openBegin
  * @description Ouvre le premier datepicker ou le ferme s'il était déjà ouvert
  * @param {Event} $event - L'évènement d'ouverture
  */
 function openBegin($event) {
   $event.preventDefault();
   $event.stopPropagation();
   $scope.openedBegin = !$scope.openedBegin;
   $scope.openedEnd = false;
   $scope.date.begin = new Date($scope.date.begin);
   $scope.date.end = new Date($scope.date.end);
 }

  /**
   * @ngdoc function
   * @name openEnd
   * @description Ouvre le second datepicker ou le ferme s'il était déjà ouvert
   * @param {Event} $event -  L'évènement d'ouverture
   */
  function openEnd($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedEnd = !$scope.openedEnd;
    $scope.openedBegin = false;
    $scope.date.begin = new Date($scope.date.begin);
    $scope.date.end = new Date( $scope.date.end);
  }

  /**
   * @description On observe si la variable date.begin change de valeur
   * Si c'est le cas, et si la nouvelle valeur est supérieur à la date de fin, on
   * met à jour la date de fin.
   */
  $scope.$watch('date.begin', function (newValue) {
    $scope.date.end = (newValue > $scope.date.end) ? newValue : $scope.date.end;
  });

  /**
   * @description On observe si la variable date.end change de valeur
   * Si c'est le cas, et si la nouvelle valeur est inférieure à la date de début, on
   * met à jour la date de début.
   */
  $scope.$watch('date.end', function (newValue) {
    $scope.date.begin = (newValue < $scope.date.begin) ? newValue : $scope.date.begin;
  });

  /**
   * @ngdoc function
   * @name save_language
   * @description Enregistre le langage courrant
   * @param {string} key - La clé du langage choisie par l'utilisateur
   */
  function save_language(key) {
    $scope.settings.language = key;
    $scope.switchLanguage(key);
    $scope.setDate(parseInt($scope.settings.format));
    usersService.insertData($scope.users);
    $scope.reloadPage();
  }

  /**
   * @ngdoc function
   * @name save_view
   * @description Enregistre la vue par défaut du calendrier
   * @param {string} view - Le nom de la vue par défaut choisie par l'utilisateur
   */
  function save_view(view) {
    $scope.settings.view = view;
    usersService.insertData($scope.users);
    $scope.reloadPage();
  }

  /**
   * @ngdoc function
   * @name saveSettings
   * @description Enregistre les paramètres par défaut
   */
  function saveSettings() {
    usersService.insertData($scope.users);
    $scope.reloadPage();
  }

  /**
   * @ngdoc function
   * @name reset
   * @description Réinitialise les paramètres par défaut
   */
   function reset() {
      $scope.settings.orders = true;
      $scope.settings.stock = true;
      $scope.settings.quality = true;
      $scope.settings.reception = true;
      $scope.settings.calendar = true;
      $scope.settings.dashboard = true;
      $scope.settings.vertical = true;
      $scope.settings.horizontal = true;
      $scope.settings.format = 4;
      $scope.settings.numOfDays = 3;
      $scope.settings.view = 'month';
      $scope.settings.subDefault = '';
      $scope.settings.vertMenu = 'normal';
      $scope.settings.horMenu = 'normal';
      $scope.settings.fixed = true;
      $scope.save_language('fr');
   }

  /**
   * @ngdoc function
   * @name reloadPage
   * @description Raffraichit la page pour prendre en compte les nouveaux paramètres
   */
   function reloadPage() {
      $window.location.reload();
   }

  /**
   * @ngdoc function
   * @name changeFixed
   * @description Enregistre le changement de paramètre pour fixer ou non la barre horizontale
   */
  function changeFixed() {
    $scope.header.fixed = $scope.settings.fixed;
    usersService.insertData($scope.users);
    $scope.reloadPage();
  }

  /**
   * @ngdoc function
   * @name colorSet
   * @description Enregistre les paramètres de couleur pour les barres de menu
   * @param {string} header - couleur de la barre horizontale
   * @param {string} leftbar - couleur de la barre verticale
   */
  function colorSet(header, leftbar) {
    $scope.settings.vertMenu = leftbar;
    $scope.settings.horMenu = header;
    $scope.color.leftbar = leftbar;
    $scope.color.header = header;
    usersService.insertData($scope.users);
    $scope.changeColor(header, leftbar);
    $scope.reloadPage();
  }


  /**
   * @ngdoc function
   * @name setDate
   * @description Met à jour l'intervalle de date
   * @param {string} format - Le format choisit par l'utilisateur
   */
  function setDate(format) {
    $scope.format=format;
    var today = moment();
    var end =  moment();
    switch (format) {
      case 0:
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(today);
        $scope.display = $filter('translate')('TODAY');
        $scope.dateRange = false;
        $scope.settings.format = 0;
        break;
      case 1:
        today = today.subtract(1, 'days');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(today);
        $scope.display = $filter('translate')('YESTERDAY');
        $scope.dateRange = false;
        $scope.settings.format  = 1;
        break;
      case 2:
        today = today.subtract(6, 'days');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(end);
        $scope.display = $filter('translate')('LAST_7_DAYS');
        $scope.dateRange = false;
        $scope.settings.format  = 2;
        break;
      case 3:
        today = today.subtract(29, 'days');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(end);
        $scope.display = $filter('translate')('LAST_30_DAYS');
        $scope.dateRange = false;
        $scope.settings.format  = 3;
        break;
      case 4:
        today = today.startOf('month');
        end = end.endOf('month');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(end);
        $scope.display = $filter('translate')('THIS_MONTH');
        $scope.dateRange = false;
        $scope.settings.format  = 4;
        break;
      case 5:
        today = today.subtract(1,'month').startOf('month');
        end =  end.subtract(1,'month').endOf('month');
        $scope.date.begin = new Date(today);
        $scope.date.end = new Date(end);
        $scope.display = $filter('translate')('LAST_MONTH');
        $scope.dateRange = false;
        $scope.settings.format  = 5;
        break;
      case 6:
        if($scope.settings.format == 6) {
          $scope.date.begin = new Date($scope.settings.startsAt);
          $scope.date.end = new Date($scope.settings.endsAt);
        }
        $scope.settings.format = 6;
        $scope.format = 6;
        if($scope.date.begin.getTime() == $scope.date.end.getTime())
          $scope.display = $filter('date')($scope.date.begin, 'mediumDate');
        else
          $scope.display = $filter('date')($scope.date.begin, 'mediumDate') + ' - ' + $filter('date')($scope.date.end, 'mediumDate');
        break;
      case 7:
        if($scope.date.begin.getTime() == $scope.date.end.getTime())
          $scope.display = $filter('date')($scope.date.begin, 'mediumDate');
        else
          $scope.display = $filter('date')($scope.date.begin, 'mediumDate') + ' - ' + $filter('date')($scope.date.end, 'mediumDate');
        $scope.dateRange = false;
        $scope.format = 6;
        $scope.settings.format  = 6;
        break;
    }
    $scope.settings.startsAt = $scope.date.begin;
    $scope.settings.endsAt = $scope.date.end;
    usersService.insertData($scope.users);
  }

}]);

/**
 * Created by VIF - 06/2015
 * @ngdoc factory
 * @name calendarService
 * @requires $http
 * @description Récupère et insère les données relatives aux évènements
 */
app.factory('calendarService',['$http',  function($http) {
  var calendarService = {};

  //Récupération des évènements
  calendarService.getData = function (callback) {
    return $http.get('../../json/events.json').success(callback);
  };

  //Insertion ou modification des évènements
  calendarService.insertData = function (data) {
    return $http({
      url: "../../json/events.json",
      method: "POST",
      data: data,
      headers: {'Content-Type': 'application/json'}});
  };

  return calendarService;
}]);

/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name calendarCtrl
 * @requires $scope
 * @requires 'scheduleService'
 * @requires 'inventoriesService'
 * @requires 'usersService'
 * @requires 'globalService'
 * @requires $pageTitle
 * @requires $filter
 * @requires $window
 * @requires 'calendarService'
 * @requires 'auth'
 * @requires 'adminService'
 * @description Interaction de la page du calendrier
 */

app.controller('calendarCtrl',['$scope', 'scheduleService', 'inventoriesService', '$pageTitle',  '$filter', 'usersService', 'globalService', '$window', 'receptionService', 'calendarService','auth', 'adminService',
  function($scope, scheduleService, inventoriesService, $pageTitle, $filter, usersService, globalService, $window, receptionService, calendarService, auth, adminService) {

  //On met la variable log.login à false pour l'apparence de la page
  $scope.log.login = false;

  //Set the page title
  $pageTitle.set($filter('translate')('CALENDAR'));

  //Récupération de l'utilisateur connecté
  var userid = auth.userName;

  //Initialisation du tableau des évènements
  $scope.events = [];

  //Initialisation des variables pour ajouter un nouvel évènement
  $scope.validAddBtn = false;
  $scope.addEvent = false;
  $scope.newEvent = {};

  //Initialisation du premier jour de la semaine pour les datepickers (1 - Lundi, 0 - Dimanche)
  $scope.dateOptions1 = {
    startingDay: 1
  };

  //Initialisation des variables pour filtrer le calendrier par type d'évènement
  $scope.calendar = {};
  $scope.filter = {};
  $scope.filter.orange = true;
  $scope.filter.blue = true;
  $scope.filter.green = true;
  $scope.filter.primary = true;
  $scope.filter.gray = true;
  $scope.filter.yellow = true;
  $scope.filter.danger = true;

  //Initialisation des fonctions
  $scope.deleteEvent =deleteEvent;
  $scope.eventDeleted =eventDeleted;
  $scope.modifyEvent = modifyEvent;
  $scope.createEvent = createEvent;
  $scope.setColor = setColor;
  $scope.openNewEvent = openNewEvent;
  $scope.openEnd = openEnd;
  $scope.openBegin =openBegin;
  $scope.displayType = displayType;
  $scope.reloadCal = reloadCal;

  //Initialisation de la date du calendrier à aujourd'hui
  $scope.calendar.day = moment();

  /**
   * @description Récupération des droits
   */
  adminService.getData(function(data) {
    $scope.rights = data;
  });

  /**
   * @description On observe si la variable newEvent.beginDate change de valeur
   * Si c'est le cas, et si la nouvelle valeur est supérieur à la date de fin, on
   * met à jour la date de fin.
   */
  $scope.$watch('newEvent.beginDate', function (newValue) {
    var addOneHour = new Date(newValue);
    addOneHour.setHours(addOneHour.getHours()+1);
    $scope.newEvent.endDate = (newValue >= $scope.newEvent.endDate) ? addOneHour : $scope.newEvent.endDate;
  });

  /**
   * @description On observe si la variable newEvent.endDate change de valeur
   * Si c'est le cas, et si la nouvelle valeur est inférieure à la date de début, on
   * met à jour la date de début.
   */
  $scope.$watch('newEvent.endDate', function (newValue) {
    var substractOneHour = new Date(newValue);
    substractOneHour.setHours(substractOneHour.getHours()-1);
    $scope.newEvent.beginDate = (newValue <= $scope.newEvent.beginDate) ? substractOneHour : $scope.newEvent.beginDate;
  });

  /**
   * @description Récupération de l'utilisateurs et des évènements qui lui sont lié
   */
 function reloadCal() {
    $scope.events = [];
    usersService.getUsers(function (data) {
      $scope.users = data;
      angular.forEach(data, function (user) {
        if (user.company == userid) {
          $scope.user = user;
          $scope.settings = user.settings;
          $scope.calendar.view = $scope.settings.view;

          /**
           * @description Récupération des ordres de fabrication et création des évènements liés
           */
          if ($scope.displayType('primary')) {
            scheduleService.getData(function (data) {
              $scope.ordres = data;
              angular.forEach(data, function (ordre) {
                if (ordre.subco == $scope.fil.subcoFilter || $scope.fil.subcoFilter == '' || $scope.fil.subcoFilter == undefined) {
                  if (!$scope.user.isSubco)
                    ordre.lib += ' [' + ordre.subco + ']';
                  var begin = new Date(ordre.date).setHours(8);
                  var end = new Date(ordre.date).setHours(18);
                  $scope.events.push({
                    title: $filter('translate')('PO') + ' ' + ordre.num + ' - ' + ordre.lib,
                    type: 'primary',
                    startsAt: begin,
                    endsAt: end,
                    editable: false,
                    deletable: false,
                    draggable: false,
                    resizable: false,
                    href: '#/orders/' + ordre.key
                  });
                }
              });
            });
          }

          /**
           * @description Récupération des inventaires et création des évènements liés
           */
          if ($scope.displayType('orange')) {
            inventoriesService.getData(function (data) {
              $scope.inventories = data;
              angular.forEach($scope.inventories, function (inventory) {
                if (inventory.subco == $scope.fil.subcoFilter || $scope.fil.subcoFilter == '' || $scope.fil.subcoFilter == undefined) {
                  if (!$scope.user.isSubco)
                    inventory.libelle += ' [' + inventory.subco + ']';
                  var begin = new Date(inventory.date).setHours(8);
                  var end = new Date(inventory.date).setHours(18);
                  $scope.events.push({
                    title: $filter('translate')('INVENTORY') + ' ' + inventory.chrono + ' - ' + inventory.libelle,
                    type: 'orange',
                    startsAt: begin,
                    endsAt: end,
                    editable: false,
                    deletable: false,
                    draggable: false,
                    resizable: false,
                    href: '#/stock/inventories/' + inventory.key
                  });
                }
              });
            });
          }

          /**
           * @description Récupération des réceptions et création des évènements liés
           */
          if ($scope.displayType('green')) {
            receptionService.getData(function (data) {
              angular.forEach(data, function (reception) {
                if (reception.subco == $scope.fil.subcoFilter || $scope.fil.subcoFilter == '' || $scope.fil.subcoFilter == undefined) {
                  if (!$scope.user.isSubco)
                    reception.wording += ' [' + reception.subco + ']';
                  var begin = new Date(reception.date).setHours(8);
                  var end = new Date(reception.date).setHours(18);
                  $scope.events.push({
                    title: $filter('translate')('RECEPTION') + ' ' + reception.command + ' - ' + reception.wording,
                    type: 'green',
                    startsAt: begin,
                    endsAt: end,
                    editable: false,
                    deletable: false,
                    draggable: false,
                    resizable: false,
                    href: '#/reception/' + reception.key
                  });
                }
              });
            });
          }

          /**
           * @description Récupération des évènements personnels
           */
          calendarService.getData(function (data) {
            $scope.eventsPers = data;
            angular.forEach($scope.eventsPers, function (event) {
              if($scope.displayType(event.type)) {
                var title = event.title.split('[');
                event.title = title[0];
                if ($scope.user.isSubco && event.subco == $scope.user.company) {
                  if (event.iddo != "" && event.iddo != null)
                    event.title += ' [' + event.iddo + ']';
                  $scope.events.push({
                    title: event.title,
                    type: event.type,
                    startsAt: event.startsAt,
                    endsAt: event.endsAt,
                    editable: (event.creator == $scope.user.company) && ((!$scope.user.isSubco && $scope.rights.custo.editEvent) || ($scope.user.isSubco && $scope.rights.subco.editEvent)),
                    deletable: (event.creator == $scope.user.company) && ((!$scope.user.isSubco && $scope.rights.custo.deleteEvent) || ($scope.user.isSubco && $scope.rights.subco.deleteEvent)),
                    draggable: true,
                    resizable: true,
                    key: event.key,
                    href: '#/calendar'
                  });
                } else if (!$scope.user.isSubco && event.iddo == $scope.user.company) {
                  if (event.subco != "" && event.subco != null)
                    event.title += ' [' + event.subco + ']';
                  $scope.events.push({
                    title: event.title,
                    type: event.type,
                    startsAt: event.startsAt,
                    endsAt: event.endsAt,
                    editable: ((!$scope.user.isSubco && $scope.rights.custo.editEvent) || ($scope.user.isSubco && $scope.rights.subco.editEvent)),
                    deletable: ((!$scope.user.isSubco && $scope.rights.custo.deleteEvent) || ($scope.user.isSubco && $scope.rights.subco.deleteEvent)),
                    draggable: true,
                    resizable: true,
                    key: event.key,
                    href: '#/calendar'
                  });
                }
              }
            })

          });
        }
      });
    });
  }

 $scope.reloadCal();

  /**
   * @ngdoc function
   * @name displayType
   * @description Définit si le type d'évènement peut être affiché ou non
   * @param {string} type - le type d'évènement dont on veut savoir s'il doit être affiché ou non
   * @returns {boolean} - true si l'évènement peut être affiché, false sinon.
   */
 function displayType(type) {
    switch(type) {
      case 'green' :
        return $scope.filter.green;
        break;
      case 'primary' :
        return $scope.filter.primary;
        break;
      case 'orange' :
        return $scope.filter.orange;
        break;
      case 'blue' :
        return $scope.filter.blue;
        break;
      case 'yellow' :
        return $scope.filter.yellow;
        break;
      case 'gray' :
        return $scope.filter.gray;
        break;
      case 'danger' :
        return $scope.filter.danger;
        break;
      default:
        return false;
        break;
    }
  }

  /**
   * @ngdoc function
   * @name openBegin
   * @description Ouvre le premier datepicker ou le ferme s'il était déjà ouvert
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openBegin($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedBegin = !$scope.openedBegin;
    $scope.openedEnd = false;
    $scope.newEvent.beginDate = new Date($scope.newEvent.beginDate);
    $scope.newEvent.endDate = new Date($scope.newEvent.endDate);
  }

  /**
   * @ngdoc function
   * @name openEnd
   * @description Ouvre le second datepicker ou le ferme s'il était déjà ouvert
   * @param {Event} $event - L'évènement d'ouverture
   */
  function openEnd($event) {
    $event.preventDefault();
    $event.stopPropagation();
    $scope.openedEnd = !$scope.openedEnd;
    $scope.openedBegin = false;
    $scope.newEvent.beginDate = new Date($scope.newEvent.beginDate);
    $scope.newEvent.endDate = new Date( $scope.newEvent.endDate);
  }


  /**
   * @ngdoc function
   * @name openNewEvent
   * @description Ouvre la boite de dialogue pour modifier ou ajouter un évènement
   * @param {Object} event - L'évènement à modifier, ou null si on souhaite en ajouter un nouveau
   */
 function openNewEvent(event) {
    $scope.newEvent = {};
    if(event == null) {
      //Add an event
      $scope.editModal = false;
      $scope.newEvent.color = 'gray';

      $scope.newEvent.beginDate = new Date();
      $scope.openBeginHour = false;
      $scope.newEvent.beginDate.setHours($scope.newEvent.beginDate.getHours() + 1);
      $scope.newEvent.beginDate.setMinutes(0);

      $scope.newEvent.endDate = new Date();
      $scope.openEndHour = false;
      $scope.newEvent.endDate.setHours($scope.newEvent.endDate.getHours() + 2);
      $scope.newEvent.endDate.setMinutes(0);

    } else {
      //Modify an event
      $scope.saveEvent = event;
      $scope.editModal = true;
      var title = event.title.split('[');
      if(title.length > 1) {
        var sub = '';
        angular.forEach(title[1], function(letter) {
          if(letter != ']')
            sub += letter;
        });
        $scope.newEvent.subco = sub;
        $scope.saveEvent.subco = sub;
      }
      $scope.newEvent.title = title[0];
      $scope.newEvent.color = event.type;
      $scope.newEvent.beginDate = event.startsAt;
      $scope.newEvent.endDate = event.endsAt;
    }
    $scope.addEvent = true;
 }

  /**
   * @ngdoc function
   * @name setColor
   * @description Change la couleur de l'évènement
   * @param {String} color - La couleur de l'évènement
   */
  function setColor(color) {
    $scope.newEvent.color = color;
  }

  /**
   * @ngdoc function
   * @name createEvent
   * @description Crée un nouvel évènement
   * @param {boolean} isValid - true si le formulaire pour créer un nouvel évènement est valide, false sinon
   */
  function createEvent(isValid) {
    if(isValid) {
      $scope.newEvent.beginDate = new Date($scope.newEvent.beginDate);
      $scope.newEvent.endDate = new Date($scope.newEvent.endDate);

      var newKey = 1;
      if ($scope.eventsPers.length > 0)
        newKey = $scope.eventsPers[$scope.eventsPers.length - 1].key + 1;
      if($scope.user.isSubco) {
        $scope.eventsPers.push({
          key: newKey,
          iddo: $scope.newEvent.subco || '',
          title: $scope.newEvent.title,
          startsAt: $scope.newEvent.beginDate,
          endsAt: $scope.newEvent.endDate,
          type: $scope.newEvent.color,
          subco: $scope.user.company,
          creator: $scope.user.company
        });
      } else {
        $scope.eventsPers.push({
          key: newKey,
          iddo: $scope.user.company,
          title: $scope.newEvent.title,
          startsAt: $scope.newEvent.beginDate,
          endsAt: $scope.newEvent.endDate,
          type: $scope.newEvent.color,
          subco: $scope.newEvent.subco,
          creator: $scope.user.company
        });
      }
      calendarService.insertData($scope.eventsPers);

      if ($scope.newEvent.subco != '' && $scope.newEvent.subco != null) {
        $scope.newEvent.title += ' [' + $scope.newEvent.subco + ']';
      }

      $scope.events.push({
        title: $scope.newEvent.title,
        type: $scope.newEvent.color,
        startsAt: $scope.newEvent.beginDate,
        endsAt: $scope.newEvent.endDate,
        editable: true,
        deletable: true,
        draggable: true,
        resizable: true,
        key: newKey,
        href: "#/calendar"
      });

      $scope.validAddBtn = false;
      $scope.addEvent = false;

      var notif = {"type":"event_new", "date":new Date(), "number" : $scope.newEvent.title.split('[')[0],  "active" : "active"};
      angular.forEach($scope.users, function(elt) {
        if($scope.newEvent.subco == elt.company) {
          elt.notifications.push(notif);
          usersService.insertData($scope.users);
        }
      });
    } else {
      $scope.validAddBtn = true;
    }
  }

  /**
   * @ngdoc function
   * @name modifyEvent
   * @description Modifie l'évènement sélectionné
   * @param {boolean} isValid - true si la forme pour modifier l'évènement est valide, false sinon
   */
  function modifyEvent(isValid) {
    var notif = {};
    if(isValid) {
      $scope.newEvent.beginDate = new Date($scope.newEvent.beginDate);
      $scope.newEvent.endDate = new Date($scope.newEvent.endDate);
      if($scope.user.isSubco) {
        angular.forEach($scope.eventsPers, function (evt) {
          if ($scope.saveEvent.key == evt.key) {
            evt.title = $scope.newEvent.title;
            evt.type = $scope.newEvent.color;
            evt.startsAt = $scope.newEvent.beginDate;
            evt.endsAt = $scope.newEvent.endDate;
            evt.iddo = $scope.newEvent.subco;
            evt.subco = $scope.user.company;
          }
        });
        if($scope.saveEvent.subco != $scope.newEvent.subco) {
          if($scope.newEvent.subco != '') {
            notif = {"type": "event_new", "date": new Date(), "title": $scope.newEvent.title.split('[')[0], "active": "active"};
            angular.forEach($scope.users, function (elt) {
              if ($scope.newEvent.subco == elt.company) {
                elt.notifications.push(notif);
                usersService.insertData($scope.users);
              }
            });
          }
          if($scope.saveEvent.subco != '') {
            notif = {"type": "event_delete", "date": new Date(), "title": $scope.newEvent.title.split('[')[0], "active": "active"};
            angular.forEach($scope.users, function (elt) {
              if ($scope.saveEvent.subco == elt.company) {
                elt.notifications.push(notif);
                usersService.insertData($scope.users);
              }
            });
          }
        } else if ($scope.saveEvent != $scope.newEvent && $scope.newEvent.subco != '') {
          notif = {"type":"event_modify", "date":new Date(), "title" : $scope.newEvent.title.split('[')[0],  "active" : "active"};
          angular.forEach($scope.users, function(elt) {
            if($scope.newEvent.subco == elt.company) {
              elt.notifications.push(notif);
              usersService.insertData($scope.users);
            }
          });
        }
      } else {
        angular.forEach($scope.eventsPers, function (evt) {
          if ($scope.saveEvent.key == evt.key) {
            evt.title = $scope.newEvent.title;
            evt.type = $scope.newEvent.color;
            evt.startsAt = $scope.newEvent.beginDate;
            evt.endsAt = $scope.newEvent.endDate;
            evt.subco = $scope.newEvent.subco;
            evt.iddo = $scope.user.company;
          }
        });
        if($scope.saveEvent.subco != $scope.newEvent.subco) {
          if($scope.newEvent.subco != '') {
            notif = {"type": "event_new", "date": new Date(), "title":$scope.newEvent.title.split('[')[0], "active": "active"};
            angular.forEach($scope.users, function (elt) {
              if ($scope.newEvent.subco == elt.company) {
                elt.notifications.push(notif);
                usersService.insertData($scope.users);
              }
            });
          }
          if($scope.saveEvent.subco != '') {
            notif = {"type": "event_delete", "date": new Date(), "title": $scope.newEvent.title.split('[')[0], "active": "active"};
            angular.forEach($scope.users, function (elt) {
              if ($scope.saveEvent.subco == elt.company) {
                elt.notifications.push(notif);
                usersService.insertData($scope.users);
              }
            });
          }
        } else if ($scope.saveEvent != $scope.newEvent && $scope.newEvent.subco != '') {
          notif = {"type":"event_modify", "date":new Date(), "title" : $scope.newEvent.title.split('[')[0],  "active" : "active"};
          angular.forEach($scope.users, function(elt) {
            if($scope.newEvent.subco == elt.company) {
              elt.notifications.push(notif);
              usersService.insertData($scope.users);
            }
          });
        }
      }
      calendarService.insertData($scope.eventsPers);

      if($scope.newEvent.subco != '' && $scope.newEvent.subco != null) {
        $scope.newEvent.title += ' [' + $scope.newEvent.subco + ']';
      }
      angular.forEach($scope.events, function(evt){
        if($scope.saveEvent == evt){
          evt.title = $scope.newEvent.title;
          evt.type = $scope.newEvent.color;
          evt.startsAt = $scope.newEvent.beginDate;
          evt.endsAt = $scope.newEvent.endDate;
        }
      });
      $scope.validAddBtn = false;
      $scope.addEvent = false;
    } else {
      $scope.validAddBtn = true;
    }
  }

  /**
   * @ngdoc function
   * @name eventDeleted
   * @description Ouvre la boite de dialogue pour supprimer un évèneement
   * @param {Event} calendarEvent - L'évènement sélectionné
   */
  function eventDeleted(calendarEvent) {
      $scope.saveEvent = calendarEvent;
      $scope.delEvt = true;
  }

  /**
   * @ngdoc function
   * @name deleteEvent
   * @description Supprime l'évènement sélectionné
   */
  function deleteEvent() {
    $scope.delEvt = false;
    var index = $scope.events.indexOf($scope.saveEvent);
    var index2 = $scope.eventsPers.indexOf($scope.saveEvent);
    $scope.events.splice(index, 1);
    $scope.eventsPers.splice(index2, 1);
    $scope.saveEvent = null;
    calendarService.insertData($scope.eventsPers);
  }

}]);

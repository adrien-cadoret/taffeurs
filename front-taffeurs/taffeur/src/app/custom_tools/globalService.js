/**
 * VIF - 06/2015
 * @ngdoc factory
 * @name globalService
 * @description Initialise les variables globales
 */

app.factory('globalService', function() {
  return {
    getUnit: ["P", "KG", "LB", "SAC", "PAL"],
    absolutePath: '../images/contacts/'
  };
});

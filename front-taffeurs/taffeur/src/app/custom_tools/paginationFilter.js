/**
 * VIF - 06/2015
 * @ngdoc filter
 * @name startFrom
 * @description Filtre qui permet de commencer une table à une page donnée (pagination)
 */

app.filter('startFrom', function () {
  return function (input, start) {
    if (input) {
      start = +start;
      return input.slice(start);
    }
    return [];
  };
});

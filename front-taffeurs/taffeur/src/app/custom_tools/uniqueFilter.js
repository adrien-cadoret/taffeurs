/**
 * VIF - 06/2015
 * @ngdoc filter
 * @name unique
 * @description Permet de supprimer les doublons dans une liste de données
 */

app.filter('unique', function() {
  return function(input, key) {
    var unique = {};
    var uniqueList = [];
    if(input != null) {
      for (var i = 0; i < input.length; i++) {
        if (typeof unique[input[i][key]] == "undefined") {
          unique[input[i][key]] = "";
          uniqueList.push(input[i]);
        }
      }
    }
    return uniqueList;
  };
});

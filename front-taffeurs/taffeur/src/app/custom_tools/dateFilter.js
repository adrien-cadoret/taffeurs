/**
 * VIF - 06/2015
 * @ngdoc filter
 * @name isBetween
 * @description Filtre les valeurs entre deux dates
 */

app.filter('isBetween', function() {
  return function(items, beginDate, endDate) {
    var begin = moment(beginDate);
    var end = moment(endDate);

    //On soustrait un jour à la date de début et un jour à la date de fin car
    //la fonction est esclusive et on veut qu'elle soit inclusive
    begin = begin.subtract(1, 'days');
    end = end.add(1, 'days');

    return items.filter(function(item){
      return moment(item.date).isBetween(begin, end, 'day');
    })
  }
});

/**
 * VIF - 06/2015
 * @ngdoc controller
 * @name 404Ctrl
 * @description Interaction de la page 404
 */

app.controller("404Ctrl", ['$scope', function ($scope) {
  $scope.log.login = true;
}]);

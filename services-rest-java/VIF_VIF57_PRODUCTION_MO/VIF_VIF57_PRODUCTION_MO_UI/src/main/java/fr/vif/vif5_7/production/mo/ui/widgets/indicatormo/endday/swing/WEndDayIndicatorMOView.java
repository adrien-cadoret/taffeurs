/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WEndDayIndicatorMOView.java,v $
 * Created on 15 avr. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.swing;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.RowSorter;
import javax.swing.SortOrder;
import javax.swing.UIManager;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableModel;
import javax.swing.table.TableRowSorter;

import com.jidesoft.dashboard.Gadget;
import com.jidesoft.dashboard.GadgetComponent;

import fr.vif.jtech.common.beans.DateBoundBean;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.events.generic.ModelChangeListener;
import fr.vif.jtech.ui.laf.swing.LAFHelper;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.views.Displayable;
import fr.vif.jtech.ui.widget.WidgetActionEvent;
import fr.vif.jtech.ui.widget.swing.SwingIndicatorWidgetView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOTypeEnum;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.InvoicedArticleBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedTrsLineBean;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.AbstractEndDayIndicatorTableModel;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.WEndDayIndicatorMOIView;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.WEndDayIndicatorMOModel;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.dialog.parameter.EndDayParameterDialogBean;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.dialog.parameter.EndDayParameterDialogCtrl;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.model.InvoicedArticleTableModel;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.model.UnclosedOnLineControlTableModel;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.model.UnclosedTrsTableModel;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.model.UnclosedWorkingPeriodTableModel;


/**
 * EndDay Indicator View.
 *
 * @author ac
 */
public class WEndDayIndicatorMOView extends SwingIndicatorWidgetView implements Displayable, WEndDayIndicatorMOIView,
        GadgetComponent, ModelChangeListener, DialogChangeListener, DisplayListener, TableModelListener, ActionListener {

    /**
     * 
     * Customized circle component.
     *
     * @author ac
     */
    public class Circle extends JComponent {

        private static final long serialVersionUID = 1L;
        private String            text             = "";

        /**
         * Unique Constructor.
         * 
         * @param text the text inside of the circle.
         */
        public Circle(final String text) {
            // CHECKSTYLE:OFF
            setPreferredSize(new Dimension(250, 50));
            this.text = text;
        }

        @Override
        public void paintComponent(final Graphics g) {
            super.paintComponent(g);
            // CHECKSTYLE:OFF
            if (0 != Integer.parseInt(text)) {
                g.setColor(new Color(223, 84, 15));
            } else {
                g.setColor(new Color(119, 221, 119));
            }
            g.fillOval(100, 0, 50, 50);
            g.setColor(Color.WHITE);
            g.setFont(new Font("Arial", Font.BOLD, 40));
            switch (text.length()) {
                case 1:
                    g.drawString(text, 115/* x */, 40/* y */);
                    break;
                case 2:
                    g.drawString(text, 102/* x */, 40/* y */);
                    break;
                case 3:
                    g.setFont(new Font("Arial", Font.BOLD, 26));
                    g.drawString(text, 105/* x */, 35/* y */);
                    break;
                default:
                    g.setFont(new Font("Arial", Font.BOLD, 20));

                    g.drawString(text, 105/* x */, 35/* y */);
                    break;
            }

            // CHECKSTYLE:ON

        }
    }

    /** End of day indicators Lists. */
    private JTable                       invoicedArticlesList;
    private JTable                       unclosedWorkingPeriodsList;
    private JTable                       unclosedTrsLinesList;
    private JTable                       unclosedOnLineControlsList;

    /** End of day indicators Panels. */
    private JPanel                       invoicePanel;
    private JPanel                       unclosedWorkingPeriodsPanel;
    private JPanel                       unclosedTrsLinesPanel;
    private JPanel                       unclosedOnLineControlsPanel;

    /** List of indicators to display. */
    private Collection<EndDayMOTypeEnum> indicatorsToDisplay = new ArrayList<EndDayMOTypeEnum>();

    /** Change production date menu item. */
    private JMenuItem                    productionDateMenuItem;

    /** Change time window menu item. */
    private JMenuItem                    timeWindowMenuItem;

    /** Popup Menu. */
    private JPopupMenu                   contextualPopupMenu;

    private JPanel                       mainPanel;

    private JLabel                       numberOfLineLabel;

    private int                          uniqueId            = 0;

    /**
     * Constructor.
     * 
     * @param gadget gadget inside the view.
     */
    public WEndDayIndicatorMOView(final Gadget gadget) {
        super(gadget);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {

        if (ACTION_PARAMETER_FEATURE.equals(e.getActionCommand())) {
            WidgetActionEvent eventToSend = new WidgetActionEvent(this, ACTION_PARAMETER_FEATURE);

            eventToSend.setEventId(ACTION_PARAMETER_FEATURE);
            fireWidgetActionPerformed(eventToSend);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addIndicator(final EndDayMOTypeEnum indicator) {
        switch (indicator) {
            case MATERIAL:
                mainPanel.add(invoicePanel);
                break;
            case WORKING_PERIOD:
                mainPanel.add(unclosedWorkingPeriodsPanel);
                break;
            case TRS:
                mainPanel.add(unclosedTrsLinesPanel);
                break;
            case ON_LINE_CONTROL:
                mainPanel.add(unclosedOnLineControlsPanel);
                break;

            default:
                break;
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addTextToTitle(final String text) {
        this.setTitle(getInitTitle() + " (" + getModel().getIdentification().getCompany() + ","
                + getModel().getIdentification().getEstablishment() + ") " + text);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {

        if (event.getSource() instanceof EndDayParameterDialogCtrl) {
            EndDayParameterDialogBean bean = (EndDayParameterDialogBean) event.getDialogBean();

            WidgetActionEvent eventToSend = new WidgetActionEvent(this, WEndDayIndicatorMOIView.EVENT_CHANGE_PARAMETER);
            eventToSend.setEventId(EVENT_CHANGE_PARAMETER);
            eventToSend.setEventParameter(bean);
            fireWidgetActionPerformed(eventToSend);

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayRequested(final DisplayEvent event) {
        event.getViewToDisplay().showView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fillIndicatorsListToDisplay(final Collection<EndDayMOTypeEnum> toDisplay) {
        removeAllIndicators();

        this.indicatorsToDisplay = toDisplay;
        // Build panel relating to the indicatorsToDisplay
        for (EndDayMOTypeEnum indicatorToDisplay : getIndicatorsToDisplay()) {
            buildIndicatorView(indicatorToDisplay);
        }
        this.updateUI();

    }

    /**
     * Gets the contextualPopupMenu.
     * 
     * @return the popupMenu.
     */
    public JPopupMenu getContextualPopupMenu() {
        if (contextualPopupMenu == null) {
            contextualPopupMenu = new JPopupMenu();
        }
        return contextualPopupMenu;
    }

    /**
     * Gets the indicatorsToDisplay.
     *
     * @return the indicatorsToDisplay.
     */
    public Collection<EndDayMOTypeEnum> getIndicatorsToDisplay() {
        return indicatorsToDisplay;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getSettings() {
        Map<String, String> map = super.getSettings();
        map.put("uniqueId", String.valueOf(uniqueId));
        return map;
    }

    /**
     * Gets the uniqueId.
     *
     * @return the uniqueId.
     */
    @Override
    public int getUniqueId() {
        return uniqueId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {
        super.initializeView();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeAllIndicators() {
        mainPanel.removeAll();
        this.updateUI();
    }

    /**
     * Sets the popupMenu.
     * 
     * @param cPopupMenu popupMenu.
     */
    public void setContextualPopupMenu(final JPopupMenu cPopupMenu) {
        this.contextualPopupMenu = cPopupMenu;
    }

    /**
     * 
     * Define Indicators Menu.
     * 
     */
    public void setIndicatorsMenu() {
        JMenuItem parameterJMenuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T40705, false));
        parameterJMenuItem.setActionCommand(WEndDayIndicatorMOIView.ACTION_PARAMETER_FEATURE);
        parameterJMenuItem.addActionListener(this);

        this.addMenuItemInToolPopup(parameterJMenuItem);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSettings(final Map<String, String> settings) {
        super.setSettings(settings);
        if (!"".equals(settings.get("uniqueId"))) {
            this.setUniqueId(Integer.parseInt(settings.get("uniqueId")));
        }
        validate();
    }

    /**
     * Sets the uniqueId.
     *
     * @param uniqueId uniqueId.
     */
    @Override
    public void setUniqueId(final int uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tableChanged(final TableModelEvent e) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateInvoicedArticleList() {
        // Add bean elements in JTable
        ((InvoicedArticleTableModel) getJTable(EndDayMOTypeEnum.MATERIAL).getModel())
                .setEndDayMOList(((WEndDayIndicatorMOModel) getModel()).getInvoicedArticleBeans());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateUnclosedOnLineControlList() {
        // Add bean elements in JTable
        ((UnclosedOnLineControlTableModel) getJTable(EndDayMOTypeEnum.ON_LINE_CONTROL).getModel())
                .setEndDayMOList(((WEndDayIndicatorMOModel) getModel()).getUnclosedOnLineControlBeans());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateUnclosedTrsList() {
        // Add bean elements in JTable
        ((UnclosedTrsTableModel) getJTable(EndDayMOTypeEnum.TRS).getModel())
                .setEndDayMOList(((WEndDayIndicatorMOModel) getModel()).getUnclosedTrsLineBeans());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateUnclosedWorkingPeriodList() {
        // Add bean elements in JTable
        ((UnclosedWorkingPeriodTableModel) getJTable(EndDayMOTypeEnum.WORKING_PERIOD).getModel())
                .setEndDayMOList(((WEndDayIndicatorMOModel) getModel()).getUnclosedWorkingPeriodBeans());
    }

    @Override
    protected void initialize() {

        super.initialize();

        getContentPane().setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        setBackground(UIManager.getColor(LAFHelper.LAF_COLLAPSIBLE_PANE_BORDER));
        // JScroll Panel
        JScrollPane indicatorScrollPane = new JScrollPane();
        // CHECKSTYLE:OFF
        indicatorScrollPane.getVerticalScrollBar().setUnitIncrement(30);
        // CHECKSTYLE:ON

        indicatorScrollPane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        indicatorScrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
        mainPanel = new JPanel(new GridLayout(2, 2));

        indicatorScrollPane.setViewportView(mainPanel);
        getContentPane().add(indicatorScrollPane);
    }

    /**
     * Build indicator view.
     * 
     * @param indicatorToDisplay the EndDay indicator
     */
    private void buildIndicatorView(final EndDayMOTypeEnum indicatorToDisplay) {

        ImageIcon img = null;
        JLabel title = null;
        JTable jTable = null;
        JPanel jPanel = null;

        switch (indicatorToDisplay) {
            case MATERIAL:
                img = new ImageIcon(this.getClass().getResource("/images/widgets/matiere16.png"));
                title = new JLabel(I18nClientManager.translate(ProductionMo.T40688, false));
                jTable = getJTable(EndDayMOTypeEnum.MATERIAL);
                invoicePanel = new JPanel();
                jPanel = invoicePanel;
                break;
            case WORKING_PERIOD:
                img = new ImageIcon(this.getClass().getResource("/images/widgets/mo16.png"));
                title = new JLabel(I18nClientManager.translate(ProductionMo.T40689, false));
                jTable = getJTable(EndDayMOTypeEnum.WORKING_PERIOD);
                unclosedWorkingPeriodsPanel = new JPanel();
                jPanel = unclosedWorkingPeriodsPanel;
                break;
            case TRS:
                img = new ImageIcon(this.getClass().getResource("/images/widgets/trs16.png"));
                title = new JLabel(I18nClientManager.translate(ProductionMo.T40690, false));
                jTable = getJTable(EndDayMOTypeEnum.TRS);
                unclosedTrsLinesPanel = new JPanel();
                jPanel = unclosedTrsLinesPanel;
                break;
            case ON_LINE_CONTROL:
                img = new ImageIcon(this.getClass().getResource("/images/widgets/qualite16.png"));
                title = new JLabel(I18nClientManager.translate(ProductionMo.T40691, false));
                jTable = getJTable(EndDayMOTypeEnum.ON_LINE_CONTROL);
                unclosedOnLineControlsPanel = new JPanel();
                jPanel = unclosedOnLineControlsPanel;
                break;
            default:
                break;
        }

        jPanel.setLayout(new BorderLayout());

        JPanel north = new JPanel(new BorderLayout());

        JLabel icon = new JLabel(img);
        // CHECKSTYLE:OFF
        title.setFont(new Font("Arial", Font.BOLD, 14));
        JPanel pTitle = new JPanel(new BorderLayout(5, 0));
        // CHECKSTYLE:ON

        pTitle.add(icon, BorderLayout.WEST);
        pTitle.add(title, BorderLayout.CENTER);
        pTitle.setBackground(Color.WHITE);

        JPanel numberOfLinePanel = new JPanel();
        numberOfLinePanel.setForeground(Color.WHITE);
        numberOfLinePanel.setBackground(Color.WHITE);

        numberOfLinePanel.add(new Circle((String.valueOf(jTable.getModel().getRowCount()))));

        north.add(pTitle, BorderLayout.SOUTH);
        north.add(numberOfLinePanel, BorderLayout.NORTH);

        jPanel.add(north, BorderLayout.NORTH);
        jPanel.add(new JScrollPane(jTable), BorderLayout.CENTER);

        // jTable.setBorder(null);
        // jPanel.setBorder(BorderFactory.createMatteBorder(1, 1, 1, 1, Color.BLACK));
        mainPanel.add(jPanel);

    }

    /**
     * Calculates the min and max date.
     * 
     * @param twBeginDate the begin date
     * @param twBeginTime the begin time
     * @param twEndDate the end date
     * @param twEndTime the end time
     * @return a dateBoundBean
     */
    private DateBoundBean calculateDateRange(final Date twBeginDate, final Date twBeginTime, final Date twEndDate,
            final Date twEndTime) {
        DateBoundBean dateBoundBean = null;

        // Min date bound.

        Calendar cBeginDate = Calendar.getInstance();
        Calendar cBeginTime = Calendar.getInstance();
        cBeginDate.setTime(twBeginDate);
        cBeginTime.setTime(twBeginTime);
        cBeginDate.add(Calendar.HOUR_OF_DAY, cBeginTime.get(Calendar.HOUR_OF_DAY));
        cBeginDate.add(Calendar.MINUTE, cBeginTime.get(Calendar.MINUTE));

        // Max date bound.

        Calendar cEndDate = Calendar.getInstance();
        Calendar cEndTime = Calendar.getInstance();
        cEndDate.setTime(twEndDate);
        cEndTime.setTime(twEndTime);
        cEndDate.add(Calendar.HOUR_OF_DAY, cEndTime.get(Calendar.HOUR_OF_DAY));
        cEndDate.add(Calendar.MINUTE, cEndTime.get(Calendar.MINUTE));

        dateBoundBean = new DateBoundBean(cBeginDate.getTime(), cEndDate.getTime());

        return dateBoundBean;

    }

    /**
     * 
     * Configure the table.
     * 
     * @param endMOList the jTable
     */
    private void configureTable(final JTable endMOList) {
        endMOList.setShowGrid(false);
        endMOList.setRowSelectionAllowed(true);
        // endMOList.setCellSelectionEnabled(true);
        endMOList.setAutoCreateRowSorter(false);
        // table.setColumnSelectionAllowed(false);
        endMOList.setTableHeader(new JTableHeader(endMOList.getColumnModel()));
        // CHECKSTYLE:OFF
        endMOList.setPreferredScrollableViewportSize(new Dimension(200, 100));
        // CHECKSTYLE:ON

        endMOList.setAutoscrolls(true);
        endMOList.setFillsViewportHeight(true);
        endMOList.getTableHeader().setResizingAllowed(true);
    }

    /**
     * 
     * Get JTable.
     * 
     * @param indicatorType the endDay indicator type
     * @return the matched table.
     */
    private JTable getJTable(final EndDayMOTypeEnum indicatorType) {

        JTable table = new JTable();

        if (EndDayMOTypeEnum.MATERIAL.equals(indicatorType)) {
            if (invoicedArticlesList == null) {
                TableModel tableModel = new InvoicedArticleTableModel();

                tableModel.addTableModelListener(this);
                invoicedArticlesList = new JTable(tableModel) {

                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    public String getToolTipText(final MouseEvent event) {
                        String tooltip = null;
                        Point p = event.getPoint();
                        int row = getJTable(EndDayMOTypeEnum.MATERIAL).rowAtPoint(p);
                        if (row >= 0) {
                            if (((InvoicedArticleTableModel) getModel()).getEndDayMOList() != null) {
                                InvoicedArticleBean bean = (InvoicedArticleBean) ((InvoicedArticleTableModel) getModel())
                                        .getEndDayMOList().get(row);
                                StringBuilder sb = new StringBuilder();

                                // Item label
                                StringBuilder itemLabel = null;
                                if (bean.getClArt().getLabel() != null && !bean.getClArt().getLabel().isEmpty()) {
                                    itemLabel = new StringBuilder();
                                    itemLabel.append("<b>")
                                            //
                                            .append(I18nClientManager.translate(ProductionMo.T39342, false))
                                            .append(" : </b>").append(bean.getClArt().getLabel());
                                }

                                // Quantity to do
                                StringBuilder qtarfer = null;
                                qtarfer = new StringBuilder();
                                qtarfer.append("<b>")
                                        //
                                        .append(I18nClientManager.translate(ProductionMo.T32547, false))
                                        .append(" : </b>").append(bean.getQtartafer1()).append(" " + bean.getCuart1());

                                // Quantity done
                                StringBuilder qtarfait = null;
                                qtarfait = new StringBuilder();
                                qtarfait.append("<b>")
                                        //
                                        .append(I18nClientManager.translate(ProductionMo.T32548, false))
                                        .append(" : </b>").append(bean.getQtartfait1()).append(" " + bean.getCuart1());

                                // Create Tooltip
                                if ((itemLabel != null && itemLabel.length() > 0)) {
                                    if (itemLabel != null) {
                                        sb.append(itemLabel);
                                    }

                                    if (qtarfer != null) {
                                        if (sb.length() > 0) {
                                            sb.append("<br>");
                                        }
                                        sb.append(qtarfer);
                                    }
                                    if (qtarfait != null) {
                                        if (sb.length() > 0) {
                                            sb.append("<br>");
                                        }
                                        sb.append(qtarfait);
                                    }
                                    if (sb.length() > 0) {
                                        sb.append("<br>");
                                    }
                                    sb.append("</html>");
                                    tooltip = "<html>" + sb.toString() + "</html>";
                                }

                            }

                        }
                        return tooltip;
                    }
                };
                // CHECKSTYLE:OFF
                setColumn(invoicedArticlesList, 0, CellInfoEnum.WORKSTATION, 70, 1000, true);
                setColumn(invoicedArticlesList, 1, CellInfoEnum.LINE, 80, 1000, true);
                setColumn(invoicedArticlesList, 2, CellInfoEnum.CHRONO, 70, 1000, true);
                setColumn(invoicedArticlesList, 3, CellInfoEnum.TYPACTA, 150, 1000, true);
                setColumn(invoicedArticlesList, 4, CellInfoEnum.CART, 140, 1000, true);
                setColumn(invoicedArticlesList, 5, CellInfoEnum.DECLARED, 90, 1000, true);

                configureTable(invoicedArticlesList);

                RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);
                List<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();
                sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(2, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(3, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(4, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(5, SortOrder.ASCENDING));
                // CHECKSTYLE:ON

                sorter.setSortKeys(sortKeys);
                invoicedArticlesList.setRowSorter(sorter);

                // invoicedArticlesList.getRowSorter().toggleSortOrder(0);
            }
            table = invoicedArticlesList;
        }
        if (EndDayMOTypeEnum.WORKING_PERIOD.equals(indicatorType)) {

            if (unclosedWorkingPeriodsList == null) {
                AbstractEndDayIndicatorTableModel tableModel = new UnclosedWorkingPeriodTableModel();

                tableModel.addTableModelListener(this);
                unclosedWorkingPeriodsList = new JTable(tableModel);
                // CHECKSTYLE:OFF
                setColumn(unclosedWorkingPeriodsList, 0, CellInfoEnum.LINE, 400, 1000, true);
                setColumn(unclosedWorkingPeriodsList, 1, CellInfoEnum.BEGIN_DATE, 400, 1000, true);
                // CHECKSTYLE:ON

                configureTable(unclosedWorkingPeriodsList);

                RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);
                unclosedWorkingPeriodsList.setRowSorter(sorter);
                List<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();
                sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
                sorter.setSortKeys(sortKeys);
            }
            table = unclosedWorkingPeriodsList;
        }

        // jtable = unclosedWorkingPeriodsList;
        if (EndDayMOTypeEnum.TRS.equals(indicatorType)) {

            if (unclosedTrsLinesList == null) {
                AbstractEndDayIndicatorTableModel tableModel = new UnclosedTrsTableModel();

                tableModel.addTableModelListener(this);
                unclosedTrsLinesList = new JTable(tableModel) {

                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    public String getToolTipText(final MouseEvent event) {
                        String tooltip = null;
                        Point p = event.getPoint();
                        int row = getJTable(EndDayMOTypeEnum.TRS).rowAtPoint(p);
                        if (row >= 0) {
                            if (((UnclosedTrsTableModel) getModel()).getEndDayMOList() != null) {
                                UnclosedTrsLineBean bean = (UnclosedTrsLineBean) ((UnclosedTrsTableModel) getModel())
                                        .getEndDayMOList().get(row);
                                StringBuilder sb = new StringBuilder();

                                // Reason Label
                                StringBuilder reasonLabel = null;
                                if (bean.getReason().getLabel() != null && !bean.getReason().getLabel().isEmpty()) {
                                    reasonLabel = new StringBuilder();
                                    reasonLabel
                                            .append("<b>")
                                            //
                                            .append(I18nClientManager.translate(ProductionMo.T40699, false))
                                            .append(" : </b>")
                                            .append(bean.getReason().getLabel().replaceAll("\\n", "<br>")
                                                    .replaceAll("\\\n", "<br>").trim());
                                }

                                // Create Tooltip
                                if ((reasonLabel != null && reasonLabel.length() > 0)) {
                                    if (reasonLabel != null) {
                                        sb.append(reasonLabel);
                                    }
                                    if (sb.length() > 0) {
                                        sb.append("<br>");
                                    }
                                    sb.append("</html>");
                                    tooltip = "<html>" + sb.toString() + "</html>";
                                }

                            }

                        }
                        return tooltip;
                    }
                };
                // CHECKSTYLE:OFF
                setColumn(unclosedTrsLinesList, 0, CellInfoEnum.LINE, 120, 1000, true);
                setColumn(unclosedTrsLinesList, 1, CellInfoEnum.BEGIN_DATE, 250, 1000, true);
                setColumn(unclosedTrsLinesList, 2, CellInfoEnum.CHRONO_MO, 130, 1000, true);
                setColumn(unclosedTrsLinesList, 3, CellInfoEnum.MO_LABEL, 200, 1000, true);
                setColumn(unclosedTrsLinesList, 4, CellInfoEnum.REASON, 120, 1000, true);
                // CHECKSTYLE:ON

                configureTable(unclosedTrsLinesList);

                RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);
                unclosedTrsLinesList.setRowSorter(sorter);
                List<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();
                sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(2, SortOrder.ASCENDING));
                sorter.setSortKeys(sortKeys);

            }
            table = unclosedTrsLinesList;
        }

        // jtable = unclosedTrsLinesList;
        if (EndDayMOTypeEnum.ON_LINE_CONTROL.equals(indicatorType)) {
            if (unclosedOnLineControlsList == null) {
                AbstractEndDayIndicatorTableModel tableModel = new UnclosedOnLineControlTableModel();

                tableModel.addTableModelListener(this);
                unclosedOnLineControlsList = new JTable(tableModel);
                // CHECKSTYLE:OFF
                setColumn(unclosedOnLineControlsList, 0, CellInfoEnum.WORKSTATION, 70, 1000, true);
                setColumn(unclosedOnLineControlsList, 1, CellInfoEnum.LINE, 120, 1000, true);
                setColumn(unclosedOnLineControlsList, 2, CellInfoEnum.CREATION_DATE, 250, 1000, true);
                setColumn(unclosedOnLineControlsList, 3, CellInfoEnum.CHRONO, 100, 1000, true);
                setColumn(unclosedOnLineControlsList, 4, CellInfoEnum.CART, 200, 1000, true);
                // CHECKSTYLE:ON

                configureTable(unclosedOnLineControlsList);

                RowSorter<TableModel> sorter = new TableRowSorter<TableModel>(tableModel);
                unclosedOnLineControlsList.setRowSorter(sorter);
                List<RowSorter.SortKey> sortKeys = new ArrayList<RowSorter.SortKey>();
                sortKeys.add(new RowSorter.SortKey(0, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(1, SortOrder.ASCENDING));
                sortKeys.add(new RowSorter.SortKey(2, SortOrder.ASCENDING));
                sorter.setSortKeys(sortKeys);

            }
            table = unclosedOnLineControlsList;
        }
        return table;
    }

    /**
     * 
     * Sets the configuration of a jtable column.
     * 
     * @param table table
     * @param columnIndex columnIndex
     * @param info info
     * @param preferredWidth preferredWidth
     * @param maxWidth maxWidth
     * @param isResizable isResizable
     */
    private void setColumn(final JTable table, final int columnIndex, final CellInfoEnum info,
            final int preferredWidth, final int maxWidth, final boolean isResizable) {
        TableColumn column = table.getColumnModel().getColumn(columnIndex);
        column.setPreferredWidth(preferredWidth);
        column.setMaxWidth(maxWidth);
        column.setResizable(isResizable);
    }

}

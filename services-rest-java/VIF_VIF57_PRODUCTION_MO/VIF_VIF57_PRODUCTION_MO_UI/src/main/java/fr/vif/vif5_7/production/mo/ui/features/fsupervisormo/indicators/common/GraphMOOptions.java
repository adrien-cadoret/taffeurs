/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_COMMON
 * File : $RCSfile: GraphMOOptions.java,v $
 * Created on 5 août 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import fr.vif.vif5_7.production.mo.constants.GanttValueTypeEnum;


/**
 * Contains options for gantt graph.
 * 
 * @author jd
 */
public class GraphMOOptions implements Serializable {
    /**
     * Serial Version UID.
     */
    private static final long        serialVersionUID   = 1L;

    /** Selected date. */
    private Date                     selectedDate       = new Date();

    /**
     * Set the firstMinuteToShow.
     */
    private int                      firstMinuteToShow  = 0;

    /**
     * Horizon to show.
     */
    private int                      nbMinutesToShow    = 0;

    /**
     * oldScrollBarValue.
     */
    private int                      oldScrollBarValue  = 0;                                  // NLE

    /**
     * scrollBarValue.
     */
    private int                      scrollBarValue     = 0;                                  // NLE

    /**
     * Number of data set to show in Gantt.
     */
    private int                      numberOfDataSetMax = 10;

    /**
     * number of the First data set to show.
     */
    private int                      startDataSet       = 0;

    /**
     * Expected begin date of first MO of the selected day.
     */
    private Calendar                 beginOfFirstMO     = Calendar.getInstance();

    /**
     * Expected end date of last MO of the selected day.
     */
    private Calendar                 endOfLastMO        = Calendar.getInstance();

    private int                      lowerBound         = 0;

    private int                      upperBound         = 1440;

    private boolean                  overview           = false;

    /**
     * show Annotations in Gantt Chart.
     */
    private boolean                  showAnnotations    = true;

    private List<GanttValueTypeEnum> toShow             = new ArrayList<GanttValueTypeEnum>();

    /**
     * Constructor.
     */
    public GraphMOOptions() {
        super();
    }

    /**
     * Gets the beginOfFirstMO.
     *
     * @return the beginOfFirstMO.
     */
    public Calendar getBeginOfFirstMO() {
        return beginOfFirstMO;
    }

    /**
     * Gets the endOfLastMO.
     *
     * @return the endOfLastMO.
     */
    public Calendar getEndOfLastMO() {
        return endOfLastMO;
    }

    /**
     * Gets the firstMinuteToShow.
     *
     * @return the firstMinuteToShow.
     */
    public int getFirstMinuteToShow() {
        return firstMinuteToShow;
    }

    /**
     * Gets the lowerBound.
     *
     * @return the lowerBound.
     */
    public int getLowerBound() {
        return lowerBound;
    }

    /**
     * Gets the nbMinutesToShow.
     *
     * @return the nbMinutesToShow.
     */
    public int getNbMinutesToShow() {
        return nbMinutesToShow;
    }

    /**
     * Gets the numberOfDataSetMax.
     * 
     * @return the numberOfDataSetMax.
     */
    public int getNumberOfDataSetMax() {
        return numberOfDataSetMax;
    }

    /**
     * Gets the oldScrollBarValue.
     * 
     * @return the oldScrollBarValue.
     */
    public int getOldScrollBarValue() {
        return oldScrollBarValue;
    }

    /**
     * Gets the overview.
     *
     * @return the overview.
     */
    public boolean getOverview() {
        return overview;
    }

    /**
     * Gets the scrollBarValue.
     * 
     * @return the scrollBarValue.
     */
    public int getScrollBarValue() {
        return scrollBarValue;
    }

    /**
     * Gets the selectedDate.
     *
     * @return the selectedDate.
     */
    public Date getSelectedDate() {
        return selectedDate;
    }

    /**
     * Gets the showAnnotations.
     * 
     * @return the showAnnotations.
     */
    public boolean getShowAnnotations() {
        return showAnnotations;
    }

    /**
     * Gets the startDataSet.
     * 
     * @return the startDataSet.
     */
    public int getStartDataSet() {
        return startDataSet;
    }

    /**
     * Gets the toShow.
     *
     * @return the toShow.
     */
    public List<GanttValueTypeEnum> getToShow() {
        return toShow;
    }

    /**
     * Gets the upperBound.
     *
     * @return the upperBound.
     */
    public int getUpperBound() {
        return upperBound;
    }

    /**
     * Sets the beginOfFirstMO.
     *
     * @param beginOfFirstMO beginOfFirstMO.
     */
    public void setBeginOfFirstMO(final Calendar beginOfFirstMO) {
        this.beginOfFirstMO = beginOfFirstMO;
    }

    /**
     * Sets the endOfLastMO.
     *
     * @param endOfLastMO endOfLastMO.
     */
    public void setEndOfLastMO(final Calendar endOfLastMO) {
        this.endOfLastMO = endOfLastMO;
    }

    /**
     * Sets the firstMinuteToShow.
     *
     * @param firstMinuteToShow firstMinuteToShow.
     */
    public void setFirstMinuteToShow(final int firstMinuteToShow) {
        this.firstMinuteToShow = firstMinuteToShow;
    }

    /**
     * Sets the lowerBound.
     *
     * @param lowerBound lowerBound.
     */
    public void setLowerBound(final int lowerBound) {
        this.lowerBound = lowerBound;
    }

    /**
     * Sets the nbMinutesToShow.
     *
     * @param nbMinutesToShow nbMinutesToShow.
     */
    public void setNbMinutesToShow(final int nbMinutesToShow) {
        this.nbMinutesToShow = nbMinutesToShow;
    }

    /**
     * Sets the numberOfDataSetMax.
     * 
     * @param numberOfDataSetMax numberOfDataSetMax.
     */
    public void setNumberOfDataSetMax(final int numberOfDataSetMax) {
        this.numberOfDataSetMax = numberOfDataSetMax;
    }

    /**
     * Sets the oldScrollBarValue.
     * 
     * @param oldScrollBarValue oldScrollBarValue.
     */
    public void setOldScrollBarValue(final int oldScrollBarValue) {
        this.oldScrollBarValue = oldScrollBarValue;
    }

    /**
     * Sets the overview.
     *
     * @param overview overview.
     */
    public void setOverview(final boolean overview) {
        this.overview = overview;
    }

    /**
     * Sets the scrollBarValue.
     * 
     * @param scrollBarValue scrollBarValue.
     */
    public void setScrollBarValue(final int scrollBarValue) {
        this.scrollBarValue = scrollBarValue;
    }

    /**
     * Sets the selectedDate.
     *
     * @param selectedDate selectedDate.
     */
    public void setSelectedDate(final Date selectedDate) {
        this.selectedDate = selectedDate;
    }

    /**
     * Sets the showAnnotations.
     * 
     * @param showAnnotations showAnnotations.
     */
    public void setShowAnnotations(final boolean showAnnotations) {
        this.showAnnotations = showAnnotations;
    }

    /**
     * Sets the startDataSet.
     * 
     * @param startDataSet startDataSet.
     */
    public void setStartDataSet(final int startDataSet) {
        this.startDataSet = startDataSet;
    }

    /**
     * Sets the toShow.
     *
     * @param toShow toShow.
     */
    public void setToShow(final List<GanttValueTypeEnum> toShow) {
        this.toShow = toShow;
    }

    /**
     * Sets the upperBound.
     *
     * @param upperBound upperBound.
     */
    public void setUpperBound(final int upperBound) {
        this.upperBound = upperBound;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "GraphOptions [numberOfDataSetMax=" + numberOfDataSetMax + ", startDataSet=" + startDataSet + "]";
    }

}

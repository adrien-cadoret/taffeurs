/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOStateCellRender.java,v $
 * Created on 13 janv. 2014 by lm
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch;


import java.awt.Component;

import javax.swing.Icon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import fr.vif.jtech.ui.browser.swing.StdRenderer;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOBBean;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.FMarkingMOIconeFactory;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.FMarkingMOIconeFactory.FMarkingMOIcone;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingEnum;
import fr.vif.vif5_7.workshop.device.ui.composites.marking.MarkingIconFactory;


/**
 * TableCellRenderer used to print a MO status (processing, simulating or nothing) as a Icon.
 * 
 * @author lm
 */
public class FMarkingMOStateCellRender extends StdRenderer<FMarkingMOBBean> implements TableCellRenderer {
    private Icon processingIcon = MarkingIconFactory.getIcon(FMarkingMOIcone.class, FMarkingMOIconeFactory.PATH_32X32,
            FMarkingMOIcone.PROCESSING_MO.getName());
    private Icon simulatingIcon = MarkingIconFactory.getIcon(FMarkingMOIcone.class, FMarkingMOIconeFactory.PATH_32X32,
            FMarkingMOIcone.SIMULATING_MO.getName());

    /**
     * Constructor.
     * 
     */
     public FMarkingMOStateCellRender() {
         super();
     }

     /**
      * {@inheritDoc}
      */
     @Override
     public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
             final boolean hasFocus, final int row, final int column) {

         // JLabel renderButton = (JLabel) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row,
         // column);
         JLabel renderLbl = new JLabel();
         renderLbl.setOpaque(true);

         if (MarkingEnum.LineState.PROCESSING.equals(value)) {
             renderLbl.setIcon(processingIcon);
         } else if (isSelected) {
             renderLbl.setIcon(simulatingIcon);
         } else {
             renderLbl.setIcon(null);
         }
         renderLbl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

         if (isSelected) {
             renderLbl.setBackground(TouchHelper.getAwtColor(MOUIConstants.TOUCHSCREEN_BG_SELECTED_BROWSE_LINE));
         } else if (row % 2 == 0) {
             renderLbl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
         } else {
             renderLbl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
         }

         // No need to have 20 unused pixels for each cell
         renderLbl.setBorder(new EmptyBorder(0, 2, 0, 2));

         return renderLbl;
     }
}

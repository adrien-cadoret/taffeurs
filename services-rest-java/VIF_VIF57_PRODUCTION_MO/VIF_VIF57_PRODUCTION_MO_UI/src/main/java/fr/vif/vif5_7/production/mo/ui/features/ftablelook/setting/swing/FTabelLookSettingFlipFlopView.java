/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTabelLookSettingFlipFlopView.java,v $
 * Created on 7 févr. 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting.swing;


import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.input.AbstractInputFlipFlopController;
import fr.vif.jtech.ui.input.InputFlipFlopModel;
import fr.vif.jtech.ui.input.swing.SwingInputFlipFlop;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FSettingBean;
import fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting.FTabelLookSettingFlipFlopIView;


/**
 * FlipFlop View.
 * 
 * @author kl
 */
public class FTabelLookSettingFlipFlopView extends SwingInputFlipFlop<FSettingBean> implements
FTabelLookSettingFlipFlopIView {

    /**
     * Superclass constructor.
     * 
     * @param ctrlClass controller.
     * @param model model.
     * @throws UIException exception.
     */
    public FTabelLookSettingFlipFlopView(final Class<? extends AbstractInputFlipFlopController> ctrlClass,
            final InputFlipFlopModel model) throws UIException {
        super(ctrlClass, model);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableButtons(final boolean enabled) {
        super.enableButtons(enabled);

        // if there not only mandatory zone, but the first selected bean is mandatory, disable the remove button.
        if (getImplBtnRemove().isButtonEnabled()) {
            getImplBtnRemove().enableButton(!getCurrentBean().isMandatory());
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FSettingBean getCurrentBean() {
        return getImplBrwResu().getModel().getCurrentBean();
    }

}

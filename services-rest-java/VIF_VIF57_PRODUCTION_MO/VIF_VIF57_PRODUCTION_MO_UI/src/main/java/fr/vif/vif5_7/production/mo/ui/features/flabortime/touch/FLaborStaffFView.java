/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffFView.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.touch;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.FLaborStaffFIView;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.StaffChart;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.TimeChart;


/**
 * labor staff feature view.
 * 
 * @author nle
 */
public class FLaborStaffFView extends StandardTouchFeature implements FLaborStaffFIView {

    private FLaborStaffBView bView;
    private FLaborStaffVView vView;
    private TitlePanel       subTitlePanel;

    /**
     * Default constructor.
     */
    public FLaborStaffFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserView getBrowserView() {
        return getBView();
    }

    /**
     * Gets the BView.
     * 
     * @category getter
     * @return the BView.
     */
    public FLaborStaffBView getBView() {
        if (bView == null) {
            bView = new FLaborStaffBView();
            bView.setBounds(0, 400, 865, 250);
            bView.setVisible(true);
        }
        return bView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public StaffChart getStaffChart() {
        return (getVView()).getStaffChart();
    }

    /**
     * Gets the subTitlePanel.
     * 
     * @return the subTitlePanel.
     */
    public TitlePanel getSubTitlePanel() {
        return subTitlePanel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public TimeChart getTimeChart() {
        return (getVView()).getTimeChart();
    }

    /**
     * Gets the VView.
     * 
     * @category getter
     * @return the VView.
     */
    public FLaborStaffVView getVView() {
        if (vView == null) {
            vView = new FLaborStaffVView();
            vView.setBounds(0, 50, 865, 335);
        }
        return vView;
    }

    /**
     * {@inheritDoc}
     */
    public void setSubTitle(final String subTitle) {
        subTitlePanel.setTitle(subTitle);
    }

    /**
     * Sets the subTitlePanel.
     * 
     * @param subTitlePanel subTitlePanel.
     */
    public void setSubTitlePanel(final TitlePanel subTitlePanel) {
        this.subTitlePanel = subTitlePanel;
    }

    /**
     * Get the subtitle panel.
     * 
     * @return the subtitle panel
     */
    private JPanel getSubTitle() {
        if (subTitlePanel == null) {
            subTitlePanel = new TitlePanel();
            subTitlePanel.setBounds(0, 0, 870, 40);
            subTitlePanel.setBorder(new MatteBorder(1, 0, 0, 2, Color.white));
            subTitlePanel.setPreferredSize(new Dimension(800, 40));
            subTitlePanel.setMinimumSize(new Dimension(500, 40));
        }

        return subTitlePanel;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setPreferredSize(new Dimension(870, 768));
        setLayout(null);
        add(getBView());
        add(getVView());
        add(getSubTitle());
    }

}

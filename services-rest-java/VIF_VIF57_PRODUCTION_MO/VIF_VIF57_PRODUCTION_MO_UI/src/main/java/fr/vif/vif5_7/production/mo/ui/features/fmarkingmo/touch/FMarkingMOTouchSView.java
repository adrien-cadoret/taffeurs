/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOTouchSView.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch;


import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.ImageIcon;
import javax.swing.JPanel;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.button.touch.TouchBtnStd;
import fr.vif.jtech.ui.button.touch.TouchToggleBtnStd;
import fr.vif.jtech.ui.dialogs.selection.touch.StandardTouchSelection;
import fr.vif.jtech.ui.input.InputFieldActionEvent;
import fr.vif.jtech.ui.input.InputFieldActionListener;
import fr.vif.jtech.ui.input.touch.TouchCompositeInputTextField;
import fr.vif.jtech.ui.input.touch.TouchInputCheckBox;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.activities.activities.ui.composites.cteam.touch.CTeamTouchView;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOSBean;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.FMarkingMOUIConstants;


/**
 * MarkingMO : Selection View.
 * 
 * @author lmo
 */
public class FMarkingMOTouchSView extends StandardTouchSelection {

    private JPanel                      panel;
    private JPanel                      jpanelDate;
    private TouchInputTextField         itfMODate;
    private TouchBtnStd                 nextDateBtn;
    private TouchBtnStd                 previousDateBtn;
    private CTeamTouchView              teamId;
    private CMarkingMOResourceTouchView cProductionResource;
    private TouchInputCheckBox          touchInputCheckBox;
    private TouchToggleBtnStd           btnTglDisplayAll;
    private ImageIcon                   hideFinishedIcon;
    private ImageIcon                   showFinishedIcon;

    /**
     * Constructs a new FMarkingMOSView.
     * 
     * @param view owner's view.
     */
    public FMarkingMOTouchSView(final Dialog view) {
        super(view);
        initialize();
        setTitle(I18nClientManager.translate(Jtech.T8241));
    }

    /**
     * Constructs a new FMarkingMOSView.
     * 
     * @param view owner's view.
     */
    public FMarkingMOTouchSView(final Frame view) {
        super();
        initialize();
        setTitle(I18nClientManager.translate(Jtech.T8241));
    }

    /**
     * Gets the hideFinishedIcon.
     * 
     * @return the hideFinishedIcon.
     */
    public ImageIcon getHideFinishedIcon() {
        if (hideFinishedIcon == null) {
            hideFinishedIcon = new ImageIcon(getClass().getResource(
                    "/images/vif/vif57/production/mo/hidefinished32x23.png"));
        }

        return hideFinishedIcon;
    }

    /**
     * Gets the showFinishedIcon.
     * 
     * @return the showFinishedIcon.
     */
    public ImageIcon getShowFinishedIcon() {
        if (showFinishedIcon == null) {
            showFinishedIcon = new ImageIcon(getClass().getResource(
                    "/images/vif/vif57/production/mo/showfinished32x23.png"));
        }
        return showFinishedIcon;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render() {

        boolean vSelected = ((FMarkingMOSBean) getSelectionModel().getSelection()).getShowFinishedMO();
        if (vSelected) {
            getBtnTglDisplayAll().setIcon(getHideFinishedIcon());
            getBtnTglDisplayAll().getBtnModel().setText(
                    TouchHelper.getBtnText(I18nClientManager.translate(ProductionMo.T29525, false),
                            TouchHelper.CENTER_ALIGN));
        } else {
            getBtnTglDisplayAll().setIcon(getShowFinishedIcon());
            getBtnTglDisplayAll().getBtnModel().setText(
                    TouchHelper.getBtnText(I18nClientManager.translate(ProductionMo.T29545, false),
                            TouchHelper.CENTER_ALIGN));
        }
        super.render();
    }

    /**
     * Add date.
     */
    private void addDate() {
        Date currentDate = (Date) getItfMODate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        getItfMODate().setValue(cal.getTime());
    }

    /**
     * Gets the btnTglDisplayAll.
     * 
     * @return the btnTglDisplayAll.
     */
    private TouchToggleBtnStd getBtnTglDisplayAll() {
        if (btnTglDisplayAll == null) {
            btnTglDisplayAll = new TouchToggleBtnStd();

            btnTglDisplayAll.setReference(FMarkingMOUIConstants.BTN_SHOW_ALL);
            btnTglDisplayAll.setBounds(757, 15, 105, 70);
            Dimension d = new Dimension(130, 70);
            btnTglDisplayAll.setPreferredSize(d);
            btnTglDisplayAll.setMinimumSize(d);
            btnTglDisplayAll.setMaximumSize(d);

            getBtnTglDisplayAll().setSelected(false);
            getBtnTglDisplayAll().setIcon(getShowFinishedIcon());
            getBtnTglDisplayAll().getBtnModel().setText(
                    TouchHelper.getBtnText(I18nClientManager.translate(ProductionMo.T29545, false),
                            TouchHelper.CENTER_ALIGN));
        }
        return btnTglDisplayAll;
    }

    /**
     * Gets the cProductionResource.
     * 
     * @return the cProductionResource.
     */
    private CMarkingMOResourceTouchView getcProductionResource() {
        if (cProductionResource == null) {
            cProductionResource = new CMarkingMOResourceTouchView();
            cProductionResource.setResettable(true);
            cProductionResource.setBeanProperty("lineCL");
        }
        return cProductionResource;
    }

    /**
     * Gets the itfMODate.
     * 
     * @category getter
     * @return the itfMODate.
     */
    private final TouchInputTextField getItfMODate() {
        if (itfMODate == null) {
            itfMODate = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_DATE,
                    I18nClientManager.translate(ProductionMo.T34329, false));
            itfMODate.setPreferredSize(new Dimension(240, 90));
            itfMODate.setBeanProperty("moDate");
            itfMODate.setDescription(I18nClientManager.translate(ProductionMo.T29584, false));
        }
        return itfMODate;
    }

    /**
     * Gets the jpanelDate.
     * 
     * @return the jpanelDate.
     */
    private JPanel getJpanelDate() {
        if (jpanelDate == null) {
            jpanelDate = new JPanel();

            GridBagConstraints vGBC;
            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.NONE;
            vGBC.weightx = 0;
            vGBC.weighty = 1;
            jpanelDate.add(getPreviousDateBtn(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 1;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 1;
            vGBC.weighty = 1;
            jpanelDate.add(getItfMODate(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 2;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.NONE;
            vGBC.weightx = 0;
            vGBC.weighty = 1;
            jpanelDate.add(getNextDateBtn(), vGBC);
        }
        return jpanelDate;
    }

    /**
     * Gets the nextDateBtn.
     * 
     * @category getter
     * @return the nextDateBtn.
     */
    private final TouchBtnStd getNextDateBtn() {
        if (nextDateBtn == null) {
            nextDateBtn = new TouchBtnStd();
            nextDateBtn.setPreferredSize(new Dimension(70, 70));
            nextDateBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayAfter.png")));
            nextDateBtn.setEnabled(true);
            nextDateBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    addDate();
                    valueChanged();
                }

            });
        }
        return nextDateBtn;

    }

    /**
     * Gets the main panel.
     * 
     * @return the main panel.
     */
    private JPanel getPanel() {
        if (panel == null) {
            panel = new JPanel(new GridBagLayout());

            GridBagConstraints vGBC = new GridBagConstraints();

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.VERTICAL;
            panel.add(getJpanelDate(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 1;
            vGBC.fill = GridBagConstraints.VERTICAL;
            panel.add(getcProductionResource(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 2;
            vGBC.fill = GridBagConstraints.VERTICAL;
            panel.add(getTeamId(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 3;
            vGBC.fill = GridBagConstraints.NONE;
            panel.add(getBtnTglDisplayAll(), vGBC);
        }

        return panel;
    }

    /**
     * Gets the previousDateBtn.
     * 
     * @category getter
     * @return the previousDateBtn.
     */
    private final TouchBtnStd getPreviousDateBtn() {
        if (previousDateBtn == null) {
            previousDateBtn = new TouchBtnStd();
            previousDateBtn.setPreferredSize(new Dimension(70, 70));
            previousDateBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayBefore.png")));
            previousDateBtn.setEnabled(true);
            previousDateBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    subDate();
                    valueChanged();
                }

            });
        }
        return previousDateBtn;

    }

    /**
     * Gets the teamId.
     * 
     * @category getter
     * @return the teamId.
     */
    private TouchCompositeInputTextField getTeamId() {
        if (teamId == null) {
            teamId = new CTeamTouchView();
            teamId.setBeanProperty("teamCL");
            teamId.setResettable(true);
        }
        return teamId;
    }

    /**
     * r.
     * 
     * @return t
     */
    private TouchInputCheckBox getTouchInputCheckBox() {
        if (touchInputCheckBox == null) {
            touchInputCheckBox = new TouchInputCheckBox();
            touchInputCheckBox.setBeanProperty("showFinishedMO");
            touchInputCheckBox.setLabel(I18nClientManager.translate(Generic.T24773, false));

            Dimension d = new Dimension(0, 0);
            touchInputCheckBox.setPreferredSize(d);
            touchInputCheckBox.setMinimumSize(d);
            touchInputCheckBox.setMaximumSize(d);
        }
        return touchInputCheckBox;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        setMainPanel(getPanel());
        setSize(435, 550);
    }

    /**
     * Sub date.
     */
    private void subDate() {
        Date currentDate = (Date) getItfMODate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        getItfMODate().setValue(cal.getTime());
    }

    /**
     * Inform the listener than the value has changed.
     */
    private void valueChanged() {
        InputFieldActionEvent event = new InputFieldActionEvent(getItfMODate(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getItfMODate());
        for (InputFieldActionListener listener : getItfMODate().getInputFieldActionListeners()) {
            listener.valueChanged(event);
        }
    }
}

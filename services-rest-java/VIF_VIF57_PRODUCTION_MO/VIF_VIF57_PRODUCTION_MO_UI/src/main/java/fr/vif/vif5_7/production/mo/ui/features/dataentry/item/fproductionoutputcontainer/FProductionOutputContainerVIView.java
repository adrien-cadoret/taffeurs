/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputContainerVIView.java,v $
 * Created on 19 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer;


import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.input.InputTextFieldView;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationBBean;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationSBean;
import fr.vif.vif5_7.production.mo.ui.composites.cdowngradeditem.CDowngradedItemIView;


/**
 * ProductionInputContainer : Viewer View Interface.
 * 
 * @author vr
 */
public interface FProductionOutputContainerVIView {

    /**
     * Adds the listeners on the container composite.
     *
     * @param dialogChangeListener the dialog change listener
     * @param displayListener the display listener
     */
    public void addContainerListener(final DialogChangeListener dialogChangeListener,
            final DisplayListener displayListener);

    /**
     * Gets the downgraded composite.
     * 
     * @return The downgraded composite.
     */
    public CDowngradedItemIView getDowngradedItemView();

    /**
     * Get itfBatch.
     * 
     * @return itfBatch
     */
    public InputTextFieldView getItfBatch();

    /**
     * Get itfContainer.
     * 
     * @return itfContainer
     */
    public InputTextFieldView getItfContainer();

    /**
     * Get itfUpperContainer.
     * 
     * @return itfUpperContainer
     */
    public InputTextFieldView getItfUpperContainer();

    /**
     * Open a location help.
     * 
     * @param parent parent
     * @param sBean selection bean
     * @return a FLocationBBean
     */
    public FLocationBBean openLocationHelp(final DisplayListener parent, final FLocationSBean sBean);

}

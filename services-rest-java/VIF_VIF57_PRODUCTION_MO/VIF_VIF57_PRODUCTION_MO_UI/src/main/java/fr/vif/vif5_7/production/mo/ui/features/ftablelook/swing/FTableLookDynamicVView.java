/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookDynamicVView.java,v $
 * Created on 4 févr. 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.swing;


import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.viewer.swing.DynamicSwingViewer;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;
import fr.vif.vif5_7.production.mo.ui.features.ftablelook.FTableLookDynamicIView;


/**
 * TableLook Viewer View.
 * 
 * @author kl
 */
public class FTableLookDynamicVView extends DynamicSwingViewer<FTableLookVBean> implements FTableLookDynamicIView {

    private SwingInputTextField codeTableLook;
    private SwingInputTextField labelTableLook;

    /**
     * Default Constructor.
     */
    public FTableLookDynamicVView() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param presentationType presentationType
     */
    public FTableLookDynamicVView(final String presentationType) {
        super(presentationType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableTableLook(final boolean insertMode) {
        getCodeTableLook().setEnabled(insertMode);
        if (insertMode) {
            getLabelTableLook().setEnabled(insertMode);
        } else {
            getLabelTableLook().setEnabled(!insertMode);
        }

    }

    /**
     * Gets the codeTableLook.
     * 
     * @return the codeTableLook.
     */
    public SwingInputTextField getCodeTableLook() {
        return codeTableLook;
    }

    /**
     * Gets the labelTableLook.
     * 
     * @return the labelTableLook.
     */
    public SwingInputTextField getLabelTableLook() {
        return labelTableLook;
    }

    /**
     * Sets the codeTableLook.
     * 
     * @param codeTableLook codeTableLook.
     */
    public void setCodeTableLook(final SwingInputTextField codeTableLook) {
        this.codeTableLook = codeTableLook;
    }

    /**
     * Sets the labelTableLook.
     * 
     * @param labelTableLook labelTableLook.
     */
    public void setLabelTableLook(final SwingInputTextField labelTableLook) {
        this.labelTableLook = labelTableLook;
    }

}

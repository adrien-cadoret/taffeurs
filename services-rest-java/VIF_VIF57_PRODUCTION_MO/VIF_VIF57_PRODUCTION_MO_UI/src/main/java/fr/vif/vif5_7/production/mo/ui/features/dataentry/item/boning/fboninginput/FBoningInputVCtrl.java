/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningInputVCtrl.java,v $
 * Created on 23 avr. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput;


import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.util.validator.VIFBindException;
import fr.vif.jtech.common.util.i18n.ITransParam;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.feature.SelfFeatureClosingRequiredEvent;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.input.InputTextFieldView;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.constant.EntityEnum.EntityUnitType;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityUnitInfos;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.Quantities;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.gen.location.business.beans.common.location.LocationKey;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputBarcodeType;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.ItemMovementKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.dialog.printdocument.DPrintDocumentBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboninginput.FBoningInputWorkBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerWorkBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fsubstituteitem.FSubstituteItemSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.BatchHelpType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.composites.csubstituteitem.CSubstituteItemIView;
import fr.vif.vif5_7.production.mo.ui.dialog.printdocument.DPrintDocumentCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionHelper;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningcommon.AbstractFBoningVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.touch.FBoningInputVView;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityEnum;
import fr.vif.vif5_7.stock.entity.business.beans.dialog.content.DContainerContentBean;
import fr.vif.vif5_7.stock.entity.business.beans.dialog.content.DContentItems;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingBBean;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingSBean;


/**
 * Viewer controller for boning input.
 * 
 * @author cj
 */
public class FBoningInputVCtrl extends AbstractFBoningVCtrl<FProductionInputContainerVBean> implements
        DialogChangeListener {

    public static final String                MOVEMENT_LIST           = "movementlist";

    /** LOGGER. */
    private static final Logger               LOGGER                  = Logger.getLogger(FBoningInputVCtrl.class);

    // CHECKSTYLE:OFF
    private boolean                           batchChecked            = false;

    private FBoningInputBCtrl                 browserCtrl             = null;

    private boolean                           criteriaChecked         = false;

    private FBoningInputFCtrl                 featureCtrl             = null;

    private FeatureView                       featureView;

    private boolean                           flashQuantites          = true;

    private FProductionInputContainerCBS      fproductionInputContainerCBS;

    private boolean                           outputAvailable         = false;

    private MovementAct                       previous                = null;

    private ItemMovementKey                   previousItemMovementKey = null;

    private boolean                           scaleWeightConnected    = false;

    private boolean                           showAll                 = false;

    private boolean                           stockChecked            = false;

    private FProductionInputContainerWorkBean workBean                = new FProductionInputContainerWorkBean();

    public void afterValidation() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - afterValidation()");
        }

        printDocuments(getBean().getDocuments());
        getBean().setForceUpdate(false);
        getBean().setAutoInsert(false);
        getBean().setCriteriaWindowOpened(false);
        if (!getWorkBean().getShowFinished() && getBean().getToFinish()) {
            getModel().setInitialBean(getBean()); // To prevent JTech dialog about modified bean
            browserCtrl.reopenQuery();
        } else {
            showUIErrorsException();
            fireButtonStatechanged();
            getFeatureCtrl().manageFinishResumeButton(getBean().getBBean());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - afterValidation()");
        }
    }

    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogCancelled(event=" + event + ")");
        }

        if (event.getDialogBean() instanceof CriteriaDBean) {

        } else if (event.getDialogBean() instanceof DContentItems) {

        }
        emptyVBean();
        getModel().setInitialBean(getBean());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogCancelled(event=" + event + ")");
        }
    }

    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }

        if (event.getDialogBean() instanceof CriteriaDBean) {
            CriteriaDBean criteriaDBean = (CriteriaDBean) event.getDialogBean();
            getModel().getBean().setBeanCriteria(criteriaDBean.getCriteriaReturnInitBean());
            getModel().getBean().getBeanCriteria().setAutoOpening(false);
            getModel().getBean().setCriteriaWindowOpened(true);
        } else if (event.getDialogBean() instanceof DContainerContentBean) {
            try {
                if (!getBean().getOneItemContainer()) {
                    OperationItemKey oik = getFproductionInputContainerCBS().getOperationItemKeyToReposit(
                            getIdCtx(),
                            ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem()
                                    .getItemId(), getWorkBean().getMoKey(), getWorkBean().getLogicalWorkstationId());
                    if (!getBean().getBBean().getOperationItemBean().getOperationItemKey().equals(oik)) {
                        CEntityBean entity = getBean().getEntity();
                        getModel().setInitialBean(getBean());
                        browserCtrl.repositionToOperationItemKey(oik);
                        getModel().getBean().setEntity(entity);
                    }
                }
                getModel()
                        .getBean()
                        .getEntity()
                        .getStockItemBean()
                        .setStockItem(
                                ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem());
                getModel().getBean().getEntity().getStockItemBean().getGeneralItemInfos()
                        .setCCompany(getIdCtx().getCompany());
                getModel()
                        .getBean()
                        .getEntity()
                        .getStockItemBean()
                        .getGeneralItemInfos()
                        .getItemCL()
                        .setCode(
                                ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem()
                                        .getItemId());
                if (InputBarcodeType.KEEP_QTIES.equals(getWorkBean().getBarcodeType())) {
                    getModel()
                            .getBean()
                            .getEntity()
                            .getStockItemBean()
                            .getKeyboardQties()
                            .setFirstQty(
                                    ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem()
                                            .getQuantities().getFirstQty());
                    getModel()
                            .getBean()
                            .getEntity()
                            .getStockItemBean()
                            .getKeyboardQties()
                            .setSecondQty(
                                    ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem()
                                            .getQuantities().getSecondQty());
                }
                getView().render();

                setBeanAndInitialBean(getFproductionInputContainerCBS().initializeEntity(getIdCtx(), getBean(),
                        getWorkBean()));
                validateBean(true, FProductionInputContainerVBeanEnum.BATCH_ID.getValue(), getBean().getEntity()
                        .getStockItemBean().getStockItem().getBatch());
            } catch (UIException e) {
                getView().show(e);
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            } catch (BusinessErrorsException e) {
                fireErrorsChanged(new UIErrorsException(e));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    @Override
    public void fireButtonStatechanged() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fireButtonStatechanged()");
        }

        // To change the visibility
        super.fireButtonStatechanged();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fireButtonStatechanged()");
        }
    }

    @Override
    public void fireErrorsChanged() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fireErrorsChanged()");
        }

        // To change the visibility
        super.fireErrorsChanged();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fireErrorsChanged()");
        }
    }

    @Override
    public void fireErrorsChanged(final UIErrorsException exception) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fireErrorsChanged(exception=" + exception + ")");
        }

        super.fireErrorsChanged(exception);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fireErrorsChanged(exception=" + exception + ")");
        }
    }

    /**
     * Gets the featureCtrl.
     * 
     * @category getter
     * @return the featureCtrl.
     */
    public FBoningInputFCtrl getFeatureCtrl() {
        return featureCtrl;
    }

    /**
     * Gets the featureView.
     * 
     * @category getter
     * @return the featureView.
     */
    public FeatureView getFeatureView() {
        return featureView;
    }

    /**
     * Gets the fproductionInputContainerCBS.
     * 
     * @category getter
     * @return the fproductionInputContainerCBS.
     */
    public FProductionInputContainerCBS getFproductionInputContainerCBS() {
        return fproductionInputContainerCBS;
    }

    /**
     * Gets the previous.
     * 
     * @category getter
     * @return the previous.
     */
    public MovementAct getPrevious() {
        return previous;
    }

    /**
     * Get the viewer view.
     * 
     * @return the Viewer View
     */
    public FBoningInputVView getViewerView() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getViewerView()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getViewerView()");
        }
        return (FBoningInputVView) getView();
    }

    /**
     * Gets the workBean.
     * 
     * @category getter
     * @return the workBean.
     */
    public FProductionInputContainerWorkBean getWorkBean() {
        return workBean;
    }

    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        super.initialize();

        // Set the batch help type
        try {
            BatchHelpType type = getFproductionInputContainerCBS().getBatchHelpType(getIdCtx());
            if (type != null) {
                if (type.equals(BatchHelpType.BATCH_HELP)) {
                    getViewerView().getItfBatch().setUseFieldHelp(true);
                } else {
                    getViewerView().getItfBatch().setUseFieldHelp(false);
                }
            }
        } catch (BusinessException e) {
            getView().show(new UIException(e));
        } catch (BusinessErrorsException e) {
            fireErrorsChanged(new UIErrorsException(e));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeViewWithBean(final FProductionInputContainerVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeViewWithBean(bean=" + bean + ")");
        }

        super.initializeViewWithBean(bean);
        setBeanAndInitialBean(bean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeViewWithBean(bean=" + bean + ")");
        }
    }

    /**
     * Is data entry ok to validate ?
     * 
     * @return Is data entry ok to validate ?
     */
    public boolean isDataEntryOK() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isDataEntryOK()");
        }

        boolean ret = true;
        if (!isValidVBean()) {
            ret = false;
        } else if (!"".equals(getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getUnit())
        // && getBean().getEntity().getStockItemBean().getStockItem().getQuantities().getFirstQty().getQty() == 0
                && getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getQty() == 0) {
            ret = false;
        } else if (!"".equals(getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().getUnit())
        // && getBean().getEntity().getStockItemBean().getStockItem().getQuantities().getSecondQty().getQty() == 0
                && getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().getQty() == 0) {
            ret = false;
        } else if (getBean().getEntity().getStockItemBean().getGeneralItemInfos().getStockByBatch()
                && getBean().getEntity().getStockItemBean().getStockItem().getBatch().isEmpty()) {
            ret = false;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isDataEntryOK()=" + ret);
        }
        return ret;
    }

    /**
     * Chech the criteria.
     * 
     */
    public void manageCriteriaWindow() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageCriteriaWindow()");
        }

        if (getBean().getBeanCriteria() != null) {
            if (!getBean().getCriteriaWindowOpened() && getBean().getBeanCriteria().getAutoOpening() != null
                    && getBean().getBeanCriteria().getAutoOpening()
                    && getBean().getBeanCriteria().getListCriteria().size() > 0) {
                CriteriaDBean criteriaDBean = new CriteriaDBean(getBean().getBeanCriteria(), getBean().getEntity()
                        .getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
                WorkShopCriteriaTools.showCriteria(this.featureView, this, this, getModel().getIdentification(),
                        criteriaDBean);
            } else {
                getModel().getBean().setCriteriaWindowOpened(true);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageCriteriaWindow()");
        }
    }

    public void manageFinishQuestion() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageFinishQuestion()");
        }

        if (getBean().getFinishQuestionNeeded()) {
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29549, false), MessageButtons.YES_NO);
            if (ret == MessageButtons.YES) {
                getBean().setToFinish(true);
            } else {
                getBean().setToFinish(false);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageFinishQuestion()");
        }
    }

    public boolean manageQuestion() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageQuestion()");
        }

        boolean isOk = true;
        for (String question : getBean().getQuestions()) {
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false), question,
                    MessageButtons.YES_NO);
            if (ret == MessageButtons.NO) {
                isOk = false;
                break;
            }
        }
        getBean().getQuestions().clear();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageQuestion()=" + isOk);
        }
        return isOk;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized boolean performSave() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - performSave()");
        }

        boolean b = false;
        b = super.performSave();
        if (b && !getBean().getAutoInsert()) {
            manageCriteriaWindow();
            if (getBean().getCriteriaWindowOpened()) {
                boolean ret = manageQuestion();
                if (ret) {
                    manageFinishQuestion();
                    getBean().setForceUpdate(true);
                    performSave();
                } else {
                    emptyVBean();
                }
            } else if (!getBean().getHasQuality()) { // if we don't have quality, we have to manage question
                boolean ret = manageQuestion();
                if (ret) {
                    manageFinishQuestion();
                    getBean().setForceUpdate(true);
                    performSave();
                } else {
                    emptyVBean();
                }
            } else {
                emptyVBean();
            }
        }

        if (getBean().getAutoInsert()) {
            afterValidation();
        } else {
            getModel().setInitialBean(getBean());
        }

        setProgressBarString();
        // close the feature if there is no item to show
        if (browserCtrl.getModel().getBeans().size() == 0) {
            try {
                getFeatureCtrl().closingRequired(
                        new ClosingEvent(this, SelfFeatureClosingRequiredEvent.EVENT_SELF_FEATURE_CLOSING_REQUIRED,
                                null));
            } catch (UIVetoException e) {
                LOGGER.error("", e);
                fireErrorsChanged(e);
            }
        }

        // DC6074 - Go back to MO list if save successed and workBean.getReturnToMOList() is true
        if (b && workBean.getReturnToMOList()) {
            fireSelfFeatureClosingRequested();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - performSave()=" + b);
        }
        return b;
    }

    public void printDocuments(final List<PrintDocument> documents) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - printDocuments(documents=" + documents + ")");
        }

        if (documents.size() > 0) {
            for (PrintDocument document : documents) {
                DPrintDocumentCtrl ctrl = new DPrintDocumentCtrl();
                DPrintDocumentBean dBean = new DPrintDocumentBean(document);
                ctrl.showDocument(getFeatureView(), this, getModel().getIdentification(), dBean);
            }
            try {
                getFproductionInputContainerCBS().printDocuments(getIdCtx(), getBean(), documents, getWorkBean());
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
            documents.clear();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - printDocuments(documents=" + documents + ")");
        }
    }

    /**
     * Sets the browserCtrl.
     * 
     * @category setter
     * @param browserCtrl browserCtrl.
     */
    public void setBrowserCtrl(final FBoningInputBCtrl browserCtrl) {
        this.browserCtrl = browserCtrl;
    }

    /**
     * Sets the featureCtrl.
     * 
     * @category setter
     * @param featureCtrl featureCtrl.
     */
    public void setFeatureCtrl(final FBoningInputFCtrl featureCtrl) {
        this.featureCtrl = featureCtrl;
    }

    /**
     * Sets the featureView.
     * 
     * @category setter
     * @param featureView featureView.
     */
    public void setFeatureView(final FeatureView featureView) {
        this.featureView = featureView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object setFieldHelpResult(final Object value, final InputTextFieldView fieldView) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setFieldHelpResult(value=" + value + ", fieldView=" + fieldView + ")");
        }

        if (value instanceof FDestockingBBean) {
            LocationKey locKey = ((FDestockingBBean) value).getLocationKey();
            getBean().getEntity().getEntityLocationBean().getEntityLocationKey().setCdepot(locKey.getCdepot());
            getBean().getEntity().getEntityLocationBean().getEntityLocationKey().setCemp(locKey.getCemp());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setFieldHelpResult(value=" + value + ", fieldView=" + fieldView + ")");
        }
        return super.setFieldHelpResult(value, fieldView);
    }

    /**
     * Sets the fproductionInputContainerCBS.
     * 
     * @category setter
     * @param fproductionInputContainerCBS fproductionInputContainerCBS.
     */
    public void setFproductionInputContainerCBS(final FProductionInputContainerCBS fproductionInputContainerCBS) {
        this.fproductionInputContainerCBS = fproductionInputContainerCBS;
    }

    /**
     * Sets the previous.
     * 
     * @category setter
     * @param previous previous.
     */
    public void setPrevious(final MovementAct previous) {
        this.previous = previous;
    }

    /**
     * Initialize the progress bar in the viewer
     */
    public void setProgressBarString() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setProgressBarString()");
        }

        int sumToDoQty = 0;
        int sumDoneQuantity = 0;
        String quantityUnit = "";
        int sumDoneQuantity2 = 0;
        String quantityUnit2 = "";
        FBoningInputWorkBean workbean = ((FBoningInputWorkBean) getWorkBean());
        for (OperationItemKey oik : workbean.getQtyItemList().keySet()) {
            sumToDoQty += workbean.getQtyItemList().get(oik)[0].getToDoQuantity();
            sumDoneQuantity = (int) workbean.getQtyItemList().get(oik)[0].getCompletedQuantity();
            quantityUnit = workbean.getQtyItemList().get(oik)[0].getUnit();

            sumDoneQuantity2 = (int) workbean.getQtyItemList().get(oik)[1].getCompletedQuantity();
            quantityUnit2 = workbean.getQtyItemList().get(oik)[1].getUnit();
        }

        getViewerView().getProgressBar().setMaximum(sumToDoQty);
        getViewerView().getProgressBar().setValue(sumDoneQuantity);
        getViewerView().getProgressBar().setString(sumDoneQuantity + " / " + sumToDoQty + " " + quantityUnit);

        getViewerView().getItfProgressionQty2().setValue(sumDoneQuantity2 + " " + quantityUnit2);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setProgressBarString()");
        }
    }

    /**
     * Set scale weight in quantities components.
     * 
     * @param result result from scale
     */
    @Override
    public void setWeightResult(final FCommScaleResult result) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setWeightResult(result=" + result + ")");
        }

        EntityUnitInfos unitInfos = getBean().getEntity().getStockItemBean().getUnitsInfos();
        Quantities k = getBean().getEntity().getStockItemBean().getKeyboardQties();
        if (EntityUnitType.WEIGHT.equals(unitInfos.getFirstUnitType())) {
            k.getFirstQty().setQty(result.getNetWeight());
        } else if (EntityUnitType.WEIGHT.equals(unitInfos.getSecondUnitType())) {
            k.getSecondQty().setQty(result.getNetWeight());
        } else if (EntityUnitType.WEIGHT.equals(unitInfos.getThirdUnitType())) {
            k.getThirdQty().setQty(result.getNetWeight());
        }

        getView().render();
        if (isDataEntryOK()) {
            performSave();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setWeightResult(result=" + result + ")");
        }
    }

    /**
     * Sets the workBean.
     * 
     * @category setter
     * @param workBean workBean.
     */
    public void setWorkBean(final FProductionInputContainerWorkBean workBean) {
        this.workBean = workBean;
    }

    /**
     * Empty error panel.
     * 
     */
    public void showUIErrorsException() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - showUIErrorsException()");
        }

        fireErrorsChanged();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - showUIErrorsException()");
        }
    }

    /**
     * Show ui errors exceptions in error panel.
     * 
     * @param exception exception to show
     */
    public void showUIErrorsException(final UIErrorsException exception) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - showUIErrorsException(exception=" + exception + ")");
        }

        fireErrorsChanged(exception);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - showUIErrorsException(exception=" + exception + ")");
        }
    }

    @Override
    public void validateBean(final boolean creationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - validateBean(creationMode=" + creationMode + ", pBeanProperty=" + pBeanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }

        if (pBeanProperty != null && !"barcode".equals(pBeanProperty)) {
            getBean().getEntity().getStockItemBean().getStockItem()
                    .setItemId(getBean().getEntity().getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
            fireErrorsChanged();
            try {
                CEntityEnum entityEnum = null;
                if (FProductionInputContainerVBeanEnum.ITEM_ID.getValue().equals(pBeanProperty)) {
                    entityEnum = CEntityEnum.ITEM_ID;
                    // KL 31/01/2013 M128375: Profil item or workstation cannot be reset to consider the units defined
                    // by default in it. Unless the item have a unit conversion constant.
                    // getBean().getEntity().getStockItemBean().setKeyboardQties(new Quantities());
                    // getBean().setDocuments(fmvtOutCBS.listDocuments(getIdCtx(), getBean(), getWorkBean()));
                } else if (FProductionInputContainerVBeanEnum.ITEM_CL.getValue().equals(pBeanProperty)) {
                    entityEnum = CEntityEnum.ITEM_ID;
                    getBean().getEntity().getStockItemBean().getGeneralItemInfos().getItemCL()
                            .setCode((String) propertyValue);
                    getBean()
                            .getEntity()
                            .getStockItemBean()
                            .getStockItem()
                            .setItemId(
                                    getBean().getEntity().getStockItemBean().getGeneralItemInfos().getItemCL()
                                            .getCode());
                } else if (FProductionInputContainerVBeanEnum.BATCH_ID.getValue().equals(pBeanProperty)) {
                    // BAtch
                    entityEnum = CEntityEnum.BATCH;
                    if (StringUtils.isEmpty(getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                            .getCdepot())) {
                        FDestockingSBean destockingSBean = (FDestockingSBean) getViewerView().getItfBatch()
                                .getInitialHelpSelection();
                        getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .setCdepot(destockingSBean.getDepotId());
                    }
                } else if (FProductionInputContainerVBeanEnum.CONTAINER_NUMBER.getValue().equals(pBeanProperty)) {
                    if (!((String) propertyValue).isEmpty()) {
                        getFeatureCtrl().interpretContainer((String) propertyValue, true);
                    }
                }
                if (entityEnum != null) {
                    getModel().setBean(
                            getFproductionInputContainerCBS().validateEntityFields(getIdCtx(), entityEnum, getBean(),
                                    getWorkBean()));
                    // if (entityInputBean != null) {
                    if (CEntityEnum.BATCH.equals(entityEnum)) {
                        manageCriteriaWindow();
                    } else if (CEntityEnum.ITEM_ID.equals(entityEnum)) {
                        // Change batch selection
                        getViewerView().getItfBatch().setInitialHelpSelection(
                                FProductionHelper.getFDestockingSBean(getBean().getBBean().getOperationItemBean(),
                                        getBean().getEntity().getStockItemBean().getGeneralItemInfos().getItemCL()
                                                .getCode(), (FDestockingSBean) getViewerView().getItfBatch()
                                                .getInitialHelpSelection()));
                    }
                    getModel().setInitialBean(getBean());
                    getView().render();
                    getView().enableComponents(true);
                    fireButtonStatechanged();
                }
                if (!FProductionInputContainerVBeanEnum.CONTAINER_NUMBER.getValue().equals(pBeanProperty)) {
                    if (isDataEntryOK()) {
                        // getBean().setDocuments(fmvtOutCBS.listDocuments(getIdCtx(), getBean(), getWorkBean()));
                        performSave();
                    }
                }
            } catch (BusinessException e) {
                throw new UIException(e);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - validateBean(creationMode=" + creationMode + ", pBeanProperty=" + pBeanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }
    }

    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

    @Override
    protected FProductionInputContainerVBean convert(final Object bean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }
        String oldProfileId = null;
        if (getBean() != null && getBean().getInputItemParameters() != null) {
            oldProfileId = getBean().getInputItemParameters().getProfileId();
        }

        FProductionInputContainerVBean vBean = new FProductionInputContainerVBean();

        FProductionBBean bbean = (FProductionBBean) bean;
        vBean.setBBean(bbean);
        try {
            vBean = getFproductionInputContainerCBS().convert(getIdCtx(), bbean, getWorkBean());
            // Change batch selection
            getViewerView().getItfBatch().setInitialHelpSelection(
                    FProductionHelper.getFDestockingSBean(vBean.getBBean().getOperationItemBean(), vBean.getEntity()
                            .getStockItemBean().getGeneralItemInfos().getItemCL().getCode(),
                            (FDestockingSBean) getViewerView().getItfBatch().getInitialHelpSelection()));

            OperationItemKey current = null;
            if (vBean != null && vBean.getBBean() != null && vBean.getBBean().getOperationItemBean() != null) {

                current = vBean.getBBean().getOperationItemBean().getOperationItemKey();
                if (current.getManagementType().equals(ManagementType.ARCHIVED)) {
                    getViewerView().getItfBatch().setAlwaysDisabled(true);
                    getViewerView().getQuantityUnit1View().getItfQuantity().setAlwaysDisabled(true);
                    getViewerView().getQuantityUnit2View().getItfQuantity().setAlwaysDisabled(true);
                } else if (current.getManagementType().equals(ManagementType.REAL)) {
                    getViewerView().getItfBatch().setAlwaysDisabled(false);
                    getViewerView().getQuantityUnit1View().getItfQuantity().setAlwaysDisabled(false);
                    getViewerView().getQuantityUnit2View().getItfQuantity().setAlwaysDisabled(false);
                }
            }

            // setProgressionQty2(vBean);
            vBean.setProgressionQty2(vBean.getBBean().getFormatedItemQtty2DoneSum());

            // Change item selection
            CSubstituteItemIView itemView = getViewerView().getSubstituteItemView();
            itemView.setSelection(FProductionHelper.getFSubstiteSBean((InputOperationItemBean) vBean.getBBean()
                    .getOperationItemBean(), vBean.getEntity().getStockItemBean().getGeneralItemInfos().getItemCL()
                    .getCode(), (FSubstituteItemSBean) itemView.getSelection()));

            if (oldProfileId == null || !oldProfileId.equals(vBean.getInputItemParameters().getProfileId())) {
                // if new profile => reset the button state
                if (vBean.getInputItemParameters().getInterpretQty()) {
                    getWorkBean().setBarcodeType(InputBarcodeType.KEEP_QTIES);
                } else {
                    getWorkBean().setBarcodeType(InputBarcodeType.IGNORE_QTIES);
                }

                getFeatureCtrl().manageBtnFlash();
            }
        } catch (BusinessException e) {
            LOGGER.error(e);
            getView().show(new UIException(e));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")=" + vBean);
        }
        return vBean;
    }

    @Override
    protected void deleteBean(final FProductionInputContainerVBean bean) throws UIException {

    }

    protected void emptyVBean() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - emptyVBean()");
        }

        getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().setQty(0.0);
        getBean().getQuestions().clear();
        getBean().setAutoInsert(false);
        getView().render();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - emptyVBean()");
        }
    }

    @Override
    protected void fireBeanUpdated(final FProductionInputContainerVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fireBeanUpdated(bean=" + bean + ")");
        }

        // To change the visibility
        super.fireBeanUpdated(bean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fireBeanUpdated(bean=" + bean + ")");
        }
    }

    @Override
    protected FProductionInputContainerVBean insertBean(final FProductionInputContainerVBean bean,
            final FProductionInputContainerVBean initialBean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertBean(bean=" + bean + ", initialBean=" + initialBean + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertBean(bean=" + bean + ", initialBean=" + initialBean + ")=" + null);
        }
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean ret = false;
        if (FBoningInputFCtrl.BTN_FEATURES_REFERENCE.equals(reference)) {
            ret = true;
        } else if (FBoningInputFCtrl.BTN_WEIGHT_REFERENCE.equals(reference)) {
            if (getWorkBean().getScale() != null //
                    && getBean() != null //
                    && (isValidVBean()) //
                    && (ManagementType.REAL.equals(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                            .getManagementType()) //
                    && ((EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                            .getFirstUnitType())) //
                    || (EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                            .getSecondUnitType()) && getBean().getEntity().getStockItemBean().getEnableFields()
                            .getSecondQty())))) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FBoningInputFCtrl.BTN_DETAIL_REFERENCE.equals(reference)) {
            if (isValidVBean()) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FBoningInputFCtrl.BTN_FINISH_REFERENCE.equals(reference)) {
            if (isValidVBean()) {
                if (ManagementType.ARCHIVED.equals(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                        .getManagementType())) {
                    ret = getWorkBean().getHasRightForUnfinishing();
                } else {
                    ret = true;
                }
            } else {
                ret = false;
            }
        } else if (FBoningInputFCtrl.BTN_SHOW_ALL_REFERENCE.equals(reference)) {
            ret = true;
        } else if (FBoningInputFCtrl.BTN_FLASH_REFERENCE.equals(reference)) {
            ret = true;
        } else if (FBoningInputFCtrl.BTN_CANCELLAST_REFERENCE.equals(reference)) {
            if (getPrevious() != null) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FBoningInputFCtrl.BTN_PDF_REFERENCE.equalsIgnoreCase(reference)) {
            ret = isValidVBean();
        } else if ("features".equalsIgnoreCase(reference)) {
            ret = FProductionTools.isValid(getBean());
        } else if (FBoningInputFCtrl.BTN_OPEN_INCIDENT_REFERENCE.equalsIgnoreCase(reference)) {
            ret = true;
        } else if (MOVEMENT_LIST.equalsIgnoreCase(reference)) {
            ret = true;
        } else if ("resetBarCode".equalsIgnoreCase(reference)) {
            ret = true;
        } else if (ButtonReference.DEBUG_REFERENCE.getReference().equals(reference)) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isPanelActionVisible(type=" + type + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isPanelActionVisible(type=" + type + ")=" + false);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setBeanAndInitialBean(final FProductionInputContainerVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setBeanAndInitialBean(bean=" + bean + ")");
        }
        // for boning input, we inititialize the first qty from the input profil
        if (bean.getEntity().getStockItemBean().getEnableFields().getSecondQty()
                && bean.getInputItemParameters().getForcedFirstQty() > 0) {
            bean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty()
                    .setQty(bean.getInputItemParameters().getForcedFirstQty());
        }
        super.setBeanAndInitialBean(bean);

        setProgressBarString();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setBeanAndInitialBean(bean=" + bean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FProductionInputContainerVBean updateBean(final FProductionInputContainerVBean pBean) throws UIException,
            UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(pBean=" + pBean + ")");
        }

        FProductionInputContainerVBean vBean = pBean;
        try {

            vBean = getFproductionInputContainerCBS().updateBean(getIdCtx(), vBean, getWorkBean());

            if (vBean.getQuestions().size() == 0) {

                // if there is questions, the qty had not be inserted.
                OperationItemKey oikey = new OperationItemKey(vBean.getBBean().getOperationItemBean()
                        .getOperationItemKey().getManagementType(), vBean.getBBean().getOperationItemBean()
                        .getOperationItemKey().getEstablishmentKey(), vBean.getBBean().getOperationItemBean()
                        .getOperationItemKey().getChrono(), vBean.getBBean().getOperationItemBean()
                        .getOperationItemKey().getCounter1(), vBean.getBBean().getOperationItemBean()
                        .getOperationItemKey().getCounter2(), 0, 0);
                if (vBean.getToFinish()) {
                    oikey.setManagementType(ManagementType.REAL);
                }
                OperationItemQuantityUnit[] qties = ((FBoningInputWorkBean) getWorkBean()).getQtyItemList().get(oikey);
                if (qties == null) {
                    qties = new OperationItemQuantityUnit[2];
                    qties[0] = new OperationItemQuantityUnit();
                    qties[1] = new OperationItemQuantityUnit();

                    ((FBoningInputWorkBean) getWorkBean()).getQtyItemList().put(oikey, qties);
                }
                if (vBean.getMovementList().size() > 0) {
                    qties[0].setCompletedQuantity(qties[0].getCompletedQuantity()
                            + vBean.getMovementList().get(0).getItemMovementBean().getItemMovementQuantityUnit()
                                    .getFirstQuantity());
                    qties[1].setCompletedQuantity(qties[1].getCompletedQuantity()
                            + vBean.getMovementList().get(0).getItemMovementBean().getItemMovementQuantityUnit()
                                    .getSecondQuantity());
                }
                // if vbean has just been archived, we change the management type
                if (vBean.getToFinish()) {
                    ((FBoningInputWorkBean) getWorkBean()).getQtyItemList().remove(oikey);
                    oikey.setManagementType(ManagementType.ARCHIVED);
                    ((FBoningInputWorkBean) getWorkBean()).getQtyItemList().put(oikey, qties);
                }
                setProgressBarString();
            }

            if (vBean.getAutoInsert()) {
                setPrevious(vBean.getPreviousMovementAct());
                getWorkBean().setOutputAvailable(vBean.getOutputAvailable());

            }
        } catch (BusinessException e) {
            emptyVBean();
            Errors erreurs = new VIFBindException(getBean());
            erreurs.rejectValue("barcode", "",
                    I18nClientManager.translate(e.getMainTrad(), true, e.getListeParams().toArray(new ITransParam[0])));
            throw new UIErrorsException(new BusinessErrorsException(erreurs));
        } catch (BusinessErrorsException e) {
            UIErrorsException ue = new UIErrorsException(e);
            emptyVBean();
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    private boolean isValidVBean() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isValidVBean()");
        }

        boolean ret = false;
        if (getBean() != null //
                && (getBean().getBBean() != null) //
                && (getBean().getBBean().getOperationItemBean() != null) //
                && (getBean().getBBean().getOperationItemBean().getOperationItemKey() != null) //
                && (getBean().getBBean().getOperationItemBean().getOperationItemKey().getChrono() != null) //
                && (getBean().getBBean().getOperationItemBean().getOperationItemKey().getChrono().getChrono() > 0)) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isValidVBean()=" + ret);
        }
        return ret;
    }
}

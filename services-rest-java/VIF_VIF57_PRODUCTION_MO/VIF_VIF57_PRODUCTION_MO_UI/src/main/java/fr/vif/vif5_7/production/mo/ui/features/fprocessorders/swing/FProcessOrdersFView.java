/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersFView.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders.swing;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.swing.StandardSwingFeature;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersVBean;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersFIView;


/**
 * ProcessOrders : Feature View.
 * 
 * @author jd
 */
public class FProcessOrdersFView extends StandardSwingFeature<FProcessOrdersBBean, FProcessOrdersVBean> implements
        FProcessOrdersFIView {

    private FProcessOrdersBView fProcessOrdersBView = null;

    private FProcessOrdersVView fProcessOrdersVView = null;

    /**
     * Default constructor.
     * 
     */
    public FProcessOrdersFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserView<FProcessOrdersBBean> getBrowserView() {
        return getFProcessOrdersBView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView<FProcessOrdersVBean> getViewerView() {
        return getFProcessOrdersVView();
    }

    /**
     * Initialisation of the browser feature.
     * 
     * @return FProcessOrdersBView
     */
    private FProcessOrdersBView getFProcessOrdersBView() {
        if (fProcessOrdersBView == null) {
            fProcessOrdersBView = new FProcessOrdersBView();
        }
        return fProcessOrdersBView;
    }

    /**
     * Initialisation of the viewer feature.
     * 
     * @return FProcessOrdersVView
     */
    private FProcessOrdersVView getFProcessOrdersVView() {
        if (fProcessOrdersVView == null) {
            fProcessOrdersVView = new FProcessOrdersVView();
        }
        return fProcessOrdersVView;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0 };
        gridBagLayout.columnWeights = new double[] { 1.0 };
        this.setLayout(gridBagLayout);
        this.setSize(new Dimension(150, 150));
        this.setPreferredSize(new Dimension(150, 150));

        GridBagConstraints gbcScrollPane = new GridBagConstraints();
        // gbcScrollPane.weighty = 0;
        // gbcScrollPane.weightx = 0;
        gbcScrollPane.fill = GridBagConstraints.BOTH;
        gbcScrollPane.gridx = 0;
        gbcScrollPane.gridy = 1;

        add(getFProcessOrdersVView(), gbcScrollPane);

        GridBagConstraints gbcBrowse = new GridBagConstraints();
        // gbcBrowse.weighty = 1.0;
        // gbcBrowse.weightx = 1.0;
        gbcBrowse.fill = GridBagConstraints.BOTH;
        gbcBrowse.gridx = 0;
        gbcBrowse.gridy = 0;

        add(getFProcessOrdersBView(), gbcBrowse);
    }

}

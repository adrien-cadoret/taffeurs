/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOLinesInternalFrame.java,v $
 * Created on 21 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.lines;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.models.mvc.MVCTriad;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersVBean;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersVModel;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.lines.swing.FSupervisorMOLinesVView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing.AbstractMODockableView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;


/**
 * Frame containing a FPiledupLine browse.
 * 
 * @author jd
 */
public class FSupervisorMOLinesInternalFrame extends AbstractMODockableView<FProcessOrdersVBean, FProcessOrdersVCtrl>
        implements DialogChangeListener {
    /** LOGGER. */
    private static final Logger     LOGGER = Logger.getLogger(FSupervisorMOLinesInternalFrame.class);

    private FSupervisorMOLinesVView iview;

    /**
     * constructor.
     * 
     * @param localFrame localFrame
     */
    @SuppressWarnings("unchecked")
    public FSupervisorMOLinesInternalFrame(final MOLocalFrame localFrame) {

        super(localFrame, new MVCTriad(FProcessOrdersVModel.class, FSupervisorMOLinesVView.class,
                FProcessOrdersVCtrl.class));
        iview = ((FSupervisorMOLinesVView) getViewerController().getView());

        iview.getBrowserController().setIdentification(localFrame.getFrameBean().getIdentification());

        iview.addGenericActionListener(this);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void convert() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refresh()");
        }

        super.refresh();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - refresh()");
        }
    }

    /**
     * Get the selection.
     * 
     * @param selection FProcessOrdersSBean
     */
    public void setSelection(final FProcessOrdersSBean selection) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setSelection(selection=" + selection + ")");
        }

        iview.getBrowserController().reopenQuery(selection);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setSelection(selection=" + selection + ")");
        }
    }

}

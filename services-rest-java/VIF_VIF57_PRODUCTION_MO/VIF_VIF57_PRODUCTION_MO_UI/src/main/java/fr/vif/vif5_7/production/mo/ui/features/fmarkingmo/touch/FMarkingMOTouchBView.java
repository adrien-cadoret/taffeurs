/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOTouchBView.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch;


import java.util.Collection;

import javax.swing.JTable;

import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.touch.TouchLineBrowser;
import fr.vif.jtech.ui.dialogs.selection.SelectionModel;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.models.mvc.SelectionMVCTriad;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOBBean;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.FMarkingMOSCtrl;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingEnum;


/**
 * MarkingMO : Browser View.
 * 
 * @author lmo
 */
public class FMarkingMOTouchBView extends TouchLineBrowser<FMarkingMOBBean> {

    private static final int          INDEX_COLUMN_MO_STATE_SIZE   = 0;
    private static final int          INDEX_COLUMN_HOUR_SIZE       = 1;
    private static final int          INDEX_COLUMN_MO_SIZE         = 2;
    private static final int          INDEX_COLUMN_ITEM_CODE_SIZE  = 3;
    private static final int          INDEX_COLUMN_ITEM_LABEL_SIZE = 4;
    private static final int          INDEX_COLUMN_QUANTITY_SIZE   = 5;

    private static final int          COLUMN_MO_STATE_SIZE         = 35;
    private static final int          COLUMN_HOUR_SIZE             = 65;
    private static final int          COLUMN_MO_SIZE               = 105;
    private static final int          COLUMN_ITEM_CODE_SIZE        = 180;
    private static final int          COLUMN_ITEM_LABEL_SIZE       = 215;
    private static final int          COLUMN_QUANTITY_SIZE         = 110;

    private FMarkingMOStateCellRender stateRenderer;

    private TitlePanel                titlePanel;

    /**
     * Default constructor.
     * 
     */
    public FMarkingMOTouchBView() {
        super();
        setSelectionMvcTriad(new SelectionMVCTriad(SelectionModel.class, FMarkingMOTouchSView.class,
                FMarkingMOSCtrl.class));
        JTable jTable = super.getJTable();
        stateRenderer = new FMarkingMOStateCellRender();
        jTable.setDefaultRenderer(MarkingEnum.LineState.class, stateRenderer);
    }

    /**
     * Gets the titlePanel.
     * 
     * @return the titlePanel.
     */
    public TitlePanel getTitlePanel() {
        return titlePanel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView(final AbstractBrowserController arg0) {
        super.initializeView(arg0);

        resizeColumns();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void invokeLaterRowChangedActions(final Collection<TableRowChangeListener> tableRowChangeListeners,
            final int selectedRow, final FMarkingMOBBean bean) {
        // don't invoke later, we need it now.
        // super.invokeLaterRowChangedActions(tableRowChangeListeners, selectedRow, bean);

        // We want to work in sequential mode ! Then we override the super to
        // do the selected row changed event in the same thread
        TableRowChangeEvent event = new TableRowChangeEvent(this, TableRowChangeEvent.EVENT_ROW_CHANGED, selectedRow,
                bean);
        for (TableRowChangeListener listener : tableRowChangeListeners) {
            listener.selectedRowChanged(event);
        }
    }

    /**
     * Sets the titlePanel.
     * 
     * @param titlePanel titlePanel.
     */
    public void setTitlePanel(final TitlePanel titlePanel) {
        this.titlePanel = titlePanel;
    }

    /**
     * Add fixe size for columns of MoList Feature.
     */
    private void resizeColumns() {

        getJTable().setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

        // STATE COLUMN
        if (getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_STATE_SIZE) != null) {
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_STATE_SIZE).setMinWidth(COLUMN_MO_STATE_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_STATE_SIZE).setPreferredWidth(COLUMN_MO_STATE_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_STATE_SIZE).setMaxWidth(COLUMN_MO_STATE_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_STATE_SIZE).setWidth(COLUMN_MO_STATE_SIZE);
        }

        // HOUR COLUMN
        if (getJTable().getColumnModel().getColumn(INDEX_COLUMN_HOUR_SIZE) != null) {
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_HOUR_SIZE).setMinWidth(COLUMN_HOUR_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_HOUR_SIZE).setPreferredWidth(COLUMN_HOUR_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_HOUR_SIZE).setMaxWidth(COLUMN_HOUR_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_HOUR_SIZE).setWidth(COLUMN_HOUR_SIZE);
        }

        // MO COLUMN
        if (getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_SIZE) != null) {
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_SIZE).setMinWidth(COLUMN_MO_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_SIZE).setPreferredWidth(COLUMN_MO_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_SIZE).setMaxWidth(COLUMN_MO_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_MO_SIZE).setWidth(COLUMN_MO_SIZE);
        }

        // ITEM CODE COLUMN
        if (getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE) != null) {
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE).setMinWidth(COLUMN_ITEM_CODE_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE)
            .setPreferredWidth(COLUMN_ITEM_CODE_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE).setMaxWidth(COLUMN_ITEM_CODE_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE).setWidth(COLUMN_ITEM_CODE_SIZE);
        }

        // ITEM LABEL COLUMN
        if (getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE) != null) {
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE).setMinWidth(COLUMN_ITEM_LABEL_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE)
                    .setPreferredWidth(COLUMN_ITEM_LABEL_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE).setMaxWidth(COLUMN_ITEM_LABEL_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_ITEM_CODE_SIZE).setWidth(COLUMN_ITEM_LABEL_SIZE);
        }

        // QUANTITY COLUMN
        if (getJTable().getColumnModel().getColumn(INDEX_COLUMN_QUANTITY_SIZE) != null) {
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_QUANTITY_SIZE).setMinWidth(COLUMN_QUANTITY_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_QUANTITY_SIZE).setPreferredWidth(COLUMN_QUANTITY_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_QUANTITY_SIZE).setMaxWidth(COLUMN_QUANTITY_SIZE);
            getJTable().getColumnModel().getColumn(INDEX_COLUMN_QUANTITY_SIZE).setWidth(COLUMN_QUANTITY_SIZE);
        }
    }
}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: MOSaveSelection.java,v $
 * Created on 23 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.common.beans.Selection;


/**
 * Contains information to create a new frame.
 * 
 * @author jd
 */
public class MOSaveSelection {

    /**
     * Selection.
     */
    private Selection        selection = null;
    /**
     * The list of views.
     */
    private List<MOSaveView> views     = new ArrayList<MOSaveView>();

    /**
     * constructor.
     */
    public MOSaveSelection() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        return true;
    }

    /**
     * Gets the selection.
     * 
     * @return the selection.
     */
    public Selection getSelection() {
        return selection;
    }

    /**
     * Gets the views.
     * 
     * @return the views.
     */
    public List<MOSaveView> getViews() {
        return views;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Sets the selection.
     * 
     * @param selection selection.
     */
    public void setSelection(final Selection selection) {
        this.selection = selection;
    }

    /**
     * Sets the views.
     * 
     * @param views views.
     */
    public void setViews(final List<MOSaveView> views) {
        this.views = views;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "MOSaveSelection [selection=" + selection + ", views=" + views + "]";
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: StaffChart.java,v $
 * Created on 13 mai 2014 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import java.awt.Color;
import java.awt.Paint;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.xy.XYLineAndShapeRenderer;
import org.jfree.data.time.RegularTimePeriod;
import org.jfree.data.time.Second;
import org.jfree.data.time.TimeSeries;
import org.jfree.data.time.TimeSeriesCollection;
import org.jfree.data.time.TimeSeriesDataItem;

import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.StaffByTime;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.StaffByTimeComparator;


/**
 * Diagram of staff by resource along the day.
 * 
 * @author nle
 */
public class StaffChart extends ChartPanel {
    private static final int            NB_COLORS = 5;
    private static final int            NB_MAX    = 20;

    private static TimeSeriesCollection dataset   = new TimeSeriesCollection();
    private static JFreeChart           chart     = ChartFactory.createXYLineChart(
            // chart title ("Evolution de l'effectif")
            I18nClientManager.translate(ProductionMo.T35124, false),
            // x axis label ("Temps")
            I18nClientManager.translate(ProductionMo.T34770, false),
            // y axis label ("Effectif")
            I18nClientManager.translate(ProductionMo.T34746, false),
            dataset, // data
            PlotOrientation.VERTICAL, // orientation
            true, // include legend
            true, // tooltips
            false // urls
            );

    //DP2192 - CHU - initialize first 5 graph colors
    private List<Color>                 colorList = new ArrayList<Color>();

    /**
     * Constructor.
     * 
     */
    public StaffChart() {

        super(chart);

        chart.getXYPlot().setDomainAxis(new DateAxis());

        // NOW DO SOME OPTIONAL CUSTOMISATION OF THE CHART...
        chart.setBackgroundPaint(toColor(StandardColor.TOUCHSCREEN_BG_START_FUNCTIONALITIES_DISABLED)); // background
        // color
        chart.setBorderPaint(toColor(StandardColor.TOUCHSCREEN_BG_START_FUNCTIONALITIES_DISABLED)); // background color
        chart.getLegend().setBackgroundPaint(toColor(StandardColor.TOUCHSCREEN_BG_START_FUNCTIONALITIES_DISABLED)); // background
        // color
        chart.getTitle().setPaint(toColor(StandardColor.TOUCHSCREEN_FG_BROWSER_CODE)); // orange

        // get a reference to the plot for further customisation...
        final XYPlot plot = chart.getXYPlot();
        plot.setBackgroundPaint(toColor(StandardColor.TOUCHSCREEN_BG_START_FUNCTIONALITIES_DISABLED)); // background
        // color
        plot.setDomainGridlinePaint(toColor(StandardColor.TOUCHSCREEN_BG_END_FUNCTIONALITIES_ENABLED)); // light gray
        plot.setRangeGridlinePaint(toColor(StandardColor.TOUCHSCREEN_BG_END_FUNCTIONALITIES_ENABLED)); // light gray

        //DP2192 - CHU - define first 5 graph colors
        colorList.add(Color.RED);
        colorList.add(Color.BLUE);
        colorList.add(Color.GREEN);
        colorList.add(Color.ORANGE);
        colorList.add(Color.BLACK);

        final XYLineAndShapeRenderer renderer = new XYLineAndShapeRenderer();
        for (int i = 0; i < NB_MAX; i++) {
            renderer.setSeriesLinesVisible(i, true);
            renderer.setSeriesShapesVisible(i, false);
            //DP2192 - CHU - set the graph colors
            if (i < NB_COLORS) {
                renderer.setSeriesPaint(i, colorList.get(i));
            }
        }
        plot.setRenderer(renderer);

        // change the auto tick unit selection to integer units only...
        final NumberAxis rangeAxis = (NumberAxis) plot.getRangeAxis();
        rangeAxis.setStandardTickUnits(NumberAxis.createIntegerTickUnits());
        // OPTIONAL CUSTOMISATION COMPLETED.

        DateAxis axis = (DateAxis) plot.getDomainAxis();
        axis.setDateFormatOverride(new SimpleDateFormat("HH:mm"));

    }

    /**
     * Gets the chart.
     * 
     * @return the chart.
     */
    @Override
    public JFreeChart getChart() {
        return chart;
    }

    /**
     * Feeds the dataset.
     * 
     * @param sbtList : the staff by time list
     */
    public void setBeanList(final List<StaffByTime> sbtList) {

        dataset.removeAllSeries();

        final List<TimeSeries> seriesList = new ArrayList<TimeSeries>();

        // sorts the list
        Collections.sort(sbtList, new StaffByTimeComparator());

        // as many series as the number of resources
        for (StaffByTime bean : sbtList) {
            TimeSeries ts = new TimeSeries(bean.getResource());
            if (!seriesList.contains(ts)) {
                seriesList.add(ts);
            }
        }

        // feeds the series
        for (StaffByTime bean : sbtList) {
            Boolean found = false;
            int ind = -1;
            // int ind = seriesList.indexOf(ts); DOESN'T FUNCTION SO LET'S SEARCH OURSELVES
            for (TimeSeries ts : seriesList) {
                if (ts.getKey().toString().equals(bean.getResource())) {
                    ind = seriesList.indexOf(ts);
                    found = true;
                    break;
                }
            }
            if (found) {
                // I add ind / 10.0 in order that the lines at the same height are distinct.
                // If I don't, we only see one of them.
                RegularTimePeriod period = new Second(bean.getTime());
                double staff = bean.getStaff() + ind / 10.0;
                TimeSeriesDataItem item = new TimeSeriesDataItem(period, staff);
                // seriesList.get(ind).delete(period);
                seriesList.get(ind).addOrUpdate(item);
            }
        }

        // adds the series to the diagram
        for (TimeSeries serie : seriesList) {
            dataset.addSeries(serie);
        }

    }

    /**
     * Transforms a color into RGB.
     * 
     * @param color : the color
     * @return the RGB
     */
    private Paint toColor(final StandardColor color) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue());
    }

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionContainerMOListVView.java,v $
 * Created on 7 janv. 09 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioncontainermolist.touch;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import fr.vif.jtech.ui.input.touch.TouchInputBarCode;
import fr.vif.jtech.ui.input.touch.TouchInputTextEditor;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.comment.business.beans.common.Comment;
import fr.vif.vif5_7.gen.comment.ui.composites.simplecomment.touch.CCommentTouchView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.ui.composites.barcode.BarCodeCtrl;
import fr.vif.vif5_7.production.mo.ui.dialog.common.touch.FullScreenTouchMessageDialog;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * viewer view.
 *
 * @author gv
 */
public class FProductionContainerMOListVView extends StandardTouchViewer<FMOListVBean> {

    private CCommentTouchView    commentTouchView     = null;
    /**
     * Manage the OF header criteria complete list.
     */
    private TouchInputTextEditor criteriaLstText      = null;
    /**
     * DP2232 - CHU - manage the OF header criteria short list.
     */
    private TouchInputTextEditor shortCriteriaLstText = null;
    private TouchInputBarCode    inputBarCode         = null;
    /** The label with flash icon. */
    private JLabel               jlIconFlash          = new JLabel(
                                                              new ImageIcon(
                                                                      getClass()
                                                                              .getResource(
                                                                                      "/images/vif/vif57/production/mo/img32x32/flashModeOn.png")));
    private JLabel               jlFlash;

    private JPanel               jpFlash;

    /**
     * Default constructor.
     * 
     */
    public FProductionContainerMOListVView() {
        super();
        initialize();
    }

    /**
     * Gets the commentTouchView.
     * 
     * @category getter
     * @return the commentTouchView.
     */
    public final CCommentTouchView getCommentTouchView() {
        return commentTouchView;
    }

    /**
     * Gets the commentView.
     * 
     * @category getter
     * @return the commentView.
     */
    public final CCommentTouchView getCommentView() {
        if (commentTouchView == null) {
            commentTouchView = new CCommentTouchView();
            Dimension d = new Dimension(870, 60);
            commentTouchView.setMaximumSize(d);
            commentTouchView.setMinimumSize(d);
            commentTouchView.setPreferredSize(d);
            commentTouchView.setBeanProperty("comment");
            commentTouchView.getEditorComment().setVisibleLabel(false);

            // Window opening when mouse click
            commentTouchView.getEditorComment().getJTextArea().addMouseListener(new MouseAdapter() {

                @Override
                public void mouseReleased(final MouseEvent e) {
                    super.mouseReleased(e);
                    if (!((Comment) commentTouchView.getValue()).getCom().equals("")) {
                        FullScreenTouchMessageDialog.showMessage(commentTouchView,
                                I18nClientManager.translate(Generic.T26356, false),
                                ((Comment) commentTouchView.getValue()).getCom(), MessageButtons.OK);
                    }
                }
            });
        }
        return commentTouchView;
    }

    /**
     * Get the touch input barcode.
     * 
     * @return the touch input bar code
     */
    public TouchInputBarCode getInputBarCode() {
        if (inputBarCode == null) {
            inputBarCode = new TouchInputBarCode(BarCodeCtrl.class);
            Dimension d = new Dimension(796, 30);
            inputBarCode.setPreferredSize(d);
            inputBarCode.setMinimumSize(d);
            inputBarCode.setMaximumSize(d);
            // inputBarCode.setBeanProperty("barcode");
            inputBarCode.setBeanBased(false);
            inputBarCode.setBorder(new LineBorder(
                    TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR), 1, false));
            inputBarCode.setEnabled(true);
        }
        return inputBarCode;
    }

    /**
     * Get the label flash.
     * 
     * @return the label flash
     */
    public JLabel getJlFlash() {
        if (jlFlash == null) {
            jlFlash = new JLabel();
            jlFlash.setBounds(10, 2, 55, 30);
            jlFlash.setForeground(new Color(238, 127, 0));
            jlFlash.setFont(new Font("Arial", Font.BOLD, 16));
            jlFlash.setText(I18nClientManager.translate(ProductionMo.T37737, false));
        }
        return jlFlash;
    }

    /**
     * Get Flash panel.
     * 
     * @return the flash panel
     */
    public JPanel getJpFlash() {
        if (jpFlash == null) {
            jpFlash = new JPanel();
            final GridBagLayout gridBagLayout = new GridBagLayout();
            gridBagLayout.columnWidths = new int[] { 0, 7 };
            jpFlash.setLayout(gridBagLayout);
            Dimension d = new Dimension(870, 0);
            jpFlash.setPreferredSize(d);
            jpFlash.setMinimumSize(d);
            jpFlash.setMaximumSize(d);
            // jpFlash.setBounds(10, 3, 850, 29);
            jpFlash.setOpaque(false);
            final GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new Insets(0, 0, 0, 5);
            gridBagConstraints.anchor = GridBagConstraints.EAST;
            gridBagConstraints.weighty = 1;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.gridx = 0;
            jpFlash.add(getJlFlash(), gridBagConstraints);
            final GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.weighty = 1;
            gridBagConstraints1.fill = GridBagConstraints.BOTH;
            gridBagConstraints1.weightx = 1;
            gridBagConstraints1.gridy = 0;
            gridBagConstraints1.gridx = 1;
            jpFlash.add(getInputBarCode(), gridBagConstraints1);
        }
        return jpFlash;
    }

    /**
     * Sets the commentTouchView.
     * 
     * @category setter
     * @param commentTouchView commentTouchView.
     */
    public final void setCommentTouchView(final CCommentTouchView commentTouchView) {
        this.commentTouchView = commentTouchView;
    }

    /**
     * Gets the CriteriaLstText.
     * 
     * @return the TouchInputTextEditor.
     */
    private TouchInputTextEditor getCriteriaLstText() {
        if (criteriaLstText == null) {
            criteriaLstText = new TouchInputTextEditor();
            criteriaLstText.getJTextArea().setOpaque(true);
            criteriaLstText.setBeanBased(true);
            criteriaLstText.setBeanProperty("listCriteria");
            criteriaLstText.setIcon("/images/vif/vif57/production/mo/littleIcon_criteria.png");
            criteriaLstText.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_LABEL_FONT));
            criteriaLstText.setAlwaysDisabled(true);
            criteriaLstText.setUpdateable(false);
            Dimension d = new Dimension(0, 0); // (870, 35); DP2232 - CHU
            criteriaLstText.setPreferredSize(d);
            criteriaLstText.setMinimumSize(d);
            criteriaLstText.setMaximumSize(d);
        }
        return criteriaLstText;
    }

    /**
     * DP2232 - CHU - Gets the shortCriteriaLstText.
     * 
     * @return the TouchInputTextEditor.
     */
    private TouchInputTextEditor getShortCriteriaLstText() {
        if (shortCriteriaLstText == null) {
            shortCriteriaLstText = new TouchInputTextEditor();
            shortCriteriaLstText.getJTextArea().setOpaque(true);
            shortCriteriaLstText.setBeanBased(true);
            shortCriteriaLstText.setBeanProperty("shortListCriteria");
            shortCriteriaLstText.setIcon("/images/vif/vif57/production/mo/littleIcon_criteria.png");
            shortCriteriaLstText.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_LABEL_FONT));
            shortCriteriaLstText.setAlwaysDisabled(true);
            shortCriteriaLstText.setUpdateable(false);
            Dimension d = new Dimension(830, 35);
            shortCriteriaLstText.setPreferredSize(d);
            shortCriteriaLstText.setMinimumSize(d);
            shortCriteriaLstText.setMaximumSize(d);

            // Window opening when mouse click
            shortCriteriaLstText.getJTextArea().addMouseListener(new MouseAdapter() {

                @Override
                public void mouseReleased(final MouseEvent e) {
                    super.mouseReleased(e);
                    if (!(criteriaLstText.getValue()).equals("")) {
                        String vLstCritDisplay = criteriaLstText.getValue().toString();
                        vLstCritDisplay = vLstCritDisplay.replace(", ", "\n"); // ALB DP2232
                        FullScreenTouchMessageDialog.showMessage(criteriaLstText,
                                I18nClientManager.translate(ProductionMo.T34359, false), vLstCritDisplay,
                                FProductionConstant.CRITERIA_ICON);

                    }
                }
            });

        }
        return shortCriteriaLstText;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(getCriteriaLstText());
        // DP2232 - CHU
        this.add(getJpFlash());
        JPanel panCriteria = new JPanel();
        panCriteria.setLayout(new BoxLayout(panCriteria, BoxLayout.X_AXIS));
        panCriteria.add(getShortCriteriaLstText());
        panCriteria.add(jlIconFlash);
        panCriteria.add(Box.createHorizontalStrut(4));
        this.add(panCriteria);
        // this.add(getShortCriteriaLstText());
        this.add(getCommentView());

    }

}

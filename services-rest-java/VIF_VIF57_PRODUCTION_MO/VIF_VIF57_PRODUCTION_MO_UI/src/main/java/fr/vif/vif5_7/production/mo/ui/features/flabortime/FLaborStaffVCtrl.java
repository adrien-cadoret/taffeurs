/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffVCtrl.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;
import fr.vif.vif5_7.production.mo.business.services.features.flaborstaff.FLaborStaffCBS;


/**
 * Labor staff viewer controller.
 * 
 * @author nle
 */
public class FLaborStaffVCtrl extends AbstractStandardViewerController<FLaborStaffBBean> {

    private FLaborStaffCBS flaborstaffCBS;

    /**
     * Gets the flaborstaffCBS.
     * 
     * @return the flaborstaffCBS.
     */
    public FLaborStaffCBS getFlaborstaffCBS() {
        return flaborstaffCBS;
    }

    /**
     * Sets the flaborstaffCBS.
     * 
     * @param flaborstaffCBS flaborstaffCBS.
     */
    public void setFlaborstaffCBS(final FLaborStaffCBS flaborstaffCBS) {
        this.flaborstaffCBS = flaborstaffCBS;
    }

    /**
     * Show an error in viewer error panel.
     * 
     * @param e the exception
     */
    public void showError(final UIErrorsException e) {
        fireErrorsChanged(e);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FLaborStaffBBean convert(final Object pBean) throws UIException {
        return (FLaborStaffBBean) pBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        // hide the button bar at the bottom of the screen
        return false;
    }
}

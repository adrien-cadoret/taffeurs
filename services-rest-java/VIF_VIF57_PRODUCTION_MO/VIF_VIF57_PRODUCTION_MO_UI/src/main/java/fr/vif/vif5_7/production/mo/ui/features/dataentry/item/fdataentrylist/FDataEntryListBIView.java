/*
 * Copyright (c) 2015 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListBIView.java,v $
 * Created on 9 oct. 2015 by alb
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import javax.swing.JTable;


/**
 * interface Data entry list.
 *
 * @author alb
 */
public interface FDataEntryListBIView {
    /**
     * Gets the Jtable !
     * 
     * @return the jtable
     */
    public JTable getJTableForResize();
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompFView.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp.touch;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.touch.FProductionBView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerFIView;
import fr.vif.vif5_7.workshop.device.ui.composites.devices.scale.touch.CPerpetualScaleWeightTouchView;


/**
 * FProduction Output Comp Feature view.
 * 
 * @author kl
 */
public class FProductionOutputCompFView extends StandardTouchFeature implements FProductionOutputContainerFIView {

    private FProductionBView               bView;
    private CPerpetualScaleWeightTouchView scaleView;
    private TitlePanel                     subTitlePanel;
    private FProductionOutputCompVView     vView;

    private FProductionOutputCompGSView    fProductionOutputCompGSView = null;

    /**
     * Default constructor.
     */
    public FProductionOutputCompFView() {
        super();
        initialize();
    }

    /**
     * Gets the bView.
     * 
     * @category getter
     * @return the bView.
     */
    public FProductionBView getBView() {
        if (bView == null) {
            bView = new FProductionBView();
            bView.setBounds(0, 214, 867, 82);
        }
        return bView;
    }

    /**
     * Gets the fProductionOutputCompGSView.
     * 
     * @return the fProductionOutputCompGSView.
     */
    public FProductionOutputCompGSView getfProductionOutputCompGSView() {
        if (fProductionOutputCompGSView == null) {
            fProductionOutputCompGSView = new FProductionOutputCompGSView();
            fProductionOutputCompGSView.setLocation(0, 140);
            fProductionOutputCompGSView.setSize(867, 65);
        }
        return fProductionOutputCompGSView;
    }

    /**
     * Get the weight composite.
     * 
     * @return the weight composite
     */
    public CPerpetualScaleWeightTouchView getScaleView() {
        if (scaleView == null) {
            scaleView = new CPerpetualScaleWeightTouchView();
            scaleView.setBounds(0, 0, 867, 100);
            scaleView.setOpaque(false);
        }
        return scaleView;
    }

    /**
     * Gets the vView.
     * 
     * @category getter
     * @return the vView.
     */
    public FProductionOutputCompVView getVView() {
        if (vView == null) {
            vView = new FProductionOutputCompVView();
            vView.setBounds(0, 300, 870, 300);
        }
        return vView;
    }

    /**
     * Sets the bView.
     * 
     * @category setter
     * @param productionBView bView.
     */
    public void setBView(final FProductionBView productionBView) {
        bView = productionBView;
    }

    /**
     * {@inheritDoc}
     */
    public void setSubTitle(final String subTitle) {
        subTitlePanel.setTitle(subTitle);

    }

    /**
     * Get the subtitle panel.
     * 
     * @return the subtitile panel
     */
    private JPanel getSubTitle() {
        if (subTitlePanel == null) {
            subTitlePanel = new TitlePanel();
            subTitlePanel.setBounds(0, 101, 870, 40);
            subTitlePanel.setBorder(new MatteBorder(1, 0, 0, 2, Color.white));
            subTitlePanel.setPreferredSize(new Dimension(800, 40));
            subTitlePanel.setMinimumSize(new Dimension(500, 40));
        }
        return subTitlePanel;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setPreferredSize(new Dimension(870, 768));
        setLayout(null);
        add(getSubTitle());
        add(getBView());
        add(getVView());
        add(getScaleView());
        add(getfProductionOutputCompGSView());
    }
}

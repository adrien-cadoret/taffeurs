/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingVModel.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingVBean;


/**
 * TableLook Viewer Model.
 * 
 * @author kl
 */
public class FTableLookSettingVModel extends ViewerModel<FTableLookSettingVBean> {

    /**
     * Default constructor.
     */
    public FTableLookSettingVModel() {
        super();
        setBeanClass(FTableLookSettingVBean.class);
        getUpdateActions().setCopyEnabled(false);
        getUpdateActions().setDeleteEnabled(false);
        getUpdateActions().setDocEnabled(false);
        getUpdateActions().setInfoEnabled(false);
        getUpdateActions().setNewEnabled(false);
        getUpdateActions().setPrintEnabled(false);
        getUpdateActions().setSaveEnabled(true);
        getUpdateActions().setUndoEnabled(true);
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMSOCreationBModel.java,v $
 * Created on 19 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.touch.TouchBrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmsocreation.FMSOCreationVBean;


/**
 * MSO creation browser model.
 * 
 * @author glc
 */
public class FMSOCreationBModel extends TouchBrowserModel<FMSOCreationVBean> {

    /**
     * Simple constructor.
     */
    public FMSOCreationBModel() {
        super();
        setBeanClass(FMSOCreationVBean.class);
        addColumn(new BrowserColumn("", "moKey.chrono.chrono", 0));
        setFetchSize(20);
        setFirstFetchSize(40);
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationBCtrl.java,v $
 * Created on 19 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;


/**
 * Controller for browser on items for MO creation.
 * 
 * @author glc
 */
public class FMOCreationBCtrl extends AbstractBrowserController {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Object convert(final Object bean) {
        return getModel().getCurrentBean();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        return new ArrayList();
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DMarkingDocumentsCtrl.java,v $
 * Created on 18 févr. 2014 by lm
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.dialogs.DialogModel;
import fr.vif.jtech.ui.dialogs.DialogView;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.models.identification.Identification;
import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.DMarkingDocumentsBean;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch.DMarkingDocumentsTouchView;


/**
 * Marking documents' selection Ctrl.
 * 
 * @author lm
 */
public class DMarkingDocumentsCtrl extends AbstractStandardDialogController {

    /**
     * Constructor.
     */
    public DMarkingDocumentsCtrl() {
        super();
    }

    /**
     * 
     * Show the documents' selection view.
     * 
     * @param featureView Feature owner
     * @param changeListener change Listener
     * @param displayListener display Listener.
     * @param identification Identification
     * @param bean the bean to show.
     * @throws UIException if error occurs or if dialog is canceled
     * @return the DMarkingDocumentsCtrl
     */
    public static DMarkingDocumentsCtrl show(final FeatureView featureView, final DialogChangeListener changeListener,
            final DisplayListener displayListener, final Identification identification, final DMarkingDocumentsBean bean)
            throws UIException {

        DMarkingDocumentsCtrl ctrl = null;
        if (featureView != null) {
            DialogView view = null;

            view = featureView.openDialog(DMarkingDocumentsTouchView.class);

            DialogModel model = new DialogModel();
            model.setIdentification(identification);
            view.setModel(model);
            ctrl = new DMarkingDocumentsCtrl();
            ctrl.setDialogView(view);
            ctrl.addDialogChangeListener(changeListener);
            ctrl.addDisplayListener(displayListener);

            // rollBean.setViewIsOpen();
            model.setDialogBean(bean);
            ctrl.initialize();
            ctrl.removeDialogChangeListener(changeListener);
            ctrl.removeDisplayListener(displayListener);

        }

        return ctrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beanValidation(final Object dialogBean) throws UIException, UIErrorsException {
        if (dialogBean instanceof DMarkingDocumentsBean) {
            DMarkingDocumentsBean vBean = (DMarkingDocumentsBean) dialogBean;

            boolean vNoDoc = true;

            if ((vBean.getDocument1() != null && EntityTools.isValid(vBean.getDocument1().getCode())) //
                    || (vBean.getDocument2() != null && EntityTools.isValid(vBean.getDocument2().getCode())) //
                    || (vBean.getDocument3() != null && EntityTools.isValid(vBean.getDocument3().getCode())) //
                    || (vBean.getDocument4() != null && EntityTools.isValid(vBean.getDocument4().getCode())) //
                    || (vBean.getDocument5() != null && EntityTools.isValid(vBean.getDocument5().getCode())) //
                    || (vBean.getDocument6() != null && EntityTools.isValid(vBean.getDocument6().getCode()))) {
                vNoDoc = false;
            }

            if (vNoDoc) {
                throw new UIException(ProductionMo.T30839);
            }
        }
        super.beanValidation(dialogBean);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {
        // nothing to do
    }

}

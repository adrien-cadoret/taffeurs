/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PREPARATION_KERNEL_UI
 * File : $RCSfile: DViewNameMOView.java,v $
 * Created on 9 sept. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.dialog.viewnamemo.swing;


import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.vif.jtech.ui.dialogs.swing.StandardSwingDialog;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;


/**
 * View of the password dialog.
 * 
 * @author vr
 */
public class DViewNameMOView extends StandardSwingDialog {

    private SwingInputTextField itfName = null;
    private JLabel              jLabel  = null;
    private JPanel              jpName  = null;

    /**
     * Constructor.
     */
    public DViewNameMOView() {
        super();
        initialize();
    }

    /**
     * This method initializes.
     * 
     * @param view a Dialog
     */
    public DViewNameMOView(final Dialog view) {
        super(view);
        initialize();
    }

    /**
     * This method initializes.
     * 
     * @param view a Frame
     */
    public DViewNameMOView(final Frame view) {
        super(view);
        initialize();
    }

    /**
     * Gets the itfName.
     * 
     * @return the itfName.
     */
    public SwingInputTextField getItfName() {
        return itfName;
    }

    // CHECKSTYLE:OFF
    /**
     * CHECKSTYLE:OFF
     * 
     * @return
     */
    public SwingInputTextField getItfPassword() {
        if (itfName == null) {
            itfName = new SwingInputTextField(String.class, "10", false, jLabel);
            itfName.setBeanProperty("name"); // NLE : to suppress if we want to put the following code,
            // if we manage to make it function
            // itfName.setBeanBased(false); // NLE
            // DViewNameMOBean bean = new DViewNameMOBean(); // NLE
            // bean.setList(getManager().getFavoriteViews(getIdCtx())); // NLE
            // itfName.setValue(bean.getName()); // NLE
            // System.out.println(super.getTitle()); // NLE

        }
        return itfName;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {
        super.initializeView();
        // getItfName().setValue(this.getTitle());
    }

    /**
     * Sets the itfName.
     * 
     * @param itfName itfName.
     */
    public void setItfName(final SwingInputTextField itfName) {
        this.itfName = itfName;
    }

    // CHECKSTYLE:ON
    /**
     * This method initializes jLabel.
     * 
     * @return javax.swing.JLabel
     */
    private JLabel getJLabel() {
        if (jLabel == null) {
            jLabel = new JLabel();
            jLabel.setText(I18nClientManager.translate(ProductionMo.T33253)); // Save name
            // jLabel.setIcon(IconFactory.getIcon48(KernelIcon.IND_VIEWS));
        }
        return jLabel;
    }

    /**
     * This method initializes jPanelBB.
     * 
     * @return javax.swing.KeyboardscreenJPanel
     */
    private JPanel getJpPassword() {
        if (jpName == null) {
            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.fill = GridBagConstraints.NONE;
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.insets = new Insets(0, 5, 0, 10);
            jpName = new JPanel();
            jpName.setPreferredSize(new Dimension(400, 70));
            jpName.setLayout(new GridBagLayout());
            jpName.add(getJLabel(), gridBagConstraints1);
            jpName.add(getItfPassword(), gridBagConstraints);
        }
        return jpName;
    }

    /**
     * Initialize selection dialog-box size and set the main panel.
     * 
     */
    private void initialize() {
        this.setSize(new Dimension(350, 100));
        this.setTitle(I18nClientManager.translate(ProductionMo.T33253)); // Save name
        super.addPanel(getJpPassword());

        // getBannerPanel().setTitle(I18nClientManager.translate(PreparationKernel.T32201));
        // getBannerPanel().setTitleIcon((ImageIcon) IconFactory.getIcon(IconFactory.PATH_32X32, KernelIcon.UN_LOCK));
        // getBannerPanel().setTitleIcon((ImageIcon) IconFactory.getIcon48(KernelIcon.IND_VIEWS));
        pack();
    }

}

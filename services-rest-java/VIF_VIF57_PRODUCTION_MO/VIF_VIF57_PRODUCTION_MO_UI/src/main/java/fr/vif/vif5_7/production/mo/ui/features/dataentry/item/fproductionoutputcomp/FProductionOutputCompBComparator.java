/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompBComparator.java,v $
 * Created on 23 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp;


import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBComparator;


/**
 * FProductionOutputScrapB comparator.
 * 
 * @author kl
 */
public class FProductionOutputCompBComparator extends FProductionBComparator {

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(final FProductionBBean o1, final FProductionBBean o2) {
        int ret = super.compare(o1, o2);

        // Compare on the item if it's a downgraded item. The OperationItemKey of o1 and a2 are equals, so we have to
        // compare on the item.
        if (MONatureComplementaryWorkshop.DOWNGRADED.equals(o1.getNatureComplementaryWorkshop())) {
            ret = o1.getItemCLs().getCode().compareToIgnoreCase(o2.getItemCLs().getCode());
        }
        return ret;
    }

}

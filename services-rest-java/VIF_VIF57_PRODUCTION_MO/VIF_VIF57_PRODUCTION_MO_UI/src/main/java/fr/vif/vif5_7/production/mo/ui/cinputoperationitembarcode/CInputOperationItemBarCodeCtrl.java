/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: CInputOperationItemBarCodeCtrl.java,v $
 * Created on 21 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.cinputoperationitembarcode;


import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.input.AbstractInputBarCodeController;


/**
 * CInputOperationItemBarCodeCtrl.
 * 
 * @author vr
 */
public class CInputOperationItemBarCodeCtrl extends AbstractInputBarCodeController {

    @Override
    public void validateBarCodeSyntax(final String value) throws UIException {

    }

}

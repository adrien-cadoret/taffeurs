/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DWeightKeyboardDCtrl.java,v $
 * Created on 16 Oct. 2014 by CJ
 */
package fr.vif.vif5_7.production.mo.ui.dialog.number;


import fr.vif.jtech.common.beans.KeyboardBean;
import fr.vif.jtech.ui.dialogs.DialogView;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.featurecontainer.FeatureContainerController;
import fr.vif.jtech.ui.input.kbdialog.KeyboardDialogModel;
import fr.vif.jtech.ui.input.kbdialog.NumKeyboardDialogController;
import fr.vif.jtech.ui.models.identification.Identification;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.production.mo.business.beans.dialog.number.NumericDBean;
import fr.vif.vif5_7.production.mo.ui.dialog.number.touch.DNumberKeyboardTouchView;


/**
 * Dialog Number Keyboard controller for weight input.
 * 
 * @author cj
 */
public class DWeightKeyboardDCtrl extends NumKeyboardDialogController implements DialogChangeListener {

    /** The bean. */
    private NumericDBean         bean;

    /** The parent dialog change listener. */
    private DialogChangeListener parentDialogChangeListener;

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        DialogChangeEvent newEvent = event;
        if (event.getDialogBean() instanceof KeyboardBean) {
            // As calling feature is waiting for Measure Value we have to change the bean
            KeyboardBean keyboardBean = (KeyboardBean) event.getDialogBean();
            if (this.bean == null) {
                this.bean = new NumericDBean();
            }
            double qty = Double.valueOf(((String) keyboardBean.getValue()).replace(",", "."));
            this.bean.getInner().setQty(qty);

            String unit = bean.getInner().getUnit();

            bean.setInnerStr(qty + unit);

            newEvent = new DialogChangeEvent(this, event.getEventName(), this.bean);
        }
        this.parentDialogChangeListener.dialogValidated(newEvent);
    }

    /**
     * Gets the bean.
     * 
     * @return the bean.
     */
    public NumericDBean getBean() {
        return bean;
    }

    /**
     * 
     * Show the Feature to entry the number of documents to print.
     * 
     * @param featureView featureView which call
     * @param displayListener display Listener.
     * @param dialogChangeListener dialog change listener
     * @param identification Identification
     * @param numericDBean Dialog bean to create
     */
    public void showKeyboard(final FeatureView featureView, final DisplayListener displayListener,
            final DialogChangeListener dialogChangeListener, final Identification identification,
            final NumericDBean numericDBean) {

        this.bean = numericDBean;
        this.parentDialogChangeListener = dialogChangeListener;

        KeyboardDialogModel model = new KeyboardDialogModel();
        model.setIdentification(identification);
        DialogView view = null;
        String viewType = System.getProperty(FeatureContainerController.VIF_UITYPE_KEY);
        if ("touch".equalsIgnoreCase(viewType)) {
            view = featureView.openDialog(DNumberKeyboardTouchView.class);

            view.setModel(model);

            view.setTitle(I18nClientManager.translate(GenKernel.T20566, false));

            setDialogView(view);
        }

        // This will be KeyBoard Input Dialog Listener
        addDialogChangeListener(this);

        addDisplayListener(displayListener);
        KeyboardBean keyBoard = new KeyboardBean();
        model.setDialogBean(keyBoard);
        initialize();
        removeDialogChangeListener(this);
        removeDisplayListener(displayListener);
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFBoningMOListFCtrl.java,v $
 * Created on 14 mar. 2013 by CJ
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist;


import java.awt.Frame;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.dialogs.DialogController;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerCtrl;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerModel;
import fr.vif.jtech.ui.docviewer.pdfviewer.touch.TouchPDFViewerView;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.sharedcontext.SharedContextEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarFunctionalitiesBtnDController;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchFunctionalitiesBtnDView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchToolBarBtnStd;
import fr.vif.jtech.ui.toolbar.buttons.toolbarbutton.ToolBarButtonController;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmolist.FBoningMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionmo.FProductionMOListCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOState;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.FeaturesId;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation.FBoningMOCreationFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerFCtrl;
import fr.vif.vif5_7.stock.kernel.business.beans.features.quality.nc.fncflow.FNCFlowSharedContextBean;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCConstants;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCActType;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCType;
import fr.vif.vif5_7.stock.kernel.ui.util.StockUITools;
import fr.vif.vif5_7.workshop.workstation.business.beans.common.WorkStationKey;
import fr.vif.vif5_7.workshop.workstation.business.beans.dialog.debug.DDebugBean;
import fr.vif.vif5_7.workshop.workstation.business.beans.features.fworkstation.FWorkStationSBean;
import fr.vif.vif5_7.workshop.workstation.ui.dialog.debug.DDebugCtrl;
import fr.vif.vif5_7.workshop.workstation.ui.features.fworkstation.WorkStationTools;


/**
 * Boning MO List : Abstract Feature Controller.
 * 
 * @author cj
 */
public abstract class AbstractFBoningMOListFCtrl extends StandardFeatureController implements TableRowChangeListener,
DialogChangeListener {

    public static final String   BTN_PDF_REFERENCE       = "BTN_PDF_REFERENCE";
    public static final String   BTN_OPEN_INCIDENT       = "OPEN_NEW_INCIDENT";                               // Non-conformity/incident
    public static final String   BTN_PDF_LIST_REFERENCE  = "BTN_PDF_LIST_REFERENCE";
    /**
     * Show all button reference.
     */
    public static final String   BTN_SHOW_ALL_REFERENCE  = "BTN_SHOW_ALL_REFERENCE";
    public static final String   LOGICAL_WORKSTATION_BTN = "logicalWorkstation";
    public static final String   MO_CREATE_BTN           = "moCreateBtn";
    public static final String   MO_CREATE_MSO_BTN       = "moCreateTech";
    public static final String   MO_FINISH_BTN           = "moFinishing";
    public static final String   MO_INPUT_BTN            = "moInput";
    public static final String   USER_VIF                = "VIF";
    public static final String   USER_VIF_INTEG          = "VIFINTEG";

    /**
     * list of btns.
     */
    public static final String   MO_OUTPUT_BTN           = "moOutput";

    private static final Logger  LOGGER                  = Logger.getLogger(AbstractFBoningMOListFCtrl.class);

    private ToolBarBtnStdModel   btnFinish               = null;
    private ToolBarBtnStdModel   btnPDFModel;
    private ToolBarBtnStdModel   btnOpenIncidentModel;
    private FProductionMOListCBS productionMOListCBS     = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        super.buttonAction(event);
        try {
            if (event.getModel().getReference().equals(AbstractFBoningMOListFCtrl.LOGICAL_WORKSTATION_BTN)) {
                String wsId = (String) getSharedContext().get(Domain.DOMAIN_THIS,
                        FProductionConstant.LOGICAL_WORKSTATION);
                FWorkStationSBean selection = new FWorkStationSBean(new WorkStationKey(getIdCtx().getCompany(),
                        getIdCtx().getEstablishment(), wsId));
                selection.setGroupOnly(true); // Sélection du groupe du poste seulement
                CodeLabel wsCL = new WorkStationTools(this).getWorkStationCodeLabel(getViewerController().getView(),
                        selection);

                if (null != wsCL && null != wsCL.getCode() && !"".equals(wsCL.getCode())) {
                    getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.LOGICAL_WORKSTATION, wsCL.getCode());
                    fireSharedContextSend(getSharedContext());
                    ((FMOListSBean) getGeneralSelectionController().getModel().getBean()).setWorkstationId(wsCL
                            .getCode());
                    refresh((Selection) getGeneralSelectionController().getModel().getBean(), null);
                }
            } else if (event.getModel().getReference().equals(AbstractFBoningMOListFCtrl.MO_FINISH_BTN)) {
                String msg = I18nClientManager.translate(ProductionMo.T17368, false);
                FBoningMOListBBean currentBean = (FBoningMOListBBean) getBrowserController().getModel()
                        .getCurrentBean();
                if (currentBean.getIsFinished()) {
                    msg = I18nClientManager.translate(ProductionMo.T29551, false);
                }

                int msgBtn = showQuestion(getBtnFinish().getText(), msg, MessageButtons.YES_NO);
                if (msgBtn == MessageButtons.YES) {
                    FMOListSBean sBean = (FMOListSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                            FProductionConstant.MO_INITIAL_SELECTON);

                    ((FBoningMOListVCtrl) getViewerController()).finishMO(((FBoningMOListBBean) getBrowserController()
                            .getModel().getCurrentBean()).getIsFinished(), sBean.getActivityItemType());
                    refresh(null, currentBean);
                }
            } else if (BTN_PDF_REFERENCE.equalsIgnoreCase(event.getModel().getReference())) {
                try {

                    List<PDFFile> listPdfFiles = getDocumentsToShow();
                    if (listPdfFiles.isEmpty()) {
                        getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                                I18nClientManager.translate(ProductionMo.T30839));
                    } else if (listPdfFiles.size() == 1) {
                        viewDocumentRecette(listPdfFiles.get(0));
                    } else {
                        viewDocumentList(listPdfFiles);
                    }

                } catch (BusinessException e) {
                    getView().show(new UIException(e));
                }
            } else if (event.getModel().getReference().equals(AbstractFBoningMOListFCtrl.BTN_SHOW_ALL_REFERENCE)) {
                FMOListSBean selection = (FMOListSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                        FProductionConstant.MO_INITIAL_SELECTON);

                selection.setShowFinished(!selection.isShowFinished());
                refresh(selection, (FBoningMOListBBean) getBrowserController().getModel().getCurrentBean());

            } else if (BTN_OPEN_INCIDENT.equals(event.getModel().getReference())) {

                // Prepare parameters bean
                FNCFlowSharedContextBean parametersBean = new FNCFlowSharedContextBean();
                parametersBean.setType(NCType.INTERNAL.getKey());

                // Feed with viewer bean informations
                FMOListVBean vBean = (FMOListVBean) getViewerController().getBean();
                parametersBean.setActType(NCActType.MANUFACTURING_ORDER.getKey());
                parametersBean.setChronoOrigin(vBean.getMobean().getMoKey().getChrono());
                StockItem stockItem = new StockItem();
                stockItem.setItemId(vBean.getMobean().getItemCLs().getCode());
                stockItem.getQuantities().getFirstQty().setQty(vBean.getMobean().getMoQuantityUnit().getToDoQuantity());
                stockItem.getQuantities().getFirstQty().setUnit(vBean.getMobean().getMoQuantityUnit().getUnit());
                parametersBean.setStockItem(stockItem);

                // Put parameters in context and open feature
                getSharedContext().put(Domain.DOMAIN_THIS, NCConstants.NCFLOW_KEY_SHARED_CONTEXT, parametersBean);
                if (StockUITools.isTouchRunning()) {
                    openChildFeature(NCConstants.FeatureName.FLOW_TOUCH.getName());
                } else {
                    openChildFeature(NCConstants.FeatureName.FLOW.getName());
                }
            } else if (BTN_PDF_LIST_REFERENCE.equals(event.getModel().getReference())) {
                PDFFile pdfFile = (PDFFile) ((ToolBarBtnStdModel) event.getModel()).getLinkedObject();
                viewDocumentRecette(pdfFile);
            } else if ("resetTableFocus".equals(event.getModel().getReference())) {
                getBrowserController().requestFocus();
            } else if (ButtonReference.DEBUG_REFERENCE.getReference().equals(event.getModel().getReference())) {
                DDebugBean bean = new DDebugBean();
                bean.getObjectsToShow().add(getBrowserController().getModel().getBeans());
                bean.getObjectsToShow().add(getViewerController().getBean());
                bean.getObjectsToShow().add(getBrowserController().getInitialSelection());
                DDebugCtrl.showDebug(this.getView(), this, this, this.getModel().getIdentification(), bean);
            }

        } catch (UIException e) {
            getView().showError(e.getLocalizedMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void childFeatureClosed(final FeatureController childFeatureController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }

        super.childFeatureClosed(childFeatureController);
        String featureId = (String) getSharedContext().get(Domain.DOMAIN_CHILD, FProductionConstant.FEATURE_ID);
        getSharedContext().remove(Domain.DOMAIN_CHILD, FProductionConstant.FEATURE_ID);

        MOBean bean = null;
        if (childFeatureController instanceof FProductionInputContainerFCtrl) {
            bean = ((FProductionInputContainerFCtrl) childFeatureController).getMoListVBean().getMobean();
        } else if (childFeatureController instanceof FProductionOutputContainerFCtrl) {
            bean = ((FProductionOutputContainerFCtrl) childFeatureController).getMoListVBean().getMobean();
        } else if (childFeatureController instanceof FBoningMOCreationFCtrl
                || childFeatureController instanceof FBoningMSOCreationFCtrl) {
            bean = (MOBean) getSharedContext().get(Domain.DOMAIN_CHILD, FProductionConstant.STARTED_MO_BEAN);
        } else {
            FBoningMOListBBean bbean = (FBoningMOListBBean) getSharedContext().get(Domain.DOMAIN_THIS,
                    FProductionConstant.MO_LIST_BBEAN);
            bean = bbean.getMobean();
        }

        if (featureId != null && !"".equals(featureId)) {
            openChildFeature(featureId);
            getSharedContext().remove(Domain.DOMAIN_CHILD, FProductionConstant.FEATURE_ID);
            FBoningMOListBBean bbean = (FBoningMOListBBean) getSharedContext().get(Domain.DOMAIN_THIS,
                    FProductionConstant.MO_LIST_BBEAN);
            refresh(null, bbean);

        } else if (childFeatureController instanceof FBoningMSOCreationFCtrl
                || childFeatureController instanceof FBoningMOCreationFCtrl) {
            FBoningMOListBBean beanSelected = (FBoningMOListBBean) getBrowserController().getModel().getCurrentBean();

            getSharedContext().remove(Domain.DOMAIN_CHILD, FProductionConstant.STARTED_MO_BEAN);

            if (bean != null) {
                ((FMOListSBean) getGeneralSelectionController().getBean()).setMoDate(new Date());
                getGeneralSelectionController().getView().render();

                FBoningMOListBBean browserBean = new FBoningMOListBBean();
                browserBean.setMobean(bean);

                getBrowserController().reopenQuery();
                ((FBoningMOListBCtrl) getBrowserController()).setSelectedBean(browserBean);
            } else {
                getBrowserController().reopenQuery();
                ((FBoningMOListBCtrl) getBrowserController()).setSelectedBean(beanSelected);
            }

        } else {
            FBoningMOListBBean bbean = new FBoningMOListBBean();
            bbean.setMobean(bean);
            refresh(null, bbean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
    }

    /**
     * Gets the btnFinish.
     * 
     * 
     * @return the btnFinish.
     */
    public ToolBarBtnStdModel getBtnFinish() {
        if (this.btnFinish == null) {
            this.btnFinish = new ToolBarBtnStdModel();
        }
        return this.btnFinish;
    }

    /**
     * 
     * Gets the current bbean.
     * 
     * @return the current bbean.
     */
    public FBoningMOListBBean getCurrentBBean() {
        FBoningMOListBBean bbean = (FBoningMOListBBean) getBrowserController().getModel().getCurrentBean();
        return bbean;
    }

    /**
     * Gets the productionMOListCBS.
     * 
     * @category getter
     * @return the productionMOListCBS.
     */
    public FProductionMOListCBS getProductionMOListCBS() {
        return productionMOListCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }
        getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.FEATURE_WAY, this.getClass().getName());
        fireSharedContextSend(getSharedContext());
        if (getIdCtx().getWorkstation() == null || "".equals(getIdCtx().getWorkstation())) {
            fireSelfFeatureClosingRequired();
        } else {
            String workstationId = (String) getSharedContext().get(Domain.DOMAIN_THIS,
                    FProductionConstant.LOGICAL_WORKSTATION);
            getViewerCtrl().setFeatureCtrl(this); // MNT            
            // if the workstation context is initialize with another workstation, it's possible that we're working for
            // another workstation
            if (workstationId != null && !workstationId.isEmpty()) {
                getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.LOGICAL_WORKSTATION, workstationId);
                getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_INITIAL_SELECTON,
                        initialSelection(workstationId));
                fireSharedContextSend(getSharedContext());
                super.initialize();
                fireSharedContextSend(getSharedContext());
                ((FMOListSBean) getGeneralSelectionController().getModel().getBean()).setWorkstationId(workstationId);
                getView().setErrorPanelAlwaysVisible(false);
                getBrowserController().addTableRowChangeListener(this);

                setTitle(getModel().getTitle());

            } else {
                String currentWorkstationId = null;

                if (getGeneralSelectionController() != null && getGeneralSelectionController().getModel() != null
                        && getGeneralSelectionController().getModel().getBean() != null) {
                    currentWorkstationId = ((FMOListSBean) getGeneralSelectionController().getModel().getBean())
                            .getWorkstationId();
                }

                // we change the current workstation just one time (if null or empty)
                if (currentWorkstationId == null || currentWorkstationId.isEmpty()) {
                    currentWorkstationId = getIdCtx().getWorkstation();
                }

                getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.LOGICAL_WORKSTATION,
                        currentWorkstationId);
                getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_INITIAL_SELECTON,
                        initialSelection(currentWorkstationId));
                fireSharedContextSend(getSharedContext());
                super.initialize();
                fireSharedContextSend(getSharedContext());

                // maybe the logical workstation has changed (done during the initialize of VCtrl) :
                String scWs = (String) getSharedContext().get(Domain.DOMAIN_THIS,
                        FProductionConstant.LOGICAL_WORKSTATION);
                if (scWs != null && !scWs.isEmpty()) {
                    currentWorkstationId = scWs;
                }

                ((FMOListSBean) getGeneralSelectionController().getModel().getBean())
                        .setWorkstationId(currentWorkstationId);
                getView().setErrorPanelAlwaysVisible(false);
                getBrowserController().addTableRowChangeListener(this);
                setTitle(getModel().getTitle());
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanged(event=" + event + ")");
        }

        FBoningMOListBBean bean = (FBoningMOListBBean) event.getSelectedBean();
        manageBtnFinishResume(bean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanged(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        // Nothing to do

    }

    /**
     * Sets the btnFinish.
     * 
     * 
     * @param btnFinish btnFinish.
     */
    public void setBtnFinish(final ToolBarBtnStdModel btnFinish) {
        this.btnFinish = btnFinish;
    }

    /**
     * Sets the productionMOListCBS.
     * 
     * @category setter
     * @param productionMOListCBS productionMOListCBS.
     */
    public void setProductionMOListCBS(final FProductionMOListCBS productionMOListCBS) {
        this.productionMOListCBS = productionMOListCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sharedContextSend(final SharedContextEvent event) {
        super.sharedContextSend(event);
        if (event.getSource().equals(getViewerCtrl())) {
            String wsId = (String) event.getSharedContext().get(Domain.DOMAIN_THIS,
                    FProductionConstant.LOGICAL_WORKSTATION);

            if (wsId != null
                    && !wsId.isEmpty()
                    && getGeneralSelectionController().getModel().getBean() != null
                    && !wsId.equals(((FMOListSBean) getGeneralSelectionController().getModel().getBean())
                            .getWorkstationId())) {
                ((FMOListSBean) getGeneralSelectionController().getModel().getBean()).setWorkstationId(wsId);
                refresh((Selection) getGeneralSelectionController().getModel().getBean(), null);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lstBtn = new ArrayList<ToolBarBtnStdBaseModel>();

        // Functionalities
        ToolBarBtnStdModel logicalWorkstation = new ToolBarBtnStdModel();
        logicalWorkstation.setReference(AbstractFBoningMOListFCtrl.LOGICAL_WORKSTATION_BTN);
        logicalWorkstation.setIconURL("/images/vif/vif57/production/mo/logicalWS.png");
        logicalWorkstation.setText(I18nClientManager.translate(ProductionMo.T22289, false));
        logicalWorkstation.setFunctionality(true);
        lstBtn.add(logicalWorkstation);

        ToolBarBtnStdModel moFinishing = new ToolBarBtnStdModel();
        moFinishing.setReference(AbstractFBoningMOListFCtrl.MO_FINISH_BTN);
        moFinishing.setIconURL("/images/vif/vif57/production/mo/finish.png");
        moFinishing.setText(I18nClientManager.translate(ProductionMo.T29510, false));
        setBtnFinish(moFinishing);
        moFinishing.setFunctionality(true);
        lstBtn.add(moFinishing);

        lstBtn.add(getBtnOpenIncidentModel());
        lstBtn.add(getBtnPDFModel());

        if (USER_VIF.equals(getIdCtx().getLogin()) || USER_VIF_INTEG.equals(getIdCtx().getLogin())) {
            ToolBarBtnStdModel btnDebug = new ToolBarBtnStdModel();
            btnDebug.setReference(ButtonReference.DEBUG_REFERENCE.getReference());
            btnDebug.setText("Maintenance");
            btnDebug.setEnabled(true);
            btnDebug.setIconURL("");
            btnDebug.setFunctionality(true);
            lstBtn.add(btnDebug);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lstBtn;
    }

    /**
     * Gets the button open incident model.
     * 
     * @return the button open incident model
     */
    protected ToolBarBtnStdModel getBtnOpenIncidentModel() {
        if (btnOpenIncidentModel == null) {
            btnOpenIncidentModel = new ToolBarBtnStdModel();
            btnOpenIncidentModel.setReference(BTN_OPEN_INCIDENT);
            btnOpenIncidentModel.setIconURL("/images/vif/vif57/production/mo/img48x48/incidentAdd.png");
            btnOpenIncidentModel.setText(I18nClientManager.translate(ProductionMo.T32884, false));
            btnOpenIncidentModel.setFunctionality(true);
        }
        return btnOpenIncidentModel;
    }

    /**
     * Gets the btnPDFModel.
     * 
     * @category getter
     * @return the btnPDFModel.
     */
    protected ToolBarBtnStdModel getBtnPDFModel() {
        if (this.btnPDFModel == null) {
            this.btnPDFModel = new ToolBarBtnStdModel();
            this.btnPDFModel.setReference(BTN_PDF_REFERENCE);
            this.btnPDFModel.setText(I18nClientManager.translate(ProductionMo.T30838));
            this.btnPDFModel.setIconURL("/images/vif/vif57/production/mo/img48x48/process.png");
            this.btnPDFModel.setFunctionality(true);
        }
        return this.btnPDFModel;
    }

    /**
     * return the initial selection for molist.
     * 
     * @param workstationId ws
     * @return is
     */
    protected FMOListSBean initialSelection(final String workstationId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialSelection(workstationId=" + workstationId + ")");
        }

        FMOListSBean bean = new FMOListSBean();
        bean.setCompanyId(getModel().getIdentification().getCompany());
        bean.setEstablishmentId(getModel().getIdentification().getEstablishment());
        bean.setMoDate(new Date());
        bean.setAll(false);
        bean.setShowFinished(false);
        bean.setWorkstationId(workstationId);
        bean.setMoDate(new Date());

        if (this.getClass().getName().contains(FeaturesId.BONING_INPUT.getValue())) {
            bean.setActivityItemType(ActivityItemType.INPUT);
        } else if (this.getClass().getName().contains(FeaturesId.BONING_OUTPUT.getValue())) {
            bean.setActivityItemType(ActivityItemType.OUTPUT);
        }

        ArrayList<MOState> list = new ArrayList<MOState>();
        list.add(MOState.STARTED);
        bean.setMoStates(list);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialSelection(workstationId=" + workstationId + ")=" + bean);
        }
        return bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void openChildFeature(final String featureId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openChildFeature(featureId=" + featureId + ")");
        }

        if (featureId.equals("VIF.BONMSOCR")) {
            MOBean originMOBoningBean = ((FMOListVBean) getViewerController().getBean()).getMobean();
            getSharedContext().put(Domain.DOMAIN_THIS, FBoningMSOCreationBCtrl.ORIGIN_MO_BEAN, originMOBoningBean);
            fireSharedContextSend(getSharedContext());
        } else if (featureId.equals(NCConstants.FeatureName.FLOW_TOUCH.getName())
                || featureId.equals(NCConstants.FeatureName.FLOW.getName())//
                || featureId.equals("VIF.BONIN") || featureId.equals("VIF.BONOUT")) {
            // Non conformity
            FMOListVBean vBean = (FMOListVBean) getViewerController().getBean();
            FBoningMOListBBean bBean = (FBoningMOListBBean) getBrowserController().getModel().getCurrentBean();
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_LIST_VBEAN, vBean);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_LIST_BBEAN, bBean);
            fireSharedContextSend(getSharedContext());
        }
        super.openChildFeature(featureId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openChildFeature(featureId=" + featureId + ")");
        }
    }

    /**
     * 
     * refreshes the browser.
     * 
     * @param selection selection. if null the current slection is re-used.
     * @param bean for repositionning (not mandatory)
     */
    protected void refresh(final Selection selection, final FBoningMOListBBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refresh(selection=" + selection + ", bean=" + bean + ")");
        }

        FBoningMOListBBean lbean = bean;
        // if (lbean == null) {
        // lbean = (FBoningMOListBBean) getBrowserController().getModel().getCurrentBean();
        // }
        if (selection == null) {
            getBrowserController().reopenQuery();
        } else {
            getBrowserController().reopenQuery(selection);
        }
        if (bean != null) {
            ((FBoningMOListBCtrl) getBrowserController()).setSelectedBean(lbean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - refresh(selection=" + selection + ", bean=" + bean + ")");
        }
    }

    /**
     * View document list.
     * 
     * @param listPDFFiles the list pdf files
     */
    protected void viewDocumentList(final List<PDFFile> listPDFFiles) {
        final DisplayListener dispListener = new DisplayListener() {
            @Override
            public void displayRequested(final DisplayEvent event) {
                event.getViewToDisplay().showView();
            }
        };

        final DialogChangeListener dialogListener = new DialogChangeListener() {
            @Override
            public void dialogCancelled(final DialogChangeEvent event) {
                // Nothing to do
            }

            @Override
            public void dialogValidated(final DialogChangeEvent event) {

            }
        };

        final List<ToolBarBtnStdView> list = new ArrayList<ToolBarBtnStdView>();

        for (PDFFile pdfFile : listPDFFiles) {
            TouchToolBarBtnStd btn = new TouchToolBarBtnStd();
            ToolBarBtnStdModel model = new ToolBarBtnStdModel();
            model.setReference(BTN_PDF_LIST_REFERENCE);
            model.setLinkedObject(pdfFile);
            model.setText(pdfFile.getTitle());
            btn.setModel(model);

            list.add(btn);
            ToolBarButtonController ctrl = new ToolBarButtonController();
            ctrl.setView(btn);

            btn.setBtnCtrl(ctrl);
            btn.getBtnCtrl().addButtonListener(this);
        }

        final TouchFunctionalitiesBtnDView dView = new TouchFunctionalitiesBtnDView(
                (Frame) ((JPanel) getView()).getTopLevelAncestor(), list);
        dView.setTitle(I18nClientManager.translate(Jtech.T22109));
        try {
            DialogController controller = null;
            controller = StandardControllerFactory.getInstance().getController(
                    ToolBarFunctionalitiesBtnDController.class);

            controller.setDialogView(dView);
            controller.addDialogChangeListener(dialogListener);
            controller.addDisplayListener(dispListener);
            controller.initialize();
        } catch (final UIException e) {
            LOGGER.error("Error instanciating Dialog :", e);
        }

    }

    /**
     * 
     * view the recette document.
     * 
     * @param pdfFile the file
     */
    protected void viewDocumentRecette(final PDFFile pdfFile) {
        if (pdfFile != null && !"".equals(pdfFile.getPath())) {
            viewDocument(pdfFile.getPath(), pdfFile.getTitle());
        } else {
            getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                    I18nClientManager.translate(ProductionMo.T30839));
        }
    }

    /**
     * Get documents to show.
     * 
     * @return the list of pdf files
     * @throws BusinessException if error occurs
     */
    private List<PDFFile> getDocumentsToShow() throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDocumentsToShow()");
        }

        MOItemKey moItemKey = new MOItemKey();

        moItemKey.setEstablishmentKey(getViewerCtrl().getBean().getMobean().getMoKey().getEstablishmentKey());
        moItemKey.setChrono(getViewerCtrl().getBean().getMobean().getMoKey().getChrono());
        moItemKey.setCounter1(0);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDocumentToShow()");
        }
        return getProductionMOListCBS().getPdfFilesOfCurrentItem(getIdCtx(), moItemKey);
    }

    /**
     * Gets the viewer ctrl.
     * 
     * @return the FProductionMOListVCtrl
     */
    private FBoningMOListVCtrl getViewerCtrl() {
        return (FBoningMOListVCtrl) getViewerController();
    }

    /**
     * Manage finish resume button.
     * 
     * @param bbean current browser bean
     */
    private void manageBtnFinishResume(final FBoningMOListBBean bbean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageBtnFinishResume(bbean=" + bbean + ")");
        }

        if (bbean != null) {
            if (bbean.getIsFinished()) {
                getBtnFinish().setIconURL("/images/vif/vif57/production/mo/resume.png");
                getBtnFinish().setText(I18nClientManager.translate(ProductionMo.T29502, false));
            } else {
                getBtnFinish().setIconURL("/images/vif/vif57/production/mo/finish.png");
                getBtnFinish().setText(I18nClientManager.translate(ProductionMo.T29510, false));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageBtnFinishResume(bbean=" + bbean + ")");
        }
    }

    /**
     * 
     * View a document file.
     * 
     * @param url url file
     * @param title title
     */
    private void viewDocument(final String url, final String title) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - viewDocument(url=" + url + ", title=" + title + ")");
        }

        TouchPDFViewerModel model = new TouchPDFViewerModel();
        model.setTitle(title);
        model.setFilePath(url);
        TouchPDFViewerView view = new TouchPDFViewerView();
        view.setModel(model);
        TouchPDFViewerCtrl controller = new TouchPDFViewerCtrl();
        controller.setDialogView(view);
        controller.initialize();
        view.showView();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - viewDocument(url=" + url + ", title=" + title + ")");
        }
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: TableLookActionModel.java,v $
 * Created on 5 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.importexport;


import fr.vif.jtech.ui.input.InputRadioSetModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.importexport.FTableLookImportExportBean;


/**
 * Model of the radio button in DTableLookImportExportDView.
 * 
 * @author kl
 */
public class TableLookActionModel extends InputRadioSetModel {

    /**
     * Superclass constructor.
     */
    public TableLookActionModel() {
        super();
        this.setEnumClass(FTableLookImportExportBean.ImportExport.class);
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeFCtrl.java,v $
 * Created on 8 janv. 2014 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeVBean;
import fr.vif.vif5_7.production.mo.business.services.features.flaborstaff.FLaborStaffCBS;
import fr.vif.vif5_7.production.mo.business.services.features.flabortime.FLaborTimeCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.touch.FLaborTimeFView;


/**
 * Labor time feature controller.
 * 
 * @author nle
 */
public class FLaborTimeFCtrl extends StandardFeatureController implements TableRowChangeListener {
    /**
     * list of btns.
     */
    public static final String  DELETELAST     = "deleteLastBtn";
    public static final String  DELETECURRENT  = "deleteCurrentBtn";
    public static final String  VALID_STAFF    = "validStaffBtn";
    public static final String  ADD            = "addBtn";
    public static final String  MODIF          = "modifBtn";

    private static final Logger LOGGER         = Logger.getLogger(FLaborTimeFCtrl.class);

    private FLaborStaffCBS      flaborstaffCBS = null;
    private FLaborTimeCBS       flabortimeCBS  = null;

    private FMOListVBean        moListVBean    = null;
    private FMOListBBean        moListBBean    = null;

    private int                 currentRow     = 0;

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        if (VALID_STAFF.equals(event.getModel().getReference())) {
            getViewerCtrl().beanUpdated(getViewerCtrl().getBean());
        } else if (DELETELAST.equals(event.getModel().getReference())) {
            // Are you sure you want to cancel the last data entry ?
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29578, false), MessageButtons.YES_NO);

            if (ret == MessageButtons.YES) {
                FLaborTimeBBean laborTimeBBean = (FLaborTimeBBean) getBrowserController().getModel().getCurrentBean();

                if (laborTimeBBean != null) {
                    laborTimeBBean.getLaborTimeKey().setNi1(0); // in order to get all the labor time keys

                    try {
                        FLaborTimeVBean laborTimeVBean = getFlabortimeCBS().getVBean(getIdCtx(), laborTimeBBean);
                        if (laborTimeVBean != null) {
                            int lastNi1 = 0;
                            for (FLaborStaffBBean lsbBean : laborTimeVBean.getLaborStaffList()) {
                                int ni1 = lsbBean.getLaborStaff().getLaborStaffKey().getLaborTimeKey().getNi1();
                                if (ni1 > lastNi1) {
                                    lastNi1 = ni1;
                                }
                            }

                            if (lastNi1 > 0) {
                                laborTimeBBean.getLaborTimeKey().setNi1(lastNi1); // re-uses this bean with the last ni1
                                FLaborTimeBBean lastBBean = getFlabortimeCBS().getLaborTimeBBean(getIdCtx(),
                                        laborTimeBBean.getLaborTimeKey());
                                try {
                                    getFlabortimeCBS().deleteLaborTimeBean(getIdCtx(), lastBBean, true);
                                } catch (BusinessErrorsException e) {
                                    LOGGER.error("", e);
                                }
                            }

                            getBrowserCtrl().reopenQuery();
                        }

                    } catch (BusinessException e) {
                        getView().show(new UIException(e));
                        LOGGER.error("", e);
                    }

                }
            }
        } else if (DELETECURRENT.equals(event.getModel().getReference())) {
            // Are you sure you want to cancel the current data entry ?
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29578, false), MessageButtons.YES_NO);

            if (ret == MessageButtons.YES) {
                FLaborTimeBBean laborTimeBBean = (FLaborTimeBBean) getBrowserController().getModel().getCurrentBean();
                try {
                    getFlabortimeCBS().deleteLaborTimeBean(getIdCtx(), laborTimeBBean, false);
                } catch (BusinessException e) {
                    LOGGER.error("", e);
                } catch (BusinessErrorsException e) {
                    LOGGER.error("", e);
                }
                getBrowserCtrl().reopenQuery();
            }
        } else if (ADD.equals(event.getModel().getReference())) {
            getSharedContext().put(Domain.DOMAIN_THIS, "MODE", ADD);
            fireSharedContextSend(getSharedContext());

            super.buttonAction(event);

        } else if (MODIF.equals(event.getModel().getReference())) {
            getSharedContext().put(Domain.DOMAIN_THIS, "MODE", MODIF);
            fireSharedContextSend(getSharedContext());

            super.buttonAction(event);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void childFeatureClosed(final FeatureController childFeatureController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }

        super.childFeatureClosed(childFeatureController);

        getBrowserCtrl().reopenQuery();

        try {
            getBrowserCtrl().changeCurrentRow(currentRow);
        } catch (UIVetoException e) {
            LOGGER.error("", e);
        }

        getSharedContext().remove(Domain.DOMAIN_CHILD, FProductionConstant.FEATURE_ID);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }
    }

    /**
     * Gets the flaborstaffCBS.
     * 
     * @return the flaborstaffCBS.
     */
    public FLaborStaffCBS getFlaborstaffCBS() {
        return flaborstaffCBS;
    }

    /**
     * Gets the flabortimeCBS.
     * 
     * @return the flabortimeCBS.
     */
    public FLaborTimeCBS getFlabortimeCBS() {
        return flabortimeCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        fireSharedContextSend(getSharedContext());

        super.initialize();

        getView().setErrorPanelAlwaysVisible(true);

        final Date datprod = (Date) getSharedContext().get(Domain.DOMAIN_PARENT, "DATPROD");
        final String line = (String) getSharedContext().get(Domain.DOMAIN_PARENT, "LINE");

        String subtitle;
        try {
            subtitle = I18nClientManager.translate(GenKernel.T31143, false)
                    + " "
                    + DateHelper.formatDate(datprod, "dd/MM/yyyy")
                    + " - "
                    + line
                    + " - "
                    + getFlabortimeCBS().getResourceLabel(getIdCtx(),
                            new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()), line);
            getFeatureView().setSubTitle(subtitle);
        } catch (BusinessException e) {
            LOGGER.error("", e);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {

    }

    /**
     * Sets the flaborstaffCBS.
     * 
     * @param flaborstaffCBS flaborstaffCBS.
     */
    public void setFlaborstaffCBS(final FLaborStaffCBS flaborstaffCBS) {
        this.flaborstaffCBS = flaborstaffCBS;
    }

    /**
     * Sets the flabortimeCBS.
     * 
     * @param flabortimeCBS flabortimeCBS.
     */
    public void setFlabortimeCBS(final FLaborTimeCBS flabortimeCBS) {
        this.flabortimeCBS = flabortimeCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        List<ToolBarBtnStdBaseModel> lstBtn = super.getActionsButtonModels();

        ToolBarBtnFeatureModel validStaff = new ToolBarBtnFeatureModel();
        validStaff.setReference(VALID_STAFF);
        validStaff.setIconURL("/images/vif/vif57/production/mo/ok.png");
        validStaff.setText(I18nClientManager.translate(ProductionMo.T29582, false));
        lstBtn.add(validStaff);

        ToolBarBtnFeatureModel modif = new ToolBarBtnFeatureModel();
        modif.setReference(MODIF);
        modif.setIconURL("/images/vif/vif57/production/mo/displayMo.png");
        modif.setFeatureId("VIF.LABTIMEMOD");
        modif.setText(I18nClientManager.translate(ProductionMo.T35072, false));
        lstBtn.add(modif);

        ToolBarBtnFeatureModel add = new ToolBarBtnFeatureModel();
        add.setReference(ADD);
        add.setIconURL("/images/vif/vif57/production/mo/newMO.png");
        add.setFeatureId("VIF.LABTIMEMOD");
        add.setText(I18nClientManager.translate(ProductionMo.T35071, false));
        lstBtn.add(add);

        // ToolBarBtnFeatureModel deleteCurrent = new ToolBarBtnFeatureModel();
        // deleteCurrent.setText("Supprimer période");
        // deleteCurrent.setReference(DELETECURRENT);
        // deleteCurrent.setIconURL("/images/vif/vif57/production/mo/cancel.png");
        // lstBtn.add(deleteCurrent);

        ToolBarBtnFeatureModel deleteLast = new ToolBarBtnFeatureModel();
        deleteLast.setText(I18nClientManager.translate(ProductionMo.T34767, false));
        deleteLast.setReference(DELETELAST);
        deleteLast.setIconURL("/images/vif/vif57/production/mo/cancel.png");
        lstBtn.add(deleteLast);

        return lstBtn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void openChildFeature(final String featureId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openChildFeature(featureId=" + featureId + ")");
        }

        if (featureId.equals("VIF.LABTIMEMOD")) {
            Date datprod = (Date) getSharedContext().get(Domain.DOMAIN_PARENT, "DATPROD");
            String cres = (String) getSharedContext().get(Domain.DOMAIN_PARENT, "LINE");

            FLaborTimeBBean laborTimeBBean = (FLaborTimeBBean) getBrowserController().getModel().getCurrentBean();

            if (laborTimeBBean != null) {
                int currentNi1 = laborTimeBBean.getLaborTimeKey().getNi1(); // current ni1
                currentRow = getBrowserController().getModel().getCurrentRowNumber();
                int prevNi1 = 0;
                int nextNi1 = 0;

                if (currentNi1 != 0) {
                    LaborTimeKey laborTimeKey = laborTimeBBean.getLaborTimeKey();
                    laborTimeKey.setNi1(0); // in order to get all the labor time keys

                    List<FLaborTimeBBean> laborTimeBBean2;
                    try {
                        laborTimeBBean2 = getFlabortimeCBS().queryElements(getIdCtx(), laborTimeKey, 0, 99999);
                        if (laborTimeBBean2 != null) {
                            for (FLaborTimeBBean ltbBean : laborTimeBBean2) {
                                if (ltbBean.getLaborTimeKey().getNi1() == currentNi1) {
                                    prevNi1 = currentNi1 - 1;
                                    nextNi1 = currentNi1 + 1;
                                    break;
                                }
                            }

                            getSharedContext().put(Domain.DOMAIN_THIS, "CSOC", laborTimeKey.getEstKey().getCsoc());
                            getSharedContext().put(Domain.DOMAIN_THIS, "CETAB", laborTimeKey.getEstKey().getCetab());
                            getSharedContext().put(Domain.DOMAIN_THIS, "DATPROD", datprod);
                            getSharedContext().put(Domain.DOMAIN_THIS, "LINE", cres);
                            getSharedContext().put(Domain.DOMAIN_THIS, "NI1", currentNi1);

                            // CURRENT BEGIN
                            getSharedContext().put(Domain.DOMAIN_THIS, "CURRENTBEGINDATEHOUR",
                                    laborTimeBBean.getBegDateHour());

                            // CURRENT END
                            getSharedContext().put(Domain.DOMAIN_THIS, "CURRENTENDDATEHOUR",
                                    laborTimeBBean.getEndDateHour());

                            // PREVIOUS END
                            if (prevNi1 == 0) {
                                getSharedContext().put(Domain.DOMAIN_THIS, "PREVENDDATEHOUR", null);
                            } else {
                                laborTimeKey.setNi1(prevNi1); // should exist
                                FLaborTimeBBean bBean = getFlabortimeCBS().getLaborTimeBBean(getIdCtx(), laborTimeKey);
                                if (bBean == null) {
                                    getSharedContext().put(Domain.DOMAIN_THIS, "PREVENDDATEHOUR", null);
                                } else {
                                    getSharedContext().put(Domain.DOMAIN_THIS, "PREVENDDATEHOUR",
                                            bBean.getEndDateHour());
                                }
                            }

                            // NEXT BEGIN
                            if (nextNi1 == 0) {
                                getSharedContext().put(Domain.DOMAIN_THIS, "NEXTBEGINDATEHOUR", null);
                            } else {
                                Boolean found = false;
                                for (FLaborTimeBBean ltbBean : laborTimeBBean2) {
                                    if (ltbBean.getLaborTimeKey().getNi1() == nextNi1) {
                                        getSharedContext().put(Domain.DOMAIN_THIS, "NEXTBEGINDATEHOUR",
                                                ltbBean.getBegDateHour());
                                        found = true;
                                        break;
                                    }
                                }
                                if (!found) {
                                    getSharedContext().put(Domain.DOMAIN_THIS, "NEXTBEGINDATEHOUR", null);
                                }
                            }

                            fireSharedContextSend(getSharedContext());
                        }

                    } catch (BusinessException e) {
                        getView().show(new UIException(e));
                        LOGGER.error("", e);
                    }
                }
            }
        }

        super.openChildFeature(featureId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openChildFeature(featureId=" + featureId + ")");
        }
    }

    /**
     * Get the browser controller.
     * 
     * @return the browser controller
     */
    private FLaborTimeBCtrl getBrowserCtrl() {
        return (FLaborTimeBCtrl) getBrowserController();

    }

    // }
    /**
     * Get the feature view.
     * 
     * @return the feature view
     */
    private FLaborTimeFView getFeatureView() {
        return (FLaborTimeFView) getView();

    }

    /**
     * Get the viewer controller.
     * 
     * @return the viewer controller
     */
    private FLaborTimeVCtrl getViewerCtrl() {
        return (FLaborTimeVCtrl) getViewerController();

    }

}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOView.java,v $
 * Created on 26 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;

import org.apache.log4j.Logger;

import fr.vif.vif5_7.production.mo.constants.GanttValueTypeEnum;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOPeriod;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.IGraphMOView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.AbstractIndicatorMOViewerView;


/**
 * Viewer containing a Chartpanel.
 * 
 * @author jd
 * @param <SRC> Bean source
 */
public class FGanttMOView<SRC> extends AbstractIndicatorMOViewerView<SRC> implements IGraphMOView, IGanttMoView<SRC> {
    /** LOGGER. */
    private static final Logger      LOGGER     = Logger.getLogger(FGanttMOView.class);

    private FGanttMOChartPanel       chartPanel = null;

    private List<GanttValueTypeEnum> toShow     = new ArrayList<GanttValueTypeEnum>();

    /**
     * Constructor.
     */
    public FGanttMOView() {

        GridBagLayout gridBagLayout = new GridBagLayout();
        setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weighty = 1.0;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(getChartPanel(), gbc);
    }

    /**
     * Add a menu to the default popup menu.
     * 
     * @param popToAdd menu to add.
     */
    public void addPopupMenu(final JMenu popToAdd) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - addPopupMenu(popToAdd=" + popToAdd + ")");
        }

        ((IGraphMOView) getChartPanel()).addPopupMenu(popToAdd);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - addPopupMenu(popToAdd=" + popToAdd + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public GraphMOPeriod<SRC> getLastClickedGraphPeriod() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLastClickedGraphPeriod()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getLastClickedGraphPeriod()");
        }
        return ((IGanttMoView) getChartPanel()).getLastClickedGraphPeriod();
    }

    /**
     * {@inheritDoc}
     */
    public void setGraph(final GraphMO graph) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setGraph(graph=" + graph + ")");
        }

        setGraph(graph, new GraphMOOptions());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setGraph(graph=" + graph + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setGraph(final GraphMO graph, final GraphMOOptions options) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setGraph(graph=" + graph + ", options=" + options + ")");
        }

        /* using of setGraph method of FGanttMOChartPanel */
        ((IGanttMoView) getChartPanel()).setGraph(graph, options);

        // Refresh the graph viewer
        getChartPanel().validate();
        validate();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setGraph(graph=" + graph + ", options=" + options + ")");
        }
    }

    /**
     * Set list of {@link IndicatorValueTypeEnum} to show.
     * 
     * @param toShow list of annotations to show.
     */
    public void setToShow(final List<GanttValueTypeEnum> toShow) {
        this.toShow = toShow;
    }

    /**
     * Get Chart Panel.
     * 
     * @return a {@link Component}
     */
    private Component getChartPanel() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getChartPanel()");
        }

        if (chartPanel == null) {
            chartPanel = new FGanttMOChartPanel();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getChartPanel()=" + chartPanel);
        }
        return chartPanel;
    }

}

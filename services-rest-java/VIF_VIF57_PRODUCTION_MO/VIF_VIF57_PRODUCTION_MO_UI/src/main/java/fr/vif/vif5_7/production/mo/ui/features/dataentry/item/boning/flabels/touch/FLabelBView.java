/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLabelBView.java,v $
 * Created on 17 Jun 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.flabels.touch;


import fr.vif.jtech.ui.browser.BrowserActionListener;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.flabels.FLabelBBean;


/**
 * Label browser view.
 * 
 * @author xg
 */
public class FLabelBView extends TouchBrowser<FLabelBBean> {

    /**
     * Default constructor.
     */
    public FLabelBView() {
        super();
        setColumns(4);
    }

    /**
     * register Action Listener to the browser.
     * 
     * @param listener listener
     */
    public void registerActionListener(final BrowserActionListener listener) {
        addActionListener(listener);
    }

}

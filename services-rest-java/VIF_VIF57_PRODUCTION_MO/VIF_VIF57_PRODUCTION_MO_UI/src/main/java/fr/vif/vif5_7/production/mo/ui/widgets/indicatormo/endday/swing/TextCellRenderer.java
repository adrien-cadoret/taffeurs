/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: TextCellRenderer.java,v $
 * Created on 22 Avril 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.swing;


import java.awt.Component;
import java.text.SimpleDateFormat;

import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import com.jidesoft.swing.StyledLabel;
import com.jidesoft.swing.StyledLabelBuilder;

import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.InvoicedArticleBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedOnLineControlBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedTrsLineBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedWorkingPeriodBean;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.AbstractEndDayIndicatorTableModel;


/**
 * TextCell renderer for JTable.
 *
 * @author ac
 */
public class TextCellRenderer extends DefaultTableCellRenderer {

    private CellInfoEnum info;

    /**
     * Default constructor.
     * 
     * @param info the displayed information.
     */
    public TextCellRenderer(final CellInfoEnum info) {
        super();
        setInfo(info);
    }

    /**
     * Gets the info.
     *
     * @return the info.
     */
    public CellInfoEnum getInfo() {
        return info;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        // Retrieve the bean to be displayed on this row
        EndDayMOBean bean = ((AbstractEndDayIndicatorTableModel) table.getModel()).getEndDayMOList().get(row);
        StyledLabel label = new StyledLabel();

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");

        if (bean instanceof InvoicedArticleBean) {
            switch (getInfo()) {
                case WORKSTATION:
                    label = new StyledLabelBuilder().add(((InvoicedArticleBean) bean).getClPoste().getCode())
                            .createLabel();
                    break;
                case CART:
                    label = new StyledLabelBuilder().add(((InvoicedArticleBean) bean).getClArt().getCode())
                            .createLabel();
                    break;
                case TYPACTA:
                    String typacta = ((InvoicedArticleBean) bean).getTypacta();
                    switch (typacta) {
                        case "ENT":
                            label = new StyledLabelBuilder().add("Entrant").createLabel();
                            break;
                        case "SOR":
                            label = new StyledLabelBuilder().add("Sortant").createLabel();

                        default:
                            break;
                    }

                    break;
                case LINE:
                    label = new StyledLabelBuilder().add(bean.getLine()).createLabel();
                    break;
                case CHRONO:
                    label = new StyledLabelBuilder().add(String.valueOf(((InvoicedArticleBean) bean).getChrono()))
                            .createLabel();
                    break;

                case CHRONO_MO:
                    label = new StyledLabelBuilder().add(String.valueOf(((InvoicedArticleBean) bean).getChrono()))
                            .createLabel();
                    break;
                default:
                    break;
            }
        }
        if (bean instanceof UnclosedWorkingPeriodBean) {
            switch (getInfo()) {
                case BEGIN_DATE:
                    label = new StyledLabelBuilder().add(
                            String.valueOf(dateFormat.format(((UnclosedWorkingPeriodBean) bean).getDatheurDeb())))
                            .createLabel();
                    break;
                case LINE:
                    label = new StyledLabelBuilder().add(String.valueOf(((UnclosedWorkingPeriodBean) bean).getLine()))
                            .createLabel();
                    break;
                default:
                    break;
            }
        }
        if (bean instanceof UnclosedTrsLineBean) {
            switch (getInfo()) {
                case CHRONO_MO:
                    label = new StyledLabelBuilder().add(String.valueOf(((UnclosedTrsLineBean) bean).getChrono()))
                            .createLabel();
                    break;
                case BEGIN_DATE:
                    label = new StyledLabelBuilder().add(
                            String.valueOf(dateFormat.format(((UnclosedTrsLineBean) bean).getBegDate()))).createLabel();
                    break;
                case LINE:
                    label = new StyledLabelBuilder().add(String.valueOf(((UnclosedTrsLineBean) bean).getLine()))
                            .createLabel();
                    break;
                default:
                    break;
            }
        }
        if (bean instanceof UnclosedOnLineControlBean) {
            switch (getInfo()) {
                case CREATION_DATE:

                    label = new StyledLabelBuilder().add(
                            String.valueOf(dateFormat.format(((UnclosedOnLineControlBean) bean).getCreationDate())))
                            .createLabel();
                    break;
                case CART:
                    label = new StyledLabelBuilder().add(String.valueOf(((UnclosedOnLineControlBean) bean).getCart()))
                            .createLabel();
                    break;
                case CHRONO_MEASURE:
                    label = new StyledLabelBuilder().add(
                            String.valueOf(((UnclosedOnLineControlBean) bean).getChronom())).createLabel();
                    break;
                case CHRONO:
                    label = new StyledLabelBuilder().add(
                            String.valueOf(((UnclosedOnLineControlBean) bean).getChronof())).createLabel();
                    break;
                case WORKSTATION:
                    label = new StyledLabelBuilder()
                            .add(String.valueOf(((UnclosedOnLineControlBean) bean).getCposte())).createLabel();
                    break;
                case LINE:
                    label = new StyledLabelBuilder().add(String.valueOf(((UnclosedOnLineControlBean) bean).getLine()))
                            .createLabel();
                    break;
                default:
                    break;
            }
        }

        this.setOpaque(true);
        return label;
    }

    /**
     * Sets the info.
     *
     * @param info info.
     */
    public void setInfo(final CellInfoEnum info) {
        this.info = info;
    }

}

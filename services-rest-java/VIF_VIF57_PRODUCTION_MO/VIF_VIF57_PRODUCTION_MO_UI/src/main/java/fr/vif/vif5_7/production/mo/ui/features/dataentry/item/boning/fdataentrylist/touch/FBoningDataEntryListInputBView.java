/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputBView.java,v $
 * Created on 11 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist.touch;


import fr.vif.jtech.ui.browser.touch.TouchLineBrowser;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;


/**
 * ItemMovement : Browser View.
 * 
 * @author xg
 */
public class FBoningDataEntryListInputBView extends TouchLineBrowser<FBoningDataEntryListInputBBean> {

    /**
     * Default constr.
     */
    public FBoningDataEntryListInputBView() {
        super();
    }
}

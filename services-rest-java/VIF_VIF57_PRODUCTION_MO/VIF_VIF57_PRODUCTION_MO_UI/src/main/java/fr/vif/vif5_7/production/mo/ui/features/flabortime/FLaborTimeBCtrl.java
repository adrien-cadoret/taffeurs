/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeBCtrl.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.events.dialogs.selection.SelectionChangeEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeVBean;
import fr.vif.vif5_7.production.mo.business.services.features.flabortime.FLaborTimeCBS;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.touch.FLaborTimeBIView;


/**
 * Labor time browser controller.
 * 
 * @author nle
 */
public class FLaborTimeBCtrl extends AbstractBrowserController<FLaborTimeBBean> {
    private static final Logger LOGGER = Logger.getLogger(FLaborTimeBCtrl.class);

    private FLaborStaffBCtrl    bCtrl;

    /**
     * Labor Time feature CBS to query elements.
     */
    private FLaborTimeCBS       flabortimeCBS;

    /**
     * Gets the bCtrl.
     * 
     * @return the bCtrl.
     */
    public FLaborStaffBCtrl getbCtrl() {
        return bCtrl;
    }

    /**
     * Gets the flabortimeCBS.
     * 
     * @return the flabortimeCBS.
     */
    public FLaborTimeCBS getFlabortimeCBS() {
        return flabortimeCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();
        ((FLaborTimeBIView) getView()).initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectionChanged(final SelectionChangeEvent event) {
        FLaborTimeBBean bean = getModel().getCurrentBean();

        super.selectionChanged(event);

        if (bean != null) {
            setSelectedBean(bean);
        }
    }

    /**
     * Sets the bCtrl.
     * 
     * @param bCtrl bCtrl.
     */
    public void setbCtrl(final FLaborStaffBCtrl bCtrl) {
        this.bCtrl = bCtrl;
    }

    /**
     * Sets the flabortimeCBS.
     * 
     * @param flabortimeCBS flabortimeCBS.
     */
    public void setFlabortimeCBS(final FLaborTimeCBS flabortimeCBS) {
        this.flabortimeCBS = flabortimeCBS;
    }

    /**
     * select the row of the bean in the browser.
     * 
     * @param bean the bean
     */
    public void setSelectedBean(final FLaborTimeBBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setSelectedBean(bean=" + bean + ")");
        }

        try {
            for (int i = 0; i < getModel().getRowCount(); i++) {

                if (bean.getLaborTimeKey().equals(getModel().getBeanAt(i).getLaborTimeKey())) {
                    changeCurrentRow(i);
                    break;
                }
            }
        } catch (UIVetoException e) {
            getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setSelectedBean(bean=" + bean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FLaborTimeBBean convert(final Object bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }

        FLaborTimeBBean ltBean = new FLaborTimeBBean();

        if (bean instanceof FLaborTimeBBean) {
            ltBean = (FLaborTimeBBean) bean;
        } else if (bean instanceof FLaborTimeVBean) {
            ltBean = ((FLaborTimeVBean) bean).getLaborTimeBBean();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")=" + bean);
        }

        return ltBean;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    protected List<FLaborTimeBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FLaborTimeBBean> ltbbList = null;

        final Date datprod = (Date) getSharedContext().get(Domain.DOMAIN_PARENT, "DATPROD");
        final String line = (String) getSharedContext().get(Domain.DOMAIN_PARENT, "LINE");

        LaborTimeKey laborTimeKey = new LaborTimeKey();
        laborTimeKey.setEstKey(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()));
        laborTimeKey.setProductionDate(datprod);
        laborTimeKey.setResourceCode(line);

        try {
            ltbbList = getFlabortimeCBS().queryElements(getIdCtx(), laborTimeKey, startIndex, rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }
        if (ltbbList == null) {
            ltbbList = new ArrayList<FLaborTimeBBean>();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + ltbbList);
        }
        return ltbbList;
    }
}

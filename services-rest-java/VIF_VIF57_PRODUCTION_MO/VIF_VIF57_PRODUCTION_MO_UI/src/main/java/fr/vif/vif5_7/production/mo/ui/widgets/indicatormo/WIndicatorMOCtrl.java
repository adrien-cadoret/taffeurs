/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WIndicatorMOCtrl.java,v $
 * Created on 03 avril 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.ui.widget.WidgetActionEvent;
import fr.vif.jtech.ui.widget.WidgetController;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGraphMOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOGraphSelectionBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSBean;
import fr.vif.vif5_7.production.mo.business.services.features.indicator.ProductivityMOCBS;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.GraphTypeEnum;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.GraphMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;


/**
 * The widget controller class.
 * 
 * @author ag
 */
public class WIndicatorMOCtrl extends WidgetController implements ActionListener {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(WIndicatorMOCtrl.class);

    private ProductivityMOCBS   productivityCBS;

    private List<MOLocalFrame>  frames = new ArrayList<MOLocalFrame>();

    private MOLocalFrame        frame  = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(e=" + e + ")");
        }

        ((WIndicatorMOIView) getView()).setUniqueId(e.getActionCommand());
        // // ((WIndicatorView) getView()).getSettings().put("uniqueId", e.getActionCommand());
        // ((WIndicatorView) getView()).getSettings();
        // Map<String, String> map = ((WIndicatorView) getView()).getSettings();
        // map.put("uniqueId", e.getActionCommand());
        // ((WIndicatorView) getView()).setSettings(map);
        refreshWidget();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final WidgetActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);
        if (WidgetActionEvent.GENERIC_EVENT_PERFORMED.equals(event.getEventName())) {

            if (frames.size() == 0) {
                initialize();
            }
            refreshWidget();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * Gets the productivityCBS.
     * 
     * @return the productivityCBS.
     */
    public ProductivityMOCBS getProductivityCBS() {
        return productivityCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        if (frames.size() == 0) {
            super.initialize();

            // view.getSettings().

            FSupervisorMOSBean sBean = new FSupervisorMOSBean();

            // Selection date
            // FIXME
            // GregorianCalendar cal = new GregorianCalendar(2012, 2, 15);
            // GregorianCalendar cal = new GregorianCalendar(2012, 1, 16);
            // sBean.setCurrentDate(cal.getTime());
            sBean.setCurrentDate(DateHelper.getTodayTime().getTime());
            // Establishment
            sBean.setEstablishmentKey(new EstablishmentKey(getIdentification().getCompany(), getIdentification()
                    .getEstablishment()));

            // if (frames.size() == 0) {
            frames = WIndicatorMOTools.getFrames(getIdentification(), sBean);

            // }

            // Define Indicators linked to the Widget (key)
            ((WIndicatorMOIView) getView()).setIndicators(WIndicatorMOTools.getFrameByWidgetId(frames,
                    ((WIndicatorMOIView) getView()).getWidgetId()));

            // Create Indicators Menu
            ((WIndicatorMOIView) getView()).setIndicatorsMenu(this);
            // view.setIndicatorsMenu(WIndicatorTools.createOtherIndicatorsMenu(this, indicators,
            // view.getGadget().getKey()));
        }
        // Refresh View
        refreshWidget();
        ((WIndicatorMOIView) getView()).validate();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Sets the productivityCBS.
     * 
     * @param productivityCBS productivityCBS.
     */
    public void setProductivityCBS(final ProductivityMOCBS productivityCBS) {
        this.productivityCBS = productivityCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void refreshWidget() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refreshWidget()");
        }

        super.refreshWidget();
        // ((WIndicatorIView) getView()).initialize();

        // String uniqueId = ((WIndicatorIView) getView()).getIndicators().get(0).getCode();
        // ((WIndicatorIView) getView()).setUniqueId(uniqueId);

        frame = WIndicatorMOTools.getFrame(frames, ((WIndicatorMOIView) getView()).getUniqueId());
        // getModel().setBean(frame);

        if (frame != null) {

            ((WIndicatorMOIView) getView()).setFrame(frame);

            // ((WIndicatorIView) getView()).setFrame(WIndicatorTools.getFrame(frames, ((WIndicatorIView) getView())
            // .getUniqueId()));

            ((WIndicatorMOIView) getView()).setGraph(refreshGraph(frame));
        }
        // Refresh widget view
        ((WIndicatorMOIView) getView()).refresh();

        ((WIndicatorMOIView) getView()).validate();

        // if (indicators.size() > 0) {
        // String uniqueId = null;
        // if (view.getSettings().get("uniqueId") == null) {
        // uniqueId = indicators.get(0).getCode();
        // } else {
        // uniqueId = view.getSettings().get("uniqueId");
        // }
        // frame = WIndicatorTools.getFrame(frames, uniqueId);
        // }
        // if (frame != null) {
        //
        // if (frame.getSelection() instanceof IndicatorGraphSelectionBean) {
        // IndicatorGraphSelectionBean sel = (IndicatorGraphSelectionBean) frame.getSelection();
        //
        // FGraphBean vbean = null;
        // try {
        // vbean = getProductivityCBS().getGraphBean(getIdCtx(), sel);
        // } catch (BusinessException e) {
        // // LOGGER.error("", e);
        // }
        //
        // Graph graph = GraphTools.convertFGraphToGraph(frame.getUniqueId(), vbean, sel);
        // if (GraphTypeEnum.BAR_STACKED.equals(graph.getType())) {
        // graph.setShowLegend(true);
        // }
        // view.refresh();
        // getModel().setBean(graph);
        //
        // JPanel panel = GraphTools.createGraphPanel(graph);
        // // JPanel panel = WIndicatorTools.loadGraphPanel(view.getGadget().getKey());
        //
        // // frame.getFrameBean().get
        // view.getContentPane().removeAll();
        // view.getContentPane().add(panel);
        // view.validate();
        // }
        // // getModel().setBean(bean);
        // // setView(view);
        // } else {
        // view.showError(I18nClientManager.translate(Generic.T1401, false, new VarParamTranslation(view.getGadget()
        // .getKey())));
        // }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - refreshWidget()");
        }
    }

    /**
     * Refresh Grah.
     * 
     * @param localFrame MOLocalFrame
     * @return GraphMO
     */
    private GraphMO refreshGraph(final MOLocalFrame localFrame) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refreshGraph(localFrame=" + localFrame + ")");
        }

        GraphMO graph = null;

        if (localFrame.getSelection() instanceof FSupervisorMOGraphSelectionBean) {
            FSupervisorMOGraphSelectionBean sel = (FSupervisorMOGraphSelectionBean) localFrame.getSelection();

            FGraphMOBean vbean = null;
            try {
                vbean = getProductivityCBS().getGraphBean(getIdCtx(), sel);
            } catch (BusinessException e) {
                LOGGER.debug("E - refreshGraph(localFrame=" + localFrame + ")=" + graph + ",error=" + e.getMessage());
            }

            graph = GraphMOTools.convertFGraphToGraph(vbean, sel);
            if (GraphTypeEnum.BAR_STACKED.equals(graph.getType())) {
                graph.setShowLegend(true);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - refreshGraph(localFrame=" + localFrame + ")=" + graph);
        }
        return graph;
    }

}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOIVView.java,v $
 * Created on 20 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing;


import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.JPopupMenu;

import com.jidesoft.docking.DockingManager;

import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOIFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;


/**
 * Interface of the viewer view.
 * 
 * @author jd
 */
public interface FSupervisorMOIVView {

    /**
     * Ask a file name.
     * 
     * @param path repository to save to
     * @return a absolute file name.
     */
    @Deprecated
    public String askFileName(String path);

    /**
     * Ask a view name.
     * 
     * @return a view name.
     */
    public String askViewName();

    /**
     * Create a flying dock.
     * 
     * @param newFrame frame
     */
    public void createFloatingDock(AbstractMODockableView newFrame);

    /**
     * create a popup up menu.
     * 
     * @param listener who listen to events.
     * @param localFrames list of frames
     * @param views list of view.
     * 
     * @return a Popup Menu
     */
    public JPopupMenu createPopupMenu(final ActionListener listener, List<MOLocalFrame> localFrames, List<String> views);

    /**
     * destroy a frame.
     * 
     * @param localFrame frame to destroy.
     */
    public void destroy(MOLocalFrame localFrame);

    /**
     * {@inheritDoc}
     */
    public DockingManager getDockingManager();

    /**
     * open a view.
     * 
     * @param pathName repository
     * @param viewName name of the view to open
     */
    @Deprecated
    public void openView(String pathName, String viewName);

    /**
     * save environment with a name.
     * 
     * @param fileName name to use.
     */
    public void saveEnvironment(String fileName);

    /**
     * Set the list of frame to show.
     * 
     * @param path repository
     * @param frames list of frames.
     */
    public void setDockables(String path, List<FSupervisorMOIFrame> frames);

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DNumberKeyboardTouchView.java,v $
 * Created on 31 May 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.dialog.number.touch;


import java.awt.Dialog;
import java.awt.Frame;

import fr.vif.jtech.ui.input.kbdialog.touch.TouchNumKeyboardDialog;


/**
 * Dialog Number Keyboard view.
 * 
 * @author xg
 */
public class DNumberKeyboardTouchView extends TouchNumKeyboardDialog {

    /**
     * Constructor.
     * 
     * @param dialog dialog
     */
    public DNumberKeyboardTouchView(final Dialog dialog) {
        super(dialog, int.class, "######", 0, false);
    }

    /**
     * Constructor.
     * 
     * @param frame frame
     */
    public DNumberKeyboardTouchView(final Frame frame) {
        super(frame, int.class, "######", 0, false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize(final Class type, final String format, final Object value, final boolean passwordField) {
        super.initialize(type, format, value, passwordField);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {
        super.initializeView();
    }

}

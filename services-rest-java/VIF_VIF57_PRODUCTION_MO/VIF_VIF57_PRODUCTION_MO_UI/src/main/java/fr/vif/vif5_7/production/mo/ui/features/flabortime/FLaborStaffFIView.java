/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_ADDON_MONITORING_UI
 * File : $RCSfile: FLaborStaffFIView.java,v $
 * Created on 14 sept. 2011 by dd
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


/**
 * Qualitative Measure Feature view interface.
 * 
 * @author dd
 */
public interface FLaborStaffFIView {

    /**
     * Gets the staff bar chart.
     * 
     * @return the staff bar chart
     */
    public StaffChart getStaffChart();

    /**
     * Gets the time bar chart.
     * 
     * @return the time bar chart
     */
    public TimeChart getTimeChart();

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOTouchFView.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOVBean;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.FMarkingMOFIView;


/**
 * MarkingMO : Feature View.
 * 
 * @author lmo
 */
public class FMarkingMOTouchFView extends StandardTouchFeature<FMarkingMOBBean, FMarkingMOVBean> implements
        FMarkingMOFIView {

    private FMarkingMOTouchBView fMarkingMOBView = null;

    private FMarkingMOTouchVView fMarkingMOVView = null;
    private TitlePanel           titlePanel;

    /**
     * Default constructor.
     * 
     */
    public FMarkingMOTouchFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserView<FMarkingMOBBean> getBrowserView() {
        return getFMarkingMOBView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView<FMarkingMOVBean> getViewerView() {
        return getFMarkingMOVView();
    }

    /**
     * Gets the title panel.
     * 
     * @return a TitlePanel
     */
    protected TitlePanel getTitlePanel() {
        if (titlePanel == null) {
            titlePanel = new TitlePanel();
            titlePanel.setBorder(new MatteBorder(1, 0, 0, 0, Color.white));
            titlePanel.setTitle(I18nClientManager.translate(ProductionMo.T29509, false));
            Dimension d = new Dimension(870, 42);
            titlePanel.setPreferredSize(d);
            titlePanel.setMinimumSize(d);
            titlePanel.setMaximumSize(d);
        }
        return titlePanel;
    }

    /**
     * Initialisation of the browser feature.
     * 
     * @return FMarkingMOBView
     */
    private FMarkingMOTouchBView getFMarkingMOBView() {
        if (fMarkingMOBView == null) {
            fMarkingMOBView = new FMarkingMOTouchBView();
            fMarkingMOBView.setPreferredSize(new Dimension(500, 180));
            fMarkingMOBView.setMinimumSize(new Dimension(500, 180));
            fMarkingMOBView.setTitlePanel(getTitlePanel());

        }
        return fMarkingMOBView;
    }

    /**
     * Initialisation of the viewer feature.
     * 
     * @return FMarkingMOVView
     */
    private FMarkingMOTouchVView getFMarkingMOVView() {
        if (fMarkingMOVView == null) {
            fMarkingMOVView = new FMarkingMOTouchVView();
        }
        return fMarkingMOVView;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {

        setLayout(new GridBagLayout());
        GridBagConstraints vGridConstraints = new GridBagConstraints();
        vGridConstraints.gridx = 0;
        vGridConstraints.gridy = 0;
        vGridConstraints.weightx = 0;
        vGridConstraints.weighty = 0;
        vGridConstraints.fill = GridBagConstraints.BOTH;
        add(getTitlePanel());

        vGridConstraints = new GridBagConstraints();
        vGridConstraints.gridx = 0;
        vGridConstraints.gridy = 1;
        vGridConstraints.weightx = 1;
        vGridConstraints.weighty = 1;
        vGridConstraints.fill = GridBagConstraints.BOTH;
        add(getFMarkingMOBView(), vGridConstraints);

        vGridConstraints = new GridBagConstraints();
        vGridConstraints.gridx = 0;
        vGridConstraints.gridy = 2;
        vGridConstraints.weightx = 1;
        vGridConstraints.weighty = 0;
        vGridConstraints.fill = GridBagConstraints.BOTH;
        add(getFMarkingMOVView(), vGridConstraints);

        // this.setSize(new java.awt.Dimension(width, height));
        // this.setContentPane(...);
    }

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListBView.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.swing;


import fr.vif.jtech.ui.browser.swing.SwingBrowser;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;


/**
 * DataEntryList : Browser View.
 * 
 * @author vr
 */
public class FDataEntryListBView extends SwingBrowser<FDataEntryListBBean> {

    /**
     * Default constructor.
     *
     */
    public FDataEntryListBView() {
        super();
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownFView.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing;


import java.awt.BorderLayout;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.swing.StandardSwingFeature;
import fr.vif.jtech.ui.selection.GeneralSelectionView;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownVBean;


/**
 * TimeBreakDown : Feature View.
 * 
 * @author cj
 * @version $Revision: 1.4 $, $Date: 2016/06/15 12:50:09 $
 */
public class FTimeBreakDownFView extends StandardSwingFeature<FTimeBreakDownVBean, FTimeBreakDownVBean> {

    private FTimeBreakDownBView  ftimeBreakDownBView  = null;
    private FTimeBreakDownGSView ftimeBreakDownGSView = null;
    private FTimeBreakDownVView  ftimeBreakDownVView  = null;

    /**
     * Default constructor.
     * 
     */
    public FTimeBreakDownFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserView<FTimeBreakDownVBean> getBrowserView() {
        return getFtimeBreakDownBView();
    }

    /**
     * Initialisation of the browser feature.
     * 
     * @return FTimeBreakDownBView
     */
    public FTimeBreakDownBView getFtimeBreakDownBView() {
        if (ftimeBreakDownBView == null) {
            ftimeBreakDownBView = new FTimeBreakDownBView();
        }
        return ftimeBreakDownBView;
    }

    /**
     * Gets the ftimeBreakDownGSView.
     * 
     * @return the ftimeBreakDownGSView.
     */
    public FTimeBreakDownGSView getFtimeBreakDownGSView() {
        if (ftimeBreakDownGSView == null) {
            ftimeBreakDownGSView = new FTimeBreakDownGSView();
        }
        return ftimeBreakDownGSView;
    }

    /**
     * Initialisation of the viewer feature.
     * 
     * @return FTimeBreakDownVView
     */
    public FTimeBreakDownVView getFtimeBreakDownVView() {
        if (ftimeBreakDownVView == null) {
            ftimeBreakDownVView = new FTimeBreakDownVView();
        }
        return ftimeBreakDownVView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GeneralSelectionView getGeneralSelectionView() {
        return getFtimeBreakDownGSView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView<FTimeBreakDownVBean> getViewerView() {
        return getFtimeBreakDownVView();
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        super.initializeView();
        // Need to use border layout to avoid flashing
        this.setLayout(new BorderLayout());
        this.add(getFtimeBreakDownGSView(), BorderLayout.PAGE_START);
        this.add(getFtimeBreakDownVView(), BorderLayout.CENTER);

    }

}

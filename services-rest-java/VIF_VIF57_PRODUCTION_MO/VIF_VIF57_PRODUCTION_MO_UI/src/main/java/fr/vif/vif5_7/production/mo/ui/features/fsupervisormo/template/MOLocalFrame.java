/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: MOLocalFrame.java,v $
 * Created on 23 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.AbstractSupervisorMOSelectionBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSelectionBean;


/**
 * Contains information to create a new frame.
 * 
 * @author jd
 */
public class MOLocalFrame {

    private FSupervisorMOFrameBean            frameBean = new FSupervisorMOFrameBean();

    private AbstractSupervisorMOSelectionBean selection = new FSupervisorMOSelectionBean();

    /**
     * Unique id of the frame. Must be constant.
     */
    private String                            uniqueId  = "";

    /**
     * Widget ID.
     */
    private String                            widgetId  = "";

    /**
     * constructor.
     */
    public MOLocalFrame() {
        super();
    }

    /**
     * constructor.
     * 
     * @param frameBean {@link FSupervisorMOFrameBean}
     * @param selection selection
     */
    public MOLocalFrame(final FSupervisorMOFrameBean frameBean, final AbstractSupervisorMOSelectionBean selection) {
        super();
        this.frameBean = frameBean;
        this.selection = selection;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MOLocalFrame other = (MOLocalFrame) obj;
        if (uniqueId == null) {
            if (other.uniqueId != null) {
                return false;
            }
        } else if (!uniqueId.equals(other.uniqueId)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the frameBean.
     * 
     * @return the frameBean.
     */
    public FSupervisorMOFrameBean getFrameBean() {
        return frameBean;
    }

    /**
     * Gets the selection.
     * 
     * @return the selection.
     */
    public AbstractSupervisorMOSelectionBean getSelection() {
        return selection;
    }

    /**
     * Gets the uniqueId.
     * 
     * @return the uniqueId.
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * Gets the widgetId.
     * 
     * @return the widgetId.
     */
    public String getWidgetId() {
        return widgetId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uniqueId == null) ? 0 : uniqueId.hashCode());
        return result;
    }

    /**
     * Sets the frameBean.
     * 
     * @param frameBean frameBean.
     */
    public void setFrameBean(final FSupervisorMOFrameBean frameBean) {
        this.frameBean = frameBean;
    }

    /**
     * Sets the selection.
     * 
     * @param selection selection.
     */
    public void setSelection(final AbstractSupervisorMOSelectionBean selection) {
        this.selection = selection;
    }

    /**
     * Sets the uniqueId.
     * 
     * @param uniqueId uniqueId.
     */
    public void setUniqueId(final String uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * Sets the widgetId.
     * 
     * @param widgetId widgetId.
     */
    public void setWidgetId(final String widgetId) {
        this.widgetId = widgetId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "LocalFrame [uniqueId=" + uniqueId + "]";
    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: IEndDayEjbCallback.java,v $
 * Created on 22 Avril 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday;


import java.util.List;

import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.gen.location.business.beans.features.farea.FAreaBBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOTypeEnum;


/**
 * Ejb Callback for endDay indicators.
 *
 * @author ac
 */
public interface IEndDayEjbCallback {

    /**
     * Refresh the view with the returned data.
     * 
     * @param returnValue the data used by the view
     */
    public void refreshAreas(final List<FAreaBBean> returnValue);

    /**
     * 
     * Refresh the view with the returned data.
     * 
     * @param returnValue the data used by the view
     * @param indicatorType indicator
     */
    public void refreshView(final List<? extends EndDayMOBean> returnValue, EndDayMOTypeEnum indicatorType);

    /**
     * Show an error message according to an exception.
     * 
     * @param exception the exception to show
     */
    public void show(final UIException exception);

}

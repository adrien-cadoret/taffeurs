/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOItemListBCtrl.java,v $
 * Created on 12 oct. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmoitemlist;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmoitemlist.FMOItemListBBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.common.fmoitemlist.FMOItemListCBS;


/**
 * MoItemList : Browser Controller.
 * 
 * @author vr
 */
public class FMOItemListBCtrl extends AbstractBrowserController<FMOItemListBBean> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FMOItemListBCtrl.class);

    private FMOItemListCBS      fmoItemListCBS;

    /**
     * Default constructor.
     * 
     */
    public FMOItemListBCtrl() {
        super();
    }

    /**
     * Gets the fmoItemListCBS.
     * 
     * @category getter
     * @return the fmoItemListCBS.
     */
    public final FMOItemListCBS getFmoItemListCBS() {
        return fmoItemListCBS;
    }

    /**
     * Sets the fmoItemListCBS.
     * 
     * @category setter
     * @param fmoItemListCBS fmoItemListCBS.
     */
    public final void setFmoItemListCBS(final FMOItemListCBS fmoItemListCBS) {
        this.fmoItemListCBS = fmoItemListCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FMOItemListBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FMOItemListBBean> lst = null;
        try {
            lst = getFmoItemListCBS().queryElements(getIdCtx(),
                    new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()), selection);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return lst;
    }

}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersVModel.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersVBean;


/**
 * ProcessOrders : Viewer Model.
 * 
 * @author jd
 */
public class FProcessOrdersVModel extends ViewerModel<FProcessOrdersVBean> {

    /**
     * Default constructor.
     * 
     */
    public FProcessOrdersVModel() {
        super();
        setBeanClass(FProcessOrdersVBean.class);
        getUpdateActions().setCopyEnabled(false);
        getUpdateActions().setDeleteEnabled(false);
        getUpdateActions().setDocEnabled(false);
        getUpdateActions().setInfoEnabled(false);
        getUpdateActions().setNewEnabled(false);
        getUpdateActions().setPrintEnabled(false);
        getUpdateActions().setSaveEnabled(false);
        getUpdateActions().setUndoEnabled(false);
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FContentRateBCtrl.java,v $
 * Created on 15 Jul 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.events.dialogs.selection.SelectionChangeEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fcontent.FContentRateCBS;


/**
 * Browser controller for content feature.
 * 
 * @author cj
 */
public class FContentRateBCtrl extends AbstractBrowserController<FContentRateBBean> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FContentRateBCtrl.class);

    private FContentRateCBS     fcontentRateCBS;

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDefaultInitialSelection()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDefaultInitialSelection()");
        }
        return new FContentRateSBean();
    }

    /**
     * Gets the fcontentRateCBS.
     * 
     * @return the fcontentRateCBS.
     */
    public FContentRateCBS getFcontentRateCBS() {
        return fcontentRateCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reopenQuery() {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reopenQuery(final Object selectedBean) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void reopenQuery(final Selection selection) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectionChanged(final SelectionChangeEvent event) {
        // nothing to do
    }

    /**
     * Sets the fcontentRateCBS.
     * 
     * @param fcontentRateCBS fcontentRateCBS.
     */
    public void setFcontentRateCBS(final FContentRateCBS fcontentRateCBS) {
        this.fcontentRateCBS = fcontentRateCBS;
    }

    /**
     * update the beans to get the expected rate and quantity.
     * 
     * @param sBean the selection bean
     * @throws UIException if error occurs
     */
    public void updateBean(final FContentRateSBean sBean) throws UIException {
        List<FContentRateBBean> lst = new ArrayList<FContentRateBBean>();
        if (sBean != null && sBean.getMoKey() != null) {
            try {
                lst = getFcontentRateCBS().updateBrowserBean(getIdCtx(), sBean, getModel().getBeans());
                getModel().removeAllBeans();
                getModel().addNextBeans(lst);
            } catch (BusinessException e) {
                LOGGER.error("", e);
                throw new UIException(e);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FContentRateBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FContentRateBBean> lst = new ArrayList<FContentRateBBean>();
        if (getModel().getBeans() == null || getModel().getBeans().size() == 0) {
            FContentRateSBean sBean = (FContentRateSBean) selection;
            if (sBean != null && sBean.getMoKey() != null) {
                try {
                    lst = getFcontentRateCBS().queryElements(getIdCtx(), sBean);
                } catch (BusinessException e) {
                    LOGGER.error("", e);
                    throw new UIException(e);
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + lst);
        }
        return lst;
    }

    /**
     * Move a row in the browser.
     * 
     * @param currentRow initial position.
     * @param newRow new position.
     */
    protected void verticalMove(final int currentRow, final int newRow) {
        FContentRateBBean bean = getModel().getBeanAt(currentRow);
        getModel().deleteBeanAt(currentRow);
        getModel().insertBeanAt(bean, newRow);
        getModel().setCurrentRowNumber(newRow);
        selectedRowChanged(new BrowserActionEvent(this, BrowserActionEvent.EVENT_SELECTED_ROW_CHANGED, newRow));
    }

}

package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.actions;


/*
 * Copyright (c) 2015 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PREPARATION_KERNEL_UI
 * File : $RCSfile: CommonRefreshAction.java,v $
 * Created on 29 feb. 2016 by ac
 */

import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOIFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;


/**
 * One instance of this class is used to manage click on refresh button of the title bar.
 *
 * @author ac
 */
public final class CommonRefreshAction extends AbstractAction {

    private static final Icon   SMALL_ICON = FSupervisorMOTools.getIcon16(FSupervisorMOTools.IFRAME_REFRESH);

    private FSupervisorMOIFrame frame;

    /**
     * Constructor.
     * 
     * @param ctrl Controller of the associated IFrame.
     */
    public CommonRefreshAction(final FSupervisorMOIFrame ctrl) {
        this.frame = ctrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        frame.refresh();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getValue(final String key) {
        Object ret;

        if (key.equals(FSupervisorMOTools.SMALL_ICON_KEY)) {
            ret = SMALL_ICON;
        } else if (key.equals(FSupervisorMOTools.SHORT_DESCRIPTION_KEY)) {
            ret = I18nClientManager.translate(ProductionMo.T39319, false);

        } else if (key.equals(ACTION_COMMAND_KEY)) {
            ret = ""; //
        } else {
            ret = super.getValue(key);
        }

        return ret;
    }
}

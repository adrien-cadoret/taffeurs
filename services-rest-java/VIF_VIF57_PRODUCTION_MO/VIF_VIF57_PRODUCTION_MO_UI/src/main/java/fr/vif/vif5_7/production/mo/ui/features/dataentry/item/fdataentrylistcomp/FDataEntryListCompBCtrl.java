/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListCompBCtrl.java,v $
 * Created on 26 sept. 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylistcomp;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylistcomp.FDataEntryListCompSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fdataentrylistcomp.FDataEntryListCompCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListBCtrl;


/**
 * DataEntryCompList : Browser Model.
 * 
 * @author kl
 */
public class FDataEntryListCompBCtrl extends FDataEntryListBCtrl {
    /** LOGGER. */
    private static final Logger   LOGGER = Logger.getLogger(FDataEntryListCompBCtrl.class);

    private FDataEntryListCompCBS fdataEntryListCompCBS;

    /**
     * Gets the fdataEntryListCompCBS.
     *
     * @return the fdataEntryListCompCBS.
     */
    public FDataEntryListCompCBS getFdataEntryListCompCBS() {
        return fdataEntryListCompCBS;
    }

    /**
     * Sets the fdataEntryListCompCBS.
     *
     * @param fdataEntryListCompCBS fdataEntryListCompCBS.
     */
    public void setFdataEntryListCompCBS(final FDataEntryListCompCBS fdataEntryListCompCBS) {
        this.fdataEntryListCompCBS = fdataEntryListCompCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FDataEntryListBBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FDataEntryListBBean> lst = null;
        try {
            lst = getFdataEntryListCompCBS().queryElements(getIdCtx(), (FDataEntryListCompSBean) selection, startIndex,
                    rowNumber);
        } catch (BusinessException e) {
            LOGGER.error("queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ") - " + e);
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + lst == null ? "null" : lst.size() + " elements");
        }
        return lst;
    }

}

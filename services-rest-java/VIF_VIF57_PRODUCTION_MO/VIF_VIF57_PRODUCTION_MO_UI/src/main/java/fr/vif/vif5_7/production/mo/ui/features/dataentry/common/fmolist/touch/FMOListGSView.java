/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOListGSView.java,v $
 * Created on 14 nov. 08 by alb
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.touch;


import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.ImageIcon;

import fr.vif.jtech.ui.button.touch.TouchBtnStd;
import fr.vif.jtech.ui.button.touch.TouchToggleBtnStd;
import fr.vif.jtech.ui.input.InputFieldActionEvent;
import fr.vif.jtech.ui.input.InputFieldActionListener;
import fr.vif.jtech.ui.input.touch.TouchCompositeInputTextField;
import fr.vif.jtech.ui.input.touch.TouchInputCheckBox;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.selection.touch.StandardTouchGeneralSelection;
import fr.vif.jtech.ui.util.Key;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.activities.activities.ui.composites.cproductionresource.touch.CProductionResourceTouchView;
import fr.vif.vif5_7.activities.activities.ui.composites.cteam.touch.CTeamTouchView;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;


/**
 * MOList : General Selection View.
 * 
 * @author alb
 */
public class FMOListGSView extends StandardTouchGeneralSelection {
    /**
     * Show all button reference.
     */
    public static final String           BTN_SHOW_ALL_REFERENCE = ButtonReference.SHOW_ALL_REFERENCE_BTN.getReference();

    private static final int             DIFF                   = 4;
    private TouchInputCheckBox           touchInputCheckBox;
    private TouchToggleBtnStd            btnTglDisplayAll;
    private TouchInputTextField          itfMODate;
    private TouchBtnStd                  nextDateBtn;
    private TouchBtnStd                  previousDateBtn;
    private CTeamTouchView               teamId;
    private CProductionResourceTouchView cProductionResource;

    /**
     * Default constructor.
     * 
     */
    public FMOListGSView() {
        super();
        setLayout(null);
        initialize();
    }

    /**
     * set the selection value.
     * 
     */
    public void changedisplayAll() {
        getTouchInputCheckBox().setValue(btnTglDisplayAll.isSelected());
        InputFieldActionEvent event = new InputFieldActionEvent(getTouchInputCheckBox(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getTouchInputCheckBox());
        for (InputFieldActionListener listener : getTouchInputCheckBox().getInputFieldActionListeners()) {
            listener.valueChanged(event);
        }
        manageBtnDisplayAll();

    }

    /**
     * Gets the cProductionResource.
     * 
     * @return the cProductionResource.
     */
    public CProductionResourceTouchView getcProductionResource() {
        if (cProductionResource == null) {
            cProductionResource = new CProductionResourceTouchView();
            cProductionResource.setResettable(true);
            ((TouchInputTextField) cProductionResource.getCodeView()).getJtfValue().setMinimumSize(
                    new Dimension(180, 44));
            ((TouchInputTextField) cProductionResource.getCodeView()).setResetButtonSize(new Dimension(44, 44));
            cProductionResource.setBeanProperty("productionResourceCL");
            cProductionResource.setBounds(515, 3, 238, 105);
        }
        return cProductionResource;
    }

    /**
     * Gets the itfMODate.
     * 
     * @category getter
     * @return the itfMODate.
     */
    public final TouchInputTextField getItfMODate() {
        if (itfMODate == null) {
            itfMODate = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_DATE,
                    I18nClientManager.translate(ProductionMo.T34329, false));
            itfMODate.setBounds(82, 0, 170, 91);
            itfMODate.setBeanProperty("moDate");
            itfMODate.getJtfValue().setMaximumSize(new Dimension(170, 44));
            itfMODate.getJtfValue().setMinimumSize(new Dimension(170, 44));
            itfMODate.getJtfValue().setPreferredSize(new Dimension(170, 44));
            itfMODate.setDescription(I18nClientManager.translate(ProductionMo.T29584, false));
        }
        return itfMODate;
    }

    /**
     * Gets the nextDateBtn.
     * 
     * @category getter
     * @return the nextDateBtn.
     */
    public final TouchBtnStd getNextDateBtn() {
        if (nextDateBtn == null) {
            nextDateBtn = new TouchBtnStd();
            nextDateBtn.setBounds(257, 15, 70, 70);
            nextDateBtn.setPreferredSize(new Dimension(70, 70));
            nextDateBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayAfter.png")));
            nextDateBtn.setEnabled(true);
            nextDateBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    addDate();
                }

            });
        }
        return nextDateBtn;

    }

    /**
     * Gets the previousDateBtn.
     * 
     * @category getter
     * @return the previousDateBtn.
     */
    public final TouchBtnStd getPreviousDateBtn() {
        if (previousDateBtn == null) {
            previousDateBtn = new TouchBtnStd();
            previousDateBtn.setBounds(8, 15, 70, 70);
            previousDateBtn.setPreferredSize(new Dimension(70, 70));
            previousDateBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayBefore.png")));
            previousDateBtn.setEnabled(true);
            previousDateBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    subDate();
                }

            });

            previousDateBtn.addKeyListener(new KeyListener() {

                @Override
                public void keyPressed(final KeyEvent e) {
                }

                @Override
                public void keyReleased(final KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        subDate();
                    }
                }

                @Override
                public void keyTyped(final KeyEvent e) {
                }
            });
        }
        return previousDateBtn;

    }

    /**
     * Gets the teamId.
     * 
     * @category getter
     * @return the teamId.
     */
    public TouchCompositeInputTextField getTeamId() {
        if (teamId == null) {
            teamId = new CTeamTouchView();
            teamId.setBounds(332, 6, 180, 100);
            ((TouchInputTextField) teamId.getCodeView()).getJtfValue().setMinimumSize(new Dimension(180, 44));
            ((TouchInputTextField) teamId.getCodeView()).setResetButtonSize(new Dimension(44, 44));
            teamId.setBeanProperty("teamCL");
            teamId.setResettable(true);
        }
        return teamId;
    }

    /**
     * Sets the itfRecDate.
     * 
     * @category setter
     * @param itfMODate itfMODate.
     */
    public final void setItfMODate(final TouchInputTextField itfMODate) {
        this.itfMODate = itfMODate;
    }

    /**
     * Sets the nextDateBtn.
     * 
     * @category setter
     * @param nextDateBtn nextDateBtn.
     */
    public final void setNextDateBtn(final TouchBtnStd nextDateBtn) {
        this.nextDateBtn = nextDateBtn;
    }

    /**
     * Sets the previousDateBtn.
     * 
     * @category setter
     * @param previousDateBtn previousDateBtn.
     */
    public final void setPreviousDateBtn(final TouchBtnStd previousDateBtn) {
        this.previousDateBtn = previousDateBtn;
    }

    /**
     * Sets the teamId.
     * 
     * @category setter
     * @param teamId teamId.
     */
    public void setTeamId(final CTeamTouchView teamId) {
        this.teamId = teamId;
    }

    /**
     * Inform the listener than the value has changed.
     */
    public void valueChanged() {
        InputFieldActionEvent event = new InputFieldActionEvent(getItfMODate(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getItfMODate());
        for (InputFieldActionListener listener : getItfMODate().getInputFieldActionListeners()) {
            listener.valueChanged(event);
        }
    }

    /**
     * Add date.
     */
    private void addDate() {
        Date currentDate = (Date) getItfMODate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        getItfMODate().setValue(cal.getTime());
        valueChanged();
    }

    /**
     * set the selection value.
     * 
     * @param isSelected if selected
     */
    private void changedisplayAll(final boolean isSelected) {

        getTouchInputCheckBox().setValue(isSelected);
        InputFieldActionEvent event = new InputFieldActionEvent(getTouchInputCheckBox(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getTouchInputCheckBox());
        for (InputFieldActionListener listener : getTouchInputCheckBox().getInputFieldActionListeners()) {
            listener.valueChanged(event);
        }
        manageBtnDisplayAll();

    }

    /**
     * r.
     * 
     * @return t
     */
    private TouchToggleBtnStd getBtnTglDisplayAll() {
        if (btnTglDisplayAll == null) {
            btnTglDisplayAll = new TouchToggleBtnStd();

            btnTglDisplayAll.getBtnModel().setText(I18nClientManager.translate(ProductionMo.T29545, false));
            btnTglDisplayAll.setIcon(new ImageIcon(getClass().getResource(
                    "/images/vif/vif57/production/mo/showfinished32x23.png")));
            btnTglDisplayAll.setReference(BTN_SHOW_ALL_REFERENCE);
            btnTglDisplayAll.setShortcut(Key.K_F8);
            btnTglDisplayAll.setBounds(757, 15, 105, 70);
            Dimension d = new Dimension(130, 70);
            btnTglDisplayAll.setPreferredSize(d);
            btnTglDisplayAll.setMinimumSize(d);
            btnTglDisplayAll.setMaximumSize(d);

            btnTglDisplayAll.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(final ActionEvent e) {
                    changedisplayAll(btnTglDisplayAll.isSelected());
                }

            });
        }
        return btnTglDisplayAll;
    }

    /**
     * r.
     * 
     * @return t
     */
    private TouchInputCheckBox getTouchInputCheckBox() {
        if (touchInputCheckBox == null) {
            touchInputCheckBox = new TouchInputCheckBox();
            touchInputCheckBox.setBeanProperty("showFinished");
            touchInputCheckBox.setLabel(I18nClientManager.translate(Generic.T24773, false));

            Dimension d = new Dimension(0, 0);
            touchInputCheckBox.setPreferredSize(d);
            touchInputCheckBox.setMinimumSize(d);
            touchInputCheckBox.setMaximumSize(d);
        }
        return touchInputCheckBox;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        setOpaque(false);
        add(getItfMODate());
        add(getPreviousDateBtn());
        add(getNextDateBtn());
        add(getTeamId());
        add(getcProductionResource());
        add(getBtnTglDisplayAll());
    }

    /**
     * change the toggle state.
     */
    private void manageBtnDisplayAll() {
        if (getBtnTglDisplayAll().isSelected()) {
            getBtnTglDisplayAll().setIcon(
                    new ImageIcon(getClass().getResource("/images/vif/vif57/production/mo/hidefinished32x23.png")));
            getBtnTglDisplayAll().getBtnModel().setText(I18nClientManager.translate(ProductionMo.T29525, false));
        } else {
            getBtnTglDisplayAll().setIcon(
                    new ImageIcon(getClass().getResource("/images/vif/vif57/production/mo/showfinished32x23.png")));
            getBtnTglDisplayAll().getBtnModel().setText(I18nClientManager.translate(ProductionMo.T29545, false));
        }

    }

    /**
     * Sub date.
     */
    private void subDate() {
        Date currentDate = (Date) getItfMODate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        getItfMODate().setValue(cal.getTime());
        valueChanged();
    }
}

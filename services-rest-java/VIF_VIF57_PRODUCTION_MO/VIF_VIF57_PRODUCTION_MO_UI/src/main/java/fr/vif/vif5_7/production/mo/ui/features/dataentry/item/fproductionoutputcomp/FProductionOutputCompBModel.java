/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompBModel.java,v $
 * Created on 18 sept. 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninput.FProductionInputBBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelArray;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.CodeFieldModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerBModel;


/**
 * Production Output Scrap Browser Model.
 * 
 * @author kl
 */
public class FProductionOutputCompBModel extends FProductionOutputContainerBModel {
    private FProductionOutputCompBComparator comparator = new FProductionOutputCompBComparator();

    /**
     * Default constructor.
     * 
     */
    public FProductionOutputCompBModel() {
        super();
        this.setComparator(comparator);
        // QA8329 - use the standard touch model and not a special one by nature
        // manageColumnsByNature(MONatureComplementaryWorkshop.MANUFACTURING);
    }

    // /**
    // * {@inheritDoc}
    // */
    // @Override
    // public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
    // final FProductionBBean bean) {
    // Format ret = null;
    // Format cellFormat1 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
    // FontStyle.PLAIN, 14));
    // Format cellFormat2 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
    // FontStyle.BOLD, 14));
    //
    // if (property.equals(FProductionInputBBeanEnum.ITEM_SHORT_LABEL.getValue())) {
    // ret = cellFormat2;
    // if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
    // ret.setForegroundColor(StandardColor.GRAY);
    // } else {
    // ret.setForegroundColor(StandardColor.BLACK);
    // }
    // } else if (property.equals(FProductionInputBBeanEnum.ITEM_CODE.getValue())) {
    // ret = cellFormat1;
    // if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
    // ret.setForegroundColor(StandardColor.GRAY);
    // } else {
    // ret.setForegroundColor(StandardColor.BLACK);
    // }
    // } else if (property.equals(FProductionInputBBeanEnum.QTTY_DONE.getValue())
    // || property.equals(FProductionInputBBeanEnum.QTTY_TODO.getValue())
    // || property.equals(FProductionInputBBeanEnum.QTTY_UNIT.getValue())) {
    // ret = cellFormat1;
    // // If Manufacturing Item, standard display
    // if (MONatureComplementaryWorkshop.MANUFACTURING.equals(bean.getNatureComplementaryWorkshop())) {
    // if (bean.getItemQttyDoneInRefUnit().concat(bean.getItemQttyToDoInRefUnit())
    // .concat(bean.getItemRefUnit()).length() >= 17) {
    // ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.PLAIN, 13));
    // }
    // } else {
    // // If Scrap or Downgraded or InProgress Item, display only the quantity Done.
    // if (bean.getItemQttyDoneInRefUnit().concat(bean.getItemRefUnit()).length() >= 17) {
    // ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.PLAIN, 13));
    // }
    // }
    // if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
    // ret.setForegroundColor(StandardColor.GRAY);
    // } else {
    // if (rowNum == getCurrentRowNumber()) {
    // ret.setForegroundColor(StandardColor.TOUCHSCREEN_FG_LABEL);
    // } else {
    // ret.setForegroundColor(StandardColor.FG_DEFAULT);
    // }
    // }
    // }
    //
    // return ret;
    // }

    /**
     * Set columns in Browser model set in parameter. if Manufacturing Item, standard display, else does'nt display
     * quantity todo.
     *
     * @param browserModel browser model
     * @param touchModelFunction touchModelFunction
     * @param complementaryWorkshop if Manufacturing Item, standard display, else does'nt display quantity todo.
     * 
     */
    public void manageColumnsByNature(final BrowserModel<?> browserModel, final TouchModelFunction touchModelFunction,
            final MONatureComplementaryWorkshop complementaryWorkshop) {

        browserModel.removeAllColumns();
        browserModel.setBrowserType(BrowserModel.RENDERER_BY_CELL);
        if (touchModelFunction != null) {

            int colDisplay = 0;

            for (TouchModelArray touchModelArray : touchModelFunction.getTouchModelArrays()) {
                CodeFieldModel codeFieldModel = CodeFieldModel.getCodeFieldModel(touchModelArray.getChamp());
                switch (codeFieldModel) {
                    case CART:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_CODE.getValue(), 0,
                                colDisplay));
                        break;
                    case LART:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_LONG_LABEL
                                .getValue(), 0, colDisplay));
                        break;
                    case RART:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_SHORT_LABEL
                                .getValue(), 0, colDisplay));
                        break;
                    case QTARTAFER_1:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_TODO.getValue(), 0,
                                colDisplay));
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;
                    case QTARTFAIT_1:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_DONE.getValue(), 0,
                                colDisplay));
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;
                    case QTARTFAIT_DONE_TODO:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_DONE.getValue(), 0,
                                colDisplay));
                        // The quantity todo only display to Manucfacturing item.
                        if (MONatureComplementaryWorkshop.MANUFACTURING.equals(complementaryWorkshop)) {

                            browserModel.addColumn(new BrowserColumn(" / ", FProductionInputBBeanEnum.QTTY_TODO
                                    .getValue(), 0, colDisplay));
                        }
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;
                    case QTARTFAIT_LEFT_TO_DO:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_LEFT_TO_BE
                                .getValue(), 0, colDisplay));
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;
                    case QTARTFAIT_LEFT_TO_DO_TODO:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_LEFT_TO_BE
                                .getValue(), 0, colDisplay));
                        // The quantity todo only display to Manucfacturing item.
                        if (MONatureComplementaryWorkshop.MANUFACTURING.equals(complementaryWorkshop)) {

                            browserModel.addColumn(new BrowserColumn(" / ", FProductionInputBBeanEnum.QTTY_TODO
                                    .getValue(), 0, colDisplay));
                        }
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;

                    default:
                        break;
                }
                colDisplay = colDisplay + 1;
            }
        } else {
            browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_SHORT_LABEL.getValue(), 0, 0));
            browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_CODE.getValue(), 0, 1));
            browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_DONE.getValue(), 0, 2));
            // The quantity todo only display to Manucfacturing item.
            if (MONatureComplementaryWorkshop.MANUFACTURING.equals(complementaryWorkshop)) {
                browserModel.addColumn(new BrowserColumn(" / ", FProductionInputBBeanEnum.QTTY_TODO.getValue(), 0, 2));
            }
            browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0, 2));
        }

   }
}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersFCtrl.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.menu.LaunchParameterBean;
import fr.vif.jtech.ui.browser.swing.SwingBrowser;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.sharedcontext.SharedContextEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersCfgBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersSBean;
import fr.vif.vif5_7.production.mo.business.services.features.fprocessorders.FProcessOrdersCBS;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ActionId;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;


/**
 * ProcessOrders : Feature Controller.
 * 
 * @author jd
 */
// public class FProcessOrdersFCtrl extends FeatureController {
public class FProcessOrdersFCtrl extends StandardFeatureController {

    public static final String    FEATURE_MOPROGT     = "MOPROGT";
    public static final String    PARAMETER_BEAN      = "FProcessOrdersSBean";

    public static final String    PARAMETER_CHRONOFS  = "FProcessOrdersChronofs";

    public static final String    VIF_FEATURE_MOPROGT = "VIF.MOPROGT";

    private static final Logger   LOGGER              = Logger.getLogger(FProcessOrdersFCtrl.class);

    private FProcessOrdersCBS     fprocessOrdersCBS   = null;

    private FProcessOrdersCfgBean processOrdersCfg    = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        super.buttonAction(event);

        List<FProcessOrdersBBean> listPO = convertToPOList(((SwingBrowser) getBrowserController().getView())
                .getJTable().getSelectedRows());
        List<ChronoPO> listChrono = new ArrayList<ChronoPO>();
        for (FProcessOrdersBBean bBean : listPO) {
            listChrono.add(bBean.getPoKey().getChrono());
        }
        if (FProcessOrdersFModel.UPDATE_MO.equals(event.getModel().getReference())
                || FProcessOrdersFModel.LIST.equals(event.getModel().getReference())
                || FProcessOrdersFModel.INANDOUT.equals(event.getModel().getReference())
                || FProcessOrdersFModel.FINISH.equals(event.getModel().getReference())
                || FProcessOrdersFModel.UPDATE_DECLARATION.equals(event.getModel().getReference())
                || FProcessOrdersFModel.VALIDATE.equals(event.getModel().getReference())) {

            LaunchParameterBean launchParameterBean = FSupervisorMOTools.getLaunchParameter(getCurrentSelection()
                    .getCurrentDate(), listChrono);

            this.openMainFeature(ActionId.getFeatureId(event.getModel().getReference()).getValue(), launchParameterBean);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * Get the current selection.
     * 
     * @return a selection.
     */
    public FProcessOrdersSBean getCurrentSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getCurrentSelection()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getCurrentSelection()");
        }
        return ((FProcessOrdersBCtrl) getBrowserController()).getCurrentSelection();
    }

    /**
     * Gets the fprocessOrdersCBS.
     * 
     * @return the fprocessOrdersCBS.
     */
    public FProcessOrdersCBS getFprocessOrdersCBS() {
        return fprocessOrdersCBS;
    }

    /**
     * Gets the processOrdersCfg.
     * 
     * @return the processOrdersCfg.
     */
    public FProcessOrdersCfgBean getProcessOrdersCfg() {
        return processOrdersCfg;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        if (getModel().getParams() != null) {
            FProcessOrdersSBean sbean = (FProcessOrdersSBean) getModel().getParams().getValues().get(PARAMETER_BEAN);

            if (sbean != null) {
                getBrowserController().setInitialSelection(sbean);
            }
        }

        super.initialize();

        EstablishmentKey establishmentKey = new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment());

        try {
            // init the configuration of the feature using workstation environment
            setProcessOrdersCfg(getFprocessOrdersCBS().initFeature(getIdCtx(), establishmentKey, FEATURE_MOPROGT,
            /* getModel().getFeatureId(), */
            getIdCtx().getLogin(), getIdCtx().getWorkstation()));

            // Share models read in cfg bean.

        } catch (BusinessException e) {
            getView().show(new UIException(e));
            fireSelfFeatureClosingRequired();
        }

        // Init default Selection
        getCurrentSelection().setItemHierarchy(FProcessOrdersTools.getConfiguration(getSharedContext()).getItemHchy());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void launchParametersUpdated() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - launchParametersUpdated()");
        }

        super.launchParametersUpdated();

        FProcessOrdersSBean sbean = (FProcessOrdersSBean) getModel().getParams().getValues().get(PARAMETER_BEAN);

        if (sbean != null) {
            getBrowserController().reopenQuery(sbean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - launchParametersUpdated()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void openMainFeature(final String featureId, final LaunchParameterBean launchParameter) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openMainFeature(featureId=" + featureId + ", launchParameter=" + launchParameter + ")");
        }

        super.openMainFeature(featureId, launchParameter);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openMainFeature(featureId=" + featureId + ", launchParameter=" + launchParameter + ")");
        }
    }

    /**
     * Sets the fprocessOrdersCBS.
     * 
     * @param fprocessOrdersCBS fprocessOrdersCBS.
     */
    public void setFprocessOrdersCBS(final FProcessOrdersCBS fprocessOrdersCBS) {
        this.fprocessOrdersCBS = fprocessOrdersCBS;
    }

    /**
     * Sets the processOrdersCfg.
     * 
     * @param processOrdersCfg processOrdersCfg.
     */
    public void setProcessOrdersCfg(final FProcessOrdersCfgBean processOrdersCfg) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setProcessOrdersCfg(processOrdersCfg=" + processOrdersCfg + ")");
        }

        this.processOrdersCfg = processOrdersCfg;
        FProcessOrdersTools.setConfiguration(this, processOrdersCfg);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setProcessOrdersCfg(processOrdersCfg=" + processOrdersCfg + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sharedContextSend(final SharedContextEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - sharedContextSend(event=" + event + ")");
        }

        super.sharedContextSend(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - sharedContextSend(event=" + event + ")");
        }
    }

    /**
     * Convert list of number rows To FProcessOrdersBBean list.
     * 
     * @param selectedRows int[]
     * @return List<FProcessOrdersBBean>
     */
    private List<FProcessOrdersBBean> convertToPOList(final int[] selectedRows) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertToPOList(selectedRows=" + selectedRows + ")");
        }

        List<FProcessOrdersBBean> listPO = new ArrayList<FProcessOrdersBBean>();

        for (int rowNum : selectedRows) {
            Object element = getBrowserController().getModel().getBeanAt(rowNum);
            if (element instanceof FProcessOrdersBBean) {
                FProcessOrdersBBean po = ((FProcessOrdersBBean) element);
                listPO.add(po);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertToPOList(selectedRows=" + selectedRows + ")=" + listPO);
        }
        return listPO;
    }

}

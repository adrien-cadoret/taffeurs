/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOListFModel.java,v $
 * Created on 14 mar. 2013 by CJ
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.GeneralSelectionMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.FMOListGSCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.FMOListGSModel;


/**
 * MOList : Feature Model.
 * 
 * @author cj
 */
public class FBoningMOListFModel extends StandardFeatureModel {

    /**
     * Default constructor.
     * 
     */
    public FBoningMOListFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FBoningMOListBModel.class, null, FBoningMOListBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FBoningMOListVModel.class, null, FBoningMOListVCtrl.class));
        setGeneralSelectionTriad(new GeneralSelectionMVCTriad(FMOListGSModel.class, null, FMOListGSCtrl.class));
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListBView.java,v $
 * Created on 10 nov. 2008 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.touch;


import java.awt.Component;

import javax.swing.JTable;

import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.swing.StdRenderer;
import fr.vif.jtech.ui.browser.touch.TouchLineBrowser;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListBIView;


/**
 * ItemMovement : Browser View.
 * 
 * @author gv
 */
public class FDataEntryListBView extends TouchLineBrowser<FDataEntryListBBean> implements FDataEntryListBIView {

    /**
     * Renderer to manage font of browser. See in FDataEntryListBModel.getCellFormat
     *
     * @author kl
     */
    private class DataEntryListRenderer extends StdRenderer<FDataEntryListBBean> {

        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {

            Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            return component;
        }

    }

    /**
     * Default constructor.
     * 
     */
    public FDataEntryListBView() {
        super();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JTable getJTableForResize() {
        return getJTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView(final AbstractBrowserController browserCtrl) {
        super.initializeView(browserCtrl);
        resizeColumns();

    }

    /**
     * Add fixe size for columns of MoList Feature.
     */
    private void resizeColumns() {

        if (getModel().getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.USE_TOUCH_MODEL) != null) {
            boolean useTouchModel = (Boolean) getModel().getSharedContext().get(Domain.DOMAIN_PARENT,
                    TouchModel.USE_TOUCH_MODEL);
            if (useTouchModel) {
                TouchModel touchModel = (TouchModel) getModel().getSharedContext().get(Domain.DOMAIN_PARENT,
                        TouchModel.TOUCH_MODEL);

                if (touchModel != null) {
                    OutputEntityType outputEntityType = null;
                    InputEntityType inputEntityType = null;

                    if (getModel().getSharedContext().get(Domain.DOMAIN_PARENT, "inputEntityType") != null) {
                        inputEntityType = (InputEntityType) getModel().getSharedContext().get(Domain.DOMAIN_PARENT,
                                "inputEntityType");
                    } else if (getModel().getSharedContext().get(Domain.DOMAIN_PARENT, "outputEntityType") != null) {
                        outputEntityType = (OutputEntityType) getModel().getSharedContext().get(Domain.DOMAIN_PARENT,
                                "outputEntityType");
                    }

                    for (int i = 0; i < getModel().getColumnCount(); i++) {
                        DataEntryListRenderer dataEntryListRenderer = new DataEntryListRenderer();
                        dataEntryListRenderer.setModel(getModel());
                        getJTable().getColumnModel().getColumn(i).setCellRenderer(dataEntryListRenderer);

                    }

                }
            }
        }
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationItemBView.java,v $
 * Created on 23 déc. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.touch;


import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.browser.touch.renderer.row.DefaultRowRenderer;


/**
 * MO creation item browser view.
 * 
 * @author glc
 */
public class FMOCreationItemBView extends TouchBrowser<CodeLabel> {

    /**
     * Constructor from super class.
     */
    public FMOCreationItemBView() {
        super();
        setColumns(3);
        ((DefaultRowRenderer) getRowRenderer()).setRendererType(BrowserModel.RENDERER_BY_ROW);
        setAutoValidateOnSelect(true);
    }

}

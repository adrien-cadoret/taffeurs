/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOPopupManager.java,v $
 * Created on 28 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


import java.awt.event.ActionListener;

import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;

import org.apache.log4j.Logger;

import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ActionId;


/**
 * FIndicator popup manager.
 * 
 * @author jd
 */
public final class FIndicatorMOPopupManager {
    /** LOGGER. */
    private static final Logger             LOGGER   = Logger.getLogger(FIndicatorMOPopupManager.class);

    private static FIndicatorMOPopupManager instance = null;

    /**
     * Constructor.
     */
    private FIndicatorMOPopupManager() {

    }

    /**
     * Get instance.
     * 
     * @return a Popup manager.
     */
    public static FIndicatorMOPopupManager getInstance() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getInstance()");
        }

        if (instance == null) {
            instance = new FIndicatorMOPopupManager();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getInstance()=" + instance);
        }
        return instance;
    }

    /**
     * Create a popup menu.
     * 
     * @param listener listener.
     * @return a {@link JPopupMenu}
     */
    public JPopupMenu createPopupMenu(final ActionListener listener) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createPopupMenu(listener=" + listener + ")");
        }

        JPopupMenu popup = new JPopupMenu();

        // Create the popup menu.

        // Détail || Details
        JMenuItem menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T29536),
                FSupervisorMOTools.getIcon16(FSupervisorMOTools.ICON_DETAIL));
        menuItem.setActionCommand(ActionId.MO_DETAIL.getValue());
        menuItem.addActionListener(listener);
        popup.add(menuItem);

        // Saisie des O.F. || MO data entry
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T27979),
                FSupervisorMOTools.getIcon16(FSupervisorMOTools.ICON_UPDATE_MO));
        menuItem.setActionCommand(ActionId.MO_UPDATE.getValue());
        menuItem.addActionListener(listener);
        popup.add(menuItem);

        // Liste des O.F. || List of Mos
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T26010),
                FSupervisorMOTools.getIcon16(FSupervisorMOTools.ICON_LIST));
        menuItem.setActionCommand(ActionId.MO_LIST.getValue());
        menuItem.addActionListener(listener);
        popup.add(menuItem);

        // Entrée-Sorties de fabrication || Manufacturing inputs and outputs
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T34231),
                FSupervisorMOTools.getIcon16(FSupervisorMOTools.ICON_INANDOUT));
        menuItem.setActionCommand(ActionId.MO_INPUTS_OUTPUTS.getValue());
        menuItem.addActionListener(listener);
        popup.add(menuItem);

        // Clôture des fabrications || Manufacturing closure
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T26891),
                FSupervisorMOTools.getIcon16(FSupervisorMOTools.ICON_FINISH));
        menuItem.setActionCommand(ActionId.MO_FINISH.getValue());
        menuItem.addActionListener(listener);
        popup.add(menuItem);

        // Modification des déclarations || Change of declarations
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T26603),
                FSupervisorMOTools.getIcon16(FSupervisorMOTools.ICON_UPDATE_DECLARATION));
        menuItem.setActionCommand(ActionId.MO_UPDATE_DECLARATION.getValue());
        menuItem.addActionListener(listener);
        popup.add(menuItem);

        // Validation des O.T. || Validation of POs
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T28422),
                FSupervisorMOTools.getIcon16(FSupervisorMOTools.ICON_VALIDATE));
        menuItem.setActionCommand(ActionId.MO_VALIDATE.getValue());
        menuItem.addActionListener(listener);
        popup.add(menuItem);

        // popup.addSeparator();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createPopupMenu(listener=" + listener + ")=" + popup);
        }
        return popup;
    }
}

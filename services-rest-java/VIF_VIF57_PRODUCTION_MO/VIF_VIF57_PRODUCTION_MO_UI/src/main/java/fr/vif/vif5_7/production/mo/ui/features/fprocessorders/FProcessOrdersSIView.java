/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersSIView.java,v $
 * Created on 21 mars 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders;


import fr.vif.vif5_7.activities.activities.ui.composites.cteamlist.swing.CTeamListView;
import fr.vif.vif5_7.gen.hierarchy.ui.composites.hierarchy.chchylist.swing.CHchyListView;
import fr.vif.vif5_7.gen.location.ui.composites.carealist.swing.CAreaListView;


/**
 * Interface for Process Orders Selection View.
 * 
 * @author ag
 */
public interface FProcessOrdersSIView {

    /**
     * Gets the cArea.
     * 
     * @return the cArea.
     */
    public CAreaListView getcArea();

    /**
     * Gets the cFLine.
     * 
     * @return the cFLine.
     */
    public CHchyListView getcFLine();

    /**
     * Gets the cItemHchy.
     * 
     * @return the cItemHchy.
     */
    public CHchyListView getcItemHchy();

    /**
     * Gets the cTeam.
     * 
     * @return the cTeam.
     */
    public CTeamListView getcTeam();

}

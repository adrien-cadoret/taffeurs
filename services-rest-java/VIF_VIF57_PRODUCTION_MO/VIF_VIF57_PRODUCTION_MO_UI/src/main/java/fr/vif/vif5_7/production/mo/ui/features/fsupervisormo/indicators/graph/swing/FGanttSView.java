/*
 * Copyright (c) 2015 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PREPARATION_KERNEL_UI
 * File : $RCSfile: FGanttSView.java,v $
 * Created on 28 juil. 2015 by nh
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import fr.vif.jtech.ui.dialogs.selection.swing.StandardSwingSelection;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.admin.config.AdminConfig;


/**
 * FGantt selection view.
 *
 * @author ac
 */
public class FGanttSView extends StandardSwingSelection {

    private static final int        WIDTH        = 450;
    private static final int        HEIGHT       = 325;

    private JPanel                  checkBoxPanel;

    private FGanttMOAnnotationPanel valuesPanel  = null;

    private FGanttMOHorizonPanel    horizonPanel = null;

    /**
     * Constructor.
     * 
     * @param view A dialog.
     * @wbp.parser.constructor
     */
    public FGanttSView(final Dialog view) {
        super(view);
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param view A Frame.
     */
    public FGanttSView(final Frame view) {
        super(view);
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableComponents() {
        super.enableComponents();
    }

    /**
     * Gets the horizonPanel.
     *
     * @return the horizonPanel.
     */
    public FGanttMOHorizonPanel getHorizonPanel() {
        return horizonPanel;
    }

    /**
     * Gets the valuesPanel.
     *
     * @return the valuesPanel.
     */
    public FGanttMOAnnotationPanel getValuesPanel() {
        return valuesPanel;
    }

    /**
     * Sets the horizonPanel.
     *
     * @param horizonPanel horizonPanel.
     */
    public void setHorizonPanel(final FGanttMOHorizonPanel horizonPanel) {
        this.horizonPanel = horizonPanel;
    }

    /**
     * Sets the valuesPanel.
     *
     * @param valuesPanel valuesPanel.
     */
    public void setValuesPanel(final FGanttMOAnnotationPanel valuesPanel) {
        this.valuesPanel = valuesPanel;
    }

    /**
     * This private method is called to initialize the view.
     */
    private void initialize() {

        setTitle(I18nClientManager.translate(AdminConfig.T25888));

        JPanel container = new JPanel(new GridBagLayout());

        valuesPanel = new FGanttMOAnnotationPanel();
        // valuesPanel.setBeanProperty("indicatorValues");
        valuesPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.fill = GridBagConstraints.BOTH;
        container.add(valuesPanel, gbc);

        horizonPanel = new FGanttMOHorizonPanel();
        horizonPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
        GridBagConstraints gbc2 = new GridBagConstraints();
        gbc2.gridx = 0;
        gbc2.gridy = 1;
        gbc2.weightx = 1;
        gbc2.anchor = GridBagConstraints.LINE_START;
        gbc2.fill = GridBagConstraints.BOTH;
        container.add(horizonPanel, gbc2);

        addPanel(container);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        setModal(true);

        // pack();
    }
}

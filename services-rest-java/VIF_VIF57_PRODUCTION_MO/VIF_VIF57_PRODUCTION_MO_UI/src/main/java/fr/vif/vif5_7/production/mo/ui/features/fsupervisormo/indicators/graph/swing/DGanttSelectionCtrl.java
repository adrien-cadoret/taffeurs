/*
 * Copyright (c) 2015 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PREPARATION_KERNEL_UI
 * File : $RCSfile: DGanttSelectionCtrl.java,v $
 * Created on Nov 25, 2015 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.exceptions.UIException;


/**
 * Gantt selection controller.
 *
 * @author ac
 */
public class DGanttSelectionCtrl extends AbstractStandardDialogController {

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

}

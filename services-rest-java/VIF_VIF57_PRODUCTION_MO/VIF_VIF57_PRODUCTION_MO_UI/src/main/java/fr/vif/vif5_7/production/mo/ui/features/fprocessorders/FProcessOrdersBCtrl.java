/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersBCtrl.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders;


import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.swing.ListSelectionModel;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.swing.SwingBrowser;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersBBeanComparator;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersSBean;
import fr.vif.vif5_7.production.mo.business.services.features.fprocessorders.FProcessOrdersCBS;


/**
 * ProcessOrders : Browser Controller.
 * 
 * @author jd
 */
public class FProcessOrdersBCtrl extends AbstractBrowserController<FProcessOrdersBBean> {
    /** LOGGER. */
    private static final Logger           LOGGER           = Logger.getLogger(FProcessOrdersBCtrl.class);

    private FProcessOrdersBBeanComparator comparator       = new FProcessOrdersBBeanComparator();
    private FProcessOrdersSBean           currentSelection = null;

    private FProcessOrdersCBS             fprocessOrdersCBS;

    /**
     * Default constructor.
     * 
     */
    public FProcessOrdersBCtrl() {
        super();
    }

    /**
     * get the current selection.
     * 
     * @return {@link FProcessOrdersSBean}
     */
    public FProcessOrdersSBean getCurrentSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getCurrentSelection()");
        }

        if (currentSelection == null) {
            currentSelection = (FProcessOrdersSBean) getDefaultInitialSelection();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getCurrentSelection()=" + currentSelection);
        }
        return currentSelection;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDefaultInitialSelection()");
        }

        FProcessOrdersSBean selection = new FProcessOrdersSBean();
        selection.getSelection().setPoDate(new Date());
        selection.getSelection().setEstablishmentKey(
                new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDefaultInitialSelection()=" + selection);
        }
        return selection;
    }

    /**
     * Gets the fprocessOrdersCBS.
     * 
     * @category getter
     * @return the fprocessOrdersCBS.
     */
    public final FProcessOrdersCBS getFprocessOrdersCBS() {
        return fprocessOrdersCBS;
    }

    /**
     * Sets the fprocessOrdersCBS.
     * 
     * @category setter
     * @param fprocessOrdersCBS fprocessOrdersCBS.
     */
    public final void setFprocessOrdersCBS(final FProcessOrdersCBS fprocessOrdersCBS) {
        this.fprocessOrdersCBS = fprocessOrdersCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FProcessOrdersBBean convert(final Object bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }

        FProcessOrdersBBean bBean = getModel().getCurrentBean();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")=" + bBean);
        }
        return bBean;
        // return super.convert(bean);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FProcessOrdersBBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        ((SwingBrowser) getView()).getJTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        List<FProcessOrdersBBean> lst = null;
        try {
            lst = getFprocessOrdersCBS().queryElements(getIdCtx(), (FProcessOrdersSBean) selection, startIndex,
                    rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }
        Collections.sort(lst, comparator);
        currentSelection = (FProcessOrdersSBean) selection;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + lst);
        }
        return lst;
    }

}

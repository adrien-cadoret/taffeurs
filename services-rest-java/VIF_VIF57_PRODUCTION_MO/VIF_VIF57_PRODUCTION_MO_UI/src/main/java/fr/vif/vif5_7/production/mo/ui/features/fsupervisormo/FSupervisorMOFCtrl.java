/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOFCtrl.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo;


import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JPopupMenu;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.UIManager;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.common.beans.menu.LaunchParameterBean;
import fr.vif.jtech.common.util.selection.PermanentSelectionValues;
import fr.vif.jtech.ui.browser.swing.SwingBrowser;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.sharedcontext.SharedContextEvent;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.swing.SwingHelper;
import fr.vif.jtech.ui.viewer.AbstractViewerController;
import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.dialog.viewname.DViewNameMOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOCfgBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSBean;
import fr.vif.vif5_7.production.mo.business.services.features.fsupervisormo.FSupervisorMOCBS;
import fr.vif.vif5_7.production.mo.business.services.features.indicator.ProductivityMOCBS;
import fr.vif.vif5_7.production.mo.ui.dialog.viewnamemo.DViewNameMOCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.lines.FSupervisorMOLinesInternalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing.AbstractMODockableView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing.FSupervisorMOIVView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOFactory;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOIFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOIMenu;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOManager;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOSaveFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOSaveSelection;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOSaveView;


/**
 * FSupervisorMO : Feature Controller.
 * 
 * @author jd
 */
public class FSupervisorMOFCtrl extends StandardFeatureController implements MouseListener, ActionListener,
        DialogChangeListener {

    private static final String              FEATURE_MOSUPGT     = "MOSUPGT";

    private static final Logger              LOGGER              = Logger.getLogger(FSupervisorMOFCtrl.class);

    private static final String              POINT               = ".";
    private static final String              VIEW_NAME_SEPARATOR = "\\|";

    private List<FSupervisorMOIFrame>        frames              = new ArrayList<FSupervisorMOIFrame>();

    private FSupervisorMOCBS                 fsupervisorMOCBS    = null;

    private List<MOLocalFrame>               localFrames         = new ArrayList<MOLocalFrame>();

    private FSupervisorMOManager             manager             = null;

    private JPopupMenu                       popup               = null;

    private ProductivityMOCBS                productivityMOCBS   = null;

    private FSupervisorMOCfgBean             supervisorCfg       = null;

    private javax.swing.Timer                timer               = null;

    private FSupervisorMOMemorizationManager memorization        = null;

    private String                           currentView         = "item";

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(e=" + e + ")");
        }

        GenericActionEvent event = new GenericActionEvent(e.getSource(), "", e.getActionCommand(), null);
        actionPerformed(event);

        if (e.getActionCommand() != null && e.getActionCommand().startsWith(FSupervisorMOTools.OPEN_FRAME)) {
            int i = new Integer(e.getActionCommand().split(VIEW_NAME_SEPARATOR)[1]);

            MOLocalFrame l = localFrames.get(i);

            createNewFrame(l, true, null);
        } else if (e.getActionCommand() != null && e.getActionCommand().startsWith(FSupervisorMOTools.SHOW_VIEW)) {

            // Name of the view
            String viewName = e.getActionCommand().split(VIEW_NAME_SEPARATOR)[1];

            // set the current view
            setCurrentView(viewName);

            // Empty the Jid view

            // Avoid Concurrent modification
            hideAll();

            // Empty list of instances
            frames.clear();

            // empty the reference list
            localFrames.clear();

            // Read XML file

            createDynamicFrames(viewName);

            // Send new frames to the view
            // send it to the viewer
            ((FSupervisorMOIVView) getViewerController().getView()).setDockables(this.getSupervisorCfg().getPath(),
                    frames);

            // create Popups
            createPopups();
            // read the Jid view file
            // ((FSupervisorMOIVView) getViewerController().getView()).openView(this.getSupervisorCfg().getPath(),
            // viewName);
            try {
                getManager().loadDocking(((FSupervisorMOIVView) getViewerController().getView()).getDockingManager(),
                        viewName);

                /* FB DP2581 load selection when view change */
                // Read all Memorizations in one call and set in shared context.
                Map<String, PermanentSelectionValues> map;

                map = getFsupervisorMOCBS().getSelectionByIds(getIdCtx(), "$$", "$$", "", viewName, "SPP");

                // Memorization manager
                // memorization = new FSupervisorMOMemorizationManager(this, map);
                memorization.setMap(map);

            } catch (UIException | BusinessException e1) {
            }

            // TODO Auto-generated catch block
            // LOGGER.error("", e1);

            setTitleWithView(viewName);
            refreshAll();

        } else if (FSupervisorMOTools.SAVE_VIEW.equalsIgnoreCase(e.getActionCommand())) {
            // Get the path from configuration
            // String completPath = FSupervisorMOManager.checkPath(this.getSupervisorCfg().getPath(), getIdCtx()
            // .getLogin());

            DViewNameMOBean bean = new DViewNameMOBean();
            // bean.setList((List<String>) getSharedContext()
            // .get(Domain.DOMAIN_THIS, KernelFeatureConstant.FAVORITE_VIEWS));
            bean.setList(getManager().getFavoriteViews(getIdCtx()));

            // NLE : extracts the name of the view : "Superviseur (Vue xxx yyy zzz)" ==> "xxx yyy zzz"

            // String[] p = getModel().getTitle().split(" ");
            // if (p.length >= 3) {
            // String[] q = p[2].split("\\)");
            // if (q.length >= 1) {
            // if (!q[0].equals("sector") && !q[0].equals("item") && !q[0].equals("line") & !q[0].equals("team")) {
            // bean.setName(q[0]);
            // }
            // }
            // }

            String[] p = getModel().getTitle().split(" ");
            if (p.length >= 3) {
                String str = "";
                String[] q = p[p.length - 1].split("\\)"); // get rid of ")"
                if (p.length == 3) { // Only xxx
                    if (!q[0].equals("sector") && !q[0].equals("item") && !q[0].equals("line") & !q[0].equals("team")) {
                        bean.setName(q[0]);
                    }
                } else { // xxx yyy zzz
                    for (int i = 2; i < p.length - 1; i++) {
                        if (str.equals("")) {
                            str = p[i]; // "" ==> xxx
                        } else {
                            str = str + " " + p[i]; // xxx ==> xxx yyy
                        }
                    }
                    str = str + " " + q[0]; // adds zzz without the final ")"
                    if (!str.equals("sector") && !str.equals("item") && !str.equals("line") & !str.equals("team")) {
                        bean.setName(str);
                    }
                }
            }

            DViewNameMOCtrl.showViewName(this.getView(), //
                    this, //
                    this, //
                    this.getViewerController().getIdentification(), //
                    bean, //
                    getModel().getTitle() // NLE
                    );

            // String fileName = ((FSupervisorMOIVView) getViewerController().getView()).askViewName();
            /* FBEA DP2581 */
            if (bean.getAccepted()) {
                String fileName = bean.getName();
                // remove extend to avoid file with .jid.jid or .xml.xml
                if (fileName.indexOf(POINT) > 0) {
                    fileName = fileName.substring(0, fileName.indexOf(POINT));
                }

                if (EntityTools.isValid(fileName)) {
                    // if (bean.getAccepted()) { NLE : to be able to overwrite a view
                    try {
                        getManager().saveLocalFrames(localFrames, fileName, getCurrentSelection());

                        // Save Jid file after saving frames to avoid existing jid without xml
                        getManager().saveDocking(
                                ((FSupervisorMOIVView) getViewerController().getView()).getDockingManager(), fileName);
                        // ((FSupervisorMOIVView) getViewerController().getView()).saveEnvironment(completPath +
                        // fileName
                        // + FSupervisorMOManager.EXTENSION_JID);

                        setTitleWithView(fileName);

                        getView().showInformation(I18nClientManager.translate(ProductionMo.T32441));

                        actionPerformed(new ActionEvent(this, 0, FSupervisorMOTools.SHOW_VIEW + VIEW_NAME_SEPARATOR
                                + fileName));

                    } catch (UIException e1) {
                        getView().show(e1);
                    }

                    // NLE : to be able to overwrite a view
                    // } else {
                    // getView()
                    // .showError(
                    // I18nClientManager.translate(Generic.T13125, false,
                    // new VarParamTranslation(bean.getName())));
                    // }
                }
            }

        } else if (FSupervisorMOTools.SELECTION.equalsIgnoreCase(e.getActionCommand())) {
            getBrowserController().selectionRequired();

            refreshAll();
        } else if (FSupervisorMOTools.FRAME_IS_CLOSING.equalsIgnoreCase(e.getActionCommand())) {
            List<FSupervisorMOIFrame> tmp = new ArrayList<FSupervisorMOIFrame>();
            tmp.addAll(frames);

            for (FSupervisorMOIFrame d : tmp) {
                if (d == e.getSource()) {
                    frames.remove(d);
                }
            }
        } else if (e.getSource() instanceof FSupervisorMOIMenu
                && e.getActionCommand().equalsIgnoreCase(FSupervisorMOTools.CREATE_NEW_DOCK)) {

            FSupervisorMOIMenu menu = (FSupervisorMOIMenu) e.getSource();
            String uniqueId = menu.getLocalFrame().getUniqueId();
            for (MOLocalFrame local : localFrames) {
                if (local.getUniqueId().equalsIgnoreCase(uniqueId)) {
                    local.getFrameBean().setIsVisible(!local.getFrameBean().getIsVisible());

                    // Change selection which may have change
                    local.getSelection().setPoDate(getCurrentSelection().getCurrentDate());

                    if (local.getFrameBean().getIsVisible()) {
                        createNewFrame(local, true, null);
                        break;
                    } else {
                        // tab not used any more
                        FSupervisorMOIFrame dock = null;
                        for (FSupervisorMOIFrame d : frames) {
                            if (d.getLocalFrame().getUniqueId().equalsIgnoreCase(uniqueId)) {
                                dock = d;
                                break;
                            }
                        }
                        removeInternalFrame(dock);
                    }

                }
            }
        } else if (FSupervisorMOTools.CLOSE_ALL.equalsIgnoreCase(e.getActionCommand())) {

            hideAll();

        } else if (FSupervisorMOTools.REFRESH.equalsIgnoreCase(e.getActionCommand())) {
            // Refresh all one time
            refreshAll();
        } else {

            if (e.getSource() instanceof Timer) {
                refreshAll();
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * Add a frame to a desktop.
     * 
     * @param internalFrame internal frame to add.
     */
    public void addFrame(final AbstractMODockableView internalFrame) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - addFrame(internalFrame=" + internalFrame + ")");
        }

        internalFrame.addActionlistener(this);

        addListeners(internalFrame);

        // tab not used any more
        frames.add(internalFrame);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - addFrame(internalFrame=" + internalFrame + ")");
        }
    }

    /**
     * Add links to a new dock.
     * 
     * @param internalFrame new dock to add links
     */
    public void addListeners(final AbstractMODockableView internalFrame) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - addListeners(internalFrame=" + internalFrame + ")");
        }

        if (internalFrame.getBrowserController() != null) {
            getBrowserController().getModel().setIdentification(getModel().getIdentification());
            getBrowserController().getModel().setSharedContext(getSharedContext());

        }

        List<AbstractViewerController> list = SwingHelper.findComponentsInstanceOf(internalFrame,
                AbstractViewerController.class, true);
        for (AbstractViewerController viewerController : list) {
            viewerController.setIdentification(getModel().getIdentification());
            viewerController.getModel().setSharedContext(getSharedContext());
        }
        List<SwingBrowser> list2 = SwingHelper.findComponentsInstanceOf(internalFrame, SwingBrowser.class, true);
        for (SwingBrowser browser : list2) {
            browser.getModel().setIdentification(getModel().getIdentification());
            browser.getModel().setSharedContext(getSharedContext());
        }

        internalFrame.setParentController(this);

        // Merge the parent controller shared context to this.
        internalFrame.getViewerController().getSharedContext().merge(getSharedContext());
        fireSharedContextSend(getSharedContext());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - addListeners(internalFrame=" + internalFrame + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closed(final ClosingEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - closed(event=" + event + ")");
        }

        // save last view (jid)
        // String completePath = FSupervisorMOManager.checkPath(this.getSupervisorCfg().getPath(),
        // getIdCtx().getLogin());
        // ((FSupervisorMOIVView) getViewerController().getView()).saveEnvironment(completePath
        // + FSupervisorMOManager.VIEW_LAST + FSupervisorMOManager.EXTENSION_JID); // FIXME Replaced by saveDocking

        if (timer != null) {
            timer.removeActionListener(this);
            timer.stop();
            timer = null;
        }

        try {
            // save last view (xml)
            getManager().saveLocalFrames(localFrames, FSupervisorMOManager.VIEW_LAST, getCurrentSelection());
            getManager().saveDocking(((FSupervisorMOIVView) getViewerController().getView()).getDockingManager(),
                    FSupervisorMOManager.VIEW_LAST);

            Map<String, PermanentSelectionValues> map;
            /* FB DP2581 load selected view and save it as LAST view */
            map = getFsupervisorMOCBS().getSelectionByIds(getIdCtx(), "$$", "$$", "", currentView, "SPP");
            getFsupervisorMOCBS().saveSelectionByIds(getIdCtx(), "$$", "$$", "", FSupervisorMOManager.VIEW_LAST, "",
                    "SPP", map);

            getManager().reinitToolTipDelay();
        } catch (UIException | BusinessException e) {
            getView().show((UIException) e);
        }

        super.closed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - closed(event=" + event + ")");
        }
    }

    /**
     * Add a floating dock.
     * 
     * @param newFrame frame to add.
     */
    public void createNewFloatingDock(final AbstractMODockableView newFrame) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createNewFloatingDock(newFrame=" + newFrame + ")");
        }

        ((FSupervisorMOIVView) getViewerController().getView()).createFloatingDock(newFrame);

        addListeners(newFrame);

        frames.add(newFrame);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createNewFloatingDock(newFrame=" + newFrame + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {

    }

    /**
     * Get the current selection.
     * 
     * @return a selection.
     */
    public FSupervisorMOSBean getCurrentSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getCurrentSelection()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getCurrentSelection()");
        }
        return ((FSupervisorMOBCtrl) getBrowserController()).getCurrentSelection();
    }

    /**
     * TODO : write the method's description
     * 
     * @return
     */
    public Object getCurrentView() {
        return currentView;
    }

    /**
     * Gets the fsupervisorMOCBS.
     * 
     * @return the fsupervisorMOCBS.
     */
    public FSupervisorMOCBS getFsupervisorMOCBS() {
        return fsupervisorMOCBS;
    }

    /**
     * Gets the manager.
     * 
     * @return the manager.
     */
    public FSupervisorMOManager getManager() {
        return manager;
    }

    /**
     * Gets the memorization.
     *
     * @return the memorization.
     */
    public FSupervisorMOMemorizationManager getMemorization() {
        return memorization;
    }

    /**
     * Gets the productivityMOCBS.
     * 
     * @return the productivityMOCBS.
     */
    public ProductivityMOCBS getProductivityMOCBS() {
        return productivityMOCBS;
    }

    /**
     * Gets the supervisorCfg.
     * 
     * @return the supervisorCfg.
     */
    public FSupervisorMOCfgBean getSupervisorCfg() {
        return supervisorCfg;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractViewerController getViewerController() {
        return super.getViewerController();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        super.initialize();

        EstablishmentKey establishmentKey = new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment());

        try {
            // init the configuration of the feature using workstation environment
            setSupervisorCfg(getFsupervisorMOCBS().initFeature(getIdCtx(), establishmentKey, FEATURE_MOSUPGT,
            /* getModel().getFeatureId(), */
            getIdCtx().getLogin(), getIdCtx().getWorkstation()));

            // Share models read in cfg bean.

        } catch (BusinessException e) {
            getView().show(new UIException(e));
            fireSelfFeatureClosingRequired();
        }

        // Read all Memorizations in one call and set in shared context.
        Map<String, PermanentSelectionValues> map;
        try {
            /* FB DP2581 load last view */
            // map = getFsupervisorMOCBS().getSelectionByIds(getIdCtx(), "$$", "$$", "", currentView, "SPP"
            // /* currentView, FSupervisorTools.MEMORIZATION_KEY */);
            map = getFsupervisorMOCBS().getSelectionByIds(getIdCtx(), "$$", "$$", "", FSupervisorMOManager.VIEW_LAST,
                    "SPP");

            // Memorization manager
            memorization = new FSupervisorMOMemorizationManager(this, map);
        } catch (BusinessException e1) {
            // TODO Auto-generated catch block
            // LOGGER.error("", e1);
        }

        // Init default Selection
        getCurrentSelection().setItemHierarchy(FSupervisorMOTools.getConfiguration(getSharedContext()).getItemHchy());

        // Init mouselistener for actions
        ((FSupervisorMOFIView) getView()).addMouseListener(this);

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("B - $Runnable.run()");
                }

                manager = new FSupervisorMOManager(FSupervisorMOFCtrl.this);

                // Create All frames

                // Load the last view of the current user from Xml file

                createDynamicFrames(FSupervisorMOManager.VIEW_LAST);

                // set the current view
                setCurrentView(FSupervisorMOManager.VIEW_LAST);

                // send it to the viewer
                ((FSupervisorMOIVView) getViewerController().getView()).setDockables(getSupervisorCfg().getPath(),
                        frames);
                try {
                    getManager().loadDocking(
                            ((FSupervisorMOIVView) getViewerController().getView()).getDockingManager(),
                            FSupervisorMOManager.VIEW_LAST);
                } catch (UIException e) {
                }

                // NLE : to display the name of the view as soon as the screen appears
                // NLE : but we won't do it because it's always saved under the name 'last'
                // setTitleWithView(FSupervisorMOManager.VIEW_LAST);

                // create Popups
                createPopups();

                // Refresh all one time

                /*
                 * ACA Useless because it's already refreshed in createNewFrame called by createDynamicFrame for each
                 * LocalFrame
                 */
                // refreshAll();
                //
                timer = new javax.swing.Timer(60000, FSupervisorMOFCtrl.this); // refresh every minutes
                timer.start();

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("E - $Runnable.run()");
                }
            }
        });

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseEntered(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseExited(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void openMainFeature(final String featureId, final LaunchParameterBean launchParameter) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openMainFeature(featureId=" + featureId + ", launchParameter=" + launchParameter + ")");
        }

        super.openMainFeature(featureId, launchParameter);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openMainFeature(featureId=" + featureId + ", launchParameter=" + launchParameter + ")");
        }
    }

    /**
     * removeInternalFrame.
     * 
     * @param fSupervisorInternalFrame interfave of an internal frame.
     */
    public void removeInternalFrame(final FSupervisorMOIFrame fSupervisorInternalFrame) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - removeInternalFrame(fSupervisorInternalFrame=" + fSupervisorInternalFrame + ")");
        }

        List<FSupervisorMOIFrame> tmp = new ArrayList<FSupervisorMOIFrame>();
        tmp.addAll(frames);
        for (FSupervisorMOIFrame frame : frames) {
            if (frame == fSupervisorInternalFrame) {
                ((FSupervisorMOIVView) getViewerController().getView()).destroy(frame.getLocalFrame());
                tmp.remove(frame);
                break;
            }
        }
        frames.clear();
        frames.addAll(tmp);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - removeInternalFrame(fSupervisorInternalFrame=" + fSupervisorInternalFrame + ")");
        }
    }

    /**
     * Sets the currentView.
     *
     * @param currentView currentView.
     */
    public void setCurrentView(final String currentView) {
        this.currentView = currentView;
    }

    /**
     * Sets the fsupervisorMOCBS.
     * 
     * @param fsupervisorMOCBS fsupervisorMOCBS.
     */
    public void setFsupervisorMOCBS(final FSupervisorMOCBS fsupervisorMOCBS) {
        this.fsupervisorMOCBS = fsupervisorMOCBS;
    }

    /**
     * Sets the manager.
     * 
     * @param manager manager.
     */
    public void setManager(final FSupervisorMOManager manager) {
        this.manager = manager;
    }

    /**
     * Sets the memorization.
     *
     * @param memorization memorization.
     */
    public void setMemorization(final FSupervisorMOMemorizationManager memorization) {
        this.memorization = memorization;
    }

    /**
     * Sets the productivityMOCBS.
     * 
     * @param productivityMOCBS productivityMOCBS.
     */
    public void setProductivityMOCBS(final ProductivityMOCBS productivityMOCBS) {
        this.productivityMOCBS = productivityMOCBS;
    }

    /**
     * Sets the supervisorCfg.
     * 
     * @param supervisorCfg supervisorCfg.
     */
    public void setSupervisorCfg(final FSupervisorMOCfgBean supervisorCfg) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setSupervisorCfg(supervisorCfg=" + supervisorCfg + ")");
        }

        // LookAndFeelFactory.installJideExtension(LookAndFeelFactory.XERTO_STYLE);
        Font titleFont = (Font) UIManager.get("DockableFrameTitlePane.font");
        titleFont = new Font(titleFont.getName(), Font.BOLD, titleFont.getSize());
        UIManager.put("DockableFrameTitlePane.font", titleFont);

        this.supervisorCfg = supervisorCfg;
        FSupervisorMOTools.setConfiguration(this, supervisorCfg);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setSupervisorCfg(supervisorCfg=" + supervisorCfg + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sharedContextSend(final SharedContextEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - sharedContextSend(event=" + event + ")");
        }

        super.sharedContextSend(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - sharedContextSend(event=" + event + ")");
        }
    }

    /**
     * Create dynamics frames from a file name.
     * 
     * @param fileName name of the file to create frames from.
     */
    private void createDynamicFrames(final String fileName) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createDynamicFrames(fileName=" + fileName + ")");
        }

        try {
            // Load the file reference.
            List<MOSaveFrame> savedFrames = manager.loadXmlFrame(FSupervisorMOManager.VIEW_REFERENTIEL,
                    MOSaveFrame.class);
            if (savedFrames == null) {
                throw new UIException(Generic.T20822);
            }

            // change the titles of the frames , using the context
            for (MOSaveFrame frame : savedFrames) {
                frame.setTitleFrameU(FSupervisorMOManager.getTitle(frame.getTitleFrameU(), frame.getTitleFrame()));
            }

            localFrames = getManager().convertToLocalFrame(savedFrames);

            // Convert the list to a map
            Map<String, MOLocalFrame> referentielMap = new HashMap<String, MOLocalFrame>();
            for (MOLocalFrame frame : localFrames) {
                referentielMap.put(frame.getUniqueId(), frame);
            }

            if (!FSupervisorMOManager.VIEW_REFERENTIEL.equals(fileName)) {

                Date currentDate = getCurrentSelection().getCurrentDate();
                EstablishmentKey establishmentKey = getCurrentSelection().getEstablishmentKey();

                // Load the selection and views
                List<MOSaveView> views = null;

                List<MOSaveSelection> saveSelections = manager.loadXmlFrame(fileName, MOSaveSelection.class);
                if (saveSelections != null && saveSelections.size() > 0) {
                    // Change the current selection ...
                    MOSaveSelection saveSelection = saveSelections.get(0); // Only one selection in this file
                    if (saveSelection.getSelection() instanceof FSupervisorMOSBean) {
                        getCurrentSelection().setSelection(
                                ((FSupervisorMOSBean) saveSelection.getSelection()).getSelection());
                    }
                    // ... but not the date
                    getCurrentSelection().setCurrentDate(currentDate);
                    getCurrentSelection().setEstablishmentKey(establishmentKey);

                    // Change the view
                    views = saveSelection.getViews();
                }
                // Old format of save views
                if (views == null) {
                    views = manager.loadXmlFrame(fileName, MOSaveView.class);
                }

                if (views != null) {
                    // merging of 2 maps
                    for (MOSaveView view : views) {
                        if (referentielMap.containsKey(view.getUniqueId())) {
                            // Frame is present ?
                            MOLocalFrame referentielFrame = referentielMap.get(view.getUniqueId());
                            referentielFrame.getFrameBean().setIsVisible(view.getIsVisible());

                            // Change titles of each view using the context
                            referentielFrame.getFrameBean().setTitle(
                                    FSupervisorMOManager.getTitle(view.getTitleFrameU(), view.getTitleFrame()));

                            if (view.getIsVisible()) {
                                createNewFrame(referentielFrame, false, null);
                            }
                        }
                    }

                }

            }
        } catch (UIException e) {
            getView().show(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createDynamicFrames(fileName=" + fileName + ")");
        }
    }

    /**
     * Create a new frame using Local frame bean.
     * 
     * @param l localFrame
     * @param floatingDock it's a floated dock.
     * @param selection TODO
     */
    private void createNewFrame(final MOLocalFrame l, final boolean floatingDock, final Selection selection) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createNewFrame(l=" + l + ", floatingDock=" + floatingDock + ")");
        }

        // l.getSelection().g;
        // getCurrentSelection().getSelection();

        try {
            AbstractMODockableView newFrame = FSupervisorMOFactory.getInstance().createNewFrame(l);
            refreshFrameSelection(newFrame, getCurrentSelection());
            if (floatingDock) {
                createNewFloatingDock(newFrame);
            } else {
                addFrame(newFrame);
            }
            newFrame.initialize();
            if (selection == null) {
                newFrame.refresh();
            } else {
                newFrame.setSelection(selection);
            }
        } catch (UIException e) {
            getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createNewFrame(l=" + l + ", floatingDock=" + floatingDock + ")");
        }
    }

    /**
     * Create the popup menus.
     */
    private void createPopups() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createPopups()");
        }

        List<String> views = getManager().getFavoriteViews(getIdCtx());
        popup = ((FSupervisorMOIVView) getViewerController().getView()).createPopupMenu(this, localFrames, views);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createPopups()");
        }
    }

    /**
     * Hide all frames.
     * 
     */
    private void hideAll() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - hideAll()");
        }

        List<FSupervisorMOIFrame> tmp = new ArrayList<FSupervisorMOIFrame>();
        tmp.addAll(frames);

        for (FSupervisorMOIFrame d : tmp) {
            ((FSupervisorMOIVView) getViewerController().getView()).destroy(d.getLocalFrame());
            frames.remove(d);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - hideAll()");
        }
    }

    /**
     * Refresh all frames.
     */
    private void refreshAll() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refreshAll()");
        }

        for (FSupervisorMOIFrame frame : frames) {

            // Selection may have changed use new values !
            refreshFrameSelection(frame, getCurrentSelection());

            frame.refresh();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - refreshAll()");
        }
    }

    /**
     * Resfresh selection frame.
     * 
     * @param frame Frame
     * @param currentSelection selection
     */
    private void refreshFrameSelection(final FSupervisorMOIFrame frame, final FSupervisorMOSBean currentSelection) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refreshFrameSelection(frame=" + frame + ", currentSelection=" + currentSelection + ")");
        }

        frame.getLocalFrame().getSelection().setPoDate(currentSelection.getCurrentDate());
        frame.getLocalFrame().getSelection().setCategories(currentSelection.getSelection().getCategories());
        frame.getLocalFrame().getSelection().setLines(currentSelection.getSelection().getLines());
        frame.getLocalFrame().getSelection().setSectors(currentSelection.getSelection().getSectors());
        frame.getLocalFrame().getSelection().setTeams(currentSelection.getSelection().getTeams());

        if (frame instanceof FSupervisorMOLinesInternalFrame) {
            FProcessOrdersSBean sBean = new FProcessOrdersSBean();
            sBean.setCurrentDate(currentSelection.getCurrentDate());
            sBean.setEstablishmentKey(currentSelection.getEstablishmentKey());
            sBean.setSelection(currentSelection.getSelection());
            ((FSupervisorMOLinesInternalFrame) frame).setSelection(sBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - refreshFrameSelection(frame=" + frame + ", currentSelection=" + currentSelection + ")");
        }
    }

    /**
     * Define the title with view selected.
     * 
     * @param viewName name of the view.
     */
    private void setTitleWithView(final String viewName) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setTitleWithView(viewName=" + viewName + ")");
        }

        String viewLabel = I18nClientManager.translate(ProductionMo.T32460); // Vue
        String title = I18nClientManager.translate(ProductionMo.T32457); // Superviseur

        if (!viewName.equals("sector") && !viewName.equals("item") && !viewName.equals("line")
                & !viewName.equals("team")) {
            setTitle(title + " (" + viewLabel + " " + viewName + ")");
        } else {
            setTitle(title);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setTitleWithView(viewName=" + viewName + ")");
        }
    }

}

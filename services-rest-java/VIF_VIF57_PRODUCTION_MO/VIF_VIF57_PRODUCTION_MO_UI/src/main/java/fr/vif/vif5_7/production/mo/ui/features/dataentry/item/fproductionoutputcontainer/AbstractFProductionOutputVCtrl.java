/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFProductionOutputVCtrl.java,v $
 * Created on 24 févr. 2016 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer;


import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.constant.EntityConstants;
import fr.vif.vif57.stock.entity.business.services.constant.EntityEnum.EntityUnitType;
import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.ContainerEntity;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityUnitInfos;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.Quantities;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.location.business.beans.common.location.LocationKey;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationBBean;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationSBean;
import fr.vif.vif5_7.gen.packaging.business.beans.common.packaging.PackagingProperties;
import fr.vif.vif5_7.gen.packaging.business.beans.common.tattooed.TattooedKey;
import fr.vif.vif5_7.gen.packaging.business.beans.dialog.DOpenTattooedBean;
import fr.vif.vif5_7.gen.packaging.business.beans.dialog.DPackagingBean;
import fr.vif.vif5_7.gen.packaging.business.beans.features.fpackaging.FPackagingSBean;
import fr.vif.vif5_7.gen.packaging.ui.dialog.DPackagingCtrl;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.dialog.printdocument.DPrintDocumentBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerVBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerWorkBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompCBS;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.dialog.printdocument.DPrintDocumentCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.AbstractFProductionFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.AbstractFProductionVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionHelper;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityEnum;
import fr.vif.vif5_7.stock.entity.business.beans.dialog.container.AbstractDContainerBean;
import fr.vif.vif5_7.stock.entity.business.beans.dialog.container.DContainerBean;
import fr.vif.vif5_7.stock.entity.ui.dialog.container.DContainerCtrl;
import fr.vif.vif5_7.stock.entity.ui.dialog.location.DLocationCtrl;
import fr.vif.vif5_7.stock.kernel.StockHelper;
import fr.vif.vif5_7.stock.kernel.business.beans.dialog.reasonentry.DReasonEntryBean;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingSBean;


/**
 * The production output feature viewer controller abstract class.
 *
 * @author cj
 * @param <V> the viewer bean extends FProductionOutputContainerVBean
 * @param <C> the CBS extends FProductionOutputContainerCBS
 */
@SuppressWarnings("unchecked")
public abstract class AbstractFProductionOutputVCtrl<V extends FProductionOutputContainerVBean, C extends FProductionOutputContainerCBS>
extends AbstractFProductionVCtrl<V> implements DialogChangeListener {
    /** LOGGER. */
    private static final Logger                LOGGER                        = Logger.getLogger(AbstractFProductionOutputVCtrl.class);

    /** Feature View. **/
    private FeatureView                        featureView;
    private AbstractFProductionOutputFCtrl<C>  featureCtrl                   = null;
    private MovementAct                        previous                      = null;

    private FProductionOutputContainerWorkBean workBean                      = new FProductionOutputContainerWorkBean();

    private C                                  fproductionOutputContainerCBS = null;

    private FProductionBCtrl                   browserCtrl                   = null;

    /**
     * ask number of document to print by document.
     * 
     * @param lst list of documents
     */
    public void askNumDocument(final List<PrintDocument> lst) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - askNumDocument()");
        }

        for (PrintDocument document : lst) {
            DPrintDocumentCtrl ctrl = new DPrintDocumentCtrl();
            DPrintDocumentBean dBean = new DPrintDocumentBean(document);
            ctrl.showDocument(getFeatureView(), this, getModel().getIdentification(), dBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - askNumDocument()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogCancelled(event=" + event + ")");
        }
        if (event.getSource() instanceof DLocationCtrl) {
            getBean().setIsLocationConfirmed(false);
        } else {
            getModel().getBean().setCriteriaChecked(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogCancelled(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }

        if (event.getDialogBean() instanceof CriteriaDBean) {
            CriteriaDBean criteriaDBean = (CriteriaDBean) event.getDialogBean();
            getModel().getBean().setBeanCriteria(criteriaDBean.getCriteriaReturnInitBean());
            getModel().getBean().getBeanCriteria().setAutoOpening(false);
            getModel().getBean().setCriteriaChecked(true);
        } else if (event.getDialogBean() instanceof DReasonEntryBean) {
            getModel().getBean().setReasonCL(((DReasonEntryBean) event.getDialogBean()).getReasonCL());
            getModel().getBean().setReasonCheck(true);
        } else if (event.getDialogBean() instanceof DOpenTattooedBean) {
            DOpenTattooedBean packagingBean = (DOpenTattooedBean) event.getDialogBean();
            if (packagingBean.getPackagingCL() != null //
                    && StockHelper.isValid(packagingBean.getPackagingCL().getCode()) //
                    && StockHelper.isValid(packagingBean.getKey().getNotat())) {

                ContainerEntity containerEntity = null;
                try {
                    containerEntity = getFproductionOutputContainerCBS().getContainerByTattooed(getIdCtx(),
                            packagingBean);
                } catch (BusinessException e) {
                    fireErrorsChanged(new UIException(e));
                }

                if (containerEntity != null) {
                    getModel().getBean().getEntity().getEntityLocationBean()
                            .setEntityLocationKey(containerEntity.getEntityLocation());
                    try {
                        validateBean(false, FProductionOutputContainerVBeanEnum.CONTAINER_NUMBER.getValue(),
                                containerEntity.getEntityLocation().getContainerNumber().getBytes());
                    } catch (UIException e) {
                        getView().show(e);
                    }
                }
            }
        } else if (event.getDialogBean() instanceof DPackagingBean) {
            DPackagingBean packagingBean = (DPackagingBean) event.getDialogBean();
            if (packagingBean.getIsPackagingUpper()) {
                getBean().getEntity().getEntityLocationBean().getUpperPackaging()
                        .setTattooedId(packagingBean.getKey().getNotat());
            } else {
                getBean().getEntity().getEntityLocationBean().getContainerPackaging()
                .setTattooedId(packagingBean.getKey().getNotat());
            }
        } else if (event.getDialogBean() instanceof DContainerBean) {
            DContainerBean containerBean = (DContainerBean) event.getDialogBean();
            if (!StringUtils.isEmpty(containerBean.getContainer().getEntityLocation().getContainerNumber())) {
                getModel().getBean().getEntity().getEntityLocationBean()
                .setEntityLocationKey(containerBean.getContainer().getEntityLocation());
                try {
                    validateBean(false, FProductionOutputContainerVBeanEnum.CONTAINER_NUMBER.getValue(), containerBean
                            .getContainer().getEntityLocation().getContainerNumber().getBytes());
                } catch (UIException e) {
                    getView().show(e);
                }
            }
        } else if (event.getSource() instanceof DLocationCtrl) {
            getBean().setIsLocationConfirmed(true);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fireButtonStatechanged() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fireButtonStatechanged()");
        }

        super.fireButtonStatechanged();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fireButtonStatechanged()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fireErrorsChanged() {
        super.fireErrorsChanged();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fireErrorsChanged(final UIErrorsException exception) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fireErrorsChanged(exception=" + exception + ")");
        }

        super.fireErrorsChanged(exception);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fireErrorsChanged(exception=" + exception + ")");
        }
    }

    /**
     * Gets the featureView.
     * 
     * @category getter
     * @return the featureView.
     */
    public FeatureView getFeatureView() {
        return featureView;
    }

    /**
     * Gets the fproductionOutputContainerCBS.
     *
     * @return the fproductionOutputContainerCBS.
     */
    public C getFproductionOutputContainerCBS() {
        return fproductionOutputContainerCBS;
    }

    /**
     * Gets the previous.
     * 
     * @category getter
     * @return the previous.
     */
    public MovementAct getPrevious() {
        return previous;
    }

    /**
     * Gets the view.
     * 
     * @return the view.
     */
    public FProductionOutputContainerVIView getViewerView() {
        return (FProductionOutputContainerVIView) getView();
    }

    /**
     * Gets the workBean.
     * 
     * @category getter
     * @return the workBean.
     */
    @Override
    public FProductionOutputContainerWorkBean getWorkBean() {
        return workBean;
    }

    /**
     * Is data entry ok to validate ?
     * 
     * @return Is data entry ok to validate ?
     */
    public boolean isDataEntryOK() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isDataEntryOK()");
        }

        boolean ret = true;
        if (getBean() == null) {
            ret = false;
        } else if (!"".equals(getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getUnit())
                && getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getQty() == 0) {
            ret = false;
        } else if (!"".equals(getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().getUnit())
                && getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().getQty() == 0) {
            ret = false;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isDataEntryOK()=" + ret);
        }
        return ret;
    }

    /**
     * Check the criteria.
     * 
     */
    public void manageCriteriaWindow() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageCriteriaWindow()");
        }

        if (getBean().getBeanCriteria() != null) {
            if (!getBean().getCriteriaChecked() && getBean().getBeanCriteria().getAutoOpening()
                    && getBean().getBeanCriteria().getListCriteria().size() > 0) {
                CriteriaDBean criteriaDBean = new CriteriaDBean(getBean().getBeanCriteria(), getBean().getEntity()
                        .getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
                WorkShopCriteriaTools.showCriteria(getFeatureView(), this, this, getModel().getIdentification(),
                        criteriaDBean);
            } else {
                getModel().getBean().setCriteriaChecked(true);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageCriteriaWindow()");
        }
    }

    /**
     * Manage finish question.
     */
    public void manageFinishQuestion() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageFinishQuestion()");
        }

        if (getBean().getFinishQuestionNeeded()) {
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29534, false), MessageButtons.YES_NO);
            if (ret == MessageButtons.YES) {
                getBean().setToFinish(true);
            } else {
                getBean().setToFinish(false);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageFinishQuestion()");
        }
    }

    /**
     * Manage the Motif Windows after criteria.
     * 
     */
    public void manageMotifWindow() {

        super.manageMotifWindow(getWorkBean(), getFeatureView(), this, this, getBean());

    }

    /**
     * Manages question (stock ...).
     * 
     * @return is ok to all questions
     */
    public boolean manageQuestion() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageQuestion()");
        }

        boolean isOk = true;
        for (String question : getBean().getQuestions()) {
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false), question,
                    MessageButtons.YES_NO);
            if (ret == MessageButtons.NO) {
                isOk = false;
                break;
            }
        }
        if (isOk) {
            getBean().getQuestions().clear();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageQuestion()=" + isOk);
        }
        return isOk;
    }

    /**
     * Remove current bean from map.
     */
    public abstract void removeCurrent();

    /**
     * Save current bean to map.
     */
    public abstract void saveCurrent();

    /**
     * Sets the featureView.
     * 
     * @category setter
     * @param featureView featureView.
     */
    public void setFeatureView(final FeatureView featureView) {
        this.featureView = featureView;
    }

    /**
     * Sets the fproductionOutputContainerCBS.
     *
     * @param fproductionOutputContainerCBS fproductionOutputContainerCBS.
     */
    public void setFproductionOutputContainerCBS(final C fproductionOutputContainerCBS) {
        this.fproductionOutputContainerCBS = fproductionOutputContainerCBS;
    }

    /**
     * Sets the previous.
     * 
     * @category setter
     * @param previous previous.
     */
    public void setPrevious(final MovementAct previous) {
        this.previous = previous;
    }

    /**
     * Set scale weight in quantities components.
     * 
     * @param result result from scale
     */
    @Override
    public void setWeightResult(final FCommScaleResult result) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setWeightResult(result=" + result + ")");
        }

        EntityUnitInfos unitInfos = getBean().getEntity().getStockItemBean().getUnitsInfos();
        Quantities k = getBean().getEntity().getStockItemBean().getKeyboardQties();
        if (EntityUnitType.WEIGHT.equals(unitInfos.getFirstUnitType())) {
            k.getFirstQty().setQty(result.getNetWeight());
        } else if (EntityUnitType.WEIGHT.equals(unitInfos.getSecondUnitType())) {
            k.getSecondQty().setQty(result.getNetWeight());
        } else if (EntityUnitType.WEIGHT.equals(unitInfos.getThirdUnitType())) {
            k.getThirdQty().setQty(result.getNetWeight());
        }
        getView().render();
        if (isDataEntryOK()) {
            performSave();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setWeightResult(result=" + result + ")");
        }
    }

    /**
     * Sets the workBean.
     * 
     * @category setter
     * @param workBean workBean.
     */
    public void setWorkBean(final FProductionOutputContainerWorkBean workBean) {
        this.workBean = workBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean pCreationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - validateBean(pCreationMode=" + pCreationMode + ", pBeanProperty=" + pBeanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }

        getBean().getEntity().getStockItemBean().getStockItem()
        .setItemId(getBean().getEntity().getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        try {
            CEntityEnum entityEnum = null;
            if (FProductionOutputContainerVBeanEnum.ITEM_ID.getValue().equals(pBeanProperty)) {
                entityEnum = CEntityEnum.ITEM_ID;
                // Reset unit and quantities
                getBean().getEntity().getStockItemBean().setKeyboardQties(new Quantities());
            } else if (FProductionInputContainerVBeanEnum.BATCH_ID.getValue().equals(pBeanProperty)) {
                // Batch
                entityEnum = CEntityEnum.BATCH;
            } else if (FProductionOutputContainerVBeanEnum.CONTAINER_NUMBER.getValue().equals(pBeanProperty)) {
                entityEnum = CEntityEnum.CONTAINER_NUMBER;
            } else if (FProductionOutputContainerVBeanEnum.UPPER_CONTAINER_NUMBER.getValue().equals(pBeanProperty)) {
                entityEnum = CEntityEnum.UPPER_CONTAINER_NUMBER;
            } else if (FProductionOutputContainerVBeanEnum.FIRST_QTY.getValue().equals(pBeanProperty)) {
                if (getBean().getOutputItemParameters().getForcedFirstQty() != 0) {
                    // we convert the quantity just if we have an initialization for the quantity from the profile
                    String secondUnit = getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty()
                            .getUnit();
                    if (secondUnit != null && !secondUnit.isEmpty()) {
                        try {
                            QtyUnit qty2 = getFproductionOutputContainerCBS().convertQty(getIdCtx(),
                                    getBean().getBBean().getItemCLs().getCode(),
                                    getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty(),
                                    secondUnit);
                            getBean().getEntity().getStockItemBean().getKeyboardQties().setSecondQty(qty2);
                        } catch (BusinessException e) {
                            LOGGER.error("", e);
                            throw new UIException(e);
                        }
                    }
                }
            }
            if (entityEnum != null) {
                getModel().setBean(
                        (V) getFproductionOutputContainerCBS().validateEntityFields(getIdCtx(), entityEnum, getBean(),
                                getWorkBean()));
                if (CEntityEnum.BATCH.equals(entityEnum)) {
                    manageCriteriaWindow();
                } else if (CEntityEnum.ITEM_ID.equals(entityEnum)) {
                    // Change batch selection
                    getViewerView().getItfBatch().setInitialHelpSelection(
                            FProductionHelper.getFDestockingSBean(getBean().getBBean().getOperationItemBean(),
                                    getBean().getEntity().getStockItemBean().getGeneralItemInfos().getItemCL()
                                    .getCode(), (FDestockingSBean) getViewerView().getItfBatch()
                                    .getInitialHelpSelection()));
                }
                getView().render();
                getView().enableComponents(true);
                fireButtonStatechanged();
            }
        } catch (BusinessException e) {
            throw new UIException(e);
        }
        getView().render();
        // if there is no initialization from the profile quantity, we can directly save the declaration
        if (isDataEntryOK() && getBean().getOutputItemParameters().getForcedFirstQty() == 0) {
            performSave();
        } else {
            saveCurrent();
        }
        getModel().setInitialBean(getBean());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - validateBean(pCreationMode=" + pCreationMode + ", pBeanProperty=" + pBeanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String pBeanProperty, final Object propertyValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - valueChanged(pBeanProperty=" + pBeanProperty + ", propertyValue=" + propertyValue + ")");
        }

        if (("entity." + CEntityEnum.BATCH.getValue()).equals(pBeanProperty) && ((String) propertyValue).isEmpty()) {
            getBean().getEntity().getStockItemBean().getStockItem().setBatch(EntityConstants.NEW);
            renderAndEnableComponents();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - valueChanged(pBeanProperty=" + pBeanProperty + ", propertyValue=" + propertyValue + ")");
        }
    }

    /**
     * Ask location.
     * 
     * @return true if validated, false otherwise
     */
    protected boolean askLocation() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - askLocation()");
        }

        boolean ret = false;

        FLocationSBean sBean = new FLocationSBean();
        sBean.setCompanyId(getIdCtx().getCompany());
        sBean.setEstablishmentId(getIdCtx().getEstablishment());
        sBean.setWareHouseId(getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCdepot());
        FLocationBBean bBean = getViewerView().openLocationHelp(this, sBean);
        if (bBean != null) {
            getBean().getEntity().getEntityLocationBean().setLocationLabel(bBean.getLocationLabel());
            getBean().getEntity().getEntityLocationBean().getEntityLocationKey().setCemp(bBean.getLocationId());
            getView().render();
            ret = true;
        } else {
            ret = false;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - askLocation()=" + ret);
        }
        return ret;
    }

    /**
     * Open a dialog to ask for a tattooed number.
     * 
     * @param vBean the viewer bean to complete
     * @param upperContainer is the tattooed for the upper container ?
     */
    protected void askTattooedNumber(final V vBean, final boolean upperContainer) {

        // Dialog bean
        DPackagingBean dialogBean = new DPackagingBean();
        if (!upperContainer) {
            dialogBean.setKey(new TattooedKey(//
                    vBean.getBBean().getOperationItemBean().getOperationItemKey().getEstablishmentKey().getCsoc(), //
                    vBean.getBBean().getOperationItemBean().getOperationItemKey().getEstablishmentKey().getCetab(), //
                    vBean.getEntity().getEntityLocationBean().getContainerPackaging().getPackagingId(), //
                    ""));
            dialogBean.setPackagingCL(new CodeLabel(vBean.getEntity().getEntityLocationBean().getContainerPackaging()
                    .getPackagingId(), null));
        } else {
            dialogBean.setKey(new TattooedKey(//
                    vBean.getBBean().getOperationItemBean().getOperationItemKey().getEstablishmentKey().getCsoc(), //
                    vBean.getBBean().getOperationItemBean().getOperationItemKey().getEstablishmentKey().getCetab(), //
                    vBean.getEntity().getEntityLocationBean().getUpperPackaging().getPackagingId(), //
                    ""));
            dialogBean.setPackagingCL(new CodeLabel(vBean.getEntity().getEntityLocationBean().getUpperPackaging()
                    .getPackagingId(), null));
        }
        dialogBean.setProperties(new PackagingProperties());
        dialogBean.getProperties().setTtat(true);
        dialogBean.setIsPackagingEnabled(false);
        dialogBean.setIsPackagingUpper(upperContainer);

        // Selection
        FPackagingSBean selection = new FPackagingSBean(getBean().getBBean().getOperationItemBean()
                .getOperationItemKey().getEstablishmentKey().getCsoc());
        selection.setTattooedPackagingOnly(true);
        selection.setNotTattooedPackagingOnly(false);
        selection.setOnlyAvailableTattooedNumber(false);
        selection.setSuperiorPackagingOnly(false);

        DPackagingCtrl.showPackaging(
                this.getFeatureView(),
                this,
                this,
                this.getModel().getIdentification(),
                dialogBean,
                selection,
                I18nClientManager.translate(StockKernel.T1588) + " ("
                        + I18nClientManager.translate(StockKernel.T17646, false) + ")");
    }

    /**
     * Gets the browserCtrl.
     *
     * @return the browserCtrl.
     */
    protected FProductionBCtrl getBrowserCtrl() {
        return browserCtrl;
    }

    /**
     * Gets the featureCtrl.
     *
     * @return the featureCtrl.
     */
    protected AbstractFProductionOutputFCtrl<C> getFeatureCtrl() {
        return featureCtrl;
    }

    /**
     * Init first and second quantity.
     */
    protected void initVBeanQuantities() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initVBeanQuantities()");
        }

        getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().setQty(0.0);
        getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().setQty(0.0);
        if (getBean().getOutputItemParameters().getForcedFirstUnit() != null
                && !getBean().getOutputItemParameters().getForcedFirstUnit().isEmpty()) {
            getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty()
            .setQty(getBean().getOutputItemParameters().getForcedFirstQty());
        }
        if (getBean().getEntity().getStockItemBean().getEnableFields().getSecondQty()) {
            getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty()
            .setQty(getBean().getOutputItemParameters().getForcedSecondQty());
        }
        getView().render();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initVBeanQuantities()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean ret = false;
        if (FProductionOutputContainerFCtrl.BTN_FEATURES_REFERENCE.equals(reference)) {
            ret = true;
        } else if (FProductionOutputContainerFCtrl.BTN_WEIGHT_REFERENCE.equals(reference)) {
            if (getWorkBean().getScale() != null //
                    && getBean() != null //
                    && (FProductionTools.isValid(getBean())) //
                    && (ManagementType.REAL.equals(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                            .getManagementType()) //
                    && ((EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                            .getFirstUnitType())) //
                    || (EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                            .getSecondUnitType()) && getBean().getEntity().getStockItemBean().getEnableFields()
                            .getSecondQty())))) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FProductionOutputContainerFCtrl.BTN_DETAIL_REFERENCE.equals(reference)) {
            if (FProductionTools.isValid(getBean())) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FProductionOutputContainerFCtrl.BTN_FINISH_REFERENCE.equals(reference)) {
            if (FProductionTools.isValid(getBean())) {
                if (ManagementType.ARCHIVED.equals(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                        .getManagementType())) {
                    ret = getWorkBean().getHasRightForUnfinishing();
                } else {
                    ret = true;
                }
            } else {
                ret = false;
            }
        } else if (FProductionOutputContainerFCtrl.BTN_FPRODUCTION_INPUT_REFERENCE.equals(reference)) {
            ret = getWorkBean().getInputAvailable();
        } else if (FProductionOutputContainerFCtrl.BTN_SHOW_ALL_REFERENCE.equals(reference)) {
            ret = true;
        } else if (FProductionOutputContainerFCtrl.BTN_CANCELLAST_REFERENCE.equals(reference)) {
            if (getPrevious() != null) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FProductionOutputContainerFCtrl.BTN_CLOSE_CONTAINER.equals(reference)) {
            ret = isContainerOpened();
        } else if (FProductionOutputContainerFCtrl.BTN_PDF_REFERENCE.equalsIgnoreCase(reference)) {
            ret = FProductionTools.isValid(getBean());
        } else if (FProductionOutputContainerFCtrl.BTN_OPEN_CONTAINER.equalsIgnoreCase(reference)) {
            ret = !isContainerOpened();
        } else if (FProductionOutputContainerFCtrl.BTN_OPEN_TATTOOED.equalsIgnoreCase(reference)) {
            ret = !isContainerOpened();
            // TODO Constant ?
        } else if ("features".equalsIgnoreCase(reference)) {
            ret = FProductionTools.isValid(getBean());
        } else if (AbstractFProductionFCtrl.BTN_OPEN_INCIDENT_REFERENCE.equalsIgnoreCase(reference)) {
            ret = true;
        } else if (FProductionOutputContainerFCtrl.BTN_VALIDATE_REFERENCE.equals(reference)) {
            if (getBean().getOutputItemParameters().getForcedFirstQty() == 0
                    || ManagementType.ARCHIVED.equals(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                            .getManagementType())) {
                ret = false;
            } else {
                ret = true;
            }
        } else if (ButtonReference.SETTING_INFORMATION.getReference().equals(reference)) {
            ret = true;
        } else if (ButtonReference.DEBUG_REFERENCE.getReference().equals(reference)) {
            ret = true;
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    /**
     * Open closed container.
     */
    protected void openContainer() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openContainer()");
        }
        AbstractDContainerBean containerBean = new DContainerBean();
        containerBean.setEstablishmentKey(new EstablishmentKey(getModel().getIdentification().getCompany(), getModel()
                .getIdentification().getEstablishment()));
        DContainerCtrl.showContainer(this.getFeatureView(), this, this, getModel().getIdentification(), containerBean,
                I18nClientManager.translate(ProductionMo.T39088, false));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openContainer()");
        }
    }

    /**
     * Open closed container by entry tattooed number.
     */
    protected void openTattooed() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openTattooed()");
        }
        // Dialog bean
        DOpenTattooedBean dialogBean = new DOpenTattooedBean(new TattooedKey(//
                getModel().getIdentification().getCompany(), //
                getModel().getIdentification().getEstablishment(), //
                getBean().getEntity().getEntityLocationBean().getContainerPackaging().getPackagingId(), ""));
        dialogBean.setProperties(new PackagingProperties());
        dialogBean.getProperties().setTtat(true);
        dialogBean.setIsPackagingEnabled(true);

        // Selection
        FPackagingSBean selection = new FPackagingSBean(getBean().getBBean().getOperationItemBean()
                .getOperationItemKey().getEstablishmentKey().getCsoc());
        selection.setTattooedPackagingOnly(true);
        selection.setNotTattooedPackagingOnly(false);
        selection.setOnlyAvailableTattooedNumber(false);
        selection.setSuperiorPackagingOnly(false);

        DPackagingCtrl.showPackaging(this.getFeatureView(), this, this, this.getModel().getIdentification(),
                dialogBean, selection, I18nClientManager.translate(ProductionMo.T39182, false));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openTattooed()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected synchronized boolean performSave() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - performSave()");
        }

        boolean b = false;
        boolean ok = false;

        // Ask location if needed and if there is a depository, no location and with location no optimise
        if (getBean().getEntity().getEntityLocationBean().getEnableFields().getLocation()
                && StringUtils.isEmpty(getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCemp())
                & !getBean().getHasDepositWithLocationOptimise()) {
            ok = askLocation();
        } else {
            ok = true;
        }
        // Ask for a tattooed number if needed
        if (getBean().getEntity().getEntityLocationBean().getContainerPackagingProperties().getTtat()
                && StringUtils.isEmpty(getBean().getEntity().getEntityLocationBean().getContainerPackaging()
                        .getTattooedId())) {
            askTattooedNumber(getBean(), false);
        }
        if (getBean().getEntity().getEntityLocationBean().getUpperPackagingProperties().getTtat()
                && StringUtils.isEmpty(getBean().getEntity().getEntityLocationBean().getUpperPackaging()
                        .getTattooedId())) {
            askTattooedNumber(getBean(), true);
        }

        if (ok) {
            b = super.performSave();
            if (b && !getBean().getAutoInsert()) {
                // Data entry not inserted
                manageCriteriaWindow();
                if (getBean().getCriteriaChecked()) {
                    boolean ret = manageQuestion();
                    if (ret) {
                        manageMotifWindow();
                        if (getBean().isReasonCheck()) {
                            manageFinishQuestion();
                            getBean().setForceUpdate(true);
                            // Try again to insert data entry
                            performSave();
                        } else {
                            initVBeanQuantities();
                        }
                    } else {
                        b = false;
                        initVBeanQuantities();
                    }
                } else {
                    initVBeanQuantities();
                }
            } else {
                // Data entry inserted
                if (!getBean().getToFinish()) {
                    // Output not finished, so we save VBean into map
                    saveCurrent();
                } else {
                    // Output finished so we try to remove Vbean from map
                    removeCurrent();
                }
                if (getBean().getDocuments().size() > 0) {
                    askNumDocument(getBean().getDocuments());
                    try {
                        V vBean = (V) getFproductionOutputContainerCBS().printDocuments(getIdCtx(), getBean(),
                                getBean().getDocuments(), getWorkBean());

                        getModel().setBean(vBean);
                        getModel().setInitialBean(vBean);
                        getView().render();
                        fireBeanUpdated(vBean);
                    } catch (BusinessException e) {
                        fireErrorsChanged(new UIException(e));
                    }
                    getBean().getDocuments().clear();
                }
                if (!getWorkBean().getShowFinished() && getBean().getToFinish()) {
                    browserCtrl.reopenQuery();
                    fireErrorsChanged();
                }
                getBean().setAutoInsert(false);
                getBean().setForceUpdate(false);
                getBean().setCriteriaChecked(false);
                getBean().setReasonCheck(false); // MP QA8239 permet de demander à nouveau le motif lors de multiple
                // saisies
                getBean().setToFinish(false);

                // this event is too early fired (before getting all informations) => we have to fire it now
                fireButtonStatechanged();
            }
        } else {
            initVBeanQuantities();
        }

        // DC6074 - Go back to MO list if save successed and workBean.getReturnToMOList() is true
        if (b && workBean.getReturnToMOList()) {
            fireSelfFeatureClosingRequested();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - performSave()=" + b);
        }
        return b;
    }

    /**
     * Sets the browserCtrl.
     *
     * @param browserCtrl browserCtrl.
     */
    protected void setBrowserCtrl(final FProductionBCtrl browserCtrl) {
        this.browserCtrl = browserCtrl;
    }

    /**
     * Sets the featureCtrl.
     *
     * @param featureCtrl featureCtrl.
     */
    protected void setFeatureCtrl(final AbstractFProductionOutputFCtrl<C> featureCtrl) {
        this.featureCtrl = featureCtrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected V updateBean(final V pBean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(pBean=" + pBean + ")");
        }

        V vBean = pBean;
        try {
            if (vBean.getHasDepositWithLocationOptimise() && !vBean.getIsLocationConfirmed()) {
                // can the user change the location ?
                if (getWorkBean().getOutputWorkstationParameters().getCtrlLocation() != 4) {
                    // we just do the control for new NSC or new NSCP or if the profile manages batch or NSP
                    if (OutputEntityType.BATCH_ITEM.equals(vBean.getOutputItemParameters().getEntityType())
                            || (OutputEntityType.CONTAINER.equals(vBean.getOutputItemParameters().getEntityType()) && EntityConstants.NEW
                                    .equals(vBean.getEntity().getEntityLocationBean().getEntityLocationKey()
                                            .getContainerNumber()))
                            || (OutputEntityType.CONTAINER_UPPER_CONTAINER.equals(vBean.getOutputItemParameters()
                                    .getEntityType()) && EntityConstants.NEW.equals(vBean.getEntity()
                                    .getEntityLocationBean().getEntityLocationKey().getUpperContainerNumber()))
                            || OutputEntityType.EU.equals(vBean.getOutputItemParameters().getEntityType())) {

                        // search the best location
                        List<String> locationlstId = getFproductionOutputContainerCBS().searchAndLockLocation(
                                getIdCtx(), vBean, true);
                        CodeLabel locationCL = new CodeLabel(vBean.getEntity().getEntityLocationBean()
                                .getEntityLocationKey().getCemp(), "");
                        if (locationlstId != null && locationlstId.size() > 0) {
                            locationCL = new CodeLabel(locationlstId.get(0), "");
                        }
                        DLocationCtrl.confirmLocation(getFeatureView(), this, this,
                                this.getModel().getIdentification(), vBean.getEntity().getEntityLocationBean()
                                .getEntityLocationKey().getCdepot(), locationCL, getWorkBean()
                                .getOutputWorkstationParameters().getCtrlLocation(), locationlstId, vBean
                                .getEntity().getEntityLocationBean().getEntityLocationKey()
                                .getContainerNumber());

                        // unlock the locked location (the first of the proposed list
                        if (locationlstId != null && locationlstId.size() > 0) {
                            getFproductionOutputContainerCBS().unLockLocation(
                                    getIdCtx(),
                                    new LocationKey(vBean.getEntity().getEntityLocationBean().getEntityLocationKey()
                                            .getCsoc(), vBean.getEntity().getEntityLocationBean()
                                            .getEntityLocationKey().getCetab(), vBean.getEntity()
                                            .getEntityLocationBean().getEntityLocationKey().getCdepot(), locationlstId
                                            .get(0)));
                        }

                        // if location is not set --> stop validation
                        if (!vBean.getIsLocationConfirmed() || !EntityTools.isValid(locationCL.getCode())) {
                            throw new UIException(StockKernel.T18703);
                        } else {
                            vBean.getEntity().getEntityLocationBean().getEntityLocationKey()
                            .setCemp(locationCL.getCode());
                        }
                    }
                }
            }
            if (vBean instanceof FProductionOutputCompVBean) {
                vBean = (V) ((FProductionOutputCompCBS) getFproductionOutputContainerCBS()).updateBean(getIdCtx(),
                        vBean, getWorkBean());
            } else {
                vBean = (V) getFproductionOutputContainerCBS().updateBean(getIdCtx(), vBean, getWorkBean());
            }
            if (vBean.getAutoInsert()) {
                setPrevious(vBean.getPreviousMovementAct());
                getWorkBean().setInputAvailable(vBean.getInputAvailable());
            }
            // ALB Q9042 Go back because problem when weight is set manually.
            // // FB M00172129 : If scale is connected, QT2 field is set with 0.0
            // if (getWorkBean().getScale() != null
            // && vBean.getEntity().getStockItemBean().getEnableFields().getSecondQty()
            // && EntityUnitType.WEIGHT.equals(vBean.getEntity().getStockItemBean().getUnitsInfos()
            // .getSecondUnitType())) {
            // vBean.getEntity().getStockItemBean().getKeyboardQties().getSecondQty().setQty(0.0);
            // }
            // // FB M00172129
        } catch (BusinessException e) {
            initVBeanQuantities();
            UIException ue = new UIException(e);
            getModel().setInitialBean(getBean());
            // fireErrorsChanged(ue);
            throw ue;
        } catch (BusinessErrorsException e) {
            initVBeanQuantities();
            UIErrorsException ue = new UIErrorsException(e);
            getModel().setInitialBean(getBean());
            // fireErrorsChanged(ue);
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * Return true if a container is opened.
     * 
     * @return true if a container is opened
     */
    private boolean isContainerOpened() {
        boolean ret = false;
        if ((!EntityConstants.NEW.equals(getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                .getUpperContainerNumber()) //
        && !getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getUpperContainerNumber().isEmpty())
                || (!EntityConstants.NEW.equals(getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                        .getContainerNumber()) //
                && !getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getContainerNumber().isEmpty())) {
            ret = true;

        }
        return ret;
    }

}

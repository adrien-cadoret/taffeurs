/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMO2ValuesCell.java,v $
 * Created on 22 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.swing;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.DecimalFormat;

import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.FontNames;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOValues;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.IndicatorTypeValueEnum;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FIndicatorMOCellProgressBar;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FSupervisorMOPanel;


/**
 * Cell of a JTable.
 * 
 * @author jd
 */
public class FIndicatorMO2ValuesCell extends FSupervisorMOPanel implements FIndicatorMOICell {
    private static final Format         CODE_FONT            = new Format(Alignment.LEFT_ALIGN, null,
            StandardColor.GRAY, new CustomFont(
                    FontNames.DEFAULT, FontStyle.BOLD, 14),
                    "30");
    private static DecimalFormat        decFormat            = new DecimalFormat("#####0.0");
    private static final Format         INFO_FONT            = new Format(Alignment.LEFT_ALIGN, null,
            StandardColor.WHITE, new CustomFont(
                    FontNames.DEFAULT, FontStyle.BOLD, 12),
                    "30");
    // private static DecimalFormat decimalFormat = new DecimalFormat("#####0");
    private static DecimalFormat        intFormat            = new DecimalFormat("#####0.0");
    private static final Format         LABEL_FONT           = new Format(Alignment.LEFT_ALIGN, null,
            StandardColor.GRAY, new CustomFont(
                    FontNames.DEFAULT, FontStyle.PLAIN, 12),
                    "30");

    private SwingInputTextField         code                 = null;
    private FIndicatorMOCellProgressBar indicatorMOCellProgressBar;
    private FIndicatorMOCellProgressBar indicatorProgressBar = null;

    private SwingInputTextField         info;
    private SwingInputTextField         label                = null;
    private String                      title                = "";

    private StringBuilder               toolTipBuilder       = new StringBuilder();

    /**
     * Constructor.
     */
    public FIndicatorMO2ValuesCell() {
        super();
        init();
    }

    /**
     * Constructor.
     * 
     * @param title title
     */
    public FIndicatorMO2ValuesCell(final String title) {
        super();
        this.title = title;
        init();
    }

    /**
     * {@inheritDoc}
     */
    public void setValues(final IndicatorMOValues bbean) {

        getCode().setValue(bbean.getIndicatorCL().getCode());
        getLabel().setValue(bbean.getIndicatorCL().getLabel());

        String unit = bbean.getValue1().getUnitID();

        double percent = 0;

        percent = bbean.getPercent();

        getProgressBar1().getProgressBar().setMaximum(100);
        getProgressBar1().getProgressBar().setValue(0);
        getProgressBar2().getProgressBar().setMaximum(100);
        getProgressBar2().getProgressBar().setValue(0);

        // percent = 10 - Math.random() * 20;
        Color color = FSupervisorMOTools.calculateColor(bbean.getTypeValue(), percent);
        getInfo().getJTextField().setForeground(color);
        if (percent < 0) {

            getProgressBar1().getProgressBar().setMaximum(100);
            getProgressBar1().getProgressBar().setValue(100 + (int) percent);

            /* FB DP2581 progressBar : Gray background */
            // getProgressBar1().getProgressBar().setForeground(Color.WHITE);
            getProgressBar1().getProgressBar().setForeground(TouchHelper.getAwtColor(StandardColor.BG_BROWSER));

            getProgressBar1().getProgressBar().setBackground(color);

        } else {
            getProgressBar2().getProgressBar().setMaximum(100);
            getProgressBar2().getProgressBar().setValue((int) percent);

            /* FB DP2581 progressBar : Gray background */
            // getProgressBar2().getProgressBar().setBackground(Color.WHITE);
            getProgressBar2().getProgressBar().setBackground(TouchHelper.getAwtColor(StandardColor.BG_BROWSER));

            getProgressBar2().getProgressBar().setForeground(color);

        }
        double done1 = 0;
        double done2 = 0;
        double theo = 0;
        String infoToShow = "";
        switch (bbean.getTypeValue()) {
            case PRODUCTIVITY:
                infoToShow = intFormat.format(percent) + "% (" + intFormat.format(bbean.getResult().getEffectiveQty())
                + " " + bbean.getResult().getUnitID() + " "
                + intFormat.format(bbean.getResult().getExpectedQty()) + " " + bbean.getResult().getUnitID()
                + ")";
                break;
            case EFFICIENCY:
                infoToShow = intFormat.format(percent) + "% (" + intFormat.format(bbean.getResult().getEffectiveQty())
                + " " + bbean.getResult().getUnitID() + " "
                + intFormat.format(bbean.getResult().getExpectedQty()) + " " + bbean.getResult().getUnitID()
                + ")";
                break;
            case GARBAGE:
                infoToShow = intFormat.format(percent) + "% (" + intFormat.format(bbean.getResult().getEffectiveQty())
                + " " + bbean.getResult().getUnitID() + " "
                + intFormat.format(bbean.getResult().getExpectedQty()) + " " + bbean.getResult().getUnitID()
                + ")";
                break;
            case ADVANCE_QTY:
                infoToShow = "";
                // barre 1 = but 1 = quantité réelle OT - (durée réelle OT * (quantité prévue OT / durée prévue OT))

                // barre 2 = but 2 = ((durée prévue OT * (quantité réelle OT / durée réelle OT)) - quantité prévue OT) /
                // quantité prévue OT

                // done1 = qty advance
                if (bbean.getValue2().getExpectedQty() != 0) { // Expected may be 0 SIC ...
                    done1 = bbean.getValue1().getEffectiveQty()
                            - (bbean.getValue2().getEffectiveQty() * (bbean.getValue1().getExpectedQty() / bbean
                                    .getValue2().getExpectedQty()));
                }

                // done2 = qty = porjected
                if (bbean.getValue2().getEffectiveQty() != 0) { // may be 0 SIC ...
                    done2 = bbean.getValue2().getExpectedQty()
                            * (bbean.getValue1().getEffectiveQty() / (bbean.getValue2().getEffectiveQty()))
                            - bbean.getValue1().getExpectedQty();
                }

                infoToShow = // I18nClientManager.translate(ProductionMo.T32646) + " " + //
                        intFormat.format(percent) + "% (" + //
                        // I18nClientManager.translate(ProductionMo.T32647) + " " + //
                        intFormat.format(done1) + " " + unit + " / " + //
                        // I18nClientManager.translate(ProductionMo.T32648) + " " +//
                        intFormat.format(done2) + " " + unit + ")";
                break;
            case ADVANCE_TIME:
                infoToShow = "";
                // barre 1 = but 1 = (quantité réelle OT / (quantité prévue OT / durée prévue OT)) - durée réelle OT

                // barre 2 = but 2 = (durée prévue OT - (quantité prévue OT / (quantité réelle OT / durée réelle OT))) /
                // durée prévue OT

                // done1 = seconds = advance
                done1 = (bbean.getValue1().getEffectiveQty() / (bbean.getValue1().getExpectedQty() / bbean.getValue2()
                        .getExpectedQty())) - bbean.getValue2().getEffectiveQty();

                // done2 = seconds = projected
                done2 = (bbean.getValue2().getExpectedQty() - (bbean.getValue1().getExpectedQty() / (bbean.getValue1()
                        .getEffectiveQty() / bbean.getValue2().getEffectiveQty())));

                infoToShow = intFormat.format(percent) + "% (" //
                        + FSupervisorMOTools.calculateHourInterval(((long) done1) * 1000) + " / " //
                        + FSupervisorMOTools.calculateHourInterval(((long) done2) * 1000) + ")";

                break;
            case GAP:
                infoToShow = "";
                theo = bbean.getValue1().getExpectedQty() * bbean.getValue2().getEffectiveQty()
                        / bbean.getValue2().getExpectedQty();

                infoToShow = intFormat.format(percent) + "% (" + intFormat.format(bbean.getValue1().getEffectiveQty())
                        + " " + unit + " / " + intFormat.format(theo) + " " + unit + ")";
                break;
            case GAP_OUTPUT:
                infoToShow = "";
                theo = bbean.getValue1().getExpectedQty() * bbean.getValue2().getEffectiveQty()
                        / bbean.getValue2().getExpectedQty();

                infoToShow = intFormat.format(percent) + "% (" + intFormat.format(bbean.getValue1().getEffectiveQty())
                        + " " + unit + " / " + intFormat.format(theo) + " " + unit + ")";
                break;
            default:
                break;

        }
        getInfo().setValue(infoToShow);

        setToolTip(bbean);

    }

    /**
     * Add Row with n cols.
     * 
     * @param col list of strings to add
     */
    private void addRow(final String... col) {
        toolTipBuilder.append("<tr>");

        for (String c : col) {
            toolTipBuilder.append("<TD><A>");
            toolTipBuilder.append(c);
            toolTipBuilder.append("</A></TD>");
        }
        toolTipBuilder.append("</TR>");

    }

    /**
     * get a textfield.
     * 
     * @return {@link SwingInputTextField}
     */
    private SwingInputTextField getCode() {
        if (code == null) {
            code = new SwingInputTextField(String.class, "30", false, null);
            code.setEnabled(false);
            code.setAlwaysDisabled(true);
            code.setUpdateable(false);
            code.setVisible(true);
            code.setOpaque(false);
            code.setBorder(null);
            code.setFieldFormat(CODE_FONT);
            code.getJTextField().setOpaque(false);
            code.getJTextField().setBorder(null);
        }
        return code;
    }

    /**
     * get a textfield.
     * 
     * @return {@link SwingInputTextField}
     */
    private SwingInputTextField getInfo() {
        if (info == null) {
            info = new SwingInputTextField(String.class, "60", false, null);
            info.setEnabled(false);
            info.setAlwaysDisabled(true);
            info.setUpdateable(false);
            info.setVisible(true);
            info.setOpaque(false);
            info.setBorder(null);
            info.setFieldFormat(INFO_FONT);
            info.getJTextField().setOpaque(false);
            info.getJTextField().setBorder(null);
            info.getJTextField().setAlignmentX(CENTER_ALIGNMENT);
        }
        return info;
    }

    /**
     * get a textfield.
     * 
     * @return {@link SwingInputTextField}
     */
    private SwingInputTextField getLabel() {
        if (label == null) {
            label = new SwingInputTextField(String.class, "30", false, null);
            label.setEnabled(false);
            label.setAlwaysDisabled(true);
            label.setUpdateable(false);
            label.setVisible(true);
            label.setOpaque(false);
            label.setBorder(null);
            label.setFieldFormat(LABEL_FONT);
            label.getJTextField().setOpaque(false);
            label.getJTextField().setBorder(null);
        }
        return label;
    }

    /**
     * Get the progress bar.
     * 
     * @return a {@link FIndicatorMOCellProgressBar}
     */
    private FIndicatorMOCellProgressBar getProgressBar1() {
        if (indicatorProgressBar == null) {
            indicatorProgressBar = new FIndicatorMOCellProgressBar();
        }
        return indicatorProgressBar;
    }

    /**
     * Get the progress bar.
     * 
     * @return a {@link FIndicatorMOCellProgressBar}
     */
    private FIndicatorMOCellProgressBar getProgressBar2() {
        if (indicatorMOCellProgressBar == null) {
            indicatorMOCellProgressBar = new FIndicatorMOCellProgressBar();
        }
        return indicatorMOCellProgressBar;
    }

    /**
     * init.
     */
    private void init() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        setLayout(gridBagLayout);

        setBorder(null);

        setOpaque(false);

        GridBagConstraints gbctextField = new GridBagConstraints();
        gbctextField.insets = new Insets(0, 0, 5, 5);
        gbctextField.weighty = 1.0;
        gbctextField.weightx = 0.5;
        gbctextField.fill = GridBagConstraints.HORIZONTAL;
        gbctextField.gridx = 0;
        gbctextField.gridy = 0;
        add(getCode(), gbctextField);

        GridBagConstraints gbctextField2 = new GridBagConstraints();
        gbctextField2.insets = new Insets(0, 0, 5, 5);
        gbctextField2.weighty = 1.0;
        gbctextField2.weightx = 0.5;
        gbctextField2.fill = GridBagConstraints.HORIZONTAL;
        gbctextField2.gridx = 0;
        gbctextField2.gridy = 1;
        add(getLabel(), gbctextField2);

        GridBagConstraints gbcProgressBar1 = new GridBagConstraints();
        gbcProgressBar1.insets = new Insets(0, 0, 0, 0);
        gbcProgressBar1.weightx = 0.25;
        gbcProgressBar1.weighty = 1;
        gbcProgressBar1.fill = GridBagConstraints.BOTH;
        gbcProgressBar1.gridx = 1;
        gbcProgressBar1.gridy = 0;
        add(getProgressBar1(), gbcProgressBar1);

        GridBagConstraints gbcProgressBar2 = new GridBagConstraints();
        gbcProgressBar2.insets = new Insets(0, 0, 0, 0);
        gbcProgressBar2.weighty = 1.0;
        gbcProgressBar2.weightx = 0.25;
        gbcProgressBar2.fill = GridBagConstraints.BOTH;
        gbcProgressBar2.gridx = 2;
        gbcProgressBar2.gridy = 0;
        add(getProgressBar2(), gbcProgressBar2);

        GridBagConstraints gbctextFieldInfo = new GridBagConstraints();
        gbctextFieldInfo.gridwidth = 2;
        gbctextFieldInfo.insets = new Insets(0, 0, 5, 5);
        gbctextFieldInfo.weighty = 1.0;
        gbctextFieldInfo.weightx = 0.5;
        gbctextFieldInfo.fill = GridBagConstraints.HORIZONTAL;
        gbctextFieldInfo.gridx = 1;
        gbctextFieldInfo.gridy = 1;
        add(getInfo(), gbctextFieldInfo);

    }

    /**
     * Generate toolTip.
     * 
     * @param bbean {@link IndicatorMOValues}
     */
    private void setToolTip(final IndicatorMOValues bbean) {
        toolTipBuilder = new StringBuilder();
        toolTipBuilder.append("<html>");
        toolTipBuilder.append("<body>");

        toolTipBuilder.append("<table>");
        addRow(I18nClientManager.translate(bbean.getTypeValue().getName()), bbean.getIndicatorCL().getCode(), bbean
                .getIndicatorCL().getLabel(), "");

        switch (bbean.getTypeValue()) {
            case GARBAGE:
                addRow("", I18nClientManager.translate(ProductionMo.T32533),
                        I18nClientManager.translate(ProductionMo.T9395), "");
                break;
            case GAP:
                addRow("", I18nClientManager.translate(ProductionMo.T9399),
                        I18nClientManager.translate(ProductionMo.T32537), "");
                break;
            case GAP_OUTPUT:
                addRow("", I18nClientManager.translate(ProductionMo.T9395),
                        I18nClientManager.translate(ProductionMo.T32537), "");
                break;
            case ADVANCE_TIME:
                addRow("", I18nClientManager.translate(ProductionMo.T32537),
                        I18nClientManager.translate(GenKernel.T23302), "");
                break;
            case ADVANCE_QTY:
                addRow("", I18nClientManager.translate(ProductionMo.T32537),
                        I18nClientManager.translate(GenKernel.T23302), "");
                break;
            default:
                break;
        }

        if (IndicatorTypeValueEnum.PRODUCTIVITY.equals(bbean.getTypeValue())) {

            // addRow(I18nClientManager.translate(values.getTypeValue().getName()), values.getIndicatorCL().getCode(),
            // values.getIndicatorCL().getLabel(), "");

            addRow("", I18nClientManager.translate(ProductionMo.T32537), I18nClientManager.translate(GenKernel.T23302),
                    "");

            addRow(I18nClientManager.translate(ProductionMo.T32606), //
                    decFormat.format(bbean.getValue1().getEffectiveQty()) + " " + bbean.getValue1().getUnitID(), //

                    // DateHelper.formatDate(
                    // DateHelper.getDateTime(new Date(), (int) bbean.getValue2().getEffectiveQty()),
                    // FSupervisorMOTools.FORMAT_HOUR), //
                    FSupervisorMOTools
                    .calculateHourIntervalWithSeconds((int) (bbean.getValue2().getEffectiveQty() * 1000)),

                    decFormat.format(bbean.getResult().getEffectiveQty()) + " " + bbean.getResult().getUnitID());

            addRow(I18nClientManager.translate(ProductionMo.T32607), //
                    decFormat.format(bbean.getValue1().getExpectedQty()) + " " + bbean.getValue1().getUnitID(), //

                    // DateHelper.formatDate(DateHelper.getDateTime(new Date(), (int)
                    // bbean.getValue2().getExpectedQty()),
                    // FSupervisorMOTools.FORMAT_HOUR), //
                    FSupervisorMOTools
                    .calculateHourIntervalWithSeconds((int) (bbean.getValue2().getExpectedQty() * 1000)),

                    decFormat.format(bbean.getResult().getExpectedQty()) + " " + bbean.getResult().getUnitID());

            addRow("", //
                    "", //
                    "", //
                    decFormat.format(bbean.getResult().getEffectiveQty() - bbean.getResult().getExpectedQty()) + " "
                    + bbean.getResult().getUnitID());

        } else if (IndicatorTypeValueEnum.EFFICIENCY.equals(bbean.getTypeValue())) {

            // addRow(I18nClientManager.translate(values.getTypeValue().getName()), values.getIndicatorCL().getCode(),
            // values.getIndicatorCL().getLabel(), "");

            // if (IndicatorTypeValueEnum.EFFICIENCY.equals(values.getTypeValue())) {
            addRow("", I18nClientManager.translate(ProductionMo.T9395),
                    I18nClientManager.translate(ProductionMo.T9399), "");
            // }

            addRow(I18nClientManager.translate(ProductionMo.T32606), //
                    decFormat.format(bbean.getValue2().getEffectiveQty()) + " " + bbean.getValue2().getUnitID(), //
                    decFormat.format(bbean.getValue1().getEffectiveQty()) + " " + bbean.getValue1().getUnitID(), //
                    decFormat.format(bbean.getResult().getEffectiveQty()) + " " + bbean.getResult().getUnitID());

            addRow(I18nClientManager.translate(ProductionMo.T32607), //
                    decFormat.format(bbean.getValue2().getExpectedQty()) + " " + bbean.getValue2().getUnitID(), //
                    decFormat.format(bbean.getValue1().getExpectedQty()) + " " + bbean.getValue1().getUnitID(), //
                    decFormat.format(bbean.getResult().getExpectedQty()) + " " + bbean.getResult().getUnitID());
            addRow("", //
                    "", //
                    "", //
                    decFormat.format(bbean.getResult().getEffectiveQty() - bbean.getResult().getExpectedQty()) + " "
                    + bbean.getResult().getUnitID());
        } else if (IndicatorTypeValueEnum.ADVANCE_TIME.equals(bbean.getTypeValue())
                || IndicatorTypeValueEnum.ADVANCE_QTY.equals(bbean.getTypeValue())) {
            addRow(I18nClientManager.translate(ProductionMo.T32606), //
                    decFormat.format(bbean.getValue1().getEffectiveQty()) + " " + bbean.getValue1().getUnitID(), //
                    FSupervisorMOTools
                    .calculateHourIntervalWithSeconds((int) (bbean.getValue2().getEffectiveQty() * 1000)), //
                    "");

            addRow(I18nClientManager.translate(ProductionMo.T32607), //
                    decFormat.format(bbean.getValue1().getExpectedQty()) + " " + bbean.getValue1().getUnitID(), //
                    FSupervisorMOTools
                    .calculateHourIntervalWithSeconds((int) (bbean.getValue2().getExpectedQty() * 1000)), //
                    "");
        } else if (IndicatorTypeValueEnum.GAP.equals(bbean.getTypeValue())
                || IndicatorTypeValueEnum.GAP_OUTPUT.equals(bbean.getTypeValue())) {
            addRow(I18nClientManager.translate(ProductionMo.T32606), //
                    decFormat.format(bbean.getValue1().getEffectiveQty()) + " " + bbean.getValue1().getUnitID(), //
                    decFormat.format(bbean.getValue2().getEffectiveQty()) + " " + bbean.getValue2().getUnitID() //
                    // decFormat.format(bbean.getValue2().getEffectiveQty()) + " " + bbean.getValue2().getUnitID()
                    );

            addRow(I18nClientManager.translate(ProductionMo.T32607), //
                    decFormat.format(bbean.getValue1().getExpectedQty()) + " " + bbean.getValue1().getUnitID(), //
                    decFormat.format(bbean.getValue2().getExpectedQty()) + " " + bbean.getValue2().getUnitID() //
                    // ""
                    );

            double theo = bbean.getValue1().getExpectedQty() * bbean.getValue2().getEffectiveQty()
                    / bbean.getValue2().getExpectedQty();

            addRow(I18nClientManager.translate(ProductionMo.T32608), //
                    decFormat.format(theo) + " " + bbean.getValue1().getUnitID(), //
                    "" //
                    // decFormat.format(theo) + " " + bbean.getValue1().getUnitID()
                    );

            // addRow("", //
            // "", //
            // "", //
            // decFormat.format(bbean.getValue2().getEffectiveQty() - theo) + " " + bbean.getValue1().getUnitID());

        } else {
            addRow(I18nClientManager.translate(ProductionMo.T32606), //
                    decFormat.format(bbean.getValue1().getEffectiveQty()) + " " + bbean.getValue1().getUnitID(), //
                    decFormat.format(bbean.getValue2().getEffectiveQty()) + " " + bbean.getValue2().getUnitID(), //
                    decFormat.format(bbean.getResult().getEffectiveQty()) + " " + bbean.getResult().getUnitID());

            addRow(I18nClientManager.translate(ProductionMo.T32607), //
                    decFormat.format(bbean.getValue1().getExpectedQty()) + " " + bbean.getValue1().getUnitID(), //
                    decFormat.format(bbean.getValue2().getExpectedQty()) + " " + bbean.getValue2().getUnitID(), //
                    decFormat.format(bbean.getResult().getExpectedQty()) + " " + bbean.getResult().getUnitID());

            addRow("", //
                    "", //
                    "", //
                    decFormat.format(bbean.getResult().getEffectiveQty() - bbean.getResult().getExpectedQty()) + " "
                    + bbean.getResult().getUnitID());
        }

        toolTipBuilder.append("</table>");
        toolTipBuilder.append("</body>");
        toolTipBuilder.append("</html>");

        setToolTipText(toolTipBuilder.toString());
    }

}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: GraphMOTools.java,v $
 * Created on 9 Feb, 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph;


import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.GradientPaint;
import java.awt.RadialGradientPaint;
import java.awt.Stroke;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.annotations.XYAnnotation;
import org.jfree.chart.axis.CategoryAxis;
import org.jfree.chart.axis.DateAxis;
import org.jfree.chart.axis.NumberAxis;
import org.jfree.chart.axis.SymbolAxis;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.labels.ItemLabelAnchor;
import org.jfree.chart.labels.ItemLabelPosition;
import org.jfree.chart.labels.StandardCategoryItemLabelGenerator;
import org.jfree.chart.labels.StandardCategoryToolTipGenerator;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.DatasetRenderingOrder;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.ValueMarker;
import org.jfree.chart.plot.XYPlot;
import org.jfree.chart.renderer.category.BarRenderer;
import org.jfree.chart.renderer.category.StandardBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.chart.urls.StandardCategoryURLGenerator;
import org.jfree.data.Range;
import org.jfree.data.category.DefaultCategoryDataset;
import org.jfree.data.gantt.TaskSeries;
import org.jfree.data.gantt.TaskSeriesCollection;
import org.jfree.data.gantt.XYTaskDataset;
import org.jfree.data.xy.IntervalXYDataset;
import org.jfree.ui.Layer;
import org.jfree.ui.LengthAdjustmentType;
import org.jfree.ui.RectangleAnchor;
import org.jfree.ui.RectangleInsets;
import org.jfree.ui.TextAnchor;
import org.jfree.util.SortOrder;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FlowType;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.CumulatedIndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOQties;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOTools;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGraphMOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOGraphSelectionBean;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.GraphTypeEnum;
import fr.vif.vif5_7.production.mo.constants.Mnemos.POIndicatorState;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOData;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMODataSet;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOPeriod;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.FGanttMOAnnotation;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.FGanttMOBarPainter;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.FGanttMORenderer;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.FGanttMOTask;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.FGanttMOToolTipGenerator;


/**
 * Tools for graphs.
 * 
 * @author jd
 */
public final class GraphMOTools {
    // public static final Color CHART_COLOR1_BEGIN = new Color(255, 175, 75);
    //
    // public static final Color CHART_COLOR1_BORDER = new Color(208, 115, 0);
    // public static final Color CHART_COLOR1_END = new Color(255, 146, 10);
    // public static final Color CHART_COLOR10_BEGIN = new Color(125, 126, 125);
    // public static final Color CHART_COLOR10_BORDER = new Color(0, 0, 0);
    //
    // public static final Color CHART_COLOR10_END = new Color(14, 14, 14);
    // public static final Color CHART_COLOR2_BEGIN = new Color(207, 207, 207);
    // public static final Color CHART_COLOR2_BORDER = new Color(116, 116, 116);
    //
    // public static final Color CHART_COLOR2_END = new Color(116, 116, 116);
    // public static final Color CHART_COLOR3_BEGIN = new Color(179, 225, 255);
    // public static final Color CHART_COLOR3_BORDER = new Color(67, 142, 206);
    //
    // public static final Color CHART_COLOR3_END = new Color(101, 182, 252);
    // public static final Color CHART_COLOR4_BEGIN = new Color(204, 234, 137);
    // public static final Color CHART_COLOR4_BORDER = new Color(135, 169, 62);
    //
    // public static final Color CHART_COLOR4_END = new Color(164, 200, 85);
    // public static final Color CHART_COLOR5_BEGIN = new Color(248, 235, 144);
    // public static final Color CHART_COLOR5_BORDER = new Color(206, 183, 13);
    //
    // public static final Color CHART_COLOR5_END = new Color(241, 218, 52);
    // public static final Color CHART_COLOR6_BEGIN = new Color(230, 112, 232);
    // public static final Color CHART_COLOR6_BORDER = new Color(131, 54, 146);
    //
    // public static final Color CHART_COLOR6_END = new Color(168, 73, 163);
    // public static final Color CHART_COLOR7_BEGIN = new Color(126, 196, 216);
    // public static final Color CHART_COLOR7_BORDER = new Color(42, 132, 157);
    //
    // public static final Color CHART_COLOR7_END = new Color(72, 165, 191);
    // public static final Color CHART_COLOR8_BEGIN = new Color(224, 87, 33);
    // public static final Color CHART_COLOR8_BORDER = new Color(121, 36, 4);
    //
    // public static final Color CHART_COLOR8_END = new Color(190, 62, 35);
    // public static final Color CHART_COLOR9_BEGIN = new Color(151, 178, 13);
    // public static final Color CHART_COLOR9_BORDER = new Color(87, 104, 0);
    //
    // public static final Color CHART_COLOR9_END = new Color(115, 136, 10);
    // Colors used by VIFSAM and defined in fr.vif.jtech.gwt.server.charts.GWTChartsConstants
    // FIXME waiting for this class was transfered in JTECH_COMMON
    public static final DecimalFormat FORMAT_DEC     = new DecimalFormat("##0.0");
    public static final Font          LABEL_FONT     = new Font("Arial", Font.BOLD, 14);

    // public static final Color RED_GREEN_1 = new Color(219, 32, 22);
    //
    // public static final Color RED_GREEN_10 = new Color(84, 181, 20);
    //
    // public static final Color RED_GREEN_2 = new Color(227, 58, 18);
    //
    // public static final Color RED_GREEN_3 = new Color(239, 98, 12);
    //
    // public static final Color RED_GREEN_4 = new Color(246, 139, 5);
    // public static final Color RED_GREEN_5 = new Color(246, 174, 1);
    // public static final Color RED_GREEN_6 = new Color(236, 185, 0);
    // public static final Color RED_GREEN_7 = new Color(201, 185, 4);
    // public static final Color RED_GREEN_8 = new Color(157, 185, 10);
    // public static final Color RED_GREEN_9 = new Color(114, 185, 16);

    public static final Font          TIC_FONT       = new Font("Arial", Font.BOLD, 12);
    /** LOGGER. */
    private static final Logger       LOGGER         = Logger.getLogger(GraphMOTools.class);

    /**
     * Constructor.
     */
    private GraphMOTools() {

    }

    /**
     * convert a FGraphBean to Graph.
     * 
     * @param vbean FGraphBean containing list of indicators
     * @param selection a graph selection bean
     * @return a Graph bean to use.
     */
    public static GraphMO convertFGraphToGraph(final FGraphMOBean vbean, final FSupervisorMOGraphSelectionBean selection) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertFGraphToGraph(vbean=" + vbean + ", selection=" + selection + ")");
        }

        GraphMO graph = new GraphMO();
        graph.setTitle(vbean.getTitle());

        graph.setType(selection.getGraphType());

        List<CumulatedIndicatorMO> cums = vbean.getCums();
        // IndicatorMOTools.accumulateIndicators(selection.getAxe(), vbean .getIndicators());

        List<POIndicatorState> orderedList = new ArrayList<POIndicatorState>();
        orderedList.add(POIndicatorState.NOT_STARTED);
        orderedList.add(POIndicatorState.STARTED);
        orderedList.add(POIndicatorState.FINISHED_IN_WORKSHOP);
        orderedList.add(POIndicatorState.FINISHED);

        for (POIndicatorState state : orderedList) {
            for (CumulatedIndicatorMO cum : cums) {
                GraphMODataSet set = new GraphMODataSet(I18nClientManager.translate(state.getName()));

                IndicatorMOQties qtiesNotStarted = IndicatorMOTools.cumulateQties(cum, FlowType.NONE, state);

                IndicatorMOQties all = IndicatorMOTools.cumulateQties(cum, FlowType.NONE, POIndicatorState.NOT_STARTED,
                        POIndicatorState.STARTED, POIndicatorState.FINISHED_IN_WORKSHOP, POIndicatorState.FINISHED);

                double val = Math.rint((100d * qtiesNotStarted.getNbProcessOrder()) / all.getNbProcessOrder());

                set.getGraphData().add(new GraphMOData<String, Double>(cum.getKey().getAxeValueCL().getCode(), val));

                graph.getGraphData().add(set);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertFGraphToGraph(vbean=" + vbean + ", selection=" + selection + ")=" + graph);
        }
        return graph;
    }

    /**
     * A utility method for creating gradient paints.
     * 
     * @param c1 color 1.
     * @param c2 color 2.
     * 
     * @return A radial gradient paint.
     */
    public static RadialGradientPaint createGradientPaint(final Color c1, final Color c2) {
        Point2D center = new Point2D.Float(0, 0);
        float radius = 100;
        float[] dist = { 0.0f, 0.80f };
        // return new RadialGradientPaint(center, radius, dist, new Color[] { c1, c2 });
        return new RadialGradientPaint(center, radius, dist, new Color[] { c1, c2 });
    }

    /**
     * 
     * Create a Graph Panel.
     * 
     * @param graph Graph
     * @param options Graph options for Gantt
     * @return ChartPanel ChartPanel
     */
    public static ChartPanel createGraphPanel(final GraphMO graph, final GraphMOOptions options) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createGraphPanel(graph=" + graph + ", options=" + options + ")");
        }

        ChartPanel panel = null;

        if (graph.getVerticalLabel() == null) {
            graph.setVerticalLabel(I18nClientManager.translate(GenKernel.T29921, false)); // Value (Planning Gantt)
        }
        if (graph.getHorizontalLabel() == null) {
            graph.setHorizontalLabel(I18nClientManager.translate(Jtech.T32134, false)); // Selection axe (Avancement)
        }

        Plot plot = createPlot(graph, options);

        if (plot != null) {
            JFreeChart chart = null;
            chart = new JFreeChart(graph.getTitle(), JFreeChart.DEFAULT_TITLE_FONT, plot, true);

            chart.getLegend().setVisible(graph.getShowLegend());

            panel = new ChartPanel(chart);
            panel.setOpaque(false);

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createGraphPanel(graph=" + graph + ", options=" + options + ")=" + panel);
        }
        return panel;
    }

    /**
     * 
     * Create a Graph Panel.
     * 
     * @param plot the plot
     * @param graph Graph
     * @param options Graph options for Gantt
     * @return ChartPanel ChartPanel
     */
    public static ChartPanel createGraphPanel(final GraphMO graph, final GraphMOOptions options, final XYPlot plot) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createGraphPanel(graph=" + graph + ", options=" + options + ")");
        }

        ChartPanel panel = null;

        if (graph.getVerticalLabel() == null) {
            graph.setVerticalLabel(I18nClientManager.translate(GenKernel.T29921, false)); // Value (Planning Gantt)
        }
        if (graph.getHorizontalLabel() == null) {
            graph.setHorizontalLabel(I18nClientManager.translate(Jtech.T32134, false)); // Selection axe (Avancement)
        }

        if (plot != null) {
            JFreeChart chart = null;
            chart = new JFreeChart(graph.getTitle(), JFreeChart.DEFAULT_TITLE_FONT, plot, true);

            chart.getLegend().setVisible(graph.getShowLegend());

            panel = new ChartPanel(chart);
            panel.setOpaque(false);

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createGraphPanel(graph=" + graph + ", options=" + options + ")=" + panel);
        }
        return panel;
    }

    /**
     * Create a plot.
     * 
     * @param graph Graph
     * @param options Graph options for Gantt
     * @return plot
     */
    public static Plot createPlot(final GraphMO graph, final GraphMOOptions options) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createPlot(graph=" + graph + ", options=" + options + ")");
        }

        Plot plot = null;

        if (graph.getType().equals(GraphTypeEnum.BAR_STACKED)) {
            plot = createBarStacked(graph);
        } else if (GraphTypeEnum.BAR_GANTT_XY.equals(graph.getType())) {
            plot = createBarGanttXY(graph, options);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createPlot(graph=" + graph + ", options=" + options + ")=" + plot);
        }
        return plot;
    }

    /**
     * Creates and returns a default instance of an XY bar chart.
     * <P>
     * The chart object returned by this method uses an {@link XYPlot} instance as the plot, with a {@link DateAxis} for
     * the domain axis, a {@link NumberAxis} as the range axis, and a {@link XYBarRenderer} as the renderer.
     * 
     * @param graph the graph bean
     * @param dateAxis make the domain axis display dates?
     * @param dataset the dataset for the chart (<code>null</code> permitted).
     * @param orientation the orientation (horizontal or vertical) (<code>null</code> NOT permitted).
     * @param legend a flag specifying whether or not a legend is required.
     * @param tooltips configure chart to generate tool tips?
     * @param urls configure chart to generate URLs?
     * @param options Graph options for Gantt
     * 
     * @return An XY bar chart.
     */
    public static JFreeChart createXYBarChart(final GraphMO graph, final boolean dateAxis,
            final IntervalXYDataset dataset, final PlotOrientation orientation, final boolean legend,
            final boolean tooltips, final boolean urls, final GraphMOOptions options) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createXYBarChart(graph=" + graph + ", dateAxis=" + dateAxis + ", dataset=" + dataset
                    + ", orientation=" + orientation + ", legend=" + legend + ", tooltips=" + tooltips + ", urls="
                    + urls + ")");
        }

        if (orientation == null) {
            throw new IllegalArgumentException("Null 'orientation' argument.");
        }
        ValueAxis domainAxis = null;
        if (dateAxis) {
            domainAxis = new DateAxis(graph.getVerticalLabel());
        } else {
            NumberAxis axis = new NumberAxis(graph.getVerticalLabel());
            axis.setAutoRangeIncludesZero(false);
            domainAxis = axis;
        }
        ValueAxis valueAxis = new NumberAxis(graph.getHorizontalLabel());

        FGanttMORenderer renderer = new FGanttMORenderer();
        // renderer.setDrawBarOutline(true);
        // renderer.setOutlinePaint(MOUIConstants.RED_GREEN_4_BORDER);
        renderer.setBarPainter(new FGanttMOBarPainter()); // , dataset));

        renderer.setBaseToolTipGenerator(new FGanttMOToolTipGenerator(graph, options));

        // renderer.setBaseToolTipGenerator(new XYToolTipGenerator() {
        // @Override
        // public String generateToolTip(final XYDataset dataset, final int series, final int item) {
        // if (LOGGER.isDebugEnabled()) {
        // LOGGER.debug("B - $XYToolTipGenerator.generateToolTip(dataset=" + dataset + ", series=" + series
        // + ", item=" + item + ")");
        // }
        //
        // XYTaskDataset dataSet = (XYTaskDataset) dataset;
        //
        // TimePeriod period = dataSet.getTasks().getSeries(series).get(item).getDuration();
        // Double percent = dataSet.getTasks().getSeries(series).get(item).getPercentComplete();
        //
        // String toolTip = dataSet.getTasks().getSeries(series).get(item).getDescription() + " : "
        // + DateHelper.formatDate(period.getStart(), FSupervisorMOTools.FORMAT_HOUR) + "-"
        // + DateHelper.formatDate(period.getEnd(), FSupervisorMOTools.FORMAT_HOUR);
        //
        // if (percent != null) {
        // toolTip = toolTip + " (" + FORMAT_DEC.format(percent * 100) + "%)";
        // }
        //
        // if (LOGGER.isDebugEnabled()) {
        // LOGGER.debug("E - $XYToolTipGenerator.generateToolTip(dataset=" + dataset + ", series=" + series
        // + ", item=" + item + ")=" + toolTip);
        // }
        // return toolTip;
        //
        // }
        // });

        XYPlot plot = new XYPlot(dataset, domainAxis, valueAxis, renderer);
        plot.setOrientation(orientation);

        JFreeChart chart = new JFreeChart(graph.getTitle(), JFreeChart.DEFAULT_TITLE_FONT, plot, legend);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createXYBarChart(graph=" + graph + ", dateAxis=" + dateAxis + ", dataset=" + dataset
                    + ", orientation=" + orientation + ", legend=" + legend + ", tooltips=" + tooltips + ", urls="
                    + urls + ")=" + chart);
        }
        return chart;

    }

    /**
     * 
     * Creates the plot with graph options.
     * 
     * @param graph the graph
     * @param options the options
     * @return XYPlot
     */
    public static XYPlot createXYPlot(final GraphMO graph, final GraphMOOptions options) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createBarGanttXY(graph=" + graph + ", options=" + options + ")");
        }

        TaskSeriesCollection dataCollection = new TaskSeriesCollection();

        List<XYAnnotation> annotation = new ArrayList<XYAnnotation>();

        int cptSet = 0;
        int nbDataset = Math.min(graph.getGraphData().size(), options.getNumberOfDataSetMax());
        String[] setTab = new String[nbDataset]; // graph.getGraphData().size()];
        TaskSeries[] seriesToAdd = new TaskSeries[nbDataset];

        // dataset.
        int currentDataSet = 0;
        int dataSetNumber = nbDataset - 1;

        for (GraphMODataSet set : graph.getGraphData()) {

            // Start at current dataSet
            if (currentDataSet >= options.getStartDataSet()) {

                if (cptSet >= options.getNumberOfDataSetMax()) {
                    break;
                }

                TaskSeries series = new TaskSeries(set.getTitle());
                series.setKey(dataSetNumber);

                for (GraphMOData data : set.getGraphData()) {
                    @SuppressWarnings("unchecked")
                    GraphMOPeriod<Object> period = data.getSecondAsGraphPeriod();

                    FGanttMOTask<GraphMOPeriod<Object>> task = new FGanttMOTask<GraphMOPeriod<Object>>(set.getTitle(),
                            period.getStart(), period.getEnd());
                    task.setBean(period);
                    task.setPercentComplete(period.getPercentComplete());

                    // Add to list
                    series.add(task);
                    double y = period.getStart().getTime()
                            + ((period.getEnd().getTime() - period.getStart().getTime()) / 2);

                    int i = 0;
                    for (String text : period.getAnnotations()) {
                        FGanttMOAnnotation annot = new FGanttMOAnnotation(i, text, dataSetNumber, y);
                        annot.setFont(TIC_FONT);
                        annotation.add(annot);
                        i++;
                    }

                }

                // NLE9
                // if (DateHelper.compareDate(dateMax, dateMin) < 0) {
                // dateMax = dateMin;
                // }

                setTab[dataSetNumber] = set.getTitle();
                seriesToAdd[dataSetNumber] = series; // be careful this order must be the same than order
                // in
                // setTab....
                cptSet++;
                dataSetNumber--;

            }
            currentDataSet++;
        }
        // allow to order series in good order.
        for (TaskSeries serie : seriesToAdd) {
            dataCollection.add(serie);
        }

        XYTaskDataset dataset = new XYTaskDataset(dataCollection);

        JFreeChart chart = createXYBarChart(graph, false, dataset, PlotOrientation.HORIZONTAL, true, true, true,
                options);

        XYPlot plot = (XYPlot) chart.getPlot();

        SymbolAxis xAxis = new SymbolAxis(graph.getVerticalLabel(), setTab);
        xAxis.setTickLabelFont(TIC_FONT);
        xAxis.setGridBandsVisible(false);

        plot.setDomainAxis(xAxis);

        plot.setRangeAxis(0, new DateAxis(""));
        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setUseYInterval(true);

        ValueMarker nowMarker = new ValueMarker(new Date().getTime());

        nowMarker.setAlpha((float) 0.5);
        float[] dash = { 10.0f };
        Stroke stroke = new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);

        nowMarker.setStroke(stroke);

        nowMarker.setLabelTextAnchor(TextAnchor.CENTER);
        nowMarker.setLabelAnchor(RectangleAnchor.BOTTOM);

        nowMarker.setLabelFont(LABEL_FONT);
        nowMarker.setLabelOffset(new RectangleInsets(10, 10, 10, 10));
        nowMarker.setLabelOffsetType(LengthAdjustmentType.EXPAND);

        plot.addRangeMarker(nowMarker, Layer.FOREGROUND);

        // Define VIF Colors
        // GraphMOTools.defaultAbstractSeriesPaint(renderer);

        // Annotations
        renderer.removeAnnotations();

        for (XYAnnotation annot : annotation) {
            renderer.addAnnotation(annot);
        }

        plot.setOutlineVisible(true);

        for (int i = 0; i < cptSet; i++) {
            // renderer.setSeriesPaint(i, MOUIConstants.RED_GREEN_1);
            renderer.setSeriesPaint(i, new GradientPaint(0.0f, 0.0f, MOUIConstants.RED_GREEN_1_END, 0.0f, 0.0f,
                    MOUIConstants.RED_GREEN_1_BEGIN));
            renderer.setSeriesOutlinePaint(i, MOUIConstants.RED_GREEN_1_BORDER);
        }

        // renderer.setBaseItemLabelsVisible(true);
        renderer.setShadowVisible(false);

        return plot;
    }

    /**
     * Return a color using a percent value ( < 1).
     * 
     * @param d percent value
     * @return a Color
     */
    public static Color getColorForPercent(final double d) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getColorForPercent(d=" + d + ")");
        }

        return GraphMOTools.getGradientPaintForPercent(d).getColor2();
    }

    /**
     * Return a color using a percent value ( < 1).
     * 
     * @param d percent value
     * @return a Color
     */
    public static GradientPaint getGradientPaintForPercent(final double d) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getGradientPaintForPercent(d=" + d + ")");
        }

        GradientPaint color = MOUIConstants.RED_GREEN_1;
        // Check ToDo values not null
        int val = 0;

        // if val is less than 0 the color is RED
        if (d < 0) {
            val = 0;
        } else {
            val = (int) (d * 10);
        }
        // Define color according to value 'val'
        switch (val) {
            case 0:
                color = MOUIConstants.RED_GREEN_1;
                break;
            case 1:
                color = MOUIConstants.RED_GREEN_1;
                break;
            case 2:
                color = MOUIConstants.RED_GREEN_2;
                break;
            case 3:
                color = MOUIConstants.RED_GREEN_3;
                break;
            case 4:
                color = MOUIConstants.RED_GREEN_4;
                break;
            case 5:
                color = MOUIConstants.RED_GREEN_5;
                break;
            case 6:
                color = MOUIConstants.RED_GREEN_6;
                break;
            case 7:
                color = MOUIConstants.RED_GREEN_7;
                break;
            case 8:
                color = MOUIConstants.RED_GREEN_8;
                break;
            case 9:
                color = MOUIConstants.RED_GREEN_9;
                break;
            case 10:
                color = MOUIConstants.RED_GREEN_10;
                break;
            default:
                color = MOUIConstants.RED_GREEN_10;
                break;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getGradientPaintForPercent(d=" + d + ")=" + color);
        }
        return color;
    }


    /**
     * 
     * create a Plot for Bar3D Graph with stacked items (from StackedBarChart3DDemo3).
     * 
     * @param graph Graph
     * @param options Graph options for Gantt
     * @return Plot Plot
     */
    @SuppressWarnings({ "unchecked", "null" })
    private static Plot createBarGanttXY(final GraphMO graph, final GraphMOOptions options) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createBarGanttXY(graph=" + graph + ", options=" + options + ")");
        }

        TaskSeriesCollection dataCollection = new TaskSeriesCollection();

        List<XYAnnotation> annotation = new ArrayList<XYAnnotation>();

        int cptSet = 0;
        int nbDataset = Math.min(graph.getGraphData().size(), options.getNumberOfDataSetMax());
        String[] setTab = new String[nbDataset]; // graph.getGraphData().size()];
        TaskSeries[] seriesToAdd = new TaskSeries[nbDataset];

        // dataset.
        int currentDataSet = 0;
        int dataSetNumber = nbDataset - 1;

        Calendar endOfLastMO = Calendar.getInstance();
        Calendar beginOfFirstMO = Calendar.getInstance();
        Calendar tmpEnd = Calendar.getInstance();
        Calendar tmpBegin = Calendar.getInstance();

        for (GraphMODataSet set : graph.getGraphData()) {

            if (set.equals(graph.getGraphData().get(0))) {
                endOfLastMO
                .setTime(graph.getGraphData().get(0).getGraphData().get(0).getSecondAsGraphPeriod().getEnd());
            }
            if (set.equals(graph.getGraphData().get(0))) {
                beginOfFirstMO.setTime(graph.getGraphData().get(0).getGraphData().get(0).getSecondAsGraphPeriod()
                        .getStart());
            }

            tmpEnd.setTime(set.getGraphData().get(0).getSecondAsGraphPeriod().getEnd());
            if (tmpEnd.compareTo(endOfLastMO) == 1) {
                endOfLastMO.setTime(tmpEnd.getTime());
            }

            tmpBegin.setTime(set.getGraphData().get(0).getSecondAsGraphPeriod().getStart());
            if (tmpBegin.compareTo(beginOfFirstMO) == -1) {
                beginOfFirstMO.setTime(tmpBegin.getTime());
            }

            // Start at current dataSet
            if (currentDataSet >= options.getStartDataSet()) {

                if (cptSet >= options.getNumberOfDataSetMax()) {
                    break;
                }

                TaskSeries series = new TaskSeries(set.getTitle());
                series.setKey(dataSetNumber);

                for (GraphMOData data : set.getGraphData()) {
                    GraphMOPeriod<Object> period = data.getSecondAsGraphPeriod();

                    FGanttMOTask<GraphMOPeriod<Object>> task = new FGanttMOTask<GraphMOPeriod<Object>>(set.getTitle(),
                            period.getStart(), period.getEnd());
                    task.setBean(period);
                    task.setPercentComplete(period.getPercentComplete());

                    // Add to list
                    series.add(task);
                    double y = period.getStart().getTime()
                            + ((period.getEnd().getTime() - period.getStart().getTime()) / 2);

                    int i = 0;
                    for (String text : period.getAnnotations()) {
                        FGanttMOAnnotation annot = new FGanttMOAnnotation(i, text, dataSetNumber, y);
                        annot.setFont(TIC_FONT);
                        annotation.add(annot);
                        i++;
                    }

                }

                // NLE9
                // if (DateHelper.compareDate(dateMax, dateMin) < 0) {
                // dateMax = dateMin;
                // }

                setTab[dataSetNumber] = set.getTitle();
                seriesToAdd[dataSetNumber] = series; // be careful this order must be the same than order
                // in
                // setTab....
                cptSet++;
                dataSetNumber--;

            }
            currentDataSet++;
        }
        // allow to order series in good order.
        for (TaskSeries serie : seriesToAdd) {
            dataCollection.add(serie);
        }

        XYTaskDataset dataset = new XYTaskDataset(dataCollection);

        JFreeChart chart = createXYBarChart(graph, false, dataset, PlotOrientation.HORIZONTAL, true, true, true,
                options);

        XYPlot plot = (XYPlot) chart.getPlot();

        SymbolAxis xAxis = new SymbolAxis(graph.getVerticalLabel(), setTab);
        xAxis.setTickLabelFont(TIC_FONT);
        xAxis.setGridBandsVisible(false);

        plot.setDomainAxis(xAxis);
        XYBarRenderer renderer = (XYBarRenderer) plot.getRenderer();
        renderer.setUseYInterval(true);

        /* AC setting up the right date in the chart, using by the scrollbar */

        Calendar firstDate = Calendar.getInstance();

        /* Setting up lastDate */
        Calendar lastDate = Calendar.getInstance();
        lastDate.setTime(firstDate.getTime());

        lastDate.add(Calendar.MINUTE, +options.getNbMinutesToShow());

        plot.setRangeAxis(0, new DateAxis(""));

        plot.getRangeAxis(0).setRange(new Range(firstDate.getTime().getTime(), lastDate.getTime().getTime()));

        // toolTip generator (may be to move to an outside class ?!) done !
        // renderer.setBaseToolTipGenerator(new FGanttMOToolTipGenerator(graph, options));

        // NLE9
        // nowMarker = new ValueMarker(new Date().getTime());

        ValueMarker nowMarker = new ValueMarker(new Date().getTime());

        nowMarker.setAlpha((float) 0.5);
        float[] dash = { 10.0f };
        Stroke stroke = new BasicStroke(3.0f, BasicStroke.CAP_BUTT, BasicStroke.JOIN_MITER, 10.0f, dash, 0.0f);

        nowMarker.setStroke(stroke);

        nowMarker.setLabelTextAnchor(TextAnchor.CENTER);
        nowMarker.setLabelAnchor(RectangleAnchor.BOTTOM);

        nowMarker.setLabelFont(LABEL_FONT);
        nowMarker.setLabelOffset(new RectangleInsets(10, 10, 10, 10));
        nowMarker.setLabelOffsetType(LengthAdjustmentType.EXPAND);

        plot.addRangeMarker(nowMarker, Layer.FOREGROUND);

        // Define VIF Colors
        // GraphMOTools.defaultAbstractSeriesPaint(renderer);

        // Annotations
        renderer.removeAnnotations();

        for (XYAnnotation annot : annotation) {
            renderer.addAnnotation(annot);
        }

        plot.setOutlineVisible(true);

        for (int i = 0; i < cptSet; i++) {
            // renderer.setSeriesPaint(i, MOUIConstants.RED_GREEN_1);
            renderer.setSeriesPaint(i, new GradientPaint(0.0f, 0.0f, MOUIConstants.RED_GREEN_1_END, 0.0f, 0.0f,
                    MOUIConstants.RED_GREEN_1_BEGIN));
            renderer.setSeriesOutlinePaint(i, MOUIConstants.RED_GREEN_1_BORDER);
        }

        // renderer.setBaseItemLabelsVisible(true);
        renderer.setShadowVisible(false);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createBarGanttXY(graph=" + graph + ", options=" + options + ")=" + plot);
        }

        return plot;

    }

    /**
     * 
     * create a Plot for Bar3D Graph with stacked items (from StackedBarChart3DDemo3).
     * 
     * @param graph Graph
     * @return Plot Plot
     */
    private static Plot createBarStacked(final GraphMO graph) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createBarStacked(graph=" + graph + ")");
        }

        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        // dataset.
        for (GraphMODataSet set : graph.getGraphData()) {
            for (GraphMOData data : set.getGraphData()) {
                dataset.addValue(data.getSecondAsDouble(), set.getTitle(), data.getFirstAsString());
            }
        }

        // Axis
        CategoryAxis categoryAxis = new CategoryAxis(graph.getHorizontalLabel());
        ValueAxis valueAxis = new NumberAxis(graph.getVerticalLabel());

        JFreeChart chartItem = ChartFactory.createStackedBarChart(graph.getTitle(), // chart title
                graph.getHorizontalLabel(), // domain axis label
                graph.getVerticalLabel(), // range axis label
                dataset, // data
                graph.getHorizontal() ? PlotOrientation.HORIZONTAL : PlotOrientation.VERTICAL, // the plot orientation
                        true, // include legend
                        true, // tooltips
                        false // urls
                );

        CategoryPlot plotItem = chartItem.getCategoryPlot();
        BarRenderer renderer = (BarRenderer) plotItem.getRenderer();

        renderer.setBaseToolTipGenerator(new StandardCategoryToolTipGenerator());
        renderer.setBaseItemURLGenerator(new StandardCategoryURLGenerator());

        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        renderer.setBaseItemLabelsVisible(true);
        renderer.setBasePositiveItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER));
        renderer.setBaseNegativeItemLabelPosition(new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER));

        // Define VIF Colors
        // defaultAbstractSeriesPaint(renderer);

        // renderer.setSeriesPaint(0, UIManager.getColor(UIManager.getColor(LAFHelper.LAF_RED_GREEN_1_END)));
        // renderer.setSeriesPaint(1, UIManager.getColor(UIManager.getColor(LAFHelper.LAF_RED_GREEN_5_END)));
        // renderer.setSeriesPaint(2, UIManager.getColor(UIManager.getColor(LAFHelper.LAF_RED_GREEN_10_END)));

        // SAM avant
        renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        renderer.setDrawBarOutline(true);
        renderer.setItemMargin(0.0);
        renderer.setShadowVisible(false);

        // final Color notStartedColor = StandardColor.RED;
        // final Color startedColor = StandardColor.YELLOW;
        // final Color finishInWorkshopColor = StandardColor.PASTEL_BLUE;
        // final Color finishColor = StandardColor.GREEN;

        renderer.setSeriesPaint(0, new Color(239, 98, 12));
        renderer.setSeriesPaint(1, new Color(246, 186, 39));
        renderer.setSeriesPaint(2, new Color(67, 142, 206));
        renderer.setSeriesPaint(3, new Color(84, 181, 20));

        // what's next gives progressive colors whereas we need fixed colors
        // renderer.setSeriesPaint(0, MOUIConstants.RED_GREEN_1);
        // renderer.setSeriesPaint(1, MOUIConstants.RED_GREEN_4);
        // renderer.setSeriesPaint(2, MOUIConstants.RED_GREEN_7);
        // renderer.setSeriesPaint(3, MOUIConstants.RED_GREEN_10);

        renderer.setSeriesOutlinePaint(0, new Color(239, 98, 12));
        renderer.setSeriesOutlinePaint(1, new Color(246, 186, 39));
        renderer.setSeriesOutlinePaint(2, new Color(67, 142, 206));
        renderer.setSeriesOutlinePaint(3, new Color(84, 181, 20));
        // what's next gives progressive colors whereas we need fixed colors
        // renderer.setSeriesOutlinePaint(0, MOUIConstants.RED_GREEN_1_BORDER);
        // renderer.setSeriesOutlinePaint(1, MOUIConstants.RED_GREEN_4_BORDER);
        // renderer.setSeriesOutlinePaint(2, MOUIConstants.RED_GREEN_7_BORDER);
        // renderer.setSeriesOutlinePaint(3, MOUIConstants.RED_GREEN_10_BORDER);

        // SAM apres
        renderer.setBarPainter(new StandardBarPainter()); // FIXME C'est çà qui enlève un effet pourri à une barre JFREE
        // renderer.setBaseItemLabelsVisible(false);
        // renderer.setBaseItemLabelPaint(Color.black);
        final ItemLabelPosition p = new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER);
        // renderer.setBasePositiveItemLabelPosition(p);

        // Plot
        CategoryPlot plot = new CategoryPlot(dataset, categoryAxis, valueAxis, renderer);
        plot.setColumnRenderingOrder(SortOrder.DESCENDING);
        plot.setDatasetRenderingOrder(DatasetRenderingOrder.FORWARD);

        plot.setOrientation(plotItem.getOrientation());

        // plot = plotItem;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createBarStacked(graph=" + graph + ")=" + plot);
        }
        return plot;
    }

}

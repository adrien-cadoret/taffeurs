/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputCompFModel.java,v $
 * Created on 7 Dec 2015 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcomp;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.GeneralSelectionMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompGSCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompGSModel;


/**
 * InputOperationItem : Feature Model.
 * 
 * @author vr
 */
public class FProductionInputCompFModel extends StandardFeatureModel {

    public static final String FEATURES          = "features";
    public static final String SHOW_ALL          = "showall";
    public static final String FINISH_RESUME     = "finishresume";
    public static final String MOVEMENT_LIST     = "movementlist";
    public static final String PRODUCTION_OUTPUT = "productionoutput";

    public static final String WEIGHT            = "weight";
    public static final String FLASH             = "flash";
    public static final String CANCEL_LAST       = "cancellast";
    public static final String SAVE              = "SAVE";

    /**
     * Default constructor.
     * 
     */
    public FProductionInputCompFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FProductionBModel.class, null, FProductionInputCompBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FProductionInputCompVModel.class, null, FProductionInputCompVCtrl.class));
        setGeneralSelectionTriad(new GeneralSelectionMVCTriad(FProductionOutputCompGSModel.class, null,
                FProductionOutputCompGSCtrl.class));
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMSOCreationBCtrl.java,v $
 * Created on 19 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaReturnInitBean;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmsocreation.FMSOCreationVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmsocreation.FMSOCreationCBS;


/**
 * Controller for browser on items for MSO creation.
 * 
 * @author glc
 */
public class FMSOCreationBCtrl extends AbstractBrowserController<FMSOCreationVBean> {

    public static final String  LOGICAL_WORKSTATION = "logicalWorkstation";
    public static final String  ORIGIN_MO_BEAN      = "originMOBean";

    /** LOGGER. */
    private static final Logger LOGGER              = Logger.getLogger(FMSOCreationBCtrl.class);

    private FMSOCreationCBS     fmSOCreationCBS     = null;

    /**
     * Gets the fmsoCreationCBS.
     * 
     * @category getter
     * @return the fmsoCreationCBS.
     */
    public FMSOCreationCBS getFmSOCreationCBS() {
        return fmSOCreationCBS;
    }

    /**
     * Sets the fmsoCreationCBS.
     * 
     * @category setter
     * @param fmSOCreationCBS fmsoCreationCBS.
     */
    public void setFmSOCreationCBS(final FMSOCreationCBS fmSOCreationCBS) {
        this.fmSOCreationCBS = fmSOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FMSOCreationVBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FMSOCreationVBean> list = new ArrayList<FMSOCreationVBean>();
        MOBean originMOBean = (MOBean) getSharedContext().get(Domain.DOMAIN_PARENT, ORIGIN_MO_BEAN);
        CriteriaReturnInitBean criteriaReturnInitBean = new CriteriaReturnInitBean();
        try {
            criteriaReturnInitBean = getFmSOCreationCBS().getCriteriaReturnInitBean(getIdCtx(), originMOBean);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        FMSOCreationVBean vbean = new FMSOCreationVBean(originMOBean.getMoKey(), originMOBean.getLabel(),
                originMOBean.getItemCLs(), originMOBean.getFabrication(), new QuantityUnit(originMOBean
                        .getMoQuantityUnit().getEffectiveQuantity(), originMOBean.getMoQuantityUnit()
                        .getEffectiveUnit()), criteriaReturnInitBean);
        list.add(vbean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return list;
    }

}

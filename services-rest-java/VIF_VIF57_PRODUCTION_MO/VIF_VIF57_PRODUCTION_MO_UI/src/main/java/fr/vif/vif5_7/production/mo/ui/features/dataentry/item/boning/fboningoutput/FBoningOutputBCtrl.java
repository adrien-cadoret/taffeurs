/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputBCtrl.java,v $
 * Created on 17 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.events.dialogs.selection.SelectionChangeEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutputcontainer.FBoningOutputVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.fboning.FBoningCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * InputOperationItem : Browser Controller.
 * 
 * @author vr
 */
public class FBoningOutputBCtrl extends AbstractBrowserController<FBoningOutputBBean> {
    /** LOGGER. */
    private static final Logger LOGGER     = Logger.getLogger(FBoningOutputBCtrl.class);

    private FBoningCBS          fboningCBS;
    private FBoningOutputVCtrl  viewerCtrl = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        FBoningOutputSBean sBean = (FBoningOutputSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                FBoningOutputConstant.MO_INITIAL_SELECTION);
        if (sBean == null) {
            sBean = new FBoningOutputSBean();
            sBean.setActivityItemType(ActivityItemType.OUTPUT);
            sBean.setWorkstationId((String) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.LOGICAL_WORKSTATION));
            sBean.setAll(false);
            FMOListVBean moListVBean = (FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.MO_LIST_VBEAN);
            sBean.setMoKey(moListVBean.getMobean().getMoKey());
        }
        return sBean;
    }

    /**
     * Gets the fboningCBS.
     * 
     * @return the fboningCBS.
     */
    public FBoningCBS getFboningCBS() {
        return fboningCBS;
    }

    /**
     * Gets the viewerCtrl.
     * 
     * @return the viewerCtrl.
     */
    public FBoningOutputVCtrl getViewerCtrl() {
        return viewerCtrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();

        // --------------------------------------------------------
        // Register a listener on row changing of the browser :
        // --------------------------------------------------------
        // getView().registerActionListener(this);
        getView().addActionListener(this);
    }

    /**
     * Reposition to an input operation bean after a flash.
     * 
     * @param operationItemKey operationItemKey bean to reposit
     * @return true if reposition ok, false otherwise
     */
    public boolean repositionToOperationItemKey(final OperationItemKey operationItemKey) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - repositionToOperationItemKey(operationItemKey=" + operationItemKey + ")");
        }

        boolean ret = false;
        // VR PERF if already reposited, don't change current row
        if (getModel().getCurrentBean() != null && getModel().getCurrentBean().getOperationItemBean() != null
                && getModel().getCurrentBean().getOperationItemBean().getOperationItemKey() != null
                && operationItemKey.equals(getModel().getCurrentBean().getOperationItemBean().getOperationItemKey())) {
            ret = true;
        } else {
            try {
                for (int i = 0; i < getModel().getRowCount(); i++) {
                    FBoningOutputBBean bbean = getModel().getBeanAt(i);

                    if (bbean.getOperationItemBean().getOperationItemKey().equals(operationItemKey)) {
                        changeCurrentRow(i);
                        ret = true;
                        break;
                    }
                }
            } catch (UIVetoException e) {
                getView().show(e);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - repositionToOperationItemKey(operationItemKey=" + operationItemKey + ")=" + ret);
        }
        return ret;
    }

    @Override
    public void selectionChanged(final SelectionChangeEvent event) {
        ((FBoningOutputBModel) getModel())
                .setBrowserFilter(((FBoningOutputSBean) event.getDialogBean()).getHierarchy());
    }

    /**
     * Sets the fboningCBS.
     * 
     * @param fboningCBS fboningCBS.
     */
    public void setFboningCBS(final FBoningCBS fboningCBS) {
        this.fboningCBS = fboningCBS;
    }

    /**
     * Sets the viewerCtrl.
     * 
     * @param viewerCtrl viewerCtrl.
     */
    public void setViewerCtrl(final FBoningOutputVCtrl viewerCtrl) {
        this.viewerCtrl = viewerCtrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tableRowSelected(final BrowserActionEvent event) {
        // super.tableRowSelected(event);
        super.selectedRowChanged(event);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FBoningOutputBBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FBoningOutputBBean> lst = null;
        try {
            lst = getFboningCBS().queryElements(getIdCtx(), (FBoningOutputSBean) selection, startIndex, rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }
        for (FBoningOutputBBean bbean : lst) {
            if (getViewerCtrl().getMap() != null
                    && getViewerCtrl().getMap().containsKey(
                            FProductionTools.operationItemKeyToMovementAct(bbean.getOperationItemBean()
                                    .getOperationItemKey()))) {
                FBoningOutputVBean bbeanSave = getViewerCtrl().getMap().get(
                        FProductionTools.operationItemKeyToMovementAct(bbean.getOperationItemBean()
                                .getOperationItemKey()));
                bbean.getStackingPlanInfoBean().setInner(bbeanSave.getBBean().getStackingPlanInfoBean().getInner());
                bbean.setInnerStr(bbeanSave.getBBean().getInnerStr());
            }
        }
        Collections.sort(lst, getModel().getComparator());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ") + return=" + (lst == null ? 0 : lst.size()));
        }
        return lst;
    }
}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOValueCell.java,v $
 * Created on 22 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.swing;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.text.DecimalFormat;
import java.util.Date;

import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.FontNames;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOValues;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.IndicatorTypeValueEnum;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FIndicatorMOCellProgressBar;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FSupervisorMOPanel;


/**
 * Cell of a JTable.
 * 
 * @author jd
 */
public class FIndicatorMOValueCell extends FSupervisorMOPanel implements FIndicatorMOICell {
    private static final Format         CODE_FONT            = new Format(Alignment.LEFT_ALIGN, null,
            StandardColor.GREEN, new CustomFont(
                    FontNames.DEFAULT, FontStyle.BOLD, 14),
                    "30");
    private static DecimalFormat        decFormat            = new DecimalFormat("#####0");
    // private static DecimalFormat decimalFormat = new DecimalFormat("#####0");
    private static DecimalFormat        intFormat            = new DecimalFormat("#####0");
    private static final Format         LABEL_FONT           = new Format(Alignment.LEFT_ALIGN, null,
            StandardColor.WHITE, new CustomFont(
                    FontNames.DEFAULT, FontStyle.BOLD, 12),
                    "30");
    private SwingInputTextField         code                 = null;
    private FIndicatorMOCellProgressBar indicatorProgressBar = null;
    private SwingInputTextField         label                = null;

    private String                      title                = "";

    private StringBuilder               toolTipBuilder       = new StringBuilder();

    /**
     * Constructor.
     */
    public FIndicatorMOValueCell() {
        super();
        init();
    }

    /**
     * Constructor.
     * 
     * @param title title
     */
    public FIndicatorMOValueCell(final String title) {
        super();
        this.title = title;
        init();
    }

    /**
     * {@inheritDoc}
     */
    public void setValues(final IndicatorMOValues bbean) {

        double done = bbean.getResult().getEffectiveQty();
        double toDo = bbean.getResult().getExpectedQty();
        String unit = bbean.getResult().getUnitID();
        getCode().setValue(bbean.getIndicatorCL().getCode());
        getLabel().setValue(bbean.getIndicatorCL().getLabel());

        if (done <= 0 && toDo <= 0) {
            getIndicatorProgressBar().getProgressBar().setString("-");
        } else {

            getIndicatorProgressBar().getProgressBar().setString(

                    (int) (done) + unit + " / " + (int) (toDo) + unit + " (" + intFormat.format(100 * (done / toDo)) + " %)");
        }
        getIndicatorProgressBar().getProgressBar().setMaximum((int) toDo);
        getIndicatorProgressBar().getProgressBar().setValue((int) done);

        getIndicatorProgressBar().getProgressBar().setForeground(
                FSupervisorMOTools.calculateColor(bbean.getTypeValue(), done));

        setToolTip(bbean);
    }

    /**
     * Add Row with n cols.
     * 
     * @param col list of strings to add
     */
    private void addRow(final String... col) {
        toolTipBuilder.append("<tr>");

        for (String c : col) {
            toolTipBuilder.append("<TD><A>");
            toolTipBuilder.append(c);
            toolTipBuilder.append("</A></TD>");
        }
        toolTipBuilder.append("</TR>");

    }

    /**
     * get a textfield.
     * 
     * @return {@link SwingInputTextField}
     */
    private SwingInputTextField getCode() {
        if (code == null) {
            code = new SwingInputTextField(String.class, "30", false, null);
            code.setEnabled(false);
            code.setAlwaysDisabled(true);
            code.setUpdateable(false);
            code.setVisible(true);
            code.setOpaque(false);
            code.setBorder(null);
            code.setFieldFormat(CODE_FONT);
            code.getJTextField().setOpaque(false);
            code.getJTextField().setBorder(null);
        }
        return code;
    }

    /**
     * Get the progress bar.
     * 
     * @return a {@link FIndicatorMOCellProgressBar}
     */
    private FIndicatorMOCellProgressBar getIndicatorProgressBar() {
        if (indicatorProgressBar == null) {
            indicatorProgressBar = new FIndicatorMOCellProgressBar();
        }
        return indicatorProgressBar;
    }

    /**
     * get a textfield.
     * 
     * @return {@link SwingInputTextField}
     */
    private SwingInputTextField getLabel() {
        if (label == null) {
            label = new SwingInputTextField(String.class, "30", false, null);
            label.setEnabled(false);
            label.setAlwaysDisabled(true);
            label.setUpdateable(false);
            label.setVisible(true);
            label.setOpaque(false);
            label.setBorder(null);
            label.setFieldFormat(LABEL_FONT);
            label.getJTextField().setOpaque(false);
            label.getJTextField().setBorder(null);
        }
        return label;
    }

    /**
     * init.
     */
    private void init() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        setLayout(gridBagLayout);

        setBorder(null);

        setOpaque(false);

        GridBagConstraints gbctextField = new GridBagConstraints();
        gbctextField.weighty = 1.0;
        gbctextField.weightx = 0.5;
        // gbctextField.insets = new Insets(0, 0, 5, 5);
        gbctextField.fill = GridBagConstraints.HORIZONTAL;
        gbctextField.gridx = 0;
        gbctextField.gridy = 0;
        add(getCode(), gbctextField);

        GridBagConstraints gbctextField2 = new GridBagConstraints();
        gbctextField2.weighty = 1.0;
        gbctextField2.weightx = 0.5;
        // gbctextField2 = new Insets(0, 0, 5, 5);
        gbctextField2.fill = GridBagConstraints.HORIZONTAL;
        gbctextField2.gridx = 0;
        gbctextField2.gridy = 1;
        add(getLabel(), gbctextField2);

        GridBagConstraints gbcindicatorProgressBar = new GridBagConstraints();
        gbcindicatorProgressBar.gridheight = 2;
        gbcindicatorProgressBar.weightx = 0.5;
        gbcindicatorProgressBar.weighty = 1;
        // gbcindicatorProgressBar.insets = new Insets(0, 0, 5, 0);
        gbcindicatorProgressBar.gridwidth = 2;
        gbcindicatorProgressBar.fill = GridBagConstraints.BOTH;
        gbcindicatorProgressBar.gridx = 1;
        gbcindicatorProgressBar.gridy = 0;
        add(getIndicatorProgressBar(), gbcindicatorProgressBar);

        setToolTipText("Test ToolTip");
    }

    /**
     * Generate tool tip.
     * 
     * @param values {@link IndicatorMOValues}
     */
    private void setToolTip(final IndicatorMOValues values) {
        toolTipBuilder = new StringBuilder();
        toolTipBuilder.append("<html>");
        toolTipBuilder.append("<body>");

        toolTipBuilder.append("<table>");

        if (IndicatorTypeValueEnum.PRODUCTIVITY.equals(values.getTypeValue())) {

            addRow(I18nClientManager.translate(values.getTypeValue().getName()), values.getIndicatorCL().getCode(),
                    values.getIndicatorCL().getLabel(), "");

            addRow("", I18nClientManager.translate(ProductionMo.T32537), I18nClientManager.translate(GenKernel.T23302),
                    "");

            addRow(I18nClientManager.translate(ProductionMo.T32606), //
                    decFormat.format(values.getValue1().getEffectiveQty()) + " " + values.getValue1().getUnitID(), //
                    DateHelper.formatDate(
                            DateHelper.getDateTime(new Date(), (int) values.getValue2().getEffectiveQty()),
                            FSupervisorMOTools.FORMAT_HOUR), //
                            decFormat.format(values.getResult().getEffectiveQty()) + " " + values.getResult().getUnitID());

            addRow(I18nClientManager.translate(ProductionMo.T32607), //
                    decFormat.format(values.getValue1().getExpectedQty()) + " " + values.getValue1().getUnitID(), //
                    DateHelper.formatDate(
                            DateHelper.getDateTime(new Date(), (int) values.getValue2().getExpectedQty()),
                            FSupervisorMOTools.FORMAT_HOUR), //
                            decFormat.format(values.getResult().getExpectedQty()) + " " + values.getResult().getUnitID());

            addRow("", //
                    "", //
                    "", //
                    decFormat.format(values.getResult().getEffectiveQty() - values.getResult().getExpectedQty()) + " "
                    + values.getResult().getUnitID());

        } else {

            addRow(I18nClientManager.translate(values.getTypeValue().getName()), values.getIndicatorCL().getCode(),
                    values.getIndicatorCL().getLabel(), "");

            if (IndicatorTypeValueEnum.EFFICIENCY.equals(values.getTypeValue())) {
                addRow("", I18nClientManager.translate(ProductionMo.T9395),
                        I18nClientManager.translate(ProductionMo.T9399), "");
            }

            addRow(I18nClientManager.translate(ProductionMo.T32606), //
                    decFormat.format(values.getValue1().getEffectiveQty()) + " " + values.getValue1().getUnitID(), //
                    decFormat.format(values.getValue2().getEffectiveQty()) + " " + values.getValue2().getUnitID(), //
                    decFormat.format(values.getResult().getEffectiveQty()) + " " + values.getResult().getUnitID());

            addRow(I18nClientManager.translate(ProductionMo.T32607), //
                    decFormat.format(values.getValue1().getExpectedQty()) + " " + values.getValue1().getUnitID(), //
                    decFormat.format(values.getValue2().getExpectedQty()) + " " + values.getValue2().getUnitID(), //
                    decFormat.format(values.getResult().getExpectedQty()) + " " + values.getResult().getUnitID());
            addRow("", //
                    "", //
                    "", //
                    decFormat.format(values.getResult().getEffectiveQty() - values.getResult().getExpectedQty()) + " "
                    + values.getResult().getUnitID());
        }

        toolTipBuilder.append("</table>");
        toolTipBuilder.append("</body>");
        toolTipBuilder.append("</html>");

        setToolTipText(toolTipBuilder.toString());
    }
}

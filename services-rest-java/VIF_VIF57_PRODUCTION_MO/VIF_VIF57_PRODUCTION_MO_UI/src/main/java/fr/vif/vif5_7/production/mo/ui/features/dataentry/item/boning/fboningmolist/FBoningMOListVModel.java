/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOListVModel.java,v $
 * Created on 14 mar. 2013 by CJ
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;


/**
 * MOList : Viewer Model.
 * 
 * @author cj
 */
public class FBoningMOListVModel extends ViewerModel<FMOListVBean> {

    /**
     * Default constructor.
     * 
     */
    public FBoningMOListVModel() {
        super();
        setBeanClass(FMOListVBean.class);
        getUpdateActions().setCopyEnabled(false);
        getUpdateActions().setDeleteEnabled(false);
        getUpdateActions().setDocEnabled(false);
        getUpdateActions().setNewEnabled(false);
        getUpdateActions().setPrintEnabled(false);
        getUpdateActions().setSaveEnabled(false);
        getUpdateActions().setUndoEnabled(false);
    }

}

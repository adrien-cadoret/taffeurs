/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMSOCreationVIView.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation;


/**
 * MSO creation feature view interface.
 * 
 * @author glc
 */
public interface FMSOCreationVIView {
}

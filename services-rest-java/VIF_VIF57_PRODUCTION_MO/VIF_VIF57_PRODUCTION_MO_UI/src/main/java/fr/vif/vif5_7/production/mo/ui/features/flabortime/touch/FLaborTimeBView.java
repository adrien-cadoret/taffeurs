/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeBView.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.touch;


import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.ui.browser.touch.TouchLineBrowser;
import fr.vif.jtech.ui.laf.swing.LAFHelper;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.FLaborStaffBCtrl;


/**
 * Labor time BView.
 * 
 * @author nle
 */
public class FLaborTimeBView extends TouchLineBrowser<FLaborTimeBBean> implements FLaborTimeBIView {
    /**
     * renderer for time delay (ex: 5 h 36, 24 mn).
     */
    private class DelayRenderer implements TableCellRenderer {

        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {

            JLabel jl = new JLabel();
            jl.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE));

            jl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            if (isSelected) {
                jl.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                jl.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));
            } else if (row % 2 == 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            } else {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            }

            FLaborTimeBBean bean = getModel().getBeanAt(row);
            String text = "";

            if ("delay".equals(getModel().getBeanAttributeAt(column))) {
                // Format for time : "3h 15 mn" or "15 mn"
                Date ddeb = bean.getBegDateHour();
                Date dfin = null;
                if (bean.getEndDateHour() == null) {
                    dfin = DateHelper.getTodayTime().getTime();
                } else {
                    dfin = bean.getEndDateHour();
                }
                if (dfin.after(ddeb)) {
                    int minutes = (int) ((dfin.getTime() - ddeb.getTime()) / 60000); // 60 seconds * 1000
                    // milliseconds
                    int hours = minutes / 60;
                    text = format(hours, minutes);

                    if (hours == 0) {
                        text = text + " mn";
                    }
                } else {
                    text = "";
                }
            }

            jl.setText(text);

            jl.setOpaque(true);
            return jl;
        }

        /**
         * Format a calendar into String.
         * 
         * @param hours value
         * @param minutes value
         * @return formatted String
         */
        private String format(final int hours, final int minutes) {
            StringBuilder result = new StringBuilder();

            Calendar cal1 = Calendar.getInstance();
            cal1.set(Calendar.SECOND, 0);
            cal1.set(Calendar.MINUTE, minutes);
            String strHour = "";
            String strMin = "";
            if (hours > 0) {
                if (hours > 9) {
                    strHour = String.valueOf(hours);
                } else {
                    strHour = String.valueOf(hours);
                }
            }
            if (cal1.get(Calendar.MINUTE) > 0) {
                if (cal1.get(Calendar.MINUTE) > 9 || hours > 0) {
                    strMin = DateHelper.formatDate(cal1.getTime(), "mm");
                } else {
                    strMin = DateHelper.formatDate(cal1.getTime(), "m");
                }
            } else {
                strMin = "00";
            }
            if (!strHour.isEmpty()) {
                result.append(strHour).append(" h ");
            }
            result.append(strMin);
            return result.toString();
        }
    }

    /**
     * renderer period : date-hour begin + arrow + date-hour end (ex: 15/06 17:08 ==> 15/06 18:03).
     */
    private class PeriodRenderer implements TableCellRenderer {
        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {

            JPanel jp = new JPanel();
            final JLabel begDate = new JLabel();
            final JLabel endDate = new JLabel();
            final JLabel begTime = new JLabel();
            final JLabel endTime = new JLabel();
            final JLabel toTime = new JLabel();
            final GridBagLayout gridBagLayout = new GridBagLayout();
            jp.setLayout(gridBagLayout);

            // Manage Font
            begDate.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE));
            endDate.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE));
            begTime.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE));
            endTime.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE));

            // Manage colors
            if (isSelected) {
                jp.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                begDate.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                endDate.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                begTime.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                endTime.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                toTime.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                jp.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));
                begDate.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));
                endDate.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));
                begTime.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));
                endTime.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));
            } else if (row % 2 == 0) {
                jp.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                begDate.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                endDate.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                begTime.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                endTime.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                toTime.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                jp.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
                begDate.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
                endDate.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
                begTime.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
                endTime.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            } else {
                jp.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                toTime.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                begDate.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                endDate.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                begTime.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                endTime.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                jp.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
                begDate.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
                endDate.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
                begTime.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
                endTime.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            }

            // Manage row
            FLaborTimeBBean lstLSBean = getModel().getBeanAt(row);
            SimpleDateFormat sdfd = new SimpleDateFormat("dd/MM/yyyy");
            SimpleDateFormat sdft = new SimpleDateFormat("HH:mm");
            if (lstLSBean.getBegDateHour() != null) {
                Date ladate = new Date();
                Date lheure = new Date();

                ladate = lstLSBean.getBegDateHour();
                lheure = lstLSBean.getBegDateHour();

                begDate.setText(sdfd.format(ladate));
                begTime.setText(sdft.format(lheure));

                begTime.setOpaque(true);

                if (lstLSBean.getEndDateHour() != null) {
                    ladate = lstLSBean.getEndDateHour();
                    lheure = lstLSBean.getEndDateHour();

                    endDate.setText(sdfd.format(ladate));
                    endTime.setText(sdft.format(lheure));
                } else {
                    endDate.setText("                   ");
                    endTime.setText("              ");
                }

                endTime.setOpaque(true);

                toTime.setIcon(new ImageIcon(getClass().getResource("/images/vif/vif57/addon/trs/toTime.png")));
                toTime.setOpaque(true);

                final GridBagConstraints gbc = new GridBagConstraints();
                gbc.weightx = 1;
                gbc.fill = GridBagConstraints.BOTH;
                gbc.insets = new Insets(0, 5, 0, 5);
                gbc.gridy = 0;
                gbc.gridx = 0;
                jp.add(begDate, gbc);

                final GridBagConstraints gbc1 = new GridBagConstraints();
                gbc1.weightx = 1;
                gbc1.fill = GridBagConstraints.BOTH;
                gbc1.insets = new Insets(0, 5, 0, 5);
                gbc1.gridy = 1;
                gbc1.gridx = 0;
                jp.add(begTime, gbc1);

                final GridBagConstraints gbc2 = new GridBagConstraints();
                gbc2.weightx = 1;
                gbc2.fill = GridBagConstraints.BOTH;
                gbc2.gridheight = 2;
                gbc2.insets = new Insets(0, 0, 0, 0);
                gbc2.gridy = 0;
                gbc2.gridx = 1;
                jp.add(toTime, gbc2);

                final GridBagConstraints gbc3 = new GridBagConstraints();
                gbc3.weightx = 1;
                gbc3.fill = GridBagConstraints.BOTH;
                gbc3.insets = new Insets(0, 0, 0, 5);
                gbc3.gridy = 0;
                gbc3.gridx = 2;
                jp.add(endDate, gbc3);

                final GridBagConstraints gbc4 = new GridBagConstraints();
                gbc4.weightx = 1;
                gbc4.fill = GridBagConstraints.BOTH;
                gbc4.insets = new Insets(0, 0, 0, 5);
                gbc4.gridy = 1;
                gbc4.gridx = 2;
                jp.add(endTime, gbc4);
            }
            return jp;
        }
    }

    private static final Logger LOGGER       = Logger.getLogger(FLaborStaffBCtrl.class);

    private static final String DATE_PATTERN = "dd/MM/yy HH:mm";

    /**
     * Default constructor.
     */
    public FLaborTimeBView() {
        super();
        setOpaque(false);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        for (int i = 0; i < getModel().getColumnCount(); i++) {
            if ("period".equals(getModel().getBeanAttributeAt(i))) {
                getJTable().getColumnModel().getColumn(i).setCellRenderer(new PeriodRenderer());
            }

            if ("delay".equals(getModel().getBeanAttributeAt(i))) {
                getJTable().getColumnModel().getColumn(i).setCellRenderer(new DelayRenderer());
            }
        }
    }

}

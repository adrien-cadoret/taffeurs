/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOVCtrl.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOVBean;
import fr.vif.vif5_7.production.mo.business.services.features.fsupervisormo.FSupervisorMOCBS;


/**
 * FSupervisorMO : Viewer Controller.
 * 
 * @author jd
 */
public class FSupervisorMOVCtrl extends AbstractStandardViewerController<FSupervisorMOVBean> {

    private static final Logger LOGGER = Logger.getLogger(FSupervisorMOVCtrl.class);

    private FSupervisorMOCBS    fsupervisorMOCBS;

    /**
     * Gets the fsupervisorMOCBS.
     * 
     * @return the fsupervisorMOCBS.
     */
    public FSupervisorMOCBS getFsupervisorMOCBS() {
        return fsupervisorMOCBS;
    }

    /**
     * Sets the fsupervisorMOCBS.
     * 
     * @param fsupervisorMOCBS fsupervisorMOCBS.
     */
    public void setFsupervisorMOCBS(final FSupervisorMOCBS fsupervisorMOCBS) {
        this.fsupervisorMOCBS = fsupervisorMOCBS;
    }

    /**
     * Sets the fSupervisorMOCBS.
     * 
     * @category setter
     * @param fSupervisorMOCBS fSupervisorMOCBS.
     */
    public final void setFSupervisorMOCBS(final FSupervisorMOCBS fSupervisorMOCBS) {
        this.fsupervisorMOCBS = fSupervisorMOCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean pCreationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String pBeanProperty, final Object propertyValue) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FSupervisorMOVBean convert(final Object pBean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(pBean=" + pBean + ")");
        }

        FSupervisorMOVBean vBean = new FSupervisorMOVBean();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FSupervisorMOVBean pBean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteBean(pBean=" + pBean + ")");
        }

        try {
            getFsupervisorMOCBS().deleteBean(getIdCtx(), pBean);
        } catch (BusinessException e) {
            UIException ue = new UIException(e);
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteBean(pBean=" + pBean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FSupervisorMOVBean insertBean(final FSupervisorMOVBean pBean, final FSupervisorMOVBean pInitialBean)
            throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertBean(pBean=" + pBean + ", pInitialBean=" + pInitialBean + ")");
        }

        FSupervisorMOVBean vBean = pBean;
        try {
            if (pInitialBean == null) {
                // Creation mode
                vBean = getFsupervisorMOCBS().insertBean(getIdCtx(), pBean);
            } else {
                // Copy mode
                vBean = getFsupervisorMOCBS().copyBean(getIdCtx(), pBean, pInitialBean);
            }
        } catch (BusinessException e) {
            UIException ue = new UIException(e);
            throw ue;
        } catch (BusinessErrorsException e) {
            UIErrorsException ue = new UIErrorsException(e);
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertBean(pBean=" + pBean + ", pInitialBean=" + pInitialBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FSupervisorMOVBean updateBean(final FSupervisorMOVBean pBean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(pBean=" + pBean + ")");
        }

        FSupervisorMOVBean vBean = pBean;
        try {
            vBean = getFsupervisorMOCBS().updateBean(getIdCtx(), vBean);
        } catch (BusinessException e) {
            UIException ue = new UIException(e);
            throw ue;
        } catch (BusinessErrorsException e) {
            UIErrorsException ue = new UIErrorsException(e);
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

}

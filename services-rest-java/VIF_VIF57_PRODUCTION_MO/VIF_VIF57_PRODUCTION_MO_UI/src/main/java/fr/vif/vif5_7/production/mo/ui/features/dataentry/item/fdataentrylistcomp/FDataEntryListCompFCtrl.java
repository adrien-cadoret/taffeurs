/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListCompFCtrl.java,v $
 * Created on 26 sept. 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylistcomp;


import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylistcomp.FDataEntryListCompSBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListOutputFCtrl;


/**
 * DataEntryCompList : Feature Controller.
 * 
 * @author kl
 */
public class FDataEntryListCompFCtrl extends FDataEntryListOutputFCtrl {

    /**
     * {@inheritDoc}
     */
    @Override
    protected void readSelection() {

        super.setAbstractOperationItemBean((OutputOperationItemBean) getSharedContext().get(Domain.DOMAIN_PARENT,
                OPERATION_ITEM_BEAN));
        MONatureComplementaryWorkshop natureComplementaryWorkshop = (MONatureComplementaryWorkshop) getSharedContext()
                .get(Domain.DOMAIN_PARENT, FDataEntryListCompFModel.CONTEXT_NATURE);
        ItemKey itemKey = (ItemKey) getSharedContext().get(Domain.DOMAIN_PARENT, "itemKey");

        if (super.getAbstractOperationItemBean() != null) {
            getBrowserController().setInitialSelection(
                    new FDataEntryListCompSBean(super.getAbstractOperationItemBean().getOperationItemKey(),
                            natureComplementaryWorkshop, itemKey));
        }
    }
}

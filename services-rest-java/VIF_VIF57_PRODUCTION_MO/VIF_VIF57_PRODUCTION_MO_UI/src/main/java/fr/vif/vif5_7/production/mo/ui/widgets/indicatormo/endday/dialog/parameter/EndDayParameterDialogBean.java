/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: EndDayParameterDialogBean.java,v $
 * Created on 2 mai 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.dialog.parameter;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.ObjectHelper;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.location.business.beans.features.farea.FAreaBBean;


/**
 * Bean of a end day's parameter dialog.
 *
 * @author ac
 */
public class EndDayParameterDialogBean implements Serializable {

    public static final String PERSONALIZED         = "PERSO";
    public static final String TODAY                = "TODAY";
    public static final String YESTERDAY            = "YESTERDAY";

    private static final long  serialVersionUID     = 1L;

    private EstablishmentKey   establishmentKey     = new EstablishmentKey();

    private CodeLabel          area                 = new CodeLabel();

    private List<FAreaBBean>   areas                = new ArrayList<FAreaBBean>();

    private String             productionDateChoice = "";

    /** Production date. */
    private Date               productionDate       = new Date();

    private String             twBeginDateChoice    = "";

    /** Time window begin date. */
    private Date               twBeginDate          = new Date();

    /** Time window begin time. */
    private Date               twBeginTime          = new Date();

    private String             twEndDateChoice      = "";

    /** Time window end date. */
    private Date               twEndDate            = new Date();

    /** Time window end time. */
    private Date               twEndTime            = new Date();

    /**
     * Indicates if the bean depends on time window. True if it does, false else.
     * */
    private boolean            isTimeWindowContext  = false;

    private boolean            material             = true;

    private boolean            workingPeriod        = true;

    private boolean            trs                  = true;

    private boolean            onLineControl        = true;

    /**
     * Default Ctor.
     */
    public EndDayParameterDialogBean() {
    }

    /**
     * Used to retrieve properties array.
     * 
     * @param bean the bean.
     * @return the array.
     */
    private static Object[] getPropertiesArray(final EndDayParameterDialogBean bean) {
        return new Object[] { bean.establishmentKey, bean.area, bean.areas, bean.productionDateChoice,
                bean.productionDate, bean.twBeginDateChoice, bean.twBeginDate, bean.twBeginTime, bean.twEndDateChoice,
                bean.twEndDate, bean.twEndTime, bean.isTimeWindowContext, bean.material, bean.workingPeriod, bean.trs,
                bean.onLineControl };
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (obj == null) {
            return false;
        } else if (getClass().equals(obj.getClass())) {
            EndDayParameterDialogBean bean = (EndDayParameterDialogBean) obj;
            return ObjectHelper.equalsArray(getPropertiesArray(this), getPropertiesArray(bean));
        } else {
            return false;
        }
    }

    /**
     * Gets the area.
     *
     * @return the area.
     */
    public CodeLabel getArea() {
        return area;
    }

    /**
     * Gets the areas.
     *
     * @return the areas.
     */
    public List<FAreaBBean> getAreas() {
        return areas;
    }

    /**
     * Gets the establishmentKey.
     *
     * @return the establishmentKey.
     */
    public EstablishmentKey getEstablishmentKey() {
        return establishmentKey;
    }

    /**
     * Gets the isTimeWindowContext.
     *
     * @return the isTimeWindowContext.
     */
    public boolean getIsTimeWindowContext() {
        return isTimeWindowContext;
    }

    /**
     * Gets the material.
     *
     * @return the material.
     */
    public boolean getMaterial() {
        return material;
    }

    /**
     * Gets the onLineControl.
     *
     * @return the onLineControl.
     */
    public boolean getOnLineControl() {
        return onLineControl;
    }

    /**
     * Gets the productionDate.
     *
     * @return the productionDate.
     */
    public Date getProductionDate() {
        return productionDate;
    }

    /**
     * Gets the productionDateChoice.
     *
     * @return the productionDateChoice.
     */
    public String getProductionDateChoice() {
        return productionDateChoice;
    }

    /**
     * Gets the trs.
     *
     * @return the trs.
     */
    public boolean getTrs() {
        return trs;
    }

    /**
     * Gets the twBeginDate.
     *
     * @return the twBeginDate.
     */
    public Date getTwBeginDate() {
        return twBeginDate;
    }

    /**
     * Gets the twBeginDateChoice.
     *
     * @return the twBeginDateChoice.
     */
    public String getTwBeginDateChoice() {
        return twBeginDateChoice;
    }

    /**
     * Gets the twBeginTime.
     *
     * @return the twBeginTime.
     */
    public Date getTwBeginTime() {
        return twBeginTime;
    }

    /**
     * Gets the twEndDate.
     *
     * @return the twEndDate.
     */
    public Date getTwEndDate() {
        return twEndDate;
    }

    /**
     * Gets the twEndDateChoice.
     *
     * @return the twEndDateChoice.
     */
    public String getTwEndDateChoice() {
        return twEndDateChoice;
    }

    /**
     * Gets the twEndTime.
     *
     * @return the twEndTime.
     */
    public Date getTwEndTime() {
        return twEndTime;
    }

    /**
     * Gets the workingPeriod.
     *
     * @return the workingPeriod.
     */
    public boolean getWorkingPeriod() {
        return workingPeriod;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return ObjectHelper.hashCodeArray(getPropertiesArray(this));
    }

    /**
     * Sets the area.
     *
     * @param area area.
     */
    public void setArea(final CodeLabel area) {
        this.area = area;
    }

    /**
     * Sets the areas.
     *
     * @param areas areas.
     */
    public void setAreas(final List<FAreaBBean> areas) {
        this.areas = areas;
    }

    /**
     * Sets the establishmentKey.
     *
     * @param establishmentKey establishmentKey.
     */
    public void setEstablishmentKey(final EstablishmentKey establishmentKey) {
        this.establishmentKey = establishmentKey;
    }

    /**
     * Sets the timeWindowContext.
     *
     * @param timeWindowContext timeWindowContext.
     */
    public void setIsTimeWindowContext(final boolean timeWindowContext) {
        this.isTimeWindowContext = timeWindowContext;
    }

    /**
     * Sets the material.
     *
     * @param material material.
     */
    public void setMaterial(final boolean material) {
        this.material = material;
    }

    /**
     * Sets the onLineControl.
     *
     * @param onLineControl onLineControl.
     */
    public void setOnLineControl(final boolean onLineControl) {
        this.onLineControl = onLineControl;
    }

    /**
     * Sets the productionDate.
     *
     * @param productionDate productionDate.
     */
    public void setProductionDate(final Date productionDate) {
        this.productionDate = productionDate;
    }

    /**
     * Sets the productionDateChoice.
     *
     * @param productionDateChoice productionDateChoice.
     */
    public void setProductionDateChoice(final String productionDateChoice) {
        this.productionDateChoice = productionDateChoice;
    }

    /**
     * Sets the trs.
     *
     * @param trs trs.
     */
    public void setTrs(final boolean trs) {
        this.trs = trs;
    }

    /**
     * Sets the twBeginDate.
     *
     * @param twBeginDate twBeginDate.
     */
    public void setTwBeginDate(final Date twBeginDate) {
        this.twBeginDate = twBeginDate;
    }

    /**
     * Sets the twBeginDateChoice.
     *
     * @param twBeginDateChoice twBeginDateChoice.
     */
    public void setTwBeginDateChoice(final String twBeginDateChoice) {
        this.twBeginDateChoice = twBeginDateChoice;
    }

    /**
     * Sets the twBeginTime.
     *
     * @param twBeginTime twBeginTime.
     */
    public void setTwBeginTime(final Date twBeginTime) {
        this.twBeginTime = twBeginTime;
    }

    /**
     * Sets the twEndDate.
     *
     * @param twEndDate twEndDate.
     */
    public void setTwEndDate(final Date twEndDate) {
        this.twEndDate = twEndDate;
    }

    /**
     * Sets the twEndDateChoice.
     *
     * @param twEndDateChoice twEndDateChoice.
     */
    public void setTwEndDateChoice(final String twEndDateChoice) {
        this.twEndDateChoice = twEndDateChoice;
    }

    /**
     * Sets the twEndTime.
     *
     * @param twEndTime twEndTime.
     */
    public void setTwEndTime(final Date twEndTime) {
        this.twEndTime = twEndTime;
    }

    /**
     * Sets the workingPeriod.
     *
     * @param workingPeriod workingPeriod.
     */
    public void setWorkingPeriod(final boolean workingPeriod) {
        this.workingPeriod = workingPeriod;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "EndDayParameterDialogBean [establishmentKey=" + establishmentKey + ", area=" + area + ", areas="
                + areas + ", productionDateChoice=" + productionDateChoice + ", productionDate=" + productionDate
                + ", twBeginDateChoice=" + twBeginDateChoice + ", twBeginDate=" + twBeginDate + ", twBeginTime="
                + twBeginTime + ", twEndDateChoice=" + twEndDateChoice + ", twEndDate=" + twEndDate + ", twEndTime="
                + twEndTime + ", isTimeWindowContext=" + isTimeWindowContext + ", material=" + material
                + ", workingPeriod=" + workingPeriod + ", trs=" + trs + ", onLineControl=" + onLineControl + "]";
    }

}

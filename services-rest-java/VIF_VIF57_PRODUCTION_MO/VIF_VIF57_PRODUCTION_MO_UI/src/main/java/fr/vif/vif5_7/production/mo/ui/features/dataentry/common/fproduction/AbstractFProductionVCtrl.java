/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFProductionVCtrl.java,v $
 * Created on 12 mars 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction;


import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.util.validator.VIFBindException;
import fr.vif.jtech.common.util.i18n.ITransParam;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.input.InputTextFieldView;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.location.business.beans.common.location.LocationKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionWorkBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerWorkBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.constants.Mnemos.NatureMotifParam;
import fr.vif.vif5_7.stock.kernel.business.beans.common.nature.Nature;
import fr.vif.vif5_7.stock.kernel.business.beans.common.nature.NatureKey;
import fr.vif.vif5_7.stock.kernel.business.beans.common.nature.NatureProperties;
import fr.vif.vif5_7.stock.kernel.business.beans.dialog.reasonentry.DReasonEntryBean;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingBBean;
import fr.vif.vif5_7.stock.kernel.ui.dialog.reasonentry.DReasonEntryCtrl;


/**
 * Abstract FProduction viewer controller.
 * 
 * @param <T>
 * @author vr
 */
public abstract class AbstractFProductionVCtrl<T> extends AbstractStandardViewerController<T> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(AbstractFProductionVCtrl.class);

    /**
     * Fire error changed with a UIException.
     * 
     * @param e UI exception to show
     */
    public void fireErrorsChanged(final UIException e) {
        Errors erreurs = new VIFBindException(getBean());
        erreurs.rejectValue("", "",
                I18nClientManager.translate(e.getMainTrad(), true, e.getListeParams().toArray(new ITransParam[0])));
        super.fireErrorsChanged(new UIErrorsException(new BusinessErrorsException(erreurs)));
    }

    /**
     * Gets the workBean.
     * 
     * @category getter
     * @return the workBean.
     */
    public abstract FProductionWorkBean getWorkBean();

    /**
     * Manage the Motif Windows after criteria.
     * 
     * @param workBean workBean which contains the param to manage the motif.
     * @param featureView feature View
     * @param changeListener changeListener
     * @param displayListener displayListener
     * @param vBean ViewerBean
     * 
     */
    public void manageMotifWindow(final FProductionOutputContainerWorkBean workBean, final FeatureView featureView,
            final DialogChangeListener changeListener, final DisplayListener displayListener,
            final FProductionOutputContainerVBean vBean) {

        LOGGER.debug("B - manageMotifWindow(workBean=" + workBean + ", featureView=" + featureView
                + ", changeListener=" + changeListener + ", displayListener=" + displayListener + ", vBean=" + vBean
                + ")");

        // Get the parameter to manage the seizure of motif.
        List<NatureMotifParam> listNatureMotifParam = workBean.getOutputWorkstationParameters().getNaturesMotif();

        if (listNatureMotifParam != null && !listNatureMotifParam.isEmpty()) {
            NatureMotifParam motifParam = null;

            // Nature complementary of the item to save. (FAB, SRAP, DEC, ENC)
            MONatureComplementaryWorkshop natureComplementaryWorkshop = vBean.getBBean()
                    .getNatureComplementaryWorkshop();

            // Nature Motif Param join to Nature complementary of the item to save
            // Example : MANUFACTURING_TO_PROPOSE is linked to MONatureComplementaryWorkshop.MANUFACTURING.
            List<NatureMotifParam> listNatureMotifByNatureCompl = NatureMotifParam
                    .getNautreMotifByMoNatureComp(natureComplementaryWorkshop);

            // Search if we will have to seizure a motif.
            for (NatureMotifParam nmp : listNatureMotifByNatureCompl) {
                if (listNatureMotifParam.contains(nmp)) {
                    motifParam = listNatureMotifParam.get(listNatureMotifParam.indexOf(nmp));
                    break;
                }
            }

            // If the CSORFAB[69] has a value set to the MONatureComplementaryWorkshop corresponding.
            // A motif will be ask for.
            if (motifParam != null) {

                // Define the parameter to load the Reason with type STOCK.
                EstablishmentKey establishmentKey = new EstablishmentKey(getIdCtx().getCompany(), getIdCtx()
                        .getEstablishment());
                Nature nature = new Nature();
                nature.setKey(new NatureKey(establishmentKey, natureComplementaryWorkshop.getValue()));

                NatureProperties natureProperties = new NatureProperties();
                natureProperties.setReasonMandatory(motifParam.getMandatory());
                natureProperties.setReasonToEntry(motifParam.getMandatory());

                nature.setProperties(natureProperties);

                DReasonEntryBean dReasonEntryBean = new DReasonEntryBean();
                dReasonEntryBean.setNature(nature);

                // Show Dialog Motif
                DReasonEntryCtrl.showHeader(
                        featureView,
                        changeListener,
                        displayListener,
                        getModel().getIdentification(),
                        dReasonEntryBean,
                        I18nClientManager.translate(ProductionMo.T34654, false) + "  "
                                + I18nClientManager.translate(ProductionMo.T34653, false) + " : "
                                + I18nClientManager.translate(natureComplementaryWorkshop.getTranslation()));

                // The motif is mandatory and the user chose "Return", so we ask him if he want to cancel its last
                // seizure.
                if (vBean.getReasonCL() == null || StringUtils.isEmpty(vBean.getReasonCL().getCode())) {
                    if (motifParam.getMandatory()) {
                        int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                                I18nClientManager.translate(ProductionMo.T29578, false), MessageButtons.YES_NO);
                        if (ret == MessageButtons.NO) {
                            manageMotifWindow(workBean, featureView, changeListener, displayListener, vBean);
                        } else {
                            vBean.setReasonCheck(false);
                        }
                    } else {
                        vBean.setReasonCheck(true);
                    }
                }

            } else {
                vBean.setReasonCheck(true);
            }
        } else {
            vBean.setReasonCheck(true);
        }

        LOGGER.debug("E - manageMotifWindow(workBean=" + workBean + ", featureView=" + featureView
                + ", changeListener=" + changeListener + ", displayListener=" + displayListener + ", vBean=" + vBean
                + ")");

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object setFieldHelpResult(final Object value, final InputTextFieldView fieldView) throws UIException {

        if (value instanceof FDestockingBBean) {
            LocationKey locKey = ((FDestockingBBean) value).getLocationKey();

            if (StringUtils.isNotEmpty(locKey.getCdepot())) {
                if (getBean() instanceof FProductionOutputContainerVBean) {
                    ((FProductionOutputContainerVBean) getBean()).getEntity().getEntityLocationBean()
                            .getEntityLocationKey().setCdepot(locKey.getCdepot());
                    ((FProductionOutputContainerVBean) getBean()).getEntity().getEntityLocationBean()
                            .getEntityLocationKey().setCemp(locKey.getCemp());
                } else if (getBean() instanceof FProductionInputContainerVBean) {
                    ((FProductionInputContainerVBean) getBean()).getEntity().getEntityLocationBean()
                            .getEntityLocationKey().setCdepot(locKey.getCdepot());
                    ((FProductionInputContainerVBean) getBean()).getEntity().getEntityLocationBean()
                            .getEntityLocationKey().setCemp(locKey.getCemp());
                }
            }
        }
        return super.setFieldHelpResult(value, fieldView);

    }

    /**
     * Set the weight result.
     * 
     * @param result the weight result
     */
    public abstract void setWeightResult(final FCommScaleResult result);

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        return false;
    }
}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOFIView.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo;


import java.awt.event.MouseListener;


/**
 * FSupervisorMO : Feature View Interface.
 * 
 * @author jd
 */
public interface FSupervisorMOFIView {
    /**
     * Add a mouse listener.
     * 
     * @param listener mouse listener to add
     */
    public void addMouseListener(final MouseListener listener);

    /**
     * Remove a Mouse Listener.
     * 
     * @param listener listener to remove.
     */
    public void removeMouseListener(final MouseListener listener);

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionBComparator.java,v $
 * Created on 4 déc. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction;


import java.util.Comparator;

import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;


/**
 * FProductionInputBBean comparator.
 * 
 * @author vr
 */
public class FProductionBComparator implements Comparator<FProductionBBean> {

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(final FProductionBBean o1, final FProductionBBean o2) {
        int ret = 0;
        FProductionBBean b1 = o1;
        FProductionBBean b2 = o2;
        if (b1.getOperationItemBean().getOperationItemKey().getCounter1() < b2.getOperationItemBean()
                .getOperationItemKey().getCounter1()) {
            ret = -1;
        } else if (b1.getOperationItemBean().getOperationItemKey().getCounter2() < b2.getOperationItemBean()
                .getOperationItemKey().getCounter2()) {
            ret = -1;
        } else if (b1.getOperationItemBean().getOperationItemKey().getCounter3() < b2.getOperationItemBean()
                .getOperationItemKey().getCounter3()) {
            ret = -1;
        } else if (b1.getOperationItemBean().getOperationItemKey().getCounter4() < b2.getOperationItemBean()
                .getOperationItemKey().getCounter4()) {
            ret = -1;
        } else {
            ret = 1;
        }
        return ret;
    }

}

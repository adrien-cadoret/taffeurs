/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputFModel.java,v $
 * Created on 25 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.GeneralSelectionMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;


/**
 * ProductionOutputContainer : Feature Model.
 * 
 * @author vr
 */
public class FBoningOutputFModel extends StandardFeatureModel {

    /**
     * Default constructor.
     * 
     */
    public FBoningOutputFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FBoningOutputBModel.class, null, FBoningOutputBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FBoningOutputVModel.class, null, FBoningOutputVCtrl.class));
        setGeneralSelectionTriad(new GeneralSelectionMVCTriad(FBoningOutputGSModel.class, null,
                FBoningOutputGSCtrl.class));
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOIView.java,v $
 * Created on 22 Janv 2014 by lm
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch;


import fr.vif.vif5_7.workshop.device.ui.composites.marking.IMarkingHeadsCtrl;
import fr.vif.vif5_7.workshop.device.ui.composites.marking.IMarkingSingleListener;


/**
 * Interface: method to get the IMarkingHeadsCtrl.
 * 
 * 
 * @author lm
 */
public interface FMarkingMOIView {

    /**
     * Get the IMarkingHeadsCtrl.
     * 
     * @return IMarkingHeadsCtrl
     */
    public IMarkingHeadsCtrl getIMarkingHeadsCtrl();

    /**
     * Get the IMarkingSingleListener.
     * 
     * @return IMarkingSingleListener
     */
    public IMarkingSingleListener getIMarkingSingleListener();

}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMORenderer.java,v $
 * Created on 24 janv. 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.ui.Layer;


/**
 * Renderer of the Gantt view.
 * 
 * @author ag
 */
public class FGanttMORenderer extends XYBarRenderer {

    /**
     * {@inheritDoc}
     */
    @Override
    public void drawAnnotations(final Graphics2D g2, final Rectangle2D dataArea, final ValueAxis domainAxis,
            final ValueAxis rangeAxis, final Layer layer, final PlotRenderingInfo info) {
        super.drawAnnotations(g2, dataArea, domainAxis, rangeAxis, layer, info);
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningInputFModel.java,v $
 * Created on 23 avr. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.touch.FBoningInputBView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.touch.FBoningInputVView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVModel;


/**
 * Feature model for the boning input.
 * 
 * @author cj
 */
public class FBoningInputFModel extends StandardFeatureModel {

    public static final String FEATURES          = "features";
    public static final String SHOW_ALL          = "showall";
    public static final String FINISH_RESUME     = "finishresume";
    public static final String MOVEMENT_LIST     = "movementlist";
    public static final String PRODUCTION_OUTPUT = "productionoutput";

    public static final String WEIGHT            = "weight";
    public static final String FLASH             = "flash";
    public static final String CANCEL_LAST       = "cancellast";
    public static final String SAVE              = "SAVE";

    /**
     * Default constructor.
     * 
     */
    public FBoningInputFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FBoningInputBModel.class, FBoningInputBView.class, FBoningInputBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FProductionInputContainerVModel.class, FBoningInputVView.class,
                FBoningInputVCtrl.class));

    }

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListContainerBModel.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBeanEnum;


/**
 * DataEntryList : Browser Model.
 * 
 * @author vr
 */
public class FDataEntryListContainerBModel extends BrowserModel<FDataEntryListBBean> {

    /**
     * Default constructor.
     * 
     */
    public FDataEntryListContainerBModel() {
        super();
        setBeanClass(FDataEntryListBBean.class);
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T9520, false),
                FDataEntryListBBeanEnum.LINE_NUMBER.getValue(), 70, "####"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29437, false),
                FDataEntryListBBeanEnum.FORMATTED_CREATION_HOUR.getValue(), 140, "dd/MM/yy HH:mm"));
        addColumn(new BrowserColumn(I18nClientManager.translate(StockKernel.T29089, false),
                FDataEntryListBBeanEnum.CONTAINER_NUMBER.getValue(), 170, "15"));
        addColumn(new BrowserColumn(I18nClientManager.translate(GenKernel.T426, false),
                FDataEntryListBBeanEnum.ITEM_ID.getValue(), 170, "15"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29552, false),
                FDataEntryListBBeanEnum.BATCH.getValue(), 170, "15"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29553, false),
                FDataEntryListBBeanEnum.FORMATTED_FIRST_QTY_UNIT.getValue(), 130));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29553, false),
                FDataEntryListBBeanEnum.FORMATTED_SECOND_QTY_UNIT.getValue(), 130));

        setFetchSize(10000);
        setFirstFetchSize(10000);
    }

}

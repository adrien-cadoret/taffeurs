/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFBoningVCtrl.java,v $
 * Created on 23 avr. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningcommon;


import org.springframework.validation.Errors;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.util.validator.VIFBindException;
import fr.vif.jtech.common.util.i18n.ITransParam;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;


/**
 * Abstract viewer controller for boning input and output.
 * 
 * @param <T> the viewer bean
 * @author cj
 */
public abstract class AbstractFBoningVCtrl<T> extends AbstractStandardViewerController<T> {
    /**
     * Fire error changed with a UIException.
     * 
     * @param e uiexception to show
     */
    public void fireErrorsChanged(final UIException e) {
        Errors erreurs = new VIFBindException(getBean());
        erreurs.rejectValue("", "",
                I18nClientManager.translate(e.getMainTrad(), true, e.getListeParams().toArray(new ITransParam[0])));
        super.fireErrorsChanged(new UIErrorsException(new BusinessErrorsException(erreurs)));
    }

    /**
     * Set the weight result.
     * 
     * @param result the weight result
     */
    public abstract void setWeightResult(final FCommScaleResult result);

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        return false;
    }
}

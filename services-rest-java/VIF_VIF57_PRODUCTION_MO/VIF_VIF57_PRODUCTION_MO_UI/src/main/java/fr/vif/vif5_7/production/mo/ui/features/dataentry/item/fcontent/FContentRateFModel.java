/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FContentRateFModel.java,v $
 * Created on 15 juil. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.GeneralSelectionMVCTriad;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent.touch.FContentRateBView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent.touch.FContentRateGSView;


/**
 * Feature model for content.
 * 
 * @author cj
 */
public class FContentRateFModel extends StandardFeatureModel {

    /**
     * Constructor.
     */
    public FContentRateFModel() {
        super();
        this.setBrowserTriad(new BrowserMVCTriad(FContentRateBModel.class, FContentRateBView.class,
                FContentRateBCtrl.class));
        this.setGeneralSelectionTriad(new GeneralSelectionMVCTriad(FContentRateGSModel.class, FContentRateGSView.class,
                FContentRateGSCtrl.class));
    }
}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListVCtrl.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fdataentrylist.FDataEntryListCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.touch.FDataEntryListVView;


/**
 * DataEntryList : Viewer Controller.
 * 
 * @author vr
 */
public class FDataEntryListVCtrl extends AbstractStandardViewerController<FDataEntryListVBean> {

    private static final Logger LOGGER = Logger.getLogger(FDataEntryListVCtrl.class);

    private FDataEntryListCBS   fdataEntryListCBS;

    /**
     * Gets the fdataEntryListCBS.
     * 
     * @category getter
     * @return the fdataEntryListCBS.
     */
    public final FDataEntryListCBS getFdataEntryListCBS() {
        return fdataEntryListCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();
        setCreationMode(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeViewWithBean(final FDataEntryListVBean bean) {
        super.initializeViewWithBean(bean);

        if (getView() instanceof FDataEntryListVView) {
            ((FDataEntryListVView) getView()).manageContainerView(
                    !StringUtils.isEmpty(bean.getBBean().getProducedEntity()),
                    !StringUtils.isEmpty(bean.getBBean().getTattooedNumber()),
                    !StringUtils.isEmpty(bean.getBBean().getUpperTattooedNumber()));
        }
    }

    /**
     * 
     * Is this detail bean valid ?
     * 
     * @param bBean the FDataEntryListBBean
     * @return is the bean valid
     */
    public boolean isValid(final FDataEntryListBBean bBean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isValid(bBean=" + bBean + ")");
        }

        boolean ret = true;
        if (bBean == null //
                || bBean.getMovementAct() == null //
                || bBean.getMovementAct().getChrono() == 0
                || bBean.getMovementAct().getPrechro() == null
                || bBean.getMovementAct().getLineNumber() == 0 //
                || bBean.getEntityLocationKey() == null) {
            ret = false;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isValid(bBean=" + bBean + ")=" + ret);
        }
        return ret;

    }

    /**
     * Sets the fdataEntryListCBS.
     * 
     * @category setter
     * @param fdataEntryListCBS fdataEntryListCBS.
     */
    public final void setFdataEntryListCBS(final FDataEntryListCBS fdataEntryListCBS) {
        this.fdataEntryListCBS = fdataEntryListCBS;
    }

    /**
     * Show ui errors exceptions in error panel.
     * 
     * @param exception exception to show
     */
    public void showUIErrorsException(final UIErrorsException exception) {
        fireErrorsChanged(exception);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean pCreationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
        // try {
        // FDataEntryListVBean vBean = getBean();
        // // ...
        // } catch (BusinessException e) {
        // UIException ue = new UIException(e);
        // throw ue;
        // }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String pBeanProperty, final Object propertyValue) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FDataEntryListVBean convert(final Object pBean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(pBean=" + pBean + ")");
        }

        FDataEntryListBBean bBean = (FDataEntryListBBean) pBean;
        FDataEntryListVBean vBean = null;
        try {
            vBean = getFdataEntryListCBS().convert(getIdCtx(), bBean);
        } catch (BusinessException e) {
            UIException ue = new UIException(e);
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FDataEntryListVBean pBean) throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FDataEntryListVBean insertBean(final FDataEntryListVBean pBean, final FDataEntryListVBean pInitialBean)
            throws UIException, UIErrorsException {
        return null;
    }

    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean ret = true;

        if (!isValid(getBean().getBBean())) {
            ret = false;
        } else if (FDataEntryListFCtrl.DELETE_BTN.equals(reference)
                || FDataEntryListFCtrl.CRITERIA_BTN.equals(reference)) {
            AbstractOperationItemBean abstractOperationItemBean = (AbstractOperationItemBean) getSharedContext().get(
                    Domain.DOMAIN_PARENT, FDataEntryListFCtrl.OPERATION_ITEM_BEAN);
            if (abstractOperationItemBean != null
                    && ManagementType.ARCHIVED.equals(abstractOperationItemBean.getOperationItemKey()
                            .getManagementType())) {
                ret = false;
            }
        } else if (FDataEntryListFCtrl.CRITERIA_BTN.equals(reference)) {
            ret = getBean().getBBean().getStockItem().getBatch() != null
                    && !"".equals(getBean().getBBean().getStockItem().getBatch());
        } else if (FDataEntryListFCtrl.PRINT_CONTAINER_BTN.equals(reference)) {
            ret = getBean().getBBean().getStockItem().getBatch() != null
                    && !"".equals(getBean().getBBean().getEntityLocationKey().getContainerNumber());
        } else if (FDataEntryListFCtrl.PRINT_CONTAINER_BTN.equals(reference)) {
            ret = getBean().getBBean().getStockItem().getBatch() != null
                    && !"".equals(getBean().getBBean().getEntityLocationKey().getContainerNumber());
        } else if (FDataEntryListOutputFCtrl.PRINT_UPPER_CONTAINER_BTN.equals(reference)) {
            ret = getBean().getBBean().getStockItem().getBatch() != null
                    && !"".equals(getBean().getBBean().getEntityLocationKey().getUpperContainerNumber());
        } else if (FDataEntryListFCtrl.BTN_OPEN_INCIDENT_REFERENCE.equals(reference)) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FDataEntryListVBean updateBean(final FDataEntryListVBean pBean) throws UIException, UIErrorsException {
        return null;
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationFModel.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.touch.FMOCreationVView;


/**
 * MO creation feautre model.
 * 
 * @author glc
 */
public class FMOCreationFModel extends StandardFeatureModel {

    /**
     * Constructor from superclass.
     */
    public FMOCreationFModel() {
        super();
        this.setBrowserTriad(new BrowserMVCTriad(FMOCreationBModel.class, TouchBrowser.class, FMOCreationBCtrl.class));
        this.setViewerTriad(new ViewerMVCTriad(FMOCreationVModel.class, FMOCreationVView.class, FMOCreationVCtrl.class));
        this.setTitle(I18nClientManager.translate(ProductionMo.T1231, false));
    }

}

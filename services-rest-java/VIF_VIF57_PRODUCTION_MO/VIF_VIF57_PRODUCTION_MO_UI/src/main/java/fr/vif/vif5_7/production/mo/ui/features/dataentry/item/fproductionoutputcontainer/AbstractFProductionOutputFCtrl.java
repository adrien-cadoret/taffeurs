/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFProductionOutputFCtrl.java,v $
 * Created on 6 mars 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif57.stock.entity.business.services.constant.EntityEnum.EntityUnitType;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityUnitInfos;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerCBS;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.AbstractFProductionFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompBCtrl;
import fr.vif.vif5_7.stock.kernel.business.beans.features.quality.nc.fncflow.FNCFlowSharedContextBean;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCConstants;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCActType;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCType;
import fr.vif.vif5_7.stock.kernel.ui.util.StockUITools;
import fr.vif.vif5_7.workshop.device.business.beans.common.devices.ScaleDevice;
import fr.vif.vif5_7.workshop.device.business.beans.common.weighting.SequentialWeighting;


/**
 * AbstractFProductionOutputFCtrl.
 *
 * @author vr
 * @param <C> the CBS interface
 */
public abstract class AbstractFProductionOutputFCtrl<C extends FProductionOutputContainerCBS> extends
        AbstractFProductionFCtrl implements DialogChangeListener, DisplayListener {
    /**
     * Close container.
     */
    public static final String     BTN_CLOSE_CONTAINER             = "BTN_CLOSE_CONTAINER";
    /**
     * Production button reference.
     */
    public static final String     BTN_FPRODUCTION_INPUT_REFERENCE = ButtonReference.MO_INPUT_BTN.getReference();

    public static final String     FDETAIL_ENTRY_CODE              = "VIF.MOOULI1T";
    /** LOGGER. */
    private static final Logger    LOGGER                          = Logger.getLogger(AbstractFProductionOutputFCtrl.class);

    private OperationItemKey       current;

    private ToolBarBtnStdModel     btnCloseContainerModel;

    private ToolBarBtnFeatureModel btnDetailModel;

    private ToolBarBtnStdModel     btnFProductionInputModel;

    private FMOListVBean           moListVBean                     = null;

    private C                      fproductionOutputContainerCBS;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        super.buttonAction(event);
        if (BTN_FINISH_REFERENCE.equals(event.getModel().getReference())) {
            processFinishResume();
        } else if (BTN_WEIGHT_REFERENCE.equals(event.getModel().getReference())) {
            performWeight();

        } else if (BTN_VALIDATE_REFERENCE.equalsIgnoreCase(event.getModel().getReference())) {
            getViewerCtrl().performSave();
        } else if (BTN_OPEN_CONTAINER.equalsIgnoreCase(event.getModel().getReference())) {
            getViewerCtrl().openContainer();
        } else if (BTN_OPEN_TATTOOED.equalsIgnoreCase(event.getModel().getReference())) {
            getViewerCtrl().openTattooed();
        } else if (ButtonReference.SETTING_INFORMATION.getReference().equals(event.getModel().getReference())) {
            viewSettingInformation(getViewerBean().getOutputItemParameters().getProfileId(), getViewerBean()
                    .getOutputItemParameters().getProfileLabel());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void childFeatureClosed(final FeatureController childFeatureController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }

        super.childFeatureClosed(childFeatureController);
        getBrowserCtrl().reopenQuery();
        // Reposition to current bean
        if (current != null) {
            getBrowserCtrl().repositionToOperationItemKey(current);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closingRequired(final ClosingEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - closingRequired(event=" + event + ")");
        }

        super.closingRequired(event);
        // do not close and print container if there is no MO key (it's possible if there is an error when opening the
        // feature)
        if (getViewerCtrl().getWorkBean().getMoKey().getChrono() != null) {
            try {
                getFproductionOutputContainerCBS().closePrintContainersOnExit(getIdCtx(), getViewerBean(),
                        getViewerCtrl().getWorkBean());
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
        }
        // ---------------------------------------------------------------------
        // if the workstation has a scale --> stop it before closing the feature
        // ---------------------------------------------------------------------
        if (getViewerCtrl().getWorkBean().getScale() != null
                && !getViewerCtrl().getWorkBean().getScale().getFDDIP().isEmpty()) {
            getFeatureView().getScaleView().getController().stopJmsAction();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - closingRequired(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
    }

    /**
     * Gets the fproductionOutputContainerCBS.
     *
     * @return the fproductionOutputContainerCBS.
     */
    public C getFproductionOutputContainerCBS() {
        return fproductionOutputContainerCBS;
    }

    /**
     * Gets the moListVBean.
     *
     * @return the moListVBean.
     */
    public FMOListVBean getMoListVBean() {
        return moListVBean;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        getViewerCtrl().setFeatureView(getView());
        getViewerCtrl().setFeatureCtrl(this);
        getViewerCtrl().setBrowserCtrl(getBrowserCtrl());
        getBrowserController().addTableRowChangeListener(getViewerCtrl());
        FProductionSBean sBean = null;
        moListVBean = (FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.MO_LIST_VBEAN);
        if (getMoListVBean() != null) {

            // -------------------------------------------
            // Get workBean and initialize scale component
            // --------------------------------------------
            try {
                getViewerCtrl().setWorkBean(getFproductionOutputContainerCBS().getWorkBean(getIdCtx()));
                getViewerCtrl().getWorkBean().setMoKey(getMoListVBean().getMobean().getMoKey());
                getViewerCtrl().getWorkBean().setRealWorkstationId(getIdCtx().getWorkstation());
                getViewerCtrl().getWorkBean().setLogicalWorkstationId(
                        (String) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.LOGICAL_WORKSTATION));
                getViewerCtrl().getWorkBean().setInputAvailable(
                        getMoListVBean().getListInputOperations() != null
                        && getMoListVBean().getListInputOperations().size() > 0);

                ScaleDevice scale = getViewerCtrl().getWorkBean().getScale();
                if (scale != null && !scale.getFDDIP().isEmpty()) {
                    getFeatureView().getScaleView().getController().initializeFDD(scale, getIdCtx().getWorkstation());
                    getFeatureView().getScaleView().getController().startJmsAction();
                }

            } catch (BusinessException e) {
                getView().show(new UIException(e));
            } catch (UIException e) {
                getView().show(e);
            }

            // -------------------------------
            // Initialize browser selection
            // -------------------------------
            sBean = new FProductionSBean();
            sBean.setActivityItemType(ActivityItemType.OUTPUT);
            sBean.setWorkstationId((String) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.LOGICAL_WORKSTATION));
            sBean.setAll(false);
            sBean.setMoKey(getMoListVBean().getMobean().getMoKey());

            // ------------------------
            // Set Subtitle feature
            // ------------------------
            getFeatureView().setSubTitle(
                    I18nClientManager.translate(ProductionMo.T29524, false)
                            + " - "
                            + I18nClientManager.translate(ProductionMo.T29500, false, new VarParamTranslation(
                                    getMoListVBean().getMobean().getMoKey().getChrono().getChrono())) + " - "
                            + getMoListVBean().getMobean().getLabel());
        }
        // For feature with complementary nature, do not overwrite the selection
        if (!(getBrowserController() instanceof FProductionOutputCompBCtrl)) {
            getBrowserController().setCurrentSelection(sBean);
            getBrowserController().setInitialSelection(sBean);
        }

        TouchModelFunction touchModelFunction = null;

        if (getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.USE_TOUCH_MODEL) != null) {
            boolean useTouchModel = (Boolean) getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.USE_TOUCH_MODEL);
            if (useTouchModel) {
                TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT,
                        TouchModel.TOUCH_MODEL);
                if (touchModel != null) {
                    touchModelFunction = touchModel.getTouchModelFunction(TouchModelMapKey.CONTAINER_OUTPUT_PRODUCTION);
                }
                ((FProductionBCtrl) getBrowserController()).changeColumns(touchModelFunction);
            }
        }

        super.initialize();
        setErrorPanelAlwaysVisible(true);
        getViewerCtrl().getViewerView().addContainerListener(this, this);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Sets the fproductionOutputContainerCBS.
     *
     * @param fproductionOutputContainerCBS fproductionOutputContainerCBS.
     */
    public void setFproductionOutputContainerCBS(final C fproductionOutputContainerCBS) {
        this.fproductionOutputContainerCBS = fproductionOutputContainerCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void actionOpenNewIncident() {

        // Prepare parameters bean
        FNCFlowSharedContextBean parametersBean = new FNCFlowSharedContextBean();
        parametersBean.setType(NCType.INTERNAL.getKey());

        // Feed with viewer bean informations
        FProductionOutputContainerVBean vBean = (FProductionOutputContainerVBean) getViewerController().getBean();
        parametersBean.setActType(NCActType.MANUFACTURING_ORDER.getKey());
        if (vBean.getBBean().getOperationItemBean() != null) { // ALB Q7369 If all the MO's item are finished, there was
            // null pointer exception
            parametersBean.setChronoOrigin(vBean.getBBean().getOperationItemBean().getOperationItemKey().getChrono());
        }
        parametersBean.setContainerNumber(vBean.getEntity().getEntityLocationBean().getEntityLocationKey()
                .getContainerNumber());
        StockItem stockItem = new StockItem();
        stockItem.setItemId(vBean.getEntity().getStockItemBean().getStockItem().getItemId());
        stockItem.setBatch(vBean.getEntity().getStockItemBean().getStockItem().getBatch());
        stockItem.setQuantities(vBean.getEntity().getStockItemBean().getKeyboardQties());
        parametersBean.setStockItem(stockItem);
        // Put parameters in context and open feature
        getSharedContext().put(Domain.DOMAIN_THIS, NCConstants.NCFLOW_KEY_SHARED_CONTEXT, parametersBean);
        if (StockUITools.isTouchRunning()) {
            openChildFeature(NCConstants.FeatureName.FLOW_TOUCH.getName());
        } else {
            openChildFeature(NCConstants.FeatureName.FLOW.getName());
        }

    }

    /**
     * Gets the btnFProductionInputModel.
     * 
     * @category getter
     * @return the btnFProductionInputModel.
     */
    protected ToolBarBtnStdModel getBtnCloseContainerModel() {
        if (this.btnCloseContainerModel == null) {
            this.btnCloseContainerModel = new ToolBarBtnStdModel();
            this.btnCloseContainerModel.setReference(BTN_CLOSE_CONTAINER);
            this.btnCloseContainerModel.setText(I18nClientManager.translate(ProductionMo.T29710, false));
            this.btnCloseContainerModel.setIconURL("/images/vif/vif57/production/mo/img48x48/CloseContainer.png");
        }
        return this.btnCloseContainerModel;
    }

    /**
     * Gets the current.
     *
     * @return the current.
     */
    protected OperationItemKey getCurrent() {
        return current;
    }

    @Override
    protected List<PDFFile> getDocumentsToShow() throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDocumentsToShow()");
        }

        MOItemKey moItemKey = new MOItemKey();
        moItemKey.setEstablishmentKey(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getEstablishmentKey());
        moItemKey.setChrono(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getChrono());
        moItemKey.setCounter1(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getCounter1());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDocumentToShow()");
        }
        return getFproductionOutputContainerCBS().getPdfFilesOfCurrentItem(getIdCtx(), moItemKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getExternalButtonModels() {

        List<ToolBarBtnStdBaseModel> lst = super.getExternalButtonModels();
        lst.add(getBtnCancelLastModel());
        lst.add(getBtnCloseContainerModel());
        lst.add(getBtnWeightModel());
        lst.add(getBtnValidateModel());

        return lst;
    }

    /**
     * Get the feature view.
     * 
     * @return the feature view
     */
    protected FProductionOutputContainerFIView getFeatureView() {
        return (FProductionOutputContainerFIView) getView();

    }

    /**
     * Gets the viewer controller.
     * 
     * @return The viewer Controller.
     */
    protected FProductionOutputContainerVBean getViewerBean() {
        return (FProductionOutputContainerVBean) getViewerController().getBean();
    }

    /**
     * Gets the viewer controller.
     * 
     * @return The viewer Controller.
     */
    protected AbstractFProductionOutputVCtrl getViewerCtrl() {
        return (AbstractFProductionOutputVCtrl) getViewerController();
    }

    /**
     * Finishes or unfinishes an item.
     */
    protected abstract void processFinishResume();

    /**
     * Sets the current.
     *
     * @param current current.
     */
    protected void setCurrent(final OperationItemKey current) {
        this.current = current;
    }

    /**
     * Sets the moListVBean.
     *
     * @param moListVBean moListVBean.
     */
    protected void setMoListVBean(final FMOListVBean moListVBean) {
        this.moListVBean = moListVBean;
    }

    /**
     * Perform weight.
     */
    private void performWeight() {
        FCommScaleResult result = null;
        try {
            EntityUnitInfos unitInfos = getViewerBean().getEntity().getStockItemBean().getUnitsInfos();
            String unit = null;
            if (EntityUnitType.WEIGHT.equals(unitInfos.getFirstUnitType())) {
                unit = getViewerBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getUnit();
            } else if (EntityUnitType.WEIGHT.equals(unitInfos.getSecondUnitType())) {
                unit = getViewerBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().getUnit();
            } else if (EntityUnitType.WEIGHT.equals(unitInfos.getThirdUnitType())) {
                unit = getViewerBean().getEntity().getStockItemBean().getKeyboardQties().getThirdQty().getUnit();
            }

            SequentialWeighting sequentialWeighingInfo = new SequentialWeighting();
            sequentialWeighingInfo.setEstablishmentKey(getViewerBean().getBBean().getOperationItemBean()
                    .getOperationItemKey().getEstablishmentKey());
            sequentialWeighingInfo.setUnit(unit);
            sequentialWeighingInfo.setUserId(getIdCtx().getLogin());
            sequentialWeighingInfo.setWeightKey(getWeightKey(getViewerBean().getBBean().getOperationItemBean()
                    .getOperationItemKey()));
            sequentialWeighingInfo.setWeightKeyType("XPILOIA");
            sequentialWeighingInfo.setWorkStationId(getViewerCtrl().getWorkBean().getLogicalWorkstationId());
            result = getFeatureView().getScaleView().getController()
                    .getLockAndSaveResult(getIdCtx(), sequentialWeighingInfo);
            getViewerCtrl().setWeightResult(result);
        } catch (UIException e) {
            getViewerCtrl().fireErrorsChanged(e);
        }
    }

}

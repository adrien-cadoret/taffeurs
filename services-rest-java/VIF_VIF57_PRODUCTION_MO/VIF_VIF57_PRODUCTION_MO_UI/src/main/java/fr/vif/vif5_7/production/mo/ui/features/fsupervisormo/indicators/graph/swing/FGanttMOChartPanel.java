/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOChartPanel.java,v $
 * Created on 30 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollBar;
import javax.swing.JSeparator;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartMouseEvent;
import org.jfree.chart.ChartMouseListener;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.chart.entity.XYItemEntity;
import org.jfree.chart.plot.XYPlot;
import org.jfree.data.Range;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOPeriod;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.GanttMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.GraphMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.IGraphMOView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FSupervisorMOPanel;


/**
 * Graph task.
 * 
 * @author jd
 * @param <SRC> source of the bean.
 */
public class FGanttMOChartPanel<SRC> extends FSupervisorMOPanel implements ChartMouseListener, ActionListener,
MouseListener, IGraphMOView, IGanttMoView<SRC>, AdjustmentListener, MouseWheelListener {
    /** LOGGER. */
    private static final Logger      LOGGER                 = Logger.getLogger(FGanttMOChartPanel.class);

    private static final int         STEP                   = 1;

    private List<ChartMouseListener> chartMouseListeners    = new ArrayList<ChartMouseListener>();

    private ChartPanel               chartPanel;

    private GraphMO                  graph                  = new GraphMO();

    private GraphMOOptions           graphOptions           = new GraphMOOptions();

    private ChartEntity              lastMouseEntityClicked = null;

    private Point                    lastPoint              = new Point();

    private List<JMenu>              menus                  = new ArrayList<JMenu>();

    private XYPlot                   plot                   = null;

    /** Horizontal scrollbar. */
    private JScrollBar               scrollBar1;

    /**
     * Constructor.
     */
    public FGanttMOChartPanel() {
        super();
        // this.chartPanel = GraphMOTools.createGraphPanel(new GraphMO(), new GraphMOOptions());
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param chartPanel {@link ChartPanel}
     */
    public FGanttMOChartPanel(final ChartPanel chartPanel) {
        super();
        this.chartPanel = chartPanel;
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(e=" + e + ")");
        }

        String actionCommand = e.getActionCommand();

        try {
            int newHorizon = Integer.parseInt(actionCommand);
            getGraphOptions().setNbMinutesToShow(newHorizon * 60);
        } catch (NumberFormatException e1) {
            System.out.println(e1.getStackTrace());
        }

        ChartEntity chartEntity = getChartPanel().getEntityForPoint((int) lastPoint.getX(), (int) lastPoint.getY());

        GenericActionEvent event = new GenericActionEvent(e.getSource(), GenericActionEvent.EVENT_ACTION_PERFORMED,
                e.getActionCommand(), chartEntity);

        fireGenericAction(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(e=" + e + ")");
        }
    }

    /**
     * Add Chart Mouse listener.
     * 
     * @param listener listener.
     */
    public void addChartMouseListener(final ChartMouseListener listener) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - addChartMouseListener(listener=" + listener + ")");
        }

        chartMouseListeners.add(listener);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - addChartMouseListener(listener=" + listener + ")");
        }
    }

    /**
     * Add a menu to the default popup menu.
     * 
     * @param popToAdd menu to add.
     */
    public void addPopupMenu(final JMenu popToAdd) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - addPopupMenu(popToAdd=" + popToAdd + ")");
        }

        menus.add(popToAdd);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - addPopupMenu(popToAdd=" + popToAdd + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void adjustmentValueChanged(final AdjustmentEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - adjustmentValueChanged(e=" + e + ")");
        }

        if (e.getSource() == getScrollBar1()) {
            int value = getScrollBar1().getValue() * STEP;
            graphOptions.setFirstMinuteToShow(value);
            refresh(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - adjustmentValueChanged(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void chartMouseClicked(final ChartMouseEvent arg0) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - chartMouseClicked(arg0=" + arg0 + ")");
        }

        for (ChartMouseListener l : chartMouseListeners) {
            l.chartMouseClicked(arg0);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - chartMouseClicked(arg0=" + arg0 + ")");
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void chartMouseMoved(final ChartMouseEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    public ChartPanel createChartPanel() {

        // Clear the graph component
        // remove(getChartPanel());

        ChartPanel chartPan = null;

        if (getGraph() == null) {
            setGraph(new GraphMO());
        }

        setPlot(GraphMOTools.createXYPlot(getGraph(), getGraphOptions()));

        chartPan = GraphMOTools.createGraphPanel(getGraph(), getGraphOptions(), getPlot());

        chartPan.addChartMouseListener(this);

        chartPan.addMouseListener(this);

        // Remove problematic menus
        for (Component c : chartPan.getPopupMenu().getComponents()) {
            if (c instanceof JMenuItem) {
                if (((JMenuItem) c).getModel().getActionCommand() == null || // ZOOM and SCALE
                        "PROPERTIES".equalsIgnoreCase(((JMenuItem) c).getModel().getActionCommand())) {
                    chartPan.getPopupMenu().remove(c);
                }
            }
        }

        chartPan.repaint();
        chartPan.validate();
        // validate();

        return chartPan;
    }

    /**
     * Gets the chartPanel.
     * 
     * @return the chartPanel.
     */
    public ChartPanel getChartPanel() {
        if (chartPanel == null) {
            chartPanel = createChartPanel();
            GridBagConstraints g = new GridBagConstraints();
            g.weightx = 1;
            g.weighty = 1;
            g.fill = GridBagConstraints.BOTH;
            g.gridx = 0;
            g.gridy = 1;

            add(chartPanel, g);
        }
        return chartPanel;
    }

    /**
     * Gets the graph.
     *
     * @return the graph.
     */
    public GraphMO getGraph() {
        return graph;
    }

    /**
     * Gets the graphOptions.
     *
     * @return the graphOptions.
     */
    public GraphMOOptions getGraphOptions() {
        return graphOptions;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public GraphMOPeriod<SRC> getLastClickedGraphPeriod() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLastClickedGraphPeriod()");
        }

        GraphMOPeriod<SRC> period = null;
        ChartEntity entity = getLastMouseEntityClicked();

        if (entity != null) {
            // We can change horizon by clicking on area and not on OT
            if (entity instanceof XYItemEntity) {
                int serie = ((XYItemEntity) entity).getSeriesIndex();
                int item = ((XYItemEntity) entity).getItem();
                // Be careful this algo is used in FIndicatorGanttChartPanel too
                period = graph
                        .getGraphData()
                        .get(graphOptions.getStartDataSet()
                                + Math.min(graph.getGraphData().size(), graphOptions.getNumberOfDataSetMax()) - serie
                                - 1).getGraphData().get(item).getSecondAsGraphPeriod();
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getLastClickedGraphPeriod()=" + period);
        }
        return period;
    }

    /**
     * {@inheritDoc}
     */
    public ChartEntity getLastMouseEntityClicked() {
        return lastMouseEntityClicked;
    }

    /**
     * {@inheritDoc}
     */
    public Point getLastPoint() {
        return lastPoint;
    }

    /**
     * Gets the plot.
     *
     * @return the plot.
     */
    public XYPlot getPlot() {
        return plot;
    }

    /**
     * Get the scroll bar.
     * 
     * @return {@link JScrollBar}
     */
    public JScrollBar getScrollBar1() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getScrollBar1()");
        }

        if (scrollBar1 == null) {

            scrollBar1 = new JScrollBar(JScrollBar.HORIZONTAL, getGraphOptions().getFirstMinuteToShow(), STEP,
                    getGraphOptions().getLowerBound(), getGraphOptions().getUpperBound());

            scrollBar1.addAdjustmentListener(this);

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getScrollBar1()=" + scrollBar1);
        }
        return scrollBar1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final MouseEvent e) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - mouseClicked(e=" + e + ")");
        }

        if (e.getButton() == 1 && e.getClickCount() == 2) {
            lastPoint = new Point(e.getX(), e.getY());
            lastMouseEntityClicked = getChartPanel().getEntityForPoint((int) lastPoint.getX(), (int) lastPoint.getY());
            ActionEvent evt = new ActionEvent(this, 0, FSupervisorMOTools.CLICK_ON_CELL);
            actionPerformed(evt);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - mouseClicked(e=" + e + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - mouseClicked(e=" + e + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - mouseClicked(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseEntered(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseExited(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final MouseEvent e) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - mousePressed(e=" + e + ")");
        }

        if (e.getButton() == 3 && e.getClickCount() == 1) {
            lastPoint = new Point(e.getX(), e.getY());
            lastMouseEntityClicked = getChartPanel().getEntityForPoint((int) lastPoint.getX(), (int) lastPoint.getY());

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - mousePressed(e=" + e + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - mousePressed(e=" + e + ")");
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseWheelMoved(final MouseWheelEvent e) {
    }

    /**
     * refresh.
     * 
     * @param withChartRemoving TODO
     */
    public void refresh(final boolean withChartRemoving) {

        if (withChartRemoving) {

            if (chartPanel != null) {
                remove(chartPanel);
            }

            chartPanel = null;

            getChartPanel();

            if (getGraphOptions().getOverview()) {
                getScrollBar1().setEnabled(false);
            } else {
                getScrollBar1().setMinimum(getGraphOptions().getLowerBound());
                getScrollBar1().setMaximum(getGraphOptions().getUpperBound());
                getScrollBar1().setValue(getGraphOptions().getFirstMinuteToShow());
                getScrollBar1().setEnabled(true);
            }

            // scrollBar1 = null;

            /* FB DP2581 Remove double separator */
            Component last = null;
            for (Component comp : chartPanel.getPopupMenu().getComponents()) {
                if (comp instanceof JSeparator && last instanceof JSeparator) {
                    chartPanel.getPopupMenu().remove(comp);
                } else {
                    last = comp;
                }
            }
            /* FB DP2581 */

            refresh(false);
        } else {

            List<Date> bounds = calculatePlotRange();

            plot.getRangeAxis(0).setRange(new Range(bounds.get(0).getTime(), bounds.get(1).getTime()));
            chartPanel.repaint();
            validate();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setGraph(final GraphMO graph) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setGraph(graph=" + graph + ")");
        }

        setGraph(graph, graphOptions);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setGraph(graph=" + graph + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setGraph(final GraphMO graphMO, final GraphMOOptions options) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setGraph(graph=" + graphMO + ", options=" + options + ")");
        }

        this.graph = graphMO;
        this.graphOptions = options;

        if (graph.getVerticalLabel() == null) {
            graph.setVerticalLabel(I18nClientManager.translate(GenKernel.T29921, false));
        }
        if (graph.getHorizontalLabel() == null) {
            graph.setHorizontalLabel(I18nClientManager.translate(Jtech.T32134, false));
        }

        // If there are at least one dataset
        if (options.getBeginOfFirstMO().compareTo(options.getEndOfLastMO()) < 0) {
            refresh(true);
        } else {
            refresh(true);
            getScrollBar1().setEnabled(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setGraph(graph=" + graphMO + ", options=" + options + ")");
        }
    }

    /**
     * Sets the graphOptions.
     *
     * @param graphOptions graphOptions.
     */
    public void setGraphOptions(final GraphMOOptions graphOptions) {
        this.graphOptions = graphOptions;
    }

    /**
     * Sets the plot.
     *
     * @param plot plot.
     */
    public void setPlot(final XYPlot plot) {
        this.plot = plot;
    }

    /**
     * 
     * Calculate the plot's range related on options.
     * 
     * @return a list of two dates (min and max)
     */
    private List<Date> calculatePlotRange() {
        List<Date> bounds = new ArrayList<Date>();
        if (getGraphOptions().getOverview() | (getGraphOptions().getUpperBound() < getGraphOptions().getLowerBound())) {
            // Margin in Overview case
            getGraphOptions().getBeginOfFirstMO().add(Calendar.MINUTE, -GanttMOTools.MARGIN_IN_GRAPH);
            getGraphOptions().getEndOfLastMO().add(Calendar.MINUTE, +GanttMOTools.MARGIN_IN_GRAPH);
            bounds.add(getGraphOptions().getBeginOfFirstMO().getTime());
            bounds.add(getGraphOptions().getEndOfLastMO().getTime());

        } else {
            int lowerBound = getGraphOptions().getLowerBound();
            int upperBound = getGraphOptions().getUpperBound();
            int firstMinuteToShow = getGraphOptions().getFirstMinuteToShow();

            if (firstMinuteToShow < lowerBound) {
                bounds.add(convertMinuteToDate(lowerBound));
                bounds.add(convertMinuteToDate(lowerBound + getGraphOptions().getNbMinutesToShow()));
            } else if (firstMinuteToShow > upperBound) {
                bounds.add(convertMinuteToDate(upperBound));
                bounds.add(convertMinuteToDate(upperBound + getGraphOptions().getNbMinutesToShow()));
            } else if (firstMinuteToShow >= lowerBound && firstMinuteToShow <= upperBound) {
                bounds.add(convertMinuteToDate(firstMinuteToShow));
                bounds.add(convertMinuteToDate(firstMinuteToShow + getGraphOptions().getNbMinutesToShow()));
            }
        }
        return bounds;

    }

    /**
     * 
     * Converts a number of minute in a Date.
     * 
     * @param minutes the number of minutes.
     * @return a Date
     */
    private Date convertMinuteToDate(final int minutes) {
        Date date = null;
        Calendar firstDateToShow = Calendar.getInstance();
        firstDateToShow.setTime(getGraphOptions().getSelectedDate());

        // Convert nbMinutesToShow in HOURS and MINUTES
        int numberHours = minutes / 60;
        int numberMinutes = minutes % 60;

        firstDateToShow.add(Calendar.HOUR_OF_DAY, numberHours);
        firstDateToShow.add(Calendar.MINUTE, numberMinutes);

        date = firstDateToShow.getTime();

        return date;
    }

    /**
     * initialize.
     */
    private void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        this.setLayout(new GridBagLayout());

        GridBagConstraints g = new GridBagConstraints();
        g.weightx = 1;
        g.weighty = 1;
        g.fill = GridBagConstraints.BOTH;
        g.gridx = 0;
        g.gridy = 1;

        add(getChartPanel(), g);

        addMouseListener(this);

        GridBagConstraints gbcScrollBar1 = new GridBagConstraints();
        gbcScrollBar1.weightx = 1.0;
        gbcScrollBar1.fill = GridBagConstraints.HORIZONTAL;
        gbcScrollBar1.insets = new Insets(0, 0, 0, 5);
        gbcScrollBar1.gridx = 0;
        gbcScrollBar1.gridy = 2;
        gbcScrollBar1.gridheight = 1;
        add(getScrollBar1(), gbcScrollBar1);

        getGraphOptions().setNumberOfDataSetMax(15);

        // refresh(true);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }
}

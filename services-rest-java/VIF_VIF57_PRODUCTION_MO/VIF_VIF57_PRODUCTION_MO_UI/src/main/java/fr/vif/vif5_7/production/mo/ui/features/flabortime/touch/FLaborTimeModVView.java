/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeModVView.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.touch;


import java.awt.Dimension;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.ImageIcon;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.ui.button.touch.TouchBtnStd;
import fr.vif.jtech.ui.input.InputFieldActionEvent;
import fr.vif.jtech.ui.input.InputFieldActionListener;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.RoundBackgroundPanelBorder;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeModVBean;


/**
 * labor staff viewer view.
 * 
 * @author nle
 */
public class FLaborTimeModVView extends StandardTouchViewer<FLaborTimeModVBean> {

    private static final Logger LOGGER       = Logger.getLogger(FLaborTimeModVView.class);

    private static final String DATE_PATTERN = "dd/MM/yy HH:mm";

    private static final String ADD          = "addBtn";
    private static final String MODIF        = "modifBtn";

    private TouchInputTextField itfPreviousEndDate;
    private TouchInputTextField itfPreviousEndHour;
    private TouchInputTextField itfCurrentBegDate;
    private TouchInputTextField itfCurrentBegHour;
    private TouchInputTextField itfCurrentEndDate;
    private TouchInputTextField itfCurrentEndHour;
    private TouchInputTextField itfNextBegDate;
    private TouchInputTextField itfNextBegHour;
    private TouchBtnStd         currentBegDateNextBtn;
    private TouchBtnStd         currentBegDatePrevBtn;
    private TouchBtnStd         currentEndDateNextBtn;
    private TouchBtnStd         currentEndDatePrevBtn;

    /**
     * Constructor.
     * 
     */
    public FLaborTimeModVView() {
        super();
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param itfPreviousEndDate the end date of the previous Labor Time
     * @param itfPreviousEndHour the end hour of the previous Labor Time
     * @param itfCurrentBegDate the begin date of the current Labor Time
     * @param itfCurrentBegHour the begin hour of the current Labor Time
     * @param itfCurrentEndDate the end date of the current Labor Time
     * @param itfCurrentEndHour the end hour of the current Labor Time
     * @param itfNextBegDate the begin date of the next Labor Time
     * @param itfNextBegHour the begin hour of the next Labor Time
     * @param nextCurrentBegDateBtn the next button of the begin date of the current Labor Time
     * @param prevCurrentBegDateBtn the previous button of the begin date of the current Labor Time
     * @param nextCurrentEndDateBtn the next button of the end date of the current Labor Time
     * @param prevCurrentEndDateBtn the previous button of the end date of the current Labor Time
     */
    public FLaborTimeModVView(final TouchInputTextField itfPreviousEndDate,
            final TouchInputTextField itfPreviousEndHour, final TouchInputTextField itfCurrentBegDate,
            final TouchInputTextField itfCurrentBegHour, final TouchInputTextField itfCurrentEndDate,
            final TouchInputTextField itfCurrentEndHour, final TouchInputTextField itfNextBegDate,
            final TouchInputTextField itfNextBegHour, final TouchBtnStd nextCurrentBegDateBtn,
            final TouchBtnStd prevCurrentBegDateBtn, final TouchBtnStd nextCurrentEndDateBtn,
            final TouchBtnStd prevCurrentEndDateBtn) {
        super();
        this.itfPreviousEndDate = itfPreviousEndDate;
        this.itfPreviousEndHour = itfPreviousEndHour;
        this.itfCurrentBegDate = itfCurrentBegDate;
        this.itfCurrentBegHour = itfCurrentBegHour;
        this.itfCurrentEndDate = itfCurrentEndDate;
        this.itfCurrentEndHour = itfCurrentEndHour;
        this.itfNextBegDate = itfNextBegDate;
        this.itfNextBegHour = itfNextBegHour;
        this.currentBegDateNextBtn = nextCurrentBegDateBtn;
        this.currentBegDatePrevBtn = prevCurrentBegDateBtn;
        this.currentEndDateNextBtn = nextCurrentEndDateBtn;
        this.currentEndDatePrevBtn = prevCurrentEndDateBtn;
    }

    /**
     * Gets the nextCurrentBegDateBtn.
     * 
     * @return the nextCurrentBegDateBtn.
     */
    public TouchBtnStd getCurrentBegDateNextBtn() {
        if (currentBegDateNextBtn == null) {
            currentBegDateNextBtn = new TouchBtnStd();
            currentBegDateNextBtn.setBounds(301, 127, 54, 54);
            currentBegDateNextBtn.setPreferredSize(new Dimension(54, 54));
            currentBegDateNextBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayAfter.png")));
            currentBegDateNextBtn.setEnabled(true);
            currentBegDateNextBtn.setReference("currentBegDateNextBtn");
            currentBegDateNextBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    addCurrentBegDate();
                }

            });
        }
        return currentBegDateNextBtn;
    }

    /**
     * Gets the prevCurrentBegDateBtn.
     * 
     * @return the prevCurrentBegDateBtn.
     */
    public TouchBtnStd getCurrentBegDatePrevBtn() {
        if (currentBegDatePrevBtn == null) {
            currentBegDatePrevBtn = new TouchBtnStd();
            currentBegDatePrevBtn.setBounds(140, 127, 54, 54);
            currentBegDatePrevBtn.setPreferredSize(new Dimension(54, 54));
            currentBegDatePrevBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayBefore.png")));
            currentBegDatePrevBtn.setEnabled(true);
            currentBegDatePrevBtn.setReference("currentBegDatePrevBtn");
            currentBegDatePrevBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    subCurrentBegDate();
                }

            });
        }
        return currentBegDatePrevBtn;
    }

    /**
     * Gets the nextCurrentEndDateBtn.
     * 
     * @return the nextCurrentEndDateBtn.
     */
    public TouchBtnStd getCurrentEndDateNextBtn() {
        if (currentEndDateNextBtn == null) {
            currentEndDateNextBtn = new TouchBtnStd();
            currentEndDateNextBtn.setBounds(601, 127, 54, 54);
            currentEndDateNextBtn.setPreferredSize(new Dimension(54, 54));
            currentEndDateNextBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayAfter.png")));
            currentEndDateNextBtn.setEnabled(true);
            currentEndDateNextBtn.setReference("currentEndDateNextBtn");
            currentEndDateNextBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    addCurrentEndDate();
                }

            });
        }
        return currentEndDateNextBtn;
    }

    /**
     * Gets the prevCurrentEndDateBtn.
     * 
     * @return the prevCurrentEndDateBtn.
     */
    public TouchBtnStd getCurrentEndDatePrevBtn() {
        if (currentEndDatePrevBtn == null) {
            currentEndDatePrevBtn = new TouchBtnStd();
            currentEndDatePrevBtn.setBounds(440, 127, 54, 54);
            currentEndDatePrevBtn.setPreferredSize(new Dimension(54, 54));
            currentEndDatePrevBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayBefore.png")));
            currentEndDatePrevBtn.setEnabled(true);
            currentEndDatePrevBtn.setReference("currentEndDatePrevBtn");
            currentEndDatePrevBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    subCurrentEndDate();
                }

            });
        }
        return currentEndDatePrevBtn;
    }

    /**
     * 
     * Get the CurrentBegdate.
     * 
     * @return the itfCurrentBegDate touchinputtextfield
     */
    public TouchInputTextField getItfCurrentBegDate() {
        if (itfCurrentBegDate == null) {
            itfCurrentBegDate = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_DATE,
                    I18nClientManager.translate(ProductionMo.T32542, false));
            itfCurrentBegDate.setSize(102, 70);
            itfCurrentBegDate.setBeanBased(false);
            itfCurrentBegDate.setEnabled(true);
            itfCurrentBegDate.setAlwaysDisabled(false);
            Dimension d = new Dimension(242, 70);
            itfCurrentBegDate.setPreferredSize(d);
            itfCurrentBegDate.setMinimumSize(d);
            itfCurrentBegDate.setMaximumSize(d);

            // don't put the date from database, keep the current date
            // itfCurrentBegDate.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            itfCurrentBegDate.setLocation(196, 111);
        }
        return itfCurrentBegDate;
    }

    /**
     * Gets the itfCurrentBegHour.
     * 
     * @return the itfCurrentBegHour.
     */
    public TouchInputTextField getItfCurrentBegHour() {
        if (itfCurrentBegHour == null) {
            itfCurrentBegHour = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_HOUR, "");
            itfCurrentBegHour.setSize(54, 70);
            itfCurrentBegHour.setBeanBased(false);
            itfCurrentBegHour.setEnabled(true);
            itfCurrentBegHour.setAlwaysDisabled(false);
            Dimension d = new Dimension(141, 70);
            itfCurrentBegHour.setPreferredSize(d);
            itfCurrentBegHour.setMinimumSize(d);
            itfCurrentBegHour.setMaximumSize(d);

            // don't put the time from database, keep the current time
            // itfHour.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            itfCurrentBegHour.setLocation(365, 111);
        }
        return itfCurrentBegHour;
    }

    /**
     * Gets the itfCurrentEndDate.
     * 
     * @return the itfCurrentEndDate.
     */
    public TouchInputTextField getItfCurrentEndDate() {
        if (itfCurrentEndDate == null) {
            itfCurrentEndDate = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_DATE,
                    I18nClientManager.translate(ProductionMo.T32544, false));
            itfCurrentEndDate.setSize(102, 70);
            itfCurrentEndDate.setBeanBased(false);
            itfCurrentEndDate.setEnabled(true);
            itfCurrentEndDate.setAlwaysDisabled(false);
            Dimension d = new Dimension(242, 70);
            itfCurrentEndDate.setPreferredSize(d);
            itfCurrentEndDate.setMinimumSize(d);
            itfCurrentEndDate.setMaximumSize(d);

            // don't put the date from database, keep the current date
            // itfDate.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            itfCurrentEndDate.setLocation(496, 111);
        }
        return itfCurrentEndDate;
    }

    /**
     * Gets the itfCurrentEndHour.
     * 
     * @return the itfCurrentEndHour.
     */
    public TouchInputTextField getItfCurrentEndHour() {
        if (itfCurrentEndHour == null) {
            itfCurrentEndHour = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_HOUR, "");
            itfCurrentEndHour.setSize(54, 70);
            itfCurrentEndHour.setBeanBased(false);
            itfCurrentEndHour.setEnabled(true);
            itfCurrentEndHour.setAlwaysDisabled(false);
            Dimension d = new Dimension(141, 70);
            itfCurrentEndHour.setPreferredSize(d);
            itfCurrentEndHour.setMinimumSize(d);
            itfCurrentEndHour.setMaximumSize(d);

            // don't put the time from database, keep the current time
            // itfHour.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            itfCurrentEndHour.setLocation(665, 111);
        }
        return itfCurrentEndHour;
    }

    /**
     * Gets the itfNextBegDate.
     * 
     * @return the itfNextBegDate.
     */
    public TouchInputTextField getItfNextBegDate() {
        if (itfNextBegDate == null) {
            itfNextBegDate = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_DATE,
                    I18nClientManager.translate(ProductionMo.T32542, false));
            itfNextBegDate.setSize(102, 70);
            itfNextBegDate.setBeanBased(false);
            itfNextBegDate.setEnabled(false);
            itfNextBegDate.setAlwaysDisabled(true);
            Dimension d = new Dimension(242, 70);
            itfNextBegDate.setPreferredSize(d);
            itfNextBegDate.setMinimumSize(d);
            itfNextBegDate.setMaximumSize(d);

            // don't put the date from database, keep the current date
            // itfDate.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            // itfNextBegDate.setLocation(196, 211);
            itfNextBegDate.setLocation(620, 211);
        }
        return itfNextBegDate;
    }

    /**
     * Gets the itfNextBegHour.
     * 
     * @return the itfNextBegHour.
     */
    public TouchInputTextField getItfNextBegHour() {
        if (itfNextBegHour == null) {
            itfNextBegHour = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_HOUR,
                    I18nClientManager.translate(ProductionMo.T35070, false));
            itfNextBegHour.setSize(54, 70);
            itfNextBegHour.setBeanBased(false);
            itfNextBegHour.setEnabled(false);
            itfNextBegHour.setAlwaysDisabled(true);
            Dimension d = new Dimension(141, 70);
            itfNextBegHour.setPreferredSize(d);
            itfNextBegHour.setMinimumSize(d);
            itfNextBegHour.setMaximumSize(d);

            // don't put the time from database, keep the current time
            // itfHour.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            // itfNextBegHour.setLocation(365, 211);
            itfNextBegHour.setLocation(789, 211);
        }
        return itfNextBegHour;
    }

    /**
     * Gets the itfPreviousEndDate.
     * 
     * @return the itfPreviousEndDate.
     */
    public TouchInputTextField getItfPreviousEndDate() {
        if (itfPreviousEndDate == null) {
            itfPreviousEndDate = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_DATE,
                    I18nClientManager.translate(ProductionMo.T32544, false));
            itfPreviousEndDate.setSize(102, 70);
            itfPreviousEndDate.setBeanBased(false);
            itfPreviousEndDate.setEnabled(false);
            itfPreviousEndDate.setAlwaysDisabled(true);
            Dimension d = new Dimension(242, 70);
            itfPreviousEndDate.setPreferredSize(d);
            itfPreviousEndDate.setMinimumSize(d);
            itfPreviousEndDate.setMaximumSize(d);

            // don't put the date from database, keep the current date
            // itfDate.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            // itfPreviousEndDate.setLocation(496, 11);
            itfPreviousEndDate.setLocation(32, 11);
        }
        return itfPreviousEndDate;
    }

    /**
     * Gets the itfPreviousEndHour.
     * 
     * @return the itfPreviousEndHour.
     */
    public TouchInputTextField getItfPreviousEndHour() {
        if (itfPreviousEndHour == null) {
            itfPreviousEndHour = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_HOUR,
                    I18nClientManager.translate(Generic.T30629, false));
            itfPreviousEndHour.setSize(54, 70);
            itfPreviousEndHour.setBeanBased(false);
            itfPreviousEndHour.setEnabled(false);
            itfPreviousEndHour.setAlwaysDisabled(true);
            Dimension d = new Dimension(141, 70);
            itfPreviousEndHour.setPreferredSize(d);
            itfPreviousEndHour.setMinimumSize(d);
            itfPreviousEndHour.setMaximumSize(d);

            // don't put the time from database, keep the current time
            // itfHour.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            // itfPreviousEndHour.setLocation(665, 11);
            itfPreviousEndHour.setLocation(201, 11);
        }
        return itfPreviousEndHour;
    }

    /**
     * Sets the itfCurrentBegDate.
     * 
     * @param itfCurrentBegDate itfCurrentBegDate.
     */
    public void setItfCurrentBegDate(final TouchInputTextField itfCurrentBegDate) {
        this.itfCurrentBegDate = itfCurrentBegDate;
    }

    /**
     * Sets the itfCurrentBegHour.
     * 
     * @param itfCurrentBegHour itfCurrentBegHour.
     */
    public void setItfCurrentBegHour(final TouchInputTextField itfCurrentBegHour) {
        this.itfCurrentBegHour = itfCurrentBegHour;
    }

    /**
     * Sets the itfCurrentEndDate.
     * 
     * @param itfCurrentEndDate itfCurrentEndDate.
     */
    public void setItfCurrentEndDate(final TouchInputTextField itfCurrentEndDate) {
        this.itfCurrentEndDate = itfCurrentEndDate;
    }

    /**
     * Sets the itfCurrentEndHour.
     * 
     * @param itfCurrentEndHour itfCurrentEndHour.
     */
    public void setItfCurrentEndHour(final TouchInputTextField itfCurrentEndHour) {
        this.itfCurrentEndHour = itfCurrentEndHour;
    }

    /**
     * Sets the itfDate.
     * 
     * @param itfDate the date.
     */
    public void setItfDate(final TouchInputTextField itfDate) {
        this.itfCurrentBegDate = itfDate;
    }

    /**
     * Sets the itfHour.
     * 
     * @param itfHour the hour.
     */
    public void setItfHour(final TouchInputTextField itfHour) {
        this.itfCurrentBegHour = itfHour;
    }

    /**
     * Sets the itfNextBegDate.
     * 
     * @param itfNextBegDate itfNextBegDate.
     */
    public void setItfNextBegDate(final TouchInputTextField itfNextBegDate) {
        this.itfNextBegDate = itfNextBegDate;
    }

    /**
     * Sets the itfNextBegHour.
     * 
     * @param itfNextBegHour itfNextBegHour.
     */
    public void setItfNextBegHour(final TouchInputTextField itfNextBegHour) {
        this.itfNextBegHour = itfNextBegHour;
    }

    /**
     * Sets the itfPreviousEndDate.
     * 
     * @param itfPreviousEndDate itfPreviousEndDate.
     */
    public void setItfPreviousEndDate(final TouchInputTextField itfPreviousEndDate) {
        this.itfPreviousEndDate = itfPreviousEndDate;
    }

    /**
     * Sets the itfPreviousEndHour.
     * 
     * @param itfPreviousEndHour itfPreviousEndHour.
     */
    public void setItfPreviousEndHour(final TouchInputTextField itfPreviousEndHour) {
        this.itfPreviousEndHour = itfPreviousEndHour;
    }

    /**
     * Sets the nextCurrentBegDateBtn.
     * 
     * @param nextCurrentBegDateBtn nextCurrentBegDateBtn.
     */
    public void setNextCurrentBegDateBtn(final TouchBtnStd nextCurrentBegDateBtn) {
        this.currentBegDateNextBtn = nextCurrentBegDateBtn;
    }

    /**
     * Sets the nextCurrentEndDateBtn.
     * 
     * @param nextCurrentEndDateBtn nextCurrentEndDateBtn.
     */
    public void setNextCurrentEndDateBtn(final TouchBtnStd nextCurrentEndDateBtn) {
        this.currentEndDateNextBtn = nextCurrentEndDateBtn;
    }

    /**
     * Sets the nextDateBtn.
     * 
     * @category setter
     * @param nextDateBtn nextDateBtn.
     */
    public final void setNextDateBtn(final TouchBtnStd nextDateBtn) {
        this.currentBegDateNextBtn = nextDateBtn;
    }

    /**
     * Sets the prevCurrentBegDateBtn.
     * 
     * @param prevCurrentBegDateBtn prevCurrentBegDateBtn.
     */
    public void setPrevCurrentBegDateBtn(final TouchBtnStd prevCurrentBegDateBtn) {
        this.currentBegDatePrevBtn = prevCurrentBegDateBtn;
    }

    /**
     * Sets the prevCurrentEndDateBtn.
     * 
     * @param prevCurrentEndDateBtn prevCurrentEndDateBtn.
     */
    public void setPrevCurrentEndDateBtn(final TouchBtnStd prevCurrentEndDateBtn) {
        this.currentEndDatePrevBtn = prevCurrentEndDateBtn;
    }

    /**
     * Sets the previousDateBtn.
     * 
     * @category setter
     * @param previousDateBtn previousDateBtn.
     */
    public final void setPreviousDateBtn(final TouchBtnStd previousDateBtn) {
        this.currentBegDatePrevBtn = previousDateBtn;
    }

    /**
     * Sets the dates and hours on screen.
     * 
     * @param mode : add or update
     * @param currentBeginDateHour : the current Begin Date Hour
     * @param currentEndDateHour : the current End Date Hour
     * @param prevEndDateHour : the previous End Date Hour
     * @param nextBeginDateHour : the next Begin Date Hour
     */
    public void setScreenValues(final String mode, final Date currentBeginDateHour, final Date currentEndDateHour,
            final Date prevEndDateHour, final Date nextBeginDateHour) {

        if (MODIF.equals(mode)) {
            if (currentBeginDateHour != null) {
                itfCurrentBegDate.setValue(currentBeginDateHour);
                itfCurrentBegHour.setValue(currentBeginDateHour);
            } else {
                itfCurrentBegDate.setTextLabel("");
                itfCurrentBegHour.setTextLabel("");
            }

            if (currentEndDateHour != null) {
                itfCurrentEndDate.setValue(currentEndDateHour);
                itfCurrentEndHour.setValue(currentEndDateHour);
            } else {
                itfCurrentEndDate.setEnabled(false);
                itfCurrentEndDate.setVisible(false);
                itfCurrentEndHour.setEnabled(false);
                itfCurrentEndHour.setVisible(false);
                currentEndDatePrevBtn.setEnabled(false);
                currentEndDatePrevBtn.setVisible(false);
                currentEndDateNextBtn.setEnabled(false);
                currentEndDateNextBtn.setVisible(false);

                itfCurrentEndDate.setTextLabel("");
            }

            if (prevEndDateHour != null) {
                itfPreviousEndDate.setValue(prevEndDateHour);
                itfPreviousEndHour.setValue(prevEndDateHour);
            } else {
                itfPreviousEndDate.setTextLabel("");
                itfPreviousEndHour.setTextLabel("");
            }

            if (nextBeginDateHour != null) {
                itfNextBegDate.setValue(nextBeginDateHour);
                itfNextBegHour.setValue(nextBeginDateHour);
            } else {
                itfNextBegDate.setTextLabel("");
                itfNextBegHour.setTextLabel("");
            }
        } else { // ADD
            Date today = DateHelper.getTodayTime().getTime();

            itfCurrentBegDate.setValue(today);
            itfCurrentBegHour.setValue(null);

            itfCurrentEndDate.setValue(today);
            itfCurrentEndHour.setValue(null);

            itfPreviousEndDate.setEnabled(false);
            itfPreviousEndHour.setVisible(false);
            itfNextBegDate.setEnabled(false);
            itfNextBegHour.setVisible(false);

            itfPreviousEndDate.setTextLabel("");
            itfNextBegDate.setTextLabel("");
        }

    }

    /**
     * Inform the listener than the value has changed.
     */
    public void valueChanged() {
        InputFieldActionEvent event1 = new InputFieldActionEvent(getItfCurrentBegDate(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getItfCurrentBegDate());
        for (InputFieldActionListener listener : getItfCurrentBegDate().getInputFieldActionListeners()) {
            listener.valueChanged(event1);
        }

        InputFieldActionEvent event2 = new InputFieldActionEvent(getItfCurrentEndDate(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getItfCurrentEndDate());
        for (InputFieldActionListener listener : getItfCurrentEndDate().getInputFieldActionListeners()) {
            listener.valueChanged(event2);
        }

        InputFieldActionEvent event3 = new InputFieldActionEvent(getItfNextBegDate(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getItfNextBegDate());
        for (InputFieldActionListener listener : getItfNextBegDate().getInputFieldActionListeners()) {
            listener.valueChanged(event3);
        }

        InputFieldActionEvent event4 = new InputFieldActionEvent(getItfPreviousEndDate(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getItfPreviousEndDate());
        for (InputFieldActionListener listener : getItfPreviousEndDate().getInputFieldActionListeners()) {
            listener.valueChanged(event4);
        }
    }

    /**
     * Add date.
     */
    private void addCurrentBegDate() {
        Date currentDate = (Date) getItfCurrentBegDate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        getItfCurrentBegDate().setValue(cal.getTime());
        valueChanged();
    }

    /**
     * Add date.
     */
    private void addCurrentEndDate() {
        Date currentDate = (Date) getItfCurrentEndDate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        getItfCurrentEndDate().setValue(cal.getTime());
        valueChanged();
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setLayout(null);
        setBorder(new RoundBackgroundPanelBorder(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR));

        add(getItfPreviousEndDate());
        add(getItfPreviousEndHour());
        add(getItfCurrentBegDate());
        add(getItfCurrentBegHour());
        add(getItfCurrentEndDate());
        add(getItfCurrentEndHour());
        add(getItfNextBegDate());
        add(getItfNextBegHour());
        add(getCurrentBegDateNextBtn());
        add(getCurrentBegDatePrevBtn());
        add(getCurrentEndDateNextBtn());
        add(getCurrentEndDatePrevBtn());
    }

    /**
     * Sub date.
     */
    private void subCurrentBegDate() {
        Date currentDate = (Date) getItfCurrentBegDate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        getItfCurrentBegDate().setValue(cal.getTime());
        valueChanged();
    }

    /**
     * Sub date.
     */
    private void subCurrentEndDate() {
        Date currentDate = (Date) getItfCurrentEndDate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        getItfCurrentEndDate().setValue(cal.getTime());
        valueChanged();
    }

}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DPrintDocumentTouchView.java,v $
 * Created on 28 Jun 2012 by cj
 */
package fr.vif.vif5_7.production.mo.ui.dialog.printdocument.touch;


import java.awt.Dialog;
import java.awt.Frame;

import fr.vif.jtech.ui.input.kbdialog.touch.TouchNumKeyboardDialog;


/**
 * Touch view for print document dialog.
 * 
 * @author cj
 */
public class DPrintDocumentTouchView extends TouchNumKeyboardDialog {
    /**
     * Constructor.
     * 
     * @param dialog dialog
     */
    public DPrintDocumentTouchView(final Dialog dialog) {
        super(dialog, Integer.class, "###", 0, false);
    }

    /**
     * Constructor.
     * 
     * @param frame frame
     */
    public DPrintDocumentTouchView(final Frame frame) {
        super(frame, Integer.class, "###", 0, false);
    }
}

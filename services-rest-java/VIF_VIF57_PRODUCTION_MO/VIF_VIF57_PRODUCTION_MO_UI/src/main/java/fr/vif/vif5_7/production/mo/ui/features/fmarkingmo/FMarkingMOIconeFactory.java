/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOIconeFactory.java,v $
 * Created on 13 janv. 2014 by lm
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import java.util.HashMap;
import java.util.Map;

import javax.swing.Icon;


/**
 * Icon factory of the FMarking.
 * 
 * @author lm
 */
public abstract class FMarkingMOIconeFactory {

    /**
     * 
     * Enumeration of the FMarking icons.
     * 
     * @author jd
     * @version $Revision: 1.2 $, $Date: 2014/04/23 16:57:31 $
     */
    public enum FMarkingMOIcone {
        PROCESSING_MO("processingMO.png"), //
        SIMULATING_MO("simulatedMO.png"), //
        SELECTED_MO("selectedMO.png");

        private String name;

        /**
         * Constructor.
         * 
         * @param name name of the icon
         */
        private FMarkingMOIcone(final String name) {
            this.name = name;
        }

        /**
         * Gets the name.
         * 
         * @category getter
         * @return the name.
         */
        public String getName() {
            return name;
        }
    }

    public static final String       PATH_16X16 = "/images/vif/vif57/production/mo/img16x16/";
    public static final String       PATH_32X32 = "/images/vif/vif57/production/mo/img32x32/";
    public static final String       PATH_48X48 = "/images/vif/vif57/production/mo/img48x48/";
    public static final String       PATH_64X64 = "/images/vif/vif57/production/mo/img64x64/";

    private static Map<String, Icon> map        = new HashMap<String, Icon>();

    /**
     * Constructor.
     */
    private FMarkingMOIconeFactory() {
        // Can't instantiate this class
    }
}

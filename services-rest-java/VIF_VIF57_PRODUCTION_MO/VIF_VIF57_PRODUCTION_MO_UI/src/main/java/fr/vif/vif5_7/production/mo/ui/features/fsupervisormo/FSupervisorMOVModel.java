/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOVModel.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOVBean;


/**
 * FSupervisorMO : Viewer Model.
 * 
 * @author jd
 */
public class FSupervisorMOVModel extends ViewerModel<FSupervisorMOVBean> {

    /**
     * Default constructor.
     * 
     */
    public FSupervisorMOVModel() {
        super();
        setBeanClass(FSupervisorMOVBean.class);
        getUpdateActions().setCopyEnabled(false);
        getUpdateActions().setDeleteEnabled(false);
        getUpdateActions().setDocEnabled(false);
        getUpdateActions().setNewEnabled(false);
        getUpdateActions().setPrintEnabled(false);
        getUpdateActions().setSaveEnabled(true);
        getUpdateActions().setUndoEnabled(false);
    }

}

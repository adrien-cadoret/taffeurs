/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: CSubstituteItemView.java,v $
 * Created on 19 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.composites.csubstituteitem.touch;


import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.input.touch.TouchCompositeInputTextField;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fsubstituteitem.FSubstituteItemBBean;
import fr.vif.vif5_7.production.mo.ui.composites.csubstituteitem.CSubstituteItemCtrl;
import fr.vif.vif5_7.production.mo.ui.composites.csubstituteitem.CSubstituteItemIView;
import fr.vif.vif5_7.production.mo.ui.features.fsubstituteitem.FSubstituteItemBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsubstituteitem.FSubstituteItemBModel;
import fr.vif.vif5_7.production.mo.ui.features.fsubstituteitem.touch.FSubstituteBView;


/**
 * Substitute item view.
 * 
 * @author vr
 */
public class CSubstituteItemView extends TouchCompositeInputTextField implements CSubstituteItemIView {

    private static final String BBEAN_CODE  = "code";
    private static final String BBEAN_LABEL = "label";
    private static final String CODE_FORMAT = "15";

    private Selection           selection;

    /**
     * Default constructor.
     */
    public CSubstituteItemView() {

        super(CSubstituteItemCtrl.class, CODE_FORMAT);
        setCodeUseFieldHelp(true);
        setCodeHelpBrowserTriad(new BrowserMVCTriad(FSubstituteItemBModel.class, FSubstituteBView.class,
                FSubstituteItemBCtrl.class));
        setCodeHelpInfos(FSubstituteItemBBean.class, BBEAN_CODE, BBEAN_LABEL);

        setTextLabel(I18nClientManager.translate(ProductionMo.T29519, false));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getSelection() {
        return this.selection;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelection(final Selection selection) {
        setCodeInitialHelpSelection(selection);
        this.selection = selection;

    }

}

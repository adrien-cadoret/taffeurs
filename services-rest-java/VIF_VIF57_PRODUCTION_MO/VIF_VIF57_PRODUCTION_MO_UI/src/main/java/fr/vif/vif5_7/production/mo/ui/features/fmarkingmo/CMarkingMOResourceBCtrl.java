/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_TRS
 * File : $RCSfile: CMarkingMOResourceBCtrl.java,v $
 * Created on 07 févr. 2014 by lm
 */

package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.activities.activities.business.beans.features.fproductionresource.FProductionResourceBBean;
import fr.vif.vif5_7.activities.activities.business.beans.features.fproductionresource.FProductionResourceVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOInitFeatureReturnBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOLine;
import fr.vif.vif5_7.production.mo.business.services.features.fmarkingmo.FMarkingMOCBS;


/**
 * Production Resources browser controller.
 * 
 * Show only the workstation's resources.
 * 
 * @author lm
 */
public class CMarkingMOResourceBCtrl extends AbstractBrowserController<FProductionResourceBBean> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(CMarkingMOResourceBCtrl.class);

    private FMarkingMOCBS       fMarkingMOCBS;

    /**
     * Gets the fMarkingMOCBS.
     * 
     * @return the fMarkingMOCBS.
     */
    public FMarkingMOCBS getfMarkingMOCBS() {
        return fMarkingMOCBS;
    }

    /**
     * Sets the fMarkingMOCBS.
     * 
     * @param fMarkingMOCBS fMarkingMOCBS.
     */
    public void setfMarkingMOCBS(final FMarkingMOCBS fMarkingMOCBS) {
        this.fMarkingMOCBS = fMarkingMOCBS;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    protected FProductionResourceBBean convert(final Object bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }

        FProductionResourceBBean fproductionresourceBBean = new FProductionResourceBBean();

        fproductionresourceBBean.setCode(((FProductionResourceVBean) bean).getResourceCode());
        fproductionresourceBBean.setLabel(((FProductionResourceVBean) bean).getResourceLabel());
        fproductionresourceBBean.setShortLabel(((FProductionResourceVBean) bean).getResourceShortLabel());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")=" + fproductionresourceBBean);
        }
        return fproductionresourceBBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FProductionResourceBBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        FMarkingMOInitFeatureReturnBean cfg = (FMarkingMOInitFeatureReturnBean) FMarkingMOSharedContext.getInstance()
                .getCtrl().getSharedContext().get(Domain.DOMAIN_THIS, FMarkingMOUIConstants.INIT_FEATURE_BEAN);

        List<FProductionResourceBBean> ret = new ArrayList<FProductionResourceBBean>();
        if (cfg != null) {
            for (FMarkingMOLine vLine : cfg.getLines()) {
                ret.add(new FProductionResourceBBean(vLine.getLineCL().getCode(), vLine.getLineCL().getLabel(), ""));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + ret);
        }
        return ret;
    }

}

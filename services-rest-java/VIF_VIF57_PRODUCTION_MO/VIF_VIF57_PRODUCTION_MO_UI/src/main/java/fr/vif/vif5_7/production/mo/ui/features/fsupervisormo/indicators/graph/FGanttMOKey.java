/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOKey.java,v $
 * Created on 22 mars. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph;


/**
 * Indicator Mo key.
 * 
 * 
 * @author jd
 */
class FGanttMOKey {
    private String key  = null;

    private int    line = 0;

    /**
     * Constructor.
     */
    public FGanttMOKey() {

    }

    /**
     * Constructor.
     * 
     * @param key key
     * @param line line
     */
    public FGanttMOKey(final String key, final int line) {
        super();
        this.key = key;
        this.line = line;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        FGanttMOKey other = (FGanttMOKey) obj;
        if (key == null) {
            if (other.key != null) {
                return false;
            }
        } else if (!key.equals(other.key)) {
            return false;
        }
        if (line != other.line) {
            return false;
        }
        return true;
    }

    /**
     * Gets the key.
     * 
     * @return the key.
     */
    public String getKey() {
        return key;
    }

    /**
     * Gets the line.
     * 
     * @return the line.
     */
    public int getLine() {
        return line;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((key == null) ? 0 : key.hashCode());
        result = prime * result + line;
        return result;
    }

    /**
     * Sets the key.
     * 
     * @param key key.
     */
    public void setKey(final String key) {
        this.key = key;
    }

    /**
     * Sets the line.
     * 
     * @param line line.
     */
    public void setLine(final int line) {
        this.line = line;
    }

}

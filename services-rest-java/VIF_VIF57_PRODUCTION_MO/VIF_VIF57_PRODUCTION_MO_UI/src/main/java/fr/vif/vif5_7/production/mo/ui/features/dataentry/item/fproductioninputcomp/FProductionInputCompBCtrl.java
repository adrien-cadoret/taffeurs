/*
 * Copyright (c) 2015 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputCompBCtrl.java,v $
 * Created on 7 Dec 2015 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcomp;


import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcomp.FProductionInputCompCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBCtrl;


/**
 * Browser controller for complementary input feature.
 *
 * @author cj
 */
public class FProductionInputCompBCtrl extends FProductionBCtrl {
    /** LOGGER. */
    private static final Logger     LOGGER = Logger.getLogger(FProductionInputCompBCtrl.class);

    private FProductionInputCompCBS fproductionInputCompCBS;

    /**
     * Gets the fproductionInputCompCBS.
     *
     * @return the fproductionInputCompCBS.
     */
    public FProductionInputCompCBS getFproductionInputCompCBS() {
        return fproductionInputCompCBS;
    }

    /**
     * Sets the fproductionInputCompCBS.
     *
     * @param fproductionInputCompCBS fproductionInputCompCBS.
     */
    public void setFproductionInputCompCBS(final FProductionInputCompCBS fproductionInputCompCBS) {
        this.fproductionInputCompCBS = fproductionInputCompCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FProductionBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FProductionBBean> lst = null;
        try {
            lst = getFproductionInputCompCBS().queryElements(getIdCtx(), (FProductionOutputCompSBean) selection,
                    startIndex, rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }
        Collections.sort(lst, getModel().getComparator());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return lst;
    }
}

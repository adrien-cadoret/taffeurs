/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: BarCodeCtrl.java,v $
 * Created on 27 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.composites.barcode;


import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.input.AbstractInputBarCodeController;


/**
 * Barcode controller.
 * 
 * @author vr
 */
public class BarCodeCtrl extends AbstractInputBarCodeController {

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBarCodeSyntax(final String value) throws UIException {
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOCreationVCtrl.java,v $
 * Created on 19 avr. 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowSelectionEvent;
import fr.vif.jtech.ui.events.table.TableRowSelectionListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.activities.activities.business.beans.features.foperation.FOperationSBean;
import fr.vif.vif5_7.activities.activities.business.beans.features.fstackingplan.FStackingPlanSBean;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FabricationType;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaCompleteBean;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaReturnInitBean;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labels.LabelEvent;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmocreation.BoningFabricationInfos;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmocreation.FBoningMOCreationVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmocreation.FBoningMOCreationVBeanEnum;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fboningmocreation.FBoningMOCreationCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.CodeFunction;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.boning.Mnemos.BoningConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.flabels.LabelPrintHelper;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation.touch.FBoningMOCreationVView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationVIView;


/**
 * bonning MO creation viewer controller.
 * 
 * @author xg
 */
public class FBoningMOCreationVCtrl extends AbstractStandardViewerController<FBoningMOCreationVBean> implements
        TableRowSelectionListener {

    private static final Logger    LOGGER = Logger.getLogger(FBoningMOCreationVCtrl.class);

    private FBoningMOCreationFCtrl boningFeatureController;

    private FBoningMOCreationCBS   fBoningMOCreationCBS;

    /**
     * Completes MO head criteria.
     * 
     * @param criteriaBean the current criteria bean to complete
     */
    public void completeFabricationCriteria(final CriteriaReturnInitBean criteriaBean) {
        if (criteriaBean.getAutoOpening() && criteriaBean.getListCriteria().size() > 0) {
            criteriaBean.setControlMessage("");
            CriteriaDBean criteriaDBean = new CriteriaDBean(criteriaBean, getModel().getBean().getItem());
            getModel().getBean().setReturnInitBean(criteriaDBean.getCriteriaReturnInitBean());
            WorkShopCriteriaTools.showCriteria(this.boningFeatureController.getView(), this.boningFeatureController,
                    this.boningFeatureController, getModel().getIdentification(), criteriaDBean,
                    I18nClientManager.translate(ProductionMo.T34359, false));
        }
    }

    /**
     * Initialize the viewer bean from the barcode.
     */
    public void createFromBarCode() {
        // add the MO head criteria
        if (getBean().getReturnInitBean().getControlMessage() != null
                && !getBean().getReturnInitBean().getControlMessage().isEmpty()) {
            completeFabricationCriteria(getBean().getReturnInitBean());
        }

        // if there is no operating process, initialize the codelabel
        if (getBean().getOperatingProcessCL() == null) {
            getBean().setOperatingProcessCL(new CodeLabel());
        }

        // Add fabrication id and qty to the stacking plan composite
        repositionOnItem(getBean().getItem());
        this.setCreationMode(true);
        // update stacking plan
        FStackingPlanSBean stackingPlanSBean = new FStackingPlanSBean();
        stackingPlanSBean.setEstablishmentKey(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx()
                .getEstablishment()));
        stackingPlanSBean.setActivityId(getBean().getFabrication().getFabricationCLs().getCode());
        stackingPlanSBean.setFabricationType(getBean().getFabrication().getFabricationType());
        stackingPlanSBean.setQty(getBean().getQuantityUnit().getQty());
        stackingPlanSBean.setRecreatedSpecId(getBean().getRecreatedSpecification() != null ? getBean()
                .getRecreatedSpecification().getCode() : null);
        ((FBoningMOCreationVView) getView()).getcStackingPlanView().setCodeInitialHelpSelection(stackingPlanSBean);

        // update operating process
        FOperationSBean fOperationSBean = new FOperationSBean();
        fOperationSBean
                .setEstablishmentKey(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()));
        fOperationSBean.setActivityId(getBean().getActivityForProcess());
        fOperationSBean.setEffectDate(getBean().getActivityEffectDate());
        ((FBoningMOCreationVView) getView()).getCoperatingProcessView().setCodeInitialHelpSelection(fOperationSBean);

        getView().render();

        try {
            preinitializeStackingPlan(stackingPlanSBean);
        } catch (UIException e) {
            LOGGER.error("", e);
            getView().show(e);
        }
        getView().enableComponents(true);

        fireButtonStatechanged();
    }

    /**
     * Fire performSave.
     * 
     * @return <code>true</code> if the bean was successfully saved.
     */
    public boolean firePerformSave() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - firePerformSave()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - firePerformSave()");
        }
        return this.performSave();

    }

    /**
     * Gets the fBoningMOCreationCBS.
     * 
     * @return the fBoningMOCreationCBS.
     */
    public FBoningMOCreationCBS getfBoningMOCreationCBS() {
        return fBoningMOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        this.addSharedContextListener(((FMOCreationVIView) getView()).getBrowserController());
        fireSharedContextSend(getSharedContext());
        super.initialize();
        // Subscription to browser row validation.
        ((FMOCreationVIView) getView()).getBrowserController().addTableRowSelectionListener(this);

        ((FBoningMOCreationVView) getView()).getcStackingPlanView().disableItfStackingPlan();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeViewWithBean(final FBoningMOCreationVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeViewWithBean(bean=" + bean + ")");
        }

        super.initializeViewWithBean(new FBoningMOCreationVBean());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeViewWithBean(bean=" + bean + ")");
        }
    }

    /**
     * Search fabrication from stage criteria.
     * 
     * @param criteriaBean the criteria bean
     */
    public void searchFabricationFromCriteria(final CriteriaReturnInitBean criteriaBean) {
        repositionOnItem(getBean().getItem());
        this.setCreationMode(true);
        initCriteria(criteriaBean);
        searchFabricationInfos();
        fireButtonStatechanged();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanging(event=" + event + ")");
        }

        if (!(event.getSource() instanceof FMOCreationBCtrl)) {
            // To avoid question when changing of item.
            performUndo();
        }
        super.selectedRowChanging(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanging(event=" + event + ")");
        }
    }

    /**
     * boningFeatureController Setter.
     * 
     * @param boningFeatureController the boning Feature Controller
     */
    public void setBoningFeatureController(final FBoningMOCreationFCtrl boningFeatureController) {
        this.boningFeatureController = boningFeatureController;
    }

    /**
     * Sets the fBoningMOCreationCBS.
     * 
     * @param fBoningMOCreationCBS fBoningMOCreationCBS.
     */
    public void setfBoningMOCreationCBS(final FBoningMOCreationCBS fBoningMOCreationCBS) {
        this.fBoningMOCreationCBS = fBoningMOCreationCBS;
    }

    /**
     * Sets the boning feature Controller.
     * 
     * @category setter
     * @param boningFController boningFeatureController.
     */
    public void setFeatureController(final FBoningMOCreationFCtrl boningFController) {
        this.boningFeatureController = boningFController;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tableRowSelected(final TableRowSelectionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - tableRowSelected(event=" + event + ")");
        }

        if (!(event.getSource() instanceof FMOCreationBCtrl)) {
            String item = ((CodeLabel) event.getSelectedBean()).getCode();
            getModel().getBean().setItem(item);
            searchFabricationFromCriteria(null);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - tableRowSelected(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - validateBean(creationMode=" + creationMode + ", beanProperty=" + beanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }

        if (FBoningMOCreationVBeanEnum.QTY.getValue().equals(beanProperty)) {
            FStackingPlanSBean stackingPlanSBean = new FStackingPlanSBean();
            stackingPlanSBean.setEstablishmentKey(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx()
                    .getEstablishment()));

            stackingPlanSBean.setActivityId(getBean().getFabrication().getFabricationCLs().getCode());
            stackingPlanSBean.setFabricationType(getBean().getFabrication().getFabricationType());
            stackingPlanSBean.setQty(getBean().getQuantityUnit().getQty());
            stackingPlanSBean.setRecreatedSpecId(getBean().getRecreatedSpecification() != null ? getBean()
                    .getRecreatedSpecification().getCode() : null);

            ((FBoningMOCreationVView) getView()).getcStackingPlanView().setCodeInitialHelpSelection(stackingPlanSBean);

            preinitializeStackingPlan(stackingPlanSBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - validateBean(creationMode=" + creationMode + ", beanProperty=" + beanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningMOCreationVBean convert(final Object bean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")");
        }
        return new FBoningMOCreationVBean();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningMOCreationVBean insertBean(final FBoningMOCreationVBean bean,
            final FBoningMOCreationVBean initialBean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertBean(bean=" + bean + ", initialBean=" + initialBean + ")");
        }

        checkFMOCreationVBean(bean);
        try {
            MOBean moBean = getfBoningMOCreationCBS().createStartedMO(getIdCtx(), bean);
            LabelEvent labelEvent = new LabelEvent();
            labelEvent.setMoKey(moBean.getMoKey());
            labelEvent.setEventType(LabelingEventType.MO_CREATION);
            labelEvent.setWorkstationId(getIdCtx().getWorkstation());
            labelEvent.setFeatureId(CodeFunction.PROD_IN.getValue());
            LabelPrintHelper labelPrintHelper = StandardControllerFactory.getInstance().getController(
                    LabelPrintHelper.class);
            labelPrintHelper.launchPrint(getView(), getIdentification(), getIdCtx(), labelEvent);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.STARTED_MO_BEAN, moBean);
            fireSharedContextSend(getSharedContext());

        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertBean(bean=" + bean + ", initialBean=" + initialBean + ")=" + bean);
        }
        return bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean ret = true;
        FBoningMOCreationVBean vbean = getModel().getBean();
        if (vbean == null || vbean.getFlowType() == null || vbean.getFabrication() == null
                || vbean.getFabrication().getFabricationType() == null
                || vbean.getFabrication().getFabricationCLs() == null) {
            ret = false;
        }
        if (ret && reference.equals(FMOCreationFCtrl.EVENT_OPENCRITERIA)) {
            ret = getModel().getBean().getReturnInitBean().getListCriteria().size() > 0;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionEnabled(final ToolBarButtonType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isPanelActionEnabled(type=" + type + ")");
        }

        boolean ret = false;
        if (type == ToolBarButtonType.SAVE) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isPanelActionEnabled(type=" + type + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isPanelActionVisible(type=" + type + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isPanelActionVisible(type=" + type + ")=" + false);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningMOCreationVBean updateBean(final FBoningMOCreationVBean bean) throws UIException,
            UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(bean=" + bean + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(bean=" + bean + ") : " + getBean());
        }
        return getBean();
    }

    /**
     * Check the FMOCretionVBean.
     * 
     * @param vbean the viewer bean
     * @throws UIException if error occurs.
     */
    private void checkFMOCreationVBean(final FBoningMOCreationVBean vbean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkFMOCreationVBean(vbean=" + vbean + ")");
        }

        if (vbean.getItem() == null || vbean.getItem().equals("")) {
            throw new UIException(Generic.T12640, new VarParamTranslation(null));
        }
        if (vbean.getQuantityUnit().getQty() == 0) {
            throw new UIException(Generic.T20724, ProductionMo.T29553);
        }

        if (getBean().getFabrication().getFabricationType().equals(FabricationType.ACTIVITIES_SEQUENCE)) {

            boolean isRecreated = determineIfRecreated(getBean().getFabrication().getFabricationCLs().getCode());
            if (isRecreated) {
                if (vbean.getStackingPlanCL() == null || vbean.getStackingPlanCL().getCode() == null
                        || vbean.getStackingPlanCL().getCode().isEmpty()) {
                    throw new UIException(Generic.T12640, ProductionMo.T20470);
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkFMOCreationVBean(vbean=" + vbean + ")");
        }
    }

    /**
     * Determine if the last activity of this item is recreated.
     * 
     * @param fabrication the soa code
     * @return true if recreated, false otherwise
     * @throws UIException for errors
     */
    private boolean determineIfRecreated(final String fabrication) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - determineIfRecreated(item=" + fabrication + ")");
        }

        boolean isRecreated = false;
        try {
            isRecreated = getfBoningMOCreationCBS().determineIfRecreated(getIdCtx(), fabrication);
        } catch (BusinessException e) {
            LOGGER.error(e);
            throw new UIException(ProductionMo.T34476);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - determineIfRecreated(item=" + fabrication + ")=" + isRecreated);
        }
        return isRecreated;
    }

    /**
     * Initializes the criteria.
     * 
     * @param criteriaBean the criteria bean if we already know some value
     */
    private void initCriteria(final CriteriaReturnInitBean criteriaBean) {
        try {
            CriteriaReturnInitBean criteriaReturnInitBean = criteriaBean;
            if (criteriaBean == null) {
                criteriaReturnInitBean = getfBoningMOCreationCBS().getCriteriaReturnInitBean(getIdCtx(),
                        getIdentification().getCompany(), getIdentification().getEstablishment(),
                        getIdentification().getLogin(), getModel().getBean().getItem());
            }

            if (criteriaReturnInitBean.getAutoOpening() && criteriaReturnInitBean.getListCriteria().size() > 0) {
                CriteriaDBean criteriaDBean = new CriteriaDBean(criteriaReturnInitBean, getModel().getBean().getItem());
                criteriaReturnInitBean = criteriaDBean.getCriteriaReturnInitBean();
                WorkShopCriteriaTools.showCriteria(this.boningFeatureController.getView(),
                        this.boningFeatureController, this.boningFeatureController, getModel().getIdentification(),
                        criteriaDBean, I18nClientManager.translate(ProductionMo.T34358, false));
            }

            getBean().setReturnInitBean(criteriaReturnInitBean);
        } catch (BusinessException e) {
            LOGGER.debug(e.getMessage());
            getModel().setBean(new FBoningMOCreationVBean());
            getView().render();
            getView().showError(e.getMessage());
        }
    }

    /**
     * Preinitialize Stacking Plan Input text field.
     * 
     * @param selection selection
     * @throws UIException if error occurs
     */
    private void preinitializeStackingPlan(final FStackingPlanSBean selection) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - preinitializeStackingPlan(selection=" + selection + ")");
        }

        List<CodeLabel> lst = new ArrayList<CodeLabel>();
        try {

            lst = getfBoningMOCreationCBS().getSelectedStackingPlans(getIdCtx(), selection, 0, 10000);

            if (lst == null || (lst != null && lst.size() == 0)) {
                getBean().setStackingPlanCL(new CodeLabel());
                ((FBoningMOCreationVView) getView()).getcStackingPlanView().disableItfStackingPlan();
            } else {
                ((FBoningMOCreationVView) getView()).getcStackingPlanView().enableItfStackingPlan();
                ((FBoningMOCreationVView) getView()).getcStackingPlanView().setEnabledForCreate(true);
            }

            if (getBean().getStackingPlanCL() == null || getBean().getStackingPlanCL().getCode() == null
                    || getBean().getStackingPlanCL().getCode().isEmpty()
                    || !lst.contains(getBean().getStackingPlanCL())) {
                if (lst != null && lst.size() > 0) {
                    getBean().setStackingPlanCL(lst.get(0));
                } else {
                    getBean().setStackingPlanCL(new CodeLabel());
                }
            }

        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - preinitializeStackingPlan(selection=" + selection + ")");
        }
    }

    /**
     * Report value of criteria $CCR set on search fabrication's stage on the launch's stage.
     * 
     * @param fabricationInfos the fabrication infos
     */
    private void reportRecSpecCriteria(final BoningFabricationInfos fabricationInfos) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - reportRecSpecCriteria(fabricationInfos=" + fabricationInfos + ")");
        }

        for (CriteriaCompleteBean criteriaCompleteBean : getModel().getBean().getReturnInitBean().getListCriteria()) {
            if (BoningConstant.REC_SPEC_CRITERIA.equals(criteriaCompleteBean.getCriteriaId())) {
                for (int index = 0; index < fabricationInfos.getReturnInitBean().getListCriteria().size(); index++) {
                    CriteriaCompleteBean criteriaCompleteBean2 = fabricationInfos.getReturnInitBean().getListCriteria()
                            .get(index);
                    if (BoningConstant.REC_SPEC_CRITERIA.equals(criteriaCompleteBean2.getCriteriaId())) {
                        if (StringUtils.isEmpty(criteriaCompleteBean2.getValueCarMin())) {
                            fabricationInfos.getReturnInitBean().getListCriteria().remove(criteriaCompleteBean2);
                            fabricationInfos.getReturnInitBean().getListCriteria().add(index, criteriaCompleteBean);
                            reportSpecCriteria(criteriaCompleteBean.getValueCarMin(), fabricationInfos);
                        }
                    }
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - reportRecSpecCriteria(fabricationInfos=" + fabricationInfos + ")");
        }
    }

    /**
     * Report value of criteria $CC corresponding to the criteria $CCR set on the launch's stage.
     * 
     * @param recSpecCriteriaValue the value of criteria $CCR
     * @param fabricationInfos the fabrication infos
     */
    private void reportSpecCriteria(final String recSpecCriteriaValue, final BoningFabricationInfos fabricationInfos) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - reportSpecCriteria(recSpecCriteriaValue=" + recSpecCriteriaValue + ", fabricationInfos="
                    + fabricationInfos + ")");
        }

        try {
            for (int index = 0; index < fabricationInfos.getReturnInitBean().getListCriteria().size(); index++) {

                CriteriaCompleteBean criteriaCompleteBean2 = fabricationInfos.getReturnInitBean().getListCriteria()
                        .get(index);
                if (BoningConstant.SPEC_CRITERIA.equals(criteriaCompleteBean2.getCriteriaId())) {
                    String valueSpecCriteria;
                    valueSpecCriteria = getfBoningMOCreationCBS().getSpecificCriteria(getIdCtx(), recSpecCriteriaValue);
                    if (!StringUtils.isEmpty(valueSpecCriteria)
                            && StringUtils.isEmpty(criteriaCompleteBean2.getValueCarMin())) {
                        CriteriaCompleteBean criteriaCompleteBeanMaj = criteriaCompleteBean2;
                        fabricationInfos.getReturnInitBean().getListCriteria().remove(criteriaCompleteBean2);
                        criteriaCompleteBeanMaj.setValueCarMin(valueSpecCriteria);
                        fabricationInfos.getReturnInitBean().getListCriteria().add(index, criteriaCompleteBeanMaj);
                    }
                }
            }
        } catch (BusinessException e) {
            LOGGER.debug(e.getMessage());
            getView().render();
            getView().showError(e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - reportSpecCriteria(recSpecCriteriaValue=" + recSpecCriteriaValue + ", fabricationInfos="
                    + fabricationInfos + ")");
        }
    }

    /**
     * Reposition on an item.
     * 
     * @param item the item id
     */
    private void repositionOnItem(final String item) {
        try {
            AbstractBrowserController<CodeLabel> browser = ((FBoningMOCreationVView) getView()).getBrowserController();
            for (int i = 0; i < browser.getModel().getRowCount(); i++) {
                CodeLabel bbean = browser.getModel().getBeanAt(i);

                if (bbean != null && bbean.getCode().equals(item)) {
                    browser.changeCurrentRow(i);
                    break;
                }
            }
        } catch (UIVetoException e) {
            getView().show(e);
        }
    }

    /**
     * Update the quantity unit.
     */
    private void searchFabricationInfos() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - searchFabricationInfos()");
        }

        try {
            String workstationId = (String) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.LOGICAL_WORKSTATION);
            BoningFabricationInfos fabricationInfos = getfBoningMOCreationCBS().getBoningFabricationInfos(getIdCtx(),
                    getIdCtx().getCompany(), getIdCtx().getEstablishment(), workstationId,
                    getModel().getBean().getItem(), getBean().getReturnInitBean().getListCriteria());

            // update the model bean
            if (!getModel().getBean().getFabrication().equals(fabricationInfos.getFabrication())
                    || getModel().getBean().getQuantityUnit() == null
                    || getModel().getBean().getQuantityUnit().getQty() == 0) {
                getModel().getBean().setQuantityUnit(fabricationInfos.getQuantityUnit());
            }
            if (!getModel().getBean().getFabrication().equals(fabricationInfos.getFabrication())
                    || getModel().getBean().getStackingPlanCL() == null
                    || getModel().getBean().getStackingPlanCL().getCode() == null) {
                getModel().getBean().setStackingPlanCL(fabricationInfos.getStackingPlan());
                ((FBoningMOCreationVView) getView()).getcStackingPlanView().enableItfStackingPlan();
                ((FBoningMOCreationVView) getView()).getcStackingPlanView().setEnabledForCreate(true);
            } else {
                ((FBoningMOCreationVView) getView()).getcStackingPlanView().disableItfStackingPlan();
            }
            getModel().getBean().setFlowType(fabricationInfos.getFlowType());
            getModel().getBean().setFabrication(fabricationInfos.getFabrication());

            reportRecSpecCriteria(fabricationInfos);
            completeFabricationCriteria(fabricationInfos.getReturnInitBean());
            getModel().getBean().setActivityEffectDate(fabricationInfos.getActivityEffectDate());
            getModel().getBean().setActivityForProcess(fabricationInfos.getActivityForProcess());

            // Add fabrication id and qty to the stacking plan composite
            FStackingPlanSBean stackingPlanSBean = new FStackingPlanSBean();
            stackingPlanSBean.setEstablishmentKey(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx()
                    .getEstablishment()));
            stackingPlanSBean.setActivityId(fabricationInfos.getFabrication().getFabricationCLs().getCode());
            stackingPlanSBean.setFabricationType(fabricationInfos.getFabrication().getFabricationType());
            stackingPlanSBean.setQty(fabricationInfos.getQuantityUnit().getQty());
            ((FBoningMOCreationVView) getView()).getcStackingPlanView().setCodeInitialHelpSelection(stackingPlanSBean);

            FOperationSBean fOperationSBean = new FOperationSBean();
            fOperationSBean.setEstablishmentKey(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx()
                    .getEstablishment()));
            fOperationSBean.setActivityId(fabricationInfos.getActivityForProcess());
            fOperationSBean.setEffectDate(fabricationInfos.getActivityEffectDate());
            ((FBoningMOCreationVView) getView()).getCoperatingProcessView()
                    .setCodeInitialHelpSelection(fOperationSBean);
            if (fabricationInfos.getDefaultProcess() != null) {
                getModel().getBean().setOperatingProcessCL(fabricationInfos.getDefaultProcess());
            }

            getView().enableComponents(true);

            // getView().render();
        } catch (BusinessException e) {
            LOGGER.debug(e.getMessage());
            getModel().setBean(new FBoningMOCreationVBean());
            getView().render();
            getView().showError(e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - searchFabricationInfos()");
        }
    }
}

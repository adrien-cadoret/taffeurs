/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: CollapseNodeAction.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.action;


import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.treetable.InputTreeTableController;
import fr.vif.jtech.ui.treetable.InputTreeTableIView;
import fr.vif.jtech.ui.treetable.InputTreeTableModel;
import fr.vif.jtech.ui.treetable.actions.CollapsePathAction;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeElt;


/**
 * CollapseNodeAction.
 * 
 * @author cj
 */
public class CollapseNodeAction extends CollapsePathAction {
    private InputTreeTableController                          treeTableCtrl;
    private InputTreeTableIView<TimeBreakDownHchyTreeNodeElt> treeTableView;
    private InputTreeTableModel                               treeTableModel;

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        treeTableCtrl = getTreeTableController();
        treeTableView = (InputTreeTableIView<TimeBreakDownHchyTreeNodeElt>) treeTableCtrl.getView();
        treeTableModel = (InputTreeTableModel) treeTableView.getModel();

        Object[] currentPath = treeTableView.getCurrentSelectedNodePath();
        if (currentPath != null && currentPath.length > 0) {
            treeTableModel.collapseAllNode(currentPath[currentPath.length - 1]);
        }
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompBCtrl.java,v $
 * Created on 18 sept. 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp;


import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBCtrl;


/**
 * Production Output Scrap Browser Controller.
 * 
 * @author kl
 */
public class FProductionOutputCompBCtrl extends FProductionBCtrl {
    /** LOGGER. */
    private static final Logger      LOGGER = Logger.getLogger(FProductionOutputCompBCtrl.class);

    private FProductionOutputCompCBS fproductionOutputCompCBS;

    /**
     * Default constructor.
     * 
     */
    public FProductionOutputCompBCtrl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        FProductionOutputCompSBean sBean = new FProductionOutputCompSBean();
        return sBean;
    }

    /**
     * Gets the fproductionOutputCompCBS.
     *
     * @return the fproductionOutputCompCBS.
     */
    public FProductionOutputCompCBS getFproductionOutputCompCBS() {
        return fproductionOutputCompCBS;
    }

    /**
     * Sets the fproductionOutputCompCBS.
     *
     * @param fproductionOutputCompCBS fproductionOutputCompCBS.
     */
    public void setFproductionOutputCompCBS(final FProductionOutputCompCBS fproductionOutputCompCBS) {
        this.fproductionOutputCompCBS = fproductionOutputCompCBS;
    }


    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FProductionBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FProductionBBean> lst = null;
        FProductionOutputCompSBean sBean = null;
        if (selection instanceof FProductionSBean && !(selection instanceof FProductionOutputCompSBean)) {
            FProductionSBean outputSBean = (FProductionSBean) selection;

            // we are in the first initialize. So we can build the selection
            sBean = new FProductionOutputCompSBean();
            sBean.setActivityItemType(outputSBean.getActivityItemType());
            sBean.setAll(outputSBean.getAll());
            sBean.setMoKey(outputSBean.getMoKey());
            sBean.setState(MONatureComplementaryWorkshop.MANUFACTURING.getValue());
            sBean.setWorkstationId(outputSBean.getWorkstationId());
        } else {
            sBean = (FProductionOutputCompSBean) selection;
        }
        if (sBean == null) {
            throw new UIException(Generic.T20829, ProductionMo.T29519);
        }

        try {

            lst = getFproductionOutputCompCBS().queryElements(getIdCtx(), sBean, startIndex, rowNumber);

            // QA8329 - use the standard touch model and not a special one by nature            
            TouchModelFunction touchModelFunction = null;
            if (getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.USE_TOUCH_MODEL) != null) {
                boolean useTouchModel = (Boolean) getSharedContext().get(Domain.DOMAIN_PARENT,
                        TouchModel.USE_TOUCH_MODEL);
                if (useTouchModel) {
                    TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT,
                            TouchModel.TOUCH_MODEL);
                    if (touchModel != null) {
                        touchModelFunction = touchModel
                                .getTouchModelFunction(TouchModelMapKey.CONTAINER_OUTPUT_PRODUCTION);
                    }

                    // // We force update of Browser view.
                    ((FProductionOutputCompBModel) getModel()).manageColumnsByNature(getModel(), touchModelFunction,
                            MONatureComplementaryWorkshop.getMONatureComplementaryWorkshop(sBean.getState()));
                }
            }

        } catch (BusinessException e) {
            throw new UIException(e);
        }
        Collections.sort(lst, getModel().getComparator());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + lst == null ? "nullé" : lst.size() + " elements");
        }

        return lst;
    }
}

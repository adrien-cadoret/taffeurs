/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersSView.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders.swing;


import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.dialogs.selection.swing.DynamicSwingSelection;
import fr.vif.jtech.ui.input.InputTextFieldModel;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.input.swing.SwingInputTextFieldMI;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.activities.activities.ui.composites.cteamlist.swing.CTeamListView;
import fr.vif.vif5_7.admin.profile.business.beans.features.fuser.FUserBBean;
import fr.vif.vif5_7.admin.profile.ui.composites.cuserlist.CUserListCtrl;
import fr.vif.vif5_7.admin.profile.ui.composites.cuserlist.swing.CUserListView;
import fr.vif.vif5_7.common.util.i18n.admin.config.AdminConfig;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.hierarchy.ui.composites.hierarchy.chchylist.swing.CHchyListView;
import fr.vif.vif5_7.gen.location.ui.composites.carealist.swing.CAreaListView;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersSIView;


/**
 * ProcessOrders : Selection View.
 * 
 * @author jd
 */
public class FProcessOrdersSView extends DynamicSwingSelection implements FProcessOrdersSIView {

    private CAreaListView                 cArea;
    private SwingInputTextFieldMI<String> cCategory;
    private SwingInputTextFieldMI<String> cLine;
    private CHchyListView                 cFLine;
    private CTeamListView                 cTeam;
    private CUserListView                 cUser;

    private SwingInputTextField           itfDate;
    private JLabel                        jlArea;

    private JLabel                        jlCategory;
    private JLabel                        jlDate;
    private JLabel                        jlLine;
    private JLabel                        jlFLine;
    private JLabel                        jlTeam;
    private JLabel                        jlUser;
    private JPanel                        jPanel;
    // private CHchyLstValueView cItemHierarchy;
    private CHchyListView                 cItemHchy;

    private JLabel                        jlItemHierarchy;

    // private CHchyLstValueView cLineHierarchy;
    private JLabel                        jlLineHierarchy;

    /**
     * Constructs a new FSupervisorMOSView.
     * 
     * @param view owner's view.
     * @wbp.parser.constructor
     */
    public FProcessOrdersSView(final Dialog view) {
        super(view);
        initialize();
    }

    /**
     * Constructs a new FSupervisorMOSView.
     * 
     * @param view owner's view.
     */
    public FProcessOrdersSView(final Frame view) {
        super(view);
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableComponents() {
        super.enableComponents();
        // if (cTeam.getInitialHelpSelection() == null) {
        // FTeamSBean selection = new FTeamSBean(true, "", ((FProcessOrdersSBean) getModel().getDialogBean())
        // .getEstablishmentKey());
        // cTeam.setInitialHelpSelection(selection);
        // }
        // if (cArea.getInitialHelpSelection() == null) {
        // FAreaSBean selection = new FAreaSBean(true, ((FProcessOrdersSBean) getModel().getDialogBean())
        // .getEstablishmentKey());
        // cArea.setInitialHelpSelection(selection);
        // }

    }

    /**
     * Gets the cArea.
     * 
     * @return the cArea.
     */
    public CAreaListView getcArea() {
        if (cArea == null) {
            cArea = new CAreaListView(getJlArea());
            cArea.setBeanProperty("selection.sectors");
        }
        return cArea;
    }

    /**
     * Gets the cFLine.
     * 
     * @return the cFLine.
     */
    public CHchyListView getcFLine() {
        if (cFLine == null) {
            cFLine = new CHchyListView(getJlFLine());
            cFLine.setBeanProperty("selection.lines");
        }
        return cFLine;
    }

    /**
     * Gets the cItemHchy.
     * 
     * @return the cItemHchy.
     */
    public CHchyListView getcItemHchy() {
        if (cItemHchy == null) {
            cItemHchy = new CHchyListView(getJlCategory());
            cItemHchy.setBeanProperty("selection.categories");
        }
        return cItemHchy;
    }

    // /**
    // * Gets the cItemHierarchy.
    // *
    // * @return the cItemHierarchy.
    // */
    // public CHchyLstValueView getcItemHierarchy() {
    // if (cItemHierarchy == null) {
    // cItemHierarchy = new CHchyLstValueView(true);
    // cItemHierarchy.setBeanProperty("itemHchyLstValue");
    // cItemHierarchy.getBtnLevel0().setVisible(false);
    // cItemHierarchy.getBtnLevel1().setVisible(false);
    // cItemHierarchy.getBtnLevel2().setVisible(false);
    // cItemHierarchy.getBtnLevel3().setVisible(false);
    // cItemHierarchy.getBtnLevel4().setVisible(false);
    // cItemHierarchy.getBtnLevel5().setVisible(false);
    // cItemHierarchy.getBtnLevel6().setVisible(false);
    // cItemHierarchy.getBtnLevel7().setVisible(false);
    // cItemHierarchy.getCHierarchyView().setVisible(false);
    // cItemHierarchy.getCHierarchyView().getLabelView().setAlwaysDisabled(false);
    // }
    // return cItemHierarchy;
    // }

    // /**
    // * Gets the cLineHierarchy.
    // *
    // * @return the cLineHierarchy.
    // */
    // public CHchyLstValueView getcLineHierarchy() {
    // if (cLineHierarchy == null) {
    // cLineHierarchy = new CHchyLstValueView(true);
    // cLineHierarchy.setBeanProperty("lineHchyLstValue");
    // cLineHierarchy.getBtnLevel0().setVisible(false);
    // cLineHierarchy.getBtnLevel1().setVisible(false);
    // cLineHierarchy.getBtnLevel2().setVisible(false);
    // cLineHierarchy.getBtnLevel3().setVisible(false);
    // cLineHierarchy.getBtnLevel4().setVisible(false);
    // cLineHierarchy.getBtnLevel5().setVisible(false);
    // cLineHierarchy.getBtnLevel6().setVisible(false);
    // cLineHierarchy.getBtnLevel7().setVisible(false);
    // cLineHierarchy.getCHierarchyView().setVisible(false);
    // cLineHierarchy.getCHierarchyView().getLabelView().setAlwaysDisabled(false);
    // }
    // return cLineHierarchy;
    // }

    /**
     * Gets the cTeam.
     * 
     * @return the cTeam.
     */
    public CTeamListView getcTeam() {
        if (cTeam == null) {
            cTeam = new CTeamListView(getJlTeam());
            cTeam.setBeanProperty("selection.teams");
        }
        return cTeam;
    }

    /**
     * Gets the jlFLine.
     * 
     * @return the jlFLine.
     */
    public JLabel getJlFLine() {
        if (jlFLine == null) {
            jlFLine = new JLabel(I18nClientManager.translate(ProductionMo.T32584));
        }
        return jlFLine;
    }

    /**
     * Gets the jlItemHierarchy.
     * 
     * @return the jlItemHierarchy.
     */
    public JLabel getJlItemHierarchy() {
        if (jlItemHierarchy == null) {
            jlItemHierarchy = new JLabel(I18nClientManager.translate(ProductionMo.T32582));
        }
        return jlItemHierarchy;
    }

    /**
     * Gets the jlItemHierarchy.
     * 
     * @return the jlItemHierarchy.
     */
    public JLabel getJlLineHierarchy() {
        if (jlLineHierarchy == null) {
            jlLineHierarchy = new JLabel(I18nClientManager.translate(ProductionMo.T32584));
        }
        return jlLineHierarchy;
    }

    /**
     * Gets the jlTeam.
     * 
     * @return the jlTeam.
     */
    public JLabel getJlTeam() {
        if (jlTeam == null) {
            jlTeam = new JLabel(I18nClientManager.translate(ProductionMo.T26196));
        }
        return jlTeam;
    }

    /**
     * Gets the jlUser.
     * 
     * @return the jlUser.
     */
    public JLabel getJlUser() {
        if (jlUser == null) {
            jlUser = new JLabel(I18nClientManager.translate(AdminConfig.T4448));
        }
        return jlUser;
    }

    /**
     * Gets the jPanel.
     * 
     * @return the jPanel.
     */
    public JPanel getjPanel() {
        return jPanel;
    }

    /**
     * Gets the cCategory.
     * 
     * @return the cCategory.
     */
    @SuppressWarnings("unchecked")
    private SwingInputTextFieldMI<String> getcCategory() {
        if (cCategory == null) {
            cCategory = new SwingInputTextFieldMI(InputTextFieldModel.TYPE_STRING, null, CUserListCtrl.class,
                    FUserBBean.class, null, null, null);
            // cUser.setHelpDialogSize(500, 650);
            cCategory.setBeanProperty("selection.categories");
        }
        return cCategory;
    }

    /**
     * Gets the cLine.
     * 
     * @return the cLine.
     */
    @SuppressWarnings("unchecked")
    private SwingInputTextFieldMI<String> getcLine() {
        if (cLine == null) {
            cLine = new SwingInputTextFieldMI(InputTextFieldModel.TYPE_STRING, null, CUserListCtrl.class,
                    FUserBBean.class, null, null, null);
            // cUser.setHelpDialogSize(500, 650);
            cLine.setBeanProperty("selection.lines");
        }
        return cLine;
    }

    /**
     * Gets the cUserListView.
     * 
     * @return the cUserListView.
     */
    private CUserListView getcUser() {
        if (cUser == null) {
            cUser = new CUserListView(getJlUser());
            cUser.setHelpDialogSize(500, 650);
            cUser.setBeanProperty("userId");
        }
        return cUser;
    }

    /**
     * Gets the itfDate.
     * 
     * @return the itfDate.
     */
    private SwingInputTextField getItfDate() {
        if (itfDate == null) {
            itfDate = new SwingInputTextField(Date.class, SwingInputTextField.FORMAT_DATE, true, getJlDate());
            itfDate.setBeanProperty("currentDate");
            itfDate.setAlwaysDisabled(false);
            itfDate.setEnabledForCreate(true);
            itfDate.setUpdateable(true);
        }
        return itfDate;
    }

    /**
     * Gets the jlArea.
     * 
     * @return the jlArea.
     */
    private JLabel getJlArea() {
        if (jlArea == null) {
            jlArea = new JLabel(I18nClientManager.translate(GenKernel.T30821));
        }
        return jlArea;
    }

    /**
     * Gets the jlCategory.
     * 
     * @return the jlCategory.
     */
    private JLabel getJlCategory() {
        if (jlCategory == null) {
            jlCategory = new JLabel(I18nClientManager.translate(ProductionMo.T32582));
        }
        return jlCategory;
    }

    /**
     * Gets the jlDate.
     * 
     * @return the jlDate.
     */
    private JLabel getJlDate() {
        if (jlDate == null) {
            jlDate = new JLabel(I18nClientManager.translate(Generic.T3057));
        }
        return jlDate;
    }

    /**
     * Gets the jlLine.
     * 
     * @return the jlLine.
     */
    private JLabel getJlLine() {
        if (jlLine == null) {
            jlLine = new JLabel(I18nClientManager.translate(ProductionMo.T32584));
        }
        return jlLine;
    }

    /**
     * This method initializes jPanelEntry.
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getJPanel() {
        if (jPanel == null) {

            // Layout
            jPanel = new JPanel();
            GridBagLayout gblPanel = new GridBagLayout();
            jPanel.setLayout(gblPanel);
            jPanel.setPreferredSize(new Dimension(350, 175));

            // Date
            GridBagConstraints gbcJlDate = new GridBagConstraints();
            gbcJlDate.insets = new Insets(0, 0, 15, 5);
            gbcJlDate.gridx = 0;
            gbcJlDate.gridy = 0;
            jPanel.add(getJlDate(), gbcJlDate);
            GridBagConstraints gbcItfDate = new GridBagConstraints();
            gbcItfDate.weightx = 1.0;
            gbcItfDate.insets = gbcJlDate.insets;
            gbcItfDate.fill = GridBagConstraints.BOTH;
            gbcItfDate.gridx = 1;
            gbcItfDate.gridy = 0;
            jPanel.add(getItfDate(), gbcItfDate);

            // Area
            GridBagConstraints gbcJlArea = new GridBagConstraints();
            gbcJlArea.insets = new Insets(0, 0, 5, 5);
            gbcJlArea.gridx = 0;
            gbcJlArea.gridy = 1;
            jPanel.add(getJlArea(), gbcJlArea);
            GridBagConstraints gbcCArea = new GridBagConstraints();
            gbcCArea.insets = gbcJlArea.insets;
            gbcCArea.weightx = 1.0;
            gbcCArea.fill = GridBagConstraints.BOTH;
            gbcCArea.gridx = 1;
            gbcCArea.gridy = 1;
            jPanel.add(getcArea(), gbcCArea);

            // // Item Category
            // GridBagConstraints gbcJlCategory = new GridBagConstraints();
            // gbcJlCategory.insets = new Insets(0, 0, 5, 5);
            // gbcJlCategory.gridx = 0;
            // gbcJlCategory.gridy = 2;
            // jPanel.add(getJlCategory(), gbcJlCategory);
            // GridBagConstraints gbcCCategory = new GridBagConstraints();
            // gbcCCategory.insets = gbcJlCategory.insets;
            // gbcCCategory.weightx = 1.0;
            // gbcCCategory.fill = GridBagConstraints.BOTH;
            // gbcCCategory.gridx = 1;
            // gbcCCategory.gridy = 2;
            // jPanel.add(getcCategory(), gbcCCategory);

            // Item Hierachy
            GridBagConstraints gbcJlItemHcy = new GridBagConstraints();
            gbcJlItemHcy.insets = new Insets(0, 0, 5, 5);
            gbcJlItemHcy.gridx = 0;
            gbcJlItemHcy.gridy = 3;
            jPanel.add(getJlCategory(), gbcJlItemHcy);
            GridBagConstraints gbcCItemHchy = new GridBagConstraints();
            gbcCItemHchy.insets = gbcJlItemHcy.insets;
            gbcCItemHchy.weightx = 1.0;
            gbcCItemHchy.fill = GridBagConstraints.BOTH;
            gbcCItemHchy.gridx = 1;
            gbcCItemHchy.gridy = 3;
            jPanel.add(getcItemHchy(), gbcCItemHchy);

            // Fabrication Line
            GridBagConstraints gbcJlFLine = new GridBagConstraints();
            gbcJlFLine.insets = new Insets(0, 0, 5, 5);
            gbcJlFLine.gridx = 0;
            gbcJlFLine.gridy = 4;
            jPanel.add(getJlFLine(), gbcJlFLine);
            GridBagConstraints gbcCFLine = new GridBagConstraints();
            gbcCFLine.insets = gbcJlFLine.insets;
            gbcCFLine.weightx = 1.0;
            gbcCFLine.fill = GridBagConstraints.BOTH;
            gbcCFLine.gridx = 1;
            gbcCFLine.gridy = 4;
            jPanel.add(getcFLine(), gbcCFLine);

            // // Manufacturing Line
            // GridBagConstraints gbcJlLine = new GridBagConstraints();
            // gbcJlLine.insets = new Insets(0, 0, 5, 5);
            // gbcJlLine.gridx = 0;
            // gbcJlLine.gridy = 4;
            // jPanel.add(getJlLine(), gbcJlLine);
            // GridBagConstraints gbcCLine = new GridBagConstraints();
            // gbcCLine.insets = gbcJlLine.insets;
            // gbcCLine.weightx = 1.0;
            // gbcCLine.fill = GridBagConstraints.BOTH;
            // gbcCLine.gridx = 1;
            // gbcCLine.gridy = 4;
            // jPanel.add(getcLine(), gbcCLine);

            // Team
            GridBagConstraints gbcJlTeam = new GridBagConstraints();
            gbcJlTeam.insets = new Insets(0, 0, 5, 5);
            gbcJlTeam.gridy = 0;
            gbcJlTeam.gridy = 5;
            jPanel.add(getJlTeam(), gbcJlTeam);
            GridBagConstraints gbcCTeam = new GridBagConstraints();
            gbcCTeam.insets = gbcJlTeam.insets;
            gbcCTeam.weightx = 1.0;
            gbcCTeam.fill = GridBagConstraints.BOTH;
            gbcCTeam.gridx = 1;
            gbcCTeam.gridy = 5;
            jPanel.add(getcTeam(), gbcCTeam);

            // // User
            // GridBagConstraints gbcJlUser = new GridBagConstraints();
            // gbcJlUser.insets = new Insets(0, 0, 5, 5);
            // gbcJlUser.gridy = 0;
            // gbcJlUser.gridy = 6;
            // jPanel.add(getJlUser(), gbcJlUser);
            // GridBagConstraints gbcCUser = new GridBagConstraints();
            // gbcCUser.insets = gbcJlUser.insets;
            // gbcCUser.weightx = 1.0;
            // gbcCUser.fill = GridBagConstraints.BOTH;
            // gbcCUser.gridx = 1;
            // gbcCUser.gridy = 6;
            // jPanel.add(getcUser(), gbcCUser);

            // // Item Hierarchy
            // GridBagConstraints gbcJlItemHierachy = new GridBagConstraints();
            // gbcJlItemHierachy.insets = new Insets(0, 0, 5, 5);
            // gbcJlItemHierachy.gridy = 0;
            // gbcJlItemHierachy.gridy = 7;
            // jPanel.add(getJlItemHierarchy(), gbcJlItemHierachy);
            // GridBagConstraints gbcCItemHierarchy = new GridBagConstraints();
            // gbcCItemHierarchy.insets = gbcJlItemHierachy.insets;
            // gbcCItemHierarchy.weightx = 1.0;
            // gbcCItemHierarchy.fill = GridBagConstraints.BOTH;
            // gbcCItemHierarchy.gridx = 1;
            // gbcCItemHierarchy.gridy = 7;
            // jPanel.add(getcItemHierarchy(), gbcCItemHierarchy);

            // // Line Hierarchy
            // GridBagConstraints gbcJlLineHierarchy = new GridBagConstraints();
            // gbcJlLineHierarchy.insets = new Insets(0, 0, 5, 5);
            // gbcJlLineHierarchy.gridy = 0;
            // gbcJlLineHierarchy.gridy = 8;
            // jPanel.add(getJlLineHierarchy(), gbcJlLineHierarchy);
            // GridBagConstraints gbcCLineHierarchy = new GridBagConstraints();
            // gbcCLineHierarchy.insets = gbcJlLineHierarchy.insets;
            // gbcCLineHierarchy.weightx = 1.0;
            // gbcCLineHierarchy.fill = GridBagConstraints.BOTH;
            // gbcCLineHierarchy.gridx = 1;
            // gbcCLineHierarchy.gridy = 8;
            // jPanel.add(getcLineHierarchy(), gbcCLineHierarchy);

        }
        return jPanel;
    }

    /**
     * Initialize selection dialog-box size and set the main panel.
     * 
     */
    private void initialize() {
        this.setSize(new Dimension(369, 400));
        this.setTitle(I18nClientManager.translate(Jtech.T8241));
        this.addPanel(getJPanel());
    }

}

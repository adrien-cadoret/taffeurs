/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: IGraphMOView.java,v $
 * Created on 4 oct. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph;


import javax.swing.JMenu;

import fr.vif.jtech.ui.events.generic.GenericActionListener;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;


/**
 * Interface of the Graph view.
 * 
 * @author jd
 */
public interface IGraphMOView {
    /**
     * Add an action listener.
     * 
     * @param listener listener to add.
     */
    public void addGenericActionListener(GenericActionListener listener);

    /**
     * Add a menu to the default popup menu.
     * 
     * @param popToAdd menu to add.
     */
    public void addPopupMenu(final JMenu popToAdd);

    /**
     * Set a graph.
     * 
     * @param graph graph
     */
    public void setGraph(final GraphMO graph);

}

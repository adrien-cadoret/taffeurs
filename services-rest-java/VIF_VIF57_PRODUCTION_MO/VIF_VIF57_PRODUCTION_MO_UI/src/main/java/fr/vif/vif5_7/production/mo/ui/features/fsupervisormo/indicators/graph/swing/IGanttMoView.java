/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: IGanttMoView.java,v $
 * Created on 16 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOPeriod;


/**
 * Interface of the Gantt view.
 * 
 * @author jd
 * @param <SRC> type of the source bean contained in GraphPeriod.
 */
public interface IGanttMoView<SRC> {

    /**
     * Return the last clicked graph period.
     * 
     * @return a bean GraphPeriod.
     */
    public GraphMOPeriod<SRC> getLastClickedGraphPeriod();

    /**
     * Set graph and options.
     * 
     * @param graph graph bean
     * @param options {@link GraphMOOptions}
     */
    public void setGraph(final GraphMO graph, final GraphMOOptions options);

}

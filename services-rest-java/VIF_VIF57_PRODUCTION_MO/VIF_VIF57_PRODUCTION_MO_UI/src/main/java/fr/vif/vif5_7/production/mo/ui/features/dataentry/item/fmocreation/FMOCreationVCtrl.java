/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationVCtrl.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowSelectionEvent;
import fr.vif.jtech.ui.events.table.TableRowSelectionListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaReturnInitBean;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.production.mo.business.beans.common.labels.LabelEvent;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FMOCreationVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FabricationInfos;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmocreation.FMOCreationCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.CodeFunction;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.flabels.LabelPrintHelper;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * MO creation viewer controller.
 * 
 * @author glc
 */
public class FMOCreationVCtrl extends AbstractStandardViewerController<FMOCreationVBean> implements
        TableRowSelectionListener {

    private static final Logger LOGGER = Logger.getLogger(FMOCreationVCtrl.class);

    private FMOCreationFCtrl    featureController;

    private FMOCreationCBS      fmOCreationCBS;

    /**
     * Fire performSave.
     * 
     * @return <code>true</code> if the bean was successfully saved.
     */
    public boolean firePerformSave() {
        return this.performSave();
    }

    /**
     * Gets the fmOCreationCBS.
     * 
     * @category getter
     * @return the fmOCreationCBS.
     */
    public FMOCreationCBS getFmOCreationCBS() {
        return fmOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        this.addSharedContextListener(((FMOCreationVIView) getView()).getBrowserController());
        fireSharedContextSend(getSharedContext());
        super.initialize();
        // Subscription to browser row validation.
        ((FMOCreationVIView) getView()).getBrowserController().addTableRowSelectionListener(this);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeViewWithBean(final FMOCreationVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeViewWithBean(bean=" + bean + ")");
        }

        super.initializeViewWithBean(new FMOCreationVBean());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeViewWithBean(bean=" + bean + ")");
        }
    }

    // /**
    // * {@inheritDoc}
    // *
    // */
    // @Override
    // public void selectedRowChanged(final TableRowChangeEvent event) {
    // if (!(event.getSource() instanceof FMOCreationBCtrl)) {
    // String item = ((CodeLabel) event.getSelectedBean()).getCode();
    // getModel().getBean().setItem(item);
    // this.setCreationMode(true);
    // searchFabricationInfos();
    // fireButtonStatechanged();
    // } else {
    // super.selectedRowChanged(event);
    // }
    // }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanging(event=" + event + ")");
        }

        if (!(event.getSource() instanceof FMOCreationBCtrl)) {
            // To avoid question when changing of item.
            performUndo();
        }
        super.selectedRowChanging(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanging(event=" + event + ")");
        }
    }

    /**
     * Sets the featureController.
     * 
     * @category setter
     * @param featureController featureController.
     */
    public void setFeatureController(final FMOCreationFCtrl featureController) {
        this.featureController = featureController;
    }

    /**
     * Sets the fmOCreationCBS.
     * 
     * @category setter
     * @param fmOCreationCBS fmOCreationCBS.
     */
    public void setFmOCreationCBS(final FMOCreationCBS fmOCreationCBS) {
        this.fmOCreationCBS = fmOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tableRowSelected(final TableRowSelectionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - tableRowSelected(event=" + event + ")");
        }

        if (!(event.getSource() instanceof FMOCreationBCtrl)) {
            String item = ((CodeLabel) event.getSelectedBean()).getCode();
            getModel().getBean().setItem(item);
            this.setCreationMode(true);
            searchFabricationInfos();
            fireButtonStatechanged();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - tableRowSelected(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMOCreationVBean convert(final Object bean) throws UIException {
        return new FMOCreationVBean();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FMOCreationVBean bean) throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMOCreationVBean insertBean(final FMOCreationVBean bean, final FMOCreationVBean initialBean)
            throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertBean(bean=" + bean + ", initialBean=" + initialBean + ")");
        }

        checkFMOCreationVBean(bean);
        try {
            MOBean moBean = getFmOCreationCBS().createStartedMO(getIdCtx(), bean);
            LabelEvent labelEvent = new LabelEvent();
            labelEvent.setMoKey(moBean.getMoKey());
            labelEvent.setEventType(LabelingEventType.MO_CREATION);
            labelEvent.setWorkstationId(getIdCtx().getWorkstation());
            labelEvent.setFeatureId(CodeFunction.PROD_IN.getValue());
            LabelPrintHelper labelPrintHelper = StandardControllerFactory.getInstance().getController(
                    LabelPrintHelper.class);
            labelPrintHelper.launchPrint(getView(), getIdentification(), getIdCtx(), labelEvent);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.STARTED_MO_BEAN, moBean);
            fireSharedContextSend(getSharedContext());

        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertBean(bean=" + bean + ", initialBean=" + initialBean + ")=" + bean);
        }
        return bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean ret = true;
        FMOCreationVBean vbean = getModel().getBean();
        if (vbean == null || vbean.getFlowType() == null || vbean.getFabrication() == null
                || vbean.getFabrication().getFabricationType() == null
                || vbean.getFabrication().getFabricationCLs() == null) {
            ret = false;
        }
        if (ret && FMOCreationFCtrl.EVENT_OPENCRITERIA.equals(reference)) {
            ret = getModel().getBean().getReturnInitBean().getListCriteria().size() > 0;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionEnabled(final ToolBarButtonType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isPanelActionEnabled(type=" + type + ")");
        }

        boolean ret = false;
        if (type == ToolBarButtonType.SAVE) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isPanelActionEnabled(type=" + type + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMOCreationVBean updateBean(final FMOCreationVBean bean) throws UIException, UIErrorsException {
        return getBean();
    }

    /**
     * Check the FMOCretionVBean.
     * 
     * @param vbean the viewer bean
     * @throws UIException if error occurs.
     */
    private void checkFMOCreationVBean(final FMOCreationVBean vbean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkFMOCreationVBean(vbean=" + vbean + ")");
        }

        if (vbean.getItem() == null || "".equals(vbean.getItem())) {
            throw new UIException(Generic.T12640, new VarParamTranslation(null));
        }
        if (vbean.getQuantityUnit().getQty() == 0) {
            throw new UIException(Generic.T20724, new VarParamTranslation(getBean().getQuantityUnit().getQty()));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkFMOCreationVBean(vbean=" + vbean + ")");
        }
    }

    /**
     * Update the quantity unit.
     */
    private void searchFabricationInfos() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - searchFabricationInfos()");
        }

        try {
            CriteriaReturnInitBean criteriaReturnInitBean = getFmOCreationCBS().getCriteriaReturnInitBean(getIdCtx(),
                    getIdentification().getCompany(), getIdentification().getEstablishment(),
                    getIdentification().getLogin(), getModel().getBean().getItem());
            if (criteriaReturnInitBean.getAutoOpening() && criteriaReturnInitBean.getListCriteria().size() > 0) {
                CriteriaDBean criteriaDBean = new CriteriaDBean(criteriaReturnInitBean, getModel().getBean().getItem());
                WorkShopCriteriaTools.showCriteria(this.featureController.getView(), this.featureController,
                        this.featureController, getModel().getIdentification(), criteriaDBean);
                criteriaReturnInitBean = criteriaDBean.getCriteriaReturnInitBean();
            }
            String workstationId = (String) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.LOGICAL_WORKSTATION);
            FabricationInfos fabricationInfos = getFmOCreationCBS().getFabricationInfos(getIdCtx(),
                    getIdCtx().getCompany(), getIdCtx().getEstablishment(), workstationId,
                    getModel().getBean().getItem(), criteriaReturnInitBean.getListCriteria());
            getModel().getBean().setFlowType(fabricationInfos.getFlowType());
            getModel().getBean().setFabrication(fabricationInfos.getFabrication());
            getModel().getBean().setQuantityUnit(fabricationInfos.getQuantityUnit());
            if (fabricationInfos.getReturnInitBean().getAutoOpening()
                    && fabricationInfos.getReturnInitBean().getListCriteria().size() > 0) {
                CriteriaDBean criteriaDBean = new CriteriaDBean(fabricationInfos.getReturnInitBean(), getModel()
                        .getBean().getItem());
                fabricationInfos.setReturnInitBean(criteriaDBean.getCriteriaReturnInitBean());
                WorkShopCriteriaTools.showCriteria(this.featureController.getView(), this.featureController,
                        this.featureController, getModel().getIdentification(), criteriaDBean);
            }
            getModel().getBean().setReturnInitBean(fabricationInfos.getReturnInitBean());
            getView().render();
        } catch (BusinessException e) {
            LOGGER.debug(e.getMessage());
            getModel().setBean(new FMOCreationVBean());
            getView().render();
            getView().showError(e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - searchFabricationInfos()");
        }
    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: EndDayIndicatorMOTableModelEvent.java,v $
 * Created on 21 avr. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.swing;


import javax.swing.event.TableModelEvent;
import javax.swing.table.TableModel;


/**
 * EndDay table model event.
 *
 * @author ac
 */
public class EndDayIndicatorMOTableModelEvent extends TableModelEvent {

    /**
     * Ctor.
     * 
     * @param source table model
     */
    public EndDayIndicatorMOTableModelEvent(final TableModel source) {
        super(source);
    }

}

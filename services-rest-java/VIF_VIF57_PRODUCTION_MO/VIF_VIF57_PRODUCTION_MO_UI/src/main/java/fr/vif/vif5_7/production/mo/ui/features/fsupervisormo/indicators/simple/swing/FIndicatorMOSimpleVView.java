/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOSimpleVView.java,v $
 * Created on 21 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.simple.swing;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FIndicatorMOValuesVBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.swing.FIndicatorMOJTableView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.AbstractIndicatorMOViewerView;


/**
 * Viewer view for the simple Indicator.
 * 
 * @author jd
 */
public class FIndicatorMOSimpleVView extends AbstractIndicatorMOViewerView<FIndicatorMOValuesVBean> {
    private FIndicatorMOJTableView indicatorJTableView;

    /**
     * Constructor.
     */
    public FIndicatorMOSimpleVView() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        setLayout(gridBagLayout);
        GridBagConstraints gbcindicatorRoundTripJTableView = new GridBagConstraints();
        gbcindicatorRoundTripJTableView.fill = GridBagConstraints.BOTH;
        gbcindicatorRoundTripJTableView.weighty = 1.0;
        gbcindicatorRoundTripJTableView.weightx = 1.0;
        gbcindicatorRoundTripJTableView.anchor = GridBagConstraints.NORTHWEST;
        gbcindicatorRoundTripJTableView.gridx = 0;
        gbcindicatorRoundTripJTableView.gridy = 0;
        add(getIndicatorJTableView(), gbcindicatorRoundTripJTableView);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render() {
        FIndicatorMOValuesVBean vbean = getModel().getBean();

        getIndicatorJTableView().setViewerBean(vbean);

        // getIndicatorJTableView().setModel(getIndicatorJTableView().getModel());

        super.render();
    }

    /**
     * 
     * get The table View.
     * 
     * @return a {@link FIndicatorMOJTableView}
     */
    private FIndicatorMOJTableView getIndicatorJTableView() {
        if (indicatorJTableView == null) {
            indicatorJTableView = new FIndicatorMOJTableView();
        }
        return indicatorJTableView;
    }

}

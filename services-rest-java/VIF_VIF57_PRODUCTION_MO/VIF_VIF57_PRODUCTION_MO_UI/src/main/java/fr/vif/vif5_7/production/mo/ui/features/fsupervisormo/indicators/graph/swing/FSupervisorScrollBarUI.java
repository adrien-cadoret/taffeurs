/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorScrollBarUI.java,v $
 * Created on 7 apr. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.plaf.metal.MetalScrollBarUI;


/**
 * The UI for scrollbar in supervisor.
 *
 * @author ac
 */
public class FSupervisorScrollBarUI extends MetalScrollBarUI {

    private Image   imageThumb = null;
    private Image   imageTrack = null;
    private JButton button     = new JButton() {

        @Override
        public Dimension getPreferredSize() {
            return new Dimension(0, 0);
        }

    };

    /**
     * Constructor.
     */
     public FSupervisorScrollBarUI() {
        // CHECKSTYLE:OFF
        imageThumb = create(8, 8, Color.GRAY.darker());
        imageTrack = create(8, 8, Color.WHITE);
        // CHECKSTYLE:ON
     }

     /**
      * Create an image to show in the scroll bar.
      * 
      * @param width width of the image
      * @param height height of the image
      * @param color color
      * @return an image.
      */
     public Image create(final int width, final int height, final Color color) {
         BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
         Graphics2D g2d = bi.createGraphics();
         g2d.setPaint(color);
         g2d.fillRect(0, 0, width, height);
         g2d.dispose();
         return bi;
     }

     /**
      * {@inheritDoc}
      */
     @Override
     protected JButton createDecreaseButton(final int orientation) {
         return button;
     }

     /**
      * {@inheritDoc}
      */

     @Override
     protected JButton createIncreaseButton(final int orientation) {
         return button;
     }

     /**
      * {@inheritDoc}
      */
     @Override
     protected void paintThumb(final Graphics g, final JComponent c, final Rectangle r) {
         g.setColor(Color.red);
         ((Graphics2D) g).drawImage(imageThumb, r.x, r.y, r.width, r.height, null);
     }

     /**
      * {@inheritDoc}
      */

     @Override
     protected void paintTrack(final Graphics g, final JComponent c, final Rectangle r) {
         ((Graphics2D) g).drawImage(imageTrack, r.x, r.y, r.width, r.height, null);
     }

}

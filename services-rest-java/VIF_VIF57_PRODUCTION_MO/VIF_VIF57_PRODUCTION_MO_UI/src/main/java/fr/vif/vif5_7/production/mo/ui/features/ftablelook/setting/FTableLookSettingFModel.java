/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingFModel.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingVBean;


/**
 * TableLook Feature Model.
 * 
 * @author kl
 */
public class FTableLookSettingFModel extends StandardFeatureModel<FTableLookSettingBBean, FTableLookSettingVBean> {

    public static final String USER_VIF = "VIF";

    /**
     * Default constructor.
     */
    public FTableLookSettingFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FTableLookSettingBModel.class, null, FTableLookSettingBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FTableLookSettingVModel.class, null, FTableLookSettingVCtrl.class));

    }
}

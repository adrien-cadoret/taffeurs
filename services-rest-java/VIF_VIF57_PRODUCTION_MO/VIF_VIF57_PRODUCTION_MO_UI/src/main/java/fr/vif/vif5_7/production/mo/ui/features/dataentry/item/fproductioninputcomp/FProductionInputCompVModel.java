/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputCompVModel.java,v $
 * Created on 7 Dec 2015 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcomp;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcomp.FProductionInputCompVBean;


/**
 * InputOperationItem : Viewer Model.
 * 
 * @author vr
 */
public class FProductionInputCompVModel extends ViewerModel<FProductionInputCompVBean> {

    /**
     * Default constructor.
     * 
     */
    public FProductionInputCompVModel() {
        super();
        setBeanClass(FProductionInputCompVBean.class);
        setDeleteNeedsConfirmation(false);
        setEnterInCreationOnEmptyBrowser(false);
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMSOCreationVCtrl.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.production.mo.business.beans.common.labels.LabelEvent;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FabricationInfos;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmsocreation.FMSOCreationVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmsocreation.FMSOCreationCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.CodeFunction;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.flabels.LabelPrintHelper;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * MSO creation viewer controller.
 * 
 * @author glc
 */
public class FMSOCreationVCtrl extends AbstractStandardViewerController<FMSOCreationVBean> {

    private static final Logger LOGGER = Logger.getLogger(FMSOCreationVCtrl.class);

    private FMSOCreationCBS     fmSOCreationCBS;

    /**
     * Fire performSave.
     * 
     * @return <code>true</code> if the bean was successfully saved.
     */
    public boolean firePerformSave() {
        return this.performSave();
    }

    /**
     * Gets the fmSOCreationCBS.
     * 
     * @category getter
     * @return the fmSOCreationCBS.
     */
    public FMSOCreationCBS getFmSOCreationCBS() {
        return fmSOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeViewWithBean(final FMSOCreationVBean bean) {
        MOBean originMOBean = (MOBean) getSharedContext().get(Domain.DOMAIN_PARENT, FMSOCreationBCtrl.ORIGIN_MO_BEAN);
        try {
            FabricationInfos fabinf = getFmSOCreationCBS().getFabricationInfos(getIdCtx(), originMOBean);
            bean.setQuantityUnit(fabinf.getQuantityUnit());
        } catch (BusinessException e) {
            LOGGER.error("", e);
            getView().show(new UIException(e));
        }
        super.initializeViewWithBean(bean);
    }

    /**
     * Sets the fmSOCreationCBS.
     * 
     * @category setter
     * @param fmSOCreationCBS fmSOCreationCBS.
     */
    public void setFmSOCreationCBS(final FMSOCreationCBS fmSOCreationCBS) {
        this.fmSOCreationCBS = fmSOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMSOCreationVBean convert(final Object bean) throws UIException {
        return (FMSOCreationVBean) bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FMSOCreationVBean bean) throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMSOCreationVBean insertBean(final FMSOCreationVBean bean, final FMSOCreationVBean initialBean)
            throws UIException, UIErrorsException {
        return bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean ret = true;
        FMSOCreationVBean vbean = getModel().getBean();
        if (vbean == null || vbean.getMoKey() == null) {
            ret = false;
        }
        if (ret && reference.equals(FMSOCreationFCtrl.EVENT_OPENCRITERIA)) {
            ret = getModel().getBean().getReturnInitBean().getListCriteria().size() > 0;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionEnabled(final ToolBarButtonType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isPanelActionEnabled(type=" + type + ")");
        }

        boolean ret = false;
        if (type == ToolBarButtonType.SAVE) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isPanelActionEnabled(type=" + type + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMSOCreationVBean updateBean(final FMSOCreationVBean bean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(bean=" + bean + ")");
        }

        checkFMSOCreationVBean(bean);
        try {
            MOBean moBean = getFmSOCreationCBS().createStartedMSO(getIdCtx(), bean);
            LabelEvent labelEvent = new LabelEvent();
            labelEvent.setMoKey(moBean.getMoKey());
            labelEvent.setEventType(LabelingEventType.MO_CREATION);
            labelEvent.setWorkstationId(getIdCtx().getWorkstation());
            labelEvent.setFeatureId(CodeFunction.PROD_IN.getValue());
            LabelPrintHelper labelPrintHelper = StandardControllerFactory.getInstance().getController(
                    LabelPrintHelper.class);
            labelPrintHelper.launchPrint(getView(), getIdentification(), getIdCtx(), labelEvent);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.STARTED_MO_BEAN, moBean);
            fireSharedContextSend(getSharedContext());
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(bean=" + bean + ")=" + bean);
        }
        return bean;
    }

    /**
     * Check the FMSOCretionVBean.
     * 
     * @param vbean the viewer bean
     * @throws UIException if error occurs.
     */
    private void checkFMSOCreationVBean(final FMSOCreationVBean vbean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkFMSOCreationVBean(vbean=" + vbean + ")");
        }

        if (vbean.getQuantityUnit().getQty() == 0) {
            throw new UIException(Generic.T20724, new VarParamTranslation(getBean().getQuantityUnit().getQty()));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkFMSOCreationVBean(vbean=" + vbean + ")");
        }
    }

}

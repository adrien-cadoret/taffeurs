/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionBCtrl.java,v $
 * Created on 17 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction;


import java.util.Collections;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBS;


/**
 * InputOperationItem : Browser Controller.
 * 
 * @author vr
 */
public class FProductionBCtrl extends AbstractBrowserController<FProductionBBean> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FProductionBCtrl.class);

    private FProductionCBS      fproductionCBS;

    /**
     * Default constructor.
     * 
     */
    public FProductionBCtrl() {
        super();
    }

    /**
     * Change the current columns.
     * 
     * @param touchModelFunction model define for the screen
     */
    public void changeColumns(final TouchModelFunction touchModelFunction) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - changeColumns(touchModelFunction=" + touchModelFunction + ")");
        }

        FProductionColumnsFactory.manageColumns(getModel(), touchModelFunction);
        getView().initializeView(this);
        if (getModel().getRowCount() != 0) {
            getModel().setCurrentRowNumber(0);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - changeColumns(touchModelFunction=" + touchModelFunction + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        FProductionSBean sBean = new FProductionSBean();
        return sBean;

    }

    /**
     * Gets the fproductionCBS.
     * 
     * @category getter
     * @return the fproductionCBS.
     */
    public FProductionCBS getFproductionCBS() {
        return fproductionCBS;
    }

    /**
     * Reposition to an input operation bean after a flash.
     * 
     * @param operationItemKey operationItemKey bean to reposit
     * @return true if reposition ok, false otherwise
     */
    public boolean repositionToOperationItemKey(final OperationItemKey operationItemKey) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - repositionToOperationItemKey(operationItemKey=" + operationItemKey + ")");
        }

        boolean ret = false;
        // VR PERF if already reposited, don't change current row
        if (getModel().getCurrentBean() != null && getModel().getCurrentBean().getOperationItemBean() != null
                && getModel().getCurrentBean().getOperationItemBean().getOperationItemKey() != null
                && operationItemKey.equals(getModel().getCurrentBean().getOperationItemBean().getOperationItemKey())) {
            ret = true;
        } else {
            try {
                for (int i = 0; i < getModel().getRowCount(); i++) {
                    FProductionBBean bbean = getModel().getBeanAt(i);

                    if (bbean.getOperationItemBean().getOperationItemKey().equals(operationItemKey)) {
                        changeCurrentRow(i);
                        ret = true;
                        break;
                    }
                }
            } catch (UIVetoException e) {
                getView().show(e);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - repositionToOperationItemKey(operationItemKey=" + operationItemKey + ")=" + ret);
        }
        return ret;
    }

    /**
     * Sets the fproductionCBS.
     * 
     * @category setter
     * @param fproductionCBS fproductionCBS.
     */
    public void setFproductionCBS(final FProductionCBS fproductionCBS) {
        this.fproductionCBS = fproductionCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FProductionBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FProductionBBean> lst = null;
        try {
            lst = getFproductionCBS().queryElements(getIdCtx(), (FProductionSBean) selection, startIndex, rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }
        Collections.sort(lst, getModel().getComparator());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return lst;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionColumnsFactory.java,v $
 * Created on 3 Mars, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninput.FProductionInputBBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelArray;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.CodeFieldModel;


/**
 * Use to display the column in Browser Model. The columns to display are set by the bean TouchModelFunction.<br/>
 * This bean is a representation of the model define in the screen parameter of model touch.
 * 
 * Manage the Screens Input/Output Production.
 * 
 * @author kl
 */
public final class FProductionColumnsFactory {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FProductionColumnsFactory.class);

    /**
     * Default constructor.
     */
    private FProductionColumnsFactory() {

    }

    /**
     * Set columns in Browser model set in parameter.
     * 
     * @param touchModelFunction touchModelFunction
     * @param browserModel browser model
     */
    public static void manageColumns(final BrowserModel<?> browserModel, final TouchModelFunction touchModelFunction) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getColumns(touchModelFunction=" + touchModelFunction + ")");
        }
        browserModel.removeAllColumns();
        browserModel.setBrowserType(BrowserModel.RENDERER_BY_CELL);
        if (touchModelFunction != null) {

            int colDisplay = 0;

            for (TouchModelArray touchModelArray : touchModelFunction.getTouchModelArrays()) {
                CodeFieldModel codeFieldModel = CodeFieldModel.getCodeFieldModel(touchModelArray.getChamp());
                switch (codeFieldModel) {
                    case CART:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_CODE.getValue(), 0,
                                colDisplay));
                        break;
                    case LART:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_LONG_LABEL
                                .getValue(), 0, colDisplay));
                        break;
                    case RART:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_SHORT_LABEL
                                .getValue(), 0, colDisplay));
                        break;
                    case QTARTAFER_1:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_TODO.getValue(), 0,
                                colDisplay));
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;
                    case QTARTFAIT_1:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_DONE.getValue(), 0,
                                colDisplay));
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;
                    case QTARTFAIT_DONE_TODO:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_DONE.getValue(), 0,
                                colDisplay));
                        browserModel.addColumn(new BrowserColumn(" / ", FProductionInputBBeanEnum.QTTY_TODO.getValue(),
                                0, colDisplay));
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;
                    case QTARTFAIT_LEFT_TO_DO:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_LEFT_TO_BE
                                .getValue(), 0, colDisplay));
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;
                    case QTARTFAIT_LEFT_TO_DO_TODO:
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_LEFT_TO_BE
                                .getValue(), 0, colDisplay));
                        browserModel.addColumn(new BrowserColumn(" / ", FProductionInputBBeanEnum.QTTY_TODO.getValue(),
                                0, colDisplay));
                        browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0,
                                colDisplay));
                        break;

                    default:
                        break;
                }
                colDisplay = colDisplay + 1;
            }
        } else {
            browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_SHORT_LABEL.getValue(), 0, 0));
            browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_CODE.getValue(), 0, 1));
            browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_DONE.getValue(), 0, 2));
            browserModel.addColumn(new BrowserColumn(" / ", FProductionInputBBeanEnum.QTTY_TODO.getValue(), 0, 2));
            browserModel.addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0, 2));
        }

    }

}

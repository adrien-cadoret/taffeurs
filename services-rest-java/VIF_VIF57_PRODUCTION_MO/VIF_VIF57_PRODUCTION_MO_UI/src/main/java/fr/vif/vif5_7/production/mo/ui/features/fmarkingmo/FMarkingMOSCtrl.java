/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOSCtrl.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import java.util.Date;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.DateTimeFormatEnum;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.dialogs.selection.AbstractStandardSelectionController;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.models.sharedcontext.SharedContext;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.activities.activities.ActivitiesActivities;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOSBean;


/**
 * MarkingMO : Selection Controller.
 * 
 * @author lmo
 */
public class FMarkingMOSCtrl extends AbstractStandardSelectionController {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FMarkingMOSCtrl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void beanValidation(final Object dialogBean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - beanValidation(dialogBean=" + dialogBean + ")");
        }

        if (dialogBean instanceof FMarkingMOSBean) {
            FMarkingMOSBean vSBean = (FMarkingMOSBean) dialogBean;
            if (vSBean.getLineCL() == null || vSBean.getLineCL().equals(new CodeLabel("", ""))) {
                throw new UIException(Jtech.T24628, ActivitiesActivities.T34102);
            }

        }
        super.beanValidation(dialogBean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - beanValidation(dialogBean=" + dialogBean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        if (event.getModel().getReference().equals(FMarkingMOUIConstants.BTN_SHOW_ALL)) {
            FMarkingMOSBean selection = (FMarkingMOSBean) getSelection();
            selection.setShowFinishedMO(!selection.getShowFinishedMO());
            overwriteRequiredValues();
            getView().render();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - validateBean(creationMode=" + creationMode + ", beanProperty=" + beanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }

        overwriteRequiredValues();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - validateBean(creationMode=" + creationMode + ", beanProperty=" + beanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - valueChanged(beanProperty=" + beanProperty + ", propertyValue=" + propertyValue + ")");
        }

        overwriteRequiredValues();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - valueChanged(beanProperty=" + beanProperty + ", propertyValue=" + propertyValue + ")");
        }
    }

    /**
     * Require some value overwrite (date, show finished MO) if there is MO processing or simulating on the selected
     * line.
     */
    private void overwriteRequiredValues() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - overwriteRequiredValues()");
        }

        FMarkingMOSBean vSBean = (FMarkingMOSBean) getModel().getDialogBean();

        if (vSBean != null) {
            // ckeck if there is an M.O. processing or simulating on this line.
            // if true, change the date to the date of the MO
            Date vDate = vSBean.getMoDate();
            CodeLabel vLineCL = vSBean.getLineCL();
            Boolean vShowFinishedMO = vSBean.getShowFinishedMO();

            if (vDate != null && vLineCL != null && vShowFinishedMO != null) {
                SharedContext vSharedContext = FMarkingMOSharedContext.getInstance().getCtrl().getSharedContext();

                Object vObjectInContext = vSharedContext
                        .get(Domain.DOMAIN_THIS, FMarkingMOUIConstants.PROCESSING_LINES);
                if (vObjectInContext instanceof Map) {
                    Map vMapProcessingDatesPerLine = (Map) vObjectInContext; // (Map<CodeLabel, Date>)
                    Object vProcessingDate = vMapProcessingDatesPerLine.get(vLineCL);
                    if (vProcessingDate != null && !vDate.equals(vProcessingDate)) {
                        getView().showInformation(
                                I18nClientManager.translate(
                                        ProductionMo.T35039,
                                        false,
                                        new VarParamTranslation(DateHelper.formatDate((Date) vProcessingDate,
                                                DateHelper.getDateFormatForLocale(DateTimeFormatEnum.DD_MM_YYYY)))));
                        vSBean.setMoDate((Date) vProcessingDate);
                    }
                }

                if (!vShowFinishedMO) {
                    vObjectInContext = vSharedContext.get(Domain.DOMAIN_THIS,
                            FMarkingMOUIConstants.FINISHED_RUNNING_LINES);
                    if (vObjectInContext instanceof Set<?>) {
                        Set<?> vListFinishedRunningMOLine = (Set<?>) vObjectInContext; // (Map<CodeLabel, Date>)
                        if (vListFinishedRunningMOLine != null && vListFinishedRunningMOLine.contains(vLineCL)) {
                            getView().showInformation(I18nClientManager.translate(ProductionMo.T35107));
                            vSBean.setShowFinishedMO(true);
                        }
                    }
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - overwriteRequiredValues()");
        }
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputFCtrl.java,v $
 * Created on 5 Jun 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.FatalException;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.bean.BeanUpdateEvent;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.Key;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaReturnInitBean;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.Unit;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.RowidKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labels.LabelEvent;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.CodeFunction;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFIView;
import fr.vif.vif5_7.stock.kernel.business.beans.features.quality.nc.fncflow.FNCFlowSharedContextBean;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCConstants;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCActType;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCType;
import fr.vif.vif5_7.stock.kernel.ui.util.StockUITools;


/**
 * Data entry list feature for boning.
 * 
 * @author cj
 */
public class FBoningDataEntryListInputFCtrl extends FDataEntryListFCtrl {

    /** LOGGER. */
    private static final Logger     LOGGER               = Logger.getLogger(FBoningDataEntryListInputFCtrl.class);
    private static final String     PRINT_ROLL_LABEL_BTN = "printRollLabelBtn";
    private Class                   entityType;
    private ToolBarBtnStdModel      printRollLabelBtn    = null;

    private FBoningDataEntryListCBS boningDataEntryListCBS;

    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        try {
            if (event.getModel().getReference().equals(PRINT_CONTAINER_BTN)) {
                // that's the print act button
                FBoningDataEntryListInputBBean movementBBean = (FBoningDataEntryListInputBBean) getBrowserController()
                        .getModel().getCurrentBean();

                FBoningHelper fBoningHelper = StandardControllerFactory.getInstance()
                        .getController(FBoningHelper.class);
                LabelEvent labelEvent = new LabelEvent();

                if (getSharedContext().get(Domain.DOMAIN_PARENT, OPERATION_ITEM_BEAN) instanceof InputOperationItemBean) {
                    labelEvent.setActivityItemType(ActivityItemType.INPUT);
                    labelEvent.setFeatureId(CodeFunction.PROD_IN.getValue());
                } else if (getSharedContext().get(Domain.DOMAIN_PARENT, OPERATION_ITEM_BEAN) instanceof OutputOperationItemBean) {
                    labelEvent.setActivityItemType(ActivityItemType.OUTPUT);
                    labelEvent.setFeatureId(CodeFunction.PROD_OUT.getValue());
                }
                labelEvent.setEventType(LabelingEventType.MO_FIRST_DECLARATION);

                MOKey moKey = new MOKey();
                moKey.setEstablishmentKey(movementBBean.getItemMovementBean().getItemMovementKey()
                        .getEstablishmentKey());
                moKey.setChrono(movementBBean.getItemMovementBean().getItemMovementKey().getChrono());
                labelEvent.setMoKey(moKey);
                labelEvent.setRowIdKey(new RowidKey(movementBBean.getItemMovementBean().getItemMovementKey()
                        .getRowidKey()));
                labelEvent.setWorkstationId(getIdCtx().getWorkstation());

                fBoningHelper.launchPrint(getView(), getModel().getIdentification(), getIdCtx(), labelEvent,
                        movementBBean);

            } else if (DELETE_BTN.equals(event.getModel().getReference())) {
                actionDelete();
            } else if (PRINT_BTN.equals(event.getModel().getReference())) {
                actionPrint();
            } else if (CRITERIA_BTN.equals(event.getModel().getReference())) {
                actionCriteria();
            } else if (BTN_OPEN_INCIDENT_REFERENCE.equals(event.getModel().getReference())) {
                actionOpenIncident();
            } else if (event.getModel().getReference().equals(PRINT_ROLL_LABEL_BTN)) {
                actionPrintRollLabel();
            }

        } catch (UIException e) {
            getView().showError(e.getLocalizedMessage());
        } catch (BusinessException e) {
            // The container is closed, do you want to reopen it?
            if (new BusinessException(new FatalException(MSG_OPEN_CONTAINER)).getMessage().equals(e.getMessage())) {
                if (MessageButtons.NO == getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                        I18nClientManager.translate(ProductionMo.T8718), MessageButtons.YES_NO)) {
                    getView().showError(
                            I18nClientManager.translate(Generic.T21381, false, new VarParamTranslation(null)));
                } else {
                    FBoningDataEntryListInputBBean bBean = (FBoningDataEntryListInputBBean) getBrowserController()
                            .getModel().getCurrentBean();
                    try {
                        getFdataEntryListCBS().deleteBean(getIdCtx(), convertBoningBBean(bBean), true);
                    } catch (BusinessException e1) {
                        getView().showError(e1.getLocalizedMessage());
                    } catch (BusinessErrorsException e1) {
                        ((FBoningDataEntryListInputVCtrl) getViewerController())
                                .showUIErrorsException(new UIErrorsException(e1));
                    }
                    getBrowserController().beanDeleted(new BeanUpdateEvent(this, BeanUpdateEvent.EVENT_DELETE, bBean));
                    recalculateSums();
                }
            } else {
                getView().showError(e.getLocalizedMessage());
            }
        } catch (BusinessErrorsException e) {
            ((FBoningDataEntryListInputVCtrl) getViewerController()).showUIErrorsException(new UIErrorsException(e));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * Gets the boningDataEntryListCBS.
     * 
     * @return the boningDataEntryListCBS.
     */
    public FBoningDataEntryListCBS getBoningDataEntryListCBS() {
        return boningDataEntryListCBS;
    }

    /**
     * getViewerCtrl.
     * 
     * @return the ctrl
     */
    public FBoningDataEntryListInputVCtrl getViewerCtrl() {
        return (FBoningDataEntryListInputVCtrl) getViewerController();
    }

    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        InputEntityType ientityType = (InputEntityType) getSharedContext().get(Domain.DOMAIN_PARENT, "inputEntityType");
        if (ientityType != null) {
            ((FBoningDataEntryListInputBCtrl) getBrowserController()).changeColumns(ientityType);
            entityType = ientityType.getClass();
        } else {
            OutputEntityType oentityType = (OutputEntityType) getSharedContext().get(Domain.DOMAIN_PARENT,
                    "outputEntityType");
            ((FBoningDataEntryListInputBCtrl) getBrowserController()).changeColumns(oentityType);
            entityType = oentityType.getClass();
        }
        super.initialize();

        getBrowserController().requestFocus();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Sets the boningDataEntryListCBS.
     * 
     * @param boningDataEntryListCBS boningDataEntryListCBS.
     */
    public void setBoningDataEntryListCBS(final FBoningDataEntryListCBS boningDataEntryListCBS) {
        this.boningDataEntryListCBS = boningDataEntryListCBS;
    }

    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lstBtn = new ArrayList<ToolBarBtnStdBaseModel>();
        lstBtn.add(getDelBtn());
        lstBtn.add(getPrintBtn());
        lstBtn.add(getPrintContainerBtn());
        lstBtn.add(getCriteriaBtn());

        if (entityType == OutputEntityType.class) {
            lstBtn.add(getPrintRollLabelBtn());
        }
        // Functionalities
        lstBtn.add(getIncidentBtn());

        // add shortcut
        getDelBtn().setShortcut(Key.K_F7);
        getPrintBtn().setShortcut(Key.K_P);
        getPrintContainerBtn().setShortcut(Key.K_Q);
        getCriteriaBtn().setShortcut(Key.K_C);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lstBtn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ToolBarBtnStdModel getPrintContainerBtn() {
        ToolBarBtnStdModel printContainerBtn = super.getPrintContainerBtn();
        printContainerBtn.setText(I18nClientManager.translate(ProductionMo.T34173, false)); // réédition acte
        return printContainerBtn;
    }

    /**
     * Gets the PrintRollLaelBtn.
     * 
     * @return the prints the upper container button
     */
    protected ToolBarBtnStdModel getPrintRollLabelBtn() {
        if (printRollLabelBtn == null) {
            printRollLabelBtn = new ToolBarBtnStdModel();
            printRollLabelBtn.setReference(PRINT_ROLL_LABEL_BTN);
            printRollLabelBtn.setText(I18nClientManager.translate(ProductionMo.T23257, false));
            printRollLabelBtn.setIconURL("/images/vif/vif57/production/mo/print.png");
        }
        return printRollLabelBtn;
    }

    /**
     * recalculates the Sums.
     * 
     */
    protected void recalculateSums() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - recalculateSums()");
        }

        List list = getBrowserController().getModel().getBeans();

        double total1 = 0;
        String u1 = "";

        double total2 = 0;
        String u2 = "";

        for (Object o : list) {
            FBoningDataEntryListInputBBean bBean = (FBoningDataEntryListInputBBean) o;

            if ("".equalsIgnoreCase(u1)) {
                u1 = bBean.getKeyboardQties().getFirstQty().getUnit();
            }
            if ("".equalsIgnoreCase(bBean.getKeyboardQties().getFirstQty().getUnit())
                    || !u1.equalsIgnoreCase(bBean.getKeyboardQties().getFirstQty().getUnit())) {
                total1 = -1;
            }
            if (total1 != -1) {
                total1 += bBean.getKeyboardQties().getFirstQty().getQty();
            }
            if ("".equalsIgnoreCase(u2)) {
                u2 = bBean.getKeyboardQties().getSecondQty().getUnit();
            }
            if ("".equalsIgnoreCase(bBean.getKeyboardQties().getSecondQty().getUnit())
                    || !u2.equalsIgnoreCase(bBean.getKeyboardQties().getSecondQty().getUnit())) {
                total2 = -1;
            }
            if (total2 != -1) {
                total2 += bBean.getKeyboardQties().getSecondQty().getQty();
            }
        }

        Unit unit1 = null;
        Unit unit2 = null;
        try {
            if (!"".equals(u1)) {
                unit1 = getFdataEntryListCBS().getUnit(getIdCtx(), u1.toUpperCase());
            }
            if (!"".equals(u2)) {
                unit2 = getFdataEntryListCBS().getUnit(getIdCtx(), u2.toUpperCase());
            }
            String v1 = (total1 == -1) ? "" : formatSum(total1, unit1);
            String v2 = (total2 == -1) ? "" : formatSum(total2, unit2);
            ((FDataEntryListFIView) getView()).setSums(v1, u1, v2, u2);
        } catch (BusinessException e) {
            showError("Calcul des sommes :" + e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - recalculateSums()");
        }
    }

    /**
     * Action criteria.
     * 
     * @throws BusinessException the business exception
     */
    private void actionCriteria() throws BusinessException {
        FBoningDataEntryListInputBBean bBean = (FBoningDataEntryListInputBBean) getBrowserController().getModel()
                .getCurrentBean();

        CriteriaReturnInitBean criteriaReturnInitBean = getFdataEntryListCBS().getCriteria(getIdCtx(),
                convertBoningBBean(bBean), "FABENT");

        if (criteriaReturnInitBean.getListCriteria().size() > 0) {
            CriteriaDBean criteriaDBean = new CriteriaDBean(criteriaReturnInitBean, bBean.getStockItem().getItemId());
            WorkShopCriteriaTools.showCriteria(getView(), this, this, getModel().getIdentification(), criteriaDBean);
        }
    }

    /**
     * Action delete.
     * 
     * @throws BusinessException the business exception
     * @throws BusinessErrorsException the business errors exception
     */
    private void actionDelete() throws BusinessException, BusinessErrorsException {
        int res = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                I18nClientManager.translate(ProductionMo.T29613, false), MessageButtons.YES_NO);
        if (res == MessageButtons.YES) {
            FBoningDataEntryListInputBBean currentBBean = (FBoningDataEntryListInputBBean) getBrowserController()
                    .getModel().getCurrentBean();

            getFdataEntryListCBS().deleteBean(getIdCtx(), convertBoningBBean(currentBBean), false);
            getBrowserController().beanDeleted(new BeanUpdateEvent(this, BeanUpdateEvent.EVENT_DELETE, currentBBean));
            recalculateSums();
        }

    }

    /**
     * Action open incident.
     */
    private void actionOpenIncident() {

        // Prepare parameters bean
        FNCFlowSharedContextBean parametersBean = new FNCFlowSharedContextBean();
        parametersBean.setType(NCType.INTERNAL.getKey());

        // Feed with viewer bean informations
        FBoningDataEntryListInputBBean bBean = (FBoningDataEntryListInputBBean) getViewerController().getBean();
        parametersBean.setActType(NCActType.MANUFACTURING_ORDER.getKey());
        parametersBean.setChronoOrigin(//
                new Chrono(//
                        bBean.getMovementAct().getPrechro(), //
                        bBean.getMovementAct().getChrono()));
        parametersBean.setContainerNumber(bBean.getEntityLocationKey().getContainerNumber());
        StockItem stockItem = new StockItem();
        stockItem.setItemId(bBean.getStockItem().getItemId());
        stockItem.setBatch(bBean.getStockItem().getBatch());
        stockItem.setQuantities(bBean.getStockItem().getQuantities());
        parametersBean.setStockItem(stockItem);

        // Put parameters in context and open feature
        getSharedContext().put(Domain.DOMAIN_THIS, NCConstants.NCFLOW_KEY_SHARED_CONTEXT, parametersBean);
        if (StockUITools.isTouchRunning()) {
            openChildFeature(NCConstants.FeatureName.FLOW_TOUCH.getName());
        } else {
            openChildFeature(NCConstants.FeatureName.FLOW.getName());
        }

    }

    /**
     * Action print.
     */
    private void actionPrint() {
        try {
            getFdataEntryListCBS().printMovement(
                    getIdCtx(),
                    convertBoningBBean((FBoningDataEntryListInputBBean) getBrowserController().getModel()
                            .getCurrentBean()));
        } catch (BusinessException e) {
            getView().show(new UIException(e));
        }
    }

    /**
     * Print the roll label.
     */
    private void actionPrintRollLabel() {
        try {
            getBoningDataEntryListCBS().printRollLabel(getIdCtx(),
                    (FDataEntryListBBean) getBrowserController().getModel().getCurrentBean(),
                    getIdCtx().getWorkstation());
            // getFdataEntryListCBS().printUpperContainer(getIdCtx(),
            // (FDataEntryListBBean) getBrowserController().getModel().getCurrentBean());
        } catch (BusinessException e) {
            getView().show(new UIException(e));
        }
    }

    /**
     * 
     * convert the current bean to use the same method of input/output production.
     * 
     * @param currentBBean a boning dataentrylist bbean
     * @return a production bbean.
     */
    private FDataEntryListBBean convertBoningBBean(final FBoningDataEntryListInputBBean currentBBean) {
        FDataEntryListBBean bbean = new FDataEntryListBBean();
        bbean.setEntityLocationKey(currentBBean.getEntityLocationKey());
        bbean.setFormattedCreationDate(currentBBean.getFormattedCreationDate());
        bbean.setFormattedCreationHour(currentBBean.getFormattedCreationHour());
        bbean.setFormattedFirstUnit(currentBBean.getFormattedFirstUnit());
        bbean.setFormattedMovementDate(currentBBean.getFormattedMovementDate());
        bbean.setFormattedSecondUnit(currentBBean.getFormattedSecondUnit());
        bbean.setKeyboardQties(currentBBean.getKeyboardQties());
        bbean.setMovementAct(currentBBean.getMovementAct());
        bbean.setSens(currentBBean.getSens());
        bbean.setStockItem(currentBBean.getStockItem());

        return bbean;
    }
}

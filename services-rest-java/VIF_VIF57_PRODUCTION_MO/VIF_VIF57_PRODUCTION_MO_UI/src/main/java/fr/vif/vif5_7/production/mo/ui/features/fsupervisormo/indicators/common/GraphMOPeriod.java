/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_COMMON
 * File : $RCSfile: GraphMOPeriod.java,v $
 * Created on 5 août 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * GanttDataPeriod to define a Gantt Data Period.
 * 
 * @author ag
 * @param <SRC> bean source of the period.
 */
public class GraphMOPeriod<SRC> implements Serializable {

    private List<String> annotations     = new ArrayList<String>();
    private SRC          beanSource      = null;
    private Date         end;
    private double       percentComplete = 0;
    private Date         start;

    /**
     * Constructor.
     */
    public GraphMOPeriod() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param start Date
     * @param end Date
     */
    public GraphMOPeriod(final Date start, final Date end) {
        super();
        this.start = start;
        this.end = end;
    }

    /**
     * Constructor with labels.
     * 
     * @param start Date
     * @param end Date
     * @param labels List<String>
     */
    private GraphMOPeriod(final Date start, final Date end, final List<String> labels) {
        super();
        this.start = start;
        this.end = end;
        this.annotations = labels;
    }

    /**
     * Gets the labels.
     * 
     * @return the labels.
     */
    public List<String> getAnnotations() {
        return annotations;
    }

    /**
     * Gets the beanSource.
     * 
     * @return the beanSource.
     */
    public SRC getBeanSource() {
        return beanSource;
    }

    /**
     * Gets the end.
     * 
     * @return the end.
     */
    public Date getEnd() {
        return end;
    }

    /**
     * Gets the percentComplete.
     * 
     * @return the percentComplete.
     */
    public double getPercentComplete() {
        return percentComplete;
    }

    /**
     * Gets the start.
     * 
     * @return the start.
     */
    public Date getStart() {
        return start;
    }

    /**
     * Sets the labels.
     * 
     * @param labels labels.
     */
    public void setAnnotations(final List<String> labels) {
        this.annotations = labels;
    }

    /**
     * Sets the beanSource.
     * 
     * @param beanSource beanSource.
     */
    public void setBeanSource(final SRC beanSource) {
        this.beanSource = beanSource;
    }

    /**
     * Sets the end.
     * 
     * @param end end.
     */
    public void setEnd(final Date end) {
        this.end = end;
    }

    /**
     * Sets the percentComplete.
     * 
     * @param percentComplete percentComplete.
     */
    public void setPercentComplete(final double percentComplete) {
        this.percentComplete = percentComplete;
    }

    /**
     * Sets the start.
     * 
     * @param start start.
     */
    public void setStart(final Date start) {
        this.start = start;
    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: EndDayParameterDialogView.java,v $
 * Created on 28 Avril 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.dialog.parameter;


import java.awt.Color;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.ui.dialogs.swing.StandardSwingDialog;
import fr.vif.jtech.ui.input.swing.SwingInputCheckBox;
import fr.vif.jtech.ui.input.swing.SwingInputComboBox;
import fr.vif.jtech.ui.input.swing.SwingInputRadioSet;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.admin.config.AdminConfig;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.features.farea.FAreaBBean;
import fr.vif.vif5_7.gen.location.ui.composites.carea.CAreaCtrl;
import fr.vif.vif5_7.gen.location.ui.composites.carea.swing.CAreaView;


/**
 * EndDayParameter Dialog view.
 *
 * @author ac
 */
public class EndDayParameterDialogView extends StandardSwingDialog implements ActionListener {

    private static final long   serialVersionUID  = 1L;
    private JPanel              jContentPane      = null;

    private CAreaView           cArea             = null;
    // For Area
    private JLabel              lbArea            = null;

    // Choice between production Date and Time Window
    private JLabel              lbDateContext     = null;

    private SwingInputRadioSet  sirDateContext    = null;

    // For production date context
    private JLabel              lbProductionDate  = null;

    private SwingInputComboBox  sicProductionDate = null;
    private SwingInputTextField sitProductionDate = null;
    // For time window context
    private JLabel              lbBeginDate       = null;
    private SwingInputComboBox  sicBeginDate      = null;

    private SwingInputTextField sitBeginDate      = null;

    private JLabel              lbBeginTime       = null;

    private SwingInputTextField sitBeginTime      = null;
    private JLabel              lbEndDate         = null;
    private SwingInputComboBox  sicEndDate        = null;
    private SwingInputTextField sitEndDate        = null;
    private JLabel              lbEndTime         = null;

    private SwingInputTextField sitEndTime        = null;
    // For alerts to show
    private JLabel              lbAlerts          = null;
    private SwingInputCheckBox  sicMaterial       = null;
    private SwingInputCheckBox  sicWorkingPeriod  = null;

    private SwingInputCheckBox  sicTrs            = null;

    private SwingInputCheckBox  sicOnLineControl  = null;

    /**
     * ctor.
     * 
     * @param view Frame
     */
    public EndDayParameterDialogView(final Dialog view) {
        super(view);
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param view Frame
     */
    public EndDayParameterDialogView(final Frame view) {
        super(view);
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        if (e.getSource().equals(getSicProductionDate().getJComboBox())) {
            switch (getSicProductionDate().getValue().toString()) {
                case EndDayParameterDialogBean.PERSONALIZED:
                    getSitProductionDate().setValue("");
                    break;
                case EndDayParameterDialogBean.TODAY:
                    getSitProductionDate().setValue(new Date());
                    break;
                case EndDayParameterDialogBean.YESTERDAY:
                    getSitProductionDate().setValue(DateHelper.addDays(new Date(), -1));

                    break;

                default:
                    break;
            }

        }
        if (e.getSource().equals(getSicBeginDate().getJComboBox())) {
            switch (getSicBeginDate().getValue().toString()) {
                case EndDayParameterDialogBean.PERSONALIZED:
                    getSitBeginDate().setValue("");
                    break;
                case EndDayParameterDialogBean.TODAY:
                    getSitBeginDate().setValue(new Date());
                    break;
                case EndDayParameterDialogBean.YESTERDAY:
                    getSitBeginDate().setValue(DateHelper.addDays(new Date(), -1));
                    break;

                default:
                    break;
            }

        }
        if (e.getSource().equals(getSicEndDate().getJComboBox())) {
            switch (getSicEndDate().getValue().toString()) {
                case EndDayParameterDialogBean.PERSONALIZED:
                    getSitEndDate().setValue("");
                    break;
                case EndDayParameterDialogBean.TODAY:
                    getSitEndDate().setValue(new Date());
                    break;
                case EndDayParameterDialogBean.YESTERDAY:
                    getSitEndDate().setValue(DateHelper.addDays(new Date(), -1));
                    break;

                default:
                    break;
            }

        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableComponents() {
        super.enableComponents();
        EndDayParameterDialogBean dialogBean = (EndDayParameterDialogBean) getModel().getDialogBean();

        // Date context selection
        if (dialogBean.getIsTimeWindowContext()) {
            getSicBeginDate().setEnabled(true);
            getSicEndDate().setEnabled(true);
            getSitBeginDate().setEnabled(true);
            getSitBeginTime().setEnabled(true);
            getSitEndDate().setEnabled(true);
            getSitEndTime().setEnabled(true);
            getSicProductionDate().setEnabled(false);
            getSitProductionDate().setEnabled(false);
            if (EndDayParameterDialogBean.TODAY.equals(getSicBeginDate().getValue())) {
                getSitBeginDate().setEnabled(false);
            }
            if (EndDayParameterDialogBean.YESTERDAY.equals(getSicBeginDate().getValue())) {
                getSitBeginDate().setEnabled(false);
            }
            if (EndDayParameterDialogBean.TODAY.equals(getSicEndDate().getValue())) {
                getSitEndDate().setEnabled(false);
            }
            if (EndDayParameterDialogBean.YESTERDAY.equals(getSicEndDate().getValue())) {
                getSitEndDate().setEnabled(false);
            }

        } else {
            getSicBeginDate().setEnabled(false);
            getSicEndDate().setEnabled(false);
            getSitBeginDate().setEnabled(false);
            getSitBeginTime().setEnabled(false);
            getSitEndDate().setEnabled(false);
            getSitEndTime().setEnabled(false);
            getSicProductionDate().setEnabled(true);
            getSitProductionDate().setEnabled(true);
            if (EndDayParameterDialogBean.TODAY.equals(getSicProductionDate().getValue())) {
                getSitProductionDate().setEnabled(false);
            }
            if (EndDayParameterDialogBean.YESTERDAY.equals(getSicProductionDate().getValue())) {
                getSitProductionDate().setEnabled(false);
            }
        }

    }

    /**
     * Gets the cArea.
     *
     * @return the cArea.
     */
    public CAreaView getcArea() {
        if (cArea == null) {
            cArea = new CAreaView(CAreaCtrl.class, getLbArea());
            cArea.setBeanProperty("area");
            cArea.setCodeUseFieldHelp(true);
            cArea.setCodeHelpInfos(FAreaBBean.class, "code", "label");
        }
        return cArea;
    }

    /**
     * Gets the sicBeginDate.
     *
     * @return the sicBeginDate.
     */
    public SwingInputComboBox getSicBeginDate() {
        if (sicBeginDate == null) {
            sicBeginDate = new SwingInputComboBox();
            sicBeginDate.addItem(EndDayParameterDialogBean.PERSONALIZED,
                    I18nClientManager.translate(ProductionMo.T40704, false));
            sicBeginDate.addItem(EndDayParameterDialogBean.TODAY,
                    I18nClientManager.translate(ProductionMo.T40701, false));
            sicBeginDate.addItem(EndDayParameterDialogBean.YESTERDAY,
                    I18nClientManager.translate(GenKernel.T20032, false));
            sicBeginDate.setBeanProperty("twBeginDateChoice");
            sicBeginDate.addActionListener(this);

        }
        return sicBeginDate;
    }

    /**
     * Gets the sicEndDate.
     *
     * @return the sicEndDate.
     */
    public SwingInputComboBox getSicEndDate() {
        if (sicEndDate == null) {
            sicEndDate = new SwingInputComboBox();
            sicEndDate.addItem(EndDayParameterDialogBean.PERSONALIZED,
                    I18nClientManager.translate(ProductionMo.T40704, false));
            sicEndDate
                    .addItem(EndDayParameterDialogBean.TODAY, I18nClientManager.translate(ProductionMo.T40701, false));
            sicEndDate.addItem(EndDayParameterDialogBean.YESTERDAY,
                    I18nClientManager.translate(GenKernel.T20032, false));
            sicEndDate.setBeanProperty("twEndDateChoice");
            sicEndDate.addActionListener(this);

        }
        return sicEndDate;
    }

    /**
     * Gets the sicMaterial.
     *
     * @return the sicMaterial.
     */
    public SwingInputCheckBox getSicMaterial() {
        if (sicMaterial == null) {
            sicMaterial = new SwingInputCheckBox();
            sicMaterial.setBeanBased(true);
            sicMaterial.setBeanProperty("material");
            sicMaterial.getJCheckBox().setText(I18nClientManager.translate(GenKernel.T29106, false));
        }
        return sicMaterial;
    }

    /**
     * Gets the sicOnLineControl.
     *
     * @return the sicOnLineControl.
     */
    public SwingInputCheckBox getSicOnLineControl() {
        if (sicOnLineControl == null) {
            sicOnLineControl = new SwingInputCheckBox();
            sicOnLineControl.setBeanBased(true);
            sicOnLineControl.setBeanProperty("onLineControl");
            sicOnLineControl.getJCheckBox().setText(I18nClientManager.translate(ProductionMo.T40703, false));
        }
        return sicOnLineControl;
    }

    /**
     * Gets the sicProductionDate.
     *
     * @return the sicProductionDate.
     */
    public SwingInputComboBox getSicProductionDate() {
        if (sicProductionDate == null) {
            sicProductionDate = new SwingInputComboBox();
            sicProductionDate.addItem(EndDayParameterDialogBean.PERSONALIZED,
                    I18nClientManager.translate(ProductionMo.T40704, false));
            sicProductionDate.addItem(EndDayParameterDialogBean.TODAY,
                    I18nClientManager.translate(ProductionMo.T40701, false));
            sicProductionDate.addItem(EndDayParameterDialogBean.YESTERDAY,
                    I18nClientManager.translate(GenKernel.T20032, false));
            sicProductionDate.setBeanProperty("productionDateChoice");
            sicProductionDate.getJComboBox().addActionListener(this);

        }
        return sicProductionDate;
    }

    /**
     * Gets the sicTrs.
     *
     * @return the sicTrs.
     */
    public SwingInputCheckBox getSicTrs() {
        if (sicTrs == null) {
            sicTrs = new SwingInputCheckBox();
            sicTrs.setBeanBased(true);
            sicTrs.setBeanProperty("trs");
            sicTrs.getJCheckBox().setText(I18nClientManager.translate(ProductionMo.T40702, false));
        }
        return sicTrs;
    }

    /**
     * Gets the sicWorkingPeriod.
     *
     * @return the sicWorkingPeriod.
     */
    public SwingInputCheckBox getSicWorkingPeriod() {
        if (sicWorkingPeriod == null) {
            sicWorkingPeriod = new SwingInputCheckBox();
            sicWorkingPeriod.setBeanBased(true);
            sicWorkingPeriod.setBeanProperty("workingPeriod");
            sicWorkingPeriod.getJCheckBox().setText(I18nClientManager.translate(ProductionMo.T36171, false));
        }
        return sicWorkingPeriod;
    }

    /**
     * Gets the sirDateContext.
     *
     * @return the sirDateContext.
     */
    public SwingInputRadioSet getSirDateContext() {
        if (sirDateContext == null) {
            sirDateContext = new SwingInputRadioSet();
            sirDateContext.setBeanProperty("isTimeWindowContext");
            sirDateContext.setMaxRowNumber(2);
            sirDateContext.addItem(true, I18nClientManager.translate(ProductionMo.T40707, false) + "", 1);
            sirDateContext.addItem(false, I18nClientManager.translate(ProductionMo.T34329, false) + "", 0);

        }

        return sirDateContext;
    }

    /**
     * Gets the sitBeginDate.
     *
     * @return the sitBeginDate.
     */
    public SwingInputTextField getSitBeginDate() {
        if (sitBeginDate == null) {
            sitBeginDate = new SwingInputTextField(Date.class, "dd/MM/yyyy", false, getLbBeginDate());
            sitBeginDate.setBeanBased(true);
            sitBeginDate.setBeanProperty("twBeginDate");
            sitBeginDate.setMandatory(false);
            sitBeginDate.setDescription("\"" + I18nClientManager.translate(AdminConfig.T32894, false) + "\"");
        }
        return sitBeginDate;
    }

    /**
     * Gets the sitBeginTime.
     *
     * @return the sitBeginTime.
     */
    public SwingInputTextField getSitBeginTime() {
        if (sitBeginTime == null) {
            sitBeginTime = new SwingInputTextField(Date.class, "HH:mm", false, null);
            sitBeginTime.setBeanBased(true);
            sitBeginTime.setBeanProperty("twBeginTime");
            sitBeginTime.setMandatory(false);
            sitBeginTime.setDescription("\"" + I18nClientManager.translate(Generic.T5019, false) + "\"");
        }
        return sitBeginTime;
    }

    /**
     * Gets the sitEndDate.
     *
     * @return the sitEndDate.
     */
    public SwingInputTextField getSitEndDate() {
        if (sitEndDate == null) {
            sitEndDate = new SwingInputTextField(Date.class, "dd/MM/yyyy", false, getLbEndDate());
            sitEndDate.setBeanBased(true);
            sitEndDate.setBeanProperty("twEndDate");
            sitEndDate.setMandatory(false);
            sitEndDate.setDescription("\"" + I18nClientManager.translate(AdminConfig.T32894, false) + "\"");
        }
        return sitEndDate;
    }

    /**
     * Gets the sitEndTime.
     *
     * @return the sitEndTime.
     */
    public SwingInputTextField getSitEndTime() {
        if (sitEndTime == null) {
            sitEndTime = new SwingInputTextField(Date.class, "HH:mm", true, null);
            sitEndTime.setBeanBased(true);
            sitEndTime.setBeanProperty("twEndTime");
            sitEndTime.setMandatory(false);
            sitEndTime.setDescription("\"" + I18nClientManager.translate(Generic.T5019, false) + "\"");
        }
        return sitEndTime;
    }

    /**
     * Retrieve Creation date swing component.
     * 
     * @return a swingInputTexteField
     */
    public SwingInputTextField getSitProductionDate() {
        if (sitProductionDate == null) {
            sitProductionDate = new SwingInputTextField(Date.class, "dd/MM/yyyy", true, getLbProductionDate());
            sitProductionDate.setBeanBased(true);
            sitProductionDate.setBeanProperty("productionDate");
            sitProductionDate.setMandatory(false);
            sitProductionDate.setDescription("\"" + I18nClientManager.translate(AdminConfig.T32894, false) + "\"");
        }
        return sitProductionDate;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {

        super.initializeView();

        JPanel buttonsPanel = (JPanel) getOkButton().getParent();
        GridBagLayout layout = ((GridBagLayout) buttonsPanel.getLayout());

        // CHECKSTYLE:OFF
        GridBagConstraints helpButtonConstraint = layout.getConstraints(getHelpButton());
        helpButtonConstraint.gridx = 3;
        layout.setConstraints(getHelpButton(), helpButtonConstraint);

        GridBagConstraints cancelButtonConstraint = layout.getConstraints(getCancelButton());
        cancelButtonConstraint.gridx = 2;
        layout.setConstraints(getCancelButton(), cancelButtonConstraint);

        GridBagConstraints okButtonConstraint = layout.getConstraints(getOkButton());
        okButtonConstraint.gridx = 1;
        okButtonConstraint.insets = new java.awt.Insets(5, 20, 20, 10);
        okButtonConstraint.weightx = 0.0010D;
        layout.setConstraints(getOkButton(), okButtonConstraint);

        GridBagConstraints buttonConstraints = new GridBagConstraints();
        buttonConstraints.gridx = 0;
        buttonConstraints.gridy = 2;
        buttonConstraints.anchor = java.awt.GridBagConstraints.SOUTHEAST;
        buttonConstraints.insets = new java.awt.Insets(5, 20, 20, 0);
        buttonConstraints.fill = java.awt.GridBagConstraints.NONE;
        buttonConstraints.weightx = 1.0D;
        buttonConstraints.weighty = 0.0;
        // CHECKSTYLE:ON

    }

    /**
     * This method initializes jContentPane.
     * 
     * @return javax.swing.JPanel
     */
    private JPanel getJContentPane() {

        if (jContentPane == null) {
            jContentPane = new JPanel();
            jContentPane.setLayout(new GridBagLayout());

            // Area

            // CHECKSTYLE:OFF

            GridBagConstraints gbclbArea = new GridBagConstraints();
            gbclbArea.insets = new Insets(5, 0, 0, 0);
            gbclbArea.gridx = 0;
            gbclbArea.gridy = 0;
            jContentPane.add(getLbArea(), gbclbArea);

            GridBagConstraints gbcCArea = new GridBagConstraints();
            gbcCArea.fill = GridBagConstraints.HORIZONTAL;
            gbcCArea.gridx = 1;
            gbcCArea.gridy = 0;
            gbcCArea.gridwidth = GridBagConstraints.RELATIVE;
            jContentPane.add(getcArea(), gbcCArea);

            // Date context
            GridBagConstraints gbcDateContext = new GridBagConstraints();
            gbcDateContext.insets = new Insets(5, 0, 0, 0);
            gbcDateContext.gridx = 0;
            gbcDateContext.gridy = 3;
            gbcDateContext.gridheight = 2;
            jContentPane.add(getSirDateContext(), gbcDateContext);

            GridBagConstraints gbcSicProductionDate = new GridBagConstraints();
            gbcSicProductionDate.fill = GridBagConstraints.HORIZONTAL;
            gbcSicProductionDate.insets = new Insets(5, 0, 0, 0);
            gbcSicProductionDate.gridx = 1;
            gbcSicProductionDate.gridy = 3;
            jContentPane.add(getSicProductionDate(), gbcSicProductionDate);

            GridBagConstraints gbcSitProductionDate = new GridBagConstraints();
            gbcSitProductionDate.insets = new Insets(5, 0, 0, 0);
            gbcSitProductionDate.fill = GridBagConstraints.HORIZONTAL;
            gbcSitProductionDate.gridx = 2;
            gbcSitProductionDate.gridy = 3;
            jContentPane.add(getSitProductionDate(), gbcSitProductionDate);

            GridBagConstraints gbcSicBeginDate = new GridBagConstraints();
            gbcSicBeginDate.fill = GridBagConstraints.HORIZONTAL;
            gbcSicBeginDate.anchor = GridBagConstraints.BASELINE;
            gbcSicBeginDate.insets = new Insets(5, 0, 0, 0);
            gbcSicBeginDate.gridx = 1;
            gbcSicBeginDate.gridy = 4;
            jContentPane.add(getSicBeginDate(), gbcSicBeginDate);

            GridBagConstraints gbcSitBeginDate = new GridBagConstraints();
            gbcSitBeginDate.insets = new Insets(5, 0, 0, 0);
            gbcSitBeginDate.fill = GridBagConstraints.HORIZONTAL;
            gbcSitBeginDate.gridx = 2;
            gbcSitBeginDate.gridy = 4;
            jContentPane.add(getSitBeginDate(), gbcSitBeginDate);

            GridBagConstraints gbcSitBeginTime = new GridBagConstraints();
            gbcSitBeginTime.fill = GridBagConstraints.HORIZONTAL;
            gbcSitBeginTime.insets = new Insets(5, 0, 0, 0);
            gbcSitBeginTime.gridx = 3;
            gbcSitBeginTime.gridy = 4;
            jContentPane.add(getSitBeginTime(), gbcSitBeginTime);

            GridBagConstraints gbcSicEndDate = new GridBagConstraints();
            gbcSicEndDate.fill = GridBagConstraints.HORIZONTAL;
            gbcSicEndDate.anchor = GridBagConstraints.LINE_START;

            gbcSicEndDate.insets = new Insets(5, 0, 0, 0);
            gbcSicEndDate.gridx = 1;
            gbcSicEndDate.gridy = 5;
            gbcSicEndDate.weightx = 0.2;
            jContentPane.add(getSicEndDate(), gbcSicEndDate);

            GridBagConstraints gbcSitEndDate = new GridBagConstraints();
            gbcSitEndDate.insets = new Insets(5, 0, 0, 0);
            gbcSitEndDate.fill = GridBagConstraints.HORIZONTAL;
            gbcSitEndDate.gridx = 2;
            gbcSitEndDate.gridy = 5;
            jContentPane.add(getSitEndDate(), gbcSitEndDate);

            GridBagConstraints gbcSitEndTime = new GridBagConstraints();
            gbcSitEndTime.fill = GridBagConstraints.HORIZONTAL;
            gbcSitEndTime.insets = new Insets(5, 0, 0, 0);
            gbcSitEndTime.gridx = 3;
            gbcSitEndTime.gridy = 5;
            jContentPane.add(getSitEndTime(), gbcSitEndTime);

            // Alerts
            GridBagConstraints gbclbAlerts = new GridBagConstraints();
            gbclbAlerts.insets = new Insets(5, 0, 0, 0);
            gbclbAlerts.weightx = 0;
            gbclbAlerts.gridx = 0;
            gbclbAlerts.gridy = 6;
            jContentPane.add(getLbAlerts(), gbclbAlerts);
            //
            GridBagConstraints gbccbMaterial = new GridBagConstraints();
            gbccbMaterial.weightx = 0.0;
            gbccbMaterial.anchor = GridBagConstraints.WEST;
            gbccbMaterial.insets = new Insets(0, 0, 0, 5);
            gbccbMaterial.gridx = 1;
            gbccbMaterial.gridy = 6;
            jContentPane.add(getSicMaterial(), gbccbMaterial);

            GridBagConstraints gbccbWorkingPeriod = new GridBagConstraints();
            gbccbWorkingPeriod.weightx = 0.0;
            gbccbWorkingPeriod.anchor = GridBagConstraints.WEST;
            gbccbWorkingPeriod.insets = new Insets(0, 0, 0, 5);
            gbccbWorkingPeriod.gridx = 2;
            gbccbWorkingPeriod.gridy = 6;
            jContentPane.add(getSicWorkingPeriod(), gbccbWorkingPeriod);

            GridBagConstraints gbccbTrs = new GridBagConstraints();
            gbccbTrs.weightx = 0.0;
            gbccbTrs.anchor = GridBagConstraints.WEST;
            gbccbTrs.insets = new Insets(0, 0, 0, 5);
            gbccbTrs.gridx = 1;
            gbccbTrs.gridy = 7;
            jContentPane.add(getSicTrs(), gbccbTrs);

            GridBagConstraints gbccbOnLineControl = new GridBagConstraints();
            gbccbOnLineControl.weightx = 0.0;
            gbccbOnLineControl.anchor = GridBagConstraints.WEST;
            gbccbOnLineControl.insets = new Insets(0, 0, 0, 5);
            gbccbOnLineControl.gridx = 2;
            gbccbOnLineControl.gridy = 7;
            jContentPane.add(getSicOnLineControl(), gbccbOnLineControl);

            // CHECKSTYLE:ON

        }
        return jContentPane;
    }

    /**
     * Gets the lbAlerts.
     *
     * @return the lbAlerts.
     */
    private JLabel getLbAlerts() {
        if (lbAlerts == null) {
            lbAlerts = new JLabel();
            lbAlerts.setOpaque(false);
            lbAlerts.setText(I18nClientManager.translate(ProductionMo.T40706, false) + "");
        }
        return lbAlerts;
    }

    /**
     * Gets the lbArea.
     *
     * @return the lbArea.
     */
    private JLabel getLbArea() {
        if (lbArea == null) {
            lbArea = new JLabel();
            lbArea.setOpaque(false);
            lbArea.setText(I18nClientManager.translate(ProductionMo.T40696, false));
        }
        return lbArea;
    }

    /**
     * Gets the lbBeginDate.
     *
     * @return the lbBeginDate.
     */
    private JLabel getLbBeginDate() {
        if (lbBeginDate == null) {
            lbBeginDate = new JLabel();
            lbBeginDate.setOpaque(true);
            lbBeginDate.setText("Du :");
            lbBeginDate.setBackground(Color.CYAN);

        }
        return lbBeginDate;
    }

    /**
     * Gets the lbBeginTime.
     *
     * @return the lbBeginTime.
     */
    private JLabel getLbBeginTime() {
        if (lbAlerts == null) {
            lbAlerts = new JLabel();
            lbAlerts.setOpaque(false);
            lbAlerts.setText(" heure");
        }
        return lbAlerts;
    }

    /**
     * Gets the lbDateContext.
     *
     * @return the lbDateContext.
     */
    private JLabel getLbDateContext() {
        return lbDateContext;
    }

    /**
     * Gets the lbEndDate.
     *
     * @return the lbEndDate.
     */
    private JLabel getLbEndDate() {
        if (lbEndDate == null) {
            lbEndDate = new JLabel();
            lbEndDate.setOpaque(false);
            lbEndDate.setText("Au : ");
        }
        return lbEndDate;
    }

    /**
     * Gets the lbEndTime.
     *
     * @return the lbEndTime.
     */
    private JLabel getLbEndTime() {
        if (lbEndTime == null) {
            lbEndTime = new JLabel();
            lbEndTime.setOpaque(false);
            lbEndTime.setText(I18nClientManager.translate(AdminConfig.T32894, false) + "");
        }
        return lbEndTime;
    }

    /**
     * Gets the lbProductionDate.
     *
     * @return the lbProductionDate.
     */
    private JLabel getLbProductionDate() {
        if (lbProductionDate == null) {
            lbProductionDate = new JLabel();
            lbProductionDate.setOpaque(false);
            lbProductionDate.setText("  ");
        }
        return lbProductionDate;
    }

    /**
     * Initializes view.
     */
    private void initialize() {
        // CHECKSTYLE:OFF

        setPreferredSize(new Dimension(600, 280));
        setMinimumSize(new Dimension(600, 280));
        setResizable(true);

        // CHECKSTYLE:ON

        addPanel(getJContentPane());
    }

}

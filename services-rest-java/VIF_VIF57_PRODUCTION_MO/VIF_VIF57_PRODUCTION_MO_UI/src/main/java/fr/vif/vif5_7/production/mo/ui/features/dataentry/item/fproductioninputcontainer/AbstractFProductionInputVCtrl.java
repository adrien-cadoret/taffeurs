/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFProductionInputVCtrl.java,v $
 * Created on 25 févr. 2016 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer;


import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.util.validator.VIFBindException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.i18n.ITransParam;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.constant.EntityEnum.EntityUnitType;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.ProducedEntity;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.UniqueItem;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityLocationKey;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityUnitInfos;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.Quantities;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.gen.packaging.business.beans.common.packaging.PackagingProperties;
import fr.vif.vif5_7.gen.packaging.business.beans.common.tattooed.TattooedKey;
import fr.vif.vif5_7.gen.packaging.business.beans.dialog.DPackagingBean;
import fr.vif.vif5_7.gen.packaging.business.beans.features.fpackaging.FPackagingSBean;
import fr.vif.vif5_7.gen.packaging.ui.dialog.DPackagingCtrl;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputBarcodeType;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.ItemMovementKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.dialog.printdocument.DPrintDocumentBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerWorkBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.BatchHelpType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.TouchDeclarationType;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.dialog.printdocument.DPrintDocumentCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.AbstractFProductionFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.AbstractFProductionVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionHelper;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;
import fr.vif.vif5_7.stock.entity.business.beans.dialog.content.DContainerContentBean;
import fr.vif.vif5_7.stock.entity.business.beans.dialog.content.DContentItems;
import fr.vif.vif5_7.stock.entity.business.beans.dialog.producedentity.DProducedEntityBean;
import fr.vif.vif5_7.stock.entity.ui.dialog.producedentity.DProducedEntityCtrl;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingSBean;


/**
 * The production input feature viewer controller abstract class.
 *
 * @author cj
 * @param <V> the CBS extends FProductionInputContainerCBS
 * @param <C> the viewer bean extends FProductionInputContainerVBean
 */
@SuppressWarnings("unchecked")
public abstract class AbstractFProductionInputVCtrl<V extends FProductionInputContainerVBean, C extends FProductionInputContainerCBS>
extends AbstractFProductionVCtrl<V> implements DialogChangeListener {
    /** LOGGER. */
    private static final Logger               LOGGER                  = Logger.getLogger(AbstractFProductionInputVCtrl.class);

    private boolean                           batchChecked            = false;

    private FProductionBCtrl                  browserCtrl             = null;

    private boolean                           criteriaChecked         = false;

    private FeatureView                       featureView;

    private boolean                           flashQuantites          = true;
    private boolean                           outputAvailable         = false;

    private MovementAct                       previous                = null;

    private ItemMovementKey                   previousItemMovementKey = null;

    private boolean                           scaleWeightConnected    = false;

    private boolean                           showAll                 = false;

    private boolean                           stockChecked            = false;

    private FProductionInputContainerWorkBean workBean                = new FProductionInputContainerWorkBean();

    private C                                 fproductionInputContainerCBS;

    private AbstractFProductionInputFCtrl     featureCtrl             = null;

    /**
     * Update ui data after validation.
     */
    public void afterValidation() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - afterValidation()");
        }

        printDocuments(getBean().getDocuments());
        getBean().setForceUpdate(false);
        getBean().setAutoInsert(false);
        getBean().setCriteriaWindowOpened(false);
        if (!getWorkBean().getShowFinished() && getBean().getToFinish()) {
            getModel().setInitialBean(getBean()); // To prevent JTech dialog about modified bean
            browserCtrl.reopenQuery();
        } else {
            showUIErrorsException();
            fireButtonStatechanged();
            getFeatureCtrl().manageFinishResumeButton(getBean().getBBean());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - afterValidation()");
        }
    }

    /**
     * Open a dialog to ask for a produced entity.
     */
    public void askProducedEntity() {

        // Dialog bean
        DProducedEntityBean dialogBean = new DProducedEntityBean();
        dialogBean.setEstablishmentKey(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getEstablishmentKey());
        dialogBean.setProducedEntity(new ProducedEntity(//
                new EntityLocationKey(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                        .getEstablishmentKey()), //
                        new UniqueItem()));

        DProducedEntityCtrl.showUniqueEntity(this.getFeatureCtrl().getView(), this, this, this.getModel()
                .getIdentification(), dialogBean, I18nClientManager.translate(ProductionMo.T17191));
    }

    /**
     * Open a dialog to ask for a tattooed number.
     */
    public void askTattooedNumber() {

        // Dialog bean
        DPackagingBean dialogBean = new DPackagingBean();
        dialogBean.setKey(new TattooedKey(//
                getBean().getBBean().getOperationItemBean().getOperationItemKey().getEstablishmentKey().getCsoc(), //
                getBean().getBBean().getOperationItemBean().getOperationItemKey().getEstablishmentKey().getCetab(), //
                getWorkBean().getInputWorkstationParameters().getDefaultTattoedPackaging(), //
                ""));
        dialogBean.setPackagingCL(new CodeLabel(getWorkBean().getInputWorkstationParameters()
                .getDefaultTattoedPackaging(), null));
        dialogBean.setProperties(new PackagingProperties());
        dialogBean.getProperties().setTtat(true);
        dialogBean.setIsPackagingEnabled(true);

        // Selection
        FPackagingSBean selection = new FPackagingSBean(getBean().getBBean().getOperationItemBean()
                .getOperationItemKey().getEstablishmentKey().getCsoc());
        selection.setTattooedPackagingOnly(true);
        selection.setNotTattooedPackagingOnly(false);
        selection.setOnlyAvailableTattooedNumber(false);
        selection.setSuperiorPackagingOnly(false);

        DPackagingCtrl.showPackaging(
                this.getFeatureCtrl().getView(),
                this,
                this,
                this.getModel().getIdentification(),
                dialogBean,
                selection,
                I18nClientManager.translate(StockKernel.T1588) + " ("
                        + I18nClientManager.translate(StockKernel.T17646, false) + ")");
    }

    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogCancelled(event=" + event + ")");
        }

        if (event.getDialogBean() instanceof CriteriaDBean) {
            // nothing to do
        } else if (event.getDialogBean() instanceof DContentItems) {
            // nothing to do
        }
        emptyVBean();
        getModel().setInitialBean(getBean());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogCancelled(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }

        if (event.getDialogBean() instanceof CriteriaDBean) {
            CriteriaDBean criteriaDBean = (CriteriaDBean) event.getDialogBean();
            getModel().getBean().setBeanCriteria(criteriaDBean.getCriteriaReturnInitBean());
            getModel().getBean().getBeanCriteria().setAutoOpening(false);
            getModel().getBean().setCriteriaWindowOpened(true);
        } else if (event.getDialogBean() instanceof DContainerContentBean) {
            try {
                if (!getBean().getOneItemContainer()) {
                    OperationItemKey oik = getFproductionInputContainerCBS().getOperationItemKeyToReposit(
                            getIdCtx(),
                            ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem()
                            .getItemId(), getWorkBean().getMoKey(), getWorkBean().getLogicalWorkstationId());
                    if (!getBean().getBBean().getOperationItemBean().getOperationItemKey().equals(oik)) {
                        CEntityBean entity = getBean().getEntity();
                        getModel().setInitialBean(getBean());
                        getBrowserCtrl().repositionToOperationItemKey(oik);
                        getModel().getBean().setEntity(entity);
                    }
                }
                getModel()
                .getBean()
                .getEntity()
                .getStockItemBean()
                .setStockItem(
                        ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem());
                getModel().getBean().getEntity().getStockItemBean().getGeneralItemInfos()
                .setCCompany(getIdCtx().getCompany());
                getModel()
                .getBean()
                .getEntity()
                .getStockItemBean()
                .getGeneralItemInfos()
                .getItemCL()
                .setCode(
                        ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem()
                        .getItemId());
                if (InputBarcodeType.KEEP_QTIES.equals(getWorkBean().getBarcodeType())) {
                    getModel()
                    .getBean()
                    .getEntity()
                    .getStockItemBean()
                    .getKeyboardQties()
                    .setFirstQty(
                            ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem()
                            .getQuantities().getFirstQty());
                    getModel()
                    .getBean()
                    .getEntity()
                    .getStockItemBean()
                    .getKeyboardQties()
                    .setSecondQty(
                            ((DContainerContentBean) event.getDialogBean()).getSelectedValue().getUniqueItem()
                            .getQuantities().getSecondQty());
                }
                getView().render();

                setBeanAndInitialBean((V) getFproductionInputContainerCBS().initializeEntity(getIdCtx(), getBean(),
                        getWorkBean()));
                validateBean(true, FProductionInputContainerVBeanEnum.BATCH_ID.getValue(), getBean().getEntity()
                        .getStockItemBean().getStockItem().getBatch());
            } catch (UIException e) {
                getView().show(e);
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            } catch (BusinessErrorsException e) {
                fireErrorsChanged(new UIErrorsException(e));
            }

        } else if (event.getDialogBean() instanceof DProducedEntityBean) {
            try {
                getModel().getBean().getEntity().getStockItemBean()
                .setStockItem(((DProducedEntityBean) event.getDialogBean()).getProducedEntity().getStockItem());
                getModel().getBean().getEntity().getStockItemBean().getGeneralItemInfos()
                .setCCompany(getIdCtx().getCompany());
                getModel()
                .getBean()
                .getEntity()
                .getStockItemBean()
                .getStockItem()
                .setUniqueNumber(
                        ((DProducedEntityBean) event.getDialogBean()).getProducedEntity().getStockItem()
                        .getUniqueNumber());
                getModel()
                .getBean()
                .getEntity()
                .getStockItemBean()
                .getGeneralItemInfos()
                .getItemCL()
                .setCode(
                        ((DProducedEntityBean) event.getDialogBean()).getProducedEntity().getStockItem()
                        .getItemId());
                getModel()
                .getBean()
                .getEntity()
                .getStockItemBean()
                .getKeyboardQties()
                .setFirstQty(
                        ((DProducedEntityBean) event.getDialogBean()).getProducedEntity().getStockItem()
                        .getQuantities().getFirstQty());
                getModel()
                .getBean()
                .getEntity()
                .getStockItemBean()
                .getKeyboardQties()
                .setSecondQty(
                        ((DProducedEntityBean) event.getDialogBean()).getProducedEntity().getStockItem()
                        .getQuantities().getSecondQty());

                getModel()
                .getBean()
                .getEntity()
                .getEntityLocationBean()
                .setContainerState(((DProducedEntityBean) event.getDialogBean()).getProducedEntity().getState());

                getView().render();
                setBeanAndInitialBean((V) getFproductionInputContainerCBS().initializeEntity(getIdCtx(), getBean(),
                        getWorkBean()));
                validateBean(true, FProductionInputContainerVBeanEnum.BATCH_ID.getValue(), getBean().getEntity()
                        .getStockItemBean().getStockItem().getBatch());
            } catch (UIException e) {
                getView().show(e);
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            } catch (BusinessErrorsException e) {
                fireErrorsChanged(new UIErrorsException(e));
            }
        }
    }

    @Override
    public void fireButtonStatechanged() {
        super.fireButtonStatechanged();
    }

    @Override
    public void fireErrorsChanged() {
        super.fireErrorsChanged();
    }

    @Override
    public void fireErrorsChanged(final UIErrorsException exception) {
        super.fireErrorsChanged(exception);
    }

    /**
     * Gets the browserCtrl.
     *
     * @return the browserCtrl.
     */
    public FProductionBCtrl getBrowserCtrl() {
        return browserCtrl;
    }

    /**
     * Gets the featureCtrl.
     * 
     * @category getter
     * @return the featureCtrl.
     */
    public AbstractFProductionInputFCtrl getFeatureCtrl() {
        return featureCtrl;
    }

    /**
     * Gets the featureView.
     * 
     * @category getter
     * @return the featureView.
     */
    public FeatureView getFeatureView() {
        return featureView;
    }

    /**
     * Gets the fproductionInputContainerCBS.
     *
     * @return the fproductionInputContainerCBS.
     */
    public C getFproductionInputContainerCBS() {
        return fproductionInputContainerCBS;
    }

    /**
     * Gets the previous.
     * 
     * @category getter
     * @return the previous.
     */
    public MovementAct getPrevious() {
        return previous;
    }

    /**
     * Get the viewer view.
     * 
     * @return the Viewer View
     */
    public FProductionInputContainerVIView getViewerView() {
        return (FProductionInputContainerVIView) getView();
    }

    /**
     * Gets the workBean.
     * 
     * @category getter
     * @return the workBean.
     */
    @Override
    public FProductionInputContainerWorkBean getWorkBean() {
        return workBean;
    }

    @Override
    public void initialize() {
        super.initialize();

        // Set the batch help type
        try {
            BatchHelpType type = getFproductionInputContainerCBS().getBatchHelpType(getIdCtx());
            if (type != null) {
                if (type.equals(BatchHelpType.BATCH_HELP)) {
                    getViewerView().getItfBatch().setUseFieldHelp(true);
                } else {
                    getViewerView().getItfBatch().setUseFieldHelp(false);
                }
            }
        } catch (BusinessException e) {
            getView().show(new UIException(e));
        } catch (BusinessErrorsException e) {
            fireErrorsChanged(new UIErrorsException(e));
        }
    }

    /**
     * Is data entry ok to validate ?
     * 
     * @return Is data entry ok to validate ?
     */
    public boolean isDataEntryOK() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isDataEntryOK()");
        }

        boolean ret = true;
        if (!isValidVBean()) {
            ret = false;
        } else if (!((InputOperationItemBean) getBean().getBBean().getOperationItemBean()).getIsWithoutQty()) {
            if (!"".equals(getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getUnit())
                    && getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getQty() == 0) {
                ret = false;
            } else if (!"".equals(getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().getUnit())
                    && getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().getQty() == 0) {
                ret = false;
            } else if (getBean().getEntity().getStockItemBean().getGeneralItemInfos().getStockByBatch()
                    && getBean().getEntity().getStockItemBean().getStockItem().getBatch().isEmpty()) {
                ret = false;
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isDataEntryOK()=" + ret);
        }
        return ret;
    }

    /**
     * Check the criteria.
     * 
     */
    public void manageCriteriaWindow() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageCriteriaWindow()");
        }

        if (getBean().getBeanCriteria() != null) {
            if (!getBean().getCriteriaWindowOpened() && getBean().getBeanCriteria().getAutoOpening() != null
                    && getBean().getBeanCriteria().getAutoOpening()
                    && getBean().getBeanCriteria().getListCriteria().size() > 0) {
                CriteriaDBean criteriaDBean = new CriteriaDBean(getBean().getBeanCriteria(), getBean().getEntity()
                        .getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
                WorkShopCriteriaTools.showCriteria(this.featureView, this, this, getModel().getIdentification(),
                        criteriaDBean);
            } else {
                getModel().getBean().setCriteriaWindowOpened(true);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageCriteriaWindow()");
        }
    }

    /**
     * Display the finish question if needed.
     */
    public void manageFinishQuestion() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageFinishQuestion()");
        }

        if (getBean().getFinishQuestionNeeded()) {
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29549, false), MessageButtons.YES_NO);
            if (ret == MessageButtons.YES) {
                getBean().setToFinish(true);
            } else {
                getBean().setToFinish(false);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageFinishQuestion()");
        }
    }

    /**
     * Display the questions if needed.
     *
     * @return true, if successful
     */
    public boolean manageQuestion() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageQuestion()");
        }

        boolean isOk = true;
        for (String question : getBean().getQuestions()) {
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false), question,
                    MessageButtons.YES_NO);
            if (ret == MessageButtons.NO) {
                isOk = false;
                break;
            }
        }
        getBean().getQuestions().clear();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageQuestion()=" + isOk);
        }
        return isOk;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized boolean performSave() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - performSave()");
        }

        boolean b = false;
        b = super.performSave();
        if (b && !getBean().getAutoInsert()) {
            manageCriteriaWindow();
            if (getBean().getCriteriaWindowOpened()) {
                boolean ret = manageQuestion();
                if (ret) {
                    manageFinishQuestion();
                    getBean().setForceUpdate(true);
                    performSave();
                } else {
                    emptyVBean();
                }
            } else if (!getBean().getHasQuality()) { // if we don't have quality, we have to manage question
                boolean ret = manageQuestion();
                if (ret) {
                    manageFinishQuestion();
                    getBean().setForceUpdate(true);
                    performSave();
                } else {
                    emptyVBean();
                }
            } else {
                emptyVBean();
            }
        }

        if (getBean().getAutoInsert()) {
            afterValidation();
        } else {
            getModel().setInitialBean(getBean());
        }

        // DC6074 - Go back to MO list if save successed and workBean.getReturnToMOList() is true
        if (b && workBean.getReturnToMOList()) {
            fireSelfFeatureClosingRequested();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - performSave()=" + b);
        }
        return b;
    }

    @Override
    public synchronized void performUndo() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - performUndo()");
        }

        super.performUndo();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - performUndo()");
        }
    }

    /**
     * Show the dialog box to know the label number to print and launch the print.
     *
     * @param documents list of documents
     */
    public void printDocuments(final List<PrintDocument> documents) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - printDocuments(documents=" + documents + ")");
        }

        if (documents.size() > 0) {
            for (PrintDocument document : documents) {
                DPrintDocumentCtrl ctrl = new DPrintDocumentCtrl();
                DPrintDocumentBean dBean = new DPrintDocumentBean(document);
                ctrl.showDocument(getFeatureView(), this, getModel().getIdentification(), dBean);
            }
            try {
                getFproductionInputContainerCBS().printDocuments(getIdCtx(), getBean(), documents, getWorkBean());
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
            documents.clear();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - printDocuments(documents=" + documents + ")");
        }
    }

    /**
     * Sets the browserCtrl.
     * 
     * @category setter
     * @param browserCtrl browserCtrl.
     */
    public void setBrowserCtrl(final FProductionBCtrl browserCtrl) {
        this.browserCtrl = browserCtrl;
    }

    /**
     * Sets the featureCtrl.
     * 
     * @category setter
     * @param featureCtrl featureCtrl.
     */
    public void setFeatureCtrl(final AbstractFProductionInputFCtrl featureCtrl) {
        this.featureCtrl = featureCtrl;
    }

    /**
     * Sets the featureView.
     * 
     * @category setter
     * @param featureView featureView.
     */
    public void setFeatureView(final FeatureView featureView) {
        this.featureView = featureView;
    }

    /**
     * Sets the fproductionInputContainerCBS.
     *
     * @param fproductionInputContainerCBS fproductionInputContainerCBS.
     */
    public void setFproductionInputContainerCBS(final C fproductionInputContainerCBS) {
        this.fproductionInputContainerCBS = fproductionInputContainerCBS;
    }

    /**
     * Sets the previous.
     * 
     * @category setter
     * @param previous previous.
     */
    public void setPrevious(final MovementAct previous) {
        this.previous = previous;
    }

    /**
     * Set scale weight in quantities components.
     * 
     * @param result result from scale
     */
    @Override
    public void setWeightResult(final FCommScaleResult result) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setWeightResult(result=" + result + ")");
        }

        // NLE 29/09/2014 Q007265

        // Modification in order to be sure that we put the weight in the screen zone where the weight unit is
        // because for example following a batch flash, the place of the weight unit can be changed.
        // Profile : U1 = Kg
        // Item : UP = L ; US = Kg

        String theWeightUnit = null;
        // Quantities itemQties = getBean().getEntity().getStockItemBean().getStockItem().getQuantities(); // units of
        // the
        // // item
        EntityUnitInfos profileUnitsInfo = getBean().getEntity().getStockItemBean().getUnitsInfos(); // unit types of
        // the profile

        try {
            Quantities screenQties = getBean().getEntity().getStockItemBean().getKeyboardQties();
            // NB : the screenQties match the profileUnitsInfo

            if (screenQties != null) {
                // Looking for a weight unit among the profile units
                // (trying to avoid a server access by the "whichOneIsTheWeightUnit" method below)
                if (profileUnitsInfo != null) {
                    if (EntityUnitType.WEIGHT.equals(profileUnitsInfo.getFirstUnitType())) {
                        theWeightUnit = screenQties.getFirstQty().getUnit();
                    } else if (EntityUnitType.WEIGHT.equals(profileUnitsInfo.getSecondUnitType())) {
                        theWeightUnit = screenQties.getSecondQty().getUnit();
                    } else if (EntityUnitType.WEIGHT.equals(profileUnitsInfo.getThirdUnitType())) {
                        theWeightUnit = screenQties.getThirdQty().getUnit();
                    }
                }

                // If the weight unit isn't one of the 3 of the profile, searches which of the 3 units on screen it is
                // Yes, should be useless...
                if (theWeightUnit == null) {
                    theWeightUnit = getFproductionInputContainerCBS().whichOneIsTheWeightUnit(getIdCtx(), screenQties);
                }

                // Puts the balance weight in the good screen field : the one which unit is the weight unit
                if (theWeightUnit != null) {
                    if (theWeightUnit.equals(screenQties.getFirstQty().getUnit())) {
                        screenQties.getFirstQty().setQty(result.getNetWeight());
                    } else if (theWeightUnit.equals(screenQties.getSecondQty().getUnit())) {
                        screenQties.getSecondQty().setQty(result.getNetWeight());
                    } else if (theWeightUnit.equals(screenQties.getThirdQty().getUnit())) {
                        screenQties.getThirdQty().setQty(result.getNetWeight());
                    }
                }
            }
        } catch (BusinessException e) {
            getView().show(new UIException(e));
        }

        // NLE 29/09/2014 Q007265 END

        getView().render();
        if (isDataEntryOK()) {
            performSave();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setWeightResult(result=" + result + ")");
        }
    }

    /**
     * Sets the workBean.
     * 
     * @category setter
     * @param workBean workBean.
     */
    public void setWorkBean(final FProductionInputContainerWorkBean workBean) {
        this.workBean = workBean;
    }

    /**
     * Empty error panel.
     * 
     */
    public void showUIErrorsException() {
        fireErrorsChanged();
    }

    /**
     * Show ui errors exceptions in error panel.
     * 
     * @param exception exception to show
     */
    public void showUIErrorsException(final UIErrorsException exception) {
        fireErrorsChanged(exception);
    }

    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {
    }

    @Override
    protected void deleteBean(final V bean) throws UIException {

    }

    /**
     * Reinitialize the viewer bean : reset the qty, clear question and update the view .
     */
    protected void emptyVBean() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - emptyVBean()");
        }

        getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().setQty(0.0);
        getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().setQty(0.0);
        getBean().getQuestions().clear();
        getBean().setAutoInsert(false);
        getView().render();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - emptyVBean()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fireBeanUpdated(final V bean) {
        super.fireBeanUpdated(bean);
    }

    @Override
    protected V insertBean(final V bean, final V initialBean) throws UIException, UIErrorsException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        boolean ret = false;
        if (AbstractFProductionFCtrl.BTN_WEIGHT_REFERENCE.equals(reference)) {
            if (getBean() != null) {
                InputOperationItemBean inputOperationItemBean = ((InputOperationItemBean) getBean().getBBean()
                        .getOperationItemBean());
                if (inputOperationItemBean != null
                        && (inputOperationItemBean.getIsWithoutQty() || TouchDeclarationType.SHOW_AUTOMATIC_TOUCH_DECLARATION
                                .equals(inputOperationItemBean.getTouchDeclarationType()))) {
                    ret = false;
                } else if (getWorkBean().getScale() != null //
                        && getBean() != null //
                        && (isValidVBean()) //
                        && (ManagementType.REAL.equals(getBean().getBBean().getOperationItemBean()
                                .getOperationItemKey().getManagementType()) //
                                && ((EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                                        .getFirstUnitType())) //
                                        || (EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                                                .getSecondUnitType()) && getBean().getEntity().getStockItemBean().getEnableFields()
                                                .getSecondQty())))) {
                    ret = true;
                } else {
                    ret = false;
                }
            }
        } else if (AbstractFProductionFCtrl.BTN_FEATURES_REFERENCE.equals(reference)) {
            ret = true;
        } else if (AbstractFProductionFCtrl.BTN_DETAIL_REFERENCE.equals(reference)) {
            if (isValidVBean()) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (AbstractFProductionFCtrl.BTN_FINISH_REFERENCE.equals(reference)) {
            if (isValidVBean()) {
                if (ManagementType.ARCHIVED.equals(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                        .getManagementType())) {
                    ret = getWorkBean().getHasRightForUnfinishing();
                } else {
                    ret = true;
                }
            } else {
                ret = false;
            }
        } else if (AbstractFProductionInputFCtrl.BTN_FPRODUCTION_OUTPUT_REFERENCE.equals(reference)) {
            ret = getWorkBean().getOutputAvailable();
        } else if (AbstractFProductionFCtrl.BTN_SHOW_ALL_REFERENCE.equals(reference)) {
            ret = true;
        } else if (AbstractFProductionInputFCtrl.BTN_FLASH_REFERENCE.equals(reference)) {
            if (getBean() != null) {
                InputOperationItemBean inputOperationItemBean = ((InputOperationItemBean) getBean().getBBean()
                        .getOperationItemBean());
                if (inputOperationItemBean != null
                        && (inputOperationItemBean.getIsWithoutQty() || TouchDeclarationType.SHOW_AUTOMATIC_TOUCH_DECLARATION
                                .equals(inputOperationItemBean.getTouchDeclarationType()))) {
                    ret = false;
                } else {
                    ret = true;
                }
            }
        } else if (AbstractFProductionFCtrl.BTN_CANCELLAST_REFERENCE.equals(reference)) {
            if (getPrevious() != null) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (AbstractFProductionFCtrl.BTN_PDF_REFERENCE.equalsIgnoreCase(reference)) {
            ret = isValidVBean();
            // TODO Constant ?
        } else if ("features".equalsIgnoreCase(reference)) {
            ret = FProductionTools.isValid(getBean());
        } else if (AbstractFProductionFCtrl.BTN_OPEN_INCIDENT_REFERENCE.equalsIgnoreCase(reference)) {
            ret = true;
        } else if (AbstractFProductionInputFCtrl.FEATURE_CONTENT_CODE.equalsIgnoreCase(reference)) {
            ret = getWorkBean().getHasRightForContentRateFeature();
            getFeatureCtrl().manageContentRateButton(ret);
        } else if (ButtonReference.ASK_PRODUCED_ENTITY_BTN.getReference().equals(reference)) {
            ret = true;
        } else if (ButtonReference.SETTING_INFORMATION.getReference().equals(reference)) {
            ret = true;
        } else if (ButtonReference.ASK_TATTOOED_NUMBER.getReference().equals(reference)) {
            if (StringUtils.isEmpty(getWorkBean().getInputWorkstationParameters().getDefaultTattoedPackaging())) {
                ret = false;
            } else {
                ret = true;

            }
        } else if (ButtonReference.DEBUG_REFERENCE.getReference().equals(reference)) {
            ret = true;
        }
        return ret;
    }

    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        return false;
    }

    /**
     * Checks if the viewer bean is valid (the mo chrono is greater than 0).
     *
     * @return true, if it's valid
     */
    protected boolean isValidVBean() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isValidVBean()");
        }

        boolean ret = false;
        if (getBean() != null //
                && (getBean().getBBean() != null) //
                && (getBean().getBBean().getOperationItemBean() != null) //
                && (getBean().getBBean().getOperationItemBean().getOperationItemKey() != null) //
                && (getBean().getBBean().getOperationItemBean().getOperationItemKey().getChrono() != null) //
                && (getBean().getBBean().getOperationItemBean().getOperationItemKey().getChrono().getChrono() > 0)) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isValidVBean()=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected V updateBean(final V pBean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(pBean=" + pBean + ")");
        }

        V vBean = pBean;
        try {
            vBean = (V) getFproductionInputContainerCBS().updateBean(getIdCtx(), vBean, getWorkBean());
            if (vBean.getAutoInsert()) {
                setPrevious(vBean.getPreviousMovementAct());
                getWorkBean().setOutputAvailable(vBean.getOutputAvailable());
                // ALB M158495 We have to change help batch selection when the item'id change.
                String itemId = vBean.getEntity().getStockItemBean().getGeneralItemInfos().getItemCL().getCode();
                if (itemId != null
                        && !itemId.equals(pBean.getEntity().getStockItemBean().getGeneralItemInfos().getItemCL()
                                .getCode())) {
                    getViewerView().getItfBatch().setInitialHelpSelection(
                            FProductionHelper.getFDestockingSBean(vBean.getBBean().getOperationItemBean(), vBean
                                    .getEntity().getStockItemBean().getGeneralItemInfos().getItemCL().getCode(),
                                    (FDestockingSBean) getViewerView().getItfBatch().getInitialHelpSelection()));
                }
            }
        } catch (BusinessException e) {
            emptyVBean();
            Errors erreurs = new VIFBindException(getBean());
            erreurs.rejectValue("barcode", "",
                    I18nClientManager.translate(e.getMainTrad(), true, e.getListeParams().toArray(new ITransParam[0])));
            throw new UIErrorsException(new BusinessErrorsException(erreurs));
        } catch (BusinessErrorsException e) {
            UIErrorsException ue = new UIErrorsException(e);
            emptyVBean();
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }
}

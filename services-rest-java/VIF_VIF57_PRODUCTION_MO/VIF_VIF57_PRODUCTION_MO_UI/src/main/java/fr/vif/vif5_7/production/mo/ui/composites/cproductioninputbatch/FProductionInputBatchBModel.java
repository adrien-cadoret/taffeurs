/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputBatchBModel.java,v $
 * Created on 12 déc. 2008 by gv
 */
package fr.vif.vif5_7.production.mo.ui.composites.cproductioninputbatch;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.FontNames;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputbatch.FProductionInputBatchBean;


/**
 * FProductionInputBatch : Browser Model.
 * 
 * @author gv
 */
public class FProductionInputBatchBModel extends BrowserModel<FProductionInputBatchBean> {

    private static Format formatBold   = new Format(StandardColor.BLUE, StandardColor.TOUCHSCREEN_FG_BROWSER,
                                               StandardFont.TOUCHSCREEN_DEFAULT_BROWSER_FONT);
    private static Format formatNormal = new Format(StandardColor.BLUE, StandardColor.TOUCHSCREEN_FG_BROWSER,
                                               StandardFont.TOUCHSCREEN_BROWSER_LABEL);

    /**
     * Default constructor.
     * 
     */
    public FProductionInputBatchBModel() {
        super();
        setBeanClass(FProductionInputBatchBean.class);
        // Add columns
        addColumn(new BrowserColumn("", "stockPointBean.batchId", 50, formatBold));
        addColumn(new BrowserColumn(I18nClientManager.translate(GenKernel.T210, false), "quantity", 50, formatNormal));
        addColumn(new BrowserColumn("", "stockPointBean.stockQty.firstUnit", 50, "3", 1));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29599, false), "stockPointBean.depotId",
                50, formatNormal));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29601, false),
                "stockPointBean.locationId", 50, formatNormal));

        setFetchSize(1000);
        setFirstFetchSize(1000);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FProductionInputBatchBean bean) {
        Format ret = null;
        Format cellFormat1 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
                FontStyle.BOLD, 12));

        Format cellFormat2 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
                FontStyle.BOLD, 14));
        cellFormat2.setForegroundColor(StandardColor.TOUCHSCREEN_FG_LABEL);

        if (property.equals("stockPointBean.batchId")) {
            ret = cellFormat2;
        } else {
            ret = cellFormat1;
        }

        return ret;
    }

    // /**
    // * {@inheritDoc}
    // */
    // @Override
    // public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
    // final FProductionInputBatchBean bean) {
    //
    // Format format = null;
    // // super.getCellFormat(rowNum, property, cellContent, bean);
    //
    // if (property.equals("stockPointBean.batchId")) {
    // format = new Format(StandardColor.BLUE, StandardColor.TOUCHSCREEN_FG_BROWSER,
    // StandardFont.TOUCHSCREEN_DEFAULT_BROWSER_FONT);
    // } else {
    // format = new Format(StandardColor.BG_DEFAULT, StandardColor.TOUCHSCREEN_FG_BROWSER,
    // StandardFont.TOUCHSCREEN_DEFAULT_BROWSER_FONT);
    // }
    // return format;
    // }

}

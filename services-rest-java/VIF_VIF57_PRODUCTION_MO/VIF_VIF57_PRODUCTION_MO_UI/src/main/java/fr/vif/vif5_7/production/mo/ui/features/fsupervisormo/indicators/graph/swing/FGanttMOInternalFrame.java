/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOInternalFrame.java,v $
 * Created on 21 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.swing.JMenu;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.common.beans.menu.LaunchParameterBean;
import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.dialogs.selection.SelectionModel;
import fr.vif.jtech.ui.dialogs.swing.StandardSwingDialog;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.mvc.MVCTriad;
import fr.vif.jtech.ui.models.sharedcontext.SharedContext;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGanttMOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGanttMOSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOCfgBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOGraphSelectionBean;
import fr.vif.vif5_7.production.mo.constants.GanttValueTypeEnum;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ActionId;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOActionManager;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOVModel;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOPeriod;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.FGraphMOGanttVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.GanttMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.IGraphMOView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing.AbstractMODockableView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;


/**
 * Frame containing a graph.
 * 
 * @author jd
 */
public class FGanttMOInternalFrame extends AbstractMODockableView<FGanttMOBean, FGraphMOGanttVCtrl> implements
DialogChangeListener, DisplayListener {
    /** LOGGER. */
    private static final Logger       LOGGER    = Logger.getLogger(FGanttMOInternalFrame.class);

    private FIndicatorMOActionManager actionManager;
    private FGanttMOBean              graphBean = new FGanttMOBean();

    private FGanttMOSBean             sbean     = new FGanttMOSBean();

    private GraphMOOptions            options   = new GraphMOOptions();

    private List<GanttValueTypeEnum>  toShow    = null;

    /**
     * Constructor.
     * 
     * @param frame {@link MOLocalFrame}
     */
    @SuppressWarnings("unchecked")
    public FGanttMOInternalFrame(final MOLocalFrame frame) {
        super(frame, new MVCTriad(FIndicatorMOVModel.class, FGanttMOView.class, FGraphMOGanttVCtrl.class));

        setTriadParameter(new MVCTriad(SelectionModel.class, FGanttSView.class, DGanttSelectionCtrl.class));

        JMenu menuAction = FSupervisorMOTools.getGanttMenuAction(this);

        // NLE8
        // JMenu menuHorizon = FSupervisorMOTools.getGanttMenuHorizon(this);

        ((IGraphMOView) getViewerController().getView()).addPopupMenu(menuAction);
        // NLE8
        // ((IGraphMOView) getViewerController().getView()).addPopupMenu(menuHorizon);

        // NLE9
        // Date dateMin = new Date();
        // Date dateMax = new Date();
        // Date dateBeg = new Date();
        // Date dateEnd = new Date();
        // for (int i = 0; i < graphBean.getIndicators().size(); i++) {
        // dateBeg = graphBean.getIndicators().get(i).getInfos().getEffectiveBeginDate();
        // dateEnd = graphBean.getIndicators().get(i).getInfos().getEffectiveEndDate();
        // if (dateMin == null || DateHelper.compareDate(dateBeg, dateMin) < 0) {
        // dateMin = dateBeg;
        // }
        // if (dateMax == null || DateHelper.compareDate(dateEnd, dateMax) > 0) {
        // dateMax = dateEnd;
        // }
        // }
        // if (DateHelper.compareDate(dateMax, dateMin) < 0) {
        // dateMax = dateMin;
        // }

        options.setNumberOfDataSetMax(Integer.MAX_VALUE);

        // NLE9
        // // options.setNbMinutesToShow(480); // 8h00 // NLE
        // int delta = DateHelper.compareDate(dateMax, dateMin);
        // options.setNbMinutesToShow(delta);
        //
        // options.setFirstMinuteToShow((DateHelper.getTodayTime().get(Calendar.HOUR_OF_DAY) * 60 + DateHelper
        // .getTodayTime().get(Calendar.MINUTE)) - (options.getNbMinutesToShow() / 2));
        //
        // // NLE : I don't know the use of what's next
        // // options.setFirstDayToShow(options.getFirstDate());
        // options.setFirstDayToShow(dateMin);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(e=" + e + ")");
        }

        /*
         * if (e.getSource() instanceof JMenuItem) { GenericActionEvent event = new GenericActionEvent(this,
         * e.getActionCommand(), e.getActionCommand(), getLocalFrame()); actionPerformed(event); } else if
         * (e.getSource() instanceof JCheckBoxMenuItem) { GanttValueTypeEnum i =
         * GanttValueTypeEnum.getValueType(e.getActionCommand());
         * 
         * if (toShow.contains(i)) { toShow.remove(i); } else { toShow.add(i); }
         * 
         * refresh();
         * 
         * } else { super.actionPerformed(e); }
         */

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        if (actionManager == null) {
            actionManager = new FIndicatorMOActionManager(getParentController());
        }

        GraphMOPeriod<IndicatorMO> period = ((IGanttMoView) getViewerController().getView())
                .getLastClickedGraphPeriod();

        if (ActionId.MO_UPDATE.getValue().equalsIgnoreCase(event.getAction())
                // || ActionId.MO_DETAIL.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_LIST.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_INPUTS_OUTPUTS.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_FINISH.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_UPDATE_DECLARATION.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_VALIDATE.getValue().equalsIgnoreCase(event.getAction())) {

            if (period != null && period.getBeanSource() != null && period.getBeanSource().getPoKey() != null
                    && period.getBeanSource().getPoKey().getChrono() != null) {
                List<ChronoPO> chronos = new ArrayList<ChronoPO>();
                chronos.add(period.getBeanSource().getPoKey().getChrono());

                LaunchParameterBean launchParameterBean = FSupervisorMOTools.getLaunchParameter(period.getBeanSource()
                        .getManufacturingDate(), chronos);

                getParentController().openMainFeature(ActionId.getFeatureId(event.getAction()).getValue(),
                        launchParameterBean);
            }

        } else {
            if (period != null) {
                actionManager.actionPerformed(event, getLocalFrame(), this, period.getBeanSource());
            }
        }

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void convert() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert()");
        }

        FSupervisorMOGraphSelectionBean sel = (FSupervisorMOGraphSelectionBean) getLocalFrame().getSelection();

        /* FB DP2581 informations not display at start */
        // ((FGanttMOView) getViewerController().getView()).setToShow(toShow);
        initSelection();
        getOptions().setToShow(toShow);

        try {

            graphBean = getViewerController().convert(getParentController().getIdCtx(), sel);

            options.setOverview(sbean.getOverview());
            options.setNbMinutesToShow((int) (Double.valueOf(sbean.getHorizon().getValue()) * 60 * 2));

            GraphMO graph = GanttMOTools.convertToGraph(graphBean, options);

            setFirstMinuteToShow();

            // NLE options.setFirstDate(sel.getPoDate());

            ((IGanttMoView) getViewerController().getView()).setGraph(graph, options);

            getViewerController().getView().render();
        } catch (UIException e) {
            getViewerController().getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        // TODO Auto-generated method stub
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        // super.dialogValidated(event);
        //

        if (event.getDialogBean() instanceof FGanttMOSBean) {
            // FIndicatorSimpleSBean sbean = (FIndicatorSimpleSBean) event.getDialogBean();

            toShow.clear();
            // toShow = new ArrayList<GanttValueTypeEnum>();
            if (sbean.getShowChrono()) {
                toShow.add(GanttValueTypeEnum.CHRONO);
            }
            if (sbean.getShowItemLabel()) {
                toShow.add(GanttValueTypeEnum.ITEM_LABEL);
            }
            if (sbean.getShowCodeLabel()) {
                toShow.add(GanttValueTypeEnum.CODE_LABEL);
            }
            if (sbean.getShowEffectiveQty()) {
                toShow.add(GanttValueTypeEnum.EFFECTIVE_QTY);
            }
            if (sbean.getShowExpectedQty()) {
                toShow.add(GanttValueTypeEnum.EXPECTED_QTY);
            }

            getOptions().setToShow(toShow);

            // refresh();
            options.setNbMinutesToShow((int) (Double.valueOf(sbean.getHorizon().getValue()) * 60 * 2));

            getParentController().getMemorization().saveMemorization(getLocalFrame().getUniqueId(), sbean);

            refresh();
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayRequested(final DisplayEvent event) {
        // TODO Auto-generated method stub

    }

    /**
     * Gets the options.
     *
     * @return the options.
     */
    public GraphMOOptions getOptions() {
        return options;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();

        if (toShow == null) {
            // Init with parameter of multiples values
            toShow = new ArrayList<GanttValueTypeEnum>();

            SharedContext ctx = getViewerController().getSharedContext();

            FSupervisorMOCfgBean cfg = FSupervisorMOTools.getConfiguration(ctx);

            // toShow.add(cfg.getDefaultValue());

            // initSelection();

            // popupMenu = getInfoMenu(this);
        }
        if (toShow.size() == 0) {
            toShow.add(GanttValueTypeEnum.CHRONO);
        }
    }

    /**
     * Open a dialog box.
     * 
     * @param triad Triad for parameter dialog
     * @param sbean bean selection
     */
    public void openDialog(final MVCTriad triad, final Selection sbean, final DialogChangeListener dialogChangeListener) {
        // Construct the view of the selection and set its model.
        @SuppressWarnings("unchecked")
        StandardSwingDialog selectionView = (StandardSwingDialog) getParentController().getView().openDialog(
                triad.getViewClass());

        SelectionModel model;
        try {
            model = (SelectionModel) triad.getModelClass().newInstance();

            selectionView.setModel(model);
            selectionView.getModel().setIdentification(getLocalFrame().getFrameBean().getIdentification());

            // initSelection();

            // Construct and initialize the selection controller.
            @SuppressWarnings("unchecked")
            AbstractStandardDialogController ctrl = (AbstractStandardDialogController) StandardControllerFactory
                    .getInstance().getController(triad.getControllerClass());
            ctrl.setDialogView(selectionView);
            ctrl.addDialogChangeListener(dialogChangeListener);
            // initSelection();
            model.setDialogBean(sbean);
            ctrl.getModel().setDialogBean(((SelectionModel) selectionView.getModel()).getSelection());
            // selectionView.setCols(sbean.getColsRef());
            ctrl.initialize();
            // ctrl.removeDialogChangeListener(dialogChangeListener);
            ctrl.addDisplayListener(this);
            selectionView.setVisible(true);
            // ctrl.getModel().setDialogBean(getModel().getSelection());
        } catch (UIException e) {
            getViewerController().getView().show(e);
        } catch (InstantiationException e) {

        } catch (IllegalAccessException e) {
        }
    }

    /**
     * Sets the options.
     *
     * @param options options.
     */
    public void setOptions(final GraphMOOptions options) {
        this.options = options;
    }

    /**
     * 
     * Opens the parameter's dialog.
     */
    public void showParameter() {
        openDialog(getTriadParameter(), sbean, this);
    }

    /**
     * 
     * Inits the selection.
     */
    private void initSelection() {
        toShow.clear();

        getParentController().getMemorization().readMemorization(getLocalFrame().getUniqueId(), sbean);

        // toShow.clear();
        if (sbean.getShowChrono()) {
            toShow.add(GanttValueTypeEnum.CHRONO);
        }
        if (sbean.getShowItemLabel()) {
            toShow.add(GanttValueTypeEnum.ITEM_LABEL);
        }
        if (sbean.getShowCodeLabel()) {
            toShow.add(GanttValueTypeEnum.CODE_LABEL);
        }
        if (sbean.getShowEffectiveQty()) {
            toShow.add(GanttValueTypeEnum.EFFECTIVE_QTY);
        }
        if (sbean.getShowExpectedQty()) {
            toShow.add(GanttValueTypeEnum.EXPECTED_QTY);
        }

        if (toShow.size() == 0) {
            sbean.setShowChrono(true);
            toShow.add(GanttValueTypeEnum.CHRONO);
        }

        options.setNbMinutesToShow((int) (Double.valueOf(sbean.getHorizon().getValue()) * 60 * 2));

    }

    /**
     * Sets the first minute to show in the Gantt.
     */
    private void setFirstMinuteToShow() {
        // Begin date of the first MO
        Calendar beginOfFirstMO = options.getBeginOfFirstMO();

        int firstMinuteToShow = 0;

        // Today calendar
        Date today = new Date();
        Calendar calendarToday = Calendar.getInstance();
        calendarToday.setTime(today);

        // Get selection date
        Date date = getLocalFrame().getSelection().getPoDate();

        Calendar selectionCalendar = Calendar.getInstance();
        selectionCalendar.setTime(date);
        selectionCalendar.set(Calendar.HOUR_OF_DAY, calendarToday.get(Calendar.HOUR_OF_DAY));
        selectionCalendar.set(Calendar.MINUTE, calendarToday.get(Calendar.MINUTE));

        selectionCalendar.add(Calendar.MINUTE, -(options.getNbMinutesToShow() / 2));

        if (selectionCalendar.compareTo(beginOfFirstMO) < 0) {
            selectionCalendar.setTime(beginOfFirstMO.getTime());
        }
        firstMinuteToShow = selectionCalendar.get(Calendar.HOUR_OF_DAY) * 60 + selectionCalendar.get(Calendar.MINUTE);
        options.setFirstMinuteToShow(firstMinuteToShow);

    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputConstant.java,v $
 * Created on 3 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


/**
 * Constant use the boning output feature.
 * 
 * @author xg
 */
public final class FBoningOutputConstant {

    /**
     * mo Initial Selection.
     */
    public static final String MO_INITIAL_SELECTION           = "moInitialSelection";

    public static final String BONING_OUTPUT_HIERARCHY_FILTER = "BONING_OUTPUT_HIERARCHY_FILTER";

    /**
     * Default constr.
     */
    private FBoningOutputConstant() {
    }
}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOFView.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseListener;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.swing.StandardSwingFeature;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOVBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.FSupervisorMOFIView;


/**
 * FSupervisorMO : Feature View.
 * 
 * @author jd
 */
public class FSupervisorMOFView extends StandardSwingFeature<FSupervisorMOBBean, FSupervisorMOVBean> implements
        FSupervisorMOFIView {

    private FSupervisorMOBView fSupervisorMOBView = null;

    private FSupervisorMOVView fSupervisorMOVView = null;

    /**
     * Default constructor.
     * 
     */
    public FSupervisorMOFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void addMouseListener(final MouseListener l) {
        super.addMouseListener(l);
        getFSupervisorMOVView().addMouseListener(l);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserView<FSupervisorMOBBean> getBrowserView() {
        return getFSupervisorMOBView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView<FSupervisorMOVBean> getViewerView() {
        return getFSupervisorMOVView();
    }

    /**
     * Initialisation of the browser feature.
     * 
     * @return FSupervisorMOBView
     */
    private FSupervisorMOBView getFSupervisorMOBView() {
        if (fSupervisorMOBView == null) {
            fSupervisorMOBView = new FSupervisorMOBView();
        }
        return fSupervisorMOBView;
    }

    /**
     * Initialisation of the viewer feature.
     * 
     * @return FSupervisorMOVView
     */
    private FSupervisorMOVView getFSupervisorMOVView() {
        if (fSupervisorMOVView == null) {
            fSupervisorMOVView = new FSupervisorMOVView();
        }
        return fSupervisorMOVView;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[] { 0.0, 0.0 };
        gridBagLayout.columnWeights = new double[] { 1.0 };
        this.setLayout(gridBagLayout);

        GridBagConstraints gbcScrollPane = new GridBagConstraints();
        gbcScrollPane.weighty = 1.0;
        gbcScrollPane.weightx = 1.0;
        gbcScrollPane.fill = GridBagConstraints.BOTH;
        gbcScrollPane.gridx = 0;
        gbcScrollPane.gridy = 1;

        add(getFSupervisorMOVView(), gbcScrollPane);
    }

}

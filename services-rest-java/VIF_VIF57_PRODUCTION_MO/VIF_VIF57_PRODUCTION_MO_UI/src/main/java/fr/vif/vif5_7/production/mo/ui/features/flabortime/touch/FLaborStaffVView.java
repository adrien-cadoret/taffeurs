/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffVView.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.touch;


import java.awt.Dimension;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.ImageIcon;

import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.ui.button.touch.TouchBtnStd;
import fr.vif.jtech.ui.input.InputFieldActionEvent;
import fr.vif.jtech.ui.input.InputFieldActionListener;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.RoundBackgroundPanelBorder;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.StaffChart;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.TimeChart;


/**
 * labor staff viewer view.
 * 
 * @author nle
 */
public class FLaborStaffVView extends StandardTouchViewer<FLaborStaffBBean> {

    private TouchInputTextField itfDate;
    private TouchInputTextField itfHour;
    private TouchBtnStd         nextDateBtn;
    private TouchBtnStd         previousDateBtn;

    private TimeChart           timeChart;
    private StaffChart          staffChart;

    /**
     * Constructor.
     * 
     */
    public FLaborStaffVView() {
        super();
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param itfDate the date of the change of staff
     * @param itfHour the hour of the change of staff
     * @param nextDateBtn date + 1
     * @param previousDateBtn date - 1
     */
    public FLaborStaffVView(final TouchInputTextField itfDate, final TouchInputTextField itfHour,
            final TouchBtnStd nextDateBtn, final TouchBtnStd previousDateBtn) {
        super();
        this.itfDate = itfDate;
        this.itfHour = itfHour;
        this.nextDateBtn = nextDateBtn;
        this.previousDateBtn = previousDateBtn;
    }

    /**
     * 
     * Get the date.
     * 
     * @return the itfDate touchinputtextfield
     */
    public TouchInputTextField getItfDate() {
        if (itfDate == null) {
            itfDate = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_DATE, I18nClientManager.translate(
                    GenKernel.T353, false));
            itfDate.setSize(102, 70);
            itfDate.setBeanBased(false);
            itfDate.setEnabled(true);
            itfDate.setAlwaysDisabled(false);
            Dimension d = new Dimension(242, 70);
            itfDate.setPreferredSize(d);
            itfDate.setMinimumSize(d);
            itfDate.setMaximumSize(d);

            // don't put the date from database, keep the current date
            // itfDate.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            itfDate.setLocation(366, 11);
            itfDate.setValue(DateHelper.getTodayTime().getTime());
        }
        return itfDate;
    }

    /**
     * 
     * Get the hour.
     * 
     * @return the itfHour touchinputtextfield
     */
    public TouchInputTextField getItfHour() {
        if (itfHour == null) {
            itfHour = new TouchInputTextField(Date.class, TouchInputTextField.FORMAT_HOUR, I18nClientManager.translate(
                    ProductionMo.T29437, false));
            itfHour.setSize(54, 70);
            itfHour.setBeanBased(false);
            itfHour.setEnabled(true);
            itfHour.setAlwaysDisabled(false);
            Dimension d = new Dimension(141, 70);
            itfHour.setPreferredSize(d);
            itfHour.setMinimumSize(d);
            itfHour.setMaximumSize(d);

            // don't put the time from database, keep the current time
            // itfHour.setBeanProperty("laborStaff.laborStaffKey.laborTimeKey.datprod");

            itfHour.setLocation(535, 11);
            itfHour.setValue(DateHelper.getTodayTime().getTime());
        }
        return itfHour;
    }

    /**
     * Gets the nextDateBtn.
     * 
     * @category getter
     * @return the nextDateBtn.
     */
    public final TouchBtnStd getNextDateBtn() {
        if (nextDateBtn == null) {
            nextDateBtn = new TouchBtnStd();
            nextDateBtn.setBounds(471, 27, 54, 54);
            nextDateBtn.setPreferredSize(new Dimension(54, 54));
            nextDateBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayAfter.png")));
            nextDateBtn.setEnabled(true);
            nextDateBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    addDate();
                }

            });
        }
        return nextDateBtn;

    }

    /**
     * Gets the previousDateBtn.
     * 
     * @category getter
     * @return the previousDateBtn.
     */
    public final TouchBtnStd getPreviousDateBtn() {
        if (previousDateBtn == null) {
            previousDateBtn = new TouchBtnStd();
            previousDateBtn.setBounds(310, 27, 54, 54);
            previousDateBtn.setPreferredSize(new Dimension(54, 54));
            previousDateBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayBefore.png")));
            previousDateBtn.setEnabled(true);
            previousDateBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    subDate();
                }

            });

            previousDateBtn.addKeyListener(new KeyListener() {

                @Override
                public void keyPressed(final KeyEvent e) {
                }

                @Override
                public void keyReleased(final KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        subDate();
                    }
                }

                @Override
                public void keyTyped(final KeyEvent e) {
                }
            });
        }
        return previousDateBtn;

    }

    /**
     * Gets the staffChart.
     * 
     * @return the staffChart.
     */
    public StaffChart getStaffChart() {
        if (staffChart == null) {
            staffChart = new StaffChart();
            staffChart.setBounds(420, 100, 420, 230);
        }
        return staffChart;
    }

    /**
     * Gets the time chart.
     * 
     * @return the time chart.
     */
    public TimeChart getTimeChart() {
        if (timeChart == null) {
            timeChart = new TimeChart();
            timeChart.setBounds(10, 100, 420, 230);
        }
        return timeChart;
    }

    /**
     * Sets the itfDate.
     * 
     * @param itfDate the date.
     */
    public void setItfDate(final TouchInputTextField itfDate) {
        this.itfDate = itfDate;
    }

    /**
     * Sets the itfHour.
     * 
     * @param itfHour the hour.
     */
    public void setItfHour(final TouchInputTextField itfHour) {
        this.itfHour = itfHour;
    }

    /**
     * Sets the nextDateBtn.
     * 
     * @category setter
     * @param nextDateBtn nextDateBtn.
     */
    public final void setNextDateBtn(final TouchBtnStd nextDateBtn) {
        this.nextDateBtn = nextDateBtn;
    }

    /**
     * Sets the previousDateBtn.
     * 
     * @category setter
     * @param previousDateBtn previousDateBtn.
     */
    public final void setPreviousDateBtn(final TouchBtnStd previousDateBtn) {
        this.previousDateBtn = previousDateBtn;
    }

    /**
     * Sets the staffChart.
     * 
     * @param staffChart staffChart.
     */
    public void setStaffChart(final StaffChart staffChart) {
        this.staffChart = staffChart;
    }

    /**
     * Sets the time chart.
     * 
     * @param timeChart : the time chart.
     */
    public void setTimeChart(final TimeChart timeChart) {
        this.timeChart = timeChart;
    }

    /**
     * Inform the listener than the value has changed.
     */
    public void valueChanged() {
        InputFieldActionEvent event = new InputFieldActionEvent(getItfDate(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getItfDate());
        for (InputFieldActionListener listener : getItfDate().getInputFieldActionListeners()) {
            listener.valueChanged(event);
        }
    }

    /**
     * Add date.
     */
    private void addDate() {
        Date currentDate = (Date) getItfDate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        getItfDate().setValue(cal.getTime());
        valueChanged();
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setLayout(null);
        setBorder(new RoundBackgroundPanelBorder(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR));

        add(getItfDate());
        add(getItfHour());
        add(getPreviousDateBtn());
        add(getNextDateBtn());
        add(getTimeChart());
        add(getStaffChart());
    }

    /**
     * Sub date.
     */
    private void subDate() {
        Date currentDate = (Date) getItfDate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        getItfDate().setValue(cal.getTime());
        valueChanged();
    }

}

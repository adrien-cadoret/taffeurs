/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOItemListBModel.java,v $
 * Created on 12 oct. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmoitemlist;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmoitemlist.FMOItemListBBean;


/**
 * MoItemList : Browser Model.
 * 
 * @author vr
 */
public class FMOItemListBModel extends BrowserModel<FMOItemListBBean> {

    /**
     * Default constructor.
     * 
     */
    public FMOItemListBModel() {
        super();
        setBeanClass(FMOItemListBBean.class);
        addColumn(new BrowserColumn(I18nClientManager.translate(StockKernel.T29224, false),
                "operationItemBean.forecastBatch", 70, "15"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T19824, false),
                "operationItemBean.operationItemKey.chrono.chrono", 70, "########"));
        addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T3057, false),
                "operationItemBean.movementDateTime", 70, "dd/MM/yyyy HH:mm"));
        addColumn(new BrowserColumn(I18nClientManager.translate(GenKernel.T210, false),
                "operationItemBean.operationItemQuantityUnit.toDoQuantity", 70, "##########.####"));
        addColumn(new BrowserColumn("", "operationItemBean.operationItemQuantityUnit.unit", 10, "3"));
        setFetchSize(9999999);
        setFirstFetchSize(999999);
        setFullyFetched(true);
        setSelectionEnabled(false);
    }
}

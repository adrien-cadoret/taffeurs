/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompFModel.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.GeneralSelectionMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;


/**
 * Production Output Scrap Feature Model.
 * 
 * @author kl
 */
public class FProductionOutputCompFModel extends StandardFeatureModel {

    /**
     * Default constructor.
     * 
     */
    public FProductionOutputCompFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FProductionOutputCompBModel.class, null, FProductionOutputCompBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FproductionOutputCompVModel.class, null, FProductionOutputCompVCtrl.class));
        setGeneralSelectionTriad(new GeneralSelectionMVCTriad(FProductionOutputCompGSModel.class, null,
                FProductionOutputCompGSCtrl.class));
    }
}

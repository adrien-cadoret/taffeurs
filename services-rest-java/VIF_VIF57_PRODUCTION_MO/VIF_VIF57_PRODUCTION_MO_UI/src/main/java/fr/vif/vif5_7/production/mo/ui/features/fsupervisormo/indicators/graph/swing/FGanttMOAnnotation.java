/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOAnnotation.java,v $
 * Created on 23 mars 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Graphics2D;
import java.awt.Shape;
import java.awt.geom.Rectangle2D;

import org.jfree.chart.annotations.XYTextAnnotation;
import org.jfree.chart.axis.ValueAxis;
import org.jfree.chart.plot.Plot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.chart.plot.PlotRenderingInfo;
import org.jfree.chart.plot.XYPlot;
import org.jfree.text.TextUtilities;
import org.jfree.ui.RectangleEdge;


/**
 * FGanttAnnotation : Allow to calculate position of three annotations.
 * 
 * @author jd
 */
public class FGanttMOAnnotation extends XYTextAnnotation {
    private int number = 0;

    /**
     * Constructor.
     * 
     * @param number number of the annotation
     * @param text text
     * @param x x
     * @param y y
     */
    public FGanttMOAnnotation(final int number, final String text, final double x, final double y) {
        super(text, x, y);
        this.number = number;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void draw(final Graphics2D g2, final XYPlot plot, final Rectangle2D dataArea, final ValueAxis domainAxis,
            final ValueAxis rangeAxis, final int rendererIndex, final PlotRenderingInfo info) {
        PlotOrientation orientation = plot.getOrientation();
        RectangleEdge domainEdge = Plot.resolveDomainAxisLocation(plot.getDomainAxisLocation(), orientation);
        // RectangleEdge rangeEdge = Plot.resolveRangeAxisLocation(plot.getRangeAxisLocation(), orientation);

        float anchorX = (float) domainAxis.valueToJava2D(getX(), dataArea, domainEdge);
        float anchorY = (float) rangeAxis.valueToJava2D(getY(), dataArea, RectangleEdge.TOP);

        if (orientation == PlotOrientation.HORIZONTAL) {
            float tempAnchor = anchorX;
            anchorX = anchorY;
            anchorY = tempAnchor;
        }

        g2.setFont(getFont());
        g2.setPaint(getPaint());
        // FIXME 15 is the number max of line in gantt view and 3 the number max of annotation !
        // Q_05480: Superviseur : problème d'affichage - vue planning
        double space = Math.max(((dataArea.getHeight() / 15f) / 3) - 5, 10);

        TextUtilities.drawRotatedString(getText(), g2, anchorX, (float) (anchorY + space * (number - 1)),
                getTextAnchor(), getRotationAngle(), getRotationAnchor());
        Shape hotspot = TextUtilities.calculateRotatedStringBounds(getText(), g2, anchorX, anchorY, getTextAnchor(),
                getRotationAngle(), getRotationAnchor());

        String toolTip = getToolTipText();
        String url = getURL();
        if (toolTip != null || url != null) {
            addEntity(info, hotspot, rendererIndex, toolTip, url);
        }
    }
}

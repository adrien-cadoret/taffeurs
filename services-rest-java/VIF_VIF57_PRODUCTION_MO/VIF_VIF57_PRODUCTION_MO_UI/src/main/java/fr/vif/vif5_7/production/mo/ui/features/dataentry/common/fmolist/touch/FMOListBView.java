/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOListBView.java,v $
 * Created on 07 nov. 2008 by ALB
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.touch;


import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import fr.vif.jtech.ui.browser.swing.StdRenderer;
import fr.vif.jtech.ui.browser.touch.TouchLineBrowser;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOType;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.FMOListBIView;


/**
 * MOList : Browser View.
 * 
 * @author alb
 */
public class FMOListBView extends TouchLineBrowser<FMOListBBean> implements FMOListBIView {

    /**
     * Renderer to manage font of browser. See in FMOListBModel.getCellFormat
     *
     * @author kl
     */
    private class FMOListRenderer extends StdRenderer<FMOListBBean> {

        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {

            Component component = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

            return component;
        }

    }

    /**
     * rendrer with icon.
     * 
     * @author gv
     * @version $Revision: 1.11 $, $Date: 2015/10/08 15:24:21 $
     */
    private class IconRenderer implements TableCellRenderer {
        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {

            JLabel jl = new JLabel();
            if (isSelected) {
                jl.setBackground(TouchHelper.getAwtColor(MOUIConstants.TOUCHSCREEN_BG_SELECTED_BROWSE_LINE));
            } else if (row % 2 == 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
            } else {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
            }

            FMOListBBean moBean = getModel().getBeanAt(row);

            if (moBean.getIsFinished()) {
                jl.setIcon(new ImageIcon(getClass().getResource("/images/vif/vif57/production/mo/finishedMo.png")));
            } else if (MOType.ORIGIN.equals(moBean.getMobean().getMoType())) {
                jl.setIcon(new ImageIcon(getClass().getResource("/images/vif/vif57/production/mo/originMo.png")));
            }
            jl.setOpaque(true);
            return jl;
        }
    }

    /**
     * Default constructor.
     * 
     */
    public FMOListBView() {
        super();
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public JTable getJTableForResize() {
        return getJTable();
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        FMOListRenderer listRenderer = null;
        for (int i = 0; i < getModel().getColumnCount(); i++) {
            if ("mobean".equals(getModel().getBeanAttributeAt(i))) {
                getJTable().getColumnModel().getColumn(i).setCellRenderer(new IconRenderer());
            } else {
                listRenderer = new FMOListRenderer();
                listRenderer.setModel(getModel());
                getJTable().getColumnModel().getColumn(i).setCellRenderer(listRenderer);
            }
        }

    }

}

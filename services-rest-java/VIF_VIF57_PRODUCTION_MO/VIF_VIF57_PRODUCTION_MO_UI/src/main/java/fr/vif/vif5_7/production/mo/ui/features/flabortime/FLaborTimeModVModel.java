/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeModVModel.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeModBBean;


/**
 * Labor time modification viewer model.
 * 
 * @author nle
 */
public class FLaborTimeModVModel extends ViewerModel<FLaborTimeModBBean> {
    /**
     * Default constructor.
     * 
     */
    public FLaborTimeModVModel() {
        super();
        setBeanClass(FLaborTimeModBBean.class);
    }

}

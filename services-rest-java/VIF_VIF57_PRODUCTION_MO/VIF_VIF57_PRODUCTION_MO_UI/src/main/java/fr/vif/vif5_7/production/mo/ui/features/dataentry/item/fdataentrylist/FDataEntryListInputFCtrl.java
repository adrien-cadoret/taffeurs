/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListInputFCtrl.java,v $
 * Created on 8 avr. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;


/**
 * Data entry INPUT list feature controller.
 * 
 * @author vr
 */
public class FDataEntryListInputFCtrl extends FDataEntryListFCtrl {

    /** LOGGER. */
    private static final Logger LOGGER     = Logger.getLogger(FDataEntryListInputFCtrl.class);

    private InputEntityType     entityType = InputEntityType.CONTAINER;

    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        // All case are managed in super class
        super.buttonAction(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        entityType = (InputEntityType) getSharedContext().get(Domain.DOMAIN_PARENT, "inputEntityType");
        super.initialize();

        TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL);
        if (touchModel != null) {
            ((FDataEntryListBCtrl) getBrowserController()).changeColumns(entityType, touchModel);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lstBtn = super.manageActionsButtonModels(true);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lstBtn;
    }

}

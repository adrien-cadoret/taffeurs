/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompGSCtrl.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.events.input.InputFieldEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.selection.AbstractGeneralSelectionController;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompSBean;


/**
 * Production Output Scrap Selection Controller.
 * 
 * @author kl
 */
public class FProductionOutputCompGSCtrl extends AbstractGeneralSelectionController {

    private static final Logger LOGGER = Logger.getLogger(FProductionOutputCompGSCtrl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();
    }

    /**
     * {@inheritDoc}
     */
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException, UIErrorsException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final InputFieldEvent event) {

        // Update the selection Bean with the new state.
        if (event != null && event.getValue() != null) {
            ((FProductionOutputCompSBean) getBean()).setState((String) event.getValue());
        }
        super.valueChanged(event);
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionBView.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.touch;


import javax.swing.BorderFactory;
import javax.swing.border.AbstractBorder;

import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.browser.touch.BrowserCell;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.browser.touch.renderer.row.DefaultRowRenderer;
import fr.vif.jtech.ui.laf.touch.JToggleButtonBorder;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;


/**
 * FProduction input browser view.
 * 
 * @author vr
 */
public class FProductionBView extends TouchBrowser<FProductionBBean> {

    /**
     * Default constructor.
     */
    public FProductionBView() {
        super();
        ((DefaultRowRenderer) getRowRenderer()).setRendererType(BrowserModel.RENDERER_BY_ROW);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void refreshJtCells() {
        createEmptyBorder(3);
        super.refreshJtCells();
    }

    /**
     * Create an empty border on left and right side.
     *
     * @param size the size (pixel)
     */
    private void createEmptyBorder(final int size) {
        for (BrowserCell<FProductionBBean> cells : getBrowserCells()) {
            AbstractBorder cellBorder = BorderFactory.createCompoundBorder(new JToggleButtonBorder.Border(),
                    BorderFactory.createEmptyBorder(0, size, 0, size));
            cells.setBorder(cellBorder);
        }

    }
}

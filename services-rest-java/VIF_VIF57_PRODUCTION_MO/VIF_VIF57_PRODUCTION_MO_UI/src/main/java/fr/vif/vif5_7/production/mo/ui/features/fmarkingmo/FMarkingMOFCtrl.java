/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOFCtrl.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import java.awt.Frame;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import javax.swing.JPanel;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.dialogs.DialogController;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerCtrl;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerModel;
import fr.vif.jtech.ui.docviewer.pdfviewer.touch.TouchPDFViewerView;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.input.touch.TouchInputCheckBox;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarFunctionalitiesBtnDController;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchFunctionalitiesBtnDView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchToolBarBtnStd;
import fr.vif.jtech.ui.toolbar.buttons.toolbarbutton.ToolBarButtonController;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.marking.MarkingBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.DMarkingDocumentsBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOInitFeatureReturnBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOLine;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOMOKey;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOVBean;
import fr.vif.vif5_7.production.mo.business.services.features.fmarkingmo.FMarkingMOCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch.FMarkingMOTouchVView;
import fr.vif.vif5_7.workshop.device.business.beans.common.document.Document;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingDevice;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingEnum.DeviceState;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingEnum.LineState;


/**
 * MarkingMO : Feature Controller.
 * 
 * @author lmo
 */
public class FMarkingMOFCtrl extends StandardFeatureController implements DialogChangeListener, TableRowChangeListener {

    /** Open Incident button reference. */
    public static final String           BTN_CHANGE_LINE           = "BTN_CHANGE_LINE";
    public static final String           BTN_REFRESH_MO            = "BTN_REFRESH_MO";
    public static final String           BTN_CHANGE_DOCUMENTS      = "BTN_CHANGE_DOCUMENTS";
    public static final String           BTN_START_MO              = "BTN_START_OF";
    public static final String           BTN_FINISH_MO             = "BTN_FINISH_MO";
    // not functionalities
    public static final String           BTN_STOP_MO               = "BTN_STOP_MO";
    public static final String           BTN_PDF_REFERENCE         = "BTN_PDF_REFERENCE";
    public static final String           BTN_PDF_LIST_REFERENCE    = "BTN_PDF_LIST_REFERENCE";

    public static final String           BTN_CHANGE_LINE_PATH      = FMarkingMOIconeFactory.PATH_48X48 + "process.png";
    public static final String           BTN_REFRESH_MO_PATH       = FMarkingMOIconeFactory.PATH_48X48
                                                                           + "actualiser.png";
    public static final String           BTN_CHANGE_DOCUMENTS_PATH = FMarkingMOIconeFactory.PATH_48X48
                                                                           + "Etiquette.png";
    public static final String           BTN_START_MO_PATH         = FMarkingMOIconeFactory.PATH_48X48 + "startMO.png";
    public static final String           BTN_FINISH_MO_PATH        = "/images/vif/vif57/production/mo/finish.png";
    // functionalities
    public static final String           BTN_STOP_MO_PATH          = FMarkingMOIconeFactory.PATH_48X48 + "stopMO.png";
    public static final String           BTN_PDF_REFERENCE_PATH    = FMarkingMOIconeFactory.PATH_48X48 + "process.png";

    /** LOGGER. */
    private static final Logger          LOGGER                    = Logger.getLogger(FMarkingMOFCtrl.class);

    private List<ToolBarBtnStdBaseModel> buttons                   = new ArrayList<ToolBarBtnStdBaseModel>();

    private ToolBarBtnStdModel           btnStartMO;
    private TouchInputCheckBox           touchInputCheckBox;

    // buttons
    private ToolBarBtnStdModel           btnFinished;
    private ToolBarBtnStdModel           btnChangeLine;
    private ToolBarBtnStdModel           btnChangeDocuments;
    private ToolBarBtnStdModel           btnRefreshMO;

    // functionalities
    private ToolBarBtnStdModel           btnStopMO;
    private ToolBarBtnStdModel           btnPDFModel;

    private FMarkingMOCBS                fmarkingMOCBS             = null;

    private FMarkingMOMOKey              processingBBean           = null;

    // do not check line control (no device SIMULATED or NOT_STARTED) while changing Selection
    private boolean                      inhibitLineControl        = false;

    /**
     * Constructor.
     */
    public FMarkingMOFCtrl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        FMarkingMOVCtrl viewerController = (FMarkingMOVCtrl) getViewerController();
        if (event.getSource() != null) {
            String vReference = ((ToolBarButtonController) event.getSource()).getReference();
            FMarkingMOBCtrl browserController = (FMarkingMOBCtrl) getBrowserController();
            FMarkingMOBBean bBean = browserController.getModel().getCurrentBean();
            FMarkingMOMOKey bBeanMOKey = toFMarkingMOMOKey(bBean);
            if (BTN_START_MO.equals(vReference)) {
                try {
                    if (processingBBean != null) {
                        actionStopMO();
                    }
                    if (processingBBean == null) {
                        viewerController.startMO();
                        bBean.setMoState(LineState.PROCESSING);
                        processingBBean = bBeanMOKey;
                    }
                } catch (UIException e) {
                    show(e);
                }
                viewerController.renderAndEnableComponents();
            } else if (BTN_STOP_MO.equals(vReference)) {
                actionStopMO();
                viewerController.renderAndEnableComponents();
            } else if (BTN_CHANGE_DOCUMENTS.equalsIgnoreCase(vReference)) {
                actionDocumentChange();
            } else if (BTN_CHANGE_LINE.equalsIgnoreCase(vReference)) {
                actionRefreshMO(true);
            } else if (BTN_REFRESH_MO.equalsIgnoreCase(vReference)) {
                actionRefreshMO(false);
            } else if (BTN_FINISH_MO.equalsIgnoreCase(vReference)) {
                actionFinish();
            } else if (BTN_PDF_REFERENCE.equalsIgnoreCase(event.getModel().getReference())) {
                actionPdfReference();
                // CHU DP2331 - add management PDF document list
            } else if (BTN_PDF_LIST_REFERENCE.equals(event.getModel().getReference())) {
                PDFFile pdfFile = (PDFFile) ((ToolBarBtnStdModel) event.getModel()).getLinkedObject();
                viewDocumentRecette(pdfFile);
            }
        }

        super.buttonAction(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closingRequired(final ClosingEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - closingRequired(event=" + event + ")");
        }

        // if there is a problem during start up (user refused to stop printers for exemple)
        // datas aren't initialized and sharedContext contains only empty beans.
        Object vFMarkingMOSBean = getSharedContext().get(Domain.DOMAIN_THIS,
                FMarkingMOUIConstants.SELECTION_BEAN_PARAM_KEY);
        if (vFMarkingMOSBean instanceof FMarkingMOSBean
                && EntityTools.isValid(((FMarkingMOSBean) vFMarkingMOSBean).getWorkStationCL().getCode())) {

            saveBeforeLigneChanged();
            FMarkingMOInitFeatureReturnBean vFeatureReturnBean = (FMarkingMOInitFeatureReturnBean) getSharedContext()
                    .get(Domain.DOMAIN_THIS, FMarkingMOUIConstants.INIT_FEATURE_BEAN);

            FMarkingMOVCtrl viewerController = (FMarkingMOVCtrl) getViewerController();

            Map<MarkingDevice, OperationItemKey> vDevicesToStop = new HashMap<MarkingDevice, OperationItemKey>();

            FMarkingMOMOKey newFMarkingMOMOKey = new FMarkingMOMOKey();
            List<String> vRunningMOLineNames = new ArrayList<String>();
            StringBuilder vSB = new StringBuilder();
            boolean vFirstLine = true;
            for (FMarkingMOLine vLine : vFeatureReturnBean.getLines()) {
                if (!newFMarkingMOMOKey.equals(vLine.getProcessingMODescription().getMoKey())
                        || !newFMarkingMOMOKey.equals(vLine.getSelectedMOKey())) {

                    for (MarkingDevice vMarkingDevice : vLine.getDevices().getDevicesList()) {
                        boolean vRunning = false;
                        if (vMarkingDevice.getState() == DeviceState.STARTED) {
                            vDevicesToStop.put(vMarkingDevice, vLine.getProcessingMODescription().getMoKey()
                                    .getOperationItemKey());
                            vRunning = true;
                        } else if (vMarkingDevice.getState() == DeviceState.SIMULATED) {
                            vDevicesToStop.put(vMarkingDevice, vLine.getSelectedMOKey().getOperationItemKey());
                            vRunning = true;
                        }
                        if (vRunning) {
                            String vLineCode = vLine.getLineCL().getCode();
                            if (!vRunningMOLineNames.contains(vLineCode)) {
                                if (vFirstLine) {
                                    vFirstLine = false;
                                } else {
                                    vSB.append(" & ");
                                }
                                vSB.append(vLineCode);
                                vRunningMOLineNames.add(vLineCode);
                            }
                        }
                    }
                }
            }

            if (!vDevicesToStop.isEmpty()) {
                String vQuestion = I18nClientManager.translate(ProductionMo.T34909)
                        + "\n"
                        + I18nClientManager.translate(ProductionMo.T34910, false,
                                new VarParamTranslation(vSB.toString()));
                int vRep = showQuestion(I18nClientManager.translate(Jtech.T28946), vQuestion, MessageButtons.OK_CANCEL,
                        MessageButtons.OK);
                if (vRep == MessageButtons.CANCEL) {
                    throw new UIVetoException(ProductionMo.T34909);
                }
                FMarkingMOHeadsCtrl vHeadsCtrl = (FMarkingMOHeadsCtrl) ((FMarkingMOTouchVView) viewerController
                        .getView()).getIMarkingHeadsCtrl();
                for (Entry<MarkingDevice, OperationItemKey> vEntry : vDevicesToStop.entrySet()) {
                    vHeadsCtrl.stopDeviceWithoutUIControl(vEntry.getKey(), vEntry.getValue());
                }
            }
        }
        super.closingRequired(event);

        FMarkingMOSharedContext.clearInstance();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - closingRequired(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }

        if (event.getDialogBean() instanceof DMarkingDocumentsBean) {
            DMarkingDocumentsBean vBean = (DMarkingDocumentsBean) event.getDialogBean();
            Set<String> vListDocCode = new HashSet<String>();
            CodeLabel vDocCL;

            vDocCL = vBean.getDocument1();
            if (vDocCL != null && EntityTools.isValid(vDocCL.getCode())) {
                vListDocCode.add(vDocCL.getCode());
            }
            vDocCL = vBean.getDocument2();
            if (vDocCL != null && EntityTools.isValid(vDocCL.getCode())) {
                vListDocCode.add(vDocCL.getCode());
            }
            vDocCL = vBean.getDocument3();
            if (vDocCL != null && EntityTools.isValid(vDocCL.getCode())) {
                vListDocCode.add(vDocCL.getCode());
            }
            vDocCL = vBean.getDocument4();
            if (vDocCL != null && EntityTools.isValid(vDocCL.getCode())) {
                vListDocCode.add(vDocCL.getCode());
            }
            vDocCL = vBean.getDocument5();
            if (vDocCL != null && EntityTools.isValid(vDocCL.getCode())) {
                vListDocCode.add(vDocCL.getCode());
            }
            vDocCL = vBean.getDocument6();
            if (vDocCL != null && EntityTools.isValid(vDocCL.getCode())) {
                vListDocCode.add(vDocCL.getCode());
            }
            try {
                List<Document> vListDocument = getFmarkingMOCBS().getDocuments(getIdCtx(), getIdCtx().getCompany(),
                        getIdCtx().getEstablishment(), new ArrayList<String>(vListDocCode));
                if (!vListDocument.isEmpty()) {
                    ((FMarkingMOVCtrl) getViewerController()).changeDocuments(vListDocument);
                }
            } catch (BusinessException e) {
                if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                    LOGGER.error("dialogValidated(event)Exception occured", e);
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    /**
     * Gets the btnChangeDocuments.
     * 
     * @return the btnChangeDocuments.
     */
    public ToolBarBtnStdModel getBtnChangeDocuments() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnChangeDocuments()");
        }

        if (this.btnChangeDocuments == null) {
            this.btnChangeDocuments = new ToolBarBtnStdModel();
            this.btnChangeDocuments.setReference(BTN_CHANGE_DOCUMENTS);
            this.btnChangeDocuments.setText(I18nClientManager.translate(ProductionMo.T34788));
            this.btnChangeDocuments.setIconURL(BTN_CHANGE_DOCUMENTS_PATH);
            this.btnChangeDocuments.setFunctionality(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnChangeDocuments()=" + this.btnChangeDocuments);
        }
        return this.btnChangeDocuments;
    }

    /**
     * Gets the btnChangeLine.
     * 
     * @return the btnChangeLine.
     */
    public ToolBarBtnStdModel getBtnChangeLine() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnChangeLine()");
        }

        if (this.btnChangeLine == null) {

            // Force always enable to allow selection when no line is available
            this.btnChangeLine = new ToolBarBtnStdModel();
            this.btnChangeLine.setReference(BTN_CHANGE_LINE);
            this.btnChangeLine.setText(I18nClientManager.translate(ProductionMo.T34787));
            this.btnChangeLine.setIconURL(BTN_CHANGE_LINE_PATH);
            this.btnChangeLine.setFunctionality(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnChangeLine()=" + this.btnChangeLine);
        }
        return this.btnChangeLine;
    }

    /**
     * Gets the btnFinished.
     * 
     * @return the btnFinished.
     */
    public ToolBarBtnStdModel getBtnFinished() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnFinished()");
        }

        if (this.btnFinished == null) {
            this.btnFinished = new ToolBarBtnStdModel();
            this.btnFinished.setReference(BTN_FINISH_MO);
            this.btnFinished.setText(I18nClientManager.translate(ProductionMo.T29510));
            this.btnFinished.setIconURL(BTN_FINISH_MO_PATH);
            this.btnFinished.setFunctionality(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnFinished()=" + this.btnFinished);
        }
        return this.btnFinished;
    }

    /**
     * Gets the btnRefreshMO.
     * 
     * @return the btnRefreshMO.
     */
    public ToolBarBtnStdModel getBtnRefreshMO() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnRefreshMO()");
        }

        if (this.btnRefreshMO == null) {

            // Force always enable to allow selection when no line is available
            this.btnRefreshMO = new ToolBarBtnStdModel();
            this.btnRefreshMO.setReference(BTN_REFRESH_MO);
            this.btnRefreshMO.setText(I18nClientManager.translate(Jtech.T31951));
            this.btnRefreshMO.setIconURL(BTN_REFRESH_MO_PATH);
            this.btnRefreshMO.setFunctionality(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnRefreshMO()=" + this.btnRefreshMO);
        }
        return this.btnRefreshMO;
    }

    /**
     * Gets the btnStartMO.
     * 
     * @return the btnStartMO.
     */
    public ToolBarBtnStdModel getBtnStartMO() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnStartMO()");
        }

        if (this.btnStartMO == null) {
            this.btnStartMO = new ToolBarBtnStdModel();
            this.btnStartMO.setReference(BTN_START_MO);
            this.btnStartMO.setText(I18nClientManager.translate(ProductionMo.T34786, false));
            this.btnStartMO.setIconURL(BTN_START_MO_PATH);
            this.btnStartMO.setFunctionality(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnStartMO()=" + this.btnStartMO);
        }
        return this.btnStartMO;
    }

    /**
     * Gets the btnStopMO.
     * 
     * @return the btnStopMO.
     */
    public ToolBarBtnStdModel getBtnStopMO() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnStopMO()");
        }

        if (this.btnStopMO == null) {
            this.btnStopMO = new ToolBarBtnStdModel();
            this.btnStopMO.setReference(BTN_STOP_MO);
            this.btnStopMO.setText(I18nClientManager.translate(ProductionMo.T34835, false));
            this.btnStopMO.setIconURL(BTN_STOP_MO_PATH);
            this.btnStopMO.setFunctionality(true);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnStopMO()=" + this.btnStopMO);
        }
        return this.btnStopMO;
    }

    /**
     * Gets the fmarkingMOCBS.
     * 
     * @return the fmarkingMOCBS.
     */
    public FMarkingMOCBS getFmarkingMOCBS() {
        return fmarkingMOCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }
        boolean vCalledByExternal = false;
        boolean vOpeningCanceled = false;

        // clear the previous values in shared context
        getSharedContext().put(Domain.DOMAIN_THIS, FMarkingMOUIConstants.PROCESSING_LINES,
                new HashMap<CodeLabel, Date>());
        getSharedContext().put(Domain.DOMAIN_THIS, FMarkingMOUIConstants.FINISHED_RUNNING_LINES,
                new HashSet<CodeLabel>());
        getSharedContext().put(Domain.DOMAIN_THIS, FMarkingMOUIConstants.INIT_FEATURE_BEAN,
                new FMarkingMOInitFeatureReturnBean());
        getSharedContext().put(Domain.DOMAIN_THIS, FMarkingMOUIConstants.CURRENT_SELECTION_BEAN, new FMarkingMOSBean());
        fireSharedContextSend(getSharedContext());

        FMarkingMOSBean sel = null;
        // final is function is called by an external function, the SBean have to be inside the params values
        if (getModel().getParams() != null) {
            sel = (FMarkingMOSBean) getModel().getParams().getValues()
                    .get(FMarkingMOUIConstants.SELECTION_BEAN_PARAM_KEY);
            vCalledByExternal = true;
            // ... but Enable selection here
            getBrowserController().getModel().setSelectionEnabled(sel == null);
        }

        // Else: open a Selection window at the opening of the function --> SetRequiredSelectionOnInit
        if (sel != null) {
            getBrowserController().setInitialSelection(sel);
            getBrowserController().setRequireSelectionOnInit(false);
        } else {
            getBrowserController().setRequireSelectionOnInit(true);
        }

        if (getIdCtx().getWorkstation() == null || "".equals(getIdCtx().getWorkstation())) {
            fireSelfFeatureClosingRequired();
        } else {
            try {
                FMarkingMOInitFeatureReturnBean vFeatureReturnBean = getFmarkingMOCBS().initFeature(getIdCtx(),
                        getIdCtx().getWorkstation());

                // if oppened from the menu and some devices are currently printing. Force stop devices.
                Map<FMarkingMOLine, List<MarkingBean>> vStartedMarkingBean = vFeatureReturnBean.getStartedMarkingBean();
                if (!vCalledByExternal && !vStartedMarkingBean.isEmpty()) {
                    List<MarkingBean> vListMarkingBean = new ArrayList<MarkingBean>();
                    StringBuilder vSB = new StringBuilder();
                    boolean vFirstLine = true;
                    for (FMarkingMOLine vLine : vStartedMarkingBean.keySet()) {
                        if (!vStartedMarkingBean.get(vLine).isEmpty()) {
                            if (vFirstLine) {
                                vFirstLine = false;
                            } else {
                                vSB.append(" & ");
                            }
                            vSB.append(vLine.getLineCL().getCode());
                            vListMarkingBean.addAll(vStartedMarkingBean.get(vLine));
                        }
                    }

                    if (!vListMarkingBean.isEmpty()) {
                        String vQuestion = I18nClientManager.translate(ProductionMo.T34909)
                                + "\n"
                                + I18nClientManager.translate(ProductionMo.T34910, false,
                                        new VarParamTranslation(vSB.toString()));
                        int vRep = showQuestion(I18nClientManager.translate(Generic.T4057), vQuestion,
                                MessageButtons.OK_CANCEL, MessageButtons.OK);
                        if (vRep == MessageButtons.CANCEL) {
                            vOpeningCanceled = true;
                        } else {
                            for (MarkingBean vMarkingBean : vListMarkingBean) {

                                OperationItemKey vOperationItemKey = new OperationItemKey();
                                vOperationItemKey.setEstablishmentKey(new EstablishmentKey(getIdCtx().getCompany(),
                                        getIdCtx().getEstablishment()));
                                vMarkingBean.setOperationItemKey(vOperationItemKey);
                                vMarkingBean.setFeatureId(getView().getModel().getFeatureId());
                                getFmarkingMOCBS().stopPrinting(getIdCtx(), vMarkingBean);
                            }
                        }
                    }
                }
                if (vOpeningCanceled) {
                    fireSelfFeatureClosingRequired();
                } else {
                    getSharedContext().put(Domain.DOMAIN_THIS, FMarkingMOUIConstants.INIT_FEATURE_BEAN,
                            vFeatureReturnBean);

                    fireSharedContextSend(getSharedContext());

                    // Create Singleton
                    FMarkingMOSharedContext.create(this);

                    super.initialize();
                    setErrorPanelAlwaysVisible(true);

                    getBrowserController().addTableRowChangeListener(this);
                }
            } catch (BusinessException e) {
                // View doesn't exist ??!!
                // show(new UIException(e));
                fireSelfFeatureClosingRequired();
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        // nothing to do
    }

    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanging(event=" + event + ")");
        }
        try {
            if (!inhibitLineControl) {
                ((FMarkingMOVCtrl) getViewerController()).checkChangingBrowserRowAllowed();
            }
        } catch (UIException e) {
            show(e);
            throw new UIVetoException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanging(event=" + event + ")");
        }
    }

    /**
     * Sets the fmarkingMOCBS.
     * 
     * @param fmarkingMOCBS fmarkingMOCBS.
     */
    public void setFmarkingMOCBS(final FMarkingMOCBS fmarkingMOCBS) {
        this.fmarkingMOCBS = fmarkingMOCBS;
    }

    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        if (buttons.size() == 0) {
            buttons.add(getBtnStartMO());
            buttons.add(getBtnStopMO());
            buttons.add(getBtnChangeLine());
            buttons.add(getBtnRefreshMO());
            buttons.add(getBtnChangeDocuments());
            buttons.add(getBtnFinished());
            buttons.add(getBtnPDFModel());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()=" + buttons);
        }
        return buttons;
    }

    /**
     * * Gets the btnPDFModel.
     * 
     * @category getter
     * @return the btnPDFModel.
     */
    protected ToolBarBtnStdModel getBtnPDFModel() {
        if (this.btnPDFModel == null) {
            this.btnPDFModel = new ToolBarBtnStdModel();
            this.btnPDFModel.setReference(BTN_PDF_REFERENCE);
            this.btnPDFModel.setText(I18nClientManager.translate(ProductionMo.T30838));
            this.btnPDFModel.setIconURL(BTN_PDF_REFERENCE_PATH);
            this.btnPDFModel.setFunctionality(true);
        }
        return this.btnPDFModel;
    }

    /**
     * Show multiple documents.
     *
     * @param listPDFFiles list of documents
     */
    protected void viewDocumentList(final List<PDFFile> listPDFFiles) {
        final DisplayListener dispListener = new DisplayListener() {
            @Override
            public void displayRequested(final DisplayEvent event) {
                event.getViewToDisplay().showView();
            }
        };

        final DialogChangeListener dialogListener = new DialogChangeListener() {
            @Override
            public void dialogCancelled(final DialogChangeEvent event) {
                // Nothing to do
            }

            @Override
            public void dialogValidated(final DialogChangeEvent event) {

            }
        };

        final List<ToolBarBtnStdView> list = new ArrayList<ToolBarBtnStdView>();

        for (PDFFile pdfFile : listPDFFiles) {
            TouchToolBarBtnStd btn = new TouchToolBarBtnStd();
            ToolBarBtnStdModel model = new ToolBarBtnStdModel();
            model.setReference(BTN_PDF_LIST_REFERENCE);
            model.setLinkedObject(pdfFile);
            model.setText(pdfFile.getTitle());
            btn.setModel(model);

            list.add(btn);
            ToolBarButtonController ctrl = new ToolBarButtonController();
            ctrl.setView(btn);

            btn.setBtnCtrl(ctrl);
            btn.getBtnCtrl().addButtonListener(this);
        }

        final TouchFunctionalitiesBtnDView dView = new TouchFunctionalitiesBtnDView(
                (Frame) ((JPanel) getView()).getTopLevelAncestor(), list);
        dView.setTitle(I18nClientManager.translate(Jtech.T22109));
        try {
            DialogController controller = null;
            controller = StandardControllerFactory.getInstance().getController(
                    ToolBarFunctionalitiesBtnDController.class);

            controller.setDialogView(dView);
            controller.addDialogChangeListener(dialogListener);
            controller.addDisplayListener(dispListener);
            controller.initialize();
        } catch (final UIException e) {
            LOGGER.error("Error instanciating Dialog :", e);
        }

    }

    /**
     * 
     * View the recette document.
     * 
     * @param pdfFile the file
     */
    protected void viewDocumentRecette(final PDFFile pdfFile) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - viewDocumentRecette(pdfFile=" + pdfFile + ")");
        }

        if (pdfFile != null && !"".equals(pdfFile.getPath())) {
            viewDocument(pdfFile.getPath(), pdfFile.getTitle());
        } else {
            getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                    I18nClientManager.translate(ProductionMo.T30839));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - viewDocumentRecette(pdfFile=" + pdfFile + ")");
        }
    }

    /**
     * Action triggered when "Change Document" button is clicked.
     */
    private void actionDocumentChange() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionDocumentChange()");
        }

        /** check critical prerequisite (selected MO not started) */
        boolean vNoProblem = checkPrerequisiteOK(false, false, Boolean.FALSE);
        if (vNoProblem) {

            /** check prerequisite that can require user validation (no SIMULATED device) */
            vNoProblem = checkPrerequisiteOK(true, false, null);
            boolean vUserValidation = true;
            if (!vNoProblem) {
                /** required users confirmation */
                String vTitle = I18nClientManager.translate(Generic.T25766);
                String vQuestion = I18nClientManager.translate(ProductionMo.T34905, false, ProductionMo.T34907) + "\n"
                        + I18nClientManager.translate(ProductionMo.T34908);
                vUserValidation = showQuestion(vTitle, vQuestion, MessageButtons.OK_CANCEL, MessageButtons.OK) == MessageButtons.OK;
            }
            if (vNoProblem || vUserValidation) {
                FMarkingMOVCtrl viewerController = (FMarkingMOVCtrl) getViewerController();

                /** stop the devices SIMULATED; */
                viewerController.stopAllDevices(DeviceState.SIMULATED);

                /** special behavior */

                // create a copy of the DocumentCLs, put them in a DMarkingDocumentsBean and open the documents
                // selection with the bean
                DMarkingDocumentsBean bean = new DMarkingDocumentsBean();
                FMarkingMOVBean vVBean = viewerController.getModel().getBean();
                List<Document> notProcessingRequiredDocuments = vVBean.getNotProcessingRequiredDocuments();
                int nbDocuments = notProcessingRequiredDocuments.size();
                for (int i = 0; i < nbDocuments; i++) {
                    Document vDocument = notProcessingRequiredDocuments.get(i);
                    CodeLabel vNewDocumentCL = new CodeLabel(vDocument.getDocumentCL().getCode(), vDocument
                            .getDocumentCL().getLabel());
                    if (i == 0) {
                        bean.setDocument1(vNewDocumentCL);
                    } else if (i == 1) {
                        bean.setDocument2(vNewDocumentCL);
                    } else if (i == 2) {
                        bean.setDocument3(vNewDocumentCL);
                    } else if (i == 3) {
                        bean.setDocument4(vNewDocumentCL);
                    } else if (i == 4) {
                        bean.setDocument5(vNewDocumentCL);
                    } else if (i == 5) {
                        bean.setDocument6(vNewDocumentCL);
                    } else {
                        break;
                    }
                }

                try {
                    DMarkingDocumentsCtrl.show(getView(), this, this, getModel().getIdentification(), bean);
                } catch (UIException e) {
                    if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                        LOGGER.error("buttonAction(event)Exception occured", e);
                    }
                    show(e);
                }
            }

        } else {
            showError(I18nClientManager.translate(ProductionMo.T34905, false, ProductionMo.T34906));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionDocumentChange()");
        }
    }

    /**
     * Action triggered when "Finish MO" button is clicked.
     */
    private void actionFinish() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionFinish()");
        }

        FMarkingMOBBean vBBean = (FMarkingMOBBean) getBrowserController().getModel().getCurrentBean();
        ManagementType vManagementType = vBBean.getOperationItemKey().getManagementType();
        boolean notFinished = (ManagementType.REAL.equals(vManagementType));
        if (notFinished) {
            /**
             * check critical prerequisite : - not simulating - no "finished" MO running or Simulating - processing bean
             * is not equal selected bean
             */
            boolean vNoProblem = true;
            FMarkingMOSBean sBean = (FMarkingMOSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                    FMarkingMOUIConstants.CURRENT_SELECTION_BEAN);
            vNoProblem = checkPrerequisiteOK(true, false, null);

            if (vNoProblem) {
                FMarkingMOBBean vProcessingBean = findProcessingBBean();
                if (vProcessingBean != null) {
                    FMarkingMOBBean vSelectedBean = (FMarkingMOBBean) getBrowserController().getModel()
                            .getCurrentBean();
                    // if the processing bean is the selected bean, we refuse to finish the MO
                    // the user have to stop it first
                    if (vProcessingBean.equals(vSelectedBean)) {
                        vNoProblem = false;
                    }
                }
            }

            if (vNoProblem) {
                /** check prerequisite that can require user validation (none) */
                /** required users confirmation */
                String vTitle = I18nClientManager.translate(ProductionMo.T29510);
                String vQuestion = I18nClientManager.translate(ProductionMo.T35058);
                boolean vUserValidation = showQuestion(vTitle, vQuestion, MessageButtons.OK_CANCEL, MessageButtons.OK) == MessageButtons.OK;
                if (vUserValidation) {
                    /**
                     * special behavior : send order to finish the MO. then (if the finished MO are hidden) call
                     * queryElement to hide the newly finished MO
                     */
                    // finish the M.O.
                    FMarkingMOBBean currentBean = (FMarkingMOBBean) getBrowserController().getModel().getCurrentBean();
                    try {
                        getFmarkingMOCBS().finishMarking(getIdCtx(),
                                new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()),
                                sBean.getWorkStationCL().getCode(), getIdCtx().getLogin(),
                                currentBean.getOperationItemKey().getChrono());

                        // reload the list
                        if (!sBean.getShowFinishedMO()) {
                            saveBeforeLigneChanged();
                            try {
                                inhibitLineControl = true;
                                getBrowserController().reopenQuery();
                                loadingAfterLigneChanged();
                            } finally {
                                inhibitLineControl = false;
                            }
                            // Q7422 n°7: When we display the finished MO, we have to refresh the current line. If not,
                            // the FinishMO button is always enabled.
                        } else {
                            int selectedRowNum = getBrowserController().getModel().getCurrentRowNumber();
                            BrowserActionEvent bae = new BrowserActionEvent(this, "", selectedRowNum);
                            getBrowserController().reopenQuery();
                            getBrowserController().selectedRowChanged(bae);
                        }
                    } catch (BusinessException e) {
                        getView().show(new UIException(e));
                    }
                }
            } else {
                showError(I18nClientManager.translate(Generic.T22494, false, ProductionMo.T35057));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionFinish()");
        }
    }

    /**
     * Action triggered when "Process" button is clicked.
     */
    private void actionPdfReference() {
        try {

            List<PDFFile> listPdfFiles = getDocumentsToShow();
            if (listPdfFiles.isEmpty()) {
                getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                        I18nClientManager.translate(ProductionMo.T30839));
            } else if (listPdfFiles.size() == 1) {
                viewDocumentRecette(listPdfFiles.get(0));
            } else {
                viewDocumentList(listPdfFiles);
            }

        } catch (BusinessException e) {
            getView().show(new UIException(e));
        }
    }

    /**
     * Refresh the browser data.
     * 
     * @param aNewSelectionRequiered if true open the selection view before updating data.
     * 
     */
    private void actionRefreshMO(final boolean aNewSelectionRequiered) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionLineChange()");
        }

        /** check critical prerequisite (none) */
        /** check prerequisite that can require user validation (none) */
        /** required users confirmation (none) */
        /**
         * special behavior: save the current position, require selection then try to select the previous bbean
         */
        saveBeforeLigneChanged();
        try {
            inhibitLineControl = true;
            if (aNewSelectionRequiered) {
                ((FMarkingMOBCtrl) getBrowserController()).selectionRequired();
            } else {
                getBrowserController().reopenQuery();
            }
            loadingAfterLigneChanged();
        } finally {
            inhibitLineControl = false;
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionLineChange()");
        }
    }

    /**
     * Action triggered when "Stop MO" button is clicked.
     */
    private void actionStopMO() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionStopMO()");
        }

        boolean vUserValidation;

        /** check critical prerequisite (none) */
        /** check prerequisite that can require user validation (none) */
        /** required users confirmation */
        String vTitle = I18nClientManager.translate(ProductionMo.T34835, false);
        String vQuestion = I18nClientManager.translate(Generic.T482, false, ProductionMo.T34835);
        vUserValidation = showQuestion(vTitle, vQuestion, MessageButtons.OK_CANCEL, MessageButtons.OK) == MessageButtons.OK;
        if (vUserValidation) {

            /** special behavior send order to stop STARTED device, update the previouly processing BBean and update UI */
            FMarkingMOVCtrl viewerController = (FMarkingMOVCtrl) getViewerController();
            viewerController.stopAllDevices(DeviceState.STARTED);
            if (!viewerController.isAnyDeviceStarted()) {
                viewerController.stopMO();
            }
            FMarkingMOBBean vBBean = findProcessingBBean();
            if (vBBean != null) {
                vBBean.setMoState(LineState.NOT_PROCESSING);
            }
            processingBBean = null;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionStopMO()");
        }
    }

    /**
     * Checks data values and returns true if all is ok. Parameters define the list of checks.
     * 
     * @param checkSimulated checks there is not device "SIMULATED"
     * @param checkStarted checks there is not device "STARTED"
     * @param checkProcessingEqualsCurrent if not null, check the comparison between selected MO and processing MO is
     *            equal to the given boolean
     * @return true if there is no problem
     */
    private boolean checkPrerequisiteOK(final boolean checkSimulated, final boolean checkStarted,
            final Boolean checkProcessingEqualsCurrent) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkPrerequisiteOK(checkSimulated=" + checkSimulated + ", checkStarted=" + checkStarted
                    + ", checkProcessingEqualsCurrent=" + checkProcessingEqualsCurrent + ")");
        }

        boolean vRet = true;
        FMarkingMOVCtrl viewerController = (FMarkingMOVCtrl) getViewerController();
        vRet &= !checkSimulated || !viewerController.isAnyDeviceSimulated();
        vRet &= !checkStarted || !viewerController.isAnyDeviceStarted();
        if (vRet && checkProcessingEqualsCurrent != null) {
            // calculate if the processing bean and the selected bean are the same and compare the result with the
            // expected value
            if (processingBBean == null) {
                vRet = !checkProcessingEqualsCurrent;
            } else {

                FMarkingMOBBean vSelectedBean = (FMarkingMOBBean) getBrowserController().getModel().getCurrentBean();
                boolean vBeanEquals = processingBBean.equals(toFMarkingMOMOKey(vSelectedBean));
                vRet = checkProcessingEqualsCurrent.equals(vBeanEquals);
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkPrerequisiteOK(checkSimulated=" + checkSimulated + ", checkStarted=" + checkStarted
                    + ", checkProcessingEqualsCurrent=" + checkProcessingEqualsCurrent + ")=" + vRet);
        }
        return vRet;
    }

    /**
     * Get the processing BBean (if exists).
     * 
     * @return the pocessing BBean
     */
    private FMarkingMOBBean findProcessingBBean() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - findProcessingBBean()");
        }

        FMarkingMOBBean vRet = null;
        if (processingBBean != null) {
            FMarkingMOBCtrl browserController = (FMarkingMOBCtrl) getBrowserController();
            for (FMarkingMOBBean vBBean : browserController.getModel().getBeans()) {
                FMarkingMOMOKey vBBeanMOKey = toFMarkingMOMOKey(vBBean);
                if (processingBBean.equals(vBBeanMOKey)) {
                    vRet = vBBean;
                    break;
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - findProcessingBBean()=" + vRet);
        }
        return vRet;
    }

    /**
     * Get the current FMarkingMOLine from the shared context.
     * 
     * @return the current FMarkingMOLine
     */
    private FMarkingMOLine getCurrentLine() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getCurrentLine()");
        }

        FMarkingMOSBean selection = (FMarkingMOSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                FMarkingMOUIConstants.CURRENT_SELECTION_BEAN);

        FMarkingMOInitFeatureReturnBean vInitFeatureBean = (FMarkingMOInitFeatureReturnBean) getSharedContext().get(
                Domain.DOMAIN_THIS, FMarkingMOUIConstants.INIT_FEATURE_BEAN);

        CodeLabel vSearchedLine = selection.getLineCL();

        FMarkingMOLine vCurrentLine = null;
        if (vSearchedLine != null) {
            for (FMarkingMOLine vLine : vInitFeatureBean.getLines()) {
                if (vSearchedLine.equals(vLine.getLineCL())) {
                    vCurrentLine = vLine;
                    break;
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getCurrentLine()=" + vCurrentLine);
        }
        return vCurrentLine;
    }

    /**
     * Get documents to show.
     * 
     * @return the list of pdf files
     * @throws BusinessException if error occurs
     */
    private List<PDFFile> getDocumentsToShow() throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDocumentsToShow()");
        }

        MOItemKey moItemKey = new MOItemKey();

        final FMarkingMOVBean vBean = ((FMarkingMOVCtrl) getViewerController()).getBean();
        final OperationItemKey vOperationItemKey = vBean.getSelectedItem().getMoKey().getOperationItemKey();
        moItemKey.setEstablishmentKey(vOperationItemKey.getEstablishmentKey());
        moItemKey.setChrono(vOperationItemKey.getChrono());
        moItemKey.setCounter1(0);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDocumentToShow()");
        }
        return getFmarkingMOCBS().getPdfFilesOfCurrentItem(getIdCtx(), moItemKey);
    }

    /**
     * After changing line, reload informations and update UI (selected line, start/stop button...).
     */
    private void loadingAfterLigneChanged() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - loadingAfterLigneChanged()");
        }

        FMarkingMOLine vCurrentLine = getCurrentLine();
        FMarkingMOMOKey newFMarkingMOMOKey = new FMarkingMOMOKey();

        final FMarkingMOMOKey vCurrentFMarkingMOMOKey = vCurrentLine.getProcessingMODescription().getMoKey();
        if (vCurrentLine != null && !newFMarkingMOMOKey.equals(vCurrentFMarkingMOMOKey)) {
            processingBBean = vCurrentFMarkingMOMOKey;
        } else {
            processingBBean = null;
        }
        FMarkingMOBCtrl browserController = (FMarkingMOBCtrl) getBrowserController();
        // search the BBean of the processing MO and change his MoState to PROCESSING
        if (processingBBean != null) {

            FMarkingMOBBean vBBean = findProcessingBBean();

            // TODO exception si vBBean == null
            vBBean.setMoState(LineState.PROCESSING);

        }
        // search the line we need to select manually
        if (vCurrentLine != null) {
            FMarkingMOMOKey simulatingMOKey = vCurrentLine.getSelectedMOKey();
            if (simulatingMOKey != null && !newFMarkingMOMOKey.equals(simulatingMOKey)) {
                boolean vRowChanged = browserController.repositionToProcess(simulatingMOKey.getOperationItemKey()
                        .getChrono(), simulatingMOKey.getItemCL());
                if (!vRowChanged) {
                    /** stop the devices SIMULATED; */
                    FMarkingMOVCtrl viewerController = (FMarkingMOVCtrl) getViewerController();
                    viewerController.stopAllDevices(DeviceState.SIMULATED);
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - loadingAfterLigneChanged()");
        }
    }

    /**
     * Before changing MO line, save required informations necessary to reload data and UI newt time we will return on
     * the current line.
     * 
     * /!\ WARNING: when queryElements() is required, the BBean don't know their state (processing or not). If the
     * selected BBean is the processing one, the first devices' calculation will be wrong. BBean will be updated in
     * "loadingAfterLineChanged()" and a event will be send to change the browser's selection. This second device's
     * calculation will be right.
     * 
     * Here, if there is no device simulating and an MO is processing, we save the selection on the processing bean.
     * This way, we guaranty there is the second devices' calculation.
     */
    @SuppressWarnings("unchecked")
    private void saveBeforeLigneChanged() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveBeforeLigneChanged()");
        }

        FMarkingMOVCtrl viewerController = (FMarkingMOVCtrl) getViewerController();
        Map<CodeLabel, Date> vMapProcessingDatesPerLine;
        Object vObjectInContext = getSharedContext().get(Domain.DOMAIN_THIS, FMarkingMOUIConstants.PROCESSING_LINES);
        if (vObjectInContext instanceof Map) {
            vMapProcessingDatesPerLine = (Map<CodeLabel, Date>) vObjectInContext;
        } else {
            vMapProcessingDatesPerLine = new HashMap<CodeLabel, Date>();
            getSharedContext().put(Domain.DOMAIN_THIS, FMarkingMOUIConstants.PROCESSING_LINES,
                    vMapProcessingDatesPerLine);
        }

        Set<CodeLabel> vListFinishedRunningMO;
        vObjectInContext = getSharedContext().get(Domain.DOMAIN_THIS, FMarkingMOUIConstants.FINISHED_RUNNING_LINES);
        if (vObjectInContext instanceof Set) {
            vListFinishedRunningMO = (Set<CodeLabel>) vObjectInContext;
        } else {
            vListFinishedRunningMO = new HashSet<CodeLabel>();
            getSharedContext().put(Domain.DOMAIN_THIS, FMarkingMOUIConstants.FINISHED_RUNNING_LINES,
                    vListFinishedRunningMO);
        }

        FMarkingMOSBean selection = (FMarkingMOSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                FMarkingMOUIConstants.CURRENT_SELECTION_BEAN);

        FMarkingMOLine vCurrentLine = getCurrentLine();

        if (processingBBean != null || viewerController.isAnyDeviceSimulated()) {
            FMarkingMOMOKey vFMarkingMOMOKey = null;

            FMarkingMOVBean vVBean = viewerController.getBean();
            if (viewerController.isAnyDeviceSimulated()) {
                vFMarkingMOMOKey = vVBean.getSelectedItem().getMoKey();
            }

            // if the the current row is the processing one, it may have some devices NOT_STARTED.
            // in this case, we store the row key in "simulatingMOKey" to repositioning on the current line
            if (vFMarkingMOMOKey == null) {
                if (processingBBean != null) {
                    vFMarkingMOMOKey = processingBBean;
                } else {
                    vFMarkingMOMOKey = new FMarkingMOMOKey();
                }
            }

            vCurrentLine.setSelectedMOKey(vFMarkingMOMOKey);
            vMapProcessingDatesPerLine.put(vCurrentLine.getLineCL(), selection.getMoDate());

            // if there is a Finished MO currently STARTED or SIMULATING, the user could not hide finished MO
            boolean vFinishedRunningMO = false;
            if (processingBBean != null) {
                FMarkingMOBBean vFMarkingMOBBean = findProcessingBBean();
                if (vFMarkingMOBBean != null) {
                    vFinishedRunningMO = ManagementType.ARCHIVED.equals(vFMarkingMOBBean.getOperationItemKey()
                            .getManagementType());
                }
            }

            if (!vFinishedRunningMO) {
                FMarkingMOBBean vSelectedBean = (FMarkingMOBBean) getBrowserController().getModel().getCurrentBean();
                if (vSelectedBean != null) {
                    vFinishedRunningMO = ManagementType.ARCHIVED.equals(vSelectedBean.getOperationItemKey()
                            .getManagementType());
                }
            }
            if (vFinishedRunningMO) {
                vListFinishedRunningMO.add(vCurrentLine.getLineCL());
            } else {
                vListFinishedRunningMO.remove(vCurrentLine.getLineCL());
            }
        } else {
            vCurrentLine.setSelectedMOKey(new FMarkingMOMOKey());
            vMapProcessingDatesPerLine.remove(vCurrentLine.getLineCL());
            vListFinishedRunningMO.remove(vCurrentLine.getLineCL());
        }
        fireSharedContextSend(getSharedContext());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveBeforeLigneChanged()");
        }
    }

    /**
     * Get the FMarkingMOMOKey corresponding to a FMarkingMOBBean.
     * 
     * @param bBean the BBean
     * @return the FMarkingMOBBean
     */
    private FMarkingMOMOKey toFMarkingMOMOKey(final FMarkingMOBBean bBean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - toFMarkingMOMOKey(bBean=" + bBean + ")");
        }

        FMarkingMOMOKey vRet = null;
        if (bBean != null) {
            vRet = new FMarkingMOMOKey();
            vRet.setOperationItemKey(bBean.getOperationItemKey());
            CodeLabel vItemCLs = bBean.getItemCL();
            vRet.setItemCL(new CodeLabel(vItemCLs.getCode(), vItemCLs.getLabel()));
            vRet.setLof(bBean.getLof());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - toFMarkingMOMOKey(bBean=" + bBean + ")=" + vRet);
        }
        return vRet;
    }

    /**
     * View a document file.
     * 
     * @param url url file
     * @param title title
     */
    private void viewDocument(final String url, final String title) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - viewDocument(url=" + url + ", title=" + title + ")");
        }

        TouchPDFViewerModel model = new TouchPDFViewerModel();
        model.setTitle(title);
        model.setFilePath(url);
        TouchPDFViewerView view = new TouchPDFViewerView();
        view.setModel(model);
        TouchPDFViewerCtrl controller = new TouchPDFViewerCtrl();
        controller.setDialogView(view);
        controller.initialize();
        view.showView();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - viewDocument(url=" + url + ", title=" + title + ")");
        }
    }
}

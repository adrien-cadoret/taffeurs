/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDowngradedItemBModel.java,v $
 * Created on 19 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.fdowngradeditem;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdowngradeditem.FDowngradedItemBBean;


/**
 * FDowngradedItem : Browser Model.
 * 
 * @author gp
 */
public class FDowngradedItemBModel extends BrowserModel<FDowngradedItemBBean> {

    /**
     * Default constructor.
     * 
     */
    public FDowngradedItemBModel() {
        super();
        setBeanClass(FDowngradedItemBBean.class);
        addColumn(new BrowserColumn("", "code", 0));
        addColumn(new BrowserColumn("", "label", 1));
        setFullyFetched(true);
        setFetchSize(Integer.MAX_VALUE);
        setFirstFetchSize(Integer.MAX_VALUE);
    }

}

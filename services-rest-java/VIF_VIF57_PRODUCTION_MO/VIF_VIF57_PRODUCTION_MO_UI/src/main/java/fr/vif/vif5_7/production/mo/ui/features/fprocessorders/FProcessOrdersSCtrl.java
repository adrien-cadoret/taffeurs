/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersSCtrl.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.ui.dialogs.selection.AbstractStandardSelectionController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.input.InputTextFieldView;
import fr.vif.vif5_7.activities.activities.business.beans.features.fteam.FTeamSBean;
import fr.vif.vif5_7.activities.activities.ui.composites.cteamlist.swing.CTeamListView;
import fr.vif.vif5_7.gen.hierarchy.ui.composites.hierarchy.chchylist.CHchyListCtrl;
import fr.vif.vif5_7.gen.location.business.beans.features.farea.FAreaSBean;
import fr.vif.vif5_7.gen.location.ui.composites.carealist.swing.CAreaListView;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersSBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.swing.FProcessOrdersSView;


/**
 * ProcessOrders : Selection Controller.
 * 
 * @author jd
 */
public class FProcessOrdersSCtrl extends AbstractStandardSelectionController {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FProcessOrdersSCtrl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Object initHelpDialogBox(final InputTextFieldView fieldView) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initHelpDialogBox(fieldView=" + fieldView + ")");
        }

        fieldView.setValue(upperCaseList(fieldView.getValue()));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initHelpDialogBox(fieldView=" + fieldView + ")");
        }
        return super.initHelpDialogBox(fieldView);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        initializeArea();

        initializeTeam();

        initializeFLine();

        initializeItemHchy();

        // initializeHierarchyItems();
        //
        // initializeHierarchyRessources();

        super.initialize();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Initialize Area composite.
     */
    public void initializeArea() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeArea()");
        }

        CAreaListView cArea = ((FProcessOrdersSView) getView()).getcArea();

        if (cArea.getInitialHelpSelection() == null) {
            FAreaSBean selection = new FAreaSBean(true,
                    ((FProcessOrdersSBean) getModel().getDialogBean()).getEstablishmentKey());
            cArea.setInitialHelpSelection(selection);

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeArea()");
        }
    }

    /**
     * 
     * Initialize FLine composite.
     */
    public void initializeFLine() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeFLine()");
        }

        ((CHchyListCtrl) ((FProcessOrdersSIView) getView()).getcFLine().getController()).setAttribute(
                getIdentification().getCompany(), Mnemos.NATURE.RESOURCE.getCode(), Mnemos.HIERACHY.PRIMARY.getCode(),
                0);

        // ((FProcessOrdersSBean) getSelection()).getLineHchyLstValue().setHierarchy(new CodeLabel("$NIVRESS", ""));
        // ((FProcessOrdersSBean) getSelection()).getLineHchyLstValue().setLevel(0);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeFLine()");
        }
    }

    /**
     * 
     * Initialize ItemHchy composite.
     */
    public void initializeItemHchy() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeItemHchy()");
        }

        // getViewerController().getSharedContext();
        // FSupervisorMOTools.getConfiguration(this);
        ((CHchyListCtrl) ((FProcessOrdersSIView) getView()).getcItemHchy().getController()).setAttribute(
                getIdentification().getCompany(), Mnemos.NATURE.ITEM.getCode(), ((FProcessOrdersSBean) getModel()
                        .getDialogBean()).getItemHierarchy(), 1);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeItemHchy()");
        }
    }

    /**
     * Initialize Team composite.
     */
    public void initializeTeam() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeTeam()");
        }

        CTeamListView cTeam = ((FProcessOrdersSView) getView()).getcTeam();

        if (cTeam.getInitialHelpSelection() == null) {
            FTeamSBean selection = new FTeamSBean(true, "",
                    ((FProcessOrdersSBean) getModel().getDialogBean()).getEstablishmentKey());
            cTeam.setInitialHelpSelection(selection);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeTeam()");
        }
    }

    // private void initializeHierarchyItems() {
    //
    // ((CHchyLstValueCtrl) ((FProcessOrdersSView) getView()).getcItemHierarchy().getController()).setAttribute(
    // getIdentification().getCompany(), Mnemos.NATURE.ITEM.getCode(), getIdentification().getEstablishment(),
    // I18nClientManager.translate(GenKernel.T426, false));
    // ((CHchyLstValueCtrl) ((FProcessOrdersSView) getView()).getcItemHierarchy().getController())
    // .setCreationMode(false);
    // // ((CHchyLstValueCtrl) ((FProcessOrdersSView) getView()).getcHierarchy().getController()).setAttribute("TOT",
    // // 0,
    // // false);
    // // ((FProcessOrdersSBean) getSelection()).getItemHchyLstValue().setHierarchy(new CodeLabel("$FAMART", ""));
    // // ((FProcessOrdersSBean) getSelection()).getItemHchyLstValue().setLevel(1);
    //
    // }

    // private void initializeHierarchyRessources() {
    //
    // ((CHchyLstValueCtrl) ((FProcessOrdersSView) getView()).getcLineHierarchy().getController()).setAttribute(
    // getIdentification().getCompany(), "RES", getIdentification().getEstablishment(), I18nClientManager
    // .translate(ProductionMo.T32440, false));
    //
    // ((FProcessOrdersSBean) getSelection()).getLineHchyLstValue().setHierarchy(new CodeLabel("$NIVRESS", ""));
    // ((FProcessOrdersSBean) getSelection()).getLineHchyLstValue().setLevel(0);
    //
    // }

    /*
     * 
     */
    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

    /**
     * 
     * Uppercase List of String values.
     * 
     * @param value Object
     * @return Object
     */
    @SuppressWarnings("unchecked")
    private Object upperCaseList(final Object value) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - upperCaseList(value=" + value + ")");
        }

        Object result = value;
        if (value instanceof List<?>) {
            List<String> listResult = new ArrayList<String>();
            for (Object elem : (List<Object>) value) {
                if (elem instanceof String) {
                    listResult.add(((String) elem).toUpperCase());
                }
            }
            if (listResult.size() > 0) {
                result = listResult;
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - upperCaseList(value=" + value + ")=" + result);
        }
        return result;
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeModFView.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.touch;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;


/**
 * labor time feature view.
 * 
 * @author nle
 */
public class FLaborTimeModFView extends StandardTouchFeature {

    private FLaborTimeModBView bView;
    private FLaborTimeModVView vView;
    private TitlePanel         subTitlePanel;

    /**
     * Constructor.
     * 
     */
    public FLaborTimeModFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserView getBrowserView() {
        return getBView();
    }

    /**
     * Gets the bsView.
     * 
     * @category getter
     * @return the BView Labor Time.
     */
    public FLaborTimeModBView getBView() {
        if (bView == null) {
            bView = new FLaborTimeModBView();
            bView.setBounds(0, 40, 870, 332);
            bView.setVisible(false);
        }
        return bView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView getViewerView() {
        return getVView();
    }

    /**
     * Gets the btView.
     * 
     * @category getter
     * @return the BView Labor Time.
     */
    public FLaborTimeModVView getVView() {
        if (vView == null) {
            vView = new FLaborTimeModVView();
            vView.setBounds(0, 200, 860, 300);
            vView.setVisible(true);
        }
        return vView;
    }

    /**
     * {@inheritDoc}
     */
    public void setSubTitle(final String subTitle) {
        subTitlePanel.setTitle(subTitle);

    }

    /**
     * Get the subtitle panel.
     * 
     * @return the subtitle panel
     */
    private JPanel getSubTitle() {
        if (subTitlePanel == null) {
            subTitlePanel = new TitlePanel();
            subTitlePanel.setBounds(0, 0, 870, 40);
            subTitlePanel.setBorder(new MatteBorder(1, 0, 0, 2, Color.white));
            subTitlePanel.setPreferredSize(new Dimension(800, 40));
            subTitlePanel.setMinimumSize(new Dimension(500, 40));
        }

        return subTitlePanel;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setPreferredSize(new Dimension(870, 768));
        setLayout(null);
        add(getBView());
        add(getVView());
        add(getSubTitle());
    }

}

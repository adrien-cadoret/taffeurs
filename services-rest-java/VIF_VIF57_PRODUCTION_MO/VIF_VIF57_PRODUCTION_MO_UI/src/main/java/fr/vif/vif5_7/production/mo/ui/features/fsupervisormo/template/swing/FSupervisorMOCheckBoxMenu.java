/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOCheckBoxMenu.java,v $
 * Created on 8 déc. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing;


import javax.swing.JCheckBoxMenuItem;
import javax.swing.JToolTip;

import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOFactory;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOIMenu;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;


/**
 * Chech box menu linked to a local frame.
 * 
 * @author jd
 */
public class FSupervisorMOCheckBoxMenu extends JCheckBoxMenuItem implements FSupervisorMOIMenu {

    private MOLocalFrame localFrame = null;

    /**
     * Constructor.
     */
    public FSupervisorMOCheckBoxMenu() {

    }

    /**
     * Constructor.
     * 
     * @param localFrame {@link MOLocalFrame}
     */
    public FSupervisorMOCheckBoxMenu(final MOLocalFrame localFrame) {
        super();
        this.localFrame = localFrame;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JToolTip createToolTip() {
        String img = FSupervisorMOFactory.getImgToolTip(getLocalFrame());
        JToolTip tip = new FIndicatorMOImgToolTip(img);
        tip.setLocation(0, 0);
        return tip;
    }

    /**
     * {@inheritDoc}
     */
    public MOLocalFrame getLocalFrame() {
        return localFrame;
    }

    /**
     * Sets the localFrame.
     * 
     * @param localFrame localFrame.
     */
    public void setLocalFrame(final MOLocalFrame localFrame) {
        this.localFrame = localFrame;
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DBoningPackagingCtrl.java,v $
 * Created on 9 oct. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.dialog.boningpackaging;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.dialogs.DialogView;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.models.identification.Identification;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.gen.packaging.business.beans.common.packaging.Packaging;
import fr.vif.vif5_7.gen.packaging.business.beans.common.tattooed.TattooedKey;
import fr.vif.vif5_7.gen.packaging.business.beans.common.tattooed.TattooedPackaging;
import fr.vif.vif5_7.gen.packaging.business.beans.dialog.DPackagingBean;
import fr.vif.vif5_7.gen.packaging.business.beans.dialog.DPackagingBeanEnum;
import fr.vif.vif5_7.gen.packaging.business.beans.features.fpackaging.FPackagingSBean;
import fr.vif.vif5_7.gen.packaging.ui.dialog.DPackagingIView;
import fr.vif.vif5_7.gen.packaging.ui.dialog.DPackagingModel;
import fr.vif.vif5_7.gen.packaging.ui.dialog.swing.DPackagingView;
import fr.vif.vif5_7.gen.packaging.ui.dialog.touch.DPackagingTouchView;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.dboningpackaging.DBoningPackagingCBS;
import fr.vif.vif5_7.stock.kernel.StockHelper;
import fr.vif.vif5_7.stock.kernel.ui.util.StockUITools;


/**
 * Special packaging dialog for boning and tattooed number.
 * 
 * @author cj
 */
public class DBoningPackagingCtrl extends AbstractStandardDialogController {

    /** The Constant LOGGER. */
    private static final Logger    LOGGER           = Logger.getLogger(DBoningPackagingCtrl.class);

    /** We save last used selection because it will be used in bean validation. */
    private static FPackagingSBean currentSelection = null;

    private DBoningPackagingCBS    dpackagingCBS;

    /**
     * Instantiates a new Packaging input Dialog controller.
     */
    public DBoningPackagingCtrl() {
    }

    /**
     * Show the dialog to manage packaging.
     * 
     * @param featureView featureView which call
     * @param changeListener change Listener
     * @param displayListener display Listener.
     * @param identification Identification
     * @param bean Dialog bean to create
     */
    public static void showPackaging(final FeatureView featureView, final DialogChangeListener changeListener,
            final DisplayListener displayListener, final Identification identification, final DPackagingBean bean) {
        FPackagingSBean selection = new FPackagingSBean(bean.getKey().getCsoc());
        showPackaging(featureView, changeListener, displayListener, identification, bean, selection, null);
    }

    /**
     * 
     * Show the dialog to manage packaging with a selection.
     * 
     * @param featureView featureView which call
     * @param changeListener change Listener
     * @param displayListener display Listener.
     * @param identification Identification
     * @param bean Dialog bean to create
     * @param selection selection
     * @param additionnalTitle additional title
     */
    public static void showPackaging(final FeatureView featureView, final DialogChangeListener changeListener,
            final DisplayListener displayListener, final Identification identification, final DPackagingBean bean,
            final FPackagingSBean selection, final String additionnalTitle) {

        if (featureView != null) {

            // Save selection
            currentSelection = selection;
            DPackagingModel model = new DPackagingModel();
            model.setIdentification(identification);
            DialogView view = null;

            if (StockUITools.isTouchRunning()) {
                view = featureView.openDialog(DPackagingTouchView.class);
            } else {
                view = featureView.openDialog(DPackagingView.class);
            }
            view.setModel(model);
            // manage attributes
            ((DPackagingIView) view).setAttributes(identification.getCompany(), identification.getEstablishment());
            ((DPackagingIView) view).setSelection(currentSelection);
            // set Title
            String title = "";
            if (additionnalTitle != null && !additionnalTitle.isEmpty()) {
                title = " - " + additionnalTitle;
            }
            if (currentSelection.getSuperiorPackagingOnly()) {
                view.setTitle(I18nClientManager.translate(StockKernel.T29717) + title);
            } else {
                view.setTitle(I18nClientManager.translate(StockKernel.T1588) + title);
            }

            DBoningPackagingCtrl ctrl;
            try {
                ctrl = StandardControllerFactory.getInstance().getController(DBoningPackagingCtrl.class);
                ctrl.setDialogView(view);
                ctrl.addDialogChangeListener(changeListener);
                ctrl.addDisplayListener(displayListener);

                model.setDialogBean(bean);
                ctrl.initialize();

                ctrl.removeDialogChangeListener(changeListener);
                ctrl.removeDisplayListener(displayListener);
            } catch (UIException e) {
                view.show(e);
            }
        } else {
            if (LOGGER.isEnabledFor(Level.WARN)) {
                LOGGER.warn("DBoningPackagingCtrl warning : feature view is null");
            }
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beanValidation(final Object dialogBean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - beanValidation(dialogBean=" + dialogBean + ")");
        }

        super.beanValidation(dialogBean);

        DPackagingBean dpackagingBean = (DPackagingBean) dialogBean;
        try {

            // Check dialog bean
            dpackagingCBS.checkDBean(getIdCtx(), getCreationMode(), dpackagingBean);

            if (!dpackagingBean.getNotTattooedPackagingOnly() && dpackagingBean.getProperties().getTtat()) {
                // If there is a tattooed id, we have to retrieve tare from it
                TattooedKey key = new TattooedKey(getIdentification().getCompany(), getIdentification()
                        .getEstablishment(), dpackagingBean.getPackagingCL().getCode(), dpackagingBean.getKey()
                        .getNotat());
                TattooedPackaging tp = dpackagingCBS.getTattooedPackaging(getIdCtx(), key);
                dpackagingBean.setTare(tp.getTare());
                dpackagingBean.setKey(key);
                dpackagingBean.setReferences(tp.getReferences());
            }

        } catch (BusinessException e) {
            throw new UIException(e);
        } catch (BusinessErrorsException e) {
            UIErrorsException uie = new UIErrorsException(e);
            throw uie;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - beanValidation(dialogBean=" + dialogBean + ")");
        }
    }

    /**
     * Gets the packaging dialog CBS.
     * 
     * @return the packaging dialog CBS
     */
    public DBoningPackagingCBS getDpackagingCBS() {
        return dpackagingCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        DPackagingBean dPackagingBean = ((DPackagingBean) getDialogBean());
        // As selection is used, we have to be coherent...
        dPackagingBean.setNotTattooedPackagingOnly(currentSelection.getNotTattooedPackagingOnly());
        dPackagingBean.setOnlyAvailableTattooed(currentSelection.getOnlyAvailableTattooedNumber());

        if (dPackagingBean.getPackagingCL() != null || StockHelper.isValid(dPackagingBean.getPackagingCL().getCode())
                && dPackagingBean.getProperties() != null && dPackagingBean.getProperties().getTtat()) {
            // Show tattooed If we have to manage it
            if (!currentSelection.getNotTattooedPackagingOnly()) {

                ((DPackagingIView) getView()).setTattooedVisible(dPackagingBean.getProperties().getTtat());
                ((DPackagingIView) getView()).setTattooedPackagingCode(dPackagingBean.getPackagingCL().getCode());
            }
        }
        super.initialize();
    }

    /**
     * Sets the packaging dialog CBS.
     * 
     * @param dpackagingCBS the new packaging dialog CBS
     */
    public void setDpackagingCBS(final DBoningPackagingCBS dpackagingCBS) {
        this.dpackagingCBS = dpackagingCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
        DPackagingBean dPackagingBean = ((DPackagingBean) getDialogBean());

        // Packaging Code label
        if (DPackagingBeanEnum.PACKAGING_CL_CODE.getValue().equalsIgnoreCase(beanProperty)) {
            // Check code-label
            if (dPackagingBean.getPackagingCL() == null || "".equals(dPackagingBean.getPackagingCL().getCode())
                    || "".equals(dPackagingBean.getPackagingCL().getLabel())) {
                throw new UIException(Generic.T12640, new VarParamTranslation(I18nClientManager.translate(
                        StockKernel.T1588, false)));
            }
            // Retrieve packaging information from packaging view
            Packaging packaging = ((DPackagingIView) getView()).getCurrentPackaging();
            dPackagingBean.setProperties(packaging.getProperties());
            dPackagingBean.setTare(packaging.getTare());
            dPackagingBean.getKey().setCcond(dPackagingBean.getPackagingCL().getCode());

            // Show tattooed If we have to manage it
            if (!currentSelection.getNotTattooedPackagingOnly()) {
                ((DPackagingIView) getView()).setTattooedVisible(dPackagingBean.getProperties().getTtat());
                ((DPackagingIView) getView()).setTattooedPackagingCode(dPackagingBean.getPackagingCL().getCode());
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }
}

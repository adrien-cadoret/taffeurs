/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownGSView.java,v $
 * Created on 7 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import fr.vif.jtech.ui.button.SwingBtnStd;
import fr.vif.jtech.ui.input.swing.SwingInputLabel;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.selection.swing.StandardSwingGeneralSelection;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.activities.activities.ui.composites.cproductionresource.CProductionResourceCtrl;
import fr.vif.vif5_7.activities.activities.ui.composites.cproductionresource.swing.CProductionResourceView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownSBeanEnum;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.FTimeBreakDownFCtrl;


/**
 * Selection view.
 * 
 * @author cj
 */
public class FTimeBreakDownGSView extends StandardSwingGeneralSelection {
    private JSeparator              jSeparator1        = null;
    private SwingBtnStd             btnBreakdown       = null;
    private SwingBtnStd             btnPeriodWithoutMO = null;
    private SwingInputLabel         lblProdDate        = null;
    private SwingInputTextField     itfProdDate        = null;
    private SwingInputLabel         lblResource        = null;
    private CProductionResourceView itfResource        = null;
    private SwingInputLabel         lblLabor           = null;
    private CProductionResourceView itfLabor           = null;

    private JPanel                  jPanelInterval     = null;

    /**
     * ctor.
     */
    public FTimeBreakDownGSView() {
        super();
        initialize();
    }

    /**
     * Gets the btnBreakdown.
     * 
     * @return the btnBreakdown.
     */
    public SwingBtnStd getBtnBreakdown() {
        if (btnBreakdown == null) {
            btnBreakdown = new SwingBtnStd();
            btnBreakdown.setPreferredSize(new Dimension(100, 50));
            btnBreakdown.setText(I18nClientManager.translate(ProductionMo.T36170, false));
            btnBreakdown.setReference(FTimeBreakDownFCtrl.BTN_BREAKDOWN);
        }
        return btnBreakdown;
    }

    /**
     * Gets the btnPeriodWithoutMO.
     * 
     * @return the btnPeriodWithoutMO.
     */
    public SwingBtnStd getBtnPeriodWithoutMO() {
        if (btnPeriodWithoutMO == null) {
            btnPeriodWithoutMO = new SwingBtnStd();
            btnPeriodWithoutMO.setPreferredSize(new Dimension(100, 50));
            btnPeriodWithoutMO.setText("<html><center>" + I18nClientManager.translate(ProductionMo.T34846, false)
                    + "</center></html>");
            btnPeriodWithoutMO.setHorizontalAlignment(JLabel.CENTER);
            btnPeriodWithoutMO.setReference(FTimeBreakDownFCtrl.BTN_PERIOD_WITHOUT_MO);
        }
        return btnPeriodWithoutMO;
    }

    /**
     * Gets the itfLabor.
     * 
     * @return the itfLabor.
     */
    public CProductionResourceView getItfLabor() {
        if (itfLabor == null) {
            itfLabor = new CProductionResourceView(CProductionResourceCtrl.class, getLblLabor().getJLabel());
            itfLabor.setBeanProperty(FTimeBreakDownSBeanEnum.LABOR_CL.getValue());
        }
        return itfLabor;
    }

    /**
     * Gets the itfProdDate.
     * 
     * @return the itfProdDate.
     */
    public SwingInputTextField getItfProdDate() {
        if (itfProdDate == null) {
            itfProdDate = new SwingInputTextField(Date.class, "dd/MM/yyyy", false, getLblProdDate().getJLabel());
            itfProdDate.setBeanProperty(FTimeBreakDownSBeanEnum.PRODDATE.getValue());
        }
        return itfProdDate;
    }

    /**
     * Gets the itfResource.
     * 
     * @return the itfResource.
     */
    public CProductionResourceView getItfResource() {
        if (itfResource == null) {
            itfResource = new CProductionResourceView(CProductionResourceCtrl.class, getLblResource().getJLabel());
            itfResource.setBeanProperty(FTimeBreakDownSBeanEnum.RESOURCE_CL.getValue());
        }
        return itfResource;
    }

    /**
     * Gets the jPanelInterval.
     * 
     * @return the jPanelInterval.
     */
    public JPanel getjPanelInterval() {
        if (jPanelInterval == null) {
            jPanelInterval = new JPanel();
        }
        return jPanelInterval;
    }

    /**
     * Gets the jSeparator1.
     * 
     * @return the jSeparator1.
     */
    public JSeparator getjSeparator1() {
        if (jSeparator1 == null) {
            jSeparator1 = new JSeparator();
            jSeparator1.setOrientation(SwingConstants.VERTICAL);
        }
        return jSeparator1;
    }

    /**
     * Gets the lblLabor.
     * 
     * @return the lblLabor.
     */
    public SwingInputLabel getLblLabor() {
        if (lblLabor == null) {
            lblLabor = new SwingInputLabel();
            lblLabor.setValue(I18nClientManager.translate(ProductionMo.T36171, false) + " : ");
        }
        return lblLabor;
    }

    /**
     * Gets the lblProdDate.
     * 
     * @return the lblProdDate.
     */
    public SwingInputLabel getLblProdDate() {
        if (lblProdDate == null) {
            lblProdDate = new SwingInputLabel();
            lblProdDate.setValue(I18nClientManager.translate(ProductionMo.T34329, false) + " : ");
        }
        return lblProdDate;
    }

    /**
     * Gets the lblResource.
     * 
     * @return the lblResource.
     */
    public SwingInputLabel getLblResource() {
        if (lblResource == null) {
            lblResource = new SwingInputLabel();
            lblResource.setValue(I18nClientManager.translate(ProductionMo.T34865, false) + " : ");
        }
        return lblResource;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        this.setLayout(new GridBagLayout());

        GridBagConstraints gbLblProddate = new GridBagConstraints();
        gbLblProddate.gridx = 0;
        gbLblProddate.gridy = 0;
        gbLblProddate.fill = GridBagConstraints.VERTICAL;
        gbLblProddate.anchor = GridBagConstraints.WEST;
        gbLblProddate.weightx = 0;
        gbLblProddate.weighty = 1;
        gbLblProddate.insets = new Insets(5, 5, 5, 0);

        GridBagConstraints gbItfProddate = new GridBagConstraints();
        gbItfProddate.gridx = 1;
        gbItfProddate.gridy = 0;
        gbItfProddate.fill = GridBagConstraints.BOTH;
        gbItfProddate.anchor = GridBagConstraints.WEST;
        gbItfProddate.weightx = 0;
        gbItfProddate.weighty = 1;
        gbItfProddate.insets = new Insets(5, 5, 5, 0);

        GridBagConstraints gbLblResource = new GridBagConstraints();
        gbLblResource.gridx = 2;
        gbLblResource.gridy = 0;
        gbLblResource.fill = GridBagConstraints.VERTICAL;
        gbLblResource.anchor = GridBagConstraints.WEST;
        gbLblResource.weightx = 0;
        gbLblResource.weighty = 1;
        gbLblResource.insets = new Insets(5, 5, 5, 0);

        GridBagConstraints gbItfResource = new GridBagConstraints();
        gbItfResource.gridx = 3;
        gbItfResource.gridy = 0;
        gbItfResource.fill = GridBagConstraints.BOTH;
        gbItfResource.anchor = GridBagConstraints.WEST;
        gbItfResource.weightx = 0.5;
        gbItfResource.weighty = 1;
        gbItfResource.insets = new Insets(5, 5, 5, 0);

        GridBagConstraints gbLblLabor = new GridBagConstraints();
        gbLblLabor.gridx = 4;
        gbLblLabor.gridy = 0;
        gbLblLabor.fill = GridBagConstraints.VERTICAL;
        gbLblLabor.anchor = GridBagConstraints.WEST;
        gbLblLabor.weightx = 0;
        gbLblLabor.weighty = 1;
        gbLblLabor.insets = new Insets(5, 15, 5, 0);

        GridBagConstraints gbItfLabor = new GridBagConstraints();
        gbItfLabor.gridx = 5;
        gbItfLabor.gridy = 0;
        gbItfLabor.fill = GridBagConstraints.BOTH;
        gbItfLabor.anchor = GridBagConstraints.WEST;
        gbItfLabor.weightx = 0.5;
        gbItfLabor.weighty = 1;
        gbItfLabor.insets = new Insets(5, 5, 5, 0);

        GridBagConstraints gbJpanelInterval = new GridBagConstraints();
        gbJpanelInterval.gridx = 6;
        gbJpanelInterval.gridy = 0;
        gbJpanelInterval.fill = GridBagConstraints.BOTH;
        gbJpanelInterval.anchor = GridBagConstraints.WEST;
        gbJpanelInterval.weightx = 0.7;
        gbJpanelInterval.weighty = 0.5;
        gbJpanelInterval.gridwidth = 2;
        gbJpanelInterval.gridheight = 2;

        GridBagConstraints gbJseparator1 = new GridBagConstraints();
        gbJseparator1.gridx = 8;
        gbJseparator1.gridy = 0;
        gbJseparator1.fill = GridBagConstraints.VERTICAL;
        gbJseparator1.anchor = GridBagConstraints.EAST;
        gbJseparator1.weightx = 0;
        gbJseparator1.weighty = 1;
        gbJseparator1.insets = new Insets(0, 5, 0, 5);
        gbJseparator1.gridheight = 2;

        GridBagConstraints gbBtnBreakdown = new GridBagConstraints();
        gbBtnBreakdown.gridx = 9;
        gbBtnBreakdown.gridy = 0;
        gbBtnBreakdown.fill = GridBagConstraints.NONE;
        gbBtnBreakdown.anchor = GridBagConstraints.EAST;
        gbBtnBreakdown.weightx = 0;
        gbBtnBreakdown.weighty = 1;
        gbBtnBreakdown.insets = new Insets(5, 5, 5, 5);
        gbBtnBreakdown.gridheight = 2;

        GridBagConstraints gbBtnPeriodWithoutMO = new GridBagConstraints();
        gbBtnPeriodWithoutMO.gridx = 10;
        gbBtnPeriodWithoutMO.gridy = 0;
        gbBtnPeriodWithoutMO.fill = GridBagConstraints.NONE;
        gbBtnPeriodWithoutMO.anchor = GridBagConstraints.EAST;
        gbBtnPeriodWithoutMO.weightx = 0;
        gbBtnPeriodWithoutMO.weighty = 1;
        gbBtnPeriodWithoutMO.insets = new Insets(5, 5, 5, 5);
        gbBtnPeriodWithoutMO.gridheight = 2;

        this.add(getLblProdDate(), gbLblProddate);
        this.add(getItfProdDate(), gbItfProddate);
        this.add(getLblResource(), gbLblResource);
        this.add(getItfResource(), gbItfResource);
        this.add(getLblLabor(), gbLblLabor);
        this.add(getItfLabor(), gbItfLabor);

        this.add(getjPanelInterval(), gbJpanelInterval);
        this.add(getjSeparator1(), gbJseparator1);
        this.add(getBtnBreakdown(), gbBtnBreakdown);
        this.add(getBtnPeriodWithoutMO(), gbBtnPeriodWithoutMO);

    }
}

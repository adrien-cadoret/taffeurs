/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMSOCreationVModel.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationVBean;


/**
 * MSO creation viewer model.
 * 
 * @author glc
 */
public class FBoningMSOCreationVModel extends ViewerModel<FBoningMSOCreationVBean> {

    public static final String MO_CHRONO     = "moKey.chrono.chrono";
    public static final String MO_ITEM       = "moItemCL";
    public static final String MO_LABEL      = "moLabel";
    public static final String QUANTITY_UNIT = "quantityUnit";
    public static final String QUANTITY      = "quantityUnit.qty";

    /**
     * Simple constructor.
     */
    public FBoningMSOCreationVModel() {
        super();
        setBeanClass(FBoningMSOCreationVBean.class);
    }

}

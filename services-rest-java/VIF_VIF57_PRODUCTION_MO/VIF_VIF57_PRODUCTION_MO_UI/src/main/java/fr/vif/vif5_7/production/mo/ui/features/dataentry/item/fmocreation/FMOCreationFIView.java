/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationFIView.java,v $
 * Created on 13 janv. 09 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


/**
 * view of feature.
 *
 * @author gv
 */
public interface FMOCreationFIView {

    /**
     * set the subTitle of the View.
     * 
     * @param subTitle subt
     */
    public void setSubTitle(String subTitle);

    /**
     * get the subTitle.
     * 
     * @return the subt
     */
    public String getSubTitle();

}

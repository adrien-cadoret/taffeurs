/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSubstituteItemBCtrl.java,v $
 * Created on 19 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.fsubstituteitem;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fsubstituteitem.FSubstituteItemSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fsubstituteitem.FSubstituteItemCBS;


/**
 * SubstituteItem : Browser Controller.
 * 
 * @author vr
 */
public class FSubstituteItemBCtrl extends AbstractBrowserController<CodeLabel> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FSubstituteItemBCtrl.class);

    private FSubstituteItemCBS  fsubstituteItemCBS;

    /**
     * Default constructor.
     * 
     */
    public FSubstituteItemBCtrl() {
        super();
    }

    /**
     * Gets the fsubstituteItemCBS.
     * 
     * @category getter
     * @return the fsubstituteItemCBS.
     */
    public final FSubstituteItemCBS getFsubstituteItemCBS() {
        return fsubstituteItemCBS;
    }

    /**
     * Sets the fsubstituteItemCBS.
     * 
     * @category setter
     * @param fsubstituteItemCBS fsubstituteItemCBS.
     */
    public final void setFsubstituteItemCBS(final FSubstituteItemCBS fsubstituteItemCBS) {
        this.fsubstituteItemCBS = fsubstituteItemCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<CodeLabel> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<CodeLabel> lst = null;
        // FSubstituteItemSBean sBean = new FSubstituteItemSBean();
        //
        // OperationItemKey oik = new OperationItemKey();
        // oik.setEstablishmentKey(new EstablishmentKey("TG", "01"));
        // oik.setChrono(new Chrono("1OF", 212));
        // oik.setCounter1(1);
        // oik.setCounter2(1);
        // oik.setCounter3(1);
        // oik.setCounter4(1);
        // oik.setManagementType(ManagementType.REAL);
        // sBean.setOperationItemKey(oik);
        // sBean.setWorkstationId("$$");
        // sBean.setActivityItemType(ActivityItemType.INPUT);
        // sBean.setAll(false);

        try {
            lst = getFsubstituteItemCBS().queryElements(getIdCtx(), (FSubstituteItemSBean) selection, startIndex,
                    rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return lst;
    }

}

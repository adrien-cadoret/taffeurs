/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractMODockableView.java,v $
 * Created on 21 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JComponent;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.JTable;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.generic.GenericActionListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.identification.Identification;
import fr.vif.jtech.ui.models.mvc.MVCTriad;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.swing.SwingHelper;
import fr.vif.jtech.ui.viewer.AbstractViewerController;
import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.FSupervisorMOFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOIFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FSupervisorMOPanel;


/**
 * Viewer view for internal frame.
 * 
 * @author jd
 * @param <V> viewer bean
 * @param <C> viewer controller
 * 
 */
public abstract class AbstractMODockableView<V, C extends AbstractViewerController<V>> extends JPanel implements
        GenericActionListener, FSupervisorMOIFrame, ActionListener {
    /** LOGGER. */
    private static final Logger                                            LOGGER           = Logger.getLogger(AbstractMODockableView.class);

    private List<ActionListener>                                           actionListeners  = new ArrayList<ActionListener>();
    private AbstractBrowserController<?>                                   browserController;

    private MOLocalFrame                                                   localFrame       = null;

    private JComponent                                                     panel            = null;

    // private JScrollPane pane = null;
    private FSupervisorMOFCtrl                                             parentController;
    private JPopupMenu                                                     popup            = null;

    private MVCTriad<? extends ViewerModel<V>, ? extends ViewerView<V>, C> triad;
    private MVCTriad<? extends ViewerModel<V>, ? extends ViewerView<V>, C> triadParameter;

    private C                                                              viewerController = null;

    /**
     * Constructor.
     * 
     * @param localFrame MOLocalFrame
     * @param triad triad
     */
    public AbstractMODockableView(final MOLocalFrame localFrame,
            final MVCTriad<? extends ViewerModel<V>, ? extends ViewerView<V>, C> triad) {
        super();
        this.localFrame = localFrame;
        this.triad = triad;
        initializeView();
    }

    /**
     * Constructor.
     * 
     * @param title title
     * @param triad {@link MVCTriad}
     * @param ident {@link Identification}
     * @param posY position x
     * @param posX position y
     * @param preferredSize {@link Dimension}
     * @deprecated use constructor with
     *             {@link fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOFrameBean}
     */
    @Deprecated
    public AbstractMODockableView(final String title,
            final MVCTriad<? extends ViewerModel<V>, ? extends ViewerView<V>, C> triad, final Identification ident,
            final int posX, final int posY, final Dimension preferredSize) {
        super();
        this.setPreferredSize(preferredSize);
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {

        for (ActionListener l : actionListeners) {
            l.actionPerformed(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {

    }

    /**
     * {@inheritDoc}
     */
    public void addActionlistener(final ActionListener listener) {
        this.actionListeners.add(listener);
    }

    /**
     * {@inheritDoc}
     */
    public void addGenericActionlistener(final GenericActionListener listener) {
        List<FSupervisorMOPanel> list = SwingHelper.findComponentsInstanceOf(this, FSupervisorMOPanel.class, true);
        for (FSupervisorMOPanel panels : list) {
            panels.addGenericActionListener(listener);
        }

    }

    /**
     * Convert.
     */
    public abstract void convert();

    /**
     * Gets the browserController.
     * 
     * @return the browserController.
     */
    public AbstractBrowserController<?> getBrowserController() {
        return browserController;
    }

    /**
     * Gets the localFrame.
     * 
     * @return the localFrame.
     */
    public MOLocalFrame getLocalFrame() {
        return localFrame;
    }

    /**
     * get the FSupervisorController.
     * 
     * @return FSupervisorMOFCtrl
     */
    public FSupervisorMOFCtrl getParentController() {
        return parentController;
    }

    /**
     * Gets the triadParameter.
     *
     * @return the triadParameter.
     */
    public MVCTriad<? extends ViewerModel<V>, ? extends ViewerView<V>, C> getTriadParameter() {
        return triadParameter;
    }

    /**
     * Gets the viewerController.
     * 
     * @return the viewerController.
     */
    public C getViewerController() {
        return viewerController;
    }

    /**
     * {@inheritDoc}
     */
    public void initialize() {

    }

    /**
     * {@inheritDoc}
     */
    public void refresh() {
        if (browserController != null) {
            browserController.reopenQuery();
        }
        convert();
        this.repaint();
    }

    /**
     * Sets the localFrame.
     * 
     * @param moLocalFrame localFrame.
     */
    public void setMOLocalFrame(final MOLocalFrame moLocalFrame) {
        this.localFrame = moLocalFrame;
    }

    /**
     * Sets the parentController.
     * 
     * @param parentController parentController.
     */
    public void setParentController(final FSupervisorMOFCtrl parentController) {
        this.parentController = parentController;
        List<FSupervisorMOPanel> list2 = SwingHelper.findComponentsInstanceOf(this, FSupervisorMOPanel.class, true);
        for (FSupervisorMOPanel moPanel : list2) {
            moPanel.setFeatureController(parentController);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelection(final Selection selection) {

    }

    /**
     * Sets the triadParameter.
     *
     * @param triadParameter triadParameter.
     */
    public void setTriadParameter(final MVCTriad<? extends ViewerModel<V>, ? extends ViewerView<V>, C> triadParameter) {
        this.triadParameter = triadParameter;
    }

    /**
     * Sets the viewerController.
     * 
     * @param viewerController viewerController.
     */
    public void setViewerController(final C viewerController) {
        this.viewerController = viewerController;
    }

    /**
     * Set the view port of the jscroll pane.
     * 
     * @param component component
     */
    public void setViewPort(final JComponent component) {
        this.add(component);
        component.setComponentPopupMenu(popup);
        // this.pack();
    }

    /**
     * init.
     */
    private void initializeView() {
        setLayout(new GridBagLayout());

        // No border.
        this.setBorder(null); // BorderFactory.createRaisedBevelBorder());

        // Translucid
        this.setOpaque(false);

        // this.setLocation(getInitBean().getPosX(), getInitBean().getPosY());
        // this.setPreferredSize(getInitBean().getPreferedSize());

        this.setVisible(true);

        // createPopupMenu();

        try {
            if (triad != null) { // FIXME Pour test seulement
                viewerController = StandardControllerFactory.getInstance().getController(triad.getControllerClass());

                viewerController.setView(triad.getViewClass().newInstance());
                viewerController.getView().setModel(triad.getModelClass().newInstance());
                // FIXME viewerController.setIdentification(this.getInitBean().getIdentification());
                panel = (JComponent) viewerController.getView();
            } else {
                panel = new JPanel();
                panel.add(new JTable(10, 10));
                panel.setBackground(Color.GREEN);
                panel.setPreferredSize(new Dimension(300, 300));
                panel.setVisible(true);
            }
            GridBagConstraints g = new GridBagConstraints();
            g.weightx = 1;
            g.weighty = 1;
            g.gridx = 0;
            g.gridy = 0;
            g.fill = GridBagConstraints.BOTH;
            add(panel, g);
        } catch (UIException e) {
            if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                LOGGER.error("initializeView()", e);
            }
        } catch (InstantiationException e) {
            if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                LOGGER.error("initializeView()", e);
            }
        } catch (IllegalAccessException e) {
            if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                LOGGER.error("initializeView()", e);
            }
        }

        addGenericActionlistener(this);
    }

}

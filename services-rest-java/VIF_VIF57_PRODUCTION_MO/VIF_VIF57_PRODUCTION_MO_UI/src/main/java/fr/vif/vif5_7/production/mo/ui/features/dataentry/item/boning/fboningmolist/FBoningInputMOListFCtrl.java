/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningInputMOListFCtrl.java,v $
 * Created on 14 Mar. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist;


import java.util.List;

import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.Key;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.system.SystemHelper;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;


/**
 * Input Boning MO List : Feature Controller. <br>
 * The name of this class is used as reference in the FeaturesId.
 * 
 * @author cj
 */
public class FBoningInputMOListFCtrl extends AbstractFBoningMOListFCtrl {

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        super.actionPerformed(event);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();
        getBrowserController().requestFocus();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        List<ToolBarBtnStdBaseModel> lstBtn = super.getActionsButtonModels();

        ToolBarBtnFeatureModel moCreate = new ToolBarBtnFeatureModel();
        moCreate.setReference(AbstractFBoningMOListFCtrl.MO_CREATE_BTN);
        moCreate.setIconURL("/images/vif/vif57/production/mo/newMO.png");
        moCreate.setFeatureId("VIF.BONMOCR");
        moCreate.setText(I18nClientManager.translate(ProductionMo.T29492, false));
        moCreate.setShortcut(Key.K_F3);
        lstBtn.add(moCreate);

        ToolBarBtnFeatureModel msoCreate = new ToolBarBtnFeatureModel();
        msoCreate.setReference(AbstractFBoningMOListFCtrl.MO_CREATE_MSO_BTN);
        msoCreate.setIconURL("/images/vif/vif57/production/mo/newMSO.png");
        msoCreate.setFeatureId("VIF.BONMSOCR");
        msoCreate.setText(I18nClientManager.translate(ProductionMo.T29494, false));
        msoCreate.setShortcut(Key.K_F4);
        lstBtn.add(msoCreate);

        ToolBarBtnFeatureModel moInput = new ToolBarBtnFeatureModel();
        moInput.setReference(AbstractFBoningMOListFCtrl.MO_INPUT_BTN);
        moInput.setIconURL("/images/vif/vif57/production/mo/boninginput48.png");
        moInput.setFeatureId("VIF.BONIN");
        moInput.setText(I18nClientManager.translate(ProductionMo.T29527, false));
        moInput.setShortcut(Key.K_F6);
        lstBtn.add(moInput);

        ToolBarBtnStdModel resetTableFocus = new ToolBarBtnStdModel();
        resetTableFocus.setReference("resetTableFocus");
        resetTableFocus.setIconURL("");
        resetTableFocus.setText(I18nClientManager.translate(ProductionMo.T34424, false));
        resetTableFocus.setShortcut(Key.K_CTRL_K);

        if (SystemHelper.getUseKeyboard()) {
            lstBtn.add(resetTableFocus);
        }

        return lstBtn;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownGSCtrl.java,v $
 * Created on 7 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.selection.AbstractGeneralSelectionController;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ResourceType;
import fr.vif.vif5_7.activities.activities.ui.composites.cproductionresource.CProductionResourceCtrl;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownSBeanEnum;
import fr.vif.vif5_7.production.mo.business.services.features.ftimebreakdown.FTimeBreakDownCBS;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing.FTimeBreakDownGSView;


/**
 * Selection controller.
 * 
 * @author cj
 */
public class FTimeBreakDownGSCtrl extends AbstractGeneralSelectionController {
    /** LOGGER. */
    private static final Logger LOGGER            = Logger.getLogger(FTimeBreakDownGSCtrl.class);

    // DP2192 - CHU - CBS to control resources
    private FTimeBreakDownCBS   fTimeBreakDownCBS = null;

    /**
     * Gets the fTimeBreakDownCBS.
     *
     * @return the fTimeBreakDownCBS.
     */
    public FTimeBreakDownCBS getfTimeBreakDownCBS() {
        return fTimeBreakDownCBS;
    }

    @Override
    public void initialize() {
        super.initialize();
        if (getBean() == null) {
            FTimeBreakDownSBean sBean = new FTimeBreakDownSBean();
            sBean.setEstablishmentKey(new EstablishmentKey(getModel().getIdentification().getCompany(), getModel()
                    .getIdentification().getEstablishment()));

            getModel().setBean(sBean);

            ((CProductionResourceCtrl) ((FTimeBreakDownGSView) getView()).getItfResource().getController())
            .setAttribute(getIdentification().getCompany(), getIdentification().getEstablishment(), null,
                    ResourceType.ELEMENTARY_RESOURCE.getValue(), ResourceType.MACHINE.getValue());

            ((CProductionResourceCtrl) ((FTimeBreakDownGSView) getView()).getItfLabor().getController()).setAttribute(
                    getIdentification().getCompany(), getIdentification().getEstablishment(), null,
                    ResourceType.ELEMENTARY_RESOURCE.getValue(), ResourceType.MANPOWER.getValue());

        }
    }

    /**
     * Sets the fTimeBreakDownCBS.
     *
     * @param fTimeBreakDownCBS fTimeBreakDownCBS.
     */
    public void setfTimeBreakDownCBS(final FTimeBreakDownCBS fTimeBreakDownCBS) {
        this.fTimeBreakDownCBS = fTimeBreakDownCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException, UIErrorsException {
        // DP2192 - CHU - resource control
        try {

            if (FTimeBreakDownSBeanEnum.RESOURCE_CL_CODE.getValue().equals(beanProperty)
                    || FTimeBreakDownSBeanEnum.LABOR_CL_CODE.getValue().equals(beanProperty)) {
                getfTimeBreakDownCBS().validateResSBean(getIdCtx(), (FTimeBreakDownSBean) getBean());
            }
        } catch (BusinessException e1) {
            getView().show(new UIException(e1));
        }
    }

}

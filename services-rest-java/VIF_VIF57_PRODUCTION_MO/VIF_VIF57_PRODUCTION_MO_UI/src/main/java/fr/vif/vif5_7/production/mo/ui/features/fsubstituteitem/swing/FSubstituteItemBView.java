/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSubstituteItemBView.java,v $
 * Created on 19 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.fsubstituteitem.swing;


import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.browser.swing.SwingBrowser;


/**
 * SubstituteItem : Browser View.
 * 
 * @author vr
 */
public class FSubstituteItemBView extends SwingBrowser<CodeLabel> {

    /**
     * Default constructor.
     *
     */
    public FSubstituteItemBView() {
        super();

    }

}

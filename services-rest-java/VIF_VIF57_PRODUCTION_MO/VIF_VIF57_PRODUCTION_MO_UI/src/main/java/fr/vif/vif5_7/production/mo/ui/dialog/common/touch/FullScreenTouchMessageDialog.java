/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FullScreenTouchMessageDialog.java,v $
 * Created on 29 avr. 2014 by dd
 */
package fr.vif.vif5_7.production.mo.ui.dialog.common.touch;


import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.SwingConstants;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.dialogs.touch.StandardTouchDialog;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.models.format.StandardSize;
import fr.vif.jtech.ui.util.KeyHelper;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.jtech.ui.util.system.SystemHelper;
import fr.vif.jtech.ui.util.touch.MessageDialogController;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * The Full Screen Touch Message Dialog.<br/>
 * Like a {@link fr.vif.jtech.ui.util.touch.TouchMessageDialog TouchMessageDialog}, but windows will be maximized
 * according to message.
 * 
 * @author dd
 */
public final class FullScreenTouchMessageDialog extends StandardTouchDialog {

    private static final int WIDTH_MAX      = 1000;
    private static final int WIDTH_MIN      = 480;
    private static final int WIDTH_PADDING  = 150;
    private static final int HEIGHT_MAX     = 720;
    private static final int HEIGHT_MIN     = 380;
    private static final int HEIGHT_PADDING = 200;

    private JButton          noButton       = null;
    private JButton          yesButton      = null;
    private JLabel           iconLabel      = null;
    private JLabel           messageLabel   = null;
    private JPanel           messagePanel   = null;
    private JScrollPane      messageSP      = null;
    private JPanel           yesNoPanel     = null;

    private String           message;
    private int              messageType;
    private int              option;

    /**
     * Instantiates a new full screen touch message dialog.
     * 
     * @param view the view of the parent.
     * @param message the message to display.
     * @param messageType the message type.
     */
    private FullScreenTouchMessageDialog(final Dialog view, final String message, final int messageType) {
        super(view);
        this.message = message;
        this.messageType = messageType;
        initialize();
    }

    /**
     * Instantiates a new full screen touch message dialog.
     * 
     * @param view the view of the parent.
     * @param message the message to display.
     * @param messageType the message type.
     * @wbp.parser.constructor
     */
    private FullScreenTouchMessageDialog(final Frame view, final String message, final int messageType) {
        super(view);
        this.message = message;
        this.messageType = messageType;
        initialize();
    }

    /**
     * Shows a message.
     * 
     * @param parent the parent component of the message box.
     * @param title the title of the message.
     * @param message the message.
     * @param messageType the type of the message.
     */
    public static void showMessage(final Component parent, final String title, final String message,
            final int messageType) {
        FullScreenTouchMessageDialog dialogView;
        // May occurs if feature is not opened yet.
        if (parent == null) {
            dialogView = new FullScreenTouchMessageDialog((JDialog) parent, message, messageType);
        } else if (parent instanceof JDialog) {
            JDialog owner = (JDialog) parent;
            dialogView = new FullScreenTouchMessageDialog(owner, message, messageType);
        } else if (parent instanceof JFrame) {
            JFrame owner = (JFrame) parent;
            dialogView = new FullScreenTouchMessageDialog(owner, message, messageType);
        } else if (((JComponent) parent).getTopLevelAncestor() instanceof JDialog) {
            JDialog owner = (JDialog) ((JComponent) parent).getTopLevelAncestor();
            dialogView = new FullScreenTouchMessageDialog(owner, message, messageType);
        } else {
            JFrame owner = (JFrame) ((JComponent) parent).getTopLevelAncestor();
            dialogView = new FullScreenTouchMessageDialog(owner, message, messageType);
        }
        MessageDialogController dialogController = new MessageDialogController();
        dialogView.setTitle(title);
        dialogView.setOkVisible(false);
        dialogView.setCancelVisible(true);
        dialogView.setYesVisible(false);
        dialogView.setNoVisible(false);
        dialogController.setDialogView(dialogView);
        dialogController.initialize();
    }

    /**
     * Shows a question message.
     * 
     * @param parent the parent component of the message box.
     * @param title the title of the message.
     * @param message the message.
     * @param buttons available buttons.
     * @return the identifier of the chosen button (this is one of the available buttons).
     * @see MessageButtons for details about buttons.
     */
    public static int showQuestion(final Component parent, final String title, final String message, final int buttons) {
        FullScreenTouchMessageDialog dialogView;
        // May occurs if feature is not opened yet.
        if (parent == null) {
            dialogView = new FullScreenTouchMessageDialog((JDialog) parent, message, JOptionPane.QUESTION_MESSAGE);
        } else if (parent instanceof JDialog) {
            JDialog owner = (JDialog) parent;
            dialogView = new FullScreenTouchMessageDialog(owner, message, JOptionPane.QUESTION_MESSAGE);
        } else if (parent instanceof JFrame) {
            JFrame owner = (JFrame) parent;
            dialogView = new FullScreenTouchMessageDialog(owner, message, JOptionPane.QUESTION_MESSAGE);
        } else if (((JComponent) parent).getTopLevelAncestor() instanceof JDialog) {
            JDialog owner = (JDialog) ((JComponent) parent).getTopLevelAncestor();
            dialogView = new FullScreenTouchMessageDialog(owner, message, JOptionPane.QUESTION_MESSAGE);
        } else {
            JFrame owner = (JFrame) ((JComponent) parent).getTopLevelAncestor();
            dialogView = new FullScreenTouchMessageDialog(owner, message, JOptionPane.QUESTION_MESSAGE);
        }
        MessageDialogController dialogController = new MessageDialogController();
        dialogView.setTitle(title);
        switch (buttons) {
            case MessageButtons.OK_CANCEL:
                dialogView.setOkVisible(false);
                dialogView.setCancelVisible(true);
                dialogView.setYesVisible(true);
                dialogView.setNoVisible(false);
                break;
            case MessageButtons.YES_NO:
                dialogView.setOkVisible(false);
                dialogView.setCancelVisible(false);
                dialogView.setYesVisible(true);
                dialogView.setNoVisible(true);
                break;
            case MessageButtons.YES_NO_CANCEL:
                dialogView.setOkVisible(false);
                dialogView.setCancelVisible(true);
                dialogView.setYesVisible(true);
                dialogView.setNoVisible(true);
                break;
            default:
                dialogView.setYesVisible(true);
                dialogView.setNoVisible(true);
                dialogView.setOkVisible(false);
                dialogView.setCancelVisible(false);
                break;
        }
        dialogController.setDialogView(dialogView);
        dialogController.initialize();
        return dialogView.getOption();
    }

    /**
     * Gets the option chosen by the user.
     * 
     * @return the option chosen by the user.
     */
    public int getOption() {
        return option;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fireDialogCancelled() {
        option = MessageButtons.CANCEL;
        super.fireDialogCancelled();
    }

    /**
     * Calculate dialog dimension.
     * 
     * @param messageText the message text
     * @return the dimension
     */
    private Dimension calculateDialogDimension(final String messageText) {

        Dimension dimension = new Dimension(WIDTH_MIN, HEIGHT_MIN);

        if (messageText != null) {

            // Calculate number of line in message text
            int lineCount = messageText.trim().split("\n").length;

            // Retrieve longer line in text
            String longerLine = "";
            String[] lineList = messageText.trim().split("\n");
            for (String line : lineList) {
                if (line.length() > longerLine.length()) {
                    longerLine = line;
                }
            }

            // Calculate according to padding and font metrics
            FontMetrics metrics = getGraphics().getFontMetrics(getUsedFont());
            int width = metrics.stringWidth(longerLine) + WIDTH_PADDING;
            int height = (metrics.getHeight() * lineCount) + HEIGHT_PADDING;

            // Check limits
            if (width > WIDTH_MAX) {
                width = WIDTH_MAX;
            } else if (width < WIDTH_MIN) {
                width = WIDTH_MIN;
            }
            if (height > HEIGHT_MAX) {
                height = HEIGHT_MAX;
            } else if (height < HEIGHT_MIN) {
                height = HEIGHT_MIN;
            }
            dimension = new Dimension(width, height);
        }
        return dimension;
    }

    /**
     * Gets the messageLabel.
     * 
     * @return the messageLabel.
     */
    private JLabel getMessageLabel() {
        if (messageLabel == null) {
            messageLabel = new JLabel();
            messageLabel.setOpaque(false);
            messageLabel.setText(TouchHelper.getMultiLineText(message, false));
            messageLabel.setFont(getUsedFont());
            messageLabel.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_MESSAGE));
        }
        return messageLabel;
    }

    /**
     * Gets the messagePanel.
     * 
     * @return the messagePanel.
     */
    private JPanel getMessagePanel() {
        if (messagePanel == null) {
            GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
            gridBagConstraints3.gridy = 0;
            gridBagConstraints3.anchor = GridBagConstraints.WEST;
            gridBagConstraints3.gridx = 0;
            iconLabel = new JLabel();
            switch (messageType) {
                case JOptionPane.INFORMATION_MESSAGE:
                    iconLabel.setIcon(new ImageIcon(getClass().getResource("/images/jtech/touch/inform.png")));
                    break;
                case JOptionPane.WARNING_MESSAGE:
                    iconLabel.setIcon(new ImageIcon(getClass().getResource("/images/jtech/touch/warn.png")));
                    break;
                case JOptionPane.ERROR_MESSAGE:
                    iconLabel.setIcon(new ImageIcon(getClass().getResource("/images/jtech/touch/error.png")));
                    break;
                case JOptionPane.QUESTION_MESSAGE:
                    iconLabel.setIcon(new ImageIcon(getClass().getResource("/images/jtech/touch/question.png")));
                    break;
                // ALB DP2232
                case FProductionConstant.CRITERIA_ICON:
                    iconLabel.setIcon(new ImageIcon(getClass().getResource(
                            "/images/vif/vif57/production/mo/littleIcon_criteria.png")));
                    break;
                default:
                    break;
            }
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 1;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.anchor = GridBagConstraints.WEST;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.insets = new Insets(0, 10, 0, 10); // DP2232 - CHU
            gridBagConstraints.gridy = 0;
            messagePanel = new JPanel();
            messagePanel.setOpaque(false);
            messagePanel.setLayout(new GridBagLayout());
            if (message.length() > 200) {
                messagePanel.setSize(new Dimension(642, 117));
            } else {
                messagePanel.setSize(new Dimension(342, 117));
            }
            messagePanel.add(getMessageSP(), gridBagConstraints);
            messagePanel.add(iconLabel, gridBagConstraints3);
        }
        return messagePanel;
    }

    /**
     * Gets the messageSP.
     * 
     * @return the messageSP.
     */
    private JScrollPane getMessageSP() {
        if (messageSP == null) {
            messageSP = new JScrollPane(//
                    getMessageLabel(), //
                    JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, //
                    JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            messageSP.setBorder(BorderFactory.createEmptyBorder());
        }
        return messageSP;
    }

    /**
     * Gets the noButton.
     * 
     * @return the noButton.
     */
    private JButton getNoButton() {
        if (noButton == null) {
            noButton = new JButton();
            noButton.setFocusable(SystemHelper.getUseKeyboard());
            noButton.setVerticalTextPosition(SwingConstants.BOTTOM);
            noButton.setHorizontalTextPosition(SwingConstants.CENTER);
            noButton.setMinimumSize(new Dimension(TouchHelper.getSize(StandardSize.DEFAULT_BTN_WIDTH), TouchHelper
                    .getSize(StandardSize.DEFAULT_BTN_HEIGHT)));
            noButton.setPreferredSize(new Dimension(TouchHelper.getSize(StandardSize.DEFAULT_BTN_WIDTH), TouchHelper
                    .getSize(StandardSize.DEFAULT_BTN_HEIGHT)));
            noButton.setIcon(new ImageIcon(getClass().getResource("/images/jtech/touch/filedelete.png")));

            String initNoKey = Character.toString(I18nClientManager.translate(Jtech.T11274, false).charAt(0))
                    .toUpperCase();

            if (SystemHelper.getUseKeyboard()) {
                noButton.setText(TouchHelper.getBtnText(I18nClientManager.translate(Jtech.T11274, false) + " ("
                        + initNoKey + ")", TouchHelper.CENTER_ALIGN));
            } else {
                noButton.setText(TouchHelper.getBtnText(I18nClientManager.translate(Jtech.T11274, false),
                        TouchHelper.CENTER_ALIGN));
            }

            KeyStroke keyNo = KeyStroke.getKeyStroke(KeyHelper.findKeyStroke(initNoKey), 0);
            Action performNo = new AbstractAction(noButton.getText(), noButton.getIcon()) {
                public void actionPerformed(final ActionEvent e) {
                    option = MessageButtons.NO;
                    okPressed();
                }
            };
            noButton.setAction(performNo);
            noButton.getActionMap().put("performNo", performNo);
            noButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyNo, "performNo");

            noButton.registerKeyboardAction(
                    noButton.getActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                    KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), JComponent.WHEN_FOCUSED);
            noButton.registerKeyboardAction(
                    noButton.getActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                    KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
        }
        return noButton;
    }

    /**
     * Gets the used font.
     * 
     * @return the used font
     */
    private Font getUsedFont() {
        return TouchHelper.getFont(StandardFont.TOUCHSCREEN_MESSAGE_FONT);
    }

    /**
     * Gets the yesButton.
     * 
     * @return the yesButton.
     */
    private JButton getYesButton() {
        if (yesButton == null) {
            yesButton = new JButton();
            yesButton.setFocusable(SystemHelper.getUseKeyboard());
            yesButton.setVerticalTextPosition(SwingConstants.BOTTOM);
            yesButton.setHorizontalTextPosition(SwingConstants.CENTER);
            yesButton.setMinimumSize(new Dimension(TouchHelper.getSize(StandardSize.DEFAULT_BTN_WIDTH), TouchHelper
                    .getSize(StandardSize.DEFAULT_BTN_HEIGHT)));
            yesButton.setPreferredSize(new Dimension(TouchHelper.getSize(StandardSize.DEFAULT_BTN_WIDTH), TouchHelper
                    .getSize(StandardSize.DEFAULT_BTN_HEIGHT)));
            yesButton.setIcon(new ImageIcon(getClass().getResource("/images/jtech/touch/ok.png")));

            String initYesKey = Character.toString(I18nClientManager.translate(Jtech.T1333, false).charAt(0))
                    .toUpperCase();

            if (SystemHelper.getUseKeyboard()) {
                yesButton.setText(TouchHelper.getBtnText(I18nClientManager.translate(Jtech.T1333, false) + " ("
                        + initYesKey + ")", TouchHelper.CENTER_ALIGN));
            } else {
                yesButton.setText(TouchHelper.getBtnText(I18nClientManager.translate(Jtech.T1333, false),
                        TouchHelper.CENTER_ALIGN));
            }

            KeyStroke keyYes = KeyStroke.getKeyStroke(KeyHelper.findKeyStroke(initYesKey), 0);
            Action performYes = new AbstractAction(yesButton.getText(), yesButton.getIcon()) {
                public void actionPerformed(final ActionEvent e) {
                    option = MessageButtons.YES;
                    okPressed();
                }
            };
            yesButton.setAction(performYes);
            yesButton.getActionMap().put("performYes", performYes);
            yesButton.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW).put(keyYes, "performYes");

            yesButton.registerKeyboardAction(
                    yesButton.getActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                    KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false), JComponent.WHEN_FOCUSED);
            yesButton.registerKeyboardAction(
                    yesButton.getActionForKeyStroke(KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                    KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_FOCUSED);
        }
        return yesButton;
    }

    /**
     * Gets the yesNoPanel.
     * 
     * @return the yesNoPanel.
     */
    private JPanel getYesNoPanel() {
        if (yesNoPanel == null) {
            GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            gridBagConstraints2.gridx = 0;
            gridBagConstraints2.gridy = 0;
            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.gridx = 1;
            gridBagConstraints1.insets = new Insets(0, 10, 0, 0);
            gridBagConstraints1.gridy = 0;
            yesNoPanel = new JPanel();
            yesNoPanel.setOpaque(false);
            yesNoPanel.setLayout(new GridBagLayout());
            yesNoPanel.add(getNoButton(), gridBagConstraints1);
            yesNoPanel.add(getYesButton(), gridBagConstraints2);
        }
        return yesNoPanel;
    }

    /**
     * Initialize.
     */
    private void initialize() {
        setMainPanel(getMessagePanel());
        setCustomActions(getYesNoPanel());

        Dimension dimension = calculateDialogDimension(message);
        this.setMinimumSize(dimension);
        this.setPreferredSize(dimension);

        pack();
    }

    /**
     * Sets the No button visible or not.
     * 
     * @param visible <code>true</code> if the button must be visible, <code>false</code> otherwise.
     */
    private void setNoVisible(final boolean visible) {
        getNoButton().setVisible(visible);
    }

    /**
     * Sets the Yes button visible or not.
     * 
     * @param visible <code>true</code> if the button must be visible, <code>false</code> otherwise.
     */
    private void setYesVisible(final boolean visible) {
        getYesButton().setVisible(visible);
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookFModel.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;


/**
 * TableLook Feature Model.
 * 
 * @author kl
 */
public class FTableLookFModel extends StandardFeatureModel<FTableLookBBean, FTableLookVBean> {

    /**
     * Default constructor.
     */
    public FTableLookFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FTableLookBModel.class, null, FTableLookBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FTableLookVModel.class, null, FTableLookVCtrl.class));

    }
}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOProgressBar.java,v $
 * Created on 13 oct. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JProgressBar;
import javax.swing.border.LineBorder;

import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.FontNames;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;


/**
 * FIndicatorProgressBar : Indicator progress bar.
 * 
 * @author jd
 */
public class FIndicatorMOProgressBar extends JProgressBar {

    public static final Format TODO_FORMAT = new Format(Alignment.RIGHT_ALIGN, null, StandardColor.FG_DEFAULT,
                                                   new CustomFont(FontNames.DEFAULT, FontStyle.PLAIN, 10), "20");

    /**
     * Constructor.
     */
    public FIndicatorMOProgressBar() {
        super();
        setPreferredSize(new Dimension(100, 20));
        setForeground(Color.WHITE);
        setBackground(new Color(255, 92, 92));
        setStringPainted(true);
        setFont(TouchHelper.getFont(TODO_FORMAT.getFont()));
        setBorderPainted(true);
        setBorder(new LineBorder(Color.BLACK, 1));
        setString("");
        setValue(0);
        setUI(new MOProgressBarUI());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setValue(final int n) {
        super.setValue(n);
        double done = n;
        double toDo = getMaximum();
        setBackground(TouchHelper.getAwtColor(StandardColor.BG_BROWSER));

        // Check ToDo values not null
        int val = 0;
        if (toDo != 0) {
            val = (int) Math.round(done / (toDo) * 10);
        }

        // if val is less than 0 the color is RED
        Color color = MOUIConstants.RED_GREEN_1_END;
        if (done < 0) {
            val = 0;
        }

        // Define color according to value 'val'
        switch (val) {
            case 0:
                color = MOUIConstants.RED_GREEN_1_END;
                break;
            case 1:
                color = MOUIConstants.RED_GREEN_1_END;
                break;
            case 2:
                color = MOUIConstants.RED_GREEN_2_END;
                break;
            case 3:
                color = MOUIConstants.RED_GREEN_3_END;
                break;
            case 4:
                color = MOUIConstants.RED_GREEN_4_END;
                break;
            case 5:
                color = MOUIConstants.RED_GREEN_5_END;
                break;
            case 6:
                color = MOUIConstants.RED_GREEN_6_END;
                break;
            case 7:
                color = MOUIConstants.RED_GREEN_7_END;
                break;
            case 8:
                color = MOUIConstants.RED_GREEN_8_END;
                break;
            case 9:
                color = MOUIConstants.RED_GREEN_9_END;
                break;
            case 10:
                color = MOUIConstants.RED_GREEN_10_END;
                break;
            default:
                color = MOUIConstants.RED_GREEN_10_END;
                break;
        }
        setForeground(color);

    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputBModel.java,v $
 * Created on 17 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.Filter;
import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.FontNames;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBeanEnum;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;


/**
 * InputOperationItem : Browser Model.
 * 
 * @author vr
 */
public class FBoningOutputBModel extends BrowserModel<FBoningOutputBBean> {

    private FBoningOutputBComparator comparator = new FBoningOutputBComparator();

    /**
     * Default constructor.
     */
    public FBoningOutputBModel() {
        super();
        setBeanClass(FBoningOutputBBean.class);
        addColumn(new BrowserColumn("", FBoningOutputBBeanEnum.HASSTACKINGPLAN.getValue(), 0, 0));
        addColumn(new BrowserColumn("", FBoningOutputBBeanEnum.ITEM_CODE.getValue(), 0, 0));
        addColumn(new BrowserColumn("", FBoningOutputBBeanEnum.INNER.getValue(), 0, 0));
        addColumn(new BrowserColumn("", FBoningOutputBBeanEnum.ITEM_LONG_LABEL.getValue(), 0, 1));
        addColumn(new BrowserColumn("", FBoningOutputBBeanEnum.QTTY_DONE.getValue(), 0, 2));
        addColumn(new BrowserColumn("", FBoningOutputBBeanEnum.QTTY_TODO.getValue(), 0, 2));
        addColumn(new BrowserColumn("", FBoningOutputBBeanEnum.QTTY_UNIT.getValue(), 0, 2));
        addColumn(new BrowserColumn("", FBoningOutputBBeanEnum.QTTY2_DONE.getValue(), 0, 2));
        addColumn(new BrowserColumn("", FBoningOutputBBeanEnum.QTTY2_UNIT.getValue(), 0, 2));

        setFullyFetched(true);
        setFetchSize(Integer.MAX_VALUE);
        setFirstFetchSize(Integer.MAX_VALUE);
        setComparator(this.comparator);
        setRowHeight(100);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FBoningOutputBBean bean) {
        Format ret = null;
        Format cellFormat1 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
                FontStyle.PLAIN, 12));
        Format cellFormat2 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
                FontStyle.BOLD, 12));

        if (property.equals(FBoningOutputBBeanEnum.HASSTACKINGPLAN.getValue())) {
            ret = cellFormat2;
        } else if (property.equals(FBoningOutputBBeanEnum.ITEM_CODE.getValue())) {
            ret = cellFormat1;
            ret.setForegroundColor(StandardColor.TOUCHSCREEN_FG_COMP_LABEL);
            ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.PLAIN, 11));

        } else if (property.equals(FBoningOutputBBeanEnum.INNER.getValue())) {
            ret = cellFormat1;
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                ret.setForegroundColor(StandardColor.BLACK);

            }
            ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.BOLD, 11));
            ret.setAlignment(Alignment.RIGHT_ALIGN);
        } else if (property.equals(FBoningOutputBBeanEnum.ITEM_LONG_LABEL.getValue())) {
            ret = cellFormat1;
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                ret.setForegroundColor(StandardColor.BLACK);

            }
            ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.BOLD_ITALIC, 11));

        } else if (property.equals(FBoningOutputBBeanEnum.QTTY_DONE.getValue())
                || property.equals(FBoningOutputBBeanEnum.QTTY_TODO.getValue())
                || property.equals(FBoningOutputBBeanEnum.QTTY_UNIT.getValue())) {
            ret = cellFormat1;
            ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.BOLD, 12));

            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                ret.setForegroundColor(StandardColor.TOUCHSCREEN_FG_LABEL);
            }

        } else if (property.equals(FBoningOutputBBeanEnum.QTTY2_DONE.getValue())
                || property.equals(FBoningOutputBBeanEnum.QTTY2_UNIT.getValue())) {
            ret = cellFormat1;
            ret.setAlignment(Alignment.RIGHT_ALIGN);
            ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.BOLD, 11));
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                ret.setForegroundColor(StandardColor.BLACK);

            }
        }

        return ret;
    }

    /**
     * Sets the browserFilter.
     * 
     * @param browserFilter browserFilter.
     */
    public void setBrowserFilter(final CodeLabel browserFilter) {
        this.setFilter(new Filter<FBoningOutputBBean>() {

            @Override
            public boolean matches(final FBoningOutputBBean bean) {
                // true if the filter is null (all item) or if the hierarchy matches
                return (browserFilter == null || browserFilter.getCode() == null || browserFilter.getCode().isEmpty() || browserFilter
                        .getCode().equals(bean.getHierarchy()));
            }

            @Override
            public boolean refines(final Filter filter) {
                return false;
            }
        });
    }
}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: TimeBreakDownCheckBoxCellRenderer.java,v $
 * Created on 18 nov. 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing;


import java.awt.Component;
import java.awt.GridBagLayout;

import javax.swing.JCheckBox;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Renderer for the check box column.
 * 
 * @author cj
 */
public class TimeBreakDownCheckBoxCellRenderer extends DefaultTableCellRenderer {

    /**
     * {@inheritDoc}
     */
    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        JPanel pan = new JPanel();
        if (value != null) {
            JCheckBox checkbox = new JCheckBox();
            checkbox.setSelected((Boolean) value);
            checkbox.setOpaque(false);

            pan.setOpaque(false);
            pan.setLayout(new GridBagLayout());
            pan.add(checkbox);
        }
        return pan;
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: ExpandNodeAction.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.action;


import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.treetable.InputTreeTableController;
import fr.vif.jtech.ui.treetable.InputTreeTableIView;
import fr.vif.jtech.ui.treetable.InputTreeTableModel;
import fr.vif.jtech.ui.treetable.actions.ExpandPathAction;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeElt;


/**
 * ExpandNodeAction.
 * 
 * @author xg
 */
public class ExpandNodeAction extends ExpandPathAction {

    private InputTreeTableController                          treeTableCtrl;
    private InputTreeTableIView<TimeBreakDownHchyTreeNodeElt> treeTableView;
    private InputTreeTableModel                               treeTableModel;

    /**
     * def ctor.
     */
    public ExpandNodeAction() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        treeTableCtrl = getTreeTableController();
        treeTableView = (InputTreeTableIView<TimeBreakDownHchyTreeNodeElt>) treeTableCtrl.getView();
        treeTableModel = (InputTreeTableModel) treeTableView.getModel();

        Object[] currentPath = treeTableView.getCurrentSelectedNodePath();
        if (currentPath != null && currentPath.length > 0) {
            treeTableModel.expandAllNode(currentPath[currentPath.length - 1]);
        }
    }
}

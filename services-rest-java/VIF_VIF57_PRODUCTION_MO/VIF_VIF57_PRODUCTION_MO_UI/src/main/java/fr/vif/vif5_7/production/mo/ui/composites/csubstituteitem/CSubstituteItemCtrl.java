/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: CSubstituteItemCtrl.java,v $
 * Created on 19 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.composites.csubstituteitem;


import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.input.AbstractCompositeITFController;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fsubstituteitem.FSubstituteItemSBean;
import fr.vif.vif5_7.production.mo.business.services.composites.csubstituteitem.CSubstituteItemCBS;


/**
 * Substitute item controller.
 * 
 * @author vr
 */
public class CSubstituteItemCtrl extends AbstractCompositeITFController {

    private FSubstituteItemSBean selection;

    private CSubstituteItemCBS   csubstituteItemCBS;

    /**
     * Gets the csubstituteItemCBS.
     * 
     * @category getter
     * @return the csubstituteItemCBS.
     */
    public CSubstituteItemCBS getCsubstituteItemCBS() {
        return csubstituteItemCBS;
    }

    /**
     * Sets the csubstituteItemCBS.
     * 
     * @category setter
     * @param csubstituteItemCBS csubstituteItemCBS.
     */
    public void setCsubstituteItemCBS(final CSubstituteItemCBS csubstituteItemCBS) {
        this.csubstituteItemCBS = csubstituteItemCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CodeLabel validateCode(final CodeLabel bean) throws UIErrorsException, UIException, BusinessException,
            BusinessErrorsException {

        return csubstituteItemCBS.validateCode(getIdCtx(),
                (FSubstituteItemSBean) ((CSubstituteItemIView) getView()).getSelection(), bean);

    }

}

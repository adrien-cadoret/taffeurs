/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DTableLookImportExportDView.java,v $
 * Created on 5 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.importexport.swing;


import java.awt.Dialog;
import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.ui.dialogs.swing.DynamicSwingDialog;
import fr.vif.jtech.ui.input.swing.SwingInputRadioSet;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.vif5_7.admin.config.ui.composites.cfilechooser.FilterDesc;
import fr.vif.vif5_7.admin.config.ui.composites.cfilechooser.swing.CFileChooserCView;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.importexport.FTableLookImportExportBean;


/**
 * Dialog Table Look use to Import or export model.
 * 
 * @author kl
 */
public class DTableLookImportExportDView extends DynamicSwingDialog {

    private CFileChooserCView   cfileChooser;
    private SwingInputRadioSet  radioImportExport;
    private SwingInputTextField tableLookCode;

    /**
     * Constructor form superclass.
     */
    public DTableLookImportExportDView() {
        super();
    }

    /**
     * Constructor form superclass.
     * 
     * @param view Dialog
     */
    public DTableLookImportExportDView(final Dialog view) {
        super(view);
    }

    /**
     * Constructor form superclass.
     * 
     * @param view Frame
     */
    public DTableLookImportExportDView(final Frame view) {
        super(view);
    }

    /**
     * Gets the cfileChooser.
     * 
     * @return the cfileChooser.
     */
    public CFileChooserCView getCfileChooser() {

        if (cfileChooser != null) {
            List<String> filterProgressFile = new ArrayList<String>();
            filterProgressFile.add("d");
            FilterDesc filterDescProgress = new FilterDesc("d", filterProgressFile);
            FilterDesc[] filterDesc = new FilterDesc[] { filterDescProgress };

            cfileChooser.setFilterDesc(filterDesc);
        }
        return cfileChooser;
    }

    /**
     * Gets the radioImportExport.
     * 
     * @return the radioImportExport.
     */
    public SwingInputRadioSet getRadioImportExport() {
        return radioImportExport;
    }

    /**
     * Gets the tableLookCode.
     * 
     * @return the tableLookCode.
     */
    public SwingInputTextField getTableLookCode() {
        return tableLookCode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {

        super.initializeView();

        // Force the path into the filechooser.
        if (getModel().getDialogBean() != null && getModel().getDialogBean() instanceof FTableLookImportExportBean) {
            this.setPath(((FTableLookImportExportBean) getModel().getDialogBean()).getPath());
        }
        getTableLookCode().setEnabled(
                FTableLookImportExportBean.ImportExport.IMPORT.getValue().equals(getTableLookCode().getValue()));

    }

    /**
     * Sets the cfileChooser.
     * 
     * @param cfileChooser cfileChooser.
     */
    public void setCfileChooser(final CFileChooserCView cfileChooser) {
        this.cfileChooser = cfileChooser;
    }

    /**
     * Sets the radioImportExport.
     * 
     * @param radioImportExport radioImportExport.
     */
    public void setRadioImportExport(final SwingInputRadioSet radioImportExport) {
        this.radioImportExport = radioImportExport;
    }

    /**
     * Sets the tableLookCode.
     * 
     * @param tableLookCode tableLookCode.
     */
    public void setTableLookCode(final SwingInputTextField tableLookCode) {
        this.tableLookCode = tableLookCode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void fireDialogValidated() {
        // To force the update of FTableLookImportExportBean.path.
        if (getModel().getDialogBean() != null && getModel().getDialogBean() instanceof FTableLookImportExportBean) {
            ((FTableLookImportExportBean) getModel().getDialogBean()).setPath(getPath());
        }
        super.fireDialogValidated();
    }

    /**
     * Set the path to the file chooser.
     * 
     * @return path path to set.
     */
    private String getPath() {
        return (String) getCfileChooser().getScifPath().getValue();
    }

    /**
     * Set the path to the file chooser.
     * 
     * @param path path to set.
     */
    private void setPath(final String path) {
        getCfileChooser().getScifPath().setValue(path);
    }

}

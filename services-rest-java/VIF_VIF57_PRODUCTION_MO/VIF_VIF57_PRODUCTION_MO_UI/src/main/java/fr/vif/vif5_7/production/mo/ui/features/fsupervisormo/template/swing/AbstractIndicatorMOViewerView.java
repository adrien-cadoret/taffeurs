/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractIndicatorMOViewerView.java,v $
 * Created on 3 oct. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing;


import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.generic.GenericActionListener;
import fr.vif.jtech.ui.viewer.swing.StandardSwingViewer;


/**
 * Viewer view to used for indicators internal frame.
 * 
 * @author jd
 * @param <VBean> viewer bean
 */
public abstract class AbstractIndicatorMOViewerView<VBean> extends StandardSwingViewer<VBean> {
    private List<GenericActionListener> actionListeners = new ArrayList<GenericActionListener>();

    /**
     * add a generic action listener.
     * 
     * @param listener listener
     */
    public void addGenericActionListener(final GenericActionListener listener) {
        actionListeners.add(listener);
    }

    /**
     * fire a generic action.
     * 
     * @param event event
     */
    public void fireGenericAction(final GenericActionEvent event) {
        for (GenericActionListener l : actionListeners) {
            l.actionPerformed(event);
        }
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningInputVView.java,v $
 * Created on 23 avr. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.touch;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.input.InputTextFieldModel;
import fr.vif.jtech.ui.input.touch.TouchInputBarCode;
import fr.vif.jtech.ui.input.touch.TouchInputSpecialViewer1N;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.comment.ui.composites.simplecomment.touch.CCommentTouchView;
import fr.vif.vif5_7.gen.service.ui.composites.cquantityunit.touch.CQuantityUnitView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrydetail.FDataEntryDetailBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBeanEnum;
import fr.vif.vif5_7.production.mo.ui.composites.barcode.BarCodeCtrl;
import fr.vif.vif5_7.production.mo.ui.composites.csubstituteitem.touch.CSubstituteItemView;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.FDataEntryDetailV1NModel;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityEnum;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingBBeanEnum;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingSBean;
import fr.vif.vif5_7.stock.kernel.ui.features.fdestocking.FDestockingBCtrl;
import fr.vif.vif5_7.stock.kernel.ui.features.fdestocking.FDestockingBModel;
import fr.vif.vif5_7.stock.kernel.ui.features.fdestocking.touch.FDestockingBView;


/**
 * Viewer view for the boning input feature.
 * 
 * @author cj
 */
public class FBoningInputVView extends StandardTouchViewer<FProductionInputContainerVBean> {
    private CCommentTouchView         commentTouchView;
    private Format                    formatQty = new Format(Alignment.RIGHT_ALIGN,
            MOUIConstants.TOUCHSCREEN_BG_BROWSER,
            StandardColor.TOUCHSCREEN_FG_BROWSER,
            StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE, "11");

    private TouchInputTextField       itfBatch;
    private JLabel                    jlAvancement;
    private JLabel                    jlFlash;
    private JPanel                    jpFlash;
    private JProgressBar              progressBar;
    private CQuantityUnitView         quantityUnit1View;
    private CQuantityUnitView         quantityUnit2View;
    private JSeparator                separator;
    private CSubstituteItemView       substituteItemView;
    private TouchInputTextField       itfProgressionQty2;
    private TouchInputBarCode         touchLEDInputBarCode;
    private TouchInputSpecialViewer1N v1NMovementList;

    /**
     * Default constructor.
     */
    public FBoningInputVView() {
        super();
        initialize();
    }

    /**
     * Get the comment touch view.
     * 
     * @return the comment touch view.
     */
    public CCommentTouchView getCommentTouchView() {
        if (commentTouchView == null) {
            commentTouchView = new CCommentTouchView();
            commentTouchView.setBounds(0, 255, 865, 51);
            commentTouchView.setBeanProperty("comment");
            commentTouchView.setFocusable(false);
        }
        return commentTouchView;
    }

    /**
     * Get the touch led input barcode.
     * 
     * @return the touch led input bar code
     */
    public TouchInputBarCode getInputBarCode() {
        if (touchLEDInputBarCode == null) {
            touchLEDInputBarCode = new TouchInputBarCode(BarCodeCtrl.class);
            touchLEDInputBarCode.setDefaultBorder(new LineBorder(TouchHelper
                    .getAwtColor(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR), 1, false));
            touchLEDInputBarCode.setBorder(new LineBorder(TouchHelper
                    .getAwtColor(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR), 1, false));
            touchLEDInputBarCode.setBounds(64, 10, 340, 30);
            touchLEDInputBarCode.setBeanProperty("barcode");

        }
        return touchLEDInputBarCode;
    }

    /**
     * @return
     */

    /**
     * 
     * Get the batch input text field.
     * 
     * @return the batch input text field
     */
    public TouchInputTextField getItfBatch() {
        if (itfBatch == null) {
            itfBatch = new TouchInputTextField(String.class, "15", I18nServerManager.translate(this.getLocale(), false,
                    ProductionMo.T34113));
            itfBatch.setBounds(448, 105, 412, 70);
            itfBatch.setPreferredSize(new Dimension(300, 80));
            itfBatch.setBeanProperty("entity.stockItemBean.stockItem.batch");
            itfBatch.setHelpBrowserBeanCode(FDestockingBBeanEnum.BATCH.getValue());
            itfBatch.setHelpBrowserTriad(new BrowserMVCTriad(FDestockingBModel.class, FDestockingBView.class,
                    FDestockingBCtrl.class));
            itfBatch.setUseFieldHelp(true);
            itfBatch.setFreeDataInput(true);
            FDestockingSBean sBean = new FDestockingSBean();
            itfBatch.setInitialHelpSelection(sBean);
        }
        return itfBatch;
    }

    /**
     * 
     * Get the Input text field progression Qty 2 view.
     * 
     * @return the Input text field progression Qty 2 view
     */
    public TouchInputTextField getItfProgressionQty2() {

        if (itfProgressionQty2 == null) {
            itfProgressionQty2 = new TouchInputTextField(String.class, "15", "");
            itfProgressionQty2.setBounds(195, 180, 185, 70);
            itfProgressionQty2.setAlignment(Alignment.CENTER_ALIGN);
            itfProgressionQty2.setBorder(BorderFactory.createLineBorder(Color.GRAY));
            itfProgressionQty2.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_LABEL));
            itfProgressionQty2.setBackground(TouchHelper.getAwtColor(MOUIConstants.TOUCHSCREEN_BG_PANEL_LIGHT));
            itfProgressionQty2.getInsets().set(2, 2, 1, 1);
            itfProgressionQty2.setForceNoHelp(true);
            itfProgressionQty2.setBeanBased(false);
            itfProgressionQty2.setEnabled(false);
            itfProgressionQty2.setFocusable(false);
        }
        return itfProgressionQty2;
    }

    /**
     * 
     * Get the avancement label.
     * 
     * @return the avancement label
     */
    public JLabel getJlAvancement() {
        if (jlAvancement == null) {
            jlAvancement = new JLabel();
            jlAvancement.setFont(new Font("Arial", Font.BOLD, 16));
            jlAvancement.setText(I18nServerManager.translate(this.getLocale(), false, ProductionMo.T32452));
            jlAvancement.setBounds(8, 177, 236, 25);
            jlAvancement.setForeground(new Color(StandardColor.TOUCHSCREEN_FG_LABEL.getRed(),
                    StandardColor.TOUCHSCREEN_FG_LABEL.getGreen(), StandardColor.TOUCHSCREEN_FG_LABEL.getBlue()));
            jlAvancement.setFocusable(false);
        }
        return jlAvancement;
    }

    /**
     * Get the label flash.
     * 
     * @return the label flash
     */
    public JLabel getJlFlash() {
        if (jlFlash == null) {
            jlFlash = new JLabel();
            jlFlash.setForeground(new Color(238, 127, 0));
            jlFlash.setFont(new Font("Arial", Font.BOLD, 16));
            jlFlash.setText(I18nClientManager.translate(ProductionMo.T29697, false));
            jlFlash.setBounds(10, 10, 48, 30);
        }
        return jlFlash;
    }

    /**
     * 
     * Get Flash panel.
     * 
     * @return the flash panel
     */
    public JPanel getJpFlash() {
        if (jpFlash == null) {
            jpFlash = new JPanel();
            final GridBagLayout gridBagLayout = new GridBagLayout();
            gridBagLayout.columnWidths = new int[] { 0, 7 };
            jpFlash.setLayout(gridBagLayout);
            jpFlash.setBounds(10, 1, 850, 29);
            jpFlash.setOpaque(false);
            final GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new Insets(0, 0, 0, 5);
            gridBagConstraints.anchor = GridBagConstraints.EAST;
            gridBagConstraints.weighty = 1;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.gridx = 0;
            jpFlash.add(getJlFlash(), gridBagConstraints);
            final GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.weighty = 1;
            gridBagConstraints1.fill = GridBagConstraints.BOTH;
            gridBagConstraints1.weightx = 1;
            gridBagConstraints1.gridy = 0;
            gridBagConstraints1.gridx = 1;
            jpFlash.add(getInputBarCode(), gridBagConstraints1);
        }
        return jpFlash;
    }

    /**
     * Get the progress bar.
     * 
     * @return the progress bar
     */
    public JProgressBar getProgressBar() {
        if (progressBar == null) {
            progressBar = new JProgressBar();
            progressBar.setFont(new Font("Arial", Font.BOLD, 20));
            progressBar.setStringPainted(true);
            progressBar.setBounds(10, 200, 185, 50);
            progressBar.setValue(0);
            progressBar.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_LABEL));
            progressBar.setBackground(TouchHelper.getAwtColor(MOUIConstants.TOUCHSCREEN_BG_PANEL_LIGHT));
            progressBar.setEnabled(false);
            progressBar.setFocusable(false);
        }
        return progressBar;
    }

    /**
     * Get the quantity unit 1.
     * 
     * @return the quantity unit 1
     */
    public CQuantityUnitView getQuantityUnit1View() {
        if (quantityUnit1View == null) {
            quantityUnit1View = new CQuantityUnitView();
            quantityUnit1View.setBounds(448, 180, 200, 70);
            quantityUnit1View.setBeanProperty(FProductionInputContainerVBeanEnum.FIRST_QTY.getValue());
            quantityUnit1View.setDescription(I18nClientManager.translate(GenKernel.T210, false) + " 1");
        }
        return quantityUnit1View;
    }

    /**
     * Get the quantity unit 2.
     * 
     * @return the quantity unit 2
     */
    public CQuantityUnitView getQuantityUnit2View() {
        if (quantityUnit2View == null) {
            quantityUnit2View = new CQuantityUnitView();
            quantityUnit2View.setBounds(654, 180, 200, 70);
            quantityUnit2View.setBeanProperty(FProductionInputContainerVBeanEnum.SECOND_QTY.getValue());
            quantityUnit2View.setDescription(I18nClientManager.translate(GenKernel.T210, false) + " 2");
        }
        return quantityUnit2View;
    }

    /**
     * Get the separator.
     * 
     * @return the separator
     */
    public JSeparator getSeparator() {
        if (separator == null) {
            separator = new JSeparator();
            separator.setOrientation(SwingConstants.VERTICAL);
            separator.setBounds(425, 36, 29, 234);
            separator.setFocusable(false);
        }
        return separator;
    }

    /**
     * 
     * Get the substituteitem view.
     * 
     * @return the substitute item view
     */
    public CSubstituteItemView getSubstituteItemView() {

        if (substituteItemView == null) {
            substituteItemView = new CSubstituteItemView();
            substituteItemView.setBounds(448, 25, 412, 90);
            substituteItemView.setBackground(TouchHelper.getAwtColor(MOUIConstants.TOUCHSCREEN_BG_PANEL_LIGHT));
            substituteItemView.getInsets().set(2, 2, 1, 1);
            substituteItemView.setBeanProperty("entity." + CEntityEnum.ITEM_CL.getValue());
            substituteItemView.getLabelView().setVisible(false);
            substituteItemView.getCodeView().setBeanProperty("label");
            ((InputTextFieldModel) substituteItemView.getCodeView().getModel()).setFormat("30");
        }
        return substituteItemView;
    }

    /**
     * 
     * Get the movement list viewer1N.
     * 
     * @return the movement list viewer1N
     */
    public TouchInputSpecialViewer1N getV1NMovementList() {
        if (v1NMovementList == null) {
            List<BrowserColumn> lstSel = new ArrayList<BrowserColumn>();
            lstSel.add(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29673, false),
                    "itemMovementBean.itemMovementKey.nlig", 15, "####"));
            lstSel.add(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29437, false),
                    "itemMovementBean.movementDateTime", 60, "dd/MM/yy HH:mm"));
            lstSel.add(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29552, false),
                    "itemMovementBean.batch", 85, "15"));
            lstSel.add(new BrowserColumn(I18nClientManager.translate(GenKernel.T426, false),
                    "itemMovementBean.itemCL.code", 85, "15"));
            lstSel.add(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29553, false) + " 1",
                    "firstQuantity", 20, formatQty));
            lstSel.add(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29553, false) + " 2",
                    "secondQuantity", 20, formatQty));
            FDataEntryDetailV1NModel model = new FDataEntryDetailV1NModel(lstSel, FDataEntryDetailBBean.class);
            v1NMovementList = new TouchInputSpecialViewer1N<FDataEntryDetailBBean>(model);
            v1NMovementList.setBounds(0, 308, 955, 104);
            v1NMovementList.setBeanProperty("movementList");
            v1NMovementList.setFocusable(false);
            v1NMovementList.setFocusTraversalKeysEnabled(false);
            v1NMovementList.setAlwaysDisabled(true);
            v1NMovementList.setRowHeight(24);

        }
        return v1NMovementList;
    }

    /**
     * {@inheritDoc}
     */
    public void setFlashLabel(final String flashLabel) {
        getJlFlash().setText(flashLabel);

    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        setLayout(null);
        setPreferredSize(new Dimension(870, 530));
        add(getCommentTouchView());
        add(getSubstituteItemView());
        add(getItfBatch());
        add(getQuantityUnit1View());
        add(getQuantityUnit2View());
        add(getSeparator());
        add(getProgressBar());
        add(getItfProgressionQty2());
        add(getJlAvancement());
        add(getV1NMovementList());
        add(getJpFlash());
    }
}

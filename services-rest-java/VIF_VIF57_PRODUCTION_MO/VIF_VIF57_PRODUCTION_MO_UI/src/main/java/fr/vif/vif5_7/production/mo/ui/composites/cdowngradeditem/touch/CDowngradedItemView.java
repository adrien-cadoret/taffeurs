/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: CDowngradedItemView.java,v $
 * Created on 19 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.composites.cdowngradeditem.touch;


import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.input.touch.TouchCompositeInputTextField;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdowngradeditem.FDowngradedItemBBean;
import fr.vif.vif5_7.production.mo.ui.composites.cdowngradeditem.CDowngradedItemCtrl;
import fr.vif.vif5_7.production.mo.ui.composites.cdowngradeditem.CDowngradedItemIView;
import fr.vif.vif5_7.production.mo.ui.features.fdowngradeditem.FDowngradedItemBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fdowngradeditem.FDowngradedItemBModel;
import fr.vif.vif5_7.production.mo.ui.features.fdowngradeditem.touch.FDowngradedItemBView;


/**
 * Component to list the dowgraded item corresponding to an output item.
 * 
 * @author gp
 */
public class CDowngradedItemView extends TouchCompositeInputTextField implements CDowngradedItemIView {

    private static final String BBEAN_CODE  = "code";
    private static final String BBEAN_LABEL = "label";
    private static final String CODE_FORMAT = "15";
    private Selection           selection;

    /**
     * Default constructor.
     */
    public CDowngradedItemView() {

        super(CDowngradedItemCtrl.class, CODE_FORMAT);
        setCodeUseFieldHelp(true);
        setCodeHelpBrowserTriad(new BrowserMVCTriad(FDowngradedItemBModel.class, FDowngradedItemBView.class,
                FDowngradedItemBCtrl.class));
        setBeanBased(true);
        setBeanProperty(BBEAN_CODE);
        setCodeHelpInfos(FDowngradedItemBBean.class, BBEAN_CODE, BBEAN_LABEL);

        setTextLabel(I18nClientManager.translate(ProductionMo.T29519, false));
        setDescription(I18nClientManager.translate(ProductionMo.T23888, false));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSelection(final Selection selection) {
        setCodeInitialHelpSelection(selection);
        this.selection = selection;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getSelection() {
        return this.selection;
    }

}

/*
 * Copyright (c) 2015 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: TimeBreakDownDateCellRenderer.java,v $
 * Created on 10 avr. 2015 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing;


import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

import fr.vif.jtech.ui.input.swing.SwingInputTextField;


/**
 * Renderer for the date column.
 * 
 * @author cj
 */
public class TimeBreakDownDateCellRenderer extends DefaultTableCellRenderer {

    /**
     * {@inheritDoc}
     */
    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        JPanel pan = new JPanel();
        if (value != null) {
            SwingInputTextField itfDate = new SwingInputTextField(Date.class, "dd/MM/yyyy HH:mm", false, new JLabel());
            itfDate.setValue(value);
            itfDate.setOpaque(false);
            itfDate.getJTextField().setBorder(null);
            itfDate.getJTextField().setOpaque(false);

            pan.setBorder(null);
            pan.setLayout(new GridBagLayout());
            pan.add(itfDate, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.LINE_START,
                    GridBagConstraints.BOTH, new Insets(0, 2, 0, 0), 0, 0));

            pan.setOpaque(true);
            if (isSelected) {
                pan.setBackground(Color.BLACK);
                itfDate.getJTextField().setForeground(Color.WHITE);
            }
        }
        return pan;
    }
}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingVCtrl.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.exceptions.BeanException;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.input.InputTextFieldView;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaCompleteBean;
import fr.vif.vif5_7.gen.criteria.business.beans.common.FCriteriaSBean;
import fr.vif.vif5_7.gen.criteria.constants.Mnemos.CriteriaDomain;
import fr.vif.vif5_7.gen.criteria.ui.composites.ccriteria.swing.CCLCriteriaView;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.FCriteriaBCtrl;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.FCriteriaBModel;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FSettingBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FSettingSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingVBean;
import fr.vif.vif5_7.production.mo.business.services.features.tablelook.setting.FTableLookSettingCBS;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.ButtonLocation;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.FontSize;
import fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting.swing.FTableLookSettingDynamicVView;


/**
 * TableLook Viewer Controller.
 * 
 * @author kl
 */
public class FTableLookSettingVCtrl extends AbstractStandardViewerController<FTableLookSettingVBean> implements
        TableRowChangeListener {

    /** LOGGER. */
    private static final Logger    LOGGER      = Logger.getLogger(FTableLookSettingVCtrl.class);

    private FeatureView            featureView;
    private CCLCriteriaView        criteriaView;
    private InputTextFieldView     criteriaInputView;

    /** CONTROLLER. **/
    private FTableLookSettingBCtrl browserCtrl = null;

    /** CBS. **/
    private FTableLookSettingCBS   ftableLookSettingCBS;

    /**
     * Gets the browserCtrl.
     * 
     * @return the browserCtrl.
     */
    public FTableLookSettingBCtrl getBrowserCtrl() {
        return browserCtrl;
    }

    /**
     * Gets the featureView.
     * 
     * @return the featureView.
     */
    public FeatureView getFeatureView() {
        return featureView;
    }

    /**
     * Gets the ftableLookSettingCBS.
     * 
     * @return the ftableLookSettingCBS.
     */
    public FTableLookSettingCBS getFtableLookSettingCBS() {
        return ftableLookSettingCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        super.initialize();

        getViewerView().getViewerFlipFlopController().getSettingSelBrwCtrl().setViewerCtrl(this);
        getViewerView().getViewerFlipFlopController().getSettingResuBrwCtrl().setViewerCtrl(this);

        getViewerView().changeListFontSize();
        getViewerView().changeListButtonLocation();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Open a Help browser to select a criteria.
     * 
     * @return value
     */
    public CriteriaCompleteBean openCriteriaHelp() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openCriteriaHelp()");
        }

        getCriteriaInputView().setInitialHelpSelection(
                new FCriteriaSBean(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()),
                        CriteriaDomain.ITEM.getValue()));

        CriteriaCompleteBean criteriaCompleteBean = getView().openFieldHelpDialog(
                I18nClientManager.translate(GenKernel.T26864, false), getCriteriaInputView(),
                new CriteriaCompleteBean(), this);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openCriteriaHelp()=" + criteriaCompleteBean);
        }
        return criteriaCompleteBean;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void renderAndEnableComponents() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - renderAndEnableComponents()");
        }

        // Update the the column 'Personnalisé' in the right zone of the flip flop with the value set in component
        // textfield under.
        super.renderAndEnableComponents();
        // Update the détail zone of the flip flop bean selected.
        updateCurrentSettingBean();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - renderAndEnableComponents()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanged(event=" + event + ")");
        }

        if (event.getSelectedBean() instanceof FTableLookSettingBBean) {
            super.selectedRowChanged(event);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanged(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanging(event=" + event + ")");
        }
        if (event.getSelectedBean() instanceof FTableLookSettingBBean) {
            if (getBean() != null) {
                FTableLookSettingVBean initialBean = getModel().getInitialBean();
                if (!initialBean.getLstSettings().equals(getBean().getLstSettings())
                        || (initialBean.getSort() != null && !initialBean.getSort().equals(getBean().getSort()))) { // AM161429
                    super.selectedRowChanging(event);
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanging(event=" + event + ")");
        }
    }

    /**
     * Sets the browserCtrl.
     * 
     * @param browserCtrl browserCtrl.
     */
    public void setBrowserCtrl(final FTableLookSettingBCtrl browserCtrl) {
        this.browserCtrl = browserCtrl;
    }

    /**
     * Sets the featureView.
     * 
     * @param featureView featureView.
     */
    public void setFeatureView(final FeatureView featureView) {
        this.featureView = featureView;
    }

    /**
     * Sets the ftableLookSettingCBS.
     * 
     * @param ftableLookSettingCBS ftableLookSettingCBS.
     */
    public void setFtableLookSettingCBS(final FTableLookSettingCBS ftableLookSettingCBS) {
        this.ftableLookSettingCBS = ftableLookSettingCBS;
    }

    /**
     * Set the inputTextField Detail to set the custom title to the selected zone.
     * 
     */
    public void updateCurrentSettingBean() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateCurrentSettingBean()");
        }

        try {
            FSettingBean currentBean = getViewerView().getFlipFlopView().getCurrentBean();
            if (currentBean != null) {
                currentBean.setFontSizeToDisplayInArray(FontSize.getFontSize(currentBean.getFontSize()).getTranslation(
                        getIdCtx().getLocale()));
                currentBean.setButtonLocationToDisplayInArray(ButtonLocation.getButtonLocation(
                        currentBean.getButtonLocation()).getTranslation(getIdCtx().getLocale()));
            }
            getBean().setCurrentSettingBean(currentBean);
            getViewerView().renderDetailSetting();
        } catch (BeanException e) {
            LOGGER.error("E - updateCurrentSettingBean() - BeanException e=" + e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateCurrentSettingBean()");
        }
    }

    /**
     * Set the inputTextField Detail to set the custom title to the selected zone.
     * 
     * @param view the view.
     */
    public void updateCurrentSettingBean(final FTableLookSettingDynamicVView view) {
        if (getViewerView().getFlipFlopView().getCurrentBean() != null) {
            getViewerView().getFlipFlopView().getCurrentBean().setSizeZone((Integer) view.getSitfSizeZone().getValue());
            getViewerView().getFlipFlopView().getCurrentBean().setTitle((String) view.getSitfTitle().getValue());
            getViewerView().getFlipFlopView().getCurrentBean()
            .setFontSize((String) view.getIcbUiSFontSize().getValue());
            view.getSitfSizeZone().getModel().setValue(view.getSitfSizeZone().getValue());
            view.getSitfTitle().getModel().setValue(view.getSitfTitle().getValue());
            view.getIcbUiSFontSize().getModel().setValue(view.getIcbUiSFontSize().getValue());
            this.updateCurrentSettingBean();
            fireBeanUpdated(getBean());
            fireUpdatePanelStateChanged();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FTableLookSettingVBean convert(final Object bean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }

        FTableLookSettingVBean vBean = new FTableLookSettingVBean();

        try {
            FTableLookSettingBBean bBean = (FTableLookSettingBBean) bean;
            FTableLookVBean vBeanParent = (FTableLookVBean) getSharedContext().get(Domain.DOMAIN_PARENT, "viewerBean");

            bBean.setCodeModel(vBeanParent.getBrowserBean().getCode());
            bBean.setCodeModelReference(vBeanParent.getBrowserBean().getModRef());

            // Selection to load the Zones in the flip flop
            getViewerView().setInitialSelection(
                    new FSettingSBean(vBeanParent.getBrowserBean().getModRef(), vBeanParent.getBrowserBean().getCode(),
                            bBean.getChapterNumber()));

            vBean = getFtableLookSettingCBS().convert(getIdCtx(), bBean);

            getViewerView().changeListSorts(bBean);
            getViewerView().udpdateView(bBean);

        } catch (BusinessException e) {
            LOGGER.info("P - BusinessException e=" + e);
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FTableLookSettingVBean updateBean(final FTableLookSettingVBean bean) throws UIException,
            UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(bean=" + bean + ")");
        }

        FTableLookSettingVBean vBean = new FTableLookSettingVBean();
        try {

            vBean = getFtableLookSettingCBS().updateBean(getIdCtx(), getModel().getInitialBean(), bean);

        } catch (BusinessException e) {
            LOGGER.info("P - BusinessException e=" + e);
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(bean=" + bean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * 
     * Get the location touch view. Not visible
     * 
     * @return the location touch view
     */
    private InputTextFieldView getCriteriaInputView() {
        if (criteriaInputView == null) {
            criteriaInputView = new SwingInputTextField(String.class, "10", false, null);
            criteriaInputView.setVisible(false);
            criteriaInputView
                    .setHelpBrowserTriad(new BrowserMVCTriad(FCriteriaBModel.class, null, FCriteriaBCtrl.class));
            criteriaInputView.setUseFieldHelp(true);

        }
        return criteriaInputView;
    }

    /**
     * 
     * Get the location touch view. Not visible
     * 
     * @return the location touch view
     */
    private CCLCriteriaView getCriteriaView() {
        if (criteriaView == null) {
            criteriaView = new CCLCriteriaView();
            criteriaView.setVisible(false);
            criteriaView
                    .setCodeHelpBrowserTriad(new BrowserMVCTriad(FCriteriaBModel.class, null, FCriteriaBCtrl.class));
            criteriaView.setCodeUseFieldHelp(true);

        }
        return criteriaView;
    }

    /**
     * Cast the view in FTableLookSettingDynamicIView.
     * 
     * @return FTableLookSettingDynamicIView
     */
    private FTableLookSettingDynamicIView getViewerView() {
        return (FTableLookSettingDynamicIView) getView();
    }

}

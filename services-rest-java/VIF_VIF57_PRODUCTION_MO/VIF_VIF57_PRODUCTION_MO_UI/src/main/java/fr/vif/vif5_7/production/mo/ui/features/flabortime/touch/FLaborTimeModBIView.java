/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeModBIView.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.touch;


/**
 * FLaborTimeBIView.
 * 
 * @author nle
 */
public interface FLaborTimeModBIView {

    /**
     * init to call after controller init !
     */
    public void initialize();
}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOFactory.java,v $
 * Created on 2 déc. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.FatalException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.IndicatorTypeValueEnum;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.FGanttMOInternalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.FGraphMOInternalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.simple.swing.FIndicatorMOSimpleInternalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing.AbstractMODockableView;


/**
 * FSupervisor frame factory.
 * 
 * @author jd
 */
public final class FSupervisorMOFactory {
    public static final int             FRAME_BAR    = 10;
    public static final int             FRAME_GANTT  = 12;
    public static final int             FRAME_LINES  = 13;

    public static final int             FRAME_PIE    = 11;
    public static final int             FRAME_SIMPLE = 0;

    private static final String         ICON_PATH    = "/images/vif/vif57/production/mo/indicator/";

    private static FSupervisorMOFactory instance     = null;

    /** LOGGER. */
    private static final Logger         LOGGER       = Logger.getLogger(FSupervisorMOFactory.class);

    /**
     * Constructor.
     */
    private FSupervisorMOFactory() {

    }

    /**
     * Get Tooltip image.
     * 
     * @param localFrame MOLocalFrame
     * @return String
     */
    public static String getImgToolTip(final MOLocalFrame localFrame) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getImgToolTip(localFrame=" + localFrame + ")");
        }

        String img = "";
        switch (localFrame.getFrameBean().getTypeFrame()) {
            case FRAME_GANTT:
                img = ICON_PATH + FSupervisorMOTools.ICON_GANTT;
                break;
            case FRAME_SIMPLE:
                if (localFrame.getSelection().getTypeOfIndicator().getValue() == IndicatorTypeValueEnum.GARBAGE
                .getValue()
                || localFrame.getSelection().getTypeOfIndicator().getValue() == IndicatorTypeValueEnum.GAP
                .getValue()
                || localFrame.getSelection().getTypeOfIndicator().getValue() == IndicatorTypeValueEnum.ADVANCE_QTY
                .getValue()
                || localFrame.getSelection().getTypeOfIndicator().getValue() == IndicatorTypeValueEnum.ADVANCE_TIME
                .getValue()
                || localFrame.getSelection().getTypeOfIndicator().getValue() == IndicatorTypeValueEnum.GAP_OUTPUT
                .getValue()) {
                    img = ICON_PATH + FSupervisorMOTools.ICON_CURSOR;
                    break;
                } else {
                    img = ICON_PATH + FSupervisorMOTools.ICON_SIMPLE;
                    break;
                }
            case FRAME_BAR:
                img = ICON_PATH + FSupervisorMOTools.ICON_BAR_HORIZONTAL;
                break;
            case FRAME_PIE:
                img = ICON_PATH + FSupervisorMOTools.ICON_PIE;
                break;
            default:
                img = "";
                break;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getImgToolTip(localFrame=" + localFrame + ")=" + img);
        }
        return img;
    }

    /**
     * Get an instance.
     * 
     * @return a instance of {@link FSupervisorMOFactory}
     */
    public static FSupervisorMOFactory getInstance() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getInstance()");
        }

        if (instance == null) {
            instance = new FSupervisorMOFactory();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getInstance()=" + instance);
        }
        return instance;
    }

    /**
     * Generate a title for a Local Frame menu.
     * 
     * @param localFrame {@link MOLocalFrame}
     * @return a title.
     */
    public static String getTitle(final MOLocalFrame localFrame) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getTitle(localFrame=" + localFrame + ")");
        }

        String title = localFrame.getFrameBean().getTitle();

        switch (localFrame.getFrameBean().getTypeFrame()) {

            case FRAME_SIMPLE:
                title = I18nClientManager.translate(localFrame.getSelection().getTypeOfIndicator().getName());

                switch (localFrame.getSelection().getTypeOfIndicator()) {
                    case ADVANCE_QTY:
                        title += " (" + I18nClientManager.translate(GenKernel.T210) + ")";
                        break;
                    case ADVANCE_TIME:
                        title += " (" + I18nClientManager.translate(GenKernel.T23302) + ")";
                        break;
                    default:
                        break;
                }

                break;
            case FRAME_PIE:
                title = I18nClientManager.translate(ProductionMo.T32464);
                break;
            case FRAME_BAR:
                title = I18nClientManager.translate(ProductionMo.T32452);
                break;
            case FRAME_GANTT:
                title = I18nClientManager.translate(ProductionMo.T32689);
                break;

            default:
                title = "";
                break;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getTitle(localFrame=" + localFrame + ")=" + title);
        }
        return title;
    }

    /**
     * Create a new frame using Local frame bean.
     * 
     * @param l localFrame
     * @return {@link AbstractMODockableView}
     * @throws UIException if error occurs.
     */
    @SuppressWarnings("unchecked")
    public AbstractMODockableView createNewFrame(final MOLocalFrame l) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createNewFrame(l=" + l + ")");
        }

        AbstractMODockableView newFrame = null;
        try {

            Constructor o = null;
            Class clazz = null;

            switch (l.getFrameBean().getTypeFrame()) {
                case FRAME_SIMPLE:
                    clazz = FIndicatorMOSimpleInternalFrame.class;
                    break;
                case FRAME_PIE:
                    clazz = FGraphMOInternalFrame.class;
                    break;
                case FRAME_BAR:
                    clazz = FGraphMOInternalFrame.class;
                    break;
                case FRAME_GANTT:
                    clazz = FGanttMOInternalFrame.class;
                    break;
                default:
                    throw new UIException(ProductionMo.T32445);
            }
            if (clazz != null) {
                o = clazz.getDeclaredConstructor(MOLocalFrame.class);
                if (o != null) {

                    newFrame = (AbstractMODockableView) o.newInstance(l);
                    /* ACA useless because its initialization is done in the controller */
                    // newFrame.initialize();

                } else {
                    LOGGER.error("No valid constructor defined for the frame with id ="
                            + l.getFrameBean().getTypeFrame());
                    throw new UIException(ProductionMo.T32445);
                }
            } else {
                LOGGER.error("No class defined for the frame with id =" + l.getFrameBean().getTypeFrame());
                throw new UIException(ProductionMo.T32445);
            }
        } catch (SecurityException e1) {
            LOGGER.error("Fatal error during create new Frame ! " + l, e1);
            throw new UIException(new BusinessException(new FatalException(e1.getMessage())));
        } catch (NoSuchMethodException e1) {
            LOGGER.error("Fatal error during create new Frame ! " + l, e1);
            throw new UIException(new BusinessException(new FatalException(e1.getMessage())));
        } catch (IllegalArgumentException e1) {
            LOGGER.error("Fatal error during create new Frame ! " + l, e1);
            throw new UIException(new BusinessException(new FatalException(e1.getMessage())));
        } catch (InstantiationException e1) {
            LOGGER.error("Fatal error during create new Frame ! " + l, e1);
            throw new UIException(new BusinessException(new FatalException(e1.getMessage())));
        } catch (IllegalAccessException e1) {
            LOGGER.error("Fatal error during create new Frame ! " + l, e1);
            throw new UIException(new BusinessException(new FatalException(e1.getMessage())));
        } catch (InvocationTargetException e1) {
            LOGGER.error("Fatal error during create new Frame ! " + l, e1);
            throw new UIException(new BusinessException(new FatalException(e1.getMessage())));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createNewFrame(l=" + l + ")=" + newFrame);
        }
        return newFrame;
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_ACTIVITIES_ACTIVITIES_UI
 * File : $RCSfile: CMarkingMOResourceTouchView.java,v $
 * Created on 07 févr. 2014 by lm
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch;


import fr.vif.jtech.ui.input.InputTextFieldModel;
import fr.vif.jtech.ui.input.touch.TouchCompositeInputTextField;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.activities.activities.business.beans.features.fproductionresource.FProductionResourceBBean;
import fr.vif.vif5_7.activities.activities.ui.composites.cproductionresource.CProductionResourceCtrl;
import fr.vif.vif5_7.activities.activities.ui.features.fproductionresource.FProductionResourceBModel;
import fr.vif.vif5_7.common.util.i18n.activities.activities.ActivitiesActivities;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.CMarkingMOResourceBCtrl;


/**
 * Production Resource Code Label touch composite : View.
 * 
 * Show only the workstation's resources.
 * 
 * @author lm
 */
public class CMarkingMOResourceTouchView extends TouchCompositeInputTextField {

    private static final String CODE_FORMAT      = "10";
    private static final String HELP_BBEAN_CODE  = "code";
    private static final String HELP_BBEAN_LABEL = "label";

    // FIXME Maybe change the CProductionResourceTouchView class from activities sactivities ??

    /**
     * Default constructor.
     */
    public CMarkingMOResourceTouchView() {
        super(CProductionResourceCtrl.class, InputTextFieldModel.TYPE_STRING, CODE_FORMAT, I18nClientManager.translate(
                ActivitiesActivities.T34102, false));
        setCodeUseFieldHelp(true);
        setCodeHelpBrowserTriad(new BrowserMVCTriad(FProductionResourceBModel.class, null,
                CMarkingMOResourceBCtrl.class));
        setCodeHelpInfos(FProductionResourceBBean.class, HELP_BBEAN_CODE, HELP_BBEAN_LABEL);
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionContainerMOListVCtrl.java,v $
 * Created on 07 nov. 2008 by ALB
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioncontainermolist;


import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.util.validator.VIFBindException;
import fr.vif.jtech.common.util.i18n.ITransParam;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListInitializeBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FProductionMOListCheckFinishBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionmo.FProductionMOListCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOType;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.FeaturesId;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * MOList : Viewer Controller.
 * 
 * @author alb
 */
public class FProductionContainerMOListVCtrl extends AbstractStandardViewerController<FMOListVBean> {

    private static final Logger             LOGGER              = Logger.getLogger(FProductionContainerMOListVCtrl.class);

    private boolean                         bfeaturesLoaded     = false;

    private Map<String, Boolean>            mapActivationBtns   = new HashMap<String, Boolean>();

    private FProductionMOListCBS            productionMOListCBS = null;

    private FProductionContainerMOListFCtrl featureCtrl         = null;

    /**
     * default ctor.
     */
    public FProductionContainerMOListVCtrl() {
        super();
    }

    /**
     * finish the mo.
     * 
     * @param isFinished if MO finished
     * @param activityItemType act
     * @throws UIException e
     */
    public void finishMO(final boolean isFinished, final ActivityItemType activityItemType) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - finishMO(isFinished=" + isFinished + ", activityItemType=" + activityItemType + ")");
        }

        try {
            String ws = (String) getSharedContext().get(Domain.DOMAIN_THIS, FProductionConstant.LOGICAL_WORKSTATION);
            if (!isFinished) {
                FProductionMOListCheckFinishBean bean = getProductionMOListCBS().checkSplitOnFinishMO(getIdCtx(),
                        getBean().getMobean().getMoKey(), ws);

                if (bean.getIsBlockingInput() || bean.getIsBlockingOutput()) {
                    getView().showError(
                            I18nClientManager.translate(
                                    ProductionMo.T29622,
                                    true,
                                    new VarParamTranslation((bean.getIsBlockingInput() ? " - "
                                            + I18nClientManager.translate(ProductionMo.T29499, false) + "\n" : null)),
                                            new VarParamTranslation((bean.getIsBlockingOutput() ? " - "
                                                    + I18nClientManager.translate(ProductionMo.T29524, false) + "\n" : null))));
                } else {
                    boolean bDoFinish = true;

                    if (bean.getMapMessagesInput().size() != 0) {

                        for (Iterator iterator = bean.getMapMessagesInput().entrySet().iterator(); iterator.hasNext();) {
                            Map.Entry<AbstractOperationItemBean, String> entry = (Map.Entry) iterator.next();
                            bDoFinish = bDoFinish
                                    && getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                                            formatQuestion(entry.getKey(), entry.getValue()), MessageButtons.YES_NO) == MessageButtons.YES;
                            if (!bDoFinish) {
                                break;
                            }
                        }
                    }

                    if (bDoFinish) {
                        // Add the information about the statement Scrap. Need to finish all the xpiloia
                        getProductionMOListCBS().finishResumeMO(
                                getIdCtx(),
                                getBean().getMobean().getMoKey(),
                                ws,
                                activityItemType,
                                false,
                                (getBean().isStatementScrap(ActivityItemType.INPUT) || getBean().isStatementScrap(
                                        ActivityItemType.OUTPUT)));
                    }
                }

            } else {
                // Add the information about the statement Scrap. Need to finish all the xpiloia
                getProductionMOListCBS().finishResumeMO(
                        getIdCtx(),
                        getBean().getMobean().getMoKey(),
                        ws,
                        activityItemType,
                        false,
                        (getBean().isStatementScrap(ActivityItemType.INPUT) || getBean().isStatementScrap(
                                ActivityItemType.OUTPUT)));
            }

        } catch (BusinessException e) {
            UIException ue = new UIException(e);
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - finishMO(isFinished=" + isFinished + ", activityItemType=" + activityItemType + ")");
        }
    }

    @Override
    public void fireErrorsChanged() {
        super.fireErrorsChanged();
    }

    /**
     * Fire error changed with a UIException.
     * 
     * @param e ui exception to show
     */
    public void fireErrorsChanged(final UIException e) {
        Errors erreurs = new VIFBindException(getBean());
        erreurs.rejectValue("", "",
                I18nClientManager.translate(e.getMainTrad(), true, e.getListeParams().toArray(new ITransParam[0])));
        super.fireErrorsChanged(new UIErrorsException(new BusinessErrorsException(erreurs)));
    }

    /**
     * Gets the featureCtrl.
     *
     * @return the featureCtrl.
     */
    public FProductionContainerMOListFCtrl getFeatureCtrl() {
        return featureCtrl;
    }

    /**
     * Gets the productionMOListCBS.
     * 
     * @category getter
     * @return the productionMOListCBS.
     */
    public FProductionMOListCBS getProductionMOListCBS() {
        return productionMOListCBS;
    }

    /**
     * Gets the bButtonActivationLoaded.
     * 
     * @category getter
     * @return the bButtonActivationLoaded.
     */
    public boolean isBfeaturesLoaded() {
        return bfeaturesLoaded;
    }

    /**
     * Sets the bButtonActivationLoaded.
     * 
     * @category setter
     * @param buttonActivationLoaded bButtonActivationLoaded.
     */
    public void setBfeaturesLoaded(final boolean buttonActivationLoaded) {
        bfeaturesLoaded = buttonActivationLoaded;
    }

    /**
     * Sets the featureCtrl.
     *
     * @param featureCtrl featureCtrl.
     */
    public void setFeatureCtrl(final FProductionContainerMOListFCtrl featureCtrl) {
        this.featureCtrl = featureCtrl;
    }

    /**
     * Sets the mapActivationBtns.
     * 
     * @category setter
     * @param mapActivationBtns mapActivationBtns.
     */
    public void setMapActivationBtns(final Map<String, Boolean> mapActivationBtns) {
        this.mapActivationBtns = mapActivationBtns;
    }

    /**
     * Sets the productionMOListCBS.
     * 
     * @category setter
     * @param productionMOListCBS productionMOListCBS.
     */
    public void setProductionMOListCBS(final FProductionMOListCBS productionMOListCBS) {
        this.productionMOListCBS = productionMOListCBS;
    }

    /**
     * Show ui errors exceptions in error panel.
     * 
     * @param exception exception to show
     */
    public void showUIErrorsException(final UIErrorsException exception) {
        fireErrorsChanged(exception);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean pCreationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String pBeanProperty, final Object propertyValue) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMOListVBean convert(final Object pBean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(pBean=" + pBean + ")");
        }

        FMOListBBean bBean = (FMOListBBean) pBean;
        FMOListVBean vBean = null;
        try {
            String ws = (String) getSharedContext().get(Domain.DOMAIN_THIS, FProductionConstant.LOGICAL_WORKSTATION);
            vBean = getProductionMOListCBS().convert(getIdCtx(),
                    bBean.getMobean().getMoKey().getEstablishmentKey().getCsoc(),
                    bBean.getMobean().getMoKey().getEstablishmentKey().getCetab(), ws, bBean);
        } catch (BusinessException e) {
            UIException ue = new UIException(e);
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FMOListVBean pBean) throws UIException {
    }

    /**
     * Gets the mapActivationBtns.
     * 
     * @category getter
     * @return the mapActivationBtns.
     */
    protected Map<String, Boolean> getMapActivationBtns() {
        return mapActivationBtns;
    }

    /**
     * loads rights on features.
     */
    protected void initFeature() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initFeature()");
        }

        try {
            // just one call to the CBS to improve perf.
            FMOListInitializeBean bean = getProductionMOListCBS().initializeViewer(getIdCtx(),
                    FeaturesId.MO_CREATION.getValue());

            boolean bHasRightForCreate = bean.getbHasRightForCreate();

            getMapActivationBtns().put(FProductionContainerMOListFCtrl.MO_CREATE_BTN, bHasRightForCreate);
            getMapActivationBtns().put(FProductionContainerMOListFCtrl.MO_CREATE_MSO_BTN, bHasRightForCreate);

            boolean bLogicalWsEnabled = bean.getbLogicalWorkstationEnabled();

            getMapActivationBtns().put(FProductionContainerMOListFCtrl.LOGICAL_WORKSTATION_BTN, bLogicalWsEnabled);
            getMapActivationBtns().put(FProductionContainerMOListFCtrl.MO_UNFINISH_BTN,
                    bean.getbHasRightForUnfinishing());
            getMapActivationBtns().put(FProductionContainerMOListFCtrl.MO_FINISH_BTN, true);

        } catch (BusinessException e) {
            getView().showError(I18nClientManager.translate(Jtech.T9159, false) + ":" + e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initFeature()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMOListVBean insertBean(final FMOListVBean bean, final FMOListVBean initialBean) throws UIException,
    UIErrorsException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        if (!isBfeaturesLoaded()) {
            initFeature();
            setBfeaturesLoaded(true);
        }
        boolean ret = true;

        if (getMapActivationBtns().containsKey(reference) && !getMapActivationBtns().get(reference).booleanValue()) {
            // load of common rights and activations.
            ret = false;

        } else if (getBean() == null || getBean().getMobean() == null) {
            ret = false;
        } else if (FProductionContainerMOListFCtrl.MO_INPUT_BTN.equals(reference)) {
            if (MOType.ORIGIN.equals(getBean().getMobean().getMoType())
                    || getBean().getListInputOperations().size() == 0) {
                ret = false;
            }
        } else if (FProductionContainerMOListFCtrl.MO_OUTPUT_BTN.equals(reference)) {
            // If one of the item has a Nature Complementary, the MO_OUTPUT_BTN is enable.
            if (MOType.ORIGIN.equals(getBean().getMobean().getMoType())
                    || getBean().getListOutputOperations().size() == 0
                    && !getBean().isStatementScrap(ActivityItemType.OUTPUT)) {
                ret = false;
            }
        } else if (FProductionContainerMOListFCtrl.MO_FINISH_BTN.equals(reference)) {
            if (getBean().getMobean().equals(new MOBean())) {
                ret = false;
            } else {
                FMOListBBean bbean = getFeatureCtrl().getCurrentBBean();
                if (bbean.getIsFinished()) {
                    ret = getMapActivationBtns().get(FProductionContainerMOListFCtrl.MO_UNFINISH_BTN).booleanValue();
                }
            }

        } else if (FProductionContainerMOListFCtrl.MO_CREATE_MSO_BTN.equals(reference)) {
            if (getBean().getMobean().equals(new MOBean()) || !MOType.ORIGIN.equals(getBean().getMobean().getMoType())) {
                ret = false;
            }
        } else if (FProductionContainerMOListFCtrl.BTN_PDF_REFERENCE.equals(reference)) {
            if (getBean() == null || getBean().getMobean().getMoKey().getChrono() == null
                    || getBean().getMobean().getMoKey().getChrono().getChrono() == 0) {
                ret = false;
            } else {
                ret = true;
            }
        } else if (FProductionContainerMOListFCtrl.LABOR_STAFF.equals(reference)) {
            // if (getBean().getListInputOperations().size() == 0 && getBean().getListOutputOperations().size() == 0) {
            // ret = false;
            // } else {
            ret = true;
            // }
        } else if (FProductionContainerMOListFCtrl.BTN_OPEN_INCIDENT.equals(reference)) {
            ret = true;
        } else if (ButtonReference.SETTING_INFORMATION.getReference().equals(reference)) {
            ret = true;
        } else if (ButtonReference.DEBUG_REFERENCE.getReference().equals(reference)) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMOListVBean updateBean(final FMOListVBean bean) throws UIException, UIErrorsException {
        return null;
    }

    /**
     * format the question message.
     * 
     * @param bean the operation bean
     * @param question questions.
     * @return the string
     */
    private String formatQuestion(final AbstractOperationItemBean bean, final String question) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - formatQuestion(bean=" + bean + ", question=" + question + ")");
        }

        StringBuilder strBQuestion = new StringBuilder();

        strBQuestion.append("\n").append(I18nClientManager.translate(ProductionMo.T29519, false)).append(" ")
        .append(bean.getItemCL().getCode()).append(" : \n");
        strBQuestion.append(question).append("\n");
        strBQuestion.append(I18nClientManager.translate(ProductionMo.T29570, false));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - formatQuestion(bean=" + bean + ", question=" + question + ")");
        }
        return strBQuestion.toString();
    }

}

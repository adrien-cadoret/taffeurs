/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompGSModel.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp;


import fr.vif.jtech.ui.selection.GeneralSelectionModel;


/**
 * Production Output Scrap Selection Model.
 * 
 * @author kl
 */
public class FProductionOutputCompGSModel extends GeneralSelectionModel {
    /**
     * Default Constructor.
     */
    public FProductionOutputCompGSModel() {
        super();
        setGlobalSelectionValidation(true);
    }
}

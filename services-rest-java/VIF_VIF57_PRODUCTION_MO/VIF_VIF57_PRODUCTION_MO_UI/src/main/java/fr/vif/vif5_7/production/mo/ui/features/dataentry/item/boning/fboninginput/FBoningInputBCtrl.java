/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningInputBCtrl.java,v $
 * Created on 23 avr. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput;


import java.util.Collections;
import java.util.List;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBS;


/**
 * Browser controler for boning input.
 * 
 * @author cj
 */
public class FBoningInputBCtrl extends AbstractBrowserController<FProductionBBean> {
    private FProductionCBS fproductionCBS;

    /**
     * Default constructor.
     * 
     */
    public FBoningInputBCtrl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        FProductionSBean sBean = new FProductionSBean();
        return sBean;

    }

    /**
     * Gets the fproductionCBS.
     * 
     * @category getter
     * @return the fproductionCBS.
     */
    public FProductionCBS getFproductionCBS() {
        return fproductionCBS;
    }

    /**
     * Reposition to an input operation bean after a flash.
     * 
     * @param operationItemKey operationItemKey bean to reposit
     * @return true if reposition ok, false otherwise
     */
    public boolean repositionToOperationItemKey(final OperationItemKey operationItemKey) {
        boolean ret = false;
        // VR PERF if already reposited, don't change current row
        if (getModel().getCurrentBean() != null && getModel().getCurrentBean().getOperationItemBean() != null
                && getModel().getCurrentBean().getOperationItemBean().getOperationItemKey() != null
                && operationItemKey.equals(getModel().getCurrentBean().getOperationItemBean().getOperationItemKey())) {
            ret = true;
        } else {
            try {
                for (int i = 0; i < getModel().getRowCount(); i++) {
                    FProductionBBean bbean = getModel().getBeanAt(i);

                    if (bbean.getOperationItemBean().getOperationItemKey().equals(operationItemKey)) {
                        changeCurrentRow(i);
                        ret = true;
                        break;
                    }
                }
            } catch (UIVetoException e) {
                getView().show(e);
            }
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final BrowserActionEvent event) {
        super.selectedRowChanged(event);
    }

    /**
     * Sets the fproductionCBS.
     * 
     * @category setter
     * @param fproductionCBS fproductionCBS.
     */
    public void setFproductionCBS(final FProductionCBS fproductionCBS) {
        this.fproductionCBS = fproductionCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FProductionBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        List<FProductionBBean> lst = null;
        try {
            lst = getFproductionCBS().queryElements(getIdCtx(), (FProductionSBean) selection, startIndex, rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }
        Collections.sort(lst, getModel().getComparator());
        return lst;
    }
}

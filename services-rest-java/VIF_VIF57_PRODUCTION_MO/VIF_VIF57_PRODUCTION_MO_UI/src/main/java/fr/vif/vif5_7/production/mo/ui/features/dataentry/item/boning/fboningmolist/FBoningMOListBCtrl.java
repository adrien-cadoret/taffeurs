/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOListBCtrl.java,v $
 * Created on 15 mars 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.events.dialogs.selection.SelectionChangeEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmolist.FBoningMOListBBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.fboningmolist.FBoningMOListCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * Browser controller for boning mo list.
 * 
 * @author cj
 */
public class FBoningMOListBCtrl extends AbstractBrowserController<FBoningMOListBBean> {
    public static final String  LOGICAL_WORKSTATION = "logicalWorkstation";
    /** LOGGER. */
    private static final Logger LOGGER              = Logger.getLogger(FBoningMOListBCtrl.class);

    private FBoningMOListCBS    fboningmoListCBS;

    /**
     * Default constructor.
     * 
     */
    public FBoningMOListBCtrl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDefaultInitialSelection()");
        }

        Object o = getSharedContext().get(Domain.DOMAIN_THIS, FProductionConstant.MO_INITIAL_SELECTON);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDefaultInitialSelection()");
        }
        return (FMOListSBean) o;
    }

    /**
     * Gets the fboningmoListCBS.
     * 
     * @return the fboningmoListCBS.
     */
    public FBoningMOListCBS getFboningmoListCBS() {
        return fboningmoListCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        super.initialize();
        ((FBoningMOListBIView) getView()).initialize();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectionChanged(final SelectionChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectionChanged(event=" + event + ")");
        }

        FBoningMOListBBean bean = getModel().getCurrentBean();

        super.selectionChanged(event);

        if (bean != null) {
            setSelectedBean(bean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectionChanged(event=" + event + ")");
        }
    }

    /**
     * Sets the fboningmoListCBS.
     * 
     * @param fboningmoListCBS fboningmoListCBS.
     */
    public void setFboningmoListCBS(final FBoningMOListCBS fboningmoListCBS) {
        this.fboningmoListCBS = fboningmoListCBS;
    }

    /**
     * select the row of the bean in the browser.
     * 
     * @param bean the bean
     */
    public void setSelectedBean(final FBoningMOListBBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setSelectedBean(bean=" + bean + ")");
        }

        try {
            for (int i = 0; i < getModel().getRowCount(); i++) {

                if (bean.getMobean().getMoKey().equals(getModel().getBeanAt(i).getMobean().getMoKey())) {
                    changeCurrentRow(i);
                    break;
                }
            }
        } catch (UIVetoException e) {
            getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setSelectedBean(bean=" + bean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FBoningMOListBBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FBoningMOListBBean> lst = null;
        try {
            FMOListSBean bean = (FMOListSBean) selection;
            lst = getFboningmoListCBS().queryElements(getIdCtx(), bean, startIndex, rowNumber);

        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + lst);
        }
        return lst;
    }
}

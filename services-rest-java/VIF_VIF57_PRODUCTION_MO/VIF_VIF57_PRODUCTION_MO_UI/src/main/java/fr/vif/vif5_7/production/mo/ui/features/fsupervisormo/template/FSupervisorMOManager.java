/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOManager.java,v $
 * Created on 30 nov. 2011 by ag
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


import java.awt.Dimension;
import java.beans.XMLDecoder;
import java.beans.XMLEncoder;
import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.ToolTipManager;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.SAXException;

import com.jidesoft.docking.DefaultDockingManager;
import com.jidesoft.docking.DockingManager;
import com.jidesoft.docking.DockingPersistenceUtils;

import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.common.beans.xmlfiles.XMLFile;
import fr.vif.jtech.common.beans.xmlfiles.XMLFileDescr;
import fr.vif.jtech.common.util.i18n.SimpleTranslation;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.identification.Identification;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.xmlfiles.XMLFilesManager;
import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOGraphSelectionBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSelectionBean;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.GraphTypeEnum;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.IndicatorAxesEnum;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.IndicatorTypeValueEnum;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.FSupervisorMOFCtrl;


/**
 * Manager of local frames.
 * 
 * @author ag
 */
public class FSupervisorMOManager {

    public static final String       EXTENSION_JID          = ".jid";
    public static final String       EXTENSION_DOCKING      = "_docking";

    /** Selection rootpath . */
    public static final String       EXTENSION_XML          = ".xml";

    public static final String       USER_VIF               = "VIF";
    public static final String       VIEW_TEAM              = "team";
    public static final String       VIEW_ITEM              = "item";
    public static final String       VIEW_LINE              = "line";
    public static final String       VIEW_SECTOR            = "sector";
    public static final String       VIEW_LAST              = "last";
    public static final String       VIEW_REFERENTIEL       = "referentiel";
    public static final List<String> VIEWS_STANDARD         = Collections.unmodifiableList(Arrays.asList(new String[] {
            VIEW_SECTOR, VIEW_ITEM, VIEW_LINE, VIEW_TEAM   }));
    public static final List<String> VIEWS_STANDARD_DOCKING = Collections.unmodifiableList(Arrays.asList(new String[] {
            VIEW_SECTOR + EXTENSION_DOCKING, VIEW_ITEM + EXTENSION_DOCKING, VIEW_LINE + EXTENSION_DOCKING,
            VIEW_TEAM + EXTENSION_DOCKING                  }));

    /** LOGGER. */
    private static final Logger      LOGGER                 = Logger.getLogger(FSupervisorMOManager.class);

    /** Pattern file . */
    private static final String      PATTERN_FILE_NAME      = "*";

    /** Selection rootpath . */
    private static final String      RESSOURCES_PATH        = "/fr/vif/vif57/production/mo/supervisor/";

    /** Selection rootpath . */
    private static final String      SELECTION_ROOTPATH     = "/supervisor/production";

    private static final int         TOOLTIP_DELAY          = 10000;
    private FSupervisorMOFCtrl       ctrl;

    private int                      toolTipInitialDelay;

    private XMLFilesManager          xmlFilesManager;

    /**
     * Constructor.
     * 
     * @param ctrl FSupervisorMOFCtrl
     */
    public FSupervisorMOManager(final FSupervisorMOFCtrl ctrl) {
        this.ctrl = ctrl;
        setToolTipDelay(TOOLTIP_DELAY);
    }

    /**
     * Check the path.
     * 
     * @param path path to check
     * @param login login.
     * @return a completed path.
     */
    public static String checkPath(final String path, final String login) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkPath(path=" + path + ", login=" + login + ")");
        }

        String completePath = path;

        if (new File(path).exists()) {
            if (!path.endsWith(File.separator)) {
                completePath = path + File.separator;
            }
            if (new File(completePath + login).exists()) {
                completePath = path + login + File.separator;
            } else {
                File newDir = new File(path + login);
                // if (newDir.canWrite()) {
                newDir.mkdir();
                // } else {
                // LOGGER.error("Can not create repository - checkPath(repository=" + newDir.getAbsolutePath() + ")");
                // }

                completePath = path + login + File.separator;
            }

        } else {
            LOGGER.error("Repository not found - checkPath(path=" + path + ", login=" + login + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkPath(path=" + path + ", login=" + login + ")=" + completePath);
        }
        return completePath;
    }

    /**
     * Convert SaveFrame to LocalFrame.
     * 
     * @param identification Identification
     * @param sBean FSupervisorMOSBean
     * @param frames List<SaveFrame>
     * @param forWidget the for widget
     * @return List<LocalFrame>
     */
    public static List<MOLocalFrame> convertToLocalFrame(final Identification identification,
            final FSupervisorMOSBean sBean, final List<MOSaveFrame> frames, final boolean forWidget) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertToLocalFrame(frames=" + frames + ")");
        }

        List<MOLocalFrame> list = new ArrayList<MOLocalFrame>();
        for (MOSaveFrame saveFrame : frames) {

            if (!forWidget && saveFrame.getWidgetOnly()) {
                // This frame is only for Widget View
            } else {
                MOLocalFrame frame = null;

                String titleFrame = saveFrame.getTitleFrame();
                titleFrame = FSupervisorMOManager.getTitle(saveFrame.getTitleFrameU(), saveFrame.getTitleFrame());
                // titleFrame = saveFrame.getTitleFrameU();
                String titleGraph = saveFrame.getTitleGraph();
                titleGraph = saveFrame.getTitleGraphU();

                FSupervisorMOFrameBean frameBean = new FSupervisorMOFrameBean(titleFrame, saveFrame.getTypeFrame(),
                        identification, saveFrame.getWidth(), saveFrame.getHeight(), new Dimension(
                                saveFrame.getWidth(), saveFrame.getHeight()));
                if (EntityTools.isValid(saveFrame.getGraphType())) {
                    FSupervisorMOGraphSelectionBean selection = new FSupervisorMOGraphSelectionBean();
                    selection.setEstablishmentKey(sBean.getSelection().getEstablishmentKey());
                    selection.setPoDate(sBean.getCurrentDate());
                    selection.setAxe(IndicatorAxesEnum.getIndicatorType(saveFrame.getAxis()));
                    selection.setTypeOfIndicator(IndicatorTypeValueEnum.getTypeOfIndicator(saveFrame
                            .getTypeOfIndicator()));
                    selection.setGraphTitle(titleGraph);
                    selection.setGraphType(GraphTypeEnum.getGraphTypeEnum(saveFrame.getGraphType()));
                    frame = new MOLocalFrame(frameBean, selection);
                } else {
                    FSupervisorMOSelectionBean selection = new FSupervisorMOSelectionBean();
                    selection.setEstablishmentKey(sBean.getSelection().getEstablishmentKey());
                    selection.setPoDate(sBean.getCurrentDate());
                    selection.setTypeOfIndicator(IndicatorTypeValueEnum.getTypeOfIndicator(saveFrame
                            .getTypeOfIndicator()));
                    selection.setAxe(IndicatorAxesEnum.getIndicatorType(saveFrame.getAxis()));
                    frame = new MOLocalFrame(frameBean, selection);
                }
                frame.getFrameBean().setIsVisible(saveFrame.getIsVisible());
                frame.setUniqueId(saveFrame.getUniqueId());
                frame.setWidgetId(saveFrame.getWidgetId());

                list.add(frame);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertToLocalFrame(frames=" + frames + ")=" + list);
        }
        return list;
    }

    /**
     * 
     * Export Object to ByteArray.
     * 
     * @param beans Object
     * @return ByteArrayOutputStream
     */
    public static ByteArrayOutputStream exportXML(final Object beans) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - exportXML(beans=" + beans + ")");
        }

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        XMLEncoder encoder = new XMLEncoder(out);

        //
        // Write an XML representation of the specified object to the output.
        //

        encoder.writeObject(beans);
        encoder.close();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - exportXML(beans=" + beans + ")=" + out);
        }
        return out;
    }

    /**
     * Get title untranslatable or title to translate by code.
     * 
     * @param titleFrameU String
     * @param codeToTranslate String
     * @return String
     */
    public static String getTitle(final String titleFrameU, final String codeToTranslate) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getTitle(titleFrameU=" + titleFrameU + ", codeToTranslate=" + codeToTranslate + ")");
        }

        String title = titleFrameU;

        if (codeToTranslate != null && codeToTranslate.indexOf("#") > 0) {
            SimpleTranslation translation = new SimpleTranslation(codeToTranslate);
            if (translation != null) {
                String translate = I18nClientManager.translate(translation);
                if (!translate.equals("#FILENOTFOUND")) {
                    title = translate;
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getTitle(titleFrameU=" + titleFrameU + ", codeToTranslate=" + codeToTranslate + ")="
                    + title);
        }
        return title;
    }

    /**
     * 
     * Import Resource file to Object.
     * 
     * @param fileName String
     * @return Object
     */
    public static Object importDefaultXML(final String fileName) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - importDefaultXML(fileName=" + fileName + ")");
        }

        Object bean = null;
        InputStream stream = FSupervisorMOManager.class.getResourceAsStream(FSupervisorMOManager.RESSOURCES_PATH
                + fileName + FSupervisorMOManager.EXTENSION_XML);
        if (stream != null) {
            XMLDecoder decoder = new XMLDecoder(new BufferedInputStream(stream));
            if (decoder != null) {
                bean = decoder.readObject();

                decoder.close();
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - importDefaultXML(fileName=" + fileName + ")=" + bean);
        }
        return bean;
    }

    /**
     * 
     * Import ByteArray to Object.
     * 
     * @param in ByteArrayInputStream
     * @return Object
     */
    public static Object importXML(final ByteArrayInputStream in) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - importXML(in=" + in + ")");
        }

        Object bean = null;

        InputStream stream = new BufferedInputStream(in);

        XMLDecoder decoder = new XMLDecoder(stream);
        bean = decoder.readObject();

        decoder.close();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - importXML(in=" + in + ")=" + bean);
        }
        return bean;
    }

    /**
     * Convert LocalFrame to SaveView. (only visible frames are converted)
     * 
     * @param frames List<LocalFrame>
     * @param selection Selection
     * @return List<SaveView>
     */
    private static List<MOSaveSelection> convertLocalFrameToSaveSelection(final List<MOLocalFrame> frames,
            final Selection selection) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertLocalFrameToSaveSelection(frames=" + frames + ", selection=" + selection + ")");
        }

        List<MOSaveSelection> listSelection = new ArrayList<MOSaveSelection>();

        MOSaveSelection moSaveSelection = new MOSaveSelection();
        moSaveSelection.setSelection(selection);
        List<MOSaveView> list = convertLocalFrameToSaveView(frames);
        moSaveSelection.setViews(list);

        listSelection.add(moSaveSelection);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertLocalFrameToSaveSelection(frames=" + frames + ", selection=" + selection + ")="
                    + listSelection);
        }
        return listSelection;
    }

    /**
     * Convert LocalFrame to SaveView. (only visible frames are converted)
     * 
     * @param frames List<LocalFrame>
     * @return List<SaveView>
     */
    private static List<MOSaveView> convertLocalFrameToSaveView(final List<MOLocalFrame> frames) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertLocalFrameToSaveView(frames=" + frames + ")");
        }

        List<MOSaveView> list = new ArrayList<MOSaveView>();

        for (MOLocalFrame frame : frames) {
            if (frame.getFrameBean().getIsVisible()) {
                MOSaveView saveView = new MOSaveView(frame.getUniqueId());
                saveView.setIsVisible(frame.getFrameBean().getIsVisible());
                saveView.setTitleFrameU(frame.getFrameBean().getTitle());
                list.add(saveView);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertLocalFrameToSaveView(frames=" + frames + ")=" + list);
        }
        return list;
    }

    /**
     * Convert SaveFrame to LocalFrame.
     * 
     * @param frames List<SaveFrame>
     * @return List<LocalFrame>
     */
    public List<MOLocalFrame> convertToLocalFrame(final List<MOSaveFrame> frames) {
        return convertToLocalFrame(ctrl.getModel().getIdentification(), ctrl.getCurrentSelection(), frames, false);
    }

    /**
     * List name of the favorites views.
     * 
     * @param idCtx IdContext
     * @return a list of translated names
     */
    public List<String> getFavoriteViews(final IdContext idCtx) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getFavoriteViews(idCtx=" + idCtx + ")");
        }

        // Get views saved in XMLFiles Manager
        List<String> fileNames = new ArrayList<String>();
        List<XMLFileDescr> listFile = null;
        try {
            listFile = getXMLFilesManager().listXMLFileDescr(idCtx, FSupervisorMOManager.SELECTION_ROOTPATH,
                    getCompPath(""), "*");
        } catch (UIException e) {
            LOGGER.error("getFavoriteViews(idCtx=" + idCtx + ")=" + null, e);
        }

        Map<String, String> map = new HashMap<String, String>();

        // and add in views list
        for (XMLFileDescr file : listFile) {
            if (!file.getName().contains(FSupervisorMOManager.EXTENSION_DOCKING)) {
                String viewName = file.getName().substring(0, file.getName().length() - 4);
                if (!viewName.startsWith(VIEW_LAST) && !viewName.startsWith(VIEW_REFERENTIEL)
                        && !map.containsKey(viewName)) {
                    fileNames.add(viewName);
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getFavoriteViews(idCtx=" + idCtx + ")=" + null);
        }
        return fileNames;
    }

    /**
     * Save local frames in xml file.
     * 
     * @param dockingManager the docking manager
     * @param fileName String
     * @throws UIException if error occurs
     */
    public void loadDocking(final DockingManager dockingManager, final String fileName) throws UIException {

        InputStream inputDocking = loadXmlStream(ctrl.getIdCtx(), fileName + FSupervisorMOManager.EXTENSION_DOCKING);

        // if (FSupervisorManager.VIEW_REFERENTIEL.equals(fileName)
        // || FSupervisorManager.VIEWS_STANDARD.contains(fileName)) {
        // loadDefaultXml(fileName + FSupervisorManager.EXTENSION_DOCKING);
        // }

        // InputStream inputDocking = FSupervisorManager.class.getResourceAsStream(FSupervisorManager.RESSOURCES_PATH
        // + fileName + FSupervisorManager.EXTENSION_DOCKING + FSupervisorManager.EXTENSION_XML);
        // // FSupervisorManager.importDefaultXML(viewName + FSupervisorManager.EXTENSION_DOCKING);

        if (inputDocking != null) {
            try {
                if (true) {
                    DockingPersistenceUtils.load((DefaultDockingManager) dockingManager, inputDocking);
                }
            } catch (ParserConfigurationException e) {
                LOGGER.error("Parse error", e);
            } catch (SAXException e) {
                LOGGER.error("Sax error", e);
            } catch (IOException e) {
                LOGGER.error("IO Error", e);
            }
        }

    }

    /**
     * Load preferences defined in xml files.
     * 
     * @param <T> Class
     * @param fileName String
     * @param clazz Class<T>
     * @return <T> List<T>
     * @throws UIException if error occurs
     */
    @SuppressWarnings("unchecked")
    public <T> List<T> loadXmlFrame(final String fileName, final Class<T> clazz) throws UIException {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - loadReferenceXML(idCtx=" + ctrl.getIdCtx() + ", fileName=" + fileName + ")");
        }

        List<T> result = null;

        ByteArrayInputStream input = loadXmlStream(ctrl.getIdCtx(), fileName);

        if (input != null) {
            result = (List<T>) importXML(input);
        } else {
            LOGGER.error("B - loadXmlFrame(fileName=" + fileName + ") Input Stream is null.");
        }

        /*
         * XMLFileDescr xmlFileDescr = null; // new XMLFileDescr(SELECTION_ROOTPATH, getCompPath(fileName), fileName +
         * FSupervisorMOManager.EXTENSION_XML);
         * 
         * XMLFile xmlFile = null; // if the user's xml file does not exist, we load the VIF xml file // Always read if
         * (!getXMLFilesManager().existXMLFile(ctrl.getIdCtx(), xmlFileDescr)) { // xmlFileDescr = new
         * XMLFileDescr(SELECTION_ROOTPATH, "VIF", fileName + // FSupervisorManager.EXTENSION_XML); // // if there is no
         * other xml file, we load the default xml file (VIF) in /ressources/. // if
         * (!getXMLFilesManager().existXMLFile(idCtx, xmlFileDescr)) {
         * 
         * if (FSupervisorMOManager.VIEW_REFERENTIEL.equals(fileName) ||
         * FSupervisorMOManager.VIEWS_STANDARD.contains(fileName)) { loadDefaultXml(fileName); }
         * 
         * xmlFileDescr = new XMLFileDescr(SELECTION_ROOTPATH, getCompPath(fileName), fileName +
         * FSupervisorMOManager.EXTENSION_XML); // }
         * 
         * // }
         * 
         * if (getXMLFilesManager().existXMLFile(ctrl.getIdCtx(), xmlFileDescr)) { xmlFile =
         * getXMLFilesManager().getXMLFile(ctrl.getIdCtx(), xmlFileDescr); result = (List<T>) importXML(new
         * ByteArrayInputStream(xmlFile.getData().getBytes())); // result = readXml(xmlFile); }
         */
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - loadReferenceXML(idCtx=" + ctrl.getIdCtx() + ", fileName=" + fileName + ")=" + result);
        }
        return result;
    }

    /**
     * Load preferences defined in xml files.
     * 
     * @param idContext context
     * @param fileName String
     * @return InputStream xml stream
     * @throws UIException if error occurs
     */
    public ByteArrayInputStream loadXmlStream(final IdContext idContext, final String fileName) throws UIException {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - loadXmlStream(idCtx=" + idContext + ", fileName=" + fileName + ")");
        }

        ByteArrayInputStream result = null;

        XMLFileDescr xmlFileDescr = new XMLFileDescr(SELECTION_ROOTPATH, getCompPath(fileName), fileName
                + FSupervisorMOManager.EXTENSION_XML);

        XMLFile xmlFile = null;
        // if the user's xml file does not exist, we load the VIF xml file
        // (only for default views)
        // Always read the referentiel file !
        // if (!getXMLFilesManager().existXMLFile(idContext, xmlFileDescr)) {
        if (FSupervisorMOManager.VIEW_REFERENTIEL.equals(fileName)
                || FSupervisorMOManager.VIEWS_STANDARD.contains(fileName)
                || FSupervisorMOManager.VIEWS_STANDARD_DOCKING.contains(fileName)) {
            if (!getXMLFilesManager().existXMLFile(idContext, xmlFileDescr)) {
                loadDefaultXml(fileName);
            }

        }
        // }

        // Load the file in XMLFilesManager
        if (getXMLFilesManager().existXMLFile(idContext, xmlFileDescr)) {
            xmlFile = getXMLFilesManager().getXMLFile(idContext, xmlFileDescr);
            if (xmlFile != null) {
                result = new ByteArrayInputStream(xmlFile.getData().getBytes());
            } else {
                LOGGER.error("loadXmlStream(idCtx=" + idContext + ", fileName=" + fileName + ",compath="
                        + xmlFileDescr.getCompPath() + ") : File does not exist in XMLFilesManager");
            }
        } else {
            LOGGER.error("loadXmlStream(idCtx=" + idContext + ", fileName=" + fileName + ",compath="
                    + xmlFileDescr.getCompPath() + ") : File does not exist in XMLFilesManager");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - loadXmlStream(idCtx=" + idContext + ", fileName=" + fileName + ")=" + result);
        }
        return result;
    }

    /**
     * 
     * Reinitialize tooltip initial delay.
     */
    public void reinitToolTipDelay() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - reinitToolTipDelay()");
        }

        ToolTipManager.sharedInstance().setDismissDelay(toolTipInitialDelay);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - reinitToolTipDelay()");
        }
    }

    /**
     * Save local frames in xml file.
     * 
     * @param dockingManager the docking manager
     * @param fileName String
     * @throws UIException if error occurs
     */
    public void saveDocking(final DockingManager dockingManager, final String fileName) throws UIException {

        /******
         * VIEWS and LAST VIEW
         */
        ByteArrayOutputStream dockingPreferences = new ByteArrayOutputStream();
        DefaultDockingManager dock = (DefaultDockingManager) dockingManager;
        try {
            dock.setHeavyweightComponentEnabled(false);
            DockingPersistenceUtils.save(dock, dockingPreferences);

        } catch (ParserConfigurationException e) {
            LOGGER.error("Parse error", e);
        } catch (IOException e) {
            LOGGER.error("IO error", e);
        }

        // Sauvegarde du ByteArray dans XMLFilesManager
        exportToXMLFilesManager(dockingPreferences, fileName + FSupervisorMOManager.EXTENSION_DOCKING);

        /******
         * REFERENTIEL
         */
        // // conversion des LocalFrame en SaveFrame
        // List<SaveFrame> saveFrames = FSupervisorManager.convertLocalFrameToSaveFrame(frames);
        // // conversion des SaveFrame en ByteArray
        // ByteArrayOutputStream out = FSupervisorManager.exportXML(saveFrames);
        // // Sauvegarde du ByteArray dans XMLFilesManager
        // exportToXMLFilesManager(out, FSupervisorManager.REFERENTIEL_FILE);

    }

    /**
     * 
     * Save local frames in xml file.
     * 
     * @param frames List<LocalFrame>
     * @param fileName String
     * @param selection Selection
     * @throws UIException if error occurs
     */
    public void saveLocalFrames(final List<MOLocalFrame> frames, final String fileName, final Selection selection)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveLocalFrames(frames=" + frames + ", fileName=" + fileName + ")");
        }

        /******
         * VIEWS, LAST VIEW AND SELECTION
         */
        // convert LocalFrame to SaveView
        List<MOSaveSelection> saveSelection = convertLocalFrameToSaveSelection(frames, selection);

        // convert SaveView to ByteArray
        ByteArrayOutputStream outSelection = exportXML(saveSelection);

        // Save ByteArray to XMLFilesManager
        exportToXMLFilesManager(outSelection, fileName);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveLocalFrames(frames=" + frames + ", fileName=" + fileName + ")");
        }
    }

    /**
     * Convert to List of SaveView.
     * 
     * @param beans Object
     * @return List<SaveView>
     */
    private List<MOSaveView> convertToSaveView(final Object beans) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertToSaveView(beans=" + beans + ")");
        }

        List<MOSaveView> list = new ArrayList<MOSaveView>();
        if (beans instanceof List) {
            for (Object obj : (List) beans) {

                if (obj instanceof MOSaveView) {
                    MOSaveView bean = (MOSaveView) obj;
                    MOLocalFrame frame = null;

                    String titleFrame = bean.getTitleFrame();
                    // String titleMenu = bean.getTitleMenu();
                    String titleGraph = bean.getTitleGraph();

                    FSupervisorMOFrameBean frameBean = new FSupervisorMOFrameBean();
                    frameBean.setTitle(titleFrame);
                    if (!titleGraph.equals("")) {
                        FSupervisorMOGraphSelectionBean selection = FSupervisorMOTools
                                .getNewIndicatorGraphLineSelection(ctrl);
                        selection.setGraphTitle(titleGraph);
                        frame = new MOLocalFrame(frameBean, selection);
                    } else {
                        FSupervisorMOSelectionBean selection = FSupervisorMOTools.getNewIndicatorSelection(ctrl);
                        frame = new MOLocalFrame(frameBean, selection);
                    }
                    frame.getFrameBean().setIsVisible(bean.getIsVisible());
                    frame.setUniqueId(bean.getUniqueId());

                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertToSaveView(beans=" + beans + ")=" + list);
        }
        return list;
    }

    /**
     * Export ByteArray to XMLFilesManager.
     * 
     * @param idCtx
     * @param out ByteArrayOutputStream
     * @param fileName String
     * @throws UIException if error occurs
     */
    private void exportToXMLFilesManager(final ByteArrayOutputStream out, final String fileName) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - exportToXMLFilesManager(out=" + out + ", fileName=" + fileName + ")");
        }

        XMLFileDescr xmlFileDescr = new XMLFileDescr(SELECTION_ROOTPATH, getCompPath(fileName), fileName
                + FSupervisorMOManager.EXTENSION_XML);

        String xmlData = out.toString();

        XMLFile xmlFile = null;
        // if the user's xml file does not exist, we display the list of xml file existing
        if (!getXMLFilesManager().existXMLFile(ctrl.getIdCtx(), xmlFileDescr)) {
            xmlFile = new XMLFile(SELECTION_ROOTPATH, xmlFileDescr.getCompPath(), xmlFileDescr.getName(),
                    "Personalized view", xmlData, 0);
            if (xmlFile != null) {
                xmlFile.setData(xmlData);
                getXMLFilesManager().insertXMLFile(ctrl.getIdCtx(), xmlFile);
            }
        } else {
            xmlFile = getXMLFilesManager().getXMLFile(ctrl.getIdCtx(), xmlFileDescr);
            if (xmlFile != null) { // NLE 25/07/2013 QA006126
                xmlFile.setData(xmlData);
                getXMLFilesManager().updateXMLFile(ctrl.getIdCtx(), xmlFile);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - exportToXMLFilesManager(out=" + out + ", fileName=" + fileName + ")");
        }
    }

    /**
     * Define Compath for XMLFilesManager.
     * 
     * @param selectionFileName String
     * @return String
     */
    private String getCompPath(final String selectionFileName) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getCompPath(selectionFileName=" + selectionFileName + ")");
        }

        String ret = ctrl.getIdCtx().getLogin();

        if (VIEW_REFERENTIEL.equals(selectionFileName) || VIEWS_STANDARD.contains(selectionFileName)
                || VIEWS_STANDARD_DOCKING.contains(selectionFileName)) {
            ret = USER_VIF;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getCompPath(selectionFileName=" + selectionFileName + ")=" + ret);
        }
        return ret;
    }

    /**
     * Returns the XML Files Manager.
     * 
     * @return XMLFilesManager
     * @throws UIException UIException
     */
    private XMLFilesManager getXMLFilesManager() throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getXMLFilesManager()");
        }

        if (xmlFilesManager == null) {
            xmlFilesManager = StandardControllerFactory.getInstance().getController(XMLFilesManager.class);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getXMLFilesManager()=" + xmlFilesManager);
        }
        return xmlFilesManager;
    }

    /**
     * Import JID File from Resource.
     * 
     * @param fileName String
     */
    private void importDefaultJID(final String fileName) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - importDefaultJID(fileName=" + fileName + ")");
        }

        InputStream inStream = FSupervisorMOManager.class.getResourceAsStream(FSupervisorMOManager.RESSOURCES_PATH
                + fileName + EXTENSION_JID);
        try {
            if (inStream != null) {

                FileOutputStream outStream = new FileOutputStream(ctrl.getSupervisorCfg().getPath()
                        + ctrl.getIdCtx().getLogin() + "//" + fileName + EXTENSION_JID);
                int c = 0;

                try {
                    while ((c = inStream.read()) != -1) {
                        outStream.write(c);
                    }
                    inStream.close();
                    outStream.close();
                } catch (IOException e) {
                    LOGGER.error("Error in stream - importDefaultJID(fileName=" + fileName + ")", e);
                }
            }
        } catch (FileNotFoundException e) {
            LOGGER.error("File not found - importDefaultJID(fileName=" + fileName + ")", e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - importDefaultJID(fileName=" + fileName + ")");
        }
    }

    /**
     * Load the default xmlfile.
     * 
     * @param fileName String
     * @throws UIException if error occurs.
     */
    private void loadDefaultXml(final String fileName) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - loadDefaultXml(fileName=" + fileName + ")");
        }

        /**
         * Instead of this assignment, we should affect the data in the default xml file provided by VIF - read the file
         * - convert in string - affect in xmldata - the name of the xml file should be "supervisorPrepa.xml" or
         * "supervisorPrepaVIF.xml".
         */
        ByteArrayOutputStream outView = null;

        // Lecture du fichier ressources
        if (fileName.contains(FSupervisorMOManager.EXTENSION_DOCKING)) {
            outView = loadResourceStream(fileName);
        } else {
            Object saves = importDefaultXML(fileName);

            if (saves != null) {
                // conversion en ByteArray
                outView = FSupervisorMOManager.exportXML(saves);
            } else {
                LOGGER.error("loadDefaultXml(fileName=" + fileName + ") saves is null");
            }
        }

        // Sauvegarde du ByteArray dans XMLFilesManager
        exportToXMLFilesManager(outView, fileName);

        // importDefaultJID(fileName);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - loadDefaultXml(fileName=" + fileName + ")");
        }
    }

    /**
     * Load Resource Stream.
     * 
     * @param fileName String
     * @return ByteArrayOutputStream
     */
    private ByteArrayOutputStream loadResourceStream(final String fileName) {
        InputStream input = FSupervisorMOManager.class.getResourceAsStream(FSupervisorMOManager.RESSOURCES_PATH
                + fileName + FSupervisorMOManager.EXTENSION_XML);
        ByteArrayOutputStream outView = new ByteArrayOutputStream();
        int c = 0;

        try {
            while ((c = input.read()) != -1) {
                outView.write(c);
            }
            input.close();
            outView.close();
        } catch (IOException e) {
            LOGGER.error("Error in stream - loadResourceStream(fileName=" + fileName + ")", e);
        }
        return outView;
    }

    /**
     * 
     * Update ToolTip delay.
     * 
     * @param delay delay
     */
    private void setToolTipDelay(final int delay) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setToolTipDelay(delay=" + delay + ")");
        }

        toolTipInitialDelay = ToolTipManager.sharedInstance().getDismissDelay();
        ToolTipManager.sharedInstance().setDismissDelay(delay);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setToolTipDelay(delay=" + delay + ")");
        }
    }
}

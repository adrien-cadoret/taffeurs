/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOJTableModel.java,v $
 * Created on 6 Dec 2007 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common;


import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;

import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOValues;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FIndicatorMOBean;


/**
 * 
 * Model of the Table .
 * 
 * @author jd
 */
public class FIndicatorMOJTableModel extends DefaultTableModel {

    private List<IndicatorMOValues> list = new ArrayList<IndicatorMOValues>();

    /**
     * Constructor.
     * 
     */
    public FIndicatorMOJTableModel() {
        super();
        list = new ArrayList<IndicatorMOValues>();
    }

    /**
     * Constructor.
     * 
     * @param list list to show
     */
    public FIndicatorMOJTableModel(final List<IndicatorMOValues> list) {
        if (list != null) {
            this.list = list;
        } else {
            this.list.clear();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void addTableModelListener(final TableModelListener l) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<?> getColumnClass(final int columnIndex) {

        return FIndicatorMOBean.class;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getColumnCount() {

        return 1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getColumnName(final int columnIndex) {

        return "";
    }

    /**
     * Gets the list.
     * 
     * @category getter
     * @return the list.
     */
    public List<IndicatorMOValues> getList() {
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getRowCount() {
        int ret = 0;
        if (list != null) {
            ret = list.size();
        } else {
            ret = 0;
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        return list.get(rowIndex);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellEditable(final int rowIndex, final int columnIndex) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void removeTableModelListener(final TableModelListener l) {

    }

    /**
     * Sets the list.
     * 
     * @category setter
     * @param list list.
     */
    public void setList(final List<IndicatorMOValues> list) {
        if (list != null) {
            this.list = list;
        } else {
            this.list.clear();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setValueAt(final Object aValue, final int rowIndex, final int columnIndex) {

    }
}

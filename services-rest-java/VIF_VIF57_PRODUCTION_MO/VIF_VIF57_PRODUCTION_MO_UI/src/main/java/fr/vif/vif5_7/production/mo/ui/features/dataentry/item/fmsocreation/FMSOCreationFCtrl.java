/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMSOCreationFCtrl.java,v $
 * Created on 28 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.toolbar.buttons.toolbarbutton.ToolBarButtonKey;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmsocreation.FMSOCreationVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmsocreation.FMSOCreationCBS;


/**
 * MSO creation feature controller.
 * 
 * @author glc
 */
public class FMSOCreationFCtrl extends StandardFeatureController implements DialogChangeListener {

    public static final String  EVENT_CREATESTARTEDMSO = "createStartedMSO";
    public static final String  EVENT_OPENCRITERIA     = "openCriteria";

    /** LOGGER. */
    private static final Logger LOGGER                 = Logger.getLogger(FMSOCreationFCtrl.class);

    private ToolBarBtnStdModel  btnCreateStartedMSO    = null;
    private ToolBarBtnStdModel  btnCriteria            = null;

    private FMSOCreationCBS     fmSOCreationCBS;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        String reference = event.getModel().getReference();
        if (reference == EVENT_CREATESTARTEDMSO) {
            if (((FMSOCreationVCtrl) getViewerController()).firePerformSave()) {
                fireSelfFeatureClosingRequired();
            }
        } else if (reference == EVENT_OPENCRITERIA) {
            CriteriaDBean criteriaDBean = new CriteriaDBean(
                    ((FMSOCreationVBean) getViewerController().getBean()).getReturnInitBean(),
                    ((FMSOCreationVBean) getViewerController().getBean()).getMoItemCL().getCode());
            WorkShopCriteriaTools.showCriteria(getView(), this, this, getModel().getIdentification(), criteriaDBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {

    }

    /**
     * Gets the fmSOCreationCBS.
     * 
     * @category getter
     * @return the fmSOCreationCBS.
     */
    public FMSOCreationCBS getFmSOCreationCBS() {
        return fmSOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        super.initialize();
        setTitle(getModel().getTitle());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Sets the fmSOCreationCBS.
     * 
     * @category setter
     * @param fmSOCreationCBS fmSOCreationCBS.
     */
    public void setFmSOCreationCBS(final FMSOCreationCBS fmSOCreationCBS) {
        this.fmSOCreationCBS = fmSOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> list = new ArrayList<ToolBarBtnStdBaseModel>();
        list.add(getBtnCreateStartedMSO());
        list.add(getBtnCriteria());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return list;
    }

    /**
     * Gets the btnCreateStartedMSO.
     * 
     * @category getter
     * @return the btnCreateStartedMSO.
     */
    private ToolBarBtnStdModel getBtnCreateStartedMSO() {
        if (btnCreateStartedMSO == null) {
            btnCreateStartedMSO = new ToolBarBtnStdModel();
            btnCreateStartedMSO.setIconURL("/images/vif/vif57/production/mo/ok.png");
            btnCreateStartedMSO.setReference(EVENT_CREATESTARTEDMSO);
            btnCreateStartedMSO.setKey(new ToolBarButtonKey(1));
            btnCreateStartedMSO.setText(I18nClientManager.translate(Jtech.T12758, false));

        }
        return btnCreateStartedMSO;
    }

    /**
     * Gets the btnOpenCriteria.
     * 
     * @category getter
     * @return the btnOpenCriteria.
     */
    private ToolBarBtnStdModel getBtnCriteria() {
        if (btnCriteria == null) {
            btnCriteria = new ToolBarBtnStdModel();
            btnCriteria.setIconURL("/images/vif/vif57/production/mo/criteria.png");
            btnCriteria.setReference(EVENT_OPENCRITERIA);
            btnCriteria.setKey(new ToolBarButtonKey(2));
            btnCriteria.setText(I18nClientManager.translate(GenKernel.T29329, false));
        }
        return btnCriteria;
    }

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListBModel.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.ProductionMoTools;


/**
 * DataEntryList : Browser Model.
 * 
 * @author vr
 */
public class FDataEntryListBModel extends BrowserModel<FDataEntryListBBean> {

    /**
     * Default constructor.
     * 
     */
    public FDataEntryListBModel() {
        super();
        setBeanClass(FDataEntryListBBean.class);
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T9520, false),
                FDataEntryListBBeanEnum.LINE_NUMBER.getValue(), 70, "####"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29437, false),
                FDataEntryListBBeanEnum.FORMATTED_CREATION_HOUR.getValue(), 140, "dd/MM/yy HH:mm"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29552, false),
                FDataEntryListBBeanEnum.BATCH.getValue(), 170, "15"));
        addColumn(new BrowserColumn(I18nClientManager.translate(GenKernel.T426, false),
                FDataEntryListBBeanEnum.ITEM_ID.getValue(), 170, "15"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29553, false),
                FDataEntryListBBeanEnum.FORMATTED_FIRST_QTY_UNIT.getValue(), 130));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29553, false),
                FDataEntryListBBeanEnum.FORMATTED_SECOND_QTY_UNIT.getValue(), 130));

        setFetchSize(10000);
        setFirstFetchSize(10000);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FDataEntryListBBean bean) {

        Format format = super.getCellFormat(rowNum, property, cellContent, bean);

        if (getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.USE_TOUCH_MODEL) != null) {
            boolean useTouchModel = (Boolean) getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.USE_TOUCH_MODEL);
            if (useTouchModel) {
                TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT,
                        TouchModel.TOUCH_MODEL);

                if (touchModel != null) {

                    OutputEntityType outputEntityType = OutputEntityType.BATCH_ITEM;
                    InputEntityType inputEntityType = InputEntityType.BATCH_ITEM;

                    TouchModelFunction touchModelFunction = touchModel
                            .getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_BATCH_ITEM);

                    if (getSharedContext().get(Domain.DOMAIN_PARENT, "inputEntityType") != null) {
                        inputEntityType = (InputEntityType) getSharedContext().get(Domain.DOMAIN_PARENT,
                                "inputEntityType");

                        if (InputEntityType.CONTAINER.equals(inputEntityType)) {
                            touchModelFunction = touchModel.getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_NSC);
                        }
                    } else if (getSharedContext().get(Domain.DOMAIN_PARENT, "outputEntityType") != null) {
                        outputEntityType = (OutputEntityType) getSharedContext().get(Domain.DOMAIN_PARENT,
                                "outputEntityType");

                        if (OutputEntityType.CONTAINER.equals(outputEntityType)) {
                            touchModelFunction = touchModel.getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_NSC);
                        } else if (OutputEntityType.CONTAINER_UPPER_CONTAINER.equals(outputEntityType)) {
                            touchModelFunction = touchModel
                                    .getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_NSCP);
                        } else {
                            outputEntityType = OutputEntityType.BATCH_ITEM;
                        }
                    }

                    format.setFont(ProductionMoTools.formatCell(format, touchModelFunction, property));

                }
            }
        }

        return format;
    }
}

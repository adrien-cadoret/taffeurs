/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownTreeTableView.java,v $
 * Created on 18 nov. 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing;


import java.awt.Color;

import javax.swing.JTable;

import org.jdesktop.swingx.JXTreeTable;

import fr.vif.jtech.ui.events.generic.ModelChangeEvent;
import fr.vif.jtech.ui.treetable.InputTreeTableModel;
import fr.vif.jtech.ui.treetable.swing.SwingInputTreeTable;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.FTimeBreakDownTreeTableModel;


/**
 * Tree table view for time breakdown.
 * 
 * @author cj
 */
public class FTimeBreakDownTreeTableView extends SwingInputTreeTable {

    /**
     * Constructor.
     * 
     * @param model model
     */
    public FTimeBreakDownTreeTableView(final InputTreeTableModel model) {
        super(model);
    }

    /**
     * 
     * constructor.
     * 
     * @param model model
     * @param controllerClass controller class
     */
    @SuppressWarnings("unchecked")
    public FTimeBreakDownTreeTableView(final InputTreeTableModel model, final Class controllerClass) {
        super(model, controllerClass);
        getJxTreeTable().setShowGrid(true, true);
        getJxTreeTable().setRootVisible(false);
        getJxTreeTable().setGridColor(Color.GRAY.brighter());
        getJxTreeTable().setTerminateEditOnFocusLost(true);
        getJxTreeTable().setColumnControlVisible(true);
        getJxTreeTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JXTreeTable getJxTreeTable() {
        return super.getJxTreeTable();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void modelChanged(final ModelChangeEvent event) {
        super.modelChanged(event);

        if (event != null && event.getSource() != null && event.getSource() instanceof FTimeBreakDownTreeTableModel) {
            Object[] selected = getCurrentSelectedNodePath();
            if (selected != null) {
                fireValueChanged();
                refreshValues();
            }
        }
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputBatchBView.java,v $
 * Created on 12 déc. 2008 by gv
 */
package fr.vif.vif5_7.production.mo.ui.composites.cproductioninputbatch.touch;


import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.browser.touch.renderer.row.DefaultRowRenderer;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputbatch.FProductionInputBatchBean;


/**
 * FProductionInputBatch : Browser View.
 * 
 * @author gv
 */
public class FProductionInputBatchBView extends TouchBrowser<FProductionInputBatchBean> {

    /**
     * Default constructor.
     *
     */
    public FProductionInputBatchBView() {
        super();
        ((DefaultRowRenderer) getRowRenderer()).setRendererType(BrowserModel.RENDERER_BY_ROW);
    }

}

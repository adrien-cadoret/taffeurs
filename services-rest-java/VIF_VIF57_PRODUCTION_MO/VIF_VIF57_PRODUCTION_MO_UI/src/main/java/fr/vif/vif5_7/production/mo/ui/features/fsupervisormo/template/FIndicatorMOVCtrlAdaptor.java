/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOVCtrlAdaptor.java,v $
 * Created on 21 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.production.mo.business.services.features.indicator.ProductivityMOCBS;


/**
 * FIndicatorVCtrlAdaptor.
 * 
 * @author jd
 * @param <V> viewer bean
 */
public class FIndicatorMOVCtrlAdaptor<V> extends AbstractStandardViewerController<V> {
    private static final Logger LOGGER            = Logger.getLogger(FIndicatorMOVCtrlAdaptor.class);

    private ProductivityMOCBS   productivityMOCBS = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public V convert(final Object bean) throws UIException {

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteBean(final V bean) throws UIException {

    }

    /**
     * Gets the productivityMOCBS.
     * 
     * @return the productivityMOCBS.
     */
    public ProductivityMOCBS getProductivityMOCBS() {
        return productivityMOCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V insertBean(final V bean, final V initialBean) throws UIException, UIErrorsException {

        return null;
    }

    /**
     * Sets the productivityMOCBS.
     * 
     * @param productivityMOCBS productivityMOCBS.
     */
    public void setProductivityMOCBS(final ProductivityMOCBS productivityMOCBS) {
        this.productivityMOCBS = productivityMOCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public V updateBean(final V bean) throws UIException, UIErrorsException {

        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

}

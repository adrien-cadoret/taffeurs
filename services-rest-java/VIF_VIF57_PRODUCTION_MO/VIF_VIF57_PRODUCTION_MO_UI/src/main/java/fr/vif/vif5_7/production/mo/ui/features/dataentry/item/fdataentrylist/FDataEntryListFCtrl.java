/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListFCtrl.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.FatalException;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.bean.BeanUpdateEvent;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaReturnInitBean;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.Unit;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fdataentrylist.FDataEntryListCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.FunctionnalityTools;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.touch.FDataEntryListGSView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylistcomp.FDataEntryListCompFModel;
import fr.vif.vif5_7.stock.kernel.business.beans.features.quality.nc.fncflow.FNCFlowSharedContextBean;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCConstants;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCActType;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCType;
import fr.vif.vif5_7.stock.kernel.ui.util.StockUITools;


/**
 * DataEntryList : Feature Controller.
 * 
 * @author vr
 */
public class FDataEntryListFCtrl extends StandardFeatureController implements DialogChangeListener {

    /* Buttons References */
    public static final String        CRITERIA_BTN                = ButtonReference.CRITERIA_BTN.getReference();

    public static final String        DELETE_BTN                  = ButtonReference.DELETE_MVT_BTN.getReference();
    public static final String        MSG_OPEN_CONTAINER          = "msg_open_container";
    public static final String        OPERATION_ITEM_BEAN         = "operationItemBean";
    public static final String        PRINT_BTN                   = ButtonReference.PRINT_BTN.getReference();
    public static final String        PRINT_CONTAINER_BTN         = ButtonReference.PRINT_CONTAINER_BTN.getReference();
    public static final String        BTN_OPEN_INCIDENT_REFERENCE = ButtonReference.OPEN_INCIDENT_REFERENCE_BTN
                                                                          .getReference();
    /** The Constant LOGGER. */
    private static final Logger       LOGGER                      = Logger.getLogger(FDataEntryListFCtrl.class);

    /* Buttons models */
    private ToolBarBtnStdModel        delBtn                      = null;

    private ToolBarBtnStdModel        printBtn                    = null;
    private ToolBarBtnStdModel        printContainerBtn           = null;
    private ToolBarBtnStdModel        criteriaBtn                 = null;
    private ToolBarBtnStdModel        incidentBtn                 = null;
    private AbstractOperationItemBean abstractOperationItemBean   = null;

    private CriteriaReturnInitBean    criteriaReturnInitBean      = null;
    private FDataEntryListCBS         fdataEntryListCBS;

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        super.buttonAction(event);
        try {
            if (DELETE_BTN.equals(event.getModel().getReference())) {
                actionDelete();
            } else if (PRINT_BTN.equals(event.getModel().getReference())) {
                actionPrint();
            } else if (PRINT_CONTAINER_BTN.equals(event.getModel().getReference())) {
                actionPrintContainer();
            } else if (CRITERIA_BTN.equals(event.getModel().getReference())) {
                actionCriteria();
            } else if (BTN_OPEN_INCIDENT_REFERENCE.equals(event.getModel().getReference())) {
                actionOpenIncident();
            }
        } catch (BusinessException e) {
            // The container is closed, do you want to reopen it?
            if (new BusinessException(new FatalException(MSG_OPEN_CONTAINER)).getMessage().equals(e.getMessage())) {
                if (MessageButtons.NO == getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                        I18nClientManager.translate(ProductionMo.T8718), MessageButtons.YES_NO)) {
                    getView().showError(
                            I18nClientManager.translate(Generic.T21381, false, new VarParamTranslation(null)));
                } else {
                    FDataEntryListBBean bBean = (FDataEntryListBBean) getBrowserController().getModel()
                            .getCurrentBean();
                    try {
                        getFdataEntryListCBS().deleteBean(getIdCtx(), bBean, true);
                    } catch (BusinessException e1) {
                        getView().showError(e1.getLocalizedMessage());
                    } catch (BusinessErrorsException e1) {
                        ((FDataEntryListVCtrl) getViewerController()).showUIErrorsException(new UIErrorsException(e1));
                    }
                    getBrowserController().beanDeleted(new BeanUpdateEvent(this, BeanUpdateEvent.EVENT_DELETE, bBean));
                    recalculateSums();
                }
            } else {
                getView().showError(e.getLocalizedMessage());
            }
        } catch (BusinessErrorsException e) {
            ((FDataEntryListVCtrl) getViewerController()).showUIErrorsException(new UIErrorsException(e));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }

        CriteriaDBean criteriaDBean = (CriteriaDBean) event.getDialogBean();
        try {
            getFdataEntryListCBS().afterCriteria(getIdCtx(), criteriaDBean.getCriteriaReturnInitBean());
        } catch (BusinessException e) {
            showError(e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    /**
     * Gets the abstractOperationItemBean.
     * 
     * @return the abstractOperationItemBean.
     */
    public AbstractOperationItemBean getAbstractOperationItemBean() {
        return abstractOperationItemBean;
    }

    /**
     * Gets the fdataEntryListCBS.
     * 
     * @category getter
     * @return the fdataEntryListCBS.
     */
    public FDataEntryListCBS getFdataEntryListCBS() {
        return fdataEntryListCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        readSelection();
        super.initialize();
        recalculateSums();
        if (abstractOperationItemBean != null && getGeneralSelectionController() != null) {
            ((FDataEntryListFIView) getView()).setSubtitle(I18nClientManager.translate(ProductionMo.T29500, false,
                    new VarParamTranslation(abstractOperationItemBean.getOperationItemKey().getChrono().getChrono()))
                    + " - "
                    + I18nClientManager.translate(GenKernel.T426, false)
                    + " : "
                    + abstractOperationItemBean.getItemCL().getCode()
                    + " - "
                    + abstractOperationItemBean.getItemCL().getLabel());
            getGeneralSelectionController().getModel().setBean(abstractOperationItemBean);
        }
        String title = (String) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.FEATURE_TITLE);
        if (title == null) {
            title = getModel().getTitle();
        }
        setTitle(title);

        MONatureComplementaryWorkshop natureComplementaryWorkshop = (MONatureComplementaryWorkshop) getSharedContext()
                .get(Domain.DOMAIN_PARENT, FDataEntryListCompFModel.CONTEXT_NATURE);
        if (getGeneralSelectionController() != null
                && getGeneralSelectionController().getView() instanceof FDataEntryListGSView) {
            if (natureComplementaryWorkshop != null) {
                ((FDataEntryListGSView) getGeneralSelectionController().getView()).getNatureLabel().setValue(
                        I18nClientManager.translate(natureComplementaryWorkshop.getTranslation()));
            } else {
                ((FDataEntryListGSView) getGeneralSelectionController().getView()).getNatureLabel().setVisible(false);
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Sets the abstractOperationItemBean.
     * 
     * @param abstractOperationItemBean abstractOperationItemBean.
     */
    public void setAbstractOperationItemBean(final AbstractOperationItemBean abstractOperationItemBean) {
        this.abstractOperationItemBean = abstractOperationItemBean;
    }

    /**
     * Sets the fdataEntryListCBS.
     * 
     * @category setter
     * @param fdataEntryListCBS fdataEntryListCBS.
     */
    public void setFdataEntryListCBS(final FDataEntryListCBS fdataEntryListCBS) {
        this.fdataEntryListCBS = fdataEntryListCBS;
    }

    /**
     * format the quantity.
     * 
     * @param quantity q
     * @param unit u
     * @return str
     */
    protected String formatSum(final Double quantity, final Unit unit) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - formatSum(quantity=" + quantity + ", unit=" + unit + ")");
        }

        String retval = "";
        if (unit != null) {
            StringBuilder decimalFormat = new StringBuilder().append("###,###,##0");
            for (int i = 1; i <= unit.getDecNb(); i++) {
                if (i == 1) {
                    decimalFormat.append(".0");
                } else {
                    decimalFormat.append("0");
                }
            }
            retval = new StringBuilder().append(new DecimalFormat(decimalFormat.toString()).format(quantity))
                    .toString();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - formatSum(quantity=" + quantity + ", unit=" + unit + ")=" + retval);
        }
        return retval;
    }

    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lstBtn = new ArrayList<ToolBarBtnStdBaseModel>();
        lstBtn.add(getDelBtn());
        lstBtn.add(getPrintBtn());
        lstBtn.add(getCriteriaBtn());
        lstBtn.add(getIncidentBtn());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lstBtn;
    }

    /**
     * Gets the criteria button.
     * 
     * @return the criteria button
     */
    protected ToolBarBtnStdModel getCriteriaBtn() {
        if (criteriaBtn == null) {
            criteriaBtn = new ToolBarBtnStdModel();
            criteriaBtn.setReference(CRITERIA_BTN);
            criteriaBtn.setText(I18nClientManager.translate(GenKernel.T29329, false));
            criteriaBtn.setIconURL("/images/vif/vif57/production/mo/criteria.png");
        }
        return criteriaBtn;
    }

    /**
     * Gets the delete button.
     * 
     * @return the delete button
     */
    protected ToolBarBtnStdModel getDelBtn() {
        if (delBtn == null) {
            delBtn = new ToolBarBtnStdModel();
            delBtn.setReference(DELETE_BTN);
            delBtn.setText(I18nClientManager.translate(ProductionMo.T29516, false));
            delBtn.setIconURL("/images/vif/vif57/production/mo/cancel.png");
        }
        return delBtn;
    }

    /**
     * Gets the incident button.
     * 
     * @return the incident button
     */
    protected ToolBarBtnStdModel getIncidentBtn() {
        if (incidentBtn == null) {
            incidentBtn = new ToolBarBtnStdModel();
            incidentBtn = new ToolBarBtnStdModel();
            incidentBtn.setReference(BTN_OPEN_INCIDENT_REFERENCE);
            incidentBtn.setText(I18nClientManager.translate(ProductionMo.T32884, false));
            incidentBtn.setIconURL("/images/vif/vif57/production/mo/img48x48/incidentAdd.png");
            incidentBtn.setFunctionality(true);
        }
        return incidentBtn;
    }

    /**
     * Gets the print button.
     * 
     * @return the print button
     */
    protected ToolBarBtnStdModel getPrintBtn() {
        if (printBtn == null) {
            printBtn = new ToolBarBtnStdModel();
            printBtn.setReference(PRINT_BTN);
            printBtn.setText(I18nClientManager.translate(ProductionMo.T29515, false, new VarParamTranslation(null)));
            printBtn.setIconURL("/images/vif/vif57/production/mo/print.png");
        }
        return printBtn;
    }

    /**
     * Gets the print-container button.
     * 
     * @return the prints the container button
     */
    protected ToolBarBtnStdModel getPrintContainerBtn() {
        if (printContainerBtn == null) {
            printContainerBtn = new ToolBarBtnStdModel();
            printContainerBtn.setReference(FDataEntryListInputFCtrl.PRINT_CONTAINER_BTN);
            printContainerBtn.setText("Réédition contenant");
            printContainerBtn.setIconURL("/images/vif/vif57/production/mo/print.png");
        }
        return printContainerBtn;
    }

    /**
     * Use to manage the display of the functionalities (Mo Creation, Input Production, Output Production, ...).
     * 
     * @param isDataEntryInput true if the dataEntry Input Screen will be displayed.
     * @return a list of ToolBarBtnStdBaseModel.
     */
    protected List<ToolBarBtnStdBaseModel> manageActionsButtonModels(final boolean isDataEntryInput) {
        List<ToolBarBtnStdBaseModel> lstBtn = new ArrayList<ToolBarBtnStdBaseModel>();

        TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL);

        if (touchModel != null) {
            TouchModelFunction touchModelFunction = touchModel
                    .getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_FUNCT);
            if (touchModelFunction != null) {
                lstBtn = FunctionnalityTools.manageFunctionnalitiesForDetail(isDataEntryInput,
                        TouchModelMapKey.DATA_ENTRY_LIST_FUNCT, touchModelFunction.getFunctionalities());
            }
        }

        return lstBtn;
    }

    /**
     * read the selection from shared context.
     */
    protected void readSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - readSelection()");
        }

        abstractOperationItemBean = (AbstractOperationItemBean) getSharedContext().get(Domain.DOMAIN_PARENT,
                OPERATION_ITEM_BEAN);

        if (abstractOperationItemBean != null) {
            getBrowserController().setInitialSelection(
                    new FDataEntryListSBean(abstractOperationItemBean.getOperationItemKey()));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - readSelection()");
        }
    }

    /**
     * Action criteria.
     * 
     * @throws BusinessException the business exception
     */
    private void actionCriteria() throws BusinessException {
        FDataEntryListBBean bBean = (FDataEntryListBBean) getBrowserController().getModel().getCurrentBean();
        runCriteria(getIdCtx(), bBean);
    }

    /**
     * Action delete.
     * 
     * @throws BusinessException the business exception
     * @throws BusinessErrorsException the business errors exception
     */
    private void actionDelete() throws BusinessException, BusinessErrorsException {
        int res = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                I18nClientManager.translate(ProductionMo.T29613, false), MessageButtons.YES_NO);
        if (res == MessageButtons.YES) {
            FDataEntryListBBean bBean = (FDataEntryListBBean) getBrowserController().getModel().getCurrentBean();

            getFdataEntryListCBS().deleteBean(getIdCtx(), bBean, false);
            getBrowserController().beanDeleted(new BeanUpdateEvent(this, BeanUpdateEvent.EVENT_DELETE, bBean));
            getBrowserController().reopenQuery();
            recalculateSums();
        }

    }

    /**
     * Action open incident.
     */
    private void actionOpenIncident() {
        // FDataEntryListVBean

        // Prepare parameters bean
        FNCFlowSharedContextBean parametersBean = new FNCFlowSharedContextBean();
        parametersBean.setType(NCType.INTERNAL.getKey());

        // Feed with viewer bean informations
        FDataEntryListVBean vBean = (FDataEntryListVBean) getViewerController().getBean();
        parametersBean.setActType(NCActType.MANUFACTURING_ORDER.getKey());
        parametersBean.setChronoOrigin(//
                new Chrono(//
                        vBean.getBBean().getMovementAct().getPrechro(), //
                        vBean.getBBean().getMovementAct().getChrono()));
        parametersBean.setContainerNumber(vBean.getBBean().getEntityLocationKey().getContainerNumber());
        StockItem stockItem = new StockItem();
        stockItem.setItemId(vBean.getBBean().getStockItem().getItemId());
        stockItem.setBatch(vBean.getBBean().getStockItem().getBatch());
        stockItem.setQuantities(vBean.getBBean().getStockItem().getQuantities());
        parametersBean.setStockItem(stockItem);

        // Put parameters in context and open feature
        getSharedContext().put(Domain.DOMAIN_THIS, NCConstants.NCFLOW_KEY_SHARED_CONTEXT, parametersBean);
        if (StockUITools.isTouchRunning()) {
            openChildFeature(NCConstants.FeatureName.FLOW_TOUCH.getName());
        } else {
            openChildFeature(NCConstants.FeatureName.FLOW.getName());
        }

    }

    /**
     * Action print.
     */
    private void actionPrint() {
        try {
            getFdataEntryListCBS().printMovement(getIdCtx(),
                    (FDataEntryListBBean) getBrowserController().getModel().getCurrentBean());
        } catch (BusinessException e) {
            getView().show(new UIException(e));
        }
    }

    /**
     * Action print container.
     */
    private void actionPrintContainer() {
        try {
            getFdataEntryListCBS().printContainer(getIdCtx(),
                    (FDataEntryListBBean) getBrowserController().getModel().getCurrentBean());
        } catch (BusinessException e) {
            getView().show(new UIException(e));
        }
    }

    /**
     * recalculates the Sums.
     * 
     */
    private void recalculateSums() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - recalculateSums()");
        }

        List list = getBrowserController().getModel().getBeans();

        double total1 = 0;
        String u1 = "";

        double total2 = 0;
        String u2 = "";

        for (Object o : list) {
            FDataEntryListBBean bBean = (FDataEntryListBBean) o;

            if ("".equalsIgnoreCase(u1)) {
                u1 = bBean.getKeyboardQties().getFirstQty().getUnit();
            }
            if ("".equalsIgnoreCase(bBean.getKeyboardQties().getFirstQty().getUnit())
                    || !u1.equalsIgnoreCase(bBean.getKeyboardQties().getFirstQty().getUnit())) {
                total1 = -1;
            }
            if (total1 != -1) {
                total1 += bBean.getKeyboardQties().getFirstQty().getQty();
            }
            if ("".equalsIgnoreCase(u2)) {
                u2 = bBean.getKeyboardQties().getSecondQty().getUnit();
            }
            if ("".equalsIgnoreCase(bBean.getKeyboardQties().getSecondQty().getUnit())
                    || !u2.equalsIgnoreCase(bBean.getKeyboardQties().getSecondQty().getUnit())) {
                total2 = -1;
            }
            if (total2 != -1) {
                total2 += bBean.getKeyboardQties().getSecondQty().getQty();
            }
        }

        Unit unit1 = null;
        Unit unit2 = null;
        try {
            if (!"".equals(u1)) {
                unit1 = getFdataEntryListCBS().getUnit(getIdCtx(), u1.toUpperCase());
            }
            if (!"".equals(u2)) {
                unit2 = getFdataEntryListCBS().getUnit(getIdCtx(), u2.toUpperCase());
            }
            String v1 = (total1 == -1) ? "" : formatSum(total1, unit1);
            String v2 = (total2 == -1) ? "" : formatSum(total2, unit2);
            ((FDataEntryListFIView) getView()).setSums(v1, u1, v2, u2);
        } catch (BusinessException e) {
            showError("Calcul des sommes :" + e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - recalculateSums()");
        }
    }

    /**
     * run the criteria part.
     * 
     * @param idctx the context of the bean.
     * @param bBean the FDataEntryListBBean
     * @throws BusinessException e
     */
    private void runCriteria(final IdContext idctx, final FDataEntryListBBean bBean) throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - runCriteria(idctx=" + idctx + ", bBean=" + bBean + ")");
        }

        String stageId = "";
        if (abstractOperationItemBean instanceof InputOperationItemBean) {
            stageId = "FABENT";
        } else if (abstractOperationItemBean instanceof OutputOperationItemBean) {
            stageId = "FABSOR";
        } else {
            showError("");
        }

        criteriaReturnInitBean = getFdataEntryListCBS().getCriteria(idctx, bBean, stageId);

        if (criteriaReturnInitBean.getListCriteria().size() > 0) {
            CriteriaDBean criteriaDBean = new CriteriaDBean(criteriaReturnInitBean, bBean.getStockItem().getItemId());
            WorkShopCriteriaTools.showCriteria(getView(), this, this, getModel().getIdentification(), criteriaDBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - runCriteria(idctx=" + idctx + ", bBean=" + bBean + ")");
        }
    }

}

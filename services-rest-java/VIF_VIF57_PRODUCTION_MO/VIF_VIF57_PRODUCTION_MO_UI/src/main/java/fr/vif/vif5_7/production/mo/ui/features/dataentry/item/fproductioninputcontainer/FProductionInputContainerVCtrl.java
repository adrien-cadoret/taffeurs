/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputContainerVCtrl.java,v $
 * Created on 17 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer;


// CHECKSTYLE:OFF
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.ObjectHelper;
import fr.vif.jtech.common.util.exceptions.ObjectException;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.gen.packaging.business.beans.dialog.DPackagingBean;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputBarcodeType;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fsubstituteitem.FSubstituteItemSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerCBS;
import fr.vif.vif5_7.production.mo.ui.composites.csubstituteitem.CSubstituteItemIView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionHelper;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityEnum;
import fr.vif.vif5_7.stock.kernel.StockHelper;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingSBean;


/**
 * InputOperationItem : Viewer Controller.
 * 
 * @author vr
 */
public class FProductionInputContainerVCtrl extends
AbstractFProductionInputVCtrl<FProductionInputContainerVBean, FProductionInputContainerCBS> {
    private static final Logger LOGGER = Logger.getLogger(FProductionInputContainerVCtrl.class);

    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        super.dialogValidated(event);
        if (event.getDialogBean() instanceof DPackagingBean) {
            DPackagingBean packagingBean = (DPackagingBean) event.getDialogBean();
            if (packagingBean.getPackagingCL() != null //
                    && StockHelper.isValid(packagingBean.getPackagingCL().getCode()) //
                    && StockHelper.isValid(packagingBean.getKey().getNotat())) {

                try {
                    FProductionInputContainerVBean vBean = getFproductionInputContainerCBS()
                            .analyseTattooedAndValidateEntity(getIdCtx(), packagingBean, getBean(),
                                    (FProductionSBean) getBrowserCtrl().getInitialSelection(), getWorkBean());

                    getFeatureCtrl().interpretContainer(vBean);
                } catch (BusinessException e) {
                    fireErrorsChanged(new UIException(e));
                } catch (BusinessErrorsException e) {
                    emptyVBean();
                    showUIErrorsException(new UIErrorsException(e));
                }
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    @Override
    public void validateBean(final boolean creationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - validateBean(creationMode=" + creationMode + ", pBeanProperty=" + pBeanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }

        if (pBeanProperty != null && !"barcode".equals(pBeanProperty)) {
            getBean().getEntity().getStockItemBean().getStockItem()
            .setItemId(getBean().getEntity().getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
            fireErrorsChanged();
            try {
                CEntityEnum entityEnum = null;
                if (FProductionInputContainerVBeanEnum.ITEM_ID.getValue().equals(pBeanProperty)) {
                    entityEnum = CEntityEnum.ITEM_ID;
                    // MP DC5735 11/02/2016 update parameters of item for the substitute item
                    getBean()
                            .setInputItemParameters(
                                    getFproductionInputContainerCBS().getInputItemParameters(
                                            getIdCtx(),
                                            getBean().getBBean().getOperationItemBean().getOperationItemKey()
                                                    .getEstablishmentKey(),
                                            getBean().getEntity().getStockItemBean().getStockItem().getItemId(),
                                            getWorkBean()));
                    // KL 31/01/2013 M128375: Profil item or workstation cannot be reset to consider the units defined
                    // by default in it. Unless the item have a unit conversion constant.
                    // getBean().getEntity().getStockItemBean().setKeyboardQties(new Quantities());
                    // getBean().setDocuments(fmvtOutCBS.listDocuments(getIdCtx(), getBean(), getWorkBean()));
                } else if (FProductionInputContainerVBeanEnum.BATCH_ID.getValue().equals(pBeanProperty)) {
                    // BAtch
                    entityEnum = CEntityEnum.BATCH;
                    if (StringUtils.isEmpty(getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                            .getCdepot())) {
                        FDestockingSBean destockingSBean = (FDestockingSBean) getViewerView().getItfBatch()
                                .getInitialHelpSelection();
                        getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                        .setCdepot(destockingSBean.getDepotId());
                    }
                } else if (FProductionInputContainerVBeanEnum.CONTAINER_NUMBER.getValue().equals(pBeanProperty)) {
                    if (!((String) propertyValue).isEmpty()) {
                        try {
                            FProductionInputContainerVBean vBean = ObjectHelper.copy(getBean());
                            vBean = getFproductionInputContainerCBS().analyseContainer(getIdCtx(),
                                    (String) propertyValue, vBean,
                                    (FProductionSBean) getBrowserCtrl().getInitialSelection(), getWorkBean(), true);

                            getFeatureCtrl().interpretContainer(vBean);
                        } catch (BusinessException e) {
                            fireErrorsChanged(new UIException(e));
                        } catch (BusinessErrorsException e) {
                            emptyVBean();
                            showUIErrorsException(new UIErrorsException(e));
                        } catch (ObjectException e) {
                            // Nothing to do
                        }
                    }
                }
                if (entityEnum != null) {
                    getModel().setBean(
                            getFproductionInputContainerCBS().validateEntityFields(getIdCtx(), entityEnum, getBean(),
                                    getWorkBean()));
                    // if (entityInputBean != null) {
                    if (CEntityEnum.BATCH.equals(entityEnum)) {
                        manageCriteriaWindow();
                    } else if (CEntityEnum.ITEM_ID.equals(entityEnum)) {
                        // Change batch selection
                        getViewerView().getItfBatch().setInitialHelpSelection(
                                FProductionHelper.getFDestockingSBean(getBean().getBBean().getOperationItemBean(),
                                        getBean().getEntity().getStockItemBean().getGeneralItemInfos().getItemCL()
                                        .getCode(), (FDestockingSBean) getViewerView().getItfBatch()
                                        .getInitialHelpSelection()));
                    }
                    getModel().setInitialBean(getBean());
                    getView().render();
                    getView().enableComponents(true);
                    fireButtonStatechanged();
                }
                if (!FProductionInputContainerVBeanEnum.CONTAINER_NUMBER.getValue().equals(pBeanProperty)) {
                    if (isDataEntryOK()) {
                        // getBean().setDocuments(fmvtOutCBS.listDocuments(getIdCtx(), getBean(), getWorkBean()));
                        performSave();
                    }
                }
            } catch (BusinessException e) {
                throw new UIException(e);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - validateBean(creationMode=" + creationMode + ", pBeanProperty=" + pBeanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }
    }

    @Override
    protected FProductionInputContainerVBean convert(final Object bean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }
        String oldProfileId = null;
        if (getBean() != null && getBean().getInputItemParameters() != null) {
            oldProfileId = getBean().getInputItemParameters().getProfileId();
        }

        FProductionInputContainerVBean vBean = new FProductionInputContainerVBean();
        FProductionBBean bbean = (FProductionBBean) bean;
        vBean.setBBean(bbean);
        try {
            vBean = getFproductionInputContainerCBS().convert(getIdCtx(), bbean, getWorkBean());

            // Change batch selection
            getViewerView().getItfBatch().setInitialHelpSelection(
                    FProductionHelper.getFDestockingSBean(vBean.getBBean().getOperationItemBean(), vBean.getEntity()
                            .getStockItemBean().getGeneralItemInfos().getItemCL().getCode(),
                            (FDestockingSBean) getViewerView().getItfBatch().getInitialHelpSelection()));

            // Change item selection
            CSubstituteItemIView itemView = (CSubstituteItemIView) getViewerView().getSubstituteItemView();
            itemView.setSelection(FProductionHelper.getFSubstiteSBean((InputOperationItemBean) vBean.getBBean()
                    .getOperationItemBean(), vBean.getEntity().getStockItemBean().getGeneralItemInfos().getItemCL()
                    .getCode(), (FSubstituteItemSBean) itemView.getSelection()));

            if (oldProfileId == null || !oldProfileId.equals(vBean.getInputItemParameters().getProfileId())) {
                // if new profile => reset the button state
                if (vBean.getInputItemParameters().getInterpretQty()) {
                    getWorkBean().setBarcodeType(InputBarcodeType.KEEP_QTIES);
                } else {
                    getWorkBean().setBarcodeType(InputBarcodeType.IGNORE_QTIES);
                }

                getFeatureCtrl().manageBtnFlash();
            }
        } catch (BusinessException e) {
            getView().show(new UIException(e));
            // throw new UIException(e);
        }
        getFeatureCtrl().fireBtnStdToolBarStateChangedEvent();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")=" + vBean);
        }
        return vBean;
    }

}

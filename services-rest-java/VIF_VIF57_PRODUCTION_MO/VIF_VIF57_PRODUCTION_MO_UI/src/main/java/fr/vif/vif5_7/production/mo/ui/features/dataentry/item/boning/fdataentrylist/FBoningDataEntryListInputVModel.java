/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputVModel.java,v $
 * Created on 10 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;


/**
 * Viewer Model.
 * 
 * @author xg
 */
public class FBoningDataEntryListInputVModel extends ViewerModel<FBoningDataEntryListInputBBean> {

    /**
     * Default constructor.
     */
    public FBoningDataEntryListInputVModel() {
        super();
        this.setBeanClass(FBoningDataEntryListInputBBean.class);
    }
}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOUIConstants.java,v $
 * Created on 5 févr. 2014 by lm
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


/**
 * FMarkingMO constants.
 * 
 * @author lm
 */
public final class FMarkingMOUIConstants {

    /** constants for the shared context. */
    // FMarkingMOSBean : current Selection
    public static final String CURRENT_SELECTION_BEAN     = "fMarkingMOSBean";
    // FMarkingMOInitFeatureBean: feature bean
    public static final String INIT_FEATURE_BEAN          = "fMarkingMOInitFeatureBean";
    // HashMap<CodeLabel, Date>: processing MO order with their date
    public static final String PROCESSING_LINES           = "processingLines";
    // Set<CodeLabel>: line with finished MO in SIMULATED or STARTED state (user can't hide finished MO)
    public static final String FINISHED_RUNNING_LINES     = "finishedLines";

    /** constants for the FCtrl params' model. */
    public static final String SELECTION_BEAN_PARAM_KEY   = "fMarkingMOSBean";

    /** model path inside FMarkingMOBBean. */
    public static final String BBEAN_DATE                 = "startDate";
    public static final String BBEAN_CHRONO               = "operationItemKey.chrono.chrono";
    public static final String BBEAN_MO_STATE             = "moState";
    public static final String BBEAN_ITEM_CODE            = "itemCL.code";
    public static final String BBEAN_OF_LABEL             = "lof";
    public static final String BBEAN_LEFT_QUANTITY        = "formattedQuantity";

    /** model path inside FMarkingMOVBean. */
    public static final String VBEAN_DEVICES              = "devices";
    public static final String VBEAN_ERROR_MESSAGE        = "markingErrorMessage";
    public static final String VBEAN_PROCESSING_CHRONO    = "processingItem.moKey.operationItemKey.chrono.chrono";
    public static final String VBEAN_PROCESSING_OF_LABEL  = "processingItem.moKey.lof";
    public static final String VBEAN_PROCESSING_DOCUMENTS = "processingItem.documentsString";
    public static final String VBEAN_SELECTED_CHRONO      = "selectedItem.moKey.operationItemKey.chrono.chrono";
    public static final String VBEAN_SELECTED_OF_LABEL    = "selectedItem.moKey.lof";
    public static final String VBEAN_SELECTED_DOCUMENTS   = "selectedItem.documentsString";
    public static final String VBEAN_SELECTED_COMMENT     = "selectedItem.comment";

    public static final String BTN_SHOW_ALL               = "BTN_SHOW_ALL";

    /**
     * Empty Constructor.
     */
    private FMarkingMOUIConstants() {
    }

}

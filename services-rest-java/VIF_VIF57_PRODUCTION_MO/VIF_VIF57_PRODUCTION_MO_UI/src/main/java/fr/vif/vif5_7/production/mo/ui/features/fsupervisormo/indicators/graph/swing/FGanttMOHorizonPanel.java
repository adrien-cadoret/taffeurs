/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOHorizonPanel.java,v $
 * Created on Apr 11, 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.LinkedHashMap;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.input.InputComboBoxModel;
import fr.vif.jtech.ui.input.swing.SwingInputCheckBox;
import fr.vif.jtech.ui.input.swing.SwingInputComboBox;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.GanttHorizonEnum;


/**
 * TODO Write the class' description
 *
 * @author ac
 */
public class FGanttMOHorizonPanel extends JPanel {

    private static final int   WIDTH  = 266;
    private static final int   HEIGHT = 75;

    private JPanel             checkBoxPanel;
    private JLabel             lblValeurs;
    private SwingInputComboBox cbValues;
    private SwingInputCheckBox cbOverview;

    /**
     * Constructor.
     * 
     */
    public FGanttMOHorizonPanel() {
        super();
        initialize();
    }

    private SwingInputComboBox getCbValues() {
        if (cbValues == null) {
            // Units
            LinkedHashMap<GanttHorizonEnum, String> units = new LinkedHashMap<GanttHorizonEnum, String>();

            for (GanttHorizonEnum horizonEnum : GanttHorizonEnum.values()) {
                switch (horizonEnum) {
                    case OVERVIEW:
                        units.put(horizonEnum, I18nClientManager.translate(ProductionMo.T39850, false));
                        break;
                    case HALF_AN_HOUR:
                        units.put(horizonEnum, " +/- 1/2 " + I18nClientManager.translate(GenKernel.T34344, false));
                        break;
                    case ONE_HOUR:
                        units.put(horizonEnum, " +/- 1 " + I18nClientManager.translate(GenKernel.T34344, false));
                        break;
                    default:
                        units.put(horizonEnum,
                                " +/- " + String.valueOf(Double.valueOf(horizonEnum.getValue()).intValue()) + " "
                                        + I18nClientManager.translate(Generic.T30238, false));
                        break;
                }
            }

            InputComboBoxModel model = new InputComboBoxModel();
            model.changeListItems(units);

            cbValues = new SwingInputComboBox(model);
            cbValues.setBeanProperty("horizon");
        }
        return cbValues;
    }

    private JPanel getCheckBoxPanel() {
        if (checkBoxPanel == null) {
            checkBoxPanel = new JPanel();
            GridBagLayout gblCheckBoxPanel = new GridBagLayout();
            gblCheckBoxPanel.rowWeights = new double[] { 1.0 };
            gblCheckBoxPanel.columnWeights = new double[] { 0.0, 1.0 };
            checkBoxPanel.setLayout(gblCheckBoxPanel);
            GridBagConstraints gbcLblValeurs = new GridBagConstraints();
            gbcLblValeurs.insets = new Insets(0, 0, 0, 5);
            gbcLblValeurs.gridx = 0;
            gbcLblValeurs.gridy = 0;
            checkBoxPanel.add(getLblValeurs(), gbcLblValeurs);
            GridBagConstraints gbcComboBox = new GridBagConstraints();
            gbcComboBox.fill = GridBagConstraints.BOTH;
            gbcComboBox.gridx = 1;
            gbcComboBox.gridy = 0;
            checkBoxPanel.add(getCbValues(), gbcComboBox);
            // checkBoxPanel.add(new JSeparator(Separator.HORIZONTAL));

        }
        return checkBoxPanel;
    }

    private JLabel getLblValeurs() {
        if (lblValeurs == null) {
            lblValeurs = new JLabel(I18nClientManager.translate(Jtech.T24957));
        }
        return lblValeurs;
    }

    private SwingInputCheckBox getOverviewCheckBox() {
        if (cbOverview == null) {
            cbOverview = new SwingInputCheckBox();
            // weightCB.setPreferredSize(new Dimension(200, 24));
            cbOverview.getJCheckBox().setText(I18nClientManager.translate(ProductionMo.T39850, false));
            cbOverview.setBeanProperty("overview");
        }
        return cbOverview;
    }

    /**
     * This private method is called to initialize the view.
     */
    private void initialize() {
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 0.4;
        gbc.anchor = GridBagConstraints.NORTH;
        gbc.fill = GridBagConstraints.BOTH;
        add(getCheckBoxPanel(), gbc);
        GridBagConstraints gbcOverviewCB = new GridBagConstraints();

        gbcOverviewCB.insets = new Insets(0, 0, 0, 5);
        gbcOverviewCB.gridx = 0;
        gbcOverviewCB.gridy = 1;
        gbcOverviewCB.weightx = 1;
        gbcOverviewCB.weighty = 0.6;
        gbcOverviewCB.anchor = GridBagConstraints.SOUTH;
        gbcOverviewCB.fill = GridBagConstraints.BOTH;
        add(getOverviewCheckBox(), gbcOverviewCB);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        // pack();
    }

}

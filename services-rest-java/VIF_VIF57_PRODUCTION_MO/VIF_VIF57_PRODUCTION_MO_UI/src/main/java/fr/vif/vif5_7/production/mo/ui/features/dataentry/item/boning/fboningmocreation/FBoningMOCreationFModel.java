/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOCreationFModel.java,v $
 * Created on 18 avr. 13 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation;


import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation.touch.FBoningMOCreationVView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationBModel;


/**
 * MO creation feature model.
 * 
 * @author glc
 */
public class FBoningMOCreationFModel extends StandardFeatureModel {

    /**
     * Constructor from superclass.
     */
    public FBoningMOCreationFModel() {
        super();
        this.setBrowserTriad(new BrowserMVCTriad(FMOCreationBModel.class, TouchBrowser.class, FMOCreationBCtrl.class));
        this.setViewerTriad(new ViewerMVCTriad(FBoningMOCreationVModel.class, FBoningMOCreationVView.class,
                FBoningMOCreationVCtrl.class));
        this.setTitle(I18nClientManager.translate(ProductionMo.T1231, false));
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputBComparator.java,v $
 * Created on 16 déc. 08 by gp
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import java.util.Comparator;

import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;


/**
 * Compare the bbean. Used to sort the bbean in the browser.
 * 
 * @author gp
 */
public class FBoningOutputBComparator implements Comparator<FBoningOutputBBean> {

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(final FBoningOutputBBean b1, final FBoningOutputBBean b2) {
        int ret = 0;
        if (b1.getOperationItemBean() != null && b2.getOperationItemBean() != null) {
            if (b1.getOperationItemBean().getOperationItemKey().getCounter1() < b2.getOperationItemBean()
                    .getOperationItemKey().getCounter1()) {
                ret = -1;
            } else if (b1.getOperationItemBean().getOperationItemKey().getCounter1() == b2.getOperationItemBean()
                    .getOperationItemKey().getCounter1()
                    && b1.getOperationItemBean().getOperationItemKey().getCounter2() < b2.getOperationItemBean()
                            .getOperationItemKey().getCounter2()) {
                ret = -1;
            } else if (b1.getOperationItemBean().getOperationItemKey().getCounter1() == b2.getOperationItemBean()
                    .getOperationItemKey().getCounter1()
                    && b1.getOperationItemBean().getOperationItemKey().getCounter2() == b2.getOperationItemBean()
                            .getOperationItemKey().getCounter2()
                    && b1.getOperationItemBean().getOperationItemKey().getCounter3() < b2.getOperationItemBean()
                            .getOperationItemKey().getCounter3()) {
                ret = -1;
            } else if (b1.getOperationItemBean().getOperationItemKey().getCounter1() == b2.getOperationItemBean()
                    .getOperationItemKey().getCounter1()
                    && b1.getOperationItemBean().getOperationItemKey().getCounter2() == b2.getOperationItemBean()
                            .getOperationItemKey().getCounter2()
                    && b1.getOperationItemBean().getOperationItemKey().getCounter3() == b2.getOperationItemBean()
                            .getOperationItemKey().getCounter3()
                    && b1.getOperationItemBean().getOperationItemKey().getCounter4() < b2.getOperationItemBean()
                            .getOperationItemKey().getCounter4()) {
                ret = -1;
            } else if (b1.getOperationItemBean().getOperationItemKey().getCounter1() == b2.getOperationItemBean()
                    .getOperationItemKey().getCounter1()
                    && b1.getOperationItemBean().getOperationItemKey().getCounter2() == b2.getOperationItemBean()
                            .getOperationItemKey().getCounter2()
                    && b1.getOperationItemBean().getOperationItemKey().getCounter3() == b2.getOperationItemBean()
                            .getOperationItemKey().getCounter3()
                    && b1.getOperationItemBean().getOperationItemKey().getCounter4() == b2.getOperationItemBean()
                            .getOperationItemKey().getCounter4()) {
                ret = 0;
            } else {
                // case b2 > b1
                ret = 1;
            }
        }
        return ret;
    }
}

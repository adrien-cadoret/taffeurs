/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingFCtrl.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import org.apache.log4j.Logger;

import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;


/**
 * TableLook Feature Controller.
 * 
 * @author kl
 */
public class FTableLookSettingFCtrl extends StandardFeatureController {
    /** LOGGER. */
    private static final Logger LOGGER                      = Logger.getLogger(FTableLookSettingFCtrl.class);

    private static final String REFERENCE_BTN_SETTING       = "setting";

    private static final String REFERENCE_BTN_IMPORT_EXPORT = "importExport";

    private static final String DEFAULT_PATH                = "C:\\temp\\";
    private static final String DEFAULT_MODEL_CODE          = "MOIOCO1T";

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        getViewerCtrl().setFeatureView(getView());
        getViewerCtrl().setBrowserCtrl(getBrowserCtrl());
        FTableLookVBean vBeanParent = (FTableLookVBean) getSharedContext().get(Domain.DOMAIN_PARENT, "viewerBean");
        setTitle(I18nClientManager.translate(ProductionMo.T34912, false,
                new VarParamTranslation(I18nClientManager.translate(ProductionMo.T35123)), new VarParamTranslation(
                        vBeanParent.getBrowserBean().getCode()), new VarParamTranslation(vBeanParent.getBrowserBean()
                        .getLabel())));
        super.initialize();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Cast the BrowserController in FTableLookBCtrl.
     * 
     * @return FTableLookBCtrl
     */
    private FTableLookSettingBCtrl getBrowserCtrl() {
        return (FTableLookSettingBCtrl) getBrowserController();
    }

    /**
     * Cast the ViewerController in FTableLookSettingVCtrl.
     * 
     * @return FTableLookSettingVCtrl
     */
    private FTableLookSettingVCtrl getViewerCtrl() {
        return (FTableLookSettingVCtrl) getViewerController();
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownVModel.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.jtech.ui.viewer.ViewerUpdateActions;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownVBean;


/**
 * TimeBreakDown : Viewer Model.
 * 
 * @author cj
 * @version $Revision: 1.4 $, $Date: 2016/10/07 16:15:06 $
 */
public class FTimeBreakDownVModel extends ViewerModel<FTimeBreakDownVBean> {

    /**
     * Default constructor.
     * 
     */
    public FTimeBreakDownVModel() {
        super();
        setBeanClass(FTimeBreakDownVBean.class);
        // DP2192 - CHU - inactive update panel
        ViewerUpdateActions vua = new ViewerUpdateActions();
        vua.setCopyEnabled(false);
        vua.setDeleteEnabled(false);
        vua.setDocEnabled(false);
        vua.setInfoEnabled(false);
        vua.setNewEnabled(false);
        vua.setPrintEnabled(false);
        vua.setSaveEnabled(false);
        vua.setUndoEnabled(false);
        setUpdateActions(vua);

    }

}

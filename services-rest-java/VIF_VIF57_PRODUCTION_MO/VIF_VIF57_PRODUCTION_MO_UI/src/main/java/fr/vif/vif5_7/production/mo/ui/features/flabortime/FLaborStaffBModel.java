/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffBModel.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBeanFields.LaborStaffBrowseColumns;


/**
 * Labor staff browser model.
 * 
 * @author nle
 */
public class FLaborStaffBModel extends BrowserModel<FLaborStaffBBean> {

    // doesn't function...
    private static Format formatBold   = new Format(StandardColor.BLUE, StandardColor.TOUCHSCREEN_FG_BROWSER,
                                               StandardFont.TOUCHSCREEN_DEFAULT_BROWSER_FONT);
    private static Format formatNormal = new Format(StandardColor.BLUE, StandardColor.TOUCHSCREEN_FG_BROWSER,
                                               StandardFont.TOUCHSCREEN_BROWSER_LABEL);
    private static Format formatBold2  = new Format(StandardColor.BLUE, StandardColor.TOUCHSCREEN_FG_BROWSER,
                                               StandardFont.BOLD);

    /**
     * Default constructor.
     */
    public FLaborStaffBModel() {
        super();
        setBeanClass(FLaborStaffBBean.class);

        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T34758, false),
                LaborStaffBrowseColumns.RESOURCE_CODE.getValue(), 50, formatBold));
        addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T1863, false),
                LaborStaffBrowseColumns.RESOURCE_LABEL.getValue(), 50, formatNormal));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T34746, false),
                LaborStaffBrowseColumns.STAFF.getValue(), 50, formatBold2));

        setFullyFetched(true);
        setFetchSize(9999999);
        setFirstFetchSize(999999);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FLaborStaffBBean bean) {
        Format format = super.getCellFormat(rowNum, property, cellContent, bean);

        // if (bean.getIsFinished()) {
        // format.setForegroundColor(StandardColor.GRAY);
        // }

        return format;

    }

}

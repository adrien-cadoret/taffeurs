/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOCellProgressBar.java,v $
 * Created on 11 sept. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;


/**
 * Show the advance of the line.
 * 
 * @author vr
 */
public class FIndicatorMOCellProgressBar extends FSupervisorMOPanel {

    private FIndicatorMOProgressBar progressBar;

    private String                  title = "";

    /**
     * Constructor.
     */
    public FIndicatorMOCellProgressBar() {
        super();
        this.title = "";
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param title String
     */
    public FIndicatorMOCellProgressBar(final String title) {
        super();
        this.title = title;
        initialize();
    }

    /**
     * Get a ProgressBar.
     * 
     * @return {@link FIndicatorMOProgressBar}
     */
    public FIndicatorMOProgressBar getProgressBar() {
        if (progressBar == null) {
            progressBar = new FIndicatorMOProgressBar();
        }
        return progressBar;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSize(final int width, final int height) {
        super.setSize(width, height);

        getProgressBar().setSize(width, height);
    }

    /**
     * init.
     */
    private void initialize() {
        final GridBagLayout gridBagLayout = new GridBagLayout();
        setLayout(gridBagLayout);
        final GridBagConstraints gridBagConstraints = new GridBagConstraints();
        // gridBagConstraints.insets = new Insets(0, 0, 0, 0);
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.3;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        setOpaque(false);
        final GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.weighty = 1.0;
        gridBagConstraints1.insets = new Insets(0, 0, 0, 0);
        gridBagConstraints1.fill = GridBagConstraints.BOTH;
        gridBagConstraints1.gridy = 0;
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.weightx = 1;
        add(getProgressBar(), gridBagConstraints1);
    }

}

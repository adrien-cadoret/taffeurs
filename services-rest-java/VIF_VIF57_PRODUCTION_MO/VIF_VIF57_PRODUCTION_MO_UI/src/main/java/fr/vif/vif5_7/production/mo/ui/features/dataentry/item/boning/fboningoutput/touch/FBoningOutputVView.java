/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputVView.java,v $
 * Created on 17 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput.touch;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.border.LineBorder;

import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationBBean;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationSBean;
import fr.vif.vif5_7.gen.location.ui.features.flocation.FLocationBCtrl;
import fr.vif.vif5_7.gen.location.ui.features.flocation.FLocationBModel;
import fr.vif.vif5_7.gen.location.ui.features.flocation.touch.FLocationBView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutputcontainer.FBoningOutputVBean;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput.FBoningOutputVIView;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityEnum;


/**
 * FProductionInput : Viewer View.
 * 
 * @author vr
 */
public class FBoningOutputVView extends StandardTouchViewer<FBoningOutputVBean> implements FBoningOutputVIView {

    private TouchInputTextField locationTouchView;
    private TouchInputTextField itfReCreatedItemsLeft;
    private TouchInputTextField itfProgressionQty2;
    private JLabel              jlProgression;
    private JLabel              jlReCreatedItemsLeft;
    private JProgressBar        progressBar;

    /**
     * Default contructor.
     */
    public FBoningOutputVView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableComponents(final boolean creationMode) {

        super.enableComponents(creationMode);

        if (getModel().getBean() == null || getModel().getBean().getBBean() == null
                || getModel().getBean().getBBean().getOperationItemBean() == null) {
            disableComponents();
        }
    }

    /**
     * 
     * Get the Input text field progression Qty 2 view.
     * 
     * @return the Input text field progression Qty 2 view
     */
    public TouchInputTextField getItfProgressionQty2() {

        if (itfProgressionQty2 == null) {
            itfProgressionQty2 = new TouchInputTextField(String.class, "15", "");
            itfProgressionQty2.setBounds(680, 185, 185, 70);
            itfProgressionQty2.setAlignment(Alignment.CENTER_ALIGN);

            itfProgressionQty2.setBackground(TouchHelper.getAwtColor(MOUIConstants.TOUCHSCREEN_BG_PANEL_LIGHT));
            LineBorder border = new LineBorder(Color.GRAY);
            border.getBorderInsets(itfProgressionQty2).set(2, 2, 1, 1);

            itfProgressionQty2.setForceNoHelp(true);
            itfProgressionQty2.setBeanBased(false);
            itfProgressionQty2.setFocusable(false);
        }
        return itfProgressionQty2;
    }

    /**
     * 
     * Get the itfReCreatedItemsLeft.
     * 
     * @return the itfReCreatedItemsLeft
     */
    public TouchInputTextField getItfReCreatedItemsLeft() {
        if (itfReCreatedItemsLeft == null) {
            itfReCreatedItemsLeft = new TouchInputTextField(String.class, "15", "");
            itfReCreatedItemsLeft.setBounds(180, 185, 120, 70);
            itfReCreatedItemsLeft.setAlignment(Alignment.CENTER_ALIGN);

            itfReCreatedItemsLeft.setForeground(TouchHelper.getAwtColor(StandardColor.RED));
            itfReCreatedItemsLeft.setBackground(TouchHelper.getAwtColor(MOUIConstants.TOUCHSCREEN_BG_PANEL_LIGHT));
            itfReCreatedItemsLeft.setBorder(new LineBorder(Color.GRAY));
            itfReCreatedItemsLeft.getInsets().set(2, 2, 1, 1);

            itfReCreatedItemsLeft.setForceNoHelp(true);
            itfReCreatedItemsLeft.setBeanBased(false);
            itfReCreatedItemsLeft.setFocusable(false);
        }
        return itfReCreatedItemsLeft;
    }

    /**
     * Gets the jlProgression.
     * 
     * @return the jlProgression.
     */
    public JLabel getJlProgression() {
        if (jlProgression == null) {
            jlProgression = new JLabel();
            jlProgression.setFont(new Font("Arial", Font.BOLD, 16));
            jlProgression.setText(I18nClientManager.translate(ProductionMo.T34142, false));
            jlProgression.setBounds(350, 217, 236, 25);
            jlProgression.setForeground(new Color(StandardColor.TOUCHSCREEN_FG_LABEL.getRed(),
                    StandardColor.TOUCHSCREEN_FG_LABEL.getGreen(), StandardColor.TOUCHSCREEN_FG_LABEL.getBlue()));
        }
        return jlProgression;
    }

    /**
     * Gets the jlProgression.
     * 
     * @return the jlProgression.
     */
    public JLabel getJlReCreatedItemsLeft() {
        if (jlReCreatedItemsLeft == null) {
            jlReCreatedItemsLeft = new JLabel();
            jlReCreatedItemsLeft.setFont(new Font("Arial", Font.BOLD, 16));
            jlReCreatedItemsLeft.setText(I18nClientManager.translate(ProductionMo.T34144, false));
            jlReCreatedItemsLeft.setBounds(5, 217, 236, 25);
            jlReCreatedItemsLeft.setForeground(new Color(StandardColor.TOUCHSCREEN_FG_LABEL.getRed(),
                    StandardColor.TOUCHSCREEN_FG_LABEL.getGreen(), StandardColor.TOUCHSCREEN_FG_LABEL.getBlue()));
        }
        return jlReCreatedItemsLeft;
    }

    /**
     * 
     * Get the location touch view. Not visible
     * 
     * @return the location touch view
     */
    public TouchInputTextField getLocationTouchView() {
        if (locationTouchView == null) {
            locationTouchView = new TouchInputTextField(String.class, "10",
                    I18nClientManager.translate(StockKernel.T29089));
            locationTouchView.setBeanProperty("entity." + CEntityEnum.LOCATION_ID.getValue());
            locationTouchView.setBounds(0, 0, 120, 30);
            locationTouchView.setVisible(false);
            locationTouchView.setHelpBrowserTriad(new BrowserMVCTriad(FLocationBModel.class, FLocationBView.class,
                    FLocationBCtrl.class));
            locationTouchView.setUseFieldHelp(true);

        }
        return locationTouchView;
    }

    /**
     * Get the progress bar.
     * 
     * @return the progress bar
     */
    public JProgressBar getProgressBar() {
        if (progressBar == null) {
            progressBar = new JProgressBar();
            progressBar.setFont(new Font("Arial", Font.BOLD, 20));
            progressBar.setStringPainted(true);
            progressBar.setBounds(495, 205, 185, 50);
            progressBar.setValue(0);
            progressBar.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_LABEL));
            progressBar.setBackground(TouchHelper.getAwtColor(MOUIConstants.TOUCHSCREEN_BG_PANEL_LIGHT));
        }
        return progressBar;
    }

    /**
     * {@inheritDoc}
     */
    public FLocationBBean openLocationHelp(final DisplayListener parent, final FLocationSBean sBean) {
        FLocationBBean bBean = null;
        getLocationTouchView().setInitialHelpSelection(sBean);
        bBean = openFieldHelpDialog(I18nClientManager.translate(ProductionMo.T29601, false), getLocationTouchView(),
                new FLocationBBean(), parent);
        return bBean;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        setLayout(null);
        setPreferredSize(new Dimension(870, 768));

        add(getProgressBar());
        add(getItfProgressionQty2());
        add(getJlProgression());

        add(getJlReCreatedItemsLeft());
        add(getItfReCreatedItemsLeft());

        add(getLocationTouchView());
    }
}

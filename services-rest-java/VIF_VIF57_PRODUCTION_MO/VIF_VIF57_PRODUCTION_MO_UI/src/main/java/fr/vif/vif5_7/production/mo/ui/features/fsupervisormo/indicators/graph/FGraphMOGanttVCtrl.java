/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGraphMOGanttVCtrl.java,v $
 * Created on 22 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph;


import java.util.Iterator;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGanttMOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOGraphSelectionBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOVCtrlAdaptor;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;


/**
 * Default graph viewer controller.
 * 
 * @author jd
 */
public class FGraphMOGanttVCtrl extends FIndicatorMOVCtrlAdaptor<FGanttMOBean> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FGraphMOGanttVCtrl.class);

    /**
     * {@inheritDoc}
     * 
     * @param sel
     */
    public FGanttMOBean convert(final IdContext idctx, final FSupervisorMOGraphSelectionBean sel) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(idctx=" + idctx + ", sel=" + sel + ")");
        }

        FGanttMOBean ret = null;
        try {
            ret = getProductivityMOCBS().getGanttBean(idctx, sel);
            // Deleting of all mo which have a null expected duration
            for (Iterator<IndicatorMO> iterator = ret.getIndicators().iterator(); iterator.hasNext();) {
                IndicatorMO indicatorMO = iterator.next();
                if (indicatorMO.getInfos().getExpectedBeginDate().equals(indicatorMO.getInfos().getExpectedEndDate())) {
                    iterator.remove();
                }
            }

            // Override default title

            ret.setTitle(I18nClientManager.translate(ProductionMo.T32687, false,
                    new VarParamTranslation(DateHelper.formatDate(sel.getPoDate(), FSupervisorMOTools.FORMAT_DATE)))
                    + " / " + I18nClientManager.translate(ret.getAxe().getName()));

            // NLE : not a good idea
            // // Planning du 19/02/2013 - 20/02/2013 / Secteurs
            // ret.setTitle(I18nClientManager.translate(
            // ProductionMo.T32687,
            // false,
            // new VarParamTranslation(DateHelper.formatDate(sel.getPoDate(), FSupervisorMOTools.FORMAT_DATE)
            // + " - "
            // + DateHelper.formatDate(DateHelper.addDays(sel.getPoDate(), 1),
            // FSupervisorMOTools.FORMAT_DATE)))
            // + " / " + I18nClientManager.translate(ret.getAxe().getName()));

        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(idctx=" + idctx + ", sel=" + sel + ")=" + ret);
        }
        return ret;
    }
}

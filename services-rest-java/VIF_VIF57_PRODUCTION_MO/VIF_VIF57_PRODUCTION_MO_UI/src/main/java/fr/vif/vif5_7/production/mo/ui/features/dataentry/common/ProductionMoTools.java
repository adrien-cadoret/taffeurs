/*
 * Copyright (c) 2015 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: ProductionMoTools.java,v $
 * Created on 28 mai 2015 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common;


import javax.swing.JTable;
import javax.swing.table.TableColumn;

import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.Font;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelArray;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;


/**
 * Tools class for Production MO.
 *
 * @author kl
 */
public final class ProductionMoTools {

    /**
     * Private constructor.
     */
    private ProductionMoTools() {
    }

    /**
     * Update Size of the font column.
     * 
     * @param format current format
     * @param touchModelFunction model with new value
     * @param property get the column to apply the font.
     * @return CustomFont
     */
    public static Font formatCell(final Format format, final TouchModelFunction touchModelFunction,
            final String property) {
        TouchModelArray touchColumn = touchModelFunction.retrieveModelByProperty(property);
        Font font = format.getFont();

        if (touchColumn != null) {
            font = new CustomFont(font.getName(), FontStyle.BOLD, touchColumn.getFontSize().getFontSize());
        }

        return font;
    }

    /**
     * Update with of Column define in touchModel bean in parameter.
     * 
     * @param touchModel model with column to display.
     * @param jTable Column to update
     * @param outputEntityType Detail for output production
     * @param inputEntityType Detail for input production
     */
    public static void resizeColumnDetailListFeature(final TouchModel touchModel, final JTable jTable,
            final OutputEntityType outputEntityType, final InputEntityType inputEntityType) {
        if (touchModel != null && jTable != null) {

            TouchModelFunction touchModelFunction = touchModel
                    .getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_BATCH_ITEM);

            if (inputEntityType != null && InputEntityType.CONTAINER.equals(inputEntityType)
                    || outputEntityType != null && OutputEntityType.CONTAINER.equals(outputEntityType)) {
                touchModelFunction = touchModel.getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_NSC);
            } else if (outputEntityType != null && OutputEntityType.CONTAINER_UPPER_CONTAINER.equals(outputEntityType)) {
                touchModelFunction = touchModel.getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_NSCP);
            }

            jTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

            int index = 0;
            for (TouchModelArray touchColumn : touchModelFunction.getTouchModelArrays()) {
                TableColumn tableColumn = jTable.getColumnModel().getColumn(index);
                if (tableColumn != null && touchColumn.getWithColumn() > 0) {
                    tableColumn.setMinWidth(touchColumn.getWithColumn());
                    tableColumn.setPreferredWidth(touchColumn.getWithColumn());
                    tableColumn.setMaxWidth(touchColumn.getWithColumn());
                    tableColumn.setWidth(touchColumn.getWithColumn());
                }
                ++index;
            }
        }
    }

    /**
     * Update with of Column define in touchModel bean in parameter.
     * 
     * @param touchModelFunction model with column to display.
     * @param jTable Column to update
     */
    public static void resizeColumnMoListFeature(final TouchModelFunction touchModelFunction, final JTable jTable) {
        if (touchModelFunction != null && jTable != null) {
            jTable.setAutoResizeMode(JTable.AUTO_RESIZE_LAST_COLUMN);

            int index = 0;
            for (TouchModelArray touchColumn : touchModelFunction.getTouchModelArrays()) {
                TableColumn tableColumn = jTable.getColumnModel().getColumn(index);
                if (tableColumn != null && touchColumn.getWithColumn() > 0) {
                    tableColumn.setMinWidth(touchColumn.getWithColumn());
                    tableColumn.setPreferredWidth(touchColumn.getWithColumn());
                    tableColumn.setMaxWidth(touchColumn.getWithColumn());
                    tableColumn.setWidth(touchColumn.getWithColumn());
                }
                ++index;
            }
        }
    }
}

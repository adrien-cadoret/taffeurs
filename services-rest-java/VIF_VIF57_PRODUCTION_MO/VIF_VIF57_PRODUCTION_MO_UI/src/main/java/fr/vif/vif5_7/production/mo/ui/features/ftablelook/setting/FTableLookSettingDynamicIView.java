/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingDynamicIView.java,v $
 * Created on 6 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.common.util.exceptions.BeanException;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingBBean;


/**
 * Interface of FTableLookSettingDynamicVView.
 * 
 * @author kl
 */
public interface FTableLookSettingDynamicIView {

    /**
     * Update the elements of the combo of button location.
     */
    public void changeListButtonLocation();

    /**
     * Update the elements of the combo of font size.
     * 
     */
    public void changeListFontSize();

    /**
     * Update the elements of the combo of sorts.
     * 
     * @param bBean bean use to set the selected sorts in sorts list.
     */
    public void changeListSorts(final FTableLookSettingBBean bBean);

    /**
     * disable or not the component ComboBox sort.
     * 
     * @param disable boolean to disable the component
     */
    public void disableCombobox(boolean disable);

    /**
     * display or not the component for button location.
     *
     * @param display the display
     */
    public void displayButtonLocation(boolean display);

    /**
     * display or not the component ComboBox sort.
     * 
     * @param display boolean to display the component
     */
    public void displayCombobox(boolean display);

    /**
     * disable or not the component Detail zone to custom the title of the zone selected..
     * 
     * @param display boolean to display the component
     */
    public void displayCustomZone(boolean display);

    /**
     * disable or not the component Detail zone to custom the size zone and the font zone of the zone selected.
     * 
     * @param display boolean to display the component
     */
    public void displayWithAndFontZone(boolean display);

    /**
     * Return the Interface FTabelLookSettingFlipFlopIView of FTabelLookSettingFlipFlopView.
     * 
     * @return FTabelLookSettingFlipFlopIView
     */
    public FTabelLookSettingFlipFlopIView getFlipFlopView();

    /**
     * Return the FlipFlop Controller.
     * 
     * @return FTableLookSettingFlipFlopCtrl
     */
    public FTableLookSettingFlipFlopCtrl getViewerFlipFlopController();

    /**
     * Update the component use to set the custom title of the selected zone.
     * 
     * @throws BeanException if an exception occur.
     */
    public void renderDetailSetting() throws BeanException;

    /**
     * Set the selection to the Flip Flop Composite.
     * 
     * @param initialSelection initialSelection
     */
    public void setInitialSelection(final Selection initialSelection);

    /**
     * Manage composite (Display, disable).
     * 
     * @param bBean Used to get the chapter.
     */
    public void udpdateView(final FTableLookSettingBBean bBean);

}

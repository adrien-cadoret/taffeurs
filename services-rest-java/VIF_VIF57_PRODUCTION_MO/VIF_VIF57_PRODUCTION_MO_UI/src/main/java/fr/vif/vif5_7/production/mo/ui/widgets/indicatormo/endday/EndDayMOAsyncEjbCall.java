/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: EndDayMOAsyncEjbCall.java,v $
 * Created on 22 Avril 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday;


import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.util.selection.PermanentSelectionValues;
import fr.vif.jtech.ui.controllers.Controller;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOSBean;
import fr.vif.vif5_7.production.mo.business.services.widget.fendday.FEndDayMOCBS;


/**
 * Callable object which is the engine which calls CBS.
 *
 * @author ac
 */
public class EndDayMOAsyncEjbCall implements Callable<Object>, Controller {

    /** LOGGER. */
    private static final Logger LOGGER      = Logger.getLogger(EndDayMOAsyncEjbCall.class);

    /** Callback object. */
    private IEndDayEjbCallback  mainWorker  = null;

    /** EndDay CBS. */
    private FEndDayMOCBS        endDayMOCBS = null;

    /** Current context. */
    private IdContext           idCtx       = null;

    /** Current selection. */
    private EndDayMOSBean       selection   = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public Object call() throws Exception {
        return null;
    }

    /**
     * 
     * Deletes a selection.
     * 
     * @param ctx idCONTEXT
     * @param companyId company
     * @param establishmentId establishment
     * @param userId userId
     * @param selectionName selectionName
     * @param type type
     */
    public void deletePermanentSelection(final IdContext ctx, final String companyId, final String establishmentId,
            final String userId, final String selectionName, final String type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deletePermanentSelection(ctx=" + ctx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", type=" + type
                    + ")");
        }

        try {
            getEndDayMOCBS().deletePermanentSelection(ctx, companyId, establishmentId, userId, selectionName, type);
        } catch (BusinessException e) {
            LOGGER.error("", e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deletePermanentSelection(ctx=" + ctx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", type=" + type
                    + ")");
        }
    }

    /**
     * Gets the endDayMOCBS.
     *
     * @return the endDayMOCBS.
     */
    public FEndDayMOCBS getEndDayMOCBS() {
        return endDayMOCBS;
    }

    /**
     * Gets the idCtx.
     * 
     * @return the idCtx.
     */
    public IdContext getIdCtx() {
        return idCtx;
    }

    /**
     * Gets the mainWorker.
     * 
     * @return the mainWorker.
     */
    public IEndDayEjbCallback getMainWorker() {
        return mainWorker;
    }

    /**
     * Gets the selection.
     *
     * @return the selection.
     */
    public EndDayMOSBean getSelection() {
        return selection;
    }

    /**
     * 
     * Get a selection by id (selectionName).
     * 
     * @param idCtx2 context
     * @param companyId company
     * @param establishmentId establishment
     * @param userId user
     * @param selectionName selection name (frame id)
     * @param type type
     * @return selectionContent
     */
    public Map<String, PermanentSelectionValues> getSelectionByIds(final IdContext idCtx2, final String companyId,
            final String establishmentId, final String userId, final String selectionName, final String type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getSelectionByIds(idCtx2=" + idCtx2 + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", type=" + type
                    + ")");
        }

        Map<String, PermanentSelectionValues> map = null;
        try {
            map = getEndDayMOCBS().getSelectionByIds(idCtx2, companyId, establishmentId, userId, selectionName, type);
        } catch (BusinessException e) {
            LOGGER.error("", e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getSelectionByIds(idCtx2=" + idCtx2 + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", type=" + type
                    + ")=" + map);
        }
        return map;
    }

    /**
     * 
     * Save selection with an id.
     * 
     * @param ctx context
     * @param companyId company
     * @param establishmentId establishment
     * @param userId user
     * @param selectionName selection name
     * @param labelName label
     * @param type type
     * @param values values
     * @throws BusinessException BusinessException thrown
     */
    public void saveSelectionByIds(final IdContext ctx, final String companyId, final String establishmentId,
            final String userId, final String selectionName, final String labelName, final String type,
            final Map<String, PermanentSelectionValues> values) throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveSelectionByIds(ctx=" + ctx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", labelName="
                    + labelName + ", type=" + type + ", values=" + values + ")");
        }

        getEndDayMOCBS().saveSelectionByIds(ctx, companyId, establishmentId, userId, selectionName, labelName, type,
                values);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveSelectionByIds(ctx=" + ctx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", labelName="
                    + labelName + ", type=" + type + ", values=" + values + ")");
        }
    }

    /**
     * Sets the endDayMOCBS.
     *
     * @param endDayMOCBS endDayMOCBS.
     */
    public void setEndDayMOCBS(final FEndDayMOCBS endDayMOCBS) {
        this.endDayMOCBS = endDayMOCBS;
    }

    /**
     * Sets the idCtx.
     * 
     * @param idCtx idCtx.
     */
    public void setIdCtx(final IdContext idCtx) {
        this.idCtx = idCtx;
    }

    /**
     * Sets the mainWorker.
     * 
     * @param mainWorker mainWorker.
     */
    public void setMainWorker(final IEndDayEjbCallback mainWorker) {
        this.mainWorker = mainWorker;
    }

    /**
     * Sets the selection.
     *
     * @param selection selection.
     */
    public void setSelection(final EndDayMOSBean selection) {
        this.selection = selection;
    }

}

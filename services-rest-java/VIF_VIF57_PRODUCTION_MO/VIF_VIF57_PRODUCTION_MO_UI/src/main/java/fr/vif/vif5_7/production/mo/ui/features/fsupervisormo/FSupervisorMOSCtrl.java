/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOSCtrl.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.dialogs.DialogActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.activities.activities.business.beans.features.fteam.FTeamSBean;
import fr.vif.vif5_7.activities.activities.ui.composites.cteamlist.swing.CTeamListView;
import fr.vif.vif5_7.gen.hierarchy.ui.composites.hierarchy.chchylist.CHchyListCtrl;
import fr.vif.vif5_7.gen.location.business.beans.features.farea.FAreaSBean;
import fr.vif.vif5_7.gen.location.ui.composites.carealist.swing.CAreaListView;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersSCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersSIView;


/**
 * FSupervisorMO : Selection Controller.
 * 
 * @author jd
 */
public class FSupervisorMOSCtrl extends FProcessOrdersSCtrl {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FSupervisorMOSCtrl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }

        super.dialogValidated(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        initializeArea();

        initializeTeam();

        initializeFLine();

        initializeItemHchy();

        // initializeHierarchyItems();
        //
        // initializeHierarchyRessources();

        super.initialize();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Initialize Area composite.
     */
    @Override
    public void initializeArea() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeArea()");
        }

        CAreaListView cArea = ((FProcessOrdersSIView) getView()).getcArea();

        if (cArea.getInitialHelpSelection() == null) {
            FAreaSBean selection = new FAreaSBean(true,
                    ((FSupervisorMOSBean) getModel().getDialogBean()).getEstablishmentKey());
            cArea.setInitialHelpSelection(selection);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeArea()");
        }
    }

    @Override
    public void initializeItemHchy() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeItemHchy()");
        }

        ((CHchyListCtrl) ((FProcessOrdersSIView) getView()).getcItemHchy().getController()).setAttribute(
                getIdentification().getCompany(), Mnemos.NATURE.ITEM.getCode(), ((FSupervisorMOSBean) getModel()
                        .getDialogBean()).getItemHierarchy(), 1);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeItemHchy()");
        }
    }

    // private void initializeHierarchyItems() {
    //
    // ((CHchyLstValueCtrl) ((FSupervisorMOSView) getView()).getcItemHierarchy().getController()).setAttribute(
    // getIdentification().getCompany(), Mnemos.NATURE.ITEM.getCode(), getIdentification().getEstablishment(),
    // I18nClientManager.translate(GenKernel.T426, false));
    // ((CHchyLstValueCtrl) ((FProcessOrdersSView) getView()).getcItemHierarchy().getController())
    // .setCreationMode(false);
    //
    // }
    //
    // private void initializeHierarchyRessources() {
    //
    // ((CHchyLstValueCtrl) ((FSupervisorMOSView) getView()).getcLineHierarchy().getController()).setAttribute(
    // getIdentification().getCompany(), "RES", getIdentification().getEstablishment(), I18nClientManager
    // .translate(ProductionMo.T32440, false));
    //
    // }

    /**
     * Initialize Team composite.
     */
    @Override
    public void initializeTeam() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeTeam()");
        }

        CTeamListView cTeam = ((FProcessOrdersSIView) getView()).getcTeam();

        if (cTeam.getInitialHelpSelection() == null) {
            FTeamSBean selection = new FTeamSBean(true, "",
                    ((FSupervisorMOSBean) getModel().getDialogBean()).getEstablishmentKey());
            cTeam.setInitialHelpSelection(selection);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeTeam()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
        super.validateBean(creationMode, beanProperty, propertyValue);
    }

}

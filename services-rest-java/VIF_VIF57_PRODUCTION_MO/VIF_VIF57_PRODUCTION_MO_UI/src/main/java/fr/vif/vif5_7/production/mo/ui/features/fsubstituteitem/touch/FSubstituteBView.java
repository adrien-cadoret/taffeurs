/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSubstituteBView.java,v $
 * Created on 19 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.fsubstituteitem.touch;


import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.browser.touch.renderer.row.CodeLabelRowRenderer;


/**
 * Substitute item browser view.
 * 
 * @param <FSubstituteItemBBean>
 * @author vr
 */
public class FSubstituteBView<FSubstituteItemBBean> extends TouchBrowser<FSubstituteItemBBean> {

    /**
     * Default contructor.
     */
    public FSubstituteBView() {
        super();
        setRowRenderer(new CodeLabelRowRenderer<FSubstituteItemBBean>());
    }

}

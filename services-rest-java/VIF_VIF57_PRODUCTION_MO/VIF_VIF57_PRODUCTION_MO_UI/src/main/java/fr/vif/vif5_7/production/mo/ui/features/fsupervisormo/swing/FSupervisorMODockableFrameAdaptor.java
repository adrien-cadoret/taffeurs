/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMODockableFrameAdaptor.java,v $
 * Created on 15 mars 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing;


import com.jidesoft.docking.event.DockableFrameEvent;
import com.jidesoft.docking.event.DockableFrameListener;

import fr.vif.jtech.ui.viewer.swing.StandardSwingViewer;


/**
 * Mod dockable frame adaptor.
 * 
 * @author jd
 * @param <VBEAN> viewer bean.
 */
public class FSupervisorMODockableFrameAdaptor<VBEAN> extends StandardSwingViewer<VBEAN> implements
        DockableFrameListener {

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameActivated(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameAdded(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameAutohidden(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameAutohideShowing(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameDeactivated(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameDocked(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameFloating(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameHidden(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameMaximized(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameMoved(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameRemoved(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameRestored(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameShown(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameTabHidden(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameTabShown(final DockableFrameEvent arg0) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameTransferred(final DockableFrameEvent arg0) {

    }

}

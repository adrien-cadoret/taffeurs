/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationItemBCtrl.java,v $
 * Created on 19 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


import org.apache.log4j.Logger;

import java.util.List;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmocreation.FMOCreationCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * Controller for browser on items for MO creation.
 * 
 * @author glc
 */
public class FMOCreationItemBCtrl extends AbstractBrowserController {
    /** LOGGER. */
    private static final Logger LOGGER         = Logger.getLogger(FMOCreationItemBCtrl.class);

    private FMOCreationCBS      fmOCreationCBS = null;

    /**
     * Gets the fmOCreationCBS.
     * 
     * @category getter
     * @return the fmOCreationCBS.
     */
    public FMOCreationCBS getFmOCreationCBS() {
        return fmOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final BrowserActionEvent event) {
        super.selectedRowChanged(event);
    }

    /**
     * Sets the fmOCreationCBS.
     * 
     * @category setter
     * @param fmOCreationCBS fmOCreationCBS.
     */
    public void setFmOCreationCBS(final FMOCreationCBS fmOCreationCBS) {
        this.fmOCreationCBS = fmOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        try {
            IdContext idCtx = getIdCtx();
            String wsId = (String) getSharedContext()
                    .get(Domain.DOMAIN_PARENT, FProductionConstant.LOGICAL_WORKSTATION);

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                        + rowNumber + ")");
            }
            return getFmOCreationCBS().queryElements(idCtx, idCtx.getCompany(), idCtx.getEstablishment(), wsId,
                    startIndex, rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }
    }

}

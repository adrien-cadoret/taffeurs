/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMSOCreationFView.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation.touch;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationVBean;


/**
 * MSO creation feature view.
 * 
 * @author xg
 */

public class FBoningMSOCreationFView extends StandardTouchFeature<Object, FBoningMSOCreationVBean> {

    private TouchBrowser            fMSOCreationBView = null;

    private FBoningMSOCreationVView fMSOCreationVView = null;

    private TitlePanel              subTitlePanel     = null;

    /**
     * Constructor.
     */
    public FBoningMSOCreationFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public BrowserView<Object> getBrowserView() {
        return getFMSOCreationBView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView<FBoningMSOCreationVBean> getViewerView() {
        return getFMSOCreationVView();
    }

    /**
     * Initialisation of the browser feature.
     * 
     * @return FMSOCreationBView
     */
    private TouchBrowser getFMSOCreationBView() {
        if (fMSOCreationBView == null) {
            fMSOCreationBView = new TouchBrowser();
            fMSOCreationBView.setVisible(false);
        }
        return fMSOCreationBView;
    }

    /**
     * Initialisation of the viewer feature.
     * 
     * @return FMSOCreationVView
     */
    private FBoningMSOCreationVView getFMSOCreationVView() {
        if (fMSOCreationVView == null) {
            fMSOCreationVView = new FBoningMSOCreationVView();
        }
        return fMSOCreationVView;
    }

    /**
     * Gets the subTitlePanel.
     * 
     * @category getter
     * @return the subTitlePanel.
     */
    private JPanel getSubTitlePanel() {
        if (subTitlePanel == null) {
            subTitlePanel = new TitlePanel();
            subTitlePanel.setBorder(new MatteBorder(1, 0, 0, 0, Color.white));
            subTitlePanel.setTitle(I18nClientManager.translate(ProductionMo.T6424, false));
            subTitlePanel.setPreferredSize(new Dimension(getWidth(), 42));
            subTitlePanel.setMinimumSize(new Dimension(getWidth(), 42));
        }
        return subTitlePanel;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 2;
        gridBagConstraints2.fill = GridBagConstraints.BOTH;
        gridBagConstraints2.weightx = 1.0;
        gridBagConstraints2.weighty = 1.0;
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 1;
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;

        this.setLayout(new GridBagLayout());
        this.add(getSubTitlePanel(), gridBagConstraints);
        this.add(getFMSOCreationBView(), gridBagConstraints1);
        this.add(getFMSOCreationVView(), gridBagConstraints2);
    }

}

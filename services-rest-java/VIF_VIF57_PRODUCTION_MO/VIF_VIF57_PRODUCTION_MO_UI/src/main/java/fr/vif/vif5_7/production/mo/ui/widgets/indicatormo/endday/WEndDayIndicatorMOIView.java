/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WEndDayIndicatorMOIView.java,v $
 * Created on 15 avr. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday;


import java.awt.event.ActionListener;
import java.util.Collection;

import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOTypeEnum;


/**
 * Interface for the end of day indicator.
 *
 * @author ac
 */
public interface WEndDayIndicatorMOIView extends ActionListener {

    /** Events. */
    public static final String EVENT_OPEN_FEATURE       = "openFeatureRequested";
    public static final String ACTION_PARAMETER_FEATURE = "actionParameterFeature";
    public static final String EVENT_CHANGE_PARAMETER   = "eventChangeParameter";
    public static final String ACTION_DETAIL_FEATURE    = "actionDetailFeature";

    /**
     * 
     * Add an indicator to the main panel.
     * 
     * @param indicator indicator
     */
    public void addIndicator(EndDayMOTypeEnum indicator);

    /**
     * 
     * Changes title of widget.
     * 
     * @param text added text
     */
    public void addTextToTitle(String text);

    /**
     * 
     * Fills indicator list to display.
     * 
     * @param toDisplay collection
     */
    public void fillIndicatorsListToDisplay(Collection<EndDayMOTypeEnum> toDisplay);

    /**
     * 
     * Gets unique Id.
     * 
     * @return id
     */
    public int getUniqueId();

    /**
     * Removes all indicators of the widget.
     */
    public void removeAllIndicators();

    /**
     * Define Indicators Menu.
     */
    public void setIndicatorsMenu();

    /**
     * Sets the widget id.
     * 
     * @param uniqueId id
     */
    public void setUniqueId(int uniqueId);

    /**
     * 
     * Updates invoiced article list.
     */
    public void updateInvoicedArticleList();

    /**
     * 
     * Updates unclosed on line list.
     */
    public void updateUnclosedOnLineControlList();

    /**
     * 
     * Updates unclosed trs list.
     */
    public void updateUnclosedTrsList();

    /**
     * 
     * Updates working period list.
     */
    public void updateUnclosedWorkingPeriodList();

}

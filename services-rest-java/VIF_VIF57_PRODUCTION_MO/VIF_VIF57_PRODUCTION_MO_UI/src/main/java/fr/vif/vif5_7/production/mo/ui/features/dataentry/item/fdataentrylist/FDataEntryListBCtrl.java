/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListBCtrl.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fdataentrylist.FDataEntryListCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.ProductionMoTools;


/**
 * DataEntryList : Browser Controller.
 * 
 * @author vr
 */
public class FDataEntryListBCtrl extends AbstractBrowserController<FDataEntryListBBean> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FDataEntryListBCtrl.class);

    private FDataEntryListCBS   fdataEntryListCBS;

    /**
     * Default constructor.
     * 
     */
    public FDataEntryListBCtrl() {
        super();
    }

    /**
     * Change the current columns.
     * 
     * @param touchModel model define for this screen.
     * @param type type of the movement
     */
    public void changeColumns(final InputEntityType type, final TouchModel touchModel) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - changeColumns(type=" + type + ")");
        }

        List<BrowserColumn> cols = FDataEntryListColumnsFactory.getColumns(type, touchModel);
        getModel().removeAllColumns();
        for (BrowserColumn c : cols) {
            getModel().addColumn(c);
        }
        getView().initializeView(this);
        if (getModel().getRowCount() != 0) {
            getModel().setCurrentRowNumber(0);
        }
        ProductionMoTools.resizeColumnDetailListFeature(touchModel,
                ((FDataEntryListBIView) getView()).getJTableForResize(), null, type);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - changeColumns(type=" + type + ")");
        }
    }

    /**
     * Change the current columns.
     * 
     * @param touchModel model define for this screen.
     * @param type type of the movement
     */
    public void changeColumns(final OutputEntityType type, final TouchModel touchModel) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - changeColumns(type=" + type + ")");
        }

        List<BrowserColumn> cols = FDataEntryListColumnsFactory.getColumns(type, touchModel);
        getModel().removeAllColumns();
        for (BrowserColumn c : cols) {
            getModel().addColumn(c);
        }
        getView().initializeView(this);
        if (getModel().getRowCount() != 0) {
            getModel().setCurrentRowNumber(0);
        }
        ProductionMoTools.resizeColumnDetailListFeature(touchModel,
                ((FDataEntryListBIView) getView()).getJTableForResize(), type, null);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - changeColumns(type=" + type + ")");
        }
    }

    /**
     * Gets the fdataEntryListCBS.
     * 
     * @category getter
     * @return the fdataEntryListCBS.
     */
    public final FDataEntryListCBS getFdataEntryListCBS() {
        return fdataEntryListCBS;
    }

    /**
     * Sets the fdataEntryListCBS.
     * 
     * @category setter
     * @param fdataEntryListCBS fdataEntryListCBS.
     */
    public final void setFdataEntryListCBS(final FDataEntryListCBS fdataEntryListCBS) {
        this.fdataEntryListCBS = fdataEntryListCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FDataEntryListBBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FDataEntryListBBean> lst = null;
        try {

            FDataEntryListSBean sBean = (FDataEntryListSBean) selection;
            sBean.setInputEntityType((InputEntityType) getSharedContext().get(Domain.DOMAIN_PARENT, "inputEntityType"));
            sBean.setOutputEntityType((OutputEntityType) getSharedContext().get(Domain.DOMAIN_PARENT,
                    "outputEntityType"));
            sBean.setTouchModel((TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL));
            lst = getFdataEntryListCBS().queryElements(getIdCtx(), sBean, startIndex, rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return lst;
    }
}

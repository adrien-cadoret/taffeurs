/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLabelBModel.java,v $
 * Created on 17 Jun 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.flabels;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.FontNames;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.flabels.FLabelBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.flabels.FLabelBBeanEnum;


/**
 * FLabel Browser model.
 * 
 * @author xg
 */
public class FLabelBModel extends BrowserModel<FLabelBBean> {

    private static Format cellFormat1;
    private static Format cellFormat2;

    static {
        cellFormat1 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.PLAIN,
                14));
        cellFormat2 = new Format(Alignment.LEFT_ALIGN,
                new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.BOLD, 14));

    }

    /**
     * Default constuctor.
     */
    public FLabelBModel() {
        super();
        setBeanClass(FLabelBBean.class);
        addColumn(new BrowserColumn("", FLabelBBeanEnum.LABEL_CODE.getValue(), 0, 0));
        addColumn(new BrowserColumn("", FLabelBBeanEnum.LABEL_LABEL.getValue(), 0, 0));
        setRowHeight(100);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FLabelBBean bean) {

        Format ret = null;

        if (property.equals(FLabelBBeanEnum.LABEL_CODE.getValue())) {
            ret = cellFormat1;
        } else if (property.equals(FLabelBBeanEnum.LABEL_LABEL.getValue())) {
            ret = cellFormat2;
        }

        return ret;
    }
}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: MOSaveFrame.java,v $
 * Created on 23 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


/**
 * Contains information to create a new frame.
 * 
 * @author jd
 */
public class MOSaveFrame {

    /**
     * Axis.
     */
    private String  axis                  = "";
    //
    /**
     * Graph Type.
     */
    private String  graphType             = "";

    /**
     * Default height.
     */
    private int     height                = 0;

    /**
     * The frame is visible ?
     */
    private boolean isVisible             = true;

    /**
     * icon.
     */
    // private KernelIcon icon = null;

    private boolean reverseSetAndCategory = false;

    /**
     * Title Frame with reference.
     */
    private String  titleFrame            = null;

    /**
     * Title Frame unstranslatable.
     */
    private String  titleFrameU           = null;

    // private List<String> listType = new ArrayList<String>();

    /**
     * Title Graph with reference.
     */
    private String  titleGraph            = null;

    /**
     * Title Graph unstranslatable.
     */
    private String  titleGraphU           = null;
    /**
     * Title Menu with reference.
     */
    private String  titleMenu             = null;

    /**
     * Title Menu unstranslatable.
     */
    private String  titleMenuU            = null;

    /**
     * Frame Type.
     */
    private int     typeFrame             = 0;

    private int     typeOfIndicator       = 0;

    /**
     * Unique id of the frame. Must be constant.
     */
    private String  uniqueId              = "";

    /**
     * The frame is it affected ?.
     */
    private String  widgetId              = "";

    /**
     * the frame affected to a widget is it concerned by supervisor feature on widget only ?
     */
    private boolean widgetOnly            = false;

    /**
     * Type value.
     */
    private String  valueType             = "";   // IndicatorValueTypeEnum.NB_PACKS.getValue();

    /**
     * Default width.
     */
    private int     width                 = 0;

    /**
     * constructor.
     */
    public MOSaveFrame() {
        super();
    }

    /**
     * constructor.
     * 
     * @param uniqueId unique id of the frame.
     */
    public MOSaveFrame(final String uniqueId) {
        super();
        this.uniqueId = uniqueId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MOSaveFrame other = (MOSaveFrame) obj;
        if (uniqueId == null) {
            if (other.uniqueId != null) {
                return false;
            }
        } else if (!uniqueId.equals(other.uniqueId)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the axis.
     * 
     * @return the axis.
     */
    public String getAxis() {
        return axis;
    }

    /**
     * Gets the graphType.
     * 
     * @return the graphType.
     */
    public String getGraphType() {
        return graphType;
    }

    /**
     * Gets the height.
     * 
     * @return the height.
     */
    public int getHeight() {
        return height;
    }

    /**
     * Gets the isVisible.
     * 
     * @return the isVisible.
     */
    public boolean getIsVisible() {
        return isVisible;
    }

    /**
     * Gets the reverseSetAndCategory.
     * 
     * @return the reverseSetAndCategory.
     */
    public boolean getReverseSetAndCategory() {
        return reverseSetAndCategory;
    }

    /**
     * Gets the titleFrame.
     * 
     * @return the titleFrame.
     */
    public String getTitleFrame() {
        return titleFrame;
    }

    /**
     * Gets the titleFrameU.
     * 
     * @return the titleFrameU.
     */
    public String getTitleFrameU() {
        return titleFrameU;
    }

    /**
     * Gets the titleGraph.
     * 
     * @return the titleGraph.
     */
    public String getTitleGraph() {
        return titleGraph;
    }

    /**
     * Gets the titleGraphU.
     * 
     * @return the titleGraphU.
     */
    public String getTitleGraphU() {
        return titleGraphU;
    }

    // /**
    // * Gets the icon.
    // *
    // * @return the icon.
    // */
    // public KernelIcon getIcon() {
    // return icon;
    // }

    /**
     * Gets the titleMenu.
     * 
     * @return the titleMenu.
     */
    public String getTitleMenu() {
        return titleMenu;
    }

    // /**
    // * Gets the listType.
    // *
    // * @return the listType.
    // */
    // public List<String> getListType() {
    // return listType;
    // }

    /**
     * Gets the titleMenuU.
     * 
     * @return the titleMenuU.
     */
    public String getTitleMenuU() {
        return titleMenuU;
    }

    /**
     * Gets the typeFrame.
     * 
     * @return the typeFrame.
     */
    public int getTypeFrame() {
        return typeFrame;
    }

    /**
     * Gets the typeOfIndicator.
     * 
     * @return the typeOfIndicator.
     */
    public int getTypeOfIndicator() {
        return typeOfIndicator;
    }

    /**
     * Gets the uniqueId.
     * 
     * @return the uniqueId.
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * Gets the valueType.
     * 
     * @return the valueType.
     */
    public String getValueType() {
        return valueType;
    }

    /**
     * Gets the widgetId.
     * 
     * @return the widgetId.
     */
    public String getWidgetId() {
        return widgetId;
    }

    /**
     * Gets the widgetOnly.
     * 
     * @return the widgetOnly.
     */
    public boolean getWidgetOnly() {
        return widgetOnly;
    }

    /**
     * Gets the width.
     * 
     * @return the width.
     */
    public int getWidth() {
        return width;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((uniqueId == null) ? 0 : uniqueId.hashCode());
        return result;
    }

    /**
     * Sets the axis.
     * 
     * @param axis axis.
     */
    public void setAxis(final String axis) {
        this.axis = axis;
    }

    /**
     * Sets the graphType.
     * 
     * @param graphType graphType.
     */
    public void setGraphType(final String graphType) {
        this.graphType = graphType;
    }

    /**
     * Sets the height.
     * 
     * @param height height.
     */
    public void setHeight(final int height) {
        this.height = height;
    }

    /**
     * Sets the isVisible.
     * 
     * @param isVisible isVisible.
     */
    public void setIsVisible(final boolean isVisible) {
        this.isVisible = isVisible;
    }

    /**
     * Sets the reverseSetAndCategory.
     * 
     * @param reverseSetAndCategory reverseSetAndCategory.
     */
    public void setReverseSetAndCategory(final boolean reverseSetAndCategory) {
        this.reverseSetAndCategory = reverseSetAndCategory;
    }

    /**
     * Sets the titleFrame.
     * 
     * @param titleFrame titleFrame.
     */
    public void setTitleFrame(final String titleFrame) {
        this.titleFrame = titleFrame;
    }

    // /**
    // * Sets the icon.
    // *
    // * @param icon icon.
    // */
    // public void setIcon(final KernelIcon icon) {
    // this.icon = icon;
    // }

    /**
     * Sets the titleFrameU.
     * 
     * @param titleFrameU titleFrameU.
     */
    public void setTitleFrameU(final String titleFrameU) {
        this.titleFrameU = titleFrameU;
    }

    // /**
    // * Sets the listType.
    // *
    // * @param listType listType.
    // */
    // public void setListType(final List<String> listType) {
    // this.listType = listType;
    // }

    /**
     * Sets the titleGraph.
     * 
     * @param titleGraph titleGraph.
     */
    public void setTitleGraph(final String titleGraph) {
        this.titleGraph = titleGraph;
    }

    /**
     * Sets the titleGraphU.
     * 
     * @param titleGraphU titleGraphU.
     */
    public void setTitleGraphU(final String titleGraphU) {
        this.titleGraphU = titleGraphU;
    }

    /**
     * Sets the titleMenu.
     * 
     * @param titleMenu titleMenu.
     */
    public void setTitleMenu(final String titleMenu) {
        this.titleMenu = titleMenu;
    }

    /**
     * Sets the titleMenuU.
     * 
     * @param titleMenuU titleMenuU.
     */
    public void setTitleMenuU(final String titleMenuU) {
        this.titleMenuU = titleMenuU;
    }

    /**
     * Sets the typeFrame.
     * 
     * @param typeFrame typeFrame.
     */
    public void setTypeFrame(final int typeFrame) {
        this.typeFrame = typeFrame;
    }

    /**
     * Sets the typeOfIndicator.
     * 
     * @param typeOfIndicator typeOfIndicator.
     */
    public void setTypeOfIndicator(final int typeOfIndicator) {
        this.typeOfIndicator = typeOfIndicator;
    }

    /**
     * Sets the uniqueId.
     * 
     * @param uniqueId uniqueId.
     */
    public void setUniqueId(final String uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * Sets the valueType.
     * 
     * @param valueType valueType.
     */
    public void setValueType(final String valueType) {
        this.valueType = valueType;
    }

    /**
     * Sets the widgetId.
     * 
     * @param widgetId widgetId.
     */
    public void setWidgetId(final String widgetId) {
        this.widgetId = widgetId;
    }

    /**
     * Sets the widgetOnly.
     * 
     * @param widgetOnly widgetOnly.
     */
    public void setWidgetOnly(final boolean widgetOnly) {
        this.widgetOnly = widgetOnly;
    }

    /**
     * Sets the width.
     * 
     * @param width width.
     */
    public void setWidth(final int width) {
        this.width = width;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "SaveFrame [uniqueId=" + uniqueId + ", isVisible=" + isVisible + ", axis=" + axis
        // + ", listType=" + listType
                + ", titleFrame=" + titleFrame + ", typeFrame=" + typeFrame + "]";
    }

}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGraphMOVCtrl.java,v $
 * Created on 22 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGraphMOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOGraphSelectionBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOVCtrlAdaptor;


/**
 * Default graph viewer controller.
 * 
 * @author jd
 * @param <BEAN> bean
 */
public class FGraphMOVCtrl<BEAN> extends FIndicatorMOVCtrlAdaptor<BEAN> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FGraphMOVCtrl.class);

    /**
     * {@inheritDoc}
     * 
     * @param sel
     */
    public FGraphMOBean convert(final IdContext idctx, final FSupervisorMOGraphSelectionBean sel) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(idctx=" + idctx + ", sel=" + sel + ")");
        }

        FGraphMOBean ret = new FGraphMOBean();
        try {

            ret = getProductivityMOCBS().getGraphBean(idctx, sel);

        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(idctx=" + idctx + ", sel=" + sel + ")=" + ret);
        }
        return ret;
    }

}

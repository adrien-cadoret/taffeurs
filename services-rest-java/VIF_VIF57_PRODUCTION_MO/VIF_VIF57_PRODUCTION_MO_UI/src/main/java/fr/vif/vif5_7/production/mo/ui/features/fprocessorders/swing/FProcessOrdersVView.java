/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersVView.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders.swing;


import fr.vif.jtech.ui.viewer.swing.DynamicSwingViewer;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersVBean;


/**
 * ProcessOrders : Viewer View.
 * 
 * @author jd
 */
public class FProcessOrdersVView extends DynamicSwingViewer<FProcessOrdersVBean> {

    /**
     * Default constructor.
     * 
     */
    public FProcessOrdersVView() {
        super();
    }

    /**
     * Constructor with presentation type as parameter.
     * 
     * @param presentationType the type of presentation of the viewer
     */
    public FProcessOrdersVView(final String presentationType) {
        super(presentationType);
    }

}

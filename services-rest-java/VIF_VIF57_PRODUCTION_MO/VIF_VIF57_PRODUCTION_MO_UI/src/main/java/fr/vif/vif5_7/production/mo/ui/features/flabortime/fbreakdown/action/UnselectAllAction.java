/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: UnselectAllAction.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.action;


import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.treetable.InputTreeTableIView;
import fr.vif.jtech.ui.treetable.actions.AbstractLineSelectionDependentAction;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.IHchyTreeNodeElt;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.IHchyTreeNodeEltInfo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeElt;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeTypeEnum;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.FTimeBreakDownVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing.FTimeBreakDownVView;


/**
 * Action to unselect all batch and all criteria.
 * 
 * @author cj
 */
public class UnselectAllAction extends
        AbstractLineSelectionDependentAction<IHchyTreeNodeElt<? extends IHchyTreeNodeEltInfo>> {
    private FTimeBreakDownVCtrl vCtrl;

    /**
     * Constructor.
     * 
     * @param vCtrl viewer controller
     */
    public UnselectAllAction(final FTimeBreakDownVCtrl vCtrl) {
        super();
        this.vCtrl = vCtrl;
        setEnabled(true);
        setCode("UnselectAllAction");
        setLargeIconURL("");
        setSmallIconURL("");
        setText(I18nClientManager.translate(ProductionMo.T36194, false));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        InputTreeTableIView view = getTreeTableController().getInputTreeTableView();
        // Get selected node
        Object[] currentPath = view.getCurrentSelectedNodePath();
        if (currentPath[0] instanceof TimeBreakDownHchyTreeNodeElt) {
            unselectAll((TimeBreakDownHchyTreeNodeElt) currentPath[0]);
            ((FTimeBreakDownVView) vCtrl.getView()).getFtimeBreakDownTreeTableView().refreshValues();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isLineSelectionDependentActionEnabled(
            final IHchyTreeNodeElt<? extends IHchyTreeNodeEltInfo> selectedNode) {
        boolean isEnabled = false;
        if (selectedNode instanceof TimeBreakDownHchyTreeNodeElt) {
            TimeBreakDownHchyTreeNodeElt node = (TimeBreakDownHchyTreeNodeElt) selectedNode;
            isEnabled = node.getNodeType().equals(TimeBreakDownHchyTreeNodeTypeEnum.LABOR.getValue())
                    || node.getNodeType().equals(TimeBreakDownHchyTreeNodeTypeEnum.LIGNE.getValue());
        }
        return isEnabled;
    }

    /**
     * Unselect all.
     * 
     * @param parent the parent element
     */
    private void unselectAll(final TimeBreakDownHchyTreeNodeElt parent) {
        parent.setIsSelected(false);
        if (parent.getChildren() != null) {
            for (IHchyTreeNodeElt<? extends IHchyTreeNodeEltInfo> children : parent.getChildren()) {
                // DP2192 - CHU - unselect only visible cells
                if (children instanceof TimeBreakDownHchyTreeNodeElt && isLineSelectionDependentActionEnabled(children)) {
                    unselectAll((TimeBreakDownHchyTreeNodeElt) children);
                }
            }
        }
    }
}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOLinesVCtrl.java,v $
 * Created on 21 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.lines;


import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOVCtrlAdaptor;


/**
 * FSupervisorLinesVCtrl.
 * 
 * 
 * @author jd
 * @param <V> viewer bean
 */
public class FSupervisorMOLinesVCtrl<V> extends FIndicatorMOVCtrlAdaptor<V> {

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

}

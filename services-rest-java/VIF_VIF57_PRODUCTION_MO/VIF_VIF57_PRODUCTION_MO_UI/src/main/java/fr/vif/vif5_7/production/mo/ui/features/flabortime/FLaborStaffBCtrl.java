/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffBCtrl.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import java.awt.Dimension;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.dialogs.selection.SelectionChangeEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.input.kbdialog.NumKeyboardDialogController;
import fr.vif.jtech.ui.input.kbdialog.touch.TouchNumKeyboardDialog;
import fr.vif.jtech.ui.input.touch.TouchCompositeInput;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.util.SpringWrapperInvocation;
import fr.vif.jtech.ui.util.factories.SpringFactoryHelper;
import fr.vif.jtech.ui.util.touch.dialogs.DefaultDialogDisplayListener;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;
import fr.vif.vif5_7.production.mo.business.services.features.flaborstaff.FLaborStaffCBS;


/**
 * Labor staff browser controller.
 * 
 * @author nle
 */
public class FLaborStaffBCtrl extends AbstractBrowserController<FLaborStaffBBean> implements DialogChangeListener {
    private static final Logger LOGGER     = Logger.getLogger(FLaborStaffBCtrl.class);

    /**
     * Labor Time feature CBS to query elements.
     */
    private FLaborStaffCBS      flaborstaffCBS;

    private Boolean             modifSaved = true;

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        super.dialogValidated(event);

        // comes from the window allowing to enter the number of people,
        // which returns a String containing something like "KeyboardBean@3d9f5f[value=85]"
        // we must extract the number (85 here)

        String myString = event.getDialogBean().toString();

        if (!"".equals(myString)) {

            // extracts the value (85 here)
            int lg = myString.length();
            String tmp = "value=";
            int i = myString.indexOf(tmp);
            if (i + tmp.length() < lg - 1) {

                String myValueStr = myString.substring(i + tmp.length(), lg - 1);

                double myValue = Double.parseDouble(myValueStr.replace(",", "."));

                // no negative !
                if (myValue < 0) {
                    myValue = -myValue;
                }

                FLaborStaffBBean bean = getModel().getCurrentBean();

                // puts the number in the button
                bean.getLaborStaff().setStaff(myValue);

                setModifSaved(false);
            }
        }

    }

    /**
     * Gets the flaborstaffCBS.
     * 
     * @return the flaborstaffCBS.
     */
    public FLaborStaffCBS getFlaborstaffCBS() {
        return flaborstaffCBS;
    }

    /**
     * Gets the modifSaved.
     * 
     * @return the modifSaved.
     */
    public Boolean getModifSaved() {
        return modifSaved;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final BrowserActionEvent event) {

        // occurs when i click on a button of the browse ==> displays the window to enter a numeric ==> number of people

        super.selectedRowChanged(event);

        SpringWrapperInvocation sdi = (SpringWrapperInvocation) SpringFactoryHelper.getInstance().getBean(
                "TouchNumKeyboardDialog");
        TouchNumKeyboardDialog view = (TouchNumKeyboardDialog) sdi.createObject();

        FLaborStaffBBean bean = getModel().getCurrentBean();

        view.initialize(TouchCompositeInput.class, "##0.00", bean.getLaborStaff().getStaff(), false); // the init value
                                                                                                      // doesn't
                                                                                                      // function
        view.setTitle(bean.getLaborStaff().getResourceLabel());

        view.setMinimumSize(new Dimension(400, 600));
        view.setSize(400, 600);
        view.setPreferredSize(view.getSize());

        NumKeyboardDialogController controller;
        controller = (NumKeyboardDialogController) SpringFactoryHelper.getInstance().getBean(
                "NumKeyboardDialogController");
        controller.addDisplayListener(DefaultDialogDisplayListener.getInstance());
        controller.addDialogChangeListener(this);
        controller.setDialogView(view);
        controller.initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectionChanged(final SelectionChangeEvent event) {
        FLaborStaffBBean bean = getModel().getCurrentBean();

        super.selectionChanged(event);

        if (bean != null) {
            setSelectedBean(bean);
        }
    }

    /**
     * Sets the flaborstaffCBS.
     * 
     * @param flaborstaffCBS the labor staff CBS.
     */
    public void setFlaborstaffCBS(final FLaborStaffCBS flaborstaffCBS) {
        this.flaborstaffCBS = flaborstaffCBS;
    }

    /**
     * Sets the modifSaved.
     * 
     * @param modifSaved modifSaved.
     */
    public void setModifSaved(final Boolean modifSaved) {
        this.modifSaved = modifSaved;
    }

    /**
     * select the row of the bean in the browser.
     * 
     * @param bean the bean
     */
    public void setSelectedBean(final FLaborStaffBBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setSelectedBean(bean=" + bean + ")");
        }

        try {
            for (int i = 0; i < getModel().getRowCount(); i++) {

                if (bean.getLaborStaff().getLaborStaffKey()
                        .equals(getModel().getBeanAt(i).getLaborStaff().getLaborStaffKey())) {
                    changeCurrentRow(i);
                    break;
                }
            }
        } catch (UIVetoException e) {
            getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setSelectedBean(bean=" + bean + ")");
        }
    }

    /**
     * 
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    protected List<FLaborStaffBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FLaborStaffBBean> lsbbList = null;

        // information comes from FProductionContainerMOListFCtrl \ openChildFeature \ if
        // (featureId.equals("VIF.LABSTAFF"))
        // or from the TRS
        final Date datprod = (Date) getSharedContext().get(Domain.DOMAIN_PARENT, "DATPROD");
        final String line = (String) getSharedContext().get(Domain.DOMAIN_PARENT, "LINE");
        final List<FMOListBBean> moListBBean = (List<FMOListBBean>) getSharedContext().get(Domain.DOMAIN_PARENT,
                "LSTOF");

        // Inits the browse with the resources of the MOs given by the context.

        try {
            if (moListBBean != null && datprod != null && !"".equals(line)) {

                List<MOKey> listMO = new ArrayList<MOKey>();

                for (FMOListBBean bbean : moListBBean) {
                    listMO.add(new MOKey(bbean.getMobean().getMoKey().getEstablishmentKey(), bbean.getMobean()
                            .getMoKey().getChrono()));
                }

                lsbbList = getFlaborstaffCBS().queryElementsByMOList(getIdCtx(), listMO, datprod, line, startIndex,
                        rowNumber);
            }

        } catch (BusinessException e) {
            throw new UIException(e);
        }
        if (lsbbList == null) {
            lsbbList = new ArrayList<FLaborStaffBBean>();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + lsbbList);
        }
        return lsbbList;
    }

}

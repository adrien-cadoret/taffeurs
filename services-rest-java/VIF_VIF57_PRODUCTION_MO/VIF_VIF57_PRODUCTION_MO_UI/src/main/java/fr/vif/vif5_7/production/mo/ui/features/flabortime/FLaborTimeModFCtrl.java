/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeModFCtrl.java,v $
 * Created on 8 janv. 2014 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.util.Key;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.business.services.features.flaborstaff.FLaborStaffCBS;
import fr.vif.vif5_7.production.mo.business.services.features.flabortime.FLaborTimeCBS;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.touch.FLaborTimeModFView;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.touch.FLaborTimeModVView;


/**
 * Labor time feature controller.
 * 
 * @author nle
 */
public class FLaborTimeModFCtrl extends StandardFeatureController implements TableRowChangeListener {

    /**
     * list of btns.
     */
    public static final String  VALID_TIME     = "validTimeBtn";
    public static final String  ADD            = "addBtn";
    public static final String  MODIF          = "modifBtn";

    private static final String DATE_PATTERN   = "dd/MM/yy HH:mm";

    private static final Logger LOGGER         = Logger.getLogger(FLaborTimeModFCtrl.class);

    private FLaborStaffCBS      flaborstaffCBS = null;
    private FLaborTimeCBS       flabortimeCBS  = null;

    private String              csoc;
    private String              cetab;
    private Date                datprod;
    private String              line;
    private int                 ni1;
    private String              mode;
    private Date                currentBeginDateHour;
    private Date                currentEndDateHour;
    private Date                prevEndDateHour;
    private Date                nextBeginDateHour;

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        if (VALID_TIME.equals(event.getModel().getReference())) {
            if (ADD.equals(mode)) {
                insertValidation();
            } else {
                updateValidation();
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * Gets the cetab.
     * 
     * @return the cetab.
     */
    public String getCetab() {
        return cetab;
    }

    /**
     * Gets the csoc.
     * 
     * @return the csoc.
     */
    public String getCsoc() {
        return csoc;
    }

    /**
     * Gets the currentBeginDateHour.
     * 
     * @return the currentBeginDateHour.
     */
    public Date getCurrentBeginDateHour() {
        return currentBeginDateHour;
    }

    /**
     * Gets the currentEndDateHour.
     * 
     * @return the currentEndDateHour.
     */
    public Date getCurrentEndDateHour() {
        return currentEndDateHour;
    }

    /**
     * Gets the datprod.
     * 
     * @return the datprod.
     */
    public Date getDatprod() {
        return datprod;
    }

    /**
     * Gets the flaborstaffCBS.
     * 
     * @return the flaborstaffCBS.
     */
    public FLaborStaffCBS getFlaborstaffCBS() {
        return flaborstaffCBS;
    }

    /**
     * Gets the flabortimeCBS.
     * 
     * @return the flabortimeCBS.
     */
    public FLaborTimeCBS getFlabortimeCBS() {
        return flabortimeCBS;
    }

    /**
     * Gets the line.
     * 
     * @return the line.
     */
    public String getLine() {
        return line;
    }

    /**
     * Gets the mode.
     * 
     * @return the mode.
     */
    public String getMode() {
        return mode;
    }

    /**
     * Gets the nextBeginDateHour.
     * 
     * @return the nextBeginDateHour.
     */
    public Date getNextBeginDateHour() {
        return nextBeginDateHour;
    }

    /**
     * Gets the ni1.
     * 
     * @return the ni1.
     */
    public int getNi1() {
        return ni1;
    }

    /**
     * Gets the prevEndDateHour.
     * 
     * @return the prevEndDateHour.
     */
    public Date getPrevEndDateHour() {
        return prevEndDateHour;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        fireSharedContextSend(getSharedContext());

        super.initialize();

        getView().setErrorPanelAlwaysVisible(true);

        setCsoc((String) getSharedContext().get(Domain.DOMAIN_PARENT, "CSOC"));
        setCetab((String) getSharedContext().get(Domain.DOMAIN_PARENT, "CETAB"));
        setDatprod((Date) getSharedContext().get(Domain.DOMAIN_PARENT, "DATPROD"));
        setLine((String) getSharedContext().get(Domain.DOMAIN_PARENT, "LINE"));
        setNi1((Integer) getSharedContext().get(Domain.DOMAIN_PARENT, "NI1"));
        setMode((String) getSharedContext().get(Domain.DOMAIN_PARENT, "MODE"));

        if (MODIF.equals(getMode())) {
            Object o1 = getSharedContext().get(Domain.DOMAIN_PARENT, "CURRENTBEGINDATEHOUR");
            if (o1 != null) {
                setCurrentBeginDateHour((Date) o1);
            }

            Object o2 = getSharedContext().get(Domain.DOMAIN_PARENT, "CURRENTENDDATEHOUR");
            if (o2 != null) {
                setCurrentEndDateHour((Date) o2);
            }
        }

        Object o3 = getSharedContext().get(Domain.DOMAIN_PARENT, "PREVENDDATEHOUR");
        if (o3 != null) {
            setPrevEndDateHour((Date) o3);
        }

        Object o4 = getSharedContext().get(Domain.DOMAIN_PARENT, "NEXTBEGINDATEHOUR");
        if (o4 != null) {
            setNextBeginDateHour((Date) o4);
        }

        FLaborTimeModVView vView = (FLaborTimeModVView) getViewerCtrl().getView();
        vView.setScreenValues(getMode(), currentBeginDateHour, currentEndDateHour, prevEndDateHour, nextBeginDateHour);

        String subtitle;
        try {
            if (ADD.equals(getMode())) {
                subtitle = I18nClientManager.translate(ProductionMo.T35071, false);
            } else {
                subtitle = I18nClientManager.translate(ProductionMo.T35072, false);
            }
            subtitle = subtitle
                    + " "
                    + DateHelper.formatDate(datprod, "dd/MM/yyyy")
                    + " - "
                    + line
                    + " - "
                    + getFlabortimeCBS().getResourceLabel(getIdCtx(),
                            new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()), line);
            getFeatureView().setSubTitle(subtitle);
        } catch (BusinessException e) {
            LOGGER.error("", e);
        }
        enableToolBarButton(true, VALID_TIME);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {

    }

    /**
     * Sets the cetab.
     * 
     * @param cetab cetab.
     */
    public void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Sets the csoc.
     * 
     * @param csoc csoc.
     */
    public void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Sets the currentBeginDateHour.
     * 
     * @param currentBeginDateHour currentBeginDateHour.
     */
    public void setCurrentBeginDateHour(final Date currentBeginDateHour) {
        this.currentBeginDateHour = currentBeginDateHour;
    }

    /**
     * Sets the currentEndDateHour.
     * 
     * @param currentEndDateHour currentEndDateHour.
     */
    public void setCurrentEndDateHour(final Date currentEndDateHour) {
        this.currentEndDateHour = currentEndDateHour;
    }

    /**
     * Sets the datprod.
     * 
     * @param datprod datprod.
     */
    public void setDatprod(final Date datprod) {
        this.datprod = datprod;
    }

    /**
     * Sets the flaborstaffCBS.
     * 
     * @param flaborstaffCBS flaborstaffCBS.
     */
    public void setFlaborstaffCBS(final FLaborStaffCBS flaborstaffCBS) {
        this.flaborstaffCBS = flaborstaffCBS;
    }

    /**
     * Sets the flabortimeCBS.
     * 
     * @param flabortimeCBS flabortimeCBS.
     */
    public void setFlabortimeCBS(final FLaborTimeCBS flabortimeCBS) {
        this.flabortimeCBS = flabortimeCBS;
    }

    /**
     * Sets the line.
     * 
     * @param line line.
     */
    public void setLine(final String line) {
        this.line = line;
    }

    /**
     * Sets the mode.
     * 
     * @param mode mode.
     */
    public void setMode(final String mode) {
        this.mode = mode;
    }

    /**
     * Sets the nextBeginDateHour.
     * 
     * @param nextBeginDateHour nextBeginDateHour.
     */
    public void setNextBeginDateHour(final Date nextBeginDateHour) {
        this.nextBeginDateHour = nextBeginDateHour;
    }

    /**
     * Sets the ni1.
     * 
     * @param ni1 ni1.
     */
    public void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Sets the prevEndDateHour.
     * 
     * @param prevEndDateHour prevEndDateHour.
     */
    public void setPrevEndDateHour(final Date prevEndDateHour) {
        this.prevEndDateHour = prevEndDateHour;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        List<ToolBarBtnStdBaseModel> lstBtn = super.getActionsButtonModels();

        ToolBarBtnFeatureModel validStaff = new ToolBarBtnFeatureModel();
        validStaff.setReference(VALID_TIME);
        validStaff.setIconURL("/images/vif/vif57/production/mo/ok.png");
        validStaff.setText(I18nClientManager.translate(ProductionMo.T29582, false));
        validStaff.setShortcut(Key.K_F5);
        validStaff.setEnabled(true);
        lstBtn.add(validStaff);
        enableToolBarButton(true, VALID_TIME);

        return lstBtn;
    }

    /**
     * Is dh1 > dh2 ?
     * 
     * @param d1 : first date
     * @param h1 : first hour
     * @param d2 : second date
     * @param h2 : second hour
     * @return true if dh1 > dh2
     */
    private boolean date1AfterDate2(final Date d1, final Date h1, final Date d2, final Date h2) {
        Boolean ret;
        if (d1 == null || h1 == null || d2 == null || h2 == null) {
            ret = false;
        } else {
            Date dh1 = DateHelper.getDate(d1, h1);
            Date dh2 = DateHelper.getDate(d2, h2);

            ret = dh1.after(dh2);
        }

        return ret;
    }

    /**
     * Is dh1 = dh2 ?
     * 
     * @param d1 : first date
     * @param h1 : first hour
     * @param d2 : second date
     * @param h2 : second hour
     * @return true if dh1 = dh2
     */
    private boolean date1EqualsDate2(final Date d1, final Date h1, final Date d2, final Date h2) {
        Boolean ret;
        if (d1 == null || h1 == null || d2 == null || h2 == null) {
            ret = false;
        } else {
            Date dh1 = DateHelper.getDate(d1, h1);
            Date dh2 = DateHelper.getDate(d2, h2);

            ret = dh1.equals(dh2);
        }

        return ret;
    }

    /**
     * Get the browser controller.
     * 
     * @return the browser controller
     */
    private FLaborTimeModBCtrl getBrowserCtrl() {
        return (FLaborTimeModBCtrl) getBrowserController();

    }

    // }
    /**
     * Get the feature view.
     * 
     * @return the feature view
     */
    private FLaborTimeModFView getFeatureView() {
        return (FLaborTimeModFView) getView();

    }

    /**
     * Get the viewer controller.
     * 
     * @return the viewer controller
     */
    private FLaborTimeModVCtrl getViewerCtrl() {
        return (FLaborTimeModVCtrl) getViewerController();

    }

    /**
     * Get the viewer view.
     * 
     * @return the viewer view
     */
    private FLaborTimeModVView getViewerView() {
        return (FLaborTimeModVView) getViewerCtrl().getView();

    }

    /**
     * Insert of a new period.
     */
    private void insertValidation() {

        // //////////////////////////////// INITS ////////////////////////////////////////

        Date currBegDate = (Date) getViewerView().getItfCurrentBegDate().getValue();
        Date currBegHour = (Date) getViewerView().getItfCurrentBegHour().getValue();
        Date currEndDate = (Date) getViewerView().getItfCurrentEndDate().getValue();
        Date currEndHour = (Date) getViewerView().getItfCurrentEndHour().getValue();
        Date prevEndDate = (Date) getViewerView().getItfPreviousEndDate().getValue();
        Date prevEndHour = (Date) getViewerView().getItfPreviousEndHour().getValue();
        Date nextBegDate = (Date) getViewerView().getItfNextBegDate().getValue();
        Date nextBegHour = (Date) getViewerView().getItfNextBegHour().getValue();
        Boolean error = false;

        // //////////////////////////////// CONTROLS ////////////////////////////////////////

        if (prevEndDate != null && prevEndHour != null) {
            if (date1AfterDate2(prevEndDate, prevEndHour, currBegDate, currBegHour)) {
                // PB : dates overlap
                getView().showError(I18nClientManager.translate(ProductionMo.T30735, true));
                error = true;
            }
        }

        if (currEndDate != null && currEndHour != null) {
            if (date1AfterDate2(currBegDate, currBegHour, currEndDate, currEndHour)
                    || date1EqualsDate2(currBegDate, currBegHour, currEndDate, currEndHour)) {
                // PB : the beginning must be BEFORE the end
                getView().showError(I18nClientManager.translate(ProductionMo.T19497, true));
                error = true;
            }

            if (nextBegDate != null && nextBegHour != null) {
                if (date1AfterDate2(currEndDate, currEndHour, nextBegDate, nextBegHour)) {
                    // PB : dates overlap
                    getView().showError(I18nClientManager.translate(ProductionMo.T30735, true));
                    error = true;
                }
            }
        }

        Date dhbegInsert = DateHelper.getDate(currBegDate, currBegHour);
        Date dhendInsert = DateHelper.getDate(currEndDate, currEndHour);

        if (!error) {
            LaborTimeKey ltKey = new LaborTimeKey();
            EstablishmentKey estKey = new EstablishmentKey();
            estKey.setCsoc(getCsoc());
            estKey.setCetab(getCetab());
            ltKey.setEstKey(estKey);
            ltKey.setProductionDate(datprod);
            ltKey.setResourceCode(line);
            ltKey.setNi1(ni1);

            if (currBegDate != null && currBegHour != null && currEndDate != null && currEndHour != null) {
                List<FLaborTimeBBean> lstBBean;
                try {
                    lstBBean = getFlabortimeCBS().queryElements(getIdCtx(), ltKey, 0, 99999);
                    if (lstBBean != null) {
                        for (FLaborTimeBBean bBean : lstBBean) {
                            Date dhdebBase = bBean.getBegDateHour();
                            if (bBean.getEndDateHour() == null) { // then end date-hour is empty (current period
                                // not closed)
                                if (dhdebBase != null) {
                                    if ((dhendInsert.after(dhdebBase))) {
                                        // PB : dates overlap
                                        getView().showError(I18nClientManager.translate(ProductionMo.T30735, true));
                                        error = true;
                                        break;
                                    }
                                } else {
                                    // PB : invalid dates
                                    getView().showError(I18nClientManager.translate(ProductionMo.T19497, true));
                                    error = true;
                                    break;
                                }

                            } else {
                                Date dhfinBase = bBean.getEndDateHour();
                                if (dhdebBase != null && dhfinBase != null) {
                                    if (dhendInsert.after(dhdebBase) && dhbegInsert.before(dhfinBase)) {
                                        // PB : dates overlap
                                        getView().showError(I18nClientManager.translate(ProductionMo.T30735, true));
                                        error = true;
                                        break;
                                    }
                                } else {
                                    // PB : invalid dates
                                    getView().showError(I18nClientManager.translate(ProductionMo.T19497, true));
                                    error = true;
                                    break;
                                }
                            }
                        }
                    }
                } catch (BusinessException e2) {
                    getView().show(new UIException(e2));
                    LOGGER.error("", e2);
                }
            } else {
                // PB : invalid dates
                getView().showError(I18nClientManager.translate(ProductionMo.T19497, true));
                error = true;
            }

            // //////////////////////// UPDATES THE NI1s OF THE FOLLOWING PERIODS ////////////////////////

            FLaborTimeBBean theBBeanBefore = new FLaborTimeBBean();
            int theNi1Before = 0;
            Boolean insertInTheMiddle = false;

            if (!error) {
                // we're going to insert a period and its staffs among the other periods.
                // But we must keep the order of the ni1s. From 1 to N, in the order of the dates-hours of the periods.
                // (because all the code is based on this rule)
                // So, let's take an example :
                // ni1 date-hour
                // 1 10
                // 2 20
                // 3 30
                // 4 40
                // We're inserting the date-hour 25.
                // We're going to go through the list by date-hour descending
                // and stop when the date-hour of the list is < the one to be inserted.
                // At each date-hour of the list > to the one to be inserted, we must add 1 to its ni1
                // and to the ones of its staffs.
                // In our case : 4 40 ==> 5 40
                // 3 30 ==> 4 30
                // After that we can insert our new period : at the ni1 where we stopped (2) + 1 ==> 3
                // For the staffs, we take those where we stopped (those of the date-hour 20).

                try {
                    ltKey.setNi1(0);
                    List<FLaborTimeBBean> lstBBean = getFlabortimeCBS().queryElements(getIdCtx(), ltKey, 0, 99999);
                    if (lstBBean != null) {
                        for (int i = lstBBean.size() - 1; i >= 0; i--) {
                            theBBeanBefore = lstBBean.get(i);
                            theNi1Before = theBBeanBefore.getLaborTimeKey().getNi1();
                            Date dhdebCurrent = theBBeanBefore.getBegDateHour();

                            if (dhdebCurrent.before(dhbegInsert)) {
                                insertInTheMiddle = true;
                                break;
                            } else {
                                // updates the LaborTime
                                try {
                                    getFlabortimeCBS().updateLaborTimeNi1(getIdCtx(), theBBeanBefore, theNi1Before + 1);
                                } catch (BusinessErrorsException e) {
                                    ((FLaborTimeModVCtrl) getViewerController()).showError(new UIErrorsException(e));
                                    ((FLaborTimeModFView) getView()).getErrorPanelView().getModel()
                                    .setErrors(e.getMapErrors());
                                    LOGGER.error("", e);
                                }

                                // updates the LaborStaffs
                                List<FLaborStaffBBean> lstLSbBean = getFlaborstaffCBS().queryElementsLaborTimeBean(
                                        getIdCtx(), theBBeanBefore, 0, 99999);
                                for (FLaborStaffBBean lsbBean : lstLSbBean) {
                                    try {
                                        getFlaborstaffCBS().updateLaborStaffNi1(getIdCtx(), lsbBean, theNi1Before + 1);
                                    } catch (BusinessErrorsException e) {
                                        ((FLaborTimeModVCtrl) getViewerController())
                                        .showError(new UIErrorsException(e));
                                        ((FLaborTimeModFView) getView()).getErrorPanelView().getModel()
                                        .setErrors(e.getMapErrors());
                                        LOGGER.error("", e);
                                    }
                                }
                            }
                        }
                    }
                } catch (BusinessException e) {
                    getView().show(new UIException(e));
                    LOGGER.error("", e);
                }

                // ///////////////// INSERTS THE NEW PERIOD AND ITS STAFFS /////////////////

                if (theBBeanBefore != null) {
                    try {
                        if (insertInTheMiddle) {
                            // STANDARD CASE : INSERT NOT AT THE HEAD OF THE LIST

                            List<FLaborStaffBBean> lstLSbBean = getFlaborstaffCBS().queryElementsLaborTimeBean(
                                    getIdCtx(), theBBeanBefore, 0, 99999);

                            theNi1Before++;

                            // inserts the LaborTime
                            theBBeanBefore.getLaborTimeKey().setNi1(theNi1Before);
                            theBBeanBefore.setBegDateHour(dhbegInsert);
                            theBBeanBefore.setEndDateHour(dhendInsert);
                            getFlabortimeCBS().insertLaborTime(getIdCtx(), theBBeanBefore);

                            // inserts the LaborStaffs : the same as the ones of the previous (in time) period
                            for (FLaborStaffBBean lsbBean : lstLSbBean) {
                                lsbBean.getLaborStaff().getLaborStaffKey().getLaborTimeKey().setNi1(theNi1Before);
                                getFlaborstaffCBS().insertLaborStaff(getIdCtx(), lsbBean);
                            }
                        } else {
                            // PARTICULAR CASE : INSERT AT THE HEAD OF THE LIST

                            LaborTimeKey laborTimeKey = new LaborTimeKey();
                            laborTimeKey.setEstKey(theBBeanBefore.getLaborTimeKey().getEstKey());
                            laborTimeKey.setProductionDate(theBBeanBefore.getLaborTimeKey().getProductionDate());
                            laborTimeKey.setResourceCode(theBBeanBefore.getLaborTimeKey().getResourceCode());
                            laborTimeKey.setNi1(2); // the staffs to insert are those of the next (in time) period,
                            // because
                            // when inserting at the head, there's no period before

                            theBBeanBefore = getFlabortimeCBS().getLaborTimeBBean(getIdCtx(), laborTimeKey);

                            List<FLaborStaffBBean> lstLSbBean = getFlaborstaffCBS().queryElementsLaborTimeBean(
                                    getIdCtx(), theBBeanBefore, 0, 99999);

                            theNi1Before = 1;

                            // inserts the LaborTime
                            theBBeanBefore.getLaborTimeKey().setNi1(theNi1Before);
                            theBBeanBefore.setBegDateHour(dhbegInsert);
                            theBBeanBefore.setEndDateHour(dhendInsert);
                            getFlabortimeCBS().insertLaborTime(getIdCtx(), theBBeanBefore);

                            // inserts the LaborStaffs : the same as the ones of the next (in time) period
                            for (FLaborStaffBBean lsbBean : lstLSbBean) {
                                lsbBean.getLaborStaff().getLaborStaffKey().getLaborTimeKey().setNi1(theNi1Before);
                                getFlaborstaffCBS().insertLaborStaff(getIdCtx(), lsbBean);
                            }
                        }

                        // closes the window
                        super.fireSelfFeatureClosingRequired();
                    } catch (BusinessException e) {
                        getView().show(new UIException(e));
                        LOGGER.error("", e);
                    } catch (BusinessErrorsException e) {
                        ((FLaborTimeModVCtrl) getViewerController()).showError(new UIErrorsException(e));
                        ((FLaborTimeModFView) getView()).getErrorPanelView().getModel().setErrors(e.getMapErrors());
                        LOGGER.error("", e);
                    }
                }
            }
        }
    }

    /**
     * Update of the period.
     */
    private void updateValidation() {

        // //////////////////////////////// INITS ////////////////////////////////////////

        Date currBegDate = (Date) getViewerView().getItfCurrentBegDate().getValue();
        Date currBegHour = (Date) getViewerView().getItfCurrentBegHour().getValue();
        Date currEndDate = (Date) getViewerView().getItfCurrentEndDate().getValue();
        Date currEndHour = (Date) getViewerView().getItfCurrentEndHour().getValue();
        Date prevEndDate = (Date) getViewerView().getItfPreviousEndDate().getValue();
        Date prevEndHour = (Date) getViewerView().getItfPreviousEndHour().getValue();
        Date nextBegDate = (Date) getViewerView().getItfNextBegDate().getValue();
        Date nextBegHour = (Date) getViewerView().getItfNextBegHour().getValue();
        Boolean error = false;

        // //////////////////////////////// CONTROLS ////////////////////////////////////////

        if (prevEndDate != null && prevEndHour != null) {
            if (date1AfterDate2(prevEndDate, prevEndHour, currBegDate, currBegHour)) {
                // PB : dates overlap
                getView().showError(I18nClientManager.translate(ProductionMo.T30735, true));
                error = true;
            }
        }

        if (currEndDate != null && currEndHour != null) {
            if (date1AfterDate2(currBegDate, currBegHour, currEndDate, currEndHour)
                    || date1EqualsDate2(currBegDate, currBegHour, currEndDate, currEndHour)) {
                // PB : the beginning must be BEFORE the end
                getView().showError(I18nClientManager.translate(ProductionMo.T19497, true));
                error = true;
            }

            if (nextBegDate != null && nextBegHour != null) {
                if (date1AfterDate2(currEndDate, currEndHour, nextBegDate, nextBegHour)) {
                    // PB : dates overlap
                    getView().showError(I18nClientManager.translate(ProductionMo.T30735, true));
                    error = true;
                }
            }
        }

        // ///////////////// UPDATES THE PERIOD /////////////////

        if (!error) {
            LaborTimeKey ltKey = new LaborTimeKey();
            EstablishmentKey estKey = new EstablishmentKey();
            estKey.setCsoc(getCsoc());
            estKey.setCetab(getCetab());
            ltKey.setEstKey(estKey);
            ltKey.setProductionDate(datprod);
            ltKey.setResourceCode(line);
            ltKey.setNi1(ni1);
            FLaborTimeBBean bBean;
            try {
                bBean = getFlabortimeCBS().getLaborTimeBBean(getIdCtx(), ltKey);
                if (bBean != null) {
                    Boolean go = false;

                    if (currBegDate != null && currBegHour != null) {
                        Date dhdeb = DateHelper.getDate(currBegDate, currBegHour);
                        bBean.setBegDateHour(dhdeb);
                        go = true;
                    }

                    if (currEndDate != null && currEndHour != null) {
                        Date dhfin = DateHelper.getDate(currEndDate, currEndHour);
                        bBean.setEndDateHour(dhfin);
                        go = true;
                    }

                    if (go) {
                        try {
                            getFlabortimeCBS().updateLaborTimeBean(getIdCtx(), bBean);

                            // closes the window
                            super.fireSelfFeatureClosingRequired();
                        } catch (BusinessException e) {
                            getView().show(new UIException(e));
                            LOGGER.error("", e);
                        } catch (BusinessErrorsException e) {
                            ((FLaborTimeModVCtrl) getViewerController()).showError(new UIErrorsException(e));
                            ((FLaborTimeModFView) getView()).getErrorPanelView().getModel().setErrors(e.getMapErrors());
                            LOGGER.error("", e);
                        }
                    }
                }
            } catch (BusinessException e1) {
                getView().show(new UIException(e1));
                LOGGER.error("", e1);
            }

        }
    }
}

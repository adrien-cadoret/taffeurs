/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeBModel.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBeanFields.LaborTimeBrowseColumns;


/**
 * Labor time browser model.
 * 
 * @author nle
 */
public class FLaborTimeBModel extends BrowserModel<FLaborTimeBBean> {
    /**
     * Default constructor.
     */
    public FLaborTimeBModel() {
        super();
        setBeanClass(FLaborTimeBBean.class);
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T34769, false),
                LaborTimeBrowseColumns.PERIOD.getValue(), 0)); // "period"
        addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T30157, false),
                LaborTimeBrowseColumns.DELAY.getValue(), 0)); // "delay"

        setFullyFetched(true);
        setFetchSize(9999999);
        setFirstFetchSize(999999);
    }
}

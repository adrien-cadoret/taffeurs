/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLabelFView.java,v $
 * Created on 17 Jun 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.flabels.touch;


import java.awt.Dimension;

import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;


/**
 * Label Feature view.
 * 
 * @author xg
 */
public class FLabelFView extends StandardTouchFeature {

    private FLabelBView bView;

    /**
     * Default constr.
     */
    public FLabelFView() {
        super();
        initialize();
    }

    /**
     * Gets the bView.
     * 
     * @return the bView.
     */
    public FLabelBView getbView() {
        if (bView == null) {
            bView = new FLabelBView();
            bView.setBounds(0, 0, 870, 500);
            Dimension d = new Dimension(1020, 700);
            bView.setPreferredSize(d);
            bView.setMinimumSize(d);
            bView.setMaximumSize(d);
        }
        return bView;
    }

    /**
     * Browser view initialisation.
     */
    private void initialize() {
        add(getbView());
    }
}

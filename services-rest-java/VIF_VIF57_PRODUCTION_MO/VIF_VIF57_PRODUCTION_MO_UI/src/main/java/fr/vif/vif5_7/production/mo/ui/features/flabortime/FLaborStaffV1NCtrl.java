/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffV1NCtrl.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import java.awt.Dimension;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.KeyboardBean;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowSelectionEvent;
import fr.vif.jtech.ui.events.table.TableRowSelectionListener;
import fr.vif.jtech.ui.input.InputSpecialViewer1NController;
import fr.vif.jtech.ui.input.kbdialog.NumKeyboardDialogController;
import fr.vif.jtech.ui.input.kbdialog.touch.TouchNumKeyboardDialog;
import fr.vif.jtech.ui.input.touch.TouchCompositeInput;
import fr.vif.jtech.ui.util.SpringWrapperInvocation;
import fr.vif.jtech.ui.util.factories.SpringFactoryHelper;
import fr.vif.jtech.ui.util.touch.dialogs.DefaultDialogDisplayListener;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;


/**
 * Viewer 1-N containing a browse of Labor Staffs.
 * 
 * @author nle
 */
public class FLaborStaffV1NCtrl extends InputSpecialViewer1NController<FLaborStaffBBean> implements
        TableRowSelectionListener {
    /** LOGGER. */
    private static final Logger LOGGER               = Logger.getLogger(FLaborStaffV1NCtrl.class);

    private int                 lastRowSelectedIndex = 0;

    private FLaborStaffBBean    currentBean          = new FLaborStaffBBean();

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {

        // comes from the window allowing to enter the number of people

        String myValueStr = (String) ((KeyboardBean) event.getDialogBean()).getValue();

        if (myValueStr != null && !"".equals(myValueStr)) {
            double myValueDbl = Double.parseDouble((myValueStr).replace(",", "."));

            // puts the number in the browse
            getCurrentBean().getLaborStaff().setStaff(myValueDbl);
        }
    }

    /**
     * Gets the lastRowSelectedIndex.
     * 
     * @return the lastRowSelectedIndex.
     */
    public int getLastRowSelectedIndex() {
        return lastRowSelectedIndex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initBrowser() {
        super.initBrowser();

        // if this line isn't there, the focus goes to the first line of the browse between the time
        // when i click on a line and the time where the staff entry window appears
        getBrowserController().getModel().setCurrentRowNumber(lastRowSelectedIndex);

        getBrowserController().addTableRowSelectionListener(this);
    }

    /**
     * Sets the lastRowSelectedIndex.
     * 
     * @param lastRowSelectedIndex lastRowSelectedIndex.
     */
    public void setLastRowSelectedIndex(final int lastRowSelectedIndex) {
        this.lastRowSelectedIndex = lastRowSelectedIndex;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tableRowSelected(final TableRowSelectionEvent event) {

        // occurs when I click on a button of the bottom browse ==> displays the window to enter a numeric ==> number of
        // people

        FLaborStaffBBean bean = getCurrentBean();

        if (bean != null) {
            SpringWrapperInvocation sdi = (SpringWrapperInvocation) SpringFactoryHelper.getInstance().getBean(
                    "TouchNumKeyboardDialog");
            TouchNumKeyboardDialog view = (TouchNumKeyboardDialog) sdi.createObject();

            view.initialize(TouchCompositeInput.class, "##0.00", bean.getLaborStaff().getStaff(), false); // the init
                                                                                                          // value
                                                                                                          // doesn't
                                                                                                          // function
            view.setTitle(bean.getLaborStaff().getResourceLabel());
            view.setMinimumSize(new Dimension(400, 600));
            view.setSize(400, 600);
            view.setPreferredSize(view.getSize());

            NumKeyboardDialogController controller;
            controller = (NumKeyboardDialogController) SpringFactoryHelper.getInstance().getBean(
                    "NumKeyboardDialogController");
            controller.addDisplayListener(DefaultDialogDisplayListener.getInstance());
            controller.addDialogChangeListener(this);
            controller.setDialogView(view);

            currentBean = (FLaborStaffBBean) getBrowserController().getModel().getBeanAt(event.getSelectedRow());

            lastRowSelectedIndex = event.getSelectedRow();

            controller.initialize();
        }
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputBModel.java,v $
 * Created on 10 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.touch.TouchBrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBeanEnum;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;


/**
 * Browser Model.
 * 
 * @author xg
 */
public class FBoningDataEntryListInputBModel extends TouchBrowserModel<FBoningDataEntryListInputBBean> {

    private Format formatQty = new Format(Alignment.RIGHT_ALIGN, MOUIConstants.TOUCHSCREEN_BG_BROWSER,
                                     StandardColor.TOUCHSCREEN_FG_BROWSER,
                                     StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE, "11");

    /**
     * Default constructor.
     * 
     */
    public FBoningDataEntryListInputBModel() {
        super();
        setBeanClass(FBoningDataEntryListInputBBean.class);

        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T9520, false),
                FBoningDataEntryListInputBBeanEnum.LINE_NUMBER.getValue(), 70, "####"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29437, false),
                FBoningDataEntryListInputBBeanEnum.FORMATTED_CREATION_DATE_TIME.getValue(), 140, "dd/MM/yy HH:mm"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29552, false),
                FBoningDataEntryListInputBBeanEnum.BATCH.getValue(), 170, "15"));
        addColumn(new BrowserColumn(I18nClientManager.translate(GenKernel.T426, false),
                FBoningDataEntryListInputBBeanEnum.ITEM_ID.getValue(), 170, "15"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29553, false),
                FBoningDataEntryListInputBBeanEnum.FORMATTED_FIRST_QTY_UNIT.getValue(), 130, formatQty));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29553, false),
                FBoningDataEntryListInputBBeanEnum.FORMATTED_SECOND_QTY_UNIT.getValue(), 130, formatQty));

        setFetchSize(10000);
        setFirstFetchSize(10000);
    }
}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownVView.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Paint;

import javax.swing.JTable;

import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.treetable.InputTreeTableController;
import fr.vif.jtech.ui.treetable.swing.SwingInputTreeTable;
import fr.vif.jtech.ui.viewer.swing.StandardSwingViewer;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownVBean;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.StaffChart;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.FTimeBreakDownTreeTableModel;


/**
 * TimeBreakDown : Viewer View.
 * 
 * @author cj
 * @version $Revision: 1.4 $, $Date: 2016/06/15 12:50:09 $
 */
public class FTimeBreakDownVView extends StandardSwingViewer<FTimeBreakDownVBean> {
    private FTimeBreakDownTreeTableView ftimeBreakDownTreeTableView = null;
    private StaffChart                  staffChart;

    /**
     * Default constructor.
     * 
     */
    public FTimeBreakDownVView() {
        super();
        initialize();
    }

    /**
     * Gets the fbatchValidationTreeTableView.
     * 
     * @return the fbatchValidationTreeTableView.
     */
    public SwingInputTreeTable getFtimeBreakDownTreeTableView() {
        if (ftimeBreakDownTreeTableView == null) {
            FTimeBreakDownTreeTableModel model = new FTimeBreakDownTreeTableModel();
            ftimeBreakDownTreeTableView = new FTimeBreakDownTreeTableView(model, InputTreeTableController.class);
            ftimeBreakDownTreeTableView.setBeanProperty("breakdownHchyTreeNodeElt");
            ftimeBreakDownTreeTableView.getJxTreeTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        }
        return ftimeBreakDownTreeTableView;
    }

    /**
     * Gets the staffChart.
     * 
     * @return the staffChart.
     */
    public StaffChart getStaffChart() {
        if (staffChart == null) {
            staffChart = new StaffChart();
            staffChart.setOpaque(false);
            staffChart.getChart().setBackgroundPaint(toColor(StandardColor.BG_DEFAULT));
            staffChart.getChart().getLegend().setBackgroundPaint(toColor(StandardColor.BG_DEFAULT));
            staffChart.getChart().getTitle().setPaint(Color.BLACK);
            staffChart.getChart().getXYPlot().setBackgroundPaint(toColor(StandardColor.BG_DEFAULT));
            staffChart.getChart().getXYPlot().setDomainGridlinePaint(Color.GRAY);
            staffChart.getChart().getXYPlot().setRangeGridlinePaint(Color.GRAY);

        }
        return staffChart;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        this.setOpaque(false);
        this.setLayout(new GridBagLayout());

        GridBagConstraints gbTreeTable = new GridBagConstraints();
        gbTreeTable.gridx = 0;
        gbTreeTable.gridy = 0;
        gbTreeTable.fill = GridBagConstraints.BOTH;
        gbTreeTable.anchor = GridBagConstraints.NORTH;
        gbTreeTable.weightx = 1;
        gbTreeTable.weighty = 1;

        GridBagConstraints gbChart = new GridBagConstraints();
        gbChart.gridx = 0;
        gbChart.gridy = 1;
        gbChart.fill = GridBagConstraints.BOTH;
        gbChart.anchor = GridBagConstraints.CENTER;
        gbChart.weightx = 1;
        gbChart.weighty = 0.6;

        this.add(getFtimeBreakDownTreeTableView(), gbTreeTable);
        this.add(getStaffChart(), gbChart);
    }

    /**
     * Transforms a color into RGB.
     * 
     * @param color : the color
     * @return the RGB
     */
    private Paint toColor(final StandardColor color) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue());
    }

}

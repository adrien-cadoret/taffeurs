/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionContainerMOListVView.java,v $
 * Created on 7 janv. 09 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioncontainermolist.swing;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import fr.vif.jtech.ui.input.swing.SwingInputTextEditor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.jtech.ui.viewer.swing.StandardSwingViewer;
import fr.vif.vif5_7.gen.comment.ui.composites.simplecomment.CCommentCtrl;
import fr.vif.vif5_7.gen.comment.ui.composites.simplecomment.swing.CCommentView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;


/**
 * viewer view.
 * 
 * @author gv
 */
public class FProductionContainerMOListVView extends StandardSwingViewer<FMOListVBean> {

    private CCommentView         commentView     = null;
    private SwingInputTextEditor criteriaLstText = null;

    /**
     * Default constructor.
     * 
     */
    public FProductionContainerMOListVView() {
        super();
        initialize();
    }

    /**
     * Gets the commentTouchView.
     * 
     * @category getter
     * @return the commentTouchView.
     */
    public final CCommentView getCommentTouchView() {
        return commentView;
    }

    /**
     * Gets the commentView.
     * 
     * @category getter
     * @return the commentView.
     */
    public final CCommentView getCommentView() {
        if (commentView == null) {
            commentView = new CCommentView(CCommentCtrl.class);
            // Dimension d = new Dimension(870, 60);
            // commentView.setMaximumSize(d);
            // commentView.setMinimumSize(d);
            // commentView.setPreferredSize(d);
            commentView.setBeanProperty("comment");
            // commentView.getEditorComment().setVisibleLabel(false);
        }
        return commentView;
    }

    /**
     * Sets the commentTouchView.
     * 
     * @category setter
     * @param cView commentTouchView.
     */
    public final void setCommentTouchView(final CCommentView cView) {
        this.commentView = cView;
    }

    /**
     * Gets the CriteriaLstText.
     * 
     * @return the SwingInputTextEditor
     */
    private SwingInputTextEditor getCriteriaLstText() {
        if (criteriaLstText == null) {
            criteriaLstText = new SwingInputTextEditor();
            criteriaLstText.getJTextArea().setOpaque(true);
            criteriaLstText.setBeanBased(true);
            criteriaLstText.setBeanProperty("listCriteria");
            // criteriaLstText.setIcon("/images/vif/vif57/production/mo/littleIcon_criteria.png");
            criteriaLstText.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_LABEL_FONT));
            criteriaLstText.setAlwaysDisabled(true);
            criteriaLstText.setUpdateable(false);
            // Dimension d = new Dimension(870, 35);
            // criteriaLstText.setPreferredSize(d);
            // criteriaLstText.setMinimumSize(d);
            // criteriaLstText.setMaximumSize(d);
        }
        return criteriaLstText;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        setLayout(new GridBagLayout());
        final GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.weighty = 0.7;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        this.add(getCriteriaLstText(), gridBagConstraints);
        final GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.fill = GridBagConstraints.BOTH;
        gridBagConstraints1.weighty = 0.3;
        gridBagConstraints1.weightx = 1;
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 1;
        gridBagConstraints1.insets = new Insets(0, 0, 1, 0);
        this.add(getCommentView(), gridBagConstraints1);

    }

}

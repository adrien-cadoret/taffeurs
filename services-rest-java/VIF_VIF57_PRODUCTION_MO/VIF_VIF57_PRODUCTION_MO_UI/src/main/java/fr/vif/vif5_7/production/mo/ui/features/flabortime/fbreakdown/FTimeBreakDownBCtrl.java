/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownBCtrl.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown;


import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownVBean;
import fr.vif.vif5_7.production.mo.business.services.features.ftimebreakdown.FTimeBreakDownCBS;


/**
 * TimeBreakDown : Browser Controller.
 * 
 * @author cj
 * @version $Revision: 1.3 $, $Date: 2016/05/26 09:41:19 $
 */
public class FTimeBreakDownBCtrl extends AbstractBrowserController<FTimeBreakDownVBean> {

    private FTimeBreakDownCBS ftimeBreakDownCBS;

    /**
     * Gets the ftimeBreakDownCBS.
     * 
     * @category getter
     * @return the ftimeBreakDownCBS.
     */
    public final FTimeBreakDownCBS getFtimeBreakDownCBS() {
        return ftimeBreakDownCBS;
    }

    /**
     * Sets the ftimeBreakDownCBS.
     * 
     * @category setter
     * @param ftimeBreakDownCBS ftimeBreakDownCBS.
     */
    public final void setFtimeBreakDownCBS(final FTimeBreakDownCBS ftimeBreakDownCBS) {
        this.ftimeBreakDownCBS = ftimeBreakDownCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FTimeBreakDownVBean convert(final Object bean) {
        return new FTimeBreakDownVBean();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FTimeBreakDownVBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        ArrayList<FTimeBreakDownVBean> result = new ArrayList<FTimeBreakDownVBean>();
        result.add(new FTimeBreakDownVBean());

        return result;
    }

}

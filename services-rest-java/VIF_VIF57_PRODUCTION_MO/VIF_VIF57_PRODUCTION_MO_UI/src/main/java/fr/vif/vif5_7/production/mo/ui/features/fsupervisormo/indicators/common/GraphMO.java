/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_COMMON
 * File : $RCSfile: GraphMO.java,v $
 * Created on 5 août 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.GraphTypeEnum;


/**
 * Contains a graph.
 * 
 * @author ag
 */
public class GraphMO implements Serializable {
    /**
     * Serial Version UID.
     */
    private static final long    serialVersionUID = 1L;

    private List<GraphMODataSet> graphData        = new ArrayList<GraphMODataSet>();
    private boolean              horizontal       = true;

    private String               horizontalLabel  = null;

    private boolean              showLegend       = true;

    private String               title            = null;

    private GraphTypeEnum        type             = GraphTypeEnum.BAR_STACKED;

    private String               verticalLabel    = null;

    /**
     * Constructor.
     */
    public GraphMO() {
        super();
        this.type = GraphTypeEnum.BAR_STACKED;
        setTitle(I18nClientManager.translate(Jtech.T30723, false));
    }

    /**
     * Constructor with title and type.
     * 
     * @param title title
     * @param type type
     */
    public GraphMO(final GraphTypeEnum type, final String title) {
        super();
        this.title = title;
        this.type = type;
    }

    /**
     * Constructor with title, type and data.
     * 
     * @param title title
     * @param type type
     * @param graphData graphData
     */
    public GraphMO(final GraphTypeEnum type, final String title, final List<GraphMODataSet> graphData) {
        super();
        this.title = title;
        this.type = type;
        this.graphData = graphData;
    }

    /**
     * Gets the graphData.
     * 
     * @return the graphData.
     */
    public List<GraphMODataSet> getGraphData() {
        return graphData;
    }

    /**
     * Gets the horizontal.
     * 
     * @return the horizontal.
     */
    public boolean getHorizontal() {
        return horizontal;
    }

    /**
     * Gets the horizontalLabel.
     * 
     * @return the horizontalLabel.
     */
    public String getHorizontalLabel() {
        return horizontalLabel;
    }

    /**
     * Gets the showLegend.
     * 
     * @return the showLegend.
     */
    public boolean getShowLegend() {
        return showLegend;
    }

    /**
     * Gets the title.
     * 
     * @return the title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the type.
     * 
     * @return the type.
     */
    public GraphTypeEnum getType() {
        return type;
    }

    /**
     * Gets the verticalLabel.
     * 
     * @return the verticalLabel.
     */
    public String getVerticalLabel() {
        return verticalLabel;
    }

    /**
     * Sets the graphData.
     * 
     * @param graphData graphData.
     */
    public void setGraphData(final List<GraphMODataSet> graphData) {
        this.graphData = graphData;
    }

    /**
     * Sets the horizontal.
     * 
     * @param horizontal horizontal.
     */
    public void setHorizontal(final boolean horizontal) {
        this.horizontal = horizontal;
    }

    /**
     * Sets the horizontalLabel.
     * 
     * @param horizontalLabel horizontalLabel.
     */
    public void setHorizontalLabel(final String horizontalLabel) {
        this.horizontalLabel = horizontalLabel;
    }

    /**
     * Sets the showLegend.
     * 
     * @param showLegend showLegend.
     */
    public void setShowLegend(final boolean showLegend) {
        this.showLegend = showLegend;
    }

    /**
     * Sets the title.
     * 
     * @param title title.
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Sets the type.
     * 
     * @param type type.
     */
    public void setType(final GraphTypeEnum type) {
        this.type = type;
    }

    /**
     * Sets the verticalLabel.
     * 
     * @param verticalLabel verticalLabel.
     */
    public void setVerticalLabel(final String verticalLabel) {
        this.verticalLabel = verticalLabel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "Graph [title=" + title + ", type=" + type + "]";
    }

}

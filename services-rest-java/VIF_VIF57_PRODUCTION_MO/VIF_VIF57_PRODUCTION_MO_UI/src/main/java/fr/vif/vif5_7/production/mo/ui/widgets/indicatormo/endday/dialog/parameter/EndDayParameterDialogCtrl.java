/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: EndDayParameterDialogCtrl.java,v $
 * Created on 28 Avril 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.dialog.parameter;


import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.sales.act.SalesAct;
import fr.vif.vif5_7.gen.location.business.beans.features.farea.FAreaSBean;
import fr.vif.vif5_7.gen.location.ui.composites.carea.CAreaCtrl;
import fr.vif.vif5_7.gen.location.ui.composites.carea.swing.CAreaView;


/**
 * End Day Parameter Dialog Controller.
 *
 * @author ac
 */
public class EndDayParameterDialogCtrl extends AbstractStandardDialogController {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(EndDayParameterDialogCtrl.class);

    private CAreaView           cArea  = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void beanValidation(final Object dialogBean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - beanValidation(dialogBean=" + dialogBean + ")");
        }

        super.beanValidation(dialogBean);

        EndDayParameterDialogBean parameterBean = (EndDayParameterDialogBean) getDialogBean();

        if (parameterBean.getArea() == null) {
            throw new UIException(Generic.T12640, new VarParamTranslation(I18nClientManager.translate(GenKernel.T30820,
                    false)));
        }

        ((CAreaCtrl) cArea.getController()).validateBean(false, "code", parameterBean.getArea());

        if (parameterBean.getIsTimeWindowContext()) {

            // Min bound
            if (parameterBean.getTwBeginDate() == null) {
                throw new UIException(Generic.T12640, new VarParamTranslation(
                        I18nClientManager.translate(Generic.T17440)));
            } else {
                if (parameterBean.getTwBeginTime() == null) {
                    throw new UIException(Generic.T12640, new VarParamTranslation(
                            I18nClientManager.translate(Generic.T5019)));
                }
            }

            // Max bound
            if (parameterBean.getTwEndDate() == null) {
                throw new UIException(Generic.T12640, new VarParamTranslation(
                        I18nClientManager.translate(Generic.T17441)));
            } else {
                if (parameterBean.getTwEndTime() == null) {
                    throw new UIException(Generic.T12640, new VarParamTranslation(I18nClientManager.translate(
                            SalesAct.T31997, false)));
                }
            }

            // Checking on maximum bound of time window

            Calendar cBeginDate = Calendar.getInstance();
            Calendar cBeginTime = Calendar.getInstance();
            cBeginDate.setTime(parameterBean.getTwBeginDate());
            cBeginTime.setTime(parameterBean.getTwBeginTime());
            long sum = cBeginDate.getTimeInMillis() + cBeginTime.getTimeInMillis();
            Calendar cBeginDateAndTime = Calendar.getInstance();
            cBeginDateAndTime.setTimeInMillis(sum);

            Calendar cEndDate = Calendar.getInstance();
            Calendar cEndTime = Calendar.getInstance();
            cEndDate.setTime(parameterBean.getTwEndDate());
            cEndTime.setTime(parameterBean.getTwEndTime());
            long sum2 = cEndDate.getTimeInMillis() + cEndTime.getTimeInMillis();
            Calendar cEndDateAndTime = Calendar.getInstance();
            cEndDateAndTime.setTimeInMillis(sum2);

            if (cEndDateAndTime.compareTo(cBeginDateAndTime) < 0) {
                throw new UIException(Generic.T15250);
            }

        } else {
            Date productionDate = parameterBean.getProductionDate();
            if (productionDate == null) {
                throw new UIException(Generic.T12640, new VarParamTranslation(I18nClientManager.translate(
                        ProductionMo.T34329, false)));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - beanValidation(dialogBean=" + dialogBean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        initializeArea();
        super.initialize();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - validateBean(creationMode=" + creationMode + ", beanProperty=" + beanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - validateBean(creationMode=" + creationMode + ", beanProperty=" + beanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - valueChanged(beanProperty=" + beanProperty + ", propertyValue=" + propertyValue + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - valueChanged(beanProperty=" + beanProperty + ", propertyValue=" + propertyValue + ")");
        }
    }

    /**
     * Initialize Area composite.
     */
    private void initializeArea() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeArea()");
        }

        cArea = ((EndDayParameterDialogView) getView()).getcArea();
        if (cArea.getInitialHelpSelection() == null) {
            FAreaSBean selection = new FAreaSBean(true,
                    ((EndDayParameterDialogBean) getModel().getDialogBean()).getEstablishmentKey());
            cArea.setCodeMandatory(true);
            cArea.setCodeUseFieldHelp(true);
            cArea.setCodeInitialHelpSelection(selection);
            cArea.setVerifyInputWhenFocusTarget(true);

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeArea()");
        }
    }
}

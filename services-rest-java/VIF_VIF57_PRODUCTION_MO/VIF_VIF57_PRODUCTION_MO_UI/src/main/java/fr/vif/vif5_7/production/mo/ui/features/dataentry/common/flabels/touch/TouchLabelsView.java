/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: TouchLabelsView.java,v $
 * Created on 20 nov. 08 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.flabels.touch;


import java.awt.Frame;

import fr.vif.jtech.ui.input.kbdialog.NumKeyboardDialogView;
import fr.vif.jtech.ui.input.kbdialog.touch.TouchNumKeyboardDialog;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.flabels.DLabelsIView;


/**
 * factory to create touchNumKeyboardDialog.
 *
 * @author gv
 */
public class TouchLabelsView implements DLabelsIView {

    /**
     * ctor.
     */
    public TouchLabelsView() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public NumKeyboardDialogView getLabelsKeyboardView(final String title) {
        TouchNumKeyboardDialog touchNumKeyboardDialog = new TouchNumKeyboardDialog((Frame) null, Integer.class, "###",
                0, false);
        touchNumKeyboardDialog.setTitle(title);
        return touchNumKeyboardDialog;
    }
}

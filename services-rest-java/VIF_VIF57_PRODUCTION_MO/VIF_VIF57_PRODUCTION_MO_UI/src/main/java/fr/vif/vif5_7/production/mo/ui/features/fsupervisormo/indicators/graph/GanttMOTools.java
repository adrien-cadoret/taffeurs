/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: GanttMOTools.java,v $
 * Created on 26 mars 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;

import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGanttMOBean;
import fr.vif.vif5_7.production.mo.constants.GanttValueTypeEnum;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.GraphTypeEnum;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.IndicatorAxesEnum;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOData;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMODataSet;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOPeriod;


/**
 * Tools for Gantt chart.
 * 
 * @author jd
 */
public final class GanttMOTools {

    /**
     * Margin (in minutes) at the begin and the end of the gantt.
     */
    public static final int     MARGIN_IN_GRAPH = 15;

    /** LOGGER. */
    private static final Logger LOGGER          = Logger.getLogger(GanttMOTools.class);

    /**
     * Constructor.
     */
    private GanttMOTools() {

    }

    /**
     * convert a {@link FGanttMOBean} to a MOGraph .
     * 
     * @param bean {@link FGanttMOBean} to convert
     * @param options the options
     * @return a {@link GraphMO}
     */
    public static GraphMO convertToGraph(final FGanttMOBean bean, final GraphMOOptions options) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertToGraph(bean=" + bean + ")");
        }

        GraphMO graph = new GraphMO();

        graph.setType(GraphTypeEnum.BAR_GANTT_XY);

        // Convert
        Map<String, GraphMODataSet> dataSets = new TreeMap<String, GraphMODataSet>();

        HashMap<FGanttMOKey, List<IndicatorMO>> keys = new HashMap<FGanttMOKey, List<IndicatorMO>>();

        // To stock in options the begin date of the first MO and the end date of the last MO
        Calendar endOfLastMO = Calendar.getInstance();
        Calendar beginOfFirstMO = Calendar.getInstance();
        Calendar tmpEnd = Calendar.getInstance();
        Calendar tmpBegin = Calendar.getInstance();
        // NLE : List of Gantt bars
        for (IndicatorMO mo : bean.getIndicators()) {
            String tmpKey = null;
            String title = "";

            // Generate a line if indicators are both on same time

            if (IndicatorAxesEnum.PRODUCTION_LINE.equals(bean.getAxe())) {
                tmpKey = mo.getInfos().getLineCL().getCode(); // + mo.getPoKey().getCounter1();
                title = mo.getInfos().getLineCL().getLabel();
            } else if (IndicatorAxesEnum.ITEM_GROUP.equals(bean.getAxe())) {
                tmpKey = mo.getInfos().getHchyValueCL().getCode(); // + mo.getPoKey().getCounter1();
                title = mo.getInfos().getHchyValueCL().getLabel();
            } else if (IndicatorAxesEnum.TEAM.equals(bean.getAxe())) {
                tmpKey = mo.getInfos().getTeamCL().getCode(); // + mo.getPoKey().getCounter1();
                title = mo.getInfos().getTeamCL().getLabel();
            } else if (IndicatorAxesEnum.SECTOR.equals(bean.getAxe())) {
                tmpKey = mo.getInfos().getSectorCL().getCode(); // + mo.getPoKey().getCounter1();
                title = mo.getInfos().getSectorCL().getLabel();
            }

            // To set up the right begin and end date and the selected date in options at the loop beginning
            if (mo.equals(bean.getIndicators().get(0))) {
                endOfLastMO.setTime(mo.getInfos().getEffectiveEndDate());
                beginOfFirstMO.setTime(mo.getInfos().getEffectiveBeginDate());
                options.setSelectedDate(mo.getManufacturingDate());
            }

            tmpEnd.setTime(mo.getInfos().getEffectiveEndDate());
            if (tmpEnd.compareTo(endOfLastMO) == 1) {
                endOfLastMO.setTime(tmpEnd.getTime());
            }

            tmpBegin.setTime(mo.getInfos().getEffectiveBeginDate());
            if (tmpBegin.compareTo(beginOfFirstMO) == -1) {
                beginOfFirstMO.setTime(tmpBegin.getTime());
            }

            // NLE : Finds the line (keyToUse) on which adding this bar in order not to be over another one
            FGanttMOKey keyToUse = null;
            for (FGanttMOKey k : keys.keySet()) {
                if (!k.getKey().equalsIgnoreCase(tmpKey)) {
                    continue;
                }
                List<IndicatorMO> mos = keys.get(k);

                // Can be set on this current line ?
                boolean keyIsUsable = true;
                for (IndicatorMO m : mos) {
                    Date d1 = mo.getInfos().getEffectiveBeginDate();
                    Date f1 = mo.getInfos().getEffectiveEndDate();
                    Date d2 = m.getInfos().getEffectiveBeginDate();
                    Date f2 = m.getInfos().getEffectiveEndDate();

                    boolean ok = false;
                    if (d1.after(f2)) {
                        ok = true;
                    } else if (d1.before(f2)) {
                        if (f1.before(d2)) {
                            ok = true;
                        }
                    }
                    if (!ok) {
                        keyIsUsable = false;
                        break;
                    }
                }
                if (keyIsUsable) {
                    keyToUse = k;
                }

            }

            // NLE : No line already existing can be re-used, add another one
            if (keyToUse == null) {
                int i = -1;
                for (FGanttMOKey k : keys.keySet()) {
                    if (!k.getKey().equalsIgnoreCase(tmpKey)) {
                        continue;
                    }
                    i = Math.max(i, k.getLine());
                }

                keyToUse = new FGanttMOKey(tmpKey, i + 1);
            }

            if (!keys.containsKey(keyToUse)) {
                List<IndicatorMO> mos = new ArrayList<IndicatorMO>();
                mos.add(mo);
                keys.put(keyToUse, mos);
            } else {
                keys.get(keyToUse).add(mo);
            }

            String key = keyToUse.getKey() + " " + keyToUse.getLine();

            GraphMODataSet set = dataSets.get(key);
            if (set == null) {
                set = new GraphMODataSet();
                set.setTitle("".equals(title) ? key : title);
                dataSets.put(key, set);
            }
            GregorianCalendar gcStart = new GregorianCalendar();
            GregorianCalendar gcEnd = new GregorianCalendar();

            // NLE if (POIndicatorState.NOT_STARTED.equals(mo.getInfos().getState())) {
            // NLE gcStart.setTime(mo.getInfos().getExpectedBeginDate());
            // NLE gcEnd.setTime(mo.getInfos().getExpectedEndDate());
            // NLE } else {
            gcStart.setTime(mo.getInfos().getEffectiveBeginDate());
            gcEnd.setTime(mo.getInfos().getEffectiveEndDate());

            // NLE }

            GraphMOPeriod<IndicatorMO> period = new GraphMOPeriod<IndicatorMO>(gcStart.getTime(), gcEnd.getTime());
            period.setBeanSource(mo);

            // period.setToolTipText(getToolTipTask(mo));
            period.setPercentComplete(mo.getQties().getProcessQtySU().getEffectiveQty()
                    / mo.getQties().getProcessQtySU().getExpectedQty());

            GraphMOData<String, GraphMOPeriod<IndicatorMO>> data = new GraphMOData<String, GraphMOPeriod<IndicatorMO>>(
                    mo.getInfos().getItemCL().getLabel(), period);

            // Set annotation
            if (options.getShowAnnotations()) { // FIXME Should be modified by a menu action
                setAnnotation(period, options.getToShow());
            }

            set.getGraphData().add(data);

        }

        // // Setting up the bounds of the gantt
        // final int margin = 900;
        // beginOfFirstMO.setTime(DateHelper.addTimeSeconds(beginOfFirstMO.getTime(), -margin)); // begins at 1/4 hour
        // // before (900 seconds)
        // endOfLastMO.setTime(DateHelper.addTimeSeconds(endOfLastMO.getTime(), +margin)); // ends at 1/4 hour after
        // (900
        // // seconds)
        options.setBeginOfFirstMO(beginOfFirstMO);
        options.setEndOfLastMO(endOfLastMO);
        setScrollBounds(beginOfFirstMO, endOfLastMO, options);

        for (GraphMODataSet set : dataSets.values()) {
            graph.getGraphData().add(set); // 0 to inverse order of datasets. JFreechart start by the bottom
        }
        // Don't show legend for Gantt ! Not useful
        graph.setShowLegend(false);
        graph.setTitle(bean.getTitle());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertToGraph(bean=" + bean + ")=" + graph);
        }
        return graph;
    }

    /**
     * 
     * Create annotation of the period.
     * 
     * @param period a graphMO period
     * @param toShow a list of annotations to show
     */
    public static void setAnnotation(final GraphMOPeriod<IndicatorMO> period, final List<GanttValueTypeEnum> toShow) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setAnnotation(period=" + period + ")");
        }

        if (toShow.contains(GanttValueTypeEnum.CHRONO)) {
            period.getAnnotations().add(Integer.toString(period.getBeanSource().getPoKey().getChrono().getChrono()));
        }

        if (toShow.contains(GanttValueTypeEnum.ITEM_LABEL)) {
            period.getAnnotations().add(period.getBeanSource().getInfos().getItemCL().getLabel());
        }

        if (toShow.contains(GanttValueTypeEnum.CODE_LABEL)) {
            period.getAnnotations().add(period.getBeanSource().getInfos().getItemCL().getCode());
        }
        if (toShow.contains(GanttValueTypeEnum.EFFECTIVE_QTY) && !toShow.contains(GanttValueTypeEnum.EXPECTED_QTY)) {
            period.getAnnotations().add(
                    GraphMOTools.FORMAT_DEC.format(period.getBeanSource().getQties().getProcessQtySU()
                            .getEffectiveQty())
                            + " " + period.getBeanSource().getQties().getProcessQtySU().getUnitID());
        }
        if (!toShow.contains(GanttValueTypeEnum.EFFECTIVE_QTY) && toShow.contains(GanttValueTypeEnum.EXPECTED_QTY)) {
            period.getAnnotations().add(
                    GraphMOTools.FORMAT_DEC
                            .format(period.getBeanSource().getQties().getProcessQtySU().getExpectedQty())
                            + " "
                            + period.getBeanSource().getQties().getProcessQtySU().getUnitID());
        }
        if (toShow.contains(GanttValueTypeEnum.EFFECTIVE_QTY) && toShow.contains(GanttValueTypeEnum.EXPECTED_QTY)) {
            period.getAnnotations().add(
                    GraphMOTools.FORMAT_DEC.format(period.getBeanSource().getQties().getProcessQtySU()
                            .getEffectiveQty())
                            + " / " + //
                            GraphMOTools.FORMAT_DEC.format(period.getBeanSource().getQties().getProcessQtySU()
                                    .getExpectedQty()) + " " + //
                            period.getBeanSource().getQties().getProcessQtySU().getUnitID());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setAnnotation(period=" + period + ")");
        }
    }

    /**
     * Set lower and upper bounds.
     * 
     * @param beginDate the begin date
     * @param endDate the end Date
     * @param options the Options used to display the Gantt
     */
    private static void setScrollBounds(final Calendar beginDate, final Calendar endDate, final GraphMOOptions options) {

        int lowerBound = options.getBeginOfFirstMO().get(Calendar.HOUR_OF_DAY) * 60
                + options.getBeginOfFirstMO().get(Calendar.MINUTE);
        int upperBound = 0;
        long duration = options.getEndOfLastMO().getTime().getTime() - options.getBeginOfFirstMO().getTime().getTime();

        if (duration > 0) {
            // If difference in minute between the two dates is higher than 45 days, the system crashes.
            upperBound = (int) (TimeUnit.MILLISECONDS.toMinutes(duration) + lowerBound);
            options.setLowerBound(lowerBound);
            options.setUpperBound(upperBound - (options.getNbMinutesToShow()));
            // options.setUpperBound(upperBound);
        } else {
            options.setLowerBound(0);
            options.setUpperBound(1440);
        }

        options.setLowerBound(options.getLowerBound() - MARGIN_IN_GRAPH);
        options.setUpperBound(options.getUpperBound() + MARGIN_IN_GRAPH);

    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputCompFCtrl.java,v $
 * Created on 7 Dec 2015 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcomp;


// CHECKSTYLE:OFF
import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcomp.FProductionInputCompVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcomp.FProductionInputCompCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylistcomp.FDataEntryListCompFModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer.AbstractFProductionInputFCtrl;


// CHECKSTYLE:ON
/**
 * InputOperationItem : Feature Controller.
 * 
 * @author vr
 */
public class FProductionInputCompFCtrl extends
        AbstractFProductionInputFCtrl<FProductionInputCompCBS, FProductionInputCompVBean> {

    private static final Logger LOGGER = Logger.getLogger(FProductionInputCompFCtrl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }
        super.buttonAction(event);

        if (event.getModel().getReference().equals(AbstractFProductionInputFCtrl.BTN_FPRODUCTION_OUTPUT_REFERENCE)) {
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL,
                    getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL));
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL, true);
            // If one the item of the activity has a Nature Complementary Workshop.
            // We force the redirection to an other screen (MOOUSCRAPDE1T), to manage the statement scrap.
            if (getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.MO_LIST_VBEAN) != null) {
                FMOListVBean vBean = (FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT,
                        FProductionConstant.MO_LIST_VBEAN);
                // NLE 06/01/2015 Q007682
                String outputFunction = "";
                if (vBean.isStatementScrap(ActivityItemType.OUTPUT)) {
                    outputFunction = "VIF.MOOUSCRAPDE1T"; // scrap output
                } else {
                    outputFunction = "VIF.MOOUCODE1T"; // classical output
                }
                getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.FEATURE_ID, outputFunction);
                fireSharedContextSend(getSharedContext());
                fireSelfFeatureClosingRequired();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        getViewerCtrl().getViewerView().getInputBarCode().addBarCodeActionListener(this);
        getViewerCtrl().setFeatureView(getView());
        getViewerCtrl().setFeatureCtrl(this);
        getViewerCtrl().setBrowserCtrl(getBrowserCtrl());
        setMoListVBean((FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.MO_LIST_VBEAN));
        if (getMoListVBean() != null) {
            // -------------------------------
            // Initialize browser selection
            // -------------------------------
            FProductionOutputCompSBean sBean = new FProductionOutputCompSBean();
            sBean.setActivityItemType(ActivityItemType.INPUT);
            sBean.setWorkstationId((String) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.LOGICAL_WORKSTATION));
            sBean.setAll(false);
            sBean.setMoKey(getMoListVBean().getMobean().getMoKey());

            // search the first item to select the good tab when opening the feature.
            int nbManufacturing = 0;
            int nbINProgress = 0;
            for (InputOperationItemBean operation : getMoListVBean().getListInputOperations()) {
                if (operation.getNatureList().isEmpty()
                        || operation.getNatureList().contains(MONatureComplementaryWorkshop.MANUFACTURING.getValue())
                        || !operation.getAutoQty()) {
                    nbManufacturing += 1;
                    break;
                }
                if (operation.getNatureList().contains(MONatureComplementaryWorkshop.INPROGRESS.getValue())) {
                    nbINProgress += 1;
                }
            }
            if (nbManufacturing > 0) {
                sBean.setState(MONatureComplementaryWorkshop.MANUFACTURING.getValue());
            } else if (nbINProgress > 0) {
                sBean.setState(MONatureComplementaryWorkshop.INPROGRESS.getValue());
            }

            // -------------------------------------------
            // Get workBean and initialize scale component
            // --------------------------------------------
            try {
                getViewerCtrl()
                        .setWorkBean(getFproductionInputContainerCBS().getWorkBean(getIdCtx(), getMoListVBean())); // Q006695
                // ALB 18/09/2013 bad constructor - MoBean replace with moLIstVbean
                getViewerCtrl().getWorkBean().setMoKey(getMoListVBean().getMobean().getMoKey());
                getViewerCtrl().getWorkBean().setRealWorkstationId(getIdCtx().getWorkstation());
                getViewerCtrl().getWorkBean().setLogicalWorkstationId(
                        (String) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.LOGICAL_WORKSTATION));
                getViewerCtrl().getWorkBean().setOutputAvailable(
                        getMoListVBean().getListOutputOperations() != null
                                && getMoListVBean().getListOutputOperations().size() > 0);

                setScale(getViewerCtrl().getWorkBean().getScale());
                if (getScale() != null && !getScale().getFDDIP().isEmpty()) {
                    getFeatureView().getScaleView().getController()
                            .initializeFDD(getScale(), getIdCtx().getWorkstation());
                    // getFeatureView().getScaleView().getController().initializeJmsAction(scale.getFDDIP(),
                    // scale.getFDDPort(), getIdCtx().getWorkstation(), scale.getIdDevice());
                    getFeatureView().getScaleView().getController().startJmsAction();
                }

            } catch (BusinessException e) {
                getViewerCtrl().fireErrorsChanged(new UIException(e));
            } catch (UIException e) {
                getViewerCtrl().fireErrorsChanged(e);
            }

            // ------------------------
            // Set Subtitle feature
            // ------------------------
            getFeatureView().setSubTitle(
                    I18nClientManager.translate(ProductionMo.T29499, false)
                            + " - "
                            + I18nClientManager.translate(ProductionMo.T29500, false, new VarParamTranslation(
                                    getMoListVBean().getMobean().getMoKey().getChrono().getChrono())) + " - "
                            + getMoListVBean().getMobean().getLabel());

            // Put the selection Bean in FProductionOutputCompGSModel
            getGeneralSelectionController().getModel().setBean(sBean);
            getBrowserController().setCurrentSelection(sBean);
            getBrowserController().setInitialSelection(sBean);
        }
        super.initialize();
        setErrorPanelAlwaysVisible(true);
        manageBtnFlash();
        fireBtnStdToolBarStateChangedEvent();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void openChildFeature(final String featureId) {
        if (featureId.equals(FDETAIL_ENTRY_CODE)) {
            getSharedContext().put(
                    Domain.DOMAIN_THIS,
                    FDataEntryListCompFModel.CONTEXT_NATURE,
                    ((FProductionInputCompVBean) getViewerController().getBean()).getBBean()
                    .getNatureComplementaryWorkshop());
        }

        super.openChildFeature(featureId);
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputColumnsFactory.java,v $
 * Created on 12 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist;


import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBeanEnum;


/**
 * Construct a Browser Model for the movements list from the nature movement type.
 * 
 * @author xg
 */
public abstract class FBoningDataEntryListInputColumnsFactory {

    private static final Format FORMAT = new Format(Alignment.RIGHT_ALIGN);

    /**
     * Constructor.
     */
    private FBoningDataEntryListInputColumnsFactory() {

    }

    /**
     * Get the browser model to use.
     * 
     * @param type type of the movement.
     * @return a browser model.
     */
    public static List<BrowserColumn> getColumns(final InputEntityType type) {

        List<BrowserColumn> ret = new ArrayList<BrowserColumn>();
        ret.add(getRank());
        if (type != null) {
            if (InputEntityType.BATCH_ITEM.equals(type)) {
                ret.add(getCreationDateTime());
                ret.add(getBatch());
                ret.add(getItem());
                ret.add(getFirstUnit());
                ret.add(getSecondUnit());
            } else if (InputEntityType.CONTAINER.equals(type)) {
                ret.add(getCreationHour());
                ret.add(getContainer());
                ret.add(getItem());
                ret.add(getBatch());
                ret.add(getFirstUnit());
                ret.add(getSecondUnit());
            }
        }
        return ret;
    }

    /**
     * Get the browser model to use.
     * 
     * @param type type of the movement.
     * @return a browser model.
     */
    public static List<BrowserColumn> getColumns(final OutputEntityType type) {

        List<BrowserColumn> ret = new ArrayList<BrowserColumn>();
        ret.add(getRank());
        if (type != null) {
            if (OutputEntityType.BATCH_ITEM.equals(type)) {
                ret.add(getCreationDateTime());
                ret.add(getItem());
                ret.add(getBatch());
                ret.add(getFirstUnit());
                ret.add(getSecondUnit());
            } else if (OutputEntityType.CONTAINER.equals(type)) {
                ret.add(getCreationHour());
                ret.add(getContainer());
                ret.add(getItem());
                ret.add(getBatch());
                ret.add(getFirstUnit());
                ret.add(getSecondUnit());
            } else if (OutputEntityType.CONTAINER_UPPER_CONTAINER.equals(type)) {
                ret.add(getContainer());
                ret.add(getUpperContainer());
                ret.add(getItem());
                ret.add(getBatch());
                ret.add(getFirstUnit());
                ret.add(getSecondUnit());
            } else if (OutputEntityType.EU.equals(type)) {
                ret.add(getContainer());
                ret.add(getNSP());
                ret.add(getItem());
                ret.add(getBatch());
                ret.add(getFirstUnit());
                ret.add(getSecondUnit());
            }

        }
        return ret;
    }

    /* CHECKSTYLE:OFF */
    private static BrowserColumn getBatch() {
        return new BrowserColumn(I18nClientManager.translate(StockKernel.T29224),
                FBoningDataEntryListInputBBeanEnum.BATCH.getValue(), 95);
    }

    private static BrowserColumn getContainer() {
        return new BrowserColumn(I18nClientManager.translate(StockKernel.T29089),
                FBoningDataEntryListInputBBeanEnum.CONTAINER_NUMBER.getValue(), 100);
    }

    private static BrowserColumn getCreationDateTime() {
        return new BrowserColumn(I18nClientManager.translate(StockKernel.T29772),
                FBoningDataEntryListInputBBeanEnum.FORMATTED_CREATION_DATE_TIME.getValue(), 105);
    }

    private static BrowserColumn getCreationHour() {
        return new BrowserColumn(I18nClientManager.translate(StockKernel.T29772),
                FBoningDataEntryListInputBBeanEnum.FORMATTED_CREATION_HOUR.getValue(), 50);
    }

    private static BrowserColumn getFirstUnit() {
        return new BrowserColumn(I18nClientManager.translate(GenKernel.T210) + " 1",
                FBoningDataEntryListInputBBeanEnum.FORMATTED_FIRST_QTY_UNIT.getValue(), 95, FORMAT);
    }

    private static BrowserColumn getItem() {
        return new BrowserColumn(I18nClientManager.translate(GenKernel.T426),
                FBoningDataEntryListInputBBeanEnum.ITEM_ID.getValue(), 100);
    }

    private static BrowserColumn getNSP() {
        return new BrowserColumn(I18nClientManager.translate(ProductionMo.T17191),
                FBoningDataEntryListInputBBeanEnum.NSP.getValue(), 115);
    }

    private static BrowserColumn getRank() {
        return new BrowserColumn(I18nClientManager.translate(StockKernel.T29533),
                FBoningDataEntryListInputBBeanEnum.LINE_NUMBER.getValue(), 30);
    }

    private static BrowserColumn getSecondUnit() {
        return new BrowserColumn(I18nClientManager.translate(GenKernel.T210) + " 2",
                FBoningDataEntryListInputBBeanEnum.FORMATTED_SECOND_QTY_UNIT.getValue(), 95, FORMAT);
    }

    private static BrowserColumn getUpperContainer() {
        return new BrowserColumn(I18nClientManager.translate(StockKernel.T29089),
                FBoningDataEntryListInputBBeanEnum.UPPER_CONTAINER_NUMBER.getValue(), 100);
    }

    /* CHECKSTYLE:ON */
}

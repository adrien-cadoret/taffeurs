/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationItemBModel.java,v $
 * Created on 19 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.touch.TouchBrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.FontNames;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;


/**
 * Model for browser on items for MO creation.
 * 
 * @author glc
 */
public class FMOCreationItemBModel extends TouchBrowserModel<CodeLabel> {

    private static Format codeFormat;
    private static Format labelFormat;

    static {
        codeFormat = new Format(Alignment.CENTER_ALIGN, StandardColor.BG_DEFAULT,
                StandardColor.TOUCHSCREEN_FG_BROWSER_CODE, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
                        FontStyle.BOLD, 14));
        labelFormat = new Format(Alignment.CENTER_ALIGN, StandardColor.BG_DEFAULT,
                StandardColor.TOUCHSCREEN_FG_BROWSER_LABEL, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
                        FontStyle.BOLD, 12));
    }

    /**
     * Simple constructor.
     */
    public FMOCreationItemBModel() {
        super();
        setBeanClass(CodeLabel.class);
        addColumn(new BrowserColumn("", "code", 15));
        addColumn(new BrowserColumn("", "label", 30));
        setSelectRowOnInitAndChanged(false);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent, final CodeLabel bean) {
        Format f = null;
        if ("code".equals(property)) {
            f = codeFormat;
        } else {
            f = labelFormat;
        }
        return f;
    }

}

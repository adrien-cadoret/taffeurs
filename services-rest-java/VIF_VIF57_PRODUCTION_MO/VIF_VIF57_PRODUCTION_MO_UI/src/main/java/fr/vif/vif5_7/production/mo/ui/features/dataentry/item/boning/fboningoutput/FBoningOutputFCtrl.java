/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputFCtrl.java,v $
 * Created on 25 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.StringHelper;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.constant.EntityConstants;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.keys.ContainerKey;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.dialog.number.NumericDBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputFinishBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.StackingPlanInfoBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutputcontainer.FBoningOutputVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutputcontainer.FBoningOutputWorkBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.flabels.FLabelBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.fboning.FBoningCBS;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.fboningoutput.FBoningOutputCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.dialog.number.DNumberKeyboardDCtrl;
import fr.vif.vif5_7.production.mo.ui.dialog.number.DWeightKeyboardDCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.flabels.FLabelFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFCtrl;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;
import fr.vif.vif5_7.stock.kernel.business.beans.features.quality.nc.fncflow.FNCFlowSharedContextBean;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCConstants;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCActType;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCType;
import fr.vif.vif5_7.stock.kernel.ui.util.StockUITools;
import fr.vif.vif5_7.workshop.device.business.beans.common.devices.ScaleDevice;
import fr.vif.vif5_7.workshop.device.business.beans.common.weighting.SequentialWeighting;


/**
 * ProductionOutputContainer : Feature Controller.
 * 
 * @author vr
 */
public class FBoningOutputFCtrl extends AbstractFBoningOutputFCtrl implements DialogChangeListener {

    /**
     * Production button reference.
     */
    private static final String FDETAIL_ENTRY_CODE = "VIF.BODETIMVT";
    private static final Logger LOGGER             = Logger.getLogger(FBoningOutputFCtrl.class);
    private boolean             flagWeightMode     = false;
    private OperationItemKey    current;

    private FBoningOutputCBS    fboningOutputContainerCBS;

    private FBoningCBS          fboningCBS;

    private FMOListVBean        moListVBean        = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        super.buttonAction(event);

        if (BTN_FINISH_REFERENCE.equals(event.getModel().getReference())) {
            processFinishResume();

        } else if (BTN_FINISH_ALL_REFERENCE.equals(event.getModel().getReference())) {
            processFinishAll(true);

        } else if (BTN_OPEN_NUMBER_REFERENCE.equals(event.getModel().getReference())) {
            NumericDBean numericDBean = new NumericDBean();
            numericDBean.setInner(new QuantityUnit(0, getBrowserCtrl().getModel().getCurrentBean()
                    .getStackingPlanInfoBean().getUnit()));
            DNumberKeyboardDCtrl ctrl = new DNumberKeyboardDCtrl();
            ctrl.showKeyboard(getView(), this, this, getModel().getIdentification(), numericDBean);
        } else if (FBoningOutputFCtrl.BTN_SHOW_ALL_REFERENCE.equals(event.getModel().getReference())) {
            processShowAllEvent();
        } else if (BTN_DETAIL_REFERENCE.equals(event.getModel().getReference())) {
            getViewerCtrl().setPrevious(null); // Maybe delete done in movement list
            getViewerCtrl().getMap().remove(
                    FProductionTools.operationItemKeyToMovementAct(getViewerCtrl().getBean().getBBean()
                            .getOperationItemBean().getOperationItemKey()));
        } else if (BTN_WEIGHT_MODE_REFERENCE.equals(event.getModel().getReference())) {
            flagWeightMode = !flagWeightMode;
            ((ToolBarBtnStdModel) event.getModel()).setSelected(flagWeightMode);
            if (flagWeightMode) {
                ((ToolBarBtnStdModel) event.getModel())
                        .setText(I18nClientManager.translate(ProductionMo.T34209, false));
            } else {
                ((ToolBarBtnStdModel) event.getModel())
                        .setText(I18nClientManager.translate(ProductionMo.T34222, false));
            }
            getViewerCtrl().disabledBtnForWeightMode(flagWeightMode);
        } else if (BTN_WEIGHT_REFERENCE.equals(event.getModel().getReference())) {
            if (getViewerCtrl().getWorkBean().getScale() != null) {
                processWeigth();
            } else {
                processWeigthManual();
            }

        } else if (BTN_CLOSE_CONTAINER.equals(event.getModel().getReference())) {
            try {
                if (!getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                        .getContainerNumber().isEmpty()
                        && !getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .getContainerNumber().equals(EntityConstants.NEW)) {
                    List<PrintDocument> lst = getFboningOutputContainerCBS().closePrintContainer(getIdCtx(),
                            getViewerCtrl().getBean(), getViewerCtrl().getWorkBean());
                    if (lst != null && lst.size() > 0) {
                        getViewerCtrl().askNumDocument(lst);

                        getFboningOutputContainerCBS().printContainer(getIdCtx(), getViewerCtrl().getBean(), lst,
                                getViewerCtrl().getWorkBean());
                    }
                    // QA8912 MP : reset location only if depot not optimize and the activity has not location
                    if (!getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCemp()
                            .isEmpty()
                            && (getViewerCtrl().getBean().getHasDepositWithLocationOptimise() || getViewerCtrl()
                                    .getBean().getBBean().getOperationItemBean().getLocationCL().getCode().isEmpty())) {
                        getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEnableFields()
                                .setLocation(true);
                        getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .setCemp("");
                    }
                    getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                            .setContainerNumber(EntityConstants.NEW);
                } else if (!getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                        .getUpperContainerNumber().isEmpty()
                        && !getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .getUpperContainerNumber().equals(EntityConstants.NEW)) {
                    List<PrintDocument> lst = getFboningOutputContainerCBS().closePrintUpperContainer(getIdCtx(),
                            getViewerCtrl().getBean(), getViewerCtrl().getWorkBean());
                    if (lst != null && lst.size() > 0) {
                        getViewerCtrl().askNumDocument(lst);
                        getFboningOutputContainerCBS().printUpperContainer(getIdCtx(), getViewerCtrl().getBean(), lst,
                                getViewerCtrl().getWorkBean());
                    }
                    // QA8912 MP : reset location only if depot not optimize and the activity has not location
                    if (!getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCemp()
                            .isEmpty()
                            && (getViewerCtrl().getBean().getHasDepositWithLocationOptimise() || getViewerCtrl()
                                    .getBean().getBBean().getOperationItemBean().getLocationCL().getCode().isEmpty())) {
                        getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEnableFields()
                                .setLocation(true);
                        getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .setCemp("");
                    }
                    getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                            .setUpperContainerNumber(EntityConstants.NEW);
                }

                getViewerCtrl().getModel().setInitialBean(getViewerCtrl().getBean());
                getViewerCtrl().renderAndEnableComponents();
                getViewerCtrl().saveCurrent();
                getViewerCtrl().fireButtonStatechanged();
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
        } else if (BTN_CANCELLAST_REFERENCE.equals(event.getModel().getReference())) {
            if (getViewerCtrl().getPrevious() != null) {
                int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                        I18nClientManager.translate(ProductionMo.T29578, false), MessageButtons.YES_NO,
                        MessageButtons.DEFAULT_NO);
                if (ret == MessageButtons.YES) {
                    try {

                        OperationItemKey oik = new OperationItemKey();
                        oik.setEstablishmentKey(getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey());
                        oik.setChrono(getViewerCtrl().getWorkBean().getMoKey().getChrono());
                        oik.setCounter1(getViewerCtrl().getPrevious().getNumber1());
                        oik.setCounter2(getViewerCtrl().getPrevious().getNumber2());
                        oik.setCounter3(getViewerCtrl().getPrevious().getNumber3());
                        oik.setCounter4(getViewerCtrl().getPrevious().getNumber4());
                        oik.setManagementType(ManagementType.REAL);
                        getFboningOutputContainerCBS().deleteLast(getIdCtx(),
                                getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey(),
                                getViewerCtrl().getPrevious());
                        getViewerCtrl().getPrevious().setLineNumber(0);
                        getViewerCtrl().getMap().remove(getViewerCtrl().getPrevious());

                        // Need to disabled auto weight mode during reopenquery because it change the selected row.
                        boolean flagWeightModeTMP = flagWeightMode;
                        flagWeightMode = false;
                        getBrowserCtrl().reopenQuery();

                        getBrowserCtrl().repositionToOperationItemKey(oik);
                        getViewerCtrl().setPrevious(null);
                        flagWeightMode = flagWeightModeTMP;
                        getViewerCtrl().fireButtonStatechanged();

                    } catch (BusinessException e) {
                        getView().show(new UIException(e));
                    } catch (BusinessErrorsException e) {
                        getViewerCtrl().fireErrorsChanged(new UIErrorsException(e));
                    }
                }
            }
        } else if (BTN_PDF_REFERENCE.equalsIgnoreCase(event.getModel().getReference())) {
            try {

                List<PDFFile> listPdfFiles = getDocumentsToShow();
                if (listPdfFiles.isEmpty()) {
                    getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                            I18nClientManager.translate(ProductionMo.T30839));
                } else if (listPdfFiles.size() == 1) {
                    viewDocumentRecette(listPdfFiles.get(0));
                } else {
                    viewDocumentList(listPdfFiles);
                }

            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void childFeatureClosed(final FeatureController childFeatureController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }

        super.childFeatureClosed(childFeatureController);

        if (childFeatureController instanceof FLabelFCtrl) {
            FLabelBBean labelBean = (FLabelBBean) getSharedContext().get(Domain.DOMAIN_CHILD,
                    FLabelFCtrl.SELECTED_LABEL);
            if (labelBean != null) {
                List<FLabelBBean> newList = new ArrayList<FLabelBBean>();
                for (FLabelBBean currentLabel : getViewerCtrl().getWorkBean().getCurrentLabelList()) {
                    // replace the current label if it is the same type.
                    if (currentLabel != null && !labelBean.getType().equals(currentLabel.getType())) {
                        newList.add(currentLabel);
                    }
                }

                newList.add(labelBean);
                getViewerCtrl().getWorkBean().setCurrentLabelList(newList);
            }
        } else {
            getBrowserCtrl().reopenQuery();
            // Reposition to current bean
            if (current != null) {
                getBrowserCtrl().repositionToOperationItemKey(current);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closingRequired(final ClosingEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - closingRequired(event=" + event + ")");
        }

        super.closingRequired(event);

        if (getViewerCtrl().getWorkBean().getStackingType() == 0) {
            try {
                if (getViewerCtrl().getWorkBean() != null && getViewerCtrl().getWorkBean().getMoKey() != null
                        && getViewerCtrl().getWorkBean().getMoKey().getChrono() != null) {
                    getFboningOutputContainerCBS().closePrintContainersOnExit(getIdCtx(), getViewerCtrl().getBean(),
                            getViewerCtrl().getWorkBean());
                }
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
        }

        // ---------------------------------------------------------------------
        // if the workstation has a scale --> stop it before closing the feature
        // ---------------------------------------------------------------------
        if (getViewerCtrl().getWorkBean().getScale() != null
                && !getViewerCtrl().getWorkBean().getScale().getFDDIP().isEmpty()) {
            getFeatureView().getScaleView().getController().stopJmsAction();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - closingRequired(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogCancelled(event=" + event + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogCancelled(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }

        QuantityUnit qty = ((NumericDBean) event.getDialogBean()).getInner();

        if (event.getSource().getClass().equals(DWeightKeyboardDCtrl.class)) {
            FCommScaleResult result = new FCommScaleResult(1, qty.getQty(), qty.getQty(), 0);
            getViewerCtrl().setWeightResult(result);
        } else {
            FBoningOutputBBean currentBBean = getBrowserCtrl().getModel().getCurrentBean();
            try {
                StackingPlanInfoBean stackingPlanInfo = getFboningOutputContainerCBS().getStackinPlanDetailForQty(
                        getIdCtx(), getViewerCtrl().getWorkBean().getMoKey(),
                        currentBBean.getStackingPlanInfoBean().getStackingPlanCL().getCode(),
                        currentBBean.getItemCLs().getCode(), qty.getQty());
                if (stackingPlanInfo != null && stackingPlanInfo.equalsKey(currentBBean.getStackingPlanInfoBean())) {
                    QtyUnit innerInSelectedItem = new QtyUnit(qty.getQty(), currentBBean.getStackingPlanInfoBean()
                            .getUnit());

                    currentBBean.setInnerStr(stackingPlanInfo.getQtyStr());
                    getViewerCtrl().getBean().setBBean(currentBBean);
                    getViewerCtrl().getBean().getEntity().getStockItemBean().getKeyboardQties()
                    .setFirstQty(innerInSelectedItem);
                    getViewerCtrl().getBean().getBBean().setStackingPlanInfoBean(stackingPlanInfo);
                    getViewerCtrl().getBean().getBBean().getStackingPlanInfoBean()
                    .setInner((int) innerInSelectedItem.getQty());
                    getViewerCtrl().getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                            .setContainerNumber(stackingPlanInfo.getContainerNumber());
                    getViewerCtrl().getBean().getEntity().getEntityLocationBean().getContainerPackaging()
                            .setPackagingId(stackingPlanInfo.getPackagingId());
                    getViewerCtrl().saveCurrent();
                    getBrowserCtrl().getView().initializeView(getBrowserCtrl());
                } else if (stackingPlanInfo == null) {
                    // no packaging for this qty
                    getView().showError(
                            I18nClientManager.translate(ProductionMo.T37917, false, new VarParamTranslation(
                                    currentBBean.getStackingPlanInfoBean().getStackingPlanCL().getCode())));
                } else {
                    // the user should ends the open packaging first
                    String maxQty = Integer
                            .toString((int) (currentBBean.getStackingPlanInfoBean().getQtyToDo() - currentBBean
                                    .getStackingPlanInfoBean().getQtyDone()))
                            + currentBBean.getStackingPlanInfoBean().getUnit();
                    getView().showError(
                            I18nClientManager.translate(ProductionMo.T38715, false, new VarParamTranslation(maxQty)));
                }
            } catch (BusinessException e) {
                LOGGER.error("", e);
                getView().show(new UIException(e));
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    /**
     * Gets the fboningCBS.
     * 
     * @return the fboningCBS.
     */
    public FBoningCBS getFboningCBS() {
        return fboningCBS;
    }

    /**
     * Gets the fboningOutputContainerCBS.
     * 
     * @return the fboningOutputContainerCBS.
     */
    public FBoningOutputCBS getFboningOutputContainerCBS() {
        return fboningOutputContainerCBS;
    }

    /**
     * Gets the moListVBean.
     * 
     * @return the moListVBean.
     */
    public FMOListVBean getMoListVBean() {
        return moListVBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        getViewerCtrl().setFeatureView(getView());
        getViewerCtrl().setFeatureCtrl(this);
        getViewerCtrl().setBrowserCtrl(getBrowserCtrl());
        getBrowserCtrl().setViewerCtrl(getViewerCtrl());
        getBrowserController().addTableRowChangeListener(getViewerCtrl());
        FBoningOutputSBean sBean = null;
        moListVBean = (FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.MO_LIST_VBEAN);
        if (moListVBean != null) {

            // -------------------------------------------
            // Get workBean and initialize scale component
            // --------------------------------------------
            try {
                FBoningOutputWorkBean fboningOutputWorkBean = getFboningOutputContainerCBS().getWorkBean(getIdCtx(),
                        moListVBean.getMobean(),
                        (String) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.LOGICAL_WORKSTATION));
                getFeatureView().setHierarchies(fboningOutputWorkBean.getHierarchies());

                getViewerCtrl().setWorkBean(fboningOutputWorkBean);
                getViewerCtrl().getWorkBean().setMoKey(moListVBean.getMobean().getMoKey());
                getViewerCtrl().getWorkBean().setRealWorkstationId(getIdCtx().getWorkstation());
                getViewerCtrl().getWorkBean().setLogicalWorkstationId(
                        (String) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.LOGICAL_WORKSTATION));
                getViewerCtrl().getWorkBean()
                        .setInputAvailable(
                                moListVBean.getListInputOperations() != null
                                        && moListVBean.getListInputOperations().size() > 0);

                ScaleDevice scale = getViewerCtrl().getWorkBean().getScale();
                if (scale != null && !scale.getFDDIP().isEmpty()) {
                    getFeatureView().getScaleView().getController().initializeFDD(scale, getIdCtx().getWorkstation());
                    getFeatureView().getScaleView().getController().startJmsAction();
                } else {
                    getView().showError(
                            I18nClientManager.translate(ProductionMo.T4653, false, new VarParamTranslation(getIdCtx()
                                    .getWorkstation())));
                }

            } catch (BusinessException e) {
                getView().show(new UIException(e));
            } catch (UIException e) {
                getView().show(e);
            }

            // -------------------------------
            // Initialize browser selection
            // -------------------------------
            sBean = new FBoningOutputSBean();
            sBean.setActivityItemType(ActivityItemType.OUTPUT);
            sBean.setWorkstationId((String) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.LOGICAL_WORKSTATION));
            sBean.setAll(false);
            sBean.setMoKey(moListVBean.getMobean().getMoKey());

            // ------------------------
            // Set Subtitle feature
            // ------------------------

            StringBuilder sb = new StringBuilder();
            sb.append(I18nClientManager.translate(ProductionMo.T29500, false, new VarParamTranslation(moListVBean
                    .getMobean().getMoKey().getChrono().getChrono())));

            String labelOF = moListVBean.getMobean().getLabel();
            if (!labelOF.isEmpty()) {
                sb.append(" - ").append(labelOF);
            }

            String customer = getViewerCtrl().getWorkBean().getCustomer();
            if (customer != null && !customer.isEmpty()) {
                sb.append(" - ").append(customer);
            }

            getFeatureView().setSubTitle(sb.toString());

        }
        getGeneralSelectionController().getModel().setBean(sBean);
        getBrowserController().setCurrentSelection(sBean);
        getBrowserController().setInitialSelection(sBean);
        super.initialize();
        setErrorPanelAlwaysVisible(true);

        if (getViewerCtrl().getWorkBean().getScale() != null
                && !getViewerCtrl().getWorkBean().getScale().getFDDIP().isEmpty()
                && getBrowserController().getModel().getBeans().size() > 0) {
            activateFlagWeightMode();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Determine if the weight mode is on (return true), or off (return false).
     * 
     * @return a boolean value
     */
    public boolean isWeightModeOn() {
        return flagWeightMode;
    }

    /**
     * Process finish.
     * 
     * @param operationItemKey the operation item key to finish
     * @param packagingId the packaging id
     * @param showQuestion should we show the question for the tattooed number (only true for the second times when we
     *            already ask the tattooed number).
     * @param repositionOnItem should we reposition on the updated item at the end ?
     */
    public void processFinish(final OperationItemKey operationItemKey, final String packagingId,
            final boolean showQuestion, final boolean repositionOnItem) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processFinish()");
        }

        try {
            boolean stop = false;

            // tattoo number
            FBoningOutputFinishBean finishBean = getFboningOutputContainerCBS().finishOperationItem(getIdCtx(),
                    operationItemKey, getIdCtx().getWorkstation(), ActivityItemType.OUTPUT, packagingId,
                    getViewerCtrl().getWorkBean().getStackingType());

            if (finishBean.getContainerTattoed().size() > 0) {
                if (showQuestion) {
                    int result = getView().showQuestion(
                            I18nClientManager.translate(Jtech.T25073, false),
                            I18nClientManager.translate(ProductionMo.T20929, false) + ". "
                                    + I18nClientManager.translate(ProductionMo.T29570, false), MessageButtons.YES_NO);
                    if (result == MessageButtons.NO) {
                        stop = true;
                    }
                }
                if (!stop) {
                    for (CEntityBean nsc : finishBean.getContainerTattoed().keySet()) {
                        getViewerCtrl().showPackaging(nsc, finishBean.getContainerTattoed().get(nsc));
                    }
                    processFinish(operationItemKey, packagingId, true, true);
                }
            } else {
                getViewerCtrl().getMap().remove(
                        FProductionTools.operationItemKeyToMovementAct(getViewerCtrl().getBean().getBBean()
                                .getOperationItemBean().getOperationItemKey()));
                current = getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey();
                current.setManagementType(ManagementType.ARCHIVED);
                if (getBrowserCtrl().getModel().getBeans().size() == 1
                        || getViewerCtrl().getBean().getBBean().equals(getBrowserCtrl().getModel().getBeans().get(0))) {
                    // close the feature if there is no item to show
                    fireSelfFeatureClosingRequired();
                } else if (repositionOnItem) {
                    getBrowserController().reopenQuery();
                    if (current != null) {
                        getBrowserCtrl().repositionToOperationItemKey(current);
                    }
                }
            }
        } catch (BusinessException e) {
            getViewerCtrl().fireErrorsChanged(new UIException(e));
        } catch (UIException e) {
            showError(e.getMessage());
            LOGGER.error("", e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processFinish()");
        }
    }

    /**
     * Launch the weight process.
     */
    public void processWeigth() {
        FCommScaleResult result = null;
        try {
            SequentialWeighting sequentialWeighingInfo = new SequentialWeighting();
            sequentialWeighingInfo.setEstablishmentKey(getViewerCtrl().getBean().getBBean().getOperationItemBean()
                    .getOperationItemKey().getEstablishmentKey());
            sequentialWeighingInfo.setUserId(getIdCtx().getLogin());
            sequentialWeighingInfo.setWeightKey(getWeightKey(getViewerCtrl().getBean().getBBean()
                    .getOperationItemBean().getOperationItemKey()));
            sequentialWeighingInfo.setWeightKeyType("XPILOIA");
            sequentialWeighingInfo.setWorkStationId(getViewerCtrl().getWorkBean().getLogicalWorkstationId());
            result = getFeatureView().getScaleView().getController()
                    .getLockAndSaveResult(getIdCtx(), sequentialWeighingInfo);

            getViewerCtrl().setWeightResult(result);
        } catch (UIException e) {
            getViewerCtrl().fireErrorsChanged(e);
            processWeigthManual();
        }
    }

    /**
     * Manual weight method : open a keyboard bean.
     */
    public void processWeigthManual() {
        NumericDBean numericDBean = new NumericDBean();
        numericDBean.setInner(new QuantityUnit(0, getViewerCtrl().getBean().getBBean().getItemRefUnit()));
        DWeightKeyboardDCtrl ctrl = new DWeightKeyboardDCtrl();
        ctrl.showKeyboard(getView(), this, this, getModel().getIdentification(), numericDBean);
    }

    /**
     * Sets the fboningCBS.
     * 
     * @param fboningCBS fboningCBS.
     */
    public void setFboningCBS(final FBoningCBS fboningCBS) {
        this.fboningCBS = fboningCBS;
    }

    /**
     * Sets the fboningOutputContainerCBS.
     * 
     * @param fboningOutputContainerCBS fboningOutputContainerCBS.
     */
    public void setFboningOutputContainerCBS(final FBoningOutputCBS fboningOutputContainerCBS) {
        this.fboningOutputContainerCBS = fboningOutputContainerCBS;
    }

    /**
     * Sets the flagWeightMode.
     * 
     * @param flagWeightMode flagWeightMode.
     */
    public void setFlagWeightMode(final boolean flagWeightMode) {
        this.flagWeightMode = flagWeightMode;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void actionOpenNewIncident() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionOpenNewIncident()");
        }

        // Prepare parameters bean
        FNCFlowSharedContextBean parametersBean = new FNCFlowSharedContextBean();
        parametersBean.setType(NCType.INTERNAL.getKey());

        // Feed with viewer bean informations
        FBoningOutputVBean vBean = (FBoningOutputVBean) getViewerController().getBean();
        parametersBean.setActType(NCActType.MANUFACTURING_ORDER.getKey());
        parametersBean.setChronoOrigin(vBean.getBBean().getOperationItemBean().getOperationItemKey().getChrono());
        parametersBean.setContainerNumber(vBean.getEntity().getEntityLocationBean().getEntityLocationKey()
                .getContainerNumber());
        StockItem stockItem = new StockItem();
        stockItem.setItemId(vBean.getEntity().getStockItemBean().getStockItem().getItemId());
        stockItem.setBatch(vBean.getEntity().getStockItemBean().getStockItem().getBatch());
        stockItem.setQuantities(vBean.getEntity().getStockItemBean().getKeyboardQties());
        parametersBean.setStockItem(stockItem);

        // Put parameters in context and open feature
        getSharedContext().put(Domain.DOMAIN_THIS, NCConstants.NCFLOW_KEY_SHARED_CONTEXT, parametersBean);
        if (StockUITools.isTouchRunning()) {
            openChildFeature(NCConstants.FeatureName.FLOW_TOUCH.getName());
        } else {
            openChildFeature(NCConstants.FeatureName.FLOW.getName());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionOpenNewIncident()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = super.getActionsButtonModels();
        lst.add(getBtnShowAllModel());
        lst.add(getBtnFinishResumeModel());
        lst.add(getBtnFinishResumeAllModel());
        lst.add(getBtnDetailModel());
        lst.add(getBtnNumberModel());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lst;
    }

    /**
     * Get the browser controller.
     * 
     * @return the browser controller
     */
    protected FBoningOutputBCtrl getBrowserCtrl() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBrowserCtrl()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBrowserCtrl()");
        }
        return (FBoningOutputBCtrl) getBrowserController();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ToolBarBtnFeatureModel getBtnDetailModel() {
        ToolBarBtnFeatureModel btn = super.getBtnDetailModel();
        btn.setFeatureId(FDETAIL_ENTRY_CODE);
        return btn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected ToolBarBtnStdModel getBtnFinishResumeModel() {
        ToolBarBtnStdModel btn = super.getBtnFinishResumeModel();
        btn.setFunctionality(true);
        return btn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getExternalButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getExternalButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = super.getExternalButtonModels();
        lst.add(getBtnLabelModel());
        lst.add(getBtnCancelLastModel());
        lst.add(getBtnCloseContainerModel());
        lst.add(getBtnWeightModeModel());
        lst.add(getBtnWeightModel());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getExternalButtonModels()");
        }
        return lst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void openChildFeature(final String featureId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openChildFeature(featureId=" + featureId + ")");
        }

        if (featureId.equals("VIF.LABLST")) {

            getSharedContext().put(Domain.DOMAIN_THIS, FLabelFCtrl.WORKSTATION_ID, getIdCtx().getWorkstation());
            getSharedContext().put(Domain.DOMAIN_THIS, FLabelFCtrl.MO_KEY, getViewerCtrl().getWorkBean().getMoKey());

            fireSharedContextSend(getSharedContext());

        } else if (featureId.equals(FDETAIL_ENTRY_CODE)) {
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.FEATURE_TITLE, getModel().getTitle());
            getSharedContext().put(Domain.DOMAIN_THIS, FDataEntryListFCtrl.OPERATION_ITEM_BEAN,
                    getViewerCtrl().getBean().getBBean().getOperationItemBean());
            getSharedContext().put(Domain.DOMAIN_THIS, "outputEntityType",
                    getViewerCtrl().getBean().getOutputItemParameters().getEntityType());
            fireSharedContextSend(getSharedContext());
            current = getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey();
        }
        super.openChildFeature(featureId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openChildFeature(featureId=" + featureId + ")");
        }
    }

    /**
     * Activate 'flagWeightMode'.
     */
    private void activateFlagWeightMode() {
        flagWeightMode = true;
        getBtnWeightModeModel().setSelected(flagWeightMode);
        if (flagWeightMode) {
            getBtnWeightModeModel().setText(I18nClientManager.translate(ProductionMo.T34209, false));
        } else {
            getBtnWeightModeModel().setText(I18nClientManager.translate(ProductionMo.T34222, false));
        }
        getViewerCtrl().disabledBtnForWeightMode(flagWeightMode);
    }

    /**
     * Determine if this item can be finished.
     * 
     * @param bean bean
     * @return true if this item can be finished
     * @throws BusinessException in error case
     */
    private boolean determineIfItemCanBeFinished(final FBoningOutputBBean bean) throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - determineIfItemCanBeFinished(bean=" + bean + ")");
        }

        boolean itemCanBeFinished;
        ContainerKey containerKey = new ContainerKey(getIdCtx().getCompany(), getIdCtx().getEstablishment(), null);

        QtyUnit neededQu = new QtyUnit(1.0, bean.getItemQttyToDoInRefUnitQU().getUnit());
        itemCanBeFinished = getFboningOutputContainerCBS().doesThisOutgoingItemCanBeFinished(getIdCtx(), containerKey,
                neededQu, getViewerCtrl().getWorkBean().getMoKey(), bean.getItemCLs().getCode());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - determineIfItemCanBeFinished(bean=" + bean + ")=" + itemCanBeFinished);
        }
        return itemCanBeFinished;
    }

    /**
     * Get documents to show.
     * 
     * @return the list of pdf files
     * @throws BusinessException if error occurs
     */
    private List<PDFFile> getDocumentsToShow() throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDocumentsToShow()");
        }

        MOItemKey moItemKey = new MOItemKey();

        moItemKey.setEstablishmentKey(getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getEstablishmentKey());
        moItemKey.setChrono(getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getChrono());
        moItemKey.setCounter1(getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getCounter1());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDocumentToShow()");
        }
        return getFboningOutputContainerCBS().getPdfFilesOfCurrentItem(getIdCtx(), moItemKey);
    }

    /**
     * Get the feature view.
     * 
     * @return the feature view
     */
    private FBoningOutputFIView getFeatureView() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getFeatureView()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getFeatureView()");
        }
        return (FBoningOutputFIView) getView();

    }

    /**
     * Gets the viewer controller.
     * 
     * @return The viewer Controller.
     */
    private FBoningOutputVCtrl getViewerCtrl() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getViewerCtrl()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getViewerCtrl()");
        }
        return (FBoningOutputVCtrl) getViewerController();

    }

    /**
     * Build the weight key to log the weight.
     * 
     * @param opKey the current item operation key
     * @return the key
     */
    private String getWeightKey(final OperationItemKey opKey) {
        return StringHelper.concat(opKey.getManagementType().getValue(), ",", //
                opKey.getEstablishmentKey().getCsoc(), ",", //
                opKey.getEstablishmentKey().getCetab(), ",", //
                opKey.getChrono().getPrechro(), ",", //
                opKey.getChrono().getChrono(), ",", //
                opKey.getCounter1(), ",", //
                opKey.getCounter2(), ",", //
                opKey.getCounter3(), ",", //
                opKey.getCounter4());
    }

    /**
     * Finishes all items.
     * 
     * @param showConfirmQuestion should we show the confirm question ?
     */
    private void processFinishAll(final boolean showConfirmQuestion) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processFinishAll()");
        }

        int result = 0;
        boolean stop = false;
        if (showConfirmQuestion) {
            result = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T34207, false), MessageButtons.YES_NO);
        } else {
            result = MessageButtons.YES;
        }
        if (result == MessageButtons.YES) {

            try {

                List<AbstractOperationItemBean> listOpBean = new ArrayList<AbstractOperationItemBean>();
                for (FBoningOutputBBean bbean : getBrowserCtrl().getModel().getBeans()) {
                    if (ManagementType.REAL.equals(bbean.getOperationItemBean().getOperationItemKey()
                            .getManagementType())) {
                        listOpBean.add(bbean.getOperationItemBean());
                    }
                }

                FBoningOutputFinishBean finishBean = getFboningOutputContainerCBS().finishAllOperationItem(
                        getIdCtx(),
                        listOpBean,
                        !showConfirmQuestion,
                        getIdCtx().getWorkstation(),
                        ActivityItemType.OUTPUT,
                        getViewerCtrl().getBean().getEntity().getEntityLocationBean().getContainerPackaging()
                                .getPackagingId(), getViewerCtrl().getWorkBean().getStackingType());

                if (!showConfirmQuestion && finishBean.getContainerTattoed().size() > 0) {
                    // the tattooed numbers are already be asked. Should we really continue ?
                    result = getView().showQuestion(
                            I18nClientManager.translate(Jtech.T25073, false),
                            I18nClientManager.translate(ProductionMo.T20929, false)
                                    + I18nClientManager.translate(ProductionMo.T29570, false), MessageButtons.YES_NO);
                    if (result == MessageButtons.NO) {
                        stop = true;
                    }
                }

                if (!stop && !finishBean.getAllItemsCanBeFinished()) {
                    // At least one stacking plan is not complete. Should we continue ?
                    result = getView().showQuestion(
                            I18nClientManager.translate(Jtech.T25073, false),
                            I18nClientManager.translate(ProductionMo.T34208, false) + ". "
                                    + I18nClientManager.translate(ProductionMo.T29570, false), MessageButtons.YES_NO);

                    if (result == MessageButtons.NO) {
                        stop = true;
                    } else if (result == MessageButtons.YES && finishBean.getContainerTattoed().size() == 0) {
                        // there is no tattooed number and we can force the end of the operations
                        finishBean = getFboningOutputContainerCBS().finishAllOperationItem(
                                getIdCtx(),
                                listOpBean,
                                true,
                                getIdCtx().getWorkstation(),
                                ActivityItemType.OUTPUT,
                                getViewerCtrl().getBean().getEntity().getEntityLocationBean().getContainerPackaging()
                                        .getPackagingId(), getViewerCtrl().getWorkBean().getStackingType());
                    }
                }

                if (!stop && finishBean.getContainerTattoed().size() > 0) {
                    for (CEntityBean nsc : finishBean.getContainerTattoed().keySet()) {
                        getViewerCtrl().showPackaging(nsc, finishBean.getContainerTattoed().get(nsc));
                    }
                    processFinishAll(false);
                }

                if (finishBean.getAllItemsCanBeFinished() && finishBean.getContainerTattoed().size() == 0) {
                    fireSelfFeatureClosingRequired();
                } else {
                    if (getBrowserCtrl().getModel().getCurrentBean() != null) {
                        current = getBrowserCtrl().getModel().getCurrentBean().getOperationItemBean()
                                .getOperationItemKey();
                    }
                    getBrowserController().reopenQuery();
                    if (current != null) {
                        getBrowserCtrl().repositionToOperationItemKey(current);
                    }
                }

            } catch (BusinessException e) {
                getViewerCtrl().fireErrorsChanged(new UIException(e));
            } catch (UIException e) {
                showError(e.getMessage());
                LOGGER.error("", e);
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processFinishAll()");
        }
    }

    /**
     * Finishes or unfinishes an item.
     */
    private void processFinishResume() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processFinishResume()");
        }

        int result = 0;
        if (getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                .equals(ManagementType.REAL)) {
            result = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29534, false), MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {

                try {
                    ContainerKey containerKey = null;
                    FBoningOutputVBean currentVbean = getViewerCtrl().getBean();

                    containerKey = new ContainerKey(currentVbean.getEntity().getEntityLocationBean()
                            .getEntityLocationKey().getCsoc(), currentVbean.getEntity().getEntityLocationBean()
                            .getEntityLocationKey().getCetab(), currentVbean.getEntity().getEntityLocationBean()
                            .getEntityLocationKey().getContainerNumber());

                    boolean canBeFinished = getFboningOutputContainerCBS().doesThisOutgoingItemCanBeFinished(
                            getIdCtx(), containerKey,
                            currentVbean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty(),
                            getViewerCtrl().getWorkBean().getMoKey(), currentVbean.getBBean().getItemCLs().getCode());

                    if (!canBeFinished) {
                        processFinish(
                                getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey(),
                                getViewerCtrl().getBean().getEntity().getEntityLocationBean().getContainerPackaging()
                                        .getPackagingId(), false, true);
                    } else {
                        result = getView().showQuestion(
                                I18nClientManager.translate(Jtech.T25073, false),
                                I18nClientManager.translate(ProductionMo.T24069, false) + " "
                                        + I18nClientManager.translate(ProductionMo.T29570, false),
                                MessageButtons.YES_NO);

                        if (result == MessageButtons.YES) {
                            processFinish(getViewerCtrl().getBean().getBBean().getOperationItemBean()
                                    .getOperationItemKey(), getViewerCtrl().getBean().getEntity()
                                    .getEntityLocationBean().getContainerPackaging().getPackagingId(), false, true);
                        }
                    }

                } catch (BusinessException e) {
                    getViewerCtrl().fireErrorsChanged(new UIException(e));
                }

            }
        } else if (getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getManagementType().equals(ManagementType.ARCHIVED)) {
            result = getView().showQuestion("Question", I18nClientManager.translate(ProductionMo.T29535, false),
                    MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {
                try {
                    getFboningOutputContainerCBS().resumeOperationItem(getIdCtx(),
                            getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey(),
                            getIdCtx().getWorkstation());
                    current = getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey();
                    current.setManagementType(ManagementType.REAL);
                    getBrowserController().reopenQuery();
                    if (current != null) {
                        getBrowserCtrl().repositionToOperationItemKey(current);
                    }
                } catch (BusinessException e) {
                    getViewerCtrl().fireErrorsChanged(new UIException(e));
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processFinishResume()");
        }
    }

    /**
     * Process the 'showall' event.
     */
    private void processShowAllEvent() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processShowAllEvent()");
        }

        FBoningOutputSBean sBean = (FBoningOutputSBean) getBrowserController().getInitialSelection();
        sBean.setAll(!sBean.getAll());
        manageBtnShowAllModelText();
        if (getBrowserCtrl().getModel().getCurrentBean() != null) {
            current = getBrowserCtrl().getModel().getCurrentBean().getOperationItemBean().getOperationItemKey();
        }
        getBrowserController().reopenQuery();
        if (current != null) {
            getBrowserCtrl().repositionToOperationItemKey(current);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processShowAllEvent()");
        }
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FContentRateBModel.java,v $
 * Created on 15 juil. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent;


import java.util.Comparator;

import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateBBeanEnum;


/**
 * Browser model for content.
 * 
 * @author cj
 */
public class FContentRateBModel extends BrowserModel<FContentRateBBean> implements Comparator<FContentRateBBean> {

    /**
     * Constructor.
     */
    public FContentRateBModel() {
        super();
        setBeanClass(FContentRateBBean.class);
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29552, false),
                FContentRateBBeanEnum.BATCH.getValue(), 10, "####"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29519, false),
                FContentRateBBeanEnum.ITEM_ID.getValue(), 10, "####"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29553, false),
                FContentRateBBeanEnum.FORMAT_QTY_UNIT.getValue(), 10, "####"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T34250, false),
                FContentRateBBeanEnum.RATE.getValue(), 10, "####"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T34251, false),
                FContentRateBBeanEnum.FORMAT_EXPECTED_QTY_UNIT.getValue(), 0, "####"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T34252, false),
                FContentRateBBeanEnum.EXPECTED_RATE.getValue(), 0, "####"));

        this.setComparator(this);
        // no fetch size because the proxy return all bean each time.
        setFirstFetchSize(999);
        setFullyFetched(true);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compare(final FContentRateBBean o1, final FContentRateBBean o2) {
        return o1.getOrder() - o2.getOrder();
    }

}

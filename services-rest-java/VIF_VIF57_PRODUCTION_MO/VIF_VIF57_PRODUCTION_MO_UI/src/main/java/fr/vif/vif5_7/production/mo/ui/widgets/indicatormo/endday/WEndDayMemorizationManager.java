/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WEndDayMemorizationManager.java,v $
 * Created on 22 Juin 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday;


import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.DateBoundBean;
import fr.vif.jtech.common.util.selection.PermanentSelectionValues;
import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;
import fr.vif.vif5_7.gen.location.business.beans.features.farea.FAreaBBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOSBeanEnum;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.dialog.parameter.EndDayParameterDialogBean;


/**
 * Memorization manager for the end of day indicator's widget.
 *
 * @author ac
 */
public class WEndDayMemorizationManager {

    public static final String                    ENDDAY_MEMO_TYPE       = "END";

    private static final int                      MAXIMAL_NUMBER_TO_SAVE = 110;

    /** LOGGER. */
    private static final Logger                   LOGGER                 = Logger.getLogger(WEndDayMemorizationManager.class);

    private WEndDayIndicatorMOCtrl                ctrl                   = null;

    private Map<String, PermanentSelectionValues> map                    = new HashMap<String, PermanentSelectionValues>();

    /**
     * Constructor.
     * 
     * @param ctrl cotnroller
     * @param map selection content.
     */
    public WEndDayMemorizationManager(final WEndDayIndicatorMOCtrl ctrl, final Map<String, PermanentSelectionValues> map) {
        this.ctrl = ctrl;
        this.map = map;
    }

    /**
     * 
     * Convert a memo data in selection.
     * 
     * @param uniqueId id
     * @return selection bean
     */
    public EndDayMOSBean convertMemoToSelection(final String uniqueId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertMemoToSelection(uniqueId=" + uniqueId + ")");
        }

        EndDayMOSBean selection = new EndDayMOSBean();

        if (this.getMap() != null) {
            String[] stored = map.get(uniqueId).getCommon().split("\\;");

            for (String selectionType : stored) {
                String[] split = selectionType.split("\\=");

                String key = split[0];
                String value = split[1];

                EndDayMOSBeanEnum type = EndDayMOSBeanEnum.getValueType(key);
                String[] dates = new String[2];
                // CHECKSTYLE:OFF
                String[] minBound = new String[4];
                String[] maxBound = new String[4];
                Calendar cMinBound = null;
                Calendar cMaxBound = null;
                // CHECKSTYLE:ON

                if (type != null) {
                    switch (type) {
                        case AREA:
                            if (EntityTools.isValid(value)) {
                                if (selection.getSector() == null) {
                                    selection.setSector(new FAreaBBean(" ", " "));
                                }
                                selection.getSector().setCode(value);
                            }
                            break;
                        case PRODUCTION_DATE_CHOICE:
                            switch (value) {
                                case "T":
                                    selection.setProductionDateChoice(EndDayParameterDialogBean.TODAY);
                                    break;
                                case "P":
                                    selection.setProductionDateChoice(EndDayParameterDialogBean.PERSONALIZED);

                                    break;
                                case "Y":
                                    selection.setProductionDateChoice(EndDayParameterDialogBean.YESTERDAY);

                                    break;

                                default:
                                    break;
                            }
                            break;
                        case BEGIN_DATE_CHOICE:
                            switch (value) {
                                case "T":
                                    selection.setBeginDateChoice(EndDayParameterDialogBean.TODAY);
                                    break;
                                case "P":
                                    selection.setBeginDateChoice(EndDayParameterDialogBean.PERSONALIZED);

                                    break;
                                case "Y":
                                    selection.setBeginDateChoice(EndDayParameterDialogBean.YESTERDAY);

                                    break;

                                default:
                                    break;
                            }
                            break;
                        case END_DATE_CHOICE:
                            switch (value) {
                                case "T":
                                    selection.setEndDateChoice(EndDayParameterDialogBean.TODAY);
                                    break;
                                case "P":
                                    selection.setEndDateChoice(EndDayParameterDialogBean.PERSONALIZED);

                                    break;
                                case "Y":
                                    selection.setEndDateChoice(EndDayParameterDialogBean.YESTERDAY);

                                    break;

                                default:
                                    break;
                            }

                            break;
                        case IS_TIME_WINDOW_CONTEXT:
                            selection.setIsTimeWindowContext("Y".equalsIgnoreCase(value));
                            break;
                        case DATE_BOUND:
                            dates = value.split("\\,");

                            // CHECKSTYLE:OFF

                            // Retrieves the minDateBound
                            minBound = dates[0].split("\\+");
                            cMinBound = Calendar.getInstance();
                            cMinBound.set(Calendar.DAY_OF_MONTH, Integer.parseInt(minBound[0]));
                            cMinBound.set(Calendar.MONTH, Integer.parseInt(minBound[1]));
                            cMinBound.set(Calendar.YEAR, Integer.parseInt(minBound[2]));
                            cMinBound.set(Calendar.HOUR_OF_DAY, Integer.parseInt(minBound[3]));
                            cMinBound.set(Calendar.MINUTE, Integer.parseInt(minBound[4]));

                            // Retrieves the maxDateBound
                            maxBound = dates[1].split("\\+");
                            cMaxBound = Calendar.getInstance();
                            cMaxBound.set(Calendar.DAY_OF_MONTH, Integer.parseInt(maxBound[0]));
                            cMaxBound.set(Calendar.MONTH, Integer.parseInt(maxBound[1]));
                            cMaxBound.set(Calendar.YEAR, Integer.parseInt(maxBound[2]));
                            cMaxBound.set(Calendar.HOUR_OF_DAY, Integer.parseInt(maxBound[3]));
                            cMaxBound.set(Calendar.MINUTE, Integer.parseInt(maxBound[4]));

                            // CHECKSTYLE:ON

                            selection.setDateBoundBean(new DateBoundBean(cMinBound.getTime(), cMaxBound.getTime()));
                            break;
                        case SHOW_MATERIAL:
                            selection.setShowMaterialIndicator("Y".equalsIgnoreCase(value));
                            break;
                        case SHOW_WORKING_PERIOD:
                            selection.setShowWorkingPeriodIndicator("Y".equalsIgnoreCase(value));
                            break;
                        case SHOW_TRS:
                            selection.setShowTrsIndicator("Y".equalsIgnoreCase(value));
                            break;
                        case SHOW_ON_LINE_CONTROL:
                            selection.setShowOnLineControlIndicator("Y".equalsIgnoreCase(value));
                            break;
                        default:
                            break;

                    }
                }
            }

        } else {
            ctrl.getView().showError("Erreur programmation", "Impossible de convertir la memo en selection");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertMemoToSelection(uniqueId=" + uniqueId + ")=" + selection);
        }
        return selection;
    }

    /**
     * Gets the ctrl.
     *
     * @return the ctrl.
     */
    public WEndDayIndicatorMOCtrl getCtrl() {
        return ctrl;
    }

    /**
     * Gets the map.
     *
     * @return the map.
     */
    public Map<String, PermanentSelectionValues> getMap() {
        return map;
    }

    /**
     * Set memorization map in shared context.
     * 
     * @param keyToGet key to get from map
     * @return {@link PermanentSelectionValues}
     */
    public PermanentSelectionValues getMemorizationById(final String keyToGet) {

        return map.get(keyToGet);

    }

    /**
     * 
     * Saves the memorization.
     * 
     * @param uniqueId id
     * @param selectionToStore selection
     */
    public void saveMemorization(final int uniqueId, final Object selectionToStore) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveMemorization(uniqueId=" + uniqueId + ", selectionToStore=" + selectionToStore + ")");
        }

        if (selectionToStore instanceof EndDayMOSBean) {
            EndDayMOSBean sbean = (EndDayMOSBean) selectionToStore;

            String toStore = "";
            PermanentSelectionValues p = new PermanentSelectionValues();
            p.setValueType(ENDDAY_MEMO_TYPE);
            String choice = "";

            for (EndDayMOSBeanEnum v : EndDayMOSBeanEnum.values()) {

                switch (v) {
                    case AREA:
                        if (sbean.getSector() != null) {
                            toStore += v.getValue() + "=" + sbean.getSector().getCode() + ";";
                        } else {
                            toStore += v.getValue() + "=" + " " + ";";
                        }
                        break;
                    case PRODUCTION_DATE_CHOICE:
                        switch (sbean.getProductionDateChoice()) {
                            case EndDayParameterDialogBean.TODAY:
                                choice = "T";
                                break;
                            case EndDayParameterDialogBean.PERSONALIZED:
                                choice = "P";
                                break;
                            case EndDayParameterDialogBean.YESTERDAY:
                                choice = "Y";
                                break;

                            default:
                                break;
                        }
                        toStore += v.getValue() + "=" + choice + ";";
                        break;
                    case BEGIN_DATE_CHOICE:
                        switch (sbean.getBeginDateChoice()) {
                            case EndDayParameterDialogBean.TODAY:
                                choice = "T";
                                break;
                            case EndDayParameterDialogBean.PERSONALIZED:
                                choice = "P";
                                break;
                            case EndDayParameterDialogBean.YESTERDAY:
                                choice = "Y";
                                break;
                            default:
                                break;
                        }
                        toStore += v.getValue() + "=" + choice + ";";
                        break;
                    case END_DATE_CHOICE:
                        switch (sbean.getEndDateChoice()) {
                            case EndDayParameterDialogBean.TODAY:
                                choice = "T";
                                break;
                            case EndDayParameterDialogBean.PERSONALIZED:
                                choice = "P";
                                break;
                            case EndDayParameterDialogBean.YESTERDAY:
                                choice = "Y";
                                break;
                            default:
                                break;
                        }
                        toStore += v.getValue() + "=" + choice + ";";

                        break;
                    case IS_TIME_WINDOW_CONTEXT:
                        toStore += v.getValue() + "=" + (sbean.isTimeWindowContext() ? "Y" : "N") + ";";
                        break;
                    case DATE_BOUND:
                        Calendar minDateBound = Calendar.getInstance();
                        minDateBound.setTime(sbean.getDateBoundBean().getMinBound());
                        Calendar maxDateBound = Calendar.getInstance();
                        maxDateBound.setTime(sbean.getDateBoundBean().getMaxBound());

                        toStore += v.getValue() + "=" + minDateBound.get(Calendar.DAY_OF_MONTH) + "+"
                                + minDateBound.get(Calendar.MONTH) + "+" + minDateBound.get(Calendar.YEAR) + "+"
                                + minDateBound.get(Calendar.HOUR_OF_DAY) + "+" + minDateBound.get(Calendar.MINUTE)
                                + "," + maxDateBound.get(Calendar.DAY_OF_MONTH) + "+"
                                + maxDateBound.get(Calendar.MONTH) + "+" + maxDateBound.get(Calendar.YEAR) + "+"
                                + maxDateBound.get(Calendar.HOUR_OF_DAY) + "+" + maxDateBound.get(Calendar.MINUTE)
                                + ";";
                        break;
                    case SHOW_MATERIAL:
                        toStore += v.getValue() + "=" + (sbean.getShowMaterialIndicator() ? "Y" : "N") + ";";
                        break;
                    case SHOW_WORKING_PERIOD:
                        toStore += v.getValue() + "=" + (sbean.getShowWorkingPeriodIndicator() ? "Y" : "N") + ";";
                        break;
                    case SHOW_TRS:
                        toStore += v.getValue() + "=" + (sbean.getShowTrsIndicator() ? "Y" : "N") + ";";
                        break;
                    case SHOW_ON_LINE_CONTROL:
                        toStore += v.getValue() + "=" + (sbean.getShowOnLineControlIndicator() ? "Y" : "N") + ";";
                        break;
                    default:
                        break;
                }

            }
            map = new HashMap<String, PermanentSelectionValues>();
            p.setCommon(toStore);
            map.put(String.valueOf(uniqueId), p);

        } else {
            ctrl.getView().showError("Erreur programmation",
                    "Aucune méthode de conversion de la sélection " + selectionToStore.toString());
        }

        if (map.get(uniqueId) != null && map.get(uniqueId).getCommon().length() > MAXIMAL_NUMBER_TO_SAVE) {
            ctrl.getView().showError("Erreur", "Trop d'information à enregistrer dans la table de mémorization.");

            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("E - saveMemorization(uniqueId=" + uniqueId + ", selectionToStore=" + selectionToStore
                        + ")");
            }
            return;
        }

        // User is not empty ! It's the user which is parameterized.
        try {
            ctrl.saveSelectionByIds(ctrl.getIdCtx(), ctrl.getIdCtx().getCompany(), ctrl.getIdCtx().getEstablishment(),
                    ctrl.getIdentification().getLogin(), String.valueOf(uniqueId), "", ENDDAY_MEMO_TYPE, map);
        } catch (BusinessException e) {
            LOGGER.error("", e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveMemorization(uniqueId=" + uniqueId + ", selectionToStore=" + selectionToStore + ")");
        }
    }

    /**
     * Sets the ctrl.
     *
     * @param ctrl ctrl.
     */
    public void setCtrl(final WEndDayIndicatorMOCtrl ctrl) {
        this.ctrl = ctrl;
    }

    /**
     * Sets the map.
     *
     * @param map map.
     */
    public void setMap(final Map<String, PermanentSelectionValues> map) {
        this.map = map;
    }
}

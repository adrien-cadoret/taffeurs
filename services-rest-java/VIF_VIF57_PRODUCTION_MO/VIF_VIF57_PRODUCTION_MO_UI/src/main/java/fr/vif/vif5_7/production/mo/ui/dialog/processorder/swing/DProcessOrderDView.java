/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DProcessOrderDView.java,v $
 * Created on 15 mars 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.dialog.processorder.swing;


import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.vif.jtech.ui.dialogs.swing.StandardSwingDialog;
import fr.vif.jtech.ui.input.InputTextFieldModel;
import fr.vif.jtech.ui.input.swing.SwingInputTextEditor;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.input.swing.SwingInputTextFieldMI;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.admin.profile.business.beans.features.fuser.FUserBBean;
import fr.vif.vif5_7.admin.profile.ui.composites.cuserlist.CUserListCtrl;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.dialog.processorder.DProcessOrderDBeanEnum;
import fr.vif.vif5_7.workshop.workstation.ui.composites.cworkstation.swing.CWorkStationView;


/**
 * The Comment mail dialog view.
 * 
 * @author mlme
 */
public class DProcessOrderDView extends StandardSwingDialog {

    private SwingInputTextEditor          iteComment     = null;
    private SwingInputTextFieldMI<String> itmChrono      = null;
    private SwingInputTextField           itfWorkStation = null;
    private CWorkStationView              cWorkStation   = null;

    private JPanel                        jContentPane   = null;

    /**
     * Default constructor.
     */
    public DProcessOrderDView() {
        super();
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param view parent of the view
     */
    public DProcessOrderDView(final Dialog view) {
        super(view);
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param view parent of the dialog
     */
    public DProcessOrderDView(final Frame view) {
        super(view);
        initialize();
    }

    /**
     * Gets the cArea.
     * 
     * @return the cArea.
     */
    public CWorkStationView getCWorkStation() {
        if (cWorkStation == null) {
            cWorkStation = new CWorkStationView();
            cWorkStation.setBeanProperty(DProcessOrderDBeanEnum.WORKSTATION.getValue());
        }
        return cWorkStation;
    }

    /**
     * Get the comment text editor.
     * 
     * @return the comment text editor
     */
    private SwingInputTextEditor getCommentTextEditor() {
        if (iteComment == null) {
            iteComment = new SwingInputTextEditor();
            iteComment.setBeanProperty(DProcessOrderDBeanEnum.COMMENT.getValue());
            iteComment.setAlwaysDisabled(true);
            iteComment.setSize(500, 300);
            iteComment.setPreferredSize(new Dimension(500, 300));
            iteComment.setMinimumSize(new Dimension(500, 300));
            iteComment.setMaximumSize(new Dimension(500, 300));
        }

        return iteComment;
    }

    /**
     * Get the comment text editor.
     * 
     * @return the comment text editor
     */
    private SwingInputTextField getItfWorkStation() {
        if (itfWorkStation == null) {
            itfWorkStation = new SwingInputTextField(String.class, "30", false, null);
            itfWorkStation.setBeanProperty(DProcessOrderDBeanEnum.WORKSTATION.getValue());
            itfWorkStation.setAlwaysDisabled(true);
        }

        return itfWorkStation;
    }

    /**
     * Gets the cCategory.
     * 
     * @return the cCategory.
     */
    @SuppressWarnings("unchecked")
    private SwingInputTextFieldMI<String> getItmChrono() {
        if (itmChrono == null) {
            itmChrono = new SwingInputTextFieldMI(InputTextFieldModel.TYPE_STRING, null, CUserListCtrl.class,
                    FUserBBean.class, null, null, null);
            // cUser.setHelpDialogSize(500, 650);
            itmChrono.setBeanProperty(DProcessOrderDBeanEnum.LIST_CHRONO.getValue());
            itmChrono.setAlwaysDisabled(true);
            // itmChrono.setSize(300, 100);
        }
        return itmChrono;
    }

    /**
     * Get the content pane of the dialog.
     * 
     * @return the content pane of the dialog
     */
    private JPanel getJContentPane() {
        if (jContentPane == null) {
            jContentPane = new JPanel();
            jContentPane.setLayout(new GridBagLayout());

            GridBagConstraints gbcJlChrono = new GridBagConstraints();
            gbcJlChrono.gridx = 0;
            gbcJlChrono.gridy = 0;
            // gbcJlChrono.anchor = GridBagConstraints.boWEST;
            jContentPane.add(new JLabel(I18nClientManager.translate(ProductionMo.T32554, false)), gbcJlChrono);

            GridBagConstraints gbcItmChrono = new GridBagConstraints();
            gbcItmChrono.gridx = 1;
            gbcItmChrono.gridy = 0;
            gbcItmChrono.fill = GridBagConstraints.BOTH;
            jContentPane.add(getItmChrono(), gbcItmChrono);

            GridBagConstraints gbcJlWorkStation = new GridBagConstraints();
            gbcJlWorkStation.gridx = 0;
            gbcJlWorkStation.gridy = 1;
            // gbcJlChrono.anchor = GridBagConstraints.boWEST;
            jContentPane.add(new JLabel(I18nClientManager.translate(Generic.T3340, false)), gbcJlWorkStation);

            GridBagConstraints gbcCWorkStation = new GridBagConstraints();
            gbcCWorkStation.gridx = 1;
            gbcCWorkStation.gridy = 1;
            gbcCWorkStation.fill = GridBagConstraints.BOTH;
            jContentPane.add(getCWorkStation(), gbcCWorkStation);

            // GridBagConstraints gbcJlWorkStation = new GridBagConstraints();
            // gbcJlWorkStation.gridx = 0;
            // gbcJlWorkStation.gridy = 1;
            // // gbcJlChrono.anchor = GridBagConstraints.boWEST;
            // jContentPane.add(new JLabel(I18nClientManager.translate(Generic.T3340, false)), gbcJlWorkStation);
            //
            // GridBagConstraints gbcItfWorkStation = new GridBagConstraints();
            // gbcItfWorkStation.gridx = 1;
            // gbcItfWorkStation.gridy = 1;
            // gbcItfWorkStation.fill = GridBagConstraints.BOTH;
            // jContentPane.add(getItfWorkStation(), gbcItfWorkStation);

            GridBagConstraints gbcJlComment = new GridBagConstraints();
            gbcJlComment.gridx = 0;
            gbcJlComment.gridy = 2;
            // gbcJlComment.anchor = GridBagConstraints.WEST;
            jContentPane.add(new JLabel(I18nClientManager.translate(GenKernel.T27744, false)), gbcJlComment);

            GridBagConstraints gbcIteComment = new GridBagConstraints();
            gbcIteComment.gridx = 1;
            gbcIteComment.gridy = 2;
            gbcIteComment.fill = GridBagConstraints.BOTH;

            jContentPane.add(getCommentTextEditor(), gbcIteComment);
        }

        return jContentPane;
    }

    /**
     * Initializes the view.
     */
    private void initialize() {
        super.addPanel(getJContentPane());
        this.setTitle(I18nClientManager.translate(ProductionMo.T32555, false));
        // this.setSize(new java.awt.Dimension(600, 500));
        // pack();
    }
}

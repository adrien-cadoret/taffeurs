/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompFCtrl.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.constant.EntityConstants;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylistcomp.FDataEntryListCompFModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer.AbstractFProductionOutputFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerFCtrl;


/**
 * Production Output complementary Feature Controller.
 *
 * @author kl
 * @param <FProductionOutputCompCBS> the CBS for complementary nature
 */
public class FProductionOutputCompFCtrl<FProductionOutputCompCBS> extends AbstractFProductionOutputFCtrl implements
        DialogChangeListener, DisplayListener {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FProductionOutputCompFCtrl.class);

    /**
     * Same code in FProductionOutputContainerFCtrl Except the BTN_DETAIL_REFERENCE case. The mapKey is different.
     * 
     * @param event event button
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        if (BTN_FPRODUCTION_INPUT_REFERENCE.equals(event.getModel().getReference())) {
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL,
                    getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL));
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL, true);
            // If one the item of the activity has a Nature Complementary Workshop.
            // We force the redirection to an other screen (VIF.MOINSCRAPDE1T), to manage the compl. nature.
            if (getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.MO_LIST_VBEAN) != null) {
                FMOListVBean vBean = (FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT,
                        FProductionConstant.MO_LIST_VBEAN);
                String inputFunction = "";
                if (vBean.isStatementScrap(ActivityItemType.INPUT)) {
                    inputFunction = "VIF.MOINSCRAPDE1T"; // comp input
                } else {
                    inputFunction = "VIF.MOINCODE1T"; // classical input
                }
                getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.FEATURE_ID, inputFunction);
                fireSharedContextSend(getSharedContext());
                fireSelfFeatureClosingRequired();
            }
        } else if (FProductionOutputContainerFCtrl.BTN_SHOW_ALL_REFERENCE.equals(event.getModel().getReference())) {
            processShowAllEvent();
        } else if (BTN_DETAIL_REFERENCE.equals(event.getModel().getReference())) {
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL,
                    getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL));
            getViewerCtrl().setPrevious(null); // Maybe delete done in movement list
            ((FProductionOutputCompVCtrl) getViewerCtrl()).getMap().remove(
                    new FProductionOutputCompMapKey(FProductionTools.operationItemKeyToMovementAct(getViewerBean()
                            .getBBean().getOperationItemBean().getOperationItemKey()), getViewerBean().getBBean()
                            .getItemCLs(), getViewerBean().getBBean().getNatureComplementaryWorkshop()));
            openChildFeature("VIF.MOOUSCRAPLI1T");
        } else if (BTN_CLOSE_CONTAINER.equals(event.getModel().getReference())) {
            try {
                if (!getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().getContainerNumber()
                        .isEmpty()
                        && !getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .getContainerNumber().equals(EntityConstants.NEW)) {
                    List<PrintDocument> lst = getFproductionOutputContainerCBS().closePrintContainer(getIdCtx(),
                            getViewerBean(), getViewerCtrl().getWorkBean());
                    if (lst != null && lst.size() > 0) {
                        getViewerCtrl().askNumDocument(lst);

                        getFproductionOutputContainerCBS().printContainer(getIdCtx(), getViewerBean(), lst,
                                getViewerCtrl().getWorkBean());
                    }
                    // QA8912 MP : reset location only if depot not optimize and the activity has not location
                    if (!getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCemp().isEmpty()
                            && (getViewerBean().getHasDepositWithLocationOptimise() || getViewerBean().getBBean()
                                    .getOperationItemBean().getLocationCL().getCode().isEmpty())) {
                        getViewerBean().getEntity().getEntityLocationBean().getEnableFields().setLocation(true);
                        getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().setCemp("");
                    }
                    getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                            .setContainerNumber(EntityConstants.NEW);
                } else if (!getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                        .getUpperContainerNumber().isEmpty()
                        && !getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .getUpperContainerNumber().equals(EntityConstants.NEW)) {
                    List<PrintDocument> lst = getFproductionOutputContainerCBS().closePrintUpperContainer(getIdCtx(),
                            getViewerBean(), getViewerCtrl().getWorkBean());
                    if (lst != null && lst.size() > 0) {
                        getViewerCtrl().askNumDocument(lst);
                        getFproductionOutputContainerCBS().printUpperContainer(getIdCtx(), getViewerBean(), lst,
                                getViewerCtrl().getWorkBean());
                    }
                    // QA8912 MP : reset location only if depot not optimize and the activity has not location
                    if (!getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCemp().isEmpty()
                            && (getViewerBean().getHasDepositWithLocationOptimise() || getViewerBean().getBBean()
                                    .getOperationItemBean().getLocationCL().getCode().isEmpty())) {
                        getViewerBean().getEntity().getEntityLocationBean().getEnableFields().setLocation(true);
                        getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().setCemp("");
                    }
                    getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                            .setUpperContainerNumber(EntityConstants.NEW);
                }

                getViewerCtrl().getModel().setInitialBean(getViewerBean());
                getViewerCtrl().renderAndEnableComponents();
                getViewerCtrl().saveCurrent();
                getViewerCtrl().fireButtonStatechanged();
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
        } else if (BTN_CANCELLAST_REFERENCE.equals(event.getModel().getReference())) {
            if (getViewerCtrl().getPrevious() != null) {
                int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                        I18nClientManager.translate(ProductionMo.T29578, false), MessageButtons.YES_NO,
                        MessageButtons.DEFAULT_NO);
                if (ret == MessageButtons.YES) {
                    try {

                        OperationItemKey oik = new OperationItemKey();
                        oik.setEstablishmentKey(getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey());
                        oik.setChrono(getViewerCtrl().getWorkBean().getMoKey().getChrono());
                        oik.setCounter1(getViewerCtrl().getPrevious().getNumber1());
                        oik.setCounter2(getViewerCtrl().getPrevious().getNumber2());
                        oik.setCounter3(getViewerCtrl().getPrevious().getNumber3());
                        oik.setCounter4(getViewerCtrl().getPrevious().getNumber4());
                        oik.setManagementType(ManagementType.REAL);
                        getFproductionOutputContainerCBS().deleteLast(getIdCtx(),
                                getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey(),
                                getViewerCtrl().getPrevious());
                        getViewerCtrl().getPrevious().setLineNumber(0);
                        ((FProductionOutputCompVCtrl) getViewerCtrl()).getMap().remove(getViewerCtrl().getPrevious());

                        getBrowserCtrl().reopenQuery();

                        getBrowserCtrl().repositionToOperationItemKey(oik);
                        getViewerCtrl().setPrevious(null);
                        getViewerCtrl().fireButtonStatechanged();

                    } catch (BusinessException e) {
                        getView().show(new UIException(e));
                    } catch (BusinessErrorsException e) {
                        getViewerCtrl().fireErrorsChanged(new UIErrorsException(e));
                    }
                }
            }
        }
        super.buttonAction(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        setMoListVBean((FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.MO_LIST_VBEAN));

        // -------------------------------
        // Initialize browser selection
        // -------------------------------
        FProductionOutputCompSBean sBean = new FProductionOutputCompSBean();
        sBean.setActivityItemType(ActivityItemType.OUTPUT);
        sBean.setWorkstationId((String) getSharedContext().get(Domain.DOMAIN_PARENT,
                FProductionConstant.LOGICAL_WORKSTATION));
        sBean.setAll(false);
        sBean.setMoKey(getMoListVBean().getMobean().getMoKey());

        // search the first item to select the good tab when opening the feature.
        int nbManufacturing = 0;
        int nbDowngraded = 0;
        int nbScrap = 0;
        int nbINProgress = 0;
        for (OutputOperationItemBean operation : getMoListVBean().getListOutputOperations()) {
            if (operation.getNatureList().isEmpty()
                    || operation.getNatureList().contains(MONatureComplementaryWorkshop.MANUFACTURING.getValue())
                    || !operation.getAutoQty()) {
                nbManufacturing += 1;
                break;
            }
            if (operation.getNatureList().contains(MONatureComplementaryWorkshop.DOWNGRADED.getValue())) {
                nbDowngraded += 1;
            }
            if (operation.getNatureList().contains(MONatureComplementaryWorkshop.SCRAP.getValue())) {
                nbScrap += 1;
            }
            if (operation.getNatureList().contains(MONatureComplementaryWorkshop.INPROGRESS.getValue())) {
                nbINProgress += 1;
            }
        }
        if (nbManufacturing > 0) {
            sBean.setState(MONatureComplementaryWorkshop.MANUFACTURING.getValue());
        } else if (nbDowngraded > 0) {
            sBean.setState(MONatureComplementaryWorkshop.DOWNGRADED.getValue());
        } else if (nbScrap > 0) {
            sBean.setState(MONatureComplementaryWorkshop.SCRAP.getValue());
        } else if (nbINProgress > 0) {
            sBean.setState(MONatureComplementaryWorkshop.INPROGRESS.getValue());
        }

        // Put the selection Bean in FProductionOutputCompGSModel
        getGeneralSelectionController().getModel().setBean(sBean);
        getBrowserController().setCurrentSelection(sBean);
        getBrowserController().setInitialSelection(sBean);

        super.initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = new ArrayList<ToolBarBtnStdBaseModel>();
        lst.addAll(super.manageActionsButtonModels(TouchModelMapKey.CONTAINER_OUTPUT_PRODUCTION));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FProductionOutputCompBCtrl getBrowserCtrl() {
        return (FProductionOutputCompBCtrl) getBrowserController();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void openChildFeature(final String featureId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openChildFeature(featureId=" + featureId + ")");
        }

        if (featureId.equals("VIF.MOOUSCRAPLI1T")) {
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.FEATURE_TITLE, getModel().getTitle());
            getSharedContext().put(Domain.DOMAIN_THIS, FDataEntryListFCtrl.OPERATION_ITEM_BEAN,
                    getViewerBean().getBBean().getOperationItemBean());
            getSharedContext().put(Domain.DOMAIN_THIS, "outputEntityType",
                    getViewerBean().getOutputItemParameters().getEntityType());
            getSharedContext().put(Domain.DOMAIN_THIS, FDataEntryListCompFModel.CONTEXT_NATURE,
                    getViewerBean().getBBean().getNatureComplementaryWorkshop());
            getSharedContext().put(Domain.DOMAIN_THIS, "itemKey",
                    new ItemKey(null, getViewerBean().getBBean().getItemCLs().getCode()));
            fireSharedContextSend(getSharedContext());
            setCurrent(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey());
        }
        super.openChildFeature(featureId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openChildFeature(featureId=" + featureId + ")");
        }
    }

    /**
     * Finishes or unfinishes an item.
     */
    @Override
    protected void processFinishResume() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processFinishResume()");
        }

        int result = 0;
        if (getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                .equals(ManagementType.REAL)) {
            result = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29534, false), MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {
                try {
                    getFproductionOutputContainerCBS().finishOperationItem(getIdCtx(),
                            getViewerBean().getBBean().getOperationItemBean().getOperationItemKey(),
                            getIdCtx().getWorkstation(), ActivityItemType.OUTPUT);
                    ((FProductionOutputCompVCtrl) getViewerCtrl()).getMap().remove(
                            FProductionTools.operationItemKeyToMovementAct(getViewerBean().getBBean()
                                    .getOperationItemBean().getOperationItemKey()));
                    setCurrent(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey());
                    getCurrent().setManagementType(ManagementType.ARCHIVED);
                    getBrowserController().reopenQuery();
                    if (getCurrent() != null) {
                        getBrowserCtrl().repositionToOperationItemKey(getCurrent());
                    }
                } catch (BusinessException e) {
                    getViewerCtrl().fireErrorsChanged(new UIException(e));
                }
            }
        } else if (getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                .equals(ManagementType.ARCHIVED)) {
            result = getView().showQuestion("Question", I18nClientManager.translate(ProductionMo.T29535, false),
                    MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {
                try {
                    getFproductionOutputContainerCBS().resumeOperationItem(getIdCtx(),
                            getViewerBean().getBBean().getOperationItemBean().getOperationItemKey(),
                            getIdCtx().getWorkstation());
                    setCurrent(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey());
                    getCurrent().setManagementType(ManagementType.REAL);
                    getBrowserController().reopenQuery();
                    if (getCurrent() != null) {
                        getBrowserCtrl().repositionToOperationItemKey(getCurrent());
                    }
                } catch (BusinessException e) {
                    getViewerCtrl().fireErrorsChanged(new UIException(e));
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processFinishResume()");
        }
    }

    /**
     * Process the 'showall' event.
     */
    private void processShowAllEvent() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processShowAllEvent()");
        }

        FProductionSBean sBean = (FProductionSBean) getBrowserController().getInitialSelection();
        sBean.setAll(!sBean.getAll());
        manageBtnShowAllModelText();
        if (getBrowserCtrl().getModel().getCurrentBean() != null) {
            setCurrent(getBrowserCtrl().getModel().getCurrentBean().getOperationItemBean().getOperationItemKey());
        }
        getBrowserController().reopenQuery();
        if (getCurrent() != null) {
            getBrowserCtrl().repositionToOperationItemKey(getCurrent());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processShowAllEvent()");
        }
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOListGSModel.java,v $
 * Created on 14 nov. 08 by alb
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist;


import fr.vif.jtech.ui.selection.GeneralSelectionModel;


/**
 * MOList: General Selection Model.
 * 
 * @author alb
 */
public class FMOListGSModel extends GeneralSelectionModel {

    /**
     * Default Constructor.
     */
    public FMOListGSModel() {
    }

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductivityTableHeaderRenderer.java,v $
 * Created on 24 sept. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing;


import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import fr.vif.jtech.ui.panel.swing.GradientSwingPanel;


/**
 * KeyboardscreenTableHeaderRenderer.
 * 
 * @author vr
 */
public class FProductivityTableHeaderRenderer implements TableCellRenderer {

    private static final Font KEYBOARDSCREEN_FONT_BROWSER_HEADER = new Font("Arial", Font.BOLD, 12);

    private MouseListener     listener                           = null;

    /**
     * Constructor.
     */
    public FProductivityTableHeaderRenderer() {
    }

    /**
     * Constructor.
     * 
     * @param listener {@link MouseListener}
     */
    public FProductivityTableHeaderRenderer(final MouseListener listener) {
        this.listener = listener;
    }

    /**
     * {@inheritDoc}
     */
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        String text = (value == null ? "" : value.toString());

        JLabel label = new JLabel(text);
        GradientSwingPanel labelPanel = new GradientSwingPanel();
        labelPanel.setPreferredSize(new Dimension(labelPanel.getWidth(), 20));
        labelPanel.setLayout(new FlowLayout(FlowLayout.CENTER));

        // labelPanel.setStartColor(new Color(133, 36, 128));
        // labelPanel.setEndColor(new Color(133, 36, 128));

        label.setFont(KEYBOARDSCREEN_FONT_BROWSER_HEADER);
        label.setForeground(Color.BLACK);
        /*
         * Border lineBorder = new LineBorder(Color.WHITE, 1, false); labelPanel.setBorder(lineBorder);
         */
        labelPanel.add(label);

        if (text != null) {
            String[] txt = text.split("-");

            String html = "";
            if (txt.length == 2) {
                html = "<html><body><TABLE>" //
                        + "<TR><TD >" + txt[0] + "</TD></TR>" //
                        + "<TR><TD>" + txt[1] + "</TD></TR>" //
                        + "</TABLE></body></html>";
            } else {
                html = text;
            }

            labelPanel.setToolTipText(html);
        }
        // JD a priori ne fonctionne pas
        if (listener != null) {
            label.addMouseListener(listener);
            labelPanel.addMouseListener(listener);
        }

        return labelPanel;
    }
}

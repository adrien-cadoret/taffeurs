/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOHeadsCtrl.java,v $
 * Created on 11 févr 2014 by lm
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.marking.MarkingBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOVBean;
import fr.vif.vif5_7.production.mo.business.services.features.fmarkingmo.FMarkingMOCBS;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingDevice;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingEnum.DeviceState;
import fr.vif.vif5_7.workshop.device.ui.composites.marking.MarkingHeadsCtrl;


/**
 * Controller: MarkingHeadsCtrl's derived class to send start and stop order to the device.
 * 
 * @author lm
 */
public class FMarkingMOHeadsCtrl extends MarkingHeadsCtrl {
    /** LOGGER. */
    private static final Logger LOGGER        = Logger.getLogger(FMarkingMOHeadsCtrl.class);

    private FMarkingMOCBS       fmarkingMOCBS = null;

    /**
     * Empty constructor.
     */
    public FMarkingMOHeadsCtrl() {
    }

    /**
     * Gets the fmarkingMOCBS.
     * 
     * @return the fmarkingMOCBS.
     */
    public FMarkingMOCBS getFmarkingMOCBS() {
        return fmarkingMOCBS;
    }

    /**
     * Sets the fmarkingMOCBS.
     * 
     * @param fmarkingMOCBS fmarkingMOCBS.
     */
    public void setFmarkingMOCBS(final FMarkingMOCBS fmarkingMOCBS) {
        this.fmarkingMOCBS = fmarkingMOCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void startDevice(final MarkingDevice aDevice) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - startDevice(aDevice=" + aDevice + ")");
        }

        try {
            MarkingBean markingBean = new MarkingBean();
            markingBean.setDeviceId(aDevice.getPrinterCL().getCode());
            if (aDevice.getState() == DeviceState.NOT_STARTED) {
                markingBean.setDocumentId(aDevice.getCurrentDocument().getDocumentCL().getCode());
                markingBean.setOperationItemKey(((FMarkingMOVBean) getBean()).getProcessingItem().getMoKey()
                        .getOperationItemKey());
            } else if (aDevice.getState() == DeviceState.REDUNDANT || aDevice.getState() == DeviceState.UNUSED) {
                markingBean.setDocumentId(aDevice.getSimulatedDocument().getDocumentCL().getCode());
                markingBean.setOperationItemKey(((FMarkingMOVBean) getBean()).getSelectedItem().getMoKey()
                        .getOperationItemKey());
            } else {
                throw new UIException(Generic.T22494, ProductionMo.T34900);
            }
            // workstation
            markingBean.setWorkstationId(getIdentification().getWorkstation());
            markingBean.setFeatureId(getView().getModel().getLinkedFeatureId());
            markingBean.setHardwareKeyType(aDevice.getHardwareKey().getHardwareKeyType());
            getFmarkingMOCBS().startPrinting(getIdCtx(), markingBean);
            // No error ? Change view.
            super.startDevice(aDevice);
        } catch (BusinessException e) {
            getView().show(new UIException(e));
        } catch (UIException e) {
            getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - startDevice(aDevice=" + aDevice + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stopDevice(final MarkingDevice aDevice) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - stopDevice(aDevice=" + aDevice + ")");
        }

        try {
            OperationItemKey vOperationItemKey = null;
            if (aDevice.getState() == DeviceState.STARTED) {
                vOperationItemKey = ((FMarkingMOVBean) getBean()).getProcessingItem().getMoKey().getOperationItemKey();
            } else if (aDevice.getState() == DeviceState.SIMULATED) {
                vOperationItemKey = ((FMarkingMOVBean) getBean()).getSelectedItem().getMoKey().getOperationItemKey();
            } else {
                throw new UIVetoException(Generic.T22494, ProductionMo.T34899);
            }
            stopDeviceWithoutUIControl(aDevice, vOperationItemKey);
            // No error ? Change view.
            super.stopDevice(aDevice);
        } catch (UIException e) {
            getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - stopDevice(aDevice=" + aDevice + ")");
        }
    }

    /**
     * Stop a {@link MarkingDevice} but doesn't reload the UI.
     * 
     * @param aDevice {@link MarkingDevice}
     * @param anOperationItemKey id of the current MO
     */
    public void stopDeviceWithoutUIControl(final MarkingDevice aDevice, final OperationItemKey anOperationItemKey) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - stopDeviceWithoutUIControl(aDevice=" + aDevice + ", anOperationItemKey="
                    + anOperationItemKey + ")");
        }

        MarkingBean markingBean = new MarkingBean();
        markingBean.setDeviceId(aDevice.getPrinterCL().getCode());
        if (aDevice.getState() == DeviceState.STARTED) {
            markingBean.setDocumentId(aDevice.getCurrentDocument().getDocumentCL().getCode());
        } else if (aDevice.getState() == DeviceState.SIMULATED) {
            markingBean.setDocumentId(aDevice.getSimulatedDocument().getDocumentCL().getCode());
        }
        markingBean.setOperationItemKey(anOperationItemKey);
        // workstation
        markingBean.setWorkstationId(getIdentification().getWorkstation());
        markingBean.setFeatureId(getView().getModel().getLinkedFeatureId());
        markingBean.setHardwareKeyType(aDevice.getHardwareKey().getHardwareKeyType());
        try {
            getFmarkingMOCBS().stopPrinting(getIdCtx(), markingBean);
        } catch (BusinessException e) {
            getView().showWarning(e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - stopDeviceWithoutUIControl(aDevice=" + aDevice + ", anOperationItemKey="
                    + anOperationItemKey + ")");
        }
    }

}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOActionManager.java,v $
 * Created on 4 oct. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common;


import java.util.List;

import org.apache.log4j.Logger;
import org.jfree.chart.entity.CategoryItemEntity;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.menu.LaunchParameterBean;
import fr.vif.jtech.common.util.ObjectHelper;
import fr.vif.jtech.common.util.exceptions.ObjectException;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOSelectionBean;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOValues;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.AbstractSupervisorMOSelectionBean;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ActionId;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.FSupervisorMOFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.IGanttMoView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing.AbstractMODockableView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;


/**
 * Action manager of the simple indicator view.
 * 
 * @author jd
 */
public class FIndicatorMOActionManager {
    /** LOGGER. */
    private static final Logger LOGGER      = Logger.getLogger(FIndicatorMOActionManager.class);

    private static final String WORKSTATION = "POSTE";

    private FSupervisorMOFCtrl  ctrl;

    /**
     * Constructor.
     * 
     * @param ctrl {@link FSupervisorMOFCtrl}
     */
    public FIndicatorMOActionManager(final FSupervisorMOFCtrl ctrl) {
        this.ctrl = ctrl;
    }

    /**
     * Managed the actions performed.
     * 
     * @param event {@link GenericActionEvent}
     * @param localFrame informations about the frame who asked
     * @param frame view of the frame.
     * @param indicatorMO IndicatorMo source bean
     */
    public void actionPerformed(final GenericActionEvent event, final MOLocalFrame localFrame,
            final AbstractMODockableView frame, final IndicatorMO indicatorMO) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ", localFrame=" + localFrame + ", frame=" + frame
                    + ", indicatorMO=" + indicatorMO + ")");
        }

        if (ActionId.MO_DETAIL.getValue().equalsIgnoreCase(event.getAction())
                || FSupervisorMOTools.CLICK_ON_CELL.equalsIgnoreCase(event.getAction())) {

            FProcessOrdersSBean sbean = new FProcessOrdersSBean();
            try {
                sbean.setSelection(ObjectHelper.copy(localFrame.getSelection()));
            } catch (ObjectException e) {
                LOGGER.error(e.getMessage());
            }

            updateSBeanWithData(localFrame, sbean.getSelection(), event);

            LaunchParameterBean launchParameterBean = new LaunchParameterBean();

            // Q_05506:Superviseur fab: Les OT du zoom ne sont pas restreints par l'état et type de flux propres à
            // l'indic
            // When chronos are set, the other fields of the selection are ignored
            if (event.getData() instanceof IndicatorMOValues) {
                List<ChronoPO> chronos = ((IndicatorMOValues) event.getData()).getChronos();
                sbean.setChronos(chronos);
            }

            launchParameterBean.getValues().put(FProcessOrdersFCtrl.PARAMETER_BEAN, sbean);

            launchParameterBean.getValues().put(WORKSTATION, ctrl.getIdCtx().getWorkstation());

            ctrl.openMainFeature(FProcessOrdersFCtrl.VIF_FEATURE_MOPROGT, launchParameterBean);

        } else if (ActionId.MO_UPDATE.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_LIST.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_INPUTS_OUTPUTS.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_FINISH.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_UPDATE_DECLARATION.getValue().equalsIgnoreCase(event.getAction())
                || ActionId.MO_VALIDATE.getValue().equalsIgnoreCase(event.getAction())) {
            try {
                AbstractSupervisorMOSelectionBean sbean = ObjectHelper.copy(frame.getLocalFrame().getSelection());

                updateSBeanWithData(localFrame, sbean, event);

                // Q_05506:Superviseur fab: Les OT du zoom ne sont pas restreints par l'état et type de flux propres à
                // l'indic
                List<ChronoPO> chronos = null;

                if (event.getData() instanceof IndicatorMOValues) {
                    chronos = ((IndicatorMOValues) event.getData()).getChronos();
                } else {
                    chronos = frame.getParentController().getProductivityMOCBS()
                            .listChronoOfSelection(frame.getParentController().getIdCtx(), sbean);
                }

                LaunchParameterBean launchParameterBean = FSupervisorMOTools.getLaunchParameter(frame.getLocalFrame()
                        .getSelection().getPoDate(), chronos);

                ctrl.openMainFeature(ActionId.getFeatureId(event.getAction()).getValue(), launchParameterBean);
            } catch (BusinessException e) {
                ctrl.show(new UIException(e));
            } catch (ObjectException e) {
                LOGGER.error(e.getMessage());
            }

        } else if (FSupervisorMOTools.REFRESH.equalsIgnoreCase(event.getAction())) {
            frame.refresh();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ", localFrame=" + localFrame + ", frame=" + frame
                    + ", indicatorMO=" + indicatorMO + ")");
        }
    }

    /**
     * Update a selection with the datas of an event.
     * 
     * @param localFrame {@link MOLocalFrame}
     * @param selection selection
     * @param event event
     */
    private void updateSBeanWithData(final MOLocalFrame localFrame, final IndicatorMOSelectionBean selection,
            final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateSBeanWithData(localFrame=" + localFrame + ", selection=" + selection + ", event="
                    + event + ")");
        }

        // Event data is IndicatorMOValues
        if (event.getData() != null && event.getData() instanceof IndicatorMOValues) {

            IndicatorMOValues value = (IndicatorMOValues) event.getData();
            switch (localFrame.getSelection().getAxe()) {
                case ITEM_GROUP:
                    selection.getCategories().clear();
                    selection.getCategories().add(value.getIndicatorCL().getCode());
                    break;
                case PRODUCTION_LINE:
                    selection.getLines().clear();
                    selection.getLines().add(value.getIndicatorCL().getCode());
                    break;
                case SECTOR:
                    selection.getSectors().clear();
                    selection.getSectors().add(value.getIndicatorCL().getCode());
                    break;
                case TEAM:
                    selection.getTeams().clear();
                    selection.getTeams().add(value.getIndicatorCL().getCode());
                    break;
                default:
                    break;
            }

        } else if (event.getSource() instanceof IGanttMoView) {
            GraphMOPeriod p = ((IGanttMoView) event.getSource()).getLastClickedGraphPeriod();
            if (p.getBeanSource() instanceof IndicatorMO) {
                IndicatorMO ind = (IndicatorMO) p.getBeanSource();

                selection.getPoKey().setChrono(
                        new ChronoPO(ind.getPoKey().getChrono().getPrechro(), ind.getPoKey().getChrono().getChrono(),
                                ind.getPoKey().getChrono().getCounter1()));
            }
        } else if (event.getData() != null && event.getData() instanceof CategoryItemEntity) {

            CategoryItemEntity value = (CategoryItemEntity) event.getData();
            switch (localFrame.getSelection().getAxe()) {
                case ITEM_GROUP:
                    selection.getCategories().clear();
                    selection.getCategories().add((String) value.getColumnKey());
                    break;
                case PRODUCTION_LINE:
                    selection.getLines().clear();
                    selection.getLines().add((String) value.getColumnKey());
                    break;
                case SECTOR:
                    selection.getSectors().clear();
                    selection.getSectors().add((String) value.getColumnKey());
                    break;
                case TEAM:
                    selection.getTeams().clear();
                    selection.getTeams().add((String) value.getColumnKey());
                    break;
                default:
                    break;
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateSBeanWithData(localFrame=" + localFrame + ", selection=" + selection + ", event="
                    + event + ")");
        }
    }
}

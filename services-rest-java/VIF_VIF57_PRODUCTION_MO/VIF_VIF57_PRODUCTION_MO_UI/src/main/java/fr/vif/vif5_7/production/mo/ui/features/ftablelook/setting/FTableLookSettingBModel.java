/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingBModel.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingBBean;


/**
 * TableLook Browser Model.
 * 
 * @author kl
 */
public class FTableLookSettingBModel extends BrowserModel<FTableLookSettingBBean> {

    /**
     * Default Constructor.
     */
    public FTableLookSettingBModel() {
        super();
        this.setBeanClass(FTableLookSettingBBean.class);
        // Sets the number of records to return in paginate mode
        setFetchSize(20);
        setFirstFetchSize(40);
        // Sets the menus available in the browser
        setSelectionEnabled(false);
        setSearchEnabled(true);
        setSearchColumn(0);
        setAdvancedSearchEnabled(false);
        setXlsExportEnabled(false);
    }
}

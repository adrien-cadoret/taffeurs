/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompVView.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp.touch;


import java.awt.Dimension;

import javax.swing.JSeparator;
import javax.swing.SwingConstants;

import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif57.stock.entity.business.services.constant.EntityConstants;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.gen.comment.ui.composites.simplecomment.touch.CCommentTouchView;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationBBean;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationSBean;
import fr.vif.vif5_7.gen.location.ui.features.flocation.FLocationBCtrl;
import fr.vif.vif5_7.gen.location.ui.features.flocation.FLocationBModel;
import fr.vif.vif5_7.gen.location.ui.features.flocation.touch.FLocationBView;
import fr.vif.vif5_7.gen.service.ui.composites.cquantityunit.touch.CQuantityUnitView;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputContainerClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompVBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.composites.cdowngradeditem.touch.CDowngradedItemView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerVIView;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityEnum;
import fr.vif.vif5_7.stock.kernel.ui.composites.container.CContainerInfoCtrl;
import fr.vif.vif5_7.stock.kernel.ui.composites.container.touch.CContainerInfoTouchView;


/**
 * FProduction Output Scrap viewer.
 * 
 * @author kl
 */
public class FProductionOutputCompVView extends StandardTouchViewer<FProductionOutputCompVBean> implements
        FProductionOutputContainerVIView {

    private CCommentTouchView       commentTouchView;
    private CDowngradedItemView     downgradedItemView;
    private TouchInputTextField     itfBatch;
    private CContainerInfoTouchView itfContainer;
    private CContainerInfoTouchView itfUpperContainer;
    private TouchInputTextField     locationTouchView;
    private CQuantityUnitView       qtty1;

    private CQuantityUnitView       qtty2;

    private JSeparator              separator;

    /**
     * Default constructor.
     */
    public FProductionOutputCompVView() {
        super();
        initialize();
    }

    @Override
    public void addContainerListener(final DialogChangeListener dialogChangeListener,
            final DisplayListener displayListener) {
        ((CContainerInfoCtrl) getItfContainer().getController()).setDialogChangeListener(dialogChangeListener);
        ((CContainerInfoCtrl) getItfContainer().getController()).setDisplayListener(displayListener);

        ((CContainerInfoCtrl) getItfUpperContainer().getController()).setDialogChangeListener(dialogChangeListener);
        ((CContainerInfoCtrl) getItfUpperContainer().getController()).setDisplayListener(displayListener);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableComponents(final boolean creationMode) {
        super.enableComponents(creationMode);
        if (getModel().getBean() == null
                || getModel().getBean().getBBean() == null
                || getModel().getBean().getBBean().getOperationItemBean() == null
                || getModel().getBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                        .equals(ManagementType.ARCHIVED)) {
            disableComponents();
        } else {
            if (OutputEntityType.BATCH_ITEM.equals(getModel().getBean().getOutputItemParameters().getEntityType())) {
                // Batch item data entry
                getItfContainer().setEnabled(false);
                getItfUpperContainer().setEnabled(false);
                if (getModel().getBean().getEntity().getStockItemBean().getGeneralItemInfos().getStockByBatch()) {
                    getItfBatch().setEnabled(true);
                } else {
                    getItfBatch().setEnabled(false);
                }

                if (MONatureComplementaryWorkshop.MANUFACTURING.equals(getModel().getBean().getBBean()
                        .getNatureComplementaryWorkshop())) {
                    getDowngradedItemView().enableComponents(true);
                } else {
                    getDowngradedItemView().disableComponents();
                }

            } else if (OutputEntityType.CONTAINER
                    .equals(getModel().getBean().getOutputItemParameters().getEntityType())) {
                // Container data entry
                getItfContainer().setEnabled(true);
                if (getModel().getBean().getEntity().getStockItemBean().getGeneralItemInfos().getStockByBatch()) {
                    getItfBatch().setEnabled(true);
                } else {
                    getItfBatch().setEnabled(false);
                }
                if (MONatureComplementaryWorkshop.MANUFACTURING.equals(getModel().getBean().getBBean()
                        .getNatureComplementaryWorkshop())) {
                    getDowngradedItemView().enableComponents(true);
                } else {
                    getDowngradedItemView().disableComponents();
                }

                getItfUpperContainer().setEnabled(false);

                if (OutputContainerClosingType.AUTOMATIC.equals(getModel().getBean().getOutputItemParameters()
                        .getContainerClosingType())) {
                    getItfContainer().setEnabled(false);
                } else if (OutputContainerClosingType.MANUAL.equals(getModel().getBean().getOutputItemParameters()
                        .getContainerClosingType())
                        || OutputContainerClosingType.CAPACITY.equals(getModel().getBean().getOutputItemParameters()
                                .getContainerClosingType())) {
                    if (EntityConstants.NEW.equals(getModel().getBean().getEntity().getEntityLocationBean()
                            .getEntityLocationKey().getContainerNumber())) {
                        getItfContainer().setEnabled(true);
                    } else {
                        getItfContainer().setEnabled(false);
                    }
                    // getItfContainer().setEnabled(true);
                }
            } else if (OutputEntityType.CONTAINER_UPPER_CONTAINER.equals(getModel().getBean().getOutputItemParameters()
            // Container and upper container data entry
                    .getEntityType())) {
                getItfContainer().setEnabled(true);
                if (getModel().getBean().getEntity().getStockItemBean().getGeneralItemInfos().getStockByBatch()) {
                    getItfBatch().setEnabled(true);
                } else {
                    getItfBatch().setEnabled(false);
                }

                if (MONatureComplementaryWorkshop.MANUFACTURING.equals(getModel().getBean().getBBean()
                        .getNatureComplementaryWorkshop())) {
                    getDowngradedItemView().enableComponents(true);
                } else {
                    getDowngradedItemView().disableComponents();
                }

                if (EntityConstants.NEW.equals(getModel().getBean().getEntity().getEntityLocationBean()
                        .getEntityLocationKey().getUpperContainerNumber())) {
                    getItfUpperContainer().setEnabled(true);
                } else {
                    getItfUpperContainer().setEnabled(false);
                }
                if (OutputContainerClosingType.AUTOMATIC.equals(getModel().getBean().getOutputItemParameters()
                        .getContainerClosingType())) {
                    getItfContainer().setEnabled(false);
                } else if (OutputContainerClosingType.MANUAL.equals(getModel().getBean().getOutputItemParameters()
                        .getContainerClosingType())) {
                    getItfContainer().setEnabled(true);
                }
            }
            if (getModel().getBean().getEntity().getStockItemBean().getEnableFields().getFirstQty()) {
                getQtty1().enableComponents(true);
            } else {
                getQtty1().disableComponents();
            }
            if (getModel().getBean().getEntity().getStockItemBean().getEnableFields().getSecondQty()) {
                getQtty2().enableComponents(true);
            } else {
                getQtty2().disableComponents();
            }
        }
    }

    /**
     * Get the comment touch view.
     * 
     * @return the comment touch view.
     */
    public CCommentTouchView getCommentTouchView() {
        if (commentTouchView == null) {
            commentTouchView = new CCommentTouchView();
            commentTouchView.setBounds(0, 199, 866, 60);
            commentTouchView.setBeanProperty("comment");
        }
        return commentTouchView;
    }

    /**
     * 
     * Get the substituteitem view.
     * 
     * @return the substitute item view
     */
    public CDowngradedItemView getDowngradedItemView() {
        if (downgradedItemView == null) {
            downgradedItemView = new CDowngradedItemView();
            downgradedItemView.setBounds(5, 100, 269, 96);
            downgradedItemView.setBeanProperty("entity." + CEntityEnum.ITEM_CL.getValue());
        }
        return downgradedItemView;
    }

    /**
     * {@inheritDoc}
     */
    public TouchInputTextField getItfBatch() {
        if (itfBatch == null) {
            itfBatch = new TouchInputTextField(String.class, "15", I18nClientManager.translate(StockKernel.T29224));
            itfBatch.setBounds(285, 91, 319, 95);
            itfBatch.setTextLabel(I18nClientManager.translate(StockKernel.T29224));
            itfBatch.setBeanProperty("entity." + CEntityEnum.BATCH.getValue());
            itfBatch.setResettable(true);
        }
        return itfBatch;
    }

    /**
     * {@inheritDoc}
     */
    public CContainerInfoTouchView getItfContainer() {
        if (itfContainer == null) {
            itfContainer = new CContainerInfoTouchView(String.class, "10",
                    I18nClientManager.translate(StockKernel.T29089));
            itfContainer.setTextLabel(I18nClientManager.translate(StockKernel.T29089, false));
            itfContainer.setBounds(10, 10, 219, 70);
            itfContainer.setNumKeyboardForAlphaFields(true);
            itfContainer.setBeanProperty("entity." + CEntityEnum.CONTAINER_NUMBER.getValue());
        }
        return itfContainer;
    }

    /**
     * {@inheritDoc}
     */
    public CContainerInfoTouchView getItfUpperContainer() {
        if (itfUpperContainer == null) {
            itfUpperContainer = new CContainerInfoTouchView(String.class, "10",
                    I18nClientManager.translate(StockKernel.T29089));
            itfUpperContainer.setTextLabel(I18nClientManager.translate(StockKernel.T30034, false));
            itfUpperContainer.setBounds(263, 10, 226, 70);
            itfUpperContainer.setNumKeyboardForAlphaFields(true);
            itfUpperContainer.setBeanProperty("entity." + CEntityEnum.UPPER_CONTAINER_NUMBER.getValue());
        }
        return itfUpperContainer;
    }

    /**
     * 
     * Get the location touch view. Not visible
     * 
     * @return the location touch view
     */
    public TouchInputTextField getLocationTouchView() {
        if (locationTouchView == null) {
            locationTouchView = new TouchInputTextField(String.class, "10",
                    I18nClientManager.translate(StockKernel.T29089));
            locationTouchView.setBeanProperty("entity." + CEntityEnum.LOCATION_ID.getValue());
            locationTouchView.setBounds(0, 0, 120, 30);
            locationTouchView.setVisible(false);
            locationTouchView.setHelpBrowserTriad(new BrowserMVCTriad(FLocationBModel.class, FLocationBView.class,
                    FLocationBCtrl.class));
            locationTouchView.setUseFieldHelp(true);

        }
        return locationTouchView;
    }

    /**
     * 
     * Get qtty1.
     * 
     * @return qtty1
     */
    public CQuantityUnitView getQtty1() {
        if (qtty1 == null) {
            qtty1 = new CQuantityUnitView();
            qtty1.setBounds(614, 38, 246, 70);
            qtty1.setBeanProperty("entity.stockItemBean.keyboardQties.firstQty");
            qtty1.setDescription(I18nClientManager.translate(GenKernel.T210, false) + " 1");
        }
        return qtty1;
    }

    /**
     * Get qtty2.
     * 
     * @return qtty2
     */
    public CQuantityUnitView getQtty2() {
        if (qtty2 == null) {
            qtty2 = new CQuantityUnitView();
            qtty2.setBounds(614, 110, 247, 70);
            qtty2.setBeanProperty("entity.stockItemBean.keyboardQties.secondQty");
            qtty2.setDescription(I18nClientManager.translate(GenKernel.T210, false) + " 2");
        }
        return qtty2;
    }

    /**
     * Get the separator.
     * 
     * @return the separator
     */
    public JSeparator getSeparator() {
        if (separator == null) {
            separator = new JSeparator();
            separator.setOrientation(SwingConstants.VERTICAL);
            separator.setBounds(608, 10, 21, 181);
        }
        return separator;
    }

    /**
     * {@inheritDoc}
     */
    public FLocationBBean openLocationHelp(final DisplayListener parent, final FLocationSBean sBean) {
        FLocationBBean bBean = null;
        getLocationTouchView().setInitialHelpSelection(sBean);
        bBean = openFieldHelpDialog(I18nClientManager.translate(ProductionMo.T29601, false), getLocationTouchView(),
                new FLocationBBean(), parent);
        return bBean;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        setLayout(null);
        setPreferredSize(new Dimension(870, 768));
        add(getCommentTouchView());
        add(getItfContainer());
        add(getItfBatch());
        add(getQtty1());
        add(getQtty2());
        add(getDowngradedItemView());
        add(getSeparator());
        add(getItfUpperContainer());
        add(getLocationTouchView());
    }

}

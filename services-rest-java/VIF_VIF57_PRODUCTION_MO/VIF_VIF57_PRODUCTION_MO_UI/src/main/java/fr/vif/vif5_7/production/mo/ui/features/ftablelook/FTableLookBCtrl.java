/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookBCtrl.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;
import fr.vif.vif5_7.production.mo.business.services.features.tablelook.FTableLookCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.User;


/**
 * Ftable look Browser Controller.
 * 
 * @author kl
 */
public class FTableLookBCtrl extends AbstractBrowserController<FTableLookBBean> {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FTableLookBCtrl.class);

    private FTableLookCBS       ftableLookCBS;

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        FTableLookSBean sBean = new FTableLookSBean();
        sBean.setUserVIf(User.VIF.getValue().equals(getIdCtx().getLogin()));
        return sBean;
    }

    /**
     * Gets the ftableLookCBS.
     * 
     * @return the ftableLookCBS.
     */
    public FTableLookCBS getFtableLookCBS() {
        return ftableLookCBS;
    }

    /**
     * Sets the ftableLookCBS.
     * 
     * @param ftableLookCBS ftableLookCBS.
     */
    public void setFtableLookCBS(final FTableLookCBS ftableLookCBS) {
        this.ftableLookCBS = ftableLookCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FTableLookBBean convert(final Object bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }

        FTableLookBBean bBean = null;
        if (bean != null && bean instanceof FTableLookVBean) {
            bBean = new FTableLookBBean((FTableLookVBean) bean);
        } else {
            bBean = new FTableLookBBean();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")=" + bBean);
        }
        return bBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FTableLookBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FTableLookBBean> bBeans = new ArrayList<FTableLookBBean>();

        try {
            bBeans = getFtableLookCBS().queryElements(getIdCtx(), (FTableLookSBean) selection, startIndex, rowNumber);
        } catch (BusinessException e) {
            LOGGER.info("P - BusinessException e=" + e);
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + bBeans);
        }
        return bBeans;
    }

}

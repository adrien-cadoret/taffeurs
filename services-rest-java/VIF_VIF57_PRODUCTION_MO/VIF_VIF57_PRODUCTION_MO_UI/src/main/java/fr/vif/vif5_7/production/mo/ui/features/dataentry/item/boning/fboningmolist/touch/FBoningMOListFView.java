/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOListFView.java,v $
 * Created on 14 mar. 2013 by CJ
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist.touch;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.selection.GeneralSelectionView;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmolist.FBoningMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.touch.FMOListGSView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist.FBoningMOListFIView;


/**
 * MOList : Feature View.
 * 
 * @author cj
 */
public class FBoningMOListFView extends StandardTouchFeature<FBoningMOListBBean, FMOListVBean> implements
        FBoningMOListFIView {

    private FBoningMOListBView fBoningMOListBView = null;
    private FMOListGSView      fMOListGSView      = null;
    private FBoningMOListVView fBoningMOListVView = null;
    private TitlePanel         titlePanel;

    /**
     * Default constructor.
     * 
     */
    public FBoningMOListFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserView<FBoningMOListBBean> getBrowserView() {
        return getFBoningMOListBView();
    }

    /**
     * Gets the fMOListGSView.
     * 
     * @category getter
     * @return the fMOListGSView.
     */
    public final FMOListGSView getFMOListGSView() {
        if (fMOListGSView == null) {
            fMOListGSView = new FMOListGSView();
            Dimension d = new Dimension(870, 105);
            fMOListGSView.setPreferredSize(d);
            fMOListGSView.setMinimumSize(d);
            fMOListGSView.setMaximumSize(d);
        }
        return fMOListGSView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GeneralSelectionView getGeneralSelectionView() {
        return getFMOListGSView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView<FMOListVBean> getViewerView() {
        return getfboningOutputMOListVView();
    }

    /**
     * Sets the fMOListGSView.
     * 
     * @category setter
     * @param listGSView fMOListGSView.
     */
    public final void setFMOListGSView(final FMOListGSView listGSView) {
        fMOListGSView = listGSView;
    }

    /**
     * Initialisation of the browser feature.
     * 
     * @return FMOListBView
     */
    protected FBoningMOListBView getFBoningMOListBView() {
        if (fBoningMOListBView == null) {
            fBoningMOListBView = new FBoningMOListBView();
            Dimension d = new Dimension(870, 430);
            // fBoningMOListBView.getJTable().setMaximumSize(d);
            // fBoningMOListBView.getJTable().setMinimumSize(d);
            fBoningMOListBView.setMinimumSize(d);
            fBoningMOListBView.setPreferredSize(d);
            fBoningMOListBView.setMaximumSize(d);
        }
        return fBoningMOListBView;
    }

    /**
     * Initialisation of the viewer feature.
     * 
     * @return FProductionMOListVView
     */
    protected FBoningMOListVView getfboningOutputMOListVView() {
        if (fBoningMOListVView == null) {
            fBoningMOListVView = new FBoningMOListVView();
            Dimension d = new Dimension(870, 130);
            fBoningMOListVView.setPreferredSize(d);
            fBoningMOListVView.setMinimumSize(d);
            fBoningMOListVView.setMaximumSize(d);
        }
        return fBoningMOListVView;
    }

    /**
     * Gets the title panel.
     * 
     * @return a TitlePanel
     */
    protected TitlePanel getTitlePanel() {
        if (titlePanel == null) {
            titlePanel = new TitlePanel();
            titlePanel.setBorder(new MatteBorder(1, 0, 0, 0, Color.white));
            titlePanel.setTitle(I18nClientManager.translate(ProductionMo.T29509, false));
            Dimension d = new Dimension(870, 42);
            titlePanel.setPreferredSize(d);
            titlePanel.setMinimumSize(d);
            titlePanel.setMaximumSize(d);
        }
        return titlePanel;
    }

    /**
     * This Method Initializes this.
     * 
     */
    protected void initialize() {

        // setAutoscrolls(true);
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(getTitlePanel());
        this.add(getFMOListGSView());
        this.add(getFBoningMOListBView());
        this.add(getfboningOutputMOListVView());

    }

}

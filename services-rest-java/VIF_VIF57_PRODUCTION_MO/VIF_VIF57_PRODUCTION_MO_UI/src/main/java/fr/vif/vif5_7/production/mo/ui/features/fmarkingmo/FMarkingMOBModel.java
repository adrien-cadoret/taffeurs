/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOBModel.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.DateTimeFormatEnum;
import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOBBean;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;


/**
 * MarkingMO : Browser Model.
 * 
 * @author lmo
 */
public class FMarkingMOBModel extends BrowserModel<FMarkingMOBBean> {

    private Format formatQty = new Format(Alignment.RIGHT_ALIGN, MOUIConstants.TOUCHSCREEN_BG_BROWSER,
                                     StandardColor.TOUCHSCREEN_FG_BROWSER,
                                     StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE, "11");

    /**
     * Default constructor.
     * 
     */
    public FMarkingMOBModel() {
        super();
        setBeanClass(FMarkingMOBBean.class);
        // Add columns
        // MO state
        addColumn(new BrowserColumn("", FMarkingMOUIConstants.BBEAN_MO_STATE, 20));
        // "Hour"
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29437), FMarkingMOUIConstants.BBEAN_DATE,
                50, DateHelper.getTimeFormatForLocale(DateTimeFormatEnum.HH_MM)));
        // "MO"
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T25925),
                FMarkingMOUIConstants.BBEAN_CHRONO, 40));
        // "Item"
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29519),
                FMarkingMOUIConstants.BBEAN_ITEM_CODE, 80));
        // "Label"
        addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T1863), FMarkingMOUIConstants.BBEAN_OF_LABEL,
                300));
        // (left) "To do"
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29438, false),
                FMarkingMOUIConstants.BBEAN_LEFT_QUANTITY, 60, formatQty));

        setFetchSize(20);
        setFirstFetchSize(40);
        setSelectionEnabled(false);
    }

}

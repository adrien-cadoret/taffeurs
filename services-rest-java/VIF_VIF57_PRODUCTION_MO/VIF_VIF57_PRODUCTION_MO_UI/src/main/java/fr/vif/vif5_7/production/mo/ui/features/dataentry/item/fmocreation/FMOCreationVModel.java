/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationVModel.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FMOCreationVBean;


/**
 * MO creation viewer model.
 * 
 * @author glc
 */
public class FMOCreationVModel extends ViewerModel<FMOCreationVBean> {

    public static final String QUANTITY_UNIT = "quantityUnit";

    /**
     * Simple constructor.
     */
    public FMOCreationVModel() {
        super();
        setBeanClass(FMOCreationVBean.class);
    }

}

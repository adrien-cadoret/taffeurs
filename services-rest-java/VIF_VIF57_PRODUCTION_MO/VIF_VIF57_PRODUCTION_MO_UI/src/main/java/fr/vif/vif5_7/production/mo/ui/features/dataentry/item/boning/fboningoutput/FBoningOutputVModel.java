/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputVModel.java,v $
 * Created on 25 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutputcontainer.FBoningOutputVBean;


/**
 * ProductionOutputContainer : Viewer Model.
 * 
 * @author vr
 */
public class FBoningOutputVModel extends ViewerModel<FBoningOutputVBean> {

    /**
     * Default constructor.
     * 
     */
    public FBoningOutputVModel() {
        super();
        setBeanClass(FBoningOutputVBean.class);
    }

}

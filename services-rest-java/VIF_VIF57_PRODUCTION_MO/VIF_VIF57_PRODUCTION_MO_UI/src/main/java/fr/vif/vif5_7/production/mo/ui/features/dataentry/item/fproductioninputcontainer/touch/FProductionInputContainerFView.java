/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputContainerFView.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer.touch;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.touch.FProductionBView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerFIView;
import fr.vif.vif5_7.workshop.device.ui.composites.devices.scale.touch.CPerpetualScaleWeightTouchView;


/**
 * FProductionInput Feature view.
 * 
 * @author vr
 */
public class FProductionInputContainerFView extends StandardTouchFeature implements FProductionInputContainerFIView {

    private FProductionBView               bView;
    private CPerpetualScaleWeightTouchView scaleView;
    private TitlePanel                     subTitlePanel;
    private FProductionInputContainerVView vView;

    /**
     * Default contructor.
     */
    public FProductionInputContainerFView() {
        super();
        initialize();
    }

    /**
     * Gets the bView.
     * 
     * @category getter
     * @return the bView.
     */
    public FProductionBView getBView() {
        if (bView == null) {
            bView = new FProductionBView();
            bView.setBounds(0, 138, 867, 162);
        }
        return bView;
    }

    /**
     * Get the weight composite.
     * 
     * @return the weight composite
     */
    public CPerpetualScaleWeightTouchView getScaleView() {
        if (scaleView == null) {
            scaleView = new CPerpetualScaleWeightTouchView();
            scaleView.setBounds(0, 0, 867, 100);
            scaleView.setOpaque(false);
        }
        return scaleView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView getViewerView() {
        return getVView();
    }

    /**
     * Gets the vView.
     * 
     * @category getter
     * @return the vView.
     */
    public FProductionInputContainerVView getVView() {
        if (vView == null) {
            vView = new FProductionInputContainerVView();
            vView.setBounds(0, 300, 870, 300);
        }
        return vView;
    }

    /**
     * Sets the bView.
     * 
     * @category setter
     * @param productionBView bView.
     */
    public void setBView(final FProductionBView productionBView) {
        bView = productionBView;
    }

    /**
     * {@inheritDoc}
     */
    public void setSubTitle(final String subTitle) {
        subTitlePanel.setTitle(subTitle);

    }

    /**
     * Get the subtitle panel.
     * 
     * @return the subtitile panel
     */
    private JPanel getSubTitle() {
        if (subTitlePanel == null) {
            subTitlePanel = new TitlePanel();
            subTitlePanel.setBounds(0, 101, 870, 40);
            subTitlePanel.setBorder(new MatteBorder(1, 0, 0, 2, Color.white));
            subTitlePanel.setPreferredSize(new Dimension(800, 40));
            subTitlePanel.setMinimumSize(new Dimension(500, 40));
        }
        return subTitlePanel;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setPreferredSize(new Dimension(870, 768));
        setLayout(null);
        add(getSubTitle());
        add(getBView());
        add(getVView());
        add(getScaleView());
        // setErrorPanelAlwaysVisible(true);
    }
}

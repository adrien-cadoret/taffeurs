/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMSOCreationBCtrl.java,v $
 * Created on 19 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaReturnInitBean;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fboningmsocreation.FBoningMSOCreationCBS;


/**
 * Controller for browser on items for MSO creation.
 * 
 * @author xg
 */
public class FBoningMSOCreationBCtrl extends AbstractBrowserController<FBoningMSOCreationVBean> {

    public static final String    LOGICAL_WORKSTATION = "logicalWorkstation";
    public static final String    ORIGIN_MO_BEAN      = "originMOBean";

    /** LOGGER. */
    private static final Logger   LOGGER              = Logger.getLogger(FBoningMSOCreationBCtrl.class);

    private FBoningMSOCreationCBS fboningmSOCreationCBS;

    /**
     * Gets the fboningmSOCreationCBS.
     * 
     * @return the fboningmSOCreationCBS.
     */
    public FBoningMSOCreationCBS getFboningmSOCreationCBS() {
        return fboningmSOCreationCBS;
    }

    /**
     * Sets the fboningmSOCreationCBS.
     * 
     * @param fboningmSOCreationCBS fboningmSOCreationCBS.
     */
    public void setFboningmSOCreationCBS(final FBoningMSOCreationCBS fboningmSOCreationCBS) {
        this.fboningmSOCreationCBS = fboningmSOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FBoningMSOCreationVBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FBoningMSOCreationVBean> list = new ArrayList<FBoningMSOCreationVBean>();
        MOBean originMOBean = (MOBean) getSharedContext().get(Domain.DOMAIN_PARENT, ORIGIN_MO_BEAN);

        CriteriaReturnInitBean criteriaReturnInitBean = new CriteriaReturnInitBean();
        try {
            criteriaReturnInitBean = getFboningmSOCreationCBS().getCriteriaReturnInitBean(getIdCtx(), originMOBean);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        FBoningMSOCreationVBean vbean = new FBoningMSOCreationVBean(originMOBean.getMoKey(), originMOBean.getLabel(),
                originMOBean.getItemCLs(), originMOBean.getFabrication(), new QuantityUnit(originMOBean
                        .getMoQuantityUnit().getEffectiveQuantity(), originMOBean.getMoQuantityUnit()
                        .getEffectiveUnit()), criteriaReturnInitBean, new CodeLabel("", ""));
        list.add(vbean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return list;
    }

}

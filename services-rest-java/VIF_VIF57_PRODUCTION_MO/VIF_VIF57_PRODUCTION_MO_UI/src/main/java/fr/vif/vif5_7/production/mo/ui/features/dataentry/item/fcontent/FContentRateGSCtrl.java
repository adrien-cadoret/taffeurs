/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FContentRateGSCtrl.java,v $
 * Created on 15 Jul 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent;


import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.selection.AbstractGeneralSelectionController;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateSBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent.touch.FContentRateGSView;


/**
 * General Selection Controller for content feature.
 * 
 * @author cj
 */
public class FContentRateGSCtrl extends AbstractGeneralSelectionController {

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();
        FContentRateSBean sBeanUp = (FContentRateSBean) getSharedContext().get(Domain.DOMAIN_PARENT,
                FContentRateFCtrl.FEATURE_CONTENT_CODE);
        getModel().setBean(sBeanUp);
        if (sBeanUp.getItemId1() == null || sBeanUp.getItemId1().getCode() == null
                || sBeanUp.getItemId1().getCode().isEmpty()) {
            ((FContentRateGSView) getView()).removeItem1();
        }
        if (sBeanUp.getItemId2() == null || sBeanUp.getItemId2().getCode() == null
                || sBeanUp.getItemId2().getCode().isEmpty()) {
            ((FContentRateGSView) getView()).removeItem2();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException, UIErrorsException {
        if ("currentRate.qty".equals(beanProperty)) {
            fireDisplayRequested(new DisplayEvent(this, beanProperty, null));
            if ((Double) propertyValue <= 0) {
                throw new UIException(ProductionMo.T34270, ProductionMo.T34250);
            }
        }
    }
}

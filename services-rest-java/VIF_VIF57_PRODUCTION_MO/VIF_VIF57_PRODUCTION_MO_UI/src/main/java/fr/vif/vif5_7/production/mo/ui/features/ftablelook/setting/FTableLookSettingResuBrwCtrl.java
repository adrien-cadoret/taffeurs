/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingResuBrwCtrl.java,v $
 * Created on 5 août 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.button.ButtonListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.input.InputFlipFlopModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FSettingBean;


/**
 * Controller for action button of Flip Flop.
 * 
 * @author kl
 */
public class FTableLookSettingResuBrwCtrl extends AbstractBrowserController<FSettingBean> implements ButtonListener {
    /** LOGGER. */
    private static final Logger           LOGGER                = Logger.getLogger(FTableLookSettingResuBrwCtrl.class);
    private static final int              MINIMAL_NUMBER_COLUMN = 3;
    private static final int              WITH_FOR_2_COLUMNS    = 50;
    private static final int              WITH_40_PERCENT       = 40;
    private static final int              WITH_30_PERCENT       = 30;
    private static final int              WITH_20_PERCENT       = 20;
    private static final int              WITH_10_PERCENT       = 10;

    private FTableLookSettingFlipFlopCtrl ffCtrl;

    private FTableLookSettingVCtrl        viewerCtrl;

    /**
     * Default constructor.
     * 
     * @param ffCtrl FlipFlop controller.
     */
    public FTableLookSettingResuBrwCtrl(final FTableLookSettingFlipFlopCtrl ffCtrl) {
        this.ffCtrl = ffCtrl;
        getModel().setXlsExportEnabled(false);
        getModel().setAdvancedSearchEnabled(false);
    }

    /**
     * {@inheritDoc}
     */
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        String ref = event.getModel().getReference();
        int currentRow = getModel().getCurrentRowNumber();

        if (InputFlipFlopModel.BTN_REMOVE_REF.equals(ref)) {
            if (currentRow != -1) {

                BrowserModel<FSettingBean> selModel = getFfCtrl().getModel().getBrwSelModel();
                FSettingBean currentBean = getModel().getCurrentBean();
                if (currentBean.getIsCriterion()) {
                    for (FSettingBean fSettingBean : selModel.getBeans()) {
                        if (fSettingBean.getIsCriterion()) {
                            currentBean.setCodeCriterion("");
                            currentBean.setTitle(fSettingBean.getTitle());
                            break;
                        }
                    }

                }

                getFfCtrl().swapItem(getModel(), selModel);

                if (getFfCtrl().getModel().getUseFilter() ? getFfCtrl().applyFilter(
                        selModel.getBeanAt(selModel.getCurrentRowNumber())) : true) {
                    getFfCtrl().getSettingSelBrwCtrl().sortItems(selModel.getBeanAt(selModel.getCurrentRowNumber()));
                } else {
                    selModel.deleteBeanAt(selModel.getCurrentRowNumber());
                    if (selModel.getRowCount() > 0) {
                        selModel.setCurrentRowNumber(0);
                    }
                }
            }
        } else if (InputFlipFlopModel.BTN_REMOVE_ALL_REF.equals(ref)) {
            getModel().setCurrentRowNumber(0);
            BrowserModel<FSettingBean> selModel = ffCtrl.getModel().getBrwSelModel();
            while (getModel().getRowCount() > 0) {
                getFfCtrl().swapItem(getModel(), selModel);
                if (getFfCtrl().getModel().getUseFilter() ? ffCtrl.applyFilter(selModel.getBeanAt(selModel
                        .getCurrentRowNumber())) : true) {
                    getFfCtrl().getSettingSelBrwCtrl().sortItems(selModel.getBeanAt(selModel.getCurrentRowNumber()));
                } else {
                    selModel.deleteBeanAt(selModel.getCurrentRowNumber());
                }
            }
            if (selModel.getRowCount() > 0) {
                selModel.setCurrentRowNumber(0);
            }
        } else if (InputFlipFlopModel.BTN_UP_REF.equals(ref)) {
            if (currentRow > 0) {
                getFfCtrl().verticalMove(getModel(), currentRow, currentRow - 1);
            }
        } else if (InputFlipFlopModel.BTN_DOWN_REF.equals(ref)) {
            if (currentRow != -1 && currentRow < getModel().getRowCount() - 1) {
                getFfCtrl().verticalMove(getModel(), currentRow, currentRow + 1);
            }
        }

        getFfCtrl().updateValue(getModel().getBeans());

        getViewerCtrl().updateCurrentSettingBean();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * Gets the ffCtrl.
     * 
     * @return the ffCtrl.
     */
    public FTableLookSettingFlipFlopCtrl getFfCtrl() {
        return ffCtrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserModel<FSettingBean> getModel() {
        return getFfCtrl().getModel().getBrwResuModel();
    }

    /**
     * Gets the viewerCtrl.
     * 
     * @return the viewerCtrl.
     */
    public FTableLookSettingVCtrl getViewerCtrl() {
        return viewerCtrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final BrowserActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanged(event=" + event + ")");
        }

        super.selectedRowChanged(event);
        if (event.getSelectedRowNum() >= 0) {
            getViewerCtrl().updateCurrentSettingBean();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanged(event=" + event + ")");
        }
    }

    /**
     * Sets the ffCtrl.
     * 
     * @param ffCtrl ffCtrl.
     */
    public void setFfCtrl(final FTableLookSettingFlipFlopCtrl ffCtrl) {
        this.ffCtrl = ffCtrl;
    }

    /**
     * Sets the viewerCtrl.
     * 
     * @param viewerCtrl viewerCtrl.
     */
    public void setViewerCtrl(final FTableLookSettingVCtrl viewerCtrl) {
        this.viewerCtrl = viewerCtrl;
    }

    /**
     * Show columns for functionality bean : zone, title, width, font size.
     */
    public void showArrayColumns() {
        getModel().removeAllColumns();
        getModel().addColumn(
                new BrowserColumn(I18nClientManager.translate(ProductionMo.T34833), "zone", WITH_FOR_2_COLUMNS));
        getModel()
        .addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T2379), "title", WITH_FOR_2_COLUMNS));
        getModel().addColumn(
                new BrowserColumn(I18nClientManager.translate(ProductionMo.T37253), "sizeZone", WITH_10_PERCENT));
        getModel().addColumn(
                new BrowserColumn(I18nClientManager.translate(ProductionMo.T37254), "fontSizeToDisplayInArray",
                        WITH_20_PERCENT));
        getModel().setCurrentRowNumber(0);
    }

    /**
     * Show columns for functionality bean : zone, location.
     */
    public void showFunctionalityColumns() {
        getModel().removeAllColumns();
        getModel().addColumn(
                new BrowserColumn(I18nClientManager.translate(ProductionMo.T34833), "zone", WITH_FOR_2_COLUMNS));
        getModel().addColumn(
                new BrowserColumn(I18nClientManager.translate(ProductionMo.T39968), "buttonLocationToDisplayInArray",
                        WITH_10_PERCENT));
        getModel().setCurrentRowNumber(0);
    }

    /**
     * Sort items in the selection browser from the reference list.
     * 
     * @param item item to insert to the right place.
     */
    public void sortItems(final FSettingBean item) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - sortItems(item=" + item + ")");
        }

        List<FSettingBean> lstSave = ffCtrl.getModel().getLstBrwSelSave();
        List<FSettingBean> lstBeans = getModel().getBeans();
        int itemPosRef = lstSave.indexOf(item);
        int itemPos = Integer.MAX_VALUE;

        int index = getModel().getCurrentRowNumber() - 1;

        while (itemPos > itemPosRef && itemPos >= 0 && index >= 0) {
            itemPos = lstSave.indexOf(lstBeans.get(index));
            if (itemPos > itemPosRef) {
                ffCtrl.verticalMove(getModel(), index + 1, index);
            }
            index = index - 1;
        }

        int sizeList = lstBeans.size() - 1;

        for (int i = sizeList; i > 0; i--) {
            FSettingBean currentBean = lstBeans.get(i);
            FSettingBean beanBefore = lstBeans.get(i - 1);

            if (currentBean.getNligaff() < beanBefore.getNligaff()) {
                int nligAffCurrentBean = currentBean.getNligaff();
                int nligAffBeanBefore = beanBefore.getNligaff();

                currentBean.setNligaff(nligAffBeanBefore);
                beanBefore.setNligaff(nligAffCurrentBean);
            } else {
                break;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - sortItems(item=" + item + ")");
        }
    }

    /**
     * Search result from the browser model.
     * 
     * @return List of beans selected.
     */
    protected List<FSettingBean> getResult() {
        return getModel().getBeans();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FSettingBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        return new ArrayList<FSettingBean>();
    }
}

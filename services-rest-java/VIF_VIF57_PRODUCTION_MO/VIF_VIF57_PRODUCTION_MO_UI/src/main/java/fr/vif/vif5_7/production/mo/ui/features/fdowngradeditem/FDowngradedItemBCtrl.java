/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDowngradedItemBCtrl.java,v $
 * Created on 19 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.fdowngradeditem;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdowngradeditem.FDowngradedItemSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fdowngradeditem.FDowngradedItemCBS;


/**
 * SubstituteItem : Browser Controller.
 * 
 * @author gp
 */
public class FDowngradedItemBCtrl extends AbstractBrowserController<CodeLabel> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FDowngradedItemBCtrl.class);

    private FDowngradedItemCBS  fdowngradedItemCBS;

    /**
     * Default constructor.
     * 
     */
    public FDowngradedItemBCtrl() {
        super();
    }

    /**
     * Gets the fdowngradedItemCBS.
     * 
     * @category getter
     * @return the fdowngradedItemCBS.
     */
    public FDowngradedItemCBS getFdowngradedItemCBS() {
        return this.fdowngradedItemCBS;
    }

    /**
     * Sets the fdowngradedItemCBS.
     * 
     * @category setter
     * @param fdowngradedItemCBS fdowngradedItemCBS.
     */
    public void setFdowngradedItemCBS(final FDowngradedItemCBS fdowngradedItemCBS) {
        this.fdowngradedItemCBS = fdowngradedItemCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<CodeLabel> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<CodeLabel> lst = null;
        try {
            lst = getFdowngradedItemCBS().queryElements(getIdCtx(), (FDowngradedItemSBean) selection, startIndex,
                    rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return lst;
    }
}

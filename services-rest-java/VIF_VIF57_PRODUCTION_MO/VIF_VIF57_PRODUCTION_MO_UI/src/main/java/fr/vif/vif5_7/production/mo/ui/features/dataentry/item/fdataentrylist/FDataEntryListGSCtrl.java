/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListGSCtrl.java,v $
 * Created on 16 févr. 2016 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.selection.AbstractGeneralSelectionController;


/**
 * General selection controller for data entry list feature.
 *
 * @author cj
 */
public class FDataEntryListGSCtrl extends AbstractGeneralSelectionController {

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException, UIErrorsException {

    }

}

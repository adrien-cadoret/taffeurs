/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOListBIView.java,v $
 * Created on 15 mars 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist;


/**
 * Interface for Browser View for boning mo list.
 * 
 * @author cj
 */
public interface FBoningMOListBIView {

    /**
     * init to call after controller init !
     */
    public void initialize();

}

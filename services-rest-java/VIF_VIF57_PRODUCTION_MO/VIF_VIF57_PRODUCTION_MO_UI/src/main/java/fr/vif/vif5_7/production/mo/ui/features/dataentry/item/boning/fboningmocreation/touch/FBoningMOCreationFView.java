/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOCreationFView.java,v $
 * Created on 18 avr. 13 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation.touch;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;

import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmocreation.FBoningMOCreationVBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationFIView;


/**
 * MO creation feature view.
 * 
 * @author glc
 */
public class FBoningMOCreationFView extends StandardTouchFeature<Object, FBoningMOCreationVBean> implements
        FMOCreationFIView {

    private TouchBrowser           fMOCreationBView = null;

    private FBoningMOCreationVView fMOCreationVView = null;

    private TitlePanel             subTitlePanel    = null;

    /**
     * Constructor.
     */
    public FBoningMOCreationFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public BrowserView<Object> getBrowserView() {
        return getFMOCreationBView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getSubTitle() {
        return getSubTitlePanel().getTitle();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView<FBoningMOCreationVBean> getViewerView() {
        return getFBoningMOCreationVView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSubTitle(final String subTitle) {
        getSubTitlePanel().setTitle(subTitle);
    }

    /**
     * Initialisation of the viewer feature.
     * 
     * @return FMSOCreationVView
     */
    private FBoningMOCreationVView getFBoningMOCreationVView() {
        if (fMOCreationVView == null) {
            fMOCreationVView = new FBoningMOCreationVView();
        }
        return fMOCreationVView;
    }

    /**
     * Initialisation of the browser feature.
     * 
     * @return FMOCreationBView
     */
    private TouchBrowser getFMOCreationBView() {
        if (fMOCreationBView == null) {
            fMOCreationBView = new TouchBrowser();
            fMOCreationBView.setVisible(false);
        }
        return fMOCreationBView;
    }

    /**
     * Gets the subTitlePanel.
     * 
     * @category getter
     * @return the subTitlePanel.
     */
    private TitlePanel getSubTitlePanel() {
        if (subTitlePanel == null) {
            subTitlePanel = new TitlePanel();
            subTitlePanel.setBorder(new MatteBorder(1, 0, 0, 1, Color.white));
            subTitlePanel.setTitle(I18nClientManager.translate(ProductionMo.T29492, false));
            subTitlePanel.setPreferredSize(new Dimension(getWidth(), 42));
            subTitlePanel.setMinimumSize(new Dimension(getWidth(), 42));
        }
        return subTitlePanel;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 2;
        gridBagConstraints2.fill = GridBagConstraints.BOTH;
        gridBagConstraints2.weightx = 1.0;
        gridBagConstraints2.weighty = 1.0;
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 1;
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 1.0;

        this.setLayout(new GridBagLayout());
        this.add(getSubTitlePanel(), gridBagConstraints);
        this.add(getFMOCreationBView(), gridBagConstraints1);
        this.add(getFBoningMOCreationVView(), gridBagConstraints2);
    }
}

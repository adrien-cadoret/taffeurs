/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOListBModel.java,v $
 * Created on 15 mars 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmolist.FBoningMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmolist.FBoningMOListBViewEnum;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;


/**
 * Browser model for boning mo list.
 * 
 * @author cj
 */
public class FBoningMOListBModel extends BrowserModel<FBoningMOListBBean> {

    private Format formatQty = new Format(Alignment.RIGHT_ALIGN, MOUIConstants.TOUCHSCREEN_BG_BROWSER,
                                     StandardColor.TOUCHSCREEN_FG_BROWSER,
                                     StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE, "11");

    /**
     * Default constructor.
     * 
     */
    public FBoningMOListBModel() {
        super();
        setBeanClass(FBoningMOListBBean.class);
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29437, false),
                FBoningMOListBViewEnum.DATE.getValue(), 55, "HH:mm"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T19824, false),
                FBoningMOListBViewEnum.CHRONO.getValue(), 90, "########"));
        addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T1863, false),
                FBoningMOListBViewEnum.LABEL.getValue(), 305));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T34018, false),
                FBoningMOListBViewEnum.CUSTOMER.getValue(), 190));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29438, false),
                FBoningMOListBViewEnum.FORMATTED_QUANTITY.getValue(), 130, formatQty));
        addColumn(new BrowserColumn("", "mobean", 45));

        setFullyFetched(true);
        setSelectionEnabled(false);
        setAdvancedSearchEnabled(false);
        setSearchEnabled(false);
        setXlsExportEnabled(false);
        setFetchSize(10);
        setFirstFetchSize(15);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FBoningMOListBBean bean) {

        Format format = super.getCellFormat(rowNum, property, cellContent, bean);

        if (bean.getIsFinished()) {
            format.setForegroundColor(StandardColor.GRAY);
        }

        return format;
    }

}

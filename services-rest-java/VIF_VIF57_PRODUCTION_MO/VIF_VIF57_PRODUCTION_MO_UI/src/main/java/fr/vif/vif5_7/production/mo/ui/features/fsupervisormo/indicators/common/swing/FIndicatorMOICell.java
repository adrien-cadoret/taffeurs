/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOICell.java,v $
 *  * Created on 6 Fev 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.swing;


import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOValues;


/**
 * Interface of the Jtable celle.
 * 
 * @author jd
 */
public interface FIndicatorMOICell {

    /**
     * Set the values.
     * 
     * @param bbean browser bean.
     */
    public void setValues(final IndicatorMOValues bbean);

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingSelBrwCtrl.java,v $
 * Created on 5 août 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.button.ButtonListener;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.input.InputFlipFlopModel;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaCompleteBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FSettingBean;


/**
 * Controller for action button of Flip Flop.
 * 
 * @author kl
 */
public class FTableLookSettingSelBrwCtrl extends AbstractBrowserController<FSettingBean> implements ButtonListener {
    /** LOGGER. */
    private static final Logger           LOGGER = Logger.getLogger(FTableLookSettingSelBrwCtrl.class);

    private FTableLookSettingFlipFlopCtrl ffCtrl;

    private FTableLookSettingVCtrl        viewerCtrl;

    /**
     * Default constructor.
     * 
     * @param ffCtrl FlipFlop controller.
     */
    public FTableLookSettingSelBrwCtrl(final FTableLookSettingFlipFlopCtrl ffCtrl) {
        this.ffCtrl = ffCtrl;
        getModel().setXlsExportEnabled(false);
        getModel().setAdvancedSearchEnabled(false);
    }

    /**
     * {@inheritDoc}
     */
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        String ref = event.getModel().getReference();

        if (InputFlipFlopModel.BTN_ADD_REF.equals(ref)) {
            if (getModel().getCurrentRowNumber() >= 0) {
                FSettingBean currentBean = getModel().getCurrentBean();
                if (currentBean.getIsCriterion()) {
                    CriteriaCompleteBean criteria = getViewerCtrl().openCriteriaHelp();
                    if (criteria == null) {
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("E - buttonAction(event=" + event + ")");
                        }
                        return;
                    }
                    currentBean.setCodeCriterion(criteria.getCriteriaId());
                    currentBean.setTitle(criteria.getCriteriaId());
                }
                getFfCtrl().swapItem(getModel(), getFfCtrl().getModel().getBrwResuModel());
            }
        } else if (InputFlipFlopModel.BTN_ADD_ALL_REF.equals(ref)) {
            getModel().setCurrentRowNumber(0);
            while (getModel().getRowCount() > 0) {
                getFfCtrl().swapItem(getModel(), getFfCtrl().getModel().getBrwResuModel());
            }
        }

        BrowserModel<FSettingBean> browserModel = getFfCtrl().getModel().getBrwResuModel();
        getFfCtrl().updateValue(browserModel.getBeans());

        getViewerCtrl().updateCurrentSettingBean();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }

        super.dialogValidated(event);
        getFfCtrl().getModel().setLstBrwSelSave(getModel().getBeans());
        getFfCtrl().buildListUsingFilter();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        return getFfCtrl().getDefaultInitialSelection();
    }

    /**
     * Gets the ffCtrl.
     * 
     * @return the ffCtrl.
     */
    public FTableLookSettingFlipFlopCtrl getFfCtrl() {
        return ffCtrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserModel<FSettingBean> getModel() {
        return getFfCtrl().getModel().getBrwSelModel();
    }

    /**
     * Gets the viewerCtrl.
     * 
     * @return the viewerCtrl.
     */
    public FTableLookSettingVCtrl getViewerCtrl() {
        return viewerCtrl;
    }

    /**
     * Sets the ffCtrl.
     * 
     * @param ffCtrl ffCtrl.
     */
    public void setFfCtrl(final FTableLookSettingFlipFlopCtrl ffCtrl) {
        this.ffCtrl = ffCtrl;
    }

    /**
     * Sets the viewerCtrl.
     * 
     * @param viewerCtrl viewerCtrl.
     */
    public void setViewerCtrl(final FTableLookSettingVCtrl viewerCtrl) {
        this.viewerCtrl = viewerCtrl;
    }

    /**
     * /** Sort items in the selection browser from the reference list.
     * 
     * @param item item to insert to the right place.
     */
    public void sortItems(final FSettingBean item) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - sortItems(item=" + item + ")");
        }

        List<FSettingBean> lstSave = getFfCtrl().getModel().getLstBrwSelSave();
        List<FSettingBean> lstBeans = getModel().getBeans();
        int itemPosRef = lstSave.indexOf(item);
        int itemPos = Integer.MAX_VALUE;

        int index = getModel().getCurrentRowNumber() - 1;
        while (itemPos > itemPosRef && itemPos >= 0 && index >= 0) {
            itemPos = lstSave.indexOf(lstBeans.get(index));
            if (itemPos > itemPosRef) {
                getFfCtrl().verticalMove(getModel(), index + 1, index);
            }
            index = index - 1;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - sortItems(item=" + item + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FSettingBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        return getFfCtrl().queryBrwSelection(selection);
    }

}

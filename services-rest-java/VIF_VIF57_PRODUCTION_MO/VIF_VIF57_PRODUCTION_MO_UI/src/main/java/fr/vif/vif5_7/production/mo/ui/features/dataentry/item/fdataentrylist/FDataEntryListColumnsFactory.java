/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_STOCK_KERNEL_UI
 * File : $RCSfile: FDataEntryListColumnsFactory.java,v $
 * Created on 12 Mar 2009 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelArray;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.CodeFieldModel;


/**
 * Construct a Browser Model for the movements list from the nature movement type.
 * 
 * @author vr
 */
public final class FDataEntryListColumnsFactory {
    /** LOGGER. */
    private static final Logger LOGGER       = Logger.getLogger(FDataEntryListColumnsFactory.class);

    private static final Format FORMAT       = new Format(Alignment.RIGHT_ALIGN);
    private static final int    DEFAULT_SIZE = 10;

    /**
     * Constructor.
     */
    private FDataEntryListColumnsFactory() {

    }

    /**
     * Get the browser model to use.
     * 
     * @param touchModel touchModel
     * @param type type of the movement.
     * @return a browser model.
     */
    public static List<BrowserColumn> getColumns(final InputEntityType type, final TouchModel touchModel) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getColumns(type=" + type + ", touchModel=" + touchModel + ")");
        }

        TouchModelFunction touchModelFunction = touchModel
                .getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_BATCH_ITEM);
        if (InputEntityType.CONTAINER.equals(type)) {
            touchModelFunction = touchModel.getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_NSC);
        }

        List<BrowserColumn> browserColumns = new ArrayList<BrowserColumn>();
        int nbValueCrit = 0;
        int nbValueCalculatedCrit = 0;

        for (TouchModelArray touchModelArray : touchModelFunction.getTouchModelArrays()) {
            CodeFieldModel codeFieldModel = CodeFieldModel.getCodeFieldModel(touchModelArray.getChamp());
            if (codeFieldModel != null) {
                switch (codeFieldModel) {
                    case RANG:
                        browserColumns.add(getRank(touchModelArray.getLabel()));
                        break;
                    case DATMVT:
                        browserColumns.add(getCreationDateTime(touchModelArray.getLabel()));
                        break;
                    case NSC:
                        browserColumns.add(getContainer(touchModelArray.getLabel()));
                        break;
                    case CART:
                        browserColumns.add(getItem(touchModelArray.getLabel()));
                        break;
                    case BATCH:
                        browserColumns.add(getBatch(touchModelArray.getLabel()));
                        break;
                    case QTE_1:
                        browserColumns.add(getFirstUnit(touchModelArray.getLabel()));
                        break;
                    case QTE_2:
                        browserColumns.add(getSecondUnit(touchModelArray.getLabel()));
                        break;
                    case DEPOT:
                        browserColumns.add(getDeposit(touchModelArray.getLabel()));
                        break;
                    case EMP:
                        browserColumns.add(getLocation(touchModelArray.getLabel()));
                        break;
                    case HEURMVT:
                        browserColumns.add(getCreationHour(touchModelArray.getLabel()));
                        break;
                    case CRITERE:
                        nbValueCrit = nbValueCrit + 1;
                        browserColumns.add(getValueCrit(touchModelArray.getLabel(), nbValueCrit));
                        break;
                    case CRITERE_RESTITUE:
                        nbValueCalculatedCrit = nbValueCalculatedCrit + 1;
                        browserColumns.add(getValueCalculatedCrit(touchModelArray.getLabel(), nbValueCalculatedCrit));
                        break;
                    case NSCP:
                        browserColumns.add(getUpperContainer(touchModelArray.getLabel()));
                        break;
                    case MOTIF:
                        browserColumns.add(getMotif(touchModelArray.getLabel()));
                        break;
                    case PACKAGING:
                        browserColumns.add(getPackaging(touchModelArray.getLabel()));
                        break;
                    case UPPER_PACKAGING:
                        browserColumns.add(getUpperPackaging(touchModelArray.getLabel()));
                        break;
                    case TATTOED_NB:
                        browserColumns.add(getTattooed(touchModelArray.getLabel()));
                        break;
                    case UPPER_TATTOED_NB:
                        browserColumns.add(getUpperTattooed(touchModelArray.getLabel()));
                        break;
                case PRODUCED_ENTITY:
                    browserColumns.add(getProducedEntity(touchModelArray.getLabel()));
                    break;

                    default:
                        break;
                }
            } else {
                // add empty column if we don't know
                browserColumns.add(new BrowserColumn(touchModelArray.getLabel(), "", DEFAULT_SIZE));
                LOGGER.error("ERROR in FDataEntryListColumnsFactory.getColumns(...) : I don't know the column : "
                        + touchModelArray);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getColumns(type=" + type + ", touchModel=" + touchModel + ")=" + browserColumns);
        }
        return browserColumns;
    }

    /**
     * Get the browser model to use.
     * 
     * @param touchModel touchModel
     * @param type type of the movement.
     * @return a browser model.
     */
    public static List<BrowserColumn> getColumns(final OutputEntityType type, final TouchModel touchModel) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getColumns(type=" + type + ", touchModel=" + touchModel + ")");
        }

        TouchModelFunction touchModelFunction = touchModel
                .getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_BATCH_ITEM);
        if (OutputEntityType.CONTAINER.equals(type)) {
            touchModelFunction = touchModel.getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_NSC);
        } else if (OutputEntityType.CONTAINER_UPPER_CONTAINER.equals(type)) {
            touchModelFunction = touchModel.getTouchModelFunction(TouchModelMapKey.DATA_ENTRY_LIST_NSCP);
        }
        List<BrowserColumn> browserColumns = new ArrayList<BrowserColumn>();
        int nbValueCrit = 0;
        int nbValueCalculatedCrit = 0;

        for (TouchModelArray touchModelArray : touchModelFunction.getTouchModelArrays()) {
            CodeFieldModel codeFieldModel = CodeFieldModel.getCodeFieldModel(touchModelArray.getChamp());
            if (codeFieldModel != null) {
                switch (codeFieldModel) {
                    case RANG:
                        browserColumns.add(getRank(touchModelArray.getLabel()));
                        break;
                    case DATMVT:
                        browserColumns.add(getCreationDateTime(touchModelArray.getLabel()));
                        break;
                    case NSC:
                        browserColumns.add(getContainer(touchModelArray.getLabel()));
                        break;
                    case CART:
                        browserColumns.add(getItem(touchModelArray.getLabel()));
                        break;
                    case BATCH:
                        browserColumns.add(getBatch(touchModelArray.getLabel()));
                        break;
                    case QTE_1:
                        browserColumns.add(getFirstUnit(touchModelArray.getLabel()));
                        break;
                    case QTE_2:
                        browserColumns.add(getSecondUnit(touchModelArray.getLabel()));
                        break;
                    case DEPOT:
                        browserColumns.add(getDeposit(touchModelArray.getLabel()));
                        break;
                    case EMP:
                        browserColumns.add(getLocation(touchModelArray.getLabel()));
                        break;
                    case HEURMVT:
                        browserColumns.add(getCreationHour(touchModelArray.getLabel()));
                        break;
                    case CRITERE:
                        nbValueCrit = nbValueCrit + 1;
                        browserColumns.add(getValueCrit(touchModelArray.getLabel(), nbValueCrit));
                        break;
                    case CRITERE_RESTITUE:
                        nbValueCalculatedCrit = nbValueCalculatedCrit + 1;
                        browserColumns.add(getValueCalculatedCrit(touchModelArray.getLabel(), nbValueCalculatedCrit));
                        break;
                    case NSCP:
                        browserColumns.add(getUpperContainer(touchModelArray.getLabel()));
                        break;
                    case MOTIF:
                        browserColumns.add(getMotif(touchModelArray.getLabel()));
                        break;
                    case PACKAGING:
                        browserColumns.add(getPackaging(touchModelArray.getLabel()));
                        break;
                    case UPPER_PACKAGING:
                        browserColumns.add(getUpperPackaging(touchModelArray.getLabel()));
                        break;
                    case TATTOED_NB:
                        browserColumns.add(getTattooed(touchModelArray.getLabel()));
                        break;
                    case UPPER_TATTOED_NB:
                        browserColumns.add(getUpperTattooed(touchModelArray.getLabel()));
                        break;

                    default:
                        break;
                }
            } else {
                // add empty column if we don't know
                browserColumns.add(new BrowserColumn(touchModelArray.getLabel(), "", DEFAULT_SIZE));
                LOGGER.error("ERROR in FDataEntryListColumnsFactory.getColumns(...) : I don't know the column : "
                        + touchModelArray);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getColumns(type=" + type + ", touchModel=" + touchModel + ")=" + browserColumns);
        }
        return browserColumns;
    }

    /* CHECKSTYLE:OFF */
    private static BrowserColumn getBatch(final String customName) {
        String columnName = I18nClientManager.translate(StockKernel.T29224);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.BATCH.getValue(), 100);
    }

    private static BrowserColumn getContainer(final String customName) {
        String columnName = I18nClientManager.translate(StockKernel.T29089);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.CONTAINER_NUMBER.getValue(), 100);
    }

    private static BrowserColumn getCreationDateTime(final String customName) {
        String columnName = I18nClientManager.translate(StockKernel.T29772);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.FORMATTED_CREATION_DATE_TIME.getValue(), 105);
    }

    private static BrowserColumn getCreationHour(final String customName) {
        String columnName = I18nClientManager.translate(StockKernel.T29772);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.FORMATTED_CREATION_HOUR.getValue(), 50);
    }

    private static BrowserColumn getDeposit(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T29599);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.DEPOSIT_ID.getValue(), 100);
    }

    private static BrowserColumn getFirstUnit(final String customName) {
        String columnName = I18nClientManager.translate(GenKernel.T210) + " 1";
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.FORMATTED_FIRST_QTY_UNIT.getValue(), 100, FORMAT);
    }

    private static BrowserColumn getItem(final String customName) {
        String columnName = I18nClientManager.translate(GenKernel.T426);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.ITEM_ID.getValue(), 100);
    }

    private static BrowserColumn getLocation(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T29601);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.LOCATION_ID.getValue(), 100);
    }

    private static BrowserColumn getMotif(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T34654);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.MOTIF.getValue(), 100);
    }

    private static BrowserColumn getPackaging(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T39175);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.PACKAGING.getValue(), 100);
    }

    private static BrowserColumn getRank(final String customName) {
        String columnName = I18nClientManager.translate(StockKernel.T29533);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.LINE_NUMBER.getValue(), 30);
    }

    private static BrowserColumn getSecondUnit(final String customName) {
        String columnName = I18nClientManager.translate(GenKernel.T210) + " 2";
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.FORMATTED_SECOND_QTY_UNIT.getValue(), 100, FORMAT);
    }

    private static BrowserColumn getTattooed(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T39177);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.TATTOOED.getValue(), 100);
    }

    private static BrowserColumn getProducedEntity(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T17191);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.PRODUCED_ENTITY.getValue(), 100);
    }

    private static BrowserColumn getUpperContainer(final String customName) {
        String columnName = I18nClientManager.translate(StockKernel.T29089);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.UPPER_CONTAINER_NUMBER.getValue(), 100);
    }

    private static BrowserColumn getUpperPackaging(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T39176);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.UPPER_PACKAGING.getValue(), 100);
    }

    private static BrowserColumn getUpperTattooed(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T39178);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FDataEntryListBBeanEnum.UPPER_TATTOOED.getValue(), 100);
    }

    private static BrowserColumn getValueCalculatedCrit(final String customName, final int valueNumber) {
        return new BrowserColumn(customName, FDataEntryListBBeanEnum.VALUE_CALCULATED_CRIT.getValue() + valueNumber,
                105);
    }

    private static BrowserColumn getValueCrit(final String customName, final int valueNumber) {
        return new BrowserColumn(customName, FDataEntryListBBeanEnum.VALUE_CRIT.getValue() + valueNumber, 105);
    }

    /* CHECKSTYLE:ON */
}

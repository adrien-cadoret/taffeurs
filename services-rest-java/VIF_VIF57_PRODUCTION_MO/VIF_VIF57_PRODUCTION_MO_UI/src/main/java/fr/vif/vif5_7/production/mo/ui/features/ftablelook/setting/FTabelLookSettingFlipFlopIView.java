/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTabelLookSettingFlipFlopIView.java,v $
 * Created on 26 févr. 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FSettingBean;


/**
 * Interface of FTabelLookSettingFlipFlopView.
 * 
 * @author kl
 */
public interface FTabelLookSettingFlipFlopIView {

    /**
     * Return the bean selected in right zone of the flip flop.
     * 
     * @return FSettingBean
     */
    public FSettingBean getCurrentBean();

}

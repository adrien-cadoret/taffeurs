/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: CDowngradedItemCtrl.java,v $
 * Created on 19 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.composites.cdowngradeditem;


import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.input.AbstractCompositeITFController;
import fr.vif.jtech.ui.input.InputTextFieldView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdowngradeditem.FDowngradedItemSBean;
import fr.vif.vif5_7.production.mo.business.services.composites.cdowngradeditem.CDowngradedItemCBS;


/**
 * CDowngradedItemView 's controller.
 * 
 * @author gp
 */
public class CDowngradedItemCtrl extends AbstractCompositeITFController {

    private CDowngradedItemCBS cdowngradedItemCBS;

    /**
     * Used by spring.
     */
    public CDowngradedItemCtrl() {
        // nothing todo.
    }

    /**
     * Gets the cdowngradedItemCBS.
     * 
     * @category getter
     * @return the cdowngradedItemCBS.
     */
    public CDowngradedItemCBS getCdowngradedItemCBS() {
        return this.cdowngradedItemCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object initHelpDialogBox(final InputTextFieldView fieldView) {
        return super.initHelpDialogBox(fieldView);
    }

    /**
     * Sets the cdowngradedItemCBS.
     * 
     * @category setter
     * @param cdowngradedItemCBS cdowngradedItemCBS.
     */
    public void setCdowngradedItemCBS(final CDowngradedItemCBS cdowngradedItemCBS) {
        this.cdowngradedItemCBS = cdowngradedItemCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected CodeLabel validateCode(final CodeLabel bean) throws UIErrorsException, UIException, BusinessException,
            BusinessErrorsException {
        return getCdowngradedItemCBS().validateCode(getIdCtx(),
                (FDowngradedItemSBean) ((CDowngradedItemIView) getView()).getSelection(), bean);

    }
}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputContainerVIView.java,v $
 * Created on 19 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer;


import fr.vif.jtech.ui.input.CompositeInputTextFieldView;
import fr.vif.jtech.ui.input.InputBarCodeView;
import fr.vif.jtech.ui.input.InputTextFieldView;


/**
 * ProductionInputContainer : Viewer View Interface.
 * 
 * @author vr
 */
public interface FProductionInputContainerVIView {

    /**
     * Get the input bar code.
     * 
     * @return the input bar code
     */
    public InputBarCodeView getInputBarCode();

    /**
     * Get the input batch text field.
     * 
     * @return the input batche text field
     */
    public InputTextFieldView getItfBatch();

    /**
     * Get the substitute item composite.
     * 
     * @return the substitute item compoiste
     */
    public CompositeInputTextFieldView getSubstituteItemView();

    /**
     * 
     * Hide all view components.
     * 
     * @param hide true if all components must be hidden
     */
    public void hideAll(final boolean hide);

    /**
     * Set the flash label.
     * 
     * @param flashLabel the flash label
     */
    public void setFlashLabel(final String flashLabel);
}

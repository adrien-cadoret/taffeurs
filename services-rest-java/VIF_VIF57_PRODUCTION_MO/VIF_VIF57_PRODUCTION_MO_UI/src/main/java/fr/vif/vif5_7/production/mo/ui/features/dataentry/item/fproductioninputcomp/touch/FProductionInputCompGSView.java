/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputCompGSView.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcomp.touch;


import java.awt.BorderLayout;

import fr.vif.jtech.ui.input.touch.TouchInputRadioSet;
import fr.vif.jtech.ui.selection.touch.StandardTouchGeneralSelection;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.constants.Mnemos;


/**
 * Production Output Comp Selection View.
 * 
 * @author kl
 */
public class FProductionInputCompGSView extends StandardTouchGeneralSelection {

    private TouchInputRadioSet irsState;

    /**
     * Default constructor.
     */
    public FProductionInputCompGSView() {
        super();
        initialize();
    }

    /**
     * This Method Initializes this.
     * 
     */
    public void initialize() {
        setLayout(new BorderLayout());
        setOpaque(false);
        add(getIrsState(), BorderLayout.CENTER);
        add(getIrsState());
    }

    /**
     * Generate a TouchInputRadioSet with values contains in Mnemos.MONatureComplementaryWorkshop.
     * 
     * @return TouchInputRadioSet
     */
    private TouchInputRadioSet getIrsState() {
        if (irsState == null) {
            irsState = new TouchInputRadioSet();
            irsState.setFocusable(false);
            irsState.setLabel("");
            irsState.setBeanProperty("state");
            irsState.addItem(Mnemos.MONatureComplementaryWorkshop.MANUFACTURING.getValue(),
                    I18nClientManager.translate(ProductionMo.T6283));
            irsState.addItem(Mnemos.MONatureComplementaryWorkshop.INPROGRESS.getValue(),
                    I18nClientManager.translate(Mnemos.MONatureComplementaryWorkshop.INPROGRESS.getTranslation()));

        }
        return irsState;
    }

}

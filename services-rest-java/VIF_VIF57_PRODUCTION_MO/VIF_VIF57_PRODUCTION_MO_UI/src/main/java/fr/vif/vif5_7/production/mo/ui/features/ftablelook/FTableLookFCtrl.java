/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookFCtrl.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook;


import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.menu.LaunchParameterBean;
import fr.vif.jtech.common.beans.menu.RelatedFeatureBean;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.dialogs.DialogModel;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.util.DialogHelper;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.admin.config.AdminConfig;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.importexport.FTableLookImportExportBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.importexport.FTableLookImportExportBean.ImportExport;
import fr.vif.vif5_7.production.mo.business.services.features.tablelook.FTableLookCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.CodeFunction;
import fr.vif.vif5_7.production.mo.constants.ProductionMOParamEnum;
import fr.vif.vif5_7.production.mo.ui.features.ftablelook.importexport.DTableLookImpotExportDCtrl;


/**
 * TableLook Feature Controller.
 * 
 * @author kl
 */
public class FTableLookFCtrl extends StandardFeatureController implements DialogChangeListener, DisplayListener {
    /** LOGGER. */
    private static final Logger LOGGER                      = Logger.getLogger(FTableLookFCtrl.class);

    private static final String REFERENCE_BTN_SETTING       = "setting";

    private static final String REFERENCE_BTN_IMPORT_EXPORT = "importExport";
    private static final String DEFAULT_PATH                = "C:/temp/";
    private static final String DEFAULT_MODEL_CODE          = CodeFunction.TABLE_LOOK.getValue();
    private FTableLookCBS       ftableLookCBS;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        if (REFERENCE_BTN_IMPORT_EXPORT.equals(event.getModel().getReference())) {
            FTableLookVBean vBean = (FTableLookVBean) getSharedContext().get(Domain.DOMAIN_THIS, "viewerBean");
            StringBuffer sPath = new StringBuffer();
            sPath.append(DEFAULT_PATH).append(vBean.getBrowserBean().getCode()).append(".d");

            FTableLookImportExportBean bean = new FTableLookImportExportBean();
            bean.setPath(sPath.toString());
            bean.setTableLookCode(vBean.getBrowserBean().getCode());

            DialogHelper.createDialog(getView(), DTableLookImpotExportDCtrl.class, DialogModel.class,
                    "/dynamicui/fr/vif/vif5_7/production/mo/ftablelook/importexport/DTableLookImportExport.xml", bean,
                    this, this, getModel().getIdentification(), "Import - Export du modèle");

        } else {
            super.buttonAction(event);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }

        // Bean use in DTableLookImportExportDView. View to Import-Export a TableLook
        if (event.getDialogBean() != null && event.getDialogBean() instanceof FTableLookImportExportBean) {
            OutputStreamWriter writer = null;
            BufferedReader reader = null;
            try {
                boolean isDone = false;
                FTableLookImportExportBean importExportBean = (FTableLookImportExportBean) event.getDialogBean();

                if (ImportExport.EXPORT.getValue().equals(importExportBean.getAction())) {
                    List<String> dataExport = getFtableLookCBS().exportModel(getIdCtx(), importExportBean);

                    if (dataExport != null && !dataExport.isEmpty()) {
                        writer = new OutputStreamWriter(new FileOutputStream(importExportBean.getPath()));
                        for (String line : dataExport) {
                            writer.append(line);
                            writer.append("\n");
                        }
                        writer.flush();

                        isDone = true;
                    }
                } else {
                    List<String> dataToImport = new ArrayList<String>();
                    String line = null;
                    if (new File(importExportBean.getPath()).exists()) {
                        reader = new BufferedReader(new FileReader(importExportBean.getPath()));
                        while ((line = reader.readLine()) != null) {
                            dataToImport.add(line);

                        }
                        isDone = getFtableLookCBS().importModel(getIdCtx(), importExportBean, dataToImport);

                        // Reload data.
                        getBrowserCtrl().reopenQuery(getBrowserCtrl().getDefaultInitialSelection());
                    } else {
                        getView().showError(
                                getModel().getTitle(),
                                I18nClientManager.translate(Generic.T5815, false, new VarParamTranslation(
                                        "Fichier non existant")));
                    }
                }
                if (isDone) {
                    getView().showInformation(getModel().getTitle(), I18nClientManager.translate(Generic.T2930, false));
                }

            } catch (BusinessException e) {
                LOGGER.info("P - BusinessException e=" + e);
                getView().show(new UIException(e));
            } catch (FileNotFoundException e) {
                LOGGER.info("P - FileNotFoundException e=" + e);
                getView().showError(
                        getModel().getTitle(),
                        I18nClientManager.translate(Generic.T22493, false,
                                new VarParamTranslation(I18nClientManager.translate(Generic.T20822, false))));
            } catch (IOException e) {
                LOGGER.info("P - IOException e=" + e);
                getView().showError(getModel().getTitle(),
                        I18nClientManager.translate(Generic.T22493, false, new VarParamTranslation("")));
            } finally {
                try {
                    if (writer != null) {
                        writer.close();
                    }

                    if (reader != null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    LOGGER.info("P - IOException e=" + e);
                }

            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    /**
     * Gets the ftableLookCBS.
     * 
     * @return the ftableLookCBS.
     */
    public FTableLookCBS getFtableLookCBS() {
        return ftableLookCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<RelatedFeatureBean> getRelatedFeatures() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getRelatedFeatures()");
        }

        List<RelatedFeatureBean> relatedFeaturesLst = new ArrayList<RelatedFeatureBean>();
        RelatedFeatureBean link1 = new RelatedFeatureBean();
        link1.setRelatedFeatureId("VIF.PACONUJ");
        link1.setRelatedFeatureLabel(I18nClientManager.translate(AdminConfig.T26582));
        relatedFeaturesLst.add(link1);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getRelatedFeatures()=" + relatedFeaturesLst);
        }
        return relatedFeaturesLst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        setTitle(I18nClientManager.translate(ProductionMo.T34913, false,
                new VarParamTranslation(I18nClientManager.translate(ProductionMo.T35123))));
        super.initialize();

    }

    /**
     * Sets the ftableLookCBS.
     * 
     * @param ftableLookCBS ftableLookCBS.
     */
    public void setFtableLookCBS(final FTableLookCBS ftableLookCBS) {
        this.ftableLookCBS = ftableLookCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected LaunchParameterBean getFeatureLaunchParameter(final String featureID) {
        LaunchParameterBean launchParameter = new LaunchParameterBean();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("ZPACON-CPACON", ProductionMOParamEnum.FABRICATION_INPUT.getCode() + ","
                + ProductionMOParamEnum.FABRICATION_OUTPUT.getCode());
        launchParameter.setValues(params);
        return launchParameter;
    }

    /**
     * Cast the BrowserController in FTableLookBCtrl.
     * 
     * @return FTableLookBCtrl
     */
    private FTableLookBCtrl getBrowserCtrl() {
        return (FTableLookBCtrl) getBrowserController();
    }

}

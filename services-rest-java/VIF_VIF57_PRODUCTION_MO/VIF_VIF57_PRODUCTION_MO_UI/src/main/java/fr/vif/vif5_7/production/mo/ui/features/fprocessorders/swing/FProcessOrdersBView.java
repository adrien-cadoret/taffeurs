/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersBView.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders.swing;


import javax.swing.ListSelectionModel;

import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.swing.SwingBrowser;
import fr.vif.jtech.ui.dialogs.selection.SelectionModel;
import fr.vif.jtech.ui.models.mvc.SelectionMVCTriad;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersBBean;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersSCtrl;


/**
 * ProcessOrders : Browser View.
 * 
 * @author jd
 */
public class FProcessOrdersBView extends SwingBrowser<FProcessOrdersBBean> {

    /**
     * Default constructor.
     * 
     */
    public FProcessOrdersBView() {
        super();
        setSelectionMvcTriad(new SelectionMVCTriad(SelectionModel.class, FProcessOrdersSView.class,
                FProcessOrdersSCtrl.class));
        getJTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView(final AbstractBrowserController<FProcessOrdersBBean> browserCtrl) {
        super.initializeView(browserCtrl);

        getJTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
    }

}

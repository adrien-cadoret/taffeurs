/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListFModel.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.GeneralSelectionMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.jtech.ui.selection.GeneralSelectionModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.touch.FDataEntryListGSView;


/**
 * DataEntryList : Feature Model.
 * 
 * @author vr
 */
public class FDataEntryListFModel extends StandardFeatureModel {

    /**
     * Default constructor.
     * 
     */
    public FDataEntryListFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FDataEntryListBModel.class, null, FDataEntryListBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FDataEntryListVModel.class, null, FDataEntryListVCtrl.class));
        setGeneralSelectionTriad(new GeneralSelectionMVCTriad(GeneralSelectionModel.class, FDataEntryListGSView.class,
                FDataEntryListGSCtrl.class));
    }

    @Override
    public BrowserMVCTriad getBrowserTriad() {
        return super.getBrowserTriad();
    }

}

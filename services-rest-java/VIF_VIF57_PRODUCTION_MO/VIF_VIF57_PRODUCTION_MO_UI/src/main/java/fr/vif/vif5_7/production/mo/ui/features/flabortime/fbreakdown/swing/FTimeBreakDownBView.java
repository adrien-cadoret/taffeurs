/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownBView.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing;


import fr.vif.jtech.ui.browser.swing.SwingBrowser;
import fr.vif.jtech.ui.dialogs.selection.SelectionModel;
import fr.vif.jtech.ui.models.mvc.SelectionMVCTriad;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownVBean;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.FTimeBreakDownSCtrl;


/**
 * TimeBreakDown : Browser View.
 * 
 * @author cj
 * @version $Revision: 1.3 $, $Date: 2016/05/26 09:41:19 $
 */
public class FTimeBreakDownBView extends SwingBrowser<FTimeBreakDownVBean> {

    /**
     * Default constructor.
     * 
     */
    public FTimeBreakDownBView() {
        super();
        setSelectionMvcTriad(new SelectionMVCTriad(SelectionModel.class, FTimeBreakDownSView.class,
                FTimeBreakDownSCtrl.class));
    }

}

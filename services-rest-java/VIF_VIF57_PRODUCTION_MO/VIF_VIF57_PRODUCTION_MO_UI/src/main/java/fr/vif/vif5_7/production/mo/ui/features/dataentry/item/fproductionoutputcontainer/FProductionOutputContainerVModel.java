/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputContainerVModel.java,v $
 * Created on 25 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerVBean;


/**
 * ProductionOutputContainer : Viewer Model.
 * 
 * @author vr
 */
public class FProductionOutputContainerVModel extends ViewerModel<FProductionOutputContainerVBean> {

    /**
     * Default constructor.
     * 
     */
    public FProductionOutputContainerVModel() {
        super();
        setBeanClass(FProductionOutputContainerVBean.class);
    }

}

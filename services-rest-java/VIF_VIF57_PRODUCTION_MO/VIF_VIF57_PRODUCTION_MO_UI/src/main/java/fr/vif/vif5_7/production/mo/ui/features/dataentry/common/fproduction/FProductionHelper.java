/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionHelper.java,v $
 * Created on 24 déc. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction;


import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdowngradeditem.FDowngradedItemSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninput.FProductionInputVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputbatch.FProductionInputBatchSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fsubstituteitem.FSubstituteItemSBean;
import fr.vif.vif5_7.stock.kernel.business.beans.common.stock.StockPointSBean;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingSBean;


/**
 * FProductionInput helper.
 * 
 * @author vr
 */
public final class FProductionHelper {

    /**
     * private constructor.
     */
    private FProductionHelper() {

    }

    /**
     * Change batch help selection after a convert or an itemId change.
     * 
     * @param vBean viewer bean
     * @param sBean old selection bean
     * @return new selection bean
     */
    public static FProductionInputBatchSBean getBatchHelpSelection(final FProductionInputVBean vBean,
            final FProductionInputBatchSBean sBean) {
        if (vBean != null) {

            StockPointSBean stb = new StockPointSBean();
            stb.setCompanyId(vBean.getInputOperationBean().getOperationItemKey().getEstablishmentKey().getCsoc());
            stb.setEstablishmentId(vBean.getInputOperationBean().getOperationItemKey().getEstablishmentKey().getCetab());
            stb.setDepotId(vBean.getInputOperationBean().getDepotCL().getCode());
            stb.setLocationId(vBean.getInputOperationBean().getLocationCL().getCode());
            stb.setItemId(vBean.getItemCL().getCode());
            stb.setOnlyPositiveQty(false);
            sBean.setStockPointSBean(stb);
        }
        return sBean;
    }

    /**
     * Change batch help selection after a convert or an itemId change.
     * 
     * @param operationItemBean operation item bean
     * @param itemId real item id
     * @param sBean selection
     * @return FDestocking selection bean
     */
    public static FDestockingSBean getFDestockingSBean(final AbstractOperationItemBean operationItemBean,
            final String itemId, final FDestockingSBean sBean) {
        FDestockingSBean ret = sBean;
        if (ret == null) {
            ret = new FDestockingSBean();
        }
        ret.setEstablishmentKey(operationItemBean.getOperationItemKey().getEstablishmentKey());
        ret.setItemId(itemId);
        ret.setDepotId(operationItemBean.getDepotCL().getCode());
        return ret;
    }

    /**
     * Change item help selection after a convert or an itemId change.
     * 
     * @param operationItemBean input operation item bean
     * @param workstationId workstation id
     * @param sBean previous selection bean
     * @return FSubstitue selection bean
     */
    public static FDowngradedItemSBean getFDowngradedItemSBean(final OutputOperationItemBean operationItemBean,
            final String workstationId, final FDowngradedItemSBean sBean) {
        FDowngradedItemSBean ret = sBean;
        if (ret == null) {
            ret = new FDowngradedItemSBean();
        }
        ret.setAll(false);
        ret.setOutputOperationItemBean(operationItemBean);
        ret.setWorkstationId(workstationId);
        return ret;
    }

    /**
     * Change item help selection after a convert or an itemId change.
     * 
     * @param operationItemBean input operation item bean
     * @param workstationId workstation id
     * @param sBean previous selection bean
     * @return FSubstitue selection bean
     */
    public static FSubstituteItemSBean getFSubstiteSBean(final InputOperationItemBean operationItemBean,
            final String workstationId, final FSubstituteItemSBean sBean) {
        FSubstituteItemSBean ret = sBean;
        if (ret == null) {
            ret = new FSubstituteItemSBean();
        }
        ret.setAll(false);
        ret.setInputOperationItemBean(operationItemBean);
        ret.setWorkstationId(workstationId);
        return ret;
    }

    /**
     * Change item help selection after a convert or an itemId change.
     * 
     * @param vBean viewer bean
     * @param sBean old selection bean
     * @return new selection bean
     */
    public static FSubstituteItemSBean getItemHelpSelection(final FProductionInputVBean vBean,
            final FSubstituteItemSBean sBean) {
        FSubstituteItemSBean ret = sBean;
        if (ret == null) {
            ret = new FSubstituteItemSBean();
        }
        if (vBean != null) {
            ret.setAll(false);
            ret.setInputOperationItemBean(vBean.getInputOperationBean());
            ret.setWorkstationId(vBean.getWorkstationId());
        }
        return sBean;
    }
}

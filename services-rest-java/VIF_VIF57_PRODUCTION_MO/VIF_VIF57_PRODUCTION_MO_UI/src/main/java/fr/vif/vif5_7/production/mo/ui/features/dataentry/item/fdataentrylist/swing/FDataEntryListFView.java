/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListFView.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.swing;


import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.swing.StandardSwingFeature;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListVBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFIView;


/**
 * DataEntryList : Feature View.
 * 
 * @author vr
 */
public class FDataEntryListFView extends StandardSwingFeature<FDataEntryListBBean, FDataEntryListVBean> implements
FDataEntryListFIView {

    private FDataEntryListBView fDataEntryListBView = null;

    private FDataEntryListVView fDataEntryListVView = null;

    /**
     * Default constructor.
     * 
     */
    public FDataEntryListFView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public BrowserView<FDataEntryListBBean> getBrowserView() {
        return getFDataEntryListBView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView<FDataEntryListVBean> getViewerView() {
        return getFDataEntryListVView();
    }

    @Override
    public void setSubtitle(final String subtitle) {

    }

    @Override
    public void setSums(final String val1, final String unit1, final String val2, final String unit2) {

    }

    /**
     * Initialisation of the browser feature.
     * 
     * @return FDataEntryListBView
     */
    private FDataEntryListBView getFDataEntryListBView() {
        if (fDataEntryListBView == null) {
            fDataEntryListBView = new FDataEntryListBView();
        }
        return fDataEntryListBView;
    }

    /**
     * Initialisation of the viewer feature.
     * 
     * @return FDataEntryListVView
     */
    private FDataEntryListVView getFDataEntryListVView() {
        if (fDataEntryListVView == null) {
            fDataEntryListVView = new FDataEntryListVView();
        }
        return fDataEntryListVView;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        // this.setSize(new java.awt.Dimension(width, height));
        // this.setContentPane(...);
    }

}

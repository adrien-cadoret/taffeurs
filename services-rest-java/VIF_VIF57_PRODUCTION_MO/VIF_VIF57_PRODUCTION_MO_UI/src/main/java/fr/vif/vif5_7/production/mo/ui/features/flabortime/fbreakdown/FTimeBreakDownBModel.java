/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownBModel.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownVBean;


/**
 * TimeBreakDown : Browser Model.
 * 
 * @author cj
 * @version $Revision: 1.3 $, $Date: 2016/05/26 09:41:19 $
 */
public class FTimeBreakDownBModel extends BrowserModel<FTimeBreakDownVBean> {

    /**
     * Default constructor.
     * 
     */
    public FTimeBreakDownBModel() {
        super();
        this.addColumn(new BrowserColumn("", "breakdownHchyTreeNodeElt.label", 0));
        setBeanClass(FTimeBreakDownVBean.class);
        setSelectionEnabled(true);
    }

}

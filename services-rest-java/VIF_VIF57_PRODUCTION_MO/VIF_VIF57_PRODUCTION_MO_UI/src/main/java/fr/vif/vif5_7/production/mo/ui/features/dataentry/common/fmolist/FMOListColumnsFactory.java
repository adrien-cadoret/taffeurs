/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOListColumnsFactory.java,v $
 * Created on 3 Mar, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListBViewEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelArray;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.CodeFieldModel;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;


/**
 * Use to display the column in Browser Model. The columns to display are set by the bean TouchModelFunction.<br/>
 * This bean is a representation of the model define in the screen parameter of model touch.
 * 
 * Manage the Screen Mo List.
 * 
 * @author kl
 */
public final class FMOListColumnsFactory {

    private static Format formatQty = new Format(Alignment.RIGHT_ALIGN, MOUIConstants.TOUCHSCREEN_BG_BROWSER,
            StandardColor.TOUCHSCREEN_FG_BROWSER,
            StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE, "11");

    /**
     * Default constructor.
     */
    private FMOListColumnsFactory() {

    }

    /**
     * Get the browser model to use.
     * 
     * @param touchModelFunction touchModelFunction
     * @return a browser model.
     */
    public static List<BrowserColumn> getColumns(final TouchModelFunction touchModelFunction) {

        List<BrowserColumn> browserColumns = new ArrayList<BrowserColumn>();
        // Use to put criterion at the good place.
        int nbValueCrit = 0;
        int nbValueCalculatedCrit = 0;

        for (TouchModelArray touchModelArray : touchModelFunction.getTouchModelArrays()) {
            CodeFieldModel codeFieldModel = CodeFieldModel.getCodeFieldModel(touchModelArray.getChamp());
            switch (codeFieldModel) {
                case CHRONOF:
                    browserColumns.add(getChrono(touchModelArray.getLabel()));
                    break;
                case CARTREFOF:
                    browserColumns.add(getItemId(touchModelArray.getLabel()));
                    break;
                case LOF:
                    browserColumns.add(getMoLabel(touchModelArray.getLabel()));
                    break;
                case LARTREFOF:
                    browserColumns.add(getItemLongLabel(touchModelArray.getLabel()));
                    break;
                case QTEREFOF:
                    browserColumns.add(getQtyTodo(touchModelArray.getLabel()));
                    break;
                case PRIOR:
                    browserColumns.add(getPriority(touchModelArray.getLabel()));
                    break;
                case DATDEBR:
                    browserColumns.add(getDate(touchModelArray.getLabel()));
                    break;
                case HEURDEBR:
                    browserColumns.add(getHour(touchModelArray.getLabel()));
                    break;
                case CRITERE:
                    nbValueCrit = nbValueCrit + 1;
                    browserColumns.add(getValueCrit(touchModelArray.getLabel(), nbValueCrit));
                    break;
                case CRITERE_RESTITUE:
                    nbValueCalculatedCrit = nbValueCalculatedCrit + 1;
                    browserColumns.add(getValueCalculatedCrit(touchModelArray.getLabel(), nbValueCalculatedCrit));
                    break;
                case TYPOF:
                    browserColumns.add(getMoType());
                    break;
                case ORIG:
                    browserColumns.add(getMoOrigin(touchModelArray.getLabel()));
                    break;
                case CRES:
                    browserColumns.add(getRessourceCode(touchModelArray.getLabel()));
                    break;

                default:
                    break;
            }
        }

        return browserColumns;
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.CHRONO value.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getChrono(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T19824, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.CHRONO.getValue(), 90, "########");
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.DATE value format in hh/dd/yyyy.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getDate(final String customName) {
        String columnName = I18nClientManager.translate(Generic.T3057, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.DATE.getValue(), 70, "dd/MM/yyyy");
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.DATE value format int HH:mm.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getHour(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T29437, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.DATE.getValue(), 55, "HH:mm");
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.ITEM_ID value.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getItemId(final String customName) {
        String columnName = I18nClientManager.translate(GenKernel.T426, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.ITEM_ID.getValue(), 190);
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.ITEM_LONG_LABEL value.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getItemLongLabel(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T34921, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.ITEM_LONG_LABEL.getValue(), 305);
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.LABEL value.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getMoLabel(final String customName) {
        String columnName = I18nClientManager.translate(Generic.T1863, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.LABEL.getValue(), 305);
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.ORIGIN_MO value.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getMoOrigin(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T34915, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.ORIGIN_MO.getValue(), 90, "########");
    }

    /**
     * Display an icon in the column.
     * 
     * @return A BrowserColumn
     */
    private static BrowserColumn getMoType() {
        return new BrowserColumn("", "mobean", 45);
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.PRIORITY value.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getPriority(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T32636, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.PRIORITY.getValue(), 45);
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.QTY_TODO value.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getQtyTodo(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T29438, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.QTY_TODO.getValue(), 130, formatQty);
    }

    /**
     * Return the BrowserColumn for the FMOListBViewEnum.RESSOURCE_CODE value.
     * 
     * @param customName if the user define an other label in the model.
     * @return A BrowserColumn
     */
    private static BrowserColumn getRessourceCode(final String customName) {
        String columnName = I18nClientManager.translate(ProductionMo.T34865, false);
        if (StringUtils.isNotEmpty(customName)) {
            columnName = customName;
        }
        return new BrowserColumn(columnName, FMOListBViewEnum.RESSOURCE_CODE.getValue(), 190);
    }

    /**
     * Return the value calculated of the criterion.
     * 
     * @param customName Name of the column (Code criterion or custom name define by the customer)
     * @param valueNumber use to match the bean properties.
     * @return BrowserColumn
     */
    private static BrowserColumn getValueCalculatedCrit(final String customName, final int valueNumber) {
        return new BrowserColumn(customName, FMOListBViewEnum.VALUE_CALCULATED_CRIT.getValue() + valueNumber, 105);
    }

    /**
     * Return the value of the criterion.
     * 
     * @param customName Name of the column (Code criterion or custom name define by the customer)
     * @param valueNumber use to match the bean properties.
     * @return BrowserColumn
     */
    private static BrowserColumn getValueCrit(final String customName, final int valueNumber) {
        return new BrowserColumn(customName, FMOListBViewEnum.VALUE_CRIT.getValue() + valueNumber, 105);
    }
}

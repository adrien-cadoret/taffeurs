/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputVCtrl.java,v $
 * Created on 10 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFCtrl;


/**
 * Viewer controller.
 * 
 * @author xg
 */
public class FBoningDataEntryListInputVCtrl extends AbstractStandardViewerController<FBoningDataEntryListInputBBean> {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FBoningDataEntryListInputVCtrl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        super.initialize();
        setCreationMode(false);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Show ui errors exceptions in error panel.
     * 
     * @param exception exception to show
     */
    public void showUIErrorsException(final UIErrorsException exception) {
        fireErrorsChanged(exception);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningDataEntryListInputBBean convert(final Object bean) throws UIException {
        return (FBoningDataEntryListInputBBean) bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FBoningDataEntryListInputBBean bean) throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningDataEntryListInputBBean insertBean(final FBoningDataEntryListInputBBean bean,
            final FBoningDataEntryListInputBBean initialBean) throws UIException, UIErrorsException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean ret = true;

        if (null == getBean().getItemMovementBean().getItemMovementKey().getChrono().getPrechro()
                || "".equals(getBean().getItemMovementBean().getItemMovementKey().getChrono().getPrechro())) {
            ret = false;
        } else if (FDataEntryListFCtrl.DELETE_BTN.equals(reference)
                || FDataEntryListFCtrl.CRITERIA_BTN.equals(reference)) {
            AbstractOperationItemBean abstractOperationItemBean = (AbstractOperationItemBean) getSharedContext().get(
                    Domain.DOMAIN_PARENT, FDataEntryListFCtrl.OPERATION_ITEM_BEAN);
            if (abstractOperationItemBean != null
                    && ManagementType.ARCHIVED.equals(abstractOperationItemBean.getOperationItemKey()
                            .getManagementType())) {
                ret = false;
            }
        } else if (FDataEntryListFCtrl.CRITERIA_BTN.equals(reference)) {

            ret = getBean().getItemMovementBean().getBatch() != null
                    && !"".equals(getBean().getItemMovementBean().getBatch());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningDataEntryListInputBBean updateBean(final FBoningDataEntryListInputBBean bean) throws UIException,
            UIErrorsException {
        return getModel().getBean();
    }

}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOBModel.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo;


import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOBBean;


/**
 * FSupervisorMO : Browser Model.
 * 
 * @author jd
 */
public class FSupervisorMOBModel extends BrowserModel<FSupervisorMOBBean> {

    /**
     * Default constructor.
     * 
     */
    public FSupervisorMOBModel() {
        super();
        setBeanClass(FSupervisorMOBBean.class);
        // Add columns
        // addColumn(new BrowserColumn(label, field, size, format));
        setFetchSize(20);
        setFirstFetchSize(40);
        setSelectionEnabled(true);
    }

}

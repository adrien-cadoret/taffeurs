/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookFView.java,v $
 * Created on 4 févr. 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.swing;


import fr.vif.jtech.ui.feature.swing.StandardSwingFeature;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;


/**
 * TableLook Feature View.
 * 
 * @author kl
 */
public class FTableLookFView extends StandardSwingFeature<FTableLookBBean, FTableLookVBean> {

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMSOCreationBModel.java,v $
 * Created on 19 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.touch.TouchBrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationVBean;


/**
 * MSO creation browser model.
 * 
 * @author xg
 */
public class FBoningMSOCreationBModel extends TouchBrowserModel<FBoningMSOCreationVBean> {

    /**
     * Simple constructor.
     */
    public FBoningMSOCreationBModel() {
        super();
        setBeanClass(FBoningMSOCreationVBean.class);
        addColumn(new BrowserColumn("", "moKey.chrono.chrono", 0));
        setFetchSize(20);
        setFirstFetchSize(40);
    }

}

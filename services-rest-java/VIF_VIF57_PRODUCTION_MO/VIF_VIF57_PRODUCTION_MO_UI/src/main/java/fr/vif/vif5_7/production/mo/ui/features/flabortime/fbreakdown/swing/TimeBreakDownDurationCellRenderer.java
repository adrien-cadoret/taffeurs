/*
 * Copyright (c) 2015 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: TimeBreakDownDurationCellRenderer.java,v $
 * Created on 10 avr. 2015 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing;


import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;


/**
 * Renderer for the duration column.
 *
 * @author cj
 */
public class TimeBreakDownDurationCellRenderer extends DefaultTableCellRenderer {

    /**
     * {@inheritDoc}
     */
    @Override
    public Component getTableCellRendererComponent(final JTable table, final Object value, final boolean isSelected,
            final boolean hasFocus, final int row, final int column) {
        JPanel pan = new JPanel();
        if (value != null) {
            int seconds = (int) (((Date) value).getTime() / 1000);
            JLabel durationLBL = new JLabel(String.format("%d:%02d:%02d", seconds / 3600, (seconds % 3600) / 60,
                    seconds % 60));
            durationLBL.setHorizontalAlignment(JLabel.RIGHT);
            durationLBL.setOpaque(false);

            pan.setOpaque(false);
            pan.setBorder(null);
            pan.setLayout(new GridBagLayout());
            pan.add(durationLBL, new GridBagConstraints(0, 0, 1, 1, 1, 1, GridBagConstraints.LINE_END,
                    GridBagConstraints.BOTH, new Insets(0, 0, 0, 2), 0, 0));

            pan.setOpaque(true);
            if (isSelected) {
                pan.setBackground(Color.BLACK);
                durationLBL.setForeground(Color.WHITE);
            }
        }
        return pan;
    }
}

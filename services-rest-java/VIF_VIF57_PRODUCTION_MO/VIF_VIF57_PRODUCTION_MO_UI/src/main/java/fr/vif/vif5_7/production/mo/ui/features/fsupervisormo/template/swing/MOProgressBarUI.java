/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: MOProgressBarUI.java,v $
 * Created on 23 sept. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing;


import java.awt.Color;

import javax.swing.plaf.basic.BasicProgressBarUI;


/**
 * Progress bar.
 * 
 * @author vr
 */
public class MOProgressBarUI extends BasicProgressBarUI {

    /**
     * {@inheritDoc}
     */
    @Override
    protected Color getSelectionBackground() {
        return Color.BLACK;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected Color getSelectionForeground() {
        return Color.WHITE;
    }

}

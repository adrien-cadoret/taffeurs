/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLabelFModel.java,v $
 * Created on 17 Jun 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.flabels;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.flabels.touch.FLabelBView;


/**
 * Feature model of FLabel.
 * 
 * @author xg
 */
public class FLabelFModel extends StandardFeatureModel {

    /**
     * Default constr.
     */
    public FLabelFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FLabelBModel.class, FLabelBView.class, FLabelBCtrl.class));
        // setViewerTriad(new ViewerMVCTriad(modelClass, viewClass, controllerClass));
    }
}

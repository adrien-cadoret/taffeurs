/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: UnclosedTrsTableModel.java,v $
 * Created on 3 mai 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.model;


import java.text.SimpleDateFormat;

import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.addon.trs.business.constants.Mnemos.Reason;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedTrsLineBean;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.AbstractEndDayIndicatorTableModel;


/**
 * UnclosedTrsTable model.
 *
 * @author ac
 */
public class UnclosedTrsTableModel extends AbstractEndDayIndicatorTableModel {

    /** column count. */
    private static final int    COLUMN_COUNT            = 5;

    private static final String PRODUCTION_REASON_LABEL = I18nClientManager.translate(ProductionMo.T25832, false);
    private static final String OPENING_REASON_LABEL    = I18nClientManager.translate(ProductionMo.T40756, false);

    private String[]            columnNames             = { I18nClientManager.translate(ProductionMo.T34865, false),
            I18nClientManager.translate(ProductionMo.T40693, false),
            I18nClientManager.translate(ProductionMo.T34916, false),
            I18nClientManager.translate(ProductionMo.T4999, false),
            I18nClientManager.translate(ProductionMo.T34654, false) };

    /**
     * Default Ctor.
     */
    public UnclosedTrsTableModel() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getColumnCount() {
        return COLUMN_COUNT;
    }

    @Override
    public String getColumnName(final int c) {
        return columnNames[c];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        Object result = new Object();
        if (getEndDayMOList() != null && getEndDayMOList().size() > 0) {
            UnclosedTrsLineBean unclosedTrsLineBean = (UnclosedTrsLineBean) getEndDayMOList().get(rowIndex);
            // CHECKSTYLE:OFF

            switch (columnIndex) {
                case 0:
                    result = unclosedTrsLineBean.getLine();
                    break;
                case 1:
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    result = dateFormat.format(unclosedTrsLineBean.getBegDate());
                    break;
                case 2:
                    result = unclosedTrsLineBean.getChrono();
                    break;
                case 3:
                    result = unclosedTrsLineBean.getMoLabel();
                    break;
                case 4:
                    if (Reason.PRODUCTION.getValue().equals(unclosedTrsLineBean.getReason().getCode())) {
                        result = PRODUCTION_REASON_LABEL;
                    } else if (Reason.LINE.getValue().equals(unclosedTrsLineBean.getReason().getCode())) {
                        result = OPENING_REASON_LABEL;
                    } else {
                        result = unclosedTrsLineBean.getReason().getCode();
                    }
                    break;
                default:
                    break;
            }
            // CHECKSTYLE:ON

        }
        return result;
    }
}

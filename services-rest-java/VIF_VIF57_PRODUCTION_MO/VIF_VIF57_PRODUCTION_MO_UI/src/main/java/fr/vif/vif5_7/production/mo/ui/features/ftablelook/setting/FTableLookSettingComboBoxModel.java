/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingComboBoxModel.java,v $
 * Created on 6 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import fr.vif.jtech.ui.input.InputComboBoxModel;


/**
 * Conbo Box Sort Model.
 * 
 * @author kl
 */
public class FTableLookSettingComboBoxModel extends InputComboBoxModel {

    /**
     * Superclass constructor.
     */
    public FTableLookSettingComboBoxModel() {
        super();
    }

}

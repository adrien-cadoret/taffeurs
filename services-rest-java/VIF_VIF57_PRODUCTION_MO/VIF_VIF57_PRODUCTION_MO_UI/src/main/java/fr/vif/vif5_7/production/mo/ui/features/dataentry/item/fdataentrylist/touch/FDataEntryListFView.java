/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListFView.java,v $
 * Created on 10 nov. 2008 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.touch;


import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListVBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFIView;


/**
 * ItemMovement : Feature View.
 * 
 * @author gv
 */
public class FDataEntryListFView extends StandardTouchFeature<FDataEntryListBBean, FDataEntryListVBean> implements
FDataEntryListFIView {

    private FDataEntryListBView  browserView   = null;
    private FDataEntryListGSView selectionView = null;
    private JPanel               panel;

    private TitlePanel           titlePanel;

    private FDataEntryListVView  viewerView;

    /**
     * Default constructor.
     * 
     */
    public FDataEntryListFView() {
        super();
        initialize();
    }

    /**
     * Gets the browser.
     * 
     * @return the FDataEntryListBView
     */
    public FDataEntryListBView getBrowser() {
        if (browserView == null) {
            browserView = new FDataEntryListBView();
            Dimension d = new Dimension(870, 400);
            browserView.setPreferredSize(d);
            browserView.setMinimumSize(d);
            browserView.setMaximumSize(d);
            browserView.setName("browserView");
        }
        return browserView;
    }

    @Override
    public BrowserView<FDataEntryListBBean> getBrowserView() {
        return getBrowser();
    }

    /**
     * Gets the selectionView.
     *
     * @return the selectionView.
     */
    public FDataEntryListGSView getSelectionView() {
        if (selectionView == null) {
            selectionView = new FDataEntryListGSView();
            Dimension d = new Dimension(870, 60);
            selectionView.setPreferredSize(d);
            selectionView.setMinimumSize(d);
            selectionView.setMaximumSize(d);

        }
        return selectionView;
    }

    /**
     * Gets the viewer.
     * 
     * @return the FDataEntryListVView
     */
    public FDataEntryListVView getViewer() {
        if (viewerView == null) {
            viewerView = new FDataEntryListVView();
            Dimension d = new Dimension(870, 210);
            viewerView.setPreferredSize(d);
            viewerView.setMinimumSize(d);
            viewerView.setMaximumSize(d);

        }
        return viewerView;
    }

    @Override
    public ViewerView<FDataEntryListVBean> getViewerView() {
        return getViewer();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSubtitle(final String subtitle) {
        getTitlePanel().setTitle(subtitle);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSums(final String val1, final String unit1, final String val2, final String unit2) {
        if (null != unit1 && !"".equals(unit1)) {
            getSelectionView().getTouchInputTextField().setValue(
                    I18nClientManager.translate(ProductionMo.T29523, false) + " : " + val1 + " " + unit1);
        } else {
            getSelectionView().getTouchInputTextField().setValue("");
        }
        if (null != unit2 && !"".equals(unit2)) {
            getSelectionView().getTouchInputTextField2().setValue(
                    I18nClientManager.translate(ProductionMo.T29523, false) + " : " + val2 + " " + unit2);
        } else {
            getSelectionView().getTouchInputTextField2().setValue("");

        }
    }

    /**
     * Gets the title panel.
     * 
     * @return the JPanel
     */
    private TitlePanel getTitlePanel() {
        if (titlePanel == null) {
            titlePanel = new TitlePanel();
            Dimension d = new Dimension(870, 40);
            titlePanel.setPreferredSize(d);
            titlePanel.setMinimumSize(d);
            titlePanel.setMaximumSize(d);
            titlePanel.setName("titlePanel");
        }
        return titlePanel;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(getTitlePanel());
        add(getSelectionView());
        add(getBrowser());
        add(getViewer());

    }

}

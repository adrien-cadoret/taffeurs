/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookVModel.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;


/**
 * TableLook Viewer Model.
 * 
 * @author kl
 */
public class FTableLookVModel extends ViewerModel<FTableLookVBean> {

    /**
     * Default constructor.
     */
    public FTableLookVModel() {
        super();
        setBeanClass(FTableLookVBean.class);
        getUpdateActions().setCopyEnabled(true);
        getUpdateActions().setDeleteEnabled(true);
        getUpdateActions().setDocEnabled(false);
        getUpdateActions().setInfoEnabled(false);
        getUpdateActions().setNewEnabled(false);
        getUpdateActions().setPrintEnabled(false);
        getUpdateActions().setSaveEnabled(true);
        getUpdateActions().setUndoEnabled(true);
    }
}

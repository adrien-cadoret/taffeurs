/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationVIView.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.browser.AbstractBrowserController;


/**
 * MO creation feature view interface.
 * 
 * @author glc
 */
public interface FMOCreationVIView {

    /**
     * Gets the controller of the browser.
     * 
     * @return the controller of the browser.
     */
    public AbstractBrowserController<CodeLabel> getBrowserController();
}

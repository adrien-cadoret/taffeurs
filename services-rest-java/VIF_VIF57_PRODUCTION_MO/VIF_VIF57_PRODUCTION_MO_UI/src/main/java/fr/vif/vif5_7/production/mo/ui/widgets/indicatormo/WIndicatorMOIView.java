/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WIndicatorMOIView.java,v $
 * Created on 03 avril 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo;


import java.awt.event.ActionListener;
import java.util.List;

import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;


/**
 * Indicator View Interface.
 * 
 * @author ag
 */
public interface WIndicatorMOIView {

    /**
     * Gets the frame.
     * 
     * @return the frame.
     */
    public MOLocalFrame getFrame();

    /**
     * Gets the graph.
     * 
     * @return the graph.
     */
    public GraphMO getGraph();

    /**
     * Gets the indicators.
     * 
     * @return the indicators.
     */
    public List<CodeLabel> getIndicators();

    /**
     * Gets the uniqueId.
     * 
     * @return the uniqueId.
     */
    public String getUniqueId();

    /**
     * Gets the widgetId.
     * 
     * @return the widgetId.
     */
    public String getWidgetId();

    /**
     * Refresh Widget View.
     * 
     */
    public void refresh();

    // public void initialize();

    /**
     * Sets the frame.
     * 
     * @param frame frame.
     */
    public void setFrame(final MOLocalFrame frame);

    /**
     * Sets the graph.
     * 
     * @param graph graph.
     */
    public void setGraph(final GraphMO graph);

    /**
     * Sets the indicators.
     * 
     * @param indicators indicators.
     */
    public void setIndicators(final List<CodeLabel> indicators);

    /**
     * Define Indicators Menu.
     * 
     * @param listener ActionListener
     */
    public void setIndicatorsMenu(final ActionListener listener);

    /**
     * Sets the uniqueId.
     * 
     * @param uniqueId uniqueId.
     */
    public void setUniqueId(final String uniqueId);

    /**
     * Validate all components of Widget View.
     * 
     */
    public void validate();

}

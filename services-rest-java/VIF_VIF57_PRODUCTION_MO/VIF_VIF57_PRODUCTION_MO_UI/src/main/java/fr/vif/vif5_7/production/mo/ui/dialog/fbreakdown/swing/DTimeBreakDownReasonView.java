/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DTimeBreakDownReasonView.java,v $
 * Created on 21 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown.swing;


import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import fr.vif.jtech.ui.dialogs.swing.StandardSwingDialog;
import fr.vif.vif5_7.stock.kernel.ui.composites.creason.swing.CReasonView;


/**
 * View controller for time breakdown reason.
 * 
 * @author cj
 */
public class DTimeBreakDownReasonView extends StandardSwingDialog {
    private CReasonView creasonView = null;

    /**
     * constructor.
     * 
     * @param f Frame
     */
    public DTimeBreakDownReasonView(final Frame f) {
        super(f);
        init();
    }

    /**
     * Gets the creasonView.
     * 
     * @return the creasonView.
     */
    public CReasonView getCreasonView() {
        if (creasonView == null) {
            creasonView = new CReasonView();
            creasonView.setBeanProperty("reasonCL");
        }
        return creasonView;
    }

    /**
     * Initialize the dialog box.
     */
    private void init() {
        this.setSize(new Dimension(200, 100));
        this.setLayout(new GridBagLayout());
        Insets insets = new Insets(2, 2, 2, 2);
        this.add(getCreasonView(), new GridBagConstraints(0, 0, 2, 1, 0, 1, GridBagConstraints.CENTER,
                GridBagConstraints.HORIZONTAL, insets, 0, 0));
        this.add(getOkButton(), new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, insets, 0, 0));
        this.add(getCancelButton(), new GridBagConstraints(1, 1, 1, 1, 0, 0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, insets, 0, 0));
    }
}

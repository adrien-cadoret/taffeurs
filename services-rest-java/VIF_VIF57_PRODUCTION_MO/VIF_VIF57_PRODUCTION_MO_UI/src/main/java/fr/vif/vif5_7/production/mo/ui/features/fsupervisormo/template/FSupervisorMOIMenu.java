/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOIMenu.java,v $
 * Created on 8 déc. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


/**
 * Interface of the supervisor menu.
 * 
 * @author jd
 */
public interface FSupervisorMOIMenu {

    /**
     * Gets the localFrame.
     * 
     * @return the localFrame.
     */
    public MOLocalFrame getLocalFrame();

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputContainerFModel.java,v $
 * Created on 25 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBCtrl;


/**
 * ProductionOutputContainer : Feature Model.
 * 
 * @author vr
 */
public class FProductionOutputContainerFModel extends StandardFeatureModel {

    /**
     * Default constructor.
     * 
     */
    public FProductionOutputContainerFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FProductionOutputContainerBModel.class, null, FProductionBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FProductionOutputContainerVModel.class, null,
                FProductionOutputContainerVCtrl.class));
    }

}

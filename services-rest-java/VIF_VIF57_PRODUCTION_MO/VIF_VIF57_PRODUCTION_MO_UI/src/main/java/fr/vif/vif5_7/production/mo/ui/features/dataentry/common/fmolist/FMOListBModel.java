/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOListBModel.java,v $
 * Created on 07 nov. 2008 by ALB
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListBViewEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.ProductionMoTools;


/**
 * MOList : Browser Model.
 * 
 * @author alb
 */
public class FMOListBModel extends BrowserModel<FMOListBBean> {

    private Format formatQty = new Format(Alignment.RIGHT_ALIGN, MOUIConstants.TOUCHSCREEN_BG_BROWSER,
                                     StandardColor.TOUCHSCREEN_FG_BROWSER,
                                     StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE, "11");

    /**
     * Default constructor.
     * 
     */
    public FMOListBModel() {
        super();
        setBeanClass(FMOListBBean.class);
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29437, false),
                FMOListBViewEnum.DATE.getValue(), 55, "HH:mm"));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T19824, false),
                FMOListBViewEnum.CHRONO.getValue(), 90, "########"));
        addColumn(new BrowserColumn(I18nClientManager.translate(GenKernel.T426, false),
                FMOListBViewEnum.ITEM_ID.getValue(), 190));
        addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T1863, false),
                FMOListBViewEnum.LABEL.getValue(), 305));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T29438, false), "formattedQuantity", 130,
                formatQty));
        addColumn(new BrowserColumn("", "mobean", 45));

        setFullyFetched(true);
        setSelectionEnabled(false);
        setAdvancedSearchEnabled(false);
        setSearchEnabled(false);
        setXlsExportEnabled(false);
        setFetchSize(1000);
        setFirstFetchSize(1000);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FMOListBBean bean) {

        Format format = super.getCellFormat(rowNum, property, cellContent, bean);

        if (bean.getIsFinished()) {
            format.setForegroundColor(StandardColor.GRAY);
        }

        TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL);

        if (touchModel != null) {
            TouchModelFunction touchModelFunction = touchModel
                    .getTouchModelFunction(TouchModelMapKey.CONTAINER_MO_LIST);

            format.setFont(ProductionMoTools.formatCell(format, touchModelFunction, property));

        }

        return format;
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningInputBView.java,v $
 * Created on 26 Apr 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.touch;


import javax.swing.BorderFactory;
import javax.swing.border.AbstractBorder;

import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.browser.touch.BrowserCell;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.browser.touch.renderer.row.DefaultRowRenderer;
import fr.vif.jtech.ui.laf.touch.JToggleButtonBorder;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;


/**
 * Browser view for boning input.
 * 
 * @author cj
 */
public class FBoningInputBView extends TouchBrowser<FProductionBBean> {

    /**
     * Default constructor.
     */
    public FBoningInputBView() {
        super();
        ((DefaultRowRenderer) getRowRenderer()).setRendererType(BrowserModel.RENDERER_BY_ROW);
        setColumns(3);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void refreshJtCells() {
        createEmptyBorder(3);
        super.refreshJtCells();
    }

    /**
     * Create an empty border on left and right side.
     *
     * @param size the size (pixel)
     */
    private void createEmptyBorder(final int size) {
        for (BrowserCell<FProductionBBean> cells : getBrowserCells()) {
            AbstractBorder cellBorder = BorderFactory.createCompoundBorder(new JToggleButtonBorder.Border(),
                    BorderFactory.createEmptyBorder(0, size, 0, size));
            cells.setBorder(cellBorder);
        }

    }

}

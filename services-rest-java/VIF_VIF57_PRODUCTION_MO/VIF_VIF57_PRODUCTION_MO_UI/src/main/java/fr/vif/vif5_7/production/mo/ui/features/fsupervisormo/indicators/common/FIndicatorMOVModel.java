/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOVModel.java,v $
 * Created on 01 Mar 2007 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FIndicatorMOValuesVBean;


/**
 * Lines : Viewer Model.
 * 
 * @author jd
 */
public class FIndicatorMOVModel extends ViewerModel<FIndicatorMOValuesVBean> {

    /**
     * Default constructor.
     * 
     */
    public FIndicatorMOVModel() {
        super();
        setBeanClass(FIndicatorMOValuesVBean.class);
        getUpdateActions().setCopyEnabled(false);
        getUpdateActions().setDeleteEnabled(false);
        getUpdateActions().setDocEnabled(false);
        getUpdateActions().setNewEnabled(false);
        getUpdateActions().setPrintEnabled(false);
        getUpdateActions().setSaveEnabled(false);
        getUpdateActions().setUndoEnabled(false);
    }

}

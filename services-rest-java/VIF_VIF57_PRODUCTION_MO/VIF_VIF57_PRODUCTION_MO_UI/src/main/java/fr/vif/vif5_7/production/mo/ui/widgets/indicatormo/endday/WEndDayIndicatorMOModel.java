/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WEndDayIndicatorMOModel.java,v $
 * Created on 15 avr. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday;


import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.common.beans.RefreshFrequencyBean;
import fr.vif.jtech.common.constants.WidgetEnum.RefreshFrequencyUnits;
import fr.vif.jtech.ui.widget.WidgetModel;
import fr.vif.vif5_7.gen.location.business.beans.features.farea.FAreaBBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.InvoicedArticleBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedOnLineControlBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedTrsLineBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedWorkingPeriodBean;


/**
 * End day indicator model.
 *
 * @author ac
 */
public class WEndDayIndicatorMOModel extends WidgetModel {

    // For refresh frequency
    private static final int                FIVE_MINUTES               = 5;
    private static final int                TEN_MINUTES                = 10;

    /** Current end of day indicators list. */
    private List<InvoicedArticleBean>       invoicedArticleBeans       = null;

    private List<UnclosedWorkingPeriodBean> unclosedWorkingPeriodBeans = null;

    private List<UnclosedTrsLineBean>       unclosedTrsLineBeans       = null;

    private List<UnclosedOnLineControlBean> unclosedOnLineControlBeans = null;

    private List<FAreaBBean>                areas                      = null;

    /**
     * Default Ctor.
     */
    public WEndDayIndicatorMOModel() {
        super();

        // Refresh frequencies
        List<RefreshFrequencyBean> refreshOptions = new ArrayList<RefreshFrequencyBean>();
        RefreshFrequencyBean fiveMinutesFrequency = new RefreshFrequencyBean(FIVE_MINUTES,
                RefreshFrequencyUnits.MINUTES);
        RefreshFrequencyBean tenMinutesFrequency = new RefreshFrequencyBean(TEN_MINUTES, RefreshFrequencyUnits.MINUTES);
        refreshOptions.add(fiveMinutesFrequency);
        refreshOptions.add(tenMinutesFrequency);
        setLstRefreshFrequencyBeans(refreshOptions);
        setManualRefreshActivated(true);
        setRefreshFrequency(tenMinutesFrequency);
    }

    /**
     * Gets the areas.
     *
     * @return the areas.
     */
    public List<FAreaBBean> getAreas() {
        return areas;
    }

    /**
     * Gets the invoicedArticleBeans.
     *
     * @return the invoicedArticleBeans.
     */
    public List<InvoicedArticleBean> getInvoicedArticleBeans() {
        return invoicedArticleBeans;
    }

    /**
     * Gets the unclosedOnLineControlBeans.
     *
     * @return the unclosedOnLineControlBeans.
     */
    public List<UnclosedOnLineControlBean> getUnclosedOnLineControlBeans() {
        return unclosedOnLineControlBeans;
    }

    /**
     * Gets the unclosedTrsLineBeans.
     *
     * @return the unclosedTrsLineBeans.
     */
    public List<UnclosedTrsLineBean> getUnclosedTrsLineBeans() {
        return unclosedTrsLineBeans;
    }

    /**
     * Gets the unclosedWorkingPeriodBeans.
     *
     * @return the unclosedWorkingPeriodBeans.
     */
    public List<UnclosedWorkingPeriodBean> getUnclosedWorkingPeriodBeans() {
        return unclosedWorkingPeriodBeans;
    }

    /**
     * Sets the areas.
     *
     * @param areas areas.
     */
    public void setAreas(final List<FAreaBBean> areas) {
        this.areas = areas;
    }

    /**
     * Sets the invoicedArticleBeans.
     *
     * @param invoicedArticleBeans invoicedArticleBeans.
     */
    public void setInvoicedArticleBeans(final List<InvoicedArticleBean> invoicedArticleBeans) {
        this.invoicedArticleBeans = invoicedArticleBeans;
    }

    /**
     * Sets the unclosedOnLineControlBeans.
     *
     * @param unclosedOnLineControlBeans unclosedOnLineControlBeans.
     */
    public void setUnclosedOnLineControlBeans(final List<UnclosedOnLineControlBean> unclosedOnLineControlBeans) {
        this.unclosedOnLineControlBeans = unclosedOnLineControlBeans;
    }

    /**
     * Sets the unclosedTrsLineBeans.
     *
     * @param unclosedTrsLineBeans unclosedTrsLineBeans.
     */
    public void setUnclosedTrsLineBeans(final List<UnclosedTrsLineBean> unclosedTrsLineBeans) {
        this.unclosedTrsLineBeans = unclosedTrsLineBeans;
    }

    /**
     * Sets the unclosedWorkingPeriodBeans.
     *
     * @param unclosedWorkingPeriodBeans unclosedWorkingPeriodBeans.
     */
    public void setUnclosedWorkingPeriodBeans(final List<UnclosedWorkingPeriodBean> unclosedWorkingPeriodBeans) {
        this.unclosedWorkingPeriodBeans = unclosedWorkingPeriodBeans;
    }

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionContainerMOListVModel.java,v $
 * Created on 7 janv. 09 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioncontainermolist;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;


/**
 * FBoningInput viewer model.
 * 
 * @author gv
 */
public class FProductionContainerMOListVModel extends ViewerModel<FMOListVBean> {

    /**
     * default.
     */
    public FProductionContainerMOListVModel() {
        super();
        setBeanClass(FMOListVBean.class);
        getUpdateActions().setCopyEnabled(false);
        getUpdateActions().setDeleteEnabled(false);
        getUpdateActions().setDocEnabled(false);
        getUpdateActions().setNewEnabled(false);
        getUpdateActions().setPrintEnabled(false);
        getUpdateActions().setSaveEnabled(false);
        getUpdateActions().setUndoEnabled(false);
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputCompFView.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcomp.touch;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.touch.FProductionBView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerFIView;
import fr.vif.vif5_7.workshop.device.ui.composites.devices.scale.touch.CPerpetualScaleWeightTouchView;


/**
 * FProductionInput Feature view.
 * 
 * @author vr
 */
public class FProductionInputCompFView extends StandardTouchFeature implements FProductionInputContainerFIView {

    private FProductionBView               bView;
    private CPerpetualScaleWeightTouchView scaleView;
    private TitlePanel                     subTitlePanel;
    private FProductionInputCompVView      vView;
    private FProductionInputCompGSView     fProductionInputCompGSView = null;

    /**
     * Default contructor.
     */
    public FProductionInputCompFView() {
        super();
        initialize();
    }

    /**
     * Gets the bView.
     * 
     * @category getter
     * @return the bView.
     */
    public FProductionBView getBView() {
        if (bView == null) {
            bView = new FProductionBView();
            bView.setBounds(0, 194, 867, 82);
        }
        return bView;
    }

    /**
     * Gets the fProductionOutputCompGSView.
     * 
     * @return the fProductionOutputCompGSView.
     */
    public FProductionInputCompGSView getfProductionInputCompGSView() {
        if (fProductionInputCompGSView == null) {
            fProductionInputCompGSView = new FProductionInputCompGSView();
            fProductionInputCompGSView.setLocation(0, 140);
            fProductionInputCompGSView.setSize(867, 55);
        }
        return fProductionInputCompGSView;
    }

    /**
     * Get the weight composite.
     * 
     * @return the weight composite
     */
    public CPerpetualScaleWeightTouchView getScaleView() {
        if (scaleView == null) {
            scaleView = new CPerpetualScaleWeightTouchView();
            scaleView.setBounds(0, 0, 867, 100);
            scaleView.setOpaque(false);
        }
        return scaleView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView getViewerView() {
        return getVView();
    }

    /**
     * Gets the vView.
     * 
     * @category getter
     * @return the vView.
     */
    public FProductionInputCompVView getVView() {
        if (vView == null) {
            vView = new FProductionInputCompVView();
            vView.setBounds(0, 300, 870, 300);
        }
        return vView;
    }

    /**
     * Sets the bView.
     * 
     * @category setter
     * @param productionBView bView.
     */
    public void setBView(final FProductionBView productionBView) {
        bView = productionBView;
    }

    /**
     * {@inheritDoc}
     */
    public void setSubTitle(final String subTitle) {
        subTitlePanel.setTitle(subTitle);

    }

    /**
     * Get the subtitle panel.
     * 
     * @return the subtitile panel
     */
    private JPanel getSubTitle() {
        if (subTitlePanel == null) {
            subTitlePanel = new TitlePanel();
            subTitlePanel.setBounds(0, 101, 870, 40);
            subTitlePanel.setBorder(new MatteBorder(1, 0, 0, 2, Color.white));
            subTitlePanel.setPreferredSize(new Dimension(800, 40));
            subTitlePanel.setMinimumSize(new Dimension(500, 40));
        }
        return subTitlePanel;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setPreferredSize(new Dimension(870, 768));
        setLayout(null);
        add(getSubTitle());
        add(getBView());
        add(getVView());
        add(getScaleView());
        add(getfProductionInputCompGSView());
    }
}

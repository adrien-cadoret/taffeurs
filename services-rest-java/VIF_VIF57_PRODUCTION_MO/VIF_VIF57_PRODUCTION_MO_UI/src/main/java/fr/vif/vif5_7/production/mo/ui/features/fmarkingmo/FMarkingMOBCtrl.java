/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOBCtrl.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.DateTimeFormatEnum;
import fr.vif.jtech.common.util.StringHelper;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOSBean;
import fr.vif.vif5_7.production.mo.business.services.features.fmarkingmo.FMarkingMOCBS;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch.FMarkingMOTouchBView;


/**
 * MarkingMO : Browser Controller.
 * 
 * @author lmo
 */
public class FMarkingMOBCtrl extends AbstractBrowserController<FMarkingMOBBean> {
    /** LOGGER. */
    private static final Logger LOGGER        = Logger.getLogger(FMarkingMOBCtrl.class);

    private FMarkingMOCBS       fmarkingMOCBS;

    private boolean             moLineChanged = false;

    /**
     * Default constructor.
     * 
     */
    public FMarkingMOBCtrl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void changeCurrentRow(final int rowNum) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - changeCurrentRow(rowNum=" + rowNum + ")");
        }

        // the user changed the selected row in the browser by changing the Selection (FmarkingMOSBean).
        // the device configuration may have been changed.
        moLineChanged = true;
        super.changeCurrentRow(rowNum);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - changeCurrentRow(rowNum=" + rowNum + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDefaultInitialSelection()");
        }

        FMarkingMOSBean ret = new FMarkingMOSBean();

        ret.setMoDate(new Date());
        // CodeLabel cl = WorkstationHelper.getSharedWorkStation(getView(), getModel().getIdentification());

        // if (cl != null) {
        ret.setWorkStationCL(new CodeLabel(getIdCtx().getWorkstation(), ""));
        // }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDefaultInitialSelection()=" + ret);
        }
        return ret;
    }

    /**
     * Gets the fmarkingMOCBS.
     * 
     * @category getter
     * @return the fmarkingMOCBS.
     */
    public final FMarkingMOCBS getFmarkingMOCBS() {
        return fmarkingMOCBS;
    }

    /**
     * Gets the moLineChanged.
     * 
     * Indicates why the BBean have been changed.
     * 
     * <br>
     * true: the selected line changed because the user changed the configuration thank to the FMarkingMOSBean.
     * 
     * <br>
     * false: the selected line changed because the user clicked on an other row in the browser.
     * 
     * @return the moLineChanged.
     */
    public boolean getMoLineChanged() {
        return moLineChanged;
    }

    /**
     * Reposition to a process chrono.
     * 
     * @param chrono the chrono
     * @param itemCL Code Label of the MO
     * 
     * @return true if the row had been found and changed;
     */
    public boolean repositionToProcess(final Chrono chrono, final CodeLabel itemCL) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - repositionToProcess(chrono=" + chrono + ", itemCL=" + itemCL + ")");
        }
        boolean vRet = false;
        for (int i = 0; i < getModel().getRowCount(); i++) {
            FMarkingMOBBean bbean = getModel().getBeanAt(i);
            // /!\ comparison CodeLabel vs CodeLabels
            if (bbean != null && itemCL.getLabel().equalsIgnoreCase(bbean.getItemCL().getLabel())
                    && bbean.getOperationItemKey().getChrono() != null
                    && bbean.getOperationItemKey().getChrono().equals(chrono)) {
                getModel().setCurrentRowNumber(i);
                BrowserActionEvent event = new BrowserActionEvent(this, BrowserActionEvent.EVENT_SELECTED_ROW_CHANGED,
                        i);
                selectedRowChanged(event);
                vRet = true;

                break;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - repositionToProcess(chrono=" + chrono + ", itemCL=" + itemCL + ")");
        }
        return vRet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final BrowserActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanged(event=" + event + ")");
        }

        // the user changed the selected row in the browser by clicking on the new one.
        // the device configuration as not bean changed.
        moLineChanged = false;
        super.selectedRowChanged(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanged(event=" + event + ")");
        }
    }

    /**
     * Sets the fmarkingMOCBS.
     * 
     * @category setter
     * @param fmarkingMOCBS fmarkingMOCBS.
     */
    public final void setFmarkingMOCBS(final FMarkingMOCBS fmarkingMOCBS) {
        this.fmarkingMOCBS = fmarkingMOCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMarkingMOBBean convert(final Object bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }

        FMarkingMOBBean vRet = getModel().getCurrentBean(); // super.convert(bean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")");
        }
        return vRet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FMarkingMOBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FMarkingMOBBean> lst = null;

        try {
            FMarkingMOSBean vFMarkingMOSBean = (FMarkingMOSBean) selection;
            lst = getFmarkingMOCBS().queryElements(getIdCtx(), vFMarkingMOSBean, startIndex, rowNumber);

            TitlePanel titlePanel = ((FMarkingMOTouchBView) getView()).getTitlePanel();

            if (titlePanel != null && vFMarkingMOSBean != null) {

                titlePanel.setTitle(StringHelper.concat(
                        I18nClientManager.translate(
                                ProductionMo.T35100,
                                false,
                                new VarParamTranslation(DateHelper.formatDate(vFMarkingMOSBean.getMoDate(),
                                        DateHelper.getDateFormatForLocale(DateTimeFormatEnum.DD_MM_YYYY)))), " - ",
                                        vFMarkingMOSBean.getLineCL().getCode()));
            }
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        getSharedContext().put(Domain.DOMAIN_THIS, FMarkingMOUIConstants.CURRENT_SELECTION_BEAN, selection);
        fireSharedContextSend(this, getSharedContext());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + lst);
        }
        return lst;
    }
}

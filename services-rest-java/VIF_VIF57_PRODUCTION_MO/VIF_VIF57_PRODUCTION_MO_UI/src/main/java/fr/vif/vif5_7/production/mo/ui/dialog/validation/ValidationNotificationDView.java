/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: ValidationNotificationDView.java,v $
 * Created on 2 Sep 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.dialog.validation;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.Timer;
import javax.swing.WindowConstants;

import fr.vif.jtech.ui.models.format.StandardFont;


/**
 * Message dialog.
 * 
 * @author cj
 */
public class ValidationNotificationDView extends JDialog {
    private static final long serialVersionUID = 1L;
    private JPanel            jContentPane     = null;
    private JLabel            jlMessage        = null;
    private String            message;

    /**
     * Default constructor.
     * 
     * @param message message.
     */
    public ValidationNotificationDView(final String message) {
        super();
        this.message = message;
        initialize();
    }

    /**
     * This method initializes jContentPane.
     * 
     * @return javax.swing.JPanel.
     */
    private JPanel getJContentPane() {
        if (jContentPane == null) {
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.fill = GridBagConstraints.HORIZONTAL;
            gridBagConstraints.ipadx = 0;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.gridy = 0;

            jlMessage = new JLabel();
            jlMessage.setForeground(Color.GREEN.darker().darker());
            StandardFont sfont = StandardFont.DEFAULT_FONT;
            jlMessage.setFont(new Font(sfont.getName().getName(), Font.BOLD, 16));
            jlMessage.setText(message);
            jlMessage.setHorizontalAlignment(SwingConstants.CENTER);
            jContentPane = new JPanel();
            jContentPane.setLayout(new GridBagLayout());
            jContentPane.add(jlMessage, gridBagConstraints);
        }
        return jContentPane;
    }

    /**
     * This method initializes this.
     */
    private void initialize() {
        this.setSize(498, 90);
        this.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        this.setModal(false);
        this.setResizable(false);
        this.setPreferredSize(new Dimension(300, 200));
        this.setContentPane(getJContentPane());
        Dimension screenSize = getToolkit().getScreenSize();
        Dimension dialogSize = getSize();
        this.setLocation((screenSize.width - dialogSize.width) / 2, 150);
        Timer timer = new Timer(1000, new ActionListener() {
            public void actionPerformed(final ActionEvent e) {
                ValidationNotificationDView.this.setVisible(false);
                ValidationNotificationDView.this.dispose();
            }
        });
        timer.setRepeats(false);
        timer.start();

    }
}

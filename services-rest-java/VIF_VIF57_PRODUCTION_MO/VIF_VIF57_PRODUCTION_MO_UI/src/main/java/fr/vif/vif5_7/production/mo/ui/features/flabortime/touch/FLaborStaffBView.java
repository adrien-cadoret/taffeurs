/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffBView.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.touch;


import java.awt.Component;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.table.TableCellRenderer;

import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.browser.touch.renderer.row.CodeLabelRowRenderer;
import fr.vif.jtech.ui.laf.swing.LAFHelper;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;


/**
 * Labor staff BView.
 * 
 * @author nle
 */
public class FLaborStaffBView extends TouchBrowser<FLaborStaffBBean> {
    /**
     * Renderer for resource code.
     */
    private class ResourceCodeRenderer implements TableCellRenderer {

        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {

            JLabel jl = new JLabel();
            jl.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE));

            jl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            jl.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
            jl.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));

            return jl;
        }
    }

    /**
     * Renderer for resource code.
     */
    private class ResourceLabelRenderer implements TableCellRenderer {

        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {

            JLabel jl = new JLabel();
            jl.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE));

            jl.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
            jl.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
            jl.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));

            return jl;
        }
    }

    /**
     * Default constructor.
     */
    public FLaborStaffBView() {
        super();
        setRowRenderer(new CodeLabelRowRenderer<FLaborStaffBBean>());
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOFIView.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


/**
 * MarkingMO : Feature View Interface.
 * 
 * @author lmo
 */
public interface FMarkingMOFIView {

}

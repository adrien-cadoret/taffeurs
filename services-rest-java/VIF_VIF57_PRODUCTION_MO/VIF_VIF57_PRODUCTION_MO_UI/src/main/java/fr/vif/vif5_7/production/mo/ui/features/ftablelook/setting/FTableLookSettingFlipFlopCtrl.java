/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingFlipFlopCtrl.java,v $
 * Created on 6 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.FlipFlopBean;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.button.BtnStdModel;
import fr.vif.jtech.ui.button.BtnStdView;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.input.AbstractInputFlipFlopController;
import fr.vif.jtech.ui.input.InputFlipFlopModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FSettingBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FSettingSBean;
import fr.vif.vif5_7.production.mo.business.services.features.tablelook.setting.FTableLookSettingCBS;
import fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting.swing.FTableLookSettingDynamicVView;


/**
 * Controller Flip Flop.
 * 
 * @author kl
 */
public class FTableLookSettingFlipFlopCtrl extends AbstractInputFlipFlopController<FSettingBean> {
    /** LOGGER. */
    private static final Logger          LOGGER               = Logger.getLogger(FTableLookSettingFlipFlopCtrl.class);

    private static final String          SELECTED_ROW_CHANGED = "selectedRowChanged";

    private FTableLookSettingCBS         ftableLookSettingCBS;

    private FTableLookSettingSelBrwCtrl  settingSelBrwCtrl    = null;

    private FTableLookSettingResuBrwCtrl settingResuBrwCtrl   = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buildListUsingFilter() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buildListUsingFilter()");
        }

        getSettingSelBrwCtrl().getModel().removeAllBeans();
        getSettingSelBrwCtrl().getModel().setFullyFetched(true);

        List<FSettingBean> lst = new ArrayList<FSettingBean>();
        for (FSettingBean ffBean : getModel().getLstBrwSelSave()) {
            if (!itemSelected(ffBean) && (getModel().getUseFilter() ? applyFilter(ffBean) : true)) {
                lst.add(ffBean);
            }
        }
        getSettingSelBrwCtrl().getModel().addNextBeans(lst);

        if (getSettingSelBrwCtrl().getModel().getRowCount() > 0) {
            getSettingSelBrwCtrl().getModel().setCurrentRowNumber(0);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buildListUsingFilter()");
        }
    }

    /**
     * Gets the ftableLookSettingCBS.
     * 
     * @return the ftableLookSettingCBS.
     */
    public FTableLookSettingCBS getFtableLookSettingCBS() {
        return ftableLookSettingCBS;
    }

    /**
     * Gets the settingResuBrwCtrl.
     * 
     * @return the settingResuBrwCtrl.
     */
    public FTableLookSettingResuBrwCtrl getSettingResuBrwCtrl() {
        return settingResuBrwCtrl;
    }

    /**
     * Gets the settingSelBrwCtrl.
     * 
     * @return the settingSelBrwCtrl.
     */
    public FTableLookSettingSelBrwCtrl getSettingSelBrwCtrl() {
        return settingSelBrwCtrl;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void initResuBean() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initResuBean()");
        }

        int currentRow = getSettingResuBrwCtrl().getModel().getCurrentRowNumber();

        getSettingResuBrwCtrl().getModel().removeAllBeans();
        getSettingResuBrwCtrl().getModel().addNextBeans(
                (List<FSettingBean>) ((FlipFlopBean) getModel().getValue()).getLstResu());

        if (getSettingResuBrwCtrl().getModel().getRowCount() > 0) {
            // MP QA8917 Reposition on the row selected
            if (currentRow > 0 && currentRow <= getSettingResuBrwCtrl().getModel().getRowCount()) {
                getSettingResuBrwCtrl().getModel().setCurrentRowNumber(currentRow);
            } else {
                getSettingResuBrwCtrl().getModel().setCurrentRowNumber(0);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initResuBean()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initSelBean() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initSelBean()");
        }

        getSettingSelBrwCtrl().getModel().removeAllBeans();
        getSettingSelBrwCtrl().getModel().setFullyFetched(true);

        // When a FlipFlop is used into a Dialog box,
        // it is not possible to initialize it with an identification equals to null when the flipflop is not in a
        // fieldHelp
        if (getSettingSelBrwCtrl().getIdentification() != null) {
            getSettingSelBrwCtrl().initialize();
        }

        getModel().setLstBrwSelSave(getSettingSelBrwCtrl().getModel().getBeans());

        if (getSettingSelBrwCtrl().getModel().getRowCount() > 0) {
            getSettingSelBrwCtrl().getModel().setCurrentRowNumber(0);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initSelBean()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<FSettingBean> queryBrwSelection(final Selection selection) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryBrwSelection(selection=" + selection + ")");
        }

        List<FSettingBean> beans = new ArrayList<FSettingBean>();
        try {
            beans = getFtableLookSettingCBS().queryBrwSelection(getIdCtx(), (FSettingSBean) selection, 0, 100);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryBrwSelection(selection=" + selection + ")=" + beans);
        }
        return beans;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanged(event=" + event + ")");
        }

        super.selectedRowChanged(event);

        // Disable the remove and removeAll button if the zone is mandatory.
        if (event.getSelectedBean() != null && event.getSelectedBean() instanceof FSettingBean) {
            FSettingBean bean = (FSettingBean) event.getSelectedBean();
            getView().getImplBtnRemove().enableButton(!bean.isMandatory());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanged(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        if (event.getSource() == getSettingResuBrwCtrl()) {
            getSettingResuBrwCtrl().getViewerCtrl().updateCurrentSettingBean(
                    (FTableLookSettingDynamicVView) getSettingResuBrwCtrl().getViewerCtrl().getView());
            if (getSettingResuBrwCtrl().getResult().size() > 0) {
                updateValue(getSettingResuBrwCtrl().getResult());
                getSettingResuBrwCtrl().getView().initializeView(getSettingResuBrwCtrl());
            }
        }
    }

    /**
     * Sets the ftableLookSettingCBS.
     * 
     * @param ftableLookSettingCBS ftableLookSettingCBS.
     */
    public void setFtableLookSettingCBS(final FTableLookSettingCBS ftableLookSettingCBS) {
        this.ftableLookSettingCBS = ftableLookSettingCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setInitialSelection(final Selection initialSelection) {
        getSettingSelBrwCtrl().setInitialSelection(initialSelection);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void swapItem(final BrowserModel<FSettingBean> srcModel, final BrowserModel<FSettingBean> destModel) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - swapItem(srcModel=" + srcModel + ", destModel=" + destModel + ")");
        }

        int srcRow = srcModel.getCurrentRowNumber();

        FSettingBean bean = srcModel.getBeanAt(srcRow);
        srcModel.deleteBeanAt(srcRow);
        if (srcModel.getRowCount() > 0) {
            if (srcRow == 0) {
                srcModel.setCurrentRowNumber(0);
            } else {
                srcModel.setCurrentRowNumber(srcRow < srcModel.getRowCount() ? srcRow : srcRow - 1);
            }
        }

        destModel.insertBeanAt(bean, destModel.getRowCount());
        destModel.setCurrentRowNumber(destModel.getRowCount() - 1);

        getSettingResuBrwCtrl().sortItems(bean);

        getView().enableButtons(true);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - swapItem(srcModel=" + srcModel + ", destModel=" + destModel + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateValue(final List<FSettingBean> beans) {
        super.updateValue(beans);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void verticalMove(final BrowserModel<FSettingBean> model, final int currentRow, final int newRow) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - verticalMove(model=" + model + ", currentRow=" + currentRow + ", newRow=" + newRow + ")");
        }

        super.verticalMove(model, currentRow, newRow);

        // Bean selected by the user
        FSettingBean selectedBean = model.getBeanAt(newRow);
        int newBeanNligaff = selectedBean.getNligaff();

        // Bean inverse with bean selected by the user
        FSettingBean movedBean = model.getBeanAt(currentRow);
        int currentBeanNligaff = movedBean.getNligaff();

        // Update the nligaff. (will be use when bean will be update).
        movedBean.setNligaff(newBeanNligaff);
        selectedBean.setNligaff(currentBeanNligaff);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - verticalMove(model=" + model + ", currentRow=" + currentRow + ", newRow=" + newRow + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createAddBtn() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createAddBtn()");
        }

        BtnStdView btn = getView().getImplBtnAdd();
        btn.getBtnCtrl().addButtonListener(settingSelBrwCtrl);
        getSettingSelBrwCtrl().addTableRowSelectionListener(btn.getBtnCtrl());

        BtnStdModel btnModel = btn.getBtnModel();
        btnModel.setIconURL(InputFlipFlopModel.BTN_ADD_ICON_URL);
        getModel().setAddBtn(btnModel);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createAddBtn()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createBrwResu() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createBrwResu()");
        }

        BrowserView<FSettingBean> browserView = getView().getImplBrwResu();

        browserView.setModel(getModel().getBrwResuModel());
        settingResuBrwCtrl = new FTableLookSettingResuBrwCtrl(this);
        getSettingResuBrwCtrl().setView(browserView);
        getSettingResuBrwCtrl().initialize();
        getSettingResuBrwCtrl().addTableRowChangeListener(this);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createBrwResu()");
        }
    }

    /**
     * Instantiate the browser which contains the selection.
     */
    @Override
    protected void createBrwSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createBrwSelection()");
        }

        BrowserView<FSettingBean> browserView = getView().getImplBrwSelection();

        browserView.setModel(getModel().getBrwSelModel());
        settingSelBrwCtrl = new FTableLookSettingSelBrwCtrl(this);
        getSettingSelBrwCtrl().setView(browserView);
        getSettingSelBrwCtrl().addTableRowChangeListener(this);
        getSettingSelBrwCtrl().addDisplayListener(this);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createBrwSelection()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createDownBtn() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createDownBtn()");
        }

        BtnStdView btn = getView().getImplBtnDown();
        btn.getBtnCtrl().addButtonListener(getSettingResuBrwCtrl());

        BtnStdModel btnModel = btn.getBtnModel();
        btnModel.setIconURL(InputFlipFlopModel.BTN_DOWN_ICON_URL);
        getModel().setDownBtn(btnModel);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createDownBtn()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createRemoveBtn() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createRemoveBtn()");
        }

        BtnStdView btn = getView().getImplBtnRemove();
        btn.getBtnCtrl().addButtonListener(getSettingResuBrwCtrl());
        getSettingResuBrwCtrl().addTableRowSelectionListener(btn.getBtnCtrl());

        BtnStdModel btnModel = btn.getBtnModel();
        btnModel.setIconURL(InputFlipFlopModel.BTN_REMOVE_ICON_URL);
        getModel().setRemoveBtn(btnModel);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createRemoveBtn()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void createUpBtn() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createUpBtn()");
        }

        BtnStdView btn = getView().getImplBtnUp();
        btn.getBtnCtrl().addButtonListener(getSettingResuBrwCtrl());

        BtnStdModel btnModel = btn.getBtnModel();
        btnModel.setIconURL(InputFlipFlopModel.BTN_UP_ICON_URL);
        getModel().setUpBtn(btnModel);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createUpBtn()");
        }
    }
}

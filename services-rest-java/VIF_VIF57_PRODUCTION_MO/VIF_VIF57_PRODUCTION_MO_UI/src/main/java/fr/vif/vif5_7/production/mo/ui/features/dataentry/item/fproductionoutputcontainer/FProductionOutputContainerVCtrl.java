/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputContainerVCtrl.java,v $
 * Created on 25 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer;


import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif57.stock.entity.business.services.constant.EntityEnum.EntityUnitType;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdowngradeditem.FDowngradedItemSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionHelper;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingSBean;


/**
 * ProductionOutputContainer : Viewer Controller.
 * 
 * @author vr
 */
public class FProductionOutputContainerVCtrl extends
AbstractFProductionOutputVCtrl<FProductionOutputContainerVBean, FProductionOutputContainerCBS> implements
TableRowChangeListener {

    private static final Logger                               LOGGER = Logger.getLogger(FProductionOutputContainerVCtrl.class);

    private Map<MovementAct, FProductionOutputContainerVBean> map    = new HashMap<MovementAct, FProductionOutputContainerVBean>();

    /**
     * {@inheritDoc}
     */
    @Override
    public void fireBeanUpdated(final FProductionOutputContainerVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fireBeanUpdated(bean=" + bean + ")");
        }

        super.fireBeanUpdated(bean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fireBeanUpdated(bean=" + bean + ")");
        }
    }

    /**
     * Gets the map.
     *
     * @return the map.
     */
    public Map<MovementAct, FProductionOutputContainerVBean> getMap() {
        return map;
    }

    @Override
    public void removeCurrent() {
        MovementAct movementAct = FProductionTools.operationItemKeyToMovementAct(getBean().getBBean()
                .getOperationItemBean().getOperationItemKey());
        if (map.containsKey(movementAct)) {
            map.remove(movementAct);
        }
    }

    /**
     * Save current bean to map.
     */
    @Override
    public void saveCurrent() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveCurrent()");
        }

        MovementAct current = FProductionTools.operationItemKeyToMovementAct(getBean().getBBean()
                .getOperationItemBean().getOperationItemKey());
        if (map.containsKey(current)) {
            map.remove(current);
        }
        map.put(current, getBean());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveCurrent()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FProductionOutputContainerVBean convert(final Object pBean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(pBean=" + pBean + ")");
        }

        FProductionBBean bBean = (FProductionBBean) pBean;
        FProductionOutputContainerVBean vBean = null;
        try {
            if (ManagementType.ARCHIVED.equals(((FProductionBBean) pBean).getOperationItemBean().getOperationItemKey()
                    .getManagementType())
                    || !map.containsKey(FProductionTools.operationItemKeyToMovementAct(((FProductionBBean) pBean)
                            .getOperationItemBean().getOperationItemKey()))) {
                vBean = getFproductionOutputContainerCBS().convert(getIdCtx(), bBean, getWorkBean());
            } else {
                vBean = map.get(FProductionTools.operationItemKeyToMovementAct(((FProductionBBean) pBean)
                        .getOperationItemBean().getOperationItemKey()));
                vBean.setBBean((FProductionBBean) pBean);
            }

            // Change batch selection
            getViewerView().getDowngradedItemView().setSelection(
                    FProductionHelper.getFDowngradedItemSBean((OutputOperationItemBean) vBean.getBBean()
                            .getOperationItemBean(), getWorkBean().getRealWorkstationId(),
                            (FDowngradedItemSBean) getViewerView().getDowngradedItemView().getSelection()));

            // Change batch selection
            getViewerView().getItfBatch().setInitialHelpSelection(
                    FProductionHelper.getFDestockingSBean(vBean.getBBean().getOperationItemBean(), vBean.getEntity()
                            .getStockItemBean().getGeneralItemInfos().getItemCL().getCode(),
                            (FDestockingSBean) getViewerView().getItfBatch().getInitialHelpSelection()));

            // FB M00172129 : If scale is connected, QT2 field is set with 0.0
            if (getWorkBean().getScale() != null
                    && vBean.getEntity().getStockItemBean().getEnableFields().getSecondQty()
                    && EntityUnitType.WEIGHT.equals(vBean.getEntity().getStockItemBean().getUnitsInfos()
                            .getSecondUnitType())) {
                vBean.getEntity().getStockItemBean().getKeyboardQties().getSecondQty().setQty(0.0);
            }
            // FB M00172129
        } catch (BusinessException e) {
            getView().show(new UIException(e));
            // throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FProductionOutputContainerVBean bean) throws UIException {
        // Nothing to do

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FProductionOutputContainerVBean insertBean(final FProductionOutputContainerVBean pBean,
            final FProductionOutputContainerVBean pInitialBean) throws UIException, UIErrorsException {
        return null;
    }

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFProductionInputFCtrl.java,v $
 * Created on 27 févr. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer;


import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.ObjectHelper;
import fr.vif.jtech.common.util.exceptions.ObjectException;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.input.InputFieldEvent;
import fr.vif.jtech.ui.events.input.InputFieldListener;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.input.InputBarCodeActionEvent;
import fr.vif.jtech.ui.input.InputBarCodeActionListener;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityLocationKey;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.keys.ContainerKey;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.location.LocationKey;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputBarcodeType;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.TouchDeclarationType;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.AbstractFProductionFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFCtrl;
import fr.vif.vif5_7.stock.entity.ui.dialog.content.DContainerContentCtrl;
import fr.vif.vif5_7.stock.kernel.business.beans.features.quality.nc.fncflow.FNCFlowSharedContextBean;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCConstants;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCActType;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCType;
import fr.vif.vif5_7.stock.kernel.ui.util.StockUITools;
import fr.vif.vif5_7.workshop.device.business.beans.common.devices.ScaleDevice;


/**
 * AbstractFProductionInputFCtrl.
 *
 * @author vr
 * @param <C> the CBS extends FProductionInputContainerCBS
 * @param <V> the viewer bean extends FProductionInputContainerVBean
 */
@SuppressWarnings("unchecked")
public abstract class AbstractFProductionInputFCtrl<C extends FProductionInputContainerCBS, V extends FProductionInputContainerVBean>
extends AbstractFProductionFCtrl implements InputFieldListener, TableRowChangeListener,
InputBarCodeActionListener, DialogChangeListener {

    public static final String     BTN_FLASH_REFERENCE              = "BTN_FLASH_REFERENCE";

    /**
     * Production button reference.
     */
    public static final String     BTN_FPRODUCTION_OUTPUT_REFERENCE = ButtonReference.MO_OUTPUT_BTN.getReference();

    public static final String     FEATURE_CONTENT_CODE             = "VIF.RDTEN";
    public static final String     FDETAIL_ENTRY_CODE               = "VIF.MOINLI1T";
    /** LOGGER. */
    private static final Logger    LOGGER                           = Logger.getLogger(AbstractFProductionInputFCtrl.class);

    private ToolBarBtnStdModel     btnFlashModel;
    private ToolBarBtnFeatureModel btnFContentModel;
    private ToolBarBtnStdModel     btnTattooedModel;

    private OperationItemKey       current;

    private boolean                flashGetQuantities               = false;

    private FMOListVBean           moListVBean                      = null;

    private ScaleDevice            scale                            = null;

    private C                      fproductionInputContainerCBS;

    /**
     * {@inheritDoc}
     */
    @Override
    public void barcodeReceived(final InputBarCodeActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - barcodeReceived(event=" + event + ")");
        }

        interpretBarcode(event.getBarcode());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - barcodeReceived(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        super.buttonAction(event);
        if (FEATURE_CONTENT_CODE.equalsIgnoreCase(event.getModel().getReference())) {
            FContentRateSBean sBean = new FContentRateSBean();

            OperationItemKey oik = getViewerBean().getBBean().getOperationItemBean().getOperationItemKey();

            MOItemKey moKey = new MOItemKey();
            moKey.setChrono(oik.getChrono());
            moKey.setEstablishmentKey(oik.getEstablishmentKey());
            moKey.setCounter1(oik.getCounter1());
            sBean.setMoKey(moKey);
            sBean.setWorkstation(getViewerCtrl().getWorkBean().getLogicalWorkstationId());

            EntityLocationKey entLocKey = getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey();
            LocationKey location = new LocationKey(entLocKey.getCsoc(), entLocKey.getCetab(), entLocKey.getCdepot(),
                    entLocKey.getCemp());
            sBean.setLocation(location);

            getSharedContext().put(Domain.DOMAIN_THIS, FEATURE_CONTENT_CODE, sBean);
            fireSharedContextSend(getSharedContext());
        } else if (event.getModel().getReference().equals(BTN_DETAIL_REFERENCE)) {
            openChildFeature(FDETAIL_ENTRY_CODE);
            getViewerCtrl().setPrevious(null); // Maybe delete done in movement list
        } else if (BTN_FINISH_REFERENCE.equals(event.getModel().getReference())) {
            processFinishResume();
            try {
                boolean isOutputContainer = getFproductionInputContainerCBS().isStateOuputContainerButton(
                        getViewerCtrl().getIdCtx(),
                        getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCsoc(),
                        getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCetab(),
                        getViewerCtrl().getWorkBean().getLogicalWorkstationId(),
                        getViewerCtrl().getWorkBean().getMoKey());

                getViewerCtrl().getWorkBean().setOutputAvailable(isOutputContainer);
                getViewerCtrl().fireButtonStatechanged();
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }

        } else if (BTN_SHOW_ALL_REFERENCE.equals(event.getModel().getReference())) {
            processShowAllEvent(event);
        } else if (BTN_FLASH_REFERENCE.equals(event.getModel().getReference())) {
            if (InputBarcodeType.KEEP_QTIES.equals(getViewerCtrl().getWorkBean().getBarcodeType())) {
                getViewerCtrl().getWorkBean().setBarcodeType(InputBarcodeType.IGNORE_QTIES);
            } else {
                getViewerCtrl().getWorkBean().setBarcodeType(InputBarcodeType.KEEP_QTIES);
            }
            manageBtnFlash();
        } else if (BTN_WEIGHT_REFERENCE.equals(event.getModel().getReference())) {
            FCommScaleResult result = null;
            try {
                result = getFeatureView().getScaleView().getModel().getAndLockResult();
                getViewerCtrl().setWeightResult(result);
            } catch (UIException e) {
                getViewerCtrl().fireErrorsChanged(e);
            }
        } else if (BTN_CANCELLAST_REFERENCE.equals(event.getModel().getReference())) {
            if (getViewerCtrl().getPrevious() != null) {
                int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                        I18nClientManager.translate(ProductionMo.T29578, false), MessageButtons.YES_NO,
                        MessageButtons.DEFAULT_NO);
                if (ret == MessageButtons.YES) {
                    try {
                        getFproductionInputContainerCBS().deleteLast(getIdCtx(),
                                getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey(),
                                getViewerCtrl().getPrevious());
                        getBrowserCtrl().reopenQuery();
                        MovementAct ma = getViewerCtrl().getPrevious();
                        getBrowserCtrl().repositionToOperationItemKey(new OperationItemKey(ManagementType.REAL, //
                                getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey(), //
                                getViewerCtrl().getWorkBean().getMoKey().getChrono(), //
                                ma.getNumber1(), //
                                ma.getNumber2(), //
                                ma.getNumber3(), //
                                ma.getNumber4()));
                        getViewerCtrl().setPrevious(null);
                        getViewerCtrl().fireButtonStatechanged();
                    } catch (BusinessException e) {
                        getViewerCtrl().fireErrorsChanged(new UIException(e));
                    } catch (BusinessErrorsException e) {
                        getViewerCtrl().fireErrorsChanged(new UIErrorsException(e));
                    }

                }
            }

        } else if (ButtonReference.ASK_PRODUCED_ENTITY_BTN.getReference().equals(event.getModel().getReference())) {
            getViewerCtrl().askProducedEntity();
        } else if (ButtonReference.SETTING_INFORMATION.getReference().equals(event.getModel().getReference())) {
            viewSettingInformation(getViewerBean().getInputItemParameters().getProfileId(), getViewerBean()
                    .getInputItemParameters().getProfileLabel());
        } else if (ButtonReference.ASK_TATTOOED_NUMBER.getReference().equals(event.getModel().getReference())) {
            getViewerCtrl().askTattooedNumber();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void childFeatureClosed(final FeatureController childFeatureController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }

        super.childFeatureClosed(childFeatureController);
        getBrowserCtrl().reopenQuery();
        // Reposition to current bean
        if (current != null) {
            getBrowserCtrl().repositionToOperationItemKey(current);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closingRequired(final ClosingEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - closingRequired(event=" + event + ")");
        }

        super.closingRequired(event);
        // ---------------------------------------------------------------------
        // if the workstation has a scale --> stop it before closing the feature
        // ---------------------------------------------------------------------
        if (getViewerCtrl().getWorkBean().getScale() != null
                && !getViewerCtrl().getWorkBean().getScale().getFDDIP().isEmpty()) {
            getFeatureView().getScaleView().getController().stopJmsAction();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - closingRequired(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fireBtnStdToolBarStateChangedEvent() {
        // To change visibility.
        super.fireBtnStdToolBarStateChangedEvent();
    }

    /**
     * Gets the fproductionInputContainerCBS.
     *
     * @return the fproductionInputContainerCBS.
     */
    public C getFproductionInputContainerCBS() {
        return fproductionInputContainerCBS;
    }

    /**
     * Gets the moListVBean.
     * 
     * @return the moListVBean.
     */
    public FMOListVBean getMoListVBean() {
        return moListVBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        TouchModelFunction touchModelFunction = null;
        if (getBrowserController() instanceof FProductionBCtrl) {

            TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL);
            touchModelFunction = touchModel.getTouchModelFunction(TouchModelMapKey.CONTAINER_INPUT_PRODUCTION);

        }
        ((FProductionBCtrl) getBrowserController()).changeColumns(touchModelFunction);
        super.initialize();

    }

    /**
     * Interprets a barcode.
     * 
     * @param barcode bar code
     */
    public void interpretBarcode(final String barcode) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - interpretBarcode(barcode=" + barcode + ")");
        }

        getViewerCtrl().fireErrorsChanged();
        getViewerCtrl().getModel().setInitialBean(getViewerBean());
        try {
            V vBean = (V) ObjectHelper.copy(getViewerController().getBean());
            vBean = (V) getFproductionInputContainerCBS().analyseBarcode(getIdCtx(), barcode, vBean,
                    (FProductionSBean) getBrowserController().getInitialSelection(), getViewerCtrl().getWorkBean());
            if (!vBean
                    .getBBean()
                    .getOperationItemBean()
                    .getOperationItemKey()
                    .equals(((FProductionBBean) getBrowserController().getModel().getCurrentBean())
                            .getOperationItemBean().getOperationItemKey())) {
                getBrowserCtrl().repositionToOperationItemKey(
                        vBean.getBBean().getOperationItemBean().getOperationItemKey());
            }
            getViewerCtrl().getModel().setBean(vBean);
            getViewerCtrl().getView().render();
            getViewerCtrl().manageCriteriaWindow(); // ALB M162494
            if (vBean.getAutoInsert()) {
                // Flash -> Data entry inserted
                getViewerCtrl().fireBeanUpdated(vBean);
                getViewerCtrl().afterValidation();
                getViewerCtrl().getModel().setInitialBean(getViewerBean());
            } else {
                // Flash -> Need more informations (answers question on item batch choice
                if (!vBean.getOneBatchContainer() || !vBean.getOneItemContainer()) {
                    // User must choose batch item
                    DContainerContentCtrl.showContent(this.getView(), getViewerCtrl(), getViewerCtrl(), getModel()
                            .getIdentification(), new ContainerKey(vBean.getEntity().getEntityLocationBean()
                            .getEntityLocationKey().getCsoc(), vBean.getEntity().getEntityLocationBean()
                            .getEntityLocationKey().getCetab(), vBean.getEntity().getEntityLocationBean()
                            .getEntityLocationKey().getContainerNumber()));
                } else if (getViewerCtrl().isDataEntryOK()) {
                    // Ask informations and try again to insert
                    getViewerCtrl().performSave();
                }
            }
        } catch (BusinessException e) {
            getViewerCtrl().emptyVBean();
            getViewerCtrl().fireErrorsChanged(new UIException(e));
        } catch (BusinessErrorsException e) {
            getViewerCtrl().emptyVBean();
            getViewerCtrl().showUIErrorsException(new UIErrorsException(e));
        } catch (ObjectException e) {
            // Nothing to do
            LOGGER.equals(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - interpretBarcode(barcode=" + barcode + ")");
        }
    }

    /**
     * Load the viewer bean after changing the container number.
     * 
     * @param vBean the new viewer bean
     */
    public void interpretContainer(final FProductionInputContainerVBean vBean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - interpretContainer(vBean=" + vBean + ")");
        }

        getViewerCtrl().fireErrorsChanged();
        getViewerCtrl().getModel().setInitialBean(getViewerBean());
        try {
            if (!vBean
                    .getBBean()
                    .getOperationItemBean()
                    .getOperationItemKey()
                    .equals(((FProductionBBean) getBrowserController().getModel().getCurrentBean())
                            .getOperationItemBean().getOperationItemKey())) {
                getViewerCtrl().getModel().setInitialBean(vBean);
                getBrowserCtrl().repositionToOperationItemKey(
                        vBean.getBBean().getOperationItemBean().getOperationItemKey());
            }
            getViewerCtrl().getModel().setBean(vBean);
            getViewerCtrl().getView().render();
            getViewerCtrl().manageCriteriaWindow(); // ALB M162494
            if (vBean.getAutoInsert()) {
                // Flash -> Data entry inserted
                getViewerCtrl().fireBeanUpdated(vBean);
                getViewerCtrl().afterValidation();
                getViewerCtrl().getModel().setInitialBean(getViewerBean());
            } else {
                // Flash -> Need more informations (answers question on item batch choice
                if (!vBean.getOneBatchContainer() || !vBean.getOneItemContainer()) {
                    // User must choose batch item
                    DContainerContentCtrl.showContent(this.getView(), getViewerCtrl(), getViewerCtrl(), getModel()
                            .getIdentification(), new ContainerKey(vBean.getEntity().getEntityLocationBean()
                                    .getEntityLocationKey().getCsoc(), vBean.getEntity().getEntityLocationBean()
                                    .getEntityLocationKey().getCetab(), vBean.getEntity().getEntityLocationBean()
                                    .getEntityLocationKey().getContainerNumber()));
                } else if (getViewerCtrl().isDataEntryOK()) {
                    // Ask informations and try again to insert
                    getViewerCtrl().performSave();
                }

            }
        } catch (BusinessException e) {
            getViewerCtrl().fireErrorsChanged(new UIException(e));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - interpretContainer(vBean=" + vBean + ")");
        }
    }

    /**
     * Manage flash button.
     */
    public void manageBtnFlash() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageBtnFlash()");
        }

        // getViewerCtrl().setFlashQuantites(!getViewerCtrl().getFlashQuantites());
        if (InputBarcodeType.KEEP_QTIES.equals(getViewerCtrl().getWorkBean().getBarcodeType())) {
            getBtnFlashModel().setIconURL("/images/vif/vif57/production/mo/noflash.png");
            getBtnFlashModel().setText(I18nClientManager.translate(ProductionMo.T29547, false));
            getViewerCtrl().getViewerView().setFlashLabel(I18nClientManager.translate(ProductionMo.T29695, false));
        } else {
            getBtnFlashModel().setIconURL("/images/vif/vif57/production/mo/flash.png");
            getBtnFlashModel().setText(I18nClientManager.translate(ProductionMo.T29546, false));
            getViewerCtrl().getViewerView().setFlashLabel(I18nClientManager.translate(ProductionMo.T29697, false));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageBtnFlash()");
        }
    }

    /**
     * Manage the visibility for the content rate button.
     * 
     * @param ret boolean : is the button visible ?
     */
    public void manageContentRateButton(final boolean ret) {
        getBtnFContentModel().setVisible(ret);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanged(event=" + event + ")");
        }

        super.selectedRowChanged(event);

        if (event != null && event.getSelectedBean() != null) {
            FProductionBBean bBean = (FProductionBBean) event.getSelectedBean();
            if (bBean != null
                    && bBean.getOperationItemBean() != null
                    && bBean.getOperationItemBean() instanceof InputOperationItemBean
                    && TouchDeclarationType.SHOW_AUTOMATIC_TOUCH_DECLARATION.equals(bBean.getOperationItemBean()
                            .getTouchDeclarationType())) {
                getViewerCtrl().getViewerView().hideAll(true);
            } else {
                getViewerCtrl().getViewerView().hideAll(false);
                getViewerCtrl().getView().enableComponents(false);
            }
            manageFinishResumeButton(bBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanged(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanging(event=" + event + ")");
        }

        getViewerCtrl().getModel().setInitialBean(getViewerBean());
        super.selectedRowChanging(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanging(event=" + event + ")");
        }
    }

    /**
     * Sets the fproductionInputContainerCBS.
     *
     * @param fproductionInputContainerCBS fproductionInputContainerCBS.
     */
    public void setFproductionInputContainerCBS(final C fproductionInputContainerCBS) {
        this.fproductionInputContainerCBS = fproductionInputContainerCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final InputFieldEvent event) {
        // Nothing to do

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void actionOpenNewIncident() {

        // Prepare parameters bean
        FNCFlowSharedContextBean parametersBean = new FNCFlowSharedContextBean();
        parametersBean.setType(NCType.INTERNAL.getKey());

        // Feed with viewer bean informations
        FProductionInputContainerVBean vBean = (FProductionInputContainerVBean) getViewerController().getBean();
        parametersBean.setActType(NCActType.MANUFACTURING_ORDER.getKey());
        if (vBean.getBBean().getOperationItemBean() != null) { // ALB Q7369 If all the MO's item are finished, there was
            // null pointer exception
            parametersBean.setChronoOrigin(vBean.getBBean().getOperationItemBean().getOperationItemKey().getChrono());
        }
        parametersBean.setContainerNumber(vBean.getEntity().getEntityLocationBean().getEntityLocationKey()
                .getContainerNumber());
        StockItem stockItem = new StockItem();
        stockItem.setItemId(vBean.getEntity().getStockItemBean().getStockItem().getItemId());
        stockItem.setBatch(vBean.getEntity().getStockItemBean().getStockItem().getBatch());
        stockItem.setQuantities(vBean.getEntity().getStockItemBean().getKeyboardQties());
        parametersBean.setStockItem(stockItem);

        // Put parameters in context and open feature
        getSharedContext().put(Domain.DOMAIN_THIS, NCConstants.NCFLOW_KEY_SHARED_CONTEXT, parametersBean);
        if (StockUITools.isTouchRunning()) {
            openChildFeature(NCConstants.FeatureName.FLOW_TOUCH.getName());
        } else {
            openChildFeature(NCConstants.FeatureName.FLOW.getName());
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = new ArrayList<ToolBarBtnStdBaseModel>();
        lst.addAll(super.manageActionsButtonModels(TouchModelMapKey.CONTAINER_INPUT_PRODUCTION));
        lst.add(getBtnFContentModel());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lst;
    }

    /**
     * Gets the btnFContentModel.
     * 
     * @category getter
     * @return the btnFContentModel.
     */
    protected ToolBarBtnFeatureModel getBtnFContentModel() {
        if (btnFContentModel == null) {
            btnFContentModel = new ToolBarBtnFeatureModel();
            btnFContentModel.setText(I18nClientManager.translate(ProductionMo.T34234, false));
            btnFContentModel.setIconURL("/images/vif/vif57/production/mo/img48x48/teneur.png");
            btnFContentModel.setReference(FEATURE_CONTENT_CODE);
            btnFContentModel.setFunctionality(false);
            btnFContentModel.setFeatureId(FEATURE_CONTENT_CODE);
        }
        return btnFContentModel;
    }

    /**
     * Gets the btnFlashModel.
     * 
     * @category getter
     * @return the btnFlashModel.
     */
    protected ToolBarBtnStdModel getBtnFlashModel() {
        if (btnFlashModel == null) {
            btnFlashModel = new ToolBarBtnStdModel();
            btnFlashModel.setIconURL("/images/vif/vif57/production/mo/flash.png");
            btnFlashModel.setReference(BTN_FLASH_REFERENCE);
            btnFlashModel.setSelected(true);
            btnFlashModel.setText(I18nClientManager.translate(ProductionMo.T29546, false));
            btnFlashModel.setToggleType(true);
        }
        return btnFlashModel;
    }

    /**
     * Gets the btnTattooedModel.
     *
     * @return the btnTattooedModel.
     */
    protected ToolBarBtnStdModel getBtnTattooedModel() {
        if (this.btnTattooedModel == null) {
            this.btnTattooedModel = new ToolBarBtnStdModel();
            this.btnTattooedModel.setReference(ButtonReference.ASK_TATTOOED_NUMBER.getReference());
            this.btnTattooedModel.setText(I18nClientManager.translate(ProductionMo.T39164, false));
            this.btnTattooedModel.setIconURL("/images/vif/vif57/production/mo/img48x48/tattooed_input_48.png");
        }
        return btnTattooedModel;
    }

    @Override
    protected List<PDFFile> getDocumentsToShow() throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDocumentsToShow()");
        }

        MOItemKey moItemKey = new MOItemKey();

        moItemKey.setEstablishmentKey(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getEstablishmentKey());
        moItemKey.setChrono(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getChrono());
        moItemKey.setCounter1(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getCounter1());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDocumentToShow()");
        }
        return fproductionInputContainerCBS.getPdfFilesOfCurrentItem(getIdCtx(), moItemKey);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getExternalButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getExternalButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = new ArrayList<ToolBarBtnStdBaseModel>();
        if (!StringUtils.isEmpty(getViewerCtrl().getWorkBean().getInputWorkstationParameters()
                .getDefaultTattoedPackaging())) {
            lst.add(getBtnTattooedModel());
        }
        lst.add(getBtnCancelLastModel());
        lst.add(getBtnFlashModel());
        lst.add(getBtnWeightModel());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getExternalButtonModels()");
        }
        return lst;
    }

    /**
     * Get the feature view.
     * 
     * @return the feature view
     */
    protected FProductionInputContainerFIView getFeatureView() {
        return (FProductionInputContainerFIView) getView();

    }

    /**
     * Gets the scale.
     *
     * @return the scale.
     */
    protected ScaleDevice getScale() {
        return scale;
    }

    /**
     * Get the viewer controller.
     * 
     * @return the viewer controller
     */
    protected AbstractFProductionInputVCtrl getViewerCtrl() {
        return (AbstractFProductionInputVCtrl) getViewerController();
    }

    /**
     * Manage button.
     * 
     * @param event to get button to manage it.
     */
    protected void manageBtnShowAllModelText(final ButtonEvent event) {

        FProductionSBean sBean = (FProductionSBean) getBrowserController().getInitialSelection();
        ToolBarBtnStdModel buttonStd = (ToolBarBtnStdModel) event.getModel();
        if (!sBean.getAll()) {
            buttonStd.setText(I18nClientManager.translate(ProductionMo.T29545, false));
            buttonStd.setIconURL("/images/vif/vif57/production/mo/showfinished.png");
        } else {
            buttonStd.setText(I18nClientManager.translate(ProductionMo.T29525, false));
            buttonStd.setIconURL("/images/vif/vif57/production/mo/hidefinished.png");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void openChildFeature(final String featureId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openChildFeature(featureId=" + featureId + ")");
        }

        if (featureId.equals("VIF.MOINLI1T")) {
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.FEATURE_TITLE, getModel().getTitle());
            getSharedContext().put(Domain.DOMAIN_THIS, FDataEntryListFCtrl.OPERATION_ITEM_BEAN,
                    getViewerBean().getBBean().getOperationItemBean());
            getSharedContext().put(Domain.DOMAIN_THIS, "inputEntityType",
                    getViewerBean().getInputItemParameters().getEntityType());
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL,
                    getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL));
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL,
                    getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.USE_TOUCH_MODEL));
            fireSharedContextSend(getSharedContext());
            current = getViewerBean().getBBean().getOperationItemBean().getOperationItemKey();
        } else if (featureId.equals("VIF.MOOUSCRAPDE1T")) {
            FMOListVBean vBean = (FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.MO_LIST_VBEAN);
            FMOListBBean bBean = (FMOListBBean) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.MO_LIST_BBEAN);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_LIST_VBEAN, vBean);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_LIST_BBEAN, bBean);
            fireSharedContextSend(getSharedContext());
        }
        super.openChildFeature(featureId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openChildFeature(featureId=" + featureId + ")");
        }
    }

    /**
     * Sets the moListVBean.
     *
     * @param moListVBean moListVBean.
     */
    protected void setMoListVBean(final FMOListVBean moListVBean) {
        this.moListVBean = moListVBean;
    }

    /**
     * Sets the scale.
     *
     * @param scale scale.
     */
    protected void setScale(final ScaleDevice scale) {
        this.scale = scale;
    }

    /**
     * Return the current viewer bean.
     *
     * @return the viewer bean
     */
    private V getViewerBean() {
        return (V) getViewerCtrl().getBean();
    }

    /**
     * Finishes or unfinishes an item.
     */
    private void processFinishResume() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processFinishResume()");
        }

        int result = 0;
        if (getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                .equals(ManagementType.REAL)) {
            result = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29549, false), MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {
                try {
                    String question = getFproductionInputContainerCBS().finishOperationItem(getIdCtx(),
                            getViewerBean().getBBean().getOperationItemBean().getOperationItemKey(),
                            getIdCtx().getWorkstation(), ActivityItemType.OUTPUT, false);
                    if (question != null
                            && !question.isEmpty()
                            && getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                                    question + "\n" + I18nClientManager.translate(ProductionMo.T29570, false),
                                    MessageButtons.YES_NO) == MessageButtons.YES) {

                        getFproductionInputContainerCBS().finishOperationItem(getIdCtx(),
                                getViewerBean().getBBean().getOperationItemBean().getOperationItemKey(),
                                getIdCtx().getWorkstation(), ActivityItemType.OUTPUT, true);
                    }
                    if (getViewerBean().getBBean().getOperationItemBean() != null) {
                        current = getViewerBean().getBBean().getOperationItemBean().getOperationItemKey();
                        current.setManagementType(ManagementType.ARCHIVED);
                    }
                } catch (BusinessException e) {
                    getView().show(new UIException(e));
                }
            }
        } else if (getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                .equals(ManagementType.ARCHIVED)) {
            result = getView().showQuestion("Question", I18nClientManager.translate(ProductionMo.T29548, false),
                    MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {
                try {
                    getFproductionInputContainerCBS().resumeOperationItem(getIdCtx(),
                            getViewerBean().getBBean().getOperationItemBean().getOperationItemKey(),
                            getIdCtx().getWorkstation());
                    if (getViewerBean().getBBean().getOperationItemBean() != null) {
                        current = getViewerBean().getBBean().getOperationItemBean().getOperationItemKey();
                        current.setManagementType(ManagementType.REAL);
                    }
                } catch (BusinessException e) {
                    getView().show(new UIException(e));
                }
            }
        }
        getBrowserController().reopenQuery();
        if (current != null) {
            getBrowserCtrl().repositionToOperationItemKey(current);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processFinishResume()");
        }
    }

    /**
     * Process the 'showall' event.
     * 
     * @param event to get button to manage it.
     */
    private void processShowAllEvent(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processShowAllEvent()");
        }

        FProductionSBean sBean = (FProductionSBean) getBrowserController().getInitialSelection();
        sBean.setAll(!sBean.getAll());
        getViewerCtrl().getWorkBean().setShowFinished(sBean.getAll());
        manageBtnShowAllModelText(event);
        if (getViewerBean().getBBean().getOperationItemBean() != null) {
            current = getViewerBean().getBBean().getOperationItemBean().getOperationItemKey();
        }
        getBrowserController().reopenQuery();
        if (current != null) {
            getBrowserCtrl().repositionToOperationItemKey(current);
            current = null;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processShowAllEvent()");
        }
    }

}

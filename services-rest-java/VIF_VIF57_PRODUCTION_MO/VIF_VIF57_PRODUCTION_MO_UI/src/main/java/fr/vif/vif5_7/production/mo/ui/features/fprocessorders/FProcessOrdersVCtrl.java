/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersVCtrl.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersVBean;
import fr.vif.vif5_7.production.mo.business.services.features.fprocessorders.FProcessOrdersCBS;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;


/**
 * ProcessOrders : Viewer Controller.
 * 
 * @author jd
 */
public class FProcessOrdersVCtrl extends AbstractStandardViewerController<FProcessOrdersVBean> {

    private static final Logger LOGGER = Logger.getLogger(FProcessOrdersVCtrl.class);

    private FProcessOrdersCBS   fprocessOrdersCBS;

    /**
     * Gets the fprocessOrdersCBS.
     * 
     * @category getter
     * @return the fprocessOrdersCBS.
     */
    public final FProcessOrdersCBS getFprocessOrdersCBS() {
        return fprocessOrdersCBS;
    }

    /**
     * Sets the fprocessOrdersCBS.
     * 
     * @category setter
     * @param fprocessOrdersCBS fprocessOrdersCBS.
     */
    public final void setFprocessOrdersCBS(final FProcessOrdersCBS fprocessOrdersCBS) {
        this.fprocessOrdersCBS = fprocessOrdersCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean pCreationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String pBeanProperty, final Object propertyValue) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FProcessOrdersVBean convert(final Object pBean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(pBean=" + pBean + ")");
        }

        FProcessOrdersVBean vBean = new FProcessOrdersVBean();

        if (pBean instanceof FProcessOrdersBBean) {
            FProcessOrdersBBean bBean = (FProcessOrdersBBean) pBean;
            // IndicatorMOSelectionBean selection = new IndicatorMOSelectionBean(bBean.getPoDate(), bBean.getPoKey()
            // .getEstablishmentKey());
            // selection.setPoKey(bBean.getPoKey());
            try {

                vBean.setIndicatorMO(getFprocessOrdersCBS().getIndicatorMO(getIdCtx(), bBean.getPoKey()));

                vBean.setEffectiveDuration(FSupervisorMOTools.calculateDuration(vBean.getIndicatorMO().getInfos()
                        .getEffectiveBeginDate(), vBean.getIndicatorMO().getInfos().getEffectiveEndDate()));
                vBean.setExpectedDuration(FSupervisorMOTools.calculateDuration(vBean.getIndicatorMO().getInfos()
                        .getExpectedBeginDate(), vBean.getIndicatorMO().getInfos().getExpectedEndDate()));

                vBean.setStateLabel(I18nClientManager.translate(vBean.getIndicatorMO().getInfos().getState().getName()));

            } catch (BusinessException e) {
                UIException ue = new UIException(e);
                throw ue;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FProcessOrdersVBean bean) throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FProcessOrdersVBean updateBean(final FProcessOrdersVBean bean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(bean=" + bean + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(bean=" + bean + ")=" + null);
        }
        return null;
    }
}

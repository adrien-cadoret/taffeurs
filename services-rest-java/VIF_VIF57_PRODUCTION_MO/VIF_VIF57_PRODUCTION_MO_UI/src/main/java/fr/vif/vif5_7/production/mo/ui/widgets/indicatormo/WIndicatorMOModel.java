/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WIndicatorMOModel.java,v $
 * Created on 03 avril 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo;


import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.common.beans.RefreshFrequencyBean;
import fr.vif.jtech.common.constants.WidgetEnum.RefreshFrequencyUnits;
import fr.vif.jtech.ui.widget.WidgetModel;


/**
 * Widget Indicator Model.
 * 
 * @author ag
 */
public class WIndicatorMOModel extends WidgetModel {

    /**
     * Constructor.
     */
    public WIndicatorMOModel() {
        super();

        // CHECKSTYLE:OFF

        RefreshFrequencyBean defaultFrequency = new RefreshFrequencyBean(30, RefreshFrequencyUnits.MINUTES);
        setRefreshFrequency(defaultFrequency);

        List<RefreshFrequencyBean> lstRefreshFrequencyBeans = new ArrayList<RefreshFrequencyBean>();
        lstRefreshFrequencyBeans.add(new RefreshFrequencyBean(5, RefreshFrequencyUnits.MINUTES));
        lstRefreshFrequencyBeans.add(defaultFrequency);
        lstRefreshFrequencyBeans.add(new RefreshFrequencyBean(60, RefreshFrequencyUnits.MINUTES));
        lstRefreshFrequencyBeans.add(new RefreshFrequencyBean(120, RefreshFrequencyUnits.MINUTES));
        setLstRefreshFrequencyBeans(lstRefreshFrequencyBeans);
        // CHECKSTYLE:ON

        setManualRefreshActivated(true);
    }
}

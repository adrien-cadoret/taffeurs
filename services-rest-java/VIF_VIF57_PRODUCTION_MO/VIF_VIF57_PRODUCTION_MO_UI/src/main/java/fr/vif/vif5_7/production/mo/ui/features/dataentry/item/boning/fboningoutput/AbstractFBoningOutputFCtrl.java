/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFBoningOutputFCtrl.java,v $
 * Created on 6 mars 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputSBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningcommon.AbstractFBoningFCtrl;


/**
 * AbstractFProductionOutputFCtrl.
 * 
 * @author vr
 */
public abstract class AbstractFBoningOutputFCtrl extends AbstractFBoningFCtrl {

    /**
     * Close container.
     */
    public static final String     BTN_CLOSE_CONTAINER             = "BTN_CLOSE_CONTAINER";

    /**
     * Production button reference.
     */
    public static final String     BTN_FPRODUCTION_INPUT_REFERENCE = "BTN_FPRODUCTION_INPUT_REFERENCE";

    private static final String    FDETAIL_ENTRY_CODE              = "VIF.MOOULI1T";

    private ToolBarBtnStdModel     btnCloseContainerModel;

    private ToolBarBtnFeatureModel btnDetailModel;

    /**
     * {@inheritDoc}
     */
    @Override
    public void manageFinishResumeButton(final Object bb) {
        FBoningOutputBBean bbean = (FBoningOutputBBean) bb;
        if (bbean != null) {
            if (ManagementType.ARCHIVED.equals(bbean.getOperationItemBean().getOperationItemKey().getManagementType())) {
                getBtnFinishResumeModel().setText(I18nClientManager.translate(ProductionMo.T29502, false));
                getBtnFinishResumeModel().setIconURL("/images/vif/vif57/production/mo/resume.png");
            } else if (ManagementType.REAL.equals(bbean.getOperationItemBean().getOperationItemKey()
                    .getManagementType())) {
                getBtnFinishResumeModel().setText(I18nClientManager.translate(ProductionMo.T29510, false));
                getBtnFinishResumeModel().setIconURL("/images/vif/vif57/production/mo/finish.png");
            }
        }
    }

    /**
     * Gets the btnFProductionInputModel.
     * 
     * @category getter
     * @return the btnFProductionInputModel.
     */
    protected ToolBarBtnStdModel getBtnCloseContainerModel() {
        if (this.btnCloseContainerModel == null) {
            this.btnCloseContainerModel = new ToolBarBtnStdModel();
            this.btnCloseContainerModel.setReference(BTN_CLOSE_CONTAINER);
            this.btnCloseContainerModel.setText(I18nClientManager.translate(ProductionMo.T29710, false));
            this.btnCloseContainerModel.setIconURL("/images/vif/vif57/production/mo/img48x48/CloseContainer.png");
        }
        return this.btnCloseContainerModel;
    }

    /**
     * Gets the btnDetailModel.
     * 
     * @category getter
     * @return the btnDetailModel.
     */
    protected ToolBarBtnFeatureModel getBtnDetailModel() {
        if (this.btnDetailModel == null) {
            this.btnDetailModel = new ToolBarBtnFeatureModel();
            this.btnDetailModel.setReference(BTN_DETAIL_REFERENCE);
            this.btnDetailModel.setFeatureId(FDETAIL_ENTRY_CODE);
            this.btnDetailModel.setText(I18nClientManager.translate(ProductionMo.T29536, false));
            this.btnDetailModel.setIconURL("/images/vif/vif57/production/mo/detail.png");

        }
        return this.btnDetailModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void manageBtnShowAllModelText() {
        FBoningOutputSBean sBean = (FBoningOutputSBean) getBrowserController().getInitialSelection();
        if (!sBean.getAll()) {
            getBtnShowAllModel().setText(I18nClientManager.translate(ProductionMo.T29545, false));
            getBtnShowAllModel().setIconURL("/images/vif/vif57/production/mo/showfinished.png");
        } else {
            getBtnShowAllModel().setText(I18nClientManager.translate(ProductionMo.T29525, false));
            getBtnShowAllModel().setIconURL("/images/vif/vif57/production/mo/hidefinished.png");
        }
    }
}

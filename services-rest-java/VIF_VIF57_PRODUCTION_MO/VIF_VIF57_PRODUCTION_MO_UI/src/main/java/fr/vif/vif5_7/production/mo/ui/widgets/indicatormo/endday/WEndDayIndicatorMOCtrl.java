/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WEndDayIndicatorMOCtrl.java,v $
 * Created on 15 avr. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday;


import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.DateBoundBean;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.selection.PermanentSelectionValues;
import fr.vif.jtech.ui.dialogs.DialogModel;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.util.DialogHelper;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.widget.WidgetActionEvent;
import fr.vif.jtech.ui.widget.WidgetController;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.location.business.beans.features.farea.FAreaBBean;
import fr.vif.vif5_7.gen.location.business.services.features.farea.FAreaCBS;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOTypeEnum;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.InvoicedArticleBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedOnLineControlBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedTrsLineBean;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedWorkingPeriodBean;
import fr.vif.vif5_7.production.mo.business.services.widget.fendday.FEndDayMOCBS;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.dialog.parameter.EndDayParameterDialogBean;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.dialog.parameter.EndDayParameterDialogCtrl;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.dialog.parameter.EndDayParameterDialogView;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.swing.WEndDayIndicatorMOView;


/**
 * Main controller of the end day indicator widget.
 *
 * @author ac
 */
public class WEndDayIndicatorMOCtrl extends WidgetController implements IEndDayEjbCallback {

    /** LOGGER. */
    private static final Logger        LOGGER              = Logger.getLogger(WEndDayIndicatorMOCtrl.class);

    /** Selection bean. */
    private EndDayMOSBean              selection           = null;

    /** Area CBS. */
    private FAreaCBS                   areaCBS             = null;

    /** EndDay CBS. */
    private FEndDayMOCBS               endDayMOCBS         = null;

    /** Executor service. */
    private ExecutorService            es;

    private boolean                    initializationOver  = false;

    private WEndDayMemorizationManager memorizationManager = null;

    /** Default Ctor. */
    public WEndDayIndicatorMOCtrl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("static-access")
    @Override
    public void actionPerformed(final WidgetActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);

        if (event.getEventId() != null && event.getEventId().equals(getWidgetView().EVENT_CHANGE_PARAMETER)) {

            deleteSelectionByIds(getIdCtx(), getIdCtx().getCompany(), getIdCtx().getEstablishment(),
                    getIdentification().getLogin(), String.valueOf(getWidgetView().getUniqueId()),
                    WEndDayMemorizationManager.ENDDAY_MEMO_TYPE);

            EndDayParameterDialogBean dialogBean = (EndDayParameterDialogBean) event.getEventParameter();

            getSelection().setIsTimeWindowContext(dialogBean.getIsTimeWindowContext());
            getSelection().setBeginDateChoice(dialogBean.getTwBeginDateChoice());
            getSelection().setEndDateChoice(dialogBean.getTwEndDateChoice());
            getSelection().setProductionDateChoice(dialogBean.getProductionDateChoice());

            if (!dialogBean.getIsTimeWindowContext()) {
                getSelection().setDateBoundBean(
                        new DateBoundBean(dialogBean.getProductionDate(), dialogBean.getProductionDate()));
            } else {
                DateBoundBean dateBoundBean = calculateDateRange(dialogBean.getTwBeginDate(),
                        dialogBean.getTwBeginTime(), dialogBean.getTwEndDate(), dialogBean.getTwEndTime());
                getSelection().setDateBoundBean(dateBoundBean);
            }

            getSelection().getSector().setCode(dialogBean.getArea().getCode());
            for (FAreaBBean areaBBean : ((WEndDayIndicatorMOModel) getModel()).getAreas()) {
                if (dialogBean.getArea().getCode().equals(areaBBean.getCode())) {
                    getSelection().getSector().setLabel(areaBBean.getLabel());
                }
            }

            if (!("".equals(getSelection().getSector().getCode()))
                    && !("".equals(getSelection().getSector().getLabel()))) {
                ((WEndDayIndicatorMOIView) getView()).addTextToTitle(I18nClientManager.translate(ProductionMo.T40696,
                        false)
                        + " "
                        + getSelection().getSector().getCode()
                        + " - "
                        + getSelection().getSector().getLabel());
            }

            // Sets the list of end day indicator to show
            if (dialogBean.getMaterial()) {
                getSelection().setShowMaterialIndicator(true);
            } else {
                getSelection().setShowMaterialIndicator(false);
            }
            if (dialogBean.getWorkingPeriod()) {
                getSelection().setShowWorkingPeriodIndicator(true);
            } else {
                getSelection().setShowWorkingPeriodIndicator(false);
            }
            if (dialogBean.getTrs()) {
                getSelection().setShowTrsIndicator(true);
            } else {
                getSelection().setShowTrsIndicator(false);
            }
            if (dialogBean.getOnLineControl()) {
                getSelection().setShowOnLineControlIndicator(true);
            } else {
                getSelection().setShowOnLineControlIndicator(false);
            }

            ((WEndDayIndicatorMOIView) getView()).setUniqueId(getSelection().hashCode());
            memorizationManager.saveMemorization(getWidgetView().getUniqueId(), getSelection());

            refreshWidget();
        }

        if (event.getEventId() != null && event.getEventId().equals(getWidgetView().ACTION_PARAMETER_FEATURE)) {
            displayParameterDialog(selection);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void deleteSelectionByIds(final IdContext idCtx, final String companyId, final String establishmentId,
            final String userId, final String selectionName, final String type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteSelectionByIds(idCtx=" + idCtx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", type=" + type
                    + ")");
        }

        if (es != null) {
            es.shutdownNow();
        }
        es = Executors.newFixedThreadPool(1);
        try {

            EndDayMOAsyncEjbCall worker = StandardControllerFactory.getInstance().getController(
                    EndDayMOAsyncEjbCall.class);
            worker.deletePermanentSelection(idCtx, companyId, establishmentId, userId, selectionName, type);
            es.submit(worker);

        } catch (UIException e) {
            getView().show(e);

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteSelectionByIds(idCtx=" + idCtx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", type=" + type
                    + ")");
        }
    }

    /**
     * Gets the areaCBS.
     *
     * @return the areaCBS.
     */
    public FAreaCBS getAreaCBS() {
        return areaCBS;
    }

    /**
     * Gets the endDayMOCBS.
     *
     * @return the endDayMOCBS.
     */
    public FEndDayMOCBS getEndDayMOCBS() {
        return endDayMOCBS;
    }

    /**
     * 
     * Get the end day MO data.
     * 
     * @param ctx context.
     * @param sel the selection bean.
     */
    public void getEndDayMOData(final IdContext ctx, final EndDayMOSBean sel) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getEndDayMOData(ctx=" + ctx + ", selection=" + sel + ")");
        }

        try {
            List<? extends EndDayMOBean> invoicedArticleList = null;
            List<? extends EndDayMOBean> unclosedWorkingPeriodList = null;
            List<? extends EndDayMOBean> unclosedTrsList = null;
            List<? extends EndDayMOBean> unclosedOnLineControlList = null;

            if (sel != null) {
                if (sel.getShowMaterialIndicator()) {

                    List<? extends EndDayMOBean> list = endDayMOCBS.getInvoicedArticles(ctx, sel);

                    if (list != null) {
                        invoicedArticleList = list;
                    } else {
                        invoicedArticleList = new ArrayList<InvoicedArticleBean>();
                    }

                }
                if (sel.getShowWorkingPeriodIndicator()) {

                    List<? extends EndDayMOBean> list = endDayMOCBS.getUnclosedWorkingPeriods(ctx, sel);

                    if (list != null) {
                        unclosedWorkingPeriodList = list;
                    } else {
                        unclosedWorkingPeriodList = new ArrayList<UnclosedWorkingPeriodBean>();
                    }
                }

                if (sel.getShowTrsIndicator()) {

                    List<? extends EndDayMOBean> list = endDayMOCBS.getUnclosedTrsLines(ctx, sel);

                    if (list != null) {
                        unclosedTrsList = list;
                    } else {
                        unclosedTrsList = new ArrayList<UnclosedTrsLineBean>();
                    }

                }
                if (sel.getShowOnLineControlIndicator()) {

                    List<? extends EndDayMOBean> list = endDayMOCBS.getUnclosedOnLineControls(ctx, sel);

                    if (list != null) {
                        unclosedOnLineControlList = list;
                    } else {
                        unclosedOnLineControlList = new ArrayList<UnclosedOnLineControlBean>();
                    }

                }

                if (invoicedArticleList != null) {
                    refreshView(invoicedArticleList, EndDayMOTypeEnum.MATERIAL);
                }
                if (unclosedWorkingPeriodList != null) {
                    refreshView(unclosedWorkingPeriodList, EndDayMOTypeEnum.WORKING_PERIOD);
                }
                if (unclosedTrsList != null) {
                    refreshView(unclosedTrsList, EndDayMOTypeEnum.TRS);
                }
                if (unclosedOnLineControlList != null) {
                    refreshView(unclosedOnLineControlList, EndDayMOTypeEnum.ON_LINE_CONTROL);
                }

            }
        } catch (BusinessException e) {
            if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                LOGGER.error("P - Error occured while retrieving tasks list  :" + e);
            }
            show(new UIException(e.getMainTrad()));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getEndDayMOData(ctx=" + ctx + ", selection=" + sel + ")");
        }
    }

    /**
     * Gets the selection.
     *
     * @return the selection.
     */
    public EndDayMOSBean getSelection() {
        return selection;
    }

    /**
     * 
     * Gets selection by id.
     * 
     * @param idCtx context
     * @param companyId company
     * @param establishmentId establishment
     * @param userId user
     * @param selectionName selection name
     * @param type type
     * @return the selection content
     */
    public Map<String, PermanentSelectionValues> getSelectionByIds(final IdContext idCtx, final String companyId,
            final String establishmentId, final String userId, final String selectionName, final String type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getSelectionByIds(idCtx=" + idCtx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", type=" + type
                    + ")");
        }

        Map<String, PermanentSelectionValues> sel = null;
        if (es != null) {
            es.shutdownNow();
        }
        es = Executors.newFixedThreadPool(1);
        try {

            EndDayMOAsyncEjbCall worker = StandardControllerFactory.getInstance().getController(
                    EndDayMOAsyncEjbCall.class);
            sel = worker.getSelectionByIds(idCtx, companyId, establishmentId, userId, selectionName, type);
            es.submit(worker);

        } catch (UIException e) {
            getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getSelectionByIds(idCtx=" + idCtx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", type=" + type
                    + ")=" + sel);
        }
        return sel;
    }

    /**
     * Retrieve the current widget view.
     * 
     * @return WEndDayIndicatorMOIView compatible instance
     */
    public WEndDayIndicatorMOIView getWidgetView() {
        return (WEndDayIndicatorMOIView) getView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        if (!initializationOver) {

            super.initialize();

            initializationOver = true;

            // To retrieve the parameters linked to the widget's uniqueID
            memorizationManager = new WEndDayMemorizationManager(this, null);
            Map<String, PermanentSelectionValues> map;
            map = getSelectionByIds(getIdCtx(), getIdCtx().getCompany(), getIdCtx().getEstablishment(),
                    getIdentification().getLogin(), String.valueOf(getWidgetView().getUniqueId()),
                    WEndDayMemorizationManager.ENDDAY_MEMO_TYPE);
            memorizationManager.setMap(map);

            selection = new EndDayMOSBean();
            if (!map.isEmpty()) {
                selection = memorizationManager.convertMemoToSelection(String.valueOf(getWidgetView().getUniqueId()));
                selection.setCompanyId(getIdCtx().getCompany());
                selection.setEstablishmentId(getIdCtx().getEstablishment());

                if (!selection.isTimeWindowContext()) {
                    switch (selection.getProductionDateChoice()) {
                        case EndDayParameterDialogBean.TODAY:
                            selection.getDateBoundBean().setMinBound(
                                    getRelativeDate(EndDayParameterDialogBean.TODAY, selection.getDateBoundBean()
                                            .getMinBound()));
                            break;
                        case EndDayParameterDialogBean.YESTERDAY:
                            selection.getDateBoundBean().setMinBound(
                                    getRelativeDate(EndDayParameterDialogBean.YESTERDAY, selection.getDateBoundBean()
                                            .getMinBound()));
                            break;
                        case EndDayParameterDialogBean.PERSONALIZED:
                            selection.getDateBoundBean().setMinBound(
                                    getRelativeDate(EndDayParameterDialogBean.PERSONALIZED, selection
                                            .getDateBoundBean().getMinBound()));
                            break;

                        default:
                            break;
                    }
                } else {
                    switch (selection.getBeginDateChoice()) {
                        case EndDayParameterDialogBean.TODAY:

                            selection.getDateBoundBean().setMinBound(
                                    getRelativeDate(EndDayParameterDialogBean.TODAY, selection.getDateBoundBean()
                                            .getMinBound()));
                            break;
                        case EndDayParameterDialogBean.YESTERDAY:
                            selection.getDateBoundBean().setMinBound(
                                    getRelativeDate(EndDayParameterDialogBean.YESTERDAY, selection.getDateBoundBean()
                                            .getMinBound()));
                            break;
                        case EndDayParameterDialogBean.PERSONALIZED:
                            selection.getDateBoundBean().setMinBound(
                                    getRelativeDate(EndDayParameterDialogBean.PERSONALIZED, selection
                                            .getDateBoundBean().getMinBound()));
                            break;
                        default:
                            break;
                    }

                    switch (selection.getEndDateChoice()) {
                        case EndDayParameterDialogBean.TODAY:
                            selection.getDateBoundBean().setMaxBound(
                                    getRelativeDate(EndDayParameterDialogBean.TODAY, selection.getDateBoundBean()
                                            .getMaxBound()));
                            break;
                        case EndDayParameterDialogBean.YESTERDAY:
                            selection.getDateBoundBean().setMaxBound(
                                    getRelativeDate(EndDayParameterDialogBean.YESTERDAY, selection.getDateBoundBean()
                                            .getMaxBound()));
                            break;
                        case EndDayParameterDialogBean.PERSONALIZED:
                            selection.getDateBoundBean().setMaxBound(
                                    getRelativeDate(EndDayParameterDialogBean.PERSONALIZED, selection
                                            .getDateBoundBean().getMaxBound()));

                        default:
                            break;
                    }
                }

            } else {
                selection.setCompanyId(getIdCtx().getCompany());
                selection.setEstablishmentId(getIdCtx().getEstablishment());
                selection.setDateBoundBean(new DateBoundBean(new Date(), new Date()));
                selection.setSector(new FAreaBBean(" ", " "));
                selection.setIsTimeWindowContext(false);
                selection.setShowMaterialIndicator(true);
                selection.setShowWorkingPeriodIndicator(true);
                selection.setShowTrsIndicator(true);
                selection.setShowOnLineControlIndicator(true);
                selection.setEndDateChoice(EndDayParameterDialogBean.TODAY);
                selection.setBeginDateChoice(EndDayParameterDialogBean.TODAY);
                selection.setProductionDateChoice(EndDayParameterDialogBean.TODAY);

            }

            ((WEndDayIndicatorMOIView) getView()).setUniqueId(getSelection().hashCode());

            memorizationManager.saveMemorization(getWidgetView().getUniqueId(), selection);

            Collection<EndDayMOTypeEnum> listSentToView = new ArrayList<EndDayMOTypeEnum>();
            if (selection.getShowMaterialIndicator()) {
                listSentToView.add(EndDayMOTypeEnum.MATERIAL);
            }
            if (selection.getShowWorkingPeriodIndicator()) {
                listSentToView.add(EndDayMOTypeEnum.WORKING_PERIOD);
            }
            if (selection.getShowTrsIndicator()) {
                listSentToView.add(EndDayMOTypeEnum.TRS);
            }
            if (selection.getShowOnLineControlIndicator()) {
                listSentToView.add(EndDayMOTypeEnum.ON_LINE_CONTROL);
            }

            try {
                List<FAreaBBean> list = areaCBS.queryElements(getIdCtx(), null, 0, 0);
                this.refreshAreas(list);
                getEndDayMOData(getIdCtx(), selection);
                ((WEndDayIndicatorMOIView) getView()).setIndicatorsMenu();
                ((WEndDayIndicatorMOIView) getView()).fillIndicatorsListToDisplay(listSentToView);
                for (FAreaBBean areaBBean : ((WEndDayIndicatorMOModel) getModel()).getAreas()) {
                    if (areaBBean.getCode().equals(selection.getSector().getCode())) {
                        getSelection().getSector().setLabel(areaBBean.getLabel());
                    }
                }

                if (!(" ".equals(selection.getSector().getCode())) && !(" ".equals(selection.getSector().getLabel()))) {
                    getWidgetView().addTextToTitle(
                            I18nClientManager.translate(ProductionMo.T40696, false) + " "
                                    + selection.getSector().getCode() + " - " + selection.getSector().getLabel());
                }
            } catch (BusinessException e1) {
                LOGGER.error("", e1);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refreshAreas(final List<FAreaBBean> returnValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refreshAreas(returnValue=" + returnValue + ")");
        }

        ((WEndDayIndicatorMOModel) getModel()).setAreas(returnValue);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - refreshAreas(returnValue=" + returnValue + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void refreshView(final List<? extends EndDayMOBean> returnValue, final EndDayMOTypeEnum indicatorType) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refreshView(returnValue=" + returnValue + ", indicatorType=" + indicatorType + ")");
        }

        if (EndDayMOTypeEnum.MATERIAL.equals(indicatorType)) {
            ((WEndDayIndicatorMOModel) getModel()).setInvoicedArticleBeans((List<InvoicedArticleBean>) returnValue);
            getWidgetView().updateInvoicedArticleList();
        }
        if (EndDayMOTypeEnum.WORKING_PERIOD.equals(indicatorType)) {
            ((WEndDayIndicatorMOModel) getModel())
            .setUnclosedWorkingPeriodBeans((List<UnclosedWorkingPeriodBean>) returnValue);
            getWidgetView().updateUnclosedWorkingPeriodList();
        }
        if (EndDayMOTypeEnum.TRS.equals(indicatorType)) {
            ((WEndDayIndicatorMOModel) getModel()).setUnclosedTrsLineBeans((List<UnclosedTrsLineBean>) returnValue);
            getWidgetView().updateUnclosedTrsList();
        }
        if (EndDayMOTypeEnum.ON_LINE_CONTROL.equals(indicatorType)) {
            ((WEndDayIndicatorMOModel) getModel())
            .setUnclosedOnLineControlBeans((List<UnclosedOnLineControlBean>) returnValue);
            getWidgetView().updateUnclosedOnLineControlList();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - refreshView(returnValue=" + returnValue + ", indicatorType=" + indicatorType + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void saveSelectionByIds(final IdContext idCtx, final String companyId, final String establishmentId,
            final String userId, final String selectionName, final String labelName, final String type,
            final Map<String, PermanentSelectionValues> values) throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveSelectionByIds(idCtx=" + idCtx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", labelName="
                    + labelName + ", type=" + type + ", values=" + values + ")");
        }

        if (es != null) {
            es.shutdownNow();
        }
        es = Executors.newFixedThreadPool(1);
        try {

            EndDayMOAsyncEjbCall worker = StandardControllerFactory.getInstance().getController(
                    EndDayMOAsyncEjbCall.class);
            worker.saveSelectionByIds(getIdCtx(), companyId, establishmentId, userId, selectionName, labelName, type,
                    values);
            es.submit(worker);

        } catch (UIException e) {
            getView().show(e);

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveSelectionByIds(idCtx=" + idCtx + ", companyId=" + companyId + ", establishmentId="
                    + establishmentId + ", userId=" + userId + ", selectionName=" + selectionName + ", labelName="
                    + labelName + ", type=" + type + ", values=" + values + ")");
        }
    }

    /**
     * Sets the areaCBS.
     *
     * @param areaCBS areaCBS.
     */
    public void setAreaCBS(final FAreaCBS areaCBS) {
        this.areaCBS = areaCBS;
    }

    /**
     * Sets the endDayMOCBS.
     *
     * @param endDayMOCBS endDayMOCBS.
     */
    public void setEndDayMOCBS(final FEndDayMOCBS endDayMOCBS) {
        this.endDayMOCBS = endDayMOCBS;
    }

    /**
     * Sets the selection.
     *
     * @param selection selection.
     */
    public void setSelection(final EndDayMOSBean selection) {
        this.selection = selection;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void show(final UIException exception) {
        getView().show(exception);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void refreshWidget() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refreshWidget()");
        }

        super.refreshWidget();

        if (initializationOver) {

            Collection<EndDayMOTypeEnum> listSentToView = new ArrayList<EndDayMOTypeEnum>();
            if (selection != null) {
                if (selection.getShowMaterialIndicator()) {
                    listSentToView.add(EndDayMOTypeEnum.MATERIAL);
                }
                if (selection.getShowWorkingPeriodIndicator()) {
                    listSentToView.add(EndDayMOTypeEnum.WORKING_PERIOD);
                }
                if (selection.getShowTrsIndicator()) {
                    listSentToView.add(EndDayMOTypeEnum.TRS);
                }
                if (selection.getShowOnLineControlIndicator()) {
                    listSentToView.add(EndDayMOTypeEnum.ON_LINE_CONTROL);
                }
            }

            this.getEndDayMOData(getIdCtx(), selection);
            ((WEndDayIndicatorMOIView) getView()).fillIndicatorsListToDisplay(listSentToView);
        } else {
            this.initialize();
            initializationOver = true;
        }

    }

    /**
     * Calculates the min and max date.
     * 
     * @param twBeginDate the begin date
     * @param twBeginTime the begin time
     * @param twEndDate the end date
     * @param twEndTime the end time
     * @return a dateBoundBean
     */
    private DateBoundBean calculateDateRange(final Date twBeginDate, final Date twBeginTime, final Date twEndDate,
            final Date twEndTime) {
        DateBoundBean dateBoundBean = null;

        // Min date bound.

        Calendar cBeginDate = Calendar.getInstance();
        Calendar cBeginTime = Calendar.getInstance();
        cBeginDate.setTime(twBeginDate);
        cBeginTime.setTime(twBeginTime);
        cBeginDate.add(Calendar.HOUR_OF_DAY, cBeginTime.get(Calendar.HOUR_OF_DAY));
        cBeginDate.add(Calendar.MINUTE, cBeginTime.get(Calendar.MINUTE));

        // Max date bound.

        Calendar cEndDate = Calendar.getInstance();
        Calendar cEndTime = Calendar.getInstance();
        cEndDate.setTime(twEndDate);
        cEndTime.setTime(twEndTime);
        cEndDate.add(Calendar.HOUR_OF_DAY, cEndTime.get(Calendar.HOUR_OF_DAY));
        cEndDate.add(Calendar.MINUTE, cEndTime.get(Calendar.MINUTE));

        dateBoundBean = new DateBoundBean(cBeginDate.getTime(), cEndDate.getTime());

        return dateBoundBean;

    }

    /**
     * Displays parameter's dialog.
     * 
     * @param sel selection
     */
    private void displayParameterDialog(final EndDayMOSBean sel) {
        EndDayParameterDialogBean parameterBean = new EndDayParameterDialogBean();

        parameterBean.setEstablishmentKey(new EstablishmentKey(sel.getCompanyId(), sel.getEstablishmentId()));
        parameterBean.setArea(new CodeLabel(sel.getSector().getCode(), sel.getSector().getLabel()));
        parameterBean.setMaterial(sel.getShowMaterialIndicator());
        parameterBean.setOnLineControl(sel.getShowOnLineControlIndicator());
        parameterBean.setTrs(sel.getShowTrsIndicator());
        parameterBean.setWorkingPeriod(sel.getShowWorkingPeriodIndicator());

        if (sel.isTimeWindowContext()) {
            parameterBean.setTwBeginDate(DateHelper.atMidnight(sel.getDateBoundBean().getMinBound()));
            parameterBean.setTwBeginTime(sel.getDateBoundBean().getMinBound());
            parameterBean.setTwBeginDateChoice(sel.getBeginDateChoice());

            parameterBean.setTwEndDate(DateHelper.atMidnight(sel.getDateBoundBean().getMaxBound()));
            parameterBean.setTwEndTime(sel.getDateBoundBean().getMaxBound());
            parameterBean.setTwEndDateChoice(sel.getEndDateChoice());
            parameterBean.setIsTimeWindowContext(sel.isTimeWindowContext());
        } else {
            parameterBean.setProductionDate(sel.getDateBoundBean().getMinBound());
            parameterBean.setProductionDateChoice(sel.getProductionDateChoice());
            parameterBean.setIsTimeWindowContext(sel.isTimeWindowContext());
        }

        DialogHelper.createDialog((WEndDayIndicatorMOView) getWidgetView(), EndDayParameterDialogCtrl.class,
                DialogModel.class, EndDayParameterDialogView.class, parameterBean,
                (WEndDayIndicatorMOView) getWidgetView(), (WEndDayIndicatorMOView) getWidgetView(), getModel()
                        .getIdentification(), I18nClientManager.translate(ProductionMo.T40705, false), false);
    }

    /**
     * Get the date relative to another date.
     * 
     * @param choice PERSONNALIZED, TODAY or YESTERDAY
     * @param date date to convert
     * @return a Date
     */
    private Date getRelativeDate(final String choice, final Date date) {

        Date dateReturned = date;

        Calendar oldDate = Calendar.getInstance();
        oldDate.setTime(date);
        int hours = oldDate.get(Calendar.HOUR_OF_DAY);
        int minutes = oldDate.get(Calendar.MINUTE);
        Calendar newDate = Calendar.getInstance();
        newDate.set(Calendar.HOUR_OF_DAY, hours);
        newDate.set(Calendar.MINUTE, minutes);

        switch (choice) {
            case EndDayParameterDialogBean.PERSONALIZED:
                dateReturned = date;
                break;
            case EndDayParameterDialogBean.TODAY:
                dateReturned = newDate.getTime();
                break;
            case EndDayParameterDialogBean.YESTERDAY:
                dateReturned = DateHelper.addDays(newDate.getTime(), -1);
                break;
            default:
                break;
        }

        return dateReturned;
    }
}

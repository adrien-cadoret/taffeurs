/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_ADDON_MONITORING_UI
 * File : $RCSfile: FTimeBreakDownTreeTableModel.java,v $
 * Created on 18 nov. 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown;


import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.treetable.InputTreeTableCellFormat;
import fr.vif.jtech.ui.treetable.InputTreeTableColumn;
import fr.vif.jtech.ui.treetable.InputTreeTableHierarchicalCellFormat;
import fr.vif.jtech.ui.treetable.InputTreeTableModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeElt;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeEltFields;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeTypeEnum;


/**
 * Tree table model for time breakdown.
 * 
 * @author cj
 */
public class FTimeBreakDownTreeTableModel extends InputTreeTableModel<TimeBreakDownHchyTreeNodeElt> {

    private static final String ICON_LINE_URL  = "/images/vif/vif57/production/mo/img16x16/resource.png";
    private static final String ICON_LABOR_URL = "/images/vif/vif57/production/mo/img16x16/labour.png";

    /**
     * Constructor.
     */
    public FTimeBreakDownTreeTableModel() {
        super();
        setTreeColumn(I18nClientManager.translate(ProductionMo.T36178, false), "label", "children", 100);
        setToolBarVisible(false);
        getUndoManager().setLimit(-1);
        setHorizontalScrollBarPolicy(InputTreeTableModel.HORIZONTAL_SCROLLBAR_AS_NEEDED);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputTreeTableCellFormat getCellFormat(final TimeBreakDownHchyTreeNodeElt node, final int modelIndexColumn) {
        InputTreeTableHierarchicalCellFormat treeTableHierarchicalCellFormat = new InputTreeTableHierarchicalCellFormat();

        if (Boolean.TRUE.equals(node.getHasAlreadyTime())) {
            treeTableHierarchicalCellFormat.setBasicFormat(new Format(
                    StandardColor.TOUCHSCREEN_BG_END_FUNCTIONALITIES_ENABLED, StandardColor.BLACK));
        }

        if (modelIndexColumn == 0) {
            if (TimeBreakDownHchyTreeNodeTypeEnum.LABOR.getValue().equals(node.getNodeType())) {
                treeTableHierarchicalCellFormat.setClosedIconResourceName(ICON_LABOR_URL);
                treeTableHierarchicalCellFormat.setIconResourceName(ICON_LABOR_URL);
            } else if (TimeBreakDownHchyTreeNodeTypeEnum.LIGNE.getValue().equals(node.getNodeType())) {
                treeTableHierarchicalCellFormat.setClosedIconResourceName(ICON_LINE_URL);
                treeTableHierarchicalCellFormat.setIconResourceName(ICON_LINE_URL);
            }
        }

        if (modelIndexColumn == 5) {
            Format format = treeTableHierarchicalCellFormat.getBasicFormat();
            format.setAlignment(Alignment.CENTER_ALIGN);
            treeTableHierarchicalCellFormat.setBasicFormat(format);
        }

        return treeTableHierarchicalCellFormat;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class getColumnClass(final int i) {
        Class classColumn;
        String columnName = getInputTreeTableColumn(i).getBeanAttribute();
        if (TimeBreakDownHchyTreeNodeEltFields.IS_SELECTED.getValue().equals(columnName)) {
            classColumn = Boolean.class;
        } else {
            classColumn = super.getColumnClass(i);
        }
        return classColumn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isCellEditable(final TimeBreakDownHchyTreeNodeElt node, final InputTreeTableColumn col) {

        boolean ret = false;
        // DP2192 - CHU - display check box only for PERIOD rows
        if (TimeBreakDownHchyTreeNodeEltFields.IS_SELECTED.getValue().equalsIgnoreCase(col.getBeanAttribute())
                && !TimeBreakDownHchyTreeNodeTypeEnum.PERIOD.getValue().equals(node.getNodeType())) {
            ret = true;
        }
        return ret;
    }

    // /**
    // * {@inheritDoc}
    // */
    // @Override
    // public void setAndNotifyValueAt(final Object value, final Object node, final int column, final boolean
    // isNotified) {
    // // TODO Auto-generated method stub
    // super.setAndNotifyValueAt(value, node, column, isNotified);
    //
    // ((TimeBreakDownHchyTreeNodeElt) node).;
    //
    // }
}

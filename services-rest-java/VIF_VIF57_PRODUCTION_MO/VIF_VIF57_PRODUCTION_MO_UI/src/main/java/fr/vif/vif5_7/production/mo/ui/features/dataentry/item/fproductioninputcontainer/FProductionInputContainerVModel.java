/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputContainerVModel.java,v $
 * Created on 17 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;


/**
 * InputOperationItem : Viewer Model.
 * 
 * @author vr
 */
public class FProductionInputContainerVModel extends ViewerModel<FProductionInputContainerVBean> {

    /**
     * Default constructor.
     * 
     */
    public FProductionInputContainerVModel() {
        super();
        setBeanClass(FProductionInputContainerVBean.class);
        setDeleteNeedsConfirmation(false);
        setEnterInCreationOnEmptyBrowser(false);
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOCreationFCtrl.java,v $
 * Created on 19 avr. 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.input.InputBarCodeActionEvent;
import fr.vif.jtech.ui.input.InputBarCodeActionListener;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.toolbar.buttons.toolbarbutton.ToolBarButtonKey;
import fr.vif.jtech.ui.util.Key;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmocreation.FBoningMOCreationVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fboningmocreation.FBoningMOCreationCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation.touch.FBoningMOCreationVView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationFIView;


/**
 * boning MO creation feature controller.
 * 
 * @author xg
 */
public class FBoningMOCreationFCtrl extends StandardFeatureController implements DialogChangeListener,
        InputBarCodeActionListener {

    public static final String   EVENT_CREATESTARTEDMO = "createStartedMO";
    public static final String   EVENT_OPENCRITERIA    = "openCriteria";

    /** LOGGER. */
    private static final Logger  LOGGER                = Logger.getLogger(FBoningMOCreationFCtrl.class);

    private ToolBarBtnStdModel   btnCreateStartedMO    = null;
    private ToolBarBtnStdModel   btnCriteria           = null;
    private FBoningMOCreationCBS boningMOCreationCBS   = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void barcodeReceived(final InputBarCodeActionEvent event) {
        interpretBarcode(event.getBarcode());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        String reference = event.getModel().getReference();
        if (reference == EVENT_CREATESTARTEDMO) {
            if (((FBoningMOCreationVCtrl) getViewerController()).firePerformSave()) {
                fireSelfFeatureClosingRequired();
            }
        } else if (reference == EVENT_OPENCRITERIA) {
            CriteriaDBean criteriaDBean = new CriteriaDBean(
                    ((FBoningMOCreationVBean) getViewerController().getBean()).getReturnInitBean(),
                    ((FBoningMOCreationVBean) getViewerController().getBean()).getItem());
            WorkShopCriteriaTools.showCriteria(getView(), this, this, getModel().getIdentification(), criteriaDBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
    }

    /**
     * Gets the boningMOCreationCBS.
     * 
     * @return the boningMOCreationCBS.
     */
    public FBoningMOCreationCBS getBoningMOCreationCBS() {
        return boningMOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        ((FBoningMOCreationVCtrl) getViewerController()).setFeatureController(this);
        ((FBoningMOCreationVView) getViewerController().getView()).getInputBarCode().addBarCodeActionListener(this);
        super.initialize();
        setTitle(getModel().getTitle());
        String wsId = (String) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.LOGICAL_WORKSTATION);
        FMOCreationFIView creationFIView = (FMOCreationFIView) getView();

        creationFIView.setSubTitle(I18nClientManager.translate(ProductionMo.T29585, false) + wsId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Sets the boningMOCreationCBS.
     * 
     * @param boningMOCreationCBS boningMOCreationCBS.
     */
    public void setBoningMOCreationCBS(final FBoningMOCreationCBS boningMOCreationCBS) {
        this.boningMOCreationCBS = boningMOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> list = new ArrayList<ToolBarBtnStdBaseModel>();
        list.add(getBtnCreateStartedMO());
        list.add(getBtnCriteria());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return list;
    }

    /**
     * Gets the btnCreateStartedMO.
     * 
     * @category getter
     * @return the btnCreateStartedMO.
     */
    private ToolBarBtnStdModel getBtnCreateStartedMO() {
        if (btnCreateStartedMO == null) {
            btnCreateStartedMO = new ToolBarBtnStdModel();
            btnCreateStartedMO.setIconURL("/images/vif/vif57/production/mo/ok.png");
            btnCreateStartedMO.setReference(EVENT_CREATESTARTEDMO);
            btnCreateStartedMO.setShortcut(Key.K_F2);
            btnCreateStartedMO.setKey(new ToolBarButtonKey(1));
            btnCreateStartedMO.setText(I18nClientManager.translate(Jtech.T12758, false));

        }
        return btnCreateStartedMO;
    }

    /**
     * Gets the btnOpenCriteria.
     * 
     * @category getter
     * @return the btnOpenCriteria.
     */
    private ToolBarBtnStdModel getBtnCriteria() {
        if (btnCriteria == null) {
            btnCriteria = new ToolBarBtnStdModel();
            btnCriteria.setIconURL("/images/vif/vif57/production/mo/criteria.png");
            btnCriteria.setReference(EVENT_OPENCRITERIA);
            btnCriteria.setShortcut(Key.K_F4);
            btnCriteria.setKey(new ToolBarButtonKey(2));
            btnCriteria.setText(I18nClientManager.translate(GenKernel.T29329, false));
        }
        return btnCriteria;
    }

    /**
     * Interprets a barcode.
     * 
     * @param barcode bar code
     */
    @SuppressWarnings("unchecked")
    private void interpretBarcode(final String barcode) {
        try {
            String workstationId = (String) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.LOGICAL_WORKSTATION);

            FBoningMOCreationVBean vbean = getBoningMOCreationCBS().analyseBarcode(
                    getIdCtx(),
                    new EstablishmentKey(getModel().getIdentification().getCompany(), getModel().getIdentification()
                            .getEstablishment()), barcode, workstationId);
            getViewerController().getModel().setBean(vbean);
            if (vbean.getFabrication() != null && vbean.getFabrication().getFabricationCLs() != null) {
                ((FBoningMOCreationVCtrl) getViewerController()).createFromBarCode();
            } else {
                ((FBoningMOCreationVCtrl) getViewerController()).searchFabricationFromCriteria(vbean
                        .getReturnInitBean());
            }
        } catch (BusinessException e) {
            LOGGER.error("", e);
            getView().show(new UIException(e));
        } catch (BusinessErrorsException e) {
            LOGGER.error("", e);
            getView().show(new UIException(new UIErrorsException(e)));
        }
    }
}

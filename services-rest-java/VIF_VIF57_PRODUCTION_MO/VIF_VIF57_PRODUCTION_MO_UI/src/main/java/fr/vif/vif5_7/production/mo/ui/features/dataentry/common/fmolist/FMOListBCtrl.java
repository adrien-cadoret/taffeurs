/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOListBCtrl.java,v $
 * Created on 07 nov. 2008 by ALB
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.events.dialogs.selection.SelectionChangeEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.common.fmolist.FMOListCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.ProductionMoTools;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * MOList : Browser Controller.
 * 
 * @author alb
 */
public class FMOListBCtrl extends AbstractBrowserController<FMOListBBean> {

    public static final String  LOGICAL_WORKSTATION = "logicalWorkstation";

    private static final Logger LOGGER              = Logger.getLogger(FMOListBCtrl.class);

    private FMOListCBS          fmoListCBS;

    /**
     * Default constructor.
     * 
     */
    public FMOListBCtrl() {
        super();
    }

    /**
     * Change the current columns.
     * 
     * @param touchModelFunction model define for the screen
     */
    public void changeColumns(final TouchModelFunction touchModelFunction) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - changeColumns(touchModelFunction=" + touchModelFunction + ")");
        }

        List<BrowserColumn> cols = FMOListColumnsFactory.getColumns(touchModelFunction);
        getModel().removeAllColumns();
        for (BrowserColumn c : cols) {
            getModel().addColumn(c);
        }
        getView().initializeView(this);
        if (getModel().getRowCount() != 0) {
            getModel().setCurrentRowNumber(0);
        }

        ProductionMoTools.resizeColumnMoListFeature(touchModelFunction,
                ((FMOListBIView) getView()).getJTableForResize());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - changeColumns(touchModelFunction=" + touchModelFunction + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {

        Object o = getSharedContext().get(Domain.DOMAIN_THIS, FProductionConstant.MO_INITIAL_SELECTON);
        return (FMOListSBean) o;
    }

    /**
     * Gets the fmoListCBS.
     * 
     * @category getter
     * @return the fmoListCBS.
     */
    public final FMOListCBS getFmoListCBS() {
        return fmoListCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();
        ((FMOListBIView) getView()).initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectionChanged(final SelectionChangeEvent event) {
        FMOListBBean bean = getModel().getCurrentBean();

        super.selectionChanged(event);

        if (bean != null) {
            setSelectedBean(bean);
        }
    }

    /**
     * Sets the fmoListCBS.
     * 
     * @category setter
     * @param fmoListCBS fmoListCBS.
     */
    public final void setFmoListCBS(final FMOListCBS fmoListCBS) {
        this.fmoListCBS = fmoListCBS;
    }

    /**
     * select the row of the bean in the browser.
     * 
     * @param bean the bean
     */
    public void setSelectedBean(final FMOListBBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setSelectedBean(bean=" + bean + ")");
        }

        try {
            if (bean != null && bean.getMobean() != null) {
                for (int i = 0; i < getModel().getRowCount(); i++) {

                    if (bean.getMobean().getMoKey().equals(getModel().getBeanAt(i).getMobean().getMoKey())) {
                        changeCurrentRow(i);
                        break;
                    }
                }

            }
        } catch (UIVetoException e) {
            getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setSelectedBean(bean=" + bean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FMOListBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FMOListBBean> lst = null;
        try {
            FMOListSBean bean = (FMOListSBean) selection;

            TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL);
            if (touchModel != null) {
                bean.setTouchModel(touchModel);
            }
            lst = getFmoListCBS().queryElements(getIdCtx(), bean, startIndex, rowNumber);

            FMOListBBean bBean = lst.get(0);
            if (bBean.getMobean() == null) {
                lst.clear();
            }
            if (touchModel == null) {
                getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL, bBean.getTouchModel());
                getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL, true);
                fireSharedContextSend(Domain.DOMAIN_THIS, getSharedContext());
            }

        } catch (BusinessException e) {
            LOGGER.error("erreur :" + e);
            LOGGER.error(selection);
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return lst;
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningInputFView.java,v $
 * Created on 23 avr. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.touch;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.workshop.device.ui.composites.devices.scale.touch.CPerpetualScaleWeightTouchView;


/**
 * Feature view for boning input.
 * 
 * @author cj
 */
public class FBoningInputFView extends StandardTouchFeature {
    private FBoningInputBView              bView;
    private CPerpetualScaleWeightTouchView scaleView;
    private TitlePanel                     subTitlePanel;
    private FBoningInputVView              vView;

    /**
     * Default contructor.
     */
    public FBoningInputFView() {
        super();
        initialize();
    }

    /**
     * Gets the bView.
     * 
     * @category getter
     * @return the bView.
     */
    public FBoningInputBView getBView() {
        if (bView == null) {
            bView = new FBoningInputBView();
            bView.setBounds(0, 180, 410, 132);
        }
        return bView;
    }

    /**
     * Get the weight composite.
     * 
     * @return the weight composite
     */
    public CPerpetualScaleWeightTouchView getScaleView() {
        if (scaleView == null) {
            scaleView = new CPerpetualScaleWeightTouchView();
            scaleView.setBounds(0, 0, 867, 100);
            scaleView.setOpaque(false);
        }
        return scaleView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView getViewerView() {
        return getVView();
    }

    /**
     * Gets the vView.
     * 
     * @category getter
     * @return the vView.
     */
    public FBoningInputVView getVView() {
        if (vView == null) {
            vView = new FBoningInputVView();
            vView.setBounds(0, 145, 870, 500);
        }
        return vView;
    }

    /**
     * Sets the bView.
     * 
     * @param bView bView.
     */
    public void setbView(final FBoningInputBView bView) {
        this.bView = bView;
    }

    /**
     * {@inheritDoc}
     */
    public void setSubTitle(final String subTitle) {
        subTitlePanel.setTitle(subTitle);

    }

    /**
     * Get the subtitle panel.
     * 
     * @return the subtitile panel
     */
    private JPanel getSubTitle() {
        if (subTitlePanel == null) {
            subTitlePanel = new TitlePanel();
            subTitlePanel.setBounds(0, 101, 870, 40);
            subTitlePanel.setBorder(new MatteBorder(1, 0, 0, 2, Color.white));
            subTitlePanel.setPreferredSize(new Dimension(800, 40));
            subTitlePanel.setMinimumSize(new Dimension(500, 40));
        }
        return subTitlePanel;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setPreferredSize(new Dimension(870, 768));
        setLayout(null);
        add(getSubTitle());
        add(getBView());
        add(getVView());
        add(getScaleView());
        // setErrorPanelAlwaysVisible(true);
    }
}

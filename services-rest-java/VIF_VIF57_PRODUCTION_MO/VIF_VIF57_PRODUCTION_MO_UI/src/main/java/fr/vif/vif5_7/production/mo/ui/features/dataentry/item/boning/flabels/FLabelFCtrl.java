/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLabelFCtrl.java,v $
 * Created on 17 Jun 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.flabels;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.browser.BrowserActionEvent;
import fr.vif.jtech.ui.browser.BrowserActionListener;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.flabels.FLabelBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.flabels.FLabelSBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.flabels.touch.FLabelBView;


/**
 * FLabel : Feature Controller.
 * 
 * @author xg
 */
public class FLabelFCtrl extends StandardFeatureController implements BrowserActionListener {
    /**
     * workstation id.
     */
    public static final String  WORKSTATION_ID = "workstationId";
    /**
     * workstation id.
     */
    public static final String  MO_KEY         = "moKey";
    public static final String  SELECTED_LABEL = "selectedLabel";

    /** LOGGER. */
    private static final Logger LOGGER         = Logger.getLogger(FLabelFCtrl.class);

    /**
     * Default constr.
     */
    public FLabelFCtrl() {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void advancedSearchRequired(final BrowserActionEvent event) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }
        super.buttonAction(event);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closed(final ClosingEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - closed(event=" + event + ")");
        }
        super.closed(event);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - closed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void goToEndRequired(final BrowserActionEvent event) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        // -------------------------------
        // Initialize browser selection
        // -------------------------------
        FLabelSBean sBean = new FLabelSBean();
        sBean.setWorkstationId((String) getSharedContext().get(Domain.DOMAIN_PARENT, WORKSTATION_ID));
        sBean.setAll(false);
        sBean.setMoKey((MOKey) getSharedContext().get(Domain.DOMAIN_PARENT, MO_KEY));

        getBrowserController().setCurrentSelection(sBean);
        getBrowserController().setInitialSelection(sBean);

        // --------------------------------------------------------
        // Register a listener on row changing of the browser :
        // --------------------------------------------------------
        ((FLabelBView) getBrowserController().getView()).registerActionListener(this);

        super.initialize();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void scrolledNearEnd(final BrowserActionEvent event) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final BrowserActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanged(event=" + event + ")");
        }

        FLabelBBean selectedLabelBBean = ((FLabelBCtrl) getBrowserController()).getModel().getBeanAt(
                event.getSelectedRowNum());

        getSharedContext().put(Domain.DOMAIN_THIS, SELECTED_LABEL, selectedLabelBBean);
        fireSharedContextSend(getSharedContext());

        fireSelfFeatureClosingRequired();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanged(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final BrowserActionEvent event) throws UIVetoException {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectionRequired(final BrowserActionEvent event) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void simpleSearchRequired(final BrowserActionEvent event) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void tableRowSelected(final BrowserActionEvent event) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void xlsExportRequired(final BrowserActionEvent event) {
        // nothing to do
    }
}

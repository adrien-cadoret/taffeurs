/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOImgToolTip.java,v $
 * Created on 3 oct. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing;


import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.net.URL;

import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JToolTip;

import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;


/**
 * Customized tooltip. Show FPiledUpLines browse in a tooltip.
 * 
 * @author jd
 */
public class FIndicatorMOImgToolTip extends JToolTip {

    private ImageIcon img;
    private String    imgPath = "/images/indicator/simple.png";

    /**
     * Constructor.
     */
    public FIndicatorMOImgToolTip() {
        super();
        init();
    }

    /**
     * Constructor.
     * 
     * @param imgPath Path of the image
     */
    public FIndicatorMOImgToolTip(final String imgPath) {
        super();
        this.imgPath = imgPath;
        init();
    }

    /**
     * init.
     */
    private void init() {
        if (EntityTools.isValid(imgPath)) {
            URL url = this.getClass().getResource(imgPath);
            if (url != null) {
                img = new ImageIcon(url);

                this.setPreferredSize(new Dimension(img.getIconWidth(), img.getIconHeight()));
            }
        }
        this.setLayout(new GridBagLayout());
        GridBagConstraints g = new GridBagConstraints();
        g.weightx = 1;
        g.weighty = 1;
        g.gridx = 0;
        g.gridy = 0;
        g.fill = GridBagConstraints.BOTH;

        JPanel jpanel = new JPanel() {
            /**
             * {@inheritDoc}
             */
            @Override
            public void paint(final Graphics g) {

                super.paint(g);

                if (img != null) {
                    g.drawImage(img.getImage(), 0, 0, this);
                }

            }
        };

        // this.setPreferredSize(new Dimension(300, 300));
        this.add(jpanel, g);

    }
}

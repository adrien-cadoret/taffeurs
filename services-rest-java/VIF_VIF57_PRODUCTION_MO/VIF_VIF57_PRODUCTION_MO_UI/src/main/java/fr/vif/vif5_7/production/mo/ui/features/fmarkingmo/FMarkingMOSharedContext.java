/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOSharedContext.java,v $
 * Created on 7 févr. 2014 by lm
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


/**
 * Singleton used to get the FMarkingMOFCtrl, in order to access to its SharedContext.
 * 
 * @author jd
 */
public final class FMarkingMOSharedContext {

    private static FMarkingMOSharedContext singleton = null;

    private FMarkingMOFCtrl                ctrl      = null;

    /**
     * Constructor.
     * 
     * @param ctrl FMarkingMOFCtrl
     */
    private FMarkingMOSharedContext(final FMarkingMOFCtrl ctrl) {
        this.ctrl = ctrl;
    }

    /**
     * Clear the singleton.
     */
    public static void clearInstance() {
        singleton = null;
    }

    /**
     * Create a new instance if not exists.
     * 
     * @param ctrl FMarkingMOFCtrl
     */
    public static void create(final FMarkingMOFCtrl ctrl) {
        if (singleton == null) {
            singleton = new FMarkingMOSharedContext(ctrl);
        }
    }

    /**
     * Get the instance.
     * 
     * @return the instance
     */
    public static FMarkingMOSharedContext getInstance() {
        return singleton;
    }

    /**
     * Gets the ctrl.
     * 
     * @return the ctrl.
     */
    public FMarkingMOFCtrl getCtrl() {
        return ctrl;
    }

}

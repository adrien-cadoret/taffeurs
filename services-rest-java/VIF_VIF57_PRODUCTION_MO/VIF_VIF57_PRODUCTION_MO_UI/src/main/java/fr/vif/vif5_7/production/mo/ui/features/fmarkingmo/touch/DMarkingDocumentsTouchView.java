/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DMarkingDocumentsTouchView.java,v $
 * Created on 18 févr. 2014 by lm
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch;


import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;

import javax.swing.JPanel;

import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.dialogs.touch.StandardTouchDialog;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.workshop.device.ui.composites.cdocument.touch.CDocumentTouchView;


/**
 * Marking documents' selection View.
 * 
 * @author lm
 */
public class DMarkingDocumentsTouchView extends StandardTouchDialog {

    private CDocumentTouchView cDocument1 = null;
    private CDocumentTouchView cDocument2 = null;
    private CDocumentTouchView cDocument3 = null;
    private CDocumentTouchView cDocument4 = null;
    private CDocumentTouchView cDocument5 = null;
    private CDocumentTouchView cDocument6 = null;
    private JPanel             jPanelMain = null;

    /**
     * Constructor.
     */
    public DMarkingDocumentsTouchView() {
        super();
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param view Dialog
     */
    public DMarkingDocumentsTouchView(final Dialog view) {
        super(view);
        initialize();
    }

    /**
     * Constructor.
     * 
     * @param view Frame
     */
    public DMarkingDocumentsTouchView(final Frame view) {
        super(view);
        initialize();
    }

    /**
     * Gets the cDocument1.
     * 
     * @return the cDocument1.
     */
    public CDocumentTouchView getcDocument1() {
        if (cDocument1 == null) {
            cDocument1 = new CDocumentTouchView("document1", I18nClientManager.translate(ProductionMo.T34872, false,
                    new VarParamTranslation(1)));
            cDocument1.setResettable(true);
        }
        return cDocument1;
    }

    /**
     * Gets the cDocument2.
     * 
     * @return the cDocument2.
     */
    public CDocumentTouchView getcDocument2() {
        if (cDocument2 == null) {
            cDocument2 = new CDocumentTouchView("document2", I18nClientManager.translate(ProductionMo.T34872, false,
                    new VarParamTranslation(2)));
            cDocument2.setResettable(true);
        }
        return cDocument2;
    }

    /**
     * Gets the cDocument3.
     * 
     * @return the cDocument3.
     */
    public CDocumentTouchView getcDocument3() {
        if (cDocument3 == null) {
            cDocument3 = new CDocumentTouchView("document3", I18nClientManager.translate(ProductionMo.T34872, false,
                    new VarParamTranslation(3)));
            cDocument3.setResettable(true);
        }
        return cDocument3;
    }

    /**
     * Gets the cDocument4.
     * 
     * @return the cDocument4.
     */
    public CDocumentTouchView getcDocument4() {
        if (cDocument4 == null) {
            cDocument4 = new CDocumentTouchView("document4", I18nClientManager.translate(ProductionMo.T34872, false,
                    new VarParamTranslation(4)));
            cDocument4.setResettable(true);
        }
        return cDocument4;
    }

    /**
     * Gets the cDocument5.
     * 
     * @return the cDocument5.
     */
    public CDocumentTouchView getcDocument5() {
        if (cDocument5 == null) {
            cDocument5 = new CDocumentTouchView("document5", I18nClientManager.translate(ProductionMo.T34872, false,
                    new VarParamTranslation(5)));
            cDocument5.setResettable(true);
        }
        return cDocument5;
    }

    /**
     * Gets the cDocument6.
     * 
     * @return the cDocument6.
     */
    public CDocumentTouchView getcDocument6() {
        if (cDocument6 == null) {
            cDocument6 = new CDocumentTouchView("document6", I18nClientManager.translate(ProductionMo.T34872, false,
                    new VarParamTranslation(6)));
            cDocument6.setResettable(true);
        }
        return cDocument6;
    }

    /**
     * Get the main JPanel.
     * 
     * @return jPanelMain
     */
    private JPanel getjPanelMain() {
        if (jPanelMain == null) {
            jPanelMain = new JPanel();
            jPanelMain.setLayout(new GridLayout(3, 2));
            jPanelMain.add(getcDocument1());
            jPanelMain.add(getcDocument2());
            jPanelMain.add(getcDocument3());
            jPanelMain.add(getcDocument4());
            jPanelMain.add(getcDocument5());
            jPanelMain.add(getcDocument6());
        }
        return jPanelMain;
    }

    /**
     * This method initializes this.
     * 
     */
    private void initialize() {
        this.setTitle(I18nClientManager.translate(ProductionMo.T34788, false));
        // this view have to be modal, else it can't open field help to pick document
        // this.setModal(false);
        this.setSize(new Dimension(622, 450));
        this.setMainPanel(getjPanelMain());

    }

}

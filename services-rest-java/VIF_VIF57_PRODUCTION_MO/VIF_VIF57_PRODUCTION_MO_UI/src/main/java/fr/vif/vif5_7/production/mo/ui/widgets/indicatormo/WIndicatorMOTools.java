/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WIndicatorMOTools.java,v $
 * Created on 03 avril 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.identification.Identification;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.FSupervisorMOFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOData;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMODataSet;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOManager;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOSaveFrame;


/**
 * Widget Indicator Tools.
 * 
 * @author ag
 */
public final class WIndicatorMOTools {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(WIndicatorMOTools.class);

    /**
     * Private Constructor.
     * 
     */
    private WIndicatorMOTools() {

    }

    /**
     * Get frame from referentiel by unique ID.
     * 
     * @param frames List<LocalFrame>
     * @param uniqueId String
     * @return LocalFrame LocalFrame
     */
    public static MOLocalFrame getFrame(final List<MOLocalFrame> frames, final String uniqueId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getFrame(frames=" + frames + ", uniqueId=" + uniqueId + ")");
        }

        MOLocalFrame result = null;

        for (MOLocalFrame frame : frames) {
            if (frame.getUniqueId().equals(uniqueId)) {
                result = frame;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getFrame(frames=" + frames + ", uniqueId=" + uniqueId + ")=" + result);
        }
        return result;
    }

    /**
     * Get frame from referentiel by unique ID.
     * 
     * @param frames List<LocalFrame>
     * @param widgetId String
     * @return List<CodeLabel> List<CodeLabel>
     */
    public static List<CodeLabel> getFrameByWidgetId(final List<MOLocalFrame> frames, final String widgetId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getFrameByWidgetId(frames=" + frames + ", widgetId=" + widgetId + ")");
        }

        List<CodeLabel> indicators = new ArrayList<CodeLabel>();
        for (MOLocalFrame frame : frames) {
            if (frame.getWidgetId().equals(widgetId)) {
                indicators.add(new CodeLabel(frame.getUniqueId(), frame.getFrameBean().getTitle()));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getFrameByWidgetId(frames=" + frames + ", widgetId=" + widgetId + ")=" + indicators);
        }
        return indicators;
    }

    /**
     * Get frames from xml referentiel.
     * 
     * @param identification Identification
     * @param sBean FSupervisorMOSBean
     * @return List<LocalFrame>
     */
    @SuppressWarnings("unchecked")
    public static List<MOLocalFrame> getFrames(final Identification identification, final FSupervisorMOSBean sBean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getFrames(identification=" + identification + ", sBean=" + sBean + ")");
        }

        List<MOSaveFrame> referentiel = (List<MOSaveFrame>) FSupervisorMOManager
                .importDefaultXML(FSupervisorMOManager.VIEW_REFERENTIEL);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getFrames(identification=" + identification + ", sBean=" + sBean + ")");
        }
        return FSupervisorMOManager.convertToLocalFrame(identification, sBean, referentiel, true);
    }

    /**
     * Load Local Frame.
     * 
     * @param context IdContext
     * @param key unique ID
     * @return LocalFrame
     */
    public static MOLocalFrame loadLocalFrame(final IdContext context, final String key) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - loadLocalFrame(context=" + context + ", key=" + key + ")");
        }

        FSupervisorMOFCtrl controler = new FSupervisorMOFCtrl();

        FSupervisorMOManager manager = new FSupervisorMOManager(controler);

        try {
            manager.loadXmlFrame(FSupervisorMOManager.VIEW_REFERENTIEL + "", MOSaveFrame.class);
        } catch (UIException e) {
            LOGGER.debug("E - loadLocalFrame(context=" + context + ", key=" + key + "),error=" + e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - loadLocalFrame(context=" + context + ", key=" + key + ")=" + null);
        }
        return null;
    }

    /**
     * 
     * Init dataset for example.
     * 
     * @return List<GraphMODataSet>
     */
    @SuppressWarnings("unchecked")
    private static List<GraphMODataSet> initDataSet() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initDataSet()");
        }

        List<GraphMODataSet> list = new ArrayList<GraphMODataSet>();

        GraphMODataSet set = new GraphMODataSet("Lancé");
        set.setGroup("G3");
        set.getGraphData().add(new GraphMOData("Escalope", 43.2));
        set.getGraphData().add(new GraphMOData("Poisson pané", 27.9));
        set.getGraphData().add(new GraphMOData("Biscuit", 79.5));
        list.add(set);

        set = new GraphMODataSet("En cours");
        set.setGroup("G4");
        set.getGraphData().add(new GraphMOData("Biscuit", 43.2));
        set.getGraphData().add(new GraphMOData("Poisson pané", 27.9));
        set.getGraphData().add(new GraphMOData("Carottes", 79.5));
        list.add(set);

        set = new GraphMODataSet("Fait");
        set.setGroup("G4");
        set.getGraphData().add(new GraphMOData("Biscuit", 3.2));
        set.getGraphData().add(new GraphMOData("Escalope", 7.9));
        set.getGraphData().add(new GraphMOData("Carottes", 9.5));
        list.add(set);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initDataSet()=" + list);
        }
        return list;

    }
}

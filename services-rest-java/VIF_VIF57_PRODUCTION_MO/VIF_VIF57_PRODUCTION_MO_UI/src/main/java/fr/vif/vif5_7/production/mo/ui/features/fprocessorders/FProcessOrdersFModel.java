/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersFModel.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;


/**
 * ProcessOrders : Feature Model.
 * 
 * @author jd
 */
public class FProcessOrdersFModel extends StandardFeatureModel {

    public static final String UPDATE_MO          = "updateMO";
    public static final String LIST               = "list";
    public static final String INANDOUT           = "inandout";
    public static final String FINISH             = "finish";
    public static final String UPDATE_DECLARATION = "updateDeclaration";
    public static final String VALIDATE           = "validate";

    /**
     * Default constructor.
     * 
     */
    public FProcessOrdersFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FProcessOrdersBModel.class, null, FProcessOrdersBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FProcessOrdersVModel.class, null, FProcessOrdersVCtrl.class));
    }

}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DPrintDocumentCtrl.java,v $
 * Created on 28 Jun 2012 by cj
 */
package fr.vif.vif5_7.production.mo.ui.dialog.printdocument;


import fr.vif.jtech.common.beans.KeyboardBean;
import fr.vif.jtech.ui.dialogs.DialogView;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.featurecontainer.FeatureContainerController;
import fr.vif.jtech.ui.input.kbdialog.KeyboardDialogModel;
import fr.vif.jtech.ui.input.kbdialog.NumKeyboardDialogController;
import fr.vif.jtech.ui.models.identification.Identification;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.dialog.printdocument.DPrintDocumentBean;
import fr.vif.vif5_7.production.mo.ui.dialog.printdocument.touch.DPrintDocumentTouchView;


/**
 * controller of the document creation Dialog box.
 * 
 * @author cj
 */
public class DPrintDocumentCtrl extends NumKeyboardDialogController implements DialogChangeListener {

    private DPrintDocumentBean bean = new DPrintDocumentBean();

    /**
     * Constructor.
     */
    public DPrintDocumentCtrl() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (event.getDialogBean() instanceof KeyboardBean) {
            if (((KeyboardBean) event.getDialogBean()).getValue() != null) {
                bean.getDocument().setDocumentCount((Integer) ((KeyboardBean) event.getDialogBean()).getValue());
            }
        }
    }

    /**
     * 
     * Show the Feature to entry the number of documents to print.
     * 
     * @param featureView featureView which call
     * @param displayListener display Listener.
     * @param identification Identification
     * @param dBean Dialog bean to create
     */
    public void showDocument(final FeatureView featureView, final DisplayListener displayListener,
            final Identification identification, final DPrintDocumentBean dBean) {
        if (featureView != null) {
            this.bean = dBean;

            KeyboardDialogModel model = new KeyboardDialogModel();
            model.setIdentification(identification);
            DialogView view = null;
            String viewType = System.getProperty(FeatureContainerController.VIF_UITYPE_KEY);
            if ("touch".equalsIgnoreCase(viewType)) {
                view = featureView.openDialog(DPrintDocumentTouchView.class);
            } else {
                // In attempt to dev the graphical object, display the touch view
                view = featureView.openDialog(DPrintDocumentTouchView.class);
            }
            view.setModel(model);

            // set Title
            view.setTitle(I18nClientManager.translate(ProductionMo.T29514) + " : " + bean.getDocument().getDocumentId());

            // attributes
            setDialogView(view);
            addDialogChangeListener(this);
            addDisplayListener(displayListener);

            KeyboardBean keyBoard = new KeyboardBean();

            model.setDialogBean(keyBoard);
            initialize();
            removeDialogChangeListener(this);
            removeDisplayListener(displayListener);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

}

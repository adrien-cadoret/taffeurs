/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FunctionnalityTools.java,v $
 * Created on 13 mars 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.ui.menu.load.MenuLoader;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.Key;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FSettingFunctionalityBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.Functionality;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCConstants;


/**
 * Tools use to manage the display of different Touch Screen.
 * 
 * <ul>
 * <li>Mo List Screen (TouchModelMapKey.CONTAINER_MO_LIST)</li>
 * <li>Input Production Screen (TouchModelMapKey.CONTAINER_INPUT_PRODUCTION)</li>
 * <li>Output Production Screen (TouchModelMapKey.CONTAINER_OUTPUT_PRODUCTION)n</li>
 * <li>Detail Entry Screen (TouchModelMapKey.DATA_ENTRY_LIST_FUNCT)</li>
 * </ul>
 * 
 * @author kl
 */
public final class FunctionnalityTools {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FunctionnalityTools.class);

    /**
     * Default Constructor.
     */
    private FunctionnalityTools() {

    }

    /**
     * Manage the display of the various available functionalities. If one functionality is not in the list, it will be
     * hide (bean.setFunctionality(true)).
     * 
     * Value of keyScreen manage :
     * <ul>
     * <li>TouchModelMapKey.CONTAINER_MO_LIST :</li> Mo List Screen
     * <li>TouchModelMapKey.CONTAINER_INPUT_PRODUCTION :</li> Container Input production Screen
     * <li>TouchModelMapKey.CONTAINER_OUTPUT_PRODUCTION :</li> Container Output production Screen *
     * </ul>
     * 
     * @param keyScreen key to define what screen will be manage.
     * @param functionalities list of functionalities to display.
     * @return a list of ToolBarBtnStdBaseModel
     */
    public static List<ToolBarBtnStdBaseModel> manageFunctionnalities(final TouchModelMapKey keyScreen,
            final List<FSettingFunctionalityBean> functionalities) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageFunctionnalities(keyScreen=" + keyScreen + ", functionalities=" + functionalities
                    + ")");
        }

        List<ToolBarBtnStdBaseModel> listButton = new ArrayList<ToolBarBtnStdBaseModel>();

        switch (keyScreen) {
            case CONTAINER_MO_LIST:
                listButton = manageMoListButtonToolbar(functionalities);
                break;
            case CONTAINER_INPUT_PRODUCTION:
                listButton = manageContainerInputProductionButtonToolbar(functionalities);
                break;
            case CONTAINER_OUTPUT_PRODUCTION:
                listButton = manageContainerOutputProductionButtonToolbar(functionalities);
                break;

            default:
                break;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageFunctionnalities(keyScreen=" + keyScreen + ", functionalities=" + functionalities
                    + ")=" + listButton);
        }
        return listButton;
    }

    /**
     * Manage the display of the various available functionalities. If one functionality is not in the list, it will be
     * hide (bean.setFunctionality(true)).
     * 
     * Manage Only this value of keyScreen manage :
     * <ul>
     * <li>TouchModelMapKey.DATA_ENTRY_LIST_FUNCT :</li> Detail Entry Input/Output Container Screen
     * </ul>
     * 
     * @param isDataEntry true is the screen to display is DetailEntryListInput *
     * @param keyScreen key to define what screen will be manage.
     * @param functionalities list of functionalities to display.
     * @return a list of ToolBarBtnStdBaseModel
     */
    public static List<ToolBarBtnStdBaseModel> manageFunctionnalitiesForDetail(final boolean isDataEntry,
            final TouchModelMapKey keyScreen, final List<FSettingFunctionalityBean> functionalities) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageFunctionnalitiesForDetail(isDataEntry=" + isDataEntry + ", keyScreen=" + keyScreen
                    + ", functionalities=" + functionalities + ")");
        }

        List<ToolBarBtnStdBaseModel> listButton = new ArrayList<ToolBarBtnStdBaseModel>();
        if (TouchModelMapKey.DATA_ENTRY_LIST_FUNCT.equals(keyScreen)) {
            listButton = manageDetailEntryButtonToolbar(isDataEntry, functionalities);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageFunctionnalitiesForDetail(isDataEntry=" + isDataEntry + ", keyScreen=" + keyScreen
                    + ", functionalities=" + functionalities + ")=" + listButton);
        }
        return listButton;
    }

    /**
     * Functionality.ASK_PRODUCED_ENTITY.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getAskProducedEntityButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.ASK_PRODUCED_ENTITY.getReferenceBtn().getReference());
        // TODO [GLC] change icon
        buttonStd.setIconURL("/images/vif/vif57/stock/entity/img48x48/open.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T39377, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.ASK_TATTOOED_NUMBER.
     *
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getAskTattooedNumberButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.ASK_TATTOOED_NUMBER.getReferenceBtn().getReference());
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T39164, false));
        buttonStd.setIconURL("/images/vif/vif57/production/mo/img48x48/tattooed_input_48.png");
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.CREAT_OF.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getCreatOfButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.CREAT_OF.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/newMO.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T29492, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.CREAT_OF_TECH.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getCreatOfTechButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.CREAT_OF_TECH.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/newMSO.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T29494, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.CRITERIA.
     * 
     * 
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getCriteriaButton() {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.CRITERIA.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/criteria.png");
        buttonStd.setText(I18nClientManager.translate(GenKernel.T29329, false));

        return buttonStd;
    }

    /**
     * Functionality.DELETE_MVT.
     * 
     * 
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getDeleteMvtButton() {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.DELETE_MVT.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/cancel.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T29516, false));

        return buttonStd;
    }

    /**
     * Functionality.DETAIL_MO_BTN.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getDetailMoButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.DETAIL_MO_BTN.getReferenceBtn().getReference());
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T29536, false));
        buttonStd.setIconURL("/images/vif/vif57/production/mo/detail.png");

        return buttonStd;
    }

    /**
     * Functionality.LABOR_STAFF.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getLaborStaffButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.LABOR_STAFF.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/img48x48/tempsoperateur.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T34692, false));
        buttonStd.setFunctionality(isFunctionnality);
        buttonStd.setShortcut(Key.K_F9);

        return buttonStd;
    }

    /**
     * Functionality.LOGICAL_WORKSTATION.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getLogicalWorkstationButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.LOGICAL_WORKSTATION.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/logicalWS.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T22289, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.MO_FINISH.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getMoFinishButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.MO_FINISH.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/finish.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T29510, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.MO_INPUT.
     * 
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getMoInputButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.MO_INPUT.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/productioninput.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T29511, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.MO_OUTPUT.
     * 
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getMoOutputButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.MO_OUTPUT.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/productionoutput.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T29512, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.OPEN_CONTAINER.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getOpenContainerButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.OPEN_CONTAINER.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/stock/entity/img48x48/open.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T39088, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.OPEN_INCIDENT_REFERENCE.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getOpenIncidentReferenceButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.OPEN_INCIDENT_REFERENCE.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/img48x48/incidentAdd.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T32884, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.OPEN_TATTOOED.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getOpenTattooedButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.OPEN_TATTOOED.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/stock/entity/img48x48/open.png"); // TODO [GLC] change icon
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T39182, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.CREAT_OF.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getPdfReferenceButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.PDF_REFERENCE.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/img48x48/process.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T30838, false));
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.PRINT.
     * 
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getPrintButton() {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.PRINT.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/print.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T29515, false));

        return buttonStd;
    }

    /**
     * Functionality.PRINT_CONTAINER.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getPrintContainerButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.PRINT_CONTAINER.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/print.png");
        buttonStd.setText("Réédition contenant");
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.PRINT_UPPER_CONTAINER.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getPrintUpperContainerButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.PRINT_UPPER_CONTAINER.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/vif/vif57/production/mo/print.png");
        buttonStd.setText("Réédition contenant père");
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Functionality.CREAT_OF.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getSettingInformationButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.SETTING_INFORMATION.getReferenceBtn().getReference());
        buttonStd.setIconURL("/images/jtech/touch/inform.png");
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T35724, false));
        buttonStd.setFunctionality(isFunctionnality);
        return buttonStd;
    }

    /**
     * Functionality.SHOW_ALL_REFERENCE.
     * 
     * @param isFunctionnality true is button is not visible as shortcut.
     * @return ToolBarBtnStdModel
     */
    private static ToolBarBtnStdModel getShowAllMoButton(final boolean isFunctionnality) {
        ToolBarBtnStdModel buttonStd = new ToolBarBtnStdModel();
        buttonStd.setReference(Functionality.SHOW_ALL_REFERENCE.getReferenceBtn().getReference());
        buttonStd.setText(I18nClientManager.translate(ProductionMo.T29545, false));
        buttonStd.setIconURL("/images/vif/vif57/production/mo/showfinished.png");
        buttonStd.setFunctionality(isFunctionnality);

        return buttonStd;
    }

    /**
     * Checks if the open incident feature is active for the user.
     * 
     * @return true, if the open incident feature is active for the user.
     */
    private static boolean isOpenIncidentActive() {
        return !(MenuLoader.getInstance().findFeatureInCurrentMenu(NCConstants.FeatureName.FLOW_TOUCH.getName()) == null)
                || !(MenuLoader.getInstance().findFeatureInCurrentMenu(NCConstants.FeatureName.FLOW.getName()) == null);
    }

    /**
     * Manage the display of the various available functionalities. If one functionality is not in the list, it will be
     * hide (bean.setFunctionality(true)).
     * 
     * List of functionality for InputContainerProduction Screen.
     * <ul>
     * <li>Functionality.MO_FINISH :</li>Mandatory display
     * <li>Functionality.OPEN_INCIDENT_REFERENCE</li>
     * <li>Functionality.PDF_REFERENCE</li>
     * </ul>
     * 
     * @param functionalities list of functionalities to display.
     * @return a list of ToolBarBtnStdBaseModel
     */
    private static List<ToolBarBtnStdBaseModel> manageContainerInputProductionButtonToolbar(
            final List<FSettingFunctionalityBean> functionalities) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageContainerInputProductionButtonToolbar(functionalities=" + functionalities + ")");
        }

        List<ToolBarBtnStdBaseModel> listButton = new ArrayList<ToolBarBtnStdBaseModel>();

        // Button defined has shortcut.
        for (FSettingFunctionalityBean functionality : functionalities) {
            if (functionality.getFunctionality() != null) {
                switch (functionality.getFunctionality()) {
                    case MO_FINISH:
                        listButton.add(getMoFinishButton(!functionality.getDirectOnScreen()));
                        break;
                    case OPEN_INCIDENT_REFERENCE:
                        if (isOpenIncidentActive()) {
                            listButton.add(getOpenIncidentReferenceButton(!functionality.getDirectOnScreen()));
                        }
                        break;
                    case PDF_REFERENCE:
                        listButton.add(getPdfReferenceButton(!functionality.getDirectOnScreen()));
                        break;
                    case ASK_PRODUCED_ENTITY:
                        listButton.add(getAskProducedEntityButton(!functionality.getDirectOnScreen()));
                        break;
                    case ASK_TATTOOED_NUMBER:
                        listButton.add(getAskTattooedNumberButton(!functionality.getDirectOnScreen()));
                        break;
                    case MO_OUTPUT:
                        listButton.add(getMoOutputButton(!functionality.getDirectOnScreen()));
                        break;
                    case DETAIL_MO_BTN:
                        listButton.add(getDetailMoButton(!functionality.getDirectOnScreen()));
                        break;
                    case SHOW_ALL_REFERENCE:
                        listButton.add(getShowAllMoButton(!functionality.getDirectOnScreen()));
                        break;
                    default:
                        break;
                }
            }
        }
        // always add info button in functionality
        listButton.add(getSettingInformationButton(true));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageContainerInputProductionButtonToolbar(functionalities=" + functionalities + ")="
                    + listButton);
        }
        return listButton;
    }

    /**
     * Manage the display of the various available functionalities. If one functionality is not in the list, it will be
     * hide (bean.setFunctionality(true)).
     * 
     * List of functionality for OutputContainerProduction Screen.
     * <ul>
     * <li>Functionality.MO_FINISH :</li>Mandatory display
     * <li>Functionality.OPEN_INCIDENT_REFERENCE</li>
     * <li>Functionality.PDF_REFERENCE</li>
     * </ul>
     * 
     * @param functionalities list of functionalities to display.
     * @return a list of ToolBarBtnStdBaseModel
     */
    private static List<ToolBarBtnStdBaseModel> manageContainerOutputProductionButtonToolbar(
            final List<FSettingFunctionalityBean> functionalities) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageContainerOutputProductionButtonToolbar(functionalities=" + functionalities + ")");
        }

        List<ToolBarBtnStdBaseModel> listButton = new ArrayList<ToolBarBtnStdBaseModel>();

        // Button defined has shortcut.
        for (FSettingFunctionalityBean functionality : functionalities) {
            switch (functionality.getFunctionality()) {
                case MO_FINISH:
                    listButton.add(getMoFinishButton(!functionality.getDirectOnScreen()));
                    break;
                case OPEN_INCIDENT_REFERENCE:
                    if (isOpenIncidentActive()) {
                        listButton.add(getOpenIncidentReferenceButton(!functionality.getDirectOnScreen()));
                    }
                    break;
                case PDF_REFERENCE:
                    listButton.add(getPdfReferenceButton(!functionality.getDirectOnScreen()));
                    break;
                case MO_INPUT:
                    listButton.add(getMoInputButton(!functionality.getDirectOnScreen()));
                    break;
                case DETAIL_MO_BTN:
                    listButton.add(getDetailMoButton(!functionality.getDirectOnScreen()));
                    break;
                case SHOW_ALL_REFERENCE:
                    listButton.add(getShowAllMoButton(!functionality.getDirectOnScreen()));
                    break;
                case OPEN_CONTAINER:
                    listButton.add(getOpenContainerButton(true));
                    break;
                case OPEN_TATTOOED:
                    listButton.add(getOpenTattooedButton(true));
                    break;
                default:
                    break;
            }
        }

        // always add info button in functionality
        listButton.add(getSettingInformationButton(true));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageContainerOutputProductionButtonToolbar(functionalities=" + functionalities + ")="
                    + listButton);
        }
        return listButton;
    }

    /**
     * Manage the display of the various available functionalities. If one functionality is not in the list, it will be
     * hide (bean.setFunctionality(true)).
     * 
     * List of functionality for DetailEntry (Input/Output) Screen.
     * <ul>
     * <li>Functionality.CRITERIA :</li>Mandatory display
     * <li>Functionality.DELETE_MVT :</li>Mandatory display
     * <li>Functionality.OPEN_INCIDENT_REFERENCE</li>
     * <li>Functionality.PRINT :</li>Mandatory display
     * <li>Functionality.PRINT_CONTAINER</li>
     * <li>Functionality.PRINT_UPPER_CONTAINER</li>
     * </ul>
     * 
     * @param isDataEntry if true, it means we are in dataEntryList, so don't display the printUpperContainer.
     * @param functionalities list of functionalities to display.
     * @return a list of ToolBarBtnStdBaseModel
     */
    private static List<ToolBarBtnStdBaseModel> manageDetailEntryButtonToolbar(final boolean isDataEntry,
            final List<FSettingFunctionalityBean> functionalities) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageDetailEntryButtonToolbar(isDataEntry=" + isDataEntry + ", functionalities="
                    + functionalities + ")");
        }

        List<ToolBarBtnStdBaseModel> listButton = new ArrayList<ToolBarBtnStdBaseModel>();

        // Button defined has shortcut.
        for (FSettingFunctionalityBean functionality : functionalities) {
            switch (functionality.getFunctionality()) {
                case CRITERIA:
                    listButton.add(getCriteriaButton());
                    break;
                case DELETE_MVT:
                    listButton.add(getDeleteMvtButton());
                    break;
                case OPEN_INCIDENT_REFERENCE:
                    if (isOpenIncidentActive()) {
                        listButton.add(getOpenIncidentReferenceButton(!functionality.getDirectOnScreen()));
                    }
                    break;
                case PRINT:
                    listButton.add(getPrintButton());
                    break;
                case PRINT_CONTAINER:
                    listButton.add(getPrintContainerButton(!functionality.getDirectOnScreen()));
                    break;
                case PRINT_UPPER_CONTAINER:
                    if (!isDataEntry) {
                        listButton.add(getPrintUpperContainerButton(!functionality.getDirectOnScreen()));
                    }
                    break;

                default:
                    break;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageDetailEntryButtonToolbar(isDataEntry=" + isDataEntry + ", functionalities="
                    + functionalities + ")=" + listButton);
        }
        return listButton;
    }

    /**
     * Manage the display of the various available functionalities. If one functionality is not in the list, it will be
     * hide (bean.setFunctionality(true)).
     * 
     * List of functionality for Mo List Screen.
     * <ul>
     * <li>Functionality.CREAT_OF</li>
     * <li>Functionality.CREAT_OF_TECH</li>
     * <li>Functionality.LABOR_STAFF</li>
     * <li>Functionality.LOGICAL_WORKSTATION</li>
     * <li>Functionality.MO_FINISH</li>
     * <li>Functionality.MO_INPUT :</li>Mandatory display
     * <li>Functionality.MO_OUTPUT :</li>Mandatory display
     * <li>Functionality.OPEN_INCIDENT_REFERENCE</li>
     * <li>Functionality.PDF_REFERENCE</li>
     * </ul>
     * 
     * @param functionalities list of functionalities to display.
     * @return a list of ToolBarBtnStdBaseModel
     */
    private static List<ToolBarBtnStdBaseModel> manageMoListButtonToolbar(
            final List<FSettingFunctionalityBean> functionalities) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageMoListButtonToolbar(functionalities=" + functionalities + ")");
        }

        List<ToolBarBtnStdBaseModel> listButton = new ArrayList<ToolBarBtnStdBaseModel>();

        // Button defined has shortcut.
        for (FSettingFunctionalityBean functionality : functionalities) {
            switch (functionality.getFunctionality()) {
                case MO_INPUT:
                    listButton.add(getMoInputButton(!functionality.getDirectOnScreen()));
                    break;
                case MO_OUTPUT:
                    listButton.add(getMoOutputButton(!functionality.getDirectOnScreen()));
                    break;
                case CREAT_OF:
                    listButton.add(getCreatOfButton(!functionality.getDirectOnScreen()));
                    break;
                case CREAT_OF_TECH:
                    listButton.add(getCreatOfTechButton(!functionality.getDirectOnScreen()));
                    break;
                case LABOR_STAFF:
                    listButton.add(getLaborStaffButton(!functionality.getDirectOnScreen()));
                    break;
                case LOGICAL_WORKSTATION:
                    listButton.add(getLogicalWorkstationButton(!functionality.getDirectOnScreen()));
                    break;
                case MO_FINISH:
                    listButton.add(getMoFinishButton(!functionality.getDirectOnScreen()));
                    break;
                case OPEN_INCIDENT_REFERENCE:
                    if (isOpenIncidentActive()) {
                        listButton.add(getOpenIncidentReferenceButton(!functionality.getDirectOnScreen()));
                    }
                    break;
                case PDF_REFERENCE:
                    listButton.add(getPdfReferenceButton(!functionality.getDirectOnScreen()));
                    break;
                default:
                    break;
            }
        }

        // always add info button in functionality
        listButton.add(getSettingInformationButton(true));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageMoListButtonToolbar(functionalities=" + functionalities + ")=" + listButton);
        }
        return listButton;
    }
}

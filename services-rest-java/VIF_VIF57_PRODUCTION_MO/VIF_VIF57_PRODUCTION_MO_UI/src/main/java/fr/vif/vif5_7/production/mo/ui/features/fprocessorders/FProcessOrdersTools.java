/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersTools.java,v $
 * Created on 31 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.models.sharedcontext.SharedContext;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersCfgBean;


/**
 * Tools for the production indicator feature.
 * 
 * @author jd
 */
public final class FProcessOrdersTools {

    public static final String  FORMAT_DATE            = "dd/MM/yyyy";
    public static final String  FORMAT_DATE_HOUR       = "dd/MM/yyyy HH:mm";
    public static final String  FORMAT_HOUR            = "HH:mm";

    private static final String CTX_PROCESS_ORDERS_CFG = "PROCESS_ORDERS_CFG";

    /** LOGGER. */
    private static final Logger LOGGER                 = Logger.getLogger(FProcessOrdersTools.class);

    /**
     * Constructor.
     */
    private FProcessOrdersTools() {

    }

    /**
     * Get the supervisor configuration.
     * 
     * @param ctx shared context
     * @return configuration bean
     */
    public static FProcessOrdersCfgBean getConfiguration(final SharedContext ctx) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getConfiguration(ctx=" + ctx + ")");
        }

        // Share the parameters for FLines

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getConfiguration(ctx=" + ctx + ")");
        }
        return (FProcessOrdersCfgBean) ctx.get(Domain.DOMAIN_THIS, CTX_PROCESS_ORDERS_CFG);
    }

    /**
     * Set the configuration in a shared context.
     * 
     * @param ctrl Supervisor controller
     * @param processOrdersCfg configuration bean.
     */
    public static void setConfiguration(final FeatureController ctrl, final FProcessOrdersCfgBean processOrdersCfg) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setConfiguration(ctrl=" + ctrl + ", processOrdersCfg=" + processOrdersCfg + ")");
        }

        // Share the parameters for FLines
        ctrl.getSharedContext().put(Domain.DOMAIN_THIS, CTX_PROCESS_ORDERS_CFG, processOrdersCfg);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setConfiguration(ctrl=" + ctrl + ", processOrdersCfg=" + processOrdersCfg + ")");
        }
    }

}

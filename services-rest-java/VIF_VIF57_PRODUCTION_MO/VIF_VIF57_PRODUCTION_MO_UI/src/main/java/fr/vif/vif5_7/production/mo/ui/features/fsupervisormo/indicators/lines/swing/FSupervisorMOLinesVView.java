/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOLinesVView.java,v $
 * Created on 20 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.lines.swing;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPopupMenu;
import javax.swing.ListSelectionModel;

import org.apache.log4j.Logger;

import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.swing.SwingBrowser;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.generic.GenericActionListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.viewer.swing.StandardSwingViewer;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersVBean;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersBModel;


/**
 * Supervisor : Viewer View.
 * 
 * @author jd
 */
public class FSupervisorMOLinesVView extends StandardSwingViewer<FProcessOrdersVBean> implements ActionListener,
        MouseListener {
    /** LOGGER. */
    private static final Logger                            LOGGER                = Logger.getLogger(FSupervisorMOLinesVView.class);

    private AbstractBrowserController<FProcessOrdersBBean> browserController     = null;
    private SwingBrowser                                   browserView;
    private List<GenericActionListener>                    genericActionListener = new ArrayList<GenericActionListener>();
    private JPopupMenu                                     popup                 = null;

    /**
     * Default constructor.
     * 
     */
    public FSupervisorMOLinesVView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {

        GenericActionEvent event = new GenericActionEvent(browserView.getJTable(),
                GenericActionEvent.EVENT_ACTION_PERFORMED, e.getActionCommand(), null);

        for (GenericActionListener listener : genericActionListener) {
            listener.actionPerformed(event);
        }

    }

    /**
     * {@inheritDoc}
     */
    public void addGenericActionListener(final GenericActionListener listener) {
        this.genericActionListener.add(listener);
    }

    /**
     * {@inheritDoc}
     */
    // @Override
    // public void changeCurrentModel(final Functionality model) {
    // IPiledUpLinesPersonalization p = FPiledUpLinesUITools.getPiledUpPersonalizationManager(browserController
    // .getSharedContext());
    //
    // if (p != null) {
    // ((IPiledUpLinesBCtrl) browserController).openQuery(p.getPersonalization(model));
    //
    // }
    //
    // }

    /**
     * {@inheritDoc}
     */
    public AbstractBrowserController<FProcessOrdersBBean> getBrowserController() {
        return browserController;
    }

    /**
     * {@inheritDoc}
     */
    public SwingBrowser getBrowserView() {
        return browserView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final MouseEvent e) {
        if (e.getSource() != null && e.getButton() == 3) {
            popup.show(this, e.getX(), e.getY());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseEntered(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseExited(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    public void removeGenericActionListener(final GenericActionListener listener) {
        this.genericActionListener.remove(listener);
    }

    /**
     * {@inheritDoc}
     */
    // @Override
    public void setPopup(final JPopupMenu popup) {
        this.popup = popup;

    }

    /**
     * get the browser view.
     * 
     * @return {@link SwingBrowser}
     */
    @SuppressWarnings("unchecked")
    private SwingBrowser<FProcessOrdersBBean> getFSupervisorMOLinesBView() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getFSupervisorMOLinesBView()");
        }

        if (browserView == null) {
            browserView = new SwingBrowser<FProcessOrdersBBean>();

            try {
                FProcessOrdersBModel browserModel = new FProcessOrdersBModel();
                browserView.setModel(browserModel);
                browserController = StandardControllerFactory.getInstance().getController(FProcessOrdersBCtrl.class);
                browserController.setView(browserView);
                browserView.getJTable().setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
                browserView.getJTable().addMouseListener(this);

                addMouseListener(this);

            } catch (UIException e) {
                LOGGER.debug("Exception - getFSupervisorMOLinesBView()=" + browserView + ",error=" + e.getMessage());
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getFSupervisorMOLinesBView()=" + browserView);
        }
        return browserView;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 1, 0 };
        gridBagLayout.rowHeights = new int[] { 1, 0 };
        gridBagLayout.columnWeights = new double[] { 1.0, Double.MIN_VALUE };
        gridBagLayout.rowWeights = new double[] { 1.0, Double.MIN_VALUE };
        setLayout(gridBagLayout);
        GridBagConstraints gbcbrowserView = new GridBagConstraints();
        gbcbrowserView.fill = GridBagConstraints.BOTH;
        gbcbrowserView.gridx = 0;
        gbcbrowserView.gridy = 0;
        add(getFSupervisorMOLinesBView(), gbcbrowserView);

    }

}

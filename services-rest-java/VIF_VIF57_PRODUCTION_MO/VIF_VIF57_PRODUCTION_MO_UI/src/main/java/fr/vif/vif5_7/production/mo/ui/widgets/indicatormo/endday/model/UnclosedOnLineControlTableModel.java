/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: UnclosedOnLineControlTableModel.java,v $
 * Created on 3 mai 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.model;


import java.text.SimpleDateFormat;

import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedOnLineControlBean;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.AbstractEndDayIndicatorTableModel;


/**
 * UnclosedOnLineControlTable model.
 *
 * @author ac
 */
public class UnclosedOnLineControlTableModel extends AbstractEndDayIndicatorTableModel {

    /** column count. */
    private static final int COLUMN_COUNT = 5;

    private String[]         columnNames  = { I18nClientManager.translate(Generic.T3340, false),
            I18nClientManager.translate(ProductionMo.T34865, false),
            I18nClientManager.translate(ProductionMo.T40694, false),
            I18nClientManager.translate(ProductionMo.T39797, false),
            I18nClientManager.translate(ProductionMo.T29519, false) };

    /**
     * Default Ctor.
     */
    public UnclosedOnLineControlTableModel() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getColumnCount() {
        return COLUMN_COUNT;
    }

    @Override
    public String getColumnName(final int c) {
        return columnNames[c];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        Object result = new Object();
        if (getEndDayMOList() != null && getEndDayMOList().size() > 0) {
            UnclosedOnLineControlBean unclosedOnLineControlBean = (UnclosedOnLineControlBean) getEndDayMOList().get(
                    rowIndex);
            // CHECKSTYLE:OFF

            switch (columnIndex) {
                case 0:
                    result = unclosedOnLineControlBean.getCposte();
                    break;
                case 1:
                    result = unclosedOnLineControlBean.getLine();
                    break;
                case 2:
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    result = dateFormat.format(unclosedOnLineControlBean.getCreationDate());
                    break;
                case 3:
                    result = unclosedOnLineControlBean.getChronof();
                    break;
                case 4:
                    result = unclosedOnLineControlBean.getCart();
                    break;
                default:
                    break;
            }
            // CHECKSTYLE:ON

        }
        return result;
    }

}

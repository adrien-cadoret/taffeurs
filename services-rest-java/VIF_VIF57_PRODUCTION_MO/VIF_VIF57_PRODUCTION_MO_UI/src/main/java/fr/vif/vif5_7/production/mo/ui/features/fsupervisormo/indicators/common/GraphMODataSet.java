/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_COMMON
 * File : $RCSfile: GraphMODataSet.java,v $
 * Created on 5 août 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * Contains a graph data set.
 * 
 * @author ag
 */
public class GraphMODataSet implements Serializable, Comparable<GraphMODataSet> {
    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    private String            title            = "";

    private String            order            = "";

    private List<GraphMOData> graphData        = new ArrayList<GraphMOData>();

    private String            group            = "G1";

    /**
     * Constructor.
     */
    public GraphMODataSet() {
        super();
        this.title = "No Title Graph Data Set";
        this.order = title;
    }

    /**
     * Constructor with title.
     * 
     * @param title title
     */
    public GraphMODataSet(final String title) {
        super();
        this.title = title;
    }

    /**
     * Constructor with title and data.
     * 
     * @param title title
     * @param graphData graphData
     */
    public GraphMODataSet(final String title, final List<GraphMOData> graphData) {
        super();
        this.title = title;
        this.graphData = graphData;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int compareTo(final GraphMODataSet set) {
        int resultat;
        resultat = this.order.compareTo(set.order);
        return resultat;
    }

    /**
     * Gets the graphData.
     * 
     * @return the graphData.
     */
    public List<GraphMOData> getGraphData() {
        return graphData;
    }

    /**
     * Gets the group.
     * 
     * @return the group.
     */
    public String getGroup() {
        return group;
    }

    /**
     * Gets the order.
     * 
     * @return the order.
     */
    public String getOrder() {
        return order;
    }

    /**
     * Gets the title.
     * 
     * @return the title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Sets the graphData.
     * 
     * @param graphData graphData.
     */
    public void setGraphData(final List<GraphMOData> graphData) {
        this.graphData = graphData;
    }

    /**
     * Sets the group.
     * 
     * @param group group.
     */
    public void setGroup(final String group) {
        this.group = group;
    }

    /**
     * Sets the order.
     * 
     * @param order order.
     */
    public void setOrder(final String order) {
        this.order = order;
    }

    /**
     * Sets the title.
     * 
     * @param title title.
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "GraphDataSet [title=" + title + "]";
    }

}

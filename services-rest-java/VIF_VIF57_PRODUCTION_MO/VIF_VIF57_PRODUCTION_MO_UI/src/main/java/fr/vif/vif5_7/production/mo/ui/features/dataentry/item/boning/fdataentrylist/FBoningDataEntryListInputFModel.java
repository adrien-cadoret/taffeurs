/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputFModel.java,v $
 * Created on 10 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;


/**
 * BoningDataEntryListInput : Feature model.
 * 
 * @author xg
 */
public class FBoningDataEntryListInputFModel extends
        StandardFeatureModel<FBoningDataEntryListInputBBean, FBoningDataEntryListInputBBean> {

    /**
     * Default constructor.
     */
    public FBoningDataEntryListInputFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FBoningDataEntryListInputBModel.class, null,
                FBoningDataEntryListInputBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FBoningDataEntryListInputVModel.class, null,
                FBoningDataEntryListInputVCtrl.class));
    }
}

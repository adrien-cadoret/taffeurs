/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOBarPainter.java,v $
 * Created on 24 janv. 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Color;
import java.awt.GradientPaint;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Stroke;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;
import java.util.Date;

import org.jfree.chart.renderer.xy.GradientXYBarPainter;
import org.jfree.chart.renderer.xy.XYBarRenderer;
import org.jfree.data.Range;
import org.jfree.data.gantt.XYTaskDataset;
import org.jfree.ui.RectangleEdge;

import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.constants.Mnemos.POIndicatorState;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOPeriod;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.GraphMOTools;


/**
 * Create a gradient painted bar.
 * 
 * @author ag
 */
public class FGanttMOBarPainter extends GradientXYBarPainter {

    // private static double g1 = 0.1;
    // private static double g2 = 0.5;
    // private static double g3 = 0.8;

    private static double g1 = 0.0;
    private static double g2 = 0.0;
    private static double g3 = 0.0;

    /**
     * Creates a new instance.
     * 
     * @param graph
     */
    public FGanttMOBarPainter() {
        super(g1, g2, g3);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void paintBar(final Graphics2D g, final XYBarRenderer renderer, final int row, final int column,
            final RectangularShape bar, final RectangleEdge base) {
        super.paintBar(g, renderer, row, column, bar, base);

        XYTaskDataset dataset = (XYTaskDataset) renderer.getPlot().getDataset();

        Range range = renderer.getPlot().getRangeAxis().getRange();
        FGanttMOTask task = (FGanttMOTask) dataset.getTasks().getSeries(row).get(column);

        Date d1 = new Date((long) range.getLowerBound());
        Date d2 = new Date((long) range.getUpperBound());

        Date s1 = task.getDuration().getStart();
        Date s2 = task.getDuration().getEnd();

        double duration = (s2.getTime() - s1.getTime()) / 1000f; // seconds. Time visible of Gantt
        double percent = 0;
        /* FB DP2581 */
        if (((IndicatorMO) ((GraphMOPeriod) task.getBean()).getBeanSource()).getInfos().getState() == POIndicatorState.FINISHED
                || ((IndicatorMO) ((GraphMOPeriod) task.getBean()).getBeanSource()).getInfos().getState() == POIndicatorState.FINISHED_IN_WORKSHOP) {
            percent = 1;
        } else {
            percent = Math.min(1, task.getPercentComplete());
        }
        double widthPct = 0;
        double scale = 0;

        double widthT = 0;

        // case one : --[-|-]-----|---
        if (s1.before(d1) && s2.before(d2)) {
            scale = bar.getWidth() / ((s2.getTime() - d1.getTime()) / 1000);
            widthT = scale * duration; // bar.getWidth() * (duration / visible); // / rangeD;

            // visibleW = visible * bar.getWidth() / rangeD;

            widthPct = Math.min(widthT - scale * duration * percent, bar.getWidth());

            // case three : ---|-[--]---|---
        } else if (s1.after(d1) && s2.before(d2)) {
            // scale = bar.getWidth() / ((s2.getTime() - d1.getTime()) / 1000);
            widthT = bar.getWidth(); // scale * duration;

            widthPct = Math.min(widthT - widthT * percent, bar.getWidth());

            // case two : ---|-----[-|-]--
        } else if (s1.after(d1) && s2.after(d2)) {
            scale = bar.getWidth() / ((d2.getTime() - s1.getTime()) / 1000);
            widthT = scale * duration;

            double notVisible = (s2.getTime() - d2.getTime()) / 1000;

            widthPct = Math.min(widthT - (widthT * percent + notVisible * scale), bar.getWidth());

            // case four : -[-|------|-]--
        } else if (s1.before(d1) && s2.after(d2)) {
            scale = bar.getWidth() / ((s2.getTime() - d1.getTime()) / 1000);
            double notVisible = (s2.getTime() - d2.getTime()) / 1000;

            widthT = scale * duration; // bar.getWidth() * (duration / visible); // / rangeD;

            widthPct = Math.min(widthT - scale * duration * percent - notVisible * scale, bar.getWidth());

        }
        // if (((GraphMOPeriod<IndicatorMO>) task.getBean()).getBeanSource().getPoKey().getChrono().getChrono() == 1114)
        // {
        // System.out.println(" task="
        // + ((GraphMOPeriod<IndicatorMO>) task.getBean()).getBeanSource().getPoKey().getChrono()//
        // + " barwidth=" + (int) bar.getWidth() //
        // + " widthT=" + (int) widthT //
        // + " %=" + percent
        // // + " widthvis=" + (int) visibleW //
        // + " widthPct=" + (int) widthPct);
        // }

        GradientPaint gradient = null;
        Color c0 = null;
        /* FB DP2581 */
        if (((IndicatorMO) ((GraphMOPeriod) task.getBean()).getBeanSource()).getInfos().getState() == POIndicatorState.FINISHED
                || ((IndicatorMO) ((GraphMOPeriod) task.getBean()).getBeanSource()).getInfos().getState() == POIndicatorState.FINISHED_IN_WORKSHOP) {
            gradient = GraphMOTools.getGradientPaintForPercent(1);
            c0 = GraphMOTools.getColorForPercent(1);
        } else {
            gradient = GraphMOTools.getGradientPaintForPercent(task.getPercentComplete());
            c0 = GraphMOTools.getColorForPercent(task.getPercentComplete());
        }

        c0 = gradient.getColor2();
        Color c1 = c0.brighter();
        c1 = gradient.getColor1();

        Rectangle2D[] regions = splitHorizontalBar(bar, g1, g2, g3, widthPct);

        // GradientPaint gp = new GradientPaint(0.0f, (float) regions[0].getMinY(), c0, 0.0f,
        // (float) regions[0].getMaxX(), Color.WHITE);
        // GradientPaint gp = new GradientPaint(0.0f, (float) regions[0].getMinY(), gradient.getColor2(), 0.0f,
        // (float) regions[0].getMaxX(), gradient.getColor1());
        // g.setPaint(gp);
        // g.fill(regions[0]);

        // gp = new GradientPaint(0.0f, (float) regions[1].getMinY(), Color.WHITE, 0.0f, (float) regions[1].getMaxY(),
        // c0);
        // g.setPaint(gp);
        // g.fill(regions[1]);

        // gp = new GradientPaint(0.0f, (float) regions[2].getMinY(), c0, 0.0f, (float) regions[2].getMaxY(), c1);
        // g.setPaint(gp);
        // g.fill(regions[2]);

        GradientPaint gp = new GradientPaint(0.0f, (float) regions[3].getMinY(), c1, 0.0f,
                (float) regions[3].getMaxY(), c0);
        g.setPaint(gp);
        g.fill(regions[3]);

        // g.setPaint(gradient);

        // renderer.setSeriesOutlinePaint(series, paint);
        if (renderer.isDrawBarOutline()
                /* && state.getBarWidth() > renderer.BAR_OUTLINE_WIDTH_THRESHOLD */) {
            Stroke stroke = renderer.getItemOutlineStroke(row, column);

            // renderer.setSeriesOutlinePaint(series, paint);
            Paint paint = renderer.getItemPaint(row, column);
            if (stroke != null && paint != null) {
                g.setStroke(stroke);
                g.setPaint(paint);
                g.draw(bar);
            }
        }

        // renderer.setShadowVisible(false);
        // renderer.setBaseItemLabelGenerator(new StandardCategoryItemLabelGenerator());
        // renderer.setDrawBarOutline(true);
        // renderer.setItemMargin(0.0);
        // renderer.setShadowVisible(false);

        // renderer.setBarPainter(new FGanttMOBarPainter()); // FIXME C'est çà qui enlève un effet pourri à une barre
        // JFREE
        // renderer.setBaseItemLabelsVisible(false);
        // renderer.setBaseItemLabelPaint(Color.black);
        // final ItemLabelPosition p = new ItemLabelPosition(ItemLabelAnchor.CENTER, TextAnchor.CENTER);
        // renderer.setBasePositiveItemLabelPosition(p);

    }

    /**
     * Splits a bar into subregions (elsewhere, these subregions will have different gradients applied to them).
     * 
     * @param bar the bar shape.
     * @param a the first division.
     * @param b the second division.
     * @param c the third division.
     * @param durationPct width of pct bar
     * @return An array containing four subregions.
     */
    private Rectangle2D[] splitHorizontalBar(final RectangularShape bar, final double a, final double b,
            final double c, final double durationPct) {
        Rectangle2D[] result = new Rectangle2D[4];
        double y0 = bar.getMinY();
        double y1 = Math.rint(y0 + (bar.getHeight() * a));
        double y2 = Math.rint(y0 + (bar.getHeight() * b));
        double y3 = Math.rint(y0 + (bar.getHeight() * c));
        result[0] = new Rectangle2D.Double(bar.getMinX(), bar.getMinY(), bar.getWidth() - durationPct, y1 - y0);
        result[1] = new Rectangle2D.Double(bar.getMinX(), y1, bar.getWidth() - durationPct, y2 - y1);
        result[2] = new Rectangle2D.Double(bar.getMinX(), y2, bar.getWidth() - durationPct, y3 - y2);
        result[3] = new Rectangle2D.Double(bar.getMinX(), y3, bar.getWidth() - durationPct, bar.getMaxY() - y3);
        return result;
    }
}

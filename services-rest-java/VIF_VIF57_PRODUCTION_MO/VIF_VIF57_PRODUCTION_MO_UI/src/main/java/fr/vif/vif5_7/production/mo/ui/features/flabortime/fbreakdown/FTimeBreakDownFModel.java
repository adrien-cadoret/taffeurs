/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownFModel.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.GeneralSelectionMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.jtech.ui.selection.GeneralSelectionModel;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing.FTimeBreakDownGSView;


/**
 * TimeBreakDown : Feature Model.
 * 
 * @author cj
 * @version $Revision: 1.3 $, $Date: 2016/05/26 09:41:19 $
 */
public class FTimeBreakDownFModel extends StandardFeatureModel {

    /**
     * Default constructor.
     * 
     */
    public FTimeBreakDownFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FTimeBreakDownBModel.class, null, FTimeBreakDownBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FTimeBreakDownVModel.class, null, FTimeBreakDownVCtrl.class));
        setGeneralSelectionTriad(new GeneralSelectionMVCTriad(GeneralSelectionModel.class, FTimeBreakDownGSView.class,
                FTimeBreakDownGSCtrl.class));
    }
}

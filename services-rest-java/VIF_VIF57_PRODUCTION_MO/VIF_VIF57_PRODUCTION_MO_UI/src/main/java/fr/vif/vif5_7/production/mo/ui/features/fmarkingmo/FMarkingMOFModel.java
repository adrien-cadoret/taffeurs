/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOFModel.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;


/**
 * MarkingMO : Feature Model.
 * 
 * @author lmo
 */
public class FMarkingMOFModel extends StandardFeatureModel {

    /**
     * Default constructor.
     * 
     */
    public FMarkingMOFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FMarkingMOBModel.class, null, FMarkingMOBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FMarkingMOVModel.class, null, FMarkingMOVCtrl.class));
    }

}

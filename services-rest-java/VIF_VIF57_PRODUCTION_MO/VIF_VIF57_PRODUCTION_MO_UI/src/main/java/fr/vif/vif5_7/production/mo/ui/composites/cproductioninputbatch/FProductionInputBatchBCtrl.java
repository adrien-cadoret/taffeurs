/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputBatchBCtrl.java,v $
 * Created on 12 déc. 2008 by gv
 */
package fr.vif.vif5_7.production.mo.ui.composites.cproductioninputbatch;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputbatch.FProductionInputBatchBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputbatch.FProductionInputBatchBeanComparator;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputbatch.FProductionInputBatchSBean;
import fr.vif.vif5_7.production.mo.business.services.composites.cproductioninputbatch.CProductionInputBatchCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.flabels.LabelPrintHelper;
import fr.vif.vif5_7.stock.kernel.business.beans.common.quality.BatchCriteria;


/**
 * FProductionInputBatch : Browser Controller.
 * 
 * @author gv
 */
public class FProductionInputBatchBCtrl extends AbstractBrowserController<FProductionInputBatchBean> {

    private static final Logger                 LOGGER           = Logger.getLogger(FProductionInputBatchBCtrl.class);

    private static String                       propertyFile     = "batchBrowser.properties";
    private static String                       freeColumnId     = "freeColumn";

    private CProductionInputBatchCBS            ffProductionInputBatchCBS;

    private FProductionInputBatchBeanComparator comparator       = new FProductionInputBatchBeanComparator();
    private DateFormat                          simpleDateFormat = new SimpleDateFormat("dd/MM/yy");
    private DecimalFormat                       numberFormat     = new DecimalFormat("###0.###");
    private String                              column           = "";
    private boolean                             useColumn        = false;

    /**
     * Default constructor.
     * 
     */
    public FProductionInputBatchBCtrl() {
        super();

    }

    /**
     * Gets the column.
     * 
     * @category getter
     * @return the column.
     */
    public String getColumn() {
        return column;
    }

    /**
     * Gets the ffProductionInputBatchCBS.
     * 
     * @category getter
     * @return the ffProductionInputBatchCBS.
     */
    public final CProductionInputBatchCBS getFfProductionInputBatchCBS() {
        return ffProductionInputBatchCBS;
    }

    /**
     * Gets the useColumn.
     * 
     * @category getter
     * @return the useColumn.
     */
    public boolean getUseColumn() {
        return useColumn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {

        try {
            Properties properties = new Properties();
            properties.load(LabelPrintHelper.class.getClassLoader().getResourceAsStream(
                    FProductionInputBatchBCtrl.propertyFile));
            column = properties.getProperty(freeColumnId);
            useColumn = true;
            if (useColumn) {
                getModel().addColumn(new BrowserColumn(column, "freeColumn", 10));
            }
        } catch (FileNotFoundException e) {
            LOGGER.error(e);
        } catch (IOException e) {
            LOGGER.error(e);
        }

        super.initialize();
    }

    /**
     * Sets the ffProductionInputBatchCBS.
     * 
     * @category setter
     * @param ffProductionInputBatchCBS ffProductionInputBatchCBS.
     */
    public final void setFfProductionInputBatchCBS(final CProductionInputBatchCBS ffProductionInputBatchCBS) {
        this.ffProductionInputBatchCBS = ffProductionInputBatchCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FProductionInputBatchBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FProductionInputBatchBean> lst = null;
        try {

            if (getUseColumn()) {
                List<String> lstCrit = new ArrayList<String>();
                lstCrit.add(getColumn());
                ((FProductionInputBatchSBean) selection).getStockPointSBean().setCriteriasInBean(lstCrit);
            }

            lst = getFfProductionInputBatchCBS().queryElements(getIdCtx(), selection, startIndex, rowNumber);
            // fill the other column.
            if (useColumn) {
                for (FProductionInputBatchBean fProductionInputBatchBean : lst) {
                    fillColumn(fProductionInputBatchBean);
                }
            }
            Collections.sort(lst, comparator);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return lst;
    }

    /**
     * fill the free col.
     * 
     * @param fProductionInputBatchBean c
     */
    private void fillColumn(final FProductionInputBatchBean fProductionInputBatchBean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fillColumn(fProductionInputBatchBean=" + fProductionInputBatchBean + ")");
        }

        if (fProductionInputBatchBean.getStockPointBean().getBatchCriteriaValues() != null) {
            for (BatchCriteria batchCriteria : fProductionInputBatchBean.getStockPointBean().getBatchCriteriaValues()) {
                if (batchCriteria.getCriteriaId().equals(column)) {
                    if (batchCriteria.getDateValue() != null) {
                        fProductionInputBatchBean.setFreeColumn(simpleDateFormat.format(batchCriteria.getDateValue()));
                    } else if (batchCriteria.getStringValue() != null) {
                        fProductionInputBatchBean.setFreeColumn(batchCriteria.getStringValue());
                    } else if (batchCriteria.getNumValue() != null && batchCriteria.getNumValue() != 0) {
                        fProductionInputBatchBean.setFreeColumn(numberFormat.format(batchCriteria.getNumValue()));
                    }
                    break;
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fillColumn(fProductionInputBatchBean=" + fProductionInputBatchBean + ")");
        }
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: CDowngradedItemIView.java,v $
 * Created on 8 déc. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.composites.cdowngradeditem;


import fr.vif.jtech.common.beans.Selection;


/**
 * Used by the controller to get data from the view.
 * 
 * @author gp
 */
public interface CDowngradedItemIView {

    /**
     * Sets the selection bean for the query elements.
     * 
     * @param selection The selection to use.
     */
    public void setSelection(Selection selection);

    /**
     * Gets the selection bean.
     * 
     * @return The Selection bean.
     */
    public Selection getSelection();
}

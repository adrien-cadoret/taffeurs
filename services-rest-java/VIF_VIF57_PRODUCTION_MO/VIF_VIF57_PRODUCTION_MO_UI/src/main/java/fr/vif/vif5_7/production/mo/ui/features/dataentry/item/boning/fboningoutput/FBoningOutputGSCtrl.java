/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputGSCtrl.java,v $
 * Created on 3 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.selection.AbstractGeneralSelectionController;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.fboningoutput.FBoningOutputCBS;


/**
 * General Selection controller.
 * 
 * @author xg
 */

public class FBoningOutputGSCtrl extends AbstractGeneralSelectionController {

    private static final Logger LOGGER = Logger.getLogger(FBoningOutputGSCtrl.class);

    private FBoningOutputCBS    fboningOutputCBS;

    /**
     * Gets the fboningOutputCBS.
     * 
     * @return the fboningOutputCBS.
     */
    public FBoningOutputCBS getFboningOutputCBS() {
        return fboningOutputCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();

        Object o = getSharedContext().get(Domain.DOMAIN_THIS, FBoningOutputConstant.MO_INITIAL_SELECTION);
        if (o != null) {
            getModel().setBean(o);
        }
    }

    /**
     * Sets the fboningOutputCBS.
     * 
     * @param fboningOutputCBS fboningOutputCBS.
     */
    public void setFboningOutputCBS(final FBoningOutputCBS fboningOutputCBS) {
        this.fboningOutputCBS = fboningOutputCBS;
    }

    /**
     * {@inheritDoc}
     */
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException, UIErrorsException {

    }

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFProductionFCtrl.java,v $
 * Created on 27 févr. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction;


import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.StringHelper;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.dialogs.DialogController;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerCtrl;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerModel;
import fr.vif.jtech.ui.docviewer.pdfviewer.touch.TouchPDFViewerView;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.events.input.InputFieldEvent;
import fr.vif.jtech.ui.events.input.InputFieldListener;
import fr.vif.jtech.ui.events.input.InputFieldValidatingEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarFunctionalitiesBtnDController;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchFunctionalitiesBtnDView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchToolBarBtnStd;
import fr.vif.jtech.ui.toolbar.buttons.toolbarbutton.ToolBarButtonController;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.FunctionnalityTools;
import fr.vif.vif5_7.workshop.workstation.business.beans.dialog.debug.DDebugBean;
import fr.vif.vif5_7.workshop.workstation.ui.dialog.debug.DDebugCtrl;


/**
 * Abstract FProduction feature controller.
 * 
 * @author vr
 */
public abstract class AbstractFProductionFCtrl extends StandardFeatureController implements InputFieldListener,
TableRowChangeListener, DialogChangeListener {

    /** Cancel button reference. */
    public static final String  BTN_CANCELLAST_REFERENCE    = "BTN_CANCELLAST_REFERENCE";
    /** Detail button reference. */
    public static final String  BTN_DETAIL_REFERENCE        = ButtonReference.DETAIL_REFERENCE_BTN.getReference();
    /** Features button reference. */
    public static final String  BTN_FEATURES_REFERENCE      = "BTN_FEATURES";
    /** Finish / Unfinish button reference. */
    public static final String  BTN_FINISH_REFERENCE        = ButtonReference.MO_FINISH_BTN.getReference();         // "BTN_FINISH_REFERENCE";
    public static final String  BTN_PDF_REFERENCE           = ButtonReference.PDF_REFERENCE_BTN.getReference();

    public static final String  BTN_OPEN_CONTAINER          = ButtonReference.OPEN_CONTAINER_BTN.getReference();

    public static final String  BTN_OPEN_TATTOOED           = ButtonReference.OPEN_TATTOOED_BTN.getReference();

    /** Show all button reference. */
    public static final String  BTN_SHOW_ALL_REFERENCE      = ButtonReference.SHOW_ALL_REFERENCE_BTN.getReference();

    /** Open Incident button reference. */
    public static final String  BTN_OPEN_INCIDENT_REFERENCE = ButtonReference.OPEN_INCIDENT_REFERENCE_BTN
                                                                    .getReference();

    /** Weight button reference. */
    public static final String  BTN_WEIGHT_REFERENCE        = "BTN_WEIGHT_REFERENCE";

    /** Validation button reference. */
    public static final String  BTN_VALIDATE_REFERENCE      = "BTN_VALIDATE_REFERENCE";

    public static final String  BTN_PDF_LIST_REFERENCE      = "BTN_PDF_LIST_REFERENCE";
    public static final String  USER_VIF                    = "VIF";
    public static final String  USER_VIF_INTEG              = "VIFINTEG";

    // It 's not necessary to declare static variable in this class. It's enough to declare static variable
    // in ButtonReference
    // public static final String SETTING_INFORMATION = ButtonReference.SETTING_INFORMATION.getReference();

    /** LOGGER. */
    private static final Logger LOGGER                      = Logger.getLogger(AbstractFProductionFCtrl.class);

    private static final String FDETAIL_ENTRY_CODE          = "VIF.MOIOLI1T";

    // Button model.
    private ToolBarBtnStdModel  btnCancelLastModel;
    private ToolBarBtnStdModel  btnFinishResumeModel;
    private ToolBarBtnStdModel  btnShowAllModel;
    private ToolBarBtnStdModel  btnWeightModel;
    private ToolBarBtnStdModel  btnValidateModel;

    // Functionalities
    private ToolBarBtnStdModel  btnOpenIncidentModel;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        super.buttonAction(event);
        if (BTN_OPEN_INCIDENT_REFERENCE.equals(event.getModel().getReference())) {
            actionOpenNewIncident();
        } else if (BTN_PDF_LIST_REFERENCE.equals(event.getModel().getReference())) {
            PDFFile pdfFile = (PDFFile) ((ToolBarBtnStdModel) event.getModel()).getLinkedObject();
            viewDocumentRecette(pdfFile);
        } else if (BTN_PDF_REFERENCE.equalsIgnoreCase(event.getModel().getReference())) {
            try {

                List<PDFFile> listPdfFiles = getDocumentsToShow();
                if (listPdfFiles.isEmpty()) {
                    getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                            I18nClientManager.translate(ProductionMo.T30839));
                } else if (listPdfFiles.size() == 1) {
                    viewDocumentRecette(listPdfFiles.get(0));
                } else {
                    viewDocumentList(listPdfFiles);
                }

            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
        } else if (ButtonReference.DEBUG_REFERENCE.getReference().equals(event.getModel().getReference())) {
            DDebugBean bean = new DDebugBean();
            bean.getObjectsToShow().add(getBrowserCtrl().getModel().getBeans());
            bean.getObjectsToShow().add(getViewerController().getBean());
            bean.getObjectsToShow().add(((AbstractFProductionVCtrl) getViewerController()).getWorkBean());
            bean.getObjectsToShow().add(getBrowserController().getInitialSelection());
            DDebugCtrl.showDebug(this.getView(), this, this, this.getModel().getIdentification(), bean);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        super.initialize();
        getBrowserController().addTableRowChangeListener(this);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void inputFieldValidating(final InputFieldValidatingEvent event) throws UIVetoException {
        // Nothing to do
    }

    /**
     * Manages finish resume button.
     * 
     * @param bbean browser bean
     */
    public void manageFinishResumeButton(final FProductionBBean bbean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageFinishResumeButton(bbean=" + bbean + ")");
        }

        if (bbean != null) {
            if (ManagementType.ARCHIVED.equals(bbean.getOperationItemBean().getOperationItemKey().getManagementType())) {
                getBtnFinishResumeModel().setText(I18nClientManager.translate(ProductionMo.T29502, false));
                getBtnFinishResumeModel().setIconURL("/images/vif/vif57/production/mo/resume.png");
            } else if (ManagementType.REAL.equals(bbean.getOperationItemBean().getOperationItemKey()
                    .getManagementType())) {
                getBtnFinishResumeModel().setText(I18nClientManager.translate(ProductionMo.T29510, false));
                getBtnFinishResumeModel().setIconURL("/images/vif/vif57/production/mo/finish.png");
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageFinishResumeButton(bbean=" + bbean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanged(event=" + event + ")");
        }

        if (event.getSelectedBean() != null) {
            manageFinishResumeButton((FProductionBBean) event.getSelectedBean());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanged(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        // Nothing to do

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void textChanged(final InputFieldEvent event) {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final InputFieldEvent event) {
        // Nothing to do
    }

    /**
     * Action open new incident.
     */
    protected abstract void actionOpenNewIncident();

    /**
     * Get the browser controller.
     * 
     * @return the browser controller
     */
    protected FProductionBCtrl getBrowserCtrl() {
        return (FProductionBCtrl) getBrowserController();
    }

    /**
     * Gets the btnCancelLastModel.
     * 
     * @category getter
     * @return the btnCancelLastModel.
     */
    protected ToolBarBtnStdModel getBtnCancelLastModel() {
        if (this.btnCancelLastModel == null) {
            this.btnCancelLastModel = new ToolBarBtnStdModel();
            this.btnCancelLastModel.setReference(BTN_CANCELLAST_REFERENCE);
            this.btnCancelLastModel.setText(I18nClientManager.translate(ProductionMo.T29538, false));
            this.btnCancelLastModel.setIconURL("/images/vif/vif57/production/mo/cancel.png");
        }
        return this.btnCancelLastModel;
    }

    /**
     * Gets the btnFinishModel.
     * 
     * @category getter
     * @return the btnFinishModel.
     */
    protected ToolBarBtnStdModel getBtnFinishResumeModel() {
        if (this.btnFinishResumeModel == null) {
            this.btnFinishResumeModel = new ToolBarBtnStdModel();
            this.btnFinishResumeModel.setReference(BTN_FINISH_REFERENCE);
            this.btnFinishResumeModel.setText(I18nClientManager.translate(ProductionMo.T29510, false));
            this.btnFinishResumeModel.setIconURL("/images/vif/vif57/production/mo/finish.png");
        }
        return this.btnFinishResumeModel;
    }

    /**
     * Gets the open incident button model.
     * 
     * @return the open incident button model
     */
    protected ToolBarBtnStdModel getBtnOpenIncidentModel() {
        if (this.btnOpenIncidentModel == null) {
            this.btnOpenIncidentModel = new ToolBarBtnStdModel();
            this.btnOpenIncidentModel.setReference(BTN_OPEN_INCIDENT_REFERENCE);
            this.btnOpenIncidentModel.setIconURL("/images/vif/vif57/production/mo/img48x48/incidentAdd.png");
            this.btnOpenIncidentModel.setText(I18nClientManager.translate(ProductionMo.T32884));
            this.btnOpenIncidentModel.setFunctionality(true);
        }
        return btnOpenIncidentModel;
    }

    /**
     * Gets the btnShowAllModel.
     * 
     * @category getter
     * @return the btnShowAllModel.
     */
    protected ToolBarBtnStdModel getBtnShowAllModel() {
        if (this.btnShowAllModel == null) {
            this.btnShowAllModel = new ToolBarBtnStdModel();
            this.btnShowAllModel.setReference(BTN_SHOW_ALL_REFERENCE);
            this.btnShowAllModel.setIconURL("/images/vif/vif57/production/mo/showfinished.png");
            manageBtnShowAllModelText();
        }
        return this.btnShowAllModel;
    }

    /**
     * Gets the btnValidateModel.
     * 
     * @category getter
     * @return the btnValidateModel.
     */
    protected ToolBarBtnStdModel getBtnValidateModel() {
        if (this.btnValidateModel == null) {
            this.btnValidateModel = new ToolBarBtnStdModel();
            this.btnValidateModel.setReference(BTN_VALIDATE_REFERENCE);
            this.btnValidateModel.setText(I18nClientManager.translate(Jtech.T12758, false));
            this.btnValidateModel.setIconURL("/images/vif/vif57/production/mo/ok.png");
        }
        return this.btnValidateModel;
    }

    /**
     * Gets the btnWeightModel.
     * 
     * @category getter
     * @return the btnWeightModel.
     */
    protected ToolBarBtnStdModel getBtnWeightModel() {
        if (this.btnWeightModel == null) {
            this.btnWeightModel = new ToolBarBtnStdModel();
            this.btnWeightModel.setReference(BTN_WEIGHT_REFERENCE);
            this.btnWeightModel.setText(I18nClientManager.translate(ProductionMo.T29537, false));
            this.btnWeightModel.setIconURL("/images/vif/vif57/production/mo/scaleweight.png");
        }
        return this.btnWeightModel;
    }

    /**
     * Get documents to show.
     * 
     * @return the list of pdf files
     * @throws BusinessException if error occurs
     */
    protected abstract List<PDFFile> getDocumentsToShow() throws BusinessException;

    /**
     * Build the weight key to log the weight.
     * 
     * @param opKey the current item operation key
     * @return the key
     */
    protected String getWeightKey(final OperationItemKey opKey) {
        return StringHelper.concat(opKey.getManagementType().getValue(), ",", //
                opKey.getEstablishmentKey().getCsoc(), ",", //
                opKey.getEstablishmentKey().getCetab(), ",", //
                opKey.getChrono().getPrechro(), ",", //
                opKey.getChrono().getChrono(), ",", //
                opKey.getCounter1(), ",", //
                opKey.getCounter2(), ",", //
                opKey.getCounter3(), ",", //
                opKey.getCounter4());
    }

    /**
     * Use to manage the display of the functionalities (Mo Creation, Input Production, Output Production, ...).
     * 
     * @param screenKey TouchModelMapKey.CONTAINER_INPUT_PRODUCTION or TouchModelMapKey.CONTAINER_OUTPUT_PRODUCTION
     * @return a list of ToolBarBtnStdBaseModel.
     */
    protected List<ToolBarBtnStdBaseModel> manageActionsButtonModels(final TouchModelMapKey screenKey) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageActionsButtonModels(screenKey=" + screenKey + ")");
        }

        List<ToolBarBtnStdBaseModel> lstBtn = new ArrayList<ToolBarBtnStdBaseModel>();

        TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL);

        if (touchModel != null) {
            TouchModelFunction touchModelFunction = touchModel.getTouchModelFunction(screenKey);
            if (touchModelFunction != null) {
                lstBtn = FunctionnalityTools.manageFunctionnalities(screenKey, touchModelFunction.getFunctionalities());
                for (ToolBarBtnStdBaseModel button : lstBtn) {
                    if (button instanceof ToolBarBtnStdModel
                            && ButtonReference.MO_FINISH_BTN.getReference().equals(
                                    ((ToolBarBtnStdModel) button).getReference())) {
                        setBtnFinishResumeModel((ToolBarBtnStdModel) button);
                    }
                }
            }
        }

        if (USER_VIF.equals(getIdCtx().getLogin()) || USER_VIF_INTEG.equals(getIdCtx().getLogin())) {
            ToolBarBtnStdModel btnDebug = new ToolBarBtnStdModel();
            btnDebug.setReference(ButtonReference.DEBUG_REFERENCE.getReference());
            btnDebug.setText("Maintenance");
            btnDebug.setEnabled(true);
            btnDebug.setIconURL("");
            btnDebug.setFunctionality(true);
            lstBtn.add(btnDebug);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageActionsButtonModels(screenKey=" + screenKey + ")=" + lstBtn);
        }
        return lstBtn;
    }

    /**
     * Switch the text of the show all button.
     */
    protected void manageBtnShowAllModelText() {
        FProductionSBean sBean = (FProductionSBean) getBrowserController().getInitialSelection();
        if (!sBean.getAll()) {
            getBtnShowAllModel().setText(I18nClientManager.translate(ProductionMo.T29545, false));
            this.btnShowAllModel.setIconURL("/images/vif/vif57/production/mo/showfinished.png");
        } else {
            getBtnShowAllModel().setText(I18nClientManager.translate(ProductionMo.T29525, false));
            this.btnShowAllModel.setIconURL("/images/vif/vif57/production/mo/hidefinished.png");
        }
    }

    /**
     * 
     * View all documents from the list.
     * 
     * @param listPDFFiles list of pdf files
     */
    protected void viewDocumentList(final List<PDFFile> listPDFFiles) {
        final DisplayListener dispListener = new DisplayListener() {
            @Override
            public void displayRequested(final DisplayEvent event) {
                event.getViewToDisplay().showView();
            }
        };

        final DialogChangeListener dialogListener = new DialogChangeListener() {
            @Override
            public void dialogCancelled(final DialogChangeEvent event) {
                // Nothing to do
            }

            @Override
            public void dialogValidated(final DialogChangeEvent event) {

            }
        };

        final List<ToolBarBtnStdView> list = new ArrayList<ToolBarBtnStdView>();

        for (PDFFile pdfFile : listPDFFiles) {
            TouchToolBarBtnStd btn = new TouchToolBarBtnStd();
            ToolBarBtnStdModel model = new ToolBarBtnStdModel();
            model.setReference(BTN_PDF_LIST_REFERENCE);
            model.setLinkedObject(pdfFile);
            model.setText(pdfFile.getTitle());
            btn.setModel(model);

            list.add(btn);
            ToolBarButtonController ctrl = new ToolBarButtonController();
            ctrl.setView(btn);

            btn.setBtnCtrl(ctrl);
            btn.getBtnCtrl().addButtonListener(this);
        }

        final TouchFunctionalitiesBtnDView dView = new TouchFunctionalitiesBtnDView(
                (Frame) ((JPanel) getView()).getTopLevelAncestor(), list);
        dView.setTitle(I18nClientManager.translate(Jtech.T22109));
        try {
            DialogController controller = null;
            controller = StandardControllerFactory.getInstance().getController(
                    ToolBarFunctionalitiesBtnDController.class);

            controller.setDialogView(dView);
            controller.addDialogChangeListener(dialogListener);
            controller.addDisplayListener(dispListener);
            controller.initialize();
        } catch (final UIException e) {
            LOGGER.error("Error instanciating Dialog :", e);
        }

    }

    /**
     * 
     * view the recette document.
     * 
     * @param pdfFile the file
     */
    protected void viewDocumentRecette(final PDFFile pdfFile) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - viewDocumentRecette(pdfFile=" + pdfFile + ")");
        }

        if (pdfFile != null && !"".equals(pdfFile.getPath())) {
            viewDocument(pdfFile.getPath(), pdfFile.getTitle());
        } else {
            getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                    I18nClientManager.translate(ProductionMo.T30839));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - viewDocumentRecette(pdfFile=" + pdfFile + ")");
        }
    }

    /**
     * View Setting information.
     * 
     * @param profileId The prodile's ID
     * @param profileLabel The prodile's label
     */
    protected void viewSettingInformation(final String profileId, final String profileLabel) {
        TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL);

        StringBuilder sb = new StringBuilder();
        sb.append(I18nClientManager.translate(ProductionMo.T34822, false)).append(" : ")
        .append(touchModel.getModelCL().getCode()).append(" - ").append(touchModel.getModelCL().getLabel())
        .append(" (").append(touchModel.getParameterName()).append(") ").append("\n\n")
        .append(I18nClientManager.translate(ProductionMo.T35759, false)).append(": ").append(profileId)
        .append(" - ").append(profileLabel);
        TouchHelper.showInformation(null, I18nClientManager.translate(ProductionMo.T35724, false), sb.toString());
    }

    /**
     * Sets the btnFinishResumeModel.
     * 
     * @param btnFinishResumeModel btnFinishResumeModel.
     */
    private void setBtnFinishResumeModel(final ToolBarBtnStdModel btnFinishResumeModel) {
        this.btnFinishResumeModel = btnFinishResumeModel;
    }

    /**
     * 
     * View a document file.
     * 
     * @param url url file
     * @param title title
     */
    private void viewDocument(final String url, final String title) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - viewDocument(url=" + url + ", title=" + title + ")");
        }

        TouchPDFViewerModel model = new TouchPDFViewerModel();
        model.setTitle(title);
        model.setFilePath(url);
        TouchPDFViewerView view = new TouchPDFViewerView();
        view.setModel(model);
        TouchPDFViewerCtrl controller = new TouchPDFViewerCtrl();
        controller.setDialogView(view);
        controller.initialize();
        view.showView();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - viewDocument(url=" + url + ", title=" + title + ")");
        }
    }
}

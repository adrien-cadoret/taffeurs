/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOSimpleInternalFrame.java,v $
 * Created on 21 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.simple.swing;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.mvc.MVCTriad;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FIndicatorMOValuesVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSelectionBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOActionManager;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOVModel;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.simple.FIndicatorMOSimpleVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing.AbstractMODockableView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;


/**
 * Frame for simple indicator view.
 * 
 * @author jd
 */
public class FIndicatorMOSimpleInternalFrame extends
        AbstractMODockableView<FIndicatorMOValuesVBean, FIndicatorMOSimpleVCtrl> {
    /** LOGGER. */
    private static final Logger       LOGGER = Logger.getLogger(FIndicatorMOSimpleInternalFrame.class);

    private FIndicatorMOActionManager actionManager;

    /**
     * Constructor.
     * 
     * @param frame {@link MOLocalFrame}
     */
    @SuppressWarnings("unchecked")
    public FIndicatorMOSimpleInternalFrame(final MOLocalFrame frame) {
        super(frame, new MVCTriad(FIndicatorMOVModel.class, FIndicatorMOSimpleVView.class,
                FIndicatorMOSimpleVCtrl.class));

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        if (actionManager == null) {
            actionManager = new FIndicatorMOActionManager(getParentController());
        }
        //

        actionManager.actionPerformed(event, getLocalFrame(), this, null);

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void convert() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert()");
        }

        try {

            FIndicatorMOValuesVBean vbean = getViewerController().convert(getParentController().getIdCtx(),
                    (FSupervisorMOSelectionBean) getLocalFrame().getSelection());

            getViewerController().setBeanAndInitialBean(vbean);

            getViewerController().getView().render();
        } catch (UIException e) {
            getParentController().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert()");
        }
    }

}

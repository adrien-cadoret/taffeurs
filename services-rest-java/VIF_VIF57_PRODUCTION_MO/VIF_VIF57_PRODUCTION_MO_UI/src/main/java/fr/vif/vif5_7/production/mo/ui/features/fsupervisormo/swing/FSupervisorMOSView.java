/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOSView.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing;


import java.awt.Dialog;
import java.awt.Frame;

import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.FProcessOrdersSIView;
import fr.vif.vif5_7.production.mo.ui.features.fprocessorders.swing.FProcessOrdersSView;


/**
 * FSupervisorMO : Selection View.
 * 
 * @author jd
 */
public class FSupervisorMOSView extends FProcessOrdersSView implements FProcessOrdersSIView {

    /**
     * Constructs a new FSupervisorMOSView.
     * 
     * @param view owner's view.
     * @wbp.parser.constructor
     */
    public FSupervisorMOSView(final Dialog view) {
        super(view);
    }

    /**
     * Constructs a new FSupervisorMOSView.
     * 
     * @param view owner's view.
     */
    public FSupervisorMOSView(final Frame view) {
        super(view);
    }

    // /**
    // * {@inheritDoc}
    // */
    // @Override
    // public void enableComponents() {
    // SwingInputContainerHelper.enableComponents(this, true);
    // if (getcTeam().getInitialHelpSelection() == null) {
    // FTeamSBean selection = new FTeamSBean(true, "", ((FSupervisorMOSBean) getModel().getDialogBean())
    // .getEstablishmentKey());
    // getcTeam().setInitialHelpSelection(selection);
    // }
    // if (getcArea().getInitialHelpSelection() == null) {
    // FAreaSBean selection = new FAreaSBean(true, ((FSupervisorMOSBean) getModel().getDialogBean())
    // .getEstablishmentKey());
    // getcArea().setInitialHelpSelection(selection);
    // }
    //
    // }

}

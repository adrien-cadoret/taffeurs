/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputFView.java,v $
 * Created on 10 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist.touch;


import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.BoxLayout;
import javax.swing.JPanel;

import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputVBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFIView;


/**
 * Feature View.
 * 
 * @author xg
 */
public class FBoningDataEntryListInputFView extends
        StandardTouchFeature<FBoningDataEntryListInputBBean, FBoningDataEntryListInputVBean> implements
        FDataEntryListFIView {

    private FBoningDataEntryListInputBView browserView = null;

    private JPanel                         panel;

    private TitlePanel                     titlePanel;

    private TouchInputTextField            touchInputTextField;
    private TouchInputTextField            touchInputTextField2;

    private FBoningDataEntryListInputVView viewerView;

    /**
     * Default constructor.
     * 
     */
    public FBoningDataEntryListInputFView() {
        super();
        initialize();
    }

    /**
     * Gets the browser.
     * 
     * @return the FBoningDataEntryListInputBView
     */
    public FBoningDataEntryListInputBView getBrowser() {
        if (browserView == null) {
            browserView = new FBoningDataEntryListInputBView();
            Dimension d = new Dimension(870, 550);
            browserView.setPreferredSize(d);
            browserView.setMinimumSize(d);
            browserView.setMaximumSize(d);
            browserView.setName("browserView");
        }
        return browserView;
    }

    @Override
    public BrowserView<FBoningDataEntryListInputBBean> getBrowserView() {
        return getBrowser();
    }

    /**
     * Gets the viewer.
     * 
     * @return the FBoningDataEntryListInputVView
     */
    public FBoningDataEntryListInputVView getViewer() {
        if (viewerView == null) {
            viewerView = new FBoningDataEntryListInputVView();
            Dimension d = new Dimension(870, 60);
            viewerView.setPreferredSize(d);
            viewerView.setMinimumSize(d);
            viewerView.setMaximumSize(d);

        }
        return viewerView;
    }

    @Override
    public ViewerView<FBoningDataEntryListInputVBean> getViewerView() {
        return getViewer();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSubtitle(final String subtitle) {
        getTitlePanel().setTitle(subtitle);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSums(final String val1, final String unit1, final String val2, final String unit2) {
        if (null != unit1 && !"".equals(unit1)) {
            getTouchInputTextField().setValue(
                    I18nClientManager.translate(ProductionMo.T29523, false) + " : " + val1 + " " + unit1);
        } else {
            getTouchInputTextField().setValue("");
        }
        if (null != unit2 && !"".equals(unit2)) {
            getTouchInputTextField2().setValue(
                    I18nClientManager.translate(ProductionMo.T29523, false) + " : " + val2 + " " + unit2);
        } else {
            getTouchInputTextField2().setValue("");

        }
    }

    /**
     * Gets the panel.
     * 
     * @return a JPanel
     */
    private JPanel getPanel() {
        if (panel == null) {
            panel = new JPanel();
            panel.setLayout(new FlowLayout(FlowLayout.RIGHT));

            Dimension d = new Dimension(870, 50);
            panel.setPreferredSize(d);
            panel.setMinimumSize(d);
            panel.setMaximumSize(d);
            panel.setName("panel");
            panel.setOpaque(false);

            panel.add(getTouchInputTextField());

            panel.add(getTouchInputTextField2());

        }
        return panel;
    }

    /**
     * Gets the title panel.
     * 
     * @return the JPanel
     */
    private TitlePanel getTitlePanel() {
        if (titlePanel == null) {
            titlePanel = new TitlePanel();
            Dimension d = new Dimension(870, 40);
            titlePanel.setPreferredSize(d);
            titlePanel.setMinimumSize(d);
            titlePanel.setMaximumSize(d);
            titlePanel.setName("titlePanel");
        }
        return titlePanel;
    }

    /**
     * gets the TouchInputTextField.
     * 
     * @return a TouchInputTextField
     */
    private TouchInputTextField getTouchInputTextField() {
        if (touchInputTextField == null) {
            touchInputTextField = new TouchInputTextField(String.class, "50", null);
            touchInputTextField.setEnabled(false);
            // touchInputTextField.setAlwaysDisabled(true);
            touchInputTextField.setBeanBased(false);
            Dimension d = new Dimension(200, 50);
            touchInputTextField.setMaximumSize(d);
            touchInputTextField.setMinimumSize(d);
            touchInputTextField.setPreferredSize(d);
        }
        return touchInputTextField;
    }

    /**
     * Gets the TouchInputTextField2.
     * 
     * @return TouchInputTextField
     */
    private TouchInputTextField getTouchInputTextField2() {
        if (touchInputTextField2 == null) {
            touchInputTextField2 = new TouchInputTextField(String.class, "50", null);
            touchInputTextField2.setEnabled(false);
            // touchInputTextField2.setAlwaysDisabled(true);
            touchInputTextField2.setBeanBased(false);
            Dimension d = new Dimension(200, 50);
            touchInputTextField2.setMaximumSize(d);
            touchInputTextField2.setMinimumSize(d);
            touchInputTextField2.setPreferredSize(d);
        }
        return touchInputTextField2;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(getTitlePanel());
        add(getBrowser());
        add(getPanel());
        add(getViewer());

    }

}

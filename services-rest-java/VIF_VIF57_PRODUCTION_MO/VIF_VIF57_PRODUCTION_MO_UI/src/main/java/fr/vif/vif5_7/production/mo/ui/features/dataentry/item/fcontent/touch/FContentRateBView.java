/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FContentRateBView.java,v $
 * Created on 15 Jul 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent.touch;


import java.awt.Component;
import java.awt.Font;

import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.table.TableCellRenderer;

import fr.vif.jtech.ui.browser.touch.TouchLineBrowser;
import fr.vif.jtech.ui.laf.swing.LAFHelper;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateBBeanEnum;


/**
 * Browser view for the content features <br>
 * Mandatory to specify a browser line type.
 * 
 * @author cj
 */
public class FContentRateBView extends TouchLineBrowser<FContentRateBBean> {
    /**
     * renderer for the item code.
     */
    private class DefaultRenderer implements TableCellRenderer {
        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {
            FContentRateBBean bbean = getModel().getBeanAt(row);
            JLabel jl = new JLabel();
            Font defaultFont = TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE);
            jl.setFont(defaultFont);
            jl.setBorder(new EmptyBorder(10, 10, 10, 10));

            if (isSelected && bbean.getExpectedQtyUnit().getQty() > 0) {
                jl.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                jl.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));
            } else if (isSelected) {
                jl.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.GRAY));
            } else if (row % 2 == 0 && bbean.getExpectedQtyUnit().getQty() > 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            } else if (row % 2 == 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.GRAY));
            } else if (bbean.getExpectedQtyUnit().getQty() > 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            } else {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.GRAY));
            }

            jl.setText((String) value);
            jl.setOpaque(true);
            return jl;
        }
    }

    /**
     * renderer for the item code.
     */
    private class ItemRenderer implements TableCellRenderer {
        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {
            FContentRateBBean bbean = getModel().getBeanAt(row);
            JLabel jl = new JLabel();
            Font defaultFont = TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE);
            jl.setFont(defaultFont);

            if (isSelected && bbean.getExpectedQtyUnit().getQty() > 0) {
                jl.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                jl.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));
            } else if (isSelected) {
                jl.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.GRAY));
            } else if (row % 2 == 0 && bbean.getExpectedQtyUnit().getQty() > 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            } else if (row % 2 == 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.GRAY));
            } else if (bbean.getExpectedQtyUnit().getQty() > 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            } else {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.GRAY));
            }

            if (bbean.getItemId().equals(bbean.getItemIdExpected())) {
                jl.setText(" * " + bbean.getItemId());
            } else {
                jl.setText("   " + bbean.getItemId());
            }
            jl.setOpaque(true);
            return jl;
        }
    }

    /**
     * renderer for the item code.
     */
    private class QtyRenderer implements TableCellRenderer {
        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {
            FContentRateBBean bbean = getModel().getBeanAt(row);
            JLabel jl = new JLabel();
            Font defaultFont = TouchHelper.getFont(StandardFont.TOUCHSCREEN_UNSELECTED_BROWSE_LINE);
            jl.setFont(defaultFont);
            jl.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
            jl.setBorder(new EmptyBorder(10, 10, 10, 10));

            if (isSelected && bbean.getExpectedQtyUnit().getQty() > 0) {
                jl.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                jl.setForeground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_FG_SELECTED_BROWSE_LINE_COLOR_KEY));
            } else if (isSelected) {
                jl.setBackground(UIManager.getColor(LAFHelper.LAF_TOUCHSCREEN_BG_SELECTED_BROWSE_LINE_COLOR_KEY));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.GRAY));
            } else if (row % 2 == 0 && bbean.getExpectedQtyUnit().getQty() > 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            } else if (row % 2 == 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.GRAY));
            } else if (bbean.getExpectedQtyUnit().getQty() > 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_FG_BROWSER));
            } else {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
                jl.setForeground(TouchHelper.getAwtColor(StandardColor.GRAY));
            }

            jl.setText((String) value);
            jl.setOpaque(true);
            return jl;
        }
    }

    /**
     * Default constructor.
     * 
     */
    public FContentRateBView() {
        super();
    }

    /**
     * Initialize the special renderer for each column.
     */
    public void initialize() {
        for (int i = 0; i < getModel().getColumnCount(); i++) {
            if (FContentRateBBeanEnum.ITEM_ID.getValue().equals(getModel().getBeanAttributeAt(i))) {
                getJTable().getColumnModel().getColumn(i).setCellRenderer(new ItemRenderer());
            } else if (FContentRateBBeanEnum.FORMAT_EXPECTED_QTY_UNIT.getValue().equals(
                    getModel().getBeanAttributeAt(i))) {
                getJTable().getColumnModel().getColumn(i).setCellRenderer(new QtyRenderer());
            } else if (FContentRateBBeanEnum.EXPECTED_RATE.getValue().equals(getModel().getBeanAttributeAt(i))) {
                getJTable().getColumnModel().getColumn(i).setCellRenderer(new QtyRenderer());
            } else {
                getJTable().getColumnModel().getColumn(i).setCellRenderer(new DefaultRenderer());
            }
        }
    }
}

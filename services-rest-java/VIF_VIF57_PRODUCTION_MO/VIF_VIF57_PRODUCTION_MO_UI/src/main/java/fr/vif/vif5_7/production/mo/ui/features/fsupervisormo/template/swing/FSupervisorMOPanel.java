/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOPanel.java,v $
 * Created on 23 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing;


import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.generic.GenericActionListener;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.FSupervisorMOFCtrl;


/**
 * FSupervisor panel.
 * 
 * @author jd
 */
public class FSupervisorMOPanel extends JPanel {

    private List<GenericActionListener> actionListeners   = new ArrayList<GenericActionListener>();

    private FSupervisorMOFCtrl          featureController = null;

    /**
     * Constructor.
     */
    public FSupervisorMOPanel() {
        super();
        // setUpdateable(false);
    }

    /**
     * add a generic action listener.
     * 
     * @param listener listener
     */
    public void addGenericActionListener(final GenericActionListener listener) {
        actionListeners.add(listener);
    }

    /**
     * fire a generic action.
     * 
     * @param event event
     */
    public void fireGenericAction(final GenericActionEvent event) {
        for (GenericActionListener l : actionListeners) {
            l.actionPerformed(event);
        }
    }

    /**
     * Gets the featureController.
     * 
     * @return the featureController.
     */
    public FSupervisorMOFCtrl getFeatureController() {
        return featureController;
    }

    /**
     * Sets the featureController.
     * 
     * @param featureController featureController.
     */
    public void setFeatureController(final FSupervisorMOFCtrl featureController) {
        this.featureController = featureController;
    }

}

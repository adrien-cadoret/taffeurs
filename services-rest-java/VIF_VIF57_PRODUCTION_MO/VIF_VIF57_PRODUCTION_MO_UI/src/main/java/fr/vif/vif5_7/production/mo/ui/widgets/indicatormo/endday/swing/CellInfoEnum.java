/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: CellInfoEnum.java,v $
 * Created on 22 Avril 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.swing;


/**
 * CellInfo enumeration.
 *
 * @author ac
 */
public enum CellInfoEnum {

    WORKSTATION("WORKSTATION"), CHRONO("CHRONO"), TYPE("TYPE"), LINE("LINE"), TYPACTA("TYPACTA"), CART("CART"), BEGIN_DATE(
            "BEGIN_DATE"), CHRONO_MO("CHRONO_MO"), CHRONO_MEASURE("CHRONO_MEASURE"), CREATION_DATE("CREATION_DATE"), DECLARED(
            "DECLARED"), MO_LABEL("MO_LABEL"), REASON("REASON");

    /**
     * The value.
     */
    private final String value;

    /**
     * Constructor.
     * 
     * @param value the value
     */
    private CellInfoEnum(final String value) {
        this.value = value;
    }

    /**
     * Gets the value.
     * 
     * @category getter
     * @return the value.
     */
    public String getValue() {
        String valueRet = value;
        return valueRet;
    }

}

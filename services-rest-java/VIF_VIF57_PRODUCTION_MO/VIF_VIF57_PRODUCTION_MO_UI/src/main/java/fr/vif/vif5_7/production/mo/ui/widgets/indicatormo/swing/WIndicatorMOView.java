/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: WIndicatorMOView.java,v $
 * Created on 03 avril 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.swing;


import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JPanel;

import com.jidesoft.dashboard.Gadget;

import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.widget.DashboardActionEvent;
import fr.vif.jtech.ui.widget.WidgetActionEvent;
import fr.vif.jtech.ui.widget.swing.SwingIndicatorWidgetView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.GraphMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.WIndicatorMOIView;


/**
 * IndicatorMO View.
 * 
 * @author ag
 */
public class WIndicatorMOView extends SwingIndicatorWidgetView implements WIndicatorMOIView {

    // List of indicators associated to the widget
    private List<CodeLabel> indicators = new ArrayList<CodeLabel>();

    // Indicator selected in the widget
    private String          uniqueId   = "SECT_10_1";

    // Widget Id
    private String          widgetId   = null;

    private MOLocalFrame    frame      = null;

    private GraphMO         graph      = null;

    /**
     * Constructor with parameter.
     * 
     * @param gadget Gadget
     */
    public WIndicatorMOView(final Gadget gadget) {
        super(gadget);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dashboardActionPerformed(final DashboardActionEvent event) {
        super.dashboardActionPerformed(event);
    }

    /**
     * Gets the frame.
     * 
     * @return the frame.
     */
    public MOLocalFrame getFrame() {
        return frame;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Gadget getGadget() {
        return super.getGadget();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GraphMO getGraph() {
        return graph;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CodeLabel> getIndicators() {
        return indicators;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, String> getSettings() {
        Map<String, String> map = super.getSettings();
        map.put("uniqueId", uniqueId);
        return map;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getWidgetId() {
        return this.getGadget().getKey();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void refresh() {

        if (frame != null) {

            JPanel panel = GraphMOTools.createGraphPanel(graph, new GraphMOOptions());

            getContentPane().removeAll();
            getContentPane().add(panel);
            validate();
            // } else {
            // showError(I18nClientManager.translate(Generic.T1401, false, new VarParamTranslation(uniqueId)));
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFrame(final MOLocalFrame frame) {
        this.frame = frame;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setGraph(final GraphMO graph) {
        this.graph = graph;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setIndicators(final List<CodeLabel> indicators) {
        this.indicators = indicators;
        // if (indicators != null && indicators.size() > 0) {
        // this.uniqueId = indicators.get(0).getCode();
        // }
    }

    /**
     * 
     * Define Indicators Menu.
     * 
     * @param listener ActionListener
     */
    public void setIndicatorsMenu(final ActionListener listener) {
        JMenu menu = new JMenu(I18nClientManager.translate(ProductionMo.T33079, false));
        for (CodeLabel item : indicators) {
            JMenuItem subMenuItem = new JMenuItem(item.getLabel()); // FIXME
            // JMenuItem subMenuItem = new JMenuItem(item.getLabel() + "(" + item.getCode() + ")");
            subMenuItem.setActionCommand(item.getCode());
            subMenuItem.addActionListener(listener);
            menu.add(subMenuItem);
        }

        this.addMenuInToolPopup(menu);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setSettings(final Map<String, String> settings) {
        super.setSettings(settings);
        if (!"".equals(settings.get("uniqueId"))) {
            this.setUniqueId(settings.get("uniqueId"));
        }

        // refresh();
        WidgetActionEvent action = new WidgetActionEvent(this, WidgetActionEvent.GENERIC_EVENT_PERFORMED);
        action.setEventId(uniqueId);
        action.setEventParameter(uniqueId);
        fireWidgetActionPerformed(action);

        validate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setUniqueId(final String uniqueId) {
        this.uniqueId = uniqueId;
    }

    @Override
    public void validate() {
        super.validate();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void initialize() {
        super.initialize();
        // getContentPane().setLayout(new GridBagLayout());

        // final CollapsiblePaneTitleButton refreshButton = new CollapsiblePaneTitleButton(this, new
        // ImageIcon(getClass()
        // .getResource("/images/jtech/widget/widget_maximize.png")));
        //
        // refreshButton.addActionListener(new ActionListener() {
        //
        // @Override
        // public void actionPerformed(final ActionEvent e) {
        // // setSettings();
        //
        // }
        // });

        // addButton(refreshButton, 1);

        // JMenu mySubMenu = new JMenu("Autres indicateurs");
        // // getGadget().getGadgetManager().
        // JMenuItem subMenuItem1 = new JMenuItem("GEN_2_60");
        // subMenuItem1.setActionCommand("GEN_2_60");
        // subMenuItem1.addActionListener(this);
        // mySubMenu.add(subMenuItem1);
        // JMenuItem subMenuItem2 = new JMenuItem("GEN_2_61");
        // mySubMenu.add(subMenuItem2);
        // JMenuItem subMenuItem3 = new JMenuItem("GEN_2_62");
        // mySubMenu.add(subMenuItem3);
        // JMenuItem subMenuItem4 = new JMenuItem("GEN_2_63");
        // mySubMenu.add(subMenuItem4);
        // this.addMenuInToolPopup(mySubMenu);

        validate();
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffFModel.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;


/**
 * Labor staff feature model.
 * 
 * @author nle
 */
public class FLaborStaffFModel extends StandardFeatureModel {
    /**
     * Default constructor.
     * 
     */
    public FLaborStaffFModel() {
        super();
        this.setBrowserTriad(new BrowserMVCTriad(FLaborStaffBModel.class, null, FLaborStaffBCtrl.class));
        this.setViewerTriad(new ViewerMVCTriad(FLaborStaffVModel.class, null, FLaborStaffVCtrl.class));
    }

}

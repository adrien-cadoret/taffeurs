/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListFIView.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


/**
 * DataEntryList : Feature View Interface.
 * 
 * @author vr
 */
public interface FDataEntryListFIView {

    /**
     * 
     * set the title.
     * 
     * @param subtitle title.
     */
    public void setSubtitle(final String subtitle);

    /**
     * set the sums values.
     * 
     * @param val1 the first sum corresponding with first unit.
     * @param unit1 the first unit
     * @param val2 the second sum corresponding with second unit
     * @param unit2 the second unit
     */
    public void setSums(String val1, String unit1, String val2, String unit2);
}

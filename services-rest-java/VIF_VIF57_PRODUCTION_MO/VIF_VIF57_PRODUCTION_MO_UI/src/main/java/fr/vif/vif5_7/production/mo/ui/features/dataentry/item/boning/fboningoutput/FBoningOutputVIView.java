/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputVIView.java,v $
 * Created on 19 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationBBean;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationSBean;


/**
 * ProductionInputContainer : Viewer View Interface.
 * 
 * @author vr
 */
public interface FBoningOutputVIView {

    /**
     * Open a location help.
     * 
     * @param parent parent
     * @param sBean selection bean
     * @return a FLocationBBean
     */
    public FLocationBBean openLocationHelp(final DisplayListener parent, final FLocationSBean sBean);

}

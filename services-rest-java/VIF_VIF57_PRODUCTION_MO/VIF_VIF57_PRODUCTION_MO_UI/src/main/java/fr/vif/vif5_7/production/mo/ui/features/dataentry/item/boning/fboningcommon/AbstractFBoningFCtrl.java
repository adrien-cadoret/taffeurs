/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractFBoningFCtrl.java,v $
 * Created on 23 avr. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningcommon;


import java.awt.Frame;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JPanel;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.dialogs.DialogController;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerCtrl;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerModel;
import fr.vif.jtech.ui.docviewer.pdfviewer.touch.TouchPDFViewerView;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.events.input.InputFieldEvent;
import fr.vif.jtech.ui.events.input.InputFieldListener;
import fr.vif.jtech.ui.events.input.InputFieldValidatingEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarFunctionalitiesBtnDController;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchFunctionalitiesBtnDView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchToolBarBtnStd;
import fr.vif.jtech.ui.toolbar.buttons.toolbarbutton.ToolBarButtonController;
import fr.vif.jtech.ui.util.Key;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.FBoningInputVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput.FBoningOutputVCtrl;
import fr.vif.vif5_7.workshop.workstation.business.beans.dialog.debug.DDebugBean;
import fr.vif.vif5_7.workshop.workstation.ui.dialog.debug.DDebugCtrl;


/**
 * Abstract feature controller for boning input and output.
 * 
 * @author cj
 */
public abstract class AbstractFBoningFCtrl extends StandardFeatureController implements InputFieldListener,
TableRowChangeListener, DialogChangeListener {
    /** Cancel button reference. */
    public static final String     BTN_CANCELLAST_REFERENCE    = "BTN_CANCELLAST_REFERENCE";
    /** Detail button reference. */
    public static final String     BTN_DETAIL_REFERENCE        = "BTN_DETAIL_REFERENCE";
    /** Features button reference. */
    public static final String     BTN_FEATURES_REFERENCE      = "BTN_FEATURES";
    /** Finish / Unfinish button reference. */
    public static final String     BTN_FINISH_REFERENCE        = "BTN_FINISH_REFERENCE";
    public static final String     BTN_FINISH_ALL_REFERENCE    = "BTN_FINISH_ALL_REFERENCE";

    public static final String     BTN_PDF_REFERENCE           = "BTN_PDF_REFERENCE";

    /** Show all button reference. */
    public static final String     BTN_SHOW_ALL_REFERENCE      = "BTN_SHOW_ALL_REFERENCE";

    /** Open Incident button reference. */
    public static final String     BTN_OPEN_INCIDENT_REFERENCE = "OPEN_INDICENT_REF";

    /** Open label button reference. */
    public static final String     BTN_OPEN_LABEL_REFERENCE    = "BTN_OPEN_LABEL_REF";

    /** Open number button reference. */
    public static final String     BTN_OPEN_NUMBER_REFERENCE   = "BTN_OPEN_NUMBER_REF";

    /** Weight button reference. */
    public static final String     BTN_WEIGHT_REFERENCE        = "BTN_WEIGHT_REFERENCE";
    /** Weight button reference. */
    public static final String     BTN_WEIGHT_MODE_REFERENCE   = "BTN_WEIGHT_MODE_REFERENCE";

    /** Validation button reference. */
    public static final String     BTN_VALIDATE_REFERENCE      = "BTN_VALIDATE_REFERENCE";

    public static final String     BTN_PDF_LIST_REFERENCE      = "BTN_PDF_LIST_REFERENCE";

    public static final String     USER_VIF                    = "VIF";
    public static final String     USER_VIF_INTEG              = "VIFINTEG";
    private static final Logger    LOGGER                      = Logger.getLogger(AbstractFBoningFCtrl.class);

    private static final String    FDETAIL_ENTRY_CODE          = "VIF.BONDETMVT";
    private static final String    FLABEL_ENTRY_CODE           = "VIF.LABLST";

    private ToolBarBtnFeatureModel btnLabelModel;
    private ToolBarBtnStdModel     btnNumberModel;
    private ToolBarBtnStdModel     btnCancelLastModel;

    private ToolBarBtnFeatureModel btnDetailModel;

    private ToolBarBtnStdModel     btnFeaturesModel;

    private ToolBarBtnStdModel     btnFinishResumeModel;
    private ToolBarBtnStdModel     btnFinishResumeAllModel;

    // Button model.
    private ToolBarBtnStdModel     btnFProductionInputModel;
    private ToolBarBtnStdModel     btnPDFModel;

    private ToolBarBtnStdModel     btnShowAllModel;
    private ToolBarBtnStdModel     btnUnfinishModel;
    private ToolBarBtnStdModel     btnWeightModel;
    private ToolBarBtnStdModel     btnWeightModeModel;
    // Functionalities
    private ToolBarBtnStdModel     btnOpenIncidentModel;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        super.buttonAction(event);
        if (BTN_OPEN_INCIDENT_REFERENCE.equals(event.getModel().getReference())) {
            actionOpenNewIncident();
        } else if (BTN_PDF_LIST_REFERENCE.equals(event.getModel().getReference())) {
            PDFFile pdfFile = (PDFFile) ((ToolBarBtnStdModel) event.getModel()).getLinkedObject();
            viewDocumentRecette(pdfFile);
        } else if (ButtonReference.DEBUG_REFERENCE.getReference().equals(event.getModel().getReference())) {
            DDebugBean bean = new DDebugBean();
            bean.getObjectsToShow().add(getBrowserController().getModel().getBeans());
            bean.getObjectsToShow().add(getViewerController().getBean());
            bean.getObjectsToShow().add(getBrowserController().getInitialSelection());
            if (getViewerController() instanceof FBoningOutputVCtrl) {
                bean.getObjectsToShow().add(((FBoningOutputVCtrl) getViewerController()).getWorkBean());
            } else if (getViewerController() instanceof FBoningInputVCtrl) {
                bean.getObjectsToShow().add(((FBoningInputVCtrl) getViewerController()).getWorkBean());
            }
            DDebugCtrl.showDebug(this.getView(), this, this, this.getModel().getIdentification(), bean);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();
        getBrowserController().addTableRowChangeListener(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void inputFieldValidating(final InputFieldValidatingEvent event) throws UIVetoException {
        // Nothing to do
    }

    /**
     * Manages finish resume button.
     * 
     * @param bbean browser bean
     */
    public abstract void manageFinishResumeButton(final Object bbean);

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        if (event.getSelectedBean() != null) {
            manageFinishResumeButton(event.getSelectedBean());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        // Nothing to do

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void textChanged(final InputFieldEvent event) {
        // Nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final InputFieldEvent event) {
        // Nothing to do
    }

    /**
     * Action open new incident.
     */
    protected abstract void actionOpenNewIncident();

    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        List<ToolBarBtnStdBaseModel> list = new ArrayList<ToolBarBtnStdBaseModel>();
        list.add(getBtnPDFModel()); // Process
        list.add(getBtnOpenIncidentModel()); // Deviation Declaration

        if (USER_VIF.equals(getIdCtx().getLogin()) || USER_VIF_INTEG.equals(getIdCtx().getLogin())) {
            ToolBarBtnStdModel btnDebug = new ToolBarBtnStdModel();
            btnDebug.setReference(ButtonReference.DEBUG_REFERENCE.getReference());
            btnDebug.setText("Maintenance");
            btnDebug.setEnabled(true);
            btnDebug.setIconURL("");
            btnDebug.setFunctionality(true);
            list.add(btnDebug);
        }
        return list;
    }

    /**
     * Gets the btnCancelLastModel.
     * 
     * @category getter
     * @return the btnCancelLastModel.
     */
    protected ToolBarBtnStdModel getBtnCancelLastModel() {
        if (this.btnCancelLastModel == null) {
            this.btnCancelLastModel = new ToolBarBtnStdModel();
            this.btnCancelLastModel.setReference(BTN_CANCELLAST_REFERENCE);
            this.btnCancelLastModel.setText(I18nClientManager.translate(ProductionMo.T29538, false));
            this.btnCancelLastModel.setIconURL("/images/vif/vif57/production/mo/cancel.png");
            this.btnCancelLastModel.setShortcut(Key.K_F7);
        }
        return this.btnCancelLastModel;
    }

    /**
     * Gets the btnFinishModel.
     * 
     * @category getter
     * @return the btnFinishModel.
     */
    protected ToolBarBtnStdModel getBtnFinishResumeAllModel() {
        if (this.btnFinishResumeAllModel == null) {
            this.btnFinishResumeAllModel = new ToolBarBtnStdModel();
            this.btnFinishResumeAllModel.setReference(BTN_FINISH_ALL_REFERENCE);
            this.btnFinishResumeAllModel.setText(I18nClientManager.translate(ProductionMo.T34206, false));
            this.btnFinishResumeAllModel.setIconURL("/images/vif/vif57/production/mo/finish.png");
        }
        return this.btnFinishResumeAllModel;
    }

    /**
     * Gets the btnFinishModel.
     * 
     * @category getter
     * @return the btnFinishModel.
     */
    protected ToolBarBtnStdModel getBtnFinishResumeModel() {
        if (this.btnFinishResumeModel == null) {
            this.btnFinishResumeModel = new ToolBarBtnStdModel();
            this.btnFinishResumeModel.setReference(BTN_FINISH_REFERENCE);
            this.btnFinishResumeModel.setText(I18nClientManager.translate(ProductionMo.T29510, false));
            this.btnFinishResumeModel.setIconURL("/images/vif/vif57/production/mo/finish.png");
        }
        return this.btnFinishResumeModel;
    }

    /**
     * Gets the btnLabelModel.
     * 
     * @return the btnLabelModel.
     */
    protected ToolBarBtnFeatureModel getBtnLabelModel() {
        if (this.btnLabelModel == null) {
            this.btnLabelModel = new ToolBarBtnFeatureModel();
            this.btnLabelModel.setReference(BTN_OPEN_LABEL_REFERENCE);
            this.btnLabelModel.setFeatureId(FLABEL_ENTRY_CODE);
            this.btnLabelModel.setIconURL("/images/vif/vif57/production/mo/img48x48/Etiquette.png");
            this.btnLabelModel.setText(I18nClientManager.translate(ProductionMo.T34131, false));
        }
        return btnLabelModel;
    }

    /**
     * Gets the btnNumberModel.
     * 
     * @return the btnNumberModel.
     */
    protected ToolBarBtnStdModel getBtnNumberModel() {
        if (this.btnNumberModel == null) {
            this.btnNumberModel = new ToolBarBtnStdModel();
            this.btnNumberModel.setReference(BTN_OPEN_NUMBER_REFERENCE);
            this.btnNumberModel.setIconURL("/images/vif/vif57/production/mo/img48x48/Nombre.png");
            this.btnNumberModel.setText(I18nClientManager.translate(ProductionMo.T34132, false));
        }
        return btnNumberModel;
    }

    /**
     * Gets the open incident button model.
     * 
     * @return the open incident button model
     */
    protected ToolBarBtnStdModel getBtnOpenIncidentModel() {
        if (this.btnOpenIncidentModel == null) {
            this.btnOpenIncidentModel = new ToolBarBtnStdModel();
            this.btnOpenIncidentModel.setReference(BTN_OPEN_INCIDENT_REFERENCE);
            this.btnOpenIncidentModel.setIconURL("/images/vif/vif57/production/mo/img48x48/incidentAdd.png");
            this.btnOpenIncidentModel.setText(I18nClientManager.translate(ProductionMo.T32884));
            this.btnOpenIncidentModel.setFunctionality(true);
        }
        return btnOpenIncidentModel;
    }

    /**
     * Gets the btnPDFModel.
     * 
     * @category getter
     * @return the btnPDFModel.
     */
    protected ToolBarBtnStdModel getBtnPDFModel() {
        if (this.btnPDFModel == null) {
            this.btnPDFModel = new ToolBarBtnStdModel();
            this.btnPDFModel.setReference(BTN_PDF_REFERENCE);
            this.btnPDFModel.setText(I18nClientManager.translate(ProductionMo.T30838));
            this.btnPDFModel.setIconURL("/images/vif/vif57/production/mo/img48x48/process.png");
            this.btnPDFModel.setFunctionality(true);
        }
        return this.btnPDFModel;
    }

    /**
     * Gets the btnShowAllModel.
     * 
     * @category getter
     * @return the btnShowAllModel.
     */
    protected ToolBarBtnStdModel getBtnShowAllModel() {
        if (this.btnShowAllModel == null) {
            this.btnShowAllModel = new ToolBarBtnStdModel();
            this.btnShowAllModel.setReference(BTN_SHOW_ALL_REFERENCE);
            this.btnShowAllModel.setIconURL("/images/vif/vif57/production/mo/showfinished.png");
            this.btnShowAllModel.setShortcut(Key.K_F8);
            manageBtnShowAllModelText();
        }
        return this.btnShowAllModel;
    }

    /**
     * Gets the btnWeightModel.
     * 
     * @category getter
     * @return the btnWeightModel.
     */
    protected ToolBarBtnStdModel getBtnWeightModel() {
        if (this.btnWeightModel == null) {
            this.btnWeightModel = new ToolBarBtnStdModel();
            this.btnWeightModel.setReference(BTN_WEIGHT_REFERENCE);
            this.btnWeightModel.setText(I18nClientManager.translate(ProductionMo.T29537, false));
            this.btnWeightModel.setIconURL("/images/vif/vif57/production/mo/scaleweight.png");
            this.btnWeightModel.setShortcut(Key.K_F2);
        }
        return this.btnWeightModel;
    }

    /**
     * Gets the btnWeightOnOffModel.
     * 
     * @category getter
     * @return the btnWeightOnOffModel.
     */
    protected ToolBarBtnStdModel getBtnWeightModeModel() {
        if (this.btnWeightModeModel == null) {
            this.btnWeightModeModel = new ToolBarBtnStdModel();
            this.btnWeightModeModel.setReference(BTN_WEIGHT_MODE_REFERENCE);
            this.btnWeightModeModel.setText(I18nClientManager.translate(ProductionMo.T34222, false));
            this.btnWeightModeModel.setIconURL("/images/vif/vif57/production/mo/scaleweight.png");
            this.btnWeightModeModel.setSelected(false);
            this.btnWeightModeModel.setToggleType(true);
        }
        return this.btnWeightModeModel;
    }

    /**
     * Switch the text of the show all button.
     */
    protected abstract void manageBtnShowAllModelText();

    /**
     * View document list.
     * 
     * @param listPDFFiles the list of pdf files
     */
    protected void viewDocumentList(final List<PDFFile> listPDFFiles) {
        final DisplayListener dispListener = new DisplayListener() {
            @Override
            public void displayRequested(final DisplayEvent event) {
                event.getViewToDisplay().showView();
            }
        };

        final DialogChangeListener dialogListener = new DialogChangeListener() {
            @Override
            public void dialogCancelled(final DialogChangeEvent event) {
                // Nothing to do
            }

            @Override
            public void dialogValidated(final DialogChangeEvent event) {

            }
        };

        final List<ToolBarBtnStdView> list = new ArrayList<ToolBarBtnStdView>();

        for (PDFFile pdfFile : listPDFFiles) {
            TouchToolBarBtnStd btn = new TouchToolBarBtnStd();
            ToolBarBtnStdModel model = new ToolBarBtnStdModel();
            model.setReference(BTN_PDF_LIST_REFERENCE);
            model.setLinkedObject(pdfFile);
            model.setText(pdfFile.getTitle());
            btn.setModel(model);

            list.add(btn);
            ToolBarButtonController ctrl = new ToolBarButtonController();
            ctrl.setView(btn);

            btn.setBtnCtrl(ctrl);
            btn.getBtnCtrl().addButtonListener(this);
        }

        final TouchFunctionalitiesBtnDView dView = new TouchFunctionalitiesBtnDView(
                (Frame) ((JPanel) getView()).getTopLevelAncestor(), list);
        dView.setTitle(I18nClientManager.translate(Jtech.T22109));
        try {
            DialogController controller = null;
            controller = StandardControllerFactory.getInstance().getController(
                    ToolBarFunctionalitiesBtnDController.class);

            controller.setDialogView(dView);
            controller.addDialogChangeListener(dialogListener);
            controller.addDisplayListener(dispListener);
            controller.initialize();
        } catch (final UIException e) {
            LOGGER.error("Error instanciating Dialog :", e);
        }

    }

    /**
     * 
     * view the recette document.
     * 
     * @param pdfFile the PDF file
     */
    protected void viewDocumentRecette(final PDFFile pdfFile) {
        if (pdfFile != null && !"".equals(pdfFile.getPath())) {
            viewDocument(pdfFile.getPath(), pdfFile.getTitle());
        } else {
            getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                    I18nClientManager.translate(ProductionMo.T30839));
        }
    }

    /**
     * 
     * View a document file.
     * 
     * @param url url file
     * @param title title
     */
    private void viewDocument(final String url, final String title) {
        TouchPDFViewerModel model = new TouchPDFViewerModel();
        model.setTitle(title);
        model.setFilePath(url);
        TouchPDFViewerView view = new TouchPDFViewerView();
        view.setModel(model);
        TouchPDFViewerCtrl controller = new TouchPDFViewerCtrl();
        controller.setDialogView(view);
        controller.initialize();
        view.showView();
    }
}

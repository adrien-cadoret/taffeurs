/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: MOSaveView.java,v $
 * Created on 23 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


/**
 * Contains information to create a new frame.
 * 
 * @author jd
 */
public class MOSaveView {

    /**
     * The frame is visible ?
     */
    private boolean isVisible   = false;

    /**
     * Title Frame with reference.
     */
    private String  titleFrame  = null;

    /**
     * Title Frame unstranslatable.
     */
    private String  titleFrameU = null;

    /**
     * Title Graph with reference.
     */
    private String  titleGraph  = null;

    /**
     * Title Graph unstranslatable.
     */
    private String  titleGraphU = null;

    /**
     * Title Menu with reference.
     */
    private String  titleMenu   = null;

    /**
     * Title Menu unstranslatable.
     */
    private String  titleMenuU  = null;

    /**
     * Unique id of the frame. Must be constant.
     */
    private String  uniqueId    = "";

    /**
     * Constructor.
     */
    public MOSaveView() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param uniqueId String
     */
    public MOSaveView(final String uniqueId) {
        super();
        this.uniqueId = uniqueId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        MOSaveView other = (MOSaveView) obj;
        if (uniqueId == null) {
            if (other.uniqueId != null) {
                return false;
            }
        } else if (!uniqueId.equals(other.uniqueId)) {
            return false;
        }
        return true;
    }

    /**
     * Gets the isVisible.
     * 
     * @return the isVisible.
     */
    public boolean getIsVisible() {
        return isVisible;
    }

    /**
     * Gets the titleFrame.
     * 
     * @return the titleFrame.
     */
    public String getTitleFrame() {
        return titleFrame;
    }

    /**
     * Gets the titleFrameU.
     * 
     * @return the titleFrameU.
     */
    public String getTitleFrameU() {
        return titleFrameU;
    }

    /**
     * Gets the titleGraph.
     * 
     * @return the titleGraph.
     */
    public String getTitleGraph() {
        return titleGraph;
    }

    /**
     * Gets the titleGraphU.
     * 
     * @return the titleGraphU.
     */
    public String getTitleGraphU() {
        return titleGraphU;
    }

    /**
     * Gets the titleMenu.
     * 
     * @return the titleMenu.
     */
    public String getTitleMenu() {
        return titleMenu;
    }

    /**
     * Gets the titleMenuU.
     * 
     * @return the titleMenuU.
     */
    public String getTitleMenuU() {
        return titleMenuU;
    }

    /**
     * Gets the uniqueId.
     * 
     * @return the uniqueId.
     */
    public String getUniqueId() {
        return uniqueId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        return super.hashCode();
    }

    /**
     * Sets the isVisible.
     * 
     * @param isVisible isVisible.
     */
    public void setIsVisible(final boolean isVisible) {
        this.isVisible = isVisible;
    }

    /**
     * Sets the titleFrame.
     * 
     * @param titleFrame titleFrame.
     */
    public void setTitleFrame(final String titleFrame) {
        this.titleFrame = titleFrame;
    }

    /**
     * Sets the titleFrameU.
     * 
     * @param titleFrameU titleFrameU.
     */
    public void setTitleFrameU(final String titleFrameU) {
        this.titleFrameU = titleFrameU;
    }

    /**
     * Sets the titleGraph.
     * 
     * @param titleGraph titleGraph.
     */
    public void setTitleGraph(final String titleGraph) {
        this.titleGraph = titleGraph;
    }

    /**
     * Sets the titleGraphU.
     * 
     * @param titleGraphU titleGraphU.
     */
    public void setTitleGraphU(final String titleGraphU) {
        this.titleGraphU = titleGraphU;
    }

    /**
     * Sets the titleMenu.
     * 
     * @param titleMenu titleMenu.
     */
    public void setTitleMenu(final String titleMenu) {
        this.titleMenu = titleMenu;
    }

    /**
     * Sets the titleMenuU.
     * 
     * @param titleMenuU titleMenuU.
     */
    public void setTitleMenuU(final String titleMenuU) {
        this.titleMenuU = titleMenuU;
    }

    /**
     * Sets the uniqueId.
     * 
     * @param uniqueId uniqueId.
     */
    public void setUniqueId(final String uniqueId) {
        this.uniqueId = uniqueId;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "SaveView [uniqueId=" + uniqueId + ", isVisible=" + isVisible + ", titleFrame=" + titleFrame + "]";
    }

}

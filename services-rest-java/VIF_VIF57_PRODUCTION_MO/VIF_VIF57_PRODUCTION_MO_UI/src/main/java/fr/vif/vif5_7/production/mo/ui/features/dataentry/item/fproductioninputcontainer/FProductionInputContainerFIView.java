/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputContainerFIView.java,v $
 * Created on 19 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer;


import fr.vif.vif5_7.workshop.device.ui.composites.devices.scale.touch.CPerpetualScaleWeightTouchView;


/**
 * ProductionInputContainer : Feature View Interface.
 * 
 * @author vr
 */
public interface FProductionInputContainerFIView {

    /**
     * Get scale weight component.
     * 
     * @return scale weights component
     */
    public CPerpetualScaleWeightTouchView getScaleView();

    /**
     * Set the subtitle text.
     * 
     * @param subTitle the subtitle to set
     */
    public void setSubTitle(final String subTitle);

}

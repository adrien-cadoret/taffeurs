/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputCompVView.java,v $
 * Created on 17 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcomp.touch;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import fr.vif.jtech.ui.input.touch.TouchInputBarCode;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.gen.comment.ui.composites.simplecomment.touch.CCommentTouchView;
import fr.vif.vif5_7.gen.service.ui.composites.cquantityunit.touch.CQuantityUnitView;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcomp.FProductionInputCompVBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.composites.barcode.BarCodeCtrl;
import fr.vif.vif5_7.production.mo.ui.composites.csubstituteitem.touch.CSubstituteItemView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVIView;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityEnum;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingBBeanEnum;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingSBean;
import fr.vif.vif5_7.stock.kernel.ui.features.fdestocking.FDestockingBCtrl;
import fr.vif.vif5_7.stock.kernel.ui.features.fdestocking.FDestockingBModel;
import fr.vif.vif5_7.stock.kernel.ui.features.fdestocking.touch.FDestockingBView;


/**
 * FProductionInput : Viewer View.
 * 
 * @author vr
 */
public class FProductionInputCompVView extends StandardTouchViewer<FProductionInputCompVBean> implements
        FProductionInputContainerVIView {

    private CCommentTouchView   commentTouchView;
    private TouchInputBarCode   inputBarCode;
    private TouchInputTextField itfBatch;
    private TouchInputTextField itfContainer;
    private JLabel              jlFlash;
    private JPanel              jpFlash;
    private CQuantityUnitView   qtty1;
    private CQuantityUnitView   qtty2;

    private JSeparator          separator;

    private CSubstituteItemView substituteItemView;

    /**
     * Default constructor.
     */
    public FProductionInputCompVView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableComponents(final boolean creationMode) {
        super.enableComponents(creationMode);
        getInputBarCode().setEnabled(true);
        if (getModel().getBean() == null
                || getModel().getBean().getBBean() == null
                || getModel().getBean().getBBean().getOperationItemBean() == null
                || getModel().getBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                        .equals(ManagementType.ARCHIVED)) {
            getItfBatch().setEnabled(false);
            getSubstituteItemView().disableComponents();
            getQtty1().disableComponents();
            getQtty2().disableComponents();
            getItfContainer().setEnabled(false);
        } else {
            if (InputParametersEnums.InputEntityType.BATCH_ITEM.equals(getModel().getBean().getInputItemParameters()

            .getEntityType())) {
                getItfContainer().setEnabled(false);
                getSubstituteItemView().enableComponents(true);
                if (getModel().getBean().getEntity().getStockItemBean().getGeneralItemInfos().getStockByBatch()) {
                    getItfBatch().setEnabled(true);
                } else {
                    getItfBatch().setEnabled(false);
                }
            } else if (InputParametersEnums.InputEntityType.CONTAINER.equals(getModel().getBean()
                    .getInputItemParameters().getEntityType())) {
                getItfContainer().setEnabled(true);
                getSubstituteItemView().disableComponents();
                getItfBatch().setEnabled(false);
            }
            if (getModel().getBean().getEntity().getStockItemBean().getEnableFields().getFirstQty()) {
                getQtty1().enableComponents(true);
            } else {
                getQtty1().disableComponents();
            }
            if (getModel().getBean().getEntity().getStockItemBean().getEnableFields().getSecondQty()) {
                getQtty2().enableComponents(true);
            } else {
                getQtty2().disableComponents();
            }

            if (MONatureComplementaryWorkshop.INPROGRESS.getValue().equals(getModel().getBean().getState())) {
                getSubstituteItemView().disableComponents();
            }

        }

    }

    /**
     * Get the comment touch view.
     * 
     * @return the comment touch view.
     */
    public CCommentTouchView getCommentTouchView() {
        if (commentTouchView == null) {
            commentTouchView = new CCommentTouchView();
            commentTouchView.setBounds(0, 199, 866, 60);
            commentTouchView.setBeanProperty("comment");
        }
        return commentTouchView;
    }

    /**
     * Get the touch led input barcode.
     * 
     * @return the touch led input bar code
     */
    public TouchInputBarCode getInputBarCode() {
        if (inputBarCode == null) {
            inputBarCode = new TouchInputBarCode(BarCodeCtrl.class);
            inputBarCode.setBounds(64, 2, 796, 30);
            // inputBarCode.setBeanProperty("barcode");
            inputBarCode.setBeanBased(false);
            inputBarCode.setBorder(new LineBorder(
                    TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR), 1, false));
            inputBarCode.setEnabled(true);
        }
        return inputBarCode;
    }

    /**
     * Get itfBatch.
     * 
     * @return itfBatch
     */
    public TouchInputTextField getItfBatch() {
        if (itfBatch == null) {
            itfBatch = new TouchInputTextField(String.class, "15", I18nClientManager.translate(StockKernel.T29224));
            itfBatch.setTextLabel(I18nClientManager.translate(StockKernel.T29224));
            itfBatch.setBeanProperty("entity." + CEntityEnum.BATCH.getValue());
            itfBatch.setHelpBrowserTriad(new BrowserMVCTriad(FDestockingBModel.class, FDestockingBView.class,
                    FDestockingBCtrl.class));
            itfBatch.setUseFieldHelp(true);
            itfBatch.setUseBrowserLine(true);
            itfBatch.setFreeDataInput(true);
            FDestockingSBean sBean = new FDestockingSBean();
            itfBatch.setInitialHelpSelection(sBean);
            itfBatch.setHelpBrowserBeanCode(FDestockingBBeanEnum.BATCH.getValue());
            itfBatch.setBounds(285, 91, 319, 95);
        }
        return itfBatch;
    }

    /**
     * Get itfContainer.
     * 
     * @return itfContainer
     */
    public TouchInputTextField getItfContainer() {
        if (itfContainer == null) {
            itfContainer = new TouchInputTextField(String.class, "10", I18nClientManager.translate(StockKernel.T29089));
            itfContainer.setBounds(5, 32, 219, 70);
            itfContainer.setNumKeyboardForAlphaFields(true);
            itfContainer.setBeanProperty("entity." + CEntityEnum.CONTAINER_NUMBER.getValue());
        }
        return itfContainer;
    }

    /**
     * Get the label flash.
     * 
     * @return the label flash
     */
    public JLabel getJlFlash() {
        if (jlFlash == null) {
            jlFlash = new JLabel();
            jlFlash.setBounds(10, 2, 55, 30);
            jlFlash.setForeground(new Color(238, 127, 0));
            jlFlash.setFont(new Font("Arial", Font.BOLD, 16));
            jlFlash.setText(I18nClientManager.translate(ProductionMo.T29697, false));
        }
        return jlFlash;
    }

    /**
     * 
     * Get Flash panel.
     * 
     * @return the flash panel
     */
    public JPanel getJpFlash() {
        if (jpFlash == null) {
            jpFlash = new JPanel();
            final GridBagLayout gridBagLayout = new GridBagLayout();
            gridBagLayout.columnWidths = new int[] { 0, 7 };
            jpFlash.setLayout(gridBagLayout);
            jpFlash.setBounds(10, 3, 850, 29);
            jpFlash.setOpaque(false);
            final GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new Insets(0, 0, 0, 5);
            gridBagConstraints.anchor = GridBagConstraints.EAST;
            gridBagConstraints.weighty = 1;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.gridx = 0;
            jpFlash.add(getJlFlash(), gridBagConstraints);
            final GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.weighty = 1;
            gridBagConstraints1.fill = GridBagConstraints.BOTH;
            gridBagConstraints1.weightx = 1;
            gridBagConstraints1.gridy = 0;
            gridBagConstraints1.gridx = 1;
            jpFlash.add(getInputBarCode(), gridBagConstraints1);
        }
        return jpFlash;
    }

    /**
     * 
     * Get qtty1.
     * 
     * @return qtty1
     */
    public CQuantityUnitView getQtty1() {
        if (qtty1 == null) {
            qtty1 = new CQuantityUnitView();
            qtty1.setBounds(614, 38, 246, 70);
            qtty1.setBeanProperty("entity.stockItemBean.keyboardQties.firstQty");
            qtty1.setDescription(I18nClientManager.translate(GenKernel.T210, false) + " 1");
        }
        return qtty1;
    }

    /**
     * Get qtty2.
     * 
     * @return qtty2
     */
    public CQuantityUnitView getQtty2() {
        if (qtty2 == null) {
            qtty2 = new CQuantityUnitView();
            qtty2.setBounds(614, 110, 247, 70);
            qtty2.setBeanProperty("entity.stockItemBean.keyboardQties.secondQty");
            qtty2.setDescription(I18nClientManager.translate(GenKernel.T210, false) + " 2");
        }
        return qtty2;
    }

    /**
     * Get the separator.
     * 
     * @return the separator
     */
    public JSeparator getSeparator() {
        if (separator == null) {
            separator = new JSeparator();
            separator.setOrientation(SwingConstants.VERTICAL);
            separator.setBounds(608, 35, 21, 156);
        }
        return separator;
    }

    /**
     * 
     * Get the substituteitem view.
     * 
     * @return the substitute item view
     */
    public CSubstituteItemView getSubstituteItemView() {
        if (substituteItemView == null) {
            substituteItemView = new CSubstituteItemView();
            substituteItemView.setBounds(5, 100, 269, 96);
            substituteItemView.setBeanProperty("entity." + CEntityEnum.ITEM_CL.getValue());
        }
        return substituteItemView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void hideAll(final boolean hide) {
        if (hide) {
            getCommentTouchView().setVisible(false);
            getItfContainer().setVisible(false);
            getItfBatch().setVisible(false);
            getQtty1().setVisible(false);
            getQtty2().setVisible(false);
            getSubstituteItemView().setVisible(false);
            getSeparator().setVisible(false);
            getJpFlash().setVisible(false);
        } else {
            getCommentTouchView().setVisible(true);
            getItfContainer().setVisible(true);
            getItfBatch().setVisible(true);
            getQtty1().setVisible(true);
            getQtty2().setVisible(true);
            getSubstituteItemView().setVisible(true);
            getSeparator().setVisible(true);
            getJpFlash().setVisible(true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setFlashLabel(final String flashLabel) {
        getJlFlash().setText(flashLabel);

    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        setLayout(null);
        setPreferredSize(new Dimension(870, 768));
        add(getCommentTouchView());
        add(getItfContainer());
        add(getItfBatch());
        add(getQtty1());
        add(getQtty2());
        add(getSubstituteItemView());
        add(getSeparator());
        add(getJpFlash());
    }
}

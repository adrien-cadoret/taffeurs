/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListOutputFCtrl.java,v $
 * Created on 8 avr. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.Key;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylistcomp.FDataEntryListCompBCtrl;


/**
 * Data entry OUTPUR list feature controller.
 * 
 * @author vr
 */
public class FDataEntryListOutputFCtrl extends FDataEntryListFCtrl {

    /** Button for print upper container reference. */
    public static final String  PRINT_UPPER_CONTAINER_BTN = ButtonReference.PRINT_UPPER_CONTAINER_BTN.getReference();

    /** LOGGER. */
    private static final Logger LOGGER                    = Logger.getLogger(FDataEntryListOutputFCtrl.class);

    private OutputEntityType    entityType                = OutputEntityType.CONTAINER;

    /** The print upper container button. */
    private ToolBarBtnStdModel  printUpperContainerBtn    = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        if (event.getModel().getReference().equals(FDataEntryListOutputFCtrl.PRINT_UPPER_CONTAINER_BTN)) {
            actionPrintUpperContainer();

        } else {
            // Others cases are managed in super class
            super.buttonAction(event);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        entityType = (OutputEntityType) getSharedContext().get(Domain.DOMAIN_PARENT, "outputEntityType");
        super.initialize();

        TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL);
        if (touchModel != null) {
            ((FDataEntryListBCtrl) getBrowserController()).changeColumns(entityType, touchModel);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }
        List<ToolBarBtnStdBaseModel> lstBtn = null;
        if (getBrowserController() instanceof FDataEntryListCompBCtrl) {
            lstBtn = new ArrayList<ToolBarBtnStdBaseModel>();
            lstBtn.add(getDelBtn());
            lstBtn.add(getPrintBtn());
            lstBtn.add(getPrintContainerBtn());
            lstBtn.add(getPrintUpperContainerBtn());
            lstBtn.add(getCriteriaBtn());
            // Functionalities
            lstBtn.add(getIncidentBtn());
        } else {
            lstBtn = super.manageActionsButtonModels(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lstBtn;
    }

    /**
     * Gets the prints the upper container button.
     * 
     * @return the prints the upper container button
     */
    protected ToolBarBtnStdModel getPrintUpperContainerBtn() {
        if (printUpperContainerBtn == null) {
            printUpperContainerBtn = new ToolBarBtnStdModel();
            printUpperContainerBtn.setReference(FDataEntryListOutputFCtrl.PRINT_UPPER_CONTAINER_BTN);
            printUpperContainerBtn.setText("Réédition contenant père");
            printUpperContainerBtn.setIconURL("/images/vif/vif57/production/mo/print.png");
            printUpperContainerBtn.setShortcut(Key.K_F4);
        }
        return printUpperContainerBtn;
    }

    /**
     * Action print upper container.
     */
    private void actionPrintUpperContainer() {
        try {
            getFdataEntryListCBS().printUpperContainer(getIdCtx(),
                    (FDataEntryListBBean) getBrowserController().getModel().getCurrentBean());
        } catch (BusinessException e) {
            getView().show(new UIException(e));
        }

    }

}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOBCtrl.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSBean;
import fr.vif.vif5_7.production.mo.business.services.features.fsupervisormo.FSupervisorMOCBS;


/**
 * FSupervisorMO : Browser Controller.
 * 
 * @author jd
 */
public class FSupervisorMOBCtrl extends AbstractBrowserController<FSupervisorMOBBean> {
    /** LOGGER. */
    private static final Logger LOGGER           = Logger.getLogger(FSupervisorMOBCtrl.class);

    private FSupervisorMOSBean  currentSelection = null;
    private FSupervisorMOCBS    fSupervisorMOCBS;

    /**
     * Default constructor.
     * 
     */
    public FSupervisorMOBCtrl() {
        super();
    }

    /**
     * get the current selection.
     * 
     * @return {@link FSupervisorSBean}
     */
    public FSupervisorMOSBean getCurrentSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getCurrentSelection()");
        }

        if (currentSelection == null) {
            currentSelection = (FSupervisorMOSBean) getDefaultInitialSelection();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getCurrentSelection()=" + currentSelection);
        }
        return currentSelection;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDefaultInitialSelection()");
        }

        FSupervisorMOSBean ret = new FSupervisorMOSBean();
        ret.getSelection().setPoDate(new Date());
        ret.getSelection().setEstablishmentKey(
                new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDefaultInitialSelection()=" + ret);
        }
        return ret;
    }

    /**
     * Gets the fSupervisorMOCBS.
     * 
     * @category getter
     * @return the fSupervisorMOCBS.
     */
    public FSupervisorMOCBS getfSupervisorMOCBS() {
        return fSupervisorMOCBS;
    }

    /**
     * Sets the fSupervisorMOCBS.
     * 
     * @category setter
     * @param fsupervisorMOCBS fSupervisorMOCBS.
     */
    public void setfSupervisorMOCBS(final FSupervisorMOCBS fsupervisorMOCBS) {
        this.fSupervisorMOCBS = fsupervisorMOCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FSupervisorMOBBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FSupervisorMOBBean> lst = new ArrayList<FSupervisorMOBBean>();

        // Store the selection and use it every time.
        currentSelection = (FSupervisorMOSBean) selection;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + lst);
        }
        return lst;
    }

}

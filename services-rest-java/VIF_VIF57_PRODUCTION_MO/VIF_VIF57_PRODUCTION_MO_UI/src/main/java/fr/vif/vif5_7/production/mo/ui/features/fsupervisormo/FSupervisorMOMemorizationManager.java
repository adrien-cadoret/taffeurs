/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PREPARATION_KERNEL_UI
 * File : $RCSfile: FSupervisorMOMemorizationManager.java,v $
 * Created on 5 janv. 2016 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.selection.PermanentSelectionValues;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGanttMOSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.GanttHorizonEnum;
import fr.vif.vif5_7.production.mo.constants.GanttValueTypeEnum;


/**
 * Manager of the memorization.
 *
 * @author ac
 */
public class FSupervisorMOMemorizationManager {
    private FSupervisorMOFCtrl                    ctrl = null;

    private Map<String, PermanentSelectionValues> map  = new HashMap<String, PermanentSelectionValues>();

    /**
     * Constructor.
     * 
     * @param map
     */
    public FSupervisorMOMemorizationManager(final FSupervisorMOFCtrl ctrl,
            final Map<String, PermanentSelectionValues> map) {
        this.ctrl = ctrl;
        this.map = map;
    }

    /**
     * Gets the ctrl.
     *
     * @return the ctrl.
     */
    public FSupervisorMOFCtrl getCtrl() {
        return ctrl;
    }

    /**
     * Gets the map.
     *
     * @return the map.
     */
    public Map<String, PermanentSelectionValues> getMap() {
        return map;
    }

    /**
     * Set memorization map in shared context.
     * 
     * @param keyToGet key to get from map
     * @return {@link PermanentSelectionValues}
     */
    public PermanentSelectionValues getMemorizationById(final String keyToGet) {

        return map.get(keyToGet);

    }

    /**
     *
     * Read the memorized data.
     * 
     * @param frameUniqueId the frame ID
     * @param selectionToRead the selection to read
     */
    public void readMemorization(final String frameUniqueId, final Object selectionToRead) {

        if (selectionToRead instanceof FGanttMOSBean) {
            PermanentSelectionValues p = getMemorizationById(frameUniqueId);

            FGanttMOSBean sbean = (FGanttMOSBean) selectionToRead;

            if (p != null) {
                String[] stored = p.getCommon().split("\\;");

                for (String frame : stored) {
                    String[] split = frame.split("\\=");

                    String key = split[0];
                    String value = split[1];

                    GanttValueTypeEnum type = GanttValueTypeEnum.getValueType(key);

                    if (type != null) {
                        switch (type) {
                            case CHRONO:
                                sbean.setShowChrono("Y".equalsIgnoreCase(value));
                                break;
                            case ITEM_LABEL:
                                sbean.setShowItemLabel("Y".equalsIgnoreCase(value));
                                break;
                            case CODE_LABEL:
                                sbean.setShowCodeLabel("Y".equalsIgnoreCase(value));
                                break;
                            case EFFECTIVE_QTY:
                                sbean.setShowEffectiveQty("Y".equalsIgnoreCase(value));
                                break;
                            case EXPECTED_QTY:
                                sbean.setShowExpectedQty("Y".equalsIgnoreCase(value));
                                break;
                            case HORIZON:
                                if (EntityTools.isValid(value)) {
                                    for (GanttHorizonEnum horizonEnum : GanttHorizonEnum.values()) {
                                        if (horizonEnum.getValue().equals(value)) {
                                            sbean.setHorizon(horizonEnum);
                                            break;
                                        }
                                    }
                                }
                            case OVERVIEW:
                                sbean.setOverview("Y".equalsIgnoreCase(value));
                                break;
                            default:
                                break;
                        }
                    }
                }

            }
        } else {
            ctrl.getView().showError("Erreur programmation",
                    "Aucune méthode de conversion de la sélection " + selectionToRead.toString());
        }
    }

    /**
     * 
     * Saves the memorization
     * 
     * @param frameUniqueId
     * @param selectionToStore
     */
    public void saveMemorization(final String frameUniqueId, final Object selectionToStore) {

        if (selectionToStore instanceof FGanttMOSBean) {
            FGanttMOSBean sbean = (FGanttMOSBean) selectionToStore;

            String toStore = "";
            PermanentSelectionValues p = new PermanentSelectionValues();

            p.setValueType("CHA");
            for (GanttValueTypeEnum v : GanttValueTypeEnum.values()) {
                boolean toShow = false;
                boolean overview = false;
                String horizon = "";
                switch (v) {
                    case CHRONO:
                        toShow = sbean.getShowChrono();
                        break;
                    case ITEM_LABEL:
                        toShow = sbean.getShowItemLabel();
                        break;
                    case CODE_LABEL:
                        toShow = sbean.getShowCodeLabel();
                        break;
                    case EFFECTIVE_QTY:
                        toShow = sbean.getShowEffectiveQty();
                        break;
                    case EXPECTED_QTY:
                        toShow = sbean.getShowExpectedQty();
                        break;
                    case HORIZON:
                        horizon = sbean.getHorizon().getValue();
                        break;
                    case OVERVIEW:
                        overview = sbean.getOverview();
                        break;
                    default:
                        break;

                }

                if (v == GanttValueTypeEnum.HORIZON) {
                    toStore += v.getValue() + "=" + horizon + ";";
                } else if (v == GanttValueTypeEnum.OVERVIEW) {
                    toStore += v.getValue() + "=" + (overview ? "Y" : "N") + ";";
                } else {
                    toStore += v.getValue() + "=" + (toShow ? "Y" : "N") + ";";
                }
            }

            p.setCommon(toStore);
            map.put(frameUniqueId, p);
        } else {
            ctrl.getView().showError("Erreur programmation",
                    "Aucune méthode de conversion de la sélection " + selectionToStore.toString());
        }

        if (map.get(frameUniqueId) != null && map.get(frameUniqueId).getCommon().length() > 110) {
            ctrl.getViewerController().getView()
                    .showError("Erreur", "Trop d'information à enregistrer dans la table de mémorization.");
            return;
        }

        try {
            // User is empty ! It's the view which is parameterized.
            ctrl.getFsupervisorMOCBS().saveSelectionByIds(ctrl.getIdCtx(), "$$", "$$", "",
                    ctrl.getCurrentView().toString(), "", "SPP",
                    /* FSupervisorTools.MEMORIZATION_KEY, */map);

        } catch (BusinessException e) {
            ctrl.getViewerController().getView().show(new UIException(e));
        }
    }

    /**
     * Sets the ctrl.
     *
     * @param ctrl ctrl.
     */
    public void setCtrl(final FSupervisorMOFCtrl ctrl) {
        this.ctrl = ctrl;
    }

    /**
     * Sets the map.
     *
     * @param map map.
     */
    public void setMap(final Map<String, PermanentSelectionValues> map) {
        this.map = map;
    }

    /**
     * Analyses the stored values.
     * 
     * @param stored
     * @return a List of CodeLabel
     */
    private List<CodeLabel> analyseStoredValues(final String[] stored, final List<CodeLabel> ref) {
        List<CodeLabel> ret = new ArrayList<CodeLabel>();
        for (String frame : stored) {
            String split[] = frame.split("\\=");

            if (split.length > 1) {
                String key = split[0];
                String value = split[1];

                switch (key) {
                    case "cols":
                        String[] cols = value.split("\\,");
                        List<CodeLabel> cls = new ArrayList<CodeLabel>();
                        for (String col : cols) {
                            CodeLabel cl = ref.get(new Integer(col));
                            if (cl != null) {
                                cls.add(cl);

                            }
                        }

                        ret.addAll(cls);

                        break;

                    default:
                        break;
                }

            }
        }
        return ret;
    }
}

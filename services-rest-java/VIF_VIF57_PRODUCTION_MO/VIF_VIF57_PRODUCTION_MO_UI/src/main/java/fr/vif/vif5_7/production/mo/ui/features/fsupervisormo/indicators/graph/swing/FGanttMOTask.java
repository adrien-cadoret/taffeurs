/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOTask.java,v $
 * Created on 22 mars 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.util.Date;

import org.jfree.data.gantt.Task;
import org.jfree.data.time.TimePeriod;


/**
 * FGanttTask. Personnalisation of Jfreechart task for gantt view.
 * 
 * @author jd
 * @param <BEAN> bean
 */
public class FGanttMOTask<BEAN> extends Task {

    private BEAN bean = null;

    /**
     * Constructor.
     * 
     * @param description description
     * @param start start
     * @param end end
     */
    public FGanttMOTask(final String description, final Date start, final Date end) {
        super(description, start, end);
    }

    /**
     * Constructor.
     * 
     * @param description description
     * @param duration duration
     */
    public FGanttMOTask(final String description, final TimePeriod duration) {
        super(description, duration);
    }

    /**
     * Gets the bean.
     * 
     * @return the bean.
     */
    public BEAN getBean() {
        return bean;
    }

    /**
     * Sets the bean.
     * 
     * @param bean bean.
     */
    public void setBean(final BEAN bean) {
        this.bean = bean;
    }

}

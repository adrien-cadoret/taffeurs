/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputVView.java,v $
 * Created on 11 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist.touch;


import java.awt.Dimension;

import fr.vif.jtech.ui.input.swing.SwingInputLabel;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.RoundBackgroundPanelBorder;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputVBean;


/**
 * FBoningDataEntryListInput Viewer View.
 * 
 * @author xg
 */
public class FBoningDataEntryListInputVView extends StandardTouchViewer<FBoningDataEntryListInputVBean> {
    private TouchInputTextField itemLabel;
    private SwingInputLabel     swingInputLabel;

    /**
     * Constructor.
     */
    public FBoningDataEntryListInputVView() {
        super();
        initialize();
    }

    /**
     * Gets the item label.
     * 
     * @return the itemlabel touchinputtextfield
     */
    public TouchInputTextField getItemLabel() {
        if (itemLabel == null) {
            itemLabel = new TouchInputTextField(String.class, "50", I18nClientManager.translate(GenKernel.T426, false));
            itemLabel.setBeanBased(true);
            itemLabel.setEnabled(false);
            itemLabel.setAlwaysDisabled(true);
            Dimension d = new Dimension(500, 60);
            itemLabel.setPreferredSize(d);
            itemLabel.setMinimumSize(d);
            itemLabel.setMaximumSize(d);
            itemLabel.setBeanProperty("itemCL.label");
        }
        return itemLabel;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setBorder(new RoundBackgroundPanelBorder(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR));

        add(getItemLabel());
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionContainerMOListFCtrl.java,v $
 * Created on 07 nov. 2008 by ALB
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioncontainermolist;


import java.awt.Frame;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.swing.JPanel;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.dialogs.DialogController;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerCtrl;
import fr.vif.jtech.ui.docviewer.pdfviewer.TouchPDFViewerModel;
import fr.vif.jtech.ui.docviewer.pdfviewer.touch.TouchPDFViewerView;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.input.InputBarCodeActionEvent;
import fr.vif.jtech.ui.input.InputBarCodeActionListener;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarFunctionalitiesBtnDController;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchFunctionalitiesBtnDView;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.touch.TouchToolBarBtnStd;
import fr.vif.jtech.ui.toolbar.buttons.toolbarbutton.ToolBarButtonController;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelFunction;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionmo.FProductionMOListCBS;
import fr.vif.vif5_7.production.mo.business.services.features.flaborstaff.FLaborStaffCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOState;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.FunctionnalityTools;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.FMOListBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.FMOListBIView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation.FMSOCreationBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation.FMSOCreationFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioncontainermolist.touch.FProductionContainerMOListVView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.FLaborStaffFCtrl;
import fr.vif.vif5_7.stock.kernel.business.beans.features.quality.nc.fncflow.FNCFlowSharedContextBean;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCConstants;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCActType;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCType;
import fr.vif.vif5_7.stock.kernel.ui.util.StockUITools;
import fr.vif.vif5_7.workshop.workstation.business.beans.common.WorkStationKey;
import fr.vif.vif5_7.workshop.workstation.business.beans.dialog.debug.DDebugBean;
import fr.vif.vif5_7.workshop.workstation.business.beans.features.fworkstation.FWorkStationSBean;
import fr.vif.vif5_7.workshop.workstation.ui.dialog.debug.DDebugCtrl;
import fr.vif.vif5_7.workshop.workstation.ui.features.fworkstation.WorkStationTools;


/**
 * MOList : Feature Controller.
 * 
 * @author vr
 */
public class FProductionContainerMOListFCtrl extends StandardFeatureController implements TableRowChangeListener,
InputBarCodeActionListener, DialogChangeListener {

    /**
     * list of btns.
     */
    public static final String   MO_BONING_INPUT_BTN     = "moBoningOutput";
    public static final String   BTN_PDF_REFERENCE       = ButtonReference.PDF_REFERENCE_BTN.getReference();
    // ALB Q7369 Problem with opening incident window
    public static final String   BTN_OPEN_INCIDENT       = ButtonReference.OPEN_INCIDENT_REFERENCE_BTN.getReference();

    public static final String   BTN_PDF_LIST_REFERENCE  = "BTN_PDF_LIST_REFERENCE";
    public static final String   BTN_SHOW_ALL_REFERENCE  = ButtonReference.SHOW_ALL_REFERENCE_BTN.getReference();
    public static final String   LOGICAL_WORKSTATION_BTN = ButtonReference.LOGICAL_WORKSTATION_BTN.getReference();
    public static final String   MO_CREATE_BTN           = ButtonReference.MO_CREATE_BTN.getReference();
    public static final String   MO_CREATE_MSO_BTN       = ButtonReference.MO_CREATE_MSO_BTN.getReference();
    public static final String   MO_FINISH_BTN           = ButtonReference.MO_FINISH_BTN.getReference();
    public static final String   MO_UNFINISH_BTN         = ButtonReference.MO_UNFINISH_BTN.getReference();
    public static final String   MO_INPUT_BTN            = ButtonReference.MO_INPUT_BTN.getReference();
    public static final String   MO_OUTPUT_BTN           = ButtonReference.MO_OUTPUT_BTN.getReference();
    public static final String   LABOR_STAFF             = ButtonReference.LABOR_STAFF_BTN.getReference();
    public static final String   USER_VIF                = "VIF";
    public static final String   USER_VIF_INTEG          = "VIFINTEG";
    private static final Logger  LOGGER                  = Logger.getLogger(FProductionContainerMOListFCtrl.class);

    private FLaborStaffCBS       laborstaffCBS;
    private ToolBarBtnStdModel   btnFinish               = null;
    private ToolBarBtnStdModel   btnPDFModel;
    private ToolBarBtnStdModel   btnOpenIncidentModel;

    private FProductionMOListCBS productionMOListCBS     = null;

    /**
     * {@inheritDoc}
     */
    public void barcodeReceived(final InputBarCodeActionEvent event) {
        interpretBarcode(event.getBarcode());
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        super.buttonAction(event);
        try {
            if (event.getModel().getReference().equals(MO_CREATE_BTN)) {
                openChildFeature("VIF.MOCREAFJ");
            } else if (event.getModel().getReference().equals(MO_CREATE_MSO_BTN)) {
                openChildFeature("VIF.MSOCREAFJ");
            } else if (event.getModel().getReference().equals(LABOR_STAFF)) {
                openChildFeature("VIF.LABSTAFF");
            } else if (event.getModel().getReference().equals(MO_OUTPUT_BTN)) {
                performOpenOutput();
            } else if (event.getModel().getReference().equals(MO_INPUT_BTN)) {
                performOpenInput();
            } else if (event.getModel().getReference().equals(FProductionContainerMOListFCtrl.LOGICAL_WORKSTATION_BTN)) {
                String wsId = (String) getSharedContext().get(Domain.DOMAIN_THIS,
                        FProductionConstant.LOGICAL_WORKSTATION);
                FWorkStationSBean selection = new FWorkStationSBean(new WorkStationKey(getIdCtx().getCompany(),
                        getIdCtx().getEstablishment(), wsId));
                selection.setGroupOnly(true); // Sélection du groupe du poste seulement
                CodeLabel wsCL = new WorkStationTools(this).getWorkStationCodeLabel(getViewerController().getView(),
                        selection);

                if (null != wsCL && null != wsCL.getCode() && !"".equals(wsCL.getCode())) {
                    getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.LOGICAL_WORKSTATION, wsCL.getCode());
                    fireSharedContextSend(getSharedContext());
                    ((FMOListSBean) getGeneralSelectionController().getModel().getBean()).setWorkstationId(wsCL
                            .getCode());
                    refresh((Selection) getGeneralSelectionController().getModel().getBean(), null);
                }
            } else if (event.getModel().getReference().equals(FProductionContainerMOListFCtrl.MO_FINISH_BTN)) {
                String msg = I18nClientManager.translate(ProductionMo.T17368, false);
                FMOListBBean currentBean = (FMOListBBean) getBrowserController().getModel().getCurrentBean();
                try {
                    if (currentBean.getIsFinished()) {
                        msg = I18nClientManager.translate(ProductionMo.T29551, false);
                    }

                    int msgBtn = showQuestion(getBtnFinish().getText(), msg, MessageButtons.YES_NO);
                    if (msgBtn == MessageButtons.YES) {
                        FMOListSBean sBean = (FMOListSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                                FProductionConstant.MO_INITIAL_SELECTON);

                        ((FProductionContainerMOListVCtrl) getViewerController()).finishMO(
                                ((FMOListBBean) getBrowserController().getModel().getCurrentBean()).getIsFinished(),
                                sBean.getActivityItemType());
                    }
                } finally {
                    refresh(null, currentBean);
                }
            } else if (BTN_PDF_REFERENCE.equalsIgnoreCase(event.getModel().getReference())) {
                try {

                    List<PDFFile> listPdfFiles = getDocumentsToShow();
                    if (listPdfFiles.isEmpty()) {
                        getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                                I18nClientManager.translate(ProductionMo.T30839));
                    } else if (listPdfFiles.size() == 1) {
                        viewDocumentRecette(listPdfFiles.get(0));
                    } else {
                        viewDocumentList(listPdfFiles);
                    }

                } catch (BusinessException e) {
                    getView().show(new UIException(e));
                }
            } else if (event.getModel().getReference().equals(FProductionContainerMOListFCtrl.BTN_SHOW_ALL_REFERENCE)) {
                FMOListSBean selection = (FMOListSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                        FProductionConstant.MO_INITIAL_SELECTON);

                selection.setShowFinished(!selection.isShowFinished());
                refresh(selection, (FMOListBBean) getBrowserController().getModel().getCurrentBean());

            } else if (BTN_OPEN_INCIDENT.equals(event.getModel().getReference())) {

                // Prepare parameters bean
                FNCFlowSharedContextBean parametersBean = new FNCFlowSharedContextBean();
                parametersBean.setType(NCType.INTERNAL.getKey());

                // Feed with viewer bean informations
                FMOListVBean vBean = (FMOListVBean) getViewerController().getBean();
                parametersBean.setActType(NCActType.MANUFACTURING_ORDER.getKey());
                parametersBean.setChronoOrigin(vBean.getMobean().getMoKey().getChrono());
                StockItem stockItem = new StockItem();
                stockItem.setItemId(vBean.getMobean().getItemCLs().getCode());
                stockItem.getQuantities().getFirstQty().setQty(vBean.getMobean().getMoQuantityUnit().getToDoQuantity());
                stockItem.getQuantities().getFirstQty().setUnit(vBean.getMobean().getMoQuantityUnit().getUnit());
                parametersBean.setStockItem(stockItem);

                // Put parameters in context and open feature
                getSharedContext().put(Domain.DOMAIN_THIS, NCConstants.NCFLOW_KEY_SHARED_CONTEXT, parametersBean);
                if (StockUITools.isTouchRunning()) {
                    openChildFeature(NCConstants.FeatureName.FLOW_TOUCH.getName());
                } else {
                    openChildFeature(NCConstants.FeatureName.FLOW.getName());
                }
            } else if (BTN_PDF_LIST_REFERENCE.equals(event.getModel().getReference())) {
                PDFFile pdfFile = (PDFFile) ((ToolBarBtnStdModel) event.getModel()).getLinkedObject();
                viewDocumentRecette(pdfFile);
            } else if (ButtonReference.SETTING_INFORMATION.getReference().equals(event.getModel().getReference())) {
                viewSettingInformation();
            } else if (ButtonReference.DEBUG_REFERENCE.getReference().equals(event.getModel().getReference())) {
                DDebugBean bean = new DDebugBean();
                bean.getObjectsToShow().add(getBrowserController().getModel().getBeans());
                bean.getObjectsToShow().add(getViewerController().getBean());
                bean.getObjectsToShow().add(getBrowserController().getInitialSelection());
                DDebugCtrl.showDebug(this.getView(), this, this, this.getModel().getIdentification(), bean);
            }
        } catch (UIException e) {
            getView().showError(e.getLocalizedMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public void childFeatureClosed(final FeatureController childFeatureController) {

        super.childFeatureClosed(childFeatureController);
        String featureId = (String) getSharedContext().get(Domain.DOMAIN_CHILD, FProductionConstant.FEATURE_ID);
        getSharedContext().remove(Domain.DOMAIN_CHILD, FProductionConstant.FEATURE_ID);

        MOBean bean = null;
        if (childFeatureController instanceof FProductionInputContainerFCtrl) {
            bean = ((FProductionInputContainerFCtrl) childFeatureController).getMoListVBean().getMobean();
        } else if (childFeatureController instanceof FProductionOutputContainerFCtrl) {
            bean = ((FProductionOutputContainerFCtrl) childFeatureController).getMoListVBean().getMobean();
        } else if (childFeatureController instanceof FMOCreationFCtrl
                || childFeatureController instanceof FMSOCreationFCtrl) {
            bean = (MOBean) getSharedContext().get(Domain.DOMAIN_CHILD, FProductionConstant.STARTED_MO_BEAN);
            // CHECKSTYLE:OFF
        } else if (childFeatureController instanceof FLaborStaffFCtrl) {
            // to skip Else
            // CHECKSTYLE:ON
        } else {
            FMOListBBean bbean = (FMOListBBean) getSharedContext().get(Domain.DOMAIN_THIS,
                    FProductionConstant.MO_LIST_BBEAN);
            // ALB Q7369 Null pointer exception when the Mo list is empty.
            if (bbean != null) {
                bean = bbean.getMobean();
            }
        }

        if (featureId != null && !"".equals(featureId) && !"VIF.LABORTIME".equals(featureId)) {
            openChildFeature(featureId);
            getSharedContext().remove(Domain.DOMAIN_CHILD, FProductionConstant.FEATURE_ID);
            // Fix DD 09/09/14 M155749 : No need to refresh after a child feature opening. Causes button management
            // crash later.
            // FMOListBBean bBean = (FMOListBBean) getSharedContext().get(Domain.DOMAIN_THIS,
            // FProductionConstant.MO_LIST_BBEAN);
            // refresh(null, bBean);

        } else if (childFeatureController instanceof FMOCreationFCtrl
                || childFeatureController instanceof FMSOCreationFCtrl) {
            FMOListBBean beanSelected = (FMOListBBean) getBrowserController().getModel().getCurrentBean();

            getSharedContext().remove(Domain.DOMAIN_CHILD, FProductionConstant.STARTED_MO_BEAN);

            if (bean != null) {
                ((FMOListSBean) getGeneralSelectionController().getBean()).setMoDate(new Date());
                getGeneralSelectionController().getView().render();

                FMOListBBean browserBean = new FMOListBBean();
                browserBean.setMobean(bean);

                getBrowserController().reopenQuery();
                ((FMOListBCtrl) getBrowserController()).setSelectedBean(browserBean);

            } else {
                getBrowserController().reopenQuery();
                ((FMOListBCtrl) getBrowserController()).setSelectedBean(beanSelected);
            }

        } else {
            FMOListBBean bbean = new FMOListBBean();
            bbean.setMobean(bean);
            refresh(null, bbean);
        }

        if (childFeatureController instanceof FMOCreationFCtrl || childFeatureController instanceof FMSOCreationFCtrl) {

            // open the input or output feature
            boolean inputEnable = false;
            boolean outputEnable = false;
            for (ToolBarBtnStdBaseModel actionBtn : getActionsButtonModels()) {
                if (FProductionContainerMOListFCtrl.MO_INPUT_BTN
                        .equals(((ToolBarBtnStdModel) actionBtn).getReference())) {
                    inputEnable = actionBtn.getEnabled();
                } else if (FProductionContainerMOListFCtrl.MO_OUTPUT_BTN.equals(((ToolBarBtnStdModel) actionBtn)
                        .getReference())) {
                    outputEnable = actionBtn.getEnabled();
                }
            }
            if (inputEnable && !outputEnable) {
                performOpenInput();
            } else if (outputEnable && !inputEnable) {
                performOpenOutput();
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
    }

    /**
     * Gets the btnFinish.
     * 
     * 
     * @return the btnFinish.
     */

    public ToolBarBtnStdModel getBtnFinish() {
        return this.btnFinish;
    }

    /**
     * 
     * Gets the current bbean.
     * 
     * @return the current bbean.
     */
    public FMOListBBean getCurrentBBean() {
        FMOListBBean bbean = (FMOListBBean) getBrowserController().getModel().getCurrentBean();
        return bbean;
    }

    /**
     * Gets the laborstaffCBS.
     * 
     * @return the laborstaffCBS.
     */
    public FLaborStaffCBS getLaborstaffCBS() {
        return laborstaffCBS;
    }

    /**
     * Gets the productionMOListCBS.
     * 
     * @category getter
     * @return the productionMOListCBS.
     */
    public FProductionMOListCBS getProductionMOListCBS() {
        return productionMOListCBS;
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        if (getIdCtx().getWorkstation() == null || "".equals(getIdCtx().getWorkstation())) {
            fireSelfFeatureClosingRequired();
        } else {
            ((FProductionContainerMOListVView) getViewerCtrl().getView()).getInputBarCode().addBarCodeActionListener(
                    this);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.LOGICAL_WORKSTATION,
                    getIdCtx().getWorkstation());
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_INITIAL_SELECTON,
                    initialSelection(getIdCtx().getWorkstation()));
            fireSharedContextSend(getSharedContext());
            getViewerCtrl().setFeatureCtrl(this);
            super.initialize();
            fireSharedContextSend(getSharedContext());
            ((FMOListSBean) getGeneralSelectionController().getModel().getBean()).setWorkstationId(getIdCtx()
                    .getWorkstation());
            getView().setErrorPanelAlwaysVisible(false);
            getBrowserController().addTableRowChangeListener(this);
            setTitle(getModel().getTitle());
            ((FMOListSBean) getGeneralSelectionController().getModel().getBean()).setWorkstationId(getIdCtx()
                    .getWorkstation());
            TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL);
            ((FMOListBCtrl) getBrowserController()).changeColumns(touchModel
                    .getTouchModelFunction(TouchModelMapKey.CONTAINER_MO_LIST));
            ((FMOListBIView) ((FMOListBCtrl) getBrowserController()).getView()).initialize();

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */

    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {

        FMOListBBean bean = (FMOListBBean) event.getSelectedBean();
        manageBtnFinishResume(bean);

    }

    /**
     * Sets the btnFinish.
     * 
     * 
     * @param btnFinish btnFinish.
     */
    public void setBtnFinish(final ToolBarBtnStdModel btnFinish) {
        this.btnFinish = btnFinish;
    }

    /**
     * Sets the laborstaffCBS.
     * 
     * @param laborstaffCBS laborstaffCBS.
     */
    public void setLaborstaffCBS(final FLaborStaffCBS laborstaffCBS) {
        this.laborstaffCBS = laborstaffCBS;
    }

    /**
     * Sets the productionMOListCBS.
     * 
     * @param productionMOListCBS productionMOListCBS.
     */
    public void setProductionMOListCBS(final FProductionMOListCBS productionMOListCBS) {
        this.productionMOListCBS = productionMOListCBS;
    }

    /**
     * {@inheritDoc}
     */

    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = manageActionsButtonModels();

        if (USER_VIF.equals(getIdCtx().getLogin()) || USER_VIF_INTEG.equals(getIdCtx().getLogin())) {
            ToolBarBtnStdModel btnDebug = new ToolBarBtnStdModel();
            btnDebug.setReference(ButtonReference.DEBUG_REFERENCE.getReference());
            btnDebug.setText("Maintenance");
            btnDebug.setEnabled(true);
            btnDebug.setIconURL("");
            btnDebug.setFunctionality(true);
            lst.add(btnDebug);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lst;

    }

    /**
     * {@inheritDoc}
     */
    protected FMOListSBean initialSelection(final String workstationId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialSelection(workstationId=" + workstationId + ")");
        }

        FMOListSBean bean = new FMOListSBean();
        bean.setCompanyId(getModel().getIdentification().getCompany());
        bean.setEstablishmentId(getModel().getIdentification().getEstablishment());
        bean.setMoDate(new Date());
        bean.setAll(false);
        bean.setShowFinished(false);
        bean.setWorkstationId(workstationId);
        bean.setMoDate(new Date());

        ArrayList<MOState> list = new ArrayList<MOState>();
        list.add(MOState.STARTED);
        bean.setMoStates(list);
        bean.setActivityItemType(null);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialSelection(workstationId=" + workstationId + ")=" + bean);
        }
        return bean;
    }

    /**
     * {@inheritDoc}
     */

    @Override
    protected void openChildFeature(final String featureId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openChildFeature(featureId=" + featureId + ")");
        }

        if (featureId.equals("VIF.MSOCREAFJ")) {
            MOBean originMOBean = ((FMOListVBean) getViewerController().getBean()).getMobean();
            getSharedContext().put(Domain.DOMAIN_THIS, FMSOCreationBCtrl.ORIGIN_MO_BEAN, originMOBean);
            fireSharedContextSend(getSharedContext());
        } else if (featureId.equals("VIF.MOINCODE1T") || featureId.equals("VIF.MOOUCODE1T")
                || featureId.equals("VIF.MOOUSCRAPDE1T") || featureId.equals("VIF.MOINSCRAPDE1T")) {
            FMOListVBean vBean = (FMOListVBean) getViewerController().getBean();
            FMOListBBean bBean = (FMOListBBean) getBrowserController().getModel().getCurrentBean();
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_LIST_VBEAN, vBean);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_LIST_BBEAN, bBean);
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL, true);
            fireSharedContextSend(getSharedContext());
        } else if (featureId.equals("VIF.LABSTAFF")) {
            List<FMOListBBean> lstBBean = new ArrayList<FMOListBBean>();

            Date datprod = ((FMOListSBean) getBrowserController().getInitialSelection()).getMoDate();
            String refLine = ((FMOListSBean) getBrowserController().getInitialSelection()).getProductionResourceCL()
                    .getCode();

            // if the line is selected, take all the MOs
            if (refLine != null && !"".equals(refLine)) {
                for (Object bbean : getBrowserController().getModel().getBeans()) {
                    lstBBean.add((FMOListBBean) bbean);
                }

            } else {
                // Gets the reference MO

                // either the current one
                FMOListBBean myBBean = (FMOListBBean) getBrowserController().getModel().getCurrentBean();

                // and if no current (no focus on a MO) take the first of the list
                if (myBBean == null) {
                    for (Object bbean : getBrowserController().getModel().getBeans()) {
                        myBBean = (FMOListBBean) bbean;
                        break;
                    }
                }

                if (myBBean != null) {
                    // searches the reference line : the one of the reference MO
                    refLine = getProdLine(myBBean);

                    if (refLine != null && !"".equals(refLine)) {
                        // only keeps the MOs on the reference line (the one of the reference MO)
                        for (Object bbean : getBrowserController().getModel().getBeans()) {
                            FMOListBBean moBean = (FMOListBBean) bbean;

                            String refLine2 = getProdLine(moBean);

                            if (refLine2 != null && refLine.equals(refLine2)) {
                                lstBBean.add(moBean);
                            }
                        }
                    }
                }
            }
            getSharedContext().put(Domain.DOMAIN_THIS, "LSTOF", lstBBean);
            getSharedContext().put(Domain.DOMAIN_THIS, "DATPROD", datprod);
            getSharedContext().put(Domain.DOMAIN_THIS, "LINE", refLine);

            fireSharedContextSend(getSharedContext());
        } else if (featureId.equals(NCConstants.FeatureName.FLOW_TOUCH.getName())
                || featureId.equals(NCConstants.FeatureName.FLOW.getName())) {
            // Non conformity
            FMOListVBean vBean = (FMOListVBean) getViewerController().getBean();
            FMOListBBean bBean = (FMOListBBean) getBrowserController().getModel().getCurrentBean();
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_LIST_VBEAN, vBean);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.MO_LIST_BBEAN, bBean);
            fireSharedContextSend(getSharedContext());
        }

        super.openChildFeature(featureId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openChildFeature(featureId=" + featureId + ")");
        }
    }

    /**
     * Action when the user selects the input button.
     */
    protected void performOpenInput() {
        // If one the item of the activity has a Nature Complementary Workshop.
        // We force the redirection to an other screen, to manage the statement scrap.
        if (((FMOListVBean) getViewerController().getBean()).isStatementScrap(ActivityItemType.INPUT)) {
            // getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL, false);
            // Modify the Feature Id.
            openChildFeature("VIF.MOINSCRAPDE1T");
        } else {
            // getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL, true);
            openChildFeature("VIF.MOINCODE1T");
        }
    }

    /**
     * Action when the user selects the output button.
     */
    protected void performOpenOutput() {
        // If one the item of the activity has a Nature Complementary Workshop.
        // We force the redirection to an other screen, to manage the statement scrap.
        if (((FMOListVBean) getViewerController().getBean()).isStatementScrap(ActivityItemType.OUTPUT)) {
            // getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL, false);
            // Modify the Feature Id.
            openChildFeature("VIF.MOOUSCRAPDE1T");
        } else {
            // getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL, true);
            openChildFeature("VIF.MOOUCODE1T");
        }
    }

    /**
     * 
     * refreshes the browser.
     * 
     * @param selection selection. if null the current slection is re-used.
     * @param bean for repositionning (not mandatory)
     */
    protected void refresh(final Selection selection, final FMOListBBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - refresh(selection=" + selection + ", bean=" + bean + ")");
        }

        FMOListBBean lbean = bean;
        // if (lbean == null) {
        // lbean = (FMOListBBean) getBrowserController().getModel().getCurrentBean();
        // }
        if (selection == null) {
            getBrowserController().reopenQuery();
        } else {
            getBrowserController().reopenQuery(selection);
        }
        if (bean != null) {
            ((FMOListBCtrl) getBrowserController()).setSelectedBean(lbean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - refresh(selection=" + selection + ", bean=" + bean + ")");
        }
    }

    /**
     * 
     * View all documents from the list.
     * 
     * @param listPDFFiles list of pdf files
     */
    protected void viewDocumentList(final List<PDFFile> listPDFFiles) {
        final DisplayListener dispListener = new DisplayListener() {
            @Override
            public void displayRequested(final DisplayEvent event) {
                event.getViewToDisplay().showView();
            }
        };

        final DialogChangeListener dialogListener = new DialogChangeListener() {
            @Override
            public void dialogCancelled(final DialogChangeEvent event) {
                // Nothing to do
            }

            @Override
            public void dialogValidated(final DialogChangeEvent event) {

            }
        };

        final List<ToolBarBtnStdView> list = new ArrayList<ToolBarBtnStdView>();

        for (PDFFile pdfFile : listPDFFiles) {
            TouchToolBarBtnStd btn = new TouchToolBarBtnStd();
            ToolBarBtnStdModel model = new ToolBarBtnStdModel();
            model.setReference(BTN_PDF_LIST_REFERENCE);
            model.setLinkedObject(pdfFile);
            model.setText(pdfFile.getTitle());
            btn.setModel(model);

            list.add(btn);
            ToolBarButtonController ctrl = new ToolBarButtonController();
            ctrl.setView(btn);

            btn.setBtnCtrl(ctrl);
            btn.getBtnCtrl().addButtonListener(this);
        }

        final TouchFunctionalitiesBtnDView dView = new TouchFunctionalitiesBtnDView(
                (Frame) ((JPanel) getView()).getTopLevelAncestor(), list);
        dView.setTitle(I18nClientManager.translate(Jtech.T22109));
        try {
            DialogController controller = null;
            controller = StandardControllerFactory.getInstance().getController(
                    ToolBarFunctionalitiesBtnDController.class);

            controller.setDialogView(dView);
            controller.addDialogChangeListener(dialogListener);
            controller.addDisplayListener(dispListener);
            controller.initialize();
        } catch (final UIException e) {
            LOGGER.error("Error instanciating Dialog :", e);
        }

    }

    /**
     * 
     * view the recette document.
     * 
     * @param pdfFile the file
     */
    protected void viewDocumentRecette(final PDFFile pdfFile) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - viewDocumentRecette(pdfFile=" + pdfFile + ")");
        }

        if (pdfFile != null && !"".equals(pdfFile.getPath())) {
            viewDocument(pdfFile.getPath(), pdfFile.getTitle());
        } else {
            getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                    I18nClientManager.translate(ProductionMo.T30839));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - viewDocumentRecette(pdfFile=" + pdfFile + ")");
        }
    }

    /**
     * Get documents to show.
     * 
     * @return the list of pdf files
     * @throws BusinessException if error occurs
     */
    private List<PDFFile> getDocumentsToShow() throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDocumentsToShow()");
        }

        MOItemKey moItemKey = new MOItemKey();

        moItemKey.setEstablishmentKey(getViewerCtrl().getBean().getMobean().getMoKey().getEstablishmentKey());
        moItemKey.setChrono(getViewerCtrl().getBean().getMobean().getMoKey().getChrono());
        moItemKey.setCounter1(0);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDocumentToShow()");
        }
        return getProductionMOListCBS().getPdfFilesOfCurrentItem(getIdCtx(), moItemKey);
    }

    /**
     * gets the production line of an MO.
     * 
     * @param myBBean : the bbean for which we search the production line
     * @return the production line.
     */
    private String getProdLine(final FMOListBBean myBBean) {
        String refLine = "";

        if (myBBean != null && myBBean.getMobean() != null) {
            if (myBBean.getMobean().getPrimaryRessource() != null
                    && myBBean.getMobean().getPrimaryRessource().getCode() != null
                    && !"".equals(myBBean.getMobean().getPrimaryRessource().getCode())) {
                refLine = myBBean.getMobean().getPrimaryRessource().getCode();
            } else {
                if (myBBean.getMobean().getMoKey() != null) {
                    MOBean moBean;
                    try {
                        moBean = getLaborstaffCBS().getMO(getIdCtx(), myBBean.getMobean().getMoKey());
                        if (moBean != null && moBean.getPrimaryRessource() != null
                                && moBean.getPrimaryRessource().getCode() != null) {
                            refLine = moBean.getPrimaryRessource().getCode();
                        }
                    } catch (BusinessException e) {
                        getView().showError(e.getLocalizedMessage());
                    } catch (BusinessErrorsException e) {
                        getView().showError(e.getLocalizedMessage());
                    }
                }
            }
        }

        return refLine;
    }

    /**
     * Gets the viewer ctrl.
     * 
     * @return the FProductionMOListVCtrl
     */
    private FProductionContainerMOListVCtrl getViewerCtrl() {
        return (FProductionContainerMOListVCtrl) getViewerController();
    }

    /**
     * Interpret the barcode.
     * 
     * @param barcode the barcode
     */
    private void interpretBarcode(final String barcode) {
        EstablishmentKey establishmentKey = new EstablishmentKey(getModel().getIdentification().getCompany(),
                getModel().getIdentification().getEstablishment());
        getViewerCtrl().fireErrorsChanged();
        try {
            MOKey mokey = getProductionMOListCBS().analyseBarcode(getIdCtx(), establishmentKey, barcode);
            if (mokey != null) {
                FMOListBBean moBBean = new FMOListBBean();
                moBBean.setMobean(new MOBean());
                moBBean.getMobean().setMoKey(mokey); // just need the moKey to reposition on the borwser bean.
                ((FMOListBCtrl) getBrowserController()).setSelectedBean(moBBean);
            }
        } catch (BusinessException e) {
            getViewerCtrl().fireErrorsChanged(new UIException(e));
        }
    }

    /**
     * Use to manage the display of the functionalities (Mo Creation, Input Production, Output Production, ...).
     * 
     * @return a list of ToolBarBtnStdBaseModel.
     */
    private List<ToolBarBtnStdBaseModel> manageActionsButtonModels() {
        List<ToolBarBtnStdBaseModel> lstBtn = new ArrayList<ToolBarBtnStdBaseModel>();

        TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL);

        if (touchModel != null) {
            TouchModelFunction touchModelFunction = touchModel
                    .getTouchModelFunction(TouchModelMapKey.CONTAINER_MO_LIST);
            if (touchModelFunction != null) {
                lstBtn = FunctionnalityTools.manageFunctionnalities(TouchModelMapKey.CONTAINER_MO_LIST,
                        touchModelFunction.getFunctionalities());

                for (ToolBarBtnStdBaseModel button : lstBtn) {
                    if (button instanceof ToolBarBtnStdModel
                            && ButtonReference.MO_FINISH_BTN.getReference().equals(
                                    ((ToolBarBtnStdModel) button).getReference())) {
                        setBtnFinish((ToolBarBtnStdModel) button);
                    }
                }

            }
        }

        return lstBtn;
    }

    /**
     * Manage finish resume button.
     * 
     * @param bbean current browser bean
     */
    private void manageBtnFinishResume(final FMOListBBean bbean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageBtnFinishResume(bbean=" + bbean + ")");
        }

        if (bbean != null && getBtnFinish() != null) {
            if (bbean.getIsFinished()) {
                getBtnFinish().setIconURL("/images/vif/vif57/production/mo/resume.png");
                getBtnFinish().setText(I18nClientManager.translate(ProductionMo.T29502, false));
            } else {
                getBtnFinish().setIconURL("/images/vif/vif57/production/mo/finish.png");
                getBtnFinish().setText(I18nClientManager.translate(ProductionMo.T29510, false));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageBtnFinishResume(bbean=" + bbean + ")");
        }
    }

    /**
     * 
     * View a document file.
     * 
     * @param url url file
     * @param title title
     */
    private void viewDocument(final String url, final String title) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - viewDocument(url=" + url + ", title=" + title + ")");
        }

        TouchPDFViewerModel model = new TouchPDFViewerModel();
        model.setTitle(title);
        model.setFilePath(url);
        TouchPDFViewerView view = new TouchPDFViewerView();
        view.setModel(model);
        TouchPDFViewerCtrl controller = new TouchPDFViewerCtrl();
        controller.setDialogView(view);
        controller.initialize();
        view.showView();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - viewDocument(url=" + url + ", title=" + title + ")");
        }
    }

    /**
     * View setting information.
     */
    private void viewSettingInformation() {
        TouchModel touchModel = (TouchModel) getSharedContext().get(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL);
        StringBuilder sb = new StringBuilder();
        sb.append(I18nClientManager.translate(ProductionMo.T34822, false)).append(" : ")
                .append(touchModel.getModelCL().getCode()).append(" - ").append(touchModel.getModelCL().getLabel())
                .append(" (").append(touchModel.getParameterName()).append(") ");
        TouchHelper.showInformation(null, I18nClientManager.translate(ProductionMo.T35724, false), sb.toString());
    }
}

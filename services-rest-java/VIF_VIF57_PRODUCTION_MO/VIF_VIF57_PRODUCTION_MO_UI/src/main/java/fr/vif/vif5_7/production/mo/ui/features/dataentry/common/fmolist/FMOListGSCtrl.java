/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOListGSCtrl.java,v $
 * Created on 14 nov. 08 by alb
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.selection.AbstractGeneralSelectionController;
import fr.vif.vif5_7.activities.activities.business.beans.features.fproductionresource.FProductionResourceSBean;
import fr.vif.vif5_7.activities.activities.business.beans.features.fteam.FTeamSBean;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ResourceType;
import fr.vif.vif5_7.activities.activities.ui.composites.cproductionresource.CProductionResourceCtrl;
import fr.vif.vif5_7.activities.activities.ui.composites.cteam.CTeamCtrl;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.common.fmolist.FMOListCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.touch.FMOListGSView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * General Selection controller.
 * 
 * @author alb
 */
public class FMOListGSCtrl extends AbstractGeneralSelectionController {
    private static final Logger LOGGER = Logger.getLogger(FMOListGSCtrl.class);

    private FMOListCBS          fmoListCBS;

    /**
     * Gets the fmoListCBS.
     * 
     * @category getter
     * @return the fmoListCBS.
     */
    public final FMOListCBS getFmoListCBS() {
        return fmoListCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();

        // Initialisation of bean to load Primary Resource
        FProductionResourceSBean sBeanInit = new FProductionResourceSBean(false, new EstablishmentKey(getIdCtx()
                .getCompany(), getIdCtx().getEstablishment()));
        sBeanInit.setTypeRes(ResourceType.ELEMENTARY_RESOURCE.getValue());
        sBeanInit.setCniv(1);
        sBeanInit.setWorkstationId(getIdCtx().getWorkstation());

        CProductionResourceCtrl c = ((CProductionResourceCtrl) ((FMOListGSView) getView()).getcProductionResource()
                .getController());
        c.setAttribute(sBeanInit);

        // initialize team composite
        FTeamSBean teamSelection = new FTeamSBean(false, null, new EstablishmentKey(getIdCtx().getCompany(), getIdCtx()
                .getEstablishment()));
        ((CTeamCtrl) ((FMOListGSView) getView()).getTeamId().getController()).setAttribute(teamSelection);

        Object o = getSharedContext().get(Domain.DOMAIN_THIS, FProductionConstant.MO_INITIAL_SELECTON);
        getModel().setBean(o);
    }

    /**
     * Sets the fmoListCBS.
     * 
     * @category setter
     * @param fmoListCBS fmoListCBS.
     */
    public final void setFmoListCBS(final FMOListCBS fmoListCBS) {
        this.fmoListCBS = fmoListCBS;
    }

    /**
     * {@inheritDoc}
     */
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException, UIErrorsException {

    }
}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOVModel.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import fr.vif.jtech.ui.feature.UpdateActionRights;
import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOVBean;


/**
 * MarkingMO : Viewer Model.
 * 
 * @author lmo
 */
public class FMarkingMOVModel extends ViewerModel<FMarkingMOVBean> {

    /**
     * Default constructor.
     * 
     */
    public FMarkingMOVModel() {
        super();
        setBeanClass(FMarkingMOVBean.class);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UpdateActionRights getUpdateActionsRights() {
        UpdateActionRights updateActionsRights = super.getUpdateActionsRights();
        if (updateActionsRights != null) {
            updateActionsRights.setDeleteAllowed(false);
            updateActionsRights.setInsertAllowed(false);
            updateActionsRights.setPrintAllowed(false);
            updateActionsRights.setUpdateAllowed(false);
        }

        return updateActionsRights;
    }

}

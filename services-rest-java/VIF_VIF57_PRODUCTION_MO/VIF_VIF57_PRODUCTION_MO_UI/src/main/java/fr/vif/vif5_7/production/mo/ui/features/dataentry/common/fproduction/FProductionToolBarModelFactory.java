/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionToolBarModelFactory.java,v $
 * Created on 27 févr. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction;


import java.util.List;

import fr.vif.jtech.common.util.i18n.SimpleTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;


/**
 * FProductionToolBarModelFactory.
 * 
 * @author vr
 */
public final class FProductionToolBarModelFactory {

    /**
     * 
     * Buttons of the production features.
     * 
     * @author vr
     * @version $Revision: 1.4 $, $Date: 2015/06/11 09:15:00 $
     */
    public enum Buttons {

        BONING_INPUT(ProductionMo.T29499, "BONING_INPUT", "/images/vif/vif57/production/mo/boninginput.png"), //
        BONING_OUTPUT(ProductionMo.T29562, "BONING_OUTPUT", "/images/vif/vif57/production/mo/boningoutput.png"), //
        CANCEL_LAST(ProductionMo.T29538, "CANCEL_LAST", "/images/vif/vif57/production/mo/cancel.png"), //

        CLOSE_CONTAINER(ProductionMo.T29710, "CLOSE_CONTAINER",
                "/images/vif/vif57/production/mo/img48x48/CloseContainer.png"), //
        CREATE_MO(ProductionMo.T29492, "moCreateBtn", "/images/vif/vif57/production/mo/newMO.png"), //
        CREATE_MSO(ProductionMo.T29494, "moCreateTech", "/images/vif/vif57/production/mo/newMSO.png"), //
        DELETE(Jtech.T6422, "DELETE", "/images/vif/vif57/production/mo/cancel.png"), //

        DETAIL(ProductionMo.T29536, "DETAIL", "/images/vif/vif57/production/mo/detail.png"), //
        // Dataenty
        FEATURES(Jtech.T29608, "FEATURES", "/images/vif/vif57/production/mo/features.png"), //
        FINISH_RESUME(ProductionMo.T29510, "FINISH_RESUME", FINISH), //
        INPUT(ProductionMo.T29499, "INPUT", "/images/vif/vif57/production/mo/productioninput.png", "VIF.PRODINCTG"), //

        LOGICAL_WORKSTATION(ProductionMo.T22289, "logicalWorkstation", "/images/vif/vif57/production/mo/logicalWS.png"), //
        OUTPUT(ProductionMo.T29524, "OUTPUT", "/images/vif/vif57/production/mo/productionoutput.png"), //
        // Detail
        PRINT(Jtech.T24936, "PRINT", "/images/vif/vif57/production/mo/print.png"), //
        SHOW_FINISHED_MODE(ProductionMo.T29545, "SHOW_FINISHED_MODE", SHOW_FINISHED_MODE_OFF, true), //

        TO_WEIGH(ProductionMo.T29537, "TO_WEIGH", "/images/vif/vif57/production/mo/scaleweight.png"), //
        TYPE_BARCODE_MODE(ProductionMo.T29547, "TYPE_BARCODE_MODE", TYPE_BARCODE_MODE_KEEP_ICON, true); //

        /**
         * The reference.
         */
        private String            reference  = null;
        /**
         * The text.
         */
        private SimpleTranslation text       = null;
        private boolean           toggleType = false;
        /**
         * Icon url.
         */
        private String            url        = null;

        /**
         * Constructor.
         * 
         * @param text text
         * @param reference reference
         * @param url url
         */
        private Buttons(final SimpleTranslation text, final String reference, final String url) {
            this.text = text;
            this.reference = reference;
            this.url = url;
        }

        /**
         * Constructor.
         * 
         * @param text text
         * @param reference reference
         * @param url url
         * @param toggleType toggle type
         */
        private Buttons(final SimpleTranslation text, final String reference, final String url, final boolean toggleType) {
            this.text = text;
            this.reference = reference;
            this.url = url;
            this.toggleType = toggleType;
        }

        /**
         * Constructor.
         * 
         * @param text text
         * @param reference reference
         * @param url url
         * @param featureId feature id
         */
        private Buttons(final SimpleTranslation text, final String reference, final String url, final String featureId) {
            this.text = text;
            this.reference = reference;
            this.url = url;

        }

        /**
         * Gets the reference.
         * 
         * @category getter
         * @return the reference.
         */
        public String getReference() {
            return reference;
        }

        /**
         * Gets the text.
         * 
         * @category getter
         * @return the text.
         */
        public SimpleTranslation getText() {
            return text;
        }

        /**
         * Gets the url.
         * 
         * @category getter
         * @return the url.
         */
        public String getUrl() {
            return url;
        }

    }

    public static final String FINISH                        = "/images/vif/vif57/production/mo/finish.png";
    public static final String RESUME                        = "/images/vif/vif57/production/mo/resume.png";

    public static final String SHOW_FINISHED_MODE_OFF        = "/images/vif/vif57/production/mo/hidefinished.png";
    public static final String SHOW_FINISHED_MODE_ON         = "/images/vif/vif57/production/mo/showfinished.png";

    public static final String TYPE_BARCODE_MODE_IGNORE_ICON = "/images/vif/vif57/production/mo/noflash.png";
    public static final String TYPE_BARCODE_MODE_KEEP_ICON   = "/images/vif/vif57/production/mo/flash.png";

    /**
     * Constructor.
     */
    private FProductionToolBarModelFactory() {

    }

    /**
     * Create an new Model.
     * 
     * @param button button to create.
     * @return a Model
     */
    public static ToolBarBtnStdModel getModel(final Buttons button) {
        ToolBarBtnStdModel ret = new ToolBarBtnStdModel();
        ret.setText(I18nClientManager.translate(button.text));
        ret.setIconURL(button.url);
        ret.setReference(button.reference);
        ret.setToggleType(button.toggleType);
        return ret;
    }

    /**
     * Look for the model of a button.
     * 
     * @param button type of button to find.
     * @param listModels list of button models
     * @return a Model or null
     */
    public static ToolBarBtnStdModel getToolBarBtnStdModel(final Buttons button,
            final List<ToolBarBtnStdBaseModel> listModels) {
        ToolBarBtnStdModel ret = null;
        for (ToolBarBtnStdBaseModel b : listModels) {
            if (b instanceof ToolBarBtnStdModel
                    && button.getReference().equalsIgnoreCase(((ToolBarBtnStdModel) b).getReference())) {
                ret = (ToolBarBtnStdModel) b;
                break;
            }
        }
        return ret;
    }
}

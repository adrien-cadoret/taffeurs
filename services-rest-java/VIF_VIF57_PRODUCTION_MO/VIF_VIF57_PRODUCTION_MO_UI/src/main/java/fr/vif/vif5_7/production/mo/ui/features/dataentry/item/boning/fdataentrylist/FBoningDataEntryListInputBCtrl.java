/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningDataEntryListInputBCtrl.java,v $
 * Created on 10 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fdataentrydetail.FDataEntryDetailCBS;


/**
 * Boning Data Entry List Input Browser controller.
 * 
 * @author xg
 */
public class FBoningDataEntryListInputBCtrl extends AbstractBrowserController<FBoningDataEntryListInputBBean> {
    /* extends AbstractBrowserController<FDataEntryDetailBBean> */
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FBoningDataEntryListInputBCtrl.class);

    private FDataEntryDetailCBS fdataEntryDetailCBS;

    /**
     * Default constructor.
     */
    public FBoningDataEntryListInputBCtrl() {
        super();
    }

    /**
     * Change the current columns.
     * 
     * @param type type of the movement
     */
    public void changeColumns(final InputEntityType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - changeColumns(type=" + type + ")");
        }

        List<BrowserColumn> cols = FBoningDataEntryListInputColumnsFactory.getColumns(type);
        getModel().removeAllColumns();
        for (BrowserColumn c : cols) {
            getModel().addColumn(c);
        }
        getView().initializeView(this);
        if (getModel().getRowCount() != 0) {
            getModel().setCurrentRowNumber(0);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - changeColumns(type=" + type + ")");
        }
    }

    /**
     * Change the current columns.
     * 
     * @param type type of the movement
     */
    public void changeColumns(final OutputEntityType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - changeColumns(type=" + type + ")");
        }

        List<BrowserColumn> cols = FBoningDataEntryListInputColumnsFactory.getColumns(type);
        getModel().removeAllColumns();
        for (BrowserColumn c : cols) {
            getModel().addColumn(c);
        }
        getView().initializeView(this);
        if (getModel().getRowCount() != 0) {
            getModel().setCurrentRowNumber(0);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - changeColumns(type=" + type + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        return null;
    }

    /**
     * Gets the fdataEntryDetailCBS.
     * 
     * @return the fdataEntryDetailCBS.
     */
    public FDataEntryDetailCBS getFdataEntryDetailCBS() {
        return fdataEntryDetailCBS;
    }

    /**
     * Sets the fdataEntryDetailCBS.
     * 
     * @param fdataEntryDetailCBS fdataEntryDetailCBS.
     */
    public void setFdataEntryDetailCBS(final FDataEntryDetailCBS fdataEntryDetailCBS) {
        this.fdataEntryDetailCBS = fdataEntryDetailCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningDataEntryListInputBBean convert(final Object bean) {
        return (FBoningDataEntryListInputBBean) bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FBoningDataEntryListInputBBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FBoningDataEntryListInputBBean> lst = null;
        try {
            lst = getFdataEntryDetailCBS().queryElements(getIdCtx(),
                    new FBoningDataEntryListInputSBean(((FDataEntryListSBean) selection)), startIndex, rowNumber);
        } catch (BusinessException e) {
            LOGGER.error(e);
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }
        return lst;
    }
}

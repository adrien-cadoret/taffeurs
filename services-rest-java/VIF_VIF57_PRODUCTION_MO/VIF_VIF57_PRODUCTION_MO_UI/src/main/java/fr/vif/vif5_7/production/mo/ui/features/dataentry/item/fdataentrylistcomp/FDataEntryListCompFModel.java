/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListCompFModel.java,v $
 * Created on 26 sept. 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylistcomp;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.GeneralSelectionMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.jtech.ui.selection.GeneralSelectionModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListBModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListGSCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListVModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.touch.FDataEntryListGSView;


/**
 * DataEntryCompList : Feature Model.
 * 
 * @author kl
 */
public class FDataEntryListCompFModel extends StandardFeatureModel {

    public static final String CONTEXT_NATURE = "nature";

    /**
     * Default constructor.
     */
    public FDataEntryListCompFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FDataEntryListBModel.class, null, FDataEntryListCompBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FDataEntryListVModel.class, null, FDataEntryListVCtrl.class));
        setGeneralSelectionTriad(new GeneralSelectionMVCTriad(GeneralSelectionModel.class, FDataEntryListGSView.class,
                FDataEntryListGSCtrl.class));
    }

    @Override
    public BrowserMVCTriad getBrowserTriad() {
        return super.getBrowserTriad();
    }
}

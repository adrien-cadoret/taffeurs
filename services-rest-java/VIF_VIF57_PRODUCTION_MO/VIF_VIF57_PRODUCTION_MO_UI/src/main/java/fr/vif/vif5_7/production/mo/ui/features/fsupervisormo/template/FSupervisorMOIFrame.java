/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOIFrame.java,v $
 * Created on 20 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


import fr.vif.jtech.common.beans.Selection;


/**
 * Interface of the supervisor frame.
 * 
 * @author jd
 */
public interface FSupervisorMOIFrame {

    /**
     * Get the information about the current dockable frame.
     * 
     * @return a {@link MOLocalFrame}
     */
    public MOLocalFrame getLocalFrame();

    /**
     * Initialize.
     */
    public void initialize();

    /**
     * refresh.
     */
    public void refresh();

    /**
     * TODO : write the method's description
     * 
     * @param selection
     */
    void setSelection(Selection selection);

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionBModel.java,v $
 * Created on 17 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction;


import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.FontNames;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninput.FProductionInputBBeanEnum;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;


/**
 * InputOperationItem : Browser Model.
 * 
 * @author vr
 */
public class FProductionBModel extends BrowserModel<FProductionBBean> {

    private FProductionBComparator comparator = new FProductionBComparator();

    /**
     * Default constructor.
     * 
     * FProductionColumnsFactory.manageColumns is called in AbstractFProductionOutputFCtrl.initialize and
     * AbstractFProductionInputFCtrl.initialize to manage columns display
     */
    public FProductionBModel() {
        super();
        setBeanClass(FProductionBBean.class);
        setFullyFetched(true);
        setFetchSize(9999999);
        setComparator(this.comparator);
        setRowHeight(80);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FProductionBBean bean) {
        Format ret = null;
        Format cellFormat1 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
                FontStyle.PLAIN, 14));

        Format cellFormat2 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT,
                FontStyle.BOLD, 14));

        if (property.equals(FProductionInputBBeanEnum.ITEM_SHORT_LABEL.getValue())) {
            ret = cellFormat2;
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                ret.setForegroundColor(StandardColor.BLACK);
            }
        } else if (property.equals(FProductionInputBBeanEnum.ITEM_LONG_LABEL.getValue())) {
            ret = cellFormat1;
            ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.PLAIN, 13));
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                ret.setForegroundColor(StandardColor.BLACK);
            }
        } else if (property.equals(FProductionInputBBeanEnum.ITEM_CODE.getValue())) {
            ret = cellFormat1;
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                ret.setForegroundColor(StandardColor.BLACK);
            }
        } else if (property.equals(FProductionInputBBeanEnum.QTTY_DONE.getValue())
                || property.equals(FProductionInputBBeanEnum.QTTY_TODO.getValue())
                || property.equals(FProductionInputBBeanEnum.QTTY_UNIT.getValue())
                || property.equals(FProductionInputBBeanEnum.QTTY_LEFT_TO_BE.getValue())) {
            ret = cellFormat1;
            if (bean.getItemQttyDoneInRefUnit().concat(bean.getItemQttyToDoInRefUnit()).concat(bean.getItemRefUnit())
                    .length() >= 17
                    || bean.getItemQttyLeftToDoInRefUnit().concat(bean.getItemQttyToDoInRefUnit())
                            .concat(bean.getItemRefUnit()).length() >= 17) {
                ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.PLAIN, 13));
            }
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                if (rowNum == getCurrentRowNumber()) {
                    ret.setForegroundColor(StandardColor.TOUCHSCREEN_FG_LABEL);
                } else {
                    ret.setForegroundColor(StandardColor.FG_DEFAULT);
                }
            }
        }

        return ret;
    }

}

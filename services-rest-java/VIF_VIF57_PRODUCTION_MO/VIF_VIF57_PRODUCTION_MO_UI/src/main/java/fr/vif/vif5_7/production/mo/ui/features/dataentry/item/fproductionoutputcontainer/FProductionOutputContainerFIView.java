/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputContainerFIView.java,v $
 * Created on 25 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer;


import fr.vif.vif5_7.workshop.device.ui.composites.devices.scale.touch.CPerpetualScaleWeightTouchView;


/**
 * ProductionOutputContainer : Feature View Interface.
 * 
 * @author vr
 */
public interface FProductionOutputContainerFIView {

    /**
     * Get scale weight component.
     * 
     * @return scale weights component
     */
    public CPerpetualScaleWeightTouchView getScaleView();

    /**
     * Set the subTitle label.
     * 
     * @param subTitle the subtitle
     */
    public void setSubTitle(final String subTitle);

}

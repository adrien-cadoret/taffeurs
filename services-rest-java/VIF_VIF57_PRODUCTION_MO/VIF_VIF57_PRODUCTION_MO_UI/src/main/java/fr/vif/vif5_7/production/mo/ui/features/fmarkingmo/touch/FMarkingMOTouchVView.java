/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOTouchVView.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch;


import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;

import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.input.swing.SwingInputTextEditor;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.jtech.ui.util.touch.TouchMessageDialog;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.comment.business.beans.common.Comment;
import fr.vif.vif5_7.gen.comment.ui.composites.simplecomment.touch.CCommentTouchView;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOVBean;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.FMarkingMOHeadsCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.FMarkingMOIconeFactory;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.FMarkingMOIconeFactory.FMarkingMOIcone;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.FMarkingMOUIConstants;
import fr.vif.vif5_7.workshop.device.business.beans.common.document.Document;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingError;
import fr.vif.vif5_7.workshop.device.ui.composites.marking.IMarkingHeadsCtrl;
import fr.vif.vif5_7.workshop.device.ui.composites.marking.IMarkingSingleListener;
import fr.vif.vif5_7.workshop.device.ui.composites.marking.MarkingIconFactory;
import fr.vif.vif5_7.workshop.device.ui.composites.marking.touch.MarkingHeadsView;


/**
 * MarkingMO : Viewer View.
 * 
 * @author lmo
 */
public class FMarkingMOTouchVView extends StandardTouchViewer<FMarkingMOVBean> implements FMarkingMOIView {
    private MarkingHeadsView     markingHeadsView;

    private JPanel               jPanelInformations;

    private JPanel               jPanelProcessingMO;
    private JLabel               jLabelProcessingMOHeaderIconContainer;
    private JLabel               jLabelProcessingMOHeaderLabel;
    private JLabel               jLabelProcessingMOChrono;
    private SwingInputTextField  sitfProcessingMOChrono;
    private SwingInputTextField  sitfProcessingMOLabel;

    private JPanel               jPanelProcessingMODocument;
    private JLabel               jLabelProcessingMODocumentLabel;
    private SwingInputTextEditor sitfProcessingMODocuments;

    private JPanel               jPanelSelectedMOWithoutDocument;
    private JPanel               jPanelSelectedMO;
    private JLabel               jLabelSelectedMOHeaderIconContainer;
    private JLabel               jLabelSelectedMOHeaderLabel;
    private JLabel               jLabelSelectedMOChrono;
    private SwingInputTextField  sitfSelectedMOChrono;
    private SwingInputTextField  sitfSelectedMOLabel;

    private JPanel               jPanelSelectedMODocument;
    private JLabel               jLabelselectedMODocument;
    private SwingInputTextEditor sitfSelectedMODocuments;
    private JPanel               jPanelSelectedMOErrors;
    private SwingInputTextEditor sitfSelectedMOErrors;

    private CCommentTouchView    commentTouchView = null;

    /**
     * Default constructor.
     * 
     */
    public FMarkingMOTouchVView() {
        super();
        initialize();
    }

    /**
     * Calculate the String representation of the MarkingError contained in the VBean. <br>
     * Then put this representation in the "markingErrorMessage" attributes of the VBean.
     */
    public void calculateMarkingErrorMessage() {

        MarkingError markingError = getModel().getBean().getMarkingError();

        if (markingError.checkNoError()) {
            getModel().getBean().setMarkingErrorMessage("");
            getJPanelSelectedMOErrors().setBackground(getBackground());
            getSitfSelectedMOErrors().getJTextArea().setBackground(getBackground());
        } else {
            getJPanelSelectedMOErrors().setBackground(Color.red);
            getSitfSelectedMOErrors().getJTextArea().setBackground(Color.red);
            StringBuilder vSB = new StringBuilder();
            final String errorMessage = markingError.getErrorMessage();
            List<Document> documentsMissing = markingError.getDocumentsMissing();
            List<Document> documentsNotReady = markingError.getDocumentsNotReady();
            if (errorMessage != null) {
                vSB.append(errorMessage);
                vSB.append("\n");
            }
            if (!documentsMissing.isEmpty()) {
                vSB.append(I18nClientManager.translate(ProductionMo.T34896, false) + ":");
                boolean vFirstDocument = true;
                for (Document vNotFoundDocument : documentsMissing) {
                    if (vFirstDocument) {
                        vSB.append(" ");
                        vFirstDocument = false;
                    } else {
                        vSB.append("; ");
                    }
                    vSB.append(I18nClientManager.translate(ProductionMo.T34898, false, new VarParamTranslation(
                            vNotFoundDocument.getDocumentCL().getCode()), new VarParamTranslation(vNotFoundDocument
                            .getRoll().getRollCL().getCode())));
                }
                vSB.append("\n");
            }
            if (!documentsNotReady.isEmpty()) {
                vSB.append(I18nClientManager.translate(ProductionMo.T34897, false) + ":");
                boolean vFirstDocument = true;
                for (Document vNotReadyDocument : documentsNotReady) {
                    if (vFirstDocument) {
                        vSB.append(" ");
                        vFirstDocument = false;
                    } else {
                        vSB.append("; ");
                    }
                    vSB.append(I18nClientManager.translate(ProductionMo.T34898, false, new VarParamTranslation(
                            vNotReadyDocument.getDocumentCL().getCode()), new VarParamTranslation(vNotReadyDocument
                            .getRoll().getRollCL().getCode())));
                }
            }
            getModel().getBean().setMarkingErrorMessage(vSB.toString());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IMarkingHeadsCtrl getIMarkingHeadsCtrl() {
        return (IMarkingHeadsCtrl) markingHeadsView.getController();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IMarkingSingleListener getIMarkingSingleListener() {
        return (IMarkingSingleListener) markingHeadsView.getController();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void render() {
        calculateMarkingErrorMessage();
        super.render();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setEnabled(final boolean enabled) {
        if (markingHeadsView != null) {
            markingHeadsView.setEnabled(enabled);
        }
        super.setEnabled(enabled);
    }

    /**
     * Gets the commentView.
     * 
     * @category getter
     * @return the commentView.
     */
    private final CCommentTouchView getCommentView() {
        if (commentTouchView == null) {
            commentTouchView = new CCommentTouchView();
            Dimension d = new Dimension(870, 60);
            commentTouchView.setMaximumSize(d);
            commentTouchView.setMinimumSize(d);
            commentTouchView.setPreferredSize(d);
            commentTouchView.setBeanProperty(FMarkingMOUIConstants.VBEAN_SELECTED_COMMENT);
            commentTouchView.getEditorComment().setVisibleLabel(false);

            // Window opening when mouse click
            commentTouchView.getEditorComment().getJTextArea().addMouseListener(new MouseAdapter() {

                @Override
                public void mouseReleased(final MouseEvent e) {
                    super.mouseReleased(e);
                    if (!((Comment) commentTouchView.getValue()).getCom().equals("")) {
                        TouchMessageDialog.showMessage(commentTouchView,
                                I18nClientManager.translate(Generic.T26356, false),
                                ((Comment) commentTouchView.getValue()).getCom(), MessageButtons.OK);
                    }
                }
            });
        }
        return commentTouchView;
    }

    /**
     * Gets the jLabelProcessingMOChrono.
     * 
     * @return the jLabelProcessingMOChrono.
     */
    private JLabel getJLabelProcessingMOChrono() {
        if (jLabelProcessingMOChrono == null) {
            jLabelProcessingMOChrono = new JLabel(I18nClientManager.translate(ProductionMo.T19272));
        }
        return jLabelProcessingMOChrono;
    }

    /**
     * Gets the jLabelProcessingMODocumentLabel.
     * 
     * @return the jLabelProcessingMODocumentLabel.
     */
    private JLabel getjLabelProcessingMODocumentLabel() {
        if (jLabelProcessingMODocumentLabel == null) {
            jLabelProcessingMODocumentLabel = new JLabel(I18nClientManager.translate(Generic.T29957) + ": ");
        }
        return jLabelProcessingMODocumentLabel;
    }

    /**
     * Gets the jLabelProcessingMOHeaderIconContainer.
     * 
     * @return the jLabelProcessingMOHeaderIconContainer.
     */
    private JLabel getJLabelProcessingMOHeaderIconContainer() {
        if (jLabelProcessingMOHeaderIconContainer == null) {
            jLabelProcessingMOHeaderIconContainer = new JLabel(MarkingIconFactory.getIcon(FMarkingMOIconeFactory.class,
                    FMarkingMOIconeFactory.PATH_16X16, FMarkingMOIcone.PROCESSING_MO.getName()));
        }
        return jLabelProcessingMOHeaderIconContainer;
    }

    /**
     * Gets the jLabelProcessingMOHeaderLabel.
     * 
     * @return the jLabelProcessingMOHeaderLabel.
     */
    private JLabel getJLabelProcessingMOHeaderLabel() {
        if (jLabelProcessingMOHeaderLabel == null) {
            jLabelProcessingMOHeaderLabel = new JLabel(I18nClientManager.translate(ProductionMo.T34916));
            jLabelProcessingMOHeaderLabel.setFont(jLabelProcessingMOHeaderLabel.getFont().deriveFont(Font.BOLD));
        }
        return jLabelProcessingMOHeaderLabel;
    }

    /**
     * Gets the jLabelSelectedMOChrono.
     * 
     * @return the jLabelSelectedMOChrono.
     */
    private JLabel getJLabelSelectedMOChrono() {
        if (jLabelSelectedMOChrono == null) {
            jLabelSelectedMOChrono = new JLabel(I18nClientManager.translate(ProductionMo.T19272));
        }
        return jLabelSelectedMOChrono;
    }

    /**
     * Gets the jLabelselectedMODocument.
     * 
     * @return the JLabelselectedMODocument.
     */
    private JLabel getjLabelselectedMODocument() {
        if (jLabelselectedMODocument == null) {
            jLabelselectedMODocument = new JLabel(I18nClientManager.translate(Generic.T29957) + ": ");
        }
        return jLabelselectedMODocument;
    }

    /**
     * Gets the jLabelSelectedMOHeaderIconContainer.
     * 
     * @return the jLabelSelectedMOHeaderIconContainer.
     */
    private JLabel getjLabelSelectedMOHeaderIconContainer() {
        if (jLabelSelectedMOHeaderIconContainer == null) {
            jLabelSelectedMOHeaderIconContainer = new JLabel(MarkingIconFactory.getIcon(
                    FMarkingMOIconeFactory.PATH_16X16, FMarkingMOIcone.SELECTED_MO.getName()));
        }
        return jLabelSelectedMOHeaderIconContainer;
    }

    /**
     * Gets the jLabelSelectedMOHeaderLabel.
     * 
     * @return the jLabelSelectedMOHeaderLabel.
     */
    private JLabel getJLabelSelectedMOHeaderLabel() {
        if (jLabelSelectedMOHeaderLabel == null) {
            jLabelSelectedMOHeaderLabel = new JLabel(I18nClientManager.translate(ProductionMo.T34917));
            jLabelSelectedMOHeaderLabel.setFont(jLabelSelectedMOHeaderLabel.getFont().deriveFont(Font.BOLD));
        }
        return jLabelSelectedMOHeaderLabel;
    }

    /**
     * Gets the jPanelInformations.
     * 
     * @return the jPanelInformations.
     */
    private JPanel getJPanelInformations() {
        if (jPanelInformations == null) {
            jPanelInformations = new JPanel();

            GridBagLayout gridBagLayout = new GridBagLayout();
            jPanelInformations.setLayout(gridBagLayout);
            GridBagConstraints vGBC = new GridBagConstraints();
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 2;
            vGBC.weighty = 0;
            vGBC.gridx = 0;
            vGBC.gridy = 0;
            jPanelInformations.add(getjPanelSelectedMO(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 1;
            vGBC.weighty = 0;
            vGBC.gridx = 1;
            vGBC.gridy = 0;
            vGBC.ipadx = 25;
            jPanelInformations.add(getJPanelProcessingMO(), vGBC);
        }
        return jPanelInformations;
    }

    /**
     * Gets the jPanelProcessingMO.
     * 
     * @return the jPanelProcessingMO.
     */
    private JPanel getJPanelProcessingMO() {
        if (jPanelProcessingMO == null) {
            jPanelProcessingMO = new JPanel(new GridBagLayout());
            jPanelProcessingMO.setBorder(BorderFactory.createLineBorder(Color.black));

            GridBagConstraints vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 0;
            vGBC.weighty = 0;
            vGBC.insets = new Insets(10, 10, 0, 5);
            vGBC.gridwidth = 1;
            jPanelProcessingMO.add(getJLabelProcessingMOHeaderIconContainer(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 1;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 0;
            vGBC.weighty = 0;
            vGBC.insets = new Insets(10, 5, 0, 0);
            vGBC.gridwidth = 2;
            jPanelProcessingMO.add(getJLabelProcessingMOHeaderLabel(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 1;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 0;
            vGBC.weighty = 0;
            vGBC.insets = new Insets(0, 10, 0, 5);
            vGBC.gridwidth = 2;
            jPanelProcessingMO.add(getJLabelProcessingMOChrono(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 2;
            vGBC.gridy = 1;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 1;
            vGBC.weighty = 0;
            vGBC.insets = new Insets(0, 10, 0, 0);
            vGBC.gridwidth = 1;
            jPanelProcessingMO.add(getSitfProcessingMOChrono(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 2;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 0;
            vGBC.weighty = 0;
            vGBC.insets = new Insets(0, 10, 0, 0);
            vGBC.gridwidth = 3;
            jPanelProcessingMO.add(getSitfProcessingMOLabel(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 3;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 0;
            vGBC.weighty = 1;
            vGBC.insets = new Insets(0, 10, 5, 10);
            vGBC.gridwidth = 3;
            jPanelProcessingMO.add(getJPanelProcessingMODocument(), vGBC);

        }
        return jPanelProcessingMO;
    }

    /**
     * Gets the jPanelProcessingMODocument.
     * 
     * @return the jPanelProcessingMODocument.
     */
    private JPanel getJPanelProcessingMODocument() {
        if (jPanelProcessingMODocument == null) {
            GridBagLayout layout = new GridBagLayout();
            jPanelProcessingMODocument = new JPanel(layout);

            GridBagConstraints vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.weightx = 1;
            vGBC.weighty = 0;
            jPanelProcessingMODocument.add(getjLabelProcessingMODocumentLabel(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 1;
            vGBC.weightx = 1;
            vGBC.weighty = 1;
            vGBC.fill = GridBagConstraints.BOTH;
            jPanelProcessingMODocument.add(getSitfProcessingMODocuments(), vGBC);
        }
        return jPanelProcessingMODocument;
    }

    /**
     * Gets the jPanelSelectedMO.
     * 
     * @return the jPanelSelectedMO.
     */
    private JPanel getjPanelSelectedMO() {
        if (jPanelSelectedMO == null) {
            jPanelSelectedMO = new JPanel(new GridBagLayout());
            jPanelSelectedMO.setBorder(BorderFactory.createLineBorder(Color.black));

            GridBagConstraints vGBC = new GridBagConstraints();

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.gridwidth = 1;
            vGBC.gridheight = 1;
            vGBC.insets = new Insets(5, 10, 5, 0);
            vGBC.weightx = 1;
            vGBC.weighty = 0;
            jPanelSelectedMO.add(getjPanelSelectedMOWithoutDocument(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 1;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.gridwidth = 1;
            vGBC.gridheight = 1;
            vGBC.insets = new Insets(5, 10, 5, 10);
            vGBC.anchor = GridBagConstraints.EAST;
            vGBC.weightx = 0;
            vGBC.weighty = 0;
            jPanelSelectedMO.add(getJPanelSelectedMODocument(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 1;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.gridwidth = 2;
            vGBC.gridheight = 1;
            vGBC.weightx = 0;
            vGBC.weighty = 1;
            jPanelSelectedMO.add(getJPanelSelectedMOErrors(), vGBC);

        }
        return jPanelSelectedMO;
    }

    /**
     * Gets the jPanelSelectedMODocument.
     * 
     * @return the jPanelSelectedMODocument.
     */
    private JPanel getJPanelSelectedMODocument() {
        if (jPanelSelectedMODocument == null) {
            jPanelSelectedMODocument = new JPanel(new GridBagLayout());

            GridBagConstraints vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.NONE;
            vGBC.anchor = GridBagConstraints.WEST;
            vGBC.weightx = 1;
            jPanelSelectedMODocument.add(getjLabelselectedMODocument(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 1;
            vGBC.weightx = 1;
            vGBC.weighty = 1;
            vGBC.fill = GridBagConstraints.NONE;
            vGBC.anchor = GridBagConstraints.WEST;
            jPanelSelectedMODocument.add(getSitfSelectedMODocuments(), vGBC);
        }
        return jPanelSelectedMODocument;
    }

    /**
     * Gets the jPanelSelectedMOErrors.
     * 
     * @return the jPanelSelectedMOErrors.
     */
    private JPanel getJPanelSelectedMOErrors() {
        if (jPanelSelectedMOErrors == null) {
            GridBagLayout layout = new GridBagLayout();
            jPanelSelectedMOErrors = new JPanel(layout);

            GridBagConstraints vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 0;
            vGBC.weightx = 1;
            vGBC.weighty = 1;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.insets = new Insets(10, 10, 10, 10);
            jPanelSelectedMOErrors.add(getSitfSelectedMOErrors(), vGBC);
        }
        return jPanelSelectedMOErrors;
    }

    /**
     * Gets the jPanelSelectedMOWithoutDocument.
     * 
     * @return the jPanelSelectedMOWithoutDocument.
     */
    private JPanel getjPanelSelectedMOWithoutDocument() {
        if (jPanelSelectedMOWithoutDocument == null) {
            jPanelSelectedMOWithoutDocument = new JPanel(new GridBagLayout());

            GridBagConstraints vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.gridwidth = 1;
            vGBC.gridheight = 1;
            vGBC.insets = new Insets(10, 0, 0, 0);
            vGBC.weightx = 0;
            vGBC.weighty = 0;
            jPanelSelectedMOWithoutDocument.add(getjLabelSelectedMOHeaderIconContainer(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 1;
            vGBC.gridy = 0;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.gridwidth = GridBagConstraints.REMAINDER;
            vGBC.gridheight = 1;
            vGBC.insets = new Insets(10, 10, 0, 0);
            vGBC.weightx = 0;
            vGBC.weighty = 0;
            jPanelSelectedMOWithoutDocument.add(getJLabelSelectedMOHeaderLabel(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 1;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.gridwidth = 2;
            vGBC.gridheight = 1;
            vGBC.weightx = 0;
            vGBC.weighty = 0;
            jPanelSelectedMOWithoutDocument.add(getJLabelSelectedMOChrono(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 2;
            vGBC.gridy = 1;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.gridwidth = GridBagConstraints.REMAINDER;
            vGBC.gridheight = 1;
            vGBC.weightx = 1;
            vGBC.weighty = 0;
            jPanelSelectedMOWithoutDocument.add(getSitfSelectedMOChrono(), vGBC);

            vGBC = new GridBagConstraints();
            vGBC.gridx = 0;
            vGBC.gridy = 2;
            vGBC.fill = GridBagConstraints.BOTH;
            vGBC.gridwidth = GridBagConstraints.REMAINDER;
            vGBC.gridheight = 1;
            vGBC.weightx = 0;
            vGBC.weighty = 0;
            jPanelSelectedMOWithoutDocument.add(getSitfSelectedMOLabel(), vGBC);
        }

        return jPanelSelectedMOWithoutDocument;
    }

    /**
     * Gets the markingHeadsView.
     * 
     * @return the markingHeadsView.
     */
    private MarkingHeadsView getMarkingHeadsView() {
        if (markingHeadsView == null) {
            markingHeadsView = new MarkingHeadsView(FMarkingMOHeadsCtrl.class);
            markingHeadsView.setBeanProperty(FMarkingMOUIConstants.VBEAN_DEVICES);
        }
        return markingHeadsView;
    }

    /**
     * Gets the sitfProcessingMOChrono.
     * 
     * @return the sitfProcessingMOChrono.
     */
    private SwingInputTextField getSitfProcessingMOChrono() {

        if (sitfProcessingMOChrono == null) {
            sitfProcessingMOChrono = new SwingInputTextField(String.class, "30", false, null);
            sitfProcessingMOChrono.setBeanProperty(FMarkingMOUIConstants.VBEAN_PROCESSING_CHRONO);
            // focusable and editable = false, enable = true to keep the black foreground color
            sitfProcessingMOChrono.setUpdateable(false);
            sitfProcessingMOChrono.setOpaque(false);
            sitfProcessingMOChrono.setFocusable(false);
            sitfProcessingMOChrono.setAlwaysDisabled(true);
            sitfProcessingMOChrono.getJTextField().setEditable(false);
            sitfProcessingMOChrono.getJTextField().setFocusable(false);
            sitfProcessingMOChrono.getJTextField().setBorder(null);
            sitfProcessingMOChrono.getJTextField().setOpaque(false);
        }
        return sitfProcessingMOChrono;
    }

    /**
     * Gets the sitfProcessingMODocuments.
     * 
     * @return the sitfProcessingMODocuments.
     */
    private SwingInputTextEditor getSitfProcessingMODocuments() {
        if (sitfProcessingMODocuments == null) {
            sitfProcessingMODocuments = new SwingInputTextEditor();
            sitfProcessingMODocuments.setBeanProperty(FMarkingMOUIConstants.VBEAN_PROCESSING_DOCUMENTS);
            // focusable and editable = false, enable = true to keep the black foreground color
            sitfProcessingMODocuments.setUpdateable(false);
            sitfProcessingMODocuments.setOpaque(false);
            sitfProcessingMODocuments.setFocusable(false);
            sitfProcessingMODocuments.setAlwaysDisabled(true);
            sitfProcessingMODocuments.setBorder(null);
            sitfProcessingMODocuments.getJTextArea().setEditable(false);
            sitfProcessingMODocuments.getJTextArea().setFocusable(false);
            sitfProcessingMODocuments.getJTextArea().setBorder(null);
            sitfProcessingMODocuments.getJTextArea().setOpaque(false);
            sitfProcessingMODocuments.getJTextArea().setLineWrap(true);
        }
        return sitfProcessingMODocuments;
    }

    /**
     * Gets the sitfProcessingMOLabel.
     * 
     * @return the sitfProcessingMOLabel.
     */
    private SwingInputTextField getSitfProcessingMOLabel() {
        if (sitfProcessingMOLabel == null) {
            sitfProcessingMOLabel = new SwingInputTextField(String.class, "50", false, null);
            sitfProcessingMOLabel.setBeanProperty(FMarkingMOUIConstants.VBEAN_PROCESSING_OF_LABEL);
            // focusable and editable = false, enable = true to keep the black foreground color
            sitfProcessingMOLabel.setUpdateable(false);
            sitfProcessingMOLabel.setOpaque(false);
            sitfProcessingMOLabel.setFocusable(false);
            sitfProcessingMOLabel.setAlwaysDisabled(true);
            sitfProcessingMOLabel.getJTextField().setEditable(false);
            sitfProcessingMOLabel.getJTextField().setFocusable(false);
            sitfProcessingMOLabel.getJTextField().setBorder(null);
            sitfProcessingMOLabel.getJTextField().setOpaque(false);
        }
        return sitfProcessingMOLabel;
    }

    /**
     * Gets the sitfSelectedMOChrono.
     * 
     * @return the sitfSelectedMOChrono.
     */
    private SwingInputTextField getSitfSelectedMOChrono() {
        if (sitfSelectedMOChrono == null) {
            sitfSelectedMOChrono = new SwingInputTextField(String.class, "30", false, null);
            sitfSelectedMOChrono.setBeanProperty(FMarkingMOUIConstants.VBEAN_SELECTED_CHRONO);
            // focusable and editable = false, enable = true to keep the black foreground color
            sitfSelectedMOChrono.setUpdateable(false);
            sitfSelectedMOChrono.setOpaque(false);
            sitfSelectedMOChrono.setFocusable(false);
            sitfSelectedMOChrono.setAlwaysDisabled(true);
            sitfSelectedMOChrono.getJTextField().setEditable(false);
            sitfSelectedMOChrono.getJTextField().setFocusable(false);
            sitfSelectedMOChrono.getJTextField().setBorder(null);
            sitfSelectedMOChrono.getJTextField().setOpaque(false);
        }
        return sitfSelectedMOChrono;
    }

    /**
     * Gets the sitfSelectedMODocuments.
     * 
     * @return the sitfSelectedMODocuments.
     */
    private SwingInputTextEditor getSitfSelectedMODocuments() {
        if (sitfSelectedMODocuments == null) {
            sitfSelectedMODocuments = new SwingInputTextEditor();
            sitfSelectedMODocuments.setBeanProperty(FMarkingMOUIConstants.VBEAN_SELECTED_DOCUMENTS);
            // focusable and editable = false, enable = true to keep the black foreground color
            sitfSelectedMODocuments.setUpdateable(false);
            sitfSelectedMODocuments.setOpaque(false);
            sitfSelectedMODocuments.setFocusable(false);
            sitfSelectedMODocuments.setAlwaysDisabled(true);
            sitfSelectedMODocuments.getJTextArea().setEditable(false);
            sitfSelectedMODocuments.getJTextArea().setFocusable(false);
            sitfSelectedMODocuments.getJTextArea().setBorder(null);
            sitfSelectedMODocuments.getJTextArea().setOpaque(false);
            sitfSelectedMODocuments.getJTextArea().setLineWrap(true);
            sitfSelectedMODocuments.setPreferredSize(new Dimension(200, 50));
            sitfSelectedMODocuments.setMinimumSize(new Dimension(200, 50));
            sitfSelectedMODocuments.setMaximumSize(new Dimension(200, 50));
        }
        return sitfSelectedMODocuments;
    }

    /**
     * Gets the sitfSelectedMOErrors.
     * 
     * @return the sitfSelectedMOErrors.
     */
    private SwingInputTextEditor getSitfSelectedMOErrors() {
        if (sitfSelectedMOErrors == null) {
            sitfSelectedMOErrors = new SwingInputTextEditor();
            sitfSelectedMOErrors.setBeanProperty(FMarkingMOUIConstants.VBEAN_ERROR_MESSAGE);
            // focusable and editable = false, enable = true to keep the black foreground color
            sitfSelectedMOErrors.setUpdateable(false);
            sitfSelectedMOErrors.setOpaque(false);
            sitfSelectedMOErrors.setFocusable(false);
            sitfSelectedMOErrors.setAlwaysDisabled(true);
            sitfSelectedMOErrors.getJTextArea().setEditable(false);
            // sitfSelectedMOErrors.getJTextArea().setWrapStyleWord(true);
            sitfSelectedMOErrors.getJTextArea().setFocusable(false);
            // the first and only included component (JScrollPane) have the border we want to remove.
            if (sitfSelectedMOErrors.getComponent(0) instanceof JComponent) {
                ((JComponent) sitfSelectedMOErrors.getComponent(0)).setBorder(null);

            }
            sitfSelectedMOErrors.getJTextArea().setOpaque(true);
            sitfSelectedMOErrors.getJTextArea().setLineWrap(true);
            sitfSelectedMOErrors.getJTextArea().setForeground(Color.white);
            sitfSelectedMOErrors.getJTextArea().setFont(
                    sitfSelectedMOErrors.getJTextArea().getFont().deriveFont(Font.BOLD));
            // sitfSelectedMOErrors.getJTextArea().setBackground(Color.red);
        }
        return sitfSelectedMOErrors;
    }

    /**
     * Gets the sitfSelectedMOLabel.
     * 
     * @return the sitfSelectedMOLabel.
     */
    private SwingInputTextField getSitfSelectedMOLabel() {
        if (sitfSelectedMOLabel == null) {
            sitfSelectedMOLabel = new SwingInputTextField(String.class, "50", false, null);
            sitfSelectedMOLabel.setBeanProperty(FMarkingMOUIConstants.VBEAN_SELECTED_OF_LABEL);
            // focusable and editable = false, enable = true to keep the black foreground color
            sitfSelectedMOLabel.setUpdateable(false);
            sitfSelectedMOLabel.setOpaque(false);
            sitfSelectedMOLabel.setFocusable(false);
            sitfSelectedMOLabel.setAlwaysDisabled(true);
            sitfSelectedMOLabel.getJTextField().setEditable(false);
            sitfSelectedMOLabel.getJTextField().setFocusable(false);
            sitfSelectedMOLabel.getJTextField().setBorder(null);
            sitfSelectedMOLabel.getJTextField().setOpaque(false);

        }
        return sitfSelectedMOLabel;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        this.setLayout(gridBagLayout);

        GridBagConstraints vGBC = new GridBagConstraints();
        vGBC.fill = GridBagConstraints.BOTH;
        vGBC.gridx = 0;
        vGBC.gridy = 0;
        vGBC.weighty = 0;
        vGBC.gridwidth = GridBagConstraints.REMAINDER;
        add(getCommentView(), vGBC);

        vGBC = new GridBagConstraints();
        vGBC.fill = GridBagConstraints.BOTH;
        vGBC.gridx = 0;
        vGBC.gridy = 1;
        vGBC.weightx = 1.33;
        vGBC.weighty = 0;
        vGBC.gridwidth = 1;
        add(getjPanelSelectedMO(), vGBC);

        vGBC = new GridBagConstraints();
        vGBC.fill = GridBagConstraints.BOTH;
        vGBC.gridx = 1;
        vGBC.gridy = 1;
        vGBC.weightx = 1;
        vGBC.weighty = 0;
        vGBC.gridwidth = 1;
        add(getJPanelProcessingMO(), vGBC);

        vGBC = new GridBagConstraints();
        vGBC.fill = GridBagConstraints.BOTH;
        vGBC.gridx = 0;
        vGBC.gridy = 2;
        vGBC.weighty = 0;
        vGBC.gridwidth = GridBagConstraints.REMAINDER;
        add(getMarkingHeadsView(), vGBC);
    }
}

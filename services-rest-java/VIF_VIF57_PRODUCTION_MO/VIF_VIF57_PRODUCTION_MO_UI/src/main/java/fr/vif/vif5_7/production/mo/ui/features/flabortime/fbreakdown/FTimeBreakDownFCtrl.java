/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownFCtrl.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown;


import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.beans.DateBoundBean;
import fr.vif.jtech.common.beans.IntegerBoundBean;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.dialogs.DialogModel;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.models.sharedcontext.SharedContext;
import fr.vif.jtech.ui.util.DialogHelper;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif5_7.activities.activities.business.services.composites.cproductionresource.CProductionResourceCBS;
import fr.vif.vif5_7.admin.profile.business.beans.features.freport.FReportSBean;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.IHchyTreeNodeElt;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.IHchyTreeNodeEltInfo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeElt;
import fr.vif.vif5_7.production.mo.business.services.features.ftimebreakdown.FTimeBreakDownCBS;
import fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown.DTimeBreakDownReasonCtrl;
import fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown.DTimeBreakDownSelectionCtrl;
import fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown.swing.DTimeBreakDownReasonView;
import fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown.swing.DTimeBreakDownSelectionView;
import fr.vif.vif5_7.stock.kernel.business.beans.common.reason.ReasonType;
import fr.vif.vif5_7.stock.kernel.business.beans.features.freason.FReasonBBean;
import fr.vif.vif5_7.stock.kernel.business.services.composites.creason.CReasonCBS;


/**
 * TimeBreakDown : Feature Controller.
 * 
 * @author cj
 * @version $Revision: 1.5 $, $Date: 2016/10/07 16:15:06 $
 */
public class FTimeBreakDownFCtrl extends StandardFeatureController implements DialogChangeListener {
    public static final String     BTN_BREAKDOWN         = "btn_breakdown";
    public static final String     BTN_PERIOD_WITHOUT_MO = "btn_periodwithoutMO";
    // DP2192 - CHU - constant for the reports
    public static final String     CR_SELECTION          = "CR";
    public static final String     CR_DOMAIN             = "PRODUCTION_MO";
    private static final Logger    LOGGER                = Logger.getLogger(FTimeBreakDownFCtrl.class);
    private boolean                validDialog           = false;
    private FTimeBreakDownCBS      ftimeBreakDownCBS     = null;
    // DP2192 - CHU - CBSs to control reason and resources
    private CReasonCBS             reasonCBS             = null;
    private CProductionResourceCBS resourceCBS           = null;

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings({ "unchecked", "deprecation" })
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (event.getModel().getReference().equals(BTN_BREAKDOWN)) {
            try {
                FReasonBBean reasonBean = new FReasonBBean();
                DialogHelper.createDialog(getView(), DTimeBreakDownReasonCtrl.class, DialogModel.class,
                        DTimeBreakDownReasonView.class, reasonBean, this, this, getModel().getIdentification(),
                        I18nClientManager.translate(ProductionMo.T34654, false));

                if (reasonBean.getReasonCL() != null && reasonBean.getReasonCL().getCode() != null
                /* && !reasonBean.getReasonCL().getCode().isEmpty() */) { // DP2192 - CHU - breakdown must be done
                    // without reason also

                    // DP2192 - CHU - the reason is valid ?
                    if (!reasonBean.getReasonCL().getCode().isEmpty()) {
                        getReasonCBS().validateBean(getIdCtx(), getIdCtx().getCompany(),
                                ReasonType.RESOURCE.getValue(), reasonBean.getReasonCL());
                    }

                    for (IHchyTreeNodeElt<? extends IHchyTreeNodeEltInfo> labElt : ((FTimeBreakDownVBean) getViewerController()
                            .getBean()).getBreakdownHchyTreeNodeElt().getChildren()) {
                        // remove line which are not selected
                        Iterator<TimeBreakDownHchyTreeNodeElt> itrLine = (Iterator<TimeBreakDownHchyTreeNodeElt>) labElt
                                .getChildren().iterator();
                        while (itrLine.hasNext()) {
                            if (!itrLine.next().getIsSelected()) {
                                itrLine.remove();
                            }
                        }
                    }

                    // remove labor which are not selected
                    Iterator<TimeBreakDownHchyTreeNodeElt> itrLabor = (Iterator<TimeBreakDownHchyTreeNodeElt>) ((FTimeBreakDownVBean) getViewerController()
                            .getBean()).getBreakdownHchyTreeNodeElt().getChildren().iterator();
                    while (itrLabor.hasNext()) {
                        if (!itrLabor.next().getIsSelected()) {
                            itrLabor.remove();
                        }
                    }
                    EstablishmentKey establishmentKey = new EstablishmentKey(getModel().getIdentification()
                            .getCompany(), getModel().getIdentification().getEstablishment());
                    Chrono chrono = getFtimeBreakDownCBS()
                            .breakdownTime(
                                    getIdCtx(),
                                    establishmentKey,
                                    ((FTimeBreakDownSBean) getGeneralSelectionController().getBean()).getProddate(),
                                    (List<TimeBreakDownHchyTreeNodeElt>) ((FTimeBreakDownVBean) getViewerController()
                                            .getBean()).getBreakdownHchyTreeNodeElt().getChildren(),
                                    reasonBean.getReasonCL().getCode());
                    // DP2192 - CHU - no impact on the screen, so no need to refresh
                    // getBrowserController().reopenQuery();
                    // DP2192 - CHU - display the report ?
                    // getView().showInformation(I18nClientManager.translate(ProductionMo.T4701, false));
                    if (chrono != null && chrono.getChrono() != 0) {
                        int ret = getView().showQuestion(
                                I18nClientManager.translate(Jtech.T25073, false),
                                I18nServerManager.translate(
                                        getIdCtx().getLocale(),
                                        false,
                                        Generic.T22975,
                                        new VarParamTranslation(I18nServerManager.translate(getIdCtx().getLocale(),
                                                false, ProductionMo.T5775)),
                                                new VarParamTranslation(chrono.getChrono())), MessageButtons.YES_NO);
                        if (ret == MessageButtons.YES) {
                            getSharedContext().put(
                                    Domain.DOMAIN_THIS,
                                    CR_SELECTION,
                                    new FReportSBean(CR_DOMAIN, chrono.getPrechro().substring(1), new IntegerBoundBean(
                                            chrono.getChrono(), chrono.getChrono()), new DateBoundBean(new Date(),
                                            new Date())));
                            // display report
                            openChildFeature("VIF.CRJ");
                        }
                    }
                }
            } catch (BusinessException e) {
                LOGGER.error("", e);
                getView().showError(e.getMessage());
            }
        } else if (event.getModel().getReference().equals(BTN_PERIOD_WITHOUT_MO)) {
            EstablishmentKey establishmentKey = new EstablishmentKey(getModel().getIdentification().getCompany(),
                    getModel().getIdentification().getEstablishment());
            try {
                Chrono chrono = getFtimeBreakDownCBS().periodWithoutMO(getIdCtx(), establishmentKey,
                        ((FTimeBreakDownSBean) getGeneralSelectionController().getBean()).getProddate());
                // DP2192 - CHU - display the report
                if (chrono != null && chrono.getChrono() != 0) {
                    // getView().showInformation(I18nClientManager.translate(ProductionMo.T32441, false));
                    getSharedContext().put(
                            Domain.DOMAIN_THIS,
                            CR_SELECTION,
                            new FReportSBean(CR_DOMAIN, chrono.getPrechro().substring(1), new IntegerBoundBean(chrono
                                    .getChrono(), chrono.getChrono()), new DateBoundBean(new Date(), new Date())));
                    // display report
                    openChildFeature("VIF.CRJ");
                }
            } catch (BusinessException e) {
                LOGGER.error("", e);
                getView().showError(e.getMessage());
            }
        } else {
            super.buttonAction(event);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogCancelled(event=" + event + ")");
        }
        fireSelfFeatureClosingRequired();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        // DP2192 - CHU - initial resource selection control
        if (event.getDialogBean() instanceof FTimeBreakDownSBean) {
            FTimeBreakDownSBean selBean = (FTimeBreakDownSBean) event.getDialogBean();
            try {
                if (selBean != null && (selBean.getResourceCL() != null || selBean.getLaborCL() != null)) {
                    getFtimeBreakDownCBS().validateResSBean(getIdCtx(), selBean);
                }
            } catch (BusinessException e1) {
                getGeneralSelectionController().getModel().setBean(event.getDialogBean());
                getSharedContext().put(Domain.DOMAIN_THIS, SharedContext.KEY_CALL_PARAM_BEAN, event.getDialogBean());
                fireSharedContextSend(getSharedContext());
                getView().show(new UIException(e1));
                initialize();
                validDialog = false;
            }
        }
        validDialog = true;
    }

    /**
     * Gets the ftimeBreakDownCBS.
     * 
     * @return the ftimeBreakDownCBS.
     */
    public FTimeBreakDownCBS getFtimeBreakDownCBS() {
        return ftimeBreakDownCBS;
    }

    /**
     * Gets the reasonCBS.
     *
     * @return the reasonCBS.
     */
    public CReasonCBS getReasonCBS() {
        return reasonCBS;
    }

    /**
     * Gets the resourceCBS.
     *
     * @return the resourceCBS.
     */
    public CProductionResourceCBS getResourceCBS() {
        return resourceCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        FTimeBreakDownSBean sBean = (FTimeBreakDownSBean) getGeneralSelectionController().getBean();
        if (sBean == null) {
            sBean = new FTimeBreakDownSBean();
        }
        sBean.setEstablishmentKey(new EstablishmentKey(getModel().getIdentification().getCompany(), getModel()
                .getIdentification().getEstablishment()));

        DialogHelper.createDialog(getView(), DTimeBreakDownSelectionCtrl.class, DialogModel.class,
                DTimeBreakDownSelectionView.class, sBean, this, this, getModel().getIdentification(),
                I18nClientManager.translate(Jtech.T8241, false), true);

        if (validDialog) {
            super.initialize();

            getGeneralSelectionController().getModel().setBean(sBean);
            getSharedContext().put(Domain.DOMAIN_THIS, SharedContext.KEY_CALL_PARAM_BEAN, sBean);
            fireSharedContextSend(getSharedContext());
            getGeneralSelectionController().updateBrowseAfterGlobalSelection();
        } else {
            fireSelfFeatureClosingRequired();
        }
    }

    /**
     * Sets the ftimeBreakDownCBS.
     * 
     * @param ftimeBreakDownCBS ftimeBreakDownCBS.
     */
    public void setFtimeBreakDownCBS(final FTimeBreakDownCBS ftimeBreakDownCBS) {
        this.ftimeBreakDownCBS = ftimeBreakDownCBS;
    }

    /**
     * Sets the reasonCBS.
     *
     * @param reasonCBS reasonCBS.
     */
    public void setReasonCBS(final CReasonCBS reasonCBS) {
        this.reasonCBS = reasonCBS;
    }

    /**
     * Sets the resourceCBS.
     *
     * @param resourceCBS resourceCBS.
     */
    public void setResourceCBS(final CProductionResourceCBS resourceCBS) {
        this.resourceCBS = resourceCBS;
    }

}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProcessOrdersBModel.java,v $
 * Created on 07 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fprocessorders;


import java.awt.GradientPaint;
import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.legend.Legend;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.Color;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.sales.act.SalesAct;
import fr.vif.vif5_7.production.mo.business.beans.features.fprocessorders.FProcessOrdersBBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.POIndicatorState;


/**
 * ProcessOrders : Browser Model.
 * 
 * @author jd
 */
public class FProcessOrdersBModel extends BrowserModel<FProcessOrdersBBean> {

    public static final String PRIORITY              = "priority";
    public static final String CHRONO                = "poKey.chrono.chrono";
    public static final String PRECHRO               = "poKey.chrono.prechro";
    public static final String STATE_VALUE           = "state.value";
    public static final String ITEM_CODE             = "itemCL.code";
    public static final String LINE_CODE             = "lineCL.code";
    public static final String BEGIN_DATE            = "effectiveBeginDate";
    public static final String END_DATE              = "effectiveEndDate";

    public static final String EXPECTED_PROCESS_QTY  = "processQty.expectedQty";
    public static final String EFFECTIVE_PROCESS_QTY = "processQty.effectiveQty";
    public static final String UNIT_ID               = "processQty.unitID";

    private Color              notStartedColor       = StandardColor.RED;        // StandardColor.BG_HIGHLIGHT;
    private Color              startedColor          = StandardColor.YELLOW;     // StandardColor.BG_HIGHLIGHT_2;
    private Color              finishInWorkshopColor = StandardColor.PASTEL_BLUE;
    private Color              finishColor           = StandardColor.GREEN;      // StandardColor.BG_HIGHLIGHT_3;

    /**
     * Default constructor.
     * 
     */
    public FProcessOrdersBModel() {
        super();
        setBeanClass(FProcessOrdersBBean.class);

        // Initialize Colors for Browse and Legend
        initializeColors();

        // Add columns
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T32440, false),
                FProcessOrdersBModel.LINE_CODE, 0));
        addColumn(new BrowserColumn(I18nClientManager.translate(SalesAct.T31902, false), FProcessOrdersBModel.PRIORITY,
                0));
        // addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T1755, false), FProcessOrdersBModel.PRECHRO,
        // 0));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T32541, false),
                FProcessOrdersBModel.CHRONO, 0));
        addColumn(new BrowserColumn(I18nClientManager.translate(GenKernel.T29496, false),
                FProcessOrdersBModel.ITEM_CODE, 0));
        addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T31686, false),
                FProcessOrdersBModel.BEGIN_DATE, 0, FProcessOrdersTools.FORMAT_DATE_HOUR));
        addColumn(new BrowserColumn(I18nClientManager.translate(Generic.T31676, false), FProcessOrdersBModel.END_DATE,
                0, FProcessOrdersTools.FORMAT_DATE_HOUR));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T32540, false),
                FProcessOrdersBModel.EFFECTIVE_PROCESS_QTY, 0));
        addColumn(new BrowserColumn(I18nClientManager.translate(ProductionMo.T32539, false),
                FProcessOrdersBModel.EXPECTED_PROCESS_QTY, 0));
        addColumn(new BrowserColumn(I18nClientManager.translate(GenKernel.T112, false), FProcessOrdersBModel.UNIT_ID, 0));
        // addColumn(new BrowserColumn(I18nClientManager.translate(ActivitiesActivities.T30592, false),
        // FProcessOrdersBModel.STATE_VALUE, 0));
        setFetchSize(20);
        setFirstFetchSize(40);
        setSelectionEnabled(true);

        setLegendEnabled(true);

        // Search by chrono not efficient
        setSearchColumn(2);
        setSearchEnabled(false);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FProcessOrdersBBean bean) {

        Format superFormat = super.getCellFormat(rowNum, property, cellContent, bean);
        Format f = new Format(Alignment.DEFAULT_ALIGN, superFormat.getFont());

        if (bean.getState().equals(POIndicatorState.FINISHED)) {
            f.setBackgroundColor(finishColor);
        } else if (bean.getState().equals(POIndicatorState.FINISHED_IN_WORKSHOP)) {
            f.setBackgroundColor(finishInWorkshopColor);
        } else if (bean.getState().equals(POIndicatorState.STARTED)) {
            f.setBackgroundColor(startedColor);
        } else if (bean.getState().equals(POIndicatorState.NOT_STARTED)) {
            f.setBackgroundColor(notStartedColor);
        }

        return f;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Legend> getLegendList() {
        List<Legend> legendList = new ArrayList<Legend>();

        for (POIndicatorState state : POIndicatorState.values()) {
            Legend legend = null;
            if (state.equals(POIndicatorState.NOT_STARTED)) {
                legend = new Legend(state.getValue(), I18nClientManager.translate(state.getName()), notStartedColor);
            } else if (state.equals(POIndicatorState.STARTED)) {
                legend = new Legend(state.getValue(), I18nClientManager.translate(state.getName()), startedColor);
            } else if (state.equals(POIndicatorState.FINISHED_IN_WORKSHOP)) {
                legend = new Legend(state.getValue(), I18nClientManager.translate(state.getName()),
                        finishInWorkshopColor);
            } else if (state.equals(POIndicatorState.FINISHED)) {
                legend = new Legend(state.getValue(), I18nClientManager.translate(state.getName()), finishColor);
            }
            legendList.add(legend);
        }
        return legendList;
    }

    /**
     * Convert Gradient paint to Jtech Color.
     * 
     * @param paint GradientPaint
     * @return fr.vif.jtech.ui.models.format.Color
     */
    private Color initializeColor(final GradientPaint paint) {

        Color color = new Color() {

            @Override
            public int getBlue() {
                return paint.getColor1().getBlue();
            }

            @Override
            public int getGreen() {
                return paint.getColor1().getGreen();
            }

            @Override
            public int getRed() {
                return paint.getColor1().getRed();
            }
        };
        return color;
    }

    /**
     * Convert Awt Color to Jtech Color.
     * 
     * @param color java.awt.Color
     * @return fr.vif.jtech.ui.models.format.Color
     */
    private Color initializeColor(final java.awt.Color color) {
        Color newColor = new Color() {

            @Override
            public int getBlue() {
                return color.getBlue();
            }

            @Override
            public int getGreen() {
                return color.getGreen();
            }

            @Override
            public int getRed() {
                return color.getRed();
            }
        };
        return newColor;
    }

    /**
     * Initialize Colors.
     */
    private void initializeColors() {
        // notStartedColor = initializeColor(MOUIConstants.RED_GREEN_1);
        // startedColor = initializeColor(MOUIConstants.RED_GREEN_5);
        // finishInWorkshopColor = initializeColor(MOUIConstants.RED_GREEN_8);
        // finishColor = initializeColor(MOUIConstants.RED_GREEN_10);
    }
}

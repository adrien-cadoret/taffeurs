/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListVModel.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListVBean;


/**
 * DataEntryList : Viewer Model.
 * 
 * @author vr
 */
public class FDataEntryListVModel extends ViewerModel<FDataEntryListVBean> {

    /**
     * Default constructor.
     *
     */
    public FDataEntryListVModel() {
        super();
        setBeanClass(FDataEntryListVBean.class);
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOListVView.java,v $
 * Created on 14 mar. 2013 by CJ
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist.touch;


import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.BoxLayout;

import fr.vif.jtech.ui.input.touch.TouchInputTextEditor;
import fr.vif.jtech.ui.models.format.StandardFont;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.jtech.ui.util.touch.TouchMessageDialog;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.gen.comment.business.beans.common.Comment;
import fr.vif.vif5_7.gen.comment.ui.composites.simplecomment.touch.CCommentTouchView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;


/**
 * MOList : Viewer View.
 * 
 * @author cj
 */
public class FBoningMOListVView extends StandardTouchViewer<FMOListVBean> {

    private CCommentTouchView    commentTouchView = null;
    private TouchInputTextEditor criteriaLstText  = null;

    /**
     * Default constructor.
     * 
     */
    public FBoningMOListVView() {
        super();
        initialize();
    }

    /**
     * Gets the commentView.
     * 
     * @category getter
     * @return the commentView.
     */
    public final CCommentTouchView getCommentView() {
        if (commentTouchView == null) {
            commentTouchView = new CCommentTouchView();
            Dimension d = new Dimension(870, 75);
            commentTouchView.setMaximumSize(d);
            commentTouchView.setMinimumSize(d);
            commentTouchView.setPreferredSize(d);
            commentTouchView.setBeanProperty("comment");
            commentTouchView.getEditorComment().setVisibleLabel(false);

            // Window opening when mouse click
            commentTouchView.getEditorComment().getJTextArea().addMouseListener(new MouseAdapter() {

                @Override
                public void mouseReleased(final MouseEvent e) {
                    super.mouseReleased(e);
                    if (!((Comment) commentTouchView.getValue()).getCom().equals("")) {
                        TouchMessageDialog.showMessage(commentTouchView,
                                I18nClientManager.translate(Generic.T26356, false),
                                ((Comment) commentTouchView.getValue()).getCom(), MessageButtons.OK);
                    }
                }
            });
        }
        return commentTouchView;
    }

    /**
     * Sets the commentTouchView.
     * 
     * @category setter
     * @param commentTouchView commentTouchView.
     */
    public final void setCommentTouchView(final CCommentTouchView commentTouchView) {
        this.commentTouchView = commentTouchView;
    }

    /**
     * Gets the CriteriaLstText.
     * 
     * @return the TouchInputTextEditor.
     */
    private TouchInputTextEditor getCriteriaLstText() {
        if (criteriaLstText == null) {
            criteriaLstText = new TouchInputTextEditor();
            criteriaLstText.getJTextArea().setOpaque(true);
            criteriaLstText.setBeanBased(true);
            criteriaLstText.setBeanProperty("listCriteria");
            criteriaLstText.setIcon("/images/vif/vif57/production/mo/littleIcon_criteria.png");
            criteriaLstText.setFont(TouchHelper.getFont(StandardFont.TOUCHSCREEN_LABEL_FONT));
            criteriaLstText.setAlwaysDisabled(true);
            criteriaLstText.setUpdateable(false);
            Dimension d = new Dimension(870, 55);
            criteriaLstText.setPreferredSize(d);
            criteriaLstText.setMinimumSize(d);
            criteriaLstText.setMaximumSize(d);
        }
        return criteriaLstText;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(getCriteriaLstText());
        this.add(getCommentView());

    }
}

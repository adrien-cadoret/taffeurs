/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompBView.java,v $
 * Created on 18 sept. 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp.touch;


import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.browser.touch.renderer.row.DefaultRowRenderer;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;


/**
 * FProduction Output Comp Browser view.
 * 
 * @author kl
 */
public class FProductionOutputCompBView extends TouchBrowser<FProductionBBean> {

    /**
     * Default constructor.
     */
    public FProductionOutputCompBView() {
        super();
        ((DefaultRowRenderer) getRowRenderer()).setRendererType(BrowserModel.RENDERER_BY_ROW);
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationBModel.java,v $
 * Created on 19 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


import fr.vif.jtech.ui.browser.touch.TouchBrowserModel;


/**
 * MO creation browser model.
 * 
 * @author glc
 */
public class FMOCreationBModel extends TouchBrowserModel {

    /**
     * Simple constructor.
     */
    public FMOCreationBModel() {
        super();
    }

}

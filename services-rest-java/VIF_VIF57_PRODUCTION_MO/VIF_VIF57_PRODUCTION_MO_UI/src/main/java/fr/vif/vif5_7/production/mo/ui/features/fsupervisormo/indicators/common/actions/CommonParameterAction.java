/*
 * Copyright (c) 2015 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PREPARATION_KERNEL_UI
 * File : $RCSfile: CommonParameterAction.java,v $
 * Created on 19 jun. 2015 by nh
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.actions;


import java.awt.event.ActionEvent;

import javax.swing.AbstractAction;
import javax.swing.Icon;

import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.admin.config.AdminConfig;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.FGanttMOInternalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOIFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;


/**
 * One instance of this class is used to manage click on selection button of the title bar.
 *
 * @author jd
 */
public final class CommonParameterAction extends AbstractAction {

    private static final Icon     SMALL_ICON = FSupervisorMOTools.getIcon16(FSupervisorMOTools.IFRAME_PARAMETER);

    private FGanttMOInternalFrame frame;

    /**
     * Constructor.
     * 
     * @param ctrl Controller of the associated IFrame.
     */
    public CommonParameterAction(final FSupervisorMOIFrame ctrl) {
        if (ctrl instanceof FGanttMOInternalFrame)
            this.frame = (FGanttMOInternalFrame) ctrl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        // frame.refresh();
        if (frame instanceof FGanttMOInternalFrame)
            frame.showParameter();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getValue(final String key) {
        Object ret;

        if (key.equals(FSupervisorMOTools.SMALL_ICON_KEY)) {
            ret = SMALL_ICON;
        } else if (key.equals(FSupervisorMOTools.SHORT_DESCRIPTION_KEY)) {
            ret = I18nClientManager.translate(AdminConfig.T25888);
        } else if (key.equals(ACTION_COMMAND_KEY)) {
            ret = ""; // WMSSupervisorUIConstants.LAUNCH_RESTOCKING;
        } else {
            ret = super.getValue(key);
        }

        return ret;
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryDetailV1NModel.java,v $
 * Created on 6 mai 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput;


import java.util.List;

import fr.vif.jtech.ui.input.InputSpecialViewer1NModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrydetail.FDataEntryDetailBBean;


/**
 * Data entry detail viewer 1N.
 * 
 * @author xg
 */
public class FDataEntryDetailV1NModel extends InputSpecialViewer1NModel<FDataEntryDetailBBean> {

    /**
     * Constructor.
     * 
     * @param browserColumns browser columns
     * @param detailBeanClass detail bean class
     */
    @SuppressWarnings("unchecked")
    public FDataEntryDetailV1NModel(final List browserColumns, final Class detailBeanClass) {
        super(browserColumns, detailBeanClass);

        // Rend les boutons (ajouter, supprimer, mise à jour, vers le haut, vers le bas) invisibles.
        setButtonsVisibilityState(InputSpecialViewer1NModel.BTN_NONE_VISIBLE);

        // Rend les éléments du menu (ajouter, supprimer, mise à jour, vers le haut, vers le bas) visibles.
        setVisibleMenuItems(false);

    }

}

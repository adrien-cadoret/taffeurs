/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: LabelPrintHelper.java,v $
 * Created on 19 nov. 08 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.flabels;


import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.KeyboardBean;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.controllers.Controller;
import fr.vif.jtech.ui.dialogs.DialogView;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.input.kbdialog.NumKeyboardDialogController;
import fr.vif.jtech.ui.models.identification.Identification;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.dialogs.DefaultDialogDisplayListener;
import fr.vif.jtech.ui.views.View;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.SearchLabel;
import fr.vif.vif5_7.production.mo.business.beans.common.labels.LabelEvent;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.common.flabels.DLabelCBS;


/**
 * Helper to print.
 * 
 * @author gv
 */
public class LabelPrintHelper implements DialogChangeListener, Controller {

    private static final Logger LOGGER       = Logger.getLogger(LabelPrintHelper.class);

    private static String       propertyFile = "labels.properties";
    private DLabelCBS           labelCBS     = null;
    private boolean             print        = false;
    private DLabelsIView        dlabelsIView = null;
    private boolean             initialized  = false;

    /**
     * default ctor.
     */
    public LabelPrintHelper() {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        print = false;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        print = true;
    }

    /**
     * Gets the labelCBS.
     * 
     * @category getter
     * @return the labelCBS.
     */
    public DLabelCBS getLabelCBS() {
        return labelCBS;
    }

    /**
     * launch the print procedure.
     * 
     * @param source the dialog source
     * @param identification the identification
     * @param idContext the context
     * @param labelEvent the label event
     * @throws UIException e
     */
    public void launchPrint(final View source, final Identification identification, final IdContext idContext,
            final LabelEvent labelEvent) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - launchPrint(source=" + source + ", identification=" + identification + ", idContext="
                    + idContext + ", labelEvent=" + labelEvent + ")");
        }

        if (!initialized) {
            initialize();
            initialized = true;
        }
        try {

            List<SearchLabel> listSearchLabel = getLabelCBS().searchAndPrintLabels(idContext, labelEvent);

            for (SearchLabel searchLabel : listSearchLabel) {
                if (searchLabel.getAskForCopiesNumber()) {
                    print = false;

                    NumKeyboardDialogController controller = new NumKeyboardDialogController();
                    controller.addDisplayListener(DefaultDialogDisplayListener.getInstance());
                    controller.addDialogChangeListener(this);
                    DialogView dv = dlabelsIView.getLabelsKeyboardView(I18nClientManager.translate(ProductionMo.T29514,
                            false) + " (" + searchLabel.getLabelId() + ")");
                    controller.setDialogView(dv);
                    controller.initialize();
                    if (print) {
                        getLabelCBS().runPrintLabel(idContext, labelEvent, searchLabel,
                                (Integer) ((KeyboardBean) controller.getModel().getDialogBean()).getValue());
                    }
                }
            }
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - launchPrint(source=" + source + ", identification=" + identification + ", idContext="
                    + idContext + ", labelEvent=" + labelEvent + ")");
        }
    }

    /**
     * Sets the labelCBS.
     * 
     * @category setter
     * @param labelCBS labelCBS.
     */
    public void setLabelCBS(final DLabelCBS labelCBS) {
        this.labelCBS = labelCBS;
    }

    /**
     * loads the necessary dialog.
     * 
     * @throws UIException e
     */
    private void initialize() throws UIException {
        try {
            Properties properties = new Properties();
            properties.load(LabelPrintHelper.class.getClassLoader().getResourceAsStream(LabelPrintHelper.propertyFile));
            String viewType = System.getProperty("vif_uitype");

            String strClass = (String) properties.get(viewType);

            Class clazz = Class.forName(strClass);
            dlabelsIView = (DLabelsIView) clazz.newInstance();

        } catch (FileNotFoundException e) {
            LOGGER.error(e);
            throw new UIException(e, Jtech.T9159);
        } catch (IOException e) {
            LOGGER.error(e);
            throw new UIException(e, Jtech.T9159);
        } catch (ClassNotFoundException e) {
            LOGGER.error(e);
            throw new UIException(e, Jtech.T9159);
        } catch (IllegalAccessException e) {
            LOGGER.error(e);
            throw new UIException(e, Jtech.T9159);
        } catch (InstantiationException e) {
            LOGGER.error(e);
            throw new UIException(e, Jtech.T9159);
        }

    }

}

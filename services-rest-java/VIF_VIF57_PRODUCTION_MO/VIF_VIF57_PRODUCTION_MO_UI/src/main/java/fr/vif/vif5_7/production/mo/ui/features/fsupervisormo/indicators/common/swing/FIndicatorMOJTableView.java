/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOJTableView.java,v $
 * Created on 2 May 2007 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.swing;


import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JPopupMenu;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ScrollPaneConstants;
import javax.swing.table.TableCellRenderer;

import org.apache.log4j.Logger;

import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOValues;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FIndicatorMOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FIndicatorMOValuesVBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOJTableModel;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FIndicatorMOPopupManager;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FProductivityTableHeaderRenderer;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FSupervisorMOPanel;


/**
 * 
 * The view of the workstation configuration.
 * 
 * @author jd
 */
public class FIndicatorMOJTableView extends FSupervisorMOPanel implements MouseListener, ActionListener {
    /** LOGGER. */
    private static final Logger     LOGGER    = Logger.getLogger(FIndicatorMOJTableView.class);

    private Class                   cellClass = null;
    private JTable                  jTable    = null;
    private Point                   lastPoint;
    private JPopupMenu              popup     = null;
    private JScrollPane             scrollPane;
    private FIndicatorMOValuesVBean vbean     = new FIndicatorMOValuesVBean();

    /**
     * Constructor.
     */
    public FIndicatorMOJTableView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(e=" + e + ")");
        }

        int rowNumber = getJTable().rowAtPoint(lastPoint);

        GenericActionEvent event = new GenericActionEvent(getJTable(), GenericActionEvent.EVENT_ACTION_PERFORMED,
                e.getActionCommand(), vbean.getValues().get(rowNumber));

        fireGenericAction(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final MouseEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - mouseClicked(e=" + e + ")");
        }

        if (e.getSource() != null && e.getButton() == 1 && e.getClickCount() == 2) {
            JTable src = (JTable) e.getSource();

            if (src.getSelectedRow() > -1) {
                GenericActionEvent event = new GenericActionEvent(src, GenericActionEvent.EVENT_ACTION_PERFORMED,
                        "CLICK_ON_CELL", vbean.getValues().get(src.getSelectedRow()));

                fireGenericAction(event);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - mouseClicked(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseEntered(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseExited(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final MouseEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - mousePressed(e=" + e + ")");
        }

        if (e.getSource() != null && e.getButton() == 3) {

            lastPoint = new Point(e.getX(), e.getY());
            popup.show(this, e.getX(), e.getY());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - mousePressed(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final MouseEvent e) {

    }

    /**
     * Sets the list.
     * 
     * @category setter
     * @param vBean vbean.
     */
    public void setViewerBean(final FIndicatorMOValuesVBean vBean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setViewerBean(vBean=" + vBean + ")");
        }

        this.vbean = vBean;

        getJTable().setModel(new FIndicatorMOJTableModel(vbean.getValues()));

        ((FIndicatorMOJTableModel) getJTable().getModel()).fireTableDataChanged();
        getScrollPane().validate();
        updateUI();

        getJTable().getColumnModel().getColumn(0).setHeaderValue(vbean.getTitle());

        getJTable().repaint();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setViewerBean(vBean=" + vBean + ")");
        }
    }

    /**
     * This method initializes jTable.
     * 
     * @return javax.swing.JTable
     */
    private JTable getJTable() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getJTable()");
        }

        if (jTable == null) {
            TableCellRenderer renderer = new TableCellRenderer() {

                public Component getTableCellRendererComponent(final JTable table, final Object value,
                        final boolean isSelected, final boolean hasFocus, final int row, final int column) {

                    IndicatorMOValues i = (IndicatorMOValues) value;

                    FIndicatorMOICell j = null;

                    cellClass = FIndicatorMO2ValuesCell.class;
                    try {
                        j = (FIndicatorMOICell) cellClass.newInstance();
                        // TITLE (i.getCodeLabel().getLabel());
                        ((Component) j).addMouseListener(FIndicatorMOJTableView.this);

                        // j.setTypeOfInfo(typInfo);

                        j.setValues(i);
                    } catch (InstantiationException e) {
                        LOGGER.error(e.getMessage());
                    } catch (IllegalAccessException e) {
                        LOGGER.error(e.getMessage());
                    }

                    return (Component) j;
                }

            };
            jTable = new JTable(new FIndicatorMOJTableModel());
            // jTable.setFocusable(false);
            FProductivityTableHeaderRenderer render = new FProductivityTableHeaderRenderer(this);

            jTable.getTableHeader().setDefaultRenderer(render);
            jTable.getTableHeader().setReorderingAllowed(false);
            jTable.setShowGrid(true);
            jTable.setRowHeight(40);
            jTable.addMouseListener(this);
            jTable.setOpaque(true);
            jTable.setBackground(Color.WHITE);
            jTable.setDefaultRenderer(FIndicatorMOBean.class, renderer);
            // jTable.setOpaque(false);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getJTable()=" + jTable);
        }
        return jTable;
    }

    /**
     * Get the ScrollPane.
     * 
     * @return a {@link JScrollPane}
     */
    private JScrollPane getScrollPane() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getScrollPane()");
        }

        if (scrollPane == null) {
            scrollPane = new JScrollPane();
            GridBagConstraints gbcjTable = new GridBagConstraints();
            gbcjTable.insets = new Insets(0, 0, 5, 0);
            gbcjTable.fill = GridBagConstraints.BOTH;
            gbcjTable.weighty = 1.0;
            gbcjTable.weightx = 1.0;
            gbcjTable.gridy = 0;
            gbcjTable.gridx = 0;
            // scrollPane.setSize(new Dimension(100, 400));
            scrollPane.setOpaque(false);
            scrollPane.getViewport().add(getJTable(), gbcjTable);
            scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
            scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
            // scrollPane.add(getJTable(), gbcjTable);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getScrollPane()=" + scrollPane);
        }
        return scrollPane;
    }

    /**
     * This method initializes this.
     * 
     */
    private void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        popup = FIndicatorMOPopupManager.getInstance().createPopupMenu(this);
        this.setPreferredSize(new Dimension(100, 400));
        this.setFocusable(false);

        this.setOpaque(false);

        GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.rowWeights = new double[] { 0.0 };
        gridBagLayout.columnWeights = new double[] { 1.0 };
        this.setLayout(gridBagLayout);

        GridBagConstraints gbcscrollPane = new GridBagConstraints();
        gbcscrollPane.weighty = 1.0;
        gbcscrollPane.weightx = 1.0;
        gbcscrollPane.fill = GridBagConstraints.BOTH;
        gbcscrollPane.gridx = 0;
        gbcscrollPane.gridy = 0;
        add(getScrollPane(), gbcscrollPane);

        getJTable().setSize(this.getWidth(), this.getHeight());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

}

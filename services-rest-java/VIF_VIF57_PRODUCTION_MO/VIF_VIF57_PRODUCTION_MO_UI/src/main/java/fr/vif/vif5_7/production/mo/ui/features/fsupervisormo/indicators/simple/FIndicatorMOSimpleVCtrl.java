/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FIndicatorMOSimpleVCtrl.java,v $
 * Created on 22 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.simple;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FIndicatorMOValuesVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSelectionBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FIndicatorMOVCtrlAdaptor;


/**
 * Findicator viewer controller.
 * 
 * @author jd
 */
public class FIndicatorMOSimpleVCtrl extends FIndicatorMOVCtrlAdaptor<FIndicatorMOValuesVBean> {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FIndicatorMOSimpleVCtrl.class);

    /**
     * {@inheritDoc}
     * 
     * @param sel
     */
    public FIndicatorMOValuesVBean convert(final IdContext idctx, final FSupervisorMOSelectionBean sel)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(idctx=" + idctx + ", sel=" + sel + ")");
        }

        FIndicatorMOValuesVBean vbean = null;

        try {
            vbean = getProductivityMOCBS().getViewerBeanWithCumulatedValues(idctx, sel);

        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(idctx=" + idctx + ", sel=" + sel + ")=" + vbean);
        }
        return vbean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setBeanAndInitialBean(final FIndicatorMOValuesVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setBeanAndInitialBean(bean=" + bean + ")");
        }

        super.setBeanAndInitialBean(bean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setBeanAndInitialBean(bean=" + bean + ")");
        }
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingButtonComboBoxModel.java,v $
 * Created on 16 mai 2016 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import fr.vif.jtech.ui.input.InputComboBoxModel;


/**
 * Combo Box Model for functionalities tools.
 * 
 * @author cj
 */
public class FTableLookSettingButtonComboBoxModel extends InputComboBoxModel {

    /**
     * Superclass constructor.
     */
    public FTableLookSettingButtonComboBoxModel() {
        super();
    }

}

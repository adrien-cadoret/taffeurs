/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLabelBCtrl.java,v $
 * Created on 17 Jun 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.flabels;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.flabels.FLabelBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.flabels.FLabelSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.flabels.FLabelsCBS;


/**
 * FLabel browser controller.
 * 
 * @author xg
 */
public class FLabelBCtrl extends AbstractBrowserController<FLabelBBean> {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(FLabelBCtrl.class);

    private FLabelsCBS          flabelsCBS;

    /**
     * Def constr.
     */
    public FLabelBCtrl() {
        // nothing to do
    }

    /**
     * Gets the flabelsCBS.
     * 
     * @return the flabelsCBS.
     */
    public FLabelsCBS getFlabelsCBS() {
        return flabelsCBS;
    }

    /**
     * Sets the flabelsCBS.
     * 
     * @param flabelsCBS flabelsCBS.
     */
    public void setFlabelsCBS(final FLabelsCBS flabelsCBS) {
        this.flabelsCBS = flabelsCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FLabelBBean> queryElements(final Selection selection, final int startIndex, final int rowNumber)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FLabelBBean> list = new ArrayList<FLabelBBean>();

        try {
            list = getFlabelsCBS().queryElements(getIdCtx(), (FLabelSBean) selection, startIndex, rowNumber);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + list);
        }
        return list;
    }

}

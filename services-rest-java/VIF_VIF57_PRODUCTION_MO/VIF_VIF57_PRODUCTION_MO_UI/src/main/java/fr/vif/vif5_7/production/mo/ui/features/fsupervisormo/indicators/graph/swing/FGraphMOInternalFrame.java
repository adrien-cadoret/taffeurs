/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGraphMOInternalFrame.java,v $
 * Created on 21 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

import org.apache.log4j.Logger;

import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.mvc.MVCTriad;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGraphMOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOGraphSelectionBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOActionManager;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.FIndicatorMOVModel;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.FGraphMOVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.GraphMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.IGraphMOView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing.AbstractMODockableView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;


/**
 * Frame containing a graph.
 * 
 * @author jd
 */
public class FGraphMOInternalFrame extends AbstractMODockableView<FGraphMOBean, FGraphMOVCtrl<FGraphMOBean>> {
    /** LOGGER. */
    private static final Logger       LOGGER    = Logger.getLogger(FGraphMOInternalFrame.class);

    private FIndicatorMOActionManager actionManager;
    private FGraphMOBean              graphBean = new FGraphMOBean();

    /**
     * Constructor.
     * 
     * @param frame {@link MOLocalFrame}
     */
    @SuppressWarnings("unchecked")
    public FGraphMOInternalFrame(final MOLocalFrame frame) {
        super(frame, new MVCTriad(FIndicatorMOVModel.class, GraphMOView.class, FGraphMOVCtrl.class));

        JMenu menuAction = FSupervisorMOTools.getGanttMenuAction((ActionListener) getViewerController().getView());

        ((IGraphMOView) getViewerController().getView()).addPopupMenu(menuAction);

        ((IGraphMOView) getViewerController().getView()).addGenericActionListener(this);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(e=" + e + ")");
        }

        if (e.getSource() instanceof JMenuItem) {
            GenericActionEvent event = new GenericActionEvent(this, e.getActionCommand(), e.getActionCommand(),
                    getLocalFrame());
            actionPerformed(event);
        } else {
            super.actionPerformed(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        if (actionManager == null) {
            actionManager = new FIndicatorMOActionManager(getParentController());
        }
        //
        actionManager.actionPerformed(event, getLocalFrame(), this, null);

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void convert() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert()");
        }

        FSupervisorMOGraphSelectionBean sel = (FSupervisorMOGraphSelectionBean) getLocalFrame().getSelection();
        try {
            graphBean = getViewerController().convert(getParentController().getIdCtx(), sel);

            ((IGraphMOView) getViewerController().getView())
            .setGraph(GraphMOTools.convertFGraphToGraph(graphBean, sel));

            getViewerController().getView().render();
        } catch (UIException e) {
            getViewerController().getView().show(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert()");
        }
    }
}

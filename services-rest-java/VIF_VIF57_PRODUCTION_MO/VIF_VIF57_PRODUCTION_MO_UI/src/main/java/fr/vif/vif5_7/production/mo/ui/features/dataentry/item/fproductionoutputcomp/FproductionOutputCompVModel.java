/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FproductionOutputCompVModel.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompVBean;


/**
 * Production Output Scrap Viewer Model.
 * 
 * @author kl
 */
public class FproductionOutputCompVModel extends ViewerModel<FProductionOutputCompVBean> {
    /**
     * Default constructor.
     * 
     */
    public FproductionOutputCompVModel() {
        super();
        setBeanClass(FProductionOutputCompVBean.class);
    }
}

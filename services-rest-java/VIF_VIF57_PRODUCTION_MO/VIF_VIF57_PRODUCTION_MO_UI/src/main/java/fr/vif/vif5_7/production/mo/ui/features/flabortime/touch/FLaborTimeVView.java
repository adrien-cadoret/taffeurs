/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeVView.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.touch;


import java.awt.Dimension;
import java.awt.Rectangle;
import java.util.ArrayList;
import java.util.List;

import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.input.touch.TouchInputSpecialViewer1N;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.RoundBackgroundPanelBorder;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBeanFields.LaborStaffBrowseColumns;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.FLaborStaffV1NCtrl;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.FLaborStaffV1NModel;


/**
 * labor staff viewer view.
 * 
 * @author nle
 */
public class FLaborTimeVView extends StandardTouchViewer<FLaborTimeBBean> {

    private TouchInputSpecialViewer1N v1NLaborStaffList;

    /**
     * Constructor.
     * 
     */
    public FLaborTimeVView() {
        super();
        initialize();
    }

    /**
     * Gets the v1NLaborStaffList.
     * 
     * @return the v1NLaborStaffList.
     */
    private TouchInputSpecialViewer1N getV1NLaborStaffList() {
        if (v1NLaborStaffList == null) {
            List<BrowserColumn> lstSel = new ArrayList<BrowserColumn>();
            lstSel.add(new BrowserColumn(I18nClientManager.translate(ProductionMo.T34758, false),
                    LaborStaffBrowseColumns.RESOURCE_CODE.getValue(), 15));
            lstSel.add(new BrowserColumn(I18nClientManager.translate(Generic.T1863, false),
                    LaborStaffBrowseColumns.RESOURCE_LABEL.getValue(), 100));
            lstSel.add(new BrowserColumn(I18nClientManager.translate(ProductionMo.T34746, false),
                    LaborStaffBrowseColumns.STAFF.getValue(), 10));

            FLaborStaffV1NModel model = new FLaborStaffV1NModel(lstSel, FLaborStaffBBean.class);
            v1NLaborStaffList = new TouchInputSpecialViewer1N<FLaborStaffBBean>(model, FLaborStaffV1NCtrl.class);
            v1NLaborStaffList.setBeanProperty("laborStaffList");
            v1NLaborStaffList.setFocusable(false);
            v1NLaborStaffList.setFocusTraversalKeysEnabled(false);
            v1NLaborStaffList.setAlwaysDisabled(false);
            v1NLaborStaffList.setRowHeight(45);
            v1NLaborStaffList.setBounds(new Rectangle(1045, 275));
        }
        return v1NLaborStaffList;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setLayout(null);
        setPreferredSize(new Dimension(870, 530));
        setBorder(new RoundBackgroundPanelBorder(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR));
        add(getV1NLaborStaffList());
    }

}

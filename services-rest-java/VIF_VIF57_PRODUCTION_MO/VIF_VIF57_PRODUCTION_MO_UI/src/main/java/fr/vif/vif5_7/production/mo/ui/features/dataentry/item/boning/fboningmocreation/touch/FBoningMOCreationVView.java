/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOCreationVView.java,v $
 * Created on 18 avr. 13 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation.touch;


import java.awt.Color;
import java.awt.Font;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.logging.Log;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.filter.AbstractIntuitiveInputPropertyFilter;
import fr.vif.jtech.ui.filter.FilterController;
import fr.vif.jtech.ui.filter.ReflectionIntuitiveInputPropertyFilter;
import fr.vif.jtech.ui.filter.touch.TouchFilterPanel;
import fr.vif.jtech.ui.input.touch.TouchInputBarCode;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.system.SystemHelper;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.activities.activities.ui.composites.coperatingprocess.touch.COperatingProcessView;
import fr.vif.vif5_7.activities.activities.ui.composites.cstackingplan.touch.CStackingPlanView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.service.ui.composites.cquantityunit.touch.CQuantityUnitView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmocreation.FBoningMOCreationVBean;
import fr.vif.vif5_7.production.mo.ui.composites.barcode.BarCodeCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation.FBoningMOCreationVModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationItemBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationItemBModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationVIView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.touch.FMOCreationItemBView;


/**
 * MO creation viewer.
 * 
 * @author glc
 */
public class FBoningMOCreationVView extends StandardTouchViewer<FBoningMOCreationVBean> implements DisplayListener,
        FMOCreationVIView {
    /**
     * Logger.
     */
    private static final Logger                  LOGGER                = Logger.getLogger(FBoningMOCreationVView.class);

    /**
     * Controller of the browser.
     */
    private AbstractBrowserController<CodeLabel> browserController;
    /**
     * Model of the browser.
     */
    private BrowserModel<CodeLabel>              browserModel;
    /**
     * Triad of the browser.
     */
    private BrowserMVCTriad                      browserTriad;

    private CQuantityUnitView                    cQuantityUnit         = null;

    private COperatingProcessView                coperatingProcessView = null;
    private CStackingPlanView                    cStackingPlanView     = null;
    private JPanel                               jpItem                = null;
    private TouchBrowser<CodeLabel>              tbHelp                = null;
    private TouchFilterPanel<CodeLabel>          touchFilterPanel      = null;
    private JLabel                               jlFlash               = null;
    private JPanel                               jpFlash               = null;
    private TouchInputBarCode                    touchLEDInputBarCode  = null;

    /**
     * Simple constructor.
     */
    public FBoningMOCreationVView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayRequested(final DisplayEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableComponents(final boolean creationMode) {
        super.enableComponents(creationMode);

        if (getModel().getBean() == null || getModel().getBean().getFabrication() == null
                || getModel().getBean().getFabrication().getFabricationCLs() == null
                || getModel().getBean().getFabrication().getFabricationCLs().getCode() == null
                || getModel().getBean().getFabrication().getFabricationCLs().getCode().isEmpty()) {
            disableComponents();
        } else {
            if (getModel().getBean().getActivityForProcess() != null
                    && !getModel().getBean().getActivityForProcess().isEmpty()) {
                getCoperatingProcessView().setAlwaysDisabled(false);
                getCoperatingProcessView().getCodeView().setEnabled(true);
            } else {
                getCoperatingProcessView().setAlwaysDisabled(true);
                getCoperatingProcessView().getCodeView().setAlwaysDisabled(true);
                getCoperatingProcessView().getCodeView().setEnabled(false);
            }
        }

        this.render();
    }

    /**
     * Gets the controller of the browser.
     * 
     * @return the controller of the browser.
     */
    @SuppressWarnings("unchecked")
    public final AbstractBrowserController<CodeLabel> getBrowserController() {
        if (browserController == null) {
            try {
                browserController = StandardControllerFactory.getInstance().getController(
                        browserTriad.getControllerClass());
            } catch (UIException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser UI error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            }
        }
        return browserController;
    }

    /**
     * Gets the browserTriad.
     * 
     * @category getter
     * @return the browserTriad.
     */
    public BrowserMVCTriad getBrowserTriad() {
        if (browserTriad == null) {
            browserTriad = new BrowserMVCTriad(FMOCreationItemBModel.class, FMOCreationItemBView.class,
                    FMOCreationItemBCtrl.class);
        }
        return browserTriad;
    }

    /**
     * Gets the coperatingProcessView.
     * 
     * @return the coperatingProcessView.
     */
    public COperatingProcessView getCoperatingProcessView() {
        if (coperatingProcessView == null) {
            coperatingProcessView = new COperatingProcessView();
            coperatingProcessView.setBeanProperty(FBoningMOCreationVModel.OPERATING_PROCESS_CL);
            // coperatingProcessView.setVisible(false);
            // coperatingProcessView.setAlwaysDisabled(true);
            // coperatingProcessView.setEnabledForCreate(true);
        }
        return coperatingProcessView;
    }

    /**
     * Gets the cStackingPlanView.
     * 
     * @return the cStackingPlanView.
     */
    public CStackingPlanView getcStackingPlanView() {
        if (cStackingPlanView == null) {
            cStackingPlanView = new CStackingPlanView();
            cStackingPlanView.setBeanProperty(FBoningMOCreationVModel.STACKING_PLAN_CL);
        }
        return cStackingPlanView;
    }

    /**
     * Get the touch led input barcode.
     * 
     * @return the touch led input bar code
     */
    public TouchInputBarCode getInputBarCode() {
        if (touchLEDInputBarCode == null) {
            touchLEDInputBarCode = new TouchInputBarCode(BarCodeCtrl.class);
            touchLEDInputBarCode.setDefaultBorder(new LineBorder(TouchHelper
                    .getAwtColor(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR), 1, false));
            touchLEDInputBarCode.setBorder(new LineBorder(TouchHelper
                    .getAwtColor(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR), 1, false));
            touchLEDInputBarCode.setBounds(64, 10, 340, 30);
            touchLEDInputBarCode.setBeanProperty("barcode");

        }
        return touchLEDInputBarCode;
    }

    /**
     * Get the label flash.
     * 
     * @return the label flash
     */
    public JLabel getJlFlash() {
        if (jlFlash == null) {
            jlFlash = new JLabel();
            jlFlash.setForeground(new Color(238, 127, 0));
            jlFlash.setFont(new Font("Arial", Font.BOLD, 16));
            jlFlash.setText(I18nClientManager.translate(ProductionMo.T35271, false));
            jlFlash.setBounds(10, 10, 48, 30);
        }
        return jlFlash;
    }

    /**
     * 
     * Get Flash panel.
     * 
     * @return the flash panel
     */
    public JPanel getJpFlash() {
        if (jpFlash == null) {
            jpFlash = new JPanel();
            final GridBagLayout gridBagLayout = new GridBagLayout();
            gridBagLayout.columnWidths = new int[] { 0, 7 };
            jpFlash.setLayout(gridBagLayout);
            jpFlash.setOpaque(false);
            final GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.insets = new Insets(0, 10, 0, 5);
            gridBagConstraints.anchor = GridBagConstraints.EAST;
            gridBagConstraints.weighty = 1;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.gridx = 0;
            jpFlash.add(getJlFlash(), gridBagConstraints);
            final GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.insets = new Insets(0, 10, 0, 0);
            gridBagConstraints1.weighty = 1;
            gridBagConstraints1.fill = GridBagConstraints.BOTH;
            gridBagConstraints1.weightx = 1;
            gridBagConstraints1.gridy = 0;
            gridBagConstraints1.gridx = 1;
            jpFlash.add(getInputBarCode(), gridBagConstraints1);
        }
        return jpFlash;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {
        super.initializeView();
        tbHelp.getModel().setIdentification(getModel().getIdentification());
        browserController.setIdentification(getModel().getIdentification());
        getBrowserController().initialize();
        getBrowserController().requestFocus();
        try {
            getBrowserController().changeCurrentRow(0);
        } catch (UIVetoException e) {
            LOGGER.error("", e);
        }
    }

    /**
     * Sets the cStackingPlanView.
     * 
     * @param cStackingPlanView cStackingPlanView.
     */
    public void setcStackingPlanView(final CStackingPlanView cStackingPlanView) {
        this.cStackingPlanView = cStackingPlanView;
    }

    /**
     * {@inheritDoc}
     */
    public void setFlashLabel(final String flashLabel) {
        getJlFlash().setText(flashLabel);

    }

    /**
     * Gets the model of the browser.
     * 
     * @return the model of the browser.
     */
    @SuppressWarnings("unchecked")
    protected final BrowserModel<CodeLabel> getBrowserModel() {
        if (browserModel == null) {
            try {
                browserModel = getBrowserTriad().getModelClass().newInstance();
            } catch (InstantiationException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser instantiation error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            } catch (IllegalAccessException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser illegal access error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            }
        }
        return browserModel;
    }

    /**
     * Gets the cQuantityUnit.
     * 
     * @category getter
     * @return the cQuantityUnit.
     */
    private CQuantityUnitView getCQuantityUnit() {
        if (cQuantityUnit == null) {
            cQuantityUnit = new CQuantityUnitView();
            cQuantityUnit.setBeanProperty(FBoningMOCreationVModel.QUANTITY_UNIT);
            cQuantityUnit.setMandatory(true);
        }
        return cQuantityUnit;
    }

    /**
     * This method initializes jpItem.
     * 
     * @return javax.swing.JPanel.
     */
    private JPanel getJpItem() {
        if (jpItem == null) {
            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.gridx = 1;
            gridBagConstraints1.insets = new Insets(0, 0, 0, 0);
            gridBagConstraints1.anchor = GridBagConstraints.NORTH;
            gridBagConstraints1.weighty = 1.0;
            gridBagConstraints1.fill = GridBagConstraints.VERTICAL;
            gridBagConstraints1.gridy = 0;
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.insets = new Insets(0, 0, 0, 10);
            gridBagConstraints.anchor = GridBagConstraints.NORTH;
            gridBagConstraints.gridy = 0;
            jpItem = new JPanel();
            jpItem.setOpaque(false);
            jpItem.setLayout(new GridBagLayout());
            jpItem.add(getTbHelp(), gridBagConstraints);
            jpItem.add(getTouchFilterPanel(), gridBagConstraints1);
        }
        return jpItem;
    }

    /**
     * This method initializes tbHelp.
     * 
     * @return fr.vif.jtech.ui.browser.touch.TouchBrowser.
     */
    @SuppressWarnings("unchecked")
    private TouchBrowser<CodeLabel> getTbHelp() {
        if (tbHelp == null) {
            try {
                tbHelp = (TouchBrowser<CodeLabel>) getBrowserTriad().getViewClass().newInstance();
                tbHelp.setModel(getBrowserModel());
                getBrowserController().setView(tbHelp);
                // tbHelp.setAutoValidateOnSelect(true);

                // This display listener transfers display events to the recorded displayer after setting this dialog as
                // the owner.
                getBrowserController().addDisplayListener(this);
            } catch (InstantiationException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser instanciation error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            } catch (IllegalAccessException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser illegal access error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            }
        }
        return tbHelp;
    }

    /**
     * This method initializes touchFilterPanel.
     * 
     * @return fr.vif.jtech.ui.filter.touch.TouchFilterPanel.
     */
    private TouchFilterPanel getTouchFilterPanel() {
        if (touchFilterPanel == null) {
            AbstractIntuitiveInputPropertyFilter<CodeLabel> filter;
            filter = new ReflectionIntuitiveInputPropertyFilter<CodeLabel>(getBrowserModel().getBeanPropertyAt(
                    getBrowserModel().getSearchColumn()));
            touchFilterPanel = new TouchFilterPanel<CodeLabel>(filter, getBrowserModel().isSelectionEnabled());
            FilterController<CodeLabel, AbstractIntuitiveInputPropertyFilter<CodeLabel>> filterController = new FilterController<CodeLabel, AbstractIntuitiveInputPropertyFilter<CodeLabel>>();
            filterController.setView(touchFilterPanel);
            filterController.addFilterListener(getBrowserController());
            touchFilterPanel.setVisible(!SystemHelper.getUseKeyboard());
        }
        return touchFilterPanel;
    }

    /**
     * This method initializes this.
     */
    private void initialize() {
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 0;
        gridBagConstraints1.gridwidth = 3;
        gridBagConstraints1.fill = GridBagConstraints.BOTH;
        gridBagConstraints1.weightx = 0.5;
        gridBagConstraints1.weighty = 0.8;

        GridBagConstraints gridBagConstraintsFlash = new GridBagConstraints();
        gridBagConstraintsFlash.gridx = 0;
        gridBagConstraintsFlash.gridy = 1;
        gridBagConstraintsFlash.gridwidth = 3;
        gridBagConstraintsFlash.fill = GridBagConstraints.BOTH;
        gridBagConstraintsFlash.weightx = 0.5;
        gridBagConstraintsFlash.weighty = 0.1;
        gridBagConstraintsFlash.insets = new Insets(10, 0, 0, 0);

        GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 2;
        gridBagConstraints2.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints2.anchor = GridBagConstraints.NORTH;
        gridBagConstraints2.weightx = 0.33;
        gridBagConstraints2.weighty = 0.1;
        gridBagConstraints2.insets = new Insets(17, 10, 10, 10);

        GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
        gridBagConstraints3.gridx = 1;
        gridBagConstraints3.gridy = 2;
        gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints3.anchor = GridBagConstraints.NORTH;
        gridBagConstraints3.weightx = 0.33;
        gridBagConstraints3.weighty = 0.1;
        gridBagConstraints3.insets = new Insets(10, 10, 10, 10);

        GridBagConstraints gridBagConstraints4 = new GridBagConstraints();
        gridBagConstraints4.gridx = 2;
        gridBagConstraints4.gridy = 2;
        gridBagConstraints4.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints4.anchor = GridBagConstraints.NORTH;
        gridBagConstraints4.weightx = 0.33;
        gridBagConstraints4.weighty = 0.1;
        gridBagConstraints4.insets = new Insets(10, 10, 10, 10);

        this.setLayout(new GridBagLayout());
        this.add(getJpItem(), gridBagConstraints1);
        this.add(getJpFlash(), gridBagConstraintsFlash);
        this.add(getCQuantityUnit(), gridBagConstraints2);
        this.add(getcStackingPlanView(), gridBagConstraints3);
        this.add(getCoperatingProcessView(), gridBagConstraints4);
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FContentRateFCtrl.java,v $
 * Created on 15 juil. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent;


import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateSBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fcontent.FContentRateCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent.touch.FContentRateBView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent.touch.FContentRateFView;


/**
 * Feature controller for content feature.
 * 
 * @author cj
 */
public class FContentRateFCtrl extends StandardFeatureController implements TableRowChangeListener {

    /** Validate button reference. */
    public static final String  BTN_VALIDATE         = "BTN_VALIDATE";

    /** Calculate button reference. */
    public static final String  BTN_CALCULATE        = "BTN_CALCULATE";
    public static final String  BTN_UP               = "BTN_UP_BATCH";
    public static final String  BTN_DOWN             = "BTN_DOWN_BATCH";
    public static final String  FEATURE_CONTENT_CODE = "VIF.RDTEN";

    /** LOGGER. */
    private static final Logger LOGGER               = Logger.getLogger(FContentRateFCtrl.class);
    private ToolBarBtnStdModel  btnValidate;
    private ToolBarBtnStdModel  btnCalculate;

    private FContentRateCBS     fcontentRateCBS;

    private ToolBarBtnStdModel  btnUp;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        super.buttonAction(event);

        if (BTN_VALIDATE.equals(event.getModel().getReference())) {
            try {
                getFcontentRateCBS().validateContentRate(getIdCtx(),
                        (FContentRateSBean) getGeneralSelectionController().getBean(),
                        ((FContentRateBCtrl) getBrowserController()).getModel().getBeans());

                fireSelfFeatureClosingRequired();
            } catch (BusinessException e) {
                LOGGER.error("", e);
                show(new UIException(e));
            }
        } else if (BTN_CALCULATE.equals(event.getModel().getReference())) {
            try {
                ((FContentRateBCtrl) getBrowserController())
                        .updateBean((FContentRateSBean) getGeneralSelectionController().getBean());
            } catch (UIException e) {
                show(e);
            }
        } else if (BTN_DOWN.equals(event.getModel().getReference())) {

            FContentRateBBean currentBean = ((FContentRateBCtrl) getBrowserController()).getModel().getCurrentBean();
            int currentOrder = currentBean.getOrder();
            int currentIndex = getBrowserController().getModel().getCurrentRowNumber();

            FContentRateBBean nextBean = null;
            if (currentIndex + 1 < getBrowserController().getModel().getRowCount()) {
                nextBean = ((FContentRateBCtrl) getBrowserController()).getModel().getBeanAt(currentIndex + 1);
                int nextOrder = nextBean.getOrder();

                if (nextBean != null) {
                    nextBean.setOrder(currentOrder);
                    currentBean.setOrder(nextOrder);
                    ((FContentRateBCtrl) getBrowserController()).verticalMove(currentIndex, currentIndex + 1);
                }
            }
        } else if (BTN_UP.equals(event.getModel().getReference())) {
            FContentRateBBean currentBean = ((FContentRateBCtrl) getBrowserController()).getModel().getCurrentBean();
            int currentOrder = currentBean.getOrder();
            int currentIndex = getBrowserController().getModel().getCurrentRowNumber();

            FContentRateBBean previousBean = null;
            if (currentIndex > 0) {
                previousBean = ((FContentRateBCtrl) getBrowserController()).getModel().getBeanAt(currentIndex - 1);
                int previousOrder = previousBean.getOrder();

                if (previousBean != null) {
                    previousBean.setOrder(currentOrder);
                    currentBean.setOrder(previousOrder);
                    ((FContentRateBCtrl) getBrowserController()).verticalMove(currentIndex, currentIndex - 1);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayRequested(final DisplayEvent event) {
        if ("currentRate.qty".equals(event.getEventName())) {
            if (((FContentRateSBean) getGeneralSelectionController().getBean()).getCurrentRate().getQty() > 0) {
                getBtnCalculate().setEnabled(true);
                getBtnValidate().setEnabled(true);
            } else {
                getBtnCalculate().setEnabled(false);
                getBtnValidate().setEnabled(false);
            }
        } else {
            super.displayRequested(event);
        }
    }

    /**
     * Gets the btnCalculate.
     * 
     * @return the btnCalculate.
     */
    public ToolBarBtnStdModel getBtnCalculate() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnCalculate()");
        }

        if (this.btnCalculate == null) {
            this.btnCalculate = new ToolBarBtnStdModel();
            this.btnCalculate.setReference(BTN_CALCULATE);
            this.btnCalculate.setIconURL("/images/vif/vif57/production/mo/img48x48/process.png");
            this.btnCalculate.setText(I18nClientManager.translate(ProductionMo.T34256, false));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnCalculate()=" + btnCalculate);
        }
        return btnCalculate;
    }

    /**
     * Gets the btnValidate.
     * 
     * @return the btnValidate.
     */
    public ToolBarBtnStdModel getBtnValidate() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnValidate()");
        }

        if (this.btnValidate == null) {
            this.btnValidate = new ToolBarBtnStdModel();
            this.btnValidate.setReference(BTN_VALIDATE);
            this.btnValidate.setIconURL("/images/vif/vif57/production/mo/ok.png");
            this.btnValidate.setText(I18nClientManager.translate(ProductionMo.T29582, false));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnValidate()=" + btnValidate);
        }
        return btnValidate;
    }

    /**
     * Gets the fcontentRateCBS.
     * 
     * @return the fcontentRateCBS.
     */
    public FContentRateCBS getFcontentRateCBS() {
        return fcontentRateCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        FContentRateSBean sBean = (FContentRateSBean) getSharedContext()
                .get(Domain.DOMAIN_PARENT, FEATURE_CONTENT_CODE);
        if (sBean != null) {
            try {
                FContentRateSBean sBeanUpdated = getFcontentRateCBS().getSelectionBean(getIdCtx(), sBean);
                getBrowserController().setInitialSelection(sBeanUpdated);
                getBrowserController().setCurrentSelection(sBeanUpdated);
                ((FContentRateBView) getBrowserController().getView()).initialize();

                getSharedContext().put(Domain.DOMAIN_PARENT, FEATURE_CONTENT_CODE, sBeanUpdated);

                getBtnCalculate().setEnabled(false);
                getBtnValidate().setEnabled(false);
            } catch (BusinessException e) {
                LOGGER.error("", e);
                show(new UIException(e));
            }
        }
        getBrowserController().addTableRowChangeListener(this);
        super.initialize();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        if (getBrowserController().getModel().getCurrentRowNumber() == getBrowserController().getModel().getRowCount() - 1) {
            ((FContentRateFView) getView()).getBtnDown().setEnabled(false);
        } else {
            ((FContentRateFView) getView()).getBtnDown().setEnabled(true);
        }

        if (getBrowserController().getModel().getCurrentRowNumber() == 0) {
            ((FContentRateFView) getView()).getBtnUp().setEnabled(false);
        } else {
            ((FContentRateFView) getView()).getBtnUp().setEnabled(true);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        // Nothing to do
    }

    /**
     * Sets the fcontentRateCBS.
     * 
     * @param fcontentRateCBS fcontentRateCBS.
     */
    public void setFcontentRateCBS(final FContentRateCBS fcontentRateCBS) {
        this.fcontentRateCBS = fcontentRateCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getExternalButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getExternalButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = super.getExternalButtonModels();
        lst.add(getBtnCalculate());
        lst.add(getBtnValidate());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getExternalButtonModels()=" + lst);
        }
        return lst;
    }
}

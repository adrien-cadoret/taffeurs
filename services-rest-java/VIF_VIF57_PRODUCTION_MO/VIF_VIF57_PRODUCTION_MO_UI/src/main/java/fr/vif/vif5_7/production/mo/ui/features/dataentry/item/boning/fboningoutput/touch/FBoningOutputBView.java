/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputBView.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput.touch;


import java.awt.Color;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;

import javax.swing.BorderFactory;

import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.browser.touch.BrowserCell;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.browser.touch.renderer.row.DefaultRowRenderer;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBeanEnum;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput.FBoningOutputBModel;


/**
 * FProduction input browser view.
 * 
 * @author vr
 */
public class FBoningOutputBView extends TouchBrowser<FBoningOutputBBean> {
    /**
     * Cell renderer for the browser, with custom styles.
     * 
     * 
     * @author xg
     */
    private class CellRenderer<BBean> extends DefaultRowRenderer {

        private static final int    ALPHA                = 75;
        private static final String BG_SELECTED_CELL     = "#F1C40F";
        private static final String TOP_BAND_BY_CATEGORY = "#00C0E4";
        private static final String TOP_BAND_RECREATED   = "#ADCC19";
        private static final String BOTTOM_BAND          = "#F7F7F7";
        private static final String FINISHED_ITEM        = "#BFBFBF";
        private static final String WHITE                = "#FFFFFF";
        private static final String FG_QUANTITY          = "#EE7F00";

        private final URL           urlPuceCat           = FBoningOutputBView.class
                .getResource("/images/vif/vif57/production/mo/img8x8/puce_cat.png");
        private final URL           urlPuceComp          = FBoningOutputBView.class
                .getResource("/images/vif/vif57/production/mo/img8x8/puce_comp.png");
        private final URL           urlSPCB              = FBoningOutputBView.class
                .getResource("/images/vif/vif57/production/mo/img8x8/SPCB.png");

        /**
         * {@inheritDoc}
         */
        @Override
        public String render(final int rowNum) {
            for (BrowserCell<FBoningOutputBBean> cell : getBrowserCells()) {
                cell.setBorder(BorderFactory.createLineBorder(new Color(128, 128, 128), 1));
            }
            FBoningOutputBBean bean = ((FBoningOutputBModel) getBrowserModel()).getBeanAt(rowNum);

            StringBuilder sb = new StringBuilder();
            if (bean != null) {
                String backgroundColor = null;

                File f = new File("C:/iconViande/" + bean.getItemCLs().getCode() + ".png");
                URL urlIcon = null;
                if (f.exists()) {
                    try {
                        urlIcon = f.toURI().toURL();
                    } catch (MalformedURLException e) {
                        urlIcon = null;
                    }
                }

                if (getBrowserModel().getCurrentBean() != null && getBrowserModel().getCurrentBean().equals(bean)) {
                    backgroundColor = BG_SELECTED_CELL;
                } else {
                    backgroundColor = WHITE;
                }

                sb.append("<html>\n").append("<body");

                sb.append(" bgcolor=\"").append(backgroundColor).append('\"');
                if (urlIcon != null) {
                    sb.append(" background=\"").append(urlIcon).append('\"');
                }
                sb.append(" style=\"filter: alpha(opacity=").append(ALPHA).append("); -moz-opacity: .").append(ALPHA)
                .append("; opacity: .").append(ALPHA)
                .append(";background-repeat: no-repeat;background-position:50% 50%;margin:0;padding:0;\"");
                sb.append(">\n");

                sb.append("<table style=\"width: 165px; border-collapse:collapse;\">\n");
                sb.append("<div id=\"cadre\" style=\"width: 100%;\" >\n");

                int colCount = getBrowserModel().getColumnCount();

                for (int i = 0; i < colCount; i++) {
                    sb.append(getColumnHtml(rowNum, i));
                }

                sb.append("</table>");
                sb.append("</body>\n</html>\n");
            }
            return sb.toString();

        }

        /**
         * Renders a row in a browser as an HTML description.
         * 
         * @param rowNum the number of the row.
         * @param colNum the number of the column.
         * @return an HTML rendering of the cell.
         */
        private StringBuilder getColumnHtml(final int rowNum, final int colNum) {
            StringBuilder ret = new StringBuilder();
            @SuppressWarnings("unchecked")
            BrowserModel<BBean> browserModel = getBrowserModel();
            FBoningOutputBBean bean = (FBoningOutputBBean) browserModel.getBeanAt(rowNum);
            Object cellContent = browserModel.getCellContent(rowNum, colNum);

            boolean isFinished = ((bean.getItemQttyDoneInRefUnitQU() != null && bean.getItemQttyToDoInRefUnitQU() != null) && !(bean
                    .getItemQttyDoneInRefUnitQU().getQty() < bean.getItemQttyToDoInRefUnitQU().getQty()))
                    || ManagementType.ARCHIVED.equals(bean.getOperationItemBean().getOperationItemKey()
                            .getManagementType());

            if (cellContent != null) {

                String strCellContext = TouchHelper.forHTML(cellContent.toString());

                String str = getBrowserModel().getBeanAttributeAt(colNum);

                if (str != null) {
                    if (str.equals(FBoningOutputBBeanEnum.HASSTACKINGPLAN.getValue())) {
                        Boolean hasStackingPlan = (Boolean) cellContent;
                        if (isFinished) {
                            ret.append("<tr style=\"color: ").append(WHITE).append("; background-color: ")
                            .append(FINISHED_ITEM).append(" \">\n");
                            ret.append("<td style=\"text-align: left;height:20px;\">\n");
                            if (hasStackingPlan) {
                                ret.append("<img src=\"" + urlPuceComp + "\" />").append("\n");
                            } else {
                                ret.append("<img src=\"" + urlPuceCat + "\" />").append("\n");
                            }
                        } else if (hasStackingPlan) {
                            ret.append("<tr style=\"color: ").append(WHITE).append("; background-color: ")
                            .append(TOP_BAND_RECREATED).append(" \">\n");
                            ret.append("<td style=\"text-align: left;height:20px;\">\n");
                            ret.append("<img src=\"" + urlPuceComp + "\" />").append("\n");
                        } else {
                            ret.append("<tr style=\"color: ").append(WHITE).append("; background-color: ")
                            .append(TOP_BAND_BY_CATEGORY).append(" \">\n");
                            ret.append("<td style=\"text-align: left;height:20px;\">\n");
                            ret.append("<img src=\"" + urlPuceCat + "\" />").append("\n");
                        }
                    } else if (str.equals(FBoningOutputBBeanEnum.ITEM_CODE.getValue())) {
                        ret.append(
                                "<span style=\"font-size: 12px; font-family: 'Arial'; color: rgb( 255, 255, 255 ); font-style: italic;\">")
                                .append("&nbsp;").append(strCellContext).append("</span>\n");
                        ret.append("</td>").append("\n");

                    } else if (str.equals(FBoningOutputBBeanEnum.INNER.getValue())) {
                        ret.append("<td style=\"text-align: right;\">").append("\n");
                        if (!strCellContext.isEmpty()) {
                            ret.append("<img src=\"" + urlSPCB + "\" />").append("\n");
                        }
                        ret.append(
                                "<b><span style=\"font-size: 12px; font-family: 'Arial'; color: rgb( 255, 255, 255 ); font-weight: bold;margin-right: 20px;\">")
                                .append("&nbsp;").append(strCellContext).append("&nbsp;").append("&nbsp;")
                                .append("&nbsp;").append("&nbsp;").append("</span></b>\n");
                        ret.append("</td>").append("\n");
                        ret.append("</tr>").append("\n");
                        ret.append("<br>").append("\n");

                    } else if (str.equals(FBoningOutputBBeanEnum.ITEM_LONG_LABEL.getValue())) {
                        ret.append("<tr>").append("\n");
                        ret.append("<td colspan=\"2\" style=\"height:35px;\">").append("\n");
                        ret.append(
                                "<i><b><span style=\"font-size: 12px; font-family: 'Arial'; color: rgb( 0, 0, 0 ); font-weight: bold;\">")
                                .append(strCellContext).append("</span></b></i>\n");
                        ret.append("</td>").append("\n");
                        ret.append("</tr>").append("\n");
                        ret.append("<br>").append("\n");
                    } else if (str.equals(FBoningOutputBBeanEnum.QTTY_DONE.getValue())) {
                        ret.append("<tr style=\"color: #EE7F00; background-color: rgb( 247, 247, 247 ); \">").append(
                                "\n");
                        ret.append("<td style=\"text-align: left;height:20px; \" >").append("\n");
                        ret.append(
                                "<b><span style=\"font-size: 12px;font-family: 'Arial';color: rgb( 239, 124, 0 );font-weight: bold;\">")
                                .append(strCellContext);

                    } else if (str.equals(FBoningOutputBBeanEnum.QTTY_TODO.getValue())) {
                        ret.append("/").append(strCellContext);

                    } else if (str.equals(FBoningOutputBBeanEnum.QTTY_UNIT.getValue())) {
                        ret.append("&nbsp;").append(strCellContext).append("</span>\n");
                        ret.append("</b></td>").append("\n");

                    } else if (str.equals(FBoningOutputBBeanEnum.QTTY2_DONE.getValue())) {
                        ret.append("<td style=\"text-align: right; \">").append("\n");
                        ret.append(
                                "<b><span style=\"font-size: 12px; font-family: 'Arial'; color: rgb( 239, 124, 0 );margin-right: 20px;\">")
                                .append(strCellContext);

                    } else if (str.equals(FBoningOutputBBeanEnum.QTTY2_UNIT.getValue())) {
                        ret.append("&nbsp;").append(strCellContext).append("&nbsp;").append("&nbsp;").append("&nbsp;")
                        .append("</span>\n");
                        ret.append("</b></td>\n</tr>\n");
                    }
                }
            }

            return ret;
        }
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void initializeView(final AbstractBrowserController bBean) {
        super.initializeView(bBean);
        setRowRenderer(new CellRenderer());
        for (BrowserCell<FBoningOutputBBean> cell : getBrowserCells()) {
            cell.setBorder(BorderFactory.createLineBorder(new Color(128, 128, 128), 1));
            cell.setMultiClickThreshhold(2000);
        }
    }
}

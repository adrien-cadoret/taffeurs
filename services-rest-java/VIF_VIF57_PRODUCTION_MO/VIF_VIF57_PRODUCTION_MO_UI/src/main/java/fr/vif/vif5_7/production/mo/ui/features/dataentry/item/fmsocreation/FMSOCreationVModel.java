/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMSOCreationVModel.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmsocreation.FMSOCreationVBean;


/**
 * MSO creation viewer model.
 * 
 * @author glc
 */
public class FMSOCreationVModel extends ViewerModel<FMSOCreationVBean> {

    public static final String MO_CHRONO     = "moKey.chrono.chrono";
    public static final String MO_ITEM       = "moItemCL";
    public static final String MO_LABEL      = "moLabel";
    public static final String QUANTITY_UNIT = "quantityUnit";

    /**
     * Simple constructor.
     */
    public FMSOCreationVModel() {
        super();
        setBeanClass(FMSOCreationVBean.class);
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffFCtrl.java,v $
 * Created on 18 dÃ©c. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.util.Key;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborStaff;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.StaffByTime;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.TimeByResource;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeVBean;
import fr.vif.vif5_7.production.mo.business.services.features.flaborstaff.FLaborStaffCBS;
import fr.vif.vif5_7.production.mo.business.services.features.flabortime.FLaborTimeCBS;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.touch.FLaborStaffFView;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.touch.FLaborStaffVView;


/**
 * Labor staff feature controller.
 * 
 * @author nle
 */
public class FLaborStaffFCtrl extends StandardFeatureController implements TableRowChangeListener {

    /**
     * list of btns.
     */
    public static final String     VALID_STAFF    = "validStaffBtn";
    public static final String     END_OF_WORK    = "endOfWorkBtn";
    public static final String     DETAIL         = "detailBtn";
    public static final String     REFRESHHOUR    = "refreshhourBtn";

    private static final Logger    LOGGER         = Logger.getLogger(FLaborStaffFCtrl.class);

    private FLaborStaffCBS         flaborstaffCBS = null;
    private FLaborTimeCBS          flabortimeCBS  = null;

    private ToolBarBtnFeatureModel endOfWork      = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionPerformed(event=" + event + ")");
        }

        super.actionPerformed(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionPerformed(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        ((FLaborStaffFView) getView()).getErrorPanelView().getModel().setErrors(new HashMap<String, List<String>>());

        if (VALID_STAFF.equals(event.getModel().getReference())) {
            validation();

            refreshHour();
        } else if (END_OF_WORK.equals(event.getModel().getReference())) {

            saveModif();

            // gets the last labor time
            FLaborTimeBBean lastLaborTime = getLastLaborTime();

            if (lastLaborTime != null) {
                if (lastLaborTime.getEndDateHour() == null) {
                    // the last labor time is open : ask if the user wants to close it
                    // (updates the end date-hour of the last labor time with those on screen)

                    // Do you wish to close the period ?
                    int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                            I18nClientManager.translate(ProductionMo.T34797, false), MessageButtons.YES_NO);

                    if (ret == MessageButtons.YES) {
                        try {
                            getFlabortimeCBS().endOfWork(getIdCtx(), lastLaborTime.getLaborTimeKey());
                        } catch (BusinessException e) {
                            LOGGER.error("", e);
                            getView().show(new UIException(e));
                        }

                        // re open query to simulate the set to zero
                        getBrowserCtrl().reopenQuery();

                        // changes the button label : back to work
                        getEndOfWork().setText(I18nClientManager.translate(ProductionMo.T34871, false));

                        refreshEndOfWorkBtn();
                    }

                } else {
                    // there's no labor time or the last labor time is closed : ask if the user wants to open it

                    // Do you wish to open the period ?
                    int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                            I18nClientManager.translate(ProductionMo.T34873, false), MessageButtons.YES_NO);

                    if (ret == MessageButtons.YES) {
                        try {
                            getFlabortimeCBS().backToWork(getIdCtx(), lastLaborTime.getLaborTimeKey());
                        } catch (BusinessException e) {
                            LOGGER.error("", e);
                            getView().show(new UIException(e));
                        }

                        // re open query to simulate the set to zero
                        getBrowserCtrl().reopenQuery();

                        refreshEndOfWorkBtn();
                    }
                }
            }

            refreshHour();

            // Update Bar Chart
            updateCharts();
        } else if (DETAIL.equals(event.getModel().getReference())) {
            saveModif();

            super.buttonAction(event);

        } else if (REFRESHHOUR.equals(event.getModel().getReference())) {
            refreshHour();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void childFeatureClosed(final FeatureController childFeatureController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }

        getBrowserCtrl().setModifSaved(true);

        refreshHour();

        refreshEndOfWorkBtn();

        super.childFeatureClosed(childFeatureController);

        getBrowserCtrl().reopenQuery();

        getSharedContext().remove(Domain.DOMAIN_CHILD, FProductionConstant.FEATURE_ID);

        // Update Bar Chart
        updateCharts();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closingRequired(final ClosingEvent event) throws UIVetoException {
        saveModif();

        super.closingRequired(event);
    }

    /**
     * Gets the endOfWork.
     * 
     * @return the endOfWork.
     */
    public ToolBarBtnFeatureModel getEndOfWork() {
        if (endOfWork == null) {
            endOfWork = new ToolBarBtnFeatureModel();
            endOfWork.setReference(END_OF_WORK);
            endOfWork.setIconURL("/images/vif/vif57/production/mo/manualdataentry.png");
            endOfWork.setText(I18nClientManager.translate(ProductionMo.T34732, false));
            endOfWork.setShortcut(Key.K_F4);
            refreshEndOfWorkBtn();
        }
        return endOfWork;
    }

    /**
     * Gets the flaborstaffCBS.
     * 
     * @return the flaborstaffCBS.
     */
    public FLaborStaffCBS getFlaborstaffCBS() {
        return flaborstaffCBS;
    }

    /**
     * Gets the flabortimeCBS.
     * 
     * @return the flabortimeCBS.
     */
    public FLaborTimeCBS getFlabortimeCBS() {
        return flabortimeCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        getBrowserCtrl().setModifSaved(true);

        fireSharedContextSend(getSharedContext());

        super.initialize();

        getView().setErrorPanelAlwaysVisible(true);

        final Date datprod = (Date) getSharedContext().get(Domain.DOMAIN_PARENT, "DATPROD");
        final String line = (String) getSharedContext().get(Domain.DOMAIN_PARENT, "LINE");

        // Update Bar Chart
        updateCharts();

        String subtitle;
        try {
            subtitle = DateHelper.formatDate(datprod, "dd/MM/yyyy")
                    + " - "
                    + line
                    + " - "
                    + getFlabortimeCBS().getResourceLabel(getIdCtx(),
                            new EstablishmentKey(getIdCtx().getCompany(), getIdCtx().getEstablishment()), line);
            getFeatureView().setSubTitle(subtitle);
        } catch (BusinessException e) {
            LOGGER.error("", e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {

    }

    /**
     * Sets the flaborstaffCBS.
     * 
     * @param flaborstaffCBS flaborstaffCBS.
     */
    public void setFlaborstaffCBS(final FLaborStaffCBS flaborstaffCBS) {
        this.flaborstaffCBS = flaborstaffCBS;
    }

    /**
     * Sets the flabortimeCBS.
     * 
     * @param flabortimeCBS flabortimeCBS.
     */
    public void setFlabortimeCBS(final FLaborTimeCBS flabortimeCBS) {
        this.flabortimeCBS = flabortimeCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        List<ToolBarBtnStdBaseModel> lstBtn = super.getActionsButtonModels();

        ToolBarBtnFeatureModel validStaff = new ToolBarBtnFeatureModel();
        validStaff.setReference(VALID_STAFF);
        validStaff.setIconURL("/images/vif/vif57/production/mo/ok.png");
        validStaff.setText(I18nClientManager.translate(ProductionMo.T29582, false));
        validStaff.setShortcut(Key.K_F3);
        lstBtn.add(validStaff);

        lstBtn.add(getEndOfWork());

        ToolBarBtnFeatureModel refreshhour = new ToolBarBtnFeatureModel();
        refreshhour.setReference(REFRESHHOUR);
        refreshhour.setIconURL("/images/vif/vif57/production/mo/calendar.png");
        refreshhour.setText(I18nClientManager.translate(ProductionMo.T34766, false));
        refreshhour.setShortcut(Key.K_F5);
        lstBtn.add(refreshhour);

        ToolBarBtnFeatureModel detail = new ToolBarBtnFeatureModel();
        detail.setReference(DETAIL);
        detail.setIconURL("/images/vif/vif57/production/mo/movementlist.png");
        detail.setFeatureId("VIF.LABTIME");
        detail.setText(I18nClientManager.translate(GenKernel.T31143, false));
        detail.setShortcut(Key.K_F9);
        lstBtn.add(detail);

        return lstBtn;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void openChildFeature(final String featureId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openChildFeature(featureId=" + featureId + ")");
        }

        if (featureId.equals("VIF.LABTIME")) {
            Date datprod = (Date) getSharedContext().get(Domain.DOMAIN_PARENT, "DATPROD");
            String cres = (String) getSharedContext().get(Domain.DOMAIN_PARENT, "LINE");

            getSharedContext().put(Domain.DOMAIN_THIS, "DATPROD", datprod);
            getSharedContext().put(Domain.DOMAIN_THIS, "LINE", cres);

            fireSharedContextSend(getSharedContext());
        }

        super.openChildFeature(featureId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openChildFeature(featureId=" + featureId + ")");
        }
    }

    /**
     * Get the browser controller.
     * 
     * @return the browser controller
     */
    private FLaborStaffBCtrl getBrowserCtrl() {
        return (FLaborStaffBCtrl) getBrowserController();

    }

    /**
     * Get the feature view.
     * 
     * @return the feature view
     */
    private FLaborStaffFView getFeatureView() {
        return (FLaborStaffFView) getView();

    }

    /**
     * Get the last labor time.
     * 
     * @return the bean
     */
    private FLaborTimeBBean getLastLaborTime() {

        FLaborTimeBBean lastLaborTime = null;

        List<FLaborStaffBBean> lstbBean = getBrowserCtrl().getModel().getBeans();

        if (lstbBean != null && lstbBean.size() > 0) {
            LaborTimeKey ltk = lstbBean.get(0).getLaborStaff().getLaborStaffKey().getLaborTimeKey();

            FLaborTimeBBean ltbb = new FLaborTimeBBean();
            ltk.setNi1(0); // in order to get all the labor time keys
            ltbb.setLaborTimeKey(ltk);

            try {
                FLaborTimeVBean laborTimeVBean = getFlabortimeCBS().getVBean(getIdCtx(), ltbb);
                if (laborTimeVBean != null) {
                    int lastNi1 = 0;
                    for (FLaborStaffBBean lsbBean : laborTimeVBean.getLaborStaffList()) {
                        int ni1 = lsbBean.getLaborStaff().getLaborStaffKey().getLaborTimeKey().getNi1();
                        if (ni1 > lastNi1) {
                            lastNi1 = ni1;
                        }
                    }

                    if (lastNi1 > 0) {
                        ltbb.getLaborTimeKey().setNi1(lastNi1); // re-uses this bean with the last ni1
                        FLaborTimeBBean lastBBean = getFlabortimeCBS().getLaborTimeBBean(getIdCtx(),
                                ltbb.getLaborTimeKey());
                        lastLaborTime = lastBBean;
                    } else {
                        lastLaborTime = null;
                    }
                } else {
                    lastLaborTime = null;
                }
            } catch (BusinessException e) {
                getView().show(new UIException(e));
                LOGGER.error("", e);
            }
        } else {
            lastLaborTime = null;
        }
        return lastLaborTime;
    }

    /**
     * Get the viewer controller.
     * 
     * @return the viewer controller
     */
    private FLaborStaffVCtrl getViewerCtrl() {
        return (FLaborStaffVCtrl) getViewerController();

    }

    /**
     * Get the viewer view.
     * 
     * @return the viewer view
     */
    private FLaborStaffVView getViewerView() {
        return (FLaborStaffVView) getViewerCtrl().getView();

    }

    /**
     * Refreshes the endOfWork button label.
     */
    private void refreshEndOfWorkBtn() {
        FLaborTimeBBean lastLaborTime = getLastLaborTime();
        if (lastLaborTime == null) {
            endOfWork.setEnabled(false);
        } else if (lastLaborTime.getEndDateHour() == null) {
            // end of work
            endOfWork.setEnabled(true);
            endOfWork.setText(I18nClientManager.translate(ProductionMo.T34732, false));
        } else {
            // back to work
            endOfWork.setEnabled(true);
            endOfWork.setText(I18nClientManager.translate(ProductionMo.T34871, false));
        }

    }

    /**
     * Refreshes the current hour.
     */
    private void refreshHour() {
        Date currentDate = DateHelper.getTodayTime().getTime();
        getViewerView().getItfDate().setValue(currentDate);
        getViewerView().getItfHour().setValue(currentDate);

    }

    /**
     * Alert : modification not saved.
     */
    private void saveModif() {
        if (!getBrowserCtrl().getModifSaved()) {
            // The table has been changed.|Do you want to save these changes? ?
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T744, false), MessageButtons.YES_NO);

            if (ret == MessageButtons.YES) {
                validation();
            }
        }
    }

    /**
     * Update bar chart.
     */
    private void updateCharts() {
        // Update bar charts

        final String csoc = getIdCtx().getCompany();
        final String cetab = getIdCtx().getEstablishment();
        final Date datprod = (Date) getSharedContext().get(Domain.DOMAIN_PARENT, "DATPROD");
        final String line = (String) getSharedContext().get(Domain.DOMAIN_PARENT, "LINE");

        LaborTimeKey laborTimeKey = new LaborTimeKey(csoc, cetab, datprod, line, 0);
        FLaborTimeBBean ltbbean = new FLaborTimeBBean();
        ltbbean.setLaborTimeKey(laborTimeKey);

        List<TimeByResource> ltsList = null;
        List<StaffByTime> sbtList = null;
        try {
            ltsList = getFlabortimeCBS().listTimeChart(getIdCtx(), ltbbean);
            sbtList = getFlaborstaffCBS().listStaffChart(getIdCtx(), ltbbean);
        } catch (BusinessException e) {
            getView().show(new UIException(e));
            LOGGER.error("", e);
        }

        if (ltsList != null) {
            ((FLaborStaffFView) getView()).getTimeChart().setBeanList(ltsList);
        }
        if (sbtList != null) {
            ((FLaborStaffFView) getView()).getStaffChart().setBeanList(sbtList);
        }
    }

    /**
     * Validates the staffs.
     */
    private void validation() {
        Date datdebr = (Date) getViewerView().getItfDate().getValue();
        Date heurdebr = (Date) getViewerView().getItfHour().getValue();

        LaborStaff ls = getViewerCtrl().getBean().getLaborStaff();

        if (ls != null) {
            List<FLaborStaffBBean> lstbBean = getBrowserCtrl().getModel().getBeans();

            try {
                // inserts 1 LaborTime (KTPSX) and N LaborStaff
                getFlaborstaffCBS().insertLaborStaffList(getIdCtx(), ls, datdebr, heurdebr, lstbBean);

                getBrowserCtrl().setModifSaved(true);

                refreshEndOfWorkBtn();

            } catch (BusinessException e) {
                getView().show(new UIException(e));
            } catch (BusinessErrorsException e) {
                ((FLaborStaffVCtrl) getViewerController()).showError(new UIErrorsException(e));
                ((FLaborStaffFView) getView()).getErrorPanelView().getModel().setErrors(e.getMapErrors());
            }

            // Update Bar Chart
            updateCharts();
        }
    }

}

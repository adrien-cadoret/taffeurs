/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingDynamicVView.java,v $
 * Created on 4 févr. 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting.swing;


import java.util.LinkedHashMap;

import javax.swing.JPanel;

import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.common.util.BeanHelper;
import fr.vif.jtech.common.util.exceptions.BeanException;
import fr.vif.jtech.ui.input.swing.SwingInputComboBox;
import fr.vif.jtech.ui.input.swing.SwingInputLabel;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.util.swing.SwingInputContainerHelper;
import fr.vif.jtech.ui.viewer.swing.DynamicSwingViewer;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingVBean;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.ButtonLocation;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.FontSize;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.MapKeyByChapter;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.SortByChapter;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.Sorts;
import fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting.FTabelLookSettingFlipFlopIView;
import fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting.FTableLookSettingDynamicIView;
import fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting.FTableLookSettingFlipFlopCtrl;


/**
 * TableLook Viewer View.
 * 
 * @author kl
 */
public class FTableLookSettingDynamicVView extends DynamicSwingViewer<FTableLookSettingVBean> implements
FTableLookSettingDynamicIView {

    private static final int              WITH_EMPTY_FIELD_FOR_MO_LIST_CHAPTER     = 41;
    private static final int              WITH_EMPTY_FIELD_FOR_PRODUCTIONS_CHAPTER = 45;
    private static final int              WITH_EMPTY_FIELD_FOR_DETAILS_CHAPTER     = 85;

    // Combo box
    private SwingInputLabel               labelSort;

    private SwingInputComboBox            icbUiSort;
    // Flip Flop
    private FTabelLookSettingFlipFlopView siffSettings;

    // Custom zone
    private SwingInputTextField           sitfZone;
    private SwingInputTextField           sitfTitle;
    private SwingInputTextField           sitfSizeZone;
    private SwingInputComboBox            icbUiSFontSize;
    private SwingInputComboBox            icbUiButtonAction;
    private JPanel                        emptyField;

    /**
     * Default Constructor.
     */
    public FTableLookSettingDynamicVView() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param presentationType presentationType
     */
    public FTableLookSettingDynamicVView(final String presentationType) {
        super(presentationType);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void changeListButtonLocation() {
        LinkedHashMap<String, String> listElmnt = new LinkedHashMap<String, String>();

        for (ButtonLocation elt : ButtonLocation.values()) {
            listElmnt.put(elt.getKey(), elt.getTranslation(getLocale()));
        }
        getIcbUiButtonAction().changeListItems(listElmnt);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void changeListFontSize() {
        LinkedHashMap<String, String> listElmnt = new LinkedHashMap<String, String>();

        for (FontSize fontSize : FontSize.values()) {
            listElmnt.put(fontSize.getKey(), fontSize.getTranslation(getLocale()));
        }
        getIcbUiSFontSize().changeListItems(listElmnt);

    }

    /**
     * {@inheritDoc}
     */
    public void changeListSorts(final FTableLookSettingBBean bBean) {
        SortByChapter sortByChapter = SortByChapter.getSortByChapter(bBean.getChapterNumber());

        LinkedHashMap<String, String> listElmnt = new LinkedHashMap<String, String>();
        if (sortByChapter != null) {
            disableCombobox(true);
            for (Sorts sort : sortByChapter.getSorts()) {
                listElmnt.put(sort.getKey(), sort.getTranslation(getLocale()));
            }
        }
        getIcbUiSort().changeListItems(listElmnt);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void disableCombobox(final boolean disable) {
        getIcbUiSort().setAlwaysDisabled(disable);

    }

    @Override
    public void displayButtonLocation(final boolean display) {
        getIcbUiButtonAction().setVisible(display);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayCombobox(final boolean display) {
        // Display or not label of combobox and combobox.
        getLabelSort().setVisible(display);
        getIcbUiSort().setVisible(display);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayCustomZone(final boolean display) {
        // Display or not Custom zone.
        getSitfZone().setVisible(display);
        getSitfTitle().setVisible(display);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayWithAndFontZone(final boolean display) {
        getSitfSizeZone().setVisible(display);
        getIcbUiSFontSize().setVisible(display);
    }

    /**
     * Gets the emptyField.
     *
     * @return the emptyField.
     */
    public JPanel getEmptyField() {
        return emptyField;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FTabelLookSettingFlipFlopIView getFlipFlopView() {
        return getSiffSettings();
    }

    /**
     * Gets the icbUiButtonAction.
     *
     * @return the icbUiButtonAction.
     */
    public SwingInputComboBox getIcbUiButtonAction() {
        return icbUiButtonAction;
    }

    /**
     * Gets the icbUiSFontSize.
     *
     * @return the icbUiSFontSize.
     */
    public SwingInputComboBox getIcbUiSFontSize() {
        return icbUiSFontSize;
    }

    /**
     * Gets the icbUiSort.
     * 
     * @return the icbUiSort.
     */
    public SwingInputComboBox getIcbUiSort() {
        return icbUiSort;
    }

    /**
     * Gets the labelSort.
     * 
     * @return the labelSort.
     */
    public SwingInputLabel getLabelSort() {
        return labelSort;
    }

    /**
     * Gets the siffSettings.
     * 
     * @return the siffSettings.
     */
    public FTabelLookSettingFlipFlopView getSiffSettings() {
        return siffSettings;
    }

    /**
     * Gets the sitfSizeZone.
     *
     * @return the sitfSizeZone.
     */
    public SwingInputTextField getSitfSizeZone() {
        return sitfSizeZone;
    }

    /**
     * Gets the sitfTitle.
     * 
     * @return the sitfTitle.
     */
    public SwingInputTextField getSitfTitle() {
        return sitfTitle;
    }

    /**
     * Gets the sitfZone.
     * 
     * @return the sitfZone.
     */
    public SwingInputTextField getSitfZone() {
        return sitfZone;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FTableLookSettingFlipFlopCtrl getViewerFlipFlopController() {
        return (FTableLookSettingFlipFlopCtrl) getSiffSettings().getController();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isModified() {
        // MP QA8917 change the inital current bean => another bean can be selected without be changed
        getModel().getInitialBean().setCurrentSettingBean(getModel().getBean().getCurrentSettingBean());
        return SwingInputContainerHelper.isModified(this, getModel().getInitialBean());
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    @Override
    public void renderDetailSetting() throws BeanException {
        // Update value display in Custom Zone.
        getSitfZone().setValue(BeanHelper.getPropertyValue(getModel().getBean(), getSitfZone().getBeanProperty()));
        getSitfTitle().setValue(BeanHelper.getPropertyValue(getModel().getBean(), getSitfTitle().getBeanProperty()));
        getSitfSizeZone().setValue(
                BeanHelper.getPropertyValue(getModel().getBean(), getSitfSizeZone().getBeanProperty()));
        getIcbUiSFontSize().setValue(
                BeanHelper.getPropertyValue(getModel().getBean(), getIcbUiSFontSize().getBeanProperty()));
        getIcbUiButtonAction().setValue(
                BeanHelper.getPropertyValue(getModel().getBean(), getIcbUiButtonAction().getBeanProperty()));
    }

    /**
     * Sets the emptyField.
     *
     * @param emptyField emptyField.
     */
    public void setEmptyField(final JPanel emptyField) {
        this.emptyField = emptyField;
    }

    /**
     * Sets the icbUiButtonAction.
     *
     * @param icbUiButtonAction icbUiButtonAction.
     */
    public void setIcbUiButtonAction(final SwingInputComboBox icbUiButtonAction) {
        this.icbUiButtonAction = icbUiButtonAction;
    }

    /**
     * Sets the icbUiSFontSize.
     *
     * @param icbUiSFontSize icbUiSFontSize.
     */
    public void setIcbUiSFontSize(final SwingInputComboBox icbUiSFontSize) {
        this.icbUiSFontSize = icbUiSFontSize;
    }

    /**
     * Sets the icbUiSort.
     * 
     * @param icbUiSort icbUiSort.
     */
    public void setIcbUiSort(final SwingInputComboBox icbUiSort) {
        this.icbUiSort = icbUiSort;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setInitialSelection(final Selection initialSelection) {
        getViewerFlipFlopController().setInitialSelection(initialSelection);
    }

    /**
     * Sets the labelSort.
     * 
     * @param labelSort labelSort.
     */
    public void setLabelSort(final SwingInputLabel labelSort) {
        this.labelSort = labelSort;
    }

    /**
     * Sets the siffSettings.
     * 
     * @param siffSettings siffSettings.
     */
    public void setSiffSettings(final FTabelLookSettingFlipFlopView siffSettings) {
        this.siffSettings = siffSettings;
    }

    /**
     * Sets the sitfSizeZone.
     *
     * @param sitfSizeZone sitfSizeZone.
     */
    public void setSitfSizeZone(final SwingInputTextField sitfSizeZone) {
        this.sitfSizeZone = sitfSizeZone;
    }

    /**
     * Sets the sitfTitle.
     * 
     * @param sitfTitle sitfTitle.
     */
    public void setSitfTitle(final SwingInputTextField sitfTitle) {
        this.sitfTitle = sitfTitle;
    }

    /**
     * Sets the sitfZone.
     * 
     * @param sitfZone sitfZone.
     */
    public void setSitfZone(final SwingInputTextField sitfZone) {
        this.sitfZone = sitfZone;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void udpdateView(final FTableLookSettingBBean bBean) {
        SortByChapter sortByChapter = SortByChapter.getSortByChapter(bBean.getChapterNumber());
        disableCombobox(sortByChapter == null || sortByChapter.getChapter() > SortByChapter.MO_LIST.getChapter());
        displayCombobox(sortByChapter != null);
        displayCustomZone(sortByChapter != null);
        displayWithAndFontZone(sortByChapter != null
                && !(sortByChapter.getChapter() == SortByChapter.PRODUCTION_INPUT.getChapter() || sortByChapter
                .getChapter() == SortByChapter.PRODUCTION_OUPUT.getChapter()));

        int chapterNb = bBean.getChapterNumber();

        // array setting
        if (sortByChapter == null
                || (sortByChapter.getChapter() == SortByChapter.PRODUCTION_INPUT.getChapter() || sortByChapter
                        .getChapter() == SortByChapter.PRODUCTION_OUPUT.getChapter())) {
            // functionalities settings
            if (chapterNb == MapKeyByChapter.CONTAINER_MO_LIST_FUNCT.getChapter() || //
                    chapterNb == MapKeyByChapter.INPUT_PRODUCTION_FUNCT.getChapter() || //
                    chapterNb == MapKeyByChapter.OUTPUT_PRODUCTION_FUNCT.getChapter() || //
                    chapterNb == MapKeyByChapter.DATA_ENTRY_LIST_FUNCT.getChapter()) {
                displayButtonLocation(true);
                getViewerFlipFlopController().getSettingResuBrwCtrl().showFunctionalityColumns();
            } else {
                displayButtonLocation(false);
                getViewerFlipFlopController().getSettingResuBrwCtrl().showArrayColumns();
            }
        }
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningInputBModel.java,v $
 * Created on 26 Apr 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.models.format.CustomFont;
import fr.vif.jtech.ui.models.format.FontNames;
import fr.vif.jtech.ui.models.format.FontStyle;
import fr.vif.jtech.ui.models.format.Format;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninput.FProductionInputBBeanEnum;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBComparator;


/**
 * Browser model for boning input.
 * 
 * @author cj
 */
public class FBoningInputBModel extends BrowserModel<FProductionBBean> {
    private static Format          cellFormat1;
    private static Format          cellFormat2;
    private FProductionBComparator comparator = new FProductionBComparator();

    static {
        cellFormat1 = new Format(Alignment.LEFT_ALIGN, new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.PLAIN,
                14));
        cellFormat2 = new Format(Alignment.LEFT_ALIGN,
                new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.BOLD, 14));

    }

    /**
     * Default constructor.
     * 
     */
    public FBoningInputBModel() {
        super();
        setBeanClass(FProductionBBean.class);

        addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_SHORT_LABEL.getValue(), 0, 0));
        addColumn(new BrowserColumn("", FProductionInputBBeanEnum.ITEM_CODE.getValue(), 0, 1));
        addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_DONE.getValue(), 0, 2));
        addColumn(new BrowserColumn(" / ", FProductionInputBBeanEnum.QTTY_TODO.getValue(), 0, 2));
        addColumn(new BrowserColumn("", FProductionInputBBeanEnum.QTTY_UNIT.getValue(), 0, 2));

        setFullyFetched(true);
        setFetchSize(9999999);
        setComparator(this.comparator);
        setRowHeight(130);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Format getCellFormat(final int rowNum, final String property, final Object cellContent,
            final FProductionBBean bean) {
        Format ret = null;

        if (property.equals(FProductionInputBBeanEnum.ITEM_SHORT_LABEL.getValue())) {
            ret = cellFormat2;
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                ret.setForegroundColor(StandardColor.BLACK);
            }
        } else if (property.equals(FProductionInputBBeanEnum.ITEM_CODE.getValue())) {
            ret = cellFormat1;
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                ret.setForegroundColor(StandardColor.BLACK);
            }
        } else if (property.equals(FProductionInputBBeanEnum.QTTY_DONE.getValue())
                || property.equals(FProductionInputBBeanEnum.QTTY_TODO.getValue())
                || property.equals(FProductionInputBBeanEnum.QTTY_UNIT.getValue())) {
            ret = cellFormat1;
            if (bean.getItemQttyDoneInRefUnit().concat(bean.getItemQttyToDoInRefUnit()).concat(bean.getItemRefUnit())
                    .length() >= 17) {
                ret.setFont(new CustomFont(FontNames.TOUCHSCREEN_DEFAULT, FontStyle.PLAIN, 13));
            }
            if (bean.getOperationItemBean().getOperationItemKey().getManagementType().equals(ManagementType.ARCHIVED)) {
                ret.setForegroundColor(StandardColor.GRAY);
            } else {
                if (rowNum == getCurrentRowNumber()) {
                    ret.setForegroundColor(StandardColor.TOUCHSCREEN_FG_LABEL);
                } else {
                    ret.setForegroundColor(StandardColor.FG_DEFAULT);
                }
            }
        }

        return ret;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DTimeBreakDownReasonCtrl.java,v $
 * Created on 21 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown;


import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown.swing.DTimeBreakDownReasonView;
import fr.vif.vif5_7.stock.kernel.business.beans.common.reason.ReasonType;
import fr.vif.vif5_7.stock.kernel.business.beans.features.freason.FReasonSBean;


/**
 * Dialog controller for time breakdown reason.
 * 
 * @author cj
 */
public class DTimeBreakDownReasonCtrl extends AbstractStandardDialogController {

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        FReasonSBean selection = new FReasonSBean();
        selection.getSelection().setCompanyId(getIdentification().getCompany());
        selection.getSelection().setReasonType(ReasonType.RESOURCE);
        ((DTimeBreakDownReasonView) getView()).getCreasonView().getCodeView().setInitialHelpSelection(selection);

        super.initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {
    }
}

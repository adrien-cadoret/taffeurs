/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DLabelsIView.java,v $
 * Created on 19 nov. 08 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.flabels;


import fr.vif.jtech.ui.input.kbdialog.NumKeyboardDialogView;


/**
 * interface.
 *
 * @author gv
 */
public interface DLabelsIView {

    /**
     * creates the numb dialog.
     * 
     * @param title title of dlg.
     * @return dialog.
     */
    public NumKeyboardDialogView getLabelsKeyboardView(String title);
}

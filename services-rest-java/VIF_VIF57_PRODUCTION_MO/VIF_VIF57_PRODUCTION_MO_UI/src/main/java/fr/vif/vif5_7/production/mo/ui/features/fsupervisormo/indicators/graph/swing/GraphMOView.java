/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: GraphMOView.java,v $
 * Created on 26 sept. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.entity.ChartEntity;
import org.jfree.data.general.DefaultPieDataset;

import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.GraphMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.IGraphMOView;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.AbstractIndicatorMOViewerView;


/**
 * Viewer containing a Chartpanel.
 * 
 * @author jd
 */
public class GraphMOView extends AbstractIndicatorMOViewerView<GraphMO> implements IGraphMOView, MouseListener,
ActionListener {
    /** LOGGER. */
    private static final Logger LOGGER      = Logger.getLogger(GraphMOView.class);

    private JFreeChart          chart       = null;
    private List<ChartPanel>    chartPanels = new ArrayList<ChartPanel>();
    private GraphMO             graph;
    private Point               lastPoint;
    private List<JMenu>         menus       = new ArrayList<JMenu>();

    /**
     * Constructor.
     */
    public GraphMOView() {
        GridBagLayout gridBagLayout = new GridBagLayout();
        setLayout(gridBagLayout);

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weighty = 1.0;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(getChartPanel(), gbc);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final ActionEvent e) {

        ChartEntity chartEntity = chartPanels.get(0).getEntityForPoint((int) lastPoint.getX(), (int) lastPoint.getY());

        GenericActionEvent event = new GenericActionEvent(e.getSource(), GenericActionEvent.EVENT_ACTION_PERFORMED,
                e.getActionCommand(), chartEntity);
        //
        fireGenericAction(event);
    }

    /**
     * Add a menu to the default popup menu.
     * 
     * @param popToAdd menu to add.
     */
    public void addPopupMenu(final JMenu popToAdd) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - addPopupMenu(popToAdd=" + popToAdd + ")");
        }

        menus.add(popToAdd);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - addPopupMenu(popToAdd=" + popToAdd + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseClicked(final MouseEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - mouseClicked(e=" + e + ")");
        }

        if (e.getSource() != null && e.getButton() == 1 && e.getClickCount() == 2) {

            ChartEntity chartEntity = chartPanels.get(0).getEntityForPoint((int) lastPoint.getX(),
                    (int) lastPoint.getY());

            GenericActionEvent event = new GenericActionEvent(chartPanels.get(0),
                    GenericActionEvent.EVENT_ACTION_PERFORMED, "CLICK_ON_CELL", chartEntity);

            fireGenericAction(event);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - mouseClicked(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseEntered(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseExited(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mousePressed(final MouseEvent e) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - mousePressed(e=" + e + ")");
        }

        if (e != null) {
            lastPoint = new Point(e.getX(), e.getY());
            // if (e.getSource() != null && e.getButton() == 3) {
            // popup.show(this, e.getX(), e.getY());
            // }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - mousePressed(e=" + e + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void mouseReleased(final MouseEvent e) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setGraph(final GraphMO graph) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setGraph(graph=" + graph + ")");
        }

        setGraph(graph, new GraphMOOptions());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setGraph(graph=" + graph + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void setGraph(final GraphMO graphMO, final GraphMOOptions options) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setGraph(graph=" + graphMO + ", options=" + options + ")");
        }

        this.graph = graphMO;
        // Clear the graph component
        for (Component c : chartPanels) {
            if (c instanceof ChartPanel) {
                remove(c);
            }
        }
        chartPanels.clear();

        // Create (again) the graph component
        ChartPanel chartPanel = GraphMOTools.createGraphPanel(graphMO, options);
        chartPanels.add(chartPanel);
        // CHECKSTYLE:OFF
        chartPanel.setPreferredSize(new java.awt.Dimension(320, 250));
        // CHECKSTYLE:ON

        chartPanel.addMouseListener(this);

        // Add the graph component in the viewer
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weighty = 1.0;
        gbc.weightx = 1.0;
        gbc.fill = GridBagConstraints.BOTH;
        gbc.anchor = GridBagConstraints.NORTHWEST;
        gbc.gridx = 0;
        gbc.gridy = 0;
        add(chartPanel, gbc);

        // Remove problematic menus
        for (Component c : chartPanel.getPopupMenu().getComponents()) {
            if (c instanceof JMenuItem) {
                if (((JMenuItem) c).getModel().getActionCommand() == null || // ZOOM and SCALE
                        "PROPERTIES".equalsIgnoreCase(((JMenuItem) c).getModel().getActionCommand())) {
                    chartPanel.getPopupMenu().remove(c);
                }
            }
        }
        for (JMenu menu : menus) {
            chartPanel.getPopupMenu().add(menu);
        }

        /* FB DP2581 Remove double separator */
        Component last = null;
        for (Component comp : chartPanel.getPopupMenu().getComponents()) {
            if (comp instanceof JSeparator && last instanceof JSeparator) {
                chartPanel.getPopupMenu().remove(comp);
            } else {
                last = comp;
            }
        }
        /* FB DP2581 */

        // chartPanel.getPopupMenu().add(FIndicatorMOPopupManager.getInstance().createPopupMenu(this));
        // Refresh the graph viewer
        validate();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setGraph(graph=" + graphMO + ", options=" + options + ")");
        }
    }

    /**
     * Get the chart panel.
     * 
     * @return {@link ChartPanel}
     */
    private ChartPanel getChartPanel() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getChartPanel()");
        }

        ChartPanel chartPanel = null;
        if (chartPanels.size() == 0) {
            // create a dataset...
            DefaultPieDataset data = new DefaultPieDataset();
            // CHECKSTYLE:OFF
            data.setValue("Category1", 43.2);
            data.setValue("Category2", 27.9);
            data.setValue("Category3", 79.5);
            // CHECKSTYLE:ON

            // create a chart...
            chart = ChartFactory
                    .createPieChart("chart", data, true/* legend? */, true/* tooltips? */, false/* URLs? */);

            chartPanel = new ChartPanel(chart);

            // CHECKSTYLE:OFF
            chartPanel.setPreferredSize(new java.awt.Dimension(350, 250));
            // CHECKSTYLE:ON

            chartPanels.add(chartPanel);

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getChartPanel()=" + chartPanel);
        }
        return chartPanel;
    }
}

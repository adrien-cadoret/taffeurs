/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOFrameBean.java,v $
 * Created on 13 oct. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


import java.awt.Dimension;

import fr.vif.jtech.ui.models.identification.Identification;


/**
 * Bean to create a frame.
 * 
 * @author jd
 */
public class FSupervisorMOFrameBean {

    /**
     * icon.
     */
    // private KernelIcon icon = null;

    /**
     * Indentification to use.
     */
    private Identification identification = null;

    /**
     * The frame is hidable ?
     */
    private boolean        isHidable      = false;

    /**
     * The frame is visible ?
     */
    private boolean        isVisible      = false;

    /**
     * Default pos X.
     */
    private int            posX           = 0;

    /**
     * Default pos Y.
     */
    private int            posY           = 0;

    /**
     * Default prefered Size.
     */
    private Dimension      preferedSize   = new Dimension();

    /**
     * Title.
     */
    private String         title          = null;
    /**
     * Class.
     */
    private int            typeFrame      = 0;

    /**
     * Constructor.
     */
    public FSupervisorMOFrameBean() {

    }

    /**
     * Constructor.
     * 
     * @param title title of the frame
     * @param typeFrame type of the frame to create
     * @param identification identification
     * @param posX default pos X
     * @param posY default pos Y
     * @param preferedSize preferedSize
     */
    public FSupervisorMOFrameBean(final String title, final int typeFrame, // final MVCTriad triad,
            final Identification identification, final int posX, final int posY, final Dimension preferedSize) {
        super();
        this.typeFrame = typeFrame;
        this.title = title;
        this.identification = identification;
        this.posX = posX;
        this.posY = posY;
        this.preferedSize = preferedSize;
    }

    // /**
    // * Gets the icon.
    // *
    // * @return the icon.
    // */
    // public KernelIcon getIcon() {
    // return icon;
    // }

    /**
     * Gets the identification.
     * 
     * @return the identification.
     */
    public Identification getIdentification() {
        return identification;
    }

    /**
     * Gets the isVisible.
     * 
     * @return the isVisible.
     */
    public boolean getIsVisible() {
        return isVisible;
    }

    /**
     * Gets the posX.
     * 
     * @return the posX.
     */
    public int getPosX() {
        return posX;
    }

    /**
     * Gets the posY.
     * 
     * @return the posY.
     */
    public int getPosY() {
        return posY;
    }

    /**
     * Gets the preferedSize.
     * 
     * @return the preferedSize.
     */
    public Dimension getPreferedSize() {
        return preferedSize;
    }

    /**
     * Gets the title.
     * 
     * @return the title.
     */
    public String getTitle() {
        return title;
    }

    /**
     * Gets the typeFrame.
     * 
     * @return the typeFrame.
     */
    public int getTypeFrame() {
        return typeFrame;
    }

    /**
     * Gets the isClosable.
     * 
     * @return the isClosable.
     */
    public boolean isHidable() {
        return isHidable;
    }

    // /**
    // * Sets the icon.
    // *
    // * @param icon icon.
    // */
    // public void setIcon(final KernelIcon icon) {
    // this.icon = icon;
    // }

    /**
     * Sets the identification.
     * 
     * @param identification identification.
     */
    public void setIdentification(final Identification identification) {
        this.identification = identification;
    }

    /**
     * Sets the isClosable.
     * 
     * @param isClosable isClosable.
     */
    public void setIsHidable(final boolean isClosable) {
        this.isHidable = isClosable;
    }

    /**
     * Sets the isVisible.
     * 
     * @param isVisible isVisible.
     */
    public void setIsVisible(final boolean isVisible) {
        this.isVisible = isVisible;
    }

    /**
     * Sets the posX.
     * 
     * @param posX posX.
     */
    public void setPosX(final int posX) {
        this.posX = posX;
    }

    /**
     * Sets the posY.
     * 
     * @param posY posY.
     */
    public void setPosY(final int posY) {
        this.posY = posY;
    }

    /**
     * Sets the preferedSize.
     * 
     * @param preferedSize preferedSize.
     */
    public void setPreferedSize(final Dimension preferedSize) {
        this.preferedSize = preferedSize;
    }

    /**
     * Sets the title.
     * 
     * @param title title.
     */
    public void setTitle(final String title) {
        this.title = title;
    }

    /**
     * Sets the typeFrame.
     * 
     * @param typeFrame typeFrame.
     */
    public void setTypeFrame(final int typeFrame) {
        this.typeFrame = typeFrame;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "FSupervisorFrameBean [typeFrame=" + typeFrame + ", isVisible=" + isVisible + ", title=" + title
                // + ", icon=" + icon
                + ", identification=" + identification + ", posX=" + posX + ", posY=" + posY + ", preferedSize="
                + preferedSize + "]";
    }

}

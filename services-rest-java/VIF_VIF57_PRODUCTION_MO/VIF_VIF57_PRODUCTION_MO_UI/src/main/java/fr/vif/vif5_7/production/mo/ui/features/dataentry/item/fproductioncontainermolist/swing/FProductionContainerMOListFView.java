/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionContainerMOListFView.java,v $
 * Created on 7 janv. 09 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioncontainermolist.swing;


import java.awt.Color;
import java.awt.Dimension;

import javax.swing.BoxLayout;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.ui.feature.swing.StandardSwingFeature;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.jtech.ui.viewer.ViewerView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.swing.FMOListBView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.swing.FMOListGSView;


/**
 * feature view.
 * 
 * @author gv
 */
public class FProductionContainerMOListFView extends StandardSwingFeature<FMOListBBean, FMOListVBean> {

    private FMOListBView                    fMOListBView  = null;
    private FMOListGSView                   fMOListGSView = null;
    private FProductionContainerMOListVView fproductionContainerMOListVView;
    private TitlePanel                      titlePanel;

    /**
     * Default constructor.
     * 
     */
    public FProductionContainerMOListFView() {
        super();
        initialize();
    }

    /**
     * Gets the fMOListGSView.
     * 
     * @category getter
     * @return the fMOListGSView.
     */
    public final FMOListGSView getFMOListGSView() {
        if (fMOListGSView == null) {
            fMOListGSView = new FMOListGSView();
        }
        return fMOListGSView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ViewerView<FMOListVBean> getViewerView() {
        return getFProductionContainerMOListVView();
    }

    /**
     * Initialization of the browser feature.
     * 
     * @return FMOListBView
     */
    protected FMOListBView getFMOListBView() {
        if (fMOListBView == null) {
            fMOListBView = new FMOListBView();
            Dimension d = new Dimension(870, 473);
            fMOListBView.getJTable().setMaximumSize(d);
            fMOListBView.getJTable().setMinimumSize(d);
        }
        return fMOListBView;
    }

    /**
     * Initialization of the viewer feature.
     * 
     * @return FProductionMOListVView
     */
    protected FProductionContainerMOListVView getFProductionContainerMOListVView() {
        if (fproductionContainerMOListVView == null) {
            fproductionContainerMOListVView = new FProductionContainerMOListVView();
            Dimension d = new Dimension(870, 95);
            fproductionContainerMOListVView.setPreferredSize(d);
            fproductionContainerMOListVView.setMinimumSize(d);
            fproductionContainerMOListVView.setMaximumSize(d);
        }
        return fproductionContainerMOListVView;
    }

    /**
     * Gets the title panel.
     * 
     * @return a TitlePanel
     */
    protected TitlePanel getTitlePanel() {
        if (titlePanel == null) {
            titlePanel = new TitlePanel();
            titlePanel.setBorder(new MatteBorder(1, 0, 0, 0, Color.white));
            titlePanel.setTitle(I18nClientManager.translate(ProductionMo.T29509, false));
            Dimension d = new Dimension(870, 42);
            titlePanel.setPreferredSize(d);
            titlePanel.setMinimumSize(d);
            titlePanel.setMaximumSize(d);
        }
        return titlePanel;
    }

    /**
     * This Method Initializes this.
     * 
     */
    protected void initialize() {

        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        this.add(getTitlePanel());
        this.add(getFMOListGSView());
        this.add(getFMOListBView());
        this.add(getFProductionContainerMOListVView());

    }
}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PREPARATION_KERNEL_UI
 * File : $RCSfile: DViewNameMOCtrl.java,v $
 * Created on 9 sept. 09 by vr
 */
package fr.vif.vif5_7.production.mo.ui.dialog.viewnamemo;


import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.dialogs.selection.SelectionModel;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.models.identification.Identification;
import fr.vif.jtech.ui.util.DialogHelper;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.production.mo.business.beans.dialog.viewname.DViewNameMOBean;
import fr.vif.vif5_7.production.mo.ui.dialog.viewnamemo.swing.DViewNameMOView;


/**
 * Password dialog controller.
 * 
 * @author vr
 */
public class DViewNameMOCtrl extends AbstractStandardDialogController {

    /**
     * Show a dialog box to keyboard a password.
     * 
     * @param featureView feature view
     * @param changeListener who listen to changes
     * @param displayListener who listen to display
     * @param identification identification
     * @param viewName the view name
     * @param theViewName the real view name
     */
    public static void showViewName(final FeatureView featureView, final DialogChangeListener changeListener,
            final DisplayListener displayListener, final Identification identification, final DViewNameMOBean viewName,
            final String theViewName // NLE
            ) {
        DialogHelper.createDialog(featureView, DViewNameMOCtrl.class, //
                SelectionModel.class, //
                DViewNameMOView.class, //
                viewName, //
                changeListener, //
                displayListener, //
                identification, //
                I18nClientManager.translate(Jtech.T25260) + " " + theViewName, // Saving <nom vue> // NLE
                // I18nClientManager.translate(PreparationKernel.T32381), //
                false);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beanValidation(final Object dialogBean) throws UIException, UIErrorsException {
        super.beanValidation(dialogBean);
        /* FB DP2581 */
        // if (((DViewNameMOBean) dialogBean).getList().contains(((DViewNameMOBean) dialogBean).getName())) {
        // ((DViewNameMOBean) dialogBean).setAccepted(false);
        // } else {
        // ((DViewNameMOBean) dialogBean).setAccepted(true);
        // }
        ((DViewNameMOBean) dialogBean).setAccepted(true);

        // if (!((DPasswordBean) dialogBean).getRealPassword().equalsIgnoreCase(
        // ((DPasswordBean) dialogBean).getEnteredPassword())) {
        // ((DPasswordBean) dialogBean).setAccepted(false);
        // throw new UIException(new BusinessException(Generic.T20724, PreparationKernel.T30333));
        // } else {
        // ((DViewNameBean) dialogBean).setAccepted(true);
        // }
        // List<String> views = getManager().getFavoriteViews(getIdCtx());
        // getContainerView().getSharedContext().get(Domain.DOMAIN_PARENT, KernelFeatureConstant.FAVORITE_VIEWS, views);

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {
    }
}

/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DProcessOrderDCtrl.java,v $
 * Created on 15 mars 2012 by ag
 */
package fr.vif.vif5_7.production.mo.ui.dialog.processorder;


import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.exceptions.UIException;


/**
 * Dialog Controller for Process Orders.
 * 
 * @author ag
 */
public class DProcessOrderDCtrl extends AbstractStandardDialogController {

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {

    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FContentRateGSView.java,v $
 * Created on 15 Jul 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent.touch;


import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.format.Alignment;
import fr.vif.jtech.ui.selection.touch.StandardTouchGeneralSelection;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.item.ui.composites.citem.CItemCtrl;
import fr.vif.vif5_7.gen.item.ui.composites.citem.touch.CItemTouchView;
import fr.vif.vif5_7.gen.service.ui.composites.cquantityunit.touch.CQuantityUnitView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fcontent.FContentRateSBeanEnum;


/**
 * General selection view for content feature.
 * 
 * @author cj
 */
public class FContentRateGSView extends StandardTouchGeneralSelection {
    private TouchInputTextField itCriteria;
    private CItemTouchView      itItem1;
    private CItemTouchView      itItem2;
    private TouchInputTextField itQtyLeft;
    private CQuantityUnitView   itCurrentRate;

    /**
     * Constructor.
     */
    public FContentRateGSView() {
        super();
        initialize();
    }

    /**
     * Gets the currentRate.
     * 
     * @return the currentRate.
     */
    public CQuantityUnitView getCurrentRate() {
        if (itCurrentRate == null) {
            itCurrentRate = new CQuantityUnitView();
            itCurrentRate.setBeanProperty(FContentRateSBeanEnum.CURRENT_RATE.getValue());
            itCurrentRate.setDescription(I18nClientManager.translate(ProductionMo.T34253, false));
            itCurrentRate.setBounds(700, 100, 200, 70);
        }
        return itCurrentRate;
    }

    /**
     * Gets the itCriteria.
     * 
     * @return the itCriteria.
     */
    public TouchInputTextField getItCriteria() {
        if (itCriteria == null) {
            itCriteria = new TouchInputTextField(String.class, "30", I18nClientManager.translate(ProductionMo.T34269,
                    false));
            itCriteria.setBeanProperty(FContentRateSBeanEnum.CRITERIA_TARGET.getValue());
            itCriteria.setBounds(10, 5, 300, 70);
            itCriteria.setAlwaysDisabled(true);
        }
        return itCriteria;
    }

    /**
     * Gets the itItem1.
     * 
     * @return the itItem1.
     */
    public CItemTouchView getItItem1() {
        if (itItem1 == null) {
            itItem1 = new CItemTouchView(CItemCtrl.class, I18nClientManager.translate(ProductionMo.T29519, false));
            itItem1.setBeanProperty(FContentRateSBeanEnum.ITEM_1.getValue());
            itItem1.setBounds(320, 5, 350, 90);
            itItem1.setAlwaysDisabled(true);
            itItem1.getCodeView().setAlwaysDisabled(true);
        }
        return itItem1;
    }

    /**
     * Gets the itItem2.
     * 
     * @return the itItem2.
     */
    public CItemTouchView getItItem2() {
        if (itItem2 == null) {
            itItem2 = new CItemTouchView(CItemCtrl.class, I18nClientManager.translate(ProductionMo.T29519, false));
            itItem2.setBeanProperty(FContentRateSBeanEnum.ITEM_2.getValue());
            itItem2.setBounds(320, 100, 350, 90);
            itItem2.setAlwaysDisabled(true);
            itItem2.getCodeView().setAlwaysDisabled(true);
        }
        return itItem2;
    }

    /**
     * Gets the itQtyLeft.
     * 
     * @return the itQtyLeft.
     */
    public TouchInputTextField getItQtyLeft() {
        if (itQtyLeft == null) {
            itQtyLeft = new TouchInputTextField(String.class, "30",
                    I18nClientManager.translate(GenKernel.T29306, false));
            itQtyLeft.setBeanProperty(FContentRateSBeanEnum.QTY_LEFT.getValue());
            itQtyLeft.setBounds(700, 5, 200, 70);
            itQtyLeft.setAlwaysDisabled(true);
            itQtyLeft.setAlignment(Alignment.RIGHT_ALIGN);
        }
        return itQtyLeft;
    }

    /**
     * Do not show component with item 1.
     */
    public void removeItem1() {
        getItItem1().setVisible(false);
    }

    /**
     * Do not show component with item 2.
     */
    public void removeItem2() {
        getItItem2().setVisible(false);
    }

    /**
     * Initialize all components.
     */
    private void initialize() {
        this.setLayout(null);
        setOpaque(false);
        add(getItCriteria());
        add(getItItem1());
        add(getItItem2());
        add(getItQtyLeft());
        add(getCurrentRate());
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputFView.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput.touch;


import java.awt.Color;
import java.awt.Dimension;
import java.util.List;

import javax.swing.JPanel;
import javax.swing.border.MatteBorder;

import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.jtech.ui.selection.GeneralSelectionView;
import fr.vif.jtech.ui.util.touch.TitlePanel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput.FBoningOutputFIView;
import fr.vif.vif5_7.workshop.device.ui.composites.devices.scale.touch.CPerpetualScaleWeightTouchView;


/**
 * FProductionInput Feature view.
 * 
 * @author vr
 */
public class FBoningOutputFView extends StandardTouchFeature implements FBoningOutputFIView {

    private FBoningOutputGSView            fboningOutputGSView = null;
    private FBoningOutputBView             bView;

    private CPerpetualScaleWeightTouchView scaleView;

    private TitlePanel                     subTitlePanel;
    private FBoningOutputVView             vView;
    private List<CodeLabel>                hierarchies         = null;

    /**
     * Default contructor.
     */
    public FBoningOutputFView() {
        super();
        initialize();
    }

    /**
     * Gets the bView.
     * 
     * @category getter
     * @return the bView.
     */
    public FBoningOutputBView getBView() {
        if (bView == null) {
            bView = new FBoningOutputBView();
            bView.setBounds(0, 200, 867, 302);
        }
        return bView;
    }

    /**
     * Gets the fboningOutputGSView.
     * 
     * @return the fboningOutputGSView.
     */
    public FBoningOutputGSView getFboningOutputGSView() {
        if (fboningOutputGSView == null) {
            fboningOutputGSView = new FBoningOutputGSView();
            fboningOutputGSView.setBounds(0, 140, 870, 60);
        }
        return fboningOutputGSView;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public GeneralSelectionView getGeneralSelectionView() {
        return getFboningOutputGSView();
    }

    /**
     * Gets the hierarchies.
     * 
     * @return the hierarchies.
     */
    public List<CodeLabel> getHierarchies() {
        return hierarchies;
    }

    /**
     * Get the weight composite.
     * 
     * @return the weight composite
     */
    public CPerpetualScaleWeightTouchView getScaleView() {
        if (scaleView == null) {
            scaleView = new CPerpetualScaleWeightTouchView();
            scaleView.setBounds(0, 0, 867, 100);
            scaleView.setOpaque(false);
        }
        return scaleView;
    }

    /**
     * Gets the vView.
     * 
     * @category getter
     * @return the vView.
     */
    public FBoningOutputVView getVView() {
        if (vView == null) {
            vView = new FBoningOutputVView();
            vView.setBounds(0, 300, 870, 300);
        }
        return vView;
    }

    /**
     * Sets the bView.
     * 
     * @category setter
     * @param boningBView bView.
     */
    public void setBView(final FBoningOutputBView boningBView) {
        bView = boningBView;
    }

    /**
     * Sets the fboningOutputGSView.
     * 
     * @param fboningOutputGSView fboningOutputGSView.
     */
    public void setFboningOutputGSView(final FBoningOutputGSView fboningOutputGSView) {
        this.fboningOutputGSView = fboningOutputGSView;
    }

    /**
     * {@inheritDoc}
     */
    public void setHierarchies(final List<CodeLabel> hierarchies) {
        this.hierarchies = hierarchies;
        this.fboningOutputGSView.setHierarchiesList(hierarchies);
    }

    /**
     * {@inheritDoc}
     */
    public void setSubTitle(final String subTitle) {
        subTitlePanel.setTitle(subTitle);

    }

    /**
     * Get the subtitle panel.
     * 
     * @return the subtitile panel
     */
    private JPanel getSubTitle() {
        if (subTitlePanel == null) {
            subTitlePanel = new TitlePanel();
            subTitlePanel.setBounds(0, 101, 870, 40);
            subTitlePanel.setBorder(new MatteBorder(1, 0, 0, 2, Color.white));
            subTitlePanel.setPreferredSize(new Dimension(800, 40));
            subTitlePanel.setMinimumSize(new Dimension(500, 40));
        }
        return subTitlePanel;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setPreferredSize(new Dimension(870, 768));
        setLayout(null);
        add(getFboningOutputGSView());
        add(getSubTitle());
        add(getBView());
        add(getVView());
        add(getScaleView());
    }

}

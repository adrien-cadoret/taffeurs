/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningInputFCtrl.java,v $
 * Created on 23 avr. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.ObjectHelper;
import fr.vif.jtech.common.util.exceptions.ObjectException;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.input.InputFieldEvent;
import fr.vif.jtech.ui.events.input.InputFieldListener;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.input.InputBarCodeActionEvent;
import fr.vif.jtech.ui.input.InputBarCodeActionListener;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.Key;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.jtech.ui.util.system.SystemHelper;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.keys.ContainerKey;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputBarcodeType;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboninginput.FBoningInputWorkBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrydetail.FDataEntryDetailBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.fboninginput.FBoningInputCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningcommon.AbstractFBoningFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.touch.FBoningInputFView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboninginput.touch.FBoningInputVView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFCtrl;
import fr.vif.vif5_7.stock.entity.ui.dialog.content.DContainerContentCtrl;
import fr.vif.vif5_7.stock.kernel.business.beans.features.quality.nc.fncflow.FNCFlowSharedContextBean;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCConstants;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCActType;
import fr.vif.vif5_7.stock.kernel.constants.quality.nc.NCMnemos.NCType;
import fr.vif.vif5_7.stock.kernel.ui.util.StockUITools;
import fr.vif.vif5_7.workshop.device.business.beans.common.devices.ScaleDevice;


/**
 * Feature controller for boning input.
 * 
 * @author cj
 */
public class FBoningInputFCtrl extends AbstractFBoningFCtrl implements InputFieldListener, TableRowChangeListener,
InputBarCodeActionListener {

    public static final String     BTN_FLASH_REFERENCE = "BTN_FLASH_REFERENCE";

    /** LOGGER. */
    private static final Logger    LOGGER              = Logger.getLogger(FBoningInputFCtrl.class);

    /**
     * Production button reference.
     */
    private static final String    FDETAIL_ENTRY_CODE  = "VIF.BODETIMVT";

    private ToolBarBtnFeatureModel btnDetailModel;
    private ToolBarBtnStdModel     btnFlashModel;
    private OperationItemKey       current;

    private boolean                flashGetQuantities  = false;

    private FBoningInputCBS        fboningInputCBS;

    private FMOListVBean           moListVBean         = null;

    private ScaleDevice            scale               = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void barcodeReceived(final InputBarCodeActionEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - barcodeReceived(event=" + event + ")");
        }

        interpretBarcode(event.getBarcode());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - barcodeReceived(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        super.buttonAction(event);
        if (BTN_FINISH_REFERENCE.equals(event.getModel().getReference())) {
            processFinishResume();
        } else if (BTN_DETAIL_REFERENCE.equals(event.getModel().getReference())) {
            getViewerCtrl().setPrevious(null); // Maybe delete done in movement list
        } else if (BTN_SHOW_ALL_REFERENCE.equals(event.getModel().getReference())) {
            processShowAllEvent();
        } else if (BTN_FLASH_REFERENCE.equals(event.getModel().getReference())) {
            if (InputBarcodeType.KEEP_QTIES.equals(getViewerCtrl().getWorkBean().getBarcodeType())) {
                getViewerCtrl().getWorkBean().setBarcodeType(InputBarcodeType.IGNORE_QTIES);
            } else {
                getViewerCtrl().getWorkBean().setBarcodeType(InputBarcodeType.KEEP_QTIES);
            }
            manageBtnFlash();
        } else if (BTN_WEIGHT_REFERENCE.equals(event.getModel().getReference())) {
            FCommScaleResult result = null;
            try {
                result = getFeatureView().getScaleView().getModel().getAndLockResult();
                getViewerCtrl().setWeightResult(result);
            } catch (UIException e) {
                getViewerCtrl().fireErrorsChanged(e);
            }
        } else if (BTN_CANCELLAST_REFERENCE.equals(event.getModel().getReference())) {
            if (getViewerCtrl().getPrevious() != null) {
                int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                        I18nClientManager.translate(ProductionMo.T29578, false), MessageButtons.YES_NO,
                        MessageButtons.DEFAULT_NO);
                if (ret == MessageButtons.YES) {
                    try {
                        getFboningInputCBS().deleteLast(getIdCtx(),
                                getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey(),
                                getViewerCtrl().getPrevious());

                        // get the deleted quantity to update workBean
                        for (FDataEntryDetailBBean detailBBean : getViewerCtrl().getBean().getMovementList()) {
                            if (getViewerCtrl().getPrevious().getLineNumber() == detailBBean.getItemMovementBean()
                                    .getItemMovementKey().getNlig()) {

                                OperationItemKey oikey = new OperationItemKey(getViewerCtrl().getBean().getBBean()
                                        .getOperationItemBean().getOperationItemKey().getManagementType(),
                                        getViewerCtrl().getBean().getBBean().getOperationItemBean()
                                        .getOperationItemKey().getEstablishmentKey(), getViewerCtrl().getBean()
                                        .getBBean().getOperationItemBean().getOperationItemKey().getChrono(),
                                        getViewerCtrl().getBean().getBBean().getOperationItemBean()
                                        .getOperationItemKey().getCounter1(), getViewerCtrl().getBean()
                                        .getBBean().getOperationItemBean().getOperationItemKey().getCounter2(),
                                        0, 0);
                                OperationItemQuantityUnit[] qties = ((FBoningInputWorkBean) getViewerCtrl()
                                        .getWorkBean()).getQtyItemList().get(oikey);

                                qties[0].setCompletedQuantity(qties[0].getCompletedQuantity()
                                        - detailBBean.getItemMovementBean().getItemMovementQuantityUnit()
                                        .getFirstQuantity());

                                qties[1].setCompletedQuantity(qties[1].getCompletedQuantity()
                                        - detailBBean.getItemMovementBean().getItemMovementQuantityUnit()
                                        .getSecondQuantity());
                            }
                        }

                        getBrowserCtrl().reopenQuery();
                        MovementAct ma = getViewerCtrl().getPrevious();
                        getBrowserCtrl().repositionToOperationItemKey(new OperationItemKey(ManagementType.REAL, //
                                getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey(), //
                                getViewerCtrl().getWorkBean().getMoKey().getChrono(), //
                                ma.getNumber1(), //
                                ma.getNumber2(), //
                                ma.getNumber3(), //
                                ma.getNumber4()));
                        getViewerCtrl().setPrevious(null);
                        getViewerCtrl().fireButtonStatechanged();
                        getViewerCtrl().setProgressBarString();
                    } catch (BusinessException e) {
                        getViewerCtrl().fireErrorsChanged(new UIException(e));
                    } catch (BusinessErrorsException e) {
                        getViewerCtrl().fireErrorsChanged(new UIErrorsException(e));
                    }

                }
            }

        } else if (BTN_PDF_REFERENCE.equalsIgnoreCase(event.getModel().getReference())) {
            try {

                List<PDFFile> listPdfFiles = getDocumentsToShow();
                if (listPdfFiles.isEmpty()) {
                    getView().showError(I18nClientManager.translate(ProductionMo.T30838),
                            I18nClientManager.translate(ProductionMo.T30839));
                } else if (listPdfFiles.size() == 1) {
                    viewDocumentRecette(listPdfFiles.get(0));
                } else {
                    viewDocumentList(listPdfFiles);
                }

            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
        } else if ("resetBarCode".equalsIgnoreCase(event.getModel().getReference())) {
            setFocusOnBarCode();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void childFeatureClosed(final FeatureController childFeatureController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }
        if (childFeatureController instanceof FBoningDataEntryListInputFCtrl) {
            // gets the qty 1 and qty 2
            try {
                getViewerCtrl().setWorkBean(getFboningInputCBS().getWorkBean(getIdCtx(), moListVBean));
                getViewerCtrl().getWorkBean().setMoKey(moListVBean.getMobean().getMoKey());
                getViewerCtrl().getWorkBean().setRealWorkstationId(getIdCtx().getWorkstation());
                getViewerCtrl().getWorkBean().setLogicalWorkstationId(
                        (String) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.LOGICAL_WORKSTATION));
                getViewerCtrl().getWorkBean().setOutputAvailable(
                        moListVBean.getListOutputOperations() != null
                        && moListVBean.getListOutputOperations().size() > 0);
            } catch (BusinessException e) {
                LOGGER.error("", e);
                show(new UIException(e));
            }

        }
        super.childFeatureClosed(childFeatureController);
        getBrowserCtrl().reopenQuery();
        // Reposition to current bean
        if (current != null) {
            getBrowserCtrl().repositionToOperationItemKey(current);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - childFeatureClosed(childFeatureController=" + childFeatureController + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void closingRequired(final ClosingEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - closingRequired(event=" + event + ")");
        }
        super.closingRequired(event);
        // ---------------------------------------------------------------------
        // if the workstation has a scale --> stop it before closing the feature
        // ---------------------------------------------------------------------
        if (getViewerCtrl().getWorkBean().getScale() != null
                && !getViewerCtrl().getWorkBean().getScale().getFDDIP().isEmpty()) {
            getFeatureView().getScaleView().getController().stopJmsAction();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - closingRequired(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
    }

    /**
     * Gets the fboningInputCBS.
     * 
     * @return the fboningInputCBS.
     */
    public FBoningInputCBS getFboningInputCBS() {
        return fboningInputCBS;
    }

    /**
     * Gets the moListVBean.
     * 
     * @return the moListVBean.
     */
    public FMOListVBean getMoListVBean() {
        return moListVBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }
        getViewerView().getInputBarCode().addBarCodeActionListener(this);
        getViewerCtrl().setFeatureView(getView());
        getViewerCtrl().setFeatureCtrl(this);
        getViewerCtrl().setBrowserCtrl(getBrowserCtrl());
        moListVBean = (FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.MO_LIST_VBEAN);
        if (moListVBean != null) {
            // -------------------------------
            // Initialize browser selection
            // -------------------------------
            FProductionSBean sBean = new FProductionSBean();
            sBean.setActivityItemType(ActivityItemType.INPUT);
            sBean.setWorkstationId((String) getSharedContext().get(Domain.DOMAIN_PARENT,
                    FProductionConstant.LOGICAL_WORKSTATION));
            sBean.setAll(false);
            sBean.setMoKey(moListVBean.getMobean().getMoKey());

            // -------------------------------------------
            // Get workBean and initialize scale component
            // --------------------------------------------
            try {
                getViewerCtrl().setWorkBean(getFboningInputCBS().getWorkBean(getIdCtx(), moListVBean));
                getViewerCtrl().getWorkBean().setMoKey(moListVBean.getMobean().getMoKey());
                getViewerCtrl().getWorkBean().setRealWorkstationId(getIdCtx().getWorkstation());
                getViewerCtrl().getWorkBean().setLogicalWorkstationId(
                        (String) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.LOGICAL_WORKSTATION));
                getViewerCtrl().getWorkBean().setOutputAvailable(
                        moListVBean.getListOutputOperations() != null
                        && moListVBean.getListOutputOperations().size() > 0);

                scale = getViewerCtrl().getWorkBean().getScale();
                if (scale != null && !scale.getFDDIP().isEmpty()) {
                    getFeatureView().getScaleView().getController().initializeFDD(scale, getIdCtx().getWorkstation());
                    // getFeatureView().getScaleView().getController().initializeJmsAction(scale.getFDDIP(),
                    // scale.getFDDPort(), getIdCtx().getWorkstation(), scale.getIdDevice());
                    getFeatureView().getScaleView().getController().startJmsAction();
                }

            } catch (BusinessException e) {
                getViewerCtrl().fireErrorsChanged(new UIException(e));
            } catch (UIException e) {
                getViewerCtrl().fireErrorsChanged(e);
            }

            // ------------------------
            // Set Subtitle feature
            // ------------------------

            StringBuilder sb = new StringBuilder();
            sb.append(I18nClientManager.translate(ProductionMo.T29500, false, new VarParamTranslation(moListVBean
                    .getMobean().getMoKey().getChrono().getChrono())));

            String labelOF = moListVBean.getMobean().getLabel();
            if (!labelOF.isEmpty()) {
                sb.append(" - ").append(labelOF);
            }

            String customer = getViewerCtrl().getWorkBean().getCustomer();
            // String customer = getIdCtx().getCompany();
            if (customer != null && !customer.isEmpty()) {
                sb.append(" - ").append(customer);
            }

            getFeatureView().setSubTitle(sb.toString());

            getBrowserController().setCurrentSelection(sBean);
            getBrowserController().setInitialSelection(sBean);
        }
        super.initialize();
        setErrorPanelAlwaysVisible(true);
        manageBtnFlash();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * Interprets a barcode.
     * 
     * @param barcode bar code
     */
    public void interpretBarcode(final String barcode) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - interpretBarcode(barcode=" + barcode + ")");
        }

        getViewerCtrl().fireErrorsChanged();
        getViewerCtrl().getModel().setInitialBean(getViewerCtrl().getBean());
        try {
            FProductionInputContainerVBean vBean = (FProductionInputContainerVBean) ObjectHelper
                    .copy(getViewerController().getBean());

            List<FDataEntryDetailBBean> oldMvtList = vBean.getMovementList();

            vBean = getFboningInputCBS().analyseBarcode(getIdCtx(), barcode, vBean,
                    (FProductionSBean) getBrowserController().getInitialSelection(), getViewerCtrl().getWorkBean());
            // we keep the first default qty
            if (vBean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getQty() == 0) {
                vBean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty()
                .setQty(vBean.getInputItemParameters().getForcedFirstQty());
            }
            if (!vBean
                    .getBBean()
                    .getOperationItemBean()
                    .getOperationItemKey()
                    .equals(((FProductionBBean) getBrowserController().getModel().getCurrentBean())
                            .getOperationItemBean().getOperationItemKey())) {
                getBrowserCtrl().repositionToOperationItemKey(
                        vBean.getBBean().getOperationItemBean().getOperationItemKey());
            }
            getViewerCtrl().getModel().setBean(vBean);
            getViewerCtrl().getView().render();
            if (vBean.getAutoInsert()) {
                // Flash -> Data entry inserted
                getViewerCtrl().fireBeanUpdated(vBean);
                getViewerCtrl().afterValidation();
                getViewerCtrl().getModel().setInitialBean(getViewerCtrl().getBean());
                List<FDataEntryDetailBBean> newMvtList = vBean.getMovementList();
                if ((newMvtList.size() == 1 && oldMvtList.size() == 0)
                        || (newMvtList.size() > 1 && oldMvtList.size() > 0 && newMvtList.get(1).equals(
                                oldMvtList.get(0))) || vBean.getToFinish()) {
                    OperationItemKey oikey = new OperationItemKey(vBean.getBBean().getOperationItemBean()
                            .getOperationItemKey().getManagementType(), vBean.getBBean().getOperationItemBean()
                            .getOperationItemKey().getEstablishmentKey(), vBean.getBBean().getOperationItemBean()
                            .getOperationItemKey().getChrono(), vBean.getBBean().getOperationItemBean()
                            .getOperationItemKey().getCounter1(), vBean.getBBean().getOperationItemBean()
                            .getOperationItemKey().getCounter2(), 0, 0);
                    if (vBean.getToFinish()) {
                        oikey.setManagementType(ManagementType.REAL);
                    }
                    OperationItemQuantityUnit[] qties = ((FBoningInputWorkBean) getViewerCtrl().getWorkBean())
                            .getQtyItemList().get(oikey);
                    qties[0].setCompletedQuantity(qties[0].getCompletedQuantity()
                            + newMvtList.get(0).getItemMovementBean().getItemMovementQuantityUnit().getFirstQuantity());
                    qties[0].setLeftToDoQuantity(qties[0].getToDoQuantity() - qties[0].getCompletedQuantity());
                    qties[1].setCompletedQuantity(qties[1].getCompletedQuantity()
                            + newMvtList.get(0).getItemMovementBean().getItemMovementQuantityUnit().getSecondQuantity());
                    qties[1].setLeftToDoQuantity(qties[1].getToDoQuantity() - qties[1].getCompletedQuantity());

                    // if vbean has just been archived, we change the management type
                    if (vBean.getToFinish()) {
                        ((FBoningInputWorkBean) getViewerCtrl().getWorkBean()).getQtyItemList().remove(oikey);
                        oikey.setManagementType(ManagementType.ARCHIVED);
                        ((FBoningInputWorkBean) getViewerCtrl().getWorkBean()).getQtyItemList().put(oikey, qties);
                    }

                    // update progress bas
                    getViewerCtrl().setProgressBarString();
                }
            } else {
                // Flash -> Need more informations (answers question on item batch choice
                if (!vBean.getOneBatchContainer() || !vBean.getOneItemContainer()) {
                    // User must choose batch item
                    DContainerContentCtrl.showContent(this.getView(), getViewerCtrl(), getViewerCtrl(), getModel()
                            .getIdentification(), new ContainerKey(vBean.getEntity().getEntityLocationBean()
                                    .getEntityLocationKey().getCsoc(), vBean.getEntity().getEntityLocationBean()
                                    .getEntityLocationKey().getCetab(), vBean.getEntity().getEntityLocationBean()
                                    .getEntityLocationKey().getContainerNumber()));
                } else if (getViewerCtrl().isDataEntryOK()) {
                    // Ask informations and try again to insert
                    getViewerCtrl().performSave();
                }
            }
        } catch (BusinessException e) {
            getViewerCtrl().emptyVBean();
            LOGGER.error(e);
            getViewerCtrl().fireErrorsChanged(new UIException(e));
        } catch (BusinessErrorsException e) {
            getViewerCtrl().emptyVBean();
            LOGGER.error(e);
            getViewerCtrl().showUIErrorsException(new UIErrorsException(e));
        } catch (ObjectException e) {
            LOGGER.error(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - interpretBarcode(barcode=" + barcode + ")");
        }
    }

    /**
     * Interpret a container.
     * 
     * @param isContainerEnteredManually : true when container has been manually entered
     * @param containerNumber the container number
     */
    public void interpretContainer(final String containerNumber, final boolean isContainerEnteredManually) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - interpretContainer(containerNumber=" + containerNumber + ")");
        }

        getViewerCtrl().fireErrorsChanged();
        getViewerCtrl().getModel().setInitialBean(getViewerCtrl().getBean());
        try {
            FProductionInputContainerVBean vBean = (FProductionInputContainerVBean) ObjectHelper
                    .copy(getViewerController().getBean());
            vBean = getFboningInputCBS().analyseContainer(getIdCtx(), containerNumber, vBean,
                    (FProductionSBean) getBrowserController().getInitialSelection(), getViewerCtrl().getWorkBean(),
                    isContainerEnteredManually);
            if (!vBean
                    .getBBean()
                    .getOperationItemBean()
                    .getOperationItemKey()
                    .equals(((FProductionBBean) getBrowserController().getModel().getCurrentBean())
                            .getOperationItemBean().getOperationItemKey())) {
                getViewerCtrl().getModel().setInitialBean(vBean);
                getBrowserCtrl().repositionToOperationItemKey(
                        vBean.getBBean().getOperationItemBean().getOperationItemKey());
            }
            getViewerCtrl().getModel().setBean(vBean);
            getViewerCtrl().getView().render();
            if (vBean.getAutoInsert()) {
                // Flash -> Data entry inserted
                getViewerCtrl().fireBeanUpdated(vBean);
                getViewerCtrl().afterValidation();
                getViewerCtrl().getModel().setInitialBean(getViewerCtrl().getBean());
            } else {
                // Flash -> Need more informations (answers question on item batch choice
                if (!vBean.getOneBatchContainer() || !vBean.getOneItemContainer()) {
                    // User must choose batch item
                    DContainerContentCtrl.showContent(this.getView(), getViewerCtrl(), getViewerCtrl(), getModel()
                            .getIdentification(), new ContainerKey(vBean.getEntity().getEntityLocationBean()
                                    .getEntityLocationKey().getCsoc(), vBean.getEntity().getEntityLocationBean()
                                    .getEntityLocationKey().getCetab(), vBean.getEntity().getEntityLocationBean()
                                    .getEntityLocationKey().getContainerNumber()));
                } else if (getViewerCtrl().isDataEntryOK()) {
                    // Ask informations and try again to insert
                    getViewerCtrl().performSave();
                }
            }
        } catch (BusinessException e) {
            LOGGER.error(e);
            getViewerCtrl().fireErrorsChanged(new UIException(e));
        } catch (BusinessErrorsException e) {
            getViewerCtrl().emptyVBean();
            LOGGER.error(e);
            getViewerCtrl().showUIErrorsException(new UIErrorsException(e));
        } catch (ObjectException e) {
            LOGGER.error(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - interpretContainer(containerNumber=" + containerNumber + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void manageFinishResumeButton(final Object bb) {
        FProductionBBean bbean = (FProductionBBean) bb;
        if (bbean != null) {
            if (ManagementType.ARCHIVED.equals(bbean.getOperationItemBean().getOperationItemKey().getManagementType())) {
                getBtnFinishResumeModel().setText(I18nClientManager.translate(ProductionMo.T29502, false));
                getBtnFinishResumeModel().setIconURL("/images/vif/vif57/production/mo/resume.png");
            } else if (ManagementType.REAL.equals(bbean.getOperationItemBean().getOperationItemKey()
                    .getManagementType())) {
                getBtnFinishResumeModel().setText(I18nClientManager.translate(ProductionMo.T29510, false));
                getBtnFinishResumeModel().setIconURL("/images/vif/vif57/production/mo/finish.png");
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanged(event=" + event + ")");
        }

        super.selectedRowChanged(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanged(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanging(event=" + event + ")");
        }

        getViewerCtrl().getModel().setInitialBean(getViewerCtrl().getBean());
        super.selectedRowChanging(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanging(event=" + event + ")");
        }
    }

    /**
     * Sets the fboningInputCBS.
     * 
     * @param fboningInputCBS fboningInputCBS.
     */
    public void setFboningInputCBS(final FBoningInputCBS fboningInputCBS) {
        this.fboningInputCBS = fboningInputCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final InputFieldEvent event) {
        // Nothing to do

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void actionOpenNewIncident() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - actionOpenNewIncident()");
        }

        // Prepare parameters bean
        FNCFlowSharedContextBean parametersBean = new FNCFlowSharedContextBean();
        parametersBean.setType(NCType.INTERNAL.getKey());

        // Feed with viewer bean informations
        FProductionInputContainerVBean vBean = (FProductionInputContainerVBean) getViewerController().getBean();
        parametersBean.setActType(NCActType.MANUFACTURING_ORDER.getKey());
        parametersBean.setChronoOrigin(vBean.getBBean().getOperationItemBean().getOperationItemKey().getChrono());
        parametersBean.setContainerNumber(vBean.getEntity().getEntityLocationBean().getEntityLocationKey()
                .getContainerNumber());
        StockItem stockItem = new StockItem();
        stockItem.setItemId(vBean.getEntity().getStockItemBean().getStockItem().getItemId());
        stockItem.setBatch(vBean.getEntity().getStockItemBean().getStockItem().getBatch());
        stockItem.setQuantities(vBean.getEntity().getStockItemBean().getKeyboardQties());
        parametersBean.setStockItem(stockItem);

        // Put parameters in context and open feature
        getSharedContext().put(Domain.DOMAIN_THIS, NCConstants.NCFLOW_KEY_SHARED_CONTEXT, parametersBean);
        if (StockUITools.isTouchRunning()) {
            openChildFeature(NCConstants.FeatureName.FLOW_TOUCH.getName());
        } else {
            openChildFeature(NCConstants.FeatureName.FLOW.getName());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - actionOpenNewIncident()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = super.getActionsButtonModels();
        lst.add(getBtnShowAllModel());
        lst.add(getBtnFinishResumeModel());
        lst.add(getBtnDetailModel());

        ToolBarBtnStdModel resetBarCode = new ToolBarBtnStdModel();
        resetBarCode.setReference("resetBarCode");
        resetBarCode.setIconURL("null.png");
        resetBarCode.setText(I18nClientManager.translate(ProductionMo.T34423, false));
        resetBarCode.setShortcut(Key.K_CTRL_K);
        if (SystemHelper.getUseKeyboard()) {
            lst.add(resetBarCode);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()=" + lst);
        }
        return lst;
    }

    /**
     * Gets the btnDetailModel.
     * 
     * @category getter
     * @return the btnDetailModel.
     */
    protected ToolBarBtnFeatureModel getBtnDetailModel() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnDetailModel()");
        }

        if (this.btnDetailModel == null) {
            this.btnDetailModel = new ToolBarBtnFeatureModel();
            this.btnDetailModel.setReference(BTN_DETAIL_REFERENCE);
            this.btnDetailModel.setFeatureId(FDETAIL_ENTRY_CODE);
            this.btnDetailModel.setText(I18nClientManager.translate(ProductionMo.T29536, false));
            this.btnDetailModel.setIconURL("/images/vif/vif57/production/mo/detail.png");
            this.btnDetailModel.setShortcut(Key.K_F6);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnDetailModel()=" + this.btnDetailModel);
        }
        return this.btnDetailModel;
    }

    /**
     * Gets the btnFlashModel.
     * 
     * @category getter
     * @return the btnFlashModel.
     */
    protected ToolBarBtnStdModel getBtnFlashModel() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBtnFlashModel()");
        }

        if (btnFlashModel == null) {
            btnFlashModel = new ToolBarBtnStdModel();
            btnFlashModel.setIconURL("/images/vif/vif57/production/mo/flash.png");
            btnFlashModel.setReference(BTN_FLASH_REFERENCE);
            btnFlashModel.setSelected(true);
            btnFlashModel.setText(I18nClientManager.translate(ProductionMo.T29546, false));
            btnFlashModel.setToggleType(true);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBtnFlashModel()=" + btnFlashModel);
        }
        return btnFlashModel;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getExternalButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getExternalButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = new ArrayList<ToolBarBtnStdBaseModel>();
        lst.add(getBtnCancelLastModel());
        lst.add(getBtnFlashModel());
        lst.add(getBtnWeightModel());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getExternalButtonModels()=" + lst);
        }
        return lst;
    }

    /**
     * Manage flash button.
     */
    protected void manageBtnFlash() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageBtnFlash()");
        }

        // getViewerCtrl().setFlashQuantites(!getViewerCtrl().getFlashQuantites());
        if (InputBarcodeType.KEEP_QTIES.equals(getViewerCtrl().getWorkBean().getBarcodeType())) {
            getBtnFlashModel().setIconURL("/images/vif/vif57/production/mo/noflash.png");
            getBtnFlashModel().setText(I18nClientManager.translate(ProductionMo.T29547, false));
            getViewerCtrl().getViewerView().setFlashLabel(I18nClientManager.translate(ProductionMo.T29695, false));
        } else {
            getBtnFlashModel().setIconURL("/images/vif/vif57/production/mo/flash.png");
            getBtnFlashModel().setText(I18nClientManager.translate(ProductionMo.T29546, false));
            getViewerCtrl().getViewerView().setFlashLabel(I18nClientManager.translate(ProductionMo.T29697, false));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageBtnFlash()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void manageBtnShowAllModelText() {
        FProductionSBean sBean = (FProductionSBean) getBrowserController().getInitialSelection();
        if (!sBean.getAll()) {
            getBtnShowAllModel().setText(I18nClientManager.translate(ProductionMo.T29545, false));
            getBtnShowAllModel().setIconURL("/images/vif/vif57/production/mo/showfinished.png");
        } else {
            getBtnShowAllModel().setText(I18nClientManager.translate(ProductionMo.T29525, false));
            getBtnShowAllModel().setIconURL("/images/vif/vif57/production/mo/hidefinished.png");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void openChildFeature(final String featureId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openChildFeature(featureId=" + featureId + ")");
        }

        if (featureId.equals(FDETAIL_ENTRY_CODE)) {
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.FEATURE_TITLE, getModel().getTitle());
            getSharedContext().put(Domain.DOMAIN_THIS, FDataEntryListFCtrl.OPERATION_ITEM_BEAN,
                    getViewerCtrl().getBean().getBBean().getOperationItemBean());
            getSharedContext().put(Domain.DOMAIN_THIS, "inputEntityType",
                    getViewerCtrl().getBean().getInputItemParameters().getEntityType());
            fireSharedContextSend(getSharedContext());
            current = getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey();
        }
        super.openChildFeature(featureId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openChildFeature(featureId=" + featureId + ")");
        }
    }

    /**
     * Get the browser controller.
     * 
     * @return the browser controller
     */
    private FBoningInputBCtrl getBrowserCtrl() {
        return (FBoningInputBCtrl) getBrowserController();
    }

    /**
     * Get documents to show.
     * 
     * @return the list of pdf files
     * @throws BusinessException if error occurs
     */
    private List<PDFFile> getDocumentsToShow() throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDocumentsToShow()");
        }

        MOItemKey moItemKey = new MOItemKey();

        moItemKey.setEstablishmentKey(getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getEstablishmentKey());
        moItemKey.setChrono(getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getChrono());
        moItemKey.setCounter1(getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getCounter1());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDocumentToShow()");
        }
        return getFboningInputCBS().getPdfFilesOfCurrentItem(getIdCtx(), moItemKey);
    }

    /**
     * Get the feature view.
     * 
     * @return the feature view
     */
    private FBoningInputFView getFeatureView() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getFeatureView()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getFeatureView()");
        }
        return (FBoningInputFView) getView();

    }

    /**
     * Get the feature controller.
     * 
     * @return the feature view
     */
    private FBoningInputVCtrl getViewerCtrl() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getViewerCtrl()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getViewerCtrl()");
        }
        return (FBoningInputVCtrl) getViewerController();
    }

    /**
     * Get the feature view.
     * 
     * @return the feature view
     */
    private FBoningInputVView getViewerView() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getViewerView()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getViewerView()");
        }
        return (FBoningInputVView) getViewerController().getView();
    }

    /**
     * Finishes or unfinishes an item.
     */
    private void processFinishResume() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processFinishResume()");
        }

        int result = 0;
        if (getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                .equals(ManagementType.REAL)) {
            result = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29549, false), MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {
                try {
                    String question = getFboningInputCBS().finishOperationItem(getIdCtx(),
                            getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey(),
                            getIdCtx().getWorkstation(), ActivityItemType.INPUT, false);
                    if (question != null
                            && !question.isEmpty()
                            && getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                                    question + "\n" + I18nClientManager.translate(ProductionMo.T29570, false),
                                    MessageButtons.YES_NO) == MessageButtons.YES) {
                        getFboningInputCBS().finishOperationItem(getIdCtx(),
                                getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey(),
                                getIdCtx().getWorkstation(), ActivityItemType.INPUT, true);
                    }
                    if (getViewerCtrl().getBean().getBBean().getOperationItemBean() != null) {
                        current = getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey();
                        current.setManagementType(ManagementType.ARCHIVED);
                    }

                    getViewerView().getItfBatch().setAlwaysDisabled(true);
                    getViewerView().getQuantityUnit1View().getItfQuantity().setAlwaysDisabled(true);
                    getViewerView().getQuantityUnit2View().getItfQuantity().setAlwaysDisabled(true);
                } catch (BusinessException e) {
                    getView().show(new UIException(e));
                }
            }
        } else if (getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey()
                .getManagementType().equals(ManagementType.ARCHIVED)) {
            result = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29548, false), MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {
                try {
                    getFboningInputCBS().resumeOperationItem(getIdCtx(),
                            getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey(),
                            getIdCtx().getWorkstation());
                    if (getViewerCtrl().getBean().getBBean().getOperationItemBean() != null) {
                        current = getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey();
                        current.setManagementType(ManagementType.REAL);
                    }
                    getViewerView().getItfBatch().setAlwaysDisabled(false);
                    getViewerView().getQuantityUnit1View().getItfQuantity().setAlwaysDisabled(false);
                    getViewerView().getQuantityUnit2View().getItfQuantity().setAlwaysDisabled(false);
                } catch (BusinessException e) {
                    getView().show(new UIException(e));
                }
            }
        }
        getBrowserController().reopenQuery();
        if (current != null) {
            getBrowserCtrl().repositionToOperationItemKey(current);
        }

        // close the feature if there is no item to show
        if (getBrowserCtrl().getModel().getBeans().size() == 0) {
            fireSelfFeatureClosingRequired();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processFinishResume()");
        }
    }

    /**
     * Process the 'showall' event.
     */
    private void processShowAllEvent() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processShowAllEvent()");
        }

        FProductionSBean sBean = (FProductionSBean) getBrowserController().getInitialSelection();
        sBean.setAll(!sBean.getAll());
        getViewerCtrl().getWorkBean().setShowFinished(sBean.getAll());
        manageBtnShowAllModelText();
        if (getViewerCtrl().getBean().getBBean().getOperationItemBean() != null) {
            current = getViewerCtrl().getBean().getBBean().getOperationItemBean().getOperationItemKey();
        }
        getBrowserController().reopenQuery();
        if (current != null) {
            getBrowserCtrl().repositionToOperationItemKey(current);
            current = null;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processShowAllEvent()");
        }
    }
}

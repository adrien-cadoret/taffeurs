/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMSOCreationVCtrl.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.activities.activities.business.beans.features.fstackingplan.FStackingPlanSBean;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labels.LabelEvent;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBoningBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fboningmocreation.FBoningMOCreationCBS;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fboningmsocreation.FBoningMSOCreationCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.CodeFunction;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.flabels.LabelPrintHelper;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation.touch.FBoningMSOCreationVView;


/**
 * MSO creation viewer controller.
 * 
 * @author glc
 */
public class FBoningMSOCreationVCtrl extends AbstractStandardViewerController<FBoningMSOCreationVBean> {

    private static final Logger   LOGGER = Logger.getLogger(FBoningMSOCreationVCtrl.class);

    private FBoningMSOCreationCBS fboningmSOCreationCBS;
    private FBoningMOCreationCBS  fBoningMOCreationCBS;

    /**
     * Fire performSave.
     * 
     * @return <code>true</code> if the bean was successfully saved.
     */
    public boolean firePerformSave() {
        return this.performSave();
    }

    /**
     * Gets the fBoningMOCreationCBS.
     *
     * @return the fBoningMOCreationCBS.
     */
    public FBoningMOCreationCBS getfBoningMOCreationCBS() {
        return fBoningMOCreationCBS;
    }

    /**
     * Gets the fboningmSOCreationCBS.
     * 
     * @return the fboningmSOCreationCBS.
     */
    public FBoningMSOCreationCBS getFboningmSOCreationCBS() {
        return fboningmSOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeViewWithBean(final FBoningMSOCreationVBean bean) {
        super.initializeViewWithBean(bean);

        // Add fabrication id and qty to the stacking plan composite
        FStackingPlanSBean stackingPlanSBean = new FStackingPlanSBean();
        stackingPlanSBean.setEstablishmentKey(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx()
                .getEstablishment()));
        stackingPlanSBean.setActivityId(bean.getFabrication().getFabricationCLs().getCode());
        stackingPlanSBean.setFabricationType(bean.getFabrication().getFabricationType());
        stackingPlanSBean.setQty(bean.getQuantityUnit().getQty());
        ((FBoningMSOCreationVView) getView()).getcStackingPlanView().setCodeInitialHelpSelection(stackingPlanSBean);
    }

    /**
     * Sets the fBoningMOCreationCBS.
     *
     * @param fBoningMOCreationCBS fBoningMOCreationCBS.
     */
    public void setfBoningMOCreationCBS(final FBoningMOCreationCBS fBoningMOCreationCBS) {
        this.fBoningMOCreationCBS = fBoningMOCreationCBS;
    }

    /**
     * Sets the fboningmSOCreationCBS.
     * 
     * @param fboningmSOCreationCBS fboningmSOCreationCBS.
     */
    public void setFboningmSOCreationCBS(final FBoningMSOCreationCBS fboningmSOCreationCBS) {
        this.fboningmSOCreationCBS = fboningmSOCreationCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
        if (FBoningMSOCreationVModel.QUANTITY.equals(beanProperty)) {
            FStackingPlanSBean stackingPlanSBean = new FStackingPlanSBean();
            stackingPlanSBean.setEstablishmentKey(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx()
                    .getEstablishment()));
            stackingPlanSBean.setActivityId(getBean().getFabrication().getFabricationCLs().getCode());
            stackingPlanSBean.setFabricationType(getBean().getFabrication().getFabricationType());
            stackingPlanSBean.setQty(getBean().getQuantityUnit().getQty());
            ((FBoningMSOCreationVView) getView()).getcStackingPlanView().setCodeInitialHelpSelection(stackingPlanSBean);
            preinitializeStackingPlan(getBean(), stackingPlanSBean);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningMSOCreationVBean convert(final Object bean) throws UIException {
        FBoningMSOCreationVBean vbean = (FBoningMSOCreationVBean) bean;
        // add the stacking plan
        FStackingPlanSBean stackingPlanSBean = new FStackingPlanSBean();
        stackingPlanSBean.setEstablishmentKey(new EstablishmentKey(getIdCtx().getCompany(), getIdCtx()
                .getEstablishment()));
        stackingPlanSBean.setActivityId(vbean.getFabrication().getFabricationCLs().getCode());
        stackingPlanSBean.setFabricationType(vbean.getFabrication().getFabricationType());
        stackingPlanSBean.setQty(vbean.getQuantityUnit().getQty());
        ((FBoningMSOCreationVView) getView()).getcStackingPlanView().setCodeInitialHelpSelection(stackingPlanSBean);

        try {
            preinitializeStackingPlan(vbean, stackingPlanSBean);
            // bean.setStackingPlanCL(getBean().getStackingPlanCL());
            // fireBeanUpdated(bean);
        } catch (UIException e) {
            LOGGER.error("", e);
            getView().showError(e.getMessage());
        }

        return vbean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningMSOCreationVBean insertBean(final FBoningMSOCreationVBean bean,
            final FBoningMSOCreationVBean initialBean) throws UIException, UIErrorsException {
        return bean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean ret = true;
        FBoningMSOCreationVBean vbean = getModel().getBean();
        if (vbean == null || vbean.getMoKey() == null) {
            ret = false;
        }
        if (ret && reference.equals(FBoningMSOCreationFCtrl.EVENT_OPENCRITERIA)) {
            ret = getModel().getBean().getReturnInitBean().getListCriteria().size() > 0;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionEnabled(final ToolBarButtonType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isPanelActionEnabled(type=" + type + ")");
        }

        boolean ret = false;
        if (type == ToolBarButtonType.SAVE) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isPanelActionEnabled(type=" + type + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningMSOCreationVBean updateBean(final FBoningMSOCreationVBean bean) throws UIException,
            UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(bean=" + bean + ")");
        }

        checkFMSOCreationVBean(bean);
        try {
            MOBoningBean moBean = getFboningmSOCreationCBS().createStartedMSO(getIdCtx(), bean);
            LabelEvent labelEvent = new LabelEvent();
            labelEvent.setMoKey(moBean.getMoKey());
            labelEvent.setEventType(LabelingEventType.MO_CREATION);
            labelEvent.setWorkstationId(getIdCtx().getWorkstation());
            labelEvent.setFeatureId(CodeFunction.PROD_IN.getValue());
            LabelPrintHelper labelPrintHelper = StandardControllerFactory.getInstance().getController(
                    LabelPrintHelper.class);
            labelPrintHelper.launchPrint(getView(), getIdentification(), getIdCtx(), labelEvent);
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.STARTED_MO_BEAN, moBean);
            fireSharedContextSend(getSharedContext());
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(bean=" + bean + ")=" + bean);
        }
        return bean;
    }

    /**
     * Check the FMSOCretionVBean.
     * 
     * @param vbean the viewer bean
     * @throws UIException if error occurs.
     */
    private void checkFMSOCreationVBean(final FBoningMSOCreationVBean vbean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkFMSOCreationVBean(vbean=" + vbean + ")");
        }

        if (vbean.getQuantityUnit().getQty() == 0) {
            throw new UIException(Generic.T20724, new VarParamTranslation(getBean().getQuantityUnit().getQty()));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkFMSOCreationVBean(vbean=" + vbean + ")");
        }
    }

    /**
     * Preinitialize Stacking Plan Input text field.
     * 
     * @param bean the bean to update
     * @param selection selection
     * @throws UIException if error occurs
     */
    private void preinitializeStackingPlan(final FBoningMSOCreationVBean bean, final FStackingPlanSBean selection)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - preinitializeStackingPlan(selection=" + selection + ")");
        }

        List<CodeLabel> lst = new ArrayList<CodeLabel>();
        try {

            lst = getfBoningMOCreationCBS().getSelectedStackingPlans(getIdCtx(), selection, 0, 10000);

            if (lst == null || (lst != null && lst.size() == 0)) {
                bean.setStackingPlanCL(new CodeLabel());
                ((FBoningMSOCreationVView) getView()).getcStackingPlanView().disableItfStackingPlan();
            } else {
                ((FBoningMSOCreationVView) getView()).getcStackingPlanView().enableItfStackingPlan();
            }

            if (bean.getStackingPlanCL() == null || bean.getStackingPlanCL().getCode() == null
                    || bean.getStackingPlanCL().getCode().isEmpty() || !lst.contains(bean.getStackingPlanCL())) {
                if (lst != null && lst.size() > 0) {
                    bean.setStackingPlanCL(lst.get(0));
                } else {
                    bean.setStackingPlanCL(new CodeLabel());
                }
            }

        } catch (BusinessException e) {
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - preinitializeStackingPlan(selection=" + selection + ")");
        }
    }

}

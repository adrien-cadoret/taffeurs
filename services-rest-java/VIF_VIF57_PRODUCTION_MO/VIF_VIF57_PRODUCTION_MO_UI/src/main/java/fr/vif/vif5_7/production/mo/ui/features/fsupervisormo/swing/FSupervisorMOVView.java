/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOVView.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing;


import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.io.File;
import java.util.Collection;
import java.util.List;

import javax.swing.Action;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPopupMenu;
import javax.swing.filechooser.FileFilter;

import org.apache.log4j.Logger;

import com.jidesoft.docking.DefaultDockingManager;
import com.jidesoft.docking.DockableFrame;
import com.jidesoft.docking.DockableHolder;
import com.jidesoft.docking.DockingManager;
import com.jidesoft.docking.event.DockableFrameEvent;
import com.jidesoft.plaf.LookAndFeelFactory;

import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.swing.SwingHelper;
import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOVBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.actions.CommonParameterAction;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.actions.CommonRefreshAction;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing.FGanttMOInternalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOIFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.MOLocalFrame;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FSupervisorMOCheckBoxMenu;


/**
 * FSupervisorMO : Viewer View.
 * 
 * @author jd
 */
public class FSupervisorMOVView extends FSupervisorMODockableFrameAdaptor<FSupervisorMOVBean> implements
FSupervisorMOIVView, DockableHolder {

    /**
     * 
     * Class for filter.
     * 
     * @author ag
     */
    class ExtensionFileFilter extends FileFilter {
        private String   description;

        private String[] extensions;

        /**
         * Constructor.
         * 
         * @param description String
         * @param extension String
         */
        public ExtensionFileFilter(final String description, final String extension) {
            this(description, new String[] { extension });
        }

        /**
         * Constructor.
         * 
         * @param description String
         * @param extensions String
         */
        public ExtensionFileFilter(final String description, final String[] extensions) {
            if (description == null) {
                this.description = extensions[0];
            } else {
                this.description = description;
            }
            this.extensions = extensions.clone();
            toLower(this.extensions);
        }

        @Override
        public boolean accept(final File file) {
            boolean result = false;
            if (file.isDirectory()) {
                result = true;
            } else {
                String path = file.getAbsolutePath().toLowerCase();
                for (int i = 0, n = extensions.length; i < n; i++) {
                    String extension = extensions[i];
                    if ((path.endsWith(extension) && (path.charAt(path.length() - extension.length() - 1)) == '.')) {
                        result = true;
                    }
                }
            }
            return result;
        }

        @Override
        public String getDescription() {
            return description;
        }

        /**
         * 
         * Lower the result string.
         * 
         * @param array String
         */
        private void toLower(final String[] array) {
            for (int i = 0, n = array.length; i < n; i++) {
                array[i] = array[i].toLowerCase();
            }
        }
    }

    /** LOGGER. */
    private static final Logger LOGGER   = Logger.getLogger(FSupervisorMOVView.class);

    private JMenuBar            bar      = new JMenuBar();

    private DockingManager      dockingManager;

    private ActionListener      listener = null;

    /**
     * Default constructor.
     * 
     */
    public FSupervisorMOVView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public synchronized void addMouseListener(final MouseListener l) {
        super.addMouseListener(l);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public String askFileName(final String path) {
        String ret = "";

        JFileChooser dialogue = new JFileChooser(new File(path));
        dialogue.setFileSelectionMode(JFileChooser.FILES_ONLY);
        // FIXME TRAD
        dialogue.setFileFilter(new ExtensionFileFilter("TITRE", /*
         * I18nClientManager.translate(PreparationKernel.T32201,
         * false),
         */
                new String[] { "JID" }));
        dialogue.setAcceptAllFileFilterUsed(false);

        dialogue.setAcceptAllFileFilterUsed(false);
        if (dialogue.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
            File f = dialogue.getSelectedFile();
            ret = f.getName(); // AbsolutePath();
        }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String askViewName() {

        String ret = JOptionPane.showInputDialog(null, I18nClientManager.translate(ProductionMo.T32601, false),
                I18nClientManager.translate(ProductionMo.T32601, false), JOptionPane.QUESTION_MESSAGE);

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void createFloatingDock(final AbstractMODockableView frameToAdd) {
        Rectangle rectangle = null;
        boolean isFloated = true;
        Point location = null;

        DockableFrame dOld = getDockingManager().getFrame(frameToAdd.getLocalFrame().getUniqueId());

        if (dOld != null) {
            isFloated = dOld.isFloated();
            rectangle = dOld.getBounds();
            location = dOld.getLocation();

            destroy(frameToAdd.getLocalFrame());
        }

        DockableFrame d = createDockableFrame(frameToAdd);
        d.setFloatable(true); // dOld == null || (dOld != null && dOld.isFloated()));

        getDockingManager().addFrame(d);

        int posX = 0; // frameToAdd.getInitBean().getPosX();
        int posY = 0; // frameToAdd.getInitBean().getPosY();
        int width = 0; // (int) frameToAdd.getInitBean().getPreferedSize().getWidth();
        int height = 0; // (int) frameToAdd.getInitBean().getPreferedSize().getHeight();

        if (rectangle != null) {

            if (isFloated) {
                rectangle.setLocation(location);
                getDockingManager().floatFrame(d.getKey(), rectangle, true);
            }
        } else {
            if (width == 0) {
                width = 500;
            }
            if (height == 0) {
                height = 300;
            }

            if (posX == 0) {
                posX = ((getWidth() - width) / 2);
            }
            if (posY == 0) {
                posY = ((getHeight() - height) / 2);
            }
            getDockingManager().floatFrame(d.getKey(), new Rectangle(posX, posY, width, height), true);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public JPopupMenu createPopupMenu(final ActionListener actionListener, final List<MOLocalFrame> localFrames,
            final List<String> views) {

        JPopupMenu popupMenu = FSupervisorMOTools.createFramePopup(actionListener, localFrames);
        //
        bar = FSupervisorMOTools.createFrameMenuBar(actionListener, localFrames, views);

        this.listener = actionListener;

        // DockableFrame dockFrame = new DockableFrame("MENUBAR");
        // dockFrame.setFloatable(true);
        // dockFrame.setDockable(false);
        // // dockFrame.setBounds(0, 0, 300, 48);
        // // dockFrame.setPreferredSize(new Dimension(300, 48));
        // // dockFrame.setTitle(view.getInitBean().getTitle());
        // // dockFrame.setToolTipText(view.getInitBean().getTitle());
        // // dockFrame.setTabTitle(view.getInitBean().getTitle());
        // dockFrame.add(bar);
        //
        // dockingManager.addFrame(dockFrame);
        //
        // dockingManager.floatFrame(dockFrame.getKey(), new Rectangle(0, 0, 300, 48), true);
        add(bar, BorderLayout.SOUTH);

        // ((BasicInternalFrameUI) getUI()).getSouthPane().setBackground(Color.BLACK);

        return popupMenu;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void destroy(final MOLocalFrame localFrame) {
        DockableFrame frame = getDockingManager().getFrame(localFrame.getUniqueId());
        if (frame != null) {
            localFrame.getFrameBean().setIsVisible(false);
            List<JMenu> list = SwingHelper.findComponentsInstanceOf(bar, JMenu.class, true);
            for (JMenu m : list) {

                for (Component c : m.getMenuComponents()) {
                    if (c instanceof FSupervisorMOCheckBoxMenu) {
                        if (localFrame.getUniqueId().equalsIgnoreCase(
                                ((FSupervisorMOCheckBoxMenu) c).getLocalFrame().getUniqueId())) {
                            ((FSupervisorMOCheckBoxMenu) c).setSelected(false);
                        }
                    }
                }
            }
            getDockingManager().removeFrame(frame.getKey());
            frame.dispose();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dockableFrameHidden(final DockableFrameEvent arg0) {
        DockableFrame frame = getDockingManager().getFrame(arg0.getDockableFrame().getKey());

        if (frame != null) {
            List<AbstractMODockableView> views = SwingHelper.findComponentsInstanceOf(frame,
                    AbstractMODockableView.class, true);

            // LocalFrame localFrame = null;
            for (AbstractMODockableView view : views) {

                ActionEvent e = new ActionEvent(view, 0, FSupervisorMOTools.FRAME_IS_CLOSING);
                listener.actionPerformed(e);
                destroy(view.getLocalFrame());

            }

        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public DockingManager getDockingManager() {

        return dockingManager;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    @Deprecated
    public void openView(final String path, final String viewName) {

        // String completePath = FSupervisorMOManager.checkPath(path, getModel().getIdentification().getLogin());
        // if (false) {
        // getDockingManager().loadLayoutDataFromFile(completePath + viewName + FSupervisorMOManager.EXTENSION_JID);
        // }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void paint(final Graphics g) {

        super.paint(g);

        if (getDockingManager() != null && getDockingManager().getAllVisibleFrameKeys().length == 0) {
            Image img = new ImageIcon(this.getClass().getResource("/images/indicator/fond186x186.png")).getImage();
            if (img != null) {
                g.drawImage(img, getWidth() / 2 - 24, getHeight() / 2 - 24, this);
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void saveEnvironment(final String fileName) {
        if (EntityTools.isValid(fileName) && getDockingManager() != null) {
            // getDockingManager().saveLayoutDataToFile(fileName);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setDockables(final String path, final List<FSupervisorMOIFrame> values) {
        this.setLayout(new BorderLayout());

        JFrame frame = (JFrame) ((JPanel) getParent()).getTopLevelAncestor();

        dockingManager = new DefaultDockingManager(frame, this);

        getDockingManager().setShowWorkspace(false);

        this.add(getDockingManager().getDockedFrameContainer(), BorderLayout.NORTH);

        LookAndFeelFactory.installJideExtension(LookAndFeelFactory.XERTO_STYLE);

        setVisible(true);

        for (FSupervisorMOIFrame f : values) {
            AbstractMODockableView frameToAdd = (AbstractMODockableView) f;

            DockableFrame d = createDockableFrame(frameToAdd);
            getDockingManager().setProfileKey("jidesoft-simplest");
            getDockingManager().addFrame(d);
        }

        Collection<String> list = getDockingManager().getAllFrames();
        for (String s : list) {
            for (FSupervisorMOIFrame f : values) {
                if (s.equalsIgnoreCase(f.getLocalFrame().getUniqueId())) {
                    getDockingManager().getFrame(s).setTitle(f.getLocalFrame().getFrameBean().getTitle());
                }
            }
        }

    }

    /**
     * 
     * Create a dockable frame.
     * 
     * @param view AbstractMODockableView
     * @return DockableFrame
     */
    private DockableFrame createDockableFrame(final AbstractMODockableView view) {

        Icon icon = null;
        if (view.getLocalFrame().getSelection() != null) {
            icon = FSupervisorMOTools.getIcon16(FSupervisorMOTools
                    .getIcon(view.getLocalFrame().getSelection().getAxe()));
        }
        // if (view.getLocalFrame().getFrameBean().getIcon() != null) {
        // icon = IconFactory.getIcon16(view.getInitBean().getIcon());
        // }
        String key = view.getLocalFrame().getUniqueId();
        // if (!EntityTools.isValid(key)) {
        // key = Integer.toString(view.getInitBean().hashCode());
        // }

        // DockableFrame dockFrame = new DockableFrame(key, icon);
        // dockFrame.setTitle(view.getLocalFrame().getFrameBean().getTitle());
        // dockFrame.setToolTipText(view.getLocalFrame().getFrameBean().getTitle());
        // dockFrame.setTabTitle(view.getLocalFrame().getFrameBean().getTitle());
        // // Not allowed to close frame by close button (not correctly managed)
        // dockFrame.setHidable(true); // view.getLocalFrame().getFrameBean().isHidable());
        // dockFrame.add(view);
        // dockFrame.setVisible(true);
        // dockFrame.addDockableFrameListener(this);
        // return dockFrame;

        DockableFrame dockFrame = new DockableFrame(key, icon) {

            /**
             * {@inheritDoc}
             */
            @Override
            public Action getCloseAction() {
                return new CloseAction() {

                    /**
                     * {@inheritDoc}
                     */
                    @Override
                    public void actionPerformed(final ActionEvent paramActionEvent) {
                        // FSupervisorDockableHolder.this.destroy(getKey());

                        Object src = null;
                        List<JMenu> list = SwingHelper.findComponentsInstanceOf(bar, JMenu.class, true);
                        for (JMenu m : list) {
                            for (Component c : m.getMenuComponents()) {
                                if (c instanceof FSupervisorMOCheckBoxMenu) {
                                    if (getKey().equalsIgnoreCase(
                                            ((FSupervisorMOCheckBoxMenu) c).getLocalFrame().getUniqueId())) {
                                        src = c;
                                        break;
                                    }
                                }
                            }
                        }
                        if (src != null) {
                            ActionEvent event = new ActionEvent(src, paramActionEvent.getID(),
                                    FSupervisorMOTools.CREATE_NEW_DOCK);
                            listener.actionPerformed(event);
                        }

                        super.actionPerformed(paramActionEvent);
                    }

                };
            }

        };
        dockFrame.addDockableFrameListener(this);

        dockFrame.setTitle(view.getLocalFrame().getFrameBean().getTitle());
        dockFrame.setToolTipText(view.getLocalFrame().getFrameBean().getTitle());
        dockFrame.setTabTitle(view.getLocalFrame().getFrameBean().getTitle());
        // Not allowed to close frame by close button (not correctly managed)
        dockFrame.setHidable(true); // view.getLocalFrame().getFrameBean().isHidable());
        dockFrame.add(view);

        dockFrame.addAdditionalButtonActions(new CommonRefreshAction(view));
        if (view instanceof FGanttMOInternalFrame)
            dockFrame.addAdditionalButtonActions(new CommonParameterAction(view));

        /*
         * if (FSupervisorMOTools.showAxesFilterAction(view.getLocalFrame())) { dockFrame.addAdditionalButtonActions(new
         * CommonAxesFilterAction(view)); }
         * 
         * if (FSupervisorMOTools.showFilterAction(view.getLocalFrame())) { dockFrame.addAdditionalButtonActions(new
         * CommonFilterAction(view)); }
         * 
         * if (FSupervisorMOTools.showMenuAction(view.getLocalFrame())) { dockFrame.addAdditionalButtonActions(new
         * CommonMenuAction(view)); }
         * 
         * if (FSupervisorMOTools.showSelectionAction(view.getLocalFrame())) { dockFrame.addAdditionalButtonActions(new
         * CommonSelectionAction(view)); }
         */

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createDockableFrame(view=" + view + ")=" + dockFrame);
        }
        return dockFrame;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        // setOpaque(false);
        // setBackground(Color.BLACK);
    }

}

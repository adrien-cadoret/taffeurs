/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOListBView.java,v $
 * Created on 15 mars 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist.touch;


import java.awt.Component;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

import fr.vif.jtech.ui.browser.touch.TouchLineBrowser;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmolist.FBoningMOListBBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOType;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist.FBoningMOListBIView;


/**
 * Browser view for boning mo list.
 * 
 * @author cj
 */
public class FBoningMOListBView extends TouchLineBrowser<FBoningMOListBBean> implements FBoningMOListBIView {
    /**
     * rendrer with icon.
     * 
     * @author gv
     * @version $Revision: 1.1 $, $Date: 2013/04/16 13:23:25 $
     */
    private class IconRenderer implements TableCellRenderer {
        /**
         * {@inheritDoc}
         */
        @Override
        public Component getTableCellRendererComponent(final JTable table, final Object value,
                final boolean isSelected, final boolean hasFocus, final int row, final int column) {

            JLabel jl = new JLabel();
            if (isSelected) {
                jl.setBackground(TouchHelper.getAwtColor(MOUIConstants.TOUCHSCREEN_BG_SELECTED_BROWSE_LINE));
            } else if (row % 2 == 0) {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_ODD_CELL));
            } else {
                jl.setBackground(TouchHelper.getAwtColor(StandardColor.TOUCHSCREEN_BG_EVEN_CELL));
            }

            FBoningMOListBBean moBean = getModel().getBeanAt(row);

            if (moBean.getIsFinished()) {
                jl.setIcon(new ImageIcon(getClass().getResource("/images/vif/vif57/production/mo/finishedMo.png")));
            } else if (MOType.ORIGIN.equals(moBean.getMobean().getMoType())) {
                jl.setIcon(new ImageIcon(getClass().getResource("/images/vif/vif57/production/mo/originMo.png")));
            }
            jl.setOpaque(true);
            return jl;
        }
    }

    /**
     * Default constructor.
     * 
     */
    public FBoningMOListBView() {
        super();
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        for (int i = 0; i < getModel().getColumnCount(); i++) {
            if ("mobean".equals(getModel().getBeanAttributeAt(i))) {
                getJTable().getColumnModel().getColumn(i).setCellRenderer(new IconRenderer());
            }
        }
    }
}

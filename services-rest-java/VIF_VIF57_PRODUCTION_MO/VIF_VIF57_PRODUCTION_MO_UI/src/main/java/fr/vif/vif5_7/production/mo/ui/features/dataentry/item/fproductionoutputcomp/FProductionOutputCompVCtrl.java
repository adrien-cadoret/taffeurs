/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompVCtrl.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp;


import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdowngradeditem.FDowngradedItemSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompMapKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionHelper;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer.AbstractFProductionOutputVCtrl;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fdestocking.FDestockingSBean;


/**
 * Production Output Comp Viewer Controller.
 * 
 * @author kl
 */
public class FProductionOutputCompVCtrl extends
AbstractFProductionOutputVCtrl<FProductionOutputCompVBean, FProductionOutputCompCBS> implements
        TableRowChangeListener {
    /** LOGGER. */
    private static final Logger                                          LOGGER = Logger.getLogger(FProductionOutputCompVCtrl.class);

    /** Bean. **/
    private Map<FProductionOutputCompMapKey, FProductionOutputCompVBean> map    = new HashMap<FProductionOutputCompMapKey, FProductionOutputCompVBean>();

    /**
     * Gets the map.
     *
     * @return the map.
     */
    public Map<FProductionOutputCompMapKey, FProductionOutputCompVBean> getMap() {
        return map;
    }

    @Override
    public void removeCurrent() {
        FProductionBBean bBean = getBean().getBBean();
        MovementAct movementAct = FProductionTools.operationItemKeyToMovementAct(bBean.getOperationItemBean()
                .getOperationItemKey());
        FProductionOutputCompMapKey key = new FProductionOutputCompMapKey(movementAct, bBean.getItemCLs(),
                bBean.getNatureComplementaryWorkshop());
        if (map.containsKey(key)) {
            map.remove(key);
        }
    }

    /**
     * Save current bean to map.
     */
    @Override
    public void saveCurrent() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveCurrent()");
        }

        FProductionBBean bBean = getBean().getBBean();
        MovementAct movementAct = FProductionTools.operationItemKeyToMovementAct(bBean.getOperationItemBean()
                .getOperationItemKey());
        FProductionOutputCompMapKey key = new FProductionOutputCompMapKey(movementAct, bBean.getItemCLs(),
                bBean.getNatureComplementaryWorkshop());
        if (map.containsKey(key)) {
            map.remove(key);
        }
        map.put(key, getBean());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveCurrent()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        super.selectedRowChanging(event);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FProductionOutputCompVBean convert(final Object pBean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(pBean=" + pBean + ")");
        }

        FProductionBBean bBean = (FProductionBBean) pBean;

        // KL / NLE 08/08/2014 Q007384
        FProductionOutputCompVBean vBean = new FProductionOutputCompVBean();
        vBean.setBBean(bBean);

        try {
            if (ManagementType.ARCHIVED.equals(((FProductionBBean) pBean).getOperationItemBean().getOperationItemKey()
                    .getManagementType())
                    || !map.containsKey(FProductionTools.operationItemKeyToMovementAct(((FProductionBBean) pBean)
                            .getOperationItemBean().getOperationItemKey()))) {
                vBean = getFproductionOutputContainerCBS().convert(getIdCtx(), bBean, getWorkBean());
            } else {
                vBean = map.get(FProductionTools.operationItemKeyToMovementAct(((FProductionBBean) pBean)
                        .getOperationItemBean().getOperationItemKey()));
                vBean.setBBean((FProductionBBean) pBean);
            }

            // Change batch selection
            getViewerView().getDowngradedItemView().setSelection(
                    FProductionHelper.getFDowngradedItemSBean((OutputOperationItemBean) vBean.getBBean()
                            .getOperationItemBean(), getWorkBean().getRealWorkstationId(),
                            (FDowngradedItemSBean) getViewerView().getDowngradedItemView().getSelection()));

            // Change batch selection
            getViewerView().getItfBatch().setInitialHelpSelection(
                    FProductionHelper.getFDestockingSBean(vBean.getBBean().getOperationItemBean(), vBean.getEntity()
                            .getStockItemBean().getGeneralItemInfos().getItemCL().getCode(),
                            (FDestockingSBean) getViewerView().getItfBatch().getInitialHelpSelection()));

        } catch (BusinessException e) {
            getView().show(new UIException(e));
            // throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

}

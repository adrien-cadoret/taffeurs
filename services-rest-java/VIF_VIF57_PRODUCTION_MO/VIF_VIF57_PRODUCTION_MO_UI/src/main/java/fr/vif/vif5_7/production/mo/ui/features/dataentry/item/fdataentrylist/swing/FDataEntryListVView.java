/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListVView.java,v $
 * Created on 07 avr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.swing;


import java.awt.GridBagLayout;

import fr.vif.jtech.ui.viewer.swing.StandardSwingViewer;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListVBean;


/**
 * DataEntryList : Viewer View.
 * 
 * @author vr
 */
public class FDataEntryListVView extends StandardSwingViewer<FDataEntryListVBean> {

    /**
     * Default constructor.
     *
     */
    public FDataEntryListVView() {
        super();
        initialize();
    }

    /**
     * This Method Initializes this.
     *
     */
    private void initialize() {
        this.setLayout(new GridBagLayout());
        // TODO : Set the feature size
        // this.setSize(new java.awt.Dimension(width, height));
        // this.setPreferredSize(new java.awt.Dimension(width, height));

        // TODO : Add components to feature
    }

}

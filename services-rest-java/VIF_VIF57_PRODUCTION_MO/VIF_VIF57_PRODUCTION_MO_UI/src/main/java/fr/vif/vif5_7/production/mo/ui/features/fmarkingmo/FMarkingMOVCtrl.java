/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMarkingMOVCtrl.java,v $
 * Created on 10 janv. 2014 by lmo
 */
package fr.vif.vif5_7.production.mo.ui.features.fmarkingmo;


import static fr.vif.jtech.ui.models.sharedcontext.Domain.DOMAIN_THIS;
import static fr.vif.jtech.ui.models.sharedcontext.SharedContext.KEY_VIEWER_BEAN;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.FatalException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.ui.events.bean.BeanUpdateEvent;
import fr.vif.jtech.ui.events.bean.BeanUpdateListener;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UILevel;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.models.sharedcontext.SharedContext;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOInitFeatureReturnBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOLine;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOMODescription;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fmarkingmo.FMarkingMOVBean;
import fr.vif.vif5_7.production.mo.business.services.features.fmarkingmo.FMarkingMOCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch.FMarkingMOIView;
import fr.vif.vif5_7.production.mo.ui.features.fmarkingmo.touch.FMarkingMOTouchVView;
import fr.vif.vif5_7.workshop.device.business.beans.common.document.Document;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingDevice;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingDevices;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingEnum.DeviceState;
import fr.vif.vif5_7.workshop.device.business.beans.composites.marking.MarkingEnum.LineState;


/**
 * MarkingMO : Viewer Controller.
 * 
 * @author lmo
 */
public class FMarkingMOVCtrl extends AbstractStandardViewerController<FMarkingMOVBean> implements BeanUpdateListener {

    /** LOGGER. */
    private static final Logger                  LOGGER          = Logger.getLogger(FMarkingMOVCtrl.class);

    private FMarkingMOCBS                        fmarkingMOCBS;

    private FMarkingMOLine                       line            = null;

    private Map<String, FMarkingMOMODescription> mapDescriptions = new HashMap<String, FMarkingMOMODescription>();

    /**
     * {@inheritDoc}
     */
    @Override
    public void beanCreated(final BeanUpdateEvent event) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beanDeleted(final BeanUpdateEvent event) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void beanUpdated(final BeanUpdateEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - beanUpdated(event=" + event + ")");
        }

        fireButtonStatechanged();
        getView().render();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - beanUpdated(event=" + event + ")");
        }
    }

    /**
     * Put new documents list into the selectedItem bean then notify the MarkingHeadsCtrl of this change.
     * 
     * /!\ VCtrl can't ask question to the user so, validation must be done BEFORE, by the FCtrl.
     * 
     * @param aListDoc new documents list
     */
    public void changeDocuments(final List<Document> aListDoc) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - changeDocuments(aListDoc=" + aListDoc + ")");
        }

        getBean().getSelectedItem().setDocuments(aListDoc);
        ((FMarkingMOIView) getView()).getIMarkingHeadsCtrl().changeDocuments();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - changeDocuments(aListDoc=" + aListDoc + ")");
        }
    }

    /**
     * Change the MO line by loading informations about the new line's devices inside the IMarkingViewCtrl.
     */
    public void changeLine() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - changeLine()");
        }

        FMarkingMOInitFeatureReturnBean vInitFeatureBean = (FMarkingMOInitFeatureReturnBean) getSharedContext().get(
                DOMAIN_THIS, FMarkingMOUIConstants.INIT_FEATURE_BEAN);

        FMarkingMOSBean selection = (FMarkingMOSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                FMarkingMOUIConstants.CURRENT_SELECTION_BEAN);
        CodeLabel vSearchedLine = selection.getLineCL();

        if (vSearchedLine != null) {
            for (FMarkingMOLine vLine : vInitFeatureBean.getLines()) {
                if (vSearchedLine.equals(vLine.getLineCL())) {
                    line = vLine;
                    break;
                }
            }
        }

        if (line == null) {
            line = new FMarkingMOLine();
        }
        FMarkingMOVBean vBean = getView().getModel().getBean();
        if (vBean != null) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("changeLine() : new line:" + line);
            }
            vBean.setDevices(line.getDevices());
            ((FMarkingMOIView) getView()).getIMarkingHeadsCtrl().setAttributes(vBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - changeLine()");
        }
    }

    /**
     * Check if there is no restriction to change row in the browser.
     * 
     * @throws UIException if the check is negative.
     * 
     */
    public void checkChangingBrowserRowAllowed() throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkChangingLineAllowed()");
        }

        FMarkingMOVBean vBean = getBean();
        if (vBean != null) {
            for (MarkingDevice vDevice : vBean.getDevices().getDevicesList()) {
                if (vDevice.getState() == DeviceState.NOT_STARTED) {
                    String vErrorMessage = I18nClientManager.translate(ProductionMo.T34901) + "\n"
                            + I18nClientManager.translate(ProductionMo.T34902);
                    getView().showError(vErrorMessage);
                    throw new UIException(UILevel.LEVEL_SILENT,
                            new BusinessException(new FatalException(vErrorMessage)));
                }
                if (vDevice.getState() == DeviceState.SIMULATED) {
                    String vErrorMessage = I18nClientManager.translate(ProductionMo.T34901) + "\n"
                            + I18nClientManager.translate(ProductionMo.T34903);
                    getView().showError(vErrorMessage);
                    throw new UIException(UILevel.LEVEL_SILENT,
                            new BusinessException(new FatalException(vErrorMessage)));
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkChangingLineAllowed()");
        }
    }

    /**
     * Check if multiple devices are simulating the same document.
     * 
     * @throws UIException if the there are multiple devices.
     */
    public void checkStartMOAllowed() throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkStartMOAllowed()");
        }

        FMarkingMOVBean vBean = getBean();
        List<Document> vListSimulatedDoc = new ArrayList<Document>();

        if (vBean != null) {

            if (!vBean.getMarkingError().checkNoError()) {
                // refresh the error message in the bean
                ((FMarkingMOTouchVView) getView()).calculateMarkingErrorMessage();
                String vErrorMessage = I18nClientManager.translate(Generic.T22494, false, ProductionMo.T34786) + "\n"
                        + vBean.getMarkingErrorMessage();
                getView().showError(vErrorMessage);
                throw new UIException(UILevel.LEVEL_SILENT, new BusinessException(new FatalException(vErrorMessage)));
            }

            for (MarkingDevice vDevice : vBean.getDevices().getDevicesList()) {
                if (vDevice.getState() == DeviceState.SIMULATED) {
                    if (vListSimulatedDoc.contains(vDevice.getSimulatedDocument())) {
                        String vErrorMessage = I18nClientManager.translate(Generic.T22494, false, ProductionMo.T34786)
                                + "\n" + I18nClientManager.translate(ProductionMo.T34904, false);
                        getView().showError(vErrorMessage);
                        throw new UIException(UILevel.LEVEL_SILENT, new BusinessException(new FatalException(
                                vErrorMessage)));
                    } else {
                        vListSimulatedDoc.add(vDevice.getSimulatedDocument());
                    }
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkStartMOAllowed()");
        }
    }

    /**
     * Gets the fmarkingMOCBS.
     * 
     * @category getter
     * @return the fmarkingMOCBS.
     */
    public FMarkingMOCBS getFmarkingMOCBS() {
        return fmarkingMOCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        super.initialize();
        // listen the MarkingHeadsCtrl to refresh the entire FView when the manager update beans
        ((FMarkingMOTouchVView) getView()).getIMarkingHeadsCtrl().addUpdateListener(this);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeViewWithBean(final FMarkingMOVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeViewWithBean(bean=" + bean + ")");
        }

        super.initializeViewWithBean(bean);
        ((FMarkingMOIView) getView()).getIMarkingHeadsCtrl().setAttributes(bean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeViewWithBean(bean=" + bean + ")");
        }
    }

    /**
     * Check is there is at least one STARTED device.
     * 
     * @return true is there is one
     */
    public boolean isAnyDeviceNotStarted() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isAnyDeviceNotStarted()");
        }

        boolean vRet = false;
        for (MarkingDevice vMarkingDevice : getBean().getDevices().getDevicesList()) {
            if (vMarkingDevice.getState() == DeviceState.NOT_STARTED) {
                vRet = true;
                break;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isAnyDeviceNotStarted()=" + vRet);
        }
        return vRet;
    }

    /**
     * Check is there is at least one SIMULATED device.
     * 
     * @return true is there is one
     */
    public boolean isAnyDeviceSimulated() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isAnyDeviceSimulated()");
        }

        boolean vRet = false;
        for (MarkingDevice vMarkingDevice : getBean().getDevices().getDevicesList()) {
            if (vMarkingDevice.getState() == DeviceState.SIMULATED) {
                vRet = true;
                break;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isAnyDeviceSimulated()=" + vRet);
        }
        return vRet;
    }

    /**
     * Check is there is at least one STARTED device.
     * 
     * @return true is there is one
     */
    public boolean isAnyDeviceStarted() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isAnyDeviceStarted()");
        }

        boolean vRet = false;
        for (MarkingDevice vMarkingDevice : getBean().getDevices().getDevicesList()) {
            if (vMarkingDevice.getState() == DeviceState.STARTED) {
                vRet = true;
                break;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isAnyDeviceStarted()=" + vRet);
        }
        return vRet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanging(final TableRowChangeEvent event) throws UIVetoException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - selectedRowChanging(event=" + event + ")");
        }

        // if the selected row changed because the FMarkingMOSBean have bean modified, change the devices
        if (event.getSource() instanceof FMarkingMOBCtrl && ((FMarkingMOBCtrl) event.getSource()).getMoLineChanged()) {
            changeLine();
        }
        super.selectedRowChanging(event);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - selectedRowChanging(event=" + event + ")");
        }
    }

    /**
     * Sets the fmarkingMOCBS.
     * 
     * @category setter
     * @param fmarkingMOCBS fmarkingMOCBS.
     */
    public void setFmarkingMOCBS(final FMarkingMOCBS fmarkingMOCBS) {
        this.fmarkingMOCBS = fmarkingMOCBS;
    }

    /**
     * Start the selected MO.
     * 
     * @throws UIException if there is an error in the simulated MO.
     */
    public void startMO() throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - startMO()");
        }

        checkStartMOAllowed();

        // all conditions ok --> start
        FMarkingMOMODescription processingDescription = getBean().getSelectedItem();
        getBean().setProcessingItem(processingDescription);
        line.setProcessingMODescription(processingDescription);

        // line.setProcessingMODescription(vProcessingDescription);
        getBean().getDevices().setState(LineState.PROCESSING);
        FMarkingMOInitFeatureReturnBean vInitFeatureBean = (FMarkingMOInitFeatureReturnBean) getSharedContext().get(
                DOMAIN_THIS, FMarkingMOUIConstants.INIT_FEATURE_BEAN);

        ((FMarkingMOIView) getView()).getIMarkingHeadsCtrl().start(vInitFeatureBean.getAutoStartDevices());

        fireButtonStatechanged();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - startMO()");
        }
    }

    /**
     * Stop all device currently in the given DeviceState (SIMULATED or STARTED).
     * 
     * @param aDeviceState DeviceState
     */
    public void stopAllDevices(final DeviceState aDeviceState) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - stopAllDevices(aDeviceState=" + aDeviceState + ")");
        }

        stopAllDevices(aDeviceState, getBean().getDevices().getDevicesList());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - stopAllDevices(aDeviceState=" + aDeviceState + ")");
        }
    }

    /**
     * Stop all device currently in the given DeviceState (SIMULATED or STARTED).
     * 
     * @param aDeviceState DeviceState
     * @param aDevicesList list of devices to stop.
     */
    public void stopAllDevices(final DeviceState aDeviceState, final List<MarkingDevice> aDevicesList) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - stopAllDevices(aDeviceState=" + aDeviceState + ")");
        }

        for (MarkingDevice vDevice : aDevicesList) {
            if (vDevice.getState() == aDeviceState) {
                ((FMarkingMOIView) getView()).getIMarkingSingleListener().stopDevice(vDevice);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - stopAllDevices(aDeviceState=" + aDeviceState + ")");
        }
    }

    /**
     * Stop the selected MO.
     */
    public void stopMO() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - stopMO()");
        }
        // it's ok ? --> refresh
        getBean().setProcessingItem(new FMarkingMOMODescription());
        line.setProcessingMODescription(new FMarkingMOMODescription());
        getBean().getDevices().setState(LineState.NOT_PROCESSING);
        ((FMarkingMOIView) getView()).getIMarkingHeadsCtrl().stop();
        fireButtonStatechanged();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - stopMO()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean pCreationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String pBeanProperty, final Object propertyValue) {
        // nothing to do
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMarkingMOVBean convert(final Object pBean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(pBean=" + pBean + ")");
        }
        FMarkingMOBBean bBean = (FMarkingMOBBean) pBean;

        FMarkingMOVBean vBean = null;
        try {
            vBean = getFmarkingMOCBS().convert(getIdCtx(), bBean, line.getDocumentTypeIds());
        } catch (BusinessException e) {
            getView().show(new UIException(e));
            vBean = createNewBean();
        }

        vBean.setDevices(getDevices());
        vBean.setProcessingItem(line.getProcessingMODescription());
        // if the current line is the running one, get values set while the MO started
        if (line.getProcessingMODescription().getMoKey().equals(vBean.getSelectedItem().getMoKey())) {
            vBean.setSelectedItem(line.getProcessingMODescription());
        }
        vBean.getDevices().setState(bBean.getMoState());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FMarkingMOVBean createNewBean() throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createNewBean()");
        }

        FMarkingMOVBean vBean = new FMarkingMOVBean();
        vBean.setDevices(getDevices());
        vBean.setProcessingItem(line.getProcessingMODescription());
        vBean.getDevices().setState(LineState.NOT_PROCESSING);
        ((StandardTouchViewer) getView()).setEnabled(true);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createNewBean()=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean vRet = super.isButtonEnabled(reference);

        FMarkingMOVBean vVBean = getBean();

        if (FMarkingMOFCtrl.BTN_CHANGE_LINE.equals(reference)) {
            // always enabled
            vRet = true;
        } else if (FMarkingMOFCtrl.BTN_CHANGE_DOCUMENTS.equals(reference)) {
            vRet &= LineState.NOT_PROCESSING.equals(vVBean.getDevices().getState());
        } else if (FMarkingMOFCtrl.BTN_START_MO.equals(reference)) {
            // MO in progress, btn label is "stop MO" and always enable
            if (vVBean.getProcessingItem() != null && !vVBean.getProcessingItem().equals(new FMarkingMOMODescription())) {
                if (!vVBean.getProcessingItem().equals(vVBean.getSelectedItem())) {
                    vRet = true;
                } else {
                    vRet = false;
                }
            } else {
                // is not processing, enable if there is no error.
                vRet &= vVBean.getMarkingError().checkNoError();
            }
        } else if (FMarkingMOFCtrl.BTN_FINISH_MO.equals(reference)) {
            if (LineState.PROCESSING.equals(vVBean.getDevices().getState()) || isAnyDeviceSimulated()) {
                vRet = false;
            } else if (vVBean.getSelectedItem() == null
                    || vVBean.getSelectedItem().getMoKey() == null
                    || vVBean.getSelectedItem().getMoKey().getOperationItemKey() == null
                    || ManagementType.ARCHIVED.equals(vVBean.getSelectedItem().getMoKey().getOperationItemKey()
                            .getManagementType())) {
                vRet = false;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + vRet);
        }
        return vRet;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isPanelActionVisible(type=" + type + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isPanelActionVisible(type=" + type + ")=" + false);
        }
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setBeanAndInitialBean(final FMarkingMOVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setBeanAndInitialBean(bean=" + bean + ")");
        }

        ViewerModel<FMarkingMOVBean> model = getModel();
        model.setInitialBean(bean);

        // Bean and initial bean are always equals !

        model.setBean(model.getInitialBean());
        SharedContext context = getSharedContext();
        // Adds this bean to the shared context.
        context.put(DOMAIN_THIS, KEY_VIEWER_BEAN, getBean());
        fireSharedContextSend(context);
        fireErrorsChanged();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setBeanAndInitialBean(bean=" + bean + ")");
        }
    }

    /**
     * Get the shared MarkingDevices object (to always work with the same object even when the BBean changes) .
     * 
     * @return the shared MarkingDevices
     */
    private MarkingDevices getDevices() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDevices()");
        }

        if (line == null) {
            changeLine();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDevices()=" + line.getDevices());
        }
        return line.getDevices();

    }

}

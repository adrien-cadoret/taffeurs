/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputGSModel.java,v $
 * Created on 3 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import fr.vif.jtech.ui.selection.GeneralSelectionModel;


/**
 * boning output : General Selection Model.
 * 
 * @author xg
 */
public class FBoningOutputGSModel extends GeneralSelectionModel {

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownVCtrl.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown;


import java.util.ArrayList;
import java.util.List;

import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.TableColumn;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.ObjectHelper;
import fr.vif.jtech.common.util.exceptions.ObjectException;
import fr.vif.jtech.ui.browser.BrowserView;
import fr.vif.jtech.ui.events.input.InputFieldEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.models.sharedcontext.SharedContext;
import fr.vif.jtech.ui.treetable.InputTreeTableColumn;
import fr.vif.jtech.ui.treetable.InputTreeTableController;
import fr.vif.jtech.ui.treetable.actions.CollapseAllAction;
import fr.vif.jtech.ui.treetable.actions.ExpandAllAction;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.kernel.ui.features.allergen.fallergenvalue.swing.actions.CollapseNodeAction;
import fr.vif.vif5_7.gen.kernel.ui.features.allergen.fallergenvalue.swing.actions.ExpandNodeAction;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.StaffByTime;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.IHchyTreeNodeElt;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.IHchyTreeNodeEltInfo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeElt;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeEltFields;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.TimeBreakDownHchyTreeNodeTypeEnum;
import fr.vif.vif5_7.production.mo.business.services.features.ftimebreakdown.FTimeBreakDownCBS;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.action.SelectAllAction;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.action.UnselectAllAction;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing.FTimeBreakDownTreeTableView;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing.FTimeBreakDownVView;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing.TimeBreakDownCheckBoxCellRenderer;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing.TimeBreakDownDateCellRenderer;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing.TimeBreakDownDurationCellRenderer;


/**
 * TimeBreakDown : Viewer Controller.
 * 
 * @author cj
 * @version $Revision: 1.6 $, $Date: 2016/10/07 16:15:06 $
 */
public class FTimeBreakDownVCtrl extends AbstractStandardViewerController<FTimeBreakDownVBean> {

    private static final Logger LOGGER = Logger.getLogger(FTimeBreakDownVCtrl.class);

    private FTimeBreakDownCBS   ftimeBreakDownCBS;

    /**
     * ctor.
     */
    public FTimeBreakDownVCtrl() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean askForUpdate() {
        // Force return true to avoid the Jtech#T19040 message.
        return true;
    }

    /**
     * Gets the ftimeBreakDownCBS.
     * 
     * @category getter
     * @return the ftimeBreakDownCBS.
     */
    public final FTimeBreakDownCBS getFtimeBreakDownCBS() {
        return ftimeBreakDownCBS;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void initialize() {
        super.initialize();
        FTimeBreakDownTreeTableModel ttModel = (FTimeBreakDownTreeTableModel) getViewerView()
                .getFtimeBreakDownTreeTableView().getModel();

        List<InputTreeTableColumn> lstCols = new ArrayList<InputTreeTableColumn>();

        lstCols.add(new InputTreeTableColumn(I18nClientManager.translate(Generic.T1863, false),
                TimeBreakDownHchyTreeNodeEltFields.RESOURCE_LABEL.getValue(), 100));
        lstCols.add(new InputTreeTableColumn(I18nClientManager.translate(ProductionMo.T36179, false),
                TimeBreakDownHchyTreeNodeEltFields.START_DATE.getValue(), 100));
        lstCols.add(new InputTreeTableColumn(I18nClientManager.translate(Generic.T5053, false),
                TimeBreakDownHchyTreeNodeEltFields.END_DATE.getValue(), 120));
        lstCols.add(new InputTreeTableColumn(I18nClientManager.translate(Generic.T30157, false),
                TimeBreakDownHchyTreeNodeEltFields.DURATION.getValue(), 80));
        // DP2192 - CHU - display number as String to avoid 0.0
        lstCols.add(new InputTreeTableColumn(I18nClientManager.translate(ProductionMo.T34746, false), "manNumber", 110));
        // TimeBreakDownHchyTreeNodeEltFields.NUMBER.getValue(), 110));
        lstCols.add(new InputTreeTableColumn(I18nClientManager.translate(ProductionMo.T5775, false),
                TimeBreakDownHchyTreeNodeEltFields.IS_SELECTED.getValue(), 0));

        ttModel.addColumnGroup(lstCols);

        // Popup menu
        ((InputTreeTableController) getViewerView().getFtimeBreakDownTreeTableView().getController())
        .addAction(new SelectAllAction(this));
        ((InputTreeTableController) getViewerView().getFtimeBreakDownTreeTableView().getController())
        .addAction(new UnselectAllAction(this));

        ((InputTreeTableController) getViewerView().getFtimeBreakDownTreeTableView().getController())
        .addAction(new ExpandNodeAction());
        ((InputTreeTableController) getViewerView().getFtimeBreakDownTreeTableView().getController())
        .addAction(new CollapseNodeAction());
        ((InputTreeTableController) getViewerView().getFtimeBreakDownTreeTableView().getController())
        .addAction(new ExpandAllAction<TimeBreakDownHchyTreeNodeEltFields>());
        ((InputTreeTableController) getViewerView().getFtimeBreakDownTreeTableView().getController())
        .addAction(new CollapseAllAction<TimeBreakDownHchyTreeNodeEltFields>());

        // use the selected row change to update the graph
        ((FTimeBreakDownTreeTableView) getViewerView().getFtimeBreakDownTreeTableView()).getJxTreeTable()
        .addTreeSelectionListener(new TreeSelectionListener() {

            @Override
            public void valueChanged(final TreeSelectionEvent e) {
                if (e.getNewLeadSelectionPath() != null) {
                    if (TimeBreakDownHchyTreeNodeTypeEnum.LIGNE.getValue().equals(
                            ((TimeBreakDownHchyTreeNodeElt) e.getNewLeadSelectionPath().getLastPathComponent())
                            .getNodeType())) {
                        fireUpdateStaffGraph((TimeBreakDownHchyTreeNodeElt) e.getNewLeadSelectionPath()
                                .getLastPathComponent());
                    } else if (TimeBreakDownHchyTreeNodeTypeEnum.PERIOD.getValue().equals(
                            ((TimeBreakDownHchyTreeNodeElt) e.getNewLeadSelectionPath().getLastPathComponent())
                            .getNodeType())) {
                        fireUpdateStaffGraph((TimeBreakDownHchyTreeNodeElt) e.getNewLeadSelectionPath()
                                .getParentPath().getLastPathComponent());
                    } else {
                        fireUpdateStaffGraph(null);
                    }
                } else {
                    fireUpdateStaffGraph(null);
                }
            }
        });
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeViewWithBean(final FTimeBreakDownVBean bean) {
        super.initializeViewWithBean(bean);

        FTimeBreakDownTreeTableView treeTableView = (FTimeBreakDownTreeTableView) ((FTimeBreakDownVView) getView())
                .getFtimeBreakDownTreeTableView();

        TableColumn startDateColumn = treeTableView.getJxTreeTable().getColumn(
                TimeBreakDownHchyTreeNodeEltFields.START_DATE.getValue());
        startDateColumn.setCellRenderer(new TimeBreakDownDateCellRenderer());

        TableColumn endDateColumn = treeTableView.getJxTreeTable().getColumn(
                TimeBreakDownHchyTreeNodeEltFields.END_DATE.getValue());
        endDateColumn.setCellRenderer(new TimeBreakDownDateCellRenderer());

        TableColumn durationColumn = treeTableView.getJxTreeTable().getColumn(
                TimeBreakDownHchyTreeNodeEltFields.DURATION.getValue());
        durationColumn.setCellRenderer(new TimeBreakDownDurationCellRenderer());

        TableColumn isSelectedColumn = treeTableView.getJxTreeTable().getColumn(
                TimeBreakDownHchyTreeNodeEltFields.IS_SELECTED.getValue());
        isSelectedColumn.setCellRenderer(new TimeBreakDownCheckBoxCellRenderer());

        fireUpdateStaffGraph(null);

    }

    /**
     * Sets the ftimeBreakDownCBS.
     * 
     * @category setter
     * @param ftimeBreakDownCBS ftimeBreakDownCBS.
     */
    public final void setFtimeBreakDownCBS(final FTimeBreakDownCBS ftimeBreakDownCBS) {
        this.ftimeBreakDownCBS = ftimeBreakDownCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean pCreationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final InputFieldEvent event) {
        Object[] selected = getViewerView().getFtimeBreakDownTreeTableView().getCurrentSelectedNodePath();
        if (selected != null) {
            TimeBreakDownHchyTreeNodeElt selectedBean = ((TimeBreakDownHchyTreeNodeElt) selected[selected.length - 1]);
            if (getViewerView().getFtimeBreakDownTreeTableView().getCurrentSelectedColumnIndex() == 6) {
                // we change the selection
                if (TimeBreakDownHchyTreeNodeTypeEnum.LABOR.getValue().equals(selectedBean.getNodeType())) {
                    for (IHchyTreeNodeElt<? extends IHchyTreeNodeEltInfo> children : selectedBean.getChildren()) {
                        ((TimeBreakDownHchyTreeNodeElt) children).setIsSelected(selectedBean.getIsSelected());
                    }

                } else if (selectedBean.getIsSelected()
                        && TimeBreakDownHchyTreeNodeTypeEnum.LIGNE.getValue().equals(selectedBean.getNodeType())) {
                    TimeBreakDownHchyTreeNodeElt parentBean = ((TimeBreakDownHchyTreeNodeElt) selected[selected.length - 2]);
                    parentBean.setIsSelected(true);
                }
            }
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FTimeBreakDownVBean convert(final Object pBean) throws UIException {
        getView().setCursor(BrowserView.WAIT_CURSOR);
        FTimeBreakDownVBean vBean = new FTimeBreakDownVBean();

        try {
            FTimeBreakDownSBean sbean = (FTimeBreakDownSBean) getSharedContext().get(Domain.DOMAIN_THIS,
                    SharedContext.KEY_CALL_PARAM_BEAN);
            if (sbean != null) {
                vBean = getFtimeBreakDownCBS().convert(getIdCtx(), sbean);
            }
        } catch (BusinessException e) {
            LOGGER.error(e);
            getView().show(new UIException(e));
        }
        getView().setCursor(BrowserView.DEFAULT_CURSOR);

        return vBean;
    }

    /**
     * Ask for updating the staff graph.
     *
     * @param lineElt the line elt
     */
    private void fireUpdateStaffGraph(final TimeBreakDownHchyTreeNodeElt lineElt) {
        if (lineElt != null && TimeBreakDownHchyTreeNodeTypeEnum.LIGNE.getValue().equals(lineElt.getNodeType())) {
            List<StaffByTime> listStaff = new ArrayList<StaffByTime>();

            // DP2192 - CHU - previous period used to detect break periods
            TimeBreakDownHchyTreeNodeElt previousPeriod = null;

            for (IHchyTreeNodeElt<? extends IHchyTreeNodeEltInfo> child : lineElt.getChildren()) {
                TimeBreakDownHchyTreeNodeElt period = (TimeBreakDownHchyTreeNodeElt) child;
                StaffByTime startbean = new StaffByTime(lineElt.getResourceId(), period.getStartDate(),
                        period.getNumber());

                StaffByTime endbean;
                if (period.getEndDate() == null) {
                    endbean = new StaffByTime(lineElt.getResourceId(), DateHelper.addTimeSeconds(DateHelper
                            .getTodayTime().getTime(), -1), period.getNumber());
                } else {
                    endbean = new StaffByTime(lineElt.getResourceId(), DateHelper.addTimeSeconds(period.getEndDate(),
                            -1), period.getNumber());
                }
                listStaff.add(startbean);
                listStaff.add(endbean);

                // DP2192 - CHU - manage break period in order to avoid diagonal lines by inserting a break period at
                // previousPeriod.getEndDate for staff number = period.getNumber
                if (previousPeriod != null && previousPeriod.getNumber() != period.getNumber()
                        && previousPeriod.getEndDate() != null) {
                    StaffByTime beginNewPeriod = new StaffByTime(lineElt.getResourceId(), previousPeriod.getEndDate(),
                            period.getNumber());
                    listStaff.add(beginNewPeriod);
                }

                try {
                    // DP2192 - CHU - initialize the previous period used to detect break periods
                    previousPeriod = ObjectHelper.copy(period);
                } catch (ObjectException e) {
                    LOGGER.error("Copy period problem", e);
                }
            }
            // add first point at 0
            if (listStaff.size() > 0) {
                listStaff.add(
                        0,
                        new StaffByTime(lineElt.getResourceId(), DateHelper.addTimeSeconds(listStaff.get(0).getTime(),
                                -1), 0));
                // DP2192 - CHU - don't add the last point at 0
                // listStaff.add(new StaffByTime(lineElt.getResourceId(), DateHelper.addTimeSeconds(
                // listStaff.get(listStaff.size() - 1).getTime(), +1), 0));
            }
            getViewerView().getStaffChart().setBeanList(listStaff);

        } else {
            getViewerView().getStaffChart().setBeanList(new ArrayList<StaffByTime>());
        }
    }

    /**
     * Gets the viewer view.
     * 
     * @return the view
     */
    private FTimeBreakDownVView getViewerView() {
        return (FTimeBreakDownVView) getView();
    }

}

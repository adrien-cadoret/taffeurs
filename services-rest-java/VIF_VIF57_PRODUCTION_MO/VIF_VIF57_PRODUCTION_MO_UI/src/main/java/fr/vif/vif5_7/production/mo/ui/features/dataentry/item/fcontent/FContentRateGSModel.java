/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FContentRateGSModel.java,v $
 * Created on 15 Jul 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent;


import fr.vif.jtech.ui.selection.GeneralSelectionModel;


/**
 * General selection model for content feature.
 * 
 * @author cj
 */
public class FContentRateGSModel extends GeneralSelectionModel {

    /**
     * Constructor.
     */
    public FContentRateGSModel() {
        super();
    }
}

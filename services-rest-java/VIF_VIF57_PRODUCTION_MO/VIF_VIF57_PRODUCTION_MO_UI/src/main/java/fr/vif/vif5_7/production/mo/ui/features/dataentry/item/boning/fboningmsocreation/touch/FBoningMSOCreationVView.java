/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMSOCreationVView.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation.touch;


import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import org.apache.log4j.Logger;

import fr.vif.jtech.ui.input.CompositeInputTextFieldController;
import fr.vif.jtech.ui.input.touch.TouchCompositeInputTextField;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.RoundBackgroundPanel;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.activities.activities.ui.composites.cstackingplan.touch.CStackingPlanView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.service.ui.composites.cquantityunit.touch.CQuantityUnitView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationVBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation.FBoningMOCreationVModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationVIView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmsocreation.FBoningMSOCreationVModel;


/**
 * MSO creation viewer.
 * 
 * @author xg
 */
public class FBoningMSOCreationVView extends StandardTouchViewer<FBoningMSOCreationVBean> implements
        FBoningMSOCreationVIView {
    /**
     * Logger.
     */
    private static final Logger          LOGGER            = Logger.getLogger(FBoningMSOCreationVView.class);

    private TouchCompositeInputTextField cMOItemCL         = null;
    private CQuantityUnitView            cQuantityUnit     = null;
    private CStackingPlanView            cStackingPlanView = null;
    private TouchInputTextField          itfMOChrono       = null;

    private TouchInputTextField          itfMOLabel        = null;

    private JPanel                       jpMain            = null;
    private RoundBackgroundPanel         rbpMO             = null;

    /**
     * Simple constructor.
     */
    public FBoningMSOCreationVView() {
        super();
        initialize();
    }

    /**
     * Gets the lOGGER.
     * 
     * @category getter
     * @return the lOGGER.
     */
    private static Logger getLOGGER() {
        return LOGGER;
    }

    /**
     * Gets the cQuantityUnit.
     * 
     * @category getter
     * @return the cQuantityUnit.
     */
    public CQuantityUnitView getCQuantityUnit() {
        if (cQuantityUnit == null) {
            cQuantityUnit = new CQuantityUnitView();
            cQuantityUnit.setBeanProperty(FBoningMSOCreationVModel.QUANTITY_UNIT);
            cQuantityUnit.setMandatory(true);
        }
        return cQuantityUnit;
    }

    /**
     * Gets the cStackingPlanView.
     * 
     * @return the cStackingPlanView.
     */
    public CStackingPlanView getcStackingPlanView() {
        if (cStackingPlanView == null) {
            cStackingPlanView = new CStackingPlanView();
            cStackingPlanView.setBeanProperty(FBoningMOCreationVModel.STACKING_PLAN_CL);
        }
        return cStackingPlanView;
    }

    /**
     * Sets the cStackingPlanView.
     * 
     * @param cStackingPlanView cStackingPlanView.
     */
    public void setcStackingPlanView(final CStackingPlanView cStackingPlanView) {
        this.cStackingPlanView = cStackingPlanView;
    }

    /**
     * Gets the itfMOItem.
     * 
     * @category getter
     * @return the itfMOItem.
     */
    private TouchCompositeInputTextField getCMOItemCL() {
        if (cMOItemCL == null) {
            cMOItemCL = new TouchCompositeInputTextField(CompositeInputTextFieldController.class);
            cMOItemCL.setTextLabel(I18nClientManager.translate(ProductionMo.T4730, false));
            cMOItemCL.setAlwaysDisabled(true);
            cMOItemCL.setCodeAlwaysDisabled(true);
            cMOItemCL.setBeanProperty(FBoningMSOCreationVModel.MO_ITEM);
        }
        return cMOItemCL;
    }

    /**
     * Gets the itfMOChrono.
     * 
     * @category getter
     * @return the itfMOChrono.
     */
    private TouchInputTextField getItfMOChrono() {
        if (itfMOChrono == null) {
            itfMOChrono = new TouchInputTextField(Integer.class, "00000000", I18nClientManager.translate(
                    ProductionMo.T22582, false));
            itfMOChrono.setAlwaysDisabled(true);
            itfMOChrono.setBeanProperty(FBoningMSOCreationVModel.MO_CHRONO);
        }
        return itfMOChrono;
    }

    /**
     * Gets the itfMOLabel.
     * 
     * @category getter
     * @return the itfMOLabel.
     */
    private TouchInputTextField getItfMOLabel() {
        if (itfMOLabel == null) {
            itfMOLabel = new TouchInputTextField(String.class, "30", I18nClientManager.translate(ProductionMo.T4999,
                    false));
            itfMOLabel.setAlwaysDisabled(true);
            itfMOLabel.setBeanProperty(FBoningMSOCreationVModel.MO_LABEL);
        }
        return itfMOLabel;
    }

    /**
     * This method initializes jpMain.
     * 
     * @return javax.swing.JPanel.
     */
    private JPanel getJpMain() {
        if (jpMain == null) {
            GridBagConstraints gridBagConstraints00 = new GridBagConstraints();
            gridBagConstraints00.gridx = 0;
            gridBagConstraints00.gridy = 0;
            gridBagConstraints00.gridwidth = 2;
            gridBagConstraints00.weightx = 1.0;
            gridBagConstraints00.weighty = 0.34;
            gridBagConstraints00.fill = GridBagConstraints.BOTH;
            gridBagConstraints00.anchor = GridBagConstraints.CENTER;
            gridBagConstraints00.insets = new Insets(10, 100, 10, 100);

            GridBagConstraints gridBagConstraints01 = new GridBagConstraints();
            gridBagConstraints01.gridx = 0;
            gridBagConstraints01.gridy = 1;
            gridBagConstraints01.weightx = 0.5;
            gridBagConstraints01.weighty = 0.33;
            gridBagConstraints01.fill = GridBagConstraints.BOTH;
            gridBagConstraints01.anchor = GridBagConstraints.NORTH;
            gridBagConstraints01.insets = new Insets(10, 20, 0, 0);

            GridBagConstraints gridBagConstraints02 = new GridBagConstraints();
            gridBagConstraints02.gridx = 0;
            gridBagConstraints02.gridy = 2;
            gridBagConstraints02.weightx = 0.5;
            gridBagConstraints02.weighty = 0.33;
            gridBagConstraints02.fill = GridBagConstraints.BOTH;
            gridBagConstraints02.anchor = GridBagConstraints.NORTH;
            gridBagConstraints02.insets = new Insets(10, 20, 0, 0);

            GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
            gridBagConstraints11.gridx = 1;
            gridBagConstraints11.gridy = 1;
            gridBagConstraints11.weightx = 0.5;
            gridBagConstraints11.weighty = 0.33;
            gridBagConstraints11.fill = GridBagConstraints.BOTH;

            jpMain = new JPanel();
            jpMain.setOpaque(false);
            jpMain.setLayout(new GridBagLayout());

            JPanel jpEmpty = new JPanel();
            jpEmpty.setOpaque(false);

            jpMain.add(getRbpMO(), gridBagConstraints00);
            jpMain.add(getCQuantityUnit(), gridBagConstraints01);
            jpMain.add(getcStackingPlanView(), gridBagConstraints02);
            jpMain.add(jpEmpty, gridBagConstraints11);
        }
        return jpMain;
    }

    /**
     * Gets the rbpMO.
     * 
     * @category getter
     * @return the rbpMO.
     */
    private RoundBackgroundPanel getRbpMO() {
        if (rbpMO == null) {
            GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
            gridBagConstraints2.gridx = 0;
            gridBagConstraints2.gridy = 2;
            gridBagConstraints2.weightx = 1.0;
            gridBagConstraints2.weighty = 1.0;
            gridBagConstraints2.fill = GridBagConstraints.BOTH;
            gridBagConstraints2.insets = new Insets(0, 10, 0, 400);
            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.gridx = 0;
            gridBagConstraints1.gridy = 1;
            gridBagConstraints1.weightx = 1.0;
            gridBagConstraints1.weighty = 1.0;
            gridBagConstraints1.fill = GridBagConstraints.BOTH;
            gridBagConstraints1.insets = new Insets(0, 10, 0, 200);
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.gridy = 0;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.insets = new Insets(0, 10, 0, 500);
            rbpMO = new RoundBackgroundPanel();
            rbpMO.setOpaque(false);
            rbpMO.setStartColor(new Color(StandardColor.TOUCHSCREEN_BG_EVEN_CELL.getRed(),
                    StandardColor.TOUCHSCREEN_BG_EVEN_CELL.getGreen(), StandardColor.TOUCHSCREEN_BG_EVEN_CELL.getBlue()));
            rbpMO.setEndColor(new Color(StandardColor.TOUCHSCREEN_BG_ODD_CELL.getRed(),
                    StandardColor.TOUCHSCREEN_BG_ODD_CELL.getGreen(), StandardColor.TOUCHSCREEN_BG_ODD_CELL.getBlue()));
            rbpMO.setLayout(new GridBagLayout());
            rbpMO.add(getItfMOChrono(), gridBagConstraints);
            rbpMO.add(getItfMOLabel(), gridBagConstraints1);
            rbpMO.add(getCMOItemCL(), gridBagConstraints2);
        }
        return rbpMO;
    }

    /**
     * This method initializes this.
     */
    private void initialize() {
        GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.weightx = 1.0;
        gridBagConstraints.weighty = 1.0;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        this.setLayout(new GridBagLayout());
        this.add(getJpMain(), gridBagConstraints);
    }

}

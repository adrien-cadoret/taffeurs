/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionConstant.java,v $
 * Created on 18 déc. 08 by gp
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction;


/**
 * Constant use the input/output feature.
 * 
 * @author gp
 */
public final class FProductionConstant {

    /**
     * id of the feature.
     */
    public static final String FEATURE_ID          = "featureId";

    /**
     * feature title.
     */
    public static final String FEATURE_TITLE       = "featureTitle";

    /**
     * Key to store the current FMOListVBean.
     */
    public static final String LOGICAL_WORKSTATION = "logicalWorkstation";

    /**
     * mo Initial Selection.
     */
    public static final String MO_INITIAL_SELECTON = "moInitialSalection";

    /**
     * Key to store workstation to work for.
     */
    public static final String MO_LIST_BBEAN       = "moListBBean";

    /**
     * Key to store workstation to work for.
     */
    public static final String MO_LIST_VBEAN       = "moListVBean";

    /**
     * MOBean created.
     */
    public static final String STARTED_MO_BEAN     = "startedMOBean";

    /**
     * Input or output feature.
     */
    public static final String FEATURE_WAY         = "featureWay";

    /**
     * Criteria's Icon.
     */
    public static final int    CRITERIA_ICON       = 4;

    /**
     * Default constructor.
     */
    private FProductionConstant() {
    }

}

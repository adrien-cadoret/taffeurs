/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListVView.java,v $
 * Created on 10 déc. 08 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.touch;


import java.awt.Dimension;

import fr.vif.jtech.ui.input.touch.TouchCompositeInputTextField;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.gen.item.ui.composites.citem.CItemCtrl;
import fr.vif.vif5_7.gen.item.ui.composites.citem.touch.CItemTouchView;
import fr.vif.vif5_7.gen.location.ui.composites.clocation.CLocationCtrl;
import fr.vif.vif5_7.gen.location.ui.composites.clocation.touch.CLocationTouchView;
import fr.vif.vif5_7.gen.location.ui.composites.cwarehouse.CWareHouseCtrl;
import fr.vif.vif5_7.gen.location.ui.composites.cwarehouse.touch.CWareHouseTouchView;
import fr.vif.vif5_7.gen.packaging.ui.composites.cpackaging.touch.CPackagingTouchView;
import fr.vif.vif5_7.gen.service.ui.composites.cquantityunit.touch.CQuantityUnitView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListVBean;
import fr.vif.vif5_7.stock.kernel.business.beans.features.fmvtlistsimple.FMvtListSimpleVBeanEnum;
import fr.vif.vif5_7.stock.kernel.ui.features.common.transfer.touch.FMvtTransferTouchFactory;
import fr.vif.vif5_7.stock.kernel.ui.features.common.transfer.touch.TouchFieldProperties;


/**
 * wide class.
 * 
 * @author gv
 */
public class FDataEntryListVView extends StandardTouchViewer<FDataEntryListVBean> {
    private static final int    COL_1        = 5;
    private static final int    COL_2        = 60;
    private static final int    COL_3        = 255;
    private static final int    COL_4        = 450;
    private static final int    COL_5        = 645;
    private static final int    COL_6        = 790;
    private static final int    FIELD_HEIGHT = 70;
    private static final int    FIELD_WIDTH  = 190;
    private static final int    LINE_0       = 5;
    private static final int    LINE_1       = 20;
    private static final int    LINE_2       = 60;
    private static final int    LINE_3       = 100;
    private static final int    LINE_4       = 135;
    private int                 gap          = 55;

    private CLocationTouchView  inputLocationTouchView;
    private CWareHouseTouchView inputWareHouseTouchView;
    private TouchInputTextField ttfInputBatch;
    private TouchInputTextField ttfInputContainer;
    private CItemTouchView      ttfInputItem;
    private CQuantityUnitView   ttfInputItemFirstQty;
    private CQuantityUnitView   ttfInputItemSecondQty;
    private TouchInputTextField ttfInputUpperContainer;
    private TouchInputTextField ttfInputTattooed;
    private TouchInputTextField ttfInputUpperTattooed;
    private CPackagingTouchView ttfInputPackagingView;
    private CPackagingTouchView ttfInputUpperPackagingView;
    private TouchInputTextField ttfInputProducedEntity;

    /**
     * Default constructor.
     * 
     */
    public FDataEntryListVView() {
        super();
        initialize();
    }

    /**
     * Re size.
     *
     * @param field the field
     * @param width the width
     * @param height the height
     */
    private static void reSize(final CQuantityUnitView field, final int width, final int height) {
        int sizeView = 0;
        int sizeViewText = 0;
        if (height != 0) {
            sizeView = height;
            sizeViewText = height - 20;

            field.setMaximumSize(new Dimension(width, sizeView));
            field.getItfQuantity().setMaximumSize(new Dimension(width, sizeViewText));
            field.getItfQuantity().getJtfValue().setMaximumSize(new Dimension(width, sizeViewText));
            field.getItfUnit().setMaximumSize(new Dimension(width, sizeViewText));
            field.getItfUnit().getJtfValue().setMaximumSize(new Dimension(width, sizeViewText));
        }

    }

    /**
     * Re size.
     * 
     * @param field the field
     * @param width the width
     * @param height the height
     */
    private static void reSize(final TouchCompositeInputTextField field, final int width, final int height) {
        int sizeLabel = 0;
        int sizeView = 0;
        int sizeViewText = 0;
        if (height != 0) {
            sizeLabel = (int) (height * 0.3d);
            sizeView = (int) (height * 0.7d);
            sizeViewText = (int) (height * 0.4d);
            field.getLabelView().setMinimumSize(new Dimension(width, sizeLabel));
            ((TouchInputTextField) field.getCodeView()).setMinimumSize(new Dimension(width, sizeView));
            ((TouchInputTextField) field.getCodeView()).getJtfValue()
                    .setMinimumSize(new Dimension(width, sizeViewText));

            field.getLabelView().setMaximumSize(new Dimension(width, sizeLabel));
            ((TouchInputTextField) field.getCodeView()).setMaximumSize(new Dimension(width, sizeView));
            ((TouchInputTextField) field.getCodeView()).getJtfValue()
                    .setMaximumSize(new Dimension(width, sizeViewText));
        }
    }

    /**
     * Re size.
     *
     * @param field the field
     * @param width the width
     * @param height the height
     */
    private static void reSize(final TouchInputTextField field, final int width, final int height) {
        int sizeLabel = 0;
        int sizeView = 0;
        int sizeViewText = 0;

        if (height != 0) {
            sizeLabel = (int) (height * 0.3d);
            sizeView = (int) (height * 0.7d);
            sizeViewText = (int) (height * 0.4d);
            field.setMinimumSize(new Dimension(width, sizeLabel));
            field.setSize(new Dimension(width, sizeView));
            field.getJtfValue().setMinimumSize(new Dimension(width, sizeViewText));
            field.getJtfValue().setSize(new Dimension(width, sizeViewText));
            field.setMaximumSize(new Dimension(width, sizeLabel));
            field.getJtfValue().setMaximumSize(new Dimension(width, sizeViewText));
        }

    }

    /**
     * Should we show the tattooed informations ?.
     *
     * @param producedEntity yes if it is a produced entity
     * @param tattooedContainer yes if the container is tattooed
     * @param tattooedUpperContainer yes if the upper container is tattooed
     */
    public void manageContainerView(final boolean producedEntity, final boolean tattooedContainer,
            final boolean tattooedUpperContainer) {

        if (producedEntity) {
            getTtfInputContainer().setVisible(false);
            getTtfInputUpperContainer().setVisible(false);
            getTtfInputItemFirstQty().setVisible(true);
            getTtfInputItemSecondQty().setVisible(true);
            getTtfInputProducedEntity().setVisible(true);
            getTtfInputTattooed().setVisible(false);
            getTtfInputPackagingView().setVisible(false);
            getTtfInputUpperPackagingView().setVisible(false);
            getTtfInputUpperTattooed().setVisible(false);
        } else if (tattooedContainer) {
            getTtfInputContainer().setVisible(true);
            getTtfInputUpperContainer().setVisible(true);
            getTtfInputItemFirstQty().setVisible(false);
            getTtfInputItemSecondQty().setVisible(false);
            getTtfInputProducedEntity().setVisible(false);
            getTtfInputTattooed().setVisible(true);
            getTtfInputPackagingView().setVisible(true);
            getTtfInputUpperPackagingView().setVisible(true);
            getTtfInputUpperTattooed().setVisible(false);
        } else if (tattooedUpperContainer) {
            getTtfInputContainer().setVisible(true);
            getTtfInputUpperContainer().setVisible(true);
            getTtfInputItemFirstQty().setVisible(false);
            getTtfInputItemSecondQty().setVisible(false);
            getTtfInputProducedEntity().setVisible(false);
            getTtfInputTattooed().setVisible(false);
            getTtfInputPackagingView().setVisible(true);
            getTtfInputUpperPackagingView().setVisible(true);
            getTtfInputUpperTattooed().setVisible(true);
        } else {
            getTtfInputContainer().setVisible(true);
            getTtfInputUpperContainer().setVisible(true);
            getTtfInputItemFirstQty().setVisible(true);
            getTtfInputItemSecondQty().setVisible(true);
            getTtfInputProducedEntity().setVisible(false);
            getTtfInputTattooed().setVisible(false);
            getTtfInputPackagingView().setVisible(false);
            getTtfInputUpperPackagingView().setVisible(false);
            getTtfInputUpperTattooed().setVisible(false);
        }
    }

    /**
     * Get the CLocationTouch view.
     * 
     * @return the CLocationTouch view
     */
    private CLocationTouchView getInputLocationTouchView() {
        if (inputLocationTouchView == null) {
            inputLocationTouchView = new CLocationTouchView(CLocationCtrl.class,
                    I18nClientManager.translate(GenKernel.T4329));
            inputLocationTouchView.setUpdateable(false);
            inputLocationTouchView.setBounds(COL_3, LINE_0, FIELD_WIDTH, FIELD_HEIGHT);
            inputLocationTouchView.setBeanProperty("locationCL");
            inputLocationTouchView.setMinimumSize(new Dimension(150, 64));
            inputLocationTouchView.setTextLabel(I18nClientManager.translate(GenKernel.T4329));
            inputLocationTouchView.setAlwaysDisabled(true);
            inputLocationTouchView.setEnabledForCreate(false);
            inputLocationTouchView.getCodeView().setAlwaysDisabled(true);
            reSize(inputLocationTouchView, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return inputLocationTouchView;
    }

    /**
     * get the CWarehouseTouch view.
     * 
     * @return the CWarehouseTouch view.
     */
    private CWareHouseTouchView getInputWareHouseTouchView() {
        if (inputWareHouseTouchView == null) {

            inputWareHouseTouchView = new CWareHouseTouchView(CWareHouseCtrl.class,
                    I18nClientManager.translate(GenKernel.T212));
            inputWareHouseTouchView.setUpdateable(false);
            inputWareHouseTouchView.setBounds(COL_2, LINE_0, FIELD_WIDTH, FIELD_HEIGHT);
            inputWareHouseTouchView.setBeanProperty("depositCL");
            inputWareHouseTouchView.setMinimumSize(new Dimension(150, 64));
            inputWareHouseTouchView.setTextLabel(I18nClientManager.translate(GenKernel.T212));
            inputWareHouseTouchView.setAlwaysDisabled(true);
            inputWareHouseTouchView.setEnabledForCreate(false);
            inputWareHouseTouchView.getCodeView().setAlwaysDisabled(true);
            reSize(inputWareHouseTouchView, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return inputWareHouseTouchView;
    }

    /**
     * Get the touch input batch.
     * 
     * @return the touch input batch
     */
    private TouchInputTextField getTtfInputBatch() {
        if (ttfInputBatch == null) {
            ttfInputBatch = new TouchInputTextField(String.class, "15", I18nClientManager.translate(StockKernel.T29224));
            ttfInputBatch.setBounds(COL_3, LINE_3, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputBatch.setBeanProperty("bBean.stockItem.batch");
            ttfInputBatch.setMinimumSize(new Dimension(150, 64));
            ttfInputBatch.setPreferredSize(new Dimension(150, 64));
            ttfInputBatch.setAlwaysDisabled(true);
            ttfInputBatch.setEnabledForCreate(false);
            reSize(ttfInputBatch, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return ttfInputBatch;
    }

    /**
     * Get the touch input container.
     * 
     * @return the touch input container
     */
    private TouchInputTextField getTtfInputContainer() {
        if (ttfInputContainer == null) {

            ttfInputContainer = new TouchInputTextField(String.class, "10",
                    I18nClientManager.translate(StockKernel.T29089));
            ttfInputContainer.setBounds(COL_4, LINE_0, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputContainer.setBeanProperty("bBean.entityLocationKey.containerNumber");
            ttfInputContainer.setMinimumSize(new Dimension(150, 64));
            ttfInputContainer.setPreferredSize(new Dimension(150, 64));
            ttfInputContainer.setAlwaysDisabled(true);
            ttfInputContainer.setEnabledForCreate(false);
            reSize(ttfInputContainer, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return ttfInputContainer;
    }

    /**
     * Get the touch input item.
     * 
     * @return the touch input item
     */
    private CItemTouchView getTtfInputItem() {
        if (ttfInputItem == null) {
            ttfInputItem = new CItemTouchView(CItemCtrl.class, I18nClientManager.translate(GenKernel.T426));
            ttfInputItem.setBounds(COL_2, LINE_3, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputItem.setBeanProperty("itemCL");
            ttfInputItem.setPreferredSize(new Dimension(150, 80));
            ttfInputItem.setMinimumSize(new Dimension(150, 80));
            ttfInputItem.getCodeView().setAutoUppercase(true);
            ttfInputItem.setCodeUseFieldHelp(false);
            ttfInputItem.setAlwaysDisabled(true);
            ttfInputItem.setEnabledForCreate(false);
            ttfInputItem.getCodeView().setAlwaysDisabled(true);
            reSize(ttfInputItem, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return ttfInputItem;
    }

    /**
     * Get the touch first Qty.
     *
     * @return the touch first qty
     */
    private CQuantityUnitView getTtfInputItemFirstQty() {
        if (ttfInputItemFirstQty == null) {
            ttfInputItemFirstQty = new CQuantityUnitView();
            ttfInputItemFirstQty.setBounds(COL_4, LINE_3, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputItemFirstQty.setBeanProperty("bBean.keyboardQties.firstQty");
            reSize(ttfInputItemFirstQty, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputItemFirstQty.setVisible(false);
            ttfInputItemFirstQty.setAlwaysDisabled(true);
            ttfInputItemFirstQty.setEnabledForCreate(false);
            ttfInputItemFirstQty.getItfQuantity().setAlwaysDisabled(true);
            ttfInputItemFirstQty.getItfQuantity().setBorder(null);
        }
        return ttfInputItemFirstQty;
    }

    /**
     * Get the touch second qty.
     *
     * @return the touch second qty
     */
    private CQuantityUnitView getTtfInputItemSecondQty() {
        if (ttfInputItemSecondQty == null) {
            ttfInputItemSecondQty = new CQuantityUnitView();
            ttfInputItemSecondQty.setBounds(COL_5, LINE_3, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputItemSecondQty.setBeanProperty("bBean.keyboardQties.secondQty");
            reSize(ttfInputItemSecondQty, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputItemSecondQty.setAlwaysDisabled(true);
            ttfInputItemSecondQty.setEnabledForCreate(false);

            ttfInputItemSecondQty.getItfQuantity().setAlwaysDisabled(true);
            ttfInputItemSecondQty.getItfQuantity().setBorder(null);
            ttfInputItemSecondQty.setVisible(false);
        }
        return ttfInputItemSecondQty;
    }

    /**
     * Gets the ttfInputPackagingView.
     *
     * @return the ttfInputPackagingView.
     */
    private CPackagingTouchView getTtfInputPackagingView() {
        if (ttfInputPackagingView == null) {
            ttfInputPackagingView = new CPackagingTouchView();
            ttfInputPackagingView.setResettable(false);
            ttfInputPackagingView.setCodeUseFieldHelp(false);
            ttfInputPackagingView.setBeanProperty("bBean.packagingCL");
            ttfInputPackagingView.setTextLabel(I18nClientManager.translate(StockKernel.T1588));
            ttfInputPackagingView.setBounds(COL_4, LINE_2, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputPackagingView.setAlwaysDisabled(true);
            ttfInputPackagingView.setEnabledForCreate(false);
            ttfInputPackagingView.getCodeView().setAlwaysDisabled(true);
            reSize(ttfInputPackagingView, FIELD_WIDTH, FIELD_HEIGHT);

        }
        return ttfInputPackagingView;
    }

    /**
     * Get the touch input produced entity.
     * 
     * @return the touch input container
     */
    private TouchInputTextField getTtfInputProducedEntity() {
        if (ttfInputProducedEntity == null) {

            ttfInputProducedEntity = new TouchInputTextField(String.class, "10",
                    I18nClientManager.translate(ProductionMo.T17191));
            ttfInputProducedEntity.setBounds(COL_4, LINE_0, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputProducedEntity.setBeanProperty("bBean.producedEntity");
            ttfInputProducedEntity.setMinimumSize(new Dimension(150, 64));
            ttfInputProducedEntity.setPreferredSize(new Dimension(150, 64));
            ttfInputProducedEntity.setAlwaysDisabled(true);
            ttfInputProducedEntity.setEnabledForCreate(false);
            reSize(ttfInputProducedEntity, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return ttfInputProducedEntity;
    }

    /**
     * Gets the ttfInputTattooed.
     *
     * @return the ttfInputTattooed.
     */
    private TouchInputTextField getTtfInputTattooed() {
        if (ttfInputTattooed == null) {
            ttfInputTattooed = new TouchInputTextField(String.class, "10",
                    I18nClientManager.translate(StockKernel.T4273));
            ttfInputTattooed.setBounds(COL_4, LINE_4, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputTattooed.setBeanProperty("bBean.tattooedNumber");
            ttfInputTattooed.setMinimumSize(new Dimension(150, 64));
            ttfInputTattooed.setPreferredSize(new Dimension(150, 64));
            ttfInputTattooed.setAlwaysDisabled(true);
            ttfInputTattooed.setEnabledForCreate(false);
            reSize(ttfInputTattooed, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return ttfInputTattooed;
    }

    /**
     * Get the touch input upper container.
     * 
     * @return the touch input upper container
     */
    private TouchInputTextField getTtfInputUpperContainer() {
        if (ttfInputUpperContainer == null) {
            ttfInputUpperContainer = FMvtTransferTouchFactory.getTtfPallet(new TouchFieldProperties(
                    FMvtListSimpleVBeanEnum.INPUT_UPPER_CONTAINER_NUMBER.getValue(), COL_5, LINE_0, FIELD_WIDTH,
                    FIELD_HEIGHT, true));

            ttfInputUpperContainer = new TouchInputTextField(String.class, "10",
                    I18nClientManager.translate(StockKernel.T30034));
            ttfInputUpperContainer.setBounds(COL_5, LINE_0, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputUpperContainer.setBeanProperty("bBean.entityLocationKey.upperContainerNumber");
            ttfInputUpperContainer.setMinimumSize(new Dimension(150, 64));
            ttfInputUpperContainer.setPreferredSize(new Dimension(150, 64));
            ttfInputUpperContainer.setAlwaysDisabled(true);
            ttfInputUpperContainer.setEnabledForCreate(false);
            reSize(ttfInputContainer, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return ttfInputUpperContainer;
    }

    /**
     * Gets the ttfInputUpperPackagingView.
     *
     * @return the ttfInputUpperPackagingView.
     */
    private CPackagingTouchView getTtfInputUpperPackagingView() {
        if (ttfInputUpperPackagingView == null) {

            ttfInputUpperPackagingView = new CPackagingTouchView();
            ttfInputUpperPackagingView.setBeanProperty("bBean.upperPackagingCL");
            ttfInputUpperPackagingView.setTextLabel(I18nClientManager.translate(StockKernel.T1588));
            ttfInputUpperPackagingView.setBounds(COL_5, LINE_2, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputUpperPackagingView.setAlwaysDisabled(true);
            ttfInputUpperPackagingView.setEnabledForCreate(false);
            ttfInputUpperPackagingView.getCodeView().setAlwaysDisabled(true);
            reSize(ttfInputUpperPackagingView, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return ttfInputUpperPackagingView;
    }

    /**
     * Gets the ttfInputUpperTattooed.
     *
     * @return the ttfInputUpperTattooed.
     */
    private TouchInputTextField getTtfInputUpperTattooed() {
        if (ttfInputUpperTattooed == null) {

            ttfInputUpperTattooed = new TouchInputTextField(String.class, "10",
                    I18nClientManager.translate(StockKernel.T39121));
            ttfInputUpperTattooed.setBounds(COL_5, LINE_4, FIELD_WIDTH, FIELD_HEIGHT);
            ttfInputUpperTattooed.setBeanProperty("bBean.tattooedNumber");
            ttfInputUpperTattooed.setMinimumSize(new Dimension(150, 64));
            ttfInputUpperTattooed.setPreferredSize(new Dimension(150, 64));
            ttfInputUpperTattooed.setAlwaysDisabled(true);
            reSize(ttfInputUpperTattooed, FIELD_WIDTH, FIELD_HEIGHT);
        }
        return ttfInputUpperTattooed;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        this.setEnabled(false);
        this.setLayout(null);
        setPreferredSize(new Dimension(865, 105));

        add(getInputWareHouseTouchView());
        add(getInputLocationTouchView());
        add(getTtfInputProducedEntity());
        add(getTtfInputContainer());
        add(getTtfInputPackagingView());
        add(getTtfInputTattooed());
        add(getTtfInputUpperContainer());
        add(getTtfInputUpperPackagingView());
        add(getTtfInputUpperTattooed());
        add(getTtfInputItem());
        add(getTtfInputBatch());
        add(getTtfInputItemFirstQty());
        add(getTtfInputItemSecondQty());

    }

}

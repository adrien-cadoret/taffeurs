/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOBView.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.swing;


import fr.vif.jtech.ui.browser.swing.SwingBrowser;
import fr.vif.jtech.ui.dialogs.selection.SelectionModel;
import fr.vif.jtech.ui.models.mvc.SelectionMVCTriad;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOBBean;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.FSupervisorMOSCtrl;


/**
 * FSupervisorMO : Browser View.
 * 
 * @author jd
 */
public class FSupervisorMOBView extends SwingBrowser<FSupervisorMOBBean> {

    /**
     * Default constructor.
     * 
     */
    public FSupervisorMOBView() {
        super();
        setSelectionMvcTriad(new SelectionMVCTriad(SelectionModel.class, FSupervisorMOSView.class,
                FSupervisorMOSCtrl.class));
    }

}

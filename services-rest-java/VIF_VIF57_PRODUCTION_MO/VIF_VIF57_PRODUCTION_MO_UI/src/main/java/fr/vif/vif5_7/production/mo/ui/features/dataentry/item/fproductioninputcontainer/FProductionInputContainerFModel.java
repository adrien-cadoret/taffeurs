/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionInputContainerFModel.java,v $
 * Created on 17 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioninputcontainer;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionBModel;


/**
 * InputOperationItem : Feature Model.
 * 
 * @author vr
 */
public class FProductionInputContainerFModel extends StandardFeatureModel {

    public static final String FEATURES          = "features";
    public static final String SHOW_ALL          = "showall";
    public static final String FINISH_RESUME     = "finishresume";
    public static final String MOVEMENT_LIST     = "movementlist";
    public static final String PRODUCTION_OUTPUT = "productionoutput";

    public static final String WEIGHT            = "weight";
    public static final String FLASH             = "flash";
    public static final String CANCEL_LAST       = "cancellast";
    public static final String SAVE              = "SAVE";

    /**
     * Default constructor.
     * 
     */
    public FProductionInputContainerFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FProductionBModel.class, null, FProductionBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FProductionInputContainerVModel.class, null,
                FProductionInputContainerVCtrl.class));

    }

}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOListBIView.java,v $
 * Created on 7 janv. 09 by gv
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist;


import javax.swing.JTable;


/**
 * interface mo list.
 *
 * @author gv
 */
public interface FMOListBIView {

    /**
     * Gets the Jtable !
     * 
     * @return the jtable
     */
    public JTable getJTableForResize();

    /**
     * init to call after controller init !
     */
    public void initialize();
}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: AbstractEndDayIndicatorTableModel.java,v $
 * Created on 21 avr. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday;


import java.util.List;

import javax.swing.table.AbstractTableModel;

import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOBean;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.swing.EndDayIndicatorMOTableModelEvent;


/**
 * Abstract model class for endDay Indicator.
 *
 * @author ac
 */
public abstract class AbstractEndDayIndicatorTableModel extends AbstractTableModel {

    /** End day indicator list. */
    private List<? extends EndDayMOBean> endDayMOList;

    private boolean                      hasListener = false;

    /**
     * Default Ctor.
     */
    public AbstractEndDayIndicatorTableModel() {
        super();
    }

    /**
     * Gets the endDayMOList.
     *
     * @return the endDayMOList.
     */
    public List<? extends EndDayMOBean> getEndDayMOList() {
        return endDayMOList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getRowCount() {
        return (endDayMOList != null ? endDayMOList.size() : 0);
    }

    /**
     * Gets the hasListener.
     *
     * @return the hasListener.
     */
    public boolean hasListener() {
        return hasListener;
    }

    /**
     * Sets the endDayMOList.
     *
     * @param endDayMOList endDayMOList.
     */
    public void setEndDayMOList(final List<? extends EndDayMOBean> endDayMOList) {
        this.endDayMOList = endDayMOList;
        EndDayIndicatorMOTableModelEvent event = new EndDayIndicatorMOTableModelEvent(this);
        fireTableChanged(event);
    }

    /**
     * Sets the hasListener.
     *
     * @param hasListener hasListener.
     */
    public void setHasListener(final boolean hasListener) {
        this.hasListener = hasListener;
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeVCtrl.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeVBean;
import fr.vif.vif5_7.production.mo.business.services.features.flaborstaff.FLaborStaffCBS;
import fr.vif.vif5_7.production.mo.business.services.features.flabortime.FLaborTimeCBS;


/**
 * Labor time viewer controller.
 * 
 * @author nle
 */
public class FLaborTimeVCtrl extends AbstractStandardViewerController<FLaborTimeVBean> {

    public static final String DELETELAST     = "deleteLastBtn";

    public static final String VALID_STAFF    = "validStaffBtn";
    public static final String ADD            = "addBtn";
    public static final String MODIF          = "modifBtn";
    private FLaborTimeCBS      flabortimeCBS  = null;

    private FLaborStaffCBS     flaborstaffCBS = null;

    /**
     * Performs the update of the bean.
     * 
     * @param vBean the labor time vBean.
     */
    public void beanUpdated(final FLaborTimeVBean vBean) {
        // performs the update of the Bean
        performSave();
    }

    /**
     * Gets the flaborstaffCBS.
     * 
     * @return the flaborstaffCBS.
     */
    public FLaborStaffCBS getFlaborstaffCBS() {
        return flaborstaffCBS;
    }

    /**
     * Gets the flabortimeCBS.
     * 
     * @return the flabortimeCBS.
     */
    public FLaborTimeCBS getFlabortimeCBS() {
        return flabortimeCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        super.selectedRowChanged(event);
    }

    /**
     * Sets the flaborstaffCBS.
     * 
     * @param flaborstaffCBS flaborstaffCBS.
     */
    public void setFlaborstaffCBS(final FLaborStaffCBS flaborstaffCBS) {
        this.flaborstaffCBS = flaborstaffCBS;
    }

    /**
     * Sets the flabortimeCBS.
     * 
     * @param flabortimeCBS flabortimeCBS.
     */
    public void setFlabortimeCBS(final FLaborTimeCBS flabortimeCBS) {
        this.flabortimeCBS = flabortimeCBS;
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FLaborTimeVBean convert(final Object bean) throws UIException {

        FLaborTimeBBean bBean = (FLaborTimeBBean) bean;
        FLaborTimeVBean vBean = new FLaborTimeVBean();

        try {
            vBean = getFlabortimeCBS().getVBean(getIdCtx(), bBean);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        Boolean ret = true;

        // Yes we can
        // // can't ADD if the end date-hour is empty
        // if (ADD.equals(reference)) {
        // if (getBean() == null || getBean().getLaborTimeBBean() == null
        // || getBean().getLaborTimeBBean().getEndDateHour() == null) {
        // ret = false;
        // }
        // }

        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        // hide the button bar at the bottom of the screen
        return false;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FLaborTimeVBean updateBean(final FLaborTimeVBean vbean) throws UIException, UIErrorsException {
        FLaborTimeVBean vbeanUpdated = new FLaborTimeVBean();
        try {
            // updates the LaborStaff
            vbeanUpdated = getFlaborstaffCBS().updateLaborStaffBean(getIdCtx(), vbean);
        } catch (BusinessException e) {
            throw new UIException(e);
        } catch (BusinessErrorsException e) {
            throw new UIErrorsException(e);
        }

        return vbeanUpdated;
    }

}

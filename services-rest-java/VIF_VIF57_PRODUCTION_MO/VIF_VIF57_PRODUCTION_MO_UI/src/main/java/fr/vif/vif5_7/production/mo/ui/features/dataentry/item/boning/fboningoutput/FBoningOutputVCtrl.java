/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputVCtrl.java,v $
 * Created on 25 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput;


import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.SwingUtilities;

import org.apache.log4j.Logger;

import fr.vif.communication.common.bean.scale.FCommScaleResult;
import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.events.feature.SelfFeatureClosingRequiredEvent;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.events.window.closing.ClosingEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.exceptions.UIVetoException;
import fr.vif.jtech.ui.feature.FeatureView;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.constant.EntityConstants;
import fr.vif.vif57.stock.entity.business.services.constant.EntityEnum.EntityUnitType;
import fr.vif.vif57.stock.entity.business.services.constant.EntityTools;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityUnitInfos;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.Quantities;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.common.util.i18n.stock.kernel.StockKernel;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationBBean;
import fr.vif.vif5_7.gen.location.business.beans.features.flocation.FLocationSBean;
import fr.vif.vif5_7.gen.packaging.business.beans.common.tattooed.TattooedKey;
import fr.vif.vif5_7.gen.packaging.business.beans.dialog.DPackagingBean;
import fr.vif.vif5_7.gen.packaging.business.beans.features.fpackaging.FPackagingSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.dialog.printdocument.DPrintDocumentBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.FBoningTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutputcontainer.FBoningOutputVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutputcontainer.FBoningOutputVBeanEnum;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutputcontainer.FBoningOutputWorkBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.boning.fboningoutput.FBoningOutputCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ButtonReference;
import fr.vif.vif5_7.production.mo.ui.dialog.boningpackaging.DBoningPackagingCtrl;
import fr.vif.vif5_7.production.mo.ui.dialog.printdocument.DPrintDocumentCtrl;
import fr.vif.vif5_7.production.mo.ui.dialog.validation.ValidationNotificationDView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.AbstractFProductionFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.AbstractFProductionVCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput.touch.FBoningOutputVView;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityEnum;


/**
 * ProductionOutputContainer : Viewer Controller.
 * 
 * @author vr
 */
public class FBoningOutputVCtrl extends AbstractFProductionVCtrl<FBoningOutputVBean> implements DialogChangeListener {

    private static final Logger                  LOGGER         = Logger.getLogger(FBoningOutputVCtrl.class);

    private FBoningOutputBCtrl                   browserCtrl    = null;
    private FBoningOutputFCtrl                   featureCtrl    = null;

    private FeatureView                          featureView;

    private FBoningOutputCBS                     fboningOutputContainerCBS;

    private Map<MovementAct, FBoningOutputVBean> map            = new HashMap<MovementAct, FBoningOutputVBean>();

    private MovementAct                          previous       = null;

    private FBoningOutputWorkBean                workBean       = new FBoningOutputWorkBean();

    private boolean                              flagWeightMode = false;

    /**
     * ask number of document to print by document.
     * 
     * @param lst list of documents
     */
    public void askNumDocument(final List<PrintDocument> lst) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - askNumDocument()");
        }

        for (PrintDocument document : lst) {
            DPrintDocumentCtrl ctrl = new DPrintDocumentCtrl();
            DPrintDocumentBean dBean = new DPrintDocumentBean(document);
            ctrl.showDocument(getFeatureView(), this, getModel().getIdentification(), dBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - askNumDocument()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogCancelled(event=" + event + ")");
        }
        getModel().getBean().setCriteriaChecked(false);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogCancelled(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - dialogValidated(event=" + event + ")");
        }
        if (event.getDialogBean() instanceof DPackagingBean) {
            // --------------------------------
            // Packaging has changed
            // --------------------------------

            DPackagingBean pack = (DPackagingBean) event.getDialogBean();
            getBean().getEntity().getEntityLocationBean().getContainerPackaging()
                    .setTattooedId(pack.getKey().getNotat());
        } else {
            CriteriaDBean criteriaDBean = (CriteriaDBean) event.getDialogBean();
            getModel().getBean().setBeanCriteria(criteriaDBean.getCriteriaReturnInitBean());
            getModel().getBean().getBeanCriteria().setAutoOpening(false);
            getModel().getBean().setCriteriaChecked(true);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - dialogValidated(event=" + event + ")");
        }
    }

    /**
     * Disabled all button if the weight mode auto is activated.
     * 
     * @param flagWeightmode enabled or not ?
     */
    public void disabledBtnForWeightMode(final boolean flagWeightmode) {
        this.flagWeightMode = flagWeightmode;
        fireButtonStatechanged();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fireButtonStatechanged() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fireButtonStatechanged()");
        }

        super.fireButtonStatechanged();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fireButtonStatechanged()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void fireErrorsChanged(final UIErrorsException exception) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - fireErrorsChanged(exception=" + exception + ")");
        }

        super.fireErrorsChanged(exception);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - fireErrorsChanged(exception=" + exception + ")");
        }
    }

    /**
     * Gets the browserCtrl.
     * 
     * @category getter
     * @return the browserCtrl.
     */
    public FBoningOutputBCtrl getBrowserCtrl() {
        return browserCtrl;
    }

    /**
     * Gets the fboningOutputContainerCBS.
     * 
     * @return the fboningOutputContainerCBS.
     */
    public FBoningOutputCBS getFboningOutputContainerCBS() {
        return fboningOutputContainerCBS;
    }

    /**
     * Gets the featureCtrl.
     * 
     * @category getter
     * @return the featureCtrl.
     */
    public FBoningOutputFCtrl getFeatureCtrl() {
        return featureCtrl;
    }

    /**
     * Gets the featureView.
     * 
     * @category getter
     * @return the featureView.
     */
    public FeatureView getFeatureView() {
        return featureView;
    }

    /**
     * Gets the map.
     * 
     * @category getter
     * @return the map.
     */
    public Map<MovementAct, FBoningOutputVBean> getMap() {
        return map;
    }

    /**
     * Gets the previous.
     * 
     * @category getter
     * @return the previous.
     */
    public MovementAct getPrevious() {
        return previous;
    }

    /**
     * Get the viewer view.
     * 
     * @return the Viewer View
     */
    public FBoningOutputVView getViewerView() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getViewerView()");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getViewerView()");
        }
        return (FBoningOutputVView) getView();
    }

    /**
     * Gets the workBean.
     * 
     * @category getter
     * @return the workBean.
     */
    @Override
    public FBoningOutputWorkBean getWorkBean() {
        return workBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeViewWithBean(final FBoningOutputVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initializeViewWithBean(bean=" + bean + ")");
        }

        super.initializeViewWithBean(bean);
        setBeanAndInitialBean(bean);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initializeViewWithBean(bean=" + bean + ")");
        }
    }

    /**
     * Is data entry ok to validate ?
     * 
     * @return Is data entry ok to validate ?
     */
    public boolean isDataEntryOK() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isDataEntryOK()");
        }

        boolean ret = true;
        if (getBean() == null) {
            ret = false;
        } else if (!"".equals(getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getUnit())
                && getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getQty() == 0) {
            ret = false;
        } else if (!"".equals(getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().getUnit())
                && getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty().getQty() == 0) {
            ret = false;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isDataEntryOK()=" + ret);
        }
        return ret;
    }

    /**
     * Check the criteria.
     * 
     */
    public void manageCriteriaWindow() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageCriteriaWindow()");
        }

        if (getBean().getBeanCriteria() != null) {
            if (!getBean().getCriteriaChecked() && getBean().getBeanCriteria().getAutoOpening()
                    && getBean().getBeanCriteria().getListCriteria().size() > 0) {
                CriteriaDBean criteriaDBean = new CriteriaDBean(getBean().getBeanCriteria(), getBean().getEntity()
                        .getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
                WorkShopCriteriaTools.showCriteria(this.featureView, this, this, getModel().getIdentification(),
                        criteriaDBean);
            } else {
                getModel().getBean().setCriteriaChecked(true);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageCriteriaWindow()");
        }
    }

    /**
     * Manage finish question.
     */
    public void manageFinishQuestion() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageFinishQuestion()");
        }

        if (getBean().getFinishQuestionNeeded()) {
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29534, false), MessageButtons.YES_NO);
            if (ret == MessageButtons.YES) {
                getBean().setToFinish(true);
            } else {
                getBean().setToFinish(false);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageFinishQuestion()");
        }
    }

    /**
     * Manages question (stock ...).
     * 
     * @return is ok to all questions
     */
    public boolean manageQuestion() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - manageQuestion()");
        }

        boolean isOk = true;
        for (String question : getBean().getQuestions()) {
            int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false), question,
                    MessageButtons.YES_NO);
            if (ret == MessageButtons.NO) {
                isOk = false;
                break;
            }
        }
        if (isOk) {
            getBean().getQuestions().clear();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - manageQuestion()=" + isOk);
        }
        return isOk;
    }

    /**
     * Save current bean to map.
     */
    public void saveCurrent() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveCurrent()");
        }

        MovementAct current = FBoningTools.operationItemKeyToMovementAct(getBean().getBBean().getOperationItemBean()
                .getOperationItemKey());
        if (map.containsKey(current)) {
            map.remove(current);
        }
        map.put(current, getBean());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveCurrent()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        super.selectedRowChanged(event);

        if (getFeatureCtrl().isWeightModeOn()) {
            getFeatureCtrl().processWeigth();
        }
    }

    /**
     * Sets the browserCtrl.
     * 
     * @category setter
     * @param browserCtrl browserCtrl.
     */
    public void setBrowserCtrl(final FBoningOutputBCtrl browserCtrl) {
        this.browserCtrl = browserCtrl;
    }

    /**
     * Sets the fboningOutputContainerCBS.
     * 
     * @param fboningOutputContainerCBS fboningOutputContainerCBS.
     */
    public void setFboningOutputContainerCBS(final FBoningOutputCBS fboningOutputContainerCBS) {
        this.fboningOutputContainerCBS = fboningOutputContainerCBS;
    }

    /**
     * Sets the featureCtrl.
     * 
     * @category setter
     * @param featureCtrl featureCtrl.
     */
    public void setFeatureCtrl(final FBoningOutputFCtrl featureCtrl) {
        this.featureCtrl = featureCtrl;
    }

    /**
     * Sets the featureView.
     * 
     * @category setter
     * @param featureView featureView.
     */
    public void setFeatureView(final FeatureView featureView) {
        this.featureView = featureView;
    }

    /**
     * Sets the map.
     * 
     * @category setter
     * @param map map.
     */
    public void setMap(final Map<MovementAct, FBoningOutputVBean> map) {
        this.map = map;
    }

    /**
     * Sets the previous.
     * 
     * @category setter
     * @param previous previous.
     */
    public void setPrevious(final MovementAct previous) {
        this.previous = previous;
    }

    /**
     * Set scale weight in quantities components.
     * 
     * @param result result from scale
     */
    @Override
    public void setWeightResult(final FCommScaleResult result) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setWeightResult(result=" + result + ")");
        }

        EntityUnitInfos unitInfos = getBean().getEntity().getStockItemBean().getUnitsInfos();
        Quantities k = getBean().getEntity().getStockItemBean().getKeyboardQties();
        if (EntityUnitType.WEIGHT.equals(unitInfos.getFirstUnitType())) {
            k.getFirstQty().setQty(result.getNetWeight());
        } else if (EntityUnitType.WEIGHT.equals(unitInfos.getSecondUnitType())) {
            k.getSecondQty().setQty(result.getNetWeight());
        } else if (EntityUnitType.WEIGHT.equals(unitInfos.getThirdUnitType())) {
            k.getThirdQty().setQty(result.getNetWeight());
        }
        getView().render();
        if (isDataEntryOK()) {
            performSave();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setWeightResult(result=" + result + ")");
        }
    }

    /**
     * Sets the workBean.
     * 
     * @category setter
     * @param workBean workBean.
     */
    public void setWorkBean(final FBoningOutputWorkBean workBean) {
        this.workBean = workBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean pCreationMode, final String pBeanProperty, final Object propertyValue)
            throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - validateBean(pCreationMode=" + pCreationMode + ", pBeanProperty=" + pBeanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }

        getBean().getEntity().getStockItemBean().getStockItem()
                .setItemId(getBean().getEntity().getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        try {
            CEntityEnum entityEnum = null;
            if (FBoningOutputVBeanEnum.ITEM_ID.getValue().equals(pBeanProperty)) {
                entityEnum = CEntityEnum.ITEM_ID;
                // Reset unit and quantities
                getBean().getEntity().getStockItemBean().setKeyboardQties(new Quantities());
            } else if (FBoningOutputVBeanEnum.BATCH_ID.getValue().equals(pBeanProperty)) {
                // Batch
                entityEnum = CEntityEnum.BATCH;
            } else if (FBoningOutputVBeanEnum.CONTAINER_NUMBER.getValue().equals(pBeanProperty)) {
                entityEnum = CEntityEnum.CONTAINER_NUMBER;
            } else if (FBoningOutputVBeanEnum.UPPER_CONTAINER_NUMBER.getValue().equals(pBeanProperty)) {
                entityEnum = CEntityEnum.UPPER_CONTAINER_NUMBER;
            } else if (FBoningOutputVBeanEnum.FIRST_QTY.getValue().equals(pBeanProperty)) {
                if (getBean().getOutputItemParameters().getForcedFirstQty() != 0) {
                    // we convert the quantity just if we have an initialization for the quantity from the profile
                    String secondUnit = getBean().getEntity().getStockItemBean().getKeyboardQties().getSecondQty()
                            .getUnit();
                    if (secondUnit != null && !secondUnit.isEmpty()) {
                        try {
                            QtyUnit qty2 = getFboningOutputContainerCBS().convertQty(getIdCtx(),
                                    getBean().getBBean().getItemCLs().getCode(),
                                    getBean().getEntity().getStockItemBean().getKeyboardQties().getFirstQty(),
                                    secondUnit);
                            getBean().getEntity().getStockItemBean().getKeyboardQties().setSecondQty(qty2);
                        } catch (BusinessException e) {
                            e.printStackTrace();
                            LOGGER.error("", e);
                        }
                    }
                }
            }
            if (entityEnum != null) {
                getModel().setBean(
                        getFboningOutputContainerCBS().validateEntityFields(getIdCtx(), entityEnum, getBean(),
                                getWorkBean()));
                if (CEntityEnum.BATCH.equals(entityEnum)) {
                    manageCriteriaWindow();
                }
                getView().render();
                getView().enableComponents(true);
                fireButtonStatechanged();
            }
        } catch (BusinessException e) {
            throw new UIException(e);
        }
        getView().render();
        // if there is no initialization from the profile quantity, we can directly save the declaration
        if (isDataEntryOK() && getBean().getOutputItemParameters().getForcedFirstQty() == 0) {
            performSave();
        } else {
            saveCurrent();
        }
        getModel().setInitialBean(getBean());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - validateBean(pCreationMode=" + pCreationMode + ", pBeanProperty=" + pBeanProperty
                    + ", propertyValue=" + propertyValue + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String pBeanProperty, final Object propertyValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - valueChanged(pBeanProperty=" + pBeanProperty + ", propertyValue=" + propertyValue + ")");
        }

        if (("entity." + CEntityEnum.BATCH.getValue()).equals(pBeanProperty) && ((String) propertyValue).isEmpty()) {
            getBean().getEntity().getStockItemBean().getStockItem().setBatch(EntityConstants.NEW);
            renderAndEnableComponents();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - valueChanged(pBeanProperty=" + pBeanProperty + ", propertyValue=" + propertyValue + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningOutputVBean convert(final Object pBean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(pBean=" + pBean + ")");
        }

        FBoningOutputBBean bBean = (FBoningOutputBBean) pBean;
        FBoningOutputVBean vBean = null;
        try {
            if (ManagementType.ARCHIVED.equals(((FBoningOutputBBean) pBean).getOperationItemBean()
                    .getOperationItemKey().getManagementType())
                    || !map.containsKey(FBoningTools.operationItemKeyToMovementAct(((FBoningOutputBBean) pBean)
                            .getOperationItemBean().getOperationItemKey()))) {
                vBean = getFboningOutputContainerCBS().convert(getIdCtx(), bBean, getWorkBean());
                vBean.getBBean().setInnerStr(bBean.getInnerStr());
                vBean.getBBean().getStackingPlanInfoBean().setInner(bBean.getStackingPlanInfoBean().getInner());

            } else {
                vBean = map.get(FBoningTools.operationItemKeyToMovementAct(((FBoningOutputBBean) pBean)
                        .getOperationItemBean().getOperationItemKey()));
                vBean.setBBean((FBoningOutputBBean) pBean);
                initVBeanQuantities(vBean);
            }

            // save the bean into the map
            MovementAct current = FBoningTools.operationItemKeyToMovementAct(vBean.getBBean().getOperationItemBean()
                    .getOperationItemKey());
            if (map.containsKey(current)) {
                map.remove(current);
            }
            map.put(current, vBean);
        } catch (BusinessException e) {
            getView().show(new UIException(e));
            vBean = new FBoningOutputVBean();
            vBean.setBBean((FBoningOutputBBean) pBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FBoningOutputVBean bean) throws UIException {
        // Nothing to do

    }

    /**
     * Empty vbean quantities.
     * 
     * @param vBean the viewer bean to update
     */
    protected void initVBeanQuantities(final FBoningOutputVBean vBean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initVBeanQuantities()");
        }
        FBoningOutputVBean bean = vBean;
        if (vBean == null) {
            bean = getBean();
        }

        bean.getEntity()
                .getStockItemBean()
                .getKeyboardQties()
                .setFirstQty(
                        new QtyUnit(0.0, bean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty().getUnit()));
        bean.getEntity()
                .getStockItemBean()
                .getKeyboardQties()
                .setSecondQty(
                        new QtyUnit(0.0, bean.getEntity().getStockItemBean().getKeyboardQties().getSecondQty()
                                .getUnit()));
        if (bean.getOutputItemParameters().getForcedFirstUnit() != null
                && !bean.getOutputItemParameters().getForcedFirstUnit().isEmpty()
                && !bean.getBBean().getHasStackingplan()) {
            bean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty()
                    .setQty(bean.getOutputItemParameters().getForcedFirstQty());
        } else if (bean.getBBean().getHasStackingplan()) {
            bean.getEntity()
            .getStockItemBean()
            .getKeyboardQties()
            .setFirstQty(
                    new QtyUnit(bean.getBBean().getStackingPlanInfoBean().getInner(), bean.getBBean()
                            .getStackingPlanInfoBean().getUnit()));
        }
        if (bean.getEntity().getStockItemBean().getEnableFields().getSecondQty()) {
            bean.getEntity().getStockItemBean().getKeyboardQties().getSecondQty()
                    .setQty(bean.getOutputItemParameters().getForcedSecondQty());
        }
        getView().render();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initVBeanQuantities()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningOutputVBean insertBean(final FBoningOutputVBean pBean, final FBoningOutputVBean pInitialBean)
            throws UIException, UIErrorsException {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean ret = false;
        if (FBoningOutputFCtrl.BTN_FEATURES_REFERENCE.equals(reference)) {
            ret = !flagWeightMode;
        } else if (FBoningOutputFCtrl.BTN_OPEN_NUMBER_REFERENCE.equals(reference)) {
            if (getBean().getBBean().getStackingPlanInfoBean() != null
                    && getBean().getBBean().getStackingPlanInfoBean().getInner() > 0 && !flagWeightMode) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FBoningOutputFCtrl.BTN_WEIGHT_REFERENCE.equals(reference)
                || FBoningOutputFCtrl.BTN_WEIGHT_MODE_REFERENCE.equals(reference)) {
            if (getWorkBean().getScale() != null
                    && //
                    getBean() != null //
                    && (FBoningTools.isValid(getBean())) //
                    && (ManagementType.REAL.equals(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                            .getManagementType()) //
                    && ((EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                            .getFirstUnitType())) //
                    || (EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                            .getSecondUnitType()) && getBean().getEntity().getStockItemBean().getEnableFields()
                            .getSecondQty())))) {
                ret = true;
            } else {
                ret = false;
            }

            if (FBoningOutputFCtrl.BTN_WEIGHT_REFERENCE.equals(reference) && flagWeightMode) {
                ret = false;
            }
            // if there is no scale, the weight btn is always enabled
            if (FBoningOutputFCtrl.BTN_WEIGHT_REFERENCE.equals(reference) && getWorkBean().getScale() == null) {
                ret = true;
            }
        } else if (FBoningOutputFCtrl.BTN_DETAIL_REFERENCE.equals(reference)) {
            if (FBoningTools.isValid(getBean()) && !flagWeightMode) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FBoningOutputFCtrl.BTN_FINISH_REFERENCE.equals(reference)) {
            if (FBoningTools.isValid(getBean()) && !flagWeightMode) {
                if (ManagementType.ARCHIVED.equals(getBean().getBBean().getOperationItemBean().getOperationItemKey()
                        .getManagementType())) {
                    ret = getWorkBean().getHasRightForUnfinishing();
                } else {
                    ret = true;
                }

            } else {
                ret = false;
            }
        } else if (FBoningOutputFCtrl.BTN_FINISH_ALL_REFERENCE.equals(reference)) {
            boolean isItemNotFinished = false;
            for (FBoningOutputBBean bbean : getBrowserCtrl().getModel().getBeans()) {
                if (!ManagementType.ARCHIVED.equals(bbean.getOperationItemBean().getOperationItemKey()
                        .getManagementType())) {
                    isItemNotFinished = true;
                }
            }
            if (isItemNotFinished && !flagWeightMode) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FBoningOutputFCtrl.BTN_SHOW_ALL_REFERENCE.equals(reference)) {
            ret = !flagWeightMode;
        } else if (FBoningOutputFCtrl.BTN_CANCELLAST_REFERENCE.equals(reference)) {
            if (getPrevious() != null) {
                ret = true;
            } else {
                ret = false;
            }
        } else if (FBoningOutputFCtrl.BTN_CLOSE_CONTAINER.equals(reference)) {
            if (((!EntityConstants.NEW.equals(getBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                    .getUpperContainerNumber()) //
            && !getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getUpperContainerNumber()
                    .isEmpty()) || (!EntityConstants.NEW.equals(getBean().getEntity().getEntityLocationBean()
                    .getEntityLocationKey().getContainerNumber()) //
            && !getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getContainerNumber().isEmpty()))
                    && !getBean().getBBean().getHasStackingplan()//
                    && !flagWeightMode) {
                ret = true;

            } else {
                ret = false;
            }
        } else if (FBoningOutputFCtrl.BTN_PDF_REFERENCE.equalsIgnoreCase(reference)) {
            ret = FBoningTools.isValid(getBean()) && !flagWeightMode;
        } else if (AbstractFProductionFCtrl.BTN_OPEN_INCIDENT_REFERENCE.equalsIgnoreCase(reference)) {
            ret = !flagWeightMode;
        } else if (FBoningOutputFCtrl.BTN_OPEN_LABEL_REFERENCE.equals(reference)) {
            ret = !flagWeightMode;
        } else if (ButtonReference.DEBUG_REFERENCE.getReference().equals(reference)) {
            ret = true;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected synchronized boolean performSave() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - performSave()");
        }

        boolean b = false;
        boolean ok = false;
        // Ask location if needed
        if (getBean().getEntity().getEntityLocationBean().getEnableFields().getLocation()
                && getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCemp().isEmpty()) {
            ok = askLocation();
        } else {
            ok = true;
        }
        if (getBean().getBBean().getHasStackingplan()
                && getBean().getBBean().getItemQttyDoneInRefUnitQU().getQty() >= getBean().getBBean()
                .getItemQttyToDoInRefUnitQU().getQty()) {
            ok = false;
        }
        if (ok) {
            b = super.performSave();
            if (b && !getBean().getAutoInsert()) {
                // Data entry not inserted
                manageCriteriaWindow();
                if (getBean().getCriteriaChecked()) {
                    boolean ret = manageQuestion();
                    if (ret) {
                        manageFinishQuestion();
                        getBean().setForceUpdate(true);
                        // Try again to insert data entry
                        performSave();
                    } else {
                        initVBeanQuantities(getBean());
                    }
                } else {
                    initVBeanQuantities(getBean());
                }
            } else {
                // Data entry inserted
                if (!getBean().getToFinish()) {
                    // Output not finished, so we save VBean into map
                    saveCurrent();
                } else {
                    // Output finished so we try to remove Vbean from map
                    getMap().remove(
                            FBoningTools.operationItemKeyToMovementAct(getBean().getBBean().getOperationItemBean()
                                    .getOperationItemKey()));
                }
                if (getBean().getDocuments().size() > 0) {
                    askNumDocument(getBean().getDocuments());
                    try {
                        FBoningOutputVBean vBean = getFboningOutputContainerCBS().printDocuments(getIdCtx(), getBean(),
                                getBean().getDocuments(), getWorkBean());

                        getModel().setBean(vBean);
                        getModel().setInitialBean(vBean);
                        getView().render();
                        fireBeanUpdated(vBean);
                    } catch (BusinessException e) {
                        fireErrorsChanged(new UIException(e));
                    }
                    getBean().getDocuments().clear();
                }
                if (!getWorkBean().getShowFinished() && getBean().getToFinish()) {
                    boolean weightMode = getFeatureCtrl().isWeightModeOn();
                    getFeatureCtrl().setFlagWeightMode(false);
                    browserCtrl.reopenQuery();
                    for (FBoningOutputBBean bbean : browserCtrl.getModel().getBeans()) {
                        if (!ManagementType.ARCHIVED.equals(bbean.getOperationItemBean().getOperationItemKey()
                                .getManagementType())) {
                            browserCtrl
                                    .repositionToOperationItemKey(bbean.getOperationItemBean().getOperationItemKey());
                            break;
                        }
                    }
                    getFeatureCtrl().setFlagWeightMode(weightMode);
                    fireErrorsChanged();
                }
                getBean().setAutoInsert(false);
                getBean().setForceUpdate(false);
                getBean().setCriteriaChecked(false);
                getBean().setToFinish(false);

                // this event is too early fired (before getting all informations) => we have to fire it now
                fireButtonStatechanged();

                // close the feature if there is no item to show
                if (getBrowserCtrl().getModel().getBeans().size() == 0) {
                    try {
                        getFeatureCtrl().closingRequired(
                                new ClosingEvent(this,
                                        SelfFeatureClosingRequiredEvent.EVENT_SELF_FEATURE_CLOSING_REQUIRED, null));
                    } catch (UIVetoException e) {
                        LOGGER.error("", e);
                        fireErrorsChanged(e);
                    }
                }
            }
        } else {
            initVBeanQuantities(getBean());
        }

        // DC6074 - Go back to MO list if save successed and workBean.getReturnToMOList() is true
        if (b && workBean.getReturnToMOList()) {
            fireSelfFeatureClosingRequested();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - performSave()=" + b);
        }
        return b;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setBeanAndInitialBean(final FBoningOutputVBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setBeanAndInitialBean(bean=" + bean + ")");
        }

        super.setBeanAndInitialBean(bean);
        setProgressBar();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setBeanAndInitialBean(bean=" + bean + ")");
        }
    }

    /**
     * Show packaging.
     * 
     * @param entityBean the entity bean
     * @param logicalNumber the lagical number of the current container
     * @throws UIException the uI exception
     */
    protected void showPackaging(final CEntityBean entityBean, final Integer logicalNumber) throws UIException {
        // Dialog bean
        DPackagingBean dialogBean = new DPackagingBean();
        dialogBean.setKey(new TattooedKey(//
                entityBean.getEntityLocationBean().getEntityLocationKey().getCsoc(), //
                entityBean.getEntityLocationBean().getEntityLocationKey().getCetab(), //
                entityBean.getEntityLocationBean().getContainerPackaging().getPackagingId(), //
                entityBean.getEntityLocationBean().getContainerPackaging().getTattooedId()));
        dialogBean.setPackagingCL(new CodeLabel(entityBean.getEntityLocationBean().getContainerPackaging()
                .getPackagingId(), null));
        dialogBean.setProperties(entityBean.getEntityLocationBean().getContainerPackagingProperties());
        dialogBean.setIsPackagingEnabled(false);

        // Selection
        FPackagingSBean selection = new FPackagingSBean(entityBean.getEntityLocationBean().getEntityLocationKey()
                .getCsoc());
        selection.setNotTattooedPackagingOnly(false);
        selection.setOnlyAvailableTattooedNumber(true);
        selection.setSuperiorPackagingOnly(false);

        String title = null;
        if (logicalNumber != null && logicalNumber > 0) {
            title = String.valueOf(logicalNumber);
        }

        DBoningPackagingCtrl.showPackaging(this.getFeatureView(), this, this, getModel().getIdentification(),
                dialogBean, selection, title);

        if (dialogBean != null && dialogBean.getProperties().getTtat()) {
            TattooedKey tattooedKey = dialogBean.getKey();
            // if packaging is not set --> stop validation
            entityBean.getEntityLocationBean().getContainerPackaging().setPackagingId(tattooedKey.getCcond());
            entityBean.getEntityLocationBean().getContainerPackaging().setTattooedId(tattooedKey.getNotat());
            getBean().setAskTattooNb(false);
            try {
                getFboningOutputContainerCBS().updateTattooedNb(getIdCtx(), entityBean, getWorkBean());
            } catch (BusinessException e) {
                LOGGER.error("", e);
                throw new UIException(e);
            }
        }

        if (!EntityTools.isValid(entityBean.getEntityLocationBean().getContainerPackaging().getPackagingId())) {
            throw new UIException(StockKernel.T18703);
        }

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FBoningOutputVBean updateBean(final FBoningOutputVBean pBean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(pBean=" + pBean + ")");
        }

        FBoningOutputVBean vBean = pBean;
        try {
            vBean = getFboningOutputContainerCBS().updateBean(getIdCtx(), vBean, getWorkBean());

            // if the bean is finished automatically and it's the last recreated output item, we need to ask the tattoed
            // number
            if ((vBean.getAskTattooNb() && !vBean.getToFinish())
                    || (vBean.getTattooedList() != null && vBean.getTattooedList().size() > 0)) {
                if (vBean.getTattooedList() != null && vBean.getTattooedList().size() > 0) {
                    for (CEntityBean nsc : vBean.getTattooedList().keySet()) {
                        showPackaging(nsc, vBean.getTattooedList().get(nsc));
                    }
                } else {
                    showPackaging(vBean.getEntity(), null);
                }
            } else {
                // Show a message if save was successful
                if (getFeatureCtrl().isWeightModeOn()) {
                    SwingUtilities.invokeLater(new Runnable() {

                        @Override
                        public void run() {
                            new ValidationNotificationDView(I18nClientManager.translate(ProductionMo.T34425, false))
                                    .setVisible(true);
                        }
                    });
                }
            }
            if (vBean.getAutoInsert()) {
                setPrevious(vBean.getPreviousMovementAct());
                getWorkBean().setInputAvailable(vBean.getInputAvailable());
            }
        } catch (BusinessException e) {
            initVBeanQuantities(getBean());
            UIException ue = new UIException(e);
            getModel().setInitialBean(getBean());
            // fireErrorsChanged(ue);
            throw ue;
        } catch (BusinessErrorsException e) {
            initVBeanQuantities(getBean());
            UIErrorsException ue = new UIErrorsException(e);
            getModel().setInitialBean(getBean());
            // fireErrorsChanged(ue);
            throw ue;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(pBean=" + pBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * Ask location.
     * 
     * @return true if validated, false otherwise
     */
    private boolean askLocation() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - askLocation()");
        }

        boolean ret = false;

        FLocationSBean sBean = new FLocationSBean();
        sBean.setCompanyId(getIdCtx().getCompany());
        sBean.setEstablishmentId(getIdCtx().getEstablishment());
        sBean.setWareHouseId(getBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCdepot());
        FLocationBBean bBean = getViewerView().openLocationHelp(this, sBean);
        if (bBean != null) {
            getBean().getEntity().getEntityLocationBean().setLocationLabel(bBean.getLocationLabel());
            getBean().getEntity().getEntityLocationBean().getEntityLocationKey().setCemp(bBean.getLocationId());
            getView().render();
            ret = true;
        } else {
            ret = false;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - askLocation()=" + ret);
        }
        return ret;
    }

    /**
     * Initialize the progress bar and the input text field "re-created items left" in the viewer.
     */
    private void setProgressBar() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setProgressBar()");
        }

        int cptRecreatedItem = 0;
        double sumDoneQuantity;
        double sumDoneQuantity2;
        String sumDoneQuantity2Format;
        String sumDoneQuantityFormat;
        double sumToDoQty;
        String sumToDoQtyFormat;

        // if the item has just one unit (kg)
        if (EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos().getFirstUnitType())
                && EntityUnitType.UNKNOWN.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                        .getSecondUnitType())) {
            sumDoneQuantity = 0;
            sumDoneQuantity2 = getBean().getBBean().getSumDoneQuantity();
            sumDoneQuantityFormat = "";
            sumDoneQuantity2Format = getBean().getBBean().getSumDoneQuantityFormat();
            sumToDoQty = 0;
            sumToDoQtyFormat = "";
        } else {
            sumDoneQuantity = getBean().getBBean().getSumDoneQuantity();
            sumDoneQuantity2 = getBean().getBBean().getSumDoneQuantity2();
            sumDoneQuantity2Format = getBean().getBBean().getSumDoneQuantity2Format();
            sumDoneQuantityFormat = getBean().getBBean().getSumDoneQuantityFormat();
            sumToDoQty = getBean().getBBean().getSumToDoQty();
            sumToDoQtyFormat = getBean().getBBean().getSumToDoQtyFormat();
        }

        int reCreatedItemsLeft = getBean().getBBean().getReCreatedItemsLeft();
        String quantityUnit = getBean().getBBean().getItemRefUnit();

        // remove filter to get all beans
        List<FBoningOutputBBean> beans = this.browserCtrl.getModel().getAllBeans();

        for (FBoningOutputBBean bean : beans) {
            // if the item has just one unit (kg)
            if (EntityUnitType.WEIGHT.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                    .getFirstUnitType())
                    && EntityUnitType.UNKNOWN.equals(getBean().getEntity().getStockItemBean().getUnitsInfos()
                            .getSecondUnitType())) {
                bean.setSumDoneQuantity(sumDoneQuantity2);
                bean.setSumDoneQuantityFormat(sumDoneQuantity2Format);
            } else {
                bean.setSumDoneQuantity(sumDoneQuantity);
                bean.setSumDoneQuantity2(sumDoneQuantity2);
                bean.setSumDoneQuantity2Format(sumDoneQuantity2Format);
                bean.setSumDoneQuantityFormat(sumDoneQuantityFormat);
                bean.setSumToDoQty(sumToDoQty);
                bean.setSumToDoQtyFormat(sumToDoQtyFormat);
                bean.setReCreatedItemsLeft(reCreatedItemsLeft);
                if (bean.getHasStackingplan()) {
                    cptRecreatedItem++;
                }
            }

            FBoningOutputVBean vBeanToUpdate = map.get(FBoningTools.operationItemKeyToMovementAct(bean
                    .getOperationItemBean().getOperationItemKey()));
            if (vBeanToUpdate != null) {
                vBeanToUpdate.getBBean().setSumDoneQuantity(sumDoneQuantity);
                vBeanToUpdate.getBBean().setSumDoneQuantity2(sumDoneQuantity2);
                vBeanToUpdate.getBBean().setSumDoneQuantity2Format(sumDoneQuantity2Format);
                vBeanToUpdate.getBBean().setSumDoneQuantityFormat(sumDoneQuantityFormat);
                vBeanToUpdate.getBBean().setSumToDoQty(sumToDoQty);
                vBeanToUpdate.getBBean().setSumToDoQtyFormat(sumToDoQtyFormat);
                vBeanToUpdate.getBBean().setReCreatedItemsLeft(reCreatedItemsLeft);
            }
        }

        StringBuilder sb = new StringBuilder();
        sb.append(sumDoneQuantityFormat);
        sb.append(" / ");
        sb.append(sumToDoQtyFormat);

        if (sb.length() == 3) { // we just have the " / "
            sb = new StringBuilder();
        }

        getViewerView().getProgressBar().setMaximum((int) sumToDoQty);
        getViewerView().getProgressBar().setValue((int) sumDoneQuantity);
        getViewerView().getProgressBar().setString(sb.toString());

        if (cptRecreatedItem == 0) {
            getViewerView().getItfReCreatedItemsLeft().setVisible(false);
            getViewerView().getJlReCreatedItemsLeft().setVisible(false);
        } else {
            getViewerView().getItfReCreatedItemsLeft().setVisible(true);
            getViewerView().getJlReCreatedItemsLeft().setVisible(true);
        }

        getViewerView().getItfReCreatedItemsLeft().setValue(reCreatedItemsLeft + " " + quantityUnit);

        getViewerView().getItfProgressionQty2().setValue(sumDoneQuantity2Format);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setProgressBar()");
        }
    }
}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookDynamicIView.java,v $
 * Created on 5 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook;


/**
 * Interface of FTableLookDynamicVView.
 * 
 * @author kl
 */
public interface FTableLookDynamicIView {

    /**
     * Manage the component use to set the code Model and the label model. If insertModel equals true, the two component
     * are enable to set the values. Else, only the label model can be updated.
     * 
     * @param insertMode true if the user insert a new model.
     */
    public void enableTableLook(boolean insertMode);
}

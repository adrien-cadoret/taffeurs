/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOListGSView.java,v $
 * Created on 14 nov. 08 by alb
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.swing;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import javax.swing.ImageIcon;
import javax.swing.JLabel;

import fr.vif.jtech.ui.button.touch.TouchBtnStd;
import fr.vif.jtech.ui.button.touch.TouchToggleBtnStd;
import fr.vif.jtech.ui.input.InputFieldActionEvent;
import fr.vif.jtech.ui.input.InputFieldActionListener;
import fr.vif.jtech.ui.input.swing.SwingInputCheckBox;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.selection.swing.StandardSwingGeneralSelection;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;


/**
 * MOList : General Selection View.
 * 
 * @author alb
 */
public class FMOListGSView extends StandardSwingGeneralSelection {

    private SwingInputCheckBox  swingInputCheckBox;
    private TouchToggleBtnStd   btnTglDisplayAll;
    private SwingInputTextField itfMODate;
    private TouchBtnStd         nextDateBtn;
    private TouchBtnStd         previousDateBtn;

    /**
     * Default constructor.
     * 
     */
    public FMOListGSView() {
        super();
        initialize();
    }

    /**
     * Gets the itfMODate.
     * 
     * @category getter
     * @return the itfMODate.
     */
    public final SwingInputTextField getItfMODate() {
        if (itfMODate == null) {
            itfMODate = new SwingInputTextField(Date.class, TouchInputTextField.FORMAT_DATE, true, new JLabel(
                    I18nClientManager.translate(ProductionMo.T29584)));
            itfMODate.setBeanProperty("moDate");
            itfMODate.setDescription(I18nClientManager.translate(ProductionMo.T29584, false));
        }
        return itfMODate;
    }

    /**
     * Gets the nextDateBtn.
     * 
     * @category getter
     * @return the nextDateBtn.
     */
    public final TouchBtnStd getNextDateBtn() {
        if (nextDateBtn == null) {
            nextDateBtn = new TouchBtnStd();
            nextDateBtn.setPreferredSize(new Dimension(70, 70));
            nextDateBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayAfter.png")));
            nextDateBtn.setEnabled(true);
            nextDateBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    addDate();
                }

            });
        }
        return nextDateBtn;

    }

    /**
     * Gets the previousDateBtn.
     * 
     * @category getter
     * @return the previousDateBtn.
     */
    public final TouchBtnStd getPreviousDateBtn() {
        if (previousDateBtn == null) {
            previousDateBtn = new TouchBtnStd();
            previousDateBtn.setPreferredSize(new Dimension(70, 70));
            previousDateBtn.setIcon(new ImageIcon(this.getClass().getResource(
                    "/images/vif/vif57/production/mo/dayBefore.png")));
            previousDateBtn.setEnabled(true);
            previousDateBtn.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    subDate();
                }

            });
        }
        return previousDateBtn;

    }

    /**
     * Sets the itfRecDate.
     * 
     * @category setter
     * @param itfMODate itfMODate.
     */
    public final void setItfMODate(final SwingInputTextField itfMODate) {
        this.itfMODate = itfMODate;
    }

    /**
     * Sets the nextDateBtn.
     * 
     * @category setter
     * @param nextDateBtn nextDateBtn.
     */
    public final void setNextDateBtn(final TouchBtnStd nextDateBtn) {
        this.nextDateBtn = nextDateBtn;
    }

    /**
     * Sets the previousDateBtn.
     * 
     * @category setter
     * @param previousDateBtn previousDateBtn.
     */
    public final void setPreviousDateBtn(final TouchBtnStd previousDateBtn) {
        this.previousDateBtn = previousDateBtn;
    }

    /**
     * Inform the listener than the value has changed.
     */
    public void valueChanged() {
        InputFieldActionEvent event = new InputFieldActionEvent(getItfMODate(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getItfMODate());
        for (InputFieldActionListener listener : getItfMODate().getInputFieldActionListeners()) {
            listener.valueChanged(event);
        }
    }

    /**
     * Add date.
     */
    private void addDate() {
        Date currentDate = (Date) getItfMODate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, 1);
        getItfMODate().setValue(cal.getTime());
        valueChanged();
    }

    /**
     * set the selection value.
     * 
     * @param isSelected if selected
     */
    private void changedisplayAll(final boolean isSelected) {

        getSwingInputCheckBox().setValue(isSelected);
        InputFieldActionEvent event = new InputFieldActionEvent(getSwingInputCheckBox(),
                InputFieldActionEvent.EVENT_VALUE_CHANGED, getSwingInputCheckBox());
        for (InputFieldActionListener listener : getSwingInputCheckBox().getInputFieldActionListeners()) {
            listener.valueChanged(event);
        }
        manageBtnDisplayAll();

    }

    /**
     * r.
     * 
     * @return t
     */
    private TouchToggleBtnStd getBtnTglDisplayAll() {
        if (btnTglDisplayAll == null) {
            btnTglDisplayAll = new TouchToggleBtnStd();

            btnTglDisplayAll.getBtnModel().setText(
                    TouchHelper.getBtnText(I18nClientManager.translate(ProductionMo.T29545, false),
                            TouchHelper.CENTER_ALIGN));
            btnTglDisplayAll.setIcon(new ImageIcon(getClass().getResource(
                    "/images/vif/vif57/production/mo/showfinished32x23.png")));
            Dimension d = new Dimension(130, 70);
            btnTglDisplayAll.setPreferredSize(d);
            btnTglDisplayAll.setMinimumSize(d);
            btnTglDisplayAll.setMaximumSize(d);

            btnTglDisplayAll.addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(final MouseEvent e) {
                }

                @Override
                public void mouseEntered(final MouseEvent e) {
                }

                @Override
                public void mouseExited(final MouseEvent e) {
                }

                @Override
                public void mousePressed(final MouseEvent e) {
                }

                @Override
                public void mouseReleased(final MouseEvent e) {
                    changedisplayAll(btnTglDisplayAll.isSelected());
                }
            });
        }
        return btnTglDisplayAll;
    }

    /**
     * r.
     * 
     * @return t
     */
    private SwingInputCheckBox getSwingInputCheckBox() {
        if (swingInputCheckBox == null) {
            swingInputCheckBox = new SwingInputCheckBox();
            swingInputCheckBox.setBeanProperty("showFinished");
            swingInputCheckBox.setLabel(I18nClientManager.translate(Generic.T24773, false));

            Dimension d = new Dimension(0, 0);
            swingInputCheckBox.setPreferredSize(d);
            swingInputCheckBox.setMinimumSize(d);
            swingInputCheckBox.setMaximumSize(d);
        }
        return swingInputCheckBox;
    }

    /**
     * This Method Initializes this.
     * 
     */
    private void initialize() {
        final GridBagLayout gridBagLayout = new GridBagLayout();
        gridBagLayout.columnWidths = new int[] { 0, 0, 0, 0, 7 };
        this.setLayout(gridBagLayout);
        setOpaque(false);
        final GridBagConstraints gridBagConstraints = new GridBagConstraints();
        gridBagConstraints.ipady = -30;
        gridBagConstraints.weighty = 0;
        gridBagConstraints.weightx = 1;
        gridBagConstraints.fill = GridBagConstraints.BOTH;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridx = 2;
        add(getItfMODate(), gridBagConstraints);

        final GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.anchor = GridBagConstraints.EAST;
        gridBagConstraints1.insets = new Insets(0, 5, 0, 5);
        gridBagConstraints1.weighty = 0;
        gridBagConstraints1.weightx = 1;
        gridBagConstraints1.gridy = 0;
        gridBagConstraints1.gridx = 1;
        gridBagConstraints1.fill = GridBagConstraints.EAST;
        add(getPreviousDateBtn(), gridBagConstraints1);

        final GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.anchor = GridBagConstraints.WEST;
        gridBagConstraints2.insets = new Insets(0, 5, 0, 5);
        gridBagConstraints2.weighty = 0;
        gridBagConstraints2.weightx = 1;
        gridBagConstraints2.gridy = 0;
        gridBagConstraints2.gridx = 3;
        gridBagConstraints2.fill = GridBagConstraints.WEST;
        add(getNextDateBtn(), gridBagConstraints2);
        final GridBagConstraints gridBagConstraints11 = new GridBagConstraints();
        gridBagConstraints11.gridy = 0;
        gridBagConstraints11.gridx = 4;
        gridBagConstraints11.anchor = GridBagConstraints.WEST;
        gridBagConstraints11.insets = new Insets(0, 5, 0, 5);
        gridBagConstraints11.weighty = 0;
        gridBagConstraints11.weightx = 1;
        gridBagConstraints2.fill = GridBagConstraints.NONE;
        add(getBtnTglDisplayAll(), gridBagConstraints11);
        add(getSwingInputCheckBox(), new GridBagConstraints());
    }

    /**
     * change the toggle state.
     */
    private void manageBtnDisplayAll() {
        ImageIcon i = new ImageIcon();
        if (getBtnTglDisplayAll().isSelected()) {
            getBtnTglDisplayAll().setIcon(
                    new ImageIcon(getClass().getResource("/images/vif/vif57/production/mo/hidefinished32x23.png")));
            getBtnTglDisplayAll().getBtnModel().setText(
                    TouchHelper.getBtnText(I18nClientManager.translate(ProductionMo.T29525, false),
                            TouchHelper.CENTER_ALIGN));
        } else {
            getBtnTglDisplayAll().setIcon(
                    new ImageIcon(getClass().getResource("/images/vif/vif57/production/mo/showfinished32x23.png")));
            getBtnTglDisplayAll().getBtnModel().setText(
                    TouchHelper.getBtnText(I18nClientManager.translate(ProductionMo.T29545, false),
                            TouchHelper.CENTER_ALIGN));
        }

    }

    /**
     * Sub date.
     */
    private void subDate() {
        Date currentDate = (Date) getItfMODate().getValue();
        Calendar cal = GregorianCalendar.getInstance();
        cal.setTime(currentDate);
        cal.add(Calendar.DAY_OF_MONTH, -1);
        getItfMODate().setValue(cal.getTime());
        valueChanged();
    }
}

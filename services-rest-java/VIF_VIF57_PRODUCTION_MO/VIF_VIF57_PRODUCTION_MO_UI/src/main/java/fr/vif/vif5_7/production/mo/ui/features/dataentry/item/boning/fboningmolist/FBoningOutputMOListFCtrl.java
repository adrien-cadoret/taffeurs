/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputMOListFCtrl.java,v $
 * Created on 14 Mar 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmolist;


import java.util.List;

import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnFeatureModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;


/**
 * Output Boning MO List : Feature Controller.
 * 
 * @author cj
 */
public class FBoningOutputMOListFCtrl extends AbstractFBoningMOListFCtrl {

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        List<ToolBarBtnStdBaseModel> lstBtn = super.getActionsButtonModels();

        ToolBarBtnFeatureModel moOutput = new ToolBarBtnFeatureModel();
        moOutput.setReference(AbstractFBoningMOListFCtrl.MO_OUTPUT_BTN);
        moOutput.setIconURL("/images/vif/vif57/production/mo/boningoutput48.png");
        moOutput.setFeatureId("VIF.BONOUT");
        moOutput.setText(I18nClientManager.translate(ProductionMo.T29562, false));
        lstBtn.add(moOutput);

        return lstBtn;
    }

}

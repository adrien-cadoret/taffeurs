/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DTableLookImpotExportDCtrl.java,v $
 * Created on 5 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.importexport;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.dialogs.DialogActionEvent;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.importexport.FTableLookImportExportBean;
import fr.vif.vif5_7.production.mo.ui.features.ftablelook.importexport.swing.DTableLookImportExportDView;


/**
 * Dialog Table Look use to Import or export model.
 * 
 * @author kl
 */
public class DTableLookImpotExportDCtrl extends AbstractStandardDialogController {
    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(DTableLookImpotExportDCtrl.class);

    /**
     * Superclass constructor.
     */
    public DTableLookImpotExportDCtrl() {
        super();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogActionEvent event) {
        super.dialogValidated(event);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - valueChanged(beanProperty=" + beanProperty + ", propertyValue=" + propertyValue + ")");
        }

        if ("action".equals(beanProperty)) {
            ((DTableLookImportExportDView) getView()).getTableLookCode().setAlwaysDisabled(
                    FTableLookImportExportBean.ImportExport.EXPORT.getValue().equals(propertyValue));

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - valueChanged(beanProperty=" + beanProperty + ", propertyValue=" + propertyValue + ")");
        }
    }

}

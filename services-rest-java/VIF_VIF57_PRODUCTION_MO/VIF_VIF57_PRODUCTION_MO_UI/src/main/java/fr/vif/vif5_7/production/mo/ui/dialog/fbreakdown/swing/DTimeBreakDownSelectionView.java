/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DTimeBreakDownSelectionView.java,v $
 * Created on 13 nov. 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown.swing;


import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.util.Date;

import fr.vif.jtech.ui.dialogs.swing.StandardSwingDialog;
import fr.vif.jtech.ui.input.swing.SwingInputLabel;
import fr.vif.jtech.ui.input.swing.SwingInputTextField;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.activities.activities.ui.composites.cproductionresource.CProductionResourceCtrl;
import fr.vif.vif5_7.activities.activities.ui.composites.cproductionresource.swing.CProductionResourceView;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownSBeanEnum;


/**
 * Dialog time breakdown selection View.
 * 
 * @author cj
 */
public class DTimeBreakDownSelectionView extends StandardSwingDialog {
    private SwingInputTextField     itfProdDate = null;
    private SwingInputLabel         lblProdDate = null;
    private CProductionResourceView itfResource = null;
    private SwingInputLabel         lblResource = null;
    private CProductionResourceView itfLabor    = null;
    private SwingInputLabel         lblLabor    = null;

    /**
     * constructor.
     * 
     * @param f Frame
     */
    public DTimeBreakDownSelectionView(final Frame f) {
        super(f);
        init();
    }

    /**
     * Gets the itfLabor.
     * 
     * @return the itfLabor.
     */
    public CProductionResourceView getItfLabor() {
        if (itfLabor == null) {
            itfLabor = new CProductionResourceView(CProductionResourceCtrl.class, getLblLabor().getJLabel());
            itfLabor.setBeanProperty(FTimeBreakDownSBeanEnum.LABOR_CL.getValue());
        }
        return itfLabor;
    }

    /**
     * Gets the itfProdDate.
     * 
     * @return the itfProdDate.
     */
    public SwingInputTextField getItfProdDate() {
        if (itfProdDate == null) {
            itfProdDate = new SwingInputTextField(Date.class, "dd/MM/yyyy", false, getLblProdDate().getJLabel());
            itfProdDate.setBeanProperty(FTimeBreakDownSBeanEnum.PRODDATE.getValue());
        }
        return itfProdDate;
    }

    /**
     * Gets the itfResource.
     * 
     * @return the itfResource.
     */
    public CProductionResourceView getItfResource() {
        if (itfResource == null) {
            itfResource = new CProductionResourceView(CProductionResourceCtrl.class, getLblResource().getJLabel());
            itfResource.setBeanProperty(FTimeBreakDownSBeanEnum.RESOURCE_CL.getValue());
        }
        return itfResource;
    }

    /**
     * Gets the lblLabor.
     * 
     * @return the lblLabor.
     */
    public SwingInputLabel getLblLabor() {
        if (lblLabor == null) {
            lblLabor = new SwingInputLabel();
            lblLabor.setValue(I18nClientManager.translate(ProductionMo.T36171, false) + " : ");
        }
        return lblLabor;
    }

    /**
     * Gets the lblProdDate.
     * 
     * @return the lblProdDate.
     */
    public SwingInputLabel getLblProdDate() {
        if (lblProdDate == null) {
            lblProdDate = new SwingInputLabel();
            lblProdDate.setValue(I18nClientManager.translate(ProductionMo.T34329, false) + " : ");
        }
        return lblProdDate;
    }

    /**
     * Gets the lblResource.
     * 
     * @return the lblResource.
     */
    public SwingInputLabel getLblResource() {
        if (lblResource == null) {
            lblResource = new SwingInputLabel();
            lblResource.setValue(I18nClientManager.translate(ProductionMo.T34865, false) + " : ");
        }
        return lblResource;
    }

    /**
     * Initialize the dialog box.
     */
    private void init() {
        this.setSize(new Dimension(500, 200));
        this.setLayout(new GridBagLayout());
        Insets insetsLeft = new Insets(2, 10, 2, 2);
        Insets insetsRight = new Insets(2, 2, 2, 10);
        this.add(getLblProdDate(), new GridBagConstraints(0, 0, 1, 1, 0, 0, GridBagConstraints.LINE_END,
                GridBagConstraints.NONE, insetsLeft, 0, 0));
        this.add(getItfProdDate(), new GridBagConstraints(1, 0, 2, 1, 0, 1, GridBagConstraints.LINE_START,
                GridBagConstraints.NONE, insetsRight, 0, 0));
        this.add(getLblResource(), new GridBagConstraints(0, 1, 1, 1, 0, 0, GridBagConstraints.LINE_END,
                GridBagConstraints.NONE, insetsLeft, 0, 0));
        this.add(getItfResource(), new GridBagConstraints(1, 1, 2, 1, 1, 1, GridBagConstraints.LINE_START,
                GridBagConstraints.HORIZONTAL, insetsRight, 0, 0));
        this.add(getLblLabor(), new GridBagConstraints(0, 2, 1, 1, 0, 0, GridBagConstraints.LINE_END,
                GridBagConstraints.NONE, insetsLeft, 0, 0));
        this.add(getItfLabor(), new GridBagConstraints(1, 2, 2, 1, 1, 1, GridBagConstraints.LINE_START,
                GridBagConstraints.HORIZONTAL, insetsRight, 0, 0));
        this.add(getOkButton(), new GridBagConstraints(1, 3, 1, 1, 0, 0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, insetsLeft, 0, 0));
        this.add(getCancelButton(), new GridBagConstraints(2, 3, 1, 1, 0, 0, GridBagConstraints.CENTER,
                GridBagConstraints.NONE, insetsRight, 0, 0));
    }
}

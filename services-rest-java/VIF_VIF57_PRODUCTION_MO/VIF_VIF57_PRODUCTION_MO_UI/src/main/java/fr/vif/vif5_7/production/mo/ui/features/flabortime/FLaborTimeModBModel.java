/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeModBModel.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeModBBean;


/**
 * Labor time browser model.
 * 
 * @author nle
 */
public class FLaborTimeModBModel extends BrowserModel<FLaborTimeModBBean> {
    /**
     * Default constructor.
     */
    public FLaborTimeModBModel() {
        super();
        setBeanClass(FLaborTimeModBBean.class);
        setFullyFetched(true);
        setFetchSize(9999999);
        setFirstFetchSize(999999);
    }
}

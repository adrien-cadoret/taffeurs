/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDataEntryListGSView.java,v $
 * Created on 16 févr. 2016 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.touch;


import java.awt.Dimension;
import java.awt.FlowLayout;

import fr.vif.jtech.ui.input.swing.SwingInputLabel;
import fr.vif.jtech.ui.input.touch.TouchInputTextField;
import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.selection.touch.StandardTouchGeneralSelection;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.touch.RoundBackgroundPanelBorder;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;


/**
 * General selection viewer for data entry list feature : just a panel with the common informations like item and
 * quantities.
 *
 * @author cj
 */
public class FDataEntryListGSView extends StandardTouchGeneralSelection {
    private SwingInputLabel     swingInputLabel;
    private TouchInputTextField natureLabel;
    private TouchInputTextField touchInputTextField;
    private TouchInputTextField touchInputTextField2;

    /**
     * Constructor.
     */
    public FDataEntryListGSView() {
        super();
        initialize();
    }

    /**
     * Gets the natureLabel.
     * 
     * @return the natureLabel.
     */
    public TouchInputTextField getNatureLabel() {
        if (natureLabel == null) {
            natureLabel = new TouchInputTextField(String.class, "50", I18nClientManager.translate(ProductionMo.T34653,
                    false));
            natureLabel.setBeanBased(false);
            natureLabel.setEnabled(false);
            natureLabel.setAlwaysDisabled(true);
            Dimension d = new Dimension(250, 60);
            natureLabel.setPreferredSize(d);
            natureLabel.setMinimumSize(d);
            natureLabel.setMaximumSize(d);
        }
        return natureLabel;
    }

    /**
     * gets the TouchInputTextField.
     * 
     * @return a TouchInputTextField
     */
    public TouchInputTextField getTouchInputTextField() {
        if (touchInputTextField == null) {
            touchInputTextField = new TouchInputTextField(String.class, "50", null);
            touchInputTextField.setEnabled(false);
            touchInputTextField.setAlwaysDisabled(true);
            touchInputTextField.setBeanBased(false);
            Dimension d = new Dimension(200, 60);
            touchInputTextField.setMaximumSize(d);
            touchInputTextField.setMinimumSize(d);
            touchInputTextField.setPreferredSize(d);
        }
        return touchInputTextField;
    }

    /**
     * Gets the TouchInputTextField2.
     * 
     * @return TouchInputTextField
     */
    public TouchInputTextField getTouchInputTextField2() {
        if (touchInputTextField2 == null) {
            touchInputTextField2 = new TouchInputTextField(String.class, "50", null);
            touchInputTextField2.setEnabled(false);
            touchInputTextField2.setAlwaysDisabled(true);
            touchInputTextField2.setBeanBased(false);
            Dimension d = new Dimension(200, 60);
            touchInputTextField2.setMaximumSize(d);
            touchInputTextField2.setMinimumSize(d);
            touchInputTextField2.setPreferredSize(d);
        }
        return touchInputTextField2;
    }

    /**
     * Initialize the view.
     */
    private void initialize() {
        setLayout(new FlowLayout(FlowLayout.CENTER));
        setBorder(new RoundBackgroundPanelBorder(StandardColor.TOUCHSCREEN_PANEL_BORDER_COLOR));

        add(getNatureLabel());
        add(getTouchInputTextField());
        add(getTouchInputTextField2());
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningOutputGSView.java,v $
 * Created on 3 juin 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningoutput.touch;


import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.input.touch.TouchInputRadioSet;
import fr.vif.jtech.ui.selection.touch.StandardTouchGeneralSelection;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;


/**
 * Boning output : General Selection View.
 * 
 * @author xg
 */
public class FBoningOutputGSView extends StandardTouchGeneralSelection {
    /** LOGGER. */
    private static final Logger LOGGER      = Logger.getLogger(FBoningOutputGSView.class);

    private TouchInputRadioSet  irsHierarchy;
    private List<CodeLabel>     hierarchies = new ArrayList<CodeLabel>();

    /**
     * Default constructor.
     */
    public FBoningOutputGSView() {
        super();
        setLayout(new BorderLayout());
        setOpaque(false);
        add(getIrsHierarchy(), BorderLayout.CENTER);
    }

    /**
     * Gets the irsHierarchy.
     * 
     * @return the irsHierarchy.
     */
    public TouchInputRadioSet getIrsHierarchy() {
        if (irsHierarchy == null) {
            irsHierarchy = new TouchInputRadioSet();
            irsHierarchy.setFocusable(false);
            irsHierarchy.setLabel("");
            irsHierarchy.setBeanProperty("hierarchy.code");
            updateButtons();
        }
        return irsHierarchy;
    }

    /**
     * Setter for the hierarchies.
     * 
     * @param hierarchiesList hierarchiesList
     */
    public void setHierarchiesList(final List<CodeLabel> hierarchiesList) {
        this.hierarchies = hierarchiesList;
        updateButtons();
    }

    /**
     * Update the filter buttons.
     */
    private void updateButtons() {
        irsHierarchy.addItem("", I18nClientManager.translate(Jtech.T235, false));
        for (CodeLabel hi : this.hierarchies) {
            irsHierarchy.addItem(hi.getCode(), hi.getLabel());
        }
    }
}

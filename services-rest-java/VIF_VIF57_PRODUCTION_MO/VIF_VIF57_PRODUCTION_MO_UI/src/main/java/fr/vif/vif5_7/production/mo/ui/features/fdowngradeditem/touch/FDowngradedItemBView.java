/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FDowngradedItemBView.java,v $
 * Created on 19 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.features.fdowngradeditem.touch;


import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.browser.touch.renderer.row.CodeLabelRowRenderer;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdowngradeditem.FDowngradedItemBBean;


/**
 * F downgraded item : browser view.
 * 
 * @author gp
 */
public class FDowngradedItemBView extends TouchBrowser<FDowngradedItemBBean> {

    /**
     * Constructor.
     */
    public FDowngradedItemBView() {
        super();
        setRowRenderer(new CodeLabelRowRenderer<FDowngradedItemBBean>());
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationVView.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.touch;


import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.logging.Log;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.events.display.DisplayEvent;
import fr.vif.jtech.ui.events.display.DisplayListener;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.filter.AbstractIntuitiveInputPropertyFilter;
import fr.vif.jtech.ui.filter.FilterController;
import fr.vif.jtech.ui.filter.ReflectionIntuitiveInputPropertyFilter;
import fr.vif.jtech.ui.filter.touch.TouchFilterPanel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.util.factories.StandardControllerFactory;
import fr.vif.jtech.ui.util.touch.TouchHelper;
import fr.vif.jtech.ui.viewer.touch.StandardTouchViewer;
import fr.vif.jtech.ui.views.Displayable;
import fr.vif.vif5_7.gen.service.ui.composites.cquantityunit.touch.CQuantityUnitView;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FMOCreationVBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationItemBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationItemBModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationVIView;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation.FMOCreationVModel;


/**
 * MO creation viewer.
 * 
 * @author glc
 */
public class FMOCreationVView extends StandardTouchViewer<FMOCreationVBean> implements Displayable, DisplayListener,
        FMOCreationVIView {
    /**
     * Logger.
     */
    private static final Logger                  LOGGER           = Logger.getLogger(FMOCreationVView.class);

    /**
     * Controller of the browser.
     */
    private AbstractBrowserController<CodeLabel> browserController;

    /**
     * Model of the browser.
     */
    private BrowserModel<CodeLabel>              browserModel;

    /**
     * Triad of the browser.
     */
    private BrowserMVCTriad                      browserTriad;

    private CQuantityUnitView                    cQuantityUnit    = null;
    private JPanel                               jpItem           = null;
    private TouchBrowser<CodeLabel>              tbHelp           = null;
    private TouchFilterPanel<CodeLabel>          touchFilterPanel = null;

    /**
     * Simple constructor.
     */
    public FMOCreationVView() {
        super();
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayRequested(final DisplayEvent event) {
        // Auto-generated method stub
    }

    /**
     * Gets the controller of the browser.
     * 
     * @return the controller of the browser.
     */
    @SuppressWarnings("unchecked")
    public final AbstractBrowserController<CodeLabel> getBrowserController() {
        if (browserController == null) {
            try {
                browserController = StandardControllerFactory.getInstance().getController(
                        browserTriad.getControllerClass());
            } catch (UIException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser UI error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            }
        }
        return browserController;
    }

    /**
     * Gets the browserTriad.
     * 
     * @category getter
     * @return the browserTriad.
     */
    public BrowserMVCTriad getBrowserTriad() {
        if (browserTriad == null) {
            browserTriad = new BrowserMVCTriad(FMOCreationItemBModel.class, FMOCreationItemBView.class,
                    FMOCreationItemBCtrl.class);
        }
        return browserTriad;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initializeView() {
        super.initializeView();
        tbHelp.getModel().setIdentification(getModel().getIdentification());
        browserController.setIdentification(getModel().getIdentification());
        getBrowserController().initialize();
    }

    /**
     * Gets the model of the browser.
     * 
     * @return the model of the browser.
     */
    @SuppressWarnings("unchecked")
    protected final BrowserModel<CodeLabel> getBrowserModel() {
        if (browserModel == null) {
            try {
                browserModel = getBrowserTriad().getModelClass().newInstance();
            } catch (InstantiationException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser instantiation error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            } catch (IllegalAccessException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser illegal access error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            }
        }
        return browserModel;
    }

    /**
     * Gets the cQuantityUnit.
     * 
     * @category getter
     * @return the cQuantityUnit.
     */
    private CQuantityUnitView getCQuantityUnit() {
        if (cQuantityUnit == null) {
            cQuantityUnit = new CQuantityUnitView();
            cQuantityUnit.setBeanProperty(FMOCreationVModel.QUANTITY_UNIT);
            cQuantityUnit.setMandatory(true);
        }
        return cQuantityUnit;
    }

    /**
     * This method initializes jpItem.
     * 
     * @return javax.swing.JPanel.
     */
    private JPanel getJpItem() {
        if (jpItem == null) {
            GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
            gridBagConstraints1.gridx = 1;
            gridBagConstraints1.insets = new Insets(0, 0, 0, 0);
            gridBagConstraints1.anchor = GridBagConstraints.NORTH;
            gridBagConstraints1.weighty = 1.0;
            gridBagConstraints1.fill = GridBagConstraints.VERTICAL;
            gridBagConstraints1.gridy = 0;
            GridBagConstraints gridBagConstraints = new GridBagConstraints();
            gridBagConstraints.gridx = 0;
            gridBagConstraints.weightx = 1.0;
            gridBagConstraints.weighty = 1.0;
            gridBagConstraints.fill = GridBagConstraints.BOTH;
            gridBagConstraints.insets = new Insets(0, 0, 0, 10);
            gridBagConstraints.anchor = GridBagConstraints.NORTH;
            gridBagConstraints.gridy = 0;
            jpItem = new JPanel();
            jpItem.setOpaque(false);
            jpItem.setLayout(new GridBagLayout());
            jpItem.add(getTbHelp(), gridBagConstraints);
            jpItem.add(getTouchFilterPanel(), gridBagConstraints1);
        }
        return jpItem;
    }

    /**
     * This method initializes tbHelp.
     * 
     * @return fr.vif.jtech.ui.browser.touch.TouchBrowser.
     */
    @SuppressWarnings("unchecked")
    private TouchBrowser<CodeLabel> getTbHelp() {
        if (tbHelp == null) {
            try {
                tbHelp = (TouchBrowser<CodeLabel>) getBrowserTriad().getViewClass().newInstance();
                tbHelp.setModel(getBrowserModel());
                getBrowserController().setView(tbHelp);
                // tbHelp.setAutoValidateOnSelect(true);

                // This display listener transfers display events to the recorded displayer after setting this dialog as
                // the owner.
                getBrowserController().addDisplayListener(this);
            } catch (InstantiationException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser instanciation error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            } catch (IllegalAccessException e) {
                if (LOGGER.isEnabledFor(Level.ERROR)) {
                    LOGGER.error(new Log("Field help browser illegal access error"), e);
                }
                TouchHelper.showError(this, "Instanciation error",
                        " Error during the instanciation of the browser triad of the field help.\n"
                                + "Verify you set correctly the browserTriad property for this field !");
            }
        }
        return tbHelp;
    }

    /**
     * This method initializes touchFilterPanel.
     * 
     * @return fr.vif.jtech.ui.filter.touch.TouchFilterPanel.
     */
    private TouchFilterPanel getTouchFilterPanel() {
        if (touchFilterPanel == null) {
            AbstractIntuitiveInputPropertyFilter<CodeLabel> filter;
            // if (initialFilter == null) {
            filter = new ReflectionIntuitiveInputPropertyFilter<CodeLabel>(getBrowserModel().getBeanPropertyAt(
                    getBrowserModel().getSearchColumn()));
            // } else if (initialFilter instanceof AbstractIntuitiveInputPropertyFilter) {
            // filter = (AbstractIntuitiveInputPropertyFilter<BBean>) initialFilter;
            // } else {
            // if (LOGGER.isEnabledFor(Level.ERROR)) {
            // LOGGER.error("Non intuitive input filter found : " + initialFilter);
            // }
            // filter = new ReflectionIntuitiveInputPropertyFilter<CodeLabel>(getBrowserModel().getBeanPropertyAt(
            // getBrowserModel().getSearchColumn()));
            // }
            touchFilterPanel = new TouchFilterPanel<CodeLabel>(filter, getBrowserModel().isSelectionEnabled());
            FilterController<CodeLabel, AbstractIntuitiveInputPropertyFilter<CodeLabel>> filterController = new FilterController<CodeLabel, AbstractIntuitiveInputPropertyFilter<CodeLabel>>();
            filterController.setView(touchFilterPanel);
            filterController.addFilterListener(getBrowserController());
        }
        return touchFilterPanel;
    }

    /**
     * This method initializes this.
     */
    private void initialize() {
        GridBagConstraints gridBagConstraints1 = new GridBagConstraints();
        gridBagConstraints1.gridx = 0;
        gridBagConstraints1.gridy = 0;
        gridBagConstraints1.gridwidth = 2;
        gridBagConstraints1.fill = GridBagConstraints.BOTH;
        gridBagConstraints1.weightx = 0.5;
        gridBagConstraints1.weighty = 0.67;
        GridBagConstraints gridBagConstraints2 = new GridBagConstraints();
        gridBagConstraints2.gridx = 0;
        gridBagConstraints2.gridy = 1;
        gridBagConstraints2.fill = GridBagConstraints.BOTH;
        gridBagConstraints2.weightx = 0.5;
        gridBagConstraints2.weighty = 0.33;
        gridBagConstraints2.insets = new Insets(10, 10, 10, 0);
        GridBagConstraints gridBagConstraints3 = new GridBagConstraints();
        gridBagConstraints3.gridx = 1;
        gridBagConstraints3.gridy = 1;
        gridBagConstraints3.fill = GridBagConstraints.HORIZONTAL;
        gridBagConstraints3.weightx = 0.5;

        JPanel jpanel = new JPanel();
        jpanel.setOpaque(false);

        this.setLayout(new GridBagLayout());
        this.add(getJpItem(), gridBagConstraints1);
        this.add(getCQuantityUnit(), gridBagConstraints2);
        this.add(jpanel, gridBagConstraints3);
    }
}

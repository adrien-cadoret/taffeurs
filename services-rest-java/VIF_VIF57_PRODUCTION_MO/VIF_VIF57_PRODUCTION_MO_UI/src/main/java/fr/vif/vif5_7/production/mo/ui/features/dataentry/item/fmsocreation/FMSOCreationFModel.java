/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMSOCreationFModel.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation;


import fr.vif.jtech.ui.browser.touch.TouchBrowser;
import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmsocreation.touch.FMSOCreationVView;


/**
 * MSO creation feautre model.
 * 
 * @author glc
 */
public class FMSOCreationFModel extends StandardFeatureModel {

    /**
     * Constructor from superclass.
     */
    public FMSOCreationFModel() {
        super();
        this.setBrowserTriad(new BrowserMVCTriad(FMSOCreationBModel.class, TouchBrowser.class, FMSOCreationBCtrl.class));
        this.setViewerTriad(new ViewerMVCTriad(FMSOCreationVModel.class, FMSOCreationVView.class,
                FMSOCreationVCtrl.class));
        this.setTitle(I18nClientManager.translate(ProductionMo.T29494, false));
    }

}

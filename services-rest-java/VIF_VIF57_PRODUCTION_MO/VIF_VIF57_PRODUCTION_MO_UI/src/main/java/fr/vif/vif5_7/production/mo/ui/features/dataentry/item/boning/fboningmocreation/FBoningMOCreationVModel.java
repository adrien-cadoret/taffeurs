/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FBoningMOCreationVModel.java,v $
 * Created on 19 avr. 2013 by xg
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.boning.fboningmocreation;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningmocreation.FBoningMOCreationVBean;


/**
 * boning MO creation viewer model.
 * 
 * @author xg
 */
public class FBoningMOCreationVModel extends ViewerModel<FBoningMOCreationVBean> {

    public static final String OPERATING_PROCESS_CL = "operatingProcessCL";
    public static final String QUANTITY_UNIT        = "quantityUnit";
    public static final String STACKING_PLAN_CL     = "stackingPlanCL";

    /**
     * Simple constructor.
     */
    public FBoningMOCreationVModel() {
        super();
        setBeanClass(FBoningMOCreationVBean.class);
    }
}

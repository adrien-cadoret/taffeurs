/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOTools.java,v $
 * Created on 31 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template;


import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GradientPaint;
import java.awt.event.ActionListener;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPopupMenu;
import javax.swing.JSeparator;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.beans.menu.LaunchParameterBean;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.feature.FeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.models.sharedcontext.SharedContext;
import fr.vif.jtech.ui.orchestrator.swing.SwingDashboardTabbedPane;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.activities.activities.ActivitiesActivities;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOCfgBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOGraphSelectionBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOSelectionBean;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.IndicatorAxesEnum;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.IndicatorTypeValueEnum;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.fproductionmo.ActionId;
import fr.vif.vif5_7.production.mo.ui.constants.MOUIConstants;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.FSupervisorMOFCtrl;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.swing.FSupervisorMOCheckBoxMenu;


/**
 * Tools for the production indicator feature.
 * 
 * @author jd
 */
public final class FSupervisorMOTools {
    public static final String  ACTION_1                 = "ACTION_1";
    public static final String  ACTION_2                 = "ACTION_2";
    public static final String  ACTION_3                 = "ACTION_3";
    public static final String  ACTION_4                 = "ACTION_4";

    public static final String  CLICK_ON_CELL            = "CLICK_ON_CELL";
    public static final String  CLOSE_ALL                = "CLOSE_ALL";
    public static final String  CREATE_NEW_DOCK          = "CREATE_NEW_DOCK";

    public static final String  FORMAT_DATE              = "dd/MM/yyyy";
    public static final String  FORMAT_DATE_HOUR         = "dd/MM/yyyy HH:mm";
    public static final String  FORMAT_HOUR              = "HH:mm";
    public static final String  FRAME_IS_CLOSING         = "FRAME_IS_CLOSING";

    // AC All horizons
    public static final String  HORIZON_1                = "1";
    public static final String  HORIZON_2                = "2";
    public static final String  HORIZON_3                = "3";
    public static final String  HORIZON_4                = "4";
    public static final String  HORIZON_5                = "5";
    public static final String  HORIZON_6                = "6";
    public static final String  HORIZON_7                = "7";
    public static final String  HORIZON_8                = "8";
    public static final String  HORIZON_9                = "9";
    public static final String  HORIZON_10               = "10";
    public static final String  HORIZON_11               = "11";
    public static final String  HORIZON_12               = "12";

    public static final String  ICON_BAR_HORIZONTAL      = "bar_horizontal.png";
    public static final String  ICON_CURSOR              = "cursor.png";
    public static final String  ICON_FINISH              = "Quittancer.png";
    public static final String  ICON_GANTT               = "gantt.png";
    public static final String  ICON_ITEM                = "item.png";
    public static final String  ICON_LINE                = "line.png";
    public static final String  ICON_LIST                = "affichertous.png";
    public static final String  ICON_INANDOUT            = "miseajour.png";

    public static final String  MENU                     = "menu.png";
    public static final String  ICON_MENU                = "menu3Barres.png";
    public static final String  ICON_PIE                 = "pie.png";
    public static final String  ICON_SECTOR              = "sector.png";
    public static final String  ICON_SELECTION           = "selection.png";
    public static final String  ICON_SIMPLE              = "simple.png";
    public static final String  ICON_TEAM                = "team.png";
    public static final String  ICON_UPDATE_DECLARATION  = "miseajour.png";
    public static final String  ICON_UPDATE_MO           = "nouveauof.png";
    public static final String  ICON_VALIDATE            = "valider.png";
    public static final String  ICON_VIEWS               = "views.png";
    public static final String  ICON_DETAIL              = "affichertous.png";
    public static final String  IFRAME_REFRESH           = "refresh.png";
    public static final String  IFRAME_PARAMETER         = "rouage-blanc.png";
    public static final String  MOSAIQUE                 = "MOSAIQUE";
    public static final String  OPEN_DATA_ENTRY          = "OPEN_DATA_ENTRY";
    public static final String  OPEN_FRAME               = "OPEN_FRAME";
    public static final String  OPEN_PRODUCTION_TAB      = "OPEN_PRODUCTION_TAB";
    public static final String  REFRESH                  = "REFRESH";
    public static final String  SAVE_VIEW                = "SAVE_VIEW";

    public static final String  SELECTION                = "SELECTION";
    public static final String  SEP                      = "¶";
    public static final String  SHOW_VIEW                = "SHOW_VIEW";

    private static final String CTX_SUPERVISOR_CFG       = "SUPERVISOR_CFG";

    private static final String DATE_FOR_PROGRESS        = "dateOF";
    private static final String FORMAT_DATE_FOR_PROGRESS = "dd/MM/yyyy";
    private static final String FORMAT_DECIMAL           = "00";
    private static final String HOUR_SEPARATOR           = ":";
    private static final String LIST_FOR_PROGRESS        = "lstOF";

    public static final String  SMALL_ICON_KEY           = "SmallIcon";

    public static final String  SHORT_DESCRIPTION_KEY    = "ShortDescription";

    // Constant
    private static final int    MINIMAL_HEIGHT           = 60;

    /** LOGGER. */
    private static final Logger LOGGER                   = Logger.getLogger(FSupervisorMOTools.class);
    private static final int    MILLI                    = 1000;
    private static final int    MIN                      = 60;

    private static final int    SEC                      = 60;
    private static final String VIEW_NAME_SEPARATOR      = "|";

    /**
     * Constructor.
     */
    private FSupervisorMOTools() {

    }

    /**
     * Calculate color to use.
     * 
     * @param typeValue IndicatorTypeValueEnum
     * @param percent double
     * @return Color color
     */
    public static Color calculateColor(final IndicatorTypeValueEnum typeValue, final double percent) {

        return calculateGradientPaint(typeValue, percent).getColor2();
    }

    /**
     * 
     * Caculate duration between 2 dates.
     * 
     * @param begin Date
     * @param end Date
     * @return duration Date
     */
    public static String calculateDuration(final Date begin, final Date end) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - calculateDuration(begin=" + begin + ", end=" + end + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - calculateDuration(begin=" + begin + ", end=" + end + ")");
        }
        return calculateHourIntervalWithSeconds(end.getTime() - begin.getTime());
    }

    /**
     * Calculate color to use.
     * 
     * @param typeValue IndicatorTypeValueEnum
     * @param percent double
     * @return Color color
     */
    public static GradientPaint calculateGradientPaint(final IndicatorTypeValueEnum typeValue, final double percent) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - calculateColor(typeValue=" + typeValue + ", percent=" + percent + ")");
        }

        // int tolerance = 20;

        double value = percent;

        if (value > 100) {
            value = 100;
        }
        if (value < -100) {
            value = -100;
        }

        // Check ToDo values not null
        int val = (int) Math.round(value / 20);

        // if val is less than 0 the color is RED
        GradientPaint color = MOUIConstants.RED_GREEN_1;

        if (IndicatorTypeValueEnum.GARBAGE.equals(typeValue)) {
            val = -val;
        }

        if (IndicatorTypeValueEnum.GAP.equals(typeValue) || IndicatorTypeValueEnum.GAP_OUTPUT.equals(typeValue)) {
            // Define color according to value 'val'
            switch (val) {
                case -5:
                    color = MOUIConstants.RED_GREEN_1;
                    break;
                case -4:
                    color = MOUIConstants.RED_GREEN_2;
                    break;
                case -3:
                    color = MOUIConstants.RED_GREEN_4;
                    break;
                case -2:
                    color = MOUIConstants.RED_GREEN_6;
                    break;
                case -1:
                    color = MOUIConstants.RED_GREEN_8;
                    break;
                case 0:
                    color = MOUIConstants.RED_GREEN_10;
                    break;
                case 1:
                    color = MOUIConstants.RED_GREEN_8;
                    break;
                case 2:
                    color = MOUIConstants.RED_GREEN_6;
                    break;
                case 3:
                    color = MOUIConstants.RED_GREEN_4;
                    break;
                case 4:
                    color = MOUIConstants.RED_GREEN_2;
                    break;
                case 5:
                    color = MOUIConstants.RED_GREEN_1;
                    break;
                default:
                    color = MOUIConstants.RED_GREEN_1;
                    break;
            }
        } else {
            // Define color according to value 'val'
            switch (val) {
                case -5:
                    color = MOUIConstants.RED_GREEN_1;
                    break;
                case -4:
                    color = MOUIConstants.RED_GREEN_1;
                    break;
                case -3:
                    color = MOUIConstants.RED_GREEN_2;
                    break;
                case -2:
                    color = MOUIConstants.RED_GREEN_3;
                    break;
                case -1:
                    color = MOUIConstants.RED_GREEN_4;
                    break;
                case 0:
                    color = MOUIConstants.RED_GREEN_5;
                    break;
                case 1:
                    color = MOUIConstants.RED_GREEN_6;
                    break;
                case 2:
                    color = MOUIConstants.RED_GREEN_7;
                    break;
                case 3:
                    color = MOUIConstants.RED_GREEN_8;
                    break;
                case 4:
                    color = MOUIConstants.RED_GREEN_9;
                    break;
                case 5:
                    color = MOUIConstants.RED_GREEN_10;
                    break;
                default:
                    color = MOUIConstants.RED_GREEN_10;
                    break;
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - calculateColor(typeValue=" + typeValue + ", percent=" + percent + ")=" + color);
        }
        return color;
    }

    /**
     * 
     * Calculate and format time interval.
     * 
     * @param time Time in milliseconds
     * @return String
     */
    public static String calculateHourInterval(final long time) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - calculateHourInterval(time=" + time + ")");
        }

        DecimalFormat format = new DecimalFormat(FORMAT_DECIMAL);

        long hour = TimeUnit.HOURS.convert(time, TimeUnit.MILLISECONDS);
        long hourInMilli = hour * MIN * SEC * MILLI;
        long minute = TimeUnit.MINUTES.convert(time - (hourInMilli), TimeUnit.MILLISECONDS);
        minute = Math.abs(minute);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - calculateHourInterval(time=" + time + ")");
        }
        return format.format(hour) + HOUR_SEPARATOR + format.format(minute);
    }

    /**
     * 
     * Calculate and format time interval.
     * 
     * @param time Time in milliseconds
     * @return String
     */
    public static String calculateHourIntervalWithSeconds(final long time) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - calculateHourIntervalWithSeconds(time=" + time + ")");
        }

        DecimalFormat format = new DecimalFormat("00");

        long hours = TimeUnit.HOURS.convert(time, TimeUnit.MILLISECONDS);
        long hoursInMilli = hours * MIN * SEC * MILLI;
        long minutes = TimeUnit.MINUTES.convert(time - (hoursInMilli), TimeUnit.MILLISECONDS);
        long minutesInMilli = minutes * SEC * MILLI;
        long seconds = TimeUnit.SECONDS.convert(time - hoursInMilli - minutesInMilli, TimeUnit.MILLISECONDS);

        minutes = Math.abs(minutes);
        seconds = Math.abs(seconds);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - calculateHourIntervalWithSeconds(time=" + time + ")");
        }
        return format.format(hours) + HOUR_SEPARATOR + format.format(minutes) + HOUR_SEPARATOR + format.format(seconds);
    }

    /**
     * Create the frame menu bar.
     * 
     * @param listener who listen to event
     * @param localFrames list of local frames
     * @param views list of views.
     * @return {@link JMenuBar}
     */
    public static JMenuBar createFrameMenuBar(final ActionListener listener, final List<MOLocalFrame> localFrames,
            final List<String> views) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createFrameMenuBar(listener=" + listener + ", localFrames=" + localFrames + ", views="
                    + views + ")");
        }

        JMenuBar menuBar = new JMenuBar();
        menuBar.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        menuBar.setOpaque(false);
        menuBar.setMinimumSize(new Dimension(0, MINIMAL_HEIGHT));
        JMenuItem menuItem = null;

        // Set the views
        JMenu infoMenu = new JMenu();
        infoMenu.setOpaque(false);
        infoMenu.setToolTipText(I18nClientManager.translate(ProductionMo.T32601));
        // infoMenu.setIcon(IconFactory.getIcon32(WMSIcon.MAIN_MENU));
        infoMenu.setIcon(getIcon32(ICON_MENU));

        // Add the standard vif views ...
        List<String> viewStd = new ArrayList<String>();

        for (String viewStdName : FSupervisorMOManager.VIEWS_STANDARD) {
            viewStd.add(viewStdName);
        }

        for (String viewName : viewStd) {
            // Translate the name if needed.
            switch (viewName) {
                case FSupervisorMOManager.VIEW_TEAM:
                    menuItem = new JMenuItem(I18nClientManager.translate(ActivitiesActivities.T29914));
                    menuItem.setIcon(getIcon16(ICON_TEAM));
                    break;
                case FSupervisorMOManager.VIEW_ITEM:
                    menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T32582));
                    menuItem.setIcon(getIcon16(ICON_ITEM));
                    break;
                case FSupervisorMOManager.VIEW_LINE:
                    menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T32584));
                    menuItem.setIcon(getIcon16(ICON_LINE));
                    break;
                case FSupervisorMOManager.VIEW_SECTOR:
                    menuItem = new JMenuItem(I18nClientManager.translate(GenKernel.T30821));
                    menuItem.setIcon(getIcon16(ICON_SECTOR));
                    break;

                default:
                    menuItem = new JMenuItem(viewName);
                    break;
            }

            // Not the best way to do that !!
            menuItem.setActionCommand(FSupervisorMOTools.SHOW_VIEW + VIEW_NAME_SEPARATOR + viewName);
            menuItem.addActionListener(listener);
            infoMenu.add(menuItem);
        }
        infoMenu.add(new JSeparator());

        // .. then add the enable views of the user.
        for (String viewName : views) {
            if (!viewName.startsWith(FSupervisorMOManager.VIEW_REFERENTIEL)
                    && !viewName.startsWith(FSupervisorMOManager.VIEW_LAST)
                    && !FSupervisorMOManager.VIEWS_STANDARD.contains(viewName)) {
                menuItem = new JMenuItem(viewName);
                // Not the best way to do that !!
                menuItem.setActionCommand(FSupervisorMOTools.SHOW_VIEW + VIEW_NAME_SEPARATOR + viewName);
                menuItem.addActionListener(listener);
                infoMenu.add(menuItem);
            }
        }

        //
        menuBar.add(infoMenu);
        // menuBar.setBackground(Color.BLACK);

        // Others functionnalities
        infoMenu = new JMenu();
        infoMenu.setOpaque(false);
        infoMenu.setToolTipText(I18nClientManager.translate(Jtech.T8241));
        infoMenu.setIcon(getIcon32(MENU));
        menuItem = new JMenuItem(I18nClientManager.translate(Jtech.T8241));
        menuItem.setActionCommand(FSupervisorMOTools.SELECTION);
        menuItem.setIcon(FSupervisorMOTools.getIcon16(FSupervisorMOTools.ICON_SELECTION));

        menuItem.addActionListener(listener);
        infoMenu.add(menuItem);

        menuItem = new JMenuItem(I18nClientManager.translate(Jtech.T25260));
        menuItem.setActionCommand(FSupervisorMOTools.SAVE_VIEW);
        menuItem.setIcon(new ImageIcon(SwingDashboardTabbedPane.class.getResource("/images/jtech/filenew.png")));
        menuItem.addActionListener(listener);
        infoMenu.add(menuItem);

        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T32434));
        menuItem.setActionCommand(FSupervisorMOTools.CLOSE_ALL);
        menuItem.setIcon(new ImageIcon(SwingDashboardTabbedPane.class.getResource("/images/jtech/closeAll.png")));
        menuItem.addActionListener(listener);
        infoMenu.add(menuItem);

        menuBar.add(infoMenu);

        // JSeparator j = new JSeparator(JSeparator.VERTICAL);
        // j.setSize(new Dimension(25, 48));
        // menuBar.add(j);

        // // List existing in referential
        Map<String, IndicatorAxesEnum> mapList = new HashMap<String, IndicatorAxesEnum>();
        for (MOLocalFrame localFrame : localFrames) {
            if (!mapList.containsKey(localFrame.getSelection().getAxe().getValue())) {

                mapList.put(localFrame.getSelection().getAxe().getValue(), localFrame.getSelection().getAxe());
            }
        }

        // Pour tous les axes avec un ordre précis
        for (IndicatorAxesEnum type2 : FSupervisorMOTools.getIndicatorAxis()) {

            // On vérifie que l'axe est présent dans le référentiel
            if (mapList.containsKey(type2.getValue())) {

                infoMenu = new JMenu();
                infoMenu.setOpaque(false);
                infoMenu.setToolTipText(I18nClientManager.translate(type2.getName()));
                infoMenu.setIcon(getIcon32(getIcon(type2)));
                // menuFrame.add(infoMenu);
                menuBar.add(infoMenu);

                for (MOLocalFrame localFrame : localFrames) {
                    FSupervisorMOCheckBoxMenu menuCheck = null;
                    if (localFrame != null && localFrame.getSelection().getAxe().equals(type2)) {

                        String title = FSupervisorMOFactory.getTitle(localFrame);

                        menuCheck = new FSupervisorMOCheckBoxMenu(localFrame);
                        menuCheck.setText(title);
                        // Tooltip delay
                        menuCheck.setToolTipText("-");

                        menuCheck.setSelected(localFrame.getFrameBean().getIsVisible());

                        menuCheck.setActionCommand(FSupervisorMOTools.CREATE_NEW_DOCK);

                        menuCheck.addActionListener(listener);

                        infoMenu.add(menuCheck);
                    }
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createFrameMenuBar(listener=" + listener + ", localFrames=" + localFrames + ", views="
                    + views + ")=" + menuBar);
        }
        return menuBar;
    }

    /**
     * Create a popup up menu.
     * 
     * @param listener listener
     * @param localFrames list of local frames.
     * @return a {@link JPopupMenu}
     */
    public static JPopupMenu createFramePopup(final ActionListener listener, final List<MOLocalFrame> localFrames) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createFramePopup(listener=" + listener + ", localFrames=" + localFrames + ")");
        }

        JPopupMenu popup = new JPopupMenu();

        JMenuItem menuItem = null;
        List<IndicatorAxesEnum> list = new ArrayList<IndicatorAxesEnum>();
        for (MOLocalFrame localFrame : localFrames) {
            if (!list.contains(localFrame.getSelection().getAxe())) {
                list.add(localFrame.getSelection().getAxe());
            }
        }
        // popup.addSeparator();

        for (IndicatorAxesEnum type2 : list) {

            JMenu infoMenu = new JMenu(I18nClientManager.translate(type2.getName()));
            infoMenu.setIcon(getIcon16(getIcon(type2)));
            popup.add(infoMenu);

            for (int i = 0; i < localFrames.size(); i++) {
                MOLocalFrame localFrame = localFrames.get(i);

                if (localFrame != null && localFrame.getSelection().getAxe().equals(type2)) {

                    // String title = FSupervisorMOFactory.getTitle(localFrame);

                    String title = "";
                    if (IndicatorAxesEnum.NONE.equals(type2)) {
                        title = I18nClientManager.translate(type2.getName()) + " "
                                + localFrame.getFrameBean().getTitle();
                    } else {
                        title = FSupervisorMOFactory.getTitle(localFrame);
                    }

                    // if (localFrame.getSelection() instanceof IndicatorSelectionBean) {
                    menuItem = new JMenuItem(title);
                    // } else {
                    // menuItem = new JMenuItem(title + " ("
                    // + I18nClientManager.translate(PreparationKernel.T32096, false) + ")");
                    // }
                    // Not the best way to do that !!
                    menuItem.setActionCommand(FSupervisorMOTools.OPEN_FRAME + VIEW_NAME_SEPARATOR + i);
                    menuItem.addActionListener(listener);
                    i++;
                    infoMenu.add(menuItem);
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createFramePopup(listener=" + listener + ", localFrames=" + localFrames + ")=" + popup);
        }
        return popup;
    }

    /**
     * Init the default PiledUpline local frame bean.
     * 
     * @return a {@link LocalFrame}
     */
    public static MOLocalFrame createMOLinesLocalFrame() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createMOLinesLocalFrame()");
        }

        MOLocalFrame localFrame = new MOLocalFrame();
        localFrame.getFrameBean().setIsHidable(true);
        localFrame.getFrameBean().setPreferedSize(new Dimension(1024, 400));
        localFrame.setUniqueId("MOLinesInternalFrame");

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createMOLinesLocalFrame()=" + localFrame);
        }
        return localFrame;
    }

    /**
     * Get the supervisor configuration.
     * 
     * @param ctx shared context
     * @return configuration bean
     */
    public static FSupervisorMOCfgBean getConfiguration(final SharedContext ctx) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getConfiguration(ctx=" + ctx + ")");
        }

        // Share the parameters for FLines

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getConfiguration(ctx=" + ctx + ")");
        }
        return (FSupervisorMOCfgBean) ctx.get(Domain.DOMAIN_THIS, CTX_SUPERVISOR_CFG);
    }

    /**
     * Generate a JMenu to add to the default JFreechart menu.
     * 
     * @param actionListener {@link ActionListener}
     * @return a {@link JMenu}
     */
    public static JMenu getGanttMenuAction(final ActionListener actionListener) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getGanttMenuAction(actionListener=" + actionListener + ")");
        }

        JMenu popup = new JMenu(I18nClientManager.translate(ProductionMo.T32514));

        // popup.addSeparator();

        // Détail || détails
        JMenuItem menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T29536),
                FSupervisorMOTools.getIcon16(ICON_DETAIL));
        menuItem.setActionCommand(ActionId.MO_DETAIL.getValue());
        menuItem.addActionListener(actionListener);
        popup.add(menuItem);

        // Saisie des O.F. || MO data entry
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T27979),
                FSupervisorMOTools.getIcon16(ICON_UPDATE_MO));
        menuItem.setActionCommand(ActionId.MO_UPDATE.getValue());
        menuItem.addActionListener(actionListener);
        popup.add(menuItem);

        // Liste des O.F. || List of Mos
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T26010),
                FSupervisorMOTools.getIcon16(ICON_LIST));
        menuItem.setActionCommand(ActionId.MO_LIST.getValue());
        menuItem.addActionListener(actionListener);
        popup.add(menuItem);

        // Entrées-Sorties de fabrication || Manufacturing inputs and outputs
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T34231),
                FSupervisorMOTools.getIcon16(ICON_INANDOUT));
        menuItem.setActionCommand(ActionId.MO_INPUTS_OUTPUTS.getValue());
        menuItem.addActionListener(actionListener);
        popup.add(menuItem);

        // Clôture des fabrications || Manufacturing closure
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T26891),
                FSupervisorMOTools.getIcon16(ICON_FINISH));
        menuItem.setActionCommand(ActionId.MO_FINISH.getValue());
        menuItem.addActionListener(actionListener);
        popup.add(menuItem);

        // Modification des déclarations || Change of declarations
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T26603),
                FSupervisorMOTools.getIcon16(ICON_UPDATE_DECLARATION));
        menuItem.setActionCommand(ActionId.MO_UPDATE_DECLARATION.getValue());
        menuItem.addActionListener(actionListener);
        popup.add(menuItem);

        // Validation des O.T. || Validation of POs
        menuItem = new JMenuItem(I18nClientManager.translate(ProductionMo.T28422),
                FSupervisorMOTools.getIcon16(ICON_VALIDATE));
        menuItem.setActionCommand(ActionId.MO_VALIDATE.getValue());
        menuItem.addActionListener(actionListener);
        popup.add(menuItem);

        // popup.addSeparator();
        //
        // JMenu menuHorizon = FSupervisorMOTools.getGanttMenuHorizon(actionListener);
        //
        // popup.add(menuHorizon);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getGanttMenuAction(actionListener=" + actionListener + ")=" + popup);
        }
        return popup;
    }

    // NLE8
    // /**
    // * Generate a JMenu to add to the default JFreechart menu.
    // *
    // * @param actionListener {@link ActionListener}
    // * @return a {@link JMenu}
    // */
    // public static JMenu getGanttMenuHorizon(final ActionListener actionListener) {
    // if (LOGGER.isDebugEnabled()) {
    // LOGGER.debug("B - getGanttMenuHorizon(actionListener=" + actionListener + ")");
    // }
    //
    // JMenu popup = new JMenu(I18nClientManager.translate(ProductionMo.T32610));
    //
    // popup.addSeparator();
    //
    // JMenuItem menuItem = new JMenuItem("4 " + I18nClientManager.translate(Generic.T30238));
    // menuItem.setActionCommand(HORIZON_1);
    // menuItem.addActionListener(actionListener);
    // popup.add(menuItem);
    //
    // menuItem = new JMenuItem("8 " + I18nClientManager.translate(Generic.T30238));
    // menuItem.setActionCommand(HORIZON_2);
    // menuItem.addActionListener(actionListener);
    // popup.add(menuItem);
    //
    // menuItem = new JMenuItem("12 " + I18nClientManager.translate(Generic.T30238));
    // menuItem.setActionCommand(HORIZON_3);
    // menuItem.addActionListener(actionListener);
    // popup.add(menuItem);
    //
    // menuItem = new JMenuItem("24 " + I18nClientManager.translate(Generic.T30238));
    // menuItem.setActionCommand(HORIZON_4);
    // menuItem.addActionListener(actionListener);
    // popup.add(menuItem);
    //
    // if (LOGGER.isDebugEnabled()) {
    // LOGGER.debug("E - getGanttMenuHorizon(actionListener=" + actionListener + ")=" + popup);
    // }
    // return popup;
    // }

    /**
     * Get the icon to show.
     * 
     * @param type2 type2
     * @return an Icon
     */
    public static String getIcon(final IndicatorAxesEnum type2) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getIcon(type2=" + type2 + ")");
        }

        String icon = null;

        switch (type2) {
            case SECTOR:
                icon = ICON_SECTOR;
                break;
            case TEAM:
                icon = ICON_TEAM;
                break;
            case ITEM_GROUP:
                icon = ICON_ITEM;
                break;
            case PRODUCTION_LINE:
                icon = ICON_LINE;
                break;
            default:
                icon = "";
                break;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getIcon(type2=" + type2 + ")=" + icon);
        }
        return icon;

    }

    /**
     * get The Icon from the resources.
     * 
     * @param path Path of the icons
     * @param name name of icon
     * @return Icon
     */
    public static Icon getIcon(final String path, final String name) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getIcon(path=" + path + ", name=" + name + ")");
        }

        ImageIcon icon = null;
        URL url = FSupervisorMOTools.class.getResource(path + name);
        if (name != null && url != null) {
            icon = new ImageIcon(url);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getIcon(path=" + path + ", name=" + name + ")=" + icon);
        }
        return icon;
    }

    /**
     * 
     * returns the icons of the fonctionnality.
     * 
     * @param name name of the file
     * @return the icon or null
     */
    public static Icon getIcon16(final String name) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getIcon16(name=" + name + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getIcon16(name=" + name + ")");
        }
        return getIcon("/images/vif/vif57/production/mo/img16x16/", name);
    }

    /**
     * 
     * returns the icons of the fonctionnality.
     * 
     * @param name name of the file
     * @return the icon or null
     */
    public static Icon getIcon32(final String name) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getIcon16(name=" + name + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getIcon16(name=" + name + ")");
        }
        return getIcon("/images/vif/vif57/production/mo/img32x32/", name);
    }

    /**
     * 
     * returns the icons of the fonctionnality.
     * 
     * @param name name of the file
     * @return the icon or null
     */
    public static Icon getIcon48(final String name) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getIcon48(name=" + name + ")");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getIcon48(name=" + name + ")");
        }
        return getIcon("/images/vif/vif57/production/mo/img48x48/", name);
    }

    /**
     * Define the order of IndicatorValueType (in multi cell and popup).
     * 
     * @return List<IndicatorValueTypeEnum>
     */
    public static List<IndicatorAxesEnum> getIndicatorAxis() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getIndicatorAxis()");
        }

        List<IndicatorAxesEnum> list = new ArrayList<IndicatorAxesEnum>();

        // list.add(IndicatorType2Enum.GENERAL);
        list.add(IndicatorAxesEnum.SECTOR);
        list.add(IndicatorAxesEnum.ITEM_GROUP);
        list.add(IndicatorAxesEnum.PRODUCTION_LINE);
        list.add(IndicatorAxesEnum.TEAM);
        list.add(IndicatorAxesEnum.NONE);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getIndicatorAxis()=" + list);
        }
        return list;
    }

    /**
     * Define launched parameters.
     * 
     * @param poDate Date
     * @param list List<Chrono>
     * @return LaunchParameterBean
     */
    public static LaunchParameterBean getLaunchParameter(final Date poDate, final List<ChronoPO> list) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLaunchParameter(poDate=" + poDate + ", list=" + list + ")");
        }

        LaunchParameterBean launchParameterBean = new LaunchParameterBean();
        StringBuilder lstOF = new StringBuilder();
        for (Chrono chrono : list) {
            lstOF.append(FSupervisorMOTools.SEP).append(chrono.getPrechro()).append(",").append(chrono.getChrono());
        }
        if (lstOF.length() > 0) {
            String stringOF = lstOF.substring(1);
            String date = fr.vif.jtech.common.util.DateHelper.formatDate(poDate, FORMAT_DATE_FOR_PROGRESS);
            launchParameterBean.getValues().put(LIST_FOR_PROGRESS, stringOF);
            launchParameterBean.getValues().put(DATE_FOR_PROGRESS, date);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getLaunchParameter(poDate=" + poDate + ", list=" + list + ")=" + launchParameterBean);
        }
        return launchParameterBean;

    }

    /**
     * Create a new default selection.
     * 
     * @param parentController {@link FSupervisorFCtrl}
     * @return an {@link FSupervisorMOSelectionBean}
     */
    public static FSupervisorMOGraphSelectionBean getNewGraphSelection(final FSupervisorMOFCtrl parentController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getNewGraphSelection(parentController=" + parentController + ")");
        }

        FSupervisorMOGraphSelectionBean sel = new FSupervisorMOGraphSelectionBean();
        sel.setEstablishmentKey(parentController.getCurrentSelection().getEstablishmentKey());
        sel.setPoDate(DateHelper.getDateTime(parentController.getCurrentSelection().getCurrentDate(), 0));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getNewGraphSelection(parentController=" + parentController + ")=" + sel);
        }
        return sel;
    }

    /**
     * Create a new default graph selection : PROJECTED, LAUNCHED, DONE.
     * 
     * @param parentController {@link FSupervisorFCtrl}
     * @return an {@link FSupervisorMOSelectionBean}
     */
    public static FSupervisorMOGraphSelectionBean getNewIndicatorGraphLineSelection(
            final FSupervisorMOFCtrl parentController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getNewIndicatorGraphLineSelection(parentController=" + parentController + ")");
        }

        FSupervisorMOGraphSelectionBean sel = getNewIndicatorGraphSelection(parentController);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getNewIndicatorGraphLineSelection(parentController=" + parentController + ")=" + sel);
        }
        return sel;
    }

    /**
     * Create a new default graph selection : PROJECTED, LAUNCHED, DONE.
     * 
     * @param parentController {@link FSupervisorFCtrl}
     * @return an {@link FSupervisorMOSelectionBean}
     */
    public static FSupervisorMOGraphSelectionBean getNewIndicatorGraphSelection(
            final FSupervisorMOFCtrl parentController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getNewIndicatorGraphSelection(parentController=" + parentController + ")");
        }

        FSupervisorMOGraphSelectionBean sel = getNewGraphSelection(parentController);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getNewIndicatorGraphSelection(parentController=" + parentController + ")=" + sel);
        }
        return sel;
    }

    /**
     * Create a new default selection.
     * 
     * @param parentController {@link FSupervisorFCtrl}
     * @return an {@link FSupervisorMOSelectionBean}
     */
    public static FSupervisorMOSelectionBean getNewIndicatorSelection(final FSupervisorMOFCtrl parentController) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getNewIndicatorSelection(parentController=" + parentController + ")");
        }

        FSupervisorMOSelectionBean sel = new FSupervisorMOSelectionBean();

        sel.setEstablishmentKey(parentController.getCurrentSelection().getEstablishmentKey());
        sel.setPoDate(DateHelper.getDateTime(parentController.getCurrentSelection().getCurrentDate(), 0));

        sel.getSectors().addAll(parentController.getCurrentSelection().getSelection().getSectors());
        sel.getCategories().addAll(parentController.getCurrentSelection().getSelection().getCategories());
        sel.getLines().addAll(parentController.getCurrentSelection().getSelection().getLines());
        sel.getTeams().addAll(parentController.getCurrentSelection().getSelection().getTeams());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getNewIndicatorSelection(parentController=" + parentController + ")=" + sel);
        }
        return sel;
    }

    /**
     * Set the configuration in a shared context.
     * 
     * @param ctrl Supervisor controller
     * @param supervisorCfg configuration bean.
     */
    public static void setConfiguration(final FeatureController ctrl, final FSupervisorMOCfgBean supervisorCfg) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - setConfiguration(ctrl=" + ctrl + ", supervisorCfg=" + supervisorCfg + ")");
        }

        // Share the parameters for FLines
        ctrl.getSharedContext().put(Domain.DOMAIN_THIS, CTX_SUPERVISOR_CFG, supervisorCfg);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - setConfiguration(ctrl=" + ctrl + ", supervisorCfg=" + supervisorCfg + ")");
        }
    }

    /**
     * Shows the AxesFilter.
     * 
     * @param localFrame the local MO frame
     * @return boolean
     */
    public static boolean showAxesFilterAction(final MOLocalFrame localFrame) {

        return localFrame.getFrameBean().getTypeFrame() == FSupervisorMOFactory.FRAME_LINES;
    }

}

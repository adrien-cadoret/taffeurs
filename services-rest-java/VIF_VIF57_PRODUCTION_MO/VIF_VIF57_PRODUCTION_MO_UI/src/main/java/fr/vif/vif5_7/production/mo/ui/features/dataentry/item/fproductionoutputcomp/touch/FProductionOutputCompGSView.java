/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputCompGSView.java,v $
 * Created on 17 Sep, 2013 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcomp.touch;


import java.awt.BorderLayout;

import fr.vif.jtech.ui.input.touch.TouchInputRadioSet;
import fr.vif.jtech.ui.selection.touch.StandardTouchGeneralSelection;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.production.mo.constants.Mnemos;


/**
 * Production Output Comp Selection View.
 * 
 * @author kl
 */
public class FProductionOutputCompGSView extends StandardTouchGeneralSelection {

    private TouchInputRadioSet irsState;

    /**
     * Default constructor.
     */
    public FProductionOutputCompGSView() {
        super();
        initialize();
    }

    /**
     * This Method Initializes this.
     * 
     */
    public void initialize() {
        setLayout(new BorderLayout());
        setOpaque(false);
        add(getIrsState(), BorderLayout.CENTER);
        add(getIrsState());
    }

    /**
     * Generate a TouchInputRadioSet with values contains in Mnemos.MONatureComplementaryWorkshop.
     * 
     * @return TouchInputRadioSet
     */
    private TouchInputRadioSet getIrsState() {
        if (irsState == null) {
            irsState = new TouchInputRadioSet();
            irsState.setFocusable(false);
            irsState.setLabel("");
            irsState.setBeanProperty("state");
            irsState.addItem(Mnemos.MONatureComplementaryWorkshop.MANUFACTURING.getValue(),
                    I18nClientManager.translate(Mnemos.MONatureComplementaryWorkshop.MANUFACTURING.getTranslation()));
            irsState.addItem(Mnemos.MONatureComplementaryWorkshop.DOWNGRADED.getValue(),
                    I18nClientManager.translate(Mnemos.MONatureComplementaryWorkshop.DOWNGRADED.getTranslation()));
            irsState.addItem(Mnemos.MONatureComplementaryWorkshop.SCRAP.getValue(),
                    I18nClientManager.translate(Mnemos.MONatureComplementaryWorkshop.SCRAP.getTranslation()));
            irsState.addItem(Mnemos.MONatureComplementaryWorkshop.INPROGRESS.getValue(),
                    I18nClientManager.translate(Mnemos.MONatureComplementaryWorkshop.INPROGRESS.getTranslation()));

        }
        return irsState;
    }

}

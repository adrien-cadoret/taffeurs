/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: DTimeBreakDownSelectionCtrl.java,v $
 * Created on 13 nov. 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown;


import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.dialogs.AbstractStandardDialogController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ResourceType;
import fr.vif.vif5_7.activities.activities.ui.composites.cproductionresource.CProductionResourceCtrl;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.fbreakdown.FTimeBreakDownSBeanEnum;
import fr.vif.vif5_7.production.mo.business.services.features.ftimebreakdown.FTimeBreakDownCBS;
import fr.vif.vif5_7.production.mo.ui.dialog.fbreakdown.swing.DTimeBreakDownSelectionView;


/**
 * Dialog time breakdown selection Ctrl.
 * 
 * @author cj
 */
public class DTimeBreakDownSelectionCtrl extends AbstractStandardDialogController {

    // DP2192 - CHU - CBS to control resources
    private FTimeBreakDownCBS fTimeBreakDownCBS = null;

    /**
     * Gets the fTimeBreakDownCBS.
     *
     * @return the fTimeBreakDownCBS.
     */
    public FTimeBreakDownCBS getfTimeBreakDownCBS() {
        return fTimeBreakDownCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        ((CProductionResourceCtrl) ((DTimeBreakDownSelectionView) getView()).getItfResource().getController())
                .setAttribute(getIdentification().getCompany(), getIdentification().getEstablishment(), null,
                        ResourceType.ELEMENTARY_RESOURCE.getValue(), ResourceType.MACHINE.getValue());

        ((CProductionResourceCtrl) ((DTimeBreakDownSelectionView) getView()).getItfLabor().getController())
                .setAttribute(getIdentification().getCompany(), getIdentification().getEstablishment(), null,
                        ResourceType.ELEMENTARY_RESOURCE.getValue(), ResourceType.MANPOWER.getValue());
        super.initialize();
    }

    /**
     * Sets the fTimeBreakDownCBS.
     *
     * @param fTimeBreakDownCBS fTimeBreakDownCBS.
     */
    public void setfTimeBreakDownCBS(final FTimeBreakDownCBS fTimeBreakDownCBS) {
        this.fTimeBreakDownCBS = fTimeBreakDownCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
        // DP2192 - CHU - resource control
        try {

            if (FTimeBreakDownSBeanEnum.RESOURCE_CL_CODE.getValue().equals(beanProperty)
                    || FTimeBreakDownSBeanEnum.LABOR_CL_CODE.getValue().equals(beanProperty)) {
                getfTimeBreakDownCBS().validateResSBean(getIdCtx(), (FTimeBreakDownSBean) getDialogBean());
            }
        } catch (BusinessException e1) {
            getView().show(new UIException(e1));
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void valueChanged(final String beanProperty, final Object propertyValue) {
    }

}

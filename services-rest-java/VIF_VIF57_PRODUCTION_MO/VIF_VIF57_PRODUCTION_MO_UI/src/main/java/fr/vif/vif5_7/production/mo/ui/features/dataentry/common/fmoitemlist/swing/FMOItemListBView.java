/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOItemListBView.java,v $
 * Created on 12 oct. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmoitemlist.swing;


import fr.vif.jtech.ui.browser.swing.SwingBrowser;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmoitemlist.FMOItemListBBean;


/**
 * MoItemList : Browser View.
 * 
 * @author vr
 */
public class FMOItemListBView extends SwingBrowser<FMOItemListBBean> {

    /**
     * Default constructor.
     *
     */
    public FMOItemListBView() {
        super();
    }

}

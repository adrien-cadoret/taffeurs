/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: TimeChart.java,v $
 * Created on 13 mai 2014 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Paint;
import java.util.Collections;
import java.util.List;

import javax.swing.JPanel;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import fr.vif.jtech.ui.models.format.StandardColor;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.TimeByResource;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.TimeByResourceComparator;


/**
 * Diagram of sum of hours by resource.
 * 
 * @author nle
 */
public class TimeChart extends JPanel {
    private JFreeChart             chart;
    private ChartPanel             chartPanel;

    private DefaultCategoryDataset dataset = new DefaultCategoryDataset();

    /**
     * Constructor.
     * 
     */
    public TimeChart() {
        super();

        chart = ChartFactory.createBarChart(I18nClientManager.translate(ProductionMo.T35125, false), // chart title
                                                                                                     // ("Temps par ressource")
                I18nClientManager.translate(ProductionMo.T413, false), // y axis label ("Ressources")
                I18nClientManager.translate(ProductionMo.T34770, false), // y axis label ("Temps")
                dataset, PlotOrientation.VERTICAL, //
                false, // include legend
                true, // tooltips
                false); // urls
        chart.setBackgroundPaint(toColor(StandardColor.TOUCHSCREEN_BG_START_FUNCTIONALITIES_DISABLED)); // background
                                                                                                        // color
        chart.getPlot().setBackgroundPaint(toColor(StandardColor.TOUCHSCREEN_BG_START_FUNCTIONALITIES_DISABLED)); // background
                                                                                                                  // color
        chart.getTitle().setPaint(toColor(StandardColor.TOUCHSCREEN_FG_BROWSER_CODE)); // orange
        CategoryPlot p = chart.getCategoryPlot();
        p.setRangeGridlinePaint(toColor(StandardColor.TOUCHSCREEN_BG_END_FUNCTIONALITIES_ENABLED)); // light gray
        setLayout(new BorderLayout());
        chartPanel = new ChartPanel(chart);
        // chartPanel.setOpaque(false);
        add(chartPanel, BorderLayout.CENTER);
    }

    /**
     * Feeds the dataset.
     * 
     * @param tcbList the time bean list
     */
    public void setBeanList(final List<TimeByResource> tcbList) {
        // CategoryPlot plot = (CategoryPlot) chart.getPlot();
        // BarRenderer renderer = (BarRenderer) plot.getRenderer();
        dataset.clear();

        // sorts the list
        Collections.sort(tcbList, new TimeByResourceComparator());

        Integer i = 0;
        for (TimeByResource bean : tcbList) {
            dataset.addValue(bean.getHours(), i.toString(), bean.getResource());
            i++;
            // renderer.setSeriesPaint(series++, UIConstants.COLOR_GREEN);
        }
    }

    /**
     * Transforms a color into RGB.
     * 
     * @param color : the color
     * @return the RGB
     */
    private Paint toColor(final StandardColor color) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue());
    }
}

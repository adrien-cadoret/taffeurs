/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_COMMON
 * File : $RCSfile: GraphMOData.java,v $
 * Created on 5 août 2011 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;


/**
 * Contains a curve data.
 * 
 * @author ag
 * @param <F> type of first axis data
 * @param <S> type of second axis data
 */
public class GraphMOData<F, S> implements Serializable {
    /**
     * Serial Version UID.
     */
    private static final long serialVersionUID = 1L;

    private F                 firstAxis;
    private S                 secondAxis;

    /**
     * Constructor.
     */
    public GraphMOData() {
        super();
    }

    /**
     * Constructor.
     * 
     * @param firstAxis firstAxis
     * @param secondAxis secondAxis
     */
    public GraphMOData(final F firstAxis, final S secondAxis) {
        super();
        this.firstAxis = firstAxis;
        this.secondAxis = secondAxis;
    }

    /**
     * Return first value as a date.
     * 
     * @return Date
     */
    public Date getFirstAsDate() {
        Date ret = null;
        if (firstAxis instanceof Date) {
            ret = (Date) firstAxis;
        }

        return ret;
    }

    /**
     * Return second value as a Double.
     * 
     * @return double
     */
    @SuppressWarnings("deprecation")
    public double getFirstAsDouble() {
        double ret = 0;
        if (firstAxis != null) {
            if (firstAxis instanceof Number) {
                ret = ((Number) firstAxis).doubleValue();
            } else if (firstAxis instanceof Date) {
                Date date = (Date) firstAxis;
                ret = date.getHours() * 3600 + date.getMinutes() * 60 / (60 * 4);
            }
        }

        return ret;
    }

    /**
     * Return first value as a String.
     * 
     * @return Date
     */
    public String getFirstAsString() {
        String ret = null;
        if (firstAxis instanceof String) {
            ret = (String) firstAxis;
        } else if (firstAxis != null) {
            ret = firstAxis.toString();
        } else {
            ret = I18nClientManager.translate(ProductionMo.T32469);
        }

        return ret;
    }

    /**
     * Gets the firstAxis.
     * 
     * @return the firstAxis.
     */
    public F getFirstAxis() {
        return firstAxis;
    }

    /**
     * Return second value as a Double.
     * 
     * @return Date
     */
    @SuppressWarnings("deprecation")
    public double getSecondAsDouble() {
        double ret = 0;
        if (secondAxis != null) {
            if (secondAxis instanceof Number) {
                ret = ((Number) secondAxis).doubleValue();
            } else if (secondAxis instanceof Date) {
                Date date = (Date) secondAxis;
                ret = date.getHours() * 3600 + date.getMinutes() * 60 / (60 * 4);
            }
        }

        return ret;
    }

    /**
     * Return second value as a GraphPeriod.
     * 
     * @return Date
     */
    public GraphMOPeriod getSecondAsGraphPeriod() {
        GraphMOPeriod ret = null; // new GraphPeriod();
        if (secondAxis != null && secondAxis instanceof GraphMOPeriod) {
            ret = (GraphMOPeriod) secondAxis;
        }

        return ret;
    }

    /**
     * Gets the secondAxis.
     * 
     * @return the secondAxis.
     */
    public S getSecondAxis() {
        return secondAxis;
    }

    /**
     * Sets the firstAxis.
     * 
     * @param firstAxis firstAxis.
     */
    public void setFirstAxis(final F firstAxis) {
        this.firstAxis = firstAxis;
    }

    /**
     * Sets the secondAxis.
     * 
     * @param secondAxis secondAxis.
     */
    public void setSecondAxis(final S secondAxis) {
        this.secondAxis = secondAxis;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return "GraphData [firstAxis=" + firstAxis + ", secondAxis=" + secondAxis + "]";
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionContainerMOListFModel.java,v $
 * Created on 07 nov. 2008 by ALB
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductioncontainermolist;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.GeneralSelectionMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.FMOListBCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.FMOListBModel;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.FMOListGSCtrl;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fmolist.FMOListGSModel;


/**
 * MOList : Feature Model.
 * 
 * @author alb
 */
public class FProductionContainerMOListFModel extends StandardFeatureModel {

    /**
     * Default constructor.
     * 
     */
    public FProductionContainerMOListFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FMOListBModel.class, null, FMOListBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FProductionContainerMOListVModel.class, null,
                FProductionContainerMOListVCtrl.class));
        setGeneralSelectionTriad(new GeneralSelectionMVCTriad(FMOListGSModel.class, null, FMOListGSCtrl.class));
    }
}

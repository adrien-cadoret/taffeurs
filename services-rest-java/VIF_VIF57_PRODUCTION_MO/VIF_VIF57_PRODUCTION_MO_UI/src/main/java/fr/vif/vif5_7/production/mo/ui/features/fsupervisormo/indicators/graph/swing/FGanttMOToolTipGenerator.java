/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOToolTipGenerator.java,v $
 * Created on 24 fev 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import org.jfree.chart.labels.XYToolTipGenerator;
import org.jfree.data.xy.XYDataset;

import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.activities.activities.ActivitiesActivities;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOPeriod;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.GraphMOTools;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.template.FSupervisorMOTools;


/**
 * FGraphGanttToolTipGenerator : Generate the tooltip of each Task of a Gantt view.
 * 
 * @author jd
 */
public class FGanttMOToolTipGenerator implements XYToolTipGenerator {
    private GraphMO        graph          = null;
    private GraphMOOptions options        = null;
    private StringBuilder  toolTipBuilder = new StringBuilder();

    /**
     * Constructor.
     * 
     * @param graph Graph
     * @param options options
     */
    public FGanttMOToolTipGenerator(final GraphMO graph, final GraphMOOptions options) {
        super();
        this.graph = graph;
        this.options = options;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public String generateToolTip(final XYDataset dataset, final int series, final int item) {
        // Be careful this algo is used in generalToolTipGenerator too
        GraphMOPeriod<IndicatorMO> period = graph
                .getGraphData()
                .get(options.getStartDataSet() + Math.min(graph.getGraphData().size(), options.getNumberOfDataSetMax())
                        - series - 1).getGraphData().get(item).getSecondAsGraphPeriod();
        toolTipBuilder = new StringBuilder();
        if (period != null) {
            double effQty = period.getBeanSource().getQties().getProcessQtySU().getEffectiveQty();
            double expQty = period.getBeanSource().getQties().getProcessQtySU().getExpectedQty();
            double percent;
            final int cent = 100;
            String percentStr;

            if (expQty == 0) {
                percentStr = "";
            } else {
                percent = (effQty / expQty) * cent;
                percentStr = " (" + Integer.toString((int) percent) + "%)";
            }

            toolTipBuilder.append("<html>");
            toolTipBuilder.append("<body>");

            toolTipBuilder.append("<table>");

            addRow(I18nClientManager.translate(ProductionMo.T32541),
                    Integer.toString(period.getBeanSource().getPoKey().getChrono().getChrono()));

            addRow(I18nClientManager.translate(GenKernel.T29496), period.getBeanSource().getInfos().getItemCL()
                    .getLabel());

            addRow(I18nClientManager.translate(ProductionMo.T32452), GraphMOTools.FORMAT_DEC.format(effQty) + " / " + //
                    GraphMOTools.FORMAT_DEC.format(expQty) + " " + //
                    period.getBeanSource().getQties().getProcessQtySU().getUnitID() + //
                    percentStr);

            addRow(I18nClientManager.translate(ProductionMo.T32440), period.getBeanSource().getInfos().getLineCL()
                    .getCode()
                    + " " + period.getBeanSource().getInfos().getLineCL().getLabel());

            addRow(I18nClientManager.translate(ProductionMo.T27494), //
                    period.getBeanSource().getInfos().getTeamCL().getCode() //
                    + " " + period.getBeanSource().getInfos().getTeamCL().getLabel());

            addRow(I18nClientManager.translate(ProductionMo.T32542), //
                    DateHelper.formatDate(period.getBeanSource().getInfos().getEffectiveBeginDate(),
                            FSupervisorMOTools.FORMAT_DATE_HOUR));

            addRow(I18nClientManager.translate(ProductionMo.T32544), //
                    DateHelper.formatDate(period.getBeanSource().getInfos().getEffectiveEndDate(),
                            FSupervisorMOTools.FORMAT_DATE_HOUR));

            addRow(I18nClientManager.translate(ProductionMo.T32536), //
                    period.getBeanSource().getQties().getInputQtyWU().getEffectiveQty() + " " + //
                    period.getBeanSource().getQties().getInputQtyWU().getUnitID());

            addRow(I18nClientManager.translate(ActivitiesActivities.T30576), //
                    period.getBeanSource().getQties().getOutputQtyWU().getEffectiveQty() + " " + //
                    period.getBeanSource().getQties().getOutputQtyWU().getUnitID());

            //
            // toolTipBuilder.append("<img src='/images/vif/vif57/production/mo/calendar.png' border='0' />");

            toolTipBuilder.append("</table>");
            toolTipBuilder.append("</body>");
            toolTipBuilder.append("</html>");
        }
        return toolTipBuilder.toString();
    }

    /**
     * Add Row with n cols.
     * 
     * @param col1 first col
     * @param col2 second col
     */
    private void addRow(final String col1, final String col2) {
        toolTipBuilder.append("<tr><TD><B><A>");
        toolTipBuilder.append(col1);
        toolTipBuilder.append("</A></B></TD>");
        toolTipBuilder.append("<TD>");
        toolTipBuilder.append(col2);
        toolTipBuilder.append("<TD>");
        toolTipBuilder.append("</TR>");

    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FGanttMOAnnotationPanel.java,v $
 * Created on Apr 11, 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph.swing;


import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

import javax.swing.JPanel;
import javax.swing.border.EtchedBorder;

import fr.vif.jtech.ui.input.swing.SwingInputCheckBox;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;


/**
 * TODO Write the class' description
 *
 * @author ac
 */
public class FGanttMOAnnotationPanel extends JPanel {

    private static final int   WIDTH  = 300;
    private static final int   HEIGHT = 50;

    private JPanel             checkBoxPanel;

    private SwingInputCheckBox chronoCB;
    private SwingInputCheckBox itemLabelCB;
    private SwingInputCheckBox codeLabelCB;
    private SwingInputCheckBox effectiveQtyCB;
    private SwingInputCheckBox expectedQtyCB;

    /**
     * Constructor.
     * 
     * @param view A dialog.
     * @wbp.parser.constructor
     */
    public FGanttMOAnnotationPanel() {
        super();
        initialize();
    }

    private JPanel getCheckBoxPanel() {
        if (checkBoxPanel == null) {
            checkBoxPanel = new JPanel();
            checkBoxPanel.setBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null));
            checkBoxPanel.setLayout(new GridBagLayout());

            GridBagConstraints gbcChronoCB = new GridBagConstraints();
            gbcChronoCB.weightx = 0.0;
            gbcChronoCB.anchor = GridBagConstraints.WEST;
            gbcChronoCB.insets = new Insets(0, 0, 0, 5);
            gbcChronoCB.gridx = 0;
            gbcChronoCB.gridy = 0;
            checkBoxPanel.add(getChronoCheckBox(), gbcChronoCB);

            GridBagConstraints gbcItemLabelCB = new GridBagConstraints();
            gbcItemLabelCB.weightx = 0.0;
            gbcItemLabelCB.anchor = GridBagConstraints.WEST;
            gbcItemLabelCB.insets = new Insets(0, 0, 0, 5);
            gbcItemLabelCB.gridx = 1;
            gbcItemLabelCB.gridy = 0;
            checkBoxPanel.add(getItemLabelCheckBox(), gbcItemLabelCB);

            GridBagConstraints gbcCodeLabelCB = new GridBagConstraints();
            gbcCodeLabelCB.weightx = 0.0;
            gbcCodeLabelCB.anchor = GridBagConstraints.WEST;
            gbcCodeLabelCB.insets = new Insets(0, 0, 0, 5);
            gbcCodeLabelCB.gridx = 0;
            gbcCodeLabelCB.gridy = 1;
            checkBoxPanel.add(getCodeLabelCheckBox(), gbcCodeLabelCB);

            GridBagConstraints gbcEffectiveQtyCB = new GridBagConstraints();
            gbcEffectiveQtyCB.weightx = 0.0;
            gbcEffectiveQtyCB.anchor = GridBagConstraints.WEST;
            gbcEffectiveQtyCB.insets = new Insets(0, 0, 0, 5);
            gbcEffectiveQtyCB.gridx = 1;
            gbcEffectiveQtyCB.gridy = 1;
            checkBoxPanel.add(getEffectiveQtyCheckBox(), gbcEffectiveQtyCB);

            GridBagConstraints gbcExpectedQtyCB = new GridBagConstraints();
            gbcExpectedQtyCB.weightx = 0.0;
            gbcExpectedQtyCB.anchor = GridBagConstraints.WEST;
            gbcExpectedQtyCB.insets = new Insets(0, 0, 0, 5);
            gbcExpectedQtyCB.gridx = 2;
            gbcExpectedQtyCB.gridy = 1;
            checkBoxPanel.add(getExpectedQtyCheckBox(), gbcExpectedQtyCB);

        }
        return checkBoxPanel;
    }

    private SwingInputCheckBox getChronoCheckBox() {
        if (chronoCB == null) {
            chronoCB = new SwingInputCheckBox();
            // weightCB.setPreferredSize(new Dimension(200, 24));
            chronoCB.getJCheckBox().setText(
                    I18nClientManager.translate(ProductionMo.T9520) + " "
                            + I18nClientManager.translate(ProductionMo.T19824));
            chronoCB.setBeanProperty("showChrono");
        }
        return chronoCB;
    }

    private SwingInputCheckBox getCodeLabelCheckBox() {
        if (codeLabelCB == null) {
            codeLabelCB = new SwingInputCheckBox();
            // weightCB.setPreferredSize(new Dimension(200, 24));
            codeLabelCB.getJCheckBox().setText(I18nClientManager.translate(GenKernel.T29496));
            codeLabelCB.setBeanProperty("showCodeLabel");
        }
        return codeLabelCB;
    }

    private SwingInputCheckBox getEffectiveQtyCheckBox() {
        if (effectiveQtyCB == null) {
            effectiveQtyCB = new SwingInputCheckBox();
            // weightCB.setPreferredSize(new Dimension(200, 24));
            effectiveQtyCB.getJCheckBox().setText(I18nClientManager.translate(ProductionMo.T32548));
            effectiveQtyCB.setBeanProperty("showEffectiveQty");
        }
        return effectiveQtyCB;
    }

    private SwingInputCheckBox getExpectedQtyCheckBox() {
        if (expectedQtyCB == null) {
            expectedQtyCB = new SwingInputCheckBox();
            // weightCB.setPreferredSize(new Dimension(200, 24));
            expectedQtyCB.getJCheckBox().setText(I18nClientManager.translate(ProductionMo.T32547));
            expectedQtyCB.setBeanProperty("showExpectedQty");
        }
        return expectedQtyCB;
    }

    private SwingInputCheckBox getItemLabelCheckBox() {
        if (itemLabelCB == null) {
            itemLabelCB = new SwingInputCheckBox();
            // weightCB.setPreferredSize(new Dimension(200, 24));
            itemLabelCB.getJCheckBox().setText(I18nClientManager.translate(ProductionMo.T39342));
            itemLabelCB.setBeanProperty("showItemLabel");
        }
        return itemLabelCB;
    }

    /**
     * This private method is called to initialize the view.
     */
    private void initialize() {

        setLayout(new GridBagLayout());

        GridBagConstraints gbc = new GridBagConstraints();
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1;
        gbc.weighty = 1;
        gbc.anchor = GridBagConstraints.LINE_START;
        gbc.fill = GridBagConstraints.BOTH;

        add(getCheckBoxPanel(), gbc);
        setPreferredSize(new Dimension(WIDTH, HEIGHT));
        // pack();
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeModBView.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.touch;


import org.apache.log4j.Logger;

import fr.vif.jtech.ui.browser.touch.TouchLineBrowser;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.ui.features.flabortime.FLaborStaffBCtrl;


/**
 * Labor time BView.
 * 
 * @author nle
 */
public class FLaborTimeModBView extends TouchLineBrowser<FLaborTimeBBean> implements FLaborTimeModBIView {

    private static final Logger LOGGER = Logger.getLogger(FLaborStaffBCtrl.class);

    /**
     * Default constructor.
     */
    public FLaborTimeModBView() {
        super();
        setOpaque(false);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
    }

}

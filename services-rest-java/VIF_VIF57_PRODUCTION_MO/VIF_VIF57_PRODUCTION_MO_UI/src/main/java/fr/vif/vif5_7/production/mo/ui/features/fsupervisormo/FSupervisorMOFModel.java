/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSupervisorMOFModel.java,v $
 * Created on 27 janv. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo;


import fr.vif.jtech.ui.feature.StandardFeatureModel;
import fr.vif.jtech.ui.models.mvc.BrowserMVCTriad;
import fr.vif.jtech.ui.models.mvc.ViewerMVCTriad;


/**
 * FSupervisorMO : Feature Model.
 * 
 * @author jd
 */
public class FSupervisorMOFModel extends StandardFeatureModel {

    /**
     * Default constructor.
     * 
     */
    public FSupervisorMOFModel() {
        super();
        setBrowserTriad(new BrowserMVCTriad(FSupervisorMOBModel.class, null, FSupervisorMOBCtrl.class));
        setViewerTriad(new ViewerMVCTriad(FSupervisorMOVModel.class, null, FSupervisorMOVCtrl.class));
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: CSubstituteItemIView.java,v $
 * Created on 8 déc. 08 by vr
 */
package fr.vif.vif5_7.production.mo.ui.composites.csubstituteitem;


import fr.vif.jtech.common.beans.Selection;


/**
 * Substitute item view interface.
 * 
 * @author vr
 */
public interface CSubstituteItemIView {

    /**
     * Get the selection.
     * 
     * @return the selection
     */
    public Selection getSelection();

    /**
     * Set selection.
     * 
     * @param selection the selection
     */
    public void setSelection(Selection selection);
}

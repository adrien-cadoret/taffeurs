/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookSettingBCtrl.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook.setting;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.beans.Selection;
import fr.vif.jtech.ui.browser.AbstractBrowserController;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.setting.FTableLookSettingVBean;
import fr.vif.vif5_7.production.mo.business.services.features.tablelook.setting.FTableLookSettingCBS;


/**
 * Ftable look Browser Controller.
 * 
 * @author kl
 */
public class FTableLookSettingBCtrl extends AbstractBrowserController<FTableLookSettingBBean> {
    /** LOGGER. */
    private static final Logger  LOGGER = Logger.getLogger(FTableLookSettingBCtrl.class);

    private FTableLookSettingCBS ftableLookSettingCBS;

    /**
     * {@inheritDoc}
     */
    @Override
    public Selection getDefaultInitialSelection() {
        FTableLookVBean vBean = (FTableLookVBean) getSharedContext().get(Domain.DOMAIN_PARENT, "viewerBean");
        FTableLookSettingSBean sBean = new FTableLookSettingSBean();
        sBean.setCodeModelRef(vBean.getBrowserBean().getModRef());

        return sBean;
    }

    /**
     * Gets the ftableLookSettingCBS.
     * 
     * @return the ftableLookSettingCBS.
     */
    public FTableLookSettingCBS getFtableLookSettingCBS() {
        return ftableLookSettingCBS;
    }

    /**
     * Sets the ftableLookSettingCBS.
     * 
     * @param ftableLookSettingCBS ftableLookSettingCBS.
     */
    public void setFtableLookSettingCBS(final FTableLookSettingCBS ftableLookSettingCBS) {
        this.ftableLookSettingCBS = ftableLookSettingCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FTableLookSettingBBean convert(final Object bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }

        FTableLookSettingBBean bBean = null;
        if (bean != null && bean instanceof FTableLookSettingVBean
                && ((FTableLookSettingVBean) bean).getBrowserBean() != null) {
            bBean = ((FTableLookSettingVBean) bean).getBrowserBean();
        } else {
            bBean = new FTableLookSettingBBean();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")=" + bBean);
        }
        return bBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<FTableLookSettingBBean> queryElements(final Selection selection, final int startIndex,
            final int rowNumber) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")");
        }

        List<FTableLookSettingBBean> bBeans = new ArrayList<FTableLookSettingBBean>();

        try {
            bBeans = getFtableLookSettingCBS().queryElements(getIdCtx(), (FTableLookSettingSBean) selection,
                    startIndex, rowNumber);
        } catch (BusinessException e) {
            LOGGER.info("P - BusinessException e=" + e);
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - queryElements(selection=" + selection + ", startIndex=" + startIndex + ", rowNumber="
                    + rowNumber + ")=" + bBeans);
        }
        return bBeans;
    }

}

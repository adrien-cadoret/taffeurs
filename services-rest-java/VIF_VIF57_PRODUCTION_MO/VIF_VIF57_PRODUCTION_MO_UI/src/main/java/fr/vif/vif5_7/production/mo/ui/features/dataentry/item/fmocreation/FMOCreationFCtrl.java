/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FMOCreationFCtrl.java,v $
 * Created on 28 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fmocreation;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeEvent;
import fr.vif.jtech.ui.events.dialogs.DialogChangeListener;
import fr.vif.jtech.ui.feature.StandardFeatureController;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.toolbar.buttons.toolbarbutton.ToolBarButtonKey;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.criteria.business.beans.features.fcriteria.CriteriaDBean;
import fr.vif.vif5_7.gen.criteria.ui.features.fcriteria.WorkShopCriteriaTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FMOCreationVBean;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;


/**
 * MO creation feature controller.
 * 
 * @author glc
 */
public class FMOCreationFCtrl extends StandardFeatureController implements DialogChangeListener {

    public static final String  EVENT_CREATESTARTEDMO = "createStartedMO";
    public static final String  EVENT_OPENCRITERIA    = "openCriteria";

    /** LOGGER. */
    private static final Logger LOGGER                = Logger.getLogger(FMOCreationFCtrl.class);

    private ToolBarBtnStdModel  btnCreateStartedMO    = null;
    private ToolBarBtnStdModel  btnCriteria           = null;

    /**
     * {@inheritDoc}
     */
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        String reference = event.getModel().getReference();
        if (reference == EVENT_CREATESTARTEDMO) {
            if (((FMOCreationVCtrl) getViewerController()).firePerformSave()) {
                fireSelfFeatureClosingRequired();
            }
        } else if (reference == EVENT_OPENCRITERIA) {
            CriteriaDBean criteriaDBean = new CriteriaDBean(
                    ((FMOCreationVBean) getViewerController().getBean()).getReturnInitBean(),
                    ((FMOCreationVBean) getViewerController().getBean()).getItem());
            WorkShopCriteriaTools.showCriteria(getView(), this, this, getModel().getIdentification(), criteriaDBean);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogCancelled(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void dialogValidated(final DialogChangeEvent event) {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initialize()");
        }

        ((FMOCreationVCtrl) getViewerController()).setFeatureController(this);
        super.initialize();
        setTitle(getModel().getTitle());
        String wsId = (String) getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.LOGICAL_WORKSTATION);
        FMOCreationFIView creationFIView = (FMOCreationFIView) getView();

        creationFIView.setSubTitle(I18nClientManager.translate(ProductionMo.T29585, false) + wsId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initialize()");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> list = new ArrayList<ToolBarBtnStdBaseModel>();
        list.add(getBtnCreateStartedMO());
        list.add(getBtnCriteria());

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return list;
    }

    /**
     * Gets the btnCreateStartedMO.
     * 
     * @category getter
     * @return the btnCreateStartedMO.
     */
    private ToolBarBtnStdModel getBtnCreateStartedMO() {
        if (btnCreateStartedMO == null) {
            btnCreateStartedMO = new ToolBarBtnStdModel();
            btnCreateStartedMO.setIconURL("/images/vif/vif57/production/mo/ok.png");
            btnCreateStartedMO.setReference(EVENT_CREATESTARTEDMO);
            btnCreateStartedMO.setKey(new ToolBarButtonKey(1));
            btnCreateStartedMO.setText(I18nClientManager.translate(Jtech.T12758, false));

        }
        return btnCreateStartedMO;
    }

    /**
     * Gets the btnOpenCriteria.
     * 
     * @category getter
     * @return the btnOpenCriteria.
     */
    private ToolBarBtnStdModel getBtnCriteria() {
        if (btnCriteria == null) {
            btnCriteria = new ToolBarBtnStdModel();
            btnCriteria.setIconURL("/images/vif/vif57/production/mo/criteria.png");
            btnCriteria.setReference(EVENT_OPENCRITERIA);
            btnCriteria.setKey(new ToolBarButtonKey(2));
            btnCriteria.setText(I18nClientManager.translate(GenKernel.T29329, false));
        }
        return btnCriteria;
    }

}

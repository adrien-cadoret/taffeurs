/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: UnclosedWorkingPeriodTableModel.java,v $
 * Created on 3 mai 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.model;


import java.text.SimpleDateFormat;

import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.UnclosedWorkingPeriodBean;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.AbstractEndDayIndicatorTableModel;


/**
 * UnclosedWorkingPeriod table model.
 *
 * @author ac
 */
public class UnclosedWorkingPeriodTableModel extends AbstractEndDayIndicatorTableModel {

    /** column count. */
    private static final int COLUMN_COUNT = 2;

    private String[]         columnNames  = { I18nClientManager.translate(ProductionMo.T34865, false),
            I18nClientManager.translate(ProductionMo.T40757, false) };

    /**
     * Default Ctor.
     */
    public UnclosedWorkingPeriodTableModel() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getColumnCount() {
        return COLUMN_COUNT;
    }

    @Override
    public String getColumnName(final int c) {
        return columnNames[c];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        Object result = new Object();
        if (getEndDayMOList() != null && getEndDayMOList().size() > 0) {
            UnclosedWorkingPeriodBean unclosedWorkingPeriodBean = (UnclosedWorkingPeriodBean) getEndDayMOList().get(
                    rowIndex);
            switch (columnIndex) {
                case 0:
                    result = unclosedWorkingPeriodBean.getLine();
                    break;
                case 1:
                    SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm");
                    result = dateFormat.format(unclosedWorkingPeriodBean.getDatheurDeb());
                    break;
                default:
                    break;
            }

        }
        return result;
    }
}

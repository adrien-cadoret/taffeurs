/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTableLookVCtrl.java,v $
 * Created on 4 Feb, 2014 by kl
 */
package fr.vif.vif5_7.production.mo.ui.features.ftablelook;


import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.FTableLookVBean;
import fr.vif.vif5_7.production.mo.business.services.features.tablelook.FTableLookCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.User;


/**
 * TableLook Viewer Controller.
 * 
 * @author kl
 */
public class FTableLookVCtrl extends AbstractStandardViewerController<FTableLookVBean> {
    /** LOGGER. */
    private static final Logger LOGGER                = Logger.getLogger(FTableLookVCtrl.class);

    private static final String REFERENCE_BTN_SETTING = "setting";
    private FTableLookCBS       ftableLookCBS;

    /**
     * Gets the ftableLookCBS.
     * 
     * @return the ftableLookCBS.
     */
    public FTableLookCBS getFtableLookCBS() {
        return ftableLookCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void initialize() {
        super.initialize();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void renderAndEnableComponents() {
        super.renderAndEnableComponents();
        getViewerView().enableTableLook(getCopyMode());

    }

    /**
     * Sets the ftableLookCBS.
     * 
     * @param ftableLookCBS ftableLookCBS.
     */
    public void setFtableLookCBS(final FTableLookCBS ftableLookCBS) {
        this.ftableLookCBS = ftableLookCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FTableLookVBean convert(final Object bean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convert(bean=" + bean + ")");
        }

        FTableLookVBean vBean = new FTableLookVBean();

        try {
            vBean = getFtableLookCBS().convert(getIdCtx(), (FTableLookBBean) bean);
        } catch (BusinessException e) {
            LOGGER.info("P - BusinessException e=" + e);
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convert(bean=" + bean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void deleteBean(final FTableLookVBean bean) throws UIException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteBean(bean=" + bean + ")");
        }

        try {
            getFtableLookCBS().deleteBean(getIdCtx(), bean);
        } catch (BusinessException e) {
            LOGGER.info("P - BusinessException e=" + e);
            throw new UIException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteBean(bean=" + bean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FTableLookVBean insertBean(final FTableLookVBean bean, final FTableLookVBean initialBean)
            throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertBean(bean=" + bean + ", initialBean=" + initialBean + ")");
        }

        FTableLookVBean vBean = new FTableLookVBean();
        if (initialBean != null) {

            try {
                vBean = getFtableLookCBS().insertBean(getIdCtx(), bean, initialBean);
            } catch (BusinessException e) {
                LOGGER.info("P - BusinessException e=" + e);
                throw new UIException(e);
            } catch (BusinessErrorsException e) {
                LOGGER.info("P - BusinessErrorsException e=" + e);
                throw new UIErrorsException(e);
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertBean(bean=" + bean + ", initialBean=" + initialBean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isButtonEnabled(reference=" + reference + ")");
        }

        boolean isButtonEnabled = false;
        // The setting button is disable if the model is a VifModel ONLY is the user is not a VIF User.
        if (REFERENCE_BTN_SETTING.equals(reference)) {
            isButtonEnabled = isUserVif() || (getBean() != null && !getBean().getBrowserBean().isVifModel());
        } else {
            isButtonEnabled = super.isButtonEnabled(reference);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isButtonEnabled(reference=" + reference + ")=" + isButtonEnabled);
        }
        return isButtonEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionEnabled(final ToolBarButtonType type) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isPanelActionEnabled(type=" + type + ")");
        }

        boolean isPanelActionEnabled = false;
        switch (type) {
            case DELETE:
                isPanelActionEnabled = isUserVif() || (getBean() != null && !getBean().getBrowserBean().isVifModel());
                break;
            case SAVE:
                if (!getCopyMode()) {
                    isPanelActionEnabled = isUserVif()
                            || (getBean() != null && !getBean().getBrowserBean().isVifModel());
                } else {
                    isPanelActionEnabled = getCopyMode();
                }
                break;
            case UNDO:
                if (!getCopyMode()) {
                    isPanelActionEnabled = isUserVif()
                            || (getBean() != null && !getBean().getBrowserBean().isVifModel());
                } else {
                    isPanelActionEnabled = getCopyMode();
                }
                break;

            default:
                isPanelActionEnabled = super.isPanelActionEnabled(type);
                break;
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isPanelActionEnabled(type=" + type + ")=" + isPanelActionEnabled);
        }
        return isPanelActionEnabled;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FTableLookVBean updateBean(final FTableLookVBean bean) throws UIException, UIErrorsException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateBean(bean=" + bean + ")");
        }

        FTableLookVBean vBean = new FTableLookVBean();
        try {
            vBean = getFtableLookCBS().updateBean(getIdCtx(), bean);
        } catch (BusinessException e) {
            LOGGER.info("P - BusinessException e=" + e);
            throw new UIException(e);
        } catch (BusinessErrorsException e) {
            LOGGER.info("P - BusinessErrorsException e=" + e);
            throw new UIErrorsException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateBean(bean=" + bean + ")=" + vBean);
        }
        return vBean;
    }

    /**
     * Return the viewer FTableLookDynamicVView.
     * 
     * @return FTableLookDynamicVView
     */
    private FTableLookDynamicIView getViewerView() {
        return (FTableLookDynamicIView) getView();
    }

    /**
     * 
     * Check if the user is VIF.
     * 
     * @return true if the user connected is VIF.
     */
    private boolean isUserVif() {
        return User.VIF.getValue().equals(getIdCtx().getLogin());
    }

}

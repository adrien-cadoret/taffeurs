/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FProductionOutputContainerFCtrl.java,v $
 * Created on 25 févr. 2009 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fproductionoutputcontainer;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.ui.events.button.ButtonEvent;
import fr.vif.jtech.ui.events.generic.GenericActionEvent;
import fr.vif.jtech.ui.events.sharedcontext.SharedContextEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.models.sharedcontext.Domain;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdBaseModel;
import fr.vif.jtech.ui.toolbar.buttons.btnstd.ToolBarBtnStdModel;
import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.jtech.ui.util.messages.MessageButtons;
import fr.vif.vif57.stock.entity.business.services.constant.EntityConstants;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionTools;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel.TouchModelMapKey;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerCBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.common.fproduction.FProductionConstant;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fdataentrylist.FDataEntryListFCtrl;


/**
 * ProductionOutputContainer : Feature Controller.
 * 
 * @author vr
 */
public class FProductionOutputContainerFCtrl extends AbstractFProductionOutputFCtrl<FProductionOutputContainerCBS> {

    private static final Logger LOGGER = Logger.getLogger(FProductionOutputContainerFCtrl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void actionPerformed(final GenericActionEvent event) {
        super.actionPerformed(event);
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public void buttonAction(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buttonAction(event=" + event + ")");
        }

        super.buttonAction(event);
        if (BTN_DETAIL_REFERENCE.equals(event.getModel().getReference())) {
            openChildFeature(FDETAIL_ENTRY_CODE);
        } else if (BTN_FPRODUCTION_INPUT_REFERENCE.equals(event.getModel().getReference())) {
            // If one the item of the activity has a Nature Complementary Workshop.
            // We force the redirection to an other screen (VIF.MOINSCRAPDE1T), to manage the compl. nature.
            if (getSharedContext().get(Domain.DOMAIN_PARENT, FProductionConstant.MO_LIST_VBEAN) != null) {
                FMOListVBean vBean = (FMOListVBean) getSharedContext().get(Domain.DOMAIN_PARENT,
                        FProductionConstant.MO_LIST_VBEAN);
                String inputFunction = "";
                if (vBean.isStatementScrap(ActivityItemType.INPUT)) {
                    inputFunction = "VIF.MOINSCRAPDE1T"; // comp input
                } else {
                    inputFunction = "VIF.MOINCODE1T"; // classical input
                }
                getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.FEATURE_ID, inputFunction);
                fireSharedContextSend(getSharedContext());
                fireSelfFeatureClosingRequired();
            }

        } else if (FProductionOutputContainerFCtrl.BTN_SHOW_ALL_REFERENCE.equals(event.getModel().getReference())) {
            processShowAllEvent(event);
        } else if (BTN_DETAIL_REFERENCE.equals(event.getModel().getReference())) {
            getViewerCtrl().setPrevious(null); // Maybe delete done in movement list
            ((FProductionOutputContainerVCtrl) getViewerCtrl()).getMap().remove(
                    FProductionTools.operationItemKeyToMovementAct(getViewerBean().getBBean().getOperationItemBean()
                            .getOperationItemKey()));
        } else if (BTN_CLOSE_CONTAINER.equals(event.getModel().getReference())) {
            try {
                if (!getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().getContainerNumber()
                        .isEmpty()
                        && !getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .getContainerNumber().equals(EntityConstants.NEW)) {
                    List<PrintDocument> lst = getFproductionOutputContainerCBS().closePrintContainer(getIdCtx(),
                            getViewerBean(), getViewerCtrl().getWorkBean());
                    if (lst != null && lst.size() > 0) {
                        getViewerCtrl().askNumDocument(lst);

                        getFproductionOutputContainerCBS().printContainer(getIdCtx(), getViewerBean(), lst,
                                getViewerCtrl().getWorkBean());
                    }
                    // QA8912 MP : reset location only if depot not optimize and the activity has not location
                    if (!getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCemp().isEmpty()
                            && (getViewerBean().getHasDepositWithLocationOptimise() || getViewerBean().getBBean()
                                    .getOperationItemBean().getLocationCL().getCode().isEmpty())) {
                        getViewerBean().getEntity().getEntityLocationBean().getEnableFields().setLocation(true);
                        getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().setCemp("");
                    }
                    // ALB Q006688 After closing a container, if there is at least one container opened for the act,
                    // this container become the current container.
                    List<String> listContainers = getFproductionOutputContainerCBS().getOpenenedContainers(getIdCtx(),
                            getViewerBean().getBBean().getOperationItemBean().getOperationItemKey());
                    if (listContainers == null || listContainers.size() == 0) {
                        getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .setContainerNumber(EntityConstants.NEW);
                        getViewerBean().getEntity().getEntityLocationBean().getContainerPackaging().setTattooedId("");
                    } else {
                        getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .setContainerNumber(listContainers.get(0));
                    }
                } else if (!getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                        .getUpperContainerNumber().isEmpty()
                        && !getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                                .getUpperContainerNumber().equals(EntityConstants.NEW)) {
                    List<PrintDocument> lst = getFproductionOutputContainerCBS().closePrintUpperContainer(getIdCtx(),
                            getViewerBean(), getViewerCtrl().getWorkBean());
                    if (lst != null && lst.size() > 0) {
                        getViewerCtrl().askNumDocument(lst);
                        getFproductionOutputContainerCBS().printUpperContainer(getIdCtx(), getViewerBean(), lst,
                                getViewerCtrl().getWorkBean());
                    }
                    // QA8912 MP : reset location only if depot not optimize and the activity has not location
                    if (!getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().getCemp().isEmpty()
                            && (getViewerBean().getHasDepositWithLocationOptimise() || getViewerBean().getBBean()
                                    .getOperationItemBean().getLocationCL().getCode().isEmpty())) {
                        getViewerBean().getEntity().getEntityLocationBean().getEnableFields().setLocation(true);
                        getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey().setCemp("");
                    }
                    getViewerBean().getEntity().getEntityLocationBean().getEntityLocationKey()
                            .setUpperContainerNumber(EntityConstants.NEW);
                    getViewerBean().getEntity().getEntityLocationBean().getUpperPackaging().setTattooedId("");
                }

                getViewerCtrl().getModel().setInitialBean(getViewerBean());
                getViewerCtrl().renderAndEnableComponents();
                getViewerCtrl().saveCurrent();
                getViewerCtrl().fireButtonStatechanged();
            } catch (BusinessException e) {
                getView().show(new UIException(e));
            }
        } else if (BTN_CANCELLAST_REFERENCE.equals(event.getModel().getReference())) {
            if (getViewerCtrl().getPrevious() != null) {
                int ret = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                        I18nClientManager.translate(ProductionMo.T29578, false), MessageButtons.YES_NO,
                        MessageButtons.DEFAULT_NO);
                if (ret == MessageButtons.YES) {
                    try {

                        OperationItemKey oik = new OperationItemKey();
                        oik.setEstablishmentKey(getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey());
                        oik.setChrono(getViewerCtrl().getWorkBean().getMoKey().getChrono());
                        oik.setCounter1(getViewerCtrl().getPrevious().getNumber1());
                        oik.setCounter2(getViewerCtrl().getPrevious().getNumber2());
                        oik.setCounter3(getViewerCtrl().getPrevious().getNumber3());
                        oik.setCounter4(getViewerCtrl().getPrevious().getNumber4());
                        oik.setManagementType(ManagementType.REAL);
                        getFproductionOutputContainerCBS().deleteLast(getIdCtx(),
                                getViewerCtrl().getWorkBean().getMoKey().getEstablishmentKey(),
                                getViewerCtrl().getPrevious());
                        getViewerCtrl().getPrevious().setLineNumber(0);
                        ((FProductionOutputContainerVCtrl) getViewerCtrl()).getMap().remove(
                                getViewerCtrl().getPrevious());

                        getBrowserCtrl().reopenQuery();

                        getBrowserCtrl().repositionToOperationItemKey(oik);
                        getViewerCtrl().setPrevious(null);
                        getViewerCtrl().fireButtonStatechanged();

                    } catch (BusinessException e) {
                        getView().show(new UIException(e));
                    } catch (BusinessErrorsException e) {
                        getViewerCtrl().fireErrorsChanged(new UIErrorsException(e));
                    }
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buttonAction(event=" + event + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void sharedContextSend(final SharedContextEvent event) {
        super.sharedContextSend(event);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected List<ToolBarBtnStdBaseModel> getActionsButtonModels() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActionsButtonModels()");
        }

        List<ToolBarBtnStdBaseModel> lst = new ArrayList<ToolBarBtnStdBaseModel>();
        lst.addAll(super.manageActionsButtonModels(TouchModelMapKey.CONTAINER_OUTPUT_PRODUCTION));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActionsButtonModels()");
        }
        return lst;
    }

    /**
     * Manage button.
     * 
     * @param event to get button to manage it.
     */
    protected void manageBtnShowAllModelText(final ButtonEvent event) {

        FProductionSBean sBean = (FProductionSBean) getBrowserController().getInitialSelection();
        ToolBarBtnStdModel buttonStd = (ToolBarBtnStdModel) event.getModel();
        if (!sBean.getAll()) {
            buttonStd.setText(I18nClientManager.translate(ProductionMo.T29545, false));
            buttonStd.setIconURL("/images/vif/vif57/production/mo/showfinished.png");
        } else {
            buttonStd.setText(I18nClientManager.translate(ProductionMo.T29525, false));
            buttonStd.setIconURL("/images/vif/vif57/production/mo/hidefinished.png");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void openChildFeature(final String featureId) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - openChildFeature(featureId=" + featureId + ")");
        }

        if (featureId.equals("VIF.MOOULI1T")) {
            getSharedContext().put(Domain.DOMAIN_THIS, FProductionConstant.FEATURE_TITLE, getModel().getTitle());
            getSharedContext().put(Domain.DOMAIN_THIS, FDataEntryListFCtrl.OPERATION_ITEM_BEAN,
                    getViewerBean().getBBean().getOperationItemBean());
            getSharedContext().put(Domain.DOMAIN_THIS, "outputEntityType",
                    getViewerBean().getOutputItemParameters().getEntityType());
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.TOUCH_MODEL,
                    getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.TOUCH_MODEL));
            getSharedContext().put(Domain.DOMAIN_THIS, TouchModel.USE_TOUCH_MODEL,
                    getSharedContext().get(Domain.DOMAIN_PARENT, TouchModel.USE_TOUCH_MODEL));
            fireSharedContextSend(getSharedContext());
            setCurrent(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey());
        }
        super.openChildFeature(featureId);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - openChildFeature(featureId=" + featureId + ")");
        }
    }

    /**
     * Finishes or unfinishes an item.
     */
    @Override
    protected void processFinishResume() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processFinishResume()");
        }

        int result = 0;
        if (getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                .equals(ManagementType.REAL)) {
            result = getView().showQuestion(I18nClientManager.translate(Jtech.T25073, false),
                    I18nClientManager.translate(ProductionMo.T29534, false), MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {
                try {
                    String info = getFproductionOutputContainerCBS().finishOperationItem(getIdCtx(),
                            getViewerBean().getBBean().getOperationItemBean().getOperationItemKey(),
                            getIdCtx().getWorkstation(), ActivityItemType.OUTPUT);
                    if (info != null && !"".equals(info)) {
                        getView().showInformation(info);
                    }
                    ((FProductionOutputContainerVCtrl) getViewerCtrl()).getMap().remove(
                            FProductionTools.operationItemKeyToMovementAct(getViewerBean().getBBean()
                                    .getOperationItemBean().getOperationItemKey()));
                    setCurrent(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey());
                    getCurrent().setManagementType(ManagementType.ARCHIVED);
                    getBrowserController().reopenQuery();
                    if (getCurrent() != null) {
                        getBrowserCtrl().repositionToOperationItemKey(getCurrent());
                    }
                } catch (BusinessException e) {
                    getViewerCtrl().fireErrorsChanged(new UIException(e));
                }
            }
        } else if (getViewerBean().getBBean().getOperationItemBean().getOperationItemKey().getManagementType()
                .equals(ManagementType.ARCHIVED)) {
            result = getView().showQuestion("Question", I18nClientManager.translate(ProductionMo.T29535, false),
                    MessageButtons.YES_NO);
            if (result == MessageButtons.YES) {
                try {
                    getFproductionOutputContainerCBS().resumeOperationItem(getIdCtx(),
                            getViewerBean().getBBean().getOperationItemBean().getOperationItemKey(),
                            getIdCtx().getWorkstation());
                    setCurrent(getViewerBean().getBBean().getOperationItemBean().getOperationItemKey());
                    getCurrent().setManagementType(ManagementType.REAL);
                    getBrowserController().reopenQuery();
                    if (getCurrent() != null) {
                        getBrowserCtrl().repositionToOperationItemKey(getCurrent());
                    }
                } catch (BusinessException e) {
                    getViewerCtrl().fireErrorsChanged(new UIException(e));
                }
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processFinishResume()");
        }
    }

    /**
     * Process the 'showall' event.
     * 
     * @param event to get button to manage it.
     */
    private void processShowAllEvent(final ButtonEvent event) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - processShowAllEvent()");
        }

        FProductionSBean sBean = (FProductionSBean) getBrowserController().getInitialSelection();
        sBean.setAll(!sBean.getAll());
        manageBtnShowAllModelText(event);
        if (getBrowserCtrl().getModel().getCurrentBean() != null) {
            setCurrent(getBrowserCtrl().getModel().getCurrentBean().getOperationItemBean().getOperationItemKey());
        }
        getBrowserController().reopenQuery();
        if (getCurrent() != null) {
            getBrowserCtrl().repositionToOperationItemKey(getCurrent());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - processShowAllEvent()");
        }
    }
}

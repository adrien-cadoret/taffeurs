/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FContentRateFView.java,v $
 * Created on 15 juil. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent.touch;


import javax.swing.ImageIcon;

import fr.vif.jtech.ui.button.touch.TouchBtnStd;
import fr.vif.jtech.ui.feature.touch.StandardTouchFeature;
import fr.vif.vif5_7.production.mo.ui.features.dataentry.item.fcontent.FContentRateFCtrl;


/**
 * Feautre view for content.
 * 
 * @author cj
 */
public class FContentRateFView extends StandardTouchFeature {
    private FContentRateGSView fcontentRateGSView = null;
    private FContentRateBView  fcontentRateBView  = null;
    private TouchBtnStd        btnUp;
    private TouchBtnStd        btnDown;

    /**
     * Default constructor.
     * 
     */
    public FContentRateFView() {
        super();
        initialize();
    }

    /**
     * Gets the btnDown.
     * 
     * @return the btnDown.
     */
    public TouchBtnStd getBtnDown() {
        if (this.btnDown == null) {
            this.btnDown = new TouchBtnStd();
            this.btnDown.setReference(FContentRateFCtrl.BTN_DOWN);
            this.btnDown
                    .setIcon(new ImageIcon(this.getClass().getResource("/images/vif/vif57/production/mo/down.png")));
            this.btnDown.setBounds(940, 390, 70, 70);
        }
        return btnDown;
    }

    /**
     * Gets the btnUp.
     * 
     * @return the btnUp.
     */
    public TouchBtnStd getBtnUp() {
        if (this.btnUp == null) {
            this.btnUp = new TouchBtnStd();
            this.btnUp.setReference(FContentRateFCtrl.BTN_UP);
            this.btnUp.setIcon(new ImageIcon(this.getClass().getResource("/images/vif/vif57/production/mo/up.png")));
            this.btnUp.setBounds(940, 300, 70, 70);
        }
        return btnUp;
    }

    /**
     * Gets the fcontentRateBView.
     * 
     * @return the fcontentRateBView.
     */
    public FContentRateBView getFcontentRateBView() {
        if (this.fcontentRateBView == null) {
            this.fcontentRateBView = new FContentRateBView();
            this.fcontentRateBView.setBounds(2, 202, 930, 390);
        }
        return fcontentRateBView;
    }

    /**
     * Gets the fcontentRateGSView.
     * 
     * @return the fcontentRateGSView.
     */
    public FContentRateGSView getFcontentRateGSView() {
        if (this.fcontentRateGSView == null) {
            this.fcontentRateGSView = new FContentRateGSView();
            this.fcontentRateGSView.setBounds(2, 2, 1000, 190);
        }
        return this.fcontentRateGSView;
    }

    /**
     * Sets the fcontentRateBView.
     * 
     * @param fcontentRateBView fcontentRateBView.
     */
    public void setFcontentRateBView(final FContentRateBView fcontentRateBView) {
        this.fcontentRateBView = fcontentRateBView;
    }

    /**
     * Sets the fcontentRateGSView.
     * 
     * @param fcontentRateGSView fcontentRateGSView.
     */
    public void setFcontentRateGSView(final FContentRateGSView fcontentRateGSView) {
        this.fcontentRateGSView = fcontentRateGSView;
    }

    /**
     * This Method Initializes this.
     * 
     */
    protected void initialize() {
        this.setLayout(null);
        this.add(getFcontentRateGSView());
        this.add(getFcontentRateBView());
        this.add(getBtnUp());
        this.add(getBtnDown());
    }
}

/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: MOUIConstants.java,v $
 * Created on 9 nov. 2011 by cj
 */
package fr.vif.vif5_7.production.mo.ui.constants;


import java.awt.Color;
import java.awt.GradientPaint;

import javax.swing.UIManager;

import fr.vif.jtech.ui.laf.swing.LAFHelper;
import fr.vif.jtech.ui.models.format.CustomColor;


/**
 * Constants for MO UI.
 * 
 * @author cj
 */
public final class MOUIConstants {

    public static final Color         CHART_COLOR1_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR1_BEGIN);
    public static final Color         CHART_COLOR1_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR1_BORDER);
    public static final Color         CHART_COLOR1_END                    = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR1_END);
    public static final Color         CHART_COLOR10_BEGIN                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR10_BEGIN);
    public static final Color         CHART_COLOR10_BORDER                = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR10_BORDER);
    public static final Color         CHART_COLOR10_END                   = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR10_END);
    public static final Color         CHART_COLOR2_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR2_BEGIN);
    public static final Color         CHART_COLOR2_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR2_BORDER);
    public static final Color         CHART_COLOR2_END                    = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR2_END);
    public static final Color         CHART_COLOR3_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR3_BEGIN);
    public static final Color         CHART_COLOR3_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR3_BORDER);
    public static final Color         CHART_COLOR3_END                    = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR3_END);

    public static final Color         CHART_COLOR4_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR4_BEGIN);
    public static final Color         CHART_COLOR4_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR4_BORDER);
    public static final Color         CHART_COLOR4_END                    = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR4_END);

    public static final Color         CHART_COLOR5_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR5_BEGIN);
    public static final Color         CHART_COLOR5_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR5_BORDER);
    public static final Color         CHART_COLOR5_END                    = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR5_END);

    public static final Color         CHART_COLOR6_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR6_BEGIN);
    public static final Color         CHART_COLOR6_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR6_BORDER);
    public static final Color         CHART_COLOR6_END                    = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR6_END);

    public static final Color         CHART_COLOR7_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR7_BEGIN);
    public static final Color         CHART_COLOR7_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR7_BORDER);
    public static final Color         CHART_COLOR7_END                    = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR7_END);

    public static final Color         CHART_COLOR8_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR8_BEGIN);
    public static final Color         CHART_COLOR8_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR8_BORDER);
    public static final Color         CHART_COLOR8_END                    = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR8_END);

    public static final Color         CHART_COLOR9_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR9_BEGIN);
    public static final Color         CHART_COLOR9_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR9_BORDER);
    public static final Color         CHART_COLOR9_END                    = UIManager
            .getColor(LAFHelper.LAF_CHART_COLOR9_END);

    public static final Color         RED_GREEN_1_BEGIN                   = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_1_BEGIN);
    public static final Color         RED_GREEN_1_BORDER                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_1_BORDER);
    public static final Color         RED_GREEN_1_END                     = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_1_END);

    public static final Color         RED_GREEN_10_BEGIN                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_10_BEGIN);
    public static final Color         RED_GREEN_10_BORDER                 = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_10_BORDER);
    public static final Color         RED_GREEN_10_END                    = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_10_END);

    public static final Color         RED_GREEN_2_BEGIN                   = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_2_BEGIN);
    public static final Color         RED_GREEN_2_BORDER                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_2_BORDER);
    public static final Color         RED_GREEN_2_END                     = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_2_END);

    public static final Color         RED_GREEN_3_BEGIN                   = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_3_BEGIN);
    public static final Color         RED_GREEN_3_BORDER                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_3_BORDER);
    public static final Color         RED_GREEN_3_END                     = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_3_END);

    public static final Color         RED_GREEN_4_BEGIN                   = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_4_BEGIN);
    public static final Color         RED_GREEN_4_BORDER                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_4_BORDER);
    public static final Color         RED_GREEN_4_END                     = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_4_END);

    public static final Color         RED_GREEN_5_BEGIN                   = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_5_BEGIN);
    public static final Color         RED_GREEN_5_BORDER                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_5_BORDER);
    public static final Color         RED_GREEN_5_END                     = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_5_END);

    public static final Color         RED_GREEN_6_BEGIN                   = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_6_BEGIN);
    public static final Color         RED_GREEN_6_BORDER                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_6_BORDER);
    public static final Color         RED_GREEN_6_END                     = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_6_END);

    public static final Color         RED_GREEN_7_BEGIN                   = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_7_BEGIN);
    public static final Color         RED_GREEN_7_BORDER                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_7_BORDER);
    public static final Color         RED_GREEN_7_END                     = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_7_END);

    public static final Color         RED_GREEN_8_BEGIN                   = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_8_BEGIN);
    public static final Color         RED_GREEN_8_BORDER                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_8_BORDER);
    public static final Color         RED_GREEN_8_END                     = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_8_END);

    public static final Color         RED_GREEN_9_BEGIN                   = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_9_BEGIN);
    public static final Color         RED_GREEN_9_BORDER                  = UIManager
            .getColor(LAFHelper.LAF_RED_GREEN_9_BORDER);
    public static final Color         RED_GREEN_9_END                     = new Color(114, 185, 16);

    // GradiantPaint
    public static final GradientPaint CHART_COLOR10                       = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR10_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR10_END);
    public static final GradientPaint CHART_COLOR1                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR1_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR1_END);
    public static final GradientPaint CHART_COLOR2                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR2_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR2_END);
    public static final GradientPaint CHART_COLOR3                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR3_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR3_END);
    public static final GradientPaint CHART_COLOR4                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR4_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR4_END);

    public static final GradientPaint CHART_COLOR5                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR5_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR5_END);

    public static final GradientPaint CHART_COLOR6                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR6_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR6_END);
    public static final GradientPaint CHART_COLOR7                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR7_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR7_END);
    public static final GradientPaint CHART_COLOR8                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR8_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR8_END);
    public static final GradientPaint CHART_COLOR9                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.CHART_COLOR9_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.CHART_COLOR9_END);

    public static final GradientPaint RED_GREEN_10                        = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_10_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_10_END);
    public static final GradientPaint RED_GREEN_1                         = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_1_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_1_END);
    public static final GradientPaint RED_GREEN_2                         = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_2_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_2_END);
    public static final GradientPaint RED_GREEN_3                         = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_3_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_3_END);
    public static final GradientPaint RED_GREEN_4                         = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_4_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_4_END);
    public static final GradientPaint RED_GREEN_5                         = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_5_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_5_END);
    public static final GradientPaint RED_GREEN_6                         = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_6_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_6_END);
    public static final GradientPaint RED_GREEN_7                         = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_7_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_7_END);
    public static final GradientPaint RED_GREEN_8                         = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_8_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_8_END);
    public static final GradientPaint RED_GREEN_9                         = new GradientPaint(0.0f, 0.0f,
            MOUIConstants.RED_GREEN_9_BEGIN,
            0.0f, 0.0f,
            MOUIConstants.RED_GREEN_9_END);

    public static final CustomColor   TOUCHSCREEN_BG_BROWSER              = new CustomColor(255, 255, 255);
    public static final CustomColor   TOUCHSCREEN_BG_PANEL_DARK           = new CustomColor(209, 209, 209);
    public static final CustomColor   TOUCHSCREEN_BG_PANEL_LIGHT          = new CustomColor(209, 209, 209);
    public static final CustomColor   TOUCHSCREEN_BG_SELECTED_BROWSE_LINE = new CustomColor(255, 131, 0);
    public static final CustomColor   TOUCHSCREEN_FG_SELECTED_BROWSE_LINE = new CustomColor(255, 255, 255);

    /**
     * Constructor.
     */
    private MOUIConstants() {

    }
}

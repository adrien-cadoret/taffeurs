/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: InvoicedArticleTableModel.java,v $
 * Created on 3 mai 2016 by ac
 */
package fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.model;


import javax.swing.ImageIcon;

import fr.vif.jtech.ui.util.i18n.I18nClientManager;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.InvoicedArticleBean;
import fr.vif.vif5_7.production.mo.ui.widgets.indicatormo.endday.AbstractEndDayIndicatorTableModel;


/**
 * InvoicedArticleTable model.
 *
 * @author ac
 */
public class InvoicedArticleTableModel extends AbstractEndDayIndicatorTableModel {

    /** column count. */
    private static final int COLUMN_COUNT = 6;

    private String[]         columnNames  = { I18nClientManager.translate(Generic.T3340, false),
            I18nClientManager.translate(ProductionMo.T34865, false),
            I18nClientManager.translate(ProductionMo.T39797, false),
            I18nClientManager.translate(GenKernel.T12097, false),
            I18nClientManager.translate(ProductionMo.T29519, false),
            I18nClientManager.translate(ProductionMo.T40687, false) };

    /**
     * Default Constructor.
     */
    public InvoicedArticleTableModel() {
        super();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Class<?> getColumnClass(final int column) {

        Class columnClass = null;

        // CHECKSTYLE:OFF
        if (column == 5) {
            // CHECKSTYLE:ON
            columnClass = ImageIcon.class;
        } else {
            columnClass = Object.class;
        }

        return columnClass;

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getColumnCount() {
        return COLUMN_COUNT;
    }

    @Override
    public String getColumnName(final int c) {
        return columnNames[c];
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Object getValueAt(final int rowIndex, final int columnIndex) {
        Object result = new Object();
        if (getEndDayMOList() != null && getEndDayMOList().size() > 0) {
            InvoicedArticleBean articleBean = (InvoicedArticleBean) getEndDayMOList().get(rowIndex);
            // CHECKSTYLE:OFF
            switch (columnIndex) {
                case 0:
                    result = articleBean.getClPoste().getCode();
                    break;
                case 1:
                    result = articleBean.getLine();
                    break;
                case 2:
                    result = articleBean.getChrono();
                    break;
                case 3:
                    switch (articleBean.getTypacta()) {
                        case "ENT":
                            result = I18nClientManager.translate(ProductionMo.T32536, false);
                            break;
                        case "SOR":
                            result = I18nClientManager.translate(ProductionMo.T40729, false);
                            break;
                        default:
                            break;
                    }
                    break;
                case 4:
                    result = articleBean.getClArt().getCode();
                    break;
                case 5:
                    if (0 != articleBean.getQtartfait1()) {
                        result = new ImageIcon(this.getClass().getResource("/images/widgets/autodataentry_16x16.png"));
                    }
                    break;
                default:
                    break;
            }
            // CHECKSTYLE:ON

        }
        return result;
    }
}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FTimeBreakDownSView.java,v $
 * Created on 07 Nov 2014 by cj
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime.fbreakdown.swing;


import java.awt.Dialog;
import java.awt.Frame;

import fr.vif.jtech.ui.dialogs.selection.swing.StandardSwingSelection;


/**
 * TimeBreakDown : Selection View.
 * 
 * @author cj
 * @version $Revision: 1.3 $, $Date: 2016/05/26 09:41:19 $
 */
public class FTimeBreakDownSView extends StandardSwingSelection {

    /**
     * Constructs a new FTimeBreakDownSView.
     * 
     * @param view owner's view.
     */
    public FTimeBreakDownSView(final Dialog view) {
        super(view);
        initialize();
    }

    /**
     * Constructs a new FTimeBreakDownSView.
     * 
     * @param view owner's view.
     */
    public FTimeBreakDownSView(final Frame view) {
        super(view);
        initialize();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void enableComponents() {
        super.enableComponents();
        // boolean all = icbAll.isSelected();
        // itfCSociete.setenabled(!all);
    }

    /**
     * Initialize selection dialog-box size and set the main panel.
     * 
     */
    private void initialize() {
        // this.setSize(new java.awt.Dimension(width, height));
        // super.addPanel(...);
    }

}

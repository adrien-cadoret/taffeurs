/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffVModel.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import fr.vif.jtech.ui.viewer.ViewerModel;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;


/**
 * Labor time viewer model.
 * 
 * @author nle
 */
public class FLaborStaffVModel extends ViewerModel<FLaborStaffBBean> {
    /**
     * Default constructor.
     * 
     */
    public FLaborStaffVModel() {
        super();
        setBeanClass(FLaborStaffBBean.class);
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborStaffV1NModel.java,v $
 * Created on 14 janv. 2014 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import java.util.List;

import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.input.InputSpecialViewer1NModel;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;


/**
 * Viewer 1-N model.
 * 
 * 
 * @author nle
 */
public class FLaborStaffV1NModel extends InputSpecialViewer1NModel<FLaborStaffBBean> {

    /**
     * Viewer 1-N model.
     * 
     * @param browserColumns the list of columns.
     * @param detailBeanClass the bean.
     */
    public FLaborStaffV1NModel(final List<BrowserColumn> browserColumns, final Class<FLaborStaffBBean> detailBeanClass) {
        super(browserColumns, detailBeanClass);

    }
}

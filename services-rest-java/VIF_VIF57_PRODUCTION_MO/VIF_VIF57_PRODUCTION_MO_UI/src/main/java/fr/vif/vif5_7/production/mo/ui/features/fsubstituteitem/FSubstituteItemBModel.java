/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FSubstituteItemBModel.java,v $
 * Created on 19 nov. 2008 by VR
 */
package fr.vif.vif5_7.production.mo.ui.features.fsubstituteitem;


import fr.vif.jtech.ui.browser.BrowserColumn;
import fr.vif.jtech.ui.browser.BrowserModel;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fsubstituteitem.FSubstituteItemBBean;


/**
 * SubstituteItem : Browser Model.
 * 
 * @author vr
 */
public class FSubstituteItemBModel extends BrowserModel<FSubstituteItemBBean> {

    /**
     * Default constructor.
     * 
     */
    public FSubstituteItemBModel() {
        super();
        setBeanClass(FSubstituteItemBBean.class);
        addColumn(new BrowserColumn("", "code", 0));
        addColumn(new BrowserColumn("", "label", 0));
        setFullyFetched(true);
        setFetchSize(Integer.MAX_VALUE);
        setFirstFetchSize(Integer.MAX_VALUE);
    }

}

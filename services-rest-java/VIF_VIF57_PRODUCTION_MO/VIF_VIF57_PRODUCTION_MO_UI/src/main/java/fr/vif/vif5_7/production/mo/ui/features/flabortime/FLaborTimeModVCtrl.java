/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_UI
 * File : $RCSfile: FLaborTimeModVCtrl.java,v $
 * Created on 17 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.ui.features.flabortime;


import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.ui.events.table.TableRowChangeEvent;
import fr.vif.jtech.ui.exceptions.UIErrorsException;
import fr.vif.jtech.ui.exceptions.UIException;
import fr.vif.jtech.ui.toolbar.buttons.ToolBarButtonType;
import fr.vif.jtech.ui.viewer.AbstractStandardViewerController;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeModBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeModVBean;
import fr.vif.vif5_7.production.mo.business.services.features.flabortime.FLaborTimeCBS;


/**
 * Labor time modification viewer controller.
 * 
 * @author nle
 */
public class FLaborTimeModVCtrl extends AbstractStandardViewerController<FLaborTimeModVBean> {

    public static final String VALID_TIME    = "validTimeBtn";

    private FLaborTimeCBS      flabortimeCBS = null;

    /**
     * Performs the update of the bean.
     * 
     * @param vBean the labor time vBean.
     */
    public void beanUpdated(final FLaborTimeModVBean vBean) {
        // performs the update of the Bean
        performSave();
    }

    /**
     * Gets the flabortimeCBS.
     * 
     * @return the flabortimeCBS.
     */
    public FLaborTimeCBS getFlabortimeCBS() {
        return flabortimeCBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void selectedRowChanged(final TableRowChangeEvent event) {
        super.selectedRowChanged(event);
    }

    /**
     * Sets the flabortimeCBS.
     * 
     * @param flabortimeCBS flabortimeCBS.
     */
    public void setFlabortimeCBS(final FLaborTimeCBS flabortimeCBS) {
        this.flabortimeCBS = flabortimeCBS;
    }

    /**
     * Show an error in viewer error panel.
     * 
     * @param e the exception
     */
    public void showError(final UIErrorsException e) {
        fireErrorsChanged(e);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Override
    public void validateBean(final boolean creationMode, final String beanProperty, final Object propertyValue)
            throws UIException {
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected FLaborTimeModVBean convert(final Object bean) throws UIException {

        FLaborTimeModBBean bBean = (FLaborTimeModBBean) bean;
        FLaborTimeModVBean vBean = new FLaborTimeModVBean();

        try {
            vBean = getFlabortimeCBS().getFLaborTimeModVBean(getIdCtx(), bBean);
        } catch (BusinessException e) {
            throw new UIException(e);
        }

        return vBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isButtonEnabled(final String reference) {
        Boolean ret = false;
        if (VALID_TIME.equals(reference) || //
                "currentBegDateNextBtn".equals(reference) || //
                "currentBegDatePrevBtn".equals(reference) || //
                "currentEndDateNextBtn".equals(reference) || //
                "currentEndDatePrevBtn".equals(reference)) {
            ret = true;
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected boolean isPanelActionVisible(final ToolBarButtonType type) {
        // hide the button bar at the bottom of the screen
        return false;
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PREPARATION_STANDALONE_SBS
 * File : $RCSfile: ProductionMOJMockUITestSuite.java,v $
 * Created on 8 Oct 2008 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph;


import junit.framework.TestCase;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;


/**
 * 
 * Launch JMock test suite.
 * 
 * @author jd
 */
@RunWith(Suite.class)
@SuiteClasses(value = { JMockIndicatorUITest.class })
public class ProductionMOJMockUITestSuite extends TestCase {

}

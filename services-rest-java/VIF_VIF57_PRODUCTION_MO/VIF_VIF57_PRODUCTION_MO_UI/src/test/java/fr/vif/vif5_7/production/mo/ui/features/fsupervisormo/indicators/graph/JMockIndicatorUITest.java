/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: JMockIndicatorUITest.java,v $
 * Created on 9 févr. 2012 by jd
 */
package fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.graph;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import junit.framework.Assert;

import org.apache.log4j.Logger;
import org.jfree.chart.ChartPanel;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.VIFContextMockManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.jmock.JMockContext;
import fr.vif.jtech.ui.laf.swing.AbstractLAFConfigurator;
import fr.vif.jtech.ui.laf.swing.LightLAF;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FlowType;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.CumulatedIndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOQties;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FGanttMOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.fsupervisormo.FSupervisorMOGraphSelectionBean;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.GraphTypeEnum;
import fr.vif.vif5_7.production.mo.constants.IndicatorMOEnum.IndicatorAxesEnum;
import fr.vif.vif5_7.production.mo.constants.Mnemos.POIndicatorState;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMO;
import fr.vif.vif5_7.production.mo.ui.features.fsupervisormo.indicators.common.GraphMOOptions;


/**
 * The JMock indicator UI test class.
 * 
 * @author jd
 */
@RunWith(JMock.class)
public class JMockIndicatorUITest {
    /**
     * Main JMock context.
     */
    protected static Mockery           context;
    protected static IdContext         idContext;

    private static final Logger        LOGGER = Logger.getLogger(JMockIndicatorUITest.class);

    private List<CumulatedIndicatorMO> cums   = new ArrayList<CumulatedIndicatorMO>();
    private ArrayList<IndicatorMO>     indicators;

    /**
     * 
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        context = JMockContext.getNewContext();

        idContext = VIFContextMockManager.getVIFContext().getIdCtx();

        idContext.setCompany("TG");
        idContext.setEstablishment("04");
        idContext.setWorkstation("P22");

        LightLAF lightLAF = new LightLAF();
        ((AbstractLAFConfigurator) lightLAF).setAsDefaultLookAndFeel();

        reloadDataTests();
    }

    @Test
    public void test_GetGantt() {

        FSupervisorMOGraphSelectionBean sel = new FSupervisorMOGraphSelectionBean();

        sel.setEstablishmentKey(new EstablishmentKey("TG", "04"));
        sel.setPoDate(new Date());
        sel.setAxe(IndicatorAxesEnum.SECTOR);
        sel.setReverseSetAndCategory(false);
        sel.setGraphType(GraphTypeEnum.BAR_GANTT_XY);
        FGanttMOBean vbean = new FGanttMOBean();

        vbean.setAxe(IndicatorAxesEnum.SECTOR);
        vbean.setEstablishmentKey(new EstablishmentKey("TG", "04"));
        vbean.setTitle("TOTO");
        vbean.setIndicators(indicators);

        GraphMO graph = GanttMOTools.convertToGraph(vbean, new GraphMOOptions());
        Assert.assertNotNull(graph);
        Assert.assertEquals(20, graph.getGraphData().size());

        ChartPanel panel = GraphMOTools.createGraphPanel(graph, new GraphMOOptions());

        Assert.assertNotNull(panel);

    }

    @Test
    public void test_GetGraph() {

        // FSupervisorMOGraphSelectionBean sel = new FSupervisorMOGraphSelectionBean();
        //
        // sel.setEstablishmentKey(new EstablishmentKey("TG", "04"));
        // sel.setPoDate(new Date());
        // sel.setAxe(IndicatorAxesEnum.SECTOR);
        // sel.setReverseSetAndCategory(false);
        // sel.setGraphType(GraphTypeEnum.BAR_STACKED);
        // FGraphMOBean vbean = new FGraphMOBean();
        //
        // vbean.setAxe(IndicatorAxesEnum.SECTOR);
        // vbean.setEstablishmentKey(new EstablishmentKey("TG", "04"));
        // vbean.setTitle("TOTO");
        // vbean.setCums(cums);
        //
        // GraphMO graph = GraphMOTools.convertFGraphToGraph(vbean, sel);
        // Assert.assertNotNull(graph);
        // Assert.assertEquals(9, graph.getGraphData().size());
        //
        // ChartPanel panel = GraphMOTools.createGraphPanel(graph, new GraphMOOptions());
        //
        // Assert.assertNotNull(panel);

    }

    /**
     * Get the VIFContext.
     * 
     * @return
     */
    private VIFContext getCtx() {
        return VIFContextMockManager.getVIFContext();
    }

    /**
     * Reload data.
     */
    private void reloadDataTests() {

        cums = new ArrayList<CumulatedIndicatorMO>();
        Date d = DateHelper.getDateTime(new Date(), 0);

        Date d2 = DateHelper.getDateTime(new Date(), 300); // 5 minutes

        Date d3 = DateHelper.getDateTime(new Date(), 400);

        for (int i = 0; i < 3; i++) {

            CumulatedIndicatorMO ind = new CumulatedIndicatorMO();
            ind.getKey().getAxeValueCL().setCode("" + i);
            ind.getKey().getAxeValueCL().setLabel("Label " + i);
            cums.add(ind);

            for (POIndicatorState state : POIndicatorState.values()) {
                HashMap<FlowType, IndicatorMOQties> map = new HashMap<FlowType, IndicatorMOQties>();
                ind.getQtiesByState().put(state, map);
                for (FlowType type : FlowType.values()) {
                    // HashMap<FlowType, IndicatorMOQties> m2 = new HashMap<FlowType, IndicatorMOQties>();
                    IndicatorMOQties qties = new IndicatorMOQties();

                    map.put(type, qties);

                    qties.setNbProcessOrder(1);
                    qties.getGarbageQtyWU().setEffectiveQty(1);
                    qties.getGarbageQtyWU().setExpectedQty(3);
                    qties.getInputQtyWU().setEffectiveQty(1);
                    qties.getInputQtyWU().setExpectedQty(3);
                    qties.getOutputQtyWU().setEffectiveQty(1);
                    qties.getOutputQtyWU().setExpectedQty(3);
                    qties.getProcessQtySU().setEffectiveQty(1);
                    qties.getProcessQtySU().setExpectedQty(3);
                    qties.getTimes().setEffectiveQty((d3.getTime() - d2.getTime()) / 1000);
                    qties.getTimes().setExpectedQty((d2.getTime() - d.getTime()) / 1000);
                    qties.getInputQtyWU().setUnitID("kg");
                    qties.getOutputQtyWU().setUnitID("kg");
                    // qties.getProcessQty().setUnitID("kg");
                    qties.getGarbageQtyWU().setUnitID("kg");

                }
            }
        }

        indicators = new ArrayList<IndicatorMO>();

        for (int i = 0; i < 20; i += 1) {

            IndicatorMO ind = new IndicatorMO();
            ind.getQties().setNbProcessOrder(1);
            ind.setManufacturingDate(d);
            ind.getQties().getGarbageQtyWU().setEffectiveQty(1);
            ind.getQties().getGarbageQtyWU().setExpectedQty(3);
            ind.getQties().getInputQtyWU().setEffectiveQty(1);
            ind.getQties().getInputQtyWU().setExpectedQty(3);
            ind.getQties().getOutputQtyWU().setEffectiveQty(1);
            ind.getQties().getOutputQtyWU().setExpectedQty(3);
            ind.getQties().getProcessQtySU().setEffectiveQty(1);
            ind.getQties().getProcessQtySU().setExpectedQty(3);
            ind.getQties().getTimes().setEffectiveQty((d3.getTime() - d2.getTime()) / 1000);
            ind.getQties().getTimes().setExpectedQty((d2.getTime() - d.getTime()) / 1000);
            ind.getQties().getInputQtyWU().setUnitID("kg");
            ind.getQties().getOutputQtyWU().setUnitID("kg");
            ind.getQties().getProcessQtySU().setUnitID("kg");
            ind.getQties().getGarbageQtyWU().setUnitID("kg");
            ind.getInfos().setFlowType(i % 2 == 0 ? FlowType.PUSHED : FlowType.PULLED);
            ind.getInfos().setState(POIndicatorState.STARTED);
            ind.getInfos().setSectorCL(new CodeLabel("SECT_1", ""));
            ind.getInfos().setTeamCL(new CodeLabel("TEAM_1", ""));
            ind.getInfos().setLineCL(new CodeLabel("LINE_1", ""));
            ind.getInfos().setHchyValueCL(new CodeLabel("HCHY_1", ""));

            ind.getInfos().setEffectiveBeginDate(d2);
            ind.getInfos().setEffectiveEndDate(d3);

            ind.getInfos().setExpectedBeginDate(d);
            ind.getInfos().setExpectedEndDate(d2);

            ind.getInfos().getItemCL().setCode("Item " + i);
            ind.getInfos().getItemCL().setLabel("Item " + i);

            indicators.add(ind);
        }

    }
}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 27 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils;


import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ws.rs.core.Cookie;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.cxf.jaxrs.impl.MetadataMap;


/**
 * TODO Write the class' description
 *
 * @author ac
 */
public class HttpHeadersMock implements HttpHeaders {

    private MultivaluedMap<String, String> requestHeaders = new MetadataMap<String, String>();

    public void add(final String key, final String value) {
        requestHeaders.add(key, value);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Locale> getAcceptableLanguages() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MediaType> getAcceptableMediaTypes() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, Cookie> getCookies() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date getDate() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getHeaderString(final String name) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Locale getLanguage() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getLength() {
        return 0;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MediaType getMediaType() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getRequestHeader(final String name) {
        return requestHeaders.get(name);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MultivaluedMap<String, String> getRequestHeaders() {
        return requestHeaders;
    }

}

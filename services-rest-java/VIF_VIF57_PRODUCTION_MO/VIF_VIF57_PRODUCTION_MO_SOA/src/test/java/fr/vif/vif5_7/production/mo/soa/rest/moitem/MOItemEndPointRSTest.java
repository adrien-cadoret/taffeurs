/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 27 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.moitem;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.common.IdContextGen;
import fr.vif.jtech.common.util.i18n.UntranslatedClassConstant;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.jmock.JMockContext;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.StatusType;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.production.mo.dao.kota.Kota;
import fr.vif.vif5_7.production.mo.dao.kotad.Kotad;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPO;
import fr.vif.vif5_7.production.mo.soa.rest.utils.ContextBuilderRS;
import fr.vif.vif5_7.production.mo.soa.rest.utils.Converter;
import fr.vif.vif5_7.production.mo.soa.rest.utils.HttpHeadersMock;
import fr.vif.vif5_7.production.mo.soa.rest.utils.ResponseDTO;
import fr.vif.vif5_7.production.mo.soa.rest.utils.UriInfoMock;
import fr.vif.vif5_7.production.mo.soa.rest.utils.error.BusinessError;


/**
 * TODO Write the class' description
 *
 * @author ac
 */
public class MOItemEndPointRSTest {

    private final static String COMPANY_TEST       = "03";
    private final static String ESTABLISHMENT_TEST = "01";

    /**
     * Main JMock context.
     */
    public static Mockery       context;

    private MoItemEndPointRS    endpoint;

    private MOSBS               moSbs;

    private ContextBuilderRS    contextBuilder;

    private SoamoitemPPO        soamoitemPPO;

    private ResponseDTO         responseDTO;

    @After
    public void after() {

    }

    @Before
    public void before() {
        context = JMockContext.getNewContext();

        moSbs = context.mock(MOSBS.class);
        soamoitemPPO = context.mock(SoamoitemPPO.class);
        contextBuilder = context.mock(ContextBuilderRS.class);
        responseDTO = context.mock(ResponseDTO.class);

        endpoint = new MoItemEndPointRS();
        endpoint.setMoSBS(moSbs);
        endpoint.setSoamoitemPPO(soamoitemPPO);
        endpoint.setContextBuilder(contextBuilder);
        endpoint.setResponseDTO(responseDTO);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testDeleteDetailedMoItem() throws ProgressBusinessException, BusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");

        context.checking(new Expectations() {
            {
                oneOf(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                oneOf(soamoitemPPO).deleteMOItem(with(any(VIFContext.class)), with(any(List.class)),
                        with(aNonNull(List.class)), with(aNonNull(TempTableHolder.class)));
                oneOf(responseDTO).getStatus();
                will(returnValue(StatusType.OK));
                oneOf(responseDTO).getMessage();
                will(returnValue("Everything is OK"));

            }
        });

        Response response = endpoint.deleteDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST,
                "1OF", 10395, 1, 1, 1);
        assertEquals(Status.NO_CONTENT.getStatusCode(), response.getStatus());
        assertEquals("Everything is OK", ((ResponseDTO) response.getEntity()).getMessage());

    }

    @SuppressWarnings("unchecked")
    @Test
    public void testDeleteDetailedMoItemWithBadRequest() throws ProgressBusinessException, BusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");

        context.checking(new Expectations() {
            {
                oneOf(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                oneOf(soamoitemPPO).deleteMOItem(with(any(VIFContext.class)), with(any(List.class)),
                        with(aNonNull(List.class)), with(aNonNull(TempTableHolder.class)));
                oneOf(responseDTO).getStatus();
                will(returnValue(StatusType.ERROR));
                oneOf(responseDTO).getMessage();
                will(returnValue("Bad Request Error"));

            }
        });

        Response response = endpoint.deleteDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST,
                "1OF", 10395, 1, 1, 1);
        assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals("Bad Request Error", ((ResponseDTO) response.getEntity()).getMessage());

    }

    @Test
    public void testDeleteDetailedMoItemWithContextError() throws BusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final Kotad result = createDetailedMoItem(1);

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(throwException(new BusinessException(new DAOException("Context error"))));

            }
        });

        Response response = endpoint.deleteDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST,
                "1OF", 10395, 1, 1, 1);
        assertEquals(Status.FORBIDDEN.getStatusCode(), response.getStatus());
        assertTrue(((BusinessError) response.getEntity()).getMessage().contains("Context error"));

    }

    @SuppressWarnings("unchecked")
    @Test
    public void testDeleteDetailedMoItemWithInternalError() throws ProgressBusinessException, BusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");

        context.checking(new Expectations() {
            {
                oneOf(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                oneOf(soamoitemPPO).deleteMOItem(with(any(VIFContext.class)), with(any(List.class)),
                        with(aNonNull(List.class)), with(aNonNull(TempTableHolder.class)));
                with(aNonNull(TempTableHolder.class));
                will(throwException(new ProgressBusinessException("")));

            }
        });

        Response response = endpoint.deleteDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST,
                "1OF", 10395, 1, 1, 1);
        assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

    }

    @SuppressWarnings("unchecked")
    @Test
    public void testInsertDetailedMoItem() throws BusinessException, ServerBusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");
        final Kotad result = createDetailedMoItem(1);

        context.checking(new Expectations() {
            {
                oneOf(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                oneOf(soamoitemPPO).insertMOItem(with(any(VIFContext.class)), with(any(List.class)),
                        with(aNonNull(List.class)), with(aNonNull(List.class)), with(aNonNull(TempTableHolder.class)),
                        with(aNonNull(TempTableHolder.class)));
                oneOf(responseDTO).getStatus();
                will(returnValue(StatusType.OK));
                oneOf(responseDTO).getMessage();
                will(returnValue("Everything is OK"));
                oneOf(responseDTO).getUri();
                will(returnValue("URI"));

            }
        });

        Response response = endpoint.insertDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST,
                Converter.convertDetailedMOItemToDTO(result));
        assertEquals(Status.CREATED.getStatusCode(), response.getStatus());
        assertEquals("Everything is OK", ((ResponseDTO) response.getEntity()).getMessage());
        assertEquals("URI", ((ResponseDTO) response.getEntity()).getUri());
    }

    @SuppressWarnings("unchecked")
    @Test
    public void testInsertDetailedMoItemWithBadRequest() throws ProgressBusinessException, BusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");
        final Kotad result = createDetailedMoItem(1);

        context.checking(new Expectations() {
            {
                oneOf(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                oneOf(soamoitemPPO).insertMOItem(with(any(VIFContext.class)), with(any(List.class)),
                        with(aNonNull(List.class)), with(aNonNull(List.class)), with(aNonNull(TempTableHolder.class)),
                        with(aNonNull(TempTableHolder.class)));
                oneOf(responseDTO).getStatus();
                will(returnValue(StatusType.ERROR));
                oneOf(responseDTO).getMessage();
                will(returnValue("Bad Request Error"));
                oneOf(responseDTO).getUri();
            }
        });

        Response response = endpoint.insertDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST,
                Converter.convertDetailedMOItemToDTO(result));
        assertEquals(Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        assertEquals("Bad Request Error", ((ResponseDTO) response.getEntity()).getMessage());
        assertEquals("", ((ResponseDTO) response.getEntity()).getUri());

    }

    @Test
    public void testInsertDetailedMoItemWithContextError() throws BusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final Kotad result = createDetailedMoItem(1);

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(throwException(new BusinessException(new DAOException("Context error"))));

            }
        });

        Response response = endpoint.insertDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST,
                Converter.convertDetailedMOItemToDTO(result));
        assertEquals(Status.FORBIDDEN.getStatusCode(), response.getStatus());
        assertTrue(((BusinessError) response.getEntity()).getMessage().contains("Context error"));

    }

    @SuppressWarnings("unchecked")
    @Test
    public void testInsertDetailedMoItemWithInternalError() throws BusinessException, ProgressBusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");
        final Kotad result = createDetailedMoItem(1);

        context.checking(new Expectations() {
            {
                oneOf(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                oneOf(soamoitemPPO).insertMOItem(with(any(VIFContext.class)), with(any(List.class)),
                        with(aNonNull(List.class)), with(aNonNull(List.class)), with(aNonNull(TempTableHolder.class)),
                        with(aNonNull(TempTableHolder.class)));
                will(throwException(new ProgressBusinessException("")));

            }
        });

        Response response = endpoint.insertDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST,
                Converter.convertDetailedMOItemToDTO(result));
        assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

    }

    @Test
    public void testReadDetailedMoItem() throws BusinessException, ServerBusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");
        final Kotad result = createDetailedMoItem(1);
        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                one(moSbs).getDetailedMoItemByPK(with(any(VIFContext.class)), with(createMOKey()), with(1), with(1),
                        with(1), with(1));
                will(returnValue(result));
            }
        });

        Response response = endpoint.readDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF",
                10395, 1, 1, 1);
        assertEquals(Status.OK.getStatusCode(), response.getStatus());
        KotadDTO dto = (KotadDTO) response.getEntity();
        assertEquals(result.getCart(), dto.getCart());
        assertEquals(result.getQte1(), dto.getQte1(), 0.0f);

    }

    @SuppressWarnings("unchecked")
    @Test
    public void testReadDetailedMoItems() throws BusinessException, ServerBusinessException {
        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");

        List<Kotad> result = createDetailedMoItemList(3);

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                one(moSbs).listDetailedMoItemsByMoItem(with(any(VIFContext.class)), with(createMOKey()), with(1),
                        with(1));
                will(returnValue(result));
            }
        });

        Response response = endpoint.readDetailedMoItems(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF",
                10395, 1, 1);
        assertEquals(Status.OK.getStatusCode(), response.getStatus());
        List<KotadDTO> dtos = (List<KotadDTO>) response.getEntity();
        assertEquals(result.size(), dtos.size());
        assertEquals(result.get(0).getNi3(), dtos.get(0).getNi3());
        assertEquals(result.get(1).getNi3(), dtos.get(1).getNi3());
        assertEquals(result.get(2).getNi3(), dtos.get(2).getNi3());

    }

    @Test
    public void testReadDetailedMoItemsWithContextError() throws BusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(throwException(new BusinessException(new DAOException("Context error"))));

            }
        });

        Response response = endpoint.readDetailedMoItems(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF",
                10395, 1, 1);

        assertEquals(Status.FORBIDDEN.getStatusCode(), response.getStatus());
        assertTrue(((BusinessError) response.getEntity()).getMessage().contains("Context error"));

    }

    @Test
    public void testReadDetailedMoItemsWithInternalError() throws BusinessException, ServerBusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                one(moSbs).listDetailedMoItemsByMoItem(with(any(VIFContext.class)), with(createMOKey()), with(1),
                        with(1));
                will(throwException(new ServerBusinessException(new DAOException("Internal Test Error"))));
            }
        });

        Response response = endpoint.readDetailedMoItems(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF",
                10395, 1, 1);

        assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        assertTrue(((BusinessError) response.getEntity()).getMessage().contains("Internal Test Error"));

    }

    @Test
    public void testReadDetailedMoItemWithContextError() throws BusinessException, ServerBusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(throwException(new BusinessException(new DAOException("Context error"))));

            }
        });

        Response response = endpoint.readDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF",
                10395, 1, 1, 1);

        assertEquals(Status.FORBIDDEN.getStatusCode(), response.getStatus());
        assertTrue(((BusinessError) response.getEntity()).getMessage().contains("Context error"));

    }

    @Test
    public void testReadDetailedMoItemWithInternalError() throws BusinessException, ServerBusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                one(moSbs).getDetailedMoItemByPK(with(any(VIFContext.class)), with(createMOKey()), with(1), with(1),
                        with(1), with(1));
                will(throwException(new ServerBusinessException(new UntranslatedClassConstant(),
                        new VarParamTranslation(Status.INTERNAL_SERVER_ERROR))));
            }
        });

        Response response = endpoint.readDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF",
                10395, 1, 1, 1);

        assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());

    }

    @Test
    public void testReadDetailedMoItemWithNotFoundResource() throws BusinessException, ServerBusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                one(moSbs).getDetailedMoItemByPK(with(any(VIFContext.class)), with(createMOKey()), with(1), with(1),
                        with(1), with(1));
                will(throwException(new ServerBusinessException(new UntranslatedClassConstant(),
                        new VarParamTranslation(Status.NOT_FOUND))));
            }
        });

        Response response = endpoint.readDetailedMoItem(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF",
                10395, 1, 1, 1);

        assertEquals(Status.NOT_FOUND.getStatusCode(), response.getStatus());

    }

    @SuppressWarnings("unchecked")
    @Test
    public void testReadMoItems() throws BusinessException, ServerBusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");

        List<Kota> result = createMoItemList(3);

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                one(moSbs).listMoItemsByMO(with(any(VIFContext.class)), with(createMOKey()));
                will(returnValue(result));
            }
        });

        Response response = endpoint.readMoItems(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF", 10395);
        assertEquals(Status.OK.getStatusCode(), response.getStatus());
        List<KotaDTO> dtos = (List<KotaDTO>) response.getEntity();
        assertEquals(result.size(), dtos.size());
        assertEquals(result.get(0).getNi2(), dtos.get(0).getNi2());
        assertEquals(result.get(1).getNi2(), dtos.get(1).getNi2());
        assertEquals(result.get(2).getNi2(), dtos.get(2).getNi2());

    }

    @Test
    public void testReadMoItemsWithContextError() throws BusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(throwException(new BusinessException(new DAOException("Context error"))));

            }
        });

        Response response = endpoint.readMoItems(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF", 10395);

        assertEquals(Status.FORBIDDEN.getStatusCode(), response.getStatus());
        assertTrue(((BusinessError) response.getEntity()).getMessage().contains("Context error"));

    }

    @Test
    public void testReadMoItemsWithInternalError() throws BusinessException, ServerBusinessException {

        final HttpHeadersMock httpHeaders = new HttpHeadersMock();
        final UriInfoMock uriInfo = new UriInfoMock();
        final IdContextGen idContext = new IdContextGen("03", "01", "fr_FR", "ACA");

        context.checking(new Expectations() {
            {
                one(contextBuilder).buildAndCheckContext(with(httpHeaders));
                will(returnValue(idContext));
                one(moSbs).listMoItemsByMO(with(any(VIFContext.class)), with(createMOKey()));
                will(throwException(new ServerBusinessException(new DAOException("Internal Test Error"))));
            }
        });

        Response response = endpoint.readMoItems(uriInfo, httpHeaders, COMPANY_TEST, ESTABLISHMENT_TEST, "1OF", 10395);

        assertEquals(Status.INTERNAL_SERVER_ERROR.getStatusCode(), response.getStatus());
        assertTrue(((BusinessError) response.getEntity()).getMessage().contains("Internal Test Error"));

    }

    /**
     * TODO : write the method's description
     * 
     * @return
     */
    private Kotad createDetailedMoItem(final int ni3) {

        Kotad mock = new Kotad();

        Date date = new Date();

        mock.setCart("BEURRE DEMI SEL");
        mock.setCdepot("AC1");
        mock.setCemp("ACEMP1");
        mock.setCetab("01");
        mock.setChrono(10395);
        mock.setCsoc("01");
        mock.setChronoct(0);
        mock.setCmonnaie("EURO");
        mock.setCmotmvk("");
        mock.setCnatstk("");
        mock.setCont("CONT1");
        mock.setQte1(100);
        mock.setQte2(200);
        mock.setQte3(300);
        mock.setCu1("Kg");
        mock.setCu2("SAC");
        mock.setCu3("PLA");
        mock.setCuprix("");
        mock.setCuser("AC");
        mock.setCv("");
        mock.setDatheurgen(null);
        mock.setDatheurmvt(date);
        mock.setNi1(1);
        mock.setNi1or(0);
        mock.setNi2(1);
        mock.setNi2or(0);
        mock.setLot("LOT1");
        mock.setMt1(0);
        mock.setNi3(ni3);
        mock.setNi3or(0);
        mock.setNi4or(0);
        mock.setNlig(2);
        mock.setPrechroct("");
        mock.setPrix(100.0);
        mock.setTare(0);
        mock.setTstock(true);
        mock.setTypor("");
        mock.setPrechro("SST");
        mock.setValmt1(0.1);
        mock.setValmt2(0.2);
        mock.setValmt3(0.3);
        mock.setValmt4(0.4);
        mock.setValmt5(0.5);
        mock.setValprix1(0.1);
        mock.setValprix2(0.2);
        mock.setValprix3(0.3);
        mock.setValprix4(0.4);
        mock.setValprix5(0.5);
        mock.setTypacta("ENT");

        return mock;
    }

    /**
     * Creates Detailed Mo Item List
     * 
     * @param i array size
     */
    private List<Kotad> createDetailedMoItemList(final int length) {
        List<Kotad> result = new ArrayList<Kotad>();

        for (int i = 0; i < length; i++) {
            result.add(createDetailedMoItem(i + 1));
        }

        return result;

    }

    /**
     * TODO : write the method's description
     * 
     * @return
     */
    private Kota createMoItem(final int ni2) {

        Kota mock = new Kota();

        Date date = new Date();

        mock.setCart("BEURRE DEMI SEL");
        mock.setCavanc("");
        mock.setCdepot("ACDEP");
        mock.setCemp("ACEMP");
        mock.setCetab("01");
        mock.setCformu("");
        mock.setChrono(10395);
        mock.setClenrj("");
        mock.setCoefcout(0.0);
        mock.setCprop("");
        mock.setCrges("");
        mock.setCsoc("03");
        mock.setCu2("SAC");
        mock.setCu2theo("SEAU");
        mock.setCunite("Kg");
        mock.setCv("");
        mock.setDatheurmvt(date);
        mock.setLotq(100.0);
        mock.setLotq2(200.0);
        mock.setLotqi(300.0);
        mock.setLotsor("LOTSOR");
        mock.setNi1(1);
        mock.setNi1acta(0);
        mock.setNi1or(0);
        mock.setNi2(ni2);
        mock.setNi2opinf(0);
        mock.setNi2or(0);
        mock.setOrig("");
        mock.setPrechro("1OF");
        mock.setQ2theo(0.0);
        mock.setQte1(200.0);
        mock.setQte2(300.0);
        mock.setQte3(400.0);
        mock.setQtedec(0.0);
        mock.setQtefr1(0.1);
        mock.setQtefr2(0.2);
        mock.setQtefr3(0.3);
        mock.setQtestk(0.4);
        mock.setTauto(false);
        mock.setTctnul(true);
        mock.setTeprod(false);
        mock.setToartgen(true);
        mock.setTqconst(false);
        mock.setTqtedif(true);
        mock.setTrdtdyn(false);
        mock.setTrdtm(true);
        mock.setTxfr(0.1);
        mock.setTypacta("ENT");
        mock.setTypdecatl("");

        return mock;
    }

    /**
     * Creates Mo Item List
     * 
     * @param i array size
     */
    private List<Kota> createMoItemList(final int length) {
        List<Kota> result = new ArrayList<Kota>();

        for (int i = 0; i < length; i++) {
            result.add(createMoItem(i + 1));
        }

        return result;

    }

    /**
     * Creates MOKey.
     * 
     * @param moKey the key
     */
    private MOKey createMOKey() {

        MOKey moKey = new MOKey();

        EstablishmentKey establishmentKey = new EstablishmentKey();
        establishmentKey.setCsoc(COMPANY_TEST);
        establishmentKey.setCetab(ESTABLISHMENT_TEST);
        Chrono chrono = new Chrono("1OF", 10395);
        moKey.setEstablishmentKey(establishmentKey);
        moKey.setChrono(chrono);

        return moKey;
    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 27 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.ping;


import static org.junit.Assert.assertEquals;

import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import org.junit.Before;
import org.junit.Test;


/**
 * TODO Write the class' description
 *
 * @author ac
 */
public class SubcoWebPortalPingRSTest {

    private SubcoWebPortalPingEndPointRS endpoint;

    @Before
    public void before() {
        endpoint = new SubcoWebPortalPingEndPointRS();
    }

    @Test
    public void testPing() {
        Response response = endpoint.ping();
        assertEquals(Status.OK.getStatusCode(), response.getStatus());
        assertEquals("OK", response.getEntity());
    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 27 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils;


import java.net.URI;
import java.util.List;

import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.PathSegment;
import javax.ws.rs.core.UriBuilder;
import javax.ws.rs.core.UriInfo;


/**
 * TODO Write the class' description
 *
 * @author ac
 */
public class UriInfoMock implements UriInfo {

    /**
     * {@inheritDoc}
     */
    @Override
    public URI getAbsolutePath() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UriBuilder getAbsolutePathBuilder() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public URI getBaseUri() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UriBuilder getBaseUriBuilder() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Object> getMatchedResources() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getMatchedURIs() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> getMatchedURIs(final boolean decode) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPath() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPath(final boolean decode) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MultivaluedMap<String, String> getPathParameters() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MultivaluedMap<String, String> getPathParameters(final boolean decode) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PathSegment> getPathSegments() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PathSegment> getPathSegments(final boolean decode) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MultivaluedMap<String, String> getQueryParameters() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MultivaluedMap<String, String> getQueryParameters(final boolean decode) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public URI getRequestUri() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public UriBuilder getRequestUriBuilder() {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public URI relativize(final URI uri) {
        return null;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public URI resolve(final URI uri) {
        return null;
    }

}

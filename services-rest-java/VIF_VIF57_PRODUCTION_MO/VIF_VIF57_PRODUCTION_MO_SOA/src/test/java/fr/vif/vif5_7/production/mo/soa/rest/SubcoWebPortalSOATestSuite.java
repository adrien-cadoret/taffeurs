/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 27 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.vif.vif5_7.production.mo.soa.rest.moitem.MOItemEndPointRSTest;
import fr.vif.vif5_7.production.mo.soa.rest.ping.SubcoWebPortalPingRSTest;
import fr.vif.vif5_7.production.mo.soa.rest.utils.ConverterTest;


/**
 * Test Suite for Subco's Web Portal services
 *
 * @author ac
 */
@RunWith(Suite.class)
@SuiteClasses(value = { SubcoWebPortalPingRSTest.class, MOItemEndPointRSTest.class, ConverterTest.class })
public class SubcoWebPortalSOATestSuite {

}

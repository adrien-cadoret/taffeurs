/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 27 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils;


import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.jmock.Mockery;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import fr.vif.jtech.jmock.JMockContext;
import fr.vif.vif5_7.production.mo.InsertMOItemDataType;
import fr.vif.vif5_7.production.mo.MOItemSelectionType;
import fr.vif.vif5_7.production.mo.UseType;
import fr.vif.vif5_7.production.mo.dao.kota.Kota;
import fr.vif.vif5_7.production.mo.dao.kotad.Kotad;
import fr.vif.vif5_7.production.mo.soa.rest.moitem.KotadDTO;


/**
 * Test Converter class
 *
 * @author ac
 */
public class ConverterTest {

    /**
     * Main JMock context.
     */
    public static Mockery context;

    private Converter     converter;

    @After
    public void after() {

    }

    @Before
    public void before() {
        context = JMockContext.getNewContext();

        converter = new Converter();
    }

    // TODO
    @Test
    public void testConvertDateToXMLGregorianCalendar() {

        // Date date = new Date();
        //
        // XMLGregorianCalendar result = converter.convertDateToXMLGregorianCalendar(date);
        //
        // XMLGregorianCalendar expected =

    }

    @SuppressWarnings("static-access")
    @Test
    public void testConvertDetailedMOItemToDTO() {

        Kotad toConvert = new Kotad();

        Date date = new Date();

        toConvert.setCart("BEURRE DEMI SEL");
        toConvert.setCdepot("AC1");
        toConvert.setCemp("ACEMP1");
        toConvert.setCetab("01");
        toConvert.setChrono(10395);
        toConvert.setCsoc("01");
        toConvert.setChronoct(0);
        toConvert.setCmonnaie("EURO");
        toConvert.setCmotmvk("");
        toConvert.setCnatstk("");
        toConvert.setCont("CONT1");
        toConvert.setQte1(100);
        toConvert.setQte2(200);
        toConvert.setQte3(300);
        toConvert.setCu1("Kg");
        toConvert.setCu2("SAC");
        toConvert.setCu3("PLA");
        toConvert.setCuprix("");
        toConvert.setCuser("AC");
        toConvert.setCv("");
        toConvert.setDatheurgen(null);
        toConvert.setDatheurmvt(date);
        toConvert.setNi1(1);
        toConvert.setNi1or(0);
        toConvert.setNi2(1);
        toConvert.setNi2or(0);
        toConvert.setLot("LOT1");
        toConvert.setMt1(0);
        toConvert.setNi3(1);
        toConvert.setNi3or(0);
        toConvert.setNi4or(0);
        toConvert.setNlig(2);
        toConvert.setPrechroct("");
        toConvert.setPrix(100.0);
        toConvert.setTare(0);
        toConvert.setTstock(true);
        toConvert.setTypor("");
        toConvert.setPrechro("SST");
        toConvert.setValmt1(0.1);
        toConvert.setValmt2(0.2);
        toConvert.setValmt3(0.3);
        toConvert.setValmt4(0.4);
        toConvert.setValmt5(0.5);
        toConvert.setValprix1(0.1);
        toConvert.setValprix2(0.2);
        toConvert.setValprix3(0.3);
        toConvert.setValprix4(0.4);
        toConvert.setValprix5(0.5);
        toConvert.setTypacta("ENT");

        KotadDTO result = converter.convertDetailedMOItemToDTO(toConvert);

        assertEquals(result.getCart(), "BEURRE DEMI SEL");
        assertEquals(result.getCdepot(), "AC1");
        assertEquals(result.getCemp(), "ACEMP1");
        assertEquals(result.getCetab(), "01");
        assertEquals(result.getChrono(), 10395);
        assertEquals(result.getCsoc(), "01");
        assertEquals(result.getChronoct(), 0);
        assertEquals(result.getCmonnaie(), "EURO");
        assertEquals(result.getCmotmvk(), "");
        assertEquals(result.getCnatstk(), "");
        assertEquals(result.getCont(), "CONT1");
        assertEquals(result.getQte1(), 100, 0.0f);
        assertEquals(result.getQte2(), 200, 0.0f);
        assertEquals(result.getQte3(), 300, 0.0f);
        assertEquals(result.getCu1(), "Kg");
        assertEquals(result.getCu2(), "SAC");
        assertEquals(result.getCu3(), "PLA");
        assertEquals(result.getCuprix(), "");
        assertEquals(result.getCuser(), "AC");
        assertEquals(result.getCv(), "");
        assertEquals(result.getDatheurgen(), null);
        assertEquals(result.getDatheurmvt(), date);
        assertEquals(result.getNi1(), 1);
        assertEquals(result.getNi1or(), 0);
        assertEquals(result.getNi2(), 1);
        assertEquals(result.getNi2or(), 0);
        assertEquals(result.getLot(), "LOT1");
        assertEquals(result.getMt1(), 0, 0.0f);
        assertEquals(result.getNi3(), 1);
        assertEquals(result.getNi3or(), 0);
        assertEquals(result.getNi4or(), 0);
        assertEquals(result.getNlig(), 2);
        assertEquals(result.getPrechroct(), "");
        assertEquals(result.getPrix(), 100, 0.0f);
        assertEquals(result.getTare(), 0, 0.0f);
        assertEquals(result.getTstock(), true);
        assertEquals(result.getTypor(), "");
        assertEquals(result.getPrechro(), "SST");
        assertEquals(result.getValmt1(), 0.1, 0.0f);
        assertEquals(result.getValmt2(), 0.2, 0.0f);
        assertEquals(result.getValmt3(), 0.3, 0.0f);
        assertEquals(result.getValmt4(), 0.4, 0.0f);
        assertEquals(result.getValmt5(), 0.5, 0.0f);
        assertEquals(result.getValprix1(), 0.1, 0.0f);
        assertEquals(result.getValprix2(), 0.2, 0.0f);
        assertEquals(result.getValprix3(), 0.3, 0.0f);
        assertEquals(result.getValprix4(), 0.4, 0.0f);
        assertEquals(result.getValprix5(), 0.5, 0.0f);
        assertEquals(result.getTypacta(), "ENT");
    }

    @SuppressWarnings("static-access")
    @Test
    public void testConvertDTOToInsertMOItemData() {

        KotadDTO toConvert = new KotadDTO();

        Date date = new Date();

        toConvert.setCart("BEURRE DEMI SEL");
        toConvert.setCdepot("AC1");
        toConvert.setCemp("ACEMP1");
        toConvert.setCetab("01");
        toConvert.setChrono(10395);
        toConvert.setCsoc("01");
        toConvert.setQte1(100);
        toConvert.setQte2(200);
        toConvert.setQte3(300);
        toConvert.setCu1("Kg");
        toConvert.setCu2("SAC");
        toConvert.setCu3("PLA");
        toConvert.setDatheurmvt(date);
        toConvert.setNi1(1);
        toConvert.setNi2(1);
        toConvert.setLot("LOT1");
        toConvert.setPrechro("SST");
        toConvert.setTypacta("ENT");

        InsertMOItemDataType result = converter.convertDTOToInsertMOItemData(toConvert);

        assertEquals(result.getEffectiveItem(), "BEURRE DEMI SEL");
        assertEquals(result.getWareLoc().getWarehouse(), "AC1");
        assertEquals(result.getWareLoc().getLocation(), "ACEMP1");
        assertEquals(result.getCompEstab().getEstablishment(), "01");
        assertEquals(result.getMoChrono().getChrono(), 10395);
        assertEquals(result.getCompEstab().getCompany(), "01");
        assertEquals(result.getEffectiveQties().getQuantity().get(0).getQty(), 100, 0.0f);
        assertEquals(result.getEffectiveQties().getQuantity().get(1).getQty(), 200, 0.0f);
        assertEquals(result.getEffectiveQties().getQuantity().get(2).getQty(), 300, 0.0f);
        assertEquals(result.getEffectiveQties().getQuantity().get(0).getUnit(), "Kg");
        assertEquals(result.getEffectiveQties().getQuantity().get(1).getUnit(), "SAC");
        assertEquals(result.getEffectiveQties().getQuantity().get(2).getUnit(), "PLA");
        assertEquals(result.getMvtDateTime(), converter.convertDateToXMLGregorianCalendar(date));
        assertEquals(result.getMoItemId().getNiMOItemId().getNi2(), 1);
        assertEquals(result.getMoItemId().getNiMOItemId().getNi1(), 1);
        assertEquals(result.getBatch(), "LOT1");
        assertEquals(result.getMoChrono().getPrechro(), "SST");
        assertEquals(result.getMoItemId().getItemMOId().getItemType(), "ENT");

    }

    @Test
    public void testConvertMOItemToDTO() {

        Kota toConvert = new Kota();

        Date date = new Date();

        toConvert.setCart("BEURRE DEMI SEL");
        toConvert.setCavanc("");
        toConvert.setCdepot("ACDEP");
        toConvert.setCemp("ACEMP");
        toConvert.setCetab("01");
        toConvert.setCformu("");
        toConvert.setChrono(10395);
        toConvert.setClenrj("");
        toConvert.setCoefcout(0.0);
        toConvert.setCprop("");
        toConvert.setCrges("");
        toConvert.setCsoc("03");
        toConvert.setCu2("SAC");
        toConvert.setCu2theo("SEAU");
        toConvert.setCunite("Kg");
        toConvert.setCv("");
        toConvert.setDatheurmvt(date);
        toConvert.setLotq(100.0);
        toConvert.setLotq2(200.0);
        toConvert.setLotqi(300.0);
        toConvert.setLotsor("LOTSOR");
        toConvert.setNi1(1);
        toConvert.setNi1acta(0);
        toConvert.setNi1or(0);
        toConvert.setNi2(1);
        toConvert.setNi2opinf(0);
        toConvert.setNi2or(0);
        toConvert.setOrig("");
        toConvert.setPrechro("1OF");
        toConvert.setQ2theo(0.0);
        toConvert.setQte1(200.0);
        toConvert.setQte2(300.0);
        toConvert.setQte3(400.0);
        toConvert.setQtedec(0.0);
        toConvert.setQtefr1(0.1);
        toConvert.setQtefr2(0.2);
        toConvert.setQtefr3(0.3);
        toConvert.setQtestk(0.4);
        toConvert.setTauto(false);
        toConvert.setTctnul(true);
        toConvert.setTeprod(false);
        toConvert.setToartgen(true);
        toConvert.setTqconst(false);
        toConvert.setTqtedif(true);
        toConvert.setTrdtdyn(false);
        toConvert.setTrdtm(true);
        toConvert.setTxfr(0.1);
        toConvert.setTypacta("ENT");
        toConvert.setTypdecatl("");

        assertEquals(toConvert.getCart(), "BEURRE DEMI SEL");
        assertEquals(toConvert.getCavanc(), "");
        assertEquals(toConvert.getCdepot(), "ACDEP");
        assertEquals(toConvert.getCemp(), "ACEMP");
        assertEquals(toConvert.getCetab(), "01");
        assertEquals(toConvert.getCformu(), "");
        assertEquals(toConvert.getChrono(), 10395);
        assertEquals(toConvert.getClenrj(), "");
        assertEquals(toConvert.getCoefcout(), 0.0, 0.0f);
        assertEquals(toConvert.getCprop(), "");
        assertEquals(toConvert.getCrges(), "");
        assertEquals(toConvert.getCsoc(), "03");
        assertEquals(toConvert.getCu2(), "SAC");
        assertEquals(toConvert.getCu2theo(), "SEAU");
        assertEquals(toConvert.getCunite(), "Kg");
        assertEquals(toConvert.getCv(), "");
        assertEquals(toConvert.getDatheurmvt(), date);
        assertEquals(toConvert.getLotq(), 100.0, 0.0f);
        assertEquals(toConvert.getLotq2(), 200.0, 0.0f);
        assertEquals(toConvert.getLotqi(), 300.0, 0.0f);
        assertEquals(toConvert.getLotsor(), "LOTSOR");
        assertEquals(toConvert.getNi1(), 1);
        assertEquals(toConvert.getNi1acta(), 0);
        assertEquals(toConvert.getNi1or(), 0);
        assertEquals(toConvert.getNi2(), 1);
        assertEquals(toConvert.getNi2opinf(), 0);
        assertEquals(toConvert.getNi2or(), 0);
        assertEquals(toConvert.getOrig(), "");
        assertEquals(toConvert.getPrechro(), "1OF");
        assertEquals(toConvert.getQ2theo(), 0.0, 0.0f);
        assertEquals(toConvert.getQte1(), 200.0, 0.0f);
        assertEquals(toConvert.getQte2(), 300.0, 0.0f);
        assertEquals(toConvert.getQte3(), 400.0, 0.0f);
        assertEquals(toConvert.getQtedec(), 0.0, 0.0f);
        assertEquals(toConvert.getQtefr1(), 0.1, 0.0f);
        assertEquals(toConvert.getQtefr2(), 0.2, 0.0f);
        assertEquals(toConvert.getQtefr3(), 0.3, 0.0f);
        assertEquals(toConvert.getQtestk(), 0.4, 0.0f);
        assertEquals(toConvert.getTauto(), false);
        assertEquals(toConvert.getTctnul(), true);
        assertEquals(toConvert.getTeprod(), false);
        assertEquals(toConvert.getToartgen(), true);
        assertEquals(toConvert.getTqconst(), false);
        assertEquals(toConvert.getTqtedif(), true);
        assertEquals(toConvert.getTrdtdyn(), false);
        assertEquals(toConvert.getTrdtm(), true);
        assertEquals(toConvert.getTxfr(), 0.1, 0.0f);
        assertEquals(toConvert.getTypacta(), "ENT");
        assertEquals(toConvert.getTypdecatl(), "");

    }

    @SuppressWarnings("static-access")
    @Test
    public void testCreateMOItemSelectionType() {

        MOItemSelectionType result = converter
                .createMOItemSelectionType("03", "01", "1OF", 10395, UseType.FAB, 1, 2, 3);

        assertEquals("03", result.getCompEstab().getCompany());
        assertEquals("01", result.getCompEstab().getEstablishment());
        assertEquals("1OF", result.getMoChrono().getPrechro());
        assertEquals(10395, result.getMoChrono().getChrono());
        assertEquals(new Integer(1), result.getNi1());
        assertEquals(new Integer(2), result.getNi2());
        assertEquals(new Integer(3), result.getNi3());

    }
}

package fr.vif.vif5_7.production.mo.soa.endpoint.mo;


import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;
import javax.xml.ws.WebServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.soa.endpoint.AbstractSOAEndPoint;
import fr.vif.vif5_7.production.mo.DeleteMosRequest;
import fr.vif.vif5_7.production.mo.DeleteMosResponse;
import fr.vif.vif5_7.production.mo.IMoSOA;
import fr.vif.vif5_7.production.mo.InsertFabricationMosRequest;
import fr.vif.vif5_7.production.mo.InsertFabricationMosResponse;
import fr.vif.vif5_7.production.mo.InsertPreparationMosRequest;
import fr.vif.vif5_7.production.mo.InsertPreparationMosResponse;
import fr.vif.vif5_7.production.mo.ReadMosRequest;
import fr.vif.vif5_7.production.mo.ReadMosResponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPO;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtDeleteresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtIncriteria;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtInsertresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtMoinsert;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtMoresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtMoselection;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtOutcriteria;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtReadresponse;


/**
 * The Mo End Point.
 * 
 * @author BaXter
 */
@WebService(serviceName = "MoEndPointService", portName = "MoEndPointPort", targetNamespace = "http://www.vif.fr/vif5_7/production/mo", wsdlLocation = "classpath:/production/mo/soa/schemas/MoSOA.wsdl", endpointInterface = "fr.vif.vif5_7.production.mo.IMoSOA")
public class MoEndPointSOAP extends AbstractSOAEndPoint implements IMoSOA {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(MoEndPointSOAP.class);

    @Autowired
    private SoamoPPO            soamoPPO;

    private MOConverter         converter;

    /**
     * {@inheritDoc}
     */
    @Override
    public DeleteMosResponse deleteMos(final DeleteMosRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteMos(request=" + request + ")");
        }

        DeleteMosResponse ret = new DeleteMosResponse();

        VIFContext ctx;
        try {
            // Check input
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOConverter.getInstance();

                // Set ttMoSelection parameter
                List<SoamoPPOTtMoselection> ttMoSelection = new ArrayList<SoamoPPOTtMoselection>();
                ttMoSelection.add(converter.convertSelectionToTt(request.getDeleteMosSelection().getMosSelection()));

                // Set ttIncriteria parameter
                List<SoamoPPOTtIncriteria> ttIncriteria = new ArrayList<SoamoPPOTtIncriteria>();
                ttIncriteria = converter.convertMOCriteriaToTtCriv(request.getDeleteMosSelection().getMosSelection()
                        .getCriteria());

                // Create response holder
                TempTableHolder<SoamoPPOTtDeleteresponse> statusResponseHolder = new TempTableHolder<SoamoPPOTtDeleteresponse>();

                // Call appServer
                getSoamoPPO().deleteMos(ctx, ttMoSelection, ttIncriteria, statusResponseHolder);

                // Standard Response
                ret.setStandardResponse(converter.convertStatusDeleteResponseToStandardResponse(statusResponseHolder));

                // Delete Response : the moKey deleted
                ret.setDeleteMosResponse(converter.convertSOADeleteResponseToDeleteResponse(statusResponseHolder));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                throw new WebServiceException(e);
            } catch (DAOException e) {
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteMOItem(request=" + request + ")=" + ret);
        }
        return ret;
    }

    /**
     * Gets the soamoPPO.
     * 
     * @return the soamoPPO.
     */
    public SoamoPPO getSoamoPPO() {
        return soamoPPO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InsertFabricationMosResponse insertFabricationMos(final InsertFabricationMosRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertFabricationMos(request=" + request + ")");
        }

        InsertFabricationMosResponse ret = new InsertFabricationMosResponse();

        VIFContext ctx;
        try {
            // Check input
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOConverter.getInstance();

                // Set ttMoSelection parameter
                List<SoamoPPOTtMoinsert> ttMoInsert = new ArrayList<SoamoPPOTtMoinsert>();
                ttMoInsert.add(converter.convertInsertFabricationToTt(request.getInsertFabricationMosData()
                        .getInsertMosData()));

                // Set ttIncriteria parameter
                List<SoamoPPOTtIncriteria> ttIncriteria = new ArrayList<SoamoPPOTtIncriteria>();
                ttIncriteria = converter.convertMOCriteriaToTtCriv(request.getInsertFabricationMosData()
                        .getInsertMosData().getCriteria());

                // Create response holder
                TempTableHolder<SoamoPPOTtInsertresponse> statusResponseHolder = new TempTableHolder<SoamoPPOTtInsertresponse>();

                // Call appServer
                getSoamoPPO().insertFabricationMos(ctx, ttMoInsert, ttIncriteria, statusResponseHolder);

                // Standard Response
                ret.setStandardResponse(converter.convertStatusInsertResponseToStandardResponse(statusResponseHolder));

                // Insert Response : the moKey inserted
                ret.setInsertFabricationMosResponse(converter
                        .convertSOAInsertFabricationResponseToInsertFabricationResponse(statusResponseHolder));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                throw new WebServiceException(e);
            } catch (DAOException e) {
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertFabricationMos(request=" + request + ")=" + request);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InsertPreparationMosResponse insertPreparationMos(final InsertPreparationMosRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertPreparationMos(request=" + request + ")");
        }

        InsertPreparationMosResponse ret = new InsertPreparationMosResponse();

        VIFContext ctx;
        try {
            // Check input
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOConverter.getInstance();

                // Set ttMoSelection parameter
                List<SoamoPPOTtMoinsert> ttMoInsert = new ArrayList<SoamoPPOTtMoinsert>();
                ttMoInsert.add(converter.convertInsertPreparationToTt(request.getInsertPreparationMosData()));

                // Set ttIncriteria parameter
                List<SoamoPPOTtIncriteria> ttIncriteria = new ArrayList<SoamoPPOTtIncriteria>();
                ttIncriteria = converter.convertMOCriteriaToTtCriv(request.getInsertPreparationMosData()
                        .getInsertMosData().getCriteria());

                // Create response holder
                TempTableHolder<SoamoPPOTtInsertresponse> statusResponseHolder = new TempTableHolder<SoamoPPOTtInsertresponse>();

                // Call appServer
                // getSoamoPPO().insertFabricationMos(ctx, ttMoInsert, ttIncriteria, statusResponseHolder);
                getSoamoPPO().insertPreparationMos(ctx, ttMoInsert, ttIncriteria, statusResponseHolder);

                // Standard Response
                ret.setStandardResponse(converter.convertStatusInsertResponseToStandardResponse(statusResponseHolder));

                // Insert Response : the moKey inserted
                ret.setInsertPreparationMosResponse(converter
                        .convertSOAInsertPreparationResponseToInsertPreparationResponse(statusResponseHolder));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                throw new WebServiceException(e);
            } catch (DAOException e) {
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertPreparationMos(request=" + request + ")=" + request);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReadMosResponse readMos(final ReadMosRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - readMos(request=" + request + ")");
        }

        ReadMosResponse ret = new ReadMosResponse();

        VIFContext ctx;
        try {
            // Check input
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOConverter.getInstance();

                // Set ttMoSelection parameter
                List<SoamoPPOTtMoselection> ttMoSelection = new ArrayList<SoamoPPOTtMoselection>();
                ttMoSelection.add(converter.convertSelectionToTt(request.getReadMosSelection().getMosSelection()));

                // Set ttIncriteria parameter
                List<SoamoPPOTtIncriteria> ttIncriteria = new ArrayList<SoamoPPOTtIncriteria>();
                ttIncriteria = converter.convertMOCriteriaToTtCriv(request.getReadMosSelection().getMosSelection()
                        .getCriteria());

                // Declare out parameters
                TempTableHolder<SoamoPPOTtReadresponse> statusResponseHolder = new TempTableHolder<SoamoPPOTtReadresponse>();
                TempTableHolder<SoamoPPOTtMoresponse> ttMoresponse = new TempTableHolder<SoamoPPOTtMoresponse>();
                TempTableHolder<SoamoPPOTtOutcriteria> ttOutcriteria = new TempTableHolder<SoamoPPOTtOutcriteria>();

                // Call appServer
                getSoamoPPO().readMos(ctx, ttMoSelection, ttIncriteria, ttMoresponse, ttOutcriteria,
                        statusResponseHolder);

                // Standard Response
                ret.setStandardResponse(converter.convertStatusReadResponseToStandardResponse(statusResponseHolder));

                // Read Response : the MOs with their criteria
                ret.setReadMosResponse(converter.convertSOAReadResponseToReadResponse(ttMoresponse, ttOutcriteria));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                throw new WebServiceException(e);
            } catch (DAOException e) {
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - readMos(request=" + request + ")=" + ret);
        }
        return ret;

    }

    /**
     * Sets the soamoPPO.
     * 
     * @param soamoPPO soamoPPO.
     */
    public void setSoamoPPO(final SoamoPPO soamoPPO) {
        this.soamoPPO = soamoPPO;
    }

}

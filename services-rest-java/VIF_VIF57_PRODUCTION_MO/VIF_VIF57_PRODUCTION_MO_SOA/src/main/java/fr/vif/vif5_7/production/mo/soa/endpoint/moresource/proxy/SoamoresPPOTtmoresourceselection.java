/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoresPPOTtmoresourceselection.java,v $
 * Created on 12 déc. 2014 by nle
 * Revision: $Revision: 1.4 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamorespxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.4 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoresPPOTtmoresourceselection extends AbstractTempTable implements Serializable {


    private String cact = "";

    private String cetab = "";

    private int chrono = 0;

    private String cofuse = "";

    private String coper = "";

    private String cres = "";

    private String creskoto = "";

    private String crub = "";

    private String csecteur = "";

    private String csoc = "";

    private Date datprod = null;

    private String ligne = "";

    private int ni1 = 0;

    private int ni2 = 0;

    private int ni4 = 0;

    private int nlig = 0;

    private String prechro = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoresPPOTtmoresourceselection() {
        super();
    }

    /**
     * Gets the cact.
     *
     * @category getter
     * @return the cact.
     */
    public final String getCact() {
        return cact;
    }

    /**
     * Sets the cact.
     *
     * @category setter
     * @param cact cact.
     */
    public final void setCact(final String cact) {
        this.cact = cact;
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the chrono.
     *
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Sets the chrono.
     *
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Gets the cofuse.
     *
     * @category getter
     * @return the cofuse.
     */
    public final String getCofuse() {
        return cofuse;
    }

    /**
     * Sets the cofuse.
     *
     * @category setter
     * @param cofuse cofuse.
     */
    public final void setCofuse(final String cofuse) {
        this.cofuse = cofuse;
    }

    /**
     * Gets the coper.
     *
     * @category getter
     * @return the coper.
     */
    public final String getCoper() {
        return coper;
    }

    /**
     * Sets the coper.
     *
     * @category setter
     * @param coper coper.
     */
    public final void setCoper(final String coper) {
        this.coper = coper;
    }

    /**
     * Gets the cres.
     *
     * @category getter
     * @return the cres.
     */
    public final String getCres() {
        return cres;
    }

    /**
     * Sets the cres.
     *
     * @category setter
     * @param cres cres.
     */
    public final void setCres(final String cres) {
        this.cres = cres;
    }

    /**
     * Gets the creskoto.
     *
     * @category getter
     * @return the creskoto.
     */
    public final String getCreskoto() {
        return creskoto;
    }

    /**
     * Sets the creskoto.
     *
     * @category setter
     * @param creskoto creskoto.
     */
    public final void setCreskoto(final String creskoto) {
        this.creskoto = creskoto;
    }

    /**
     * Gets the crub.
     *
     * @category getter
     * @return the crub.
     */
    public final String getCrub() {
        return crub;
    }

    /**
     * Sets the crub.
     *
     * @category setter
     * @param crub crub.
     */
    public final void setCrub(final String crub) {
        this.crub = crub;
    }

    /**
     * Gets the csecteur.
     *
     * @category getter
     * @return the csecteur.
     */
    public final String getCsecteur() {
        return csecteur;
    }

    /**
     * Sets the csecteur.
     *
     * @category setter
     * @param csecteur csecteur.
     */
    public final void setCsecteur(final String csecteur) {
        this.csecteur = csecteur;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the datprod.
     *
     * @category getter
     * @return the datprod.
     */
    public final Date getDatprod() {
        return datprod;
    }

    /**
     * Sets the datprod.
     *
     * @category setter
     * @param datprod datprod.
     */
    public final void setDatprod(final Date datprod) {
        this.datprod = datprod;
    }

    /**
     * Gets the ligne.
     *
     * @category getter
     * @return the ligne.
     */
    public final String getLigne() {
        return ligne;
    }

    /**
     * Sets the ligne.
     *
     * @category setter
     * @param ligne ligne.
     */
    public final void setLigne(final String ligne) {
        this.ligne = ligne;
    }

    /**
     * Gets the ni1.
     *
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Sets the ni1.
     *
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Gets the ni2.
     *
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Sets the ni2.
     *
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Gets the ni4.
     *
     * @category getter
     * @return the ni4.
     */
    public final int getNi4() {
        return ni4;
    }

    /**
     * Sets the ni4.
     *
     * @category setter
     * @param ni4 ni4.
     */
    public final void setNi4(final int ni4) {
        this.ni4 = ni4;
    }

    /**
     * Gets the nlig.
     *
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Sets the nlig.
     *
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Gets the prechro.
     *
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Sets the prechro.
     *
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

}

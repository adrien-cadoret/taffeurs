/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoresPPOTtmoresourcereadresponse.java,v $
 * Created on 12 déc. 2014 by nle
 * Revision: $Revision: 1.4 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamorespxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.4 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoresPPOTtmoresourcereadresponse extends AbstractTempTable implements Serializable {


    private String cetab = "";

    private int chrono = 0;

    private String cmotmvk = "";

    private String coper = "";

    private String cres = "";

    private String creskoto = "";

    private String crub = "";

    private String csecteur = "";

    private String csoc = "";

    private String cuser = "";

    private Date datdebr = null;

    private Date datfinr = null;

    private Date datmod = null;

    private double duree2 = 0;

    private int heurdebr = 0;

    private int heurfinr = 0;

    private int heurmod = 0;

    private String lmotmvk = "";

    private String loper = "";

    private String lres = "";

    private String lreskoto = "";

    private String lrub = "";

    private String lsecteur = "";

    private double nbr = 0;

    private int ni1 = 0;

    private int ni2 = 0;

    private int ni4 = 0;

    private int nlig = 0;

    private String prechro = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoresPPOTtmoresourcereadresponse() {
        super();
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the chrono.
     *
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Sets the chrono.
     *
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Gets the cmotmvk.
     *
     * @category getter
     * @return the cmotmvk.
     */
    public final String getCmotmvk() {
        return cmotmvk;
    }

    /**
     * Sets the cmotmvk.
     *
     * @category setter
     * @param cmotmvk cmotmvk.
     */
    public final void setCmotmvk(final String cmotmvk) {
        this.cmotmvk = cmotmvk;
    }

    /**
     * Gets the coper.
     *
     * @category getter
     * @return the coper.
     */
    public final String getCoper() {
        return coper;
    }

    /**
     * Sets the coper.
     *
     * @category setter
     * @param coper coper.
     */
    public final void setCoper(final String coper) {
        this.coper = coper;
    }

    /**
     * Gets the cres.
     *
     * @category getter
     * @return the cres.
     */
    public final String getCres() {
        return cres;
    }

    /**
     * Sets the cres.
     *
     * @category setter
     * @param cres cres.
     */
    public final void setCres(final String cres) {
        this.cres = cres;
    }

    /**
     * Gets the creskoto.
     *
     * @category getter
     * @return the creskoto.
     */
    public final String getCreskoto() {
        return creskoto;
    }

    /**
     * Sets the creskoto.
     *
     * @category setter
     * @param creskoto creskoto.
     */
    public final void setCreskoto(final String creskoto) {
        this.creskoto = creskoto;
    }

    /**
     * Gets the crub.
     *
     * @category getter
     * @return the crub.
     */
    public final String getCrub() {
        return crub;
    }

    /**
     * Sets the crub.
     *
     * @category setter
     * @param crub crub.
     */
    public final void setCrub(final String crub) {
        this.crub = crub;
    }

    /**
     * Gets the csecteur.
     *
     * @category getter
     * @return the csecteur.
     */
    public final String getCsecteur() {
        return csecteur;
    }

    /**
     * Sets the csecteur.
     *
     * @category setter
     * @param csecteur csecteur.
     */
    public final void setCsecteur(final String csecteur) {
        this.csecteur = csecteur;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the cuser.
     *
     * @category getter
     * @return the cuser.
     */
    public final String getCuser() {
        return cuser;
    }

    /**
     * Sets the cuser.
     *
     * @category setter
     * @param cuser cuser.
     */
    public final void setCuser(final String cuser) {
        this.cuser = cuser;
    }

    /**
     * Gets the datdebr.
     *
     * @category getter
     * @return the datdebr.
     */
    public final Date getDatdebr() {
        return datdebr;
    }

    /**
     * Sets the datdebr.
     *
     * @category setter
     * @param datdebr datdebr.
     */
    public final void setDatdebr(final Date datdebr) {
        this.datdebr = datdebr;
    }

    /**
     * Gets the datfinr.
     *
     * @category getter
     * @return the datfinr.
     */
    public final Date getDatfinr() {
        return datfinr;
    }

    /**
     * Sets the datfinr.
     *
     * @category setter
     * @param datfinr datfinr.
     */
    public final void setDatfinr(final Date datfinr) {
        this.datfinr = datfinr;
    }

    /**
     * Gets the datmod.
     *
     * @category getter
     * @return the datmod.
     */
    public final Date getDatmod() {
        return datmod;
    }

    /**
     * Sets the datmod.
     *
     * @category setter
     * @param datmod datmod.
     */
    public final void setDatmod(final Date datmod) {
        this.datmod = datmod;
    }

    /**
     * Gets the duree2.
     *
     * @category getter
     * @return the duree2.
     */
    public final double getDuree2() {
        return duree2;
    }

    /**
     * Sets the duree2.
     *
     * @category setter
     * @param duree2 duree2.
     */
    public final void setDuree2(final double duree2) {
        this.duree2 = duree2;
    }

    /**
     * Gets the heurdebr.
     *
     * @category getter
     * @return the heurdebr.
     */
    public final int getHeurdebr() {
        return heurdebr;
    }

    /**
     * Sets the heurdebr.
     *
     * @category setter
     * @param heurdebr heurdebr.
     */
    public final void setHeurdebr(final int heurdebr) {
        this.heurdebr = heurdebr;
    }

    /**
     * Gets the heurfinr.
     *
     * @category getter
     * @return the heurfinr.
     */
    public final int getHeurfinr() {
        return heurfinr;
    }

    /**
     * Sets the heurfinr.
     *
     * @category setter
     * @param heurfinr heurfinr.
     */
    public final void setHeurfinr(final int heurfinr) {
        this.heurfinr = heurfinr;
    }

    /**
     * Gets the heurmod.
     *
     * @category getter
     * @return the heurmod.
     */
    public final int getHeurmod() {
        return heurmod;
    }

    /**
     * Sets the heurmod.
     *
     * @category setter
     * @param heurmod heurmod.
     */
    public final void setHeurmod(final int heurmod) {
        this.heurmod = heurmod;
    }

    /**
     * Gets the lmotmvk.
     *
     * @category getter
     * @return the lmotmvk.
     */
    public final String getLmotmvk() {
        return lmotmvk;
    }

    /**
     * Sets the lmotmvk.
     *
     * @category setter
     * @param lmotmvk lmotmvk.
     */
    public final void setLmotmvk(final String lmotmvk) {
        this.lmotmvk = lmotmvk;
    }

    /**
     * Gets the loper.
     *
     * @category getter
     * @return the loper.
     */
    public final String getLoper() {
        return loper;
    }

    /**
     * Sets the loper.
     *
     * @category setter
     * @param loper loper.
     */
    public final void setLoper(final String loper) {
        this.loper = loper;
    }

    /**
     * Gets the lres.
     *
     * @category getter
     * @return the lres.
     */
    public final String getLres() {
        return lres;
    }

    /**
     * Sets the lres.
     *
     * @category setter
     * @param lres lres.
     */
    public final void setLres(final String lres) {
        this.lres = lres;
    }

    /**
     * Gets the lreskoto.
     *
     * @category getter
     * @return the lreskoto.
     */
    public final String getLreskoto() {
        return lreskoto;
    }

    /**
     * Sets the lreskoto.
     *
     * @category setter
     * @param lreskoto lreskoto.
     */
    public final void setLreskoto(final String lreskoto) {
        this.lreskoto = lreskoto;
    }

    /**
     * Gets the lrub.
     *
     * @category getter
     * @return the lrub.
     */
    public final String getLrub() {
        return lrub;
    }

    /**
     * Sets the lrub.
     *
     * @category setter
     * @param lrub lrub.
     */
    public final void setLrub(final String lrub) {
        this.lrub = lrub;
    }

    /**
     * Gets the lsecteur.
     *
     * @category getter
     * @return the lsecteur.
     */
    public final String getLsecteur() {
        return lsecteur;
    }

    /**
     * Sets the lsecteur.
     *
     * @category setter
     * @param lsecteur lsecteur.
     */
    public final void setLsecteur(final String lsecteur) {
        this.lsecteur = lsecteur;
    }

    /**
     * Gets the nbr.
     *
     * @category getter
     * @return the nbr.
     */
    public final double getNbr() {
        return nbr;
    }

    /**
     * Sets the nbr.
     *
     * @category setter
     * @param nbr nbr.
     */
    public final void setNbr(final double nbr) {
        this.nbr = nbr;
    }

    /**
     * Gets the ni1.
     *
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Sets the ni1.
     *
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Gets the ni2.
     *
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Sets the ni2.
     *
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Gets the ni4.
     *
     * @category getter
     * @return the ni4.
     */
    public final int getNi4() {
        return ni4;
    }

    /**
     * Sets the ni4.
     *
     * @category setter
     * @param ni4 ni4.
     */
    public final void setNi4(final int ni4) {
        this.ni4 = ni4;
    }

    /**
     * Gets the nlig.
     *
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Sets the nlig.
     *
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Gets the prechro.
     *
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Sets the prechro.
     *
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

}

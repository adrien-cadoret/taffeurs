/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 4 août 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.mo;


import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import fr.vif.vif5_7.gen.comment.business.beans.common.CommentKey;


/**
 * Comment Data Transfer Object.
 *
 * @author ac
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class CommentDTO {

    private String     com = "";
    private String     cupdateUser;
    private CommentKey key = new CommentKey();
    private Date       updateDate;

    /**
     * Gets the com.
     * 
     * @category getter
     * @return the com.
     */
    public String getCom() {
        return com;
    }

    /**
     * Gets the cupdateUser.
     * 
     * @category getter
     * @return the cupdateUser.
     */
    public String getCupdateUser() {
        return cupdateUser;
    }

    /**
     * Gets the key.
     * 
     * @category getter
     * @return the key.
     */
    public CommentKey getKey() {
        return key;
    }

    /**
     * Gets the updateDate.
     * 
     * @category getter
     * @return the updateDate.
     */
    public Date getUpdateDate() {
        return updateDate;
    }

    /**
     * Sets the com.
     * 
     * @category setter
     * @param com com.
     */
    public void setCom(final String com) {
        this.com = com;
    }

    /**
     * Sets the cupdateUser.
     * 
     * @category setter
     * @param cupdateUser cupdateUser.
     */
    public void setCupdateUser(final String cupdateUser) {
        this.cupdateUser = cupdateUser;
    }

    /**
     * Sets the key.
     * 
     * @category setter
     * @param key key.
     */
    public void setKey(final CommentKey key) {
        this.key = key;
    }

    /**
     * Sets the updateDate.
     * 
     * @category setter
     * @param updateDate updateDate.
     */
    public void setUpdateDate(final Date updateDate) {
        this.updateDate = updateDate;
    }

}

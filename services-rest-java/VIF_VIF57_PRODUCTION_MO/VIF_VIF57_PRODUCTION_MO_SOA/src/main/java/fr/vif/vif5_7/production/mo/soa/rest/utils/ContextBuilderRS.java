/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on Jul 19, 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils;


import javax.ws.rs.core.HttpHeaders;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.common.IdContextGen;
import fr.vif.jtech.common.util.i18n.UntranslatedClassConstant;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;


/**
 * TODO Write the class' description
 *
 * @author ac
 */
@Component
public class ContextBuilderRS {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(ContextBuilderRS.class);

    // @Autowired
    // private CLoginValidator cloginValidator;
    //
    // @Autowired
    // private ParameterSBS paramSBS;

    /**
     * build and check id context from http header request.
     * 
     * @param httpHeaders http header request.
     * @return id context.
     * @throws BusinessException when contect can't be build or accs is not authorized.
     */
    public IdContextGen buildAndCheckContext(final HttpHeaders httpHeaders) throws BusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - buildContext(httpHeaders=" + httpHeaders + ")");
        }

        IdContextGen idContext = null;

        idContext = buildContext(httpHeaders);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - buildContext(httpHeaders=" + httpHeaders + ")=" + idContext);
        }
        return idContext;
    }


    /**
     * build id context from http header request.
     * 
     * @param httpHeaders http header request.
     * @return id context.
     * @throws BusinessException when contect can't be build
     */
    IdContextGen buildContext(final HttpHeaders httpHeaders) throws BusinessException {
        IdContextGen idContext;
        idContext = extractIdContext(httpHeaders);
        if (idContext == null) {
            throw new BusinessException(new UntranslatedClassConstant(), new VarParamTranslation(
                    "IdContext Error : VIFERR_001"));
        }
        return idContext;
    }


    /**
     * Extract the Idcontext from the httpHeaders.
     * 
     * @param httpHeaders httpHeadrers request
     * @return idContext. null if no idContext is found in the httpHeader.
     */
    private IdContextGen extractIdContext(final HttpHeaders httpHeaders) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - extractIdContext(httpHeaders=" + httpHeaders + ")");
        }

        IdContextGen idContext = null;
        String httpContext = httpHeaders.getRequestHeaders().getFirst("X-VIFContext");
        if (httpContext != null) {
            String[] ctxArray = httpContext.split(",");
            if (ctxArray.length == 4) {
                idContext = new IdContextGen(ctxArray[1], ctxArray[2], ctxArray[3], ctxArray[0]);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - extractIdContext(httpHeaders=" + httpHeaders + ")="
                    + (idContext == null ? "null" : idContext.toStrings()));
        }
        return idContext;
    }

}

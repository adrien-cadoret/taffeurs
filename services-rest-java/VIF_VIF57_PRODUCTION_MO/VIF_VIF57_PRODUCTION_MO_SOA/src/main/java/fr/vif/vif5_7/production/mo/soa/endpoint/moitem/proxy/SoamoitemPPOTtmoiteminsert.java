/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoitemPPOTtmoiteminsert.java,v $
 * Created on 24 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamoitempxy.p.xml.
 * 
 * @author vl
 */
public class SoamoitemPPOTtmoiteminsert extends AbstractTempTable implements Serializable {

    private String  cact       = "";

    private String  cartprev   = "";

    private String  cartr      = "";

    private String  cclelien   = "";

    private String  ccond      = "";

    private String  ccondp     = "";

    private String  cdepot     = "";

    private String  cemp       = "";

    private String  cetab      = "";

    private int     chrono     = 0;

    private String  cmotmvk    = "";

    private String  cnatstk    = "";

    private String  cofuse     = "";

    private String  cposte     = "";

    private String  csoc       = "";

    private String  csubdiv    = "";

    private String  csubdivp   = "";

    private String  cu1        = "";

    private String  cu2        = "";

    private String  cu3        = "";

    private String  cunitar    = "";

    private String  cunitarp   = "";

    private String  cuser      = "";

    private Date    datmvt     = null;

    private Date    datprod    = null;

    private int     heurmvt    = 0;

    private String  lot        = "";

    private int     ni1        = 0;

    private int     ni2        = 0;

    private String  notat      = "";

    private String  notatp     = "";

    private String  nsc        = "";

    private String  nscp       = "";

    private String  nsp        = "";

    private String  prechro    = "";

    private double  qte1       = 0;

    private double  qte2       = 0;

    private double  qte3       = 0;

    private String  sscc       = "";

    private String  ssccp      = "";

    private double  tare       = 0;

    private double  tarep      = 0;

    private boolean tfermensc  = false;

    private boolean tfermenscp = false;

    private String  typacta    = "";

    /**
     * Default Constructor.
     * 
     */
    public SoamoitemPPOTtmoiteminsert() {
        super();
    }

    /**
     * Gets the cact.
     * 
     * @category getter
     * @return the cact.
     */
    public final String getCact() {
        return cact;
    }

    /**
     * Gets the cartprev.
     * 
     * @category getter
     * @return the cartprev.
     */
    public final String getCartprev() {
        return cartprev;
    }

    /**
     * Gets the cartr.
     * 
     * @category getter
     * @return the cartr.
     */
    public final String getCartr() {
        return cartr;
    }

    /**
     * Gets the cclelien.
     * 
     * @category getter
     * @return the cclelien.
     */
    public final String getCclelien() {
        return cclelien;
    }

    /**
     * Gets the ccond.
     * 
     * @category getter
     * @return the ccond.
     */
    public final String getCcond() {
        return ccond;
    }

    /**
     * Gets the ccondp.
     * 
     * @category getter
     * @return the ccondp.
     */
    public final String getCcondp() {
        return ccondp;
    }

    /**
     * Gets the cdepot.
     * 
     * @category getter
     * @return the cdepot.
     */
    public final String getCdepot() {
        return cdepot;
    }

    /**
     * Gets the cemp.
     * 
     * @category getter
     * @return the cemp.
     */
    public final String getCemp() {
        return cemp;
    }

    /**
     * Gets the cetab.
     * 
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Gets the chrono.
     * 
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Gets the cmotmvk.
     * 
     * @category getter
     * @return the cmotmvk.
     */
    public final String getCmotmvk() {
        return cmotmvk;
    }

    /**
     * Gets the cnatstk.
     * 
     * @category getter
     * @return the cnatstk.
     */
    public final String getCnatstk() {
        return cnatstk;
    }

    /**
     * Gets the cofuse.
     * 
     * @category getter
     * @return the cofuse.
     */
    public final String getCofuse() {
        return cofuse;
    }

    /**
     * Gets the cposte.
     * 
     * @category getter
     * @return the cposte.
     */
    public final String getCposte() {
        return cposte;
    }

    /**
     * Gets the csoc.
     * 
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Gets the csubdiv.
     * 
     * @category getter
     * @return the csubdiv.
     */
    public final String getCsubdiv() {
        return csubdiv;
    }

    /**
     * Gets the csubdivp.
     * 
     * @category getter
     * @return the csubdivp.
     */
    public final String getCsubdivp() {
        return csubdivp;
    }

    /**
     * Gets the cu1.
     * 
     * @category getter
     * @return the cu1.
     */
    public final String getCu1() {
        return cu1;
    }

    /**
     * Gets the cu2.
     * 
     * @category getter
     * @return the cu2.
     */
    public final String getCu2() {
        return cu2;
    }

    /**
     * Gets the cu3.
     * 
     * @category getter
     * @return the cu3.
     */
    public final String getCu3() {
        return cu3;
    }

    /**
     * Gets the cunitar.
     * 
     * @category getter
     * @return the cunitar.
     */
    public final String getCunitar() {
        return cunitar;
    }

    /**
     * Gets the cunitarp.
     * 
     * @category getter
     * @return the cunitarp.
     */
    public final String getCunitarp() {
        return cunitarp;
    }

    /**
     * Gets the cuser.
     * 
     * @category getter
     * @return the cuser.
     */
    public final String getCuser() {
        return cuser;
    }

    /**
     * Gets the datmvt.
     * 
     * @category getter
     * @return the datmvt.
     */
    public final Date getDatmvt() {
        return datmvt;
    }

    /**
     * Gets the datprod.
     * 
     * @category getter
     * @return the datprod.
     */
    public final Date getDatprod() {
        return datprod;
    }

    /**
     * Gets the heurmvt.
     * 
     * @category getter
     * @return the heurmvt.
     */
    public final int getHeurmvt() {
        return heurmvt;
    }

    /**
     * Gets the lot.
     * 
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Gets the ni1.
     * 
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Gets the ni2.
     * 
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Gets the notat.
     * 
     * @category getter
     * @return the notat.
     */
    public final String getNotat() {
        return notat;
    }

    /**
     * Gets the notatp.
     * 
     * @category getter
     * @return the notatp.
     */
    public final String getNotatp() {
        return notatp;
    }

    /**
     * Gets the nsc.
     * 
     * @category getter
     * @return the nsc.
     */
    public final String getNsc() {
        return nsc;
    }

    /**
     * Gets the nscp.
     * 
     * @category getter
     * @return the nscp.
     */
    public final String getNscp() {
        return nscp;
    }

    /**
     * Gets the nsp.
     * 
     * @category getter
     * @return the nsp.
     */
    public final String getNsp() {
        return nsp;
    }

    /**
     * Gets the prechro.
     * 
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Gets the qte1.
     * 
     * @category getter
     * @return the qte1.
     */
    public final double getQte1() {
        return qte1;
    }

    /**
     * Gets the qte2.
     * 
     * @category getter
     * @return the qte2.
     */
    public final double getQte2() {
        return qte2;
    }

    /**
     * Gets the qte3.
     * 
     * @category getter
     * @return the qte3.
     */
    public final double getQte3() {
        return qte3;
    }

    /**
     * Gets the sscc.
     * 
     * @category getter
     * @return the sscc.
     */
    public final String getSscc() {
        return sscc;
    }

    /**
     * Gets the ssccp.
     * 
     * @category getter
     * @return the ssccp.
     */
    public final String getSsccp() {
        return ssccp;
    }

    /**
     * Gets the tare.
     * 
     * @category getter
     * @return the tare.
     */
    public final double getTare() {
        return tare;
    }

    /**
     * Gets the tarep.
     * 
     * @category getter
     * @return the tarep.
     */
    public final double getTarep() {
        return tarep;
    }

    /**
     * Gets the tfermensc.
     * 
     * @category getter
     * @return the tfermensc.
     */
    public final boolean getTfermensc() {
        return tfermensc;
    }

    /**
     * Gets the tfermenscp.
     * 
     * @category getter
     * @return the tfermenscp.
     */
    public final boolean getTfermenscp() {
        return tfermenscp;
    }

    /**
     * Gets the typacta.
     * 
     * @category getter
     * @return the typacta.
     */
    public final String getTypacta() {
        return typacta;
    }

    /**
     * Sets the cact.
     * 
     * @category setter
     * @param cact cact.
     */
    public final void setCact(final String cact) {
        this.cact = cact;
    }

    /**
     * Sets the cartprev.
     * 
     * @category setter
     * @param cartprev cartprev.
     */
    public final void setCartprev(final String cartprev) {
        this.cartprev = cartprev;
    }

    /**
     * Sets the cartr.
     * 
     * @category setter
     * @param cartr cartr.
     */
    public final void setCartr(final String cartr) {
        this.cartr = cartr;
    }

    /**
     * Sets the cclelien.
     * 
     * @category setter
     * @param cclelien cclelien.
     */
    public final void setCclelien(final String cclelien) {
        this.cclelien = cclelien;
    }

    /**
     * Sets the ccond.
     * 
     * @category setter
     * @param ccond ccond.
     */
    public final void setCcond(final String ccond) {
        this.ccond = ccond;
    }

    /**
     * Sets the ccondp.
     * 
     * @category setter
     * @param ccondp ccondp.
     */
    public final void setCcondp(final String ccondp) {
        this.ccondp = ccondp;
    }

    /**
     * Sets the cdepot.
     * 
     * @category setter
     * @param cdepot cdepot.
     */
    public final void setCdepot(final String cdepot) {
        this.cdepot = cdepot;
    }

    /**
     * Sets the cemp.
     * 
     * @category setter
     * @param cemp cemp.
     */
    public final void setCemp(final String cemp) {
        this.cemp = cemp;
    }

    /**
     * Sets the cetab.
     * 
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Sets the chrono.
     * 
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Sets the cmotmvk.
     * 
     * @category setter
     * @param cmotmvk cmotmvk.
     */
    public final void setCmotmvk(final String cmotmvk) {
        this.cmotmvk = cmotmvk;
    }

    /**
     * Sets the cnatstk.
     * 
     * @category setter
     * @param cnatstk cnatstk.
     */
    public final void setCnatstk(final String cnatstk) {
        this.cnatstk = cnatstk;
    }

    /**
     * Sets the cofuse.
     * 
     * @category setter
     * @param cofuse cofuse.
     */
    public final void setCofuse(final String cofuse) {
        this.cofuse = cofuse;
    }

    /**
     * Sets the cposte.
     * 
     * @category setter
     * @param cposte cposte.
     */
    public final void setCposte(final String cposte) {
        this.cposte = cposte;
    }

    /**
     * Sets the csoc.
     * 
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Sets the csubdiv.
     * 
     * @category setter
     * @param csubdiv csubdiv.
     */
    public final void setCsubdiv(final String csubdiv) {
        this.csubdiv = csubdiv;
    }

    /**
     * Sets the csubdivp.
     * 
     * @category setter
     * @param csubdivp csubdivp.
     */
    public final void setCsubdivp(final String csubdivp) {
        this.csubdivp = csubdivp;
    }

    /**
     * Sets the cu1.
     * 
     * @category setter
     * @param cu1 cu1.
     */
    public final void setCu1(final String cu1) {
        this.cu1 = cu1;
    }

    /**
     * Sets the cu2.
     * 
     * @category setter
     * @param cu2 cu2.
     */
    public final void setCu2(final String cu2) {
        this.cu2 = cu2;
    }

    /**
     * Sets the cu3.
     * 
     * @category setter
     * @param cu3 cu3.
     */
    public final void setCu3(final String cu3) {
        this.cu3 = cu3;
    }

    /**
     * Sets the cunitar.
     * 
     * @category setter
     * @param cunitar cunitar.
     */
    public final void setCunitar(final String cunitar) {
        this.cunitar = cunitar;
    }

    /**
     * Sets the cunitarp.
     * 
     * @category setter
     * @param cunitarp cunitarp.
     */
    public final void setCunitarp(final String cunitarp) {
        this.cunitarp = cunitarp;
    }

    /**
     * Sets the cuser.
     * 
     * @category setter
     * @param cuser cuser.
     */
    public final void setCuser(final String cuser) {
        this.cuser = cuser;
    }

    /**
     * Sets the datmvt.
     * 
     * @category setter
     * @param datmvt datmvt.
     */
    public final void setDatmvt(final Date datmvt) {
        this.datmvt = datmvt;
    }

    /**
     * Sets the datprod.
     * 
     * @category setter
     * @param datprod datprod.
     */
    public final void setDatprod(final Date datprod) {
        this.datprod = datprod;
    }

    /**
     * Sets the heurmvt.
     * 
     * @category setter
     * @param heurmvt heurmvt.
     */
    public final void setHeurmvt(final int heurmvt) {
        this.heurmvt = heurmvt;
    }

    /**
     * Sets the lot.
     * 
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Sets the ni1.
     * 
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Sets the ni2.
     * 
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Sets the notat.
     * 
     * @category setter
     * @param notat notat.
     */
    public final void setNotat(final String notat) {
        this.notat = notat;
    }

    /**
     * Sets the notatp.
     * 
     * @category setter
     * @param notatp notatp.
     */
    public final void setNotatp(final String notatp) {
        this.notatp = notatp;
    }

    /**
     * Sets the nsc.
     * 
     * @category setter
     * @param nsc nsc.
     */
    public final void setNsc(final String nsc) {
        this.nsc = nsc;
    }

    /**
     * Sets the nscp.
     * 
     * @category setter
     * @param nscp nscp.
     */
    public final void setNscp(final String nscp) {
        this.nscp = nscp;
    }

    /**
     * Sets the nsp.
     * 
     * @category setter
     * @param nsp nsp.
     */
    public final void setNsp(final String nsp) {
        this.nsp = nsp;
    }

    /**
     * Sets the prechro.
     * 
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

    /**
     * Sets the qte1.
     * 
     * @category setter
     * @param qte1 qte1.
     */
    public final void setQte1(final double qte1) {
        this.qte1 = qte1;
    }

    /**
     * Sets the qte2.
     * 
     * @category setter
     * @param qte2 qte2.
     */
    public final void setQte2(final double qte2) {
        this.qte2 = qte2;
    }

    /**
     * Sets the qte3.
     * 
     * @category setter
     * @param qte3 qte3.
     */
    public final void setQte3(final double qte3) {
        this.qte3 = qte3;
    }

    /**
     * Sets the sscc.
     * 
     * @category setter
     * @param sscc sscc.
     */
    public final void setSscc(final String sscc) {
        this.sscc = sscc;
    }

    /**
     * Sets the ssccp.
     * 
     * @category setter
     * @param ssccp ssccp.
     */
    public final void setSsccp(final String ssccp) {
        this.ssccp = ssccp;
    }

    /**
     * Sets the tare.
     * 
     * @category setter
     * @param tare tare.
     */
    public final void setTare(final double tare) {
        this.tare = tare;
    }

    /**
     * Sets the tarep.
     * 
     * @category setter
     * @param tarep tarep.
     */
    public final void setTarep(final double tarep) {
        this.tarep = tarep;
    }

    /**
     * Sets the tfermensc.
     * 
     * @category setter
     * @param tfermensc tfermensc.
     */
    public final void setTfermensc(final boolean tfermensc) {
        this.tfermensc = tfermensc;
    }

    /**
     * Sets the tfermenscp.
     * 
     * @category setter
     * @param tfermenscp tfermenscp.
     */
    public final void setTfermenscp(final boolean tfermenscp) {
        this.tfermenscp = tfermenscp;
    }

    /**
     * Sets the typacta.
     * 
     * @category setter
     * @param typacta typacta.
     */
    public final void setTypacta(final String typacta) {
        this.typacta = typacta;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoitemPPOTtmoitemreadxmvtmresponse.java,v $
 * Created on 24 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamoitempxy.p.xml.
 * 
 * @author vl
 */
public class SoamoitemPPOTtmoitemreadxmvtmresponse extends AbstractTempTable implements Serializable {

    private String cart     = "";

    private String cdepot   = "";

    private String cemp     = "";

    private String cetab    = "";

    private int    chrono   = 0;

    private String cmotmvk  = "";

    private String cnatstk  = "";

    private String cposte   = "";

    private String csoc     = "";

    private String csubdiv  = "";

    private String csubdivp = "";

    private String cug      = "";

    private String cus      = "";

    private String cuser    = "";

    private String cut      = "";

    private Date   datgen   = null;

    private Date   datmvt   = null;

    private Date   datsto   = null;

    private int    heurgen  = 0;

    private int    heurmvt  = 0;

    private int    heursto  = 0;

    private String lart     = "";

    private String lmotmvk  = "";

    private String lot      = "";

    private int    ni1      = 0;

    private int    ni2      = 0;

    private int    ni3      = 0;

    private int    ni4      = 0;

    private int    nlig     = 0;

    private String notat    = "";

    private String notatp   = "";

    private String nsc      = "";

    private String nscp     = "";

    private String nsp      = "";

    private String prechro  = "";

    private double qug      = 0;

    private double qus      = 0;

    private double qut      = 0;

    private String rstock   = "";

    private String sens     = "";

    private String typacta  = "";

    private String typgest  = "";

    private String typmvt   = "";

    private String typres   = "";

    /**
     * Default Constructor.
     * 
     */
    public SoamoitemPPOTtmoitemreadxmvtmresponse() {
        super();
    }

    /**
     * Gets the cart.
     * 
     * @category getter
     * @return the cart.
     */
    public final String getCart() {
        return cart;
    }

    /**
     * Gets the cdepot.
     * 
     * @category getter
     * @return the cdepot.
     */
    public final String getCdepot() {
        return cdepot;
    }

    /**
     * Gets the cemp.
     * 
     * @category getter
     * @return the cemp.
     */
    public final String getCemp() {
        return cemp;
    }

    /**
     * Gets the cetab.
     * 
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Gets the chrono.
     * 
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Gets the cmotmvk.
     * 
     * @category getter
     * @return the cmotmvk.
     */
    public final String getCmotmvk() {
        return cmotmvk;
    }

    /**
     * Gets the cnatstk.
     * 
     * @category getter
     * @return the cnatstk.
     */
    public final String getCnatstk() {
        return cnatstk;
    }

    /**
     * Gets the cposte.
     * 
     * @category getter
     * @return the cposte.
     */
    public final String getCposte() {
        return cposte;
    }

    /**
     * Gets the csoc.
     * 
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Gets the csubdiv.
     * 
     * @category getter
     * @return the csubdiv.
     */
    public final String getCsubdiv() {
        return csubdiv;
    }

    /**
     * Gets the csubdivp.
     * 
     * @category getter
     * @return the csubdivp.
     */
    public final String getCsubdivp() {
        return csubdivp;
    }

    /**
     * Gets the cug.
     * 
     * @category getter
     * @return the cug.
     */
    public final String getCug() {
        return cug;
    }

    /**
     * Gets the cus.
     * 
     * @category getter
     * @return the cus.
     */
    public final String getCus() {
        return cus;
    }

    /**
     * Gets the cuser.
     * 
     * @category getter
     * @return the cuser.
     */
    public final String getCuser() {
        return cuser;
    }

    /**
     * Gets the cut.
     * 
     * @category getter
     * @return the cut.
     */
    public final String getCut() {
        return cut;
    }

    /**
     * Gets the datgen.
     * 
     * @category getter
     * @return the datgen.
     */
    public final Date getDatgen() {
        return datgen;
    }

    /**
     * Gets the datmvt.
     * 
     * @category getter
     * @return the datmvt.
     */
    public final Date getDatmvt() {
        return datmvt;
    }

    /**
     * Gets the datsto.
     * 
     * @category getter
     * @return the datsto.
     */
    public final Date getDatsto() {
        return datsto;
    }

    /**
     * Gets the heurgen.
     * 
     * @category getter
     * @return the heurgen.
     */
    public final int getHeurgen() {
        return heurgen;
    }

    /**
     * Gets the heurmvt.
     * 
     * @category getter
     * @return the heurmvt.
     */
    public final int getHeurmvt() {
        return heurmvt;
    }

    /**
     * Gets the heursto.
     * 
     * @category getter
     * @return the heursto.
     */
    public final int getHeursto() {
        return heursto;
    }

    /**
     * Gets the lart.
     * 
     * @category getter
     * @return the lart.
     */
    public final String getLart() {
        return lart;
    }

    /**
     * Gets the lmotmvk.
     * 
     * @category getter
     * @return the lmotmvk.
     */
    public final String getLmotmvk() {
        return lmotmvk;
    }

    /**
     * Gets the lot.
     * 
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Gets the ni1.
     * 
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Gets the ni2.
     * 
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Gets the ni3.
     * 
     * @category getter
     * @return the ni3.
     */
    public final int getNi3() {
        return ni3;
    }

    /**
     * Gets the ni4.
     * 
     * @category getter
     * @return the ni4.
     */
    public final int getNi4() {
        return ni4;
    }

    /**
     * Gets the nlig.
     * 
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Gets the notat.
     * 
     * @category getter
     * @return the notat.
     */
    public final String getNotat() {
        return notat;
    }

    /**
     * Gets the notatp.
     * 
     * @category getter
     * @return the notatp.
     */
    public final String getNotatp() {
        return notatp;
    }

    /**
     * Gets the nsc.
     * 
     * @category getter
     * @return the nsc.
     */
    public final String getNsc() {
        return nsc;
    }

    /**
     * Gets the nscp.
     * 
     * @category getter
     * @return the nscp.
     */
    public final String getNscp() {
        return nscp;
    }

    /**
     * Gets the nsp.
     * 
     * @category getter
     * @return the nsp.
     */
    public final String getNsp() {
        return nsp;
    }

    /**
     * Gets the prechro.
     * 
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Gets the qug.
     * 
     * @category getter
     * @return the qug.
     */
    public final double getQug() {
        return qug;
    }

    /**
     * Gets the qus.
     * 
     * @category getter
     * @return the qus.
     */
    public final double getQus() {
        return qus;
    }

    /**
     * Gets the qut.
     * 
     * @category getter
     * @return the qut.
     */
    public final double getQut() {
        return qut;
    }

    /**
     * Gets the rstock.
     * 
     * @category getter
     * @return the rstock.
     */
    public final String getRstock() {
        return rstock;
    }

    /**
     * Gets the sens.
     * 
     * @category getter
     * @return the sens.
     */
    public final String getSens() {
        return sens;
    }

    /**
     * Gets the typacta.
     * 
     * @category getter
     * @return the typacta.
     */
    public final String getTypacta() {
        return typacta;
    }

    /**
     * Gets the typgest.
     * 
     * @category getter
     * @return the typgest.
     */
    public final String getTypgest() {
        return typgest;
    }

    /**
     * Gets the typmvt.
     * 
     * @category getter
     * @return the typmvt.
     */
    public final String getTypmvt() {
        return typmvt;
    }

    /**
     * Gets the typres.
     * 
     * @category getter
     * @return the typres.
     */
    public final String getTypres() {
        return typres;
    }

    /**
     * Sets the cart.
     * 
     * @category setter
     * @param cart cart.
     */
    public final void setCart(final String cart) {
        this.cart = cart;
    }

    /**
     * Sets the cdepot.
     * 
     * @category setter
     * @param cdepot cdepot.
     */
    public final void setCdepot(final String cdepot) {
        this.cdepot = cdepot;
    }

    /**
     * Sets the cemp.
     * 
     * @category setter
     * @param cemp cemp.
     */
    public final void setCemp(final String cemp) {
        this.cemp = cemp;
    }

    /**
     * Sets the cetab.
     * 
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Sets the chrono.
     * 
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Sets the cmotmvk.
     * 
     * @category setter
     * @param cmotmvk cmotmvk.
     */
    public final void setCmotmvk(final String cmotmvk) {
        this.cmotmvk = cmotmvk;
    }

    /**
     * Sets the cnatstk.
     * 
     * @category setter
     * @param cnatstk cnatstk.
     */
    public final void setCnatstk(final String cnatstk) {
        this.cnatstk = cnatstk;
    }

    /**
     * Sets the cposte.
     * 
     * @category setter
     * @param cposte cposte.
     */
    public final void setCposte(final String cposte) {
        this.cposte = cposte;
    }

    /**
     * Sets the csoc.
     * 
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Sets the csubdiv.
     * 
     * @category setter
     * @param csubdiv csubdiv.
     */
    public final void setCsubdiv(final String csubdiv) {
        this.csubdiv = csubdiv;
    }

    /**
     * Sets the csubdivp.
     * 
     * @category setter
     * @param csubdivp csubdivp.
     */
    public final void setCsubdivp(final String csubdivp) {
        this.csubdivp = csubdivp;
    }

    /**
     * Sets the cug.
     * 
     * @category setter
     * @param cug cug.
     */
    public final void setCug(final String cug) {
        this.cug = cug;
    }

    /**
     * Sets the cus.
     * 
     * @category setter
     * @param cus cus.
     */
    public final void setCus(final String cus) {
        this.cus = cus;
    }

    /**
     * Sets the cuser.
     * 
     * @category setter
     * @param cuser cuser.
     */
    public final void setCuser(final String cuser) {
        this.cuser = cuser;
    }

    /**
     * Sets the cut.
     * 
     * @category setter
     * @param cut cut.
     */
    public final void setCut(final String cut) {
        this.cut = cut;
    }

    /**
     * Sets the datgen.
     * 
     * @category setter
     * @param datgen datgen.
     */
    public final void setDatgen(final Date datgen) {
        this.datgen = datgen;
    }

    /**
     * Sets the datmvt.
     * 
     * @category setter
     * @param datmvt datmvt.
     */
    public final void setDatmvt(final Date datmvt) {
        this.datmvt = datmvt;
    }

    /**
     * Sets the datsto.
     * 
     * @category setter
     * @param datsto datsto.
     */
    public final void setDatsto(final Date datsto) {
        this.datsto = datsto;
    }

    /**
     * Sets the heurgen.
     * 
     * @category setter
     * @param heurgen heurgen.
     */
    public final void setHeurgen(final int heurgen) {
        this.heurgen = heurgen;
    }

    /**
     * Sets the heurmvt.
     * 
     * @category setter
     * @param heurmvt heurmvt.
     */
    public final void setHeurmvt(final int heurmvt) {
        this.heurmvt = heurmvt;
    }

    /**
     * Sets the heursto.
     * 
     * @category setter
     * @param heursto heursto.
     */
    public final void setHeursto(final int heursto) {
        this.heursto = heursto;
    }

    /**
     * Sets the lart.
     * 
     * @category setter
     * @param lart lart.
     */
    public final void setLart(final String lart) {
        this.lart = lart;
    }

    /**
     * Sets the lmotmvk.
     * 
     * @category setter
     * @param lmotmvk lmotmvk.
     */
    public final void setLmotmvk(final String lmotmvk) {
        this.lmotmvk = lmotmvk;
    }

    /**
     * Sets the lot.
     * 
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Sets the ni1.
     * 
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Sets the ni2.
     * 
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Sets the ni3.
     * 
     * @category setter
     * @param ni3 ni3.
     */
    public final void setNi3(final int ni3) {
        this.ni3 = ni3;
    }

    /**
     * Sets the ni4.
     * 
     * @category setter
     * @param ni4 ni4.
     */
    public final void setNi4(final int ni4) {
        this.ni4 = ni4;
    }

    /**
     * Sets the nlig.
     * 
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Sets the notat.
     * 
     * @category setter
     * @param notat notat.
     */
    public final void setNotat(final String notat) {
        this.notat = notat;
    }

    /**
     * Sets the notatp.
     * 
     * @category setter
     * @param notatp notatp.
     */
    public final void setNotatp(final String notatp) {
        this.notatp = notatp;
    }

    /**
     * Sets the nsc.
     * 
     * @category setter
     * @param nsc nsc.
     */
    public final void setNsc(final String nsc) {
        this.nsc = nsc;
    }

    /**
     * Sets the nscp.
     * 
     * @category setter
     * @param nscp nscp.
     */
    public final void setNscp(final String nscp) {
        this.nscp = nscp;
    }

    /**
     * Sets the nsp.
     * 
     * @category setter
     * @param nsp nsp.
     */
    public final void setNsp(final String nsp) {
        this.nsp = nsp;
    }

    /**
     * Sets the prechro.
     * 
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

    /**
     * Sets the qug.
     * 
     * @category setter
     * @param qug qug.
     */
    public final void setQug(final double qug) {
        this.qug = qug;
    }

    /**
     * Sets the qus.
     * 
     * @category setter
     * @param qus qus.
     */
    public final void setQus(final double qus) {
        this.qus = qus;
    }

    /**
     * Sets the qut.
     * 
     * @category setter
     * @param qut qut.
     */
    public final void setQut(final double qut) {
        this.qut = qut;
    }

    /**
     * Sets the rstock.
     * 
     * @category setter
     * @param rstock rstock.
     */
    public final void setRstock(final String rstock) {
        this.rstock = rstock;
    }

    /**
     * Sets the sens.
     * 
     * @category setter
     * @param sens sens.
     */
    public final void setSens(final String sens) {
        this.sens = sens;
    }

    /**
     * Sets the typacta.
     * 
     * @category setter
     * @param typacta typacta.
     */
    public final void setTypacta(final String typacta) {
        this.typacta = typacta;
    }

    /**
     * Sets the typgest.
     * 
     * @category setter
     * @param typgest typgest.
     */
    public final void setTypgest(final String typgest) {
        this.typgest = typgest;
    }

    /**
     * Sets the typmvt.
     * 
     * @category setter
     * @param typmvt typmvt.
     */
    public final void setTypmvt(final String typmvt) {
        this.typmvt = typmvt;
    }

    /**
     * Sets the typres.
     * 
     * @category setter
     * @param typres typres.
     */
    public final void setTypres(final String typres) {
        this.typres = typres;
    }

}

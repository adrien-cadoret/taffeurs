/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoitemPPOTtmoitemstatusresponse.java,v $
 * Created on 24 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamoitempxy.p.xml.
 * 
 * @author vl
 */
public class SoamoitemPPOTtmoitemstatusresponse extends AbstractTempTable implements Serializable {

    private String cstatus = "";

    private String msg     = "";

    /**
     * Default Constructor.
     * 
     */
    public SoamoitemPPOTtmoitemstatusresponse() {
        super();
    }

    /**
     * Gets the cstatus.
     * 
     * @category getter
     * @return the cstatus.
     */
    public final String getCstatus() {
        return cstatus;
    }

    /**
     * Gets the msg.
     * 
     * @category getter
     * @return the msg.
     */
    public final String getMsg() {
        return msg;
    }

    /**
     * Sets the cstatus.
     * 
     * @category setter
     * @param cstatus cstatus.
     */
    public final void setCstatus(final String cstatus) {
        this.cstatus = cstatus;
    }

    /**
     * Sets the msg.
     * 
     * @category setter
     * @param msg msg.
     */
    public final void setMsg(final String msg) {
        this.msg = msg;
    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 20 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.moitem;


import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import fr.vif.jtech.common.util.ObjectHelper;


/**
 * Kotad Data Transfer Object.
 *
 * @author ac
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class KotadDTO {

    /**
     * Code Societe .
     */
    private String   csoc      = "";

    /**
     * Etablissement.
     */
    private String   cetab     = "";

    /**
     * Type du chrono.
     */
    private String   prechro   = "";

    /**
     * Chrono.
     */
    private int      chrono    = 0;

    /**
     * Numero Interne.
     */
    private int      ni1       = 0;

    /**
     * Numero Interne.
     */
    private int      ni2       = 0;

    /**
     * Nunéro interne 3.
     */
    private int      ni3       = 1;

    /**
     * Rang.
     */
    private int      nlig      = 0;

    /**
     * ENT -> Entrants SOR ->Sortants ART -> Articles de coût.
     */
    private String   typacta   = "";

    /**
     * Code Article.
     */
    private String   cart      = "";

    /**
     * Variante.
     */
    private String   cv        = "";

    /**
     * Quantite 1.
     */
    private double   qte1      = 0;

    /**
     * Quantite 2.
     */
    private double   qte2      = 0;

    /**
     * Quantite 3.
     */
    private double   qte3      = 0;

    /**
     * Unité 1.
     */
    private String   cu1       = "";

    /**
     * Unité 2.
     */
    private String   cu2       = "";

    /**
     * Unité 3.
     */
    private String   cu3       = "";

    /**
     * Code dépot.
     */
    private String   cdepot    = "";

    /**
     * Code emplacement.
     */
    private String   cemp      = "";

    /**
     * Lot.
     */
    private String   lot       = "";

    /**
     * Tare.
     */
    private double   tare      = 0;

    /**
     * Contenant.
     */
    private String   cont      = "";

    /**
     * Date du mouvement (avec l'heure).
     */
    // @XmlJavaTypeAdapter(XMLDateAdapter.class)
    private Date     datheurmvt;

    /**
     * Date de génération (avec l'heure).
     */
    // @XmlJavaTypeAdapter(XMLDateAdapter.class)
    private Date     datheurgen;

    /**
     * Dernier utilisateur à avoir modifié l'enregistrement.
     */
    private String   cuser     = "";

    /**
     * Nature de fabrication.
     */
    private String   cnatstk   = "";

    /**
     * Prix.
     */
    private double   prix      = 0;

    /**
     * Inutile.
     */
    private double[] valprix;

    /**
     * Unité du prix.
     */
    private String   cuprix    = "";

    /**
     * Monnaie des montants.
     */
    private String   cmonnaie  = "";

    /**
     * Montant.
     */
    private double   mt1       = 0;

    /**
     * Inutile.
     */
    private double[] valmt;

    /**
     * Type du chrono des composantes de coût.
     */
    private String   prechroct = "";

    /**
     * Chrono composante de coût.
     */
    private int      chronoct  = 0;

    /**
     * Motif du mouvement.
     */
    private String   cmotmvk   = "";

    /**
     * Témoin de mise à jour des stocks.
     */
    private boolean  tstock    = true;

    /**
     * Origine de la déclaration : MANU -> MANUelle REAT -> REmontée ATelier AUTO -> AUTOmatisme.
     */
    private String   typor     = "";

    /**
     * renseigné si c'est un article fantôme.
     */
    private int      ni1or     = 0;

    /**
     * renseigné si c'est un article fantôme.
     */
    private int      ni2or     = 0;

    /**
     * renseigné si c'est un article fantôme.
     */
    private int      ni3or     = 0;

    /**
     * renseigné si c'est un article fantôme.
     */
    private int      ni4or     = 0;

    /**
     * Gets the Code Article.
     * 
     * @return String
     */
    public String getCart() {
        return cart;
    }

    /**
     * Gets the Code dépot.
     * 
     * @return String
     */
    public String getCdepot() {
        return cdepot;
    }

    /**
     * Gets the Code emplacement.
     * 
     * @return String
     */
    public String getCemp() {
        return cemp;
    }

    /**
     * Gets the Etablissement.
     * 
     * @return String
     */
    public String getCetab() {
        return cetab;
    }

    /**
     * Gets the Chrono.
     * 
     * @return int
     */
    public int getChrono() {
        return chrono;
    }

    /**
     * Gets the Chrono composante de coût.
     * 
     * @return int
     */
    public int getChronoct() {
        return chronoct;
    }

    /**
     * Gets the Monnaie des montants.
     * 
     * @return String
     */
    public String getCmonnaie() {
        return cmonnaie;
    }

    /**
     * Gets the Motif du mouvement.
     * 
     * @return String
     */
    public String getCmotmvk() {
        return cmotmvk;
    }

    /**
     * Gets the Nature de fabrication.
     * 
     * @return String
     */
    public String getCnatstk() {
        return cnatstk;
    }

    /**
     * Gets the Contenant.
     * 
     * @return String
     */
    public String getCont() {
        return cont;
    }

    /**
     * Gets the Code Societe .
     * 
     * @return String
     */
    public String getCsoc() {
        return csoc;
    }

    /**
     * Gets the Unité 1.
     * 
     * @return String
     */
    public String getCu1() {
        return cu1;
    }

    /**
     * Gets the Unité 2.
     * 
     * @return String
     */
    public String getCu2() {
        return cu2;
    }

    /**
     * Gets the Unité 3.
     * 
     * @return String
     */
    public String getCu3() {
        return cu3;
    }

    /**
     * Gets the Unité du prix.
     * 
     * @return String
     */
    public String getCuprix() {
        return cuprix;
    }

    /**
     * Gets the Dernier utilisateur à avoir modifié l'enregistrement.
     * 
     * @return String
     */
    public String getCuser() {
        return cuser;
    }

    /**
     * Gets the Variante.
     * 
     * @return String
     */
    public String getCv() {
        return cv;
    }

    /**
     * Gets the Date de génération (avec l'heure).
     * 
     * @return Date
     */
    public Date getDatheurgen() {
        return datheurgen;
    }

    /**
     * Gets the Date du mouvement (avec l'heure).
     * 
     * @return Date
     */
    public Date getDatheurmvt() {
        return datheurmvt;
    }

    /**
     * Gets the Lot.
     * 
     * @return String
     */
    public String getLot() {
        return lot;
    }

    /**
     * Gets the Montant.
     * 
     * @return double
     */
    public double getMt1() {
        return mt1;
    }

    /**
     * Gets the Numero Interne.
     * 
     * @return int
     */
    public int getNi1() {
        return ni1;
    }

    /**
     * Gets the renseigné si c'est un article fantôme.
     * 
     * @return int
     */
    public int getNi1or() {
        return ni1or;
    }

    /**
     * Gets the Numero Interne.
     * 
     * @return int
     */
    public int getNi2() {
        return ni2;
    }

    /**
     * Gets the renseigné si c'est un article fantôme.
     * 
     * @return int
     */
    public int getNi2or() {
        return ni2or;
    }

    /**
     * Gets the Nunéro interne 3.
     * 
     * @return int
     */
    public int getNi3() {
        return ni3;
    }

    /**
     * Gets the renseigné si c'est un article fantôme.
     * 
     * @return int
     */
    public int getNi3or() {
        return ni3or;
    }

    /**
     * Gets the renseigné si c'est un article fantôme.
     * 
     * @return int
     */
    public int getNi4or() {
        return ni4or;
    }

    /**
     * Gets the Rang.
     * 
     * @return int
     */
    public int getNlig() {
        return nlig;
    }

    /**
     * Gets the Type du chrono.
     * 
     * @return String
     */
    public String getPrechro() {
        return prechro;
    }

    /**
     * Gets the Type du chrono des composantes de coût.
     * 
     * @return String
     */
    public String getPrechroct() {
        return prechroct;
    }

    /**
     * Gets the Prix.
     * 
     * @return double
     */
    public double getPrix() {
        return prix;
    }

    /**
     * Gets the Quantite 1.
     * 
     * @return double
     */
    public double getQte1() {
        return qte1;
    }

    /**
     * Gets the Quantite 2.
     * 
     * @return double
     */
    public double getQte2() {
        return qte2;
    }

    /**
     * Gets the Quantite 3.
     * 
     * @return double
     */
    public double getQte3() {
        return qte3;
    }

    /**
     * Gets the Tare.
     * 
     * @return double
     */
    public double getTare() {
        return tare;
    }

    /**
     * Gets the Témoin de mise à jour des stocks.
     * 
     * @return boolean
     */
    public boolean getTstock() {
        return tstock;
    }

    /**
     * Gets the ENT -> Entrants SOR ->Sortants ART -> Articles de coût.
     * 
     * @return String
     */
    public String getTypacta() {
        return typacta;
    }

    /**
     * Gets the Origine de la déclaration : MANU -> MANUelle REAT -> REmontée ATelier AUTO -> AUTOmatisme.
     * 
     * @return String
     */
    public String getTypor() {
        return typor;
    }

    /**
     * Gets the Inutile.
     * 
     * @return double[]
     */
    public double[] getValmt() {
        return valmt;
    }

    /**
     * Gets the 1th value of Valmt extent.
     * 
     * @return that value
     */
    public double getValmt1() {
        if (this.valmt == null) {
            // CHECKSTYLE:OFF
            this.valmt = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valmt[0];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 2th value of Valmt extent.
     * 
     * @return that value
     */
    public double getValmt2() {
        if (this.valmt == null) {
            // CHECKSTYLE:OFF
            this.valmt = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valmt[1];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 3th value of Valmt extent.
     * 
     * @return that value
     */
    public double getValmt3() {
        if (this.valmt == null) {
            // CHECKSTYLE:OFF
            this.valmt = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valmt[2];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 4th value of Valmt extent.
     * 
     * @return that value
     */
    public double getValmt4() {
        if (this.valmt == null) {
            // CHECKSTYLE:OFF
            this.valmt = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valmt[3];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 5th value of Valmt extent.
     * 
     * @return that value
     */
    public double getValmt5() {
        if (this.valmt == null) {
            // CHECKSTYLE:OFF
            this.valmt = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valmt[4];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the Inutile.
     * 
     * @return double[]
     */
    public double[] getValprix() {
        return valprix;
    }

    /**
     * Gets the 1th value of Valprix extent.
     * 
     * @return that value
     */
    public double getValprix1() {
        if (this.valprix == null) {
            // CHECKSTYLE:OFF
            this.valprix = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valprix[0];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 2th value of Valprix extent.
     * 
     * @return that value
     */
    public double getValprix2() {
        if (this.valprix == null) {
            // CHECKSTYLE:OFF
            this.valprix = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valprix[1];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 3th value of Valprix extent.
     * 
     * @return that value
     */
    public double getValprix3() {
        if (this.valprix == null) {
            // CHECKSTYLE:OFF
            this.valprix = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valprix[2];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 4th value of Valprix extent.
     * 
     * @return that value
     */
    public double getValprix4() {
        if (this.valprix == null) {
            // CHECKSTYLE:OFF
            this.valprix = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valprix[3];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 5th value of Valprix extent.
     * 
     * @return that value
     */
    public double getValprix5() {
        if (this.valprix == null) {
            // CHECKSTYLE:OFF
            this.valprix = new double[5];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.valprix[4];
        // CHECKSTYLE:ON
    }

    /**
     * Retrieve a string representation for the current PLC key.
     * 
     * @return key as a string value
     */
    public String keyToString() {
        return "[csoc=" + getCsoc() + ", cetab=" + getCetab() + ", prechro=" + getPrechro() + ", chrono=" + getChrono()
                + ", ni1=" + getNi1() + ", ni2=" + getNi2() + ", ni3=" + getNi3() + ", nlig=" + getNlig() + "]";
    }

    /**
     * Returns the list of values of the key properties of this PLC.
     * 
     * @return the list of values of the key properties of this PLC.
     */
    public Object[] listKeyPropertyValues() {
        return new Object[] { csoc, cetab, prechro, new Integer(chrono), new Integer(ni1), new Integer(ni2),
                new Integer(ni3), new Integer(nlig) };
    }

    /**
     * Sets the Code Article.
     * 
     * @param cart String
     */
    public void setCart(final String cart) {
        this.cart = cart;
    }

    /**
     * Sets the Code dépot.
     * 
     * @param cdepot String
     */
    public void setCdepot(final String cdepot) {
        this.cdepot = cdepot;
    }

    /**
     * Sets the Code emplacement.
     * 
     * @param cemp String
     */
    public void setCemp(final String cemp) {
        this.cemp = cemp;
    }

    /**
     * Sets the Etablissement.
     * 
     * @param cetab String
     */
    public void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Sets the Chrono.
     * 
     * @param chrono int
     */
    public void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Sets the Chrono composante de coût.
     * 
     * @param chronoct int
     */
    public void setChronoct(final int chronoct) {
        this.chronoct = chronoct;
    }

    /**
     * Sets the Monnaie des montants.
     * 
     * @param cmonnaie String
     */
    public void setCmonnaie(final String cmonnaie) {
        this.cmonnaie = cmonnaie;
    }

    /**
     * Sets the Motif du mouvement.
     * 
     * @param cmotmvk String
     */
    public void setCmotmvk(final String cmotmvk) {
        this.cmotmvk = cmotmvk;
    }

    /**
     * Sets the Nature de fabrication.
     * 
     * @param cnatstk String
     */
    public void setCnatstk(final String cnatstk) {
        this.cnatstk = cnatstk;
    }

    /**
     * Sets the Contenant.
     * 
     * @param cont String
     */
    public void setCont(final String cont) {
        this.cont = cont;
    }

    /**
     * Sets the Code Societe .
     * 
     * @param csoc String
     */
    public void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Sets the Unité 1.
     * 
     * @param cu1 String
     */
    public void setCu1(final String cu1) {
        this.cu1 = cu1;
    }

    /**
     * Sets the Unité 2.
     * 
     * @param cu2 String
     */
    public void setCu2(final String cu2) {
        this.cu2 = cu2;
    }

    /**
     * Sets the Unité 3.
     * 
     * @param cu3 String
     */
    public void setCu3(final String cu3) {
        this.cu3 = cu3;
    }

    /**
     * Sets the Unité du prix.
     * 
     * @param cuprix String
     */
    public void setCuprix(final String cuprix) {
        this.cuprix = cuprix;
    }

    /**
     * Sets the Dernier utilisateur à avoir modifié l'enregistrement.
     * 
     * @param cuser String
     */
    public void setCuser(final String cuser) {
        this.cuser = cuser;
    }

    /**
     * Sets the Variante.
     * 
     * @param cv String
     */
    public void setCv(final String cv) {
        this.cv = cv;
    }

    /**
     * Sets the Date de génération (avec l'heure).
     * 
     * @param datheurgen Date
     */
    public void setDatheurgen(final Date datheurgen) {
        this.datheurgen = datheurgen;
    }

    /**
     * Sets the Date du mouvement (avec l'heure).
     * 
     * @param datheurmvt Date
     */
    public void setDatheurmvt(final Date datheurmvt) {
        this.datheurmvt = datheurmvt;
    }

    /**
     * Sets the Lot.
     * 
     * @param lot String
     */
    public void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Sets the Montant.
     * 
     * @param mt1 double
     */
    public void setMt1(final double mt1) {
        this.mt1 = mt1;
    }

    /**
     * Sets the Numero Interne.
     * 
     * @param ni1 int
     */
    public void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Sets the renseigné si c'est un article fantôme.
     * 
     * @param ni1or int
     */
    public void setNi1or(final int ni1or) {
        this.ni1or = ni1or;
    }

    /**
     * Sets the Numero Interne.
     * 
     * @param ni2 int
     */
    public void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Sets the renseigné si c'est un article fantôme.
     * 
     * @param ni2or int
     */
    public void setNi2or(final int ni2or) {
        this.ni2or = ni2or;
    }

    /**
     * Sets the Nunéro interne 3.
     * 
     * @param ni3 int
     */
    public void setNi3(final int ni3) {
        this.ni3 = ni3;
    }

    /**
     * Sets the renseigné si c'est un article fantôme.
     * 
     * @param ni3or int
     */
    public void setNi3or(final int ni3or) {
        this.ni3or = ni3or;
    }

    /**
     * Sets the renseigné si c'est un article fantôme.
     * 
     * @param ni4or int
     */
    public void setNi4or(final int ni4or) {
        this.ni4or = ni4or;
    }

    /**
     * Sets the Rang.
     * 
     * @param nlig int
     */
    public void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Sets the Type du chrono.
     * 
     * @param prechro String
     */
    public void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

    /**
     * Sets the Type du chrono des composantes de coût.
     * 
     * @param prechroct String
     */
    public void setPrechroct(final String prechroct) {
        this.prechroct = prechroct;
    }

    /**
     * Sets the Prix.
     * 
     * @param prix double
     */
    public void setPrix(final double prix) {
        this.prix = prix;
    }

    /**
     * Sets the Quantite 1.
     * 
     * @param qte1 double
     */
    public void setQte1(final double qte1) {
        this.qte1 = qte1;
    }

    /**
     * Sets the Quantite 2.
     * 
     * @param qte2 double
     */
    public void setQte2(final double qte2) {
        this.qte2 = qte2;
    }

    /**
     * Sets the Quantite 3.
     * 
     * @param qte3 double
     */
    public void setQte3(final double qte3) {
        this.qte3 = qte3;
    }

    /**
     * Sets the Tare.
     * 
     * @param tare double
     */
    public void setTare(final double tare) {
        this.tare = tare;
    }

    /**
     * Sets the Témoin de mise à jour des stocks.
     * 
     * @param tstock boolean
     */
    public void setTstock(final boolean tstock) {
        this.tstock = tstock;
    }

    /**
     * Sets the ENT -> Entrants SOR ->Sortants ART -> Articles de coût.
     * 
     * @param typacta String
     */
    public void setTypacta(final String typacta) {
        this.typacta = typacta;
    }

    /**
     * Sets the Origine de la déclaration : MANU -> MANUelle REAT -> REmontée ATelier AUTO -> AUTOmatisme.
     * 
     * @param typor String
     */
    public void setTypor(final String typor) {
        this.typor = typor;
    }

    /**
     * Sets the Inutile.
     * 
     * @param valmt double
     */
    public void setValmt(final double[] valmt) {
        this.valmt = valmt;
    }

    /**
     * Sets the Inutile.
     * 
     * @param idx int
     * @param pvalmt double
     */
    public void setValmt(final int idx, final double pvalmt) {
        if (this.valmt == null) {
            // CHECKSTYLE:OFF
            this.valmt = new double[5];
            // CHECKSTYLE:ON
        }
        this.valmt[idx - 1] = pvalmt;
    }

    /**
     * Sets the 1th value of Valmt extent.
     * 
     * @param valmt1 the new value
     */
    public void setValmt1(final double valmt1) {
        // CHECKSTYLE:OFF
        setValmt(1, valmt1);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 2th value of Valmt extent.
     * 
     * @param valmt2 the new value
     */
    public void setValmt2(final double valmt2) {
        // CHECKSTYLE:OFF
        setValmt(2, valmt2);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 3th value of Valmt extent.
     * 
     * @param valmt3 the new value
     */
    public void setValmt3(final double valmt3) {
        // CHECKSTYLE:OFF
        setValmt(3, valmt3);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 4th value of Valmt extent.
     * 
     * @param valmt4 the new value
     */
    public void setValmt4(final double valmt4) {
        // CHECKSTYLE:OFF
        setValmt(4, valmt4);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 5th value of Valmt extent.
     * 
     * @param valmt5 the new value
     */
    public void setValmt5(final double valmt5) {
        // CHECKSTYLE:OFF
        setValmt(5, valmt5);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the Inutile.
     * 
     * @param valprix double
     */
    public void setValprix(final double[] valprix) {
        this.valprix = valprix;
    }

    /**
     * Sets the Inutile.
     * 
     * @param idx int
     * @param pvalprix double
     */
    public void setValprix(final int idx, final double pvalprix) {
        if (this.valprix == null) {
            // CHECKSTYLE:OFF
            this.valprix = new double[5];
            // CHECKSTYLE:ON
        }
        this.valprix[idx - 1] = pvalprix;
    }

    /**
     * Sets the 1th value of Valprix extent.
     * 
     * @param valprix1 the new value
     */
    public void setValprix1(final double valprix1) {
        // CHECKSTYLE:OFF
        setValprix(1, valprix1);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 2th value of Valprix extent.
     * 
     * @param valprix2 the new value
     */
    public void setValprix2(final double valprix2) {
        // CHECKSTYLE:OFF
        setValprix(2, valprix2);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 3th value of Valprix extent.
     * 
     * @param valprix3 the new value
     */
    public void setValprix3(final double valprix3) {
        // CHECKSTYLE:OFF
        setValprix(3, valprix3);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 4th value of Valprix extent.
     * 
     * @param valprix4 the new value
     */
    public void setValprix4(final double valprix4) {
        // CHECKSTYLE:OFF
        setValprix(4, valprix4);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 5th value of Valprix extent.
     * 
     * @param valprix5 the new value
     */
    public void setValprix5(final double valprix5) {
        // CHECKSTYLE:OFF
        setValprix(5, valprix5);
        // CHECKSTYLE:ON
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ObjectHelper.getShortReference(this) + keyToString();
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoPPOTtMoinsert.java,v $
 * Created on 19 nov. 2014 by nle
 * Revision: $Revision: 1.3 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamopxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.3 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoPPOTtMoinsert extends AbstractTempTable implements Serializable {


    private String cact = "";

    private String cart = "";

    private String cequip = "";

    private String cetab = "";

    private String cetat = "";

    private int chronor = 0;

    private String citi = "";

    private String cofuse = "";

    private String cres = "";

    private String csoc = "";

    private String cunite = "";

    private String cuser = "";

    private Date ladate = null;

    private int lheure = 0;

    private String lot = "";

    private String lotent = "";

    private int prior = 0;

    private double qte = 0;

    private boolean traisdat = false;

    private String typof = "";

    private String xCacteurLiv = "";

    private String xCdepotexp = "";

    private String xCdepotorig = "";

    private String xCmodprep = "";

    private String xCtie = "";

    private String xLexpe = "";

    private String xLprepa = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoPPOTtMoinsert() {
        super();
    }

    /**
     * Gets the cact.
     *
     * @category getter
     * @return the cact.
     */
    public final String getCact() {
        return cact;
    }

    /**
     * Sets the cact.
     *
     * @category setter
     * @param cact cact.
     */
    public final void setCact(final String cact) {
        this.cact = cact;
    }

    /**
     * Gets the cart.
     *
     * @category getter
     * @return the cart.
     */
    public final String getCart() {
        return cart;
    }

    /**
     * Sets the cart.
     *
     * @category setter
     * @param cart cart.
     */
    public final void setCart(final String cart) {
        this.cart = cart;
    }

    /**
     * Gets the cequip.
     *
     * @category getter
     * @return the cequip.
     */
    public final String getCequip() {
        return cequip;
    }

    /**
     * Sets the cequip.
     *
     * @category setter
     * @param cequip cequip.
     */
    public final void setCequip(final String cequip) {
        this.cequip = cequip;
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the cetat.
     *
     * @category getter
     * @return the cetat.
     */
    public final String getCetat() {
        return cetat;
    }

    /**
     * Sets the cetat.
     *
     * @category setter
     * @param cetat cetat.
     */
    public final void setCetat(final String cetat) {
        this.cetat = cetat;
    }

    /**
     * Gets the chronor.
     *
     * @category getter
     * @return the chronor.
     */
    public final int getChronor() {
        return chronor;
    }

    /**
     * Sets the chronor.
     *
     * @category setter
     * @param chronor chronor.
     */
    public final void setChronor(final int chronor) {
        this.chronor = chronor;
    }

    /**
     * Gets the citi.
     *
     * @category getter
     * @return the citi.
     */
    public final String getCiti() {
        return citi;
    }

    /**
     * Sets the citi.
     *
     * @category setter
     * @param citi citi.
     */
    public final void setCiti(final String citi) {
        this.citi = citi;
    }

    /**
     * Gets the cofuse.
     *
     * @category getter
     * @return the cofuse.
     */
    public final String getCofuse() {
        return cofuse;
    }

    /**
     * Sets the cofuse.
     *
     * @category setter
     * @param cofuse cofuse.
     */
    public final void setCofuse(final String cofuse) {
        this.cofuse = cofuse;
    }

    /**
     * Gets the cres.
     *
     * @category getter
     * @return the cres.
     */
    public final String getCres() {
        return cres;
    }

    /**
     * Sets the cres.
     *
     * @category setter
     * @param cres cres.
     */
    public final void setCres(final String cres) {
        this.cres = cres;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the cunite.
     *
     * @category getter
     * @return the cunite.
     */
    public final String getCunite() {
        return cunite;
    }

    /**
     * Sets the cunite.
     *
     * @category setter
     * @param cunite cunite.
     */
    public final void setCunite(final String cunite) {
        this.cunite = cunite;
    }

    /**
     * Gets the cuser.
     *
     * @category getter
     * @return the cuser.
     */
    public final String getCuser() {
        return cuser;
    }

    /**
     * Sets the cuser.
     *
     * @category setter
     * @param cuser cuser.
     */
    public final void setCuser(final String cuser) {
        this.cuser = cuser;
    }

    /**
     * Gets the ladate.
     *
     * @category getter
     * @return the ladate.
     */
    public final Date getLadate() {
        return ladate;
    }

    /**
     * Sets the ladate.
     *
     * @category setter
     * @param ladate ladate.
     */
    public final void setLadate(final Date ladate) {
        this.ladate = ladate;
    }

    /**
     * Gets the lheure.
     *
     * @category getter
     * @return the lheure.
     */
    public final int getLheure() {
        return lheure;
    }

    /**
     * Sets the lheure.
     *
     * @category setter
     * @param lheure lheure.
     */
    public final void setLheure(final int lheure) {
        this.lheure = lheure;
    }

    /**
     * Gets the lot.
     *
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Sets the lot.
     *
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Gets the lotent.
     *
     * @category getter
     * @return the lotent.
     */
    public final String getLotent() {
        return lotent;
    }

    /**
     * Sets the lotent.
     *
     * @category setter
     * @param lotent lotent.
     */
    public final void setLotent(final String lotent) {
        this.lotent = lotent;
    }

    /**
     * Gets the prior.
     *
     * @category getter
     * @return the prior.
     */
    public final int getPrior() {
        return prior;
    }

    /**
     * Sets the prior.
     *
     * @category setter
     * @param prior prior.
     */
    public final void setPrior(final int prior) {
        this.prior = prior;
    }

    /**
     * Gets the qte.
     *
     * @category getter
     * @return the qte.
     */
    public final double getQte() {
        return qte;
    }

    /**
     * Sets the qte.
     *
     * @category setter
     * @param qte qte.
     */
    public final void setQte(final double qte) {
        this.qte = qte;
    }

    /**
     * Gets the traisdat.
     *
     * @category getter
     * @return the traisdat.
     */
    public final boolean getTraisdat() {
        return traisdat;
    }

    /**
     * Sets the traisdat.
     *
     * @category setter
     * @param traisdat traisdat.
     */
    public final void setTraisdat(final boolean traisdat) {
        this.traisdat = traisdat;
    }

    /**
     * Gets the typof.
     *
     * @category getter
     * @return the typof.
     */
    public final String getTypof() {
        return typof;
    }

    /**
     * Sets the typof.
     *
     * @category setter
     * @param typof typof.
     */
    public final void setTypof(final String typof) {
        this.typof = typof;
    }

    /**
     * Gets the xCacteurLiv.
     *
     * @category getter
     * @return the xCacteurLiv.
     */
    public final String getXCacteurLiv() {
        return xCacteurLiv;
    }

    /**
     * Sets the xCacteurLiv.
     *
     * @category setter
     * @param xCacteurLiv xCacteurLiv.
     */
    public final void setXCacteurLiv(final String xCacteurLiv) {
        this.xCacteurLiv = xCacteurLiv;
    }

    /**
     * Gets the xCdepotexp.
     *
     * @category getter
     * @return the xCdepotexp.
     */
    public final String getXCdepotexp() {
        return xCdepotexp;
    }

    /**
     * Sets the xCdepotexp.
     *
     * @category setter
     * @param xCdepotexp xCdepotexp.
     */
    public final void setXCdepotexp(final String xCdepotexp) {
        this.xCdepotexp = xCdepotexp;
    }

    /**
     * Gets the xCdepotorig.
     *
     * @category getter
     * @return the xCdepotorig.
     */
    public final String getXCdepotorig() {
        return xCdepotorig;
    }

    /**
     * Sets the xCdepotorig.
     *
     * @category setter
     * @param xCdepotorig xCdepotorig.
     */
    public final void setXCdepotorig(final String xCdepotorig) {
        this.xCdepotorig = xCdepotorig;
    }

    /**
     * Gets the xCmodprep.
     *
     * @category getter
     * @return the xCmodprep.
     */
    public final String getXCmodprep() {
        return xCmodprep;
    }

    /**
     * Sets the xCmodprep.
     *
     * @category setter
     * @param xCmodprep xCmodprep.
     */
    public final void setXCmodprep(final String xCmodprep) {
        this.xCmodprep = xCmodprep;
    }

    /**
     * Gets the xCtie.
     *
     * @category getter
     * @return the xCtie.
     */
    public final String getXCtie() {
        return xCtie;
    }

    /**
     * Sets the xCtie.
     *
     * @category setter
     * @param xCtie xCtie.
     */
    public final void setXCtie(final String xCtie) {
        this.xCtie = xCtie;
    }

    /**
     * Gets the xLexpe.
     *
     * @category getter
     * @return the xLexpe.
     */
    public final String getXLexpe() {
        return xLexpe;
    }

    /**
     * Sets the xLexpe.
     *
     * @category setter
     * @param xLexpe xLexpe.
     */
    public final void setXLexpe(final String xLexpe) {
        this.xLexpe = xLexpe;
    }

    /**
     * Gets the xLprepa.
     *
     * @category getter
     * @return the xLprepa.
     */
    public final String getXLprepa() {
        return xLprepa;
    }

    /**
     * Sets the xLprepa.
     *
     * @category setter
     * @param xLprepa xLprepa.
     */
    public final void setXLprepa(final String xLprepa) {
        this.xLprepa = xLprepa;
    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 2 août 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ws.rs.WebApplicationException;


/**
 * TODO Write the class' description
 *
 * @author ac
 */
public class DateParam {
    private final Date date;

    public DateParam(final String dateStr) throws WebApplicationException {
        if (dateStr.length() == 0) {
            this.date = null;
            return;
        }
        final DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        try {
            this.date = dateFormat.parse(dateStr);
        } catch (ParseException e) {
            throw new WebApplicationException();
        }
    }

    public Date getDate() {
        return date;
    }
}

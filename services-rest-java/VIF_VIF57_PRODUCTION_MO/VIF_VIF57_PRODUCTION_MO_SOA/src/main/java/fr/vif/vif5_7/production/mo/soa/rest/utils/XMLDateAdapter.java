/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 20 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils;


import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.bind.annotation.adapters.XmlAdapter;
import javax.xml.datatype.DatatypeFactory;


/**
 * XML Date Adapter for DTO.
 *
 * @author ac
 */
public class XMLDateAdapter extends XmlAdapter<String, Date> {

    /**
     * {@inheritDoc}
     */
    @Override
    public String marshal(final Date v) throws Exception {
        GregorianCalendar gc = new GregorianCalendar();
        gc.setTime(v);
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(gc).toXMLFormat();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Date unmarshal(final String v) throws Exception {
        return DatatypeFactory.newInstance().newXMLGregorianCalendar(v).toGregorianCalendar().getTime();
    }

}

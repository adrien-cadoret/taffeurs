/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: MOCommonConverter.java,v $
 * Created on 10 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.converters;


import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import fr.vif.jtech.common.util.DateHelper;
import fr.vif.vif5_7.production.mo.ChronoType;
import fr.vif.vif5_7.production.mo.CodeLabelType;
import fr.vif.vif5_7.production.mo.CompEstabType;
import fr.vif.vif5_7.production.mo.DateHourBoundsType;
import fr.vif.vif5_7.production.mo.MOCompleteIdType;
import fr.vif.vif5_7.production.mo.MOItemCompleteIdType;
import fr.vif.vif5_7.production.mo.MOResourceCompleteIdType;
import fr.vif.vif5_7.production.mo.MOWorkshopCompleteIdType;
import fr.vif.vif5_7.production.mo.ModificationType;
import fr.vif.vif5_7.production.mo.OriginType;
import fr.vif.vif5_7.production.mo.OriginTypeType;
import fr.vif.vif5_7.production.mo.QuantitiesType;
import fr.vif.vif5_7.production.mo.QuantityType;
import fr.vif.vif5_7.production.mo.StockUpdateType;
import fr.vif.vif5_7.production.mo.WareLocType;


/**
 * MO Common Converter Abstract Class.
 * 
 * @author vl
 */
public abstract class MOCommonConverter {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(MOCommonConverter.class);

    /**
     * Return empty if null.
     * 
     * @param stringToTest the String
     * @return a String not null
     */
    protected String emptyIfNull(final String stringToTest) {
        String ret = "";
        if (stringToTest != null) {
            ret = stringToTest;
        }
        return ret;
    }

    /**
     * Return false if null.
     * 
     * @param booleanToTest the boolean
     * @return boolean not null
     */
    protected boolean falseIfNull(final Boolean booleanToTest) {
        boolean ret = false;
        if (booleanToTest != null) {
            ret = booleanToTest;
        }
        return ret;
    }

    /**
     * Get Chrono Type.
     * 
     * @param prechro String prechro
     * @param chrono int chrono
     * @return ChronoType
     */
    protected ChronoType getChronoType(final String prechro, final int chrono) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getChronoType(prechro=" + prechro + ", chrono=" + chrono + ")");
        }

        ChronoType chronoType = new ChronoType();
        chronoType.setPrechro(prechro);
        chronoType.setChrono(chrono);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getChronoType(prechro=" + prechro + ", chrono=" + chrono + ")=" + chronoType);
        }
        return chronoType;
    }

    /**
     * Get CodeLabel Type.
     * 
     * @param code Code
     * @param label Label
     * @return CodeLabelType
     */
    protected CodeLabelType getCodeLabelType(final String code, final String label) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getCodeLabelType(code=" + code + ", label=" + label + ")");
        }

        CodeLabelType ret = new CodeLabelType();
        ret.setCode(code);
        ret.setLabel(label);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getCodeLabelType(code=" + code + ", label=" + label + ")=" + ret);
        }
        return ret;
    }

    /**
     * Get CompEstab Type.
     * 
     * @param csoc String csoc
     * @param cetab String cetab
     * @return CompEstabType
     */
    protected CompEstabType getCompEstabType(final String csoc, final String cetab) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getCompEstabType(csoc=" + csoc + ", cetab=" + cetab + ")");
        }

        CompEstabType compEstabType = new CompEstabType();
        compEstabType.setCompany(csoc);
        compEstabType.setEstablishment(cetab);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getCompEstabType(csoc=" + csoc + ", cetab=" + cetab + ")=" + compEstabType);
        }
        return compEstabType;
    }

    /**
     * Get DateHourBounds Type.
     * 
     * @param datdebr Date datdebr
     * @param heurdebr int heurdebr
     * @param datfinr Date datfinr
     * @param heurfinr int heurfinr
     * @return DateHourBoundsType
     */
    protected DateHourBoundsType getEffectiveDateHourBounds(final Date datdebr, final int heurdebr, final Date datfinr,
            final int heurfinr) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getEffectiveDateHourBounds(datdebr=" + datdebr + ", heurdebr=" + heurdebr + ", datfinr="
                    + datfinr + ", heurfinr=" + heurfinr + ")");
        }
        DateHourBoundsType ret = null;
        if (datdebr != null && datfinr != null) {
            ret = new DateHourBoundsType();
            ret.setBeginDateHour(getXMLGregorianCalendar(datdebr, heurdebr));
            ret.setEndDateHour(getXMLGregorianCalendar(datfinr, heurfinr));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getEffectiveDateHourBounds(datdebr=" + datdebr + ", heurdebr=" + heurdebr + ", datfinr="
                    + datfinr + ", heurfinr=" + heurfinr + ")=" + ret);
        }
        return ret;
    }

    /**
     * Get MoCompleteId Type.
     * 
     * @param csoc String csoc
     * @param cetab String cetab
     * @param prechro String prechro
     * @param chrono int chrono
     * @return MoCompleteIdType
     */
    protected MOCompleteIdType getMOCompleteIdType(final String csoc, final String cetab, final String prechro,
            final int chrono) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getMoCompleteIdType(csoc=" + csoc + ", cetab=" + cetab + ", prechro=" + prechro
                    + ", chrono=" + chrono + ")");
        }

        MOCompleteIdType moCompleteIdType = new MOCompleteIdType();
        moCompleteIdType.setCompEstab(getCompEstabType(csoc, cetab));
        moCompleteIdType.setChrono(getChronoType(prechro, chrono));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getMoCompleteIdType(csoc=" + csoc + ", cetab=" + cetab + ", prechro=" + prechro
                    + ", chrono=" + chrono + ")=" + moCompleteIdType);
        }
        return moCompleteIdType;
    }

    /**
     * Get Modification Object.
     * 
     * @param datmod Date datmod
     * @param heurmod int heurmod
     * @param cuser String cuser
     * @return ModificationType
     */
    protected ModificationType getModification(final Date datmod, final int heurmod, final String cuser) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getModification(datmod=" + datmod + ", heurmod=" + heurmod + ", cuser=" + cuser + ")");
        }

        ModificationType ret = null;
        if (datmod != null && cuser != null && !"".equals(cuser)) {
            ret = new ModificationType();
            ret.setDatehour(getXMLGregorianCalendar(datmod, heurmod));
            ret.setUser(cuser);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getModification(datmod=" + datmod + ", heurmod=" + heurmod + ", cuser=" + cuser + ")="
                    + ret);
        }
        return ret;
    }

    /**
     * Get MOItemCompleteIdType.
     * 
     * @param csoc society
     * @param cetab establishment
     * @param prechro prechro
     * @param chrono chrono
     * @param ni1 ni1
     * @param ni2 ni2
     * @param ni3 ni3
     * @param nlig nlig
     * @return MOItemCompleteIdType
     */
    protected MOItemCompleteIdType getMOItemCompleteIdType(final String csoc, final String cetab, final String prechro,
            final int chrono, final int ni1, final int ni2, final int ni3, final int nlig) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getMOItemCompleteIdType(csoc=" + csoc + ", cetab=" + cetab + ", prechro=" + prechro
                    + ", chrono=" + chrono + ", ni1=" + ni1 + ", ni2=" + ni2 + ", ni3=" + ni3 + ", nlig=" + nlig + ")");
        }

        MOItemCompleteIdType ret = new MOItemCompleteIdType();
        ret.setMoId(getMOCompleteIdType(csoc, cetab, prechro, chrono));
        ret.setNi1(ni1);
        ret.setNi2(ni2);
        ret.setNi3(ni3);
        ret.setNlig(nlig);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getMOItemCompleteIdType(csoc=" + csoc + ", cetab=" + cetab + ", prechro=" + prechro
                    + ", chrono=" + chrono + ", ni1=" + ni1 + ", ni2=" + ni2 + ", ni3=" + ni3 + ", nlig=" + nlig + ")="
                    + ret);
        }
        return ret;
    }

    /**
     * Get MoResourceCompleteId Type.
     * 
     * @param csoc String csoc
     * @param cetab String cetab
     * @param prechro String prechro
     * @param chrono int chrono
     * @param ni1 int ni1
     * @param ni2 int ni2
     * @param nlig int nlig
     * @param ni4 int ni4
     * @return MoResourceCompleteIdType
     */
    protected MOResourceCompleteIdType getMOResourceId(final String csoc, final String cetab, final String prechro,
            final int chrono, final int ni1, final int ni2, final int nlig, final int ni4) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getMoResourceId(csoc=" + csoc + ", cetab=" + cetab + ", prechro=" + prechro + ", chrono="
                    + chrono + ", ni1=" + ni1 + ", ni2=" + ni2 + ", nlig=" + nlig + ", ni4=" + ni4 + ")");
        }

        MOResourceCompleteIdType moResourceCompleteIdType = new MOResourceCompleteIdType();
        moResourceCompleteIdType.setMoId(getMOCompleteIdType(csoc, cetab, prechro, chrono));
        moResourceCompleteIdType.setNi1(ni1);
        moResourceCompleteIdType.setNi2(ni2);
        moResourceCompleteIdType.setNlig(nlig);
        moResourceCompleteIdType.setNi4(ni4);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getMoResourceId(csoc=" + csoc + ", cetab=" + cetab + ", prechro=" + prechro + ", chrono="
                    + chrono + ", ni1=" + ni1 + ", ni2=" + ni2 + ", nlig=" + nlig + ", ni4=" + ni4 + ")="
                    + moResourceCompleteIdType);
        }
        return moResourceCompleteIdType;
    }

    /**
     * Get MOWorkshopCompleteIdType.
     * 
     * @param csoc society
     * @param cetab establishment
     * @param prechro prechro
     * @param chrono chrono
     * @param ni1 ni1
     * @param ni2 ni2
     * @param ni3 ni3
     * @param ni4 ni4
     * @param nlig nlig
     * @param typgest management type
     * @param rstock stock update
     * @return MOWorkshopCompleteIdType
     */
    protected MOWorkshopCompleteIdType getMOWorkshopCompleteIdType(final String csoc, final String cetab,
            final String prechro, final int chrono, final int ni1, final int ni2, final int ni3, final int ni4,
            final int nlig, final String typgest, final String rstock) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getMOWorkshopCompleteIdType(csoc=" + csoc + ", cetab=" + cetab + ", prechro=" + prechro
                    + ", chrono=" + chrono + ", ni1=" + ni1 + ", ni2=" + ni2 + ", ni3=" + ni3 + ", ni4=" + ni4
                    + ", nlig=" + nlig + ", typgest=" + typgest + ", rstock=" + rstock + ")");
        }

        MOWorkshopCompleteIdType ret = new MOWorkshopCompleteIdType();
        ret.setMoId(getMOCompleteIdType(csoc, cetab, prechro, chrono));
        ret.setManagementType(typgest);
        ret.setStockUpdate(StockUpdateType.fromValue(rstock));
        ret.setNi1(ni1);
        ret.setNi2(ni2);
        ret.setNi3(ni3);
        ret.setNi4(ni4);
        ret.setNlig(nlig);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getMOWorkshopCompleteIdType(csoc=" + csoc + ", cetab=" + cetab + ", prechro=" + prechro
                    + ", chrono=" + chrono + ", ni1=" + ni1 + ", ni2=" + ni2 + ", ni3=" + ni3 + ", ni4=" + ni4
                    + ", nlig=" + nlig + ", typgest=" + typgest + ", rstock=" + rstock + ")=" + ret);
        }
        return ret;
    }

    /**
     * Get OriginType.
     * 
     * @param typor Type Origin
     * @param ni1or ni1 Origin
     * @param ni2or ni2 Origin
     * @param ni3or ni3 Origin
     * @param ni4or ni4 Origin
     * @return OriginType
     */
    protected OriginType getOriginType(final String typor, final int ni1or, final int ni2or, final int ni3or,
            final int ni4or) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getOriginType(typor=" + typor + ", ni1or=" + ni1or + ", ni2or=" + ni2or + ", ni3or="
                    + ni3or + ", ni4or=" + ni4or + ")");
        }
        OriginType ret = null;
        if (typor != null && !typor.isEmpty() && ni1or != 0 && ni2or != 0 && ni3or != 0 && ni4or != 0) {
            ret = new OriginType();
            ret.setOriginType(OriginTypeType.fromValue(typor));
            ret.setNi1Or(ni1or);
            ret.setNi2Or(ni2or);
            ret.setNi3Or(ni3or);
            ret.setNi4Or(ni4or);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getOriginType(typor=" + typor + ", ni1or=" + ni1or + ", ni2or=" + ni2or + ", ni3or="
                    + ni3or + ", ni4or=" + ni4or + ")=" + ret);
        }
        return ret;
    }

    /**
     * Get Quantities Type.
     * 
     * @param qte1 quantity 1
     * @param cu1 unit 1
     * @param qte2 quantity 2
     * @param cu2 unit 2
     * @param qte3 quantity 3
     * @param cu3 unit 3
     * @return Quantities Type
     */
    protected QuantitiesType getQuantitiesType(final double qte1, final String cu1, final double qte2,
            final String cu2, final double qte3, final String cu3) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getQuantitiesType(qte1=" + qte1 + ", cu1=" + cu1 + ", qte2=" + qte2 + ", cu2=" + cu2
                    + ", qte3=" + qte3 + ", cu3=" + cu3 + ")");
        }

        QuantitiesType ret = new QuantitiesType();
        ret.getQuantity().add(getQuantityType(qte1, cu1));
        ret.getQuantity().add(getQuantityType(qte2, cu2));
        ret.getQuantity().add(getQuantityType(qte3, cu3));

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getQuantitiesType(qte1=" + qte1 + ", cu1=" + cu1 + ", qte2=" + qte2 + ", cu2=" + cu2
                    + ", qte3=" + qte3 + ", cu3=" + cu3 + ")=" + ret);
        }
        return ret;
    }

    /**
     * Get Quantity Type.
     * 
     * @param qte quantity
     * @param cu unit
     * @return QuantityType
     */
    protected QuantityType getQuantityType(final double qte, final String cu) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getQuantityType(qte=" + qte + ", cu=" + cu + ")");
        }

        QuantityType ret = new QuantityType();
        ret.setQty(qte);
        ret.setUnit(cu);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getQuantityType(qte=" + qte + ", cu=" + cu + ")=" + ret);
        }
        return ret;
    }

    /**
     * Get WareLocType.
     * 
     * @param cdepot cdepot
     * @param cemp cemp
     * @return WareLocType
     */
    protected WareLocType getWareLocType(final String cdepot, final String cemp) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getWareLocType(cdepot=" + cdepot + ", cemp=" + cemp + ")");
        }

        WareLocType ret = new WareLocType();
        ret.setWarehouse(cdepot);
        ret.setLocation(cemp);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getWareLocType(cdepot=" + cdepot + ", cemp=" + cemp + ")=" + null);
        }
        return null;
    }

    /**
     * Get XMLGregorianCalendar.
     * 
     * @param date Date date
     * @param heure int heure
     * @return XMLGregorianCalendar
     * @throws DatatypeConfigurationException
     */
    protected XMLGregorianCalendar getXMLGregorianCalendar(final Date date, final int heure) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getXMLGregorianCalendar(date=" + date + ", heure=" + heure + ")");
        }

        XMLGregorianCalendar ret = null;
        if (date != null) {
            Date dateTime = DateHelper.getDateTime(date, heure);
            GregorianCalendar gc = new GregorianCalendar();
            gc.setTime(dateTime);
            try {
                ret = DatatypeFactory.newInstance().newXMLGregorianCalendar(gc);
            } catch (DatatypeConfigurationException e) {
                LOGGER.error("getXMLGregorianCalendar()", e);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getXMLGregorianCalendar(date=" + date + ", heure=" + heure + ")=" + ret);
        }
        return ret;
    }

    /**
     * Return null if empty.
     * 
     * @param stringToTest the String
     * @return null if empty
     */
    protected String nullIfEmpty(final String stringToTest) {
        String ret = null;
        if (stringToTest != null && !stringToTest.isEmpty()) {
            ret = stringToTest;
        }
        return ret;
    }

    /**
     * Return double if not null.
     * 
     * @param doubleToTest the Double
     * @return a Double not null
     */
    protected double zeroIfNull(final Double doubleToTest) {
        double ret = 0;
        if (doubleToTest != null) {
            ret = doubleToTest;
        }
        return ret;
    }

    /**
     * Return int if not null.
     * 
     * @param integerToTest the Integer
     * @return a Integer not null
     */
    protected int zeroIfNull(final Integer integerToTest) {
        int ret = 0;
        if (integerToTest != null) {
            ret = integerToTest;
        }
        return ret;
    }
}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on Jul 19, 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.ping;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import fr.vif.jtech.soa.endpoint.AbstractSOAEndPoint;


/**
 * Ping serving to check URL server.
 *
 * @author ac
 */

@Path("ping")
@Service
public class SubcoWebPortalPingEndPointRS extends AbstractSOAEndPoint {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(SubcoWebPortalPingEndPointRS.class);

    /**
     * 
     * Ping method.
     * 
     * @return empty object.
     */
    @GET
    @Produces({ MediaType.TEXT_PLAIN })
    public Response ping() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - ping()");
        }
        Response ret = Response.ok("OK").build();

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - ping()=" + ret);
        }
        return ret;
    }

}

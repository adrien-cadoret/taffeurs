/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 1 août 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.mo;


import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.vif.jtech.soa.endpoint.AbstractSOAEndPoint;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.production.mo.soa.rest.moitem.MoItemEndPointRS;
import fr.vif.vif5_7.production.mo.soa.rest.utils.ContextBuilderRS;
import fr.vif.vif5_7.production.mo.soa.rest.utils.ResponseDTO;


/**
 * REST Endpoint which is used to retrieve, insert, update or delete MOs.
 *
 * @author ac
 */
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
@Path("/mo/")
@Service
public class MoEndPointRS extends AbstractSOAEndPoint {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(MoItemEndPointRS.class);

    @Autowired
    @Qualifier("MOSBSImpl")
    private MOSBS               moSBS;

    @Autowired
    private ContextBuilderRS    contextBuilder;

    private ResponseDTO         responseDTO;

    /**
     * Gets the contextBuilder.
     *
     * @return the contextBuilder.
     */
    public ContextBuilderRS getContextBuilder() {
        return contextBuilder;
    }

    /**
     * Gets the moSBS.
     *
     * @return the moSBS.
     */
    public MOSBS getMoSBS() {
        return moSBS;
    }

    /**
     * Gets the responseDTO.
     *
     * @return the responseDTO.
     */
    public ResponseDTO getResponseDTO() {
        if (responseDTO == null) {
            responseDTO = new ResponseDTO();
        }
        return responseDTO;
    }

    /**
     * Create or update one contact.
     *
     * @param ui ui
     * @param httpHeaders http headers
     * @param commentDTO the comment
     * @return customer created or updated
     */
    @POST
    @Path("/comment")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response insertComment(@Context final UriInfo ui, @Context final HttpHeaders httpHeaders,
                                  final CommentDTO commentDTO) {

        Response response = null;

        try {
            VIFContext context = createVIFContext(contextBuilder.buildAndCheckContext(httpHeaders));
            try {

                // Call mo SBS
                getMoSBS().insertComment(context, CommentDomain.PRODUCTION, Converter.convertDTOToComment(commentDTO));
                getResponseDTO().setUri("");
                getResponseDTO().setMessage("");
                getResponseDTO().setStatus(StatusType.OK);
                response = Response.status(Status.CREATED).entity(getResponseDTO()).build();
            } catch (ServerBusinessException e) {
                getResponseDTO().setMessage(e.getListeParams().get(0).toString());
                response = Response.status(Status.BAD_REQUEST).entity(getResponseDTO()).build();
            } finally {
                close(context);
            }
        } catch (BusinessException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(e);
            }
            response = ResponseErrorHelper.createError(Status.FORBIDDEN, e);
        }

        return response;
    }

    /**
     *
     * Gets MOs.
     *
     * @param ui ui
     * @param httpHeaders httpHeaders
     * @param company company
     * @param establishment establishment
     * @param prechro prechro
     * @param chrono chrono
     * @return Response
     */
    @GET
    @Path("{company}/{establishment}/{prechro}/{chrono}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response readMo(@Context final UriInfo ui, @Context final HttpHeaders httpHeaders,
                           @PathParam("company") final String company, @PathParam("establishment") final String establishment,
                           @PathParam("prechro") final String prechro, @PathParam("chrono") final Integer chrono) {

        Response response = null;
        try {
            VIFContext context = createVIFContext(contextBuilder.buildAndCheckContext(httpHeaders));
            try {

                // Sets the mo's selection
                List<Mnemos.MOUse> moUses = new ArrayList<Mnemos.MOUse>();
                moUses.add(MOUse.PRODUCTION);
                MOSBean selection = new MOSBean();
                selection.setCompanyId(company);
                selection.setEstablishmentId(establishment);
                selection.setChrono(chrono);
                selection.setMoUses(moUses);
                List<MOBean> mo = moSBS.listMOBean(context, selection, null, false, 0, 1);

                MODTO dto = Converter.convertMOBeanToDTO(mo.get(0));
                dto.setComment(getMoSBS().getComment(context, mo.get(0).getMoKey()));
                response = Response.status(Status.OK).entity(dto).build();
            } catch (ServerBusinessException e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(e);
                }
                if (Status.NOT_FOUND.getReasonPhrase().equals(
                        ((VarParamTranslation) e.getListeParams().get(0)).getValue())) {
                    response = ResponseErrorHelper.createError(Status.NOT_FOUND, e);
                } else {
                    response = ResponseErrorHelper.createError(Status.INTERNAL_SERVER_ERROR, e);
                }
            } finally {
                close(context);
            }
        } catch (BusinessException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(e);
            }
            response = ResponseErrorHelper.createError(Status.FORBIDDEN, e);
        }

        return response;
    }

    /**
     *
     * Gets MOs.
     *
     * @param ui ui
     * @param httpHeaders httpHeaders
     * @param company company
     * @param establishment establishment
     * @param subcontractor subcontractor
     * @param min min
     * @param max max
     * @return Response
     */
    @GET
    @Path("{company}/{establishment}/{subcontractor}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response readMos(@Context final UriInfo ui, @Context final HttpHeaders httpHeaders,
                            @PathParam("company") final String company, @PathParam("establishment") final String establishment,
                            @PathParam("subcontractor") final String subcontractor, @QueryParam("from") final String min,
                            @QueryParam("to") final String max) {

        Response response = null;

        DateParam minDate = new DateParam(min);
        DateParam maxDate = new DateParam(max);

        try {
            VIFContext context = createVIFContext(contextBuilder.buildAndCheckContext(httpHeaders));
            try {

                // Sets the mo's selection
                MODateBoundSBean dateBoundSBean = new MODateBoundSBean();
                dateBoundSBean.setDateBounds(new DateBoundBean(minDate.getDate(), maxDate.getDate()));
                dateBoundSBean.setMoBoundReferenceType(MODateReferenceType.PRODUCTION_BEGIN);
                List<Mnemos.MOUse> moUses = new ArrayList<Mnemos.MOUse>();
                moUses.add(MOUse.SUBCONTRACTING);
                List<Mnemos.MOState> moStates = new ArrayList<Mnemos.MOState>();
                moStates.add(MOState.REOPEN);
                moStates.add(MOState.STARTED);
                moStates.add(MOState.CLOSED);
                MOSBean selection = new MOSBean();
                selection.setCompanyId(company);
                selection.setEstablishmentId(establishment);
                selection.setMoDateBoundSBean(dateBoundSBean);
                selection.setMoUses(moUses);
                selection.setSubcontractor(subcontractor);
                selection.setMoStates(moStates);

                List<MOBean> mos = moSBS.listMOBean(context, selection, null, false, 0, Integer.MAX_VALUE);
                List<MODTO> dtos = new ArrayList<MODTO>();
                MODTO dto;
                for (MOBean mo : mos) {
                    dto = Converter.convertMOBeanToDTO(mo);
                    dto.setComment(getMoSBS().getComment(context, mo.getMoKey()));
                    dtos.add(dto);
                }
                response = Response.status(Status.OK).entity(dtos).build();
            } catch (ServerBusinessException e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(e);
                }
                response = ResponseErrorHelper.createError(Status.INTERNAL_SERVER_ERROR, e);
            } finally {
                close(context);
            }
        } catch (BusinessException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(e);
            }
            response = ResponseErrorHelper.createError(Status.FORBIDDEN, e);
        }

        return response;
    }

    /**
     * Sets the contextBuilder.
     *
     * @param contextBuilder contextBuilder.
     */
    public void setContextBuilder(final ContextBuilderRS contextBuilder) {
        this.contextBuilder = contextBuilder;
    }

    /**
     * Sets the moSBS.
     *
     * @param moSBS moSBS.
     */
    public void setMoSBS(final MOSBS moSBS) {
        this.moSBS = moSBS;
    }

    /**
     * Sets the responseDTO.
     *
     * @param responseDTO responseDTO.
     */
    public void setResponseDTO(final ResponseDTO responseDTO) {
        this.responseDTO = responseDTO;
    }
}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoresPPOTtmoresourcecrivlan.java,v $
 * Created on 12 déc. 2014 by nle
 * Revision: $Revision: 1.4 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamorespxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.4 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoresPPOTtmoresourcecrivlan extends AbstractTempTable implements Serializable {


    private String ccri = "";

    private String val = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoresPPOTtmoresourcecrivlan() {
        super();
    }

    /**
     * Gets the ccri.
     *
     * @category getter
     * @return the ccri.
     */
    public final String getCcri() {
        return ccri;
    }

    /**
     * Sets the ccri.
     *
     * @category setter
     * @param ccri ccri.
     */
    public final void setCcri(final String ccri) {
        this.ccri = ccri;
    }

    /**
     * Gets the val.
     *
     * @category getter
     * @return the val.
     */
    public final String getVal() {
        return val;
    }

    /**
     * Sets the val.
     *
     * @category setter
     * @param val val.
     */
    public final void setVal(final String val) {
        this.val = val;
    }

}

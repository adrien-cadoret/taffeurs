/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoitemPPOTtmoitemcrivlot.java,v $
 * Created on 24 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamoitempxy.p.xml.
 * 
 * @author vl
 */
public class SoamoitemPPOTtmoitemcrivlot extends AbstractTempTable implements Serializable {

    private String ccri = "";

    private String val  = "";

    /**
     * Default Constructor.
     * 
     */
    public SoamoitemPPOTtmoitemcrivlot() {
        super();
    }

    /**
     * Gets the ccri.
     * 
     * @category getter
     * @return the ccri.
     */
    public final String getCcri() {
        return ccri;
    }

    /**
     * Gets the val.
     * 
     * @category getter
     * @return the val.
     */
    public final String getVal() {
        return val;
    }

    /**
     * Sets the ccri.
     * 
     * @category setter
     * @param ccri ccri.
     */
    public final void setCcri(final String ccri) {
        this.ccri = ccri;
    }

    /**
     * Sets the val.
     * 
     * @category setter
     * @param val val.
     */
    public final void setVal(final String val) {
        this.val = val;
    }

}

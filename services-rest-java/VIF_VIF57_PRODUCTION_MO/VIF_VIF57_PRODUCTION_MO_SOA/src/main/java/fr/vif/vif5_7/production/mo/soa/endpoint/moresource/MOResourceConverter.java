/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: MOResourceConverter.java,v $
 * Created on 4 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moresource;


import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.TimeHelper;
import fr.vif.vif5_7.production.mo.ChronoType;
import fr.vif.vif5_7.production.mo.CompEstabType;
import fr.vif.vif5_7.production.mo.InsertMOResourceDataType;
import fr.vif.vif5_7.production.mo.InsertMOResourceResponseType;
import fr.vif.vif5_7.production.mo.MOResourceInputMode;
import fr.vif.vif5_7.production.mo.MOResourceResponseType;
import fr.vif.vif5_7.production.mo.MOResourceSelectionType;
import fr.vif.vif5_7.production.mo.MOResourceSingleIdType;
import fr.vif.vif5_7.production.mo.NiMOResourceSingleIdType;
import fr.vif.vif5_7.production.mo.OperationSingleIdType;
import fr.vif.vif5_7.production.mo.ProcessingSingleIdType;
import fr.vif.vif5_7.production.mo.ReadMOResourceResponseType;
import fr.vif.vif5_7.production.mo.ResourceMOSingleIdType;
import fr.vif.vif5_7.production.mo.ResourceSingleIdType;
import fr.vif.vif5_7.production.mo.StandardResponseType;
import fr.vif.vif5_7.production.mo.StatusType;
import fr.vif.vif5_7.production.mo.UniqueValueCriteriaType;
import fr.vif.vif5_7.production.mo.UniqueValueCriterionType;
import fr.vif.vif5_7.production.mo.UseType;
import fr.vif.vif5_7.production.mo.soa.converters.MOCommonConverter;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourcecrivlan;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourceinsert;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourceinsertresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourcereadresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourceselection;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourcestatusresponse;


/**
 * MoResource web service converter class .
 * 
 * @author vl
 */
public final class MOResourceConverter extends MOCommonConverter {

    /** LOGGER. */
    private static final Logger        LOGGER = Logger.getLogger(MOResourceConverter.class);

    private static MOResourceConverter instance;

    /**
     * Default constructor.
     */
    private MOResourceConverter() {
    }

    /**
     * Returns the (unique) instance of the helper class (singleton).
     * 
     * @return the MOResourceConverter instance.
     */
    public static MOResourceConverter getInstance() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getInstance()");
        }

        if (instance == null) {
            instance = new MOResourceConverter();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getInstance()=" + instance);
        }
        return instance;
    }

    /**
     * Convert InsertMoResourceDataType to SoamoresPPOTtmoresourceinsert.
     * 
     * @param insertData InsertMoResourceDataType
     * @return SoamoresPPOTtmoresourceinsert
     */
    public SoamoresPPOTtmoresourceinsert convertInsertDataToTt(final InsertMOResourceDataType insertData) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertInsertDataToTt(insertData=" + insertData + ")");
        }

        SoamoresPPOTtmoresourceinsert ret = new SoamoresPPOTtmoresourceinsert();
        if (insertData != null) {

            CompEstabType compEstabType = insertData.getCompEstab();
            if (compEstabType != null) {
                ret.setCsoc(compEstabType.getCompany());
                ret.setCetab(compEstabType.getEstablishment());
            }

            ChronoType chronoType = insertData.getMoChrono();
            if (chronoType != null) {
                ret.setPrechro(emptyIfNull(chronoType.getPrechro()));
                ret.setChrono(zeroIfNull(chronoType.getChrono()));
            }

            MOResourceSingleIdType singleIdType = insertData.getMoResourceId();
            if (singleIdType != null) {
                NiMOResourceSingleIdType niMoResourceSingleIdType = singleIdType.getNiMOResourceId();
                if (niMoResourceSingleIdType != null) {
                    ret.setNi1(zeroIfNull(niMoResourceSingleIdType.getNi1()));
                    ret.setNi2(zeroIfNull(niMoResourceSingleIdType.getNi2()));
                    ret.setNlig(zeroIfNull(niMoResourceSingleIdType.getNlig()));
                }

                ResourceMOSingleIdType resourceMoSingleIdType = singleIdType.getResourceMOId();
                if (resourceMoSingleIdType != null) {
                    ret.setCoper(emptyIfNull(resourceMoSingleIdType.getOperation()));
                    ret.setCres(emptyIfNull(resourceMoSingleIdType.getResource()));
                    ret.setCrub(emptyIfNull(resourceMoSingleIdType.getResourceCategory()));
                    ret.setCact(emptyIfNull(resourceMoSingleIdType.getActivity()));
                }

            }

            UseType use = insertData.getUse();
            if (use != null) {
                ret.setCofuse(use.value());
            }

            MOResourceInputMode inputMode = insertData.getInputMode();
            if (inputMode != null) {
                ret.setInputmode(inputMode.value());
            }

            XMLGregorianCalendar effectiveBeginDateHour = insertData.getEffectiveBeginDateHour();
            if (effectiveBeginDateHour != null) {
                GregorianCalendar gc = effectiveBeginDateHour.toGregorianCalendar();
                ret.setDatdebr(DateHelper.atMidnight(gc.getTime()));
                ret.setHeurdebr(TimeHelper.getTime(gc.getTime()));
            }

            XMLGregorianCalendar effectiveEndDateHour = insertData.getEffectiveEndDateHour();
            if (effectiveEndDateHour != null) {
                GregorianCalendar gc = effectiveEndDateHour.toGregorianCalendar();
                ret.setDatfinr(DateHelper.atMidnight(gc.getTime()));
                ret.setHeurfinr(TimeHelper.getTime(gc.getTime()));
            }

            XMLGregorianCalendar productionDate = insertData.getProductionDate();
            if (productionDate != null) {
                GregorianCalendar gc = productionDate.toGregorianCalendar();
                ret.setDatprod(DateHelper.atMidnight(gc.getTime()));
            }

            ret.setCmotmvk(emptyIfNull(insertData.getReason()));
            ret.setCuser(emptyIfNull(insertData.getUser()));
            ret.setNbr(zeroIfNull(insertData.getEffectiveStaff()));
            ret.setDuree2(zeroIfNull(insertData.getEffectiveDuration()));

            ret.setLigne(emptyIfNull(insertData.getProductionLine()));
            ret.setCresmo(emptyIfNull(insertData.getStaffResource()));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertInsertDataToTt(insertData=" + insertData + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert InsertTtResponseHolder to InsertMoResourceResponseType.
     * 
     * @param insertTtResponseHolder ttHolder
     * @return InsertMoResourceResponseType
     */
    public InsertMOResourceResponseType convertInsertTtToResponse(
            final TempTableHolder<SoamoresPPOTtmoresourceinsertresponse> insertTtResponseHolder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertInsertTtToResponse(insertTtResponseHolder=" + insertTtResponseHolder + ")");
        }

        InsertMOResourceResponseType ret = new InsertMOResourceResponseType();

        if (insertTtResponseHolder != null) {
            List<SoamoresPPOTtmoresourceinsertresponse> insertTtResponseList = insertTtResponseHolder.getListValue();
            if (insertTtResponseList != null && insertTtResponseList.size() > 0) {
                SoamoresPPOTtmoresourceinsertresponse insertTtResponse = insertTtResponseList.get(0);

                ChronoType moChrono = new ChronoType();
                moChrono.setChrono(insertTtResponse.getChrono());
                moChrono.setPrechro(insertTtResponse.getPrechro());

                ret.setMoChrono(moChrono);
                ret.setNi1(insertTtResponse.getNi1());
                ret.setNi2(insertTtResponse.getNi2());
                ret.setNlig(insertTtResponse.getNlig());
                ret.setNi4(insertTtResponse.getNi4());
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertInsertTtToResponse(insertTtResponseHolder=" + insertTtResponseHolder + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert UniqueValueCriteriaType to SoamoresPPOTtmoresourcecriv.
     * 
     * @param moCriteria UniqueValueCriteriaType
     * @return SoamoresPPOTtmoresourcecriv
     */
    public List<SoamoresPPOTtmoresourcecrivlan> convertMOCriteriaToTtCriv(final UniqueValueCriteriaType moCriteria) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertMOCriteriaToTtCriv(moCriteria=" + moCriteria + ")");
        }

        List<SoamoresPPOTtmoresourcecrivlan> ret = new ArrayList<SoamoresPPOTtmoresourcecrivlan>();

        if (moCriteria != null) {
            SoamoresPPOTtmoresourcecrivlan ttmoresourcecriv;

            for (UniqueValueCriterionType criterion : moCriteria.getUniqueValueCriterion()) {
                ttmoresourcecriv = new SoamoresPPOTtmoresourcecrivlan();
                ttmoresourcecriv.setCcri(emptyIfNull(criterion.getCriterionCode()));
                ttmoresourcecriv.setVal(emptyIfNull(criterion.getCriterionValue()));
                ret.add(ttmoresourcecriv);
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertMOCriteriaToTtCriv(moCriteria=" + moCriteria + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert ReadResponseHolder to ReadMoResourceResponseType.
     * 
     * @param readResponseHolder SoamoresPPOTtmoresourcereadresponse
     * @return ReadMoResourceResponseType
     */
    public ReadMOResourceResponseType convertReadTtToResponse(
            final TempTableHolder<SoamoresPPOTtmoresourcereadresponse> readResponseHolder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertReadTtToResponse(readResponseHolder=" + readResponseHolder + ")");
        }

        ReadMOResourceResponseType ret = new ReadMOResourceResponseType();
        if (readResponseHolder != null) {

            MOResourceResponseType moResourceResponseType;

            for (SoamoresPPOTtmoresourcereadresponse readResponse : readResponseHolder.getListValue()) {

                moResourceResponseType = new MOResourceResponseType();
                moResourceResponseType.setMoResourceId(getMOResourceId(readResponse.getCsoc(), readResponse.getCetab(),
                        readResponse.getPrechro(), readResponse.getChrono(), readResponse.getNi1(),
                        readResponse.getNi2(), readResponse.getNlig(), readResponse.getNi4()));

                moResourceResponseType.setOperation(getCodeLabelType(readResponse.getCoper(), readResponse.getLoper()));
                moResourceResponseType.setSector(getCodeLabelType(readResponse.getCsecteur(),
                        readResponse.getLsecteur()));
                moResourceResponseType.setPrimaryWorkCenter(getCodeLabelType(readResponse.getCreskoto(),
                        readResponse.getLreskoto()));
                moResourceResponseType.setResourceCategory(getCodeLabelType(readResponse.getCrub(),
                        readResponse.getLrub()));
                moResourceResponseType.setResource(getCodeLabelType(readResponse.getCres(), readResponse.getLres()));
                moResourceResponseType.setEffectiveDateHourBounds(getEffectiveDateHourBounds(readResponse.getDatdebr(),
                        readResponse.getHeurdebr(), readResponse.getDatfinr(), readResponse.getHeurfinr()));
                moResourceResponseType.setEffectiveDuration(readResponse.getDuree2());
                moResourceResponseType.setEffectiveStaff(readResponse.getNbr());
                moResourceResponseType
                        .setReason(getCodeLabelType(readResponse.getCmotmvk(), readResponse.getLmotmvk()));
                moResourceResponseType.setModification(getModification(readResponse.getDatmod(),
                        readResponse.getHeurmod(), readResponse.getCuser()));
                ret.getMoResourceResponses().add(moResourceResponseType);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertReadTtToResponse(readResponseHolder=" + readResponseHolder + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert MoResourceSelectionType to SoamoresPPOTtmoresourceselection .
     * 
     * @param moResourceSelectionType MoResourceSelectionType
     * @return SoamoresPPOTtmoresourceselection
     */
    public SoamoresPPOTtmoresourceselection convertSelectionToTt(final MOResourceSelectionType moResourceSelectionType) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertSelectionToTt(moResourceSelectionType=" + moResourceSelectionType + ")");
        }

        SoamoresPPOTtmoresourceselection ret = new SoamoresPPOTtmoresourceselection();

        if (moResourceSelectionType != null) {
            CompEstabType compEstabType = moResourceSelectionType.getCompEstab();
            if (compEstabType != null) {
                ret.setCsoc(compEstabType.getCompany());
                ret.setCetab(compEstabType.getEstablishment());
            }

            ChronoType chronoType = moResourceSelectionType.getMoChrono();
            if (chronoType != null) {
                ret.setPrechro(emptyIfNull(chronoType.getPrechro()));
                ret.setChrono(zeroIfNull(chronoType.getChrono()));
            }

            UseType use = moResourceSelectionType.getUse();
            if (use != null) {
                ret.setCofuse(use.value());
            }

            ProcessingSingleIdType processing = moResourceSelectionType.getProcessingId();
            if (processing != null) {
                ret.setCact(emptyIfNull(processing.getActivity()));
                ret.setNi1(zeroIfNull(processing.getNi1()));
            }

            OperationSingleIdType operation = moResourceSelectionType.getOperationId();
            if (operation != null) {
                ret.setCoper(emptyIfNull(operation.getOperation()));
                ret.setNi2(zeroIfNull(operation.getNi2()));
            }

            ResourceSingleIdType resource = moResourceSelectionType.getResourceId();
            if (resource != null) {
                ret.setNlig(zeroIfNull(resource.getNlig()));
                ret.setCres(emptyIfNull(resource.getResource()));
                ret.setCrub(emptyIfNull(resource.getResourceCategory()));

            }

            XMLGregorianCalendar productionDate = moResourceSelectionType.getProductionDate();
            if (productionDate != null) {
                GregorianCalendar gc = productionDate.toGregorianCalendar();
                ret.setDatprod(DateHelper.atMidnight(gc.getTime()));
            }

            ret.setCsecteur(emptyIfNull(moResourceSelectionType.getSector()));
            ret.setCreskoto(emptyIfNull(moResourceSelectionType.getPrimaryWorkCenter()));
            ret.setNi4(zeroIfNull(moResourceSelectionType.getNi4()));

            ret.setLigne(emptyIfNull(moResourceSelectionType.getProductionLine()));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertSelectionToTt(moResourceSelectionType=" + moResourceSelectionType + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert StatusResponseHolder to StandardResponseType.
     * 
     * @param statusResponseHolder status response holder
     * @return StandardResponseType
     */
    public StandardResponseType convertStatusResponseToStandardResponse(
            final TempTableHolder<SoamoresPPOTtmoresourcestatusresponse> statusResponseHolder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertStatusResponseToStandardResponse(statusResponseHolder=" + statusResponseHolder
                    + ")");
        }

        StandardResponseType ret = new StandardResponseType();

        if (statusResponseHolder != null) {
            List<SoamoresPPOTtmoresourcestatusresponse> statusResponseList = statusResponseHolder.getListValue();
            if (statusResponseList != null && statusResponseList.size() > 0) {
                SoamoresPPOTtmoresourcestatusresponse statusResponse = statusResponseList.get(0);
                ret.setMessage(statusResponse.getMsg());
                ret.setStatus(StatusType.fromValue(statusResponse.getCstatus()));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertStatusResponseToStandardResponse(statusResponseHolder=" + statusResponseHolder
                    + ")=" + ret);
        }
        return ret;
    }

}

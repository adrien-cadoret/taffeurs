/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 3 août 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.mo;


import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.vif5_7.activities.activities.business.beans.common.fabrication.Fabrication;
import fr.vif.vif5_7.activities.activities.constants.Mnemos;
import fr.vif.vif5_7.gen.comment.business.beans.common.Comment;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.unit.MOQuantityUnit;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOState;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOUse;


/**
 * Class for MO Data Transfer Object.
 *
 * @author ac
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class MODTO {

    private Date            effectiveBeginDateTime = null;
    private Date            effectiveEndDateTime   = null;
    private Fabrication     fabrication            = new Fabrication();
    private Mnemos.FlowType flowType               = null;
    private Date            forecastBeginDateTime  = null;
    private Date            forecastEndDateTime    = null;
    private CodeLabels      itemCLs                = new CodeLabels();
    private String          label                  = "";
    private MOKey           moKey                  = new MOKey();
    private MOQuantityUnit  moQuantityUnit         = new MOQuantityUnit();
    private MOState         moState                = null;
    private MOUse           moUse                  = null;
    private int             priority               = 0;
    private String          shortLabel             = "";
    private MOType          moType                 = null;
    private Chrono          chronoOrig             = null;
    private CodeLabel       primaryRessource       = new CodeLabel();
    private String          valueCrit1             = null;
    private String          valueCrit2             = null;
    private String          valueCrit3             = null;
    private String          valueCrit4             = null;
    private String          valueCrit5             = null;
    private String          valueCalculatedCrit1   = null;
    private String          valueCalculatedCrit2   = null;
    private String          valueCalculatedCrit3   = null;
    private String          valueCalculatedCrit4   = null;
    private String          valueCalculatedCrit5   = null;
    private String          subcontractor          = null;
    private Comment         comment                = null;

    /**
     * Gets the chronoOrig.
     * 
     * @return the chronoOrig.
     */
    public Chrono getChronoOrig() {
        return chronoOrig;
    }

    /**
     * Gets the comment.
     *
     * @return the comment.
     */
    public Comment getComment() {
        return comment;
    }

    /**
     * Gets the effectiveBeginDateTime.
     * 
     * @category getter
     * @return the effectiveBeginDateTime.
     */
    public Date getEffectiveBeginDateTime() {
        return effectiveBeginDateTime;
    }

    /**
     * Gets the effectiveEndDateTime.
     * 
     * @category getter
     * @return the effectiveEndDateTime.
     */
    public Date getEffectiveEndDateTime() {
        return effectiveEndDateTime;
    }

    /**
     * Gets the fabrication.
     * 
     * @category getter
     * @return the fabrication.
     */
    public Fabrication getFabrication() {
        return fabrication;
    }

    /**
     * Gets the flowType.
     * 
     * @category getter
     * @return the flowType.
     */
    public Mnemos.FlowType getFlowType() {
        return flowType;
    }

    /**
     * Gets the forecastBeginDateTime.
     * 
     * @category getter
     * @return the forecastBeginDateTime.
     */
    public Date getForecastBeginDateTime() {
        return forecastBeginDateTime;
    }

    /**
     * Gets the forecastEndDateTime.
     * 
     * @category getter
     * @return the forecastEndDateTime.
     */
    public Date getForecastEndDateTime() {
        return forecastEndDateTime;
    }

    /**
     * Gets the itemCLs.
     * 
     * @category getter
     * @return the itemCLs.
     */
    public CodeLabels getItemCLs() {
        return itemCLs;
    }

    /**
     * Gets the label.
     * 
     * @category getter
     * @return the label.
     */
    public String getLabel() {
        return label;
    }

    /**
     * Gets the moKey.
     * 
     * @category getter
     * @return the moKey.
     */
    public MOKey getMoKey() {
        return moKey;
    }

    /**
     * Gets the moQuantityUnit.
     * 
     * @category getter
     * @return the moQuantityUnit.
     */
    public MOQuantityUnit getMoQuantityUnit() {
        return moQuantityUnit;
    }

    /**
     * Gets the moState.
     * 
     * @category getter
     * @return the moState.
     */
    public MOState getMoState() {
        return moState;
    }

    /**
     * Gets the moType.
     * 
     * @category getter
     * @return the moType.
     */
    public MOType getMoType() {
        return moType;
    }

    /**
     * Gets the moUse.
     * 
     * @category getter
     * @return the moUse.
     */
    public MOUse getMoUse() {
        return moUse;
    }

    /**
     * Gets the primaryRessource.
     * 
     * @return the primaryRessource.
     */
    public CodeLabel getPrimaryRessource() {
        return primaryRessource;
    }

    /**
     * Gets the priority.
     * 
     * @category getter
     * @return the priority.
     */
    public int getPriority() {
        return priority;
    }

    /**
     * Gets the shortLabel.
     * 
     * @category getter
     * @return the shortLabel.
     */
    public String getShortLabel() {
        return shortLabel;
    }

    /**
     * Gets the subcontractor.
     *
     * @return the subcontractor.
     */
    public String getSubcontractor() {
        return subcontractor;
    }

    /**
     * Gets the valueCalculatedCrit1.
     * 
     * @return the valueCalculatedCrit1.
     */
    public String getValueCalculatedCrit1() {
        return valueCalculatedCrit1;
    }

    /**
     * Gets the valueCalculatedCrit2.
     * 
     * @return the valueCalculatedCrit2.
     */
    public String getValueCalculatedCrit2() {
        return valueCalculatedCrit2;
    }

    /**
     * Gets the valueCalculatedCrit3.
     * 
     * @return the valueCalculatedCrit3.
     */
    public String getValueCalculatedCrit3() {
        return valueCalculatedCrit3;
    }

    /**
     * Gets the valueCalculatedCrit4.
     * 
     * @return the valueCalculatedCrit4.
     */
    public String getValueCalculatedCrit4() {
        return valueCalculatedCrit4;
    }

    /**
     * Gets the valueCalculatedCrit5.
     * 
     * @return the valueCalculatedCrit5.
     */
    public String getValueCalculatedCrit5() {
        return valueCalculatedCrit5;
    }

    /**
     * Gets the valueCrit1.
     * 
     * @return the valueCrit1.
     */
    public String getValueCrit1() {
        return valueCrit1;
    }

    /**
     * Gets the valueCrit2.
     * 
     * @return the valueCrit2.
     */
    public String getValueCrit2() {
        return valueCrit2;
    }

    /**
     * Gets the valueCrit3.
     * 
     * @return the valueCrit3.
     */
    public String getValueCrit3() {
        return valueCrit3;
    }

    /**
     * Gets the valueCrit4.
     * 
     * @return the valueCrit4.
     */
    public String getValueCrit4() {
        return valueCrit4;
    }

    /**
     * Gets the valueCrit5.
     * 
     * @return the valueCrit5.
     */
    public String getValueCrit5() {
        return valueCrit5;
    }

    /**
     * Sets the chronoOrig.
     * 
     * @param chronoOrig chronoOrig.
     */
    public void setChronoOrig(final Chrono chronoOrig) {
        this.chronoOrig = chronoOrig;
    }

    /**
     * Sets the comment.
     *
     * @param comment comment.
     */
    public void setComment(final Comment comment) {
        this.comment = comment;
    }

    /**
     * Sets the effectiveBeginDateTime.
     * 
     * @category setter
     * @param effectiveBeginDateTime effectiveBeginDateTime.
     */
    public void setEffectiveBeginDateTime(final Date effectiveBeginDateTime) {
        this.effectiveBeginDateTime = effectiveBeginDateTime;
    }

    /**
     * Sets the effectiveEndDateTime.
     * 
     * @category setter
     * @param effectiveEndDateTime effectiveEndDateTime.
     */
    public void setEffectiveEndDateTime(final Date effectiveEndDateTime) {
        this.effectiveEndDateTime = effectiveEndDateTime;
    }

    /**
     * Sets the fabrication.
     * 
     * @category setter
     * @param fabrication fabrication.
     */
    public void setFabrication(final Fabrication fabrication) {
        this.fabrication = fabrication;
    }

    /**
     * Sets the flowType.
     * 
     * @category setter
     * @param flowType flowType.
     */
    public void setFlowType(final Mnemos.FlowType flowType) {
        this.flowType = flowType;
    }

    /**
     * Sets the forecastBeginDateTime.
     * 
     * @category setter
     * @param forecastBeginDateTime forecastBeginDateTime.
     */
    public void setForecastBeginDateTime(final Date forecastBeginDateTime) {
        this.forecastBeginDateTime = forecastBeginDateTime;
    }

    /**
     * Sets the forecastEndDateTime.
     * 
     * @category setter
     * @param forecastEndDateTime forecastEndDateTime.
     */
    public void setForecastEndDateTime(final Date forecastEndDateTime) {
        this.forecastEndDateTime = forecastEndDateTime;
    }

    /**
     * Sets the itemCLs.
     * 
     * @category setter
     * @param itemCLs itemCLs.
     */
    public void setItemCLs(final CodeLabels itemCLs) {
        this.itemCLs = itemCLs;
    }

    /**
     * Sets the label.
     * 
     * @category setter
     * @param label label.
     */
    public void setLabel(final String label) {
        this.label = label;
    }

    /**
     * Sets the moKey.
     * 
     * @category setter
     * @param moKey moKey.
     */
    public void setMoKey(final MOKey moKey) {
        this.moKey = moKey;
    }

    /**
     * Sets the moQuantityUnit.
     * 
     * @category setter
     * @param moQuantityUnit moQuantityUnit.
     */
    public void setMoQuantityUnit(final MOQuantityUnit moQuantityUnit) {
        this.moQuantityUnit = moQuantityUnit;
    }

    /**
     * Sets the moState.
     * 
     * @category setter
     * @param moState moState.
     */
    public void setMoState(final MOState moState) {
        this.moState = moState;
    }

    /**
     * Sets the moType.
     * 
     * @category setter
     * @param moType moType.
     */
    public void setMoType(final MOType moType) {
        this.moType = moType;
    }

    /**
     * Sets the moUse.
     * 
     * @category setter
     * @param moUse moUse.
     */
    public void setMoUse(final MOUse moUse) {
        this.moUse = moUse;
    }

    /**
     * Sets the primaryRessource.
     * 
     * @param primaryRessource primaryRessource.
     */
    public void setPrimaryRessource(final CodeLabel primaryRessource) {
        this.primaryRessource = primaryRessource;
    }

    /**
     * Sets the priority.
     * 
     * @category setter
     * @param priority priority.
     */
    public void setPriority(final int priority) {
        this.priority = priority;
    }

    /**
     * Sets the shortLabel.
     * 
     * @category setter
     * @param shortLabel shortLabel.
     */
    public void setShortLabel(final String shortLabel) {
        this.shortLabel = shortLabel;
    }

    /**
     * Sets the subcontractor.
     *
     * @param subcontractor subcontractor.
     */
    public void setSubcontractor(final String subcontractor) {
        this.subcontractor = subcontractor;
    }

    /**
     * Sets the valueCalculatedCrit1.
     * 
     * @param valueCalculatedCrit1 valueCalculatedCrit1.
     */
    public void setValueCalculatedCrit1(final String valueCalculatedCrit1) {
        this.valueCalculatedCrit1 = valueCalculatedCrit1;
    }

    /**
     * Sets the valueCalculatedCrit2.
     * 
     * @param valueCalculatedCrit2 valueCalculatedCrit2.
     */
    public void setValueCalculatedCrit2(final String valueCalculatedCrit2) {
        this.valueCalculatedCrit2 = valueCalculatedCrit2;
    }

    /**
     * Sets the valueCalculatedCrit3.
     * 
     * @param valueCalculatedCrit3 valueCalculatedCrit3.
     */
    public void setValueCalculatedCrit3(final String valueCalculatedCrit3) {
        this.valueCalculatedCrit3 = valueCalculatedCrit3;
    }

    /**
     * Sets the valueCalculatedCrit4.
     * 
     * @param valueCalculatedCrit4 valueCalculatedCrit4.
     */
    public void setValueCalculatedCrit4(final String valueCalculatedCrit4) {
        this.valueCalculatedCrit4 = valueCalculatedCrit4;
    }

    /**
     * Sets the valueCalculatedCrit5.
     * 
     * @param valueCalculatedCrit5 valueCalculatedCrit5.
     */
    public void setValueCalculatedCrit5(final String valueCalculatedCrit5) {
        this.valueCalculatedCrit5 = valueCalculatedCrit5;
    }

    /**
     * Sets the valueCrit1.
     * 
     * @param valueCrit1 valueCrit1.
     */
    public void setValueCrit1(final String valueCrit1) {
        this.valueCrit1 = valueCrit1;
    }

    /**
     * Sets the valueCrit2.
     * 
     * @param valueCrit2 valueCrit2.
     */
    public void setValueCrit2(final String valueCrit2) {
        this.valueCrit2 = valueCrit2;
    }

    /**
     * Sets the valueCrit3.
     * 
     * @param valueCrit3 valueCrit3.
     */
    public void setValueCrit3(final String valueCrit3) {
        this.valueCrit3 = valueCrit3;
    }

    /**
     * Sets the valueCrit4.
     * 
     * @param valueCrit4 valueCrit4.
     */
    public void setValueCrit4(final String valueCrit4) {
        this.valueCrit4 = valueCrit4;
    }

    /**
     * Sets the valueCrit5.
     * 
     * @param valueCrit5 valueCrit5.
     */
    public void setValueCrit5(final String valueCrit5) {
        this.valueCrit5 = valueCrit5;
    }

}

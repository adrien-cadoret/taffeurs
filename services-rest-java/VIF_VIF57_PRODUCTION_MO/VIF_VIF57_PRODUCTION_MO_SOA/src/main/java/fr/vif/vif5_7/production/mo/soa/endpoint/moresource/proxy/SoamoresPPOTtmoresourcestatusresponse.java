/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoresPPOTtmoresourcestatusresponse.java,v $
 * Created on 12 déc. 2014 by nle
 * Revision: $Revision: 1.4 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamorespxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.4 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoresPPOTtmoresourcestatusresponse extends AbstractTempTable implements Serializable {


    private String cstatus = "";

    private String msg = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoresPPOTtmoresourcestatusresponse() {
        super();
    }

    /**
     * Gets the cstatus.
     *
     * @category getter
     * @return the cstatus.
     */
    public final String getCstatus() {
        return cstatus;
    }

    /**
     * Sets the cstatus.
     *
     * @category setter
     * @param cstatus cstatus.
     */
    public final void setCstatus(final String cstatus) {
        this.cstatus = cstatus;
    }

    /**
     * Gets the msg.
     *
     * @category getter
     * @return the msg.
     */
    public final String getMsg() {
        return msg;
    }

    /**
     * Sets the msg.
     *
     * @category setter
     * @param msg msg.
     */
    public final void setMsg(final String msg) {
        this.msg = msg;
    }

}

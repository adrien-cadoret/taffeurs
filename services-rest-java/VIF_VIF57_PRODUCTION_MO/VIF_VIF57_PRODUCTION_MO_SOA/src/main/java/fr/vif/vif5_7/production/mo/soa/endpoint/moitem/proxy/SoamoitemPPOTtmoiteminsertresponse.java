/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoitemPPOTtmoiteminsertresponse.java,v $
 * Created on 24 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamoitempxy.p.xml.
 * 
 * @author vl
 */
public class SoamoitemPPOTtmoiteminsertresponse extends AbstractTempTable implements Serializable {

    private int    chrono  = 0;

    private String lot     = "";

    private int    ni1     = 0;

    private int    ni2     = 0;

    private int    ni3     = 0;

    private int    nlig    = 0;

    private String nsc     = "";

    private String prechro = "";

    /**
     * Default Constructor.
     * 
     */
    public SoamoitemPPOTtmoiteminsertresponse() {
        super();
    }

    /**
     * Gets the chrono.
     * 
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Gets the lot.
     * 
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Gets the ni1.
     * 
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Gets the ni2.
     * 
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Gets the ni3.
     * 
     * @category getter
     * @return the ni3.
     */
    public final int getNi3() {
        return ni3;
    }

    /**
     * Gets the nlig.
     * 
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Gets the nsc.
     * 
     * @category getter
     * @return the nsc.
     */
    public final String getNsc() {
        return nsc;
    }

    /**
     * Gets the prechro.
     * 
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Sets the chrono.
     * 
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Sets the lot.
     * 
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Sets the ni1.
     * 
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Sets the ni2.
     * 
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Sets the ni3.
     * 
     * @category setter
     * @param ni3 ni3.
     */
    public final void setNi3(final int ni3) {
        this.ni3 = ni3;
    }

    /**
     * Sets the nlig.
     * 
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Sets the nsc.
     * 
     * @category setter
     * @param nsc nsc.
     */
    public final void setNsc(final String nsc) {
        this.nsc = nsc;
    }

    /**
     * Sets the prechro.
     * 
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoPPOTtIncriteria.java,v $
 * Created on 19 nov. 2014 by nle
 * Revision: $Revision: 1.3 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamopxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.3 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoPPOTtIncriteria extends AbstractTempTable implements Serializable {


    private String ccri = "";

    private String msaisie = "";

    private String typcri = "";

    private String valmax = "";

    private String valmin = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoPPOTtIncriteria() {
        super();
    }

    /**
     * Gets the ccri.
     *
     * @category getter
     * @return the ccri.
     */
    public final String getCcri() {
        return ccri;
    }

    /**
     * Sets the ccri.
     *
     * @category setter
     * @param ccri ccri.
     */
    public final void setCcri(final String ccri) {
        this.ccri = ccri;
    }

    /**
     * Gets the msaisie.
     *
     * @category getter
     * @return the msaisie.
     */
    public final String getMsaisie() {
        return msaisie;
    }

    /**
     * Sets the msaisie.
     *
     * @category setter
     * @param msaisie msaisie.
     */
    public final void setMsaisie(final String msaisie) {
        this.msaisie = msaisie;
    }

    /**
     * Gets the typcri.
     *
     * @category getter
     * @return the typcri.
     */
    public final String getTypcri() {
        return typcri;
    }

    /**
     * Sets the typcri.
     *
     * @category setter
     * @param typcri typcri.
     */
    public final void setTypcri(final String typcri) {
        this.typcri = typcri;
    }

    /**
     * Gets the valmax.
     *
     * @category getter
     * @return the valmax.
     */
    public final String getValmax() {
        return valmax;
    }

    /**
     * Sets the valmax.
     *
     * @category setter
     * @param valmax valmax.
     */
    public final void setValmax(final String valmax) {
        this.valmax = valmax;
    }

    /**
     * Gets the valmin.
     *
     * @category getter
     * @return the valmin.
     */
    public final String getValmin() {
        return valmin;
    }

    /**
     * Sets the valmin.
     *
     * @category setter
     * @param valmin valmin.
     */
    public final void setValmin(final String valmin) {
        this.valmin = valmin;
    }

}

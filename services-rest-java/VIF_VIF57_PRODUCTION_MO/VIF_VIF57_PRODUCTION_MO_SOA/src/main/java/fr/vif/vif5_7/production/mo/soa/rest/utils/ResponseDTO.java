/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on Jul 22, 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils;


import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import fr.vif.vif5_7.production.mo.StatusType;


/**
 * Response DTO which take 3 attributes : an http status code, a message and a DTO.
 *
 * @author ac
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class ResponseDTO {

    private StatusType status;
    private String     message;
    private String     uri;

    /**
     * Gets the value of the message property.
     * 
     * @return possible object is {@link String }
     * 
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return possible object is {@link StatusType }
     * 
     */
    public StatusType getStatus() {
        return status;
    }

    /**
     * Gets the uri.
     *
     * @return the uri.
     */
    public String getUri() {
        return uri;
    }

    /**
     * Sets the value of the message property.
     * 
     * @param value allowed object is {@link String }
     * 
     */
    public void setMessage(final String value) {
        this.message = value;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value allowed object is {@link StatusType }
     * 
     */
    public void setStatus(final StatusType value) {
        this.status = value;
    }

    /**
     * Sets the uri.
     *
     * @param uri uri.
     */
    public void setUri(final String uri) {
        this.uri = uri;
    }

}

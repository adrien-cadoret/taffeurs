/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on Jul 7, 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.moitem;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriInfo;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.soa.endpoint.AbstractSOAEndPoint;
import fr.vif.vif5_7.production.mo.StatusType;
import fr.vif.vif5_7.production.mo.UniqueValueCriteriaType;
import fr.vif.vif5_7.production.mo.UseType;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.production.mo.business.services.libraries.operationitem.MOItemSBS;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.MOItemConverter;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPO;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemcrivlan;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemcrivlot;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoiteminsert;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoiteminsertresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemselection;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemstatusresponse;
import fr.vif.vif5_7.production.mo.soa.rest.utils.ContextBuilderRS;
import fr.vif.vif5_7.production.mo.soa.rest.utils.Converter;
import fr.vif.vif5_7.production.mo.soa.rest.utils.ResponseDTO;
import fr.vif.vif5_7.production.mo.soa.rest.utils.error.ResponseErrorHelper;


/**
 * REST Endpoint which is used to retrieve, insert, update or delete mo items.
 *
 * @author ac
 */
@Produces({ MediaType.APPLICATION_JSON })
@Consumes({ MediaType.APPLICATION_JSON })
@Path("/moitems/")
@Service
public class MoItemEndPointRS extends AbstractSOAEndPoint {

    /** LOGGER. */
    private static final Logger LOGGER       = Logger.getLogger(MoItemEndPointRS.class);

    @Autowired
    @Qualifier("MOSBSImpl")
    private MOSBS               moSBS;

    @Autowired
    private MOItemSBS           moItemSBS;

    @Autowired
    private ContextBuilderRS    contextBuilder;

    @Autowired
    private SoamoitemPPO        soamoitemPPO = null;

    private MOItemConverter     converter;

    private ResponseDTO         responseDTO;

    /**
     *
     * Delete a detailed mo item.
     *
     * @param httpHeaders header
     * @param company company
     * @param establishment establishment
     * @param prechro prechro
     * @param chrono chrono
     * @param ni1 ni1
     * @param ni2 ni2
     * @param ni3 ni3
     * @return response
     */
    @DELETE
    @Path("{company}/{establishment}/{prechro}/{chrono}/{ni1}/{ni2}/{ni3}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response deleteDetailedMoItem(@Context final UriInfo ui, @Context final HttpHeaders httpHeaders,
                                         @PathParam("company") final String company, @PathParam("establishment") final String establishment,
                                         @PathParam("prechro") final String prechro, @PathParam("chrono") final int chrono,
                                         @PathParam("ni1") final int ni1, @PathParam("ni2") final int ni2, @PathParam("ni3") final int ni3) {

        Response response = null;

        try {
            VIFContext context = createVIFContext(contextBuilder.buildAndCheckContext(httpHeaders));
            try {
                // Init converter
                converter = MOItemConverter.getInstance();

                // Set ttMOItemSelection parameter
                List<SoamoitemPPOTtmoitemselection> ttMoItemSelection = new ArrayList<SoamoitemPPOTtmoitemselection>();
                ttMoItemSelection.add(converter.convertSelectionToTt(Converter.createMOItemSelectionType(company,
                        establishment, prechro, chrono, UseType.SST, ni1, ni2, ni3)));

                // Set ttMOItemCrivLan parameter
                List<SoamoitemPPOTtmoitemcrivlan> ttMOItemCrivLans = converter.convertMOCriteriaToTtCriv(null);

                // Create response holder
                TempTableHolder<SoamoitemPPOTtmoitemstatusresponse> statusResponseHolder = new TempTableHolder<SoamoitemPPOTtmoitemstatusresponse>();

                // Call appServer
                getSoamoitemPPO().deleteMOItem(context, ttMoItemSelection, ttMOItemCrivLans, statusResponseHolder);

                // Manage Response

                if (statusResponseHolder != null) {
                    List<SoamoitemPPOTtmoitemstatusresponse> statusResponseList = statusResponseHolder.getListValue();
                    if (statusResponseList != null && statusResponseList.size() > 0) {
                        SoamoitemPPOTtmoitemstatusresponse statusResponse = statusResponseList.get(0);
                        getResponseDTO().setMessage(statusResponse.getMsg());
                        getResponseDTO().setStatus(StatusType.fromValue(statusResponse.getCstatus()));
                    }
                }

                if (!StatusType.ERROR.equals(getResponseDTO().getStatus())) {
                    response = Response.status(Status.NO_CONTENT).entity(getResponseDTO()).build();
                } else {
                    response = Response.status(Status.BAD_REQUEST).entity(getResponseDTO()).build();
                }

            } catch (ProgressBusinessException e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(e);
                }
                response = ResponseErrorHelper.createError(Status.INTERNAL_SERVER_ERROR, e);
            } finally {
                close(context);
            }
        } catch (BusinessException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(e);
            }
            response = ResponseErrorHelper.createError(Status.FORBIDDEN, e);
        }

        return response;
    }

    /**
     * Gets the contextBuilder.
     *
     * @return the contextBuilder.
     */
    public ContextBuilderRS getContextBuilder() {
        return contextBuilder;
    }

    /**
     * Gets the converter.
     *
     * @return the converter.
     */
    public MOItemConverter getConverter() {
        return converter;
    }

    /**
     * Gets the moItemSBS.
     *
     * @return the moItemSBS.
     */
    public MOItemSBS getMoItemSBS() {
        return moItemSBS;
    }

    /**
     * Gets the moSBS.
     *
     * @return the moSBS.
     */
    public MOSBS getMoSBS() {
        return moSBS;
    }

    /**
     * Gets the getResponseDTO().
     *
     * @return the getResponseDTO().
     */
    public ResponseDTO getResponseDTO() {
        if (responseDTO == null) {
            responseDTO = new ResponseDTO();
        }
        return responseDTO;
    }

    /**
     * Gets the soamoitemPPO.
     *
     * @return the soamoitemPPO.
     */
    public SoamoitemPPO getSoamoitemPPO() {
        return soamoitemPPO;
    }

    /**
     * Create or update one contact.
     *
     * @param httpHeaders http headers
     * @param company compabny
     * @param establishment establishment
     * @param kotadDto the kotad
     * @return customer created or updated
     */
    @POST
    @Path("{company}/{establishment}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response insertDetailedMoItem(@Context final UriInfo ui, @Context final HttpHeaders httpHeaders,
                                         @PathParam("company") final String company, @PathParam("establishment") final String establishment,
                                         final KotadDTO kotadDto) {

        Response response = null;

        try {
            VIFContext context = createVIFContext(contextBuilder.buildAndCheckContext(httpHeaders));
            try {
                // Init converter
                converter = MOItemConverter.getInstance();

                // Set ttMOItemInsert parameter
                List<SoamoitemPPOTtmoiteminsert> ttMOItemInserts = new ArrayList<SoamoitemPPOTtmoiteminsert>();
                ttMOItemInserts.add(converter.convertInsertDataToTt(Converter.convertDTOToInsertMOItemData(kotadDto)));

                // Set ttMOItemCrivLan parameter
                List<SoamoitemPPOTtmoitemcrivlan> ttMOItemCrivLans = converter
                        .convertMOCriteriaToTtCriv(new UniqueValueCriteriaType());

                // Set ttMOItemCrivLot parameter
                List<SoamoitemPPOTtmoitemcrivlot> ttMOItemCrivLots = converter
                        .convertBatchCriteriaToTtCriv(new UniqueValueCriteriaType());

                // Create response holder
                TempTableHolder<SoamoitemPPOTtmoiteminsertresponse> insertResponseHolder = new TempTableHolder<SoamoitemPPOTtmoiteminsertresponse>();
                TempTableHolder<SoamoitemPPOTtmoitemstatusresponse> statusResponseHolder = new TempTableHolder<SoamoitemPPOTtmoitemstatusresponse>();

                // Call appServer
                getSoamoitemPPO().insertMOItem(context, ttMOItemInserts, ttMOItemCrivLans, ttMOItemCrivLots,
                        statusResponseHolder, insertResponseHolder);

                // Manage Response
                // getResponseDTO() = new getResponseDTO()();

                if (statusResponseHolder != null) {
                    List<SoamoitemPPOTtmoitemstatusresponse> statusResponseList = statusResponseHolder.getListValue();
                    if (statusResponseList != null && statusResponseList.size() > 0) {
                        SoamoitemPPOTtmoitemstatusresponse statusResponse = statusResponseList.get(0);
                        getResponseDTO().setMessage(statusResponse.getMsg());
                        getResponseDTO().setStatus(StatusType.fromValue(statusResponse.getCstatus()));
                        if (insertResponseHolder != null && insertResponseHolder.getListValue().size() > 0) {
                            String uri = ui.getAbsolutePath().toString();
                            uri = uri + "/" + insertResponseHolder.getListValue().get(0).getPrechro() + "/"
                                    + insertResponseHolder.getListValue().get(0).getChrono() + "/"
                                    + insertResponseHolder.getListValue().get(0).getNi1() + "/"
                                    + insertResponseHolder.getListValue().get(0).getNi2() + "/"
                                    + insertResponseHolder.getListValue().get(0).getNi3();

                            getResponseDTO().setUri(uri);
                        } else {
                            getResponseDTO().setUri("");
                        }
                    }
                }
                if (!StatusType.ERROR.equals(getResponseDTO().getStatus())) {
                    response = Response.status(Status.CREATED).entity(getResponseDTO()).build();
                } else {
                    response = Response.status(Status.BAD_REQUEST).entity(getResponseDTO()).build();
                }

            } finally {
                close(context);
            }
        } catch (BusinessException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(e);
            }
            response = ResponseErrorHelper.createError(Status.FORBIDDEN, e);
        } catch (ProgressBusinessException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(e);
            }
            response = ResponseErrorHelper.createError(Status.INTERNAL_SERVER_ERROR, e);
        }

        return response;
    }

    /**
     *
     * Gets a detailed mo item.
     *
     * @param httpHeaders header
     * @param company company
     * @param establishment establishment
     * @param prechro prechro
     * @param chrono chrono
     * @param ni1 ni1
     * @param ni2 ni2
     * @param ni3 ni3
     * @return response
     */
    @GET
    @Path("{company}/{establishment}/{prechro}/{chrono}/{ni1}/{ni2}/{ni3}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response readDetailedMoItem(@Context final UriInfo ui, @Context final HttpHeaders httpHeaders,
                                       @PathParam("company") final String company, @PathParam("establishment") final String establishment,
                                       @PathParam("prechro") final String prechro, @PathParam("chrono") final int chrono,
                                       @PathParam("ni1") final int ni1, @PathParam("ni2") final int ni2, @PathParam("ni3") final int ni3) {

        Response response = null;

        try {
            VIFContext context = createVIFContext(contextBuilder.buildAndCheckContext(httpHeaders));
            try {
                MOKey moKey = new MOKey(new EstablishmentKey(company, establishment), new Chrono(prechro, chrono));
                Kotad kotad = moSBS.getDetailedMoItemByPK(context, moKey, ni1, ni2, ni3, ni3);
                KotadDTO dto = Converter.convertDetailedMOItemToDTO(kotad);
                response = Response.status(Status.OK).entity(dto).build();

            } catch (ServerBusinessException e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(e);
                }
                if (Status.NOT_FOUND.getReasonPhrase().equals(
                        ((VarParamTranslation) e.getListeParams().get(0)).getValue())) {
                    response = ResponseErrorHelper.createError(Status.NOT_FOUND, e);
                }
                if (Status.INTERNAL_SERVER_ERROR.getReasonPhrase().equals(
                        ((VarParamTranslation) e.getListeParams().get(0)).getValue())) {
                    response = ResponseErrorHelper.createError(Status.INTERNAL_SERVER_ERROR, e);
                }
            } finally {
                close(context);
            }
        } catch (BusinessException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(e);
            }
            response = ResponseErrorHelper.createError(Status.FORBIDDEN, e);
        }

        return response;
    }

    /**
     *
     * Get Detailed MO Items (KOTAD) related to an MO Item (KOTA).
     *
     * @param httpHeaders http headers
     * @param company company
     * @param establishment establishment
     * @param prechro the prechro
     * @param chrono chrono of the subcontracted mo
     * @param ni1 ni for po
     * @param ni2 ni for item
     * @return list of input and output.
     */
    @GET
    @Path("{company}/{establishment}/{prechro}/{chrono}/{ni1}/{ni2}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response readDetailedMoItems(@Context final UriInfo ui, @Context final HttpHeaders httpHeaders,
                                        @PathParam("company") final String company, @PathParam("establishment") final String establishment,
                                        @PathParam("prechro") final String prechro, @PathParam("chrono") final int chrono,
                                        @PathParam("ni1") final int ni1, @PathParam("ni2") final int ni2) {

        Response response = null;

        try {
            VIFContext context = createVIFContext(contextBuilder.buildAndCheckContext(httpHeaders));
            try {
                MOKey moKey = new MOKey(new EstablishmentKey(company, establishment), new Chrono(prechro, chrono));
                List<Kotad> kotads = moSBS.listDetailedMoItemsByMoItem(context, moKey, ni1, ni2);
                List<KotadDTO> dtos = new ArrayList<KotadDTO>();
                for (Kotad kotad : kotads) {
                    dtos.add(Converter.convertDetailedMOItemToDTO(kotad));
                }
                response = Response.status(Status.OK).entity(dtos).build();
            } catch (ServerBusinessException e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(e);
                }
                response = ResponseErrorHelper.createError(Status.INTERNAL_SERVER_ERROR, e);
            } finally {
                close(context);
            }
        } catch (BusinessException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(e);
            }
            response = ResponseErrorHelper.createError(Status.FORBIDDEN, e);
        }

        return response;
    }

    /**
     *
     * Get Customer.
     *
     * @param httpHeaders http headers
     * @param company company
     * @param establishment establishment
     * @param prechro the prechro
     * @param chrono chrono of the subcontracted mo
     * @return list of input and output.
     */
    @GET
    @Path("{company}/{establishment}/{prechro}/{chrono}")
    @Produces({ MediaType.APPLICATION_JSON })
    public Response readMoItems(@Context final UriInfo ui, @Context final HttpHeaders httpHeaders,
                                @PathParam("company") final String company, @PathParam("establishment") final String establishment,
                                @PathParam("prechro") final String prechro, @PathParam("chrono") final int chrono) {

        Response response = null;

        try {
            VIFContext context = createVIFContext(contextBuilder.buildAndCheckContext(httpHeaders));
            try {
                MOKey moKey = new MOKey(new EstablishmentKey(company, establishment), new Chrono(prechro, chrono));
                List<Kota> kotas = moSBS.listMoItemsByMO(context, moKey);
                List<KotaDTO> dtos = new ArrayList<KotaDTO>();
                for (Kota kota : kotas) {
                    dtos.add(Converter.convertMOItemToDTO(kota));
                }
                response = Response.status(Status.OK).entity(dtos).build();

            } catch (ServerBusinessException e) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(e);
                }
                response = ResponseErrorHelper.createError(Status.INTERNAL_SERVER_ERROR, e);
            } finally {
                close(context);
            }
        } catch (BusinessException e) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug(e);
            }
            response = ResponseErrorHelper.createError(Status.FORBIDDEN, e);
        }

        return response;
    }

    /**
     * Sets the contextBuilder.
     *
     * @param contextBuilder contextBuilder.
     */
    public void setContextBuilder(final ContextBuilderRS contextBuilder) {
        this.contextBuilder = contextBuilder;
    }

    /**
     * Sets the converter.
     *
     * @param converter converter.
     */
    public void setConverter(final MOItemConverter converter) {
        this.converter = converter;
    }

    /**
     * Sets the moItemSBS.
     *
     * @param moItemSBS moItemSBS.
     */
    public void setMoItemSBS(final MOItemSBS moItemSBS) {
        this.moItemSBS = moItemSBS;
    }

    /**
     * Sets the moSBS.
     *
     * @param moSBS moSBS.
     */
    public void setMoSBS(final MOSBS moSBS) {
        this.moSBS = moSBS;
    }

    /**
     * Sets the responseDTO.
     *
     * @param responseDTO responseDTO.
     */
    public void setResponseDTO(final ResponseDTO responseDTO) {
        this.responseDTO = responseDTO;
    }

    /**
     * Sets the soamoitemPPO.
     *
     * @param soamoitemPPO soamoitemPPO.
     */
    public void setSoamoitemPPO(final SoamoitemPPO soamoitemPPO) {
        this.soamoitemPPO = soamoitemPPO;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoitemPPO.java,v $
 * Created on 24 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.Parameter;
import com.progress.open4gl.ProResultSet;
import com.progress.open4gl.ProResultSetMetaDataImpl;
import com.progress.open4gl.ProSQLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.ProInputTempTable;
import fr.vif.jtech.business.progress.TempTableHolder;


/**
 * Generated class from soamoitempxy.p.
 * 
 * @author vl
 */
@Service
public class SoamoitemPPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger             LOGGER                            = Logger.getLogger(SoamoitemPPO.class);

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOItemCrivLan.
     */
    private static ProResultSetMetaDataImpl metadatattMOItemCrivLan           = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOItemCrivLot.
     */
    private static ProResultSetMetaDataImpl metadatattMOItemCrivLot           = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOItemInsert.
     */
    private static ProResultSetMetaDataImpl metadatattMOItemInsert            = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOItemInsertResponse.
     */
    private static ProResultSetMetaDataImpl metadatattMOItemInsertResponse    = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOItemReadResponse.
     */
    private static ProResultSetMetaDataImpl metadatattMOItemReadResponse      = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOItemReadXmvtmResponse.
     */
    private static ProResultSetMetaDataImpl metadatattMOItemReadXmvtmResponse = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOItemSelection.
     */
    private static ProResultSetMetaDataImpl metadatattMOItemSelection         = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOItemStatusResponse.
     */
    private static ProResultSetMetaDataImpl metadatattMOItemStatusResponse    = null;

    /**
     * Default Constructor.
     */
    public SoamoitemPPO() {
        super();
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOItemCrivLan List of SoamoitemPPOTtmoitemcrivlan
     * @return ProInputTempTable Convert a record set to a list of SoamoitemPPOTtmoitemcrivlan
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOItemCrivLan(
            final List<SoamoitemPPOTtmoitemcrivlan> ttMOItemCrivLan) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoitemPPOTtmoitemcrivlan");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoitemPPOTtmoitemcrivlan> iter = ttMOItemCrivLan.iterator();
        while (iter.hasNext()) {
            SoamoitemPPOTtmoitemcrivlan row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCcri());
            list.add(row.getVal());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOItemCrivLot List of SoamoitemPPOTtmoitemcrivlot
     * @return ProInputTempTable Convert a record set to a list of SoamoitemPPOTtmoitemcrivlot
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOItemCrivLot(
            final List<SoamoitemPPOTtmoitemcrivlot> ttMOItemCrivLot) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoitemPPOTtmoitemcrivlot");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoitemPPOTtmoitemcrivlot> iter = ttMOItemCrivLot.iterator();
        while (iter.hasNext()) {
            SoamoitemPPOTtmoitemcrivlot row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCcri());
            list.add(row.getVal());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOItemInsert List of SoamoitemPPOTtmoiteminsert
     * @return ProInputTempTable Convert a record set to a list of SoamoitemPPOTtmoiteminsert
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOItemInsert(
            final List<SoamoitemPPOTtmoiteminsert> ttMOItemInsert) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoitemPPOTtmoiteminsert");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoitemPPOTtmoiteminsert> iter = ttMOItemInsert.iterator();
        while (iter.hasNext()) {
            SoamoitemPPOTtmoiteminsert row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            list.add(row.getCact());
            list.add(row.getCartprev());
            list.add(row.getTypacta());
            list.add(row.getCartr());
            list.add(new BigDecimal(row.getQte1()).setScale(5, RoundingMode.HALF_EVEN));
            list.add(row.getCu1());
            list.add(new BigDecimal(row.getQte2()).setScale(5, RoundingMode.HALF_EVEN));
            list.add(row.getCu2());
            list.add(new BigDecimal(row.getQte3()).setScale(5, RoundingMode.HALF_EVEN));
            list.add(row.getCu3());
            list.add(row.getCdepot());
            list.add(row.getCemp());
            list.add(row.getLot());
            list.add(dateToGreg(row.getDatmvt()));
            list.add(row.getHeurmvt());
            list.add(row.getCnatstk());
            list.add(row.getCuser());
            list.add(row.getCclelien());
            list.add(row.getCmotmvk());
            list.add(row.getNsc());
            list.add(row.getSscc());
            list.add(row.getCsubdiv());
            list.add(row.getNotat());
            list.add(row.getCcond());
            list.add(new BigDecimal(row.getTare()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCunitar());
            list.add(row.getTfermensc());
            list.add(row.getNsp());
            list.add(row.getCposte());
            list.add(row.getNscp());
            list.add(row.getSsccp());
            list.add(row.getCsubdivp());
            list.add(row.getNotatp());
            list.add(row.getCcondp());
            list.add(new BigDecimal(row.getTarep()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCunitarp());
            list.add(row.getTfermenscp());
            list.add(dateToGreg(row.getDatprod()));
            list.add(row.getCofuse());
            list.add(row.getNi1());
            list.add(row.getNi2());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOItemSelection List of SoamoitemPPOTtmoitemselection
     * @return ProInputTempTable Convert a record set to a list of SoamoitemPPOTtmoitemselection
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOItemSelection(
            final List<SoamoitemPPOTtmoitemselection> ttMOItemSelection) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoitemPPOTtmoitemselection");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoitemPPOTtmoitemselection> iter = ttMOItemSelection.iterator();
        while (iter.hasNext()) {
            SoamoitemPPOTtmoitemselection row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            list.add(dateToGreg(row.getDatprod()));
            list.add(row.getCofuse());
            list.add(row.getNi1());
            list.add(row.getCact());
            list.add(row.getTypacta());
            list.add(row.getNi2());
            list.add(row.getCartprev());
            list.add(row.getNi3());
            list.add(row.getCuser());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttMOItemCrivLan.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOItemCrivLan.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOItemCrivLan() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOItemCrivLan");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOItemCrivLan == null) {
            metadatattMOItemCrivLan = new ProResultSetMetaDataImpl(2);
            metadatattMOItemCrivLan.setFieldMetaData(1, "ccri", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemCrivLan.setFieldMetaData(2, "val", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOItemCrivLan;
    }

    /**
     * Generate the MetaData of tempTable ttMOItemCrivLot.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOItemCrivLot.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOItemCrivLot() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOItemCrivLot");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOItemCrivLot == null) {
            metadatattMOItemCrivLot = new ProResultSetMetaDataImpl(2);
            metadatattMOItemCrivLot.setFieldMetaData(1, "ccri", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemCrivLot.setFieldMetaData(2, "val", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOItemCrivLot;
    }

    /**
     * Generate the MetaData of tempTable ttMOItemInsert.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOItemInsert.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOItemInsert() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOItemInsert");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOItemInsert == null) {
            metadatattMOItemInsert = new ProResultSetMetaDataImpl(45);
            metadatattMOItemInsert.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMOItemInsert.setFieldMetaData(5, "cact", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(6, "cartprev", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(7, "typacta", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(8, "cartr", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(9, "qte1", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemInsert.setFieldMetaData(10, "cu1", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(11, "qte2", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemInsert.setFieldMetaData(12, "cu2", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(13, "qte3", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemInsert.setFieldMetaData(14, "cu3", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(15, "cdepot", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(16, "cemp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(17, "lot", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(18, "datmvt", 0, Parameter.PRO_DATE);
            metadatattMOItemInsert.setFieldMetaData(19, "heurmvt", 0, Parameter.PRO_INTEGER);
            metadatattMOItemInsert.setFieldMetaData(20, "cnatstk", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(21, "cuser", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(22, "cclelien", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(23, "cmotmvk", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(24, "nsc", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(25, "sscc", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(26, "csubdiv", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(27, "notat", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(28, "ccond", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(29, "tare", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemInsert.setFieldMetaData(30, "cunitar", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(31, "tfermensc", 0, Parameter.PRO_LOGICAL);
            metadatattMOItemInsert.setFieldMetaData(32, "nsp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(33, "cposte", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(34, "nscp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(35, "ssccp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(36, "csubdivp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(37, "notatp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(38, "ccondp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(39, "tarep", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemInsert.setFieldMetaData(40, "cunitarp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(41, "tfermenscp", 0, Parameter.PRO_LOGICAL);
            metadatattMOItemInsert.setFieldMetaData(42, "datprod", 0, Parameter.PRO_DATE);
            metadatattMOItemInsert.setFieldMetaData(43, "cofuse", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsert.setFieldMetaData(44, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattMOItemInsert.setFieldMetaData(45, "ni2", 0, Parameter.PRO_INTEGER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOItemInsert;
    }

    /**
     * Generate the MetaData of tempTable ttMOItemInsertResponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOItemInsertResponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOItemInsertResponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOItemInsertResponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOItemInsertResponse == null) {
            metadatattMOItemInsertResponse = new ProResultSetMetaDataImpl(8);
            metadatattMOItemInsertResponse.setFieldMetaData(1, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsertResponse.setFieldMetaData(2, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMOItemInsertResponse.setFieldMetaData(3, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattMOItemInsertResponse.setFieldMetaData(4, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattMOItemInsertResponse.setFieldMetaData(5, "ni3", 0, Parameter.PRO_INTEGER);
            metadatattMOItemInsertResponse.setFieldMetaData(6, "nlig", 0, Parameter.PRO_INTEGER);
            metadatattMOItemInsertResponse.setFieldMetaData(7, "lot", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemInsertResponse.setFieldMetaData(8, "nsc", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOItemInsertResponse;
    }

    /**
     * Generate the MetaData of tempTable ttMOItemReadResponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOItemReadResponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOItemReadResponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOItemReadResponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOItemReadResponse == null) {
            metadatattMOItemReadResponse = new ProResultSetMetaDataImpl(35);
            metadatattMOItemReadResponse.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(5, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(6, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(7, "ni3", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(8, "nlig", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(9, "typacta", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(10, "cart", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(11, "lart", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(12, "lot", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(13, "cdepot", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(14, "cemp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(15, "qte1", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemReadResponse.setFieldMetaData(16, "cu1", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(17, "qte2", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemReadResponse.setFieldMetaData(18, "cu2", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(19, "qte3", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemReadResponse.setFieldMetaData(20, "cu3", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(21, "tare", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemReadResponse.setFieldMetaData(22, "datmvt", 0, Parameter.PRO_DATE);
            metadatattMOItemReadResponse.setFieldMetaData(23, "heurmvt", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(24, "datgen", 0, Parameter.PRO_DATE);
            metadatattMOItemReadResponse.setFieldMetaData(25, "heurgen", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(26, "cuser", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(27, "cnatstk", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(28, "cmotmvk", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(29, "lmotmvk", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(30, "tstock", 0, Parameter.PRO_LOGICAL);
            metadatattMOItemReadResponse.setFieldMetaData(31, "typor", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadResponse.setFieldMetaData(32, "ni1or", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(33, "ni2or", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(34, "ni3or", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadResponse.setFieldMetaData(35, "ni4or", 0, Parameter.PRO_INTEGER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOItemReadResponse;
    }

    /**
     * Generate the MetaData of tempTable ttMOItemReadXmvtmResponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOItemReadXmvtmResponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOItemReadXmvtmResponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOItemReadXmvtmResponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOItemReadXmvtmResponse == null) {
            metadatattMOItemReadXmvtmResponse = new ProResultSetMetaDataImpl(44);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(5, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(6, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(7, "ni3", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(8, "ni4", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(9, "nlig", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(10, "typgest", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(11, "typmvt", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(12, "cnatstk", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(13, "cmotmvk", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(14, "lmotmvk", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(15, "typacta", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(16, "cart", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(17, "lart", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(18, "lot", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(19, "sens", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(20, "datmvt", 0, Parameter.PRO_DATE);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(21, "heurmvt", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(22, "datgen", 0, Parameter.PRO_DATE);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(23, "heurgen", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(24, "qug", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(25, "cug", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(26, "qus", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(27, "cus", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(28, "qut", 0, Parameter.PRO_DECIMAL);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(29, "cut", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(30, "cuser", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(31, "cposte", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(32, "typres", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(33, "rstock", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(34, "datsto", 0, Parameter.PRO_DATE);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(35, "heursto", 0, Parameter.PRO_INTEGER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(36, "cdepot", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(37, "cemp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(38, "nsp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(39, "nsc", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(40, "nscp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(41, "csubdiv", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(42, "csubdivp", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(43, "notat", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemReadXmvtmResponse.setFieldMetaData(44, "notatp", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOItemReadXmvtmResponse;
    }

    /**
     * Generate the MetaData of tempTable ttMOItemSelection.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOItemSelection.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOItemSelection() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOItemSelection");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOItemSelection == null) {
            metadatattMOItemSelection = new ProResultSetMetaDataImpl(13);
            metadatattMOItemSelection.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemSelection.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemSelection.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemSelection.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMOItemSelection.setFieldMetaData(5, "datprod", 0, Parameter.PRO_DATE);
            metadatattMOItemSelection.setFieldMetaData(6, "cofuse", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemSelection.setFieldMetaData(7, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattMOItemSelection.setFieldMetaData(8, "cact", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemSelection.setFieldMetaData(9, "typacta", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemSelection.setFieldMetaData(10, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattMOItemSelection.setFieldMetaData(11, "cartprev", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemSelection.setFieldMetaData(12, "ni3", 0, Parameter.PRO_INTEGER);
            metadatattMOItemSelection.setFieldMetaData(13, "cuser", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOItemSelection;
    }

    /**
     * Generate the MetaData of tempTable ttMOItemStatusResponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOItemStatusResponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOItemStatusResponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOItemStatusResponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOItemStatusResponse == null) {
            metadatattMOItemStatusResponse = new ProResultSetMetaDataImpl(2);
            metadatattMOItemStatusResponse.setFieldMetaData(1, "cstatus", 0, Parameter.PRO_CHARACTER);
            metadatattMOItemStatusResponse.setFieldMetaData(2, "msg", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOItemStatusResponse;
    }

    /**
     * Generated Class deleteMOItem.
     * 
     * @param ctx VIFContext.
     * @param ttMOItemSelection TABLE INPUT
     * @param ttMOItemCrivLan TABLE INPUT
     * @param ttMOItemStatusResponseHolder TABLE OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void deleteMOItem(final VIFContext ctx, final List<SoamoitemPPOTtmoitemselection> ttMOItemSelection,
            final List<SoamoitemPPOTtmoitemcrivlan> ttMOItemCrivLan,
            final TempTableHolder<SoamoitemPPOTtmoitemstatusresponse> ttMOItemStatusResponseHolder)
                    throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttMOItemStatusResponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : deleteMOItem values :" + "ttMOItemStatusResponseHolder = "
                        + ttMOItemStatusResponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method deleteMOItem " + " ttMOItemSelection = " + ttMOItemSelection
                    + " ttMOItemCrivLan = " + ttMOItemCrivLan + " ttMOItemStatusResponseHolder = "
                    + ttMOItemStatusResponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(3);

        try {
            // ------------------------------------------
            // Make the temp-table ttMOItemSelection
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOItemSelection");
            }
            params.addTable(0, convertTempTablettMOItemSelection(ttMOItemSelection), ParamArrayMode.INPUT,
                    getMetaDatattMOItemSelection());
            // ------------------------------------------
            // Make the temp-table ttMOItemCrivLan
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOItemCrivLan");
            }
            params.addTable(1, convertTempTablettMOItemCrivLan(ttMOItemCrivLan), ParamArrayMode.INPUT,
                    getMetaDatattMOItemCrivLan());
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattMOItemStatusResponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "deleteMOItem", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttMOItemStatusResponse

            ttMOItemStatusResponseHolder.setListValue(convertRecordttMOItemStatusResponse(params, 2));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "soamoitempxy.p";
    }

    /**
     * Generated Class insertMOItem.
     * 
     * @param ctx VIFContext.
     * @param ttMOItemInsert TABLE INPUT
     * @param ttMOItemCrivLan TABLE INPUT
     * @param ttMOItemCrivLot TABLE INPUT
     * @param ttMOItemStatusResponseHolder TABLE OUTPUT
     * @param ttMOItemInsertResponseHolder TABLE OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void insertMOItem(final VIFContext ctx, final List<SoamoitemPPOTtmoiteminsert> ttMOItemInsert,
            final List<SoamoitemPPOTtmoitemcrivlan> ttMOItemCrivLan,
            final List<SoamoitemPPOTtmoitemcrivlot> ttMOItemCrivLot,
            final TempTableHolder<SoamoitemPPOTtmoitemstatusresponse> ttMOItemStatusResponseHolder,
            final TempTableHolder<SoamoitemPPOTtmoiteminsertresponse> ttMOItemInsertResponseHolder)
                    throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttMOItemStatusResponseHolder == null || ttMOItemInsertResponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : insertMOItem values :" + "ttMOItemStatusResponseHolder = "
                        + ttMOItemStatusResponseHolder + "ttMOItemInsertResponseHolder = "
                        + ttMOItemInsertResponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method insertMOItem " + " ttMOItemInsert = " + ttMOItemInsert + " ttMOItemCrivLan = "
                    + ttMOItemCrivLan + " ttMOItemCrivLot = " + ttMOItemCrivLot + " ttMOItemStatusResponseHolder = "
                    + ttMOItemStatusResponseHolder + " ttMOItemInsertResponseHolder = " + ttMOItemInsertResponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);

        try {
            // ------------------------------------------
            // Make the temp-table ttMOItemInsert
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOItemInsert");
            }
            params.addTable(0, convertTempTablettMOItemInsert(ttMOItemInsert), ParamArrayMode.INPUT,
                    getMetaDatattMOItemInsert());
            // ------------------------------------------
            // Make the temp-table ttMOItemCrivLan
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOItemCrivLan");
            }
            params.addTable(1, convertTempTablettMOItemCrivLan(ttMOItemCrivLan), ParamArrayMode.INPUT,
                    getMetaDatattMOItemCrivLan());
            // ------------------------------------------
            // Make the temp-table ttMOItemCrivLot
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOItemCrivLot");
            }
            params.addTable(2, convertTempTablettMOItemCrivLot(ttMOItemCrivLot), ParamArrayMode.INPUT,
                    getMetaDatattMOItemCrivLot());
            params.addTable(3, null, ParamArrayMode.OUTPUT, getMetaDatattMOItemStatusResponse());
            params.addTable(4, null, ParamArrayMode.OUTPUT, getMetaDatattMOItemInsertResponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "insertMOItem", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttMOItemStatusResponse

            ttMOItemStatusResponseHolder.setListValue(convertRecordttMOItemStatusResponse(params, 3));
            // Make the temp-table ttMOItemInsertResponse

            ttMOItemInsertResponseHolder.setListValue(convertRecordttMOItemInsertResponse(params, 4));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class readMOItem.
     * 
     * @param ctx VIFContext.
     * @param ttMOItemSelection TABLE INPUT
     * @param ttMOItemCrivLan TABLE INPUT
     * @param ttMOItemStatusResponseHolder TABLE OUTPUT
     * @param ttMOItemReadResponseHolder TABLE OUTPUT
     * @param ttMOItemReadXmvtmResponseHolder TABLE OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void readMOItem(final VIFContext ctx, final List<SoamoitemPPOTtmoitemselection> ttMOItemSelection,
            final List<SoamoitemPPOTtmoitemcrivlan> ttMOItemCrivLan,
            final TempTableHolder<SoamoitemPPOTtmoitemstatusresponse> ttMOItemStatusResponseHolder,
            final TempTableHolder<SoamoitemPPOTtmoitemreadresponse> ttMOItemReadResponseHolder,
            final TempTableHolder<SoamoitemPPOTtmoitemreadxmvtmresponse> ttMOItemReadXmvtmResponseHolder)
                    throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttMOItemStatusResponseHolder == null || ttMOItemReadResponseHolder == null
                || ttMOItemReadXmvtmResponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : readMOItem values :" + "ttMOItemStatusResponseHolder = "
                        + ttMOItemStatusResponseHolder + "ttMOItemReadResponseHolder = " + ttMOItemReadResponseHolder
                        + "ttMOItemReadXmvtmResponseHolder = " + ttMOItemReadXmvtmResponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method readMOItem " + " ttMOItemSelection = " + ttMOItemSelection + " ttMOItemCrivLan = "
                    + ttMOItemCrivLan + " ttMOItemStatusResponseHolder = " + ttMOItemStatusResponseHolder
                    + " ttMOItemReadResponseHolder = " + ttMOItemReadResponseHolder
                    + " ttMOItemReadXmvtmResponseHolder = " + ttMOItemReadXmvtmResponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);

        try {
            // ------------------------------------------
            // Make the temp-table ttMOItemSelection
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOItemSelection");
            }
            params.addTable(0, convertTempTablettMOItemSelection(ttMOItemSelection), ParamArrayMode.INPUT,
                    getMetaDatattMOItemSelection());
            // ------------------------------------------
            // Make the temp-table ttMOItemCrivLan
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOItemCrivLan");
            }
            params.addTable(1, convertTempTablettMOItemCrivLan(ttMOItemCrivLan), ParamArrayMode.INPUT,
                    getMetaDatattMOItemCrivLan());
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattMOItemStatusResponse());
            params.addTable(3, null, ParamArrayMode.OUTPUT, getMetaDatattMOItemReadResponse());
            params.addTable(4, null, ParamArrayMode.OUTPUT, getMetaDatattMOItemReadXmvtmResponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "readMOItem", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttMOItemStatusResponse

            ttMOItemStatusResponseHolder.setListValue(convertRecordttMOItemStatusResponse(params, 2));
            // Make the temp-table ttMOItemReadResponse

            ttMOItemReadResponseHolder.setListValue(convertRecordttMOItemReadResponse(params, 3));
            // Make the temp-table ttMOItemReadXmvtmResponse

            ttMOItemReadXmvtmResponseHolder.setListValue(convertRecordttMOItemReadXmvtmResponse(params, 4));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoitemPPOTtmoiteminsertresponse&gt; Convert a record set to a list of
     *         SoamoitemPPOTtmoiteminsertresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoitemPPOTtmoiteminsertresponse> convertRecordttMOItemInsertResponse(final ParamArray params,
            final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoitemPPOTtmoiteminsertresponse");
        }

        ProResultSet rs = null;
        List<SoamoitemPPOTtmoiteminsertresponse> list = new ArrayList<SoamoitemPPOTtmoiteminsertresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoitemPPOTtmoiteminsertresponse tempTable = new SoamoitemPPOTtmoiteminsertresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                tempTable.setNi1(rs.getInt("ni1"));
                tempTable.setNi2(rs.getInt("ni2"));
                tempTable.setNi3(rs.getInt("ni3"));
                tempTable.setNlig(rs.getInt("nlig"));
                tempTable.setLot(rs.getString("lot"));
                tempTable.setNsc(rs.getString("nsc"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoitemPPOTtmoitemreadresponse&gt; Convert a record set to a list of
     *         SoamoitemPPOTtmoitemreadresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoitemPPOTtmoitemreadresponse> convertRecordttMOItemReadResponse(final ParamArray params,
            final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoitemPPOTtmoitemreadresponse");
        }

        ProResultSet rs = null;
        List<SoamoitemPPOTtmoitemreadresponse> list = new ArrayList<SoamoitemPPOTtmoitemreadresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoitemPPOTtmoitemreadresponse tempTable = new SoamoitemPPOTtmoitemreadresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                tempTable.setNi1(rs.getInt("ni1"));
                tempTable.setNi2(rs.getInt("ni2"));
                tempTable.setNi3(rs.getInt("ni3"));
                tempTable.setNlig(rs.getInt("nlig"));
                tempTable.setTypacta(rs.getString("typacta"));
                tempTable.setCart(rs.getString("cart"));
                tempTable.setLart(rs.getString("lart"));
                tempTable.setLot(rs.getString("lot"));
                tempTable.setCdepot(rs.getString("cdepot"));
                tempTable.setCemp(rs.getString("cemp"));
                tempTable.setQte1(rs.getDouble("qte1"));
                tempTable.setCu1(rs.getString("cu1"));
                tempTable.setQte2(rs.getDouble("qte2"));
                tempTable.setCu2(rs.getString("cu2"));
                tempTable.setQte3(rs.getDouble("qte3"));
                tempTable.setCu3(rs.getString("cu3"));
                tempTable.setTare(rs.getDouble("tare"));
                GregorianCalendar gdatmvt = rs.getGregorianCalendar("datmvt");
                tempTable.setDatmvt(gdatmvt != null ? gdatmvt.getTime() : null);
                tempTable.setHeurmvt(rs.getInt("heurmvt"));
                GregorianCalendar gdatgen = rs.getGregorianCalendar("datgen");
                tempTable.setDatgen(gdatgen != null ? gdatgen.getTime() : null);
                tempTable.setHeurgen(rs.getInt("heurgen"));
                tempTable.setCuser(rs.getString("cuser"));
                tempTable.setCnatstk(rs.getString("cnatstk"));
                tempTable.setCmotmvk(rs.getString("cmotmvk"));
                tempTable.setLmotmvk(rs.getString("lmotmvk"));
                tempTable.setTstock(rs.getBoolean("tstock"));
                tempTable.setTypor(rs.getString("typor"));
                tempTable.setNi1or(rs.getInt("ni1or"));
                tempTable.setNi2or(rs.getInt("ni2or"));
                tempTable.setNi3or(rs.getInt("ni3or"));
                tempTable.setNi4or(rs.getInt("ni4or"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoitemPPOTtmoitemreadxmvtmresponse&gt; Convert a record set to a list of
     *         SoamoitemPPOTtmoitemreadxmvtmresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoitemPPOTtmoitemreadxmvtmresponse> convertRecordttMOItemReadXmvtmResponse(final ParamArray params,
            final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoitemPPOTtmoitemreadxmvtmresponse");
        }

        ProResultSet rs = null;
        List<SoamoitemPPOTtmoitemreadxmvtmresponse> list = new ArrayList<SoamoitemPPOTtmoitemreadxmvtmresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoitemPPOTtmoitemreadxmvtmresponse tempTable = new SoamoitemPPOTtmoitemreadxmvtmresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                tempTable.setNi1(rs.getInt("ni1"));
                tempTable.setNi2(rs.getInt("ni2"));
                tempTable.setNi3(rs.getInt("ni3"));
                tempTable.setNi4(rs.getInt("ni4"));
                tempTable.setNlig(rs.getInt("nlig"));
                tempTable.setTypgest(rs.getString("typgest"));
                tempTable.setTypmvt(rs.getString("typmvt"));
                tempTable.setCnatstk(rs.getString("cnatstk"));
                tempTable.setCmotmvk(rs.getString("cmotmvk"));
                tempTable.setLmotmvk(rs.getString("lmotmvk"));
                tempTable.setTypacta(rs.getString("typacta"));
                tempTable.setCart(rs.getString("cart"));
                tempTable.setLart(rs.getString("lart"));
                tempTable.setLot(rs.getString("lot"));
                tempTable.setSens(rs.getString("sens"));
                GregorianCalendar gdatmvt = rs.getGregorianCalendar("datmvt");
                tempTable.setDatmvt(gdatmvt != null ? gdatmvt.getTime() : null);
                tempTable.setHeurmvt(rs.getInt("heurmvt"));
                GregorianCalendar gdatgen = rs.getGregorianCalendar("datgen");
                tempTable.setDatgen(gdatgen != null ? gdatgen.getTime() : null);
                tempTable.setHeurgen(rs.getInt("heurgen"));
                tempTable.setQug(rs.getDouble("qug"));
                tempTable.setCug(rs.getString("cug"));
                tempTable.setQus(rs.getDouble("qus"));
                tempTable.setCus(rs.getString("cus"));
                tempTable.setQut(rs.getDouble("qut"));
                tempTable.setCut(rs.getString("cut"));
                tempTable.setCuser(rs.getString("cuser"));
                tempTable.setCposte(rs.getString("cposte"));
                tempTable.setTypres(rs.getString("typres"));
                tempTable.setRstock(rs.getString("rstock"));
                GregorianCalendar gdatsto = rs.getGregorianCalendar("datsto");
                tempTable.setDatsto(gdatsto != null ? gdatsto.getTime() : null);
                tempTable.setHeursto(rs.getInt("heursto"));
                tempTable.setCdepot(rs.getString("cdepot"));
                tempTable.setCemp(rs.getString("cemp"));
                tempTable.setNsp(rs.getString("nsp"));
                tempTable.setNsc(rs.getString("nsc"));
                tempTable.setNscp(rs.getString("nscp"));
                tempTable.setCsubdiv(rs.getString("csubdiv"));
                tempTable.setCsubdivp(rs.getString("csubdivp"));
                tempTable.setNotat(rs.getString("notat"));
                tempTable.setNotatp(rs.getString("notatp"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoitemPPOTtmoitemstatusresponse&gt; Convert a record set to a list of
     *         SoamoitemPPOTtmoitemstatusresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoitemPPOTtmoitemstatusresponse> convertRecordttMOItemStatusResponse(final ParamArray params,
            final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoitemPPOTtmoitemstatusresponse");
        }

        ProResultSet rs = null;
        List<SoamoitemPPOTtmoitemstatusresponse> list = new ArrayList<SoamoitemPPOTtmoitemstatusresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoitemPPOTtmoitemstatusresponse tempTable = new SoamoitemPPOTtmoitemstatusresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setCstatus(rs.getString("cstatus"));
                tempTable.setMsg(rs.getString("msg"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

}

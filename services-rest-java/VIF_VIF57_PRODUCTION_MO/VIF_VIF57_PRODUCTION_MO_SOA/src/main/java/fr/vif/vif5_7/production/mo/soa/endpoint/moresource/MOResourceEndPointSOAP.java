/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: MOResourceEndPointSOAP.java,v $
 * Created on 20 mars 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moresource;


import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;
import javax.xml.ws.WebServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.soa.endpoint.AbstractSOAEndPoint;
import fr.vif.vif5_7.production.mo.DeleteMOResourceRequest;
import fr.vif.vif5_7.production.mo.DeleteMOResourceResponse;
import fr.vif.vif5_7.production.mo.IMOResourceSOA;
import fr.vif.vif5_7.production.mo.InsertMOResourceRequest;
import fr.vif.vif5_7.production.mo.InsertMOResourceResponse;
import fr.vif.vif5_7.production.mo.ReadMOResourceRequest;
import fr.vif.vif5_7.production.mo.ReadMOResourceResponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPO;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourcecrivlan;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourceinsert;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourceinsertresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourcereadresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourceselection;
import fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy.SoamoresPPOTtmoresourcestatusresponse;


/**
 * Web Service for resource declaration.
 * 
 * @author vl
 */

@WebService(serviceName = "MOResourceEndPointService", portName = "MOResourceEndPointPort", targetNamespace = "http://www.vif.fr/vif5_7/production/mo", wsdlLocation = "classpath:/production/mo/soa/schemas/MOResourceSOA.wsdl", endpointInterface = "fr.vif.vif5_7.production.mo.IMOResourceSOA")
public class MOResourceEndPointSOAP extends AbstractSOAEndPoint implements IMOResourceSOA {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(MOResourceEndPointSOAP.class);

    @Autowired
    private SoamoresPPO         soamoresPPO;

    private MOResourceConverter converter;

    /**
     * {@inheritDoc}
     */
    @Override
    public DeleteMOResourceResponse deleteMOResource(final DeleteMOResourceRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteMOResource(request=" + request + ")");
        }

        DeleteMOResourceResponse ret = new DeleteMOResourceResponse();

        VIFContext ctx;
        try {
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOResourceConverter.getInstance();

                // Set ttMoResourceSelection parameter
                List<SoamoresPPOTtmoresourceselection> ttMoResourceSelection = new ArrayList<SoamoresPPOTtmoresourceselection>();
                ttMoResourceSelection.add(converter.convertSelectionToTt(request.getDeleteMOResourceSelection()
                        .getMoResourceSelectionType()));

                // Set ttMoResourceCriv parameter
                List<SoamoresPPOTtmoresourcecrivlan> ttMoResourceCrivs = converter.convertMOCriteriaToTtCriv(request
                        .getDeleteMOResourceSelection().getMoResourceSelectionType().getMoCriteria());

                // Create response holder
                TempTableHolder<SoamoresPPOTtmoresourcestatusresponse> statusResponseHolder = new TempTableHolder<SoamoresPPOTtmoresourcestatusresponse>();

                // Call appServer
                getSoamoresPPO().deleteMOResource(ctx, ttMoResourceSelection, ttMoResourceCrivs, statusResponseHolder);

                // Manage Response
                ret.setStandardResponse(converter.convertStatusResponseToStandardResponse(statusResponseHolder));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                LOGGER.error("P - insertMoResource(e=" + e + ")");
                throw new WebServiceException(e);
            } catch (DAOException e) {
                LOGGER.error("P - insertMoResource(e=" + e + ")");
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteMOResource(request=" + request + ")=" + ret);
        }
        return ret;
    }

    /**
     * Gets the soamoresPPO.
     * 
     * @return the soamoresPPO.
     */
    public SoamoresPPO getSoamoresPPO() {
        return soamoresPPO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InsertMOResourceResponse insertMOResource(final InsertMOResourceRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertMOResource(request=" + request + ")");
        }

        InsertMOResourceResponse ret = new InsertMOResourceResponse();

        VIFContext ctx;
        try {
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOResourceConverter.getInstance();

                // Set ttMoResourceInsert parameter
                List<SoamoresPPOTtmoresourceinsert> ttMoResourceInserts = new ArrayList<SoamoresPPOTtmoresourceinsert>();
                ttMoResourceInserts.add(converter.convertInsertDataToTt(request.getInsertMOResourceData()));

                // Set ttMoResourceCriv parameter
                List<SoamoresPPOTtmoresourcecrivlan> ttMoResourceCrivs = converter.convertMOCriteriaToTtCriv(request
                        .getInsertMOResourceData().getMoCriteria());

                // Create response holder
                TempTableHolder<SoamoresPPOTtmoresourceinsertresponse> insertResponseHolder = new TempTableHolder<SoamoresPPOTtmoresourceinsertresponse>();
                TempTableHolder<SoamoresPPOTtmoresourcestatusresponse> statusResponseHolder = new TempTableHolder<SoamoresPPOTtmoresourcestatusresponse>();

                // Call appServer
                getSoamoresPPO().insertMOResource(ctx, ttMoResourceInserts, ttMoResourceCrivs, statusResponseHolder,
                        insertResponseHolder);

                // Manage Response
                ret.setStandardResponse(converter.convertStatusResponseToStandardResponse(statusResponseHolder));
                ret.setInsertMOResourceResponse(converter.convertInsertTtToResponse(insertResponseHolder));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                LOGGER.error("P - insertMoResource(e=" + e + ")");
                throw new WebServiceException(e);
            } catch (DAOException e) {
                LOGGER.error("P - insertMoResource(e=" + e + ")");
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertMOResource(request=" + request + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReadMOResourceResponse readMOResource(final ReadMOResourceRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - readMOResource(request=" + request + ")");
        }

        ReadMOResourceResponse ret = new ReadMOResourceResponse();

        VIFContext ctx;
        try {
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOResourceConverter.getInstance();

                // Set ttMoResourceSelection parameter
                List<SoamoresPPOTtmoresourceselection> ttMoResourceSelection = new ArrayList<SoamoresPPOTtmoresourceselection>();
                ttMoResourceSelection.add(converter.convertSelectionToTt(request.getReadMOResourceSelection()
                        .getMoResourceSelectionType()));

                // Set ttMoResourceCriv parameter
                List<SoamoresPPOTtmoresourcecrivlan> ttMoResourceCrivs = converter.convertMOCriteriaToTtCriv(request
                        .getReadMOResourceSelection().getMoResourceSelectionType().getMoCriteria());

                // Create response holder
                TempTableHolder<SoamoresPPOTtmoresourcestatusresponse> statusResponseHolder = new TempTableHolder<SoamoresPPOTtmoresourcestatusresponse>();
                TempTableHolder<SoamoresPPOTtmoresourcereadresponse> readResponseHolder = new TempTableHolder<SoamoresPPOTtmoresourcereadresponse>();

                // Call appServer
                getSoamoresPPO().readMOResource(ctx, ttMoResourceSelection, ttMoResourceCrivs, statusResponseHolder,
                        readResponseHolder);

                // Manage Response
                ret.setStandardResponse(converter.convertStatusResponseToStandardResponse(statusResponseHolder));
                ret.setReadMOResourceResponse(converter.convertReadTtToResponse(readResponseHolder));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                LOGGER.error("P - insertMoResource(e=" + e + ")");
                throw new WebServiceException(e);
            } catch (DAOException e) {
                LOGGER.error("P - insertMoResource(e=" + e + ")");
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - readMOResource(request=" + request + ")=" + ret);
        }
        return ret;
    }

    /**
     * Sets the soamoresPPO.
     * 
     * @param soamoresPPO soamoresPPO.
     */
    public void setSoamoresPPO(final SoamoresPPO soamoresPPO) {
        this.soamoresPPO = soamoresPPO;
    }

}

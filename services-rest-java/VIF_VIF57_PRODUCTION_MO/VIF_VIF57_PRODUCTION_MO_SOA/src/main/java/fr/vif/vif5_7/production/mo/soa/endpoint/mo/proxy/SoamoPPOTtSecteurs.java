/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoPPOTtSecteurs.java,v $
 * Created on 18 nov. 2014 by nle
 * Revision: $Revision: 1.3 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamopxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.3 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoPPOTtSecteurs extends AbstractTempTable implements Serializable {


    private String csecteur = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoPPOTtSecteurs() {
        super();
    }

    /**
     * Gets the csecteur.
     *
     * @category getter
     * @return the csecteur.
     */
    public final String getCsecteur() {
        return csecteur;
    }

    /**
     * Sets the csecteur.
     *
     * @category setter
     * @param csecteur csecteur.
     */
    public final void setCsecteur(final String csecteur) {
        this.csecteur = csecteur;
    }

}

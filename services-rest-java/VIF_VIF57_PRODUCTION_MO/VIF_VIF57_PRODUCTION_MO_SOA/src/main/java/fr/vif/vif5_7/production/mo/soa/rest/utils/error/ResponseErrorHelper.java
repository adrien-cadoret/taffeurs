/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on Jul 19, 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils.error;


import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.TranslatedException;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.util.i18n.AbstractTransConstant;
import fr.vif.jtech.common.util.i18n.ITransParam;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;


/**
 * Response Error Builder.
 * 
 * @author ac
 */
public final class ResponseErrorHelper {

    /**
     * Hide constructor.
     */
    private ResponseErrorHelper() {

    }

    /**
     * 
     * Create Error.
     * 
     * @param status Http status
     * @param e exception
     * @return Reponse sith status and built with a Business Error.
     */
    public static Response createError(final Status status, final TranslatedException e) {
        BusinessError error = new BusinessError(getMainTradValue(e.getMainTrad()), e.getLocalizedMessage());
        Response ret = buildResponse(status, error);
        return ret;
    }

    /**
     * 
     * Creator Error with translate message.
     * 
     * @param status Http status
     * @param context context (to get the locale)
     * @param mainTrad : The mainTrad (ComposedTranslation)
     * @param listeParams : The parameters (ITransParam)
     * @return built Response.
     */
    public static Response createError(final Status status, final VIFContext context,
            final AbstractTransConstant mainTrad, final ITransParam... listeParams) {
        String errMessage = I18nServerManager.translate(context.getIdCtx().getLocale(), true, Generic.T1401,
                listeParams);
        BusinessError error = new BusinessError(getMainTradValue(mainTrad), errMessage);
        Response ret = buildResponse(status, error);
        return ret;

    }

    /**
     * Build the response.
     * 
     * @param status status
     * @param error Error to build in response
     * @return built response.
     */
    private static Response buildResponse(final Status status, final BusinessError error) {
        Response ret = Response.status(status).entity(error).type(MediaType.APPLICATION_JSON_TYPE).build();
        return ret;
    }

    /**
     * 
     * Return the mainTrad's value.
     * 
     * @param mainTrad : The mainTrad (ComposedTranslation)
     * @return mainTrad.getValue() of null if mainTrad = null
     */
    private static String getMainTradValue(final AbstractTransConstant mainTrad) {
        return (mainTrad == null ? "" : mainTrad.getValue());
    }
}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoresPPOTtmoresourceinsert.java,v $
 * Created on 12 déc. 2014 by nle
 * Revision: $Revision: 1.4 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamorespxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.4 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoresPPOTtmoresourceinsert extends AbstractTempTable implements Serializable {


    private String cact = "";

    private String cetab = "";

    private int chrono = 0;

    private String cmotmvk = "";

    private String cofuse = "";

    private String coper = "";

    private String cres = "";

    private String cresmo = "";

    private String crub = "";

    private String csoc = "";

    private String cuser = "";

    private Date datdebr = null;

    private Date datfinr = null;

    private Date datprod = null;

    private double duree2 = 0;

    private int heurdebr = 0;

    private int heurfinr = 0;

    private String inputmode = "";

    private String ligne = "";

    private double nbr = 0;

    private int ni1 = 0;

    private int ni2 = 0;

    private int nlig = 0;

    private String prechro = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoresPPOTtmoresourceinsert() {
        super();
    }

    /**
     * Gets the cact.
     *
     * @category getter
     * @return the cact.
     */
    public final String getCact() {
        return cact;
    }

    /**
     * Sets the cact.
     *
     * @category setter
     * @param cact cact.
     */
    public final void setCact(final String cact) {
        this.cact = cact;
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the chrono.
     *
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Sets the chrono.
     *
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Gets the cmotmvk.
     *
     * @category getter
     * @return the cmotmvk.
     */
    public final String getCmotmvk() {
        return cmotmvk;
    }

    /**
     * Sets the cmotmvk.
     *
     * @category setter
     * @param cmotmvk cmotmvk.
     */
    public final void setCmotmvk(final String cmotmvk) {
        this.cmotmvk = cmotmvk;
    }

    /**
     * Gets the cofuse.
     *
     * @category getter
     * @return the cofuse.
     */
    public final String getCofuse() {
        return cofuse;
    }

    /**
     * Sets the cofuse.
     *
     * @category setter
     * @param cofuse cofuse.
     */
    public final void setCofuse(final String cofuse) {
        this.cofuse = cofuse;
    }

    /**
     * Gets the coper.
     *
     * @category getter
     * @return the coper.
     */
    public final String getCoper() {
        return coper;
    }

    /**
     * Sets the coper.
     *
     * @category setter
     * @param coper coper.
     */
    public final void setCoper(final String coper) {
        this.coper = coper;
    }

    /**
     * Gets the cres.
     *
     * @category getter
     * @return the cres.
     */
    public final String getCres() {
        return cres;
    }

    /**
     * Sets the cres.
     *
     * @category setter
     * @param cres cres.
     */
    public final void setCres(final String cres) {
        this.cres = cres;
    }

    /**
     * Gets the cresmo.
     *
     * @category getter
     * @return the cresmo.
     */
    public final String getCresmo() {
        return cresmo;
    }

    /**
     * Sets the cresmo.
     *
     * @category setter
     * @param cresmo cresmo.
     */
    public final void setCresmo(final String cresmo) {
        this.cresmo = cresmo;
    }

    /**
     * Gets the crub.
     *
     * @category getter
     * @return the crub.
     */
    public final String getCrub() {
        return crub;
    }

    /**
     * Sets the crub.
     *
     * @category setter
     * @param crub crub.
     */
    public final void setCrub(final String crub) {
        this.crub = crub;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the cuser.
     *
     * @category getter
     * @return the cuser.
     */
    public final String getCuser() {
        return cuser;
    }

    /**
     * Sets the cuser.
     *
     * @category setter
     * @param cuser cuser.
     */
    public final void setCuser(final String cuser) {
        this.cuser = cuser;
    }

    /**
     * Gets the datdebr.
     *
     * @category getter
     * @return the datdebr.
     */
    public final Date getDatdebr() {
        return datdebr;
    }

    /**
     * Sets the datdebr.
     *
     * @category setter
     * @param datdebr datdebr.
     */
    public final void setDatdebr(final Date datdebr) {
        this.datdebr = datdebr;
    }

    /**
     * Gets the datfinr.
     *
     * @category getter
     * @return the datfinr.
     */
    public final Date getDatfinr() {
        return datfinr;
    }

    /**
     * Sets the datfinr.
     *
     * @category setter
     * @param datfinr datfinr.
     */
    public final void setDatfinr(final Date datfinr) {
        this.datfinr = datfinr;
    }

    /**
     * Gets the datprod.
     *
     * @category getter
     * @return the datprod.
     */
    public final Date getDatprod() {
        return datprod;
    }

    /**
     * Sets the datprod.
     *
     * @category setter
     * @param datprod datprod.
     */
    public final void setDatprod(final Date datprod) {
        this.datprod = datprod;
    }

    /**
     * Gets the duree2.
     *
     * @category getter
     * @return the duree2.
     */
    public final double getDuree2() {
        return duree2;
    }

    /**
     * Sets the duree2.
     *
     * @category setter
     * @param duree2 duree2.
     */
    public final void setDuree2(final double duree2) {
        this.duree2 = duree2;
    }

    /**
     * Gets the heurdebr.
     *
     * @category getter
     * @return the heurdebr.
     */
    public final int getHeurdebr() {
        return heurdebr;
    }

    /**
     * Sets the heurdebr.
     *
     * @category setter
     * @param heurdebr heurdebr.
     */
    public final void setHeurdebr(final int heurdebr) {
        this.heurdebr = heurdebr;
    }

    /**
     * Gets the heurfinr.
     *
     * @category getter
     * @return the heurfinr.
     */
    public final int getHeurfinr() {
        return heurfinr;
    }

    /**
     * Sets the heurfinr.
     *
     * @category setter
     * @param heurfinr heurfinr.
     */
    public final void setHeurfinr(final int heurfinr) {
        this.heurfinr = heurfinr;
    }

    /**
     * Gets the inputmode.
     *
     * @category getter
     * @return the inputmode.
     */
    public final String getInputmode() {
        return inputmode;
    }

    /**
     * Sets the inputmode.
     *
     * @category setter
     * @param inputmode inputmode.
     */
    public final void setInputmode(final String inputmode) {
        this.inputmode = inputmode;
    }

    /**
     * Gets the ligne.
     *
     * @category getter
     * @return the ligne.
     */
    public final String getLigne() {
        return ligne;
    }

    /**
     * Sets the ligne.
     *
     * @category setter
     * @param ligne ligne.
     */
    public final void setLigne(final String ligne) {
        this.ligne = ligne;
    }

    /**
     * Gets the nbr.
     *
     * @category getter
     * @return the nbr.
     */
    public final double getNbr() {
        return nbr;
    }

    /**
     * Sets the nbr.
     *
     * @category setter
     * @param nbr nbr.
     */
    public final void setNbr(final double nbr) {
        this.nbr = nbr;
    }

    /**
     * Gets the ni1.
     *
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Sets the ni1.
     *
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Gets the ni2.
     *
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Sets the ni2.
     *
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Gets the nlig.
     *
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Sets the nlig.
     *
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Gets the prechro.
     *
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Sets the prechro.
     *
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

}

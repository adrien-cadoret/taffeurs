/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: MOItemConverter.java,v $
 * Created on 4 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem;


import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.TimeHelper;
import fr.vif.vif5_7.production.mo.ChronoType;
import fr.vif.vif5_7.production.mo.CompEstabType;
import fr.vif.vif5_7.production.mo.ContainerType;
import fr.vif.vif5_7.production.mo.InsertMOItemDataType;
import fr.vif.vif5_7.production.mo.InsertMOItemResponseType;
import fr.vif.vif5_7.production.mo.ItemMOSingleIdType;
import fr.vif.vif5_7.production.mo.MOItemResponseType;
import fr.vif.vif5_7.production.mo.MOItemSelectionType;
import fr.vif.vif5_7.production.mo.MOItemSingleIdType;
import fr.vif.vif5_7.production.mo.MOItemWorkshopResponseType;
import fr.vif.vif5_7.production.mo.NiMOItemIdType;
import fr.vif.vif5_7.production.mo.QuantitiesType;
import fr.vif.vif5_7.production.mo.QuantityType;
import fr.vif.vif5_7.production.mo.ReadMOItemResponseType;
import fr.vif.vif5_7.production.mo.StandardResponseType;
import fr.vif.vif5_7.production.mo.StatusType;
import fr.vif.vif5_7.production.mo.UniqueValueCriteriaType;
import fr.vif.vif5_7.production.mo.UniqueValueCriterionType;
import fr.vif.vif5_7.production.mo.UseType;
import fr.vif.vif5_7.production.mo.WareLocType;
import fr.vif.vif5_7.production.mo.soa.converters.MOCommonConverter;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemcrivlan;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemcrivlot;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoiteminsert;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoiteminsertresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemreadresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemreadxmvtmresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemselection;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemstatusresponse;


/**
 * MoItem web service converter class .
 * 
 * @author ac
 */
public class MOItemConverter extends MOCommonConverter {
    /** LOGGER. */
    private static final Logger    LOGGER = Logger.getLogger(MOItemConverter.class);

    private static MOItemConverter instance;

    /**
     * Default constructor.
     */
    private MOItemConverter() {
    }

    /**
     * Returns the (unique) instance of the helper class (singleton).
     * 
     * @return the MOItemConverter instance.
     */
    public static MOItemConverter getInstance() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getInstance()");
        }

        if (instance == null) {
            instance = new MOItemConverter();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getInstance()=" + instance);
        }
        return instance;
    }

    /**
     * Convert UniqueValueCriteriaType to SoamoitemPPOTtmoitemcrivlot.
     * 
     * @param batchCriteria UniqueValueCriteriaType
     * @return SoamoitemPPOTtmoitemcrivlot
     */
    public List<SoamoitemPPOTtmoitemcrivlot> convertBatchCriteriaToTtCriv(final UniqueValueCriteriaType batchCriteria) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertBatchCriteriaToTtCriv(batchCriteria=" + batchCriteria + ")");
        }

        List<SoamoitemPPOTtmoitemcrivlot> ret = new ArrayList<SoamoitemPPOTtmoitemcrivlot>();

        if (batchCriteria != null) {
            SoamoitemPPOTtmoitemcrivlot ttmoitemcrivlot;

            for (UniqueValueCriterionType criterion : batchCriteria.getUniqueValueCriterion()) {
                ttmoitemcrivlot = new SoamoitemPPOTtmoitemcrivlot();
                ttmoitemcrivlot.setCcri(emptyIfNull(criterion.getCriterionCode()));
                ttmoitemcrivlot.setVal(emptyIfNull(criterion.getCriterionValue()));
                ret.add(ttmoitemcrivlot);
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertBatchCriteriaToTtCriv(batchCriteria=" + batchCriteria + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert InsertMOItemDataType to SoamoitemPPOTtmoiteminsert.
     * 
     * @param insertData InsertMOItemDataType
     * @return SoamoitemPPOTtmoiteminsert
     */
    public SoamoitemPPOTtmoiteminsert convertInsertDataToTt(final InsertMOItemDataType insertData) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertInsertDataToTt(insertData=" + insertData + ")");
        }

        SoamoitemPPOTtmoiteminsert ret = new SoamoitemPPOTtmoiteminsert();
        if (insertData != null) {

            CompEstabType compEstabType = insertData.getCompEstab();
            if (compEstabType != null) {
                ret.setCsoc(compEstabType.getCompany());
                ret.setCetab(compEstabType.getEstablishment());
            }

            ChronoType chronoType = insertData.getMoChrono();
            if (chronoType != null) {
                ret.setPrechro(emptyIfNull(chronoType.getPrechro()));
                ret.setChrono(zeroIfNull(chronoType.getChrono()));
            }

            ContainerType container = insertData.getContainer();
            if (container != null) {
                ret.setNsc(emptyIfNull(container.getContainerNumber()));
                ret.setSscc(emptyIfNull(container.getSscc()));
                ret.setCsubdiv(emptyIfNull(container.getSubdivision()));
                ret.setNotat(emptyIfNull(container.getTattooedNumber()));
                ret.setCcond(emptyIfNull(container.getPackagingId()));
                ret.setTfermensc(falseIfNull(container.isCloseContainer()));
                ret.setNsp(emptyIfNull(container.getProductNumber()));

                QuantityType tare = container.getTare();
                if (tare != null) {
                    ret.setTare(zeroIfNull(tare.getQty()));
                    ret.setCunitar(emptyIfNull(tare.getUnit()));
                }
            }

            ContainerType fatherContainer = insertData.getFatherContainer();
            if (fatherContainer != null) {
                ret.setNscp(emptyIfNull(fatherContainer.getContainerNumber()));
                ret.setSsccp(emptyIfNull(fatherContainer.getSscc()));
                ret.setCsubdivp(emptyIfNull(fatherContainer.getSubdivision()));
                ret.setNotatp(emptyIfNull(fatherContainer.getTattooedNumber()));
                ret.setCcondp(emptyIfNull(fatherContainer.getPackagingId()));
                ret.setTfermenscp(fatherContainer.isCloseContainer());

                QuantityType tare = fatherContainer.getTare();
                if (tare != null) {
                    ret.setTarep(zeroIfNull(tare.getQty()));
                    ret.setCunitarp(emptyIfNull(tare.getUnit()));
                }

            }

            QuantitiesType quantities = insertData.getEffectiveQties();
            if (quantities != null) {
                List<QuantityType> quantityList = quantities.getQuantity();
                if (quantityList != null) {
                    if (quantityList.size() > 0) {
                        QuantityType q1 = quantityList.get(0);
                        ret.setQte1(zeroIfNull(q1.getQty()));
                        ret.setCu1(emptyIfNull(q1.getUnit()));
                    }
                    if (quantityList.size() > 1) {
                        QuantityType q2 = quantityList.get(1);
                        ret.setQte2(zeroIfNull(q2.getQty()));
                        ret.setCu2(emptyIfNull(q2.getUnit()));
                    }
                    if (quantityList.size() > 2) {
                        QuantityType q3 = quantityList.get(2);
                        ret.setQte3(zeroIfNull(q3.getQty()));
                        ret.setCu3(emptyIfNull(q3.getUnit()));
                    }
                }
            }

            MOItemSingleIdType moItemSingleId = insertData.getMoItemId();
            if (moItemSingleId != null) {
                ItemMOSingleIdType itemMOSingleId = moItemSingleId.getItemMOId();
                if (itemMOSingleId != null) {
                    ret.setCartprev(emptyIfNull(itemMOSingleId.getForecastItem()));
                    ret.setTypacta(emptyIfNull(itemMOSingleId.getItemType()));
                }
                NiMOItemIdType niMOItemId = moItemSingleId.getNiMOItemId();
                if (niMOItemId != null) {
                    ret.setNi1(zeroIfNull(niMOItemId.getNi1()));
                    ret.setNi2(zeroIfNull(niMOItemId.getNi2()));
                }
            }

            XMLGregorianCalendar mvtDateTime = insertData.getMvtDateTime();
            if (mvtDateTime != null) {
                GregorianCalendar gc = mvtDateTime.toGregorianCalendar();
                ret.setDatmvt(DateHelper.atMidnight(gc.getTime()));
                ret.setHeurmvt(TimeHelper.getTime(gc.getTime()));
            }

            XMLGregorianCalendar productionDate = insertData.getProductionDate();
            if (productionDate != null) {
                GregorianCalendar gc = productionDate.toGregorianCalendar();
                ret.setDatprod(DateHelper.atMidnight(gc.getTime()));
            }

            UseType use = insertData.getUse();
            if (use != null) {
                ret.setCofuse(use.value());
            }

            WareLocType wareLoc = insertData.getWareLoc();
            if (wareLoc != null) {
                ret.setCdepot(emptyIfNull(wareLoc.getWarehouse()));
                ret.setCemp(emptyIfNull(wareLoc.getLocation()));
            }

            ret.setCact(emptyIfNull(insertData.getActivity()));
            ret.setCartr(emptyIfNull(insertData.getEffectiveItem()));
            ret.setCclelien(emptyIfNull(insertData.getKeyLink()));
            ret.setCmotmvk(emptyIfNull(insertData.getReason()));
            ret.setCnatstk(emptyIfNull(insertData.getNature()));
            ret.setCposte(emptyIfNull(insertData.getWorkstation()));
            ret.setCuser(emptyIfNull(insertData.getUser()));
            ret.setLot(emptyIfNull(insertData.getBatch()));

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertInsertDataToTt(insertData=" + insertData + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert InsertTtResponseHolder to InsertMOItemResponseType.
     * 
     * @param insertTtResponseHolder ttHolder
     * @return InsertMOItemResponseType
     */
    public InsertMOItemResponseType convertInsertTtToResponse(
            final TempTableHolder<SoamoitemPPOTtmoiteminsertresponse> insertTtResponseHolder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertInsertTtToResponse(insertTtResponseHolder=" + insertTtResponseHolder + ")");
        }

        InsertMOItemResponseType ret = new InsertMOItemResponseType();

        if (insertTtResponseHolder != null) {
            List<SoamoitemPPOTtmoiteminsertresponse> insertTtResponseList = insertTtResponseHolder.getListValue();
            if (insertTtResponseList != null && insertTtResponseList.size() > 0) {
                SoamoitemPPOTtmoiteminsertresponse insertTtResponse = insertTtResponseList.get(0);

                ChronoType moChrono = new ChronoType();
                moChrono.setChrono(insertTtResponse.getChrono());
                moChrono.setPrechro(insertTtResponse.getPrechro());
                ret.setMoChrono(moChrono);

                ret.setNi1(insertTtResponse.getNi1());
                ret.setNi2(insertTtResponse.getNi2());
                ret.setNi3(insertTtResponse.getNi3());
                ret.setNlig(insertTtResponse.getNlig());

                ret.setBatch(nullIfEmpty(insertTtResponse.getLot()));
                ret.setContainerNumber(nullIfEmpty(insertTtResponse.getNsc()));

            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertInsertTtToResponse(insertTtResponseHolder=" + insertTtResponseHolder + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert UniqueValueCriteriaType to SoamoitemPPOTtmoitemcrivlan.
     * 
     * @param moCriteria UniqueValueCriteriaType
     * @return SoamoitemPPOTtmoitemcrivlan
     */
    public List<SoamoitemPPOTtmoitemcrivlan> convertMOCriteriaToTtCriv(final UniqueValueCriteriaType moCriteria) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertMOCriteriaToTtCriv(moCriteria=" + moCriteria + ")");
        }

        List<SoamoitemPPOTtmoitemcrivlan> ret = new ArrayList<SoamoitemPPOTtmoitemcrivlan>();

        if (moCriteria != null) {
            SoamoitemPPOTtmoitemcrivlan ttmoitemcrivlan;

            for (UniqueValueCriterionType criterion : moCriteria.getUniqueValueCriterion()) {
                ttmoitemcrivlan = new SoamoitemPPOTtmoitemcrivlan();
                ttmoitemcrivlan.setCcri(emptyIfNull(criterion.getCriterionCode()));
                ttmoitemcrivlan.setVal(emptyIfNull(criterion.getCriterionValue()));
                ret.add(ttmoitemcrivlan);
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertMOCriteriaToTtCriv(moCriteria=" + moCriteria + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert ReadResponseHolder to ReadMOItemResponseType.
     * 
     * @param readResponseHolder SoamoitemPPOTtmoitemreadresponse
     * @param readXmvtmResponseHolder SoamoitemPPOTtmoitemreadxmvtmresponse
     * @return ReadMOItemResponseType
     */
    public ReadMOItemResponseType convertReadTtToResponse(
            final TempTableHolder<SoamoitemPPOTtmoitemreadresponse> readResponseHolder,
            final TempTableHolder<SoamoitemPPOTtmoitemreadxmvtmresponse> readXmvtmResponseHolder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertReadTtToResponse(readResponseHolder=" + readResponseHolder
                    + ", readXmvtmResponseHolder=" + readXmvtmResponseHolder + ")");
        }

        ReadMOItemResponseType ret = new ReadMOItemResponseType();
        MOItemResponseType moItemResponse;

        if (readResponseHolder != null) {
            List<SoamoitemPPOTtmoitemreadresponse> listReadResponse = readResponseHolder.getListValue();
            for (SoamoitemPPOTtmoitemreadresponse readResponse : listReadResponse) {
                moItemResponse = new MOItemResponseType();
                moItemResponse.setBatch(readResponse.getLot());
                moItemResponse.setEffectiveQties(getQuantitiesType(readResponse.getQte1(), readResponse.getCu1(),
                        readResponse.getQte2(), readResponse.getCu2(), readResponse.getQte3(), readResponse.getCu3()));
                moItemResponse.setGenDateTime(getXMLGregorianCalendar(readResponse.getDatgen(),
                        readResponse.getHeurgen()));
                moItemResponse.setItem(getCodeLabelType(readResponse.getCart(), readResponse.getLart()));
                moItemResponse.setItemType(readResponse.getTypacta());
                moItemResponse.setMoItemId(getMOItemCompleteIdType(readResponse.getCsoc(), readResponse.getCetab(),
                        readResponse.getPrechro(), readResponse.getChrono(), readResponse.getNi1(),
                        readResponse.getNi2(), readResponse.getNi3(), readResponse.getNlig()));
                moItemResponse.setMvtDateTime(getXMLGregorianCalendar(readResponse.getDatmvt(),
                        readResponse.getHeurmvt()));
                moItemResponse.setNature(readResponse.getCnatstk());
                moItemResponse.setOrigin(getOriginType(readResponse.getTypor(), readResponse.getNi1or(),
                        readResponse.getNi2or(), readResponse.getNi3or(), readResponse.getNi4or()));
                moItemResponse.setReason(getCodeLabelType(readResponse.getCmotmvk(), readResponse.getLmotmvk()));
                moItemResponse.setStock(readResponse.getTstock());
                moItemResponse.setTare(readResponse.getTare());
                moItemResponse.setUser(readResponse.getCuser());
                moItemResponse.setWareLoc(getWareLocType(readResponse.getCdepot(), readResponse.getCemp()));
                ret.getMoItemResponses().add(moItemResponse);
            }
        }

        MOItemWorkshopResponseType workshopResponse;
        if (readXmvtmResponseHolder != null) {
            List<SoamoitemPPOTtmoitemreadxmvtmresponse> listXmvtmResponse = readXmvtmResponseHolder.getListValue();
            for (SoamoitemPPOTtmoitemreadxmvtmresponse xmvtmResponse : listXmvtmResponse) {
                workshopResponse = new MOItemWorkshopResponseType();
                workshopResponse.setBatch(xmvtmResponse.getLot());
                workshopResponse.setDirection(xmvtmResponse.getSens());
                workshopResponse
                        .setEffectiveQties(getQuantitiesType(xmvtmResponse.getQug(), xmvtmResponse.getCug(),
                                xmvtmResponse.getQus(), xmvtmResponse.getCus(), xmvtmResponse.getQut(),
                                xmvtmResponse.getCut()));
                workshopResponse.setGenDateTime(getXMLGregorianCalendar(xmvtmResponse.getDatgen(),
                        xmvtmResponse.getHeurgen()));
                workshopResponse.setItem(getCodeLabelType(xmvtmResponse.getCart(), xmvtmResponse.getLart()));
                workshopResponse.setMoWorkshopId(getMOWorkshopCompleteIdType(xmvtmResponse.getCsoc(),
                        xmvtmResponse.getCetab(), xmvtmResponse.getPrechro(), xmvtmResponse.getChrono(),
                        xmvtmResponse.getNi1(), xmvtmResponse.getNi2(), xmvtmResponse.getNi3(), xmvtmResponse.getNi4(),
                        xmvtmResponse.getNlig(), xmvtmResponse.getTypgest(), xmvtmResponse.getRstock()));
                workshopResponse.setMvtDateTime(getXMLGregorianCalendar(xmvtmResponse.getDatmvt(),
                        xmvtmResponse.getHeurmvt()));
                workshopResponse.setMvtType(xmvtmResponse.getTypmvt());
                workshopResponse.setNature(xmvtmResponse.getCnatstk());
                workshopResponse.setReason(getCodeLabelType(xmvtmResponse.getCmotmvk(), xmvtmResponse.getLmotmvk()));
                workshopResponse.setReservationType(xmvtmResponse.getTypres());
                workshopResponse.setStockUpdateDateTime(getXMLGregorianCalendar(xmvtmResponse.getDatsto(),
                        xmvtmResponse.getHeursto()));
                workshopResponse.setUser(xmvtmResponse.getCuser());
                workshopResponse.setWareLoc(getWareLocType(xmvtmResponse.getCdepot(), xmvtmResponse.getCemp()));
                workshopResponse.setWorkstation(xmvtmResponse.getCposte());
                workshopResponse.setProductNumber(xmvtmResponse.getNsp());
                workshopResponse.setContainerNumber(xmvtmResponse.getNsc());
                workshopResponse.setContainerNumberFather(xmvtmResponse.getNscp());
                workshopResponse.setSubdivision(xmvtmResponse.getCsubdiv());
                workshopResponse.setSubdivisionFather(xmvtmResponse.getCsubdivp());
                workshopResponse.setTattooedNumber(xmvtmResponse.getNotat());
                workshopResponse.setTattooedNumberFather(xmvtmResponse.getNotatp());

                ret.getMoItemWorkshopResponses().add(workshopResponse);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertReadTtToResponse(readResponseHolder=" + readResponseHolder
                    + ", readXmvtmResponseHolder=" + readXmvtmResponseHolder + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert MOItemSelectionType to SoamoitemPPOTtmoitemselection .
     * 
     * @param moItemSelectionType MOItemSelectionType
     * @return SoamoitemPPOTtmoitemselection
     */
    public SoamoitemPPOTtmoitemselection convertSelectionToTt(final MOItemSelectionType moItemSelectionType) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertSelectionToTt(moItemSelectionType=" + moItemSelectionType + ")");
        }

        SoamoitemPPOTtmoitemselection ret = new SoamoitemPPOTtmoitemselection();

        if (moItemSelectionType != null) {

            CompEstabType compEstab = moItemSelectionType.getCompEstab();
            if (compEstab != null) {
                ret.setCsoc(compEstab.getCompany());
                ret.setCetab(compEstab.getEstablishment());
            }

            ChronoType chrono = moItemSelectionType.getMoChrono();
            if (chrono != null) {
                ret.setPrechro(emptyIfNull(chrono.getPrechro()));
                ret.setChrono(zeroIfNull(chrono.getChrono()));
            }

            XMLGregorianCalendar productionDate = moItemSelectionType.getProductionDate();
            if (productionDate != null) {
                GregorianCalendar gc = productionDate.toGregorianCalendar();
                ret.setDatprod(DateHelper.atMidnight(gc.getTime()));
            }

            UseType use = moItemSelectionType.getUse();
            if (use != null) {
                ret.setCofuse(use.value());
            }

            ret.setCact(emptyIfNull(moItemSelectionType.getActivity()));
            ret.setCartprev(emptyIfNull(moItemSelectionType.getForecastItem()));
            ret.setCuser(emptyIfNull(moItemSelectionType.getUser()));
            ret.setNi1(zeroIfNull(moItemSelectionType.getNi1()));
            ret.setNi2(zeroIfNull(moItemSelectionType.getNi2()));
            ret.setNi3(zeroIfNull(moItemSelectionType.getNi3()));
            ret.setTypacta(emptyIfNull(moItemSelectionType.getItemType()));

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertSelectionToTt(moItemSelectionType=" + moItemSelectionType + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert StatusResponseHolder to StandardResponseType.
     * 
     * @param statusResponseHolder status response holder
     * @return StandardResponseType
     */
    public StandardResponseType convertStatusResponseToStandardResponse(
            final TempTableHolder<SoamoitemPPOTtmoitemstatusresponse> statusResponseHolder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertStatusResponseToStandardResponse(statusResponseHolder=" + statusResponseHolder
                    + ")");
        }

        StandardResponseType ret = new StandardResponseType();

        if (statusResponseHolder != null) {
            List<SoamoitemPPOTtmoitemstatusresponse> statusResponseList = statusResponseHolder.getListValue();
            if (statusResponseList != null && statusResponseList.size() > 0) {
                SoamoitemPPOTtmoitemstatusresponse statusResponse = statusResponseList.get(0);
                ret.setMessage(statusResponse.getMsg());
                ret.setStatus(StatusType.fromValue(statusResponse.getCstatus()));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertStatusResponseToStandardResponse(statusResponseHolder=" + statusResponseHolder
                    + ")=" + ret);
        }
        return ret;
    }

}

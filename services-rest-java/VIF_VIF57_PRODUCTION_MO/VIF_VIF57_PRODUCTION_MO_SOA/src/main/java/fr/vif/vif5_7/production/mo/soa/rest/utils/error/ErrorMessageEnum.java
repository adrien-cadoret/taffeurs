/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on Jul 28, 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils.error;


/**
 * Enum for all error messages.
 *
 * @author ac
 */
public enum ErrorMessageEnum {

    DATA_ACCESS_ERROR("An error occurred during the data access");

    /**
     * The value.
     */
    private final String value;

    /**
     * Constructor.
     * 
     * @param value the value
     */
    private ErrorMessageEnum(final String value) {
        this.value = value;
    }

    /**
     * Gets the value.
     * 
     * @category getter
     * @return the value.
     */
    public String getValue() {
        String valueRet = value;
        return valueRet;
    }

}

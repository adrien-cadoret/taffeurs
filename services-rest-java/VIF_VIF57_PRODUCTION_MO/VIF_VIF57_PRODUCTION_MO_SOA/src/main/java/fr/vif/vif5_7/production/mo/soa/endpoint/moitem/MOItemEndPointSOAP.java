/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: MOItemEndPointSOAP.java,v $
 * Created on 20 mars 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem;


import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;
import javax.xml.ws.WebServiceException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.soa.endpoint.AbstractSOAEndPoint;
import fr.vif.vif5_7.production.mo.DeleteMOItemRequest;
import fr.vif.vif5_7.production.mo.DeleteMOItemResponse;
import fr.vif.vif5_7.production.mo.IMOItemSOA;
import fr.vif.vif5_7.production.mo.InsertMOItemRequest;
import fr.vif.vif5_7.production.mo.InsertMOItemResponse;
import fr.vif.vif5_7.production.mo.ReadMOItemRequest;
import fr.vif.vif5_7.production.mo.ReadMOItemResponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPO;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemcrivlan;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemcrivlot;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoiteminsert;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoiteminsertresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemreadresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemreadxmvtmresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemselection;
import fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy.SoamoitemPPOTtmoitemstatusresponse;


/**
 * Web Service for item declaration.
 * 
 * @author vl
 */

@WebService(serviceName = "MOItemEndPointService", portName = "MOItemEndPointPort", targetNamespace = "http://www.vif.fr/vif5_7/production/mo", wsdlLocation = "classpath:/production/mo/soa/schemas/MOItemSOA.wsdl", endpointInterface = "fr.vif.vif5_7.production.mo.IMOItemSOA")
public class MOItemEndPointSOAP extends AbstractSOAEndPoint implements IMOItemSOA {
    /** LOGGER. */
    private static final Logger LOGGER       = Logger.getLogger(MOItemEndPointSOAP.class);

    @Autowired
    private SoamoitemPPO        soamoitemPPO = null;

    private MOItemConverter     converter;

    /**
     * {@inheritDoc}
     */
    @Override
    public DeleteMOItemResponse deleteMOItem(final DeleteMOItemRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteMOItem(request=" + request + ")");
        }

        DeleteMOItemResponse ret = new DeleteMOItemResponse();

        VIFContext ctx;
        try {
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOItemConverter.getInstance();

                // Set ttMOItemSelection parameter
                List<SoamoitemPPOTtmoitemselection> ttMoItemSelection = new ArrayList<SoamoitemPPOTtmoitemselection>();
                ttMoItemSelection.add(converter.convertSelectionToTt(request.getDeleteMOItemSelection()
                        .getMoItemSelectionType()));

                // Set ttMOItemCrivLan parameter
                List<SoamoitemPPOTtmoitemcrivlan> ttMOItemCrivLans = converter.convertMOCriteriaToTtCriv(request
                        .getDeleteMOItemSelection().getMoItemSelectionType().getMoCriteria());

                // Create response holder
                TempTableHolder<SoamoitemPPOTtmoitemstatusresponse> statusResponseHolder = new TempTableHolder<SoamoitemPPOTtmoitemstatusresponse>();

                // Call appServer
                getSoamoitemPPO().deleteMOItem(ctx, ttMoItemSelection, ttMOItemCrivLans, statusResponseHolder);

                // Manage Response
                ret.setStandardResponse(converter.convertStatusResponseToStandardResponse(statusResponseHolder));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                throw new WebServiceException(e);
            } catch (DAOException e) {
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteMOItem(request=" + request + ")=" + ret);
        }
        return ret;
    }

    /**
     * Gets the soamoitemPPO.
     * 
     * @return the soamoitemPPO.
     */
    public SoamoitemPPO getSoamoitemPPO() {
        return soamoitemPPO;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InsertMOItemResponse insertMOItem(final InsertMOItemRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertMOItem(request=" + request + ")");
        }

        InsertMOItemResponse ret = new InsertMOItemResponse();

        VIFContext ctx;
        try {
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOItemConverter.getInstance();

                // Set ttMOItemInsert parameter
                List<SoamoitemPPOTtmoiteminsert> ttMOItemInserts = new ArrayList<SoamoitemPPOTtmoiteminsert>();
                ttMOItemInserts.add(converter.convertInsertDataToTt(request.getInsertMOItemData()));

                // Set ttMOItemCrivLan parameter
                List<SoamoitemPPOTtmoitemcrivlan> ttMOItemCrivLans = converter.convertMOCriteriaToTtCriv(request
                        .getInsertMOItemData().getMoCriteria());

                // Set ttMOItemCrivLot parameter
                List<SoamoitemPPOTtmoitemcrivlot> ttMOItemCrivLots = converter.convertBatchCriteriaToTtCriv(request
                        .getInsertMOItemData().getBatchCriteria());

                // Create response holder
                TempTableHolder<SoamoitemPPOTtmoiteminsertresponse> insertResponseHolder = new TempTableHolder<SoamoitemPPOTtmoiteminsertresponse>();
                TempTableHolder<SoamoitemPPOTtmoitemstatusresponse> statusResponseHolder = new TempTableHolder<SoamoitemPPOTtmoitemstatusresponse>();

                // Call appServer
                getSoamoitemPPO().insertMOItem(ctx, ttMOItemInserts, ttMOItemCrivLans, ttMOItemCrivLots,
                        statusResponseHolder, insertResponseHolder);

                // Manage Response
                ret.setStandardResponse(converter.convertStatusResponseToStandardResponse(statusResponseHolder));
                ret.setInsertMOItemResponse(converter.convertInsertTtToResponse(insertResponseHolder));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                throw new WebServiceException(e);
            } catch (DAOException e) {
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertMOItem(request=" + request + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public ReadMOItemResponse readMOItem(final ReadMOItemRequest request) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - readMOItem(request=" + request + ")");
        }

        ReadMOItemResponse ret = new ReadMOItemResponse();

        VIFContext ctx;
        try {
            if (request == null) {
                throw new WebServiceException("Request parameter is null.");
            }

            String consumerId = "";
            if (request.getGeneralInfos() != null && request.getGeneralInfos().getConsumerId() != null) {
                consumerId = request.getGeneralInfos().getConsumerId();
            }
            ctx = createVIFContext(getSOAContext(consumerId));
            try {

                // Init converter
                converter = MOItemConverter.getInstance();

                // Set ttMOItemSelection parameter
                List<SoamoitemPPOTtmoitemselection> ttMOItemSelection = new ArrayList<SoamoitemPPOTtmoitemselection>();
                ttMOItemSelection.add(converter.convertSelectionToTt(request.getReadMOItemSelection()
                        .getMoItemSelectionType()));

                // Set ttMOItemCrivLan parameter
                List<SoamoitemPPOTtmoitemcrivlan> ttMOItemCrivLans = converter.convertMOCriteriaToTtCriv(request
                        .getReadMOItemSelection().getMoItemSelectionType().getMoCriteria());

                // Create response holder
                TempTableHolder<SoamoitemPPOTtmoitemstatusresponse> statusResponseHolder = new TempTableHolder<SoamoitemPPOTtmoitemstatusresponse>();
                TempTableHolder<SoamoitemPPOTtmoitemreadresponse> readResponseHolder = new TempTableHolder<SoamoitemPPOTtmoitemreadresponse>();
                TempTableHolder<SoamoitemPPOTtmoitemreadxmvtmresponse> readXmvtmResponseHolder = new TempTableHolder<SoamoitemPPOTtmoitemreadxmvtmresponse>();

                // Call appServer
                getSoamoitemPPO().readMOItem(ctx, ttMOItemSelection, ttMOItemCrivLans, statusResponseHolder,
                        readResponseHolder, readXmvtmResponseHolder);

                // Manage Response
                ret.setStandardResponse(converter.convertStatusResponseToStandardResponse(statusResponseHolder));
                ret.setReadMOItemResponse(converter
                        .convertReadTtToResponse(readResponseHolder, readXmvtmResponseHolder));

                commit(ctx);
            } catch (ProgressBusinessException e) {
                throw new WebServiceException(e);
            } catch (DAOException e) {
                throw new WebServiceException(e);
            } finally {
                close(ctx);
            }
        } catch (BusinessException e1) {
            throw new WebServiceException(e1.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - readMOItem(request=" + request + ")=" + ret);
        }
        return ret;
    }

    /**
     * Sets the soamoitemPPO.
     * 
     * @param soamoitemPPO soamoitemPPO.
     */
    public void setSoamoitemPPO(final SoamoitemPPO soamoitemPPO) {
        this.soamoitemPPO = soamoitemPPO;
    }

}

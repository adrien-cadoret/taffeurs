/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoPPOTtDeleteresponse.java,v $
 * Created on 19 nov. 2014 by nle
 * Revision: $Revision: 1.3 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamopxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.3 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoPPOTtDeleteresponse extends AbstractTempTable implements Serializable {


    private String cetab = "";

    private int chrono = 0;

    private String csoc = "";

    private String cstatus = "";

    private String msg = "";

    private String prechro = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoPPOTtDeleteresponse() {
        super();
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the chrono.
     *
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Sets the chrono.
     *
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the cstatus.
     *
     * @category getter
     * @return the cstatus.
     */
    public final String getCstatus() {
        return cstatus;
    }

    /**
     * Sets the cstatus.
     *
     * @category setter
     * @param cstatus cstatus.
     */
    public final void setCstatus(final String cstatus) {
        this.cstatus = cstatus;
    }

    /**
     * Gets the msg.
     *
     * @category getter
     * @return the msg.
     */
    public final String getMsg() {
        return msg;
    }

    /**
     * Sets the msg.
     *
     * @category setter
     * @param msg msg.
     */
    public final void setMsg(final String msg) {
        this.msg = msg;
    }

    /**
     * Gets the prechro.
     *
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Sets the prechro.
     *
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

}

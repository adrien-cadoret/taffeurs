/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoPPO.java,v $
 * Created on 19 nov. 2014 by nle
 * Revision: $Revision: 1.4 $ $Date: 2015/03/10 11:16:38 $ $Author: gbe $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.Parameter;
import com.progress.open4gl.ProResultSet;
import com.progress.open4gl.ProResultSetMetaDataImpl;
import com.progress.open4gl.ProSQLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.ProInputTempTable;
import fr.vif.jtech.business.progress.TempTableHolder;


/**
 * Generated class from soamopxy.p.
 * 
 * @author nle
 * @version $Revision: 1.4 $, $Date: 2015/03/10 11:16:38 $
 */
@Service
public class SoamoPPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger             LOGGER                   = Logger.getLogger(SoamoPPO.class);

    /**
     * ProResultSetMetaDataImpl for the temp-table ttDeleteresponse.
     */
    private static ProResultSetMetaDataImpl metadatattDeleteresponse = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttIncriteria.
     */
    private static ProResultSetMetaDataImpl metadatattIncriteria     = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttInsertresponse.
     */
    private static ProResultSetMetaDataImpl metadatattInsertresponse = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMoinsert.
     */
    private static ProResultSetMetaDataImpl metadatattMoinsert       = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMoresponse.
     */
    private static ProResultSetMetaDataImpl metadatattMoresponse     = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMoselection.
     */
    private static ProResultSetMetaDataImpl metadatattMoselection    = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttOutcriteria.
     */
    private static ProResultSetMetaDataImpl metadatattOutcriteria    = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttReadresponse.
     */
    private static ProResultSetMetaDataImpl metadatattReadresponse   = null;

    /**
     * Default Constructor.
     */
    public SoamoPPO() {
        super();
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttDeleteresponse List of SoamoPPOTtDeleteresponse
     * @return ProInputTempTable Convert a record set to a list of SoamoPPOTtDeleteresponse
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettDeleteresponse(
            final List<SoamoPPOTtDeleteresponse> ttDeleteresponse) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoPPOTtDeleteresponse");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoPPOTtDeleteresponse> iter = ttDeleteresponse.iterator();
        while (iter.hasNext()) {
            SoamoPPOTtDeleteresponse row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCstatus());
            list.add(row.getMsg());
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttIncriteria List of SoamoPPOTtIncriteria
     * @return ProInputTempTable Convert a record set to a list of SoamoPPOTtIncriteria
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettIncriteria(final List<SoamoPPOTtIncriteria> ttIncriteria)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoPPOTtIncriteria");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoPPOTtIncriteria> iter = ttIncriteria.iterator();
        while (iter.hasNext()) {
            SoamoPPOTtIncriteria row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCcri());
            list.add(row.getTypcri());
            list.add(row.getMsaisie());
            list.add(row.getValmin());
            list.add(row.getValmax());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttInsertresponse List of SoamoPPOTtInsertresponse
     * @return ProInputTempTable Convert a record set to a list of SoamoPPOTtInsertresponse
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettInsertresponse(
            final List<SoamoPPOTtInsertresponse> ttInsertresponse) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoPPOTtInsertresponse");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoPPOTtInsertresponse> iter = ttInsertresponse.iterator();
        while (iter.hasNext()) {
            SoamoPPOTtInsertresponse row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCstatus());
            list.add(row.getMsg());
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMoinsert List of SoamoPPOTtMoinsert
     * @return ProInputTempTable Convert a record set to a list of SoamoPPOTtMoinsert
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMoinsert(final List<SoamoPPOTtMoinsert> ttMoinsert)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoPPOTtMoinsert");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoPPOTtMoinsert> iter = ttMoinsert.iterator();
        while (iter.hasNext()) {
            SoamoPPOTtMoinsert row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getCofuse());
            list.add(row.getTypof());
            list.add(row.getChronor());
            list.add(row.getCetat());
            list.add(row.getCart());
            list.add(row.getCact());
            list.add(row.getCiti());
            list.add(row.getCres());
            list.add(row.getCequip());
            list.add(row.getCuser());
            list.add(row.getTraisdat());
            list.add(dateToGreg(row.getLadate()));
            list.add(row.getLheure());
            list.add(row.getPrior());
            list.add(new BigDecimal(row.getQte()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCunite());
            list.add(row.getLotent());
            list.add(row.getLot());
            list.add(row.getXCtie());
            list.add(row.getXCacteurLiv());
            list.add(row.getXLprepa());
            list.add(row.getXLexpe());
            list.add(row.getXCdepotorig());
            list.add(row.getXCdepotexp());
            list.add(row.getXCmodprep());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMoresponse List of SoamoPPOTtMoresponse
     * @return ProInputTempTable Convert a record set to a list of SoamoPPOTtMoresponse
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMoresponse(final List<SoamoPPOTtMoresponse> ttMoresponse)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoPPOTtMoresponse");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoPPOTtMoresponse> iter = ttMoresponse.iterator();
        while (iter.hasNext()) {
            SoamoPPOTtMoresponse row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            list.add(row.getCetat());
            list.add(row.getCarts());
            list.add(row.getCact());
            list.add(row.getCiti());
            list.add(row.getCres());
            list.add(row.getCequip());
            list.add(row.getCuser());
            list.add(dateToGreg(row.getDatdeb()));
            list.add(row.getHeurdeb());
            list.add(dateToGreg(row.getDatfin()));
            list.add(row.getHeurfin());
            list.add(dateToGreg(row.getDatdebr()));
            list.add(row.getHeurdebr());
            list.add(dateToGreg(row.getDatfinr()));
            list.add(row.getHeurfinr());
            list.add(row.getPrior());
            list.add(row.getLof());
            list.add(row.getRof());
            list.add(new BigDecimal(row.getQtep()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getQter()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getQtet()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getQtestk()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getQtestk2()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCunite());
            list.add(row.getCustk2());
            list.add(row.getLotent());
            list.add(row.getLot());
            list.add(row.getCcom());
            list.add(row.getTypflux());
            list.add(row.getTypof());
            list.add(row.getCsocor());
            list.add(row.getCetabor());
            list.add(row.getPrechror());
            list.add(row.getChronor());
            list.add(row.getRessprim());
            list.add(dateToGreg(row.getDatprod()));
            list.add(row.getCofuse());
            list.add(row.getLstsect());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMoselection List of SoamoPPOTtMoselection
     * @return ProInputTempTable Convert a record set to a list of SoamoPPOTtMoselection
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMoselection(final List<SoamoPPOTtMoselection> ttMoselection)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoPPOTtMoselection");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoPPOTtMoselection> iter = ttMoselection.iterator();
        while (iter.hasNext()) {
            SoamoPPOTtMoselection row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            list.add(row.getCart());
            list.add(row.getLstcetat());
            list.add(row.getCres());
            list.add(row.getLstcsecteur());
            list.add(row.getCequip());
            list.add(row.getLstcofuse());
            list.add(row.getTypof());
            list.add(dateToGreg(row.getDatprod()));
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttOutcriteria List of SoamoPPOTtOutcriteria
     * @return ProInputTempTable Convert a record set to a list of SoamoPPOTtOutcriteria
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettOutcriteria(final List<SoamoPPOTtOutcriteria> ttOutcriteria)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoPPOTtOutcriteria");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoPPOTtOutcriteria> iter = ttOutcriteria.iterator();
        while (iter.hasNext()) {
            SoamoPPOTtOutcriteria row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            list.add(row.getCcri());
            list.add(row.getTypcri());
            list.add(row.getMsaisie());
            list.add(row.getValmin());
            list.add(row.getValmax());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttReadresponse List of SoamoPPOTtReadresponse
     * @return ProInputTempTable Convert a record set to a list of SoamoPPOTtReadresponse
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettReadresponse(final List<SoamoPPOTtReadresponse> ttReadresponse)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoPPOTtReadresponse");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoPPOTtReadresponse> iter = ttReadresponse.iterator();
        while (iter.hasNext()) {
            SoamoPPOTtReadresponse row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCstatus());
            list.add(row.getMsg());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttDeleteresponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttDeleteresponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattDeleteresponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttDeleteresponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattDeleteresponse == null) {
            metadatattDeleteresponse = new ProResultSetMetaDataImpl(6);
            metadatattDeleteresponse.setFieldMetaData(1, "cstatus", 0, Parameter.PRO_CHARACTER);
            metadatattDeleteresponse.setFieldMetaData(2, "msg", 0, Parameter.PRO_CHARACTER);
            metadatattDeleteresponse.setFieldMetaData(3, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattDeleteresponse.setFieldMetaData(4, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattDeleteresponse.setFieldMetaData(5, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattDeleteresponse.setFieldMetaData(6, "chrono", 0, Parameter.PRO_INTEGER);
        }
        /* CHECKSTYLE:ON */
        return metadatattDeleteresponse;
    }

    /**
     * Generate the MetaData of tempTable ttIncriteria.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttIncriteria.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattIncriteria() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttIncriteria");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattIncriteria == null) {
            metadatattIncriteria = new ProResultSetMetaDataImpl(5);
            metadatattIncriteria.setFieldMetaData(1, "ccri", 0, Parameter.PRO_CHARACTER);
            metadatattIncriteria.setFieldMetaData(2, "typcri", 0, Parameter.PRO_CHARACTER);
            metadatattIncriteria.setFieldMetaData(3, "msaisie", 0, Parameter.PRO_CHARACTER);
            metadatattIncriteria.setFieldMetaData(4, "valmin", 0, Parameter.PRO_CHARACTER);
            metadatattIncriteria.setFieldMetaData(5, "valmax", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattIncriteria;
    }

    /**
     * Generate the MetaData of tempTable ttInsertresponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttInsertresponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattInsertresponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttInsertresponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattInsertresponse == null) {
            metadatattInsertresponse = new ProResultSetMetaDataImpl(6);
            metadatattInsertresponse.setFieldMetaData(1, "cstatus", 0, Parameter.PRO_CHARACTER);
            metadatattInsertresponse.setFieldMetaData(2, "msg", 0, Parameter.PRO_CHARACTER);
            metadatattInsertresponse.setFieldMetaData(3, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattInsertresponse.setFieldMetaData(4, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattInsertresponse.setFieldMetaData(5, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattInsertresponse.setFieldMetaData(6, "chrono", 0, Parameter.PRO_INTEGER);
        }
        /* CHECKSTYLE:ON */
        return metadatattInsertresponse;
    }

    /**
     * Generate the MetaData of tempTable ttMoinsert.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMoinsert.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMoinsert() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMoinsert");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMoinsert == null) {
            metadatattMoinsert = new ProResultSetMetaDataImpl(27);
            metadatattMoinsert.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(3, "cofuse", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(4, "typof", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(5, "chronor", 0, Parameter.PRO_INTEGER);
            metadatattMoinsert.setFieldMetaData(6, "cetat", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(7, "cart", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(8, "cact", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(9, "citi", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(10, "cres", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(11, "cequip", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(12, "cuser", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(13, "traisdat", 0, Parameter.PRO_LOGICAL);
            metadatattMoinsert.setFieldMetaData(14, "ladate", 0, Parameter.PRO_DATE);
            metadatattMoinsert.setFieldMetaData(15, "lheure", 0, Parameter.PRO_INTEGER);
            metadatattMoinsert.setFieldMetaData(16, "prior", 0, Parameter.PRO_INTEGER);
            metadatattMoinsert.setFieldMetaData(17, "qte", 0, Parameter.PRO_DECIMAL);
            metadatattMoinsert.setFieldMetaData(18, "cunite", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(19, "lotent", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(20, "lot", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(21, "XCtie", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(22, "XCacteurLiv", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(23, "XLprepa", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(24, "XLexpe", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(25, "XCdepotorig", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(26, "XCdepotexp", 0, Parameter.PRO_CHARACTER);
            metadatattMoinsert.setFieldMetaData(27, "XCmodprep", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMoinsert;
    }

    /**
     * Generate the MetaData of tempTable ttMoresponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMoresponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMoresponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMoresponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMoresponse == null) {
            metadatattMoresponse = new ProResultSetMetaDataImpl(42);
            metadatattMoresponse.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMoresponse.setFieldMetaData(5, "cetat", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(6, "carts", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(7, "cact", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(8, "citi", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(9, "cres", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(10, "cequip", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(11, "cuser", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(12, "datdeb", 0, Parameter.PRO_DATE);
            metadatattMoresponse.setFieldMetaData(13, "heurdeb", 0, Parameter.PRO_INTEGER);
            metadatattMoresponse.setFieldMetaData(14, "datfin", 0, Parameter.PRO_DATE);
            metadatattMoresponse.setFieldMetaData(15, "heurfin", 0, Parameter.PRO_INTEGER);
            metadatattMoresponse.setFieldMetaData(16, "datdebr", 0, Parameter.PRO_DATE);
            metadatattMoresponse.setFieldMetaData(17, "heurdebr", 0, Parameter.PRO_INTEGER);
            metadatattMoresponse.setFieldMetaData(18, "datfinr", 0, Parameter.PRO_DATE);
            metadatattMoresponse.setFieldMetaData(19, "heurfinr", 0, Parameter.PRO_INTEGER);
            metadatattMoresponse.setFieldMetaData(20, "prior", 0, Parameter.PRO_INTEGER);
            metadatattMoresponse.setFieldMetaData(21, "lof", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(22, "rof", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(23, "qtep", 0, Parameter.PRO_DECIMAL);
            metadatattMoresponse.setFieldMetaData(24, "qter", 0, Parameter.PRO_DECIMAL);
            metadatattMoresponse.setFieldMetaData(25, "qtet", 0, Parameter.PRO_DECIMAL);
            metadatattMoresponse.setFieldMetaData(26, "qtestk", 0, Parameter.PRO_DECIMAL);
            metadatattMoresponse.setFieldMetaData(27, "qtestk2", 0, Parameter.PRO_DECIMAL);
            metadatattMoresponse.setFieldMetaData(28, "cunite", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(29, "custk2", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(30, "lotent", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(31, "lot", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(32, "ccom", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(33, "typflux", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(34, "typof", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(35, "csocor", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(36, "cetabor", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(37, "prechror", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(38, "chronor", 0, Parameter.PRO_INTEGER);
            metadatattMoresponse.setFieldMetaData(39, "ressprim", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(40, "datprod", 0, Parameter.PRO_DATE);
            metadatattMoresponse.setFieldMetaData(41, "cofuse", 0, Parameter.PRO_CHARACTER);
            metadatattMoresponse.setFieldMetaData(42, "lstsect", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMoresponse;
    }

    /**
     * Generate the MetaData of tempTable ttMoselection.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMoselection.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMoselection() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMoselection");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMoselection == null) {
            metadatattMoselection = new ProResultSetMetaDataImpl(12);
            metadatattMoselection.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMoselection.setFieldMetaData(5, "cart", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(6, "lstcetat", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(7, "cres", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(8, "lstcsecteur", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(9, "cequip", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(10, "lstcofuse", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(11, "typof", 0, Parameter.PRO_CHARACTER);
            metadatattMoselection.setFieldMetaData(12, "datprod", 0, Parameter.PRO_DATE);
        }
        /* CHECKSTYLE:ON */
        return metadatattMoselection;
    }

    /**
     * Generate the MetaData of tempTable ttOutcriteria.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttOutcriteria.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattOutcriteria() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttOutcriteria");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattOutcriteria == null) {
            metadatattOutcriteria = new ProResultSetMetaDataImpl(9);
            metadatattOutcriteria.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattOutcriteria.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattOutcriteria.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattOutcriteria.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattOutcriteria.setFieldMetaData(5, "ccri", 0, Parameter.PRO_CHARACTER);
            metadatattOutcriteria.setFieldMetaData(6, "typcri", 0, Parameter.PRO_CHARACTER);
            metadatattOutcriteria.setFieldMetaData(7, "msaisie", 0, Parameter.PRO_CHARACTER);
            metadatattOutcriteria.setFieldMetaData(8, "valmin", 0, Parameter.PRO_CHARACTER);
            metadatattOutcriteria.setFieldMetaData(9, "valmax", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattOutcriteria;
    }

    /**
     * Generate the MetaData of tempTable ttReadresponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttReadresponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattReadresponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttReadresponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattReadresponse == null) {
            metadatattReadresponse = new ProResultSetMetaDataImpl(2);
            metadatattReadresponse.setFieldMetaData(1, "cstatus", 0, Parameter.PRO_CHARACTER);
            metadatattReadresponse.setFieldMetaData(2, "msg", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattReadresponse;
    }

    /**
     * Generated Class deleteMos.
     * 
     * @param ttMoselection TABLE INPUT
     * @param ttIncriteria TABLE INPUT
     * @param ttDeleteresponseHolder TABLE OUTPUT
     * @param ctx Database Context.
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void deleteMos(final VIFContext ctx, final List<SoamoPPOTtMoselection> ttMoselection,
            final List<SoamoPPOTtIncriteria> ttIncriteria,
            final TempTableHolder<SoamoPPOTtDeleteresponse> ttDeleteresponseHolder) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttDeleteresponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : deleteMos values :" + "ttDeleteresponseHolder = " + ttDeleteresponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method deleteMos " + " ttMoselection = " + ttMoselection + " ttIncriteria = "
                    + ttIncriteria + " ttDeleteresponseHolder = " + ttDeleteresponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(3);

        try {
            // ------------------------------------------
            // Make the temp-table ttMoselection
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMoselection");
            }
            params.addTable(0, convertTempTablettMoselection(ttMoselection), ParamArrayMode.INPUT,
                    getMetaDatattMoselection());
            // ------------------------------------------
            // Make the temp-table ttIncriteria
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttIncriteria");
            }
            params.addTable(1, convertTempTablettIncriteria(ttIncriteria), ParamArrayMode.INPUT,
                    getMetaDatattIncriteria());
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattDeleteresponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "deleteMos", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttDeleteresponse

            ttDeleteresponseHolder.setListValue(convertRecordttDeleteresponse(params, 2));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "soamopxy.p";
    }

    /**
     * Generated Class insertFabricationMos.
     * 
     * @param ttMoinsert TABLE INPUT
     * @param ttIncriteria TABLE INPUT
     * @param ttInsertresponseHolder TABLE OUTPUT
     * @param ctx Database Context.
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void insertFabricationMos(final VIFContext ctx, final List<SoamoPPOTtMoinsert> ttMoinsert,
            final List<SoamoPPOTtIncriteria> ttIncriteria,
            final TempTableHolder<SoamoPPOTtInsertresponse> ttInsertresponseHolder) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttInsertresponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : insertFabricationMos values :" + "ttInsertresponseHolder = "
                        + ttInsertresponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method insertFabricationMos " + " ttMoinsert = " + ttMoinsert + " ttIncriteria = "
                    + ttIncriteria + " ttInsertresponseHolder = " + ttInsertresponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(3);

        try {
            // ------------------------------------------
            // Make the temp-table ttMoinsert
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMoinsert");
            }
            params.addTable(0, convertTempTablettMoinsert(ttMoinsert), ParamArrayMode.INPUT, getMetaDatattMoinsert());
            // ------------------------------------------
            // Make the temp-table ttIncriteria
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttIncriteria");
            }
            params.addTable(1, convertTempTablettIncriteria(ttIncriteria), ParamArrayMode.INPUT,
                    getMetaDatattIncriteria());
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattInsertresponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "insertFabricationMos", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttInsertresponse

            ttInsertresponseHolder.setListValue(convertRecordttInsertresponse(params, 2));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class insertPreparationMos.
     * 
     * @param ttMoinsert TABLE INPUT
     * @param ttIncriteria TABLE INPUT
     * @param ttInsertresponseHolder TABLE OUTPUT
     * @param ctx Database Context.
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void insertPreparationMos(final VIFContext ctx, final List<SoamoPPOTtMoinsert> ttMoinsert,
            final List<SoamoPPOTtIncriteria> ttIncriteria,
            final TempTableHolder<SoamoPPOTtInsertresponse> ttInsertresponseHolder) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttInsertresponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : insertPreparationMos values :" + "ttInsertresponseHolder = "
                        + ttInsertresponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method insertPreparationMos " + " ttMoinsert = " + ttMoinsert + " ttIncriteria = "
                    + ttIncriteria + " ttInsertresponseHolder = " + ttInsertresponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(3);

        try {
            // ------------------------------------------
            // Make the temp-table ttMoinsert
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMoinsert");
            }
            params.addTable(0, convertTempTablettMoinsert(ttMoinsert), ParamArrayMode.INPUT, getMetaDatattMoinsert());
            // ------------------------------------------
            // Make the temp-table ttIncriteria
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttIncriteria");
            }
            params.addTable(1, convertTempTablettIncriteria(ttIncriteria), ParamArrayMode.INPUT,
                    getMetaDatattIncriteria());
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattInsertresponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "insertPreparationMos", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttInsertresponse

            ttInsertresponseHolder.setListValue(convertRecordttInsertresponse(params, 2));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class readMos.
     * 
     * @param ttMoselection TABLE INPUT
     * @param ttIncriteria TABLE INPUT
     * @param ttMoresponseHolder TABLE OUTPUT
     * @param ttOutcriteriaHolder TABLE OUTPUT
     * @param ttReadresponseHolder TABLE OUTPUT
     * @param ctx Database Context.
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void readMos(final VIFContext ctx, final List<SoamoPPOTtMoselection> ttMoselection,
            final List<SoamoPPOTtIncriteria> ttIncriteria,
            final TempTableHolder<SoamoPPOTtMoresponse> ttMoresponseHolder,
            final TempTableHolder<SoamoPPOTtOutcriteria> ttOutcriteriaHolder,
            final TempTableHolder<SoamoPPOTtReadresponse> ttReadresponseHolder) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttMoresponseHolder == null || ttOutcriteriaHolder == null || ttReadresponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : readMos values :" + "ttMoresponseHolder = " + ttMoresponseHolder
                        + "ttOutcriteriaHolder = " + ttOutcriteriaHolder + "ttReadresponseHolder = "
                        + ttReadresponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method readMos " + " ttMoselection = " + ttMoselection + " ttIncriteria = " + ttIncriteria
                    + " ttMoresponseHolder = " + ttMoresponseHolder + " ttOutcriteriaHolder = " + ttOutcriteriaHolder
                    + " ttReadresponseHolder = " + ttReadresponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);

        try {
            // ------------------------------------------
            // Make the temp-table ttMoselection
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMoselection");
            }
            params.addTable(0, convertTempTablettMoselection(ttMoselection), ParamArrayMode.INPUT,
                    getMetaDatattMoselection());
            // ------------------------------------------
            // Make the temp-table ttIncriteria
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttIncriteria");
            }
            params.addTable(1, convertTempTablettIncriteria(ttIncriteria), ParamArrayMode.INPUT,
                    getMetaDatattIncriteria());
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattMoresponse());
            params.addTable(3, null, ParamArrayMode.OUTPUT, getMetaDatattOutcriteria());
            params.addTable(4, null, ParamArrayMode.OUTPUT, getMetaDatattReadresponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "readMos", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttMoresponse

            ttMoresponseHolder.setListValue(convertRecordttMoresponse(params, 2));
            // Make the temp-table ttOutcriteria

            ttOutcriteriaHolder.setListValue(convertRecordttOutcriteria(params, 3));
            // Make the temp-table ttReadresponse

            ttReadresponseHolder.setListValue(convertRecordttReadresponse(params, 4));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoPPOTtDeleteresponse&gt; Convert a record set to a list of SoamoPPOTtDeleteresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoPPOTtDeleteresponse> convertRecordttDeleteresponse(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoPPOTtDeleteresponse");
        }

        ProResultSet rs = null;
        List<SoamoPPOTtDeleteresponse> list = new ArrayList<SoamoPPOTtDeleteresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoPPOTtDeleteresponse tempTable = new SoamoPPOTtDeleteresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setCstatus(rs.getString("cstatus"));
                tempTable.setMsg(rs.getString("msg"));
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoPPOTtIncriteria&gt; Convert a record set to a list of SoamoPPOTtIncriteria
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoPPOTtIncriteria> convertRecordttIncriteria(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoPPOTtIncriteria");
        }

        ProResultSet rs = null;
        List<SoamoPPOTtIncriteria> list = new ArrayList<SoamoPPOTtIncriteria>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoPPOTtIncriteria tempTable = new SoamoPPOTtIncriteria();
                /* CHECKSTYLE:OFF */
                tempTable.setCcri(rs.getString("ccri"));
                tempTable.setTypcri(rs.getString("typcri"));
                tempTable.setMsaisie(rs.getString("msaisie"));
                tempTable.setValmin(rs.getString("valmin"));
                tempTable.setValmax(rs.getString("valmax"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoPPOTtInsertresponse&gt; Convert a record set to a list of SoamoPPOTtInsertresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoPPOTtInsertresponse> convertRecordttInsertresponse(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoPPOTtInsertresponse");
        }

        ProResultSet rs = null;
        List<SoamoPPOTtInsertresponse> list = new ArrayList<SoamoPPOTtInsertresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoPPOTtInsertresponse tempTable = new SoamoPPOTtInsertresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setCstatus(rs.getString("cstatus"));
                tempTable.setMsg(rs.getString("msg"));
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoPPOTtMoinsert&gt; Convert a record set to a list of SoamoPPOTtMoinsert
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoPPOTtMoinsert> convertRecordttMoinsert(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoPPOTtMoinsert");
        }

        ProResultSet rs = null;
        List<SoamoPPOTtMoinsert> list = new ArrayList<SoamoPPOTtMoinsert>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoPPOTtMoinsert tempTable = new SoamoPPOTtMoinsert();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setCofuse(rs.getString("cofuse"));
                tempTable.setTypof(rs.getString("typof"));
                tempTable.setChronor(rs.getInt("chronor"));
                tempTable.setCetat(rs.getString("cetat"));
                tempTable.setCart(rs.getString("cart"));
                tempTable.setCact(rs.getString("cact"));
                tempTable.setCiti(rs.getString("citi"));
                tempTable.setCres(rs.getString("cres"));
                tempTable.setCequip(rs.getString("cequip"));
                tempTable.setCuser(rs.getString("cuser"));
                tempTable.setTraisdat(rs.getBoolean("traisdat"));
                GregorianCalendar gladate = rs.getGregorianCalendar("ladate");
                tempTable.setLadate(gladate != null ? gladate.getTime() : null);
                tempTable.setLheure(rs.getInt("lheure"));
                tempTable.setPrior(rs.getInt("prior"));
                tempTable.setQte(rs.getDouble("qte"));
                tempTable.setCunite(rs.getString("cunite"));
                tempTable.setLotent(rs.getString("lotent"));
                tempTable.setLot(rs.getString("lot"));
                tempTable.setXCtie(rs.getString("XCtie"));
                tempTable.setXCacteurLiv(rs.getString("XCacteurLiv"));
                tempTable.setXLprepa(rs.getString("XLprepa"));
                tempTable.setXLexpe(rs.getString("XLexpe"));
                tempTable.setXCdepotorig(rs.getString("XCdepotorig"));
                tempTable.setXCdepotexp(rs.getString("XCdepotexp"));
                tempTable.setXCmodprep(rs.getString("XCmodprep"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoPPOTtMoresponse&gt; Convert a record set to a list of SoamoPPOTtMoresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoPPOTtMoresponse> convertRecordttMoresponse(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoPPOTtMoresponse");
        }

        ProResultSet rs = null;
        List<SoamoPPOTtMoresponse> list = new ArrayList<SoamoPPOTtMoresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoPPOTtMoresponse tempTable = new SoamoPPOTtMoresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                tempTable.setCetat(rs.getString("cetat"));
                tempTable.setCarts(rs.getString("carts"));
                tempTable.setCact(rs.getString("cact"));
                tempTable.setCiti(rs.getString("citi"));
                tempTable.setCres(rs.getString("cres"));
                tempTable.setCequip(rs.getString("cequip"));
                tempTable.setCuser(rs.getString("cuser"));
                GregorianCalendar gdatdeb = rs.getGregorianCalendar("datdeb");
                tempTable.setDatdeb(gdatdeb != null ? gdatdeb.getTime() : null);
                tempTable.setHeurdeb(rs.getInt("heurdeb"));
                GregorianCalendar gdatfin = rs.getGregorianCalendar("datfin");
                tempTable.setDatfin(gdatfin != null ? gdatfin.getTime() : null);
                tempTable.setHeurfin(rs.getInt("heurfin"));
                GregorianCalendar gdatdebr = rs.getGregorianCalendar("datdebr");
                tempTable.setDatdebr(gdatdebr != null ? gdatdebr.getTime() : null);
                tempTable.setHeurdebr(rs.getInt("heurdebr"));
                GregorianCalendar gdatfinr = rs.getGregorianCalendar("datfinr");
                tempTable.setDatfinr(gdatfinr != null ? gdatfinr.getTime() : null);
                tempTable.setHeurfinr(rs.getInt("heurfinr"));
                tempTable.setPrior(rs.getInt("prior"));
                tempTable.setLof(rs.getString("lof"));
                tempTable.setRof(rs.getString("rof"));
                tempTable.setQtep(rs.getDouble("qtep"));
                tempTable.setQter(rs.getDouble("qter"));
                tempTable.setQtet(rs.getDouble("qtet"));
                tempTable.setQtestk(rs.getDouble("qtestk"));
                tempTable.setQtestk2(rs.getDouble("qtestk2"));
                tempTable.setCunite(rs.getString("cunite"));
                tempTable.setCustk2(rs.getString("custk2"));
                tempTable.setLotent(rs.getString("lotent"));
                tempTable.setLot(rs.getString("lot"));
                tempTable.setCcom(rs.getString("ccom"));
                tempTable.setTypflux(rs.getString("typflux"));
                tempTable.setTypof(rs.getString("typof"));
                tempTable.setCsocor(rs.getString("csocor"));
                tempTable.setCetabor(rs.getString("cetabor"));
                tempTable.setPrechror(rs.getString("prechror"));
                tempTable.setChronor(rs.getInt("chronor"));
                tempTable.setRessprim(rs.getString("ressprim"));
                GregorianCalendar gdatprod = rs.getGregorianCalendar("datprod");
                tempTable.setDatprod(gdatprod != null ? gdatprod.getTime() : null);
                tempTable.setCofuse(rs.getString("cofuse"));
                tempTable.setLstsect(rs.getString("lstsect"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoPPOTtMoselection&gt; Convert a record set to a list of SoamoPPOTtMoselection
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoPPOTtMoselection> convertRecordttMoselection(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoPPOTtMoselection");
        }

        ProResultSet rs = null;
        List<SoamoPPOTtMoselection> list = new ArrayList<SoamoPPOTtMoselection>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoPPOTtMoselection tempTable = new SoamoPPOTtMoselection();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                tempTable.setCart(rs.getString("cart"));
                tempTable.setLstcetat(rs.getString("lstcetat"));
                tempTable.setCres(rs.getString("cres"));
                tempTable.setLstcsecteur(rs.getString("lstcsecteur"));
                tempTable.setCequip(rs.getString("cequip"));
                tempTable.setLstcofuse(rs.getString("lstcofuse"));
                tempTable.setTypof(rs.getString("typof"));
                GregorianCalendar gdatprod = rs.getGregorianCalendar("datprod");
                tempTable.setDatprod(gdatprod != null ? gdatprod.getTime() : null);
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoPPOTtOutcriteria&gt; Convert a record set to a list of SoamoPPOTtOutcriteria
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoPPOTtOutcriteria> convertRecordttOutcriteria(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoPPOTtOutcriteria");
        }

        ProResultSet rs = null;
        List<SoamoPPOTtOutcriteria> list = new ArrayList<SoamoPPOTtOutcriteria>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoPPOTtOutcriteria tempTable = new SoamoPPOTtOutcriteria();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                tempTable.setCcri(rs.getString("ccri"));
                tempTable.setTypcri(rs.getString("typcri"));
                tempTable.setMsaisie(rs.getString("msaisie"));
                tempTable.setValmin(rs.getString("valmin"));
                tempTable.setValmax(rs.getString("valmax"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoPPOTtReadresponse&gt; Convert a record set to a list of SoamoPPOTtReadresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoPPOTtReadresponse> convertRecordttReadresponse(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoPPOTtReadresponse");
        }

        ProResultSet rs = null;
        List<SoamoPPOTtReadresponse> list = new ArrayList<SoamoPPOTtReadresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoPPOTtReadresponse tempTable = new SoamoPPOTtReadresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setCstatus(rs.getString("cstatus"));
                tempTable.setMsg(rs.getString("msg"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

}

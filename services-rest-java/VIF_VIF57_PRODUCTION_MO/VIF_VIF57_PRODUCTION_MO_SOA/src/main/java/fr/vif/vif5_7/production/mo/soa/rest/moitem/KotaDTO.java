/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 20 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.moitem;


import java.util.Arrays;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;

import org.codehaus.jackson.annotate.JsonIgnoreProperties;

import fr.vif.jtech.common.util.ObjectHelper;


/**
 * Kota Data Transfer Object.
 *
 * @author ac
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class KotaDTO {

    /**
     * Code Societe .
     */
    private String   csoc      = "";

    /**
     * Etablissement.
     */
    private String   cetab     = "";

    /**
     * Type du chrono.
     */
    private String   prechro   = "";

    /**
     * Chrono.
     */
    private int      chrono    = 0;

    /**
     * Numero Interne.
     */
    private int      ni1       = 0;

    /**
     * Numero Interne.
     */
    private int      ni2       = 0;

    /**
     * ENT -> Entrants SOR ->Sortants ART -> Articles de coût.
     */
    private String   typacta   = "";

    /**
     * Code propriété ADM -> Administratif ATE -> Atelier TOU -> Tous.
     */
    private String   cprop     = "";

    /**
     * Code avancement : EAT->En Attente de Traitement TEC -> Traitement En Cours TRT -> TRaitement Terminé.
     */
    private String   cavanc    = "";

    /**
     * Code Article.
     */
    private String   cart      = "";

    /**
     * Variante.
     */
    private String   cv        = "";

    /**
     * Lot sortant.
     */
    private String   lotsor    = "";

    /**
     * (utilisé pour stocker l'info 'Ligne de freinte' - LIGFR - de kacta à la génération des kota).
     */
    private String   cformu    = "";

    /**
     * Unité.
     */
    private String   cunite    = "";

    /**
     * Quantité de référence temps.
     */
    private double   lotq      = 0;

    /**
     * 2ème unité.
     */
    private String   cu2       = "";

    /**
     * Quantité de référence temps.
     */
    private double   lotq2     = 0;

    /**
     * Quantité de référence temps initiale.
     */
    private double   lotqi     = 0;

    /**
     * Quantités : prévue, théorique et réelle.
     */
    private double[] qte;

    /**
     * Origine.
     */
    private String   orig      = "";

    /**
     * Clé de la ressource énergétique : kotod.ni2,kotod.nlig.
     */
    private String   clenrj    = "";

    /**
     * Quantité constante.
     */
    private boolean  tqconst   = true;

    /**
     * Mouvement de stock automatique.
     */
    private boolean  tauto     = false;

    /**
     * Dépôt préférentiel.
     */
    private String   cdepot    = "";

    /**
     * Emplacement.
     */
    private String   cemp      = "";

    /**
     * Règle de calcul des coûts sortants.
     */
    private String   crges     = "";

    /**
     * Coefficient de répartition des coûts sur les sortants.
     */
    private double   coefcout  = 0;

    /**
     * Pourcentage de freinte.
     */
    private double   txfr      = 0;

    /**
     * Quantite de freinte.
     */
    private double[] qtefr;

    /**
     * Rendement matière.
     */
    private boolean  trdtm     = false;

    /**
     * Quantité automatique par différence.
     */
    private boolean  tqtedif   = false;

    /**
     * Zone obsolète.
     */
    private boolean  tctnul    = false;

    /**
     * renseigné si c'est un article fantôme.
     */
    private int      ni1or     = 0;

    /**
     * renseigné si c'est un article fantôme.
     */
    private int      ni2or     = 0;

    /**
     * Ni2 opération liée.
     */
    private int      ni2opinf  = 0;

    /**
     * Edité sur les documents de production.
     */
    private boolean  teprod    = true;

    /**
     * Unité 2ème théorique.
     */
    private String   cu2theo   = "";

    /**
     * Quantité 2ème théorique.
     */
    private double   q2theo    = 0;

    /**
     * Article générique obligatoire.
     */
    private boolean  toartgen  = false;

    /**
     * Quantité effective (avec freinte).
     */
    private double   qtestk    = 0;

    /**
     * Date du mouvement prévu (avec l'heure).
     */
    @XmlJavaTypeAdapter(fr.vif.vif5_7.production.mo.soa.rest.utils.XMLDateAdapter.class)
    private Date     datheurmvt;

    /**
     * Numéro interne de l'article dans l'activité (kacta.ni1).
     */
    private int      ni1acta   = 0;

    /**
     * Participe au rendement dynamique.
     */
    private boolean  trdtdyn   = false;

    /**
     * Quantité réelle de déclassés.
     */
    private double   qtedec    = 0;

    /**
     * NORMALE,SSQTE,AUTOHIDE,AUTOVISU.
     */
    private String   typdecatl = "";

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean equals(final Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        KotaDTO other = (KotaDTO) obj;
        if (cart == null) {
            if (other.cart != null)
                return false;
        } else if (!cart.equals(other.cart))
            return false;
        if (cavanc == null) {
            if (other.cavanc != null)
                return false;
        } else if (!cavanc.equals(other.cavanc))
            return false;
        if (cdepot == null) {
            if (other.cdepot != null)
                return false;
        } else if (!cdepot.equals(other.cdepot))
            return false;
        if (cemp == null) {
            if (other.cemp != null)
                return false;
        } else if (!cemp.equals(other.cemp))
            return false;
        if (cetab == null) {
            if (other.cetab != null)
                return false;
        } else if (!cetab.equals(other.cetab))
            return false;
        if (cformu == null) {
            if (other.cformu != null)
                return false;
        } else if (!cformu.equals(other.cformu))
            return false;
        if (chrono != other.chrono)
            return false;
        if (clenrj == null) {
            if (other.clenrj != null)
                return false;
        } else if (!clenrj.equals(other.clenrj))
            return false;
        if (Double.doubleToLongBits(coefcout) != Double.doubleToLongBits(other.coefcout))
            return false;
        if (cprop == null) {
            if (other.cprop != null)
                return false;
        } else if (!cprop.equals(other.cprop))
            return false;
        if (crges == null) {
            if (other.crges != null)
                return false;
        } else if (!crges.equals(other.crges))
            return false;
        if (csoc == null) {
            if (other.csoc != null)
                return false;
        } else if (!csoc.equals(other.csoc))
            return false;
        if (cu2 == null) {
            if (other.cu2 != null)
                return false;
        } else if (!cu2.equals(other.cu2))
            return false;
        if (cu2theo == null) {
            if (other.cu2theo != null)
                return false;
        } else if (!cu2theo.equals(other.cu2theo))
            return false;
        if (cunite == null) {
            if (other.cunite != null)
                return false;
        } else if (!cunite.equals(other.cunite))
            return false;
        if (cv == null) {
            if (other.cv != null)
                return false;
        } else if (!cv.equals(other.cv))
            return false;
        if (datheurmvt == null) {
            if (other.datheurmvt != null)
                return false;
        } else if (!datheurmvt.equals(other.datheurmvt))
            return false;
        if (Double.doubleToLongBits(lotq) != Double.doubleToLongBits(other.lotq))
            return false;
        if (Double.doubleToLongBits(lotq2) != Double.doubleToLongBits(other.lotq2))
            return false;
        if (Double.doubleToLongBits(lotqi) != Double.doubleToLongBits(other.lotqi))
            return false;
        if (lotsor == null) {
            if (other.lotsor != null)
                return false;
        } else if (!lotsor.equals(other.lotsor))
            return false;
        if (ni1 != other.ni1)
            return false;
        if (ni1acta != other.ni1acta)
            return false;
        if (ni1or != other.ni1or)
            return false;
        if (ni2 != other.ni2)
            return false;
        if (ni2opinf != other.ni2opinf)
            return false;
        if (ni2or != other.ni2or)
            return false;
        if (orig == null) {
            if (other.orig != null)
                return false;
        } else if (!orig.equals(other.orig))
            return false;
        if (prechro == null) {
            if (other.prechro != null)
                return false;
        } else if (!prechro.equals(other.prechro))
            return false;
        if (Double.doubleToLongBits(q2theo) != Double.doubleToLongBits(other.q2theo))
            return false;
        if (!Arrays.equals(qte, other.qte))
            return false;
        if (Double.doubleToLongBits(qtedec) != Double.doubleToLongBits(other.qtedec))
            return false;
        if (!Arrays.equals(qtefr, other.qtefr))
            return false;
        if (Double.doubleToLongBits(qtestk) != Double.doubleToLongBits(other.qtestk))
            return false;
        if (tauto != other.tauto)
            return false;
        if (tctnul != other.tctnul)
            return false;
        if (teprod != other.teprod)
            return false;
        if (toartgen != other.toartgen)
            return false;
        if (tqconst != other.tqconst)
            return false;
        if (tqtedif != other.tqtedif)
            return false;
        if (trdtdyn != other.trdtdyn)
            return false;
        if (trdtm != other.trdtm)
            return false;
        if (Double.doubleToLongBits(txfr) != Double.doubleToLongBits(other.txfr))
            return false;
        if (typacta == null) {
            if (other.typacta != null)
                return false;
        } else if (!typacta.equals(other.typacta))
            return false;
        if (typdecatl == null) {
            if (other.typdecatl != null)
                return false;
        } else if (!typdecatl.equals(other.typdecatl))
            return false;
        return true;
    }

    /**
     * Gets the Code Article.
     *
     * @return String
     */
    public String getCart() {
        return cart;
    }

    /**
     * Gets the Code avancement : EAT->En Attente de Traitement TEC -> Traitement En Cours TRT -> TRaitement Terminé.
     *
     * @return String
     */
    public String getCavanc() {
        return cavanc;
    }

    /**
     * Gets the Dépôt préférentiel.
     *
     * @return String
     */
    public String getCdepot() {
        return cdepot;
    }

    /**
     * Gets the Emplacement.
     *
     * @return String
     */
    public String getCemp() {
        return cemp;
    }

    /**
     * Gets the Etablissement.
     *
     * @return String
     */
    public String getCetab() {
        return cetab;
    }

    /**
     * Gets the (utilisé pour stocker l'info 'Ligne de freinte' - LIGFR - de kacta à la génération des kota).
     *
     * @return String
     */
    public String getCformu() {
        return cformu;
    }

    /**
     * Gets the Chrono.
     *
     * @return int
     */
    public int getChrono() {
        return chrono;
    }

    /**
     * Gets the Clé de la ressource énergétique : kotod.ni2,kotod.nlig.
     *
     * @return String
     */
    public String getClenrj() {
        return clenrj;
    }

    /**
     * Gets the Coefficient de répartition des coûts sur les sortants.
     *
     * @return double
     */
    public double getCoefcout() {
        return coefcout;
    }

    /**
     * Gets the Code propriété ADM -> Administratif ATE -> Atelier TOU -> Tous.
     *
     * @return String
     */
    public String getCprop() {
        return cprop;
    }

    /**
     * Gets the Règle de calcul des coûts sortants.
     *
     * @return String
     */
    public String getCrges() {
        return crges;
    }

    /**
     * Gets the Code Societe .
     *
     * @return String
     */
    public String getCsoc() {
        return csoc;
    }

    /**
     * Gets the 2ème unité.
     *
     * @return String
     */
    public String getCu2() {
        return cu2;
    }

    /**
     * Gets the Unité 2ème théorique.
     *
     * @return String
     */
    public String getCu2theo() {
        return cu2theo;
    }

    /**
     * Gets the Unité.
     *
     * @return String
     */
    public String getCunite() {
        return cunite;
    }

    /**
     * Gets the Variante.
     *
     * @return String
     */
    public String getCv() {
        return cv;
    }

    /**
     * Gets the Date du mouvement prévu (avec l'heure).
     *
     * @return Date
     */
    public Date getDatheurmvt() {
        return datheurmvt;
    }

    /**
     * Gets the Quantité de référence temps.
     *
     * @return double
     */
    public double getLotq() {
        return lotq;
    }

    /**
     * Gets the Quantité de référence temps.
     *
     * @return double
     */
    public double getLotq2() {
        return lotq2;
    }

    /**
     * Gets the Quantité de référence temps initiale.
     *
     * @return double
     */
    public double getLotqi() {
        return lotqi;
    }

    /**
     * Gets the Lot sortant.
     *
     * @return String
     */
    public String getLotsor() {
        return lotsor;
    }

    /**
     * Gets the Numero Interne.
     *
     * @return int
     */
    public int getNi1() {
        return ni1;
    }

    /**
     * Gets the Numéro interne de l'article dans l'activité (kacta.ni1).
     *
     * @return int
     */
    public int getNi1acta() {
        return ni1acta;
    }

    /**
     * Gets the renseigné si c'est un article fantôme.
     *
     * @return int
     */
    public int getNi1or() {
        return ni1or;
    }

    /**
     * Gets the Numero Interne.
     *
     * @return int
     */
    public int getNi2() {
        return ni2;
    }

    /**
     * Gets the Ni2 opération liée.
     *
     * @return int
     */
    public int getNi2opinf() {
        return ni2opinf;
    }

    /**
     * Gets the renseigné si c'est un article fantôme.
     *
     * @return int
     */
    public int getNi2or() {
        return ni2or;
    }

    /**
     * Gets the Origine.
     *
     * @return String
     */
    public String getOrig() {
        return orig;
    }

    /**
     * Gets the Type du chrono.
     *
     * @return String
     */
    public String getPrechro() {
        return prechro;
    }

    /**
     * Gets the Quantité 2ème théorique.
     *
     * @return double
     */
    public double getQ2theo() {
        return q2theo;
    }

    /**
     * Gets the Quantités : prévue, théorique et réelle.
     *
     * @return double[]
     */
    public double[] getQte() {
        return qte;
    }

    /**
     * Gets the 1th value of Qte extent.
     * 
     * @return that value
     */
    public double getQte1() {
        if (this.qte == null) {
            // CHECKSTYLE:OFF
            this.qte = new double[3];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.qte[0];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 2th value of Qte extent.
     * 
     * @return that value
     */
    public double getQte2() {
        if (this.qte == null) {
            // CHECKSTYLE:OFF
            this.qte = new double[3];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.qte[1];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 3th value of Qte extent.
     * 
     * @return that value
     */
    public double getQte3() {
        if (this.qte == null) {
            // CHECKSTYLE:OFF
            this.qte = new double[3];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.qte[2];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the Quantité réelle de déclassés.
     *
     * @return double
     */
    public double getQtedec() {
        return qtedec;
    }

    /**
     * Gets the Quantite de freinte.
     *
     * @return double[]
     */
    public double[] getQtefr() {
        return qtefr;
    }

    /**
     * Gets the 1th value of Qtefr extent.
     * 
     * @return that value
     */
    public double getQtefr1() {
        if (this.qtefr == null) {
            // CHECKSTYLE:OFF
            this.qtefr = new double[3];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.qtefr[0];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 2th value of Qtefr extent.
     * 
     * @return that value
     */
    public double getQtefr2() {
        if (this.qtefr == null) {
            // CHECKSTYLE:OFF
            this.qtefr = new double[3];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.qtefr[1];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the 3th value of Qtefr extent.
     * 
     * @return that value
     */
    public double getQtefr3() {
        if (this.qtefr == null) {
            // CHECKSTYLE:OFF
            this.qtefr = new double[3];
            // CHECKSTYLE:ON
        }
        // CHECKSTYLE:OFF
        return this.qtefr[2];
        // CHECKSTYLE:ON
    }

    /**
     * Gets the Quantité effective (avec freinte).
     *
     * @return double
     */
    public double getQtestk() {
        return qtestk;
    }

    /**
     * Gets the Mouvement de stock automatique.
     *
     * @return boolean
     */
    public boolean getTauto() {
        return tauto;
    }

    /**
     * Gets the Zone obsolète.
     *
     * @return boolean
     */
    public boolean getTctnul() {
        return tctnul;
    }

    /**
     * Gets the Edité sur les documents de production.
     *
     * @return boolean
     */
    public boolean getTeprod() {
        return teprod;
    }

    /**
     * Gets the Article générique obligatoire.
     *
     * @return boolean
     */
    public boolean getToartgen() {
        return toartgen;
    }

    /**
     * Gets the Quantité constante.
     *
     * @return boolean
     */
    public boolean getTqconst() {
        return tqconst;
    }

    /**
     * Gets the Quantité automatique par différence.
     *
     * @return boolean
     */
    public boolean getTqtedif() {
        return tqtedif;
    }

    /**
     * Gets the Participe au rendement dynamique.
     *
     * @return boolean
     */
    public boolean getTrdtdyn() {
        return trdtdyn;
    }

    /**
     * Gets the Rendement matière.
     *
     * @return boolean
     */
    public boolean getTrdtm() {
        return trdtm;
    }

    /**
     * Gets the Pourcentage de freinte.
     *
     * @return double
     */
    public double getTxfr() {
        return txfr;
    }

    /**
     * Gets the ENT -> Entrants SOR ->Sortants ART -> Articles de coût.
     *
     * @return String
     */
    public String getTypacta() {
        return typacta;
    }

    /**
     * Gets the NORMALE,SSQTE,AUTOHIDE,AUTOVISU.
     *
     * @return String
     */
    public String getTypdecatl() {
        return typdecatl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((cart == null) ? 0 : cart.hashCode());
        result = prime * result + ((cavanc == null) ? 0 : cavanc.hashCode());
        result = prime * result + ((cdepot == null) ? 0 : cdepot.hashCode());
        result = prime * result + ((cemp == null) ? 0 : cemp.hashCode());
        result = prime * result + ((cetab == null) ? 0 : cetab.hashCode());
        result = prime * result + ((cformu == null) ? 0 : cformu.hashCode());
        result = prime * result + chrono;
        result = prime * result + ((clenrj == null) ? 0 : clenrj.hashCode());
        long temp;
        temp = Double.doubleToLongBits(coefcout);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((cprop == null) ? 0 : cprop.hashCode());
        result = prime * result + ((crges == null) ? 0 : crges.hashCode());
        result = prime * result + ((csoc == null) ? 0 : csoc.hashCode());
        result = prime * result + ((cu2 == null) ? 0 : cu2.hashCode());
        result = prime * result + ((cu2theo == null) ? 0 : cu2theo.hashCode());
        result = prime * result + ((cunite == null) ? 0 : cunite.hashCode());
        result = prime * result + ((cv == null) ? 0 : cv.hashCode());
        result = prime * result + ((datheurmvt == null) ? 0 : datheurmvt.hashCode());
        temp = Double.doubleToLongBits(lotq);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lotq2);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(lotqi);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((lotsor == null) ? 0 : lotsor.hashCode());
        result = prime * result + ni1;
        result = prime * result + ni1acta;
        result = prime * result + ni1or;
        result = prime * result + ni2;
        result = prime * result + ni2opinf;
        result = prime * result + ni2or;
        result = prime * result + ((orig == null) ? 0 : orig.hashCode());
        result = prime * result + ((prechro == null) ? 0 : prechro.hashCode());
        temp = Double.doubleToLongBits(q2theo);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + Arrays.hashCode(qte);
        temp = Double.doubleToLongBits(qtedec);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + Arrays.hashCode(qtefr);
        temp = Double.doubleToLongBits(qtestk);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + (tauto ? 1231 : 1237);
        result = prime * result + (tctnul ? 1231 : 1237);
        result = prime * result + (teprod ? 1231 : 1237);
        result = prime * result + (toartgen ? 1231 : 1237);
        result = prime * result + (tqconst ? 1231 : 1237);
        result = prime * result + (tqtedif ? 1231 : 1237);
        result = prime * result + (trdtdyn ? 1231 : 1237);
        result = prime * result + (trdtm ? 1231 : 1237);
        temp = Double.doubleToLongBits(txfr);
        result = prime * result + (int) (temp ^ (temp >>> 32));
        result = prime * result + ((typacta == null) ? 0 : typacta.hashCode());
        result = prime * result + ((typdecatl == null) ? 0 : typdecatl.hashCode());
        return result;
    }

    /**
     * Retrieve a string representation for the current PLC key.
     * 
     * @return key as a string value
     */
    public String keyToString() {
        return "[csoc=" + getCsoc() + ", cetab=" + getCetab() + ", prechro=" + getPrechro() + ", chrono=" + getChrono()
                + ", ni1=" + getNi1() + ", ni2=" + getNi2() + "]";
    }

    /**
     * Returns the list of values of the key properties of this PLC.
     * 
     * @return the list of values of the key properties of this PLC.
     */
    public Object[] listKeyPropertyValues() {
        return new Object[] { csoc, cetab, prechro, new Integer(chrono), new Integer(ni1), new Integer(ni2) };
    }

    /**
     * Sets the Code Article.
     *
     * @param cart String
     */
    public void setCart(final String cart) {
        this.cart = cart;
    }

    /**
     * Sets the Code avancement : EAT->En Attente de Traitement TEC -> Traitement En Cours TRT -> TRaitement Terminé.
     *
     * @param cavanc String
     */
    public void setCavanc(final String cavanc) {
        this.cavanc = cavanc;
    }

    /**
     * Sets the Dépôt préférentiel.
     *
     * @param cdepot String
     */
    public void setCdepot(final String cdepot) {
        this.cdepot = cdepot;
    }

    /**
     * Sets the Emplacement.
     *
     * @param cemp String
     */
    public void setCemp(final String cemp) {
        this.cemp = cemp;
    }

    /**
     * Sets the Etablissement.
     *
     * @param cetab String
     */
    public void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Sets the (utilisé pour stocker l'info 'Ligne de freinte' - LIGFR - de kacta à la génération des kota).
     *
     * @param cformu String
     */
    public void setCformu(final String cformu) {
        this.cformu = cformu;
    }

    /**
     * Sets the Chrono.
     *
     * @param chrono int
     */
    public void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Sets the Clé de la ressource énergétique : kotod.ni2,kotod.nlig.
     *
     * @param clenrj String
     */
    public void setClenrj(final String clenrj) {
        this.clenrj = clenrj;
    }

    /**
     * Sets the Coefficient de répartition des coûts sur les sortants.
     *
     * @param coefcout double
     */
    public void setCoefcout(final double coefcout) {
        this.coefcout = coefcout;
    }

    /**
     * Sets the Code propriété ADM -> Administratif ATE -> Atelier TOU -> Tous.
     *
     * @param cprop String
     */
    public void setCprop(final String cprop) {
        this.cprop = cprop;
    }

    /**
     * Sets the Règle de calcul des coûts sortants.
     *
     * @param crges String
     */
    public void setCrges(final String crges) {
        this.crges = crges;
    }

    /**
     * Sets the Code Societe .
     *
     * @param csoc String
     */
    public void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Sets the 2ème unité.
     *
     * @param cu2 String
     */
    public void setCu2(final String cu2) {
        this.cu2 = cu2;
    }

    /**
     * Sets the Unité 2ème théorique.
     *
     * @param cu2theo String
     */
    public void setCu2theo(final String cu2theo) {
        this.cu2theo = cu2theo;
    }

    /**
     * Sets the Unité.
     *
     * @param cunite String
     */
    public void setCunite(final String cunite) {
        this.cunite = cunite;
    }

    /**
     * Sets the Variante.
     *
     * @param cv String
     */
    public void setCv(final String cv) {
        this.cv = cv;
    }

    /**
     * Sets the Date du mouvement prévu (avec l'heure).
     *
     * @param datheurmvt Date
     */
    public void setDatheurmvt(final Date datheurmvt) {
        this.datheurmvt = datheurmvt;
    }

    /**
     * Sets the Quantité de référence temps.
     *
     * @param lotq double
     */
    public void setLotq(final double lotq) {
        this.lotq = lotq;
    }

    /**
     * Sets the Quantité de référence temps.
     *
     * @param lotq2 double
     */
    public void setLotq2(final double lotq2) {
        this.lotq2 = lotq2;
    }

    /**
     * Sets the Quantité de référence temps initiale.
     *
     * @param lotqi double
     */
    public void setLotqi(final double lotqi) {
        this.lotqi = lotqi;
    }

    /**
     * Sets the Lot sortant.
     *
     * @param lotsor String
     */
    public void setLotsor(final String lotsor) {
        this.lotsor = lotsor;
    }

    /**
     * Sets the Numero Interne.
     *
     * @param ni1 int
     */
    public void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Sets the Numéro interne de l'article dans l'activité (kacta.ni1).
     *
     * @param ni1acta int
     */
    public void setNi1acta(final int ni1acta) {
        this.ni1acta = ni1acta;
    }

    /**
     * Sets the renseigné si c'est un article fantôme.
     *
     * @param ni1or int
     */
    public void setNi1or(final int ni1or) {
        this.ni1or = ni1or;
    }

    /**
     * Sets the Numero Interne.
     *
     * @param ni2 int
     */
    public void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Sets the Ni2 opération liée.
     *
     * @param ni2opinf int
     */
    public void setNi2opinf(final int ni2opinf) {
        this.ni2opinf = ni2opinf;
    }

    /**
     * Sets the renseigné si c'est un article fantôme.
     *
     * @param ni2or int
     */
    public void setNi2or(final int ni2or) {
        this.ni2or = ni2or;
    }

    /**
     * Sets the Origine.
     *
     * @param orig String
     */
    public void setOrig(final String orig) {
        this.orig = orig;
    }

    /**
     * Sets the Type du chrono.
     *
     * @param prechro String
     */
    public void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

    /**
     * Sets the Quantité 2ème théorique.
     *
     * @param q2theo double
     */
    public void setQ2theo(final double q2theo) {
        this.q2theo = q2theo;
    }

    /**
     * Sets the Quantités : prévue, théorique et réelle.
     *
     * @param qte double
     */
    public void setQte(final double[] qte) {
        this.qte = qte;
    }

    /**
     * Sets the Quantités : prévue, théorique et réelle.
     *
     * @param idx int
     * @param pqte double
     */
    public void setQte(final int idx, final double pqte) {
        if (this.qte == null) {
            // CHECKSTYLE:OFF
            this.qte = new double[3];
            // CHECKSTYLE:ON
        }
        this.qte[idx - 1] = pqte;
    }

    /**
     * Sets the 1th value of Qte extent.
     * 
     * @param qte1 the new value
     */
    public void setQte1(final double qte1) {
        // CHECKSTYLE:OFF
        setQte(1, qte1);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 2th value of Qte extent.
     * 
     * @param qte2 the new value
     */
    public void setQte2(final double qte2) {
        // CHECKSTYLE:OFF
        setQte(2, qte2);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 3th value of Qte extent.
     * 
     * @param qte3 the new value
     */
    public void setQte3(final double qte3) {
        // CHECKSTYLE:OFF
        setQte(3, qte3);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the Quantité réelle de déclassés.
     *
     * @param qtedec double
     */
    public void setQtedec(final double qtedec) {
        this.qtedec = qtedec;
    }

    /**
     * Sets the Quantite de freinte.
     *
     * @param qtefr double
     */
    public void setQtefr(final double[] qtefr) {
        this.qtefr = qtefr;
    }

    /**
     * Sets the Quantite de freinte.
     *
     * @param idx int
     * @param pqtefr double
     */
    public void setQtefr(final int idx, final double pqtefr) {
        if (this.qtefr == null) {
            // CHECKSTYLE:OFF
            this.qtefr = new double[3];
            // CHECKSTYLE:ON
        }
        this.qtefr[idx - 1] = pqtefr;
    }

    /**
     * Sets the 1th value of Qtefr extent.
     * 
     * @param qtefr1 the new value
     */
    public void setQtefr1(final double qtefr1) {
        // CHECKSTYLE:OFF
        setQtefr(1, qtefr1);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 2th value of Qtefr extent.
     * 
     * @param qtefr2 the new value
     */
    public void setQtefr2(final double qtefr2) {
        // CHECKSTYLE:OFF
        setQtefr(2, qtefr2);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the 3th value of Qtefr extent.
     * 
     * @param qtefr3 the new value
     */
    public void setQtefr3(final double qtefr3) {
        // CHECKSTYLE:OFF
        setQtefr(3, qtefr3);
        // CHECKSTYLE:ON
    }

    /**
     * Sets the Quantité effective (avec freinte).
     *
     * @param qtestk double
     */
    public void setQtestk(final double qtestk) {
        this.qtestk = qtestk;
    }

    /**
     * Sets the Mouvement de stock automatique.
     *
     * @param tauto boolean
     */
    public void setTauto(final boolean tauto) {
        this.tauto = tauto;
    }

    /**
     * Sets the Zone obsolète.
     *
     * @param tctnul boolean
     */
    public void setTctnul(final boolean tctnul) {
        this.tctnul = tctnul;
    }

    /**
     * Sets the Edité sur les documents de production.
     *
     * @param teprod boolean
     */
    public void setTeprod(final boolean teprod) {
        this.teprod = teprod;
    }

    /**
     * Sets the Article générique obligatoire.
     *
     * @param toartgen boolean
     */
    public void setToartgen(final boolean toartgen) {
        this.toartgen = toartgen;
    }

    /**
     * Sets the Quantité constante.
     *
     * @param tqconst boolean
     */
    public void setTqconst(final boolean tqconst) {
        this.tqconst = tqconst;
    }

    /**
     * Sets the Quantité automatique par différence.
     *
     * @param tqtedif boolean
     */
    public void setTqtedif(final boolean tqtedif) {
        this.tqtedif = tqtedif;
    }

    /**
     * Sets the Participe au rendement dynamique.
     *
     * @param trdtdyn boolean
     */
    public void setTrdtdyn(final boolean trdtdyn) {
        this.trdtdyn = trdtdyn;
    }

    /**
     * Sets the Rendement matière.
     *
     * @param trdtm boolean
     */
    public void setTrdtm(final boolean trdtm) {
        this.trdtm = trdtm;
    }

    /**
     * Sets the Pourcentage de freinte.
     *
     * @param txfr double
     */
    public void setTxfr(final double txfr) {
        this.txfr = txfr;
    }

    /**
     * Sets the ENT -> Entrants SOR ->Sortants ART -> Articles de coût.
     *
     * @param typacta String
     */
    public void setTypacta(final String typacta) {
        this.typacta = typacta;
    }

    /**
     * Sets the NORMALE,SSQTE,AUTOHIDE,AUTOVISU.
     *
     * @param typdecatl String
     */
    public void setTypdecatl(final String typdecatl) {
        this.typdecatl = typdecatl;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String toString() {
        return ObjectHelper.getShortReference(this) + keyToString();
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoresPPO.java,v $
 * Created on 12 déc. 2014 by nle
 * Revision: $Revision: 1.5 $ $Date: 2015/03/10 11:16:38 $ $Author: gbe $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.Parameter;
import com.progress.open4gl.ProResultSet;
import com.progress.open4gl.ProResultSetMetaDataImpl;
import com.progress.open4gl.ProSQLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.ProInputTempTable;
import fr.vif.jtech.business.progress.TempTableHolder;


/**
 * Generated class from soamorespxy.p.
 * 
 * @author nle
 * @version $Revision: 1.5 $, $Date: 2015/03/10 11:16:38 $
 */
@Service
public class SoamoresPPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger             LOGGER                             = Logger.getLogger(SoamoresPPO.class);

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOResourceCrivLan.
     */
    private static ProResultSetMetaDataImpl metadatattMOResourceCrivLan        = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOResourceInsert.
     */
    private static ProResultSetMetaDataImpl metadatattMOResourceInsert         = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOResourceInsertResponse.
     */
    private static ProResultSetMetaDataImpl metadatattMOResourceInsertResponse = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOResourceReadResponse.
     */
    private static ProResultSetMetaDataImpl metadatattMOResourceReadResponse   = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOResourceSelection.
     */
    private static ProResultSetMetaDataImpl metadatattMOResourceSelection      = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMOResourceStatusResponse.
     */
    private static ProResultSetMetaDataImpl metadatattMOResourceStatusResponse = null;

    /**
     * Default Constructor.
     */
    public SoamoresPPO() {
        super();
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOResourceCrivLan List of SoamoresPPOTtmoresourcecrivlan
     * @return ProInputTempTable Convert a record set to a list of SoamoresPPOTtmoresourcecrivlan
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOResourceCrivLan(
            final List<SoamoresPPOTtmoresourcecrivlan> ttMOResourceCrivLan) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoresPPOTtmoresourcecrivlan");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoresPPOTtmoresourcecrivlan> iter = ttMOResourceCrivLan.iterator();
        while (iter.hasNext()) {
            SoamoresPPOTtmoresourcecrivlan row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCcri());
            list.add(row.getVal());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOResourceInsert List of SoamoresPPOTtmoresourceinsert
     * @return ProInputTempTable Convert a record set to a list of SoamoresPPOTtmoresourceinsert
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOResourceInsert(
            final List<SoamoresPPOTtmoresourceinsert> ttMOResourceInsert) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoresPPOTtmoresourceinsert");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoresPPOTtmoresourceinsert> iter = ttMOResourceInsert.iterator();
        while (iter.hasNext()) {
            SoamoresPPOTtmoresourceinsert row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            list.add(dateToGreg(row.getDatprod()));
            list.add(row.getCofuse());
            list.add(row.getNi1());
            list.add(row.getNi2());
            list.add(row.getNlig());
            list.add(row.getCact());
            list.add(row.getCoper());
            list.add(row.getCrub());
            list.add(row.getCres());
            list.add(row.getInputmode());
            list.add(dateToGreg(row.getDatdebr()));
            list.add(row.getHeurdebr());
            list.add(dateToGreg(row.getDatfinr()));
            list.add(row.getHeurfinr());
            list.add(new BigDecimal(row.getDuree2()).setScale(2, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getNbr()).setScale(3, RoundingMode.HALF_EVEN));
            list.add(row.getCmotmvk());
            list.add(row.getCuser());
            list.add(row.getLigne());
            list.add(row.getCresmo());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOResourceInsertResponse List of SoamoresPPOTtmoresourceinsertresponse
     * @return ProInputTempTable Convert a record set to a list of SoamoresPPOTtmoresourceinsertresponse
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOResourceInsertResponse(
            final List<SoamoresPPOTtmoresourceinsertresponse> ttMOResourceInsertResponse)
                    throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoresPPOTtmoresourceinsertresponse");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoresPPOTtmoresourceinsertresponse> iter = ttMOResourceInsertResponse.iterator();
        while (iter.hasNext()) {
            SoamoresPPOTtmoresourceinsertresponse row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getPrechro());
            list.add(row.getChrono());
            list.add(row.getNi1());
            list.add(row.getNi2());
            list.add(row.getNlig());
            list.add(row.getNi4());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOResourceReadResponse List of SoamoresPPOTtmoresourcereadresponse
     * @return ProInputTempTable Convert a record set to a list of SoamoresPPOTtmoresourcereadresponse
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOResourceReadResponse(
            final List<SoamoresPPOTtmoresourcereadresponse> ttMOResourceReadResponse) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoresPPOTtmoresourcereadresponse");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoresPPOTtmoresourcereadresponse> iter = ttMOResourceReadResponse.iterator();
        while (iter.hasNext()) {
            SoamoresPPOTtmoresourcereadresponse row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            list.add(row.getNi1());
            list.add(row.getNi2());
            list.add(row.getNlig());
            list.add(row.getNi4());
            list.add(row.getCoper());
            list.add(row.getLoper());
            list.add(row.getCsecteur());
            list.add(row.getLsecteur());
            list.add(row.getCreskoto());
            list.add(row.getLreskoto());
            list.add(row.getCrub());
            list.add(row.getLrub());
            list.add(row.getCres());
            list.add(row.getLres());
            list.add(dateToGreg(row.getDatdebr()));
            list.add(row.getHeurdebr());
            list.add(dateToGreg(row.getDatfinr()));
            list.add(row.getHeurfinr());
            list.add(new BigDecimal(row.getDuree2()).setScale(2, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getNbr()).setScale(3, RoundingMode.HALF_EVEN));
            list.add(row.getCmotmvk());
            list.add(row.getLmotmvk());
            list.add(row.getCuser());
            list.add(dateToGreg(row.getDatmod()));
            list.add(row.getHeurmod());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOResourceSelection List of SoamoresPPOTtmoresourceselection
     * @return ProInputTempTable Convert a record set to a list of SoamoresPPOTtmoresourceselection
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOResourceSelection(
            final List<SoamoresPPOTtmoresourceselection> ttMOResourceSelection) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoresPPOTtmoresourceselection");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoresPPOTtmoresourceselection> iter = ttMOResourceSelection.iterator();
        while (iter.hasNext()) {
            SoamoresPPOTtmoresourceselection row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechro());
            list.add(row.getChrono());
            list.add(dateToGreg(row.getDatprod()));
            list.add(row.getCofuse());
            list.add(row.getNi1());
            list.add(row.getCact());
            list.add(row.getCsecteur());
            list.add(row.getCreskoto());
            list.add(row.getNi2());
            list.add(row.getCoper());
            list.add(row.getNlig());
            list.add(row.getCrub());
            list.add(row.getCres());
            list.add(row.getNi4());
            list.add(row.getLigne());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttMOResourceStatusResponse List of SoamoresPPOTtmoresourcestatusresponse
     * @return ProInputTempTable Convert a record set to a list of SoamoresPPOTtmoresourcestatusresponse
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMOResourceStatusResponse(
            final List<SoamoresPPOTtmoresourcestatusresponse> ttMOResourceStatusResponse)
                    throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for SoamoresPPOTtmoresourcestatusresponse");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<SoamoresPPOTtmoresourcestatusresponse> iter = ttMOResourceStatusResponse.iterator();
        while (iter.hasNext()) {
            SoamoresPPOTtmoresourcestatusresponse row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCstatus());
            list.add(row.getMsg());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttMOResourceCrivLan.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOResourceCrivLan.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOResourceCrivLan() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOResourceCrivLan");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOResourceCrivLan == null) {
            metadatattMOResourceCrivLan = new ProResultSetMetaDataImpl(2);
            metadatattMOResourceCrivLan.setFieldMetaData(1, "ccri", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceCrivLan.setFieldMetaData(2, "val", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOResourceCrivLan;
    }

    /**
     * Generate the MetaData of tempTable ttMOResourceInsert.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOResourceInsert.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOResourceInsert() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOResourceInsert");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOResourceInsert == null) {
            metadatattMOResourceInsert = new ProResultSetMetaDataImpl(24);
            metadatattMOResourceInsert.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsert.setFieldMetaData(5, "datprod", 0, Parameter.PRO_DATE);
            metadatattMOResourceInsert.setFieldMetaData(6, "cofuse", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(7, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsert.setFieldMetaData(8, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsert.setFieldMetaData(9, "nlig", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsert.setFieldMetaData(10, "cact", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(11, "coper", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(12, "crub", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(13, "cres", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(14, "inputmode", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(15, "datdebr", 0, Parameter.PRO_DATE);
            metadatattMOResourceInsert.setFieldMetaData(16, "heurdebr", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsert.setFieldMetaData(17, "datfinr", 0, Parameter.PRO_DATE);
            metadatattMOResourceInsert.setFieldMetaData(18, "heurfinr", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsert.setFieldMetaData(19, "duree2", 0, Parameter.PRO_DECIMAL);
            metadatattMOResourceInsert.setFieldMetaData(20, "nbr", 0, Parameter.PRO_DECIMAL);
            metadatattMOResourceInsert.setFieldMetaData(21, "cmotmvk", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(22, "cuser", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(23, "ligne", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsert.setFieldMetaData(24, "cresmo", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOResourceInsert;
    }

    /**
     * Generate the MetaData of tempTable ttMOResourceInsertResponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOResourceInsertResponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOResourceInsertResponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOResourceInsertResponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOResourceInsertResponse == null) {
            metadatattMOResourceInsertResponse = new ProResultSetMetaDataImpl(6);
            metadatattMOResourceInsertResponse.setFieldMetaData(1, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceInsertResponse.setFieldMetaData(2, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsertResponse.setFieldMetaData(3, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsertResponse.setFieldMetaData(4, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsertResponse.setFieldMetaData(5, "nlig", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceInsertResponse.setFieldMetaData(6, "ni4", 0, Parameter.PRO_INTEGER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOResourceInsertResponse;
    }

    /**
     * Generate the MetaData of tempTable ttMOResourceReadResponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOResourceReadResponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOResourceReadResponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOResourceReadResponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOResourceReadResponse == null) {
            metadatattMOResourceReadResponse = new ProResultSetMetaDataImpl(29);
            metadatattMOResourceReadResponse.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceReadResponse.setFieldMetaData(5, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceReadResponse.setFieldMetaData(6, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceReadResponse.setFieldMetaData(7, "nlig", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceReadResponse.setFieldMetaData(8, "ni4", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceReadResponse.setFieldMetaData(9, "coper", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(10, "loper", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(11, "csecteur", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(12, "lsecteur", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(13, "creskoto", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(14, "lreskoto", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(15, "crub", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(16, "lrub", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(17, "cres", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(18, "lres", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(19, "datdebr", 0, Parameter.PRO_DATE);
            metadatattMOResourceReadResponse.setFieldMetaData(20, "heurdebr", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceReadResponse.setFieldMetaData(21, "datfinr", 0, Parameter.PRO_DATE);
            metadatattMOResourceReadResponse.setFieldMetaData(22, "heurfinr", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceReadResponse.setFieldMetaData(23, "duree2", 0, Parameter.PRO_DECIMAL);
            metadatattMOResourceReadResponse.setFieldMetaData(24, "nbr", 0, Parameter.PRO_DECIMAL);
            metadatattMOResourceReadResponse.setFieldMetaData(25, "cmotmvk", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(26, "lmotmvk", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(27, "cuser", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceReadResponse.setFieldMetaData(28, "datmod", 0, Parameter.PRO_DATE);
            metadatattMOResourceReadResponse.setFieldMetaData(29, "heurmod", 0, Parameter.PRO_INTEGER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOResourceReadResponse;
    }

    /**
     * Generate the MetaData of tempTable ttMOResourceSelection.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOResourceSelection.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOResourceSelection() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOResourceSelection");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOResourceSelection == null) {
            metadatattMOResourceSelection = new ProResultSetMetaDataImpl(17);
            metadatattMOResourceSelection.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(3, "prechro", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(4, "chrono", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceSelection.setFieldMetaData(5, "datprod", 0, Parameter.PRO_DATE);
            metadatattMOResourceSelection.setFieldMetaData(6, "cofuse", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(7, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceSelection.setFieldMetaData(8, "cact", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(9, "csecteur", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(10, "creskoto", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(11, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceSelection.setFieldMetaData(12, "coper", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(13, "nlig", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceSelection.setFieldMetaData(14, "crub", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(15, "cres", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceSelection.setFieldMetaData(16, "ni4", 0, Parameter.PRO_INTEGER);
            metadatattMOResourceSelection.setFieldMetaData(17, "ligne", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOResourceSelection;
    }

    /**
     * Generate the MetaData of tempTable ttMOResourceStatusResponse.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMOResourceStatusResponse.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMOResourceStatusResponse() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMOResourceStatusResponse");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMOResourceStatusResponse == null) {
            metadatattMOResourceStatusResponse = new ProResultSetMetaDataImpl(2);
            metadatattMOResourceStatusResponse.setFieldMetaData(1, "cstatus", 0, Parameter.PRO_CHARACTER);
            metadatattMOResourceStatusResponse.setFieldMetaData(2, "msg", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMOResourceStatusResponse;
    }

    /**
     * Generated Class deleteMOResource.
     * 
     * @param ttMOResourceSelection TABLE INPUT
     * @param ttMOResourceCrivLan TABLE INPUT
     * @param ttMOResourceStatusResponseHolder TABLE OUTPUT
     * @param ctx CONTEXT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void deleteMOResource(final VIFContext ctx,
            final List<SoamoresPPOTtmoresourceselection> ttMOResourceSelection,
            final List<SoamoresPPOTtmoresourcecrivlan> ttMOResourceCrivLan,
            final TempTableHolder<SoamoresPPOTtmoresourcestatusresponse> ttMOResourceStatusResponseHolder)
                    throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttMOResourceStatusResponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : deleteMOResource values :" + "ttMOResourceStatusResponseHolder = "
                        + ttMOResourceStatusResponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method deleteMOResource " + " ttMOResourceSelection = " + ttMOResourceSelection
                    + " ttMOResourceCrivLan = " + ttMOResourceCrivLan + " ttMOResourceStatusResponseHolder = "
                    + ttMOResourceStatusResponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(3);

        try {
            // ------------------------------------------
            // Make the temp-table ttMOResourceSelection
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOResourceSelection");
            }
            params.addTable(0, convertTempTablettMOResourceSelection(ttMOResourceSelection), ParamArrayMode.INPUT,
                    getMetaDatattMOResourceSelection());
            // ------------------------------------------
            // Make the temp-table ttMOResourceCrivLan
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOResourceCrivLan");
            }
            params.addTable(1, convertTempTablettMOResourceCrivLan(ttMOResourceCrivLan), ParamArrayMode.INPUT,
                    getMetaDatattMOResourceCrivLan());
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattMOResourceStatusResponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "deleteMOResource", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttMOResourceStatusResponse

            ttMOResourceStatusResponseHolder.setListValue(convertRecordttMOResourceStatusResponse(params, 2));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "soamorespxy.p";
    }

    /**
     * Generated Class insertMOResource.
     * 
     * @param ttMOResourceInsert TABLE INPUT
     * @param ttMOResourceCrivLan TABLE INPUT
     * @param ttMOResourceStatusResponseHolder TABLE OUTPUT
     * @param ttMOResourceInsertResponseHolder TABLE OUTPUT
     * @param ctx CONTEXT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void insertMOResource(final VIFContext ctx, final List<SoamoresPPOTtmoresourceinsert> ttMOResourceInsert,
            final List<SoamoresPPOTtmoresourcecrivlan> ttMOResourceCrivLan,
            final TempTableHolder<SoamoresPPOTtmoresourcestatusresponse> ttMOResourceStatusResponseHolder,
            final TempTableHolder<SoamoresPPOTtmoresourceinsertresponse> ttMOResourceInsertResponseHolder)
                    throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttMOResourceStatusResponseHolder == null || ttMOResourceInsertResponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : insertMOResource values :" + "ttMOResourceStatusResponseHolder = "
                        + ttMOResourceStatusResponseHolder + "ttMOResourceInsertResponseHolder = "
                        + ttMOResourceInsertResponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method insertMOResource " + " ttMOResourceInsert = " + ttMOResourceInsert
                    + " ttMOResourceCrivLan = " + ttMOResourceCrivLan + " ttMOResourceStatusResponseHolder = "
                    + ttMOResourceStatusResponseHolder + " ttMOResourceInsertResponseHolder = "
                    + ttMOResourceInsertResponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(4);

        try {
            // ------------------------------------------
            // Make the temp-table ttMOResourceInsert
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOResourceInsert");
            }
            params.addTable(0, convertTempTablettMOResourceInsert(ttMOResourceInsert), ParamArrayMode.INPUT,
                    getMetaDatattMOResourceInsert());
            // ------------------------------------------
            // Make the temp-table ttMOResourceCrivLan
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOResourceCrivLan");
            }
            params.addTable(1, convertTempTablettMOResourceCrivLan(ttMOResourceCrivLan), ParamArrayMode.INPUT,
                    getMetaDatattMOResourceCrivLan());
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattMOResourceStatusResponse());
            params.addTable(3, null, ParamArrayMode.OUTPUT, getMetaDatattMOResourceInsertResponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "insertMOResource", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttMOResourceStatusResponse

            ttMOResourceStatusResponseHolder.setListValue(convertRecordttMOResourceStatusResponse(params, 2));
            // Make the temp-table ttMOResourceInsertResponse

            ttMOResourceInsertResponseHolder.setListValue(convertRecordttMOResourceInsertResponse(params, 3));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class readMOResource.
     * 
     * @param ttMOResourceSelection TABLE INPUT
     * @param ttMOResourceCrivLan TABLE INPUT
     * @param ttMOResourceStatusResponseHolder TABLE OUTPUT
     * @param ttMOResourceReadResponseHolder TABLE OUTPUT
     * @param ctx CONTEXT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void readMOResource(final VIFContext ctx,
            final List<SoamoresPPOTtmoresourceselection> ttMOResourceSelection,
            final List<SoamoresPPOTtmoresourcecrivlan> ttMOResourceCrivLan,
            final TempTableHolder<SoamoresPPOTtmoresourcestatusresponse> ttMOResourceStatusResponseHolder,
            final TempTableHolder<SoamoresPPOTtmoresourcereadresponse> ttMOResourceReadResponseHolder)
                    throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttMOResourceStatusResponseHolder == null || ttMOResourceReadResponseHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : readMOResource values :" + "ttMOResourceStatusResponseHolder = "
                        + ttMOResourceStatusResponseHolder + "ttMOResourceReadResponseHolder = "
                        + ttMOResourceReadResponseHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method readMOResource " + " ttMOResourceSelection = " + ttMOResourceSelection
                    + " ttMOResourceCrivLan = " + ttMOResourceCrivLan + " ttMOResourceStatusResponseHolder = "
                    + ttMOResourceStatusResponseHolder + " ttMOResourceReadResponseHolder = "
                    + ttMOResourceReadResponseHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(4);

        try {
            // ------------------------------------------
            // Make the temp-table ttMOResourceSelection
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOResourceSelection");
            }
            params.addTable(0, convertTempTablettMOResourceSelection(ttMOResourceSelection), ParamArrayMode.INPUT,
                    getMetaDatattMOResourceSelection());
            // ------------------------------------------
            // Make the temp-table ttMOResourceCrivLan
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttMOResourceCrivLan");
            }
            params.addTable(1, convertTempTablettMOResourceCrivLan(ttMOResourceCrivLan), ParamArrayMode.INPUT,
                    getMetaDatattMOResourceCrivLan());
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattMOResourceStatusResponse());
            params.addTable(3, null, ParamArrayMode.OUTPUT, getMetaDatattMOResourceReadResponse());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "readMOResource", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttMOResourceStatusResponse

            ttMOResourceStatusResponseHolder.setListValue(convertRecordttMOResourceStatusResponse(params, 2));
            // Make the temp-table ttMOResourceReadResponse

            ttMOResourceReadResponseHolder.setListValue(convertRecordttMOResourceReadResponse(params, 3));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoresPPOTtmoresourcecrivlan&gt; Convert a record set to a list of
     *         SoamoresPPOTtmoresourcecrivlan
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoresPPOTtmoresourcecrivlan> convertRecordttMOResourceCrivLan(final ParamArray params,
            final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoresPPOTtmoresourcecrivlan");
        }

        ProResultSet rs = null;
        List<SoamoresPPOTtmoresourcecrivlan> list = new ArrayList<SoamoresPPOTtmoresourcecrivlan>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoresPPOTtmoresourcecrivlan tempTable = new SoamoresPPOTtmoresourcecrivlan();
                /* CHECKSTYLE:OFF */
                tempTable.setCcri(rs.getString("ccri"));
                tempTable.setVal(rs.getString("val"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoresPPOTtmoresourceinsert&gt; Convert a record set to a list of SoamoresPPOTtmoresourceinsert
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoresPPOTtmoresourceinsert> convertRecordttMOResourceInsert(final ParamArray params,
            final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoresPPOTtmoresourceinsert");
        }

        ProResultSet rs = null;
        List<SoamoresPPOTtmoresourceinsert> list = new ArrayList<SoamoresPPOTtmoresourceinsert>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoresPPOTtmoresourceinsert tempTable = new SoamoresPPOTtmoresourceinsert();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                GregorianCalendar gdatprod = rs.getGregorianCalendar("datprod");
                tempTable.setDatprod(gdatprod != null ? gdatprod.getTime() : null);
                tempTable.setCofuse(rs.getString("cofuse"));
                tempTable.setNi1(rs.getInt("ni1"));
                tempTable.setNi2(rs.getInt("ni2"));
                tempTable.setNlig(rs.getInt("nlig"));
                tempTable.setCact(rs.getString("cact"));
                tempTable.setCoper(rs.getString("coper"));
                tempTable.setCrub(rs.getString("crub"));
                tempTable.setCres(rs.getString("cres"));
                tempTable.setInputmode(rs.getString("inputmode"));
                GregorianCalendar gdatdebr = rs.getGregorianCalendar("datdebr");
                tempTable.setDatdebr(gdatdebr != null ? gdatdebr.getTime() : null);
                tempTable.setHeurdebr(rs.getInt("heurdebr"));
                GregorianCalendar gdatfinr = rs.getGregorianCalendar("datfinr");
                tempTable.setDatfinr(gdatfinr != null ? gdatfinr.getTime() : null);
                tempTable.setHeurfinr(rs.getInt("heurfinr"));
                tempTable.setDuree2(rs.getDouble("duree2"));
                tempTable.setNbr(rs.getDouble("nbr"));
                tempTable.setCmotmvk(rs.getString("cmotmvk"));
                tempTable.setCuser(rs.getString("cuser"));
                tempTable.setLigne(rs.getString("ligne"));
                tempTable.setCresmo(rs.getString("cresmo"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoresPPOTtmoresourceinsertresponse&gt; Convert a record set to a list of
     *         SoamoresPPOTtmoresourceinsertresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoresPPOTtmoresourceinsertresponse> convertRecordttMOResourceInsertResponse(
            final ParamArray params, final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoresPPOTtmoresourceinsertresponse");
        }

        ProResultSet rs = null;
        List<SoamoresPPOTtmoresourceinsertresponse> list = new ArrayList<SoamoresPPOTtmoresourceinsertresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoresPPOTtmoresourceinsertresponse tempTable = new SoamoresPPOTtmoresourceinsertresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                tempTable.setNi1(rs.getInt("ni1"));
                tempTable.setNi2(rs.getInt("ni2"));
                tempTable.setNlig(rs.getInt("nlig"));
                tempTable.setNi4(rs.getInt("ni4"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoresPPOTtmoresourcereadresponse&gt; Convert a record set to a list of
     *         SoamoresPPOTtmoresourcereadresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoresPPOTtmoresourcereadresponse> convertRecordttMOResourceReadResponse(final ParamArray params,
            final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoresPPOTtmoresourcereadresponse");
        }

        ProResultSet rs = null;
        List<SoamoresPPOTtmoresourcereadresponse> list = new ArrayList<SoamoresPPOTtmoresourcereadresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoresPPOTtmoresourcereadresponse tempTable = new SoamoresPPOTtmoresourcereadresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                tempTable.setNi1(rs.getInt("ni1"));
                tempTable.setNi2(rs.getInt("ni2"));
                tempTable.setNlig(rs.getInt("nlig"));
                tempTable.setNi4(rs.getInt("ni4"));
                tempTable.setCoper(rs.getString("coper"));
                tempTable.setLoper(rs.getString("loper"));
                tempTable.setCsecteur(rs.getString("csecteur"));
                tempTable.setLsecteur(rs.getString("lsecteur"));
                tempTable.setCreskoto(rs.getString("creskoto"));
                tempTable.setLreskoto(rs.getString("lreskoto"));
                tempTable.setCrub(rs.getString("crub"));
                tempTable.setLrub(rs.getString("lrub"));
                tempTable.setCres(rs.getString("cres"));
                tempTable.setLres(rs.getString("lres"));
                GregorianCalendar gdatdebr = rs.getGregorianCalendar("datdebr");
                tempTable.setDatdebr(gdatdebr != null ? gdatdebr.getTime() : null);
                tempTable.setHeurdebr(rs.getInt("heurdebr"));
                GregorianCalendar gdatfinr = rs.getGregorianCalendar("datfinr");
                tempTable.setDatfinr(gdatfinr != null ? gdatfinr.getTime() : null);
                tempTable.setHeurfinr(rs.getInt("heurfinr"));
                tempTable.setDuree2(rs.getDouble("duree2"));
                tempTable.setNbr(rs.getDouble("nbr"));
                tempTable.setCmotmvk(rs.getString("cmotmvk"));
                tempTable.setLmotmvk(rs.getString("lmotmvk"));
                tempTable.setCuser(rs.getString("cuser"));
                GregorianCalendar gdatmod = rs.getGregorianCalendar("datmod");
                tempTable.setDatmod(gdatmod != null ? gdatmod.getTime() : null);
                tempTable.setHeurmod(rs.getInt("heurmod"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoresPPOTtmoresourceselection&gt; Convert a record set to a list of
     *         SoamoresPPOTtmoresourceselection
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoresPPOTtmoresourceselection> convertRecordttMOResourceSelection(final ParamArray params,
            final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoresPPOTtmoresourceselection");
        }

        ProResultSet rs = null;
        List<SoamoresPPOTtmoresourceselection> list = new ArrayList<SoamoresPPOTtmoresourceselection>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoresPPOTtmoresourceselection tempTable = new SoamoresPPOTtmoresourceselection();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechro(rs.getString("prechro"));
                tempTable.setChrono(rs.getInt("chrono"));
                GregorianCalendar gdatprod = rs.getGregorianCalendar("datprod");
                tempTable.setDatprod(gdatprod != null ? gdatprod.getTime() : null);
                tempTable.setCofuse(rs.getString("cofuse"));
                tempTable.setNi1(rs.getInt("ni1"));
                tempTable.setCact(rs.getString("cact"));
                tempTable.setCsecteur(rs.getString("csecteur"));
                tempTable.setCreskoto(rs.getString("creskoto"));
                tempTable.setNi2(rs.getInt("ni2"));
                tempTable.setCoper(rs.getString("coper"));
                tempTable.setNlig(rs.getInt("nlig"));
                tempTable.setCrub(rs.getString("crub"));
                tempTable.setCres(rs.getString("cres"));
                tempTable.setNi4(rs.getInt("ni4"));
                tempTable.setLigne(rs.getString("ligne"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;SoamoresPPOTtmoresourcestatusresponse&gt; Convert a record set to a list of
     *         SoamoresPPOTtmoresourcestatusresponse
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<SoamoresPPOTtmoresourcestatusresponse> convertRecordttMOResourceStatusResponse(
            final ParamArray params, final int nbParam) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for SoamoresPPOTtmoresourcestatusresponse");
        }

        ProResultSet rs = null;
        List<SoamoresPPOTtmoresourcestatusresponse> list = new ArrayList<SoamoresPPOTtmoresourcestatusresponse>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                SoamoresPPOTtmoresourcestatusresponse tempTable = new SoamoresPPOTtmoresourcestatusresponse();
                /* CHECKSTYLE:OFF */
                tempTable.setCstatus(rs.getString("cstatus"));
                tempTable.setMsg(rs.getString("msg"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoresPPOTtmoresourceinsertresponse.java,v $
 * Created on 12 déc. 2014 by nle
 * Revision: $Revision: 1.4 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moresource.proxy;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamorespxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.4 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoresPPOTtmoresourceinsertresponse extends AbstractTempTable implements Serializable {


    private int chrono = 0;

    private int ni1 = 0;

    private int ni2 = 0;

    private int ni4 = 0;

    private int nlig = 0;

    private String prechro = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoresPPOTtmoresourceinsertresponse() {
        super();
    }

    /**
     * Gets the chrono.
     *
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Sets the chrono.
     *
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Gets the ni1.
     *
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Sets the ni1.
     *
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Gets the ni2.
     *
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Sets the ni2.
     *
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Gets the ni4.
     *
     * @category getter
     * @return the ni4.
     */
    public final int getNi4() {
        return ni4;
    }

    /**
     * Sets the ni4.
     *
     * @category setter
     * @param ni4 ni4.
     */
    public final void setNi4(final int ni4) {
        this.ni4 = ni4;
    }

    /**
     * Gets the nlig.
     *
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Sets the nlig.
     *
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Gets the prechro.
     *
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Sets the prechro.
     *
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoPPOTtMoresponse.java,v $
 * Created on 19 nov. 2014 by nle
 * Revision: $Revision: 1.3 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamopxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.3 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoPPOTtMoresponse extends AbstractTempTable implements Serializable {


    private String cact = "";

    private String carts = "";

    private String ccom = "";

    private String cequip = "";

    private String cetab = "";

    private String cetabor = "";

    private String cetat = "";

    private int chrono = 0;

    private int chronor = 0;

    private String citi = "";

    private String cofuse = "";

    private String cres = "";

    private String csoc = "";

    private String csocor = "";

    private String cunite = "";

    private String cuser = "";

    private String custk2 = "";

    private Date datdeb = null;

    private Date datdebr = null;

    private Date datfin = null;

    private Date datfinr = null;

    private Date datprod = null;

    private int heurdeb = 0;

    private int heurdebr = 0;

    private int heurfin = 0;

    private int heurfinr = 0;

    private String lof = "";

    private String lot = "";

    private String lotent = "";

    private String lstsect = "";

    private String prechro = "";

    private String prechror = "";

    private int prior = 0;

    private double qtep = 0;

    private double qter = 0;

    private double qtestk = 0;

    private double qtestk2 = 0;

    private double qtet = 0;

    private String ressprim = "";

    private String rof = "";

    private String typflux = "";

    private String typof = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoPPOTtMoresponse() {
        super();
    }

    /**
     * Gets the cact.
     *
     * @category getter
     * @return the cact.
     */
    public final String getCact() {
        return cact;
    }

    /**
     * Sets the cact.
     *
     * @category setter
     * @param cact cact.
     */
    public final void setCact(final String cact) {
        this.cact = cact;
    }

    /**
     * Gets the carts.
     *
     * @category getter
     * @return the carts.
     */
    public final String getCarts() {
        return carts;
    }

    /**
     * Sets the carts.
     *
     * @category setter
     * @param carts carts.
     */
    public final void setCarts(final String carts) {
        this.carts = carts;
    }

    /**
     * Gets the ccom.
     *
     * @category getter
     * @return the ccom.
     */
    public final String getCcom() {
        return ccom;
    }

    /**
     * Sets the ccom.
     *
     * @category setter
     * @param ccom ccom.
     */
    public final void setCcom(final String ccom) {
        this.ccom = ccom;
    }

    /**
     * Gets the cequip.
     *
     * @category getter
     * @return the cequip.
     */
    public final String getCequip() {
        return cequip;
    }

    /**
     * Sets the cequip.
     *
     * @category setter
     * @param cequip cequip.
     */
    public final void setCequip(final String cequip) {
        this.cequip = cequip;
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the cetabor.
     *
     * @category getter
     * @return the cetabor.
     */
    public final String getCetabor() {
        return cetabor;
    }

    /**
     * Sets the cetabor.
     *
     * @category setter
     * @param cetabor cetabor.
     */
    public final void setCetabor(final String cetabor) {
        this.cetabor = cetabor;
    }

    /**
     * Gets the cetat.
     *
     * @category getter
     * @return the cetat.
     */
    public final String getCetat() {
        return cetat;
    }

    /**
     * Sets the cetat.
     *
     * @category setter
     * @param cetat cetat.
     */
    public final void setCetat(final String cetat) {
        this.cetat = cetat;
    }

    /**
     * Gets the chrono.
     *
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Sets the chrono.
     *
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Gets the chronor.
     *
     * @category getter
     * @return the chronor.
     */
    public final int getChronor() {
        return chronor;
    }

    /**
     * Sets the chronor.
     *
     * @category setter
     * @param chronor chronor.
     */
    public final void setChronor(final int chronor) {
        this.chronor = chronor;
    }

    /**
     * Gets the citi.
     *
     * @category getter
     * @return the citi.
     */
    public final String getCiti() {
        return citi;
    }

    /**
     * Sets the citi.
     *
     * @category setter
     * @param citi citi.
     */
    public final void setCiti(final String citi) {
        this.citi = citi;
    }

    /**
     * Gets the cofuse.
     *
     * @category getter
     * @return the cofuse.
     */
    public final String getCofuse() {
        return cofuse;
    }

    /**
     * Sets the cofuse.
     *
     * @category setter
     * @param cofuse cofuse.
     */
    public final void setCofuse(final String cofuse) {
        this.cofuse = cofuse;
    }

    /**
     * Gets the cres.
     *
     * @category getter
     * @return the cres.
     */
    public final String getCres() {
        return cres;
    }

    /**
     * Sets the cres.
     *
     * @category setter
     * @param cres cres.
     */
    public final void setCres(final String cres) {
        this.cres = cres;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the csocor.
     *
     * @category getter
     * @return the csocor.
     */
    public final String getCsocor() {
        return csocor;
    }

    /**
     * Sets the csocor.
     *
     * @category setter
     * @param csocor csocor.
     */
    public final void setCsocor(final String csocor) {
        this.csocor = csocor;
    }

    /**
     * Gets the cunite.
     *
     * @category getter
     * @return the cunite.
     */
    public final String getCunite() {
        return cunite;
    }

    /**
     * Sets the cunite.
     *
     * @category setter
     * @param cunite cunite.
     */
    public final void setCunite(final String cunite) {
        this.cunite = cunite;
    }

    /**
     * Gets the cuser.
     *
     * @category getter
     * @return the cuser.
     */
    public final String getCuser() {
        return cuser;
    }

    /**
     * Sets the cuser.
     *
     * @category setter
     * @param cuser cuser.
     */
    public final void setCuser(final String cuser) {
        this.cuser = cuser;
    }

    /**
     * Gets the custk2.
     *
     * @category getter
     * @return the custk2.
     */
    public final String getCustk2() {
        return custk2;
    }

    /**
     * Sets the custk2.
     *
     * @category setter
     * @param custk2 custk2.
     */
    public final void setCustk2(final String custk2) {
        this.custk2 = custk2;
    }

    /**
     * Gets the datdeb.
     *
     * @category getter
     * @return the datdeb.
     */
    public final Date getDatdeb() {
        return datdeb;
    }

    /**
     * Sets the datdeb.
     *
     * @category setter
     * @param datdeb datdeb.
     */
    public final void setDatdeb(final Date datdeb) {
        this.datdeb = datdeb;
    }

    /**
     * Gets the datdebr.
     *
     * @category getter
     * @return the datdebr.
     */
    public final Date getDatdebr() {
        return datdebr;
    }

    /**
     * Sets the datdebr.
     *
     * @category setter
     * @param datdebr datdebr.
     */
    public final void setDatdebr(final Date datdebr) {
        this.datdebr = datdebr;
    }

    /**
     * Gets the datfin.
     *
     * @category getter
     * @return the datfin.
     */
    public final Date getDatfin() {
        return datfin;
    }

    /**
     * Sets the datfin.
     *
     * @category setter
     * @param datfin datfin.
     */
    public final void setDatfin(final Date datfin) {
        this.datfin = datfin;
    }

    /**
     * Gets the datfinr.
     *
     * @category getter
     * @return the datfinr.
     */
    public final Date getDatfinr() {
        return datfinr;
    }

    /**
     * Sets the datfinr.
     *
     * @category setter
     * @param datfinr datfinr.
     */
    public final void setDatfinr(final Date datfinr) {
        this.datfinr = datfinr;
    }

    /**
     * Gets the datprod.
     *
     * @category getter
     * @return the datprod.
     */
    public final Date getDatprod() {
        return datprod;
    }

    /**
     * Sets the datprod.
     *
     * @category setter
     * @param datprod datprod.
     */
    public final void setDatprod(final Date datprod) {
        this.datprod = datprod;
    }

    /**
     * Gets the heurdeb.
     *
     * @category getter
     * @return the heurdeb.
     */
    public final int getHeurdeb() {
        return heurdeb;
    }

    /**
     * Sets the heurdeb.
     *
     * @category setter
     * @param heurdeb heurdeb.
     */
    public final void setHeurdeb(final int heurdeb) {
        this.heurdeb = heurdeb;
    }

    /**
     * Gets the heurdebr.
     *
     * @category getter
     * @return the heurdebr.
     */
    public final int getHeurdebr() {
        return heurdebr;
    }

    /**
     * Sets the heurdebr.
     *
     * @category setter
     * @param heurdebr heurdebr.
     */
    public final void setHeurdebr(final int heurdebr) {
        this.heurdebr = heurdebr;
    }

    /**
     * Gets the heurfin.
     *
     * @category getter
     * @return the heurfin.
     */
    public final int getHeurfin() {
        return heurfin;
    }

    /**
     * Sets the heurfin.
     *
     * @category setter
     * @param heurfin heurfin.
     */
    public final void setHeurfin(final int heurfin) {
        this.heurfin = heurfin;
    }

    /**
     * Gets the heurfinr.
     *
     * @category getter
     * @return the heurfinr.
     */
    public final int getHeurfinr() {
        return heurfinr;
    }

    /**
     * Sets the heurfinr.
     *
     * @category setter
     * @param heurfinr heurfinr.
     */
    public final void setHeurfinr(final int heurfinr) {
        this.heurfinr = heurfinr;
    }

    /**
     * Gets the lof.
     *
     * @category getter
     * @return the lof.
     */
    public final String getLof() {
        return lof;
    }

    /**
     * Sets the lof.
     *
     * @category setter
     * @param lof lof.
     */
    public final void setLof(final String lof) {
        this.lof = lof;
    }

    /**
     * Gets the lot.
     *
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Sets the lot.
     *
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Gets the lotent.
     *
     * @category getter
     * @return the lotent.
     */
    public final String getLotent() {
        return lotent;
    }

    /**
     * Sets the lotent.
     *
     * @category setter
     * @param lotent lotent.
     */
    public final void setLotent(final String lotent) {
        this.lotent = lotent;
    }

    /**
     * Gets the lstsect.
     *
     * @category getter
     * @return the lstsect.
     */
    public final String getLstsect() {
        return lstsect;
    }

    /**
     * Sets the lstsect.
     *
     * @category setter
     * @param lstsect lstsect.
     */
    public final void setLstsect(final String lstsect) {
        this.lstsect = lstsect;
    }

    /**
     * Gets the prechro.
     *
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Sets the prechro.
     *
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

    /**
     * Gets the prechror.
     *
     * @category getter
     * @return the prechror.
     */
    public final String getPrechror() {
        return prechror;
    }

    /**
     * Sets the prechror.
     *
     * @category setter
     * @param prechror prechror.
     */
    public final void setPrechror(final String prechror) {
        this.prechror = prechror;
    }

    /**
     * Gets the prior.
     *
     * @category getter
     * @return the prior.
     */
    public final int getPrior() {
        return prior;
    }

    /**
     * Sets the prior.
     *
     * @category setter
     * @param prior prior.
     */
    public final void setPrior(final int prior) {
        this.prior = prior;
    }

    /**
     * Gets the qtep.
     *
     * @category getter
     * @return the qtep.
     */
    public final double getQtep() {
        return qtep;
    }

    /**
     * Sets the qtep.
     *
     * @category setter
     * @param qtep qtep.
     */
    public final void setQtep(final double qtep) {
        this.qtep = qtep;
    }

    /**
     * Gets the qter.
     *
     * @category getter
     * @return the qter.
     */
    public final double getQter() {
        return qter;
    }

    /**
     * Sets the qter.
     *
     * @category setter
     * @param qter qter.
     */
    public final void setQter(final double qter) {
        this.qter = qter;
    }

    /**
     * Gets the qtestk.
     *
     * @category getter
     * @return the qtestk.
     */
    public final double getQtestk() {
        return qtestk;
    }

    /**
     * Sets the qtestk.
     *
     * @category setter
     * @param qtestk qtestk.
     */
    public final void setQtestk(final double qtestk) {
        this.qtestk = qtestk;
    }

    /**
     * Gets the qtestk2.
     *
     * @category getter
     * @return the qtestk2.
     */
    public final double getQtestk2() {
        return qtestk2;
    }

    /**
     * Sets the qtestk2.
     *
     * @category setter
     * @param qtestk2 qtestk2.
     */
    public final void setQtestk2(final double qtestk2) {
        this.qtestk2 = qtestk2;
    }

    /**
     * Gets the qtet.
     *
     * @category getter
     * @return the qtet.
     */
    public final double getQtet() {
        return qtet;
    }

    /**
     * Sets the qtet.
     *
     * @category setter
     * @param qtet qtet.
     */
    public final void setQtet(final double qtet) {
        this.qtet = qtet;
    }

    /**
     * Gets the ressprim.
     *
     * @category getter
     * @return the ressprim.
     */
    public final String getRessprim() {
        return ressprim;
    }

    /**
     * Sets the ressprim.
     *
     * @category setter
     * @param ressprim ressprim.
     */
    public final void setRessprim(final String ressprim) {
        this.ressprim = ressprim;
    }

    /**
     * Gets the rof.
     *
     * @category getter
     * @return the rof.
     */
    public final String getRof() {
        return rof;
    }

    /**
     * Sets the rof.
     *
     * @category setter
     * @param rof rof.
     */
    public final void setRof(final String rof) {
        this.rof = rof;
    }

    /**
     * Gets the typflux.
     *
     * @category getter
     * @return the typflux.
     */
    public final String getTypflux() {
        return typflux;
    }

    /**
     * Sets the typflux.
     *
     * @category setter
     * @param typflux typflux.
     */
    public final void setTypflux(final String typflux) {
        this.typflux = typflux;
    }

    /**
     * Gets the typof.
     *
     * @category getter
     * @return the typof.
     */
    public final String getTypof() {
        return typof;
    }

    /**
     * Sets the typof.
     *
     * @category setter
     * @param typof typof.
     */
    public final void setTypof(final String typof) {
        this.typof = typof;
    }

}

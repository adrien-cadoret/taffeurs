/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoitemPPOTtmoitemreadresponse.java,v $
 * Created on 24 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamoitempxy.p.xml.
 * 
 * @author vl
 */
public class SoamoitemPPOTtmoitemreadresponse extends AbstractTempTable implements Serializable {

    private String  cart    = "";

    private String  cdepot  = "";

    private String  cemp    = "";

    private String  cetab   = "";

    private int     chrono  = 0;

    private String  cmotmvk = "";

    private String  cnatstk = "";

    private String  csoc    = "";

    private String  cu1     = "";

    private String  cu2     = "";

    private String  cu3     = "";

    private String  cuser   = "";

    private Date    datgen  = null;

    private Date    datmvt  = null;

    private int     heurgen = 0;

    private int     heurmvt = 0;

    private String  lart    = "";

    private String  lmotmvk = "";

    private String  lot     = "";

    private int     ni1     = 0;

    private int     ni1or   = 0;

    private int     ni2     = 0;

    private int     ni2or   = 0;

    private int     ni3     = 0;

    private int     ni3or   = 0;

    private int     ni4or   = 0;

    private int     nlig    = 0;

    private String  prechro = "";

    private double  qte1    = 0;

    private double  qte2    = 0;

    private double  qte3    = 0;

    private double  tare    = 0;

    private boolean tstock  = false;

    private String  typacta = "";

    private String  typor   = "";

    /**
     * Default Constructor.
     * 
     */
    public SoamoitemPPOTtmoitemreadresponse() {
        super();
    }

    /**
     * Gets the cart.
     * 
     * @category getter
     * @return the cart.
     */
    public final String getCart() {
        return cart;
    }

    /**
     * Gets the cdepot.
     * 
     * @category getter
     * @return the cdepot.
     */
    public final String getCdepot() {
        return cdepot;
    }

    /**
     * Gets the cemp.
     * 
     * @category getter
     * @return the cemp.
     */
    public final String getCemp() {
        return cemp;
    }

    /**
     * Gets the cetab.
     * 
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Gets the chrono.
     * 
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Gets the cmotmvk.
     * 
     * @category getter
     * @return the cmotmvk.
     */
    public final String getCmotmvk() {
        return cmotmvk;
    }

    /**
     * Gets the cnatstk.
     * 
     * @category getter
     * @return the cnatstk.
     */
    public final String getCnatstk() {
        return cnatstk;
    }

    /**
     * Gets the csoc.
     * 
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Gets the cu1.
     * 
     * @category getter
     * @return the cu1.
     */
    public final String getCu1() {
        return cu1;
    }

    /**
     * Gets the cu2.
     * 
     * @category getter
     * @return the cu2.
     */
    public final String getCu2() {
        return cu2;
    }

    /**
     * Gets the cu3.
     * 
     * @category getter
     * @return the cu3.
     */
    public final String getCu3() {
        return cu3;
    }

    /**
     * Gets the cuser.
     * 
     * @category getter
     * @return the cuser.
     */
    public final String getCuser() {
        return cuser;
    }

    /**
     * Gets the datgen.
     * 
     * @category getter
     * @return the datgen.
     */
    public final Date getDatgen() {
        return datgen;
    }

    /**
     * Gets the datmvt.
     * 
     * @category getter
     * @return the datmvt.
     */
    public final Date getDatmvt() {
        return datmvt;
    }

    /**
     * Gets the heurgen.
     * 
     * @category getter
     * @return the heurgen.
     */
    public final int getHeurgen() {
        return heurgen;
    }

    /**
     * Gets the heurmvt.
     * 
     * @category getter
     * @return the heurmvt.
     */
    public final int getHeurmvt() {
        return heurmvt;
    }

    /**
     * Gets the lart.
     * 
     * @category getter
     * @return the lart.
     */
    public final String getLart() {
        return lart;
    }

    /**
     * Gets the lmotmvk.
     * 
     * @category getter
     * @return the lmotmvk.
     */
    public final String getLmotmvk() {
        return lmotmvk;
    }

    /**
     * Gets the lot.
     * 
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Gets the ni1.
     * 
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Gets the ni1or.
     * 
     * @category getter
     * @return the ni1or.
     */
    public final int getNi1or() {
        return ni1or;
    }

    /**
     * Gets the ni2.
     * 
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Gets the ni2or.
     * 
     * @category getter
     * @return the ni2or.
     */
    public final int getNi2or() {
        return ni2or;
    }

    /**
     * Gets the ni3.
     * 
     * @category getter
     * @return the ni3.
     */
    public final int getNi3() {
        return ni3;
    }

    /**
     * Gets the ni3or.
     * 
     * @category getter
     * @return the ni3or.
     */
    public final int getNi3or() {
        return ni3or;
    }

    /**
     * Gets the ni4or.
     * 
     * @category getter
     * @return the ni4or.
     */
    public final int getNi4or() {
        return ni4or;
    }

    /**
     * Gets the nlig.
     * 
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Gets the prechro.
     * 
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Gets the qte1.
     * 
     * @category getter
     * @return the qte1.
     */
    public final double getQte1() {
        return qte1;
    }

    /**
     * Gets the qte2.
     * 
     * @category getter
     * @return the qte2.
     */
    public final double getQte2() {
        return qte2;
    }

    /**
     * Gets the qte3.
     * 
     * @category getter
     * @return the qte3.
     */
    public final double getQte3() {
        return qte3;
    }

    /**
     * Gets the tare.
     * 
     * @category getter
     * @return the tare.
     */
    public final double getTare() {
        return tare;
    }

    /**
     * Gets the tstock.
     * 
     * @category getter
     * @return the tstock.
     */
    public final boolean getTstock() {
        return tstock;
    }

    /**
     * Gets the typacta.
     * 
     * @category getter
     * @return the typacta.
     */
    public final String getTypacta() {
        return typacta;
    }

    /**
     * Gets the typor.
     * 
     * @category getter
     * @return the typor.
     */
    public final String getTypor() {
        return typor;
    }

    /**
     * Sets the cart.
     * 
     * @category setter
     * @param cart cart.
     */
    public final void setCart(final String cart) {
        this.cart = cart;
    }

    /**
     * Sets the cdepot.
     * 
     * @category setter
     * @param cdepot cdepot.
     */
    public final void setCdepot(final String cdepot) {
        this.cdepot = cdepot;
    }

    /**
     * Sets the cemp.
     * 
     * @category setter
     * @param cemp cemp.
     */
    public final void setCemp(final String cemp) {
        this.cemp = cemp;
    }

    /**
     * Sets the cetab.
     * 
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Sets the chrono.
     * 
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Sets the cmotmvk.
     * 
     * @category setter
     * @param cmotmvk cmotmvk.
     */
    public final void setCmotmvk(final String cmotmvk) {
        this.cmotmvk = cmotmvk;
    }

    /**
     * Sets the cnatstk.
     * 
     * @category setter
     * @param cnatstk cnatstk.
     */
    public final void setCnatstk(final String cnatstk) {
        this.cnatstk = cnatstk;
    }

    /**
     * Sets the csoc.
     * 
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Sets the cu1.
     * 
     * @category setter
     * @param cu1 cu1.
     */
    public final void setCu1(final String cu1) {
        this.cu1 = cu1;
    }

    /**
     * Sets the cu2.
     * 
     * @category setter
     * @param cu2 cu2.
     */
    public final void setCu2(final String cu2) {
        this.cu2 = cu2;
    }

    /**
     * Sets the cu3.
     * 
     * @category setter
     * @param cu3 cu3.
     */
    public final void setCu3(final String cu3) {
        this.cu3 = cu3;
    }

    /**
     * Sets the cuser.
     * 
     * @category setter
     * @param cuser cuser.
     */
    public final void setCuser(final String cuser) {
        this.cuser = cuser;
    }

    /**
     * Sets the datgen.
     * 
     * @category setter
     * @param datgen datgen.
     */
    public final void setDatgen(final Date datgen) {
        this.datgen = datgen;
    }

    /**
     * Sets the datmvt.
     * 
     * @category setter
     * @param datmvt datmvt.
     */
    public final void setDatmvt(final Date datmvt) {
        this.datmvt = datmvt;
    }

    /**
     * Sets the heurgen.
     * 
     * @category setter
     * @param heurgen heurgen.
     */
    public final void setHeurgen(final int heurgen) {
        this.heurgen = heurgen;
    }

    /**
     * Sets the heurmvt.
     * 
     * @category setter
     * @param heurmvt heurmvt.
     */
    public final void setHeurmvt(final int heurmvt) {
        this.heurmvt = heurmvt;
    }

    /**
     * Sets the lart.
     * 
     * @category setter
     * @param lart lart.
     */
    public final void setLart(final String lart) {
        this.lart = lart;
    }

    /**
     * Sets the lmotmvk.
     * 
     * @category setter
     * @param lmotmvk lmotmvk.
     */
    public final void setLmotmvk(final String lmotmvk) {
        this.lmotmvk = lmotmvk;
    }

    /**
     * Sets the lot.
     * 
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Sets the ni1.
     * 
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Sets the ni1or.
     * 
     * @category setter
     * @param ni1or ni1or.
     */
    public final void setNi1or(final int ni1or) {
        this.ni1or = ni1or;
    }

    /**
     * Sets the ni2.
     * 
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Sets the ni2or.
     * 
     * @category setter
     * @param ni2or ni2or.
     */
    public final void setNi2or(final int ni2or) {
        this.ni2or = ni2or;
    }

    /**
     * Sets the ni3.
     * 
     * @category setter
     * @param ni3 ni3.
     */
    public final void setNi3(final int ni3) {
        this.ni3 = ni3;
    }

    /**
     * Sets the ni3or.
     * 
     * @category setter
     * @param ni3or ni3or.
     */
    public final void setNi3or(final int ni3or) {
        this.ni3or = ni3or;
    }

    /**
     * Sets the ni4or.
     * 
     * @category setter
     * @param ni4or ni4or.
     */
    public final void setNi4or(final int ni4or) {
        this.ni4or = ni4or;
    }

    /**
     * Sets the nlig.
     * 
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Sets the prechro.
     * 
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

    /**
     * Sets the qte1.
     * 
     * @category setter
     * @param qte1 qte1.
     */
    public final void setQte1(final double qte1) {
        this.qte1 = qte1;
    }

    /**
     * Sets the qte2.
     * 
     * @category setter
     * @param qte2 qte2.
     */
    public final void setQte2(final double qte2) {
        this.qte2 = qte2;
    }

    /**
     * Sets the qte3.
     * 
     * @category setter
     * @param qte3 qte3.
     */
    public final void setQte3(final double qte3) {
        this.qte3 = qte3;
    }

    /**
     * Sets the tare.
     * 
     * @category setter
     * @param tare tare.
     */
    public final void setTare(final double tare) {
        this.tare = tare;
    }

    /**
     * Sets the tstock.
     * 
     * @category setter
     * @param tstock tstock.
     */
    public final void setTstock(final boolean tstock) {
        this.tstock = tstock;
    }

    /**
     * Sets the typacta.
     * 
     * @category setter
     * @param typacta typacta.
     */
    public final void setTypacta(final String typacta) {
        this.typacta = typacta;
    }

    /**
     * Sets the typor.
     * 
     * @category setter
     * @param typor typor.
     */
    public final void setTypor(final String typor) {
        this.typor = typor;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: MOConverter.java,v $
 * Created on 27 févr. 2014 by nle
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.mo;


import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;

import javax.xml.datatype.XMLGregorianCalendar;

import org.apache.log4j.Logger;

import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.TimeHelper;
import fr.vif.vif5_7.production.mo.ChronoType;
import fr.vif.vif5_7.production.mo.CompEstabType;
import fr.vif.vif5_7.production.mo.DateHourBoundsType;
import fr.vif.vif5_7.production.mo.DeleteMosResponseType;
import fr.vif.vif5_7.production.mo.InsertFabricationMoResponseType;
import fr.vif.vif5_7.production.mo.InsertMosDataType;
import fr.vif.vif5_7.production.mo.InsertPreparationMoDataType;
import fr.vif.vif5_7.production.mo.InsertPreparationMoResponseType;
import fr.vif.vif5_7.production.mo.LabelsType;
import fr.vif.vif5_7.production.mo.MOCompleteIdType;
import fr.vif.vif5_7.production.mo.MOState3Type;
import fr.vif.vif5_7.production.mo.MOStates3ListType;
import fr.vif.vif5_7.production.mo.MoResponseType;
import fr.vif.vif5_7.production.mo.MoResponsesListType;
import fr.vif.vif5_7.production.mo.MoSelectionType;
import fr.vif.vif5_7.production.mo.MultiValueCriteriaType;
import fr.vif.vif5_7.production.mo.MultiValueCriterionType;
import fr.vif.vif5_7.production.mo.QuantityType;
import fr.vif.vif5_7.production.mo.ReadMosResponseType;
import fr.vif.vif5_7.production.mo.SectorListType;
import fr.vif.vif5_7.production.mo.StandardResponseType;
import fr.vif.vif5_7.production.mo.StatusType;
import fr.vif.vif5_7.production.mo.TeamsListType;
import fr.vif.vif5_7.production.mo.UseType;
import fr.vif.vif5_7.production.mo.UsesListType;
import fr.vif.vif5_7.production.mo.soa.converters.MOCommonConverter;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtDeleteresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtIncriteria;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtInsertresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtMoinsert;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtMoresponse;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtMoselection;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtOutcriteria;
import fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy.SoamoPPOTtReadresponse;


/**
 * Converts a list of responses of MO EXPORT from the Progress proxy to a list of responses in the xsd format.
 * 
 * @author nle
 */
public final class MOConverter extends MOCommonConverter {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(MOConverter.class);

    private static MOConverter  instance;

    /**
     * Default constructor.
     */
    private MOConverter() {
    }

    /**
     * Returns the (unique) instance of the helper class (singleton).
     * 
     * @return the MOConverter instance.
     */
    public static MOConverter getInstance() {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getInstance()");
        }

        if (instance == null) {
            instance = new MOConverter();
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getInstance()=" + instance);
        }
        return instance;
    }

    /**
     * Convert InsertFabricationMoDataType to SoamoPPOTtMoinsert .
     * 
     * @param insertMosData InsertFabricationMoDataType
     * @return SoamoPPOTtMoinsert
     */
    public SoamoPPOTtMoinsert convertInsertFabricationToTt(final InsertMosDataType insertMosData) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertInsertFabricationToTt(InsertFabricationMoDataType=" + insertMosData + ")");
        }

        SoamoPPOTtMoinsert ret = new SoamoPPOTtMoinsert();

        if (insertMosData != null) {
            /* Company - Establishment */
            CompEstabType compEstab = insertMosData.getCompEstab();
            if (compEstab != null) {
                ret.setCsoc(compEstab.getCompany());
                ret.setCetab(compEstab.getEstablishment());
            }

            /* Use */
            UseType cofuse = insertMosData.getUse();
            if (cofuse != null) {
                ret.setCofuse(cofuse.toString());
            }

            /* Item */
            String refItem = insertMosData.getRefItem();
            if (refItem != null) {
                ret.setCart(refItem);
            }

            /* Operating process / Resource */
            String process = insertMosData.getOperatingProcess();
            if (process != null) {
                ret.setCres(process);
            }

            /* MO Type */
            String ofType = insertMosData.getMoType();
            if (ofType != null) {
                ret.setTypof(ofType);
            }

            /* Date = begin or end ? */
            Boolean traisdat = insertMosData.isTraisdat();
            ret.setTraisdat(traisdat);

            /* Date-Hour */
            XMLGregorianCalendar date = insertMosData.getDateTime();
            if (date != null) {
                GregorianCalendar gc = date.toGregorianCalendar();
                ret.setLadate(DateHelper.atMidnight(gc.getTime()));
                ret.setLheure(TimeHelper.getTime(gc.getTime()));
            }

            /* Team */
            String team = insertMosData.getTeam();
            if (team != null) {
                ret.setCequip(team);
            }

            /* State */
            if (insertMosData.getMoState() != null) {
                String moState = insertMosData.getMoState().toString();
                if (moState != null) {
                    ret.setCetat(moState.toString());
                }
            }

            if (insertMosData.getForecastQty() != null) {
                /* Qty */
                ret.setQte(insertMosData.getForecastQty().getQty());

                /* Unit */
                String unit = insertMosData.getForecastQty().getUnit();
                if (unit != null) {
                    ret.setCunite(unit);
                }
            }

            /* Out Batch */
            String lotsor = insertMosData.getOutBatch();
            if (lotsor != null) {
                ret.setLot(lotsor);
            }

            /* In Batch */
            String lotent = insertMosData.getInBatch();
            if (lotent != null) {
                ret.setLotent(lotent);
            }

            if (insertMosData.getFabrication() != null) {
                /* Activities Sequence */
                String iti = insertMosData.getFabrication().getActivitiesSequenceId();
                if (iti != null) {
                    ret.setCiti(iti);
                }

                /* Activity */
                String act = insertMosData.getFabrication().getActivity();
                if (act != null) {
                    ret.setCact(act);
                }
            }

            /* Origin Chrono */
            if (insertMosData.getOriginChrono() != null) {
                ret.setChronor(insertMosData.getOriginChrono());
            }

            /* Priority */
            if (insertMosData.getPriority() != null) {
                ret.setPrior(insertMosData.getPriority());
            }

            /* User */
            String cuser = insertMosData.getUser();
            if (cuser != null) {
                ret.setCuser(cuser);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertInsertFabricationToTt(InsertFabricationMoDataType=" + insertMosData + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert InsertPreparationMoDataType to SoamoPPOTtMoinsert .
     * 
     * @param insertPreparationMoData InsertPreparationMoDataType
     * @return SoamoPPOTtMoinsert
     */
    public SoamoPPOTtMoinsert convertInsertPreparationToTt(final InsertPreparationMoDataType insertPreparationMoData) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertInsertPreparationToTt(InsertPreparationMoDataType=" + insertPreparationMoData
                    + ")");
        }

        SoamoPPOTtMoinsert ret = new SoamoPPOTtMoinsert();

        if (insertPreparationMoData != null) {
            // Common part with the Fabrication MOs
            ret = convertInsertFabricationToTt(insertPreparationMoData.getInsertMosData());

            // Specific part of the Preparation MOs

            /* Third part */
            String ctie = insertPreparationMoData.getThirdParty();
            if (ctie != null) {
                ret.setXCtie(ctie);
            }

            /* Delivery actor */
            String cacteurLiv = insertPreparationMoData.getDeliveryActor();
            if (cacteurLiv != null) {
                ret.setXCacteurLiv(cacteurLiv);
            }

            /* Preparation place */
            String lPrepa = insertPreparationMoData.getPreparationPlace();
            if (lPrepa != null) {
                ret.setXLprepa(lPrepa);
            }

            /* Delivery place */
            String lExpe = insertPreparationMoData.getDeliveryPlace();
            if (lExpe != null) {
                ret.setXLexpe(lExpe);
            }

            /* Origin warehouse */
            String cDepotOrig = insertPreparationMoData.getOriginWarehouse();
            if (cDepotOrig != null) {
                ret.setXCdepotorig(cDepotOrig);
            }

            /* Delivery warehouse */
            String cDepotExp = insertPreparationMoData.getDeliveryWarehouse();
            if (cDepotExp != null) {
                ret.setXCdepotexp(cDepotExp);
            }

            /* Preparation mode */
            String cModPrep = insertPreparationMoData.getPreparationMode();
            if (cModPrep != null) {
                ret.setXCmodprep(cModPrep);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertInsertPreparationToTt(InsertPreparationMoDataType=" + insertPreparationMoData
                    + ")");
        }
        return ret;
    }

    /**
     * Convert MOSelectionType to SoamoPPOTtIncriteria .
     * 
     * @param criteria MultiValueCriteriaType
     * @return SoamoPPOTtIncriteria
     */
    public List<SoamoPPOTtIncriteria> convertMOCriteriaToTtCriv(final MultiValueCriteriaType criteria) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertMOCriteriaToTtCriv(MultiValueCriteriaType=" + criteria + ")");
        }

        List<SoamoPPOTtIncriteria> ret = new ArrayList<SoamoPPOTtIncriteria>();

        SoamoPPOTtIncriteria inCriteria = new SoamoPPOTtIncriteria();

        if (criteria != null && criteria.getMultiValueCriterion() != null) {
            for (MultiValueCriterionType criterion : criteria.getMultiValueCriterion()) {
                inCriteria.setCcri(criterion.getCriterionCode());
                inCriteria.setTypcri(criterion.getCriterionType());
                inCriteria.setMsaisie(criterion.getCriterionEntryMode());
                inCriteria.setValmin(criterion.getCriterionMinValue());
                inCriteria.setValmax(criterion.getCriterionMaxValue());
                ret.add(inCriteria);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertMOCriteriaToTtCriv(MultiValueCriteriaType=" + criteria + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert MOSelectionType to SoamoPPOTtmoselection .
     * 
     * @param moSelectionType MOSelectionType
     * @return SoamoPPOTtmoselection
     */
    public SoamoPPOTtMoselection convertSelectionToTt(final MoSelectionType moSelectionType) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertSelectionToTt(moSelectionType=" + moSelectionType + ")");
        }

        SoamoPPOTtMoselection ret = new SoamoPPOTtMoselection();

        if (moSelectionType != null) {

            /* Company - Establishment */
            CompEstabType compEstab = moSelectionType.getCompEstab();
            if (compEstab != null) {
                ret.setCsoc(compEstab.getCompany());
                ret.setCetab(compEstab.getEstablishment());
            }

            /* Uses list */
            UsesListType usesList = moSelectionType.getUsesList();
            if (usesList != null) {
                // transforms a List containing the elements (a, b, c) in a String containing "a,b,c"
                String lstcofuse = "";
                int i = 1;
                for (UseType use : usesList.getUse()) {
                    lstcofuse = lstcofuse.concat(use.value());
                    if (i < usesList.getUse().size()) {
                        lstcofuse = lstcofuse.concat(",");
                    }
                    i++;
                }
                ret.setLstcofuse(lstcofuse);
            }

            /* Item */
            String refItem = moSelectionType.getRefItem();
            if (refItem != null) {
                ret.setCart(refItem);
            }

            /* Chrono */
            ChronoType chrono = moSelectionType.getChrono();
            if (chrono != null) {
                ret.setPrechro(chrono.getPrechro());
                ret.setChrono(chrono.getChrono());
            }

            /* Operating Process */
            String process = moSelectionType.getOperatingProcess();
            if (process != null) {
                ret.setCres(process);
            }

            /* MO Type */
            String ofType = moSelectionType.getMoType();
            if (ofType != null) {
                ret.setTypof(ofType);
            }

            /* Production Date */
            XMLGregorianCalendar productionDate = moSelectionType.getProductionDate();
            if (productionDate != null) {
                GregorianCalendar gc = productionDate.toGregorianCalendar();
                ret.setDatprod(DateHelper.atMidnight(gc.getTime()));
            }

            /* Teams list */
            TeamsListType teamsList = moSelectionType.getTeamsList();
            if (teamsList != null) {
                // transforms a List containing the elements (a, b, c) in a String containing "a,b,c"
                String lstteams = "";
                int i = 1;
                for (String team : teamsList.getTeam()) {
                    lstteams = lstteams.concat(team);
                    if (i < teamsList.getTeam().size()) {
                        lstteams = lstteams.concat(",");
                    }
                    i++;
                }
                ret.setCequip(lstteams);
            }

            /* Sectors list */
            SectorListType sectorsList = moSelectionType.getSectorsList();
            if (sectorsList != null) {
                // transforms a List containing the elements (a, b, c) in a String containing "a,b,c"
                String lstsectors = "";
                int i = 1;
                for (String sector : sectorsList.getSector()) {
                    lstsectors = lstsectors.concat(sector);
                    if (i < sectorsList.getSector().size()) {
                        lstsectors = lstsectors.concat(",");
                    }
                    i++;
                }
                ret.setLstcsecteur(lstsectors);
            }

            /* States list */
            MOStates3ListType statesList = moSelectionType.getMoStatesList();
            if (statesList != null) {
                // transforms a List containing the elements (a, b, c) in a String containing "a,b,c"
                String lststates = "";
                int i = 1;
                for (MOState3Type state : statesList.getMoState3()) {
                    lststates = lststates.concat(state.toString());
                    if (i < statesList.getMoState3().size()) {
                        lststates = lststates.concat(",");
                    }
                    i++;
                }
                ret.setLstcetat(lststates);
            }

        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertSelectionToTt(moSelectionType=" + moSelectionType + ")=" + ret);
        }

        return ret;
    }

    /**
     * Convert SoamoPPOTtDeleteresponse (coming from progress) to DeleteMosResponseType .
     * 
     * @param statusResponseHolder : the Soa moPPOTtDeleteresponse
     * @return DeleteMosResponseType
     */
    public DeleteMosResponseType convertSOADeleteResponseToDeleteResponse(
            final TempTableHolder<SoamoPPOTtDeleteresponse> statusResponseHolder) {
        CompEstabType compEstab = new CompEstabType();
        compEstab.setCompany(statusResponseHolder.getListValue().get(0).getCsoc());
        compEstab.setEstablishment(statusResponseHolder.getListValue().get(0).getCetab());
        ChronoType chrono = new ChronoType();
        chrono.setPrechro(statusResponseHolder.getListValue().get(0).getPrechro());
        chrono.setChrono(statusResponseHolder.getListValue().get(0).getChrono());
        MOCompleteIdType mo = new MOCompleteIdType();
        mo.setCompEstab(compEstab);
        mo.setChrono(chrono);
        DeleteMosResponseType resp = new DeleteMosResponseType();
        resp.setMo(mo);

        return resp;
    }

    /**
     * Convert SoamoPPOTtInsertresponse (coming from progress) to InsertFabricationMoResponseType .
     * 
     * @param statusResponseHolder : the Soa moPPOTtInsertresponse
     * @return InsertFabricationMoResponseType
     */
    public InsertFabricationMoResponseType convertSOAInsertFabricationResponseToInsertFabricationResponse(
            final TempTableHolder<SoamoPPOTtInsertresponse> statusResponseHolder) {
        CompEstabType compEstab = new CompEstabType();
        compEstab.setCompany(statusResponseHolder.getListValue().get(0).getCsoc());
        compEstab.setEstablishment(statusResponseHolder.getListValue().get(0).getCetab());
        ChronoType chrono = new ChronoType();
        chrono.setPrechro(statusResponseHolder.getListValue().get(0).getPrechro());
        chrono.setChrono(statusResponseHolder.getListValue().get(0).getChrono());
        MOCompleteIdType mo = new MOCompleteIdType();
        mo.setCompEstab(compEstab);
        mo.setChrono(chrono);
        InsertFabricationMoResponseType resp = new InsertFabricationMoResponseType();
        resp.setMo(mo);

        return resp;
    }

    /**
     * Convert SoamoPPOTtInsertresponse (coming from progress) to InsertPreparationMoResponseType .
     * 
     * @param statusResponseHolder : the Soa moPPOTtInsertPreparationresponse
     * @return InsertPreparationMoResponseType
     */
    public InsertPreparationMoResponseType convertSOAInsertPreparationResponseToInsertPreparationResponse(
            final TempTableHolder<SoamoPPOTtInsertresponse> statusResponseHolder) {
        CompEstabType compEstab = new CompEstabType();
        compEstab.setCompany(statusResponseHolder.getListValue().get(0).getCsoc());
        compEstab.setEstablishment(statusResponseHolder.getListValue().get(0).getCetab());
        ChronoType chrono = new ChronoType();
        chrono.setPrechro(statusResponseHolder.getListValue().get(0).getPrechro());
        chrono.setChrono(statusResponseHolder.getListValue().get(0).getChrono());
        MOCompleteIdType mo = new MOCompleteIdType();
        mo.setCompEstab(compEstab);
        mo.setChrono(chrono);
        InsertPreparationMoResponseType resp = new InsertPreparationMoResponseType();
        resp.setMo(mo);

        return resp;
    }

    /**
     * Convert SoamoPPOTtReadresponse (coming from progress) to ReadMosResponseType .
     * 
     * @param ttMoresponse : the Soa moPPOTtMoresponse
     * @param ttOutcriteria : the Soa moPPOTtOutcriteria
     * @return ReadMosResponseType
     */
    public ReadMosResponseType convertSOAReadResponseToReadResponse(
            final TempTableHolder<SoamoPPOTtMoresponse> ttMoresponse,
            final TempTableHolder<SoamoPPOTtOutcriteria> ttOutcriteria) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertSOAReadResponseToReadResponse(ttMoresponse=" + ttMoresponse + ", ttOutcriteria="
                    + ttOutcriteria + ")");
        }

        MoResponsesListType moRespList = new MoResponsesListType();

        List<SoamoPPOTtMoresponse> listMoResponse = ttMoresponse.getListValue();
        for (SoamoPPOTtMoresponse readMoResponse : listMoResponse) {
            MoResponseType moResp = new MoResponseType();

            // Comment
            moResp.setComment(readMoResponse.getCcom());

            // Effective dates-hours (begin and end)
            DateHourBoundsType dateHour = new DateHourBoundsType();
            dateHour.setBeginDateHour(getXMLGregorianCalendar(readMoResponse.getDatdebr(), readMoResponse.getHeurdebr()));
            dateHour.setEndDateHour(getXMLGregorianCalendar(readMoResponse.getDatfinr(), readMoResponse.getHeurfinr()));
            moResp.setEffectiveDateHour(dateHour);

            // Effective qty
            QuantityType qty1 = new QuantityType();
            qty1.setQty(readMoResponse.getQter());
            qty1.setUnit(readMoResponse.getCunite());
            moResp.setEffectiveQty(qty1);

            // Fabrication type
            if (readMoResponse.getCiti() != null && !"".equals(readMoResponse.getCiti())) {
                moResp.setFabrication(readMoResponse.getCiti());
            } else {
                moResp.setFabrication(readMoResponse.getCact());
            }

            // Flow type
            moResp.setFlowType(fr.vif.vif5_7.production.mo.FlowType.valueOf(readMoResponse.getTypflux()));

            // Forecast dates-hours (begin and end)
            dateHour.setBeginDateHour(getXMLGregorianCalendar(readMoResponse.getDatdeb(), readMoResponse.getHeurdeb()));
            dateHour.setEndDateHour(getXMLGregorianCalendar(readMoResponse.getDatfin(), readMoResponse.getHeurfin()));
            moResp.setForecastDateHour(dateHour);

            // Forecast qty
            QuantityType qty2 = new QuantityType();
            qty2.setQty(readMoResponse.getQtep());
            qty2.setUnit(readMoResponse.getCunite());
            moResp.setForecastQty(qty2);

            // Stock qty 1
            QuantityType qty3 = new QuantityType();
            qty3.setQty(readMoResponse.getQtestk());
            qty3.setUnit(readMoResponse.getCunite());
            moResp.setForecastStockQty1(qty3);

            // Stock qty 2
            QuantityType qty4 = new QuantityType();
            qty4.setQty(readMoResponse.getQtestk2());
            qty4.setUnit(readMoResponse.getCustk2());
            moResp.setForecastStockQty2(qty4);

            // In batch
            moResp.setInBatch(readMoResponse.getLotent());

            // Labels (short and long)
            LabelsType labels = new LabelsType();
            labels.setShortLabel(readMoResponse.getRof());
            labels.setLongLabel(readMoResponse.getLof());
            moResp.setLabels(labels);

            // Mo id (company, estab., prechro, chrono)
            CompEstabType compEstab = new CompEstabType();
            compEstab.setCompany(readMoResponse.getCsoc());
            compEstab.setEstablishment(readMoResponse.getCetab());
            ChronoType chrono = new ChronoType();
            chrono.setPrechro(readMoResponse.getPrechro());
            chrono.setChrono(readMoResponse.getChrono());
            MOCompleteIdType mo = new MOCompleteIdType();
            mo.setCompEstab(compEstab);
            mo.setChrono(chrono);
            moResp.setMoId(mo);

            // MO State
            moResp.setMoState(readMoResponse.getCetat());

            // MO type
            moResp.setMoType(readMoResponse.getTypof());

            // Operating process
            moResp.setOperatingProcess(readMoResponse.getCres());

            // Origin Mo id (company, estab., prechro, chrono)
            CompEstabType compEstab2 = new CompEstabType();
            compEstab2.setCompany(readMoResponse.getCsocor());
            compEstab2.setEstablishment(readMoResponse.getCetabor());
            ChronoType chrono2 = new ChronoType();
            chrono2.setPrechro(readMoResponse.getPrechror());
            chrono2.setChrono(readMoResponse.getChronor());
            MOCompleteIdType mo2 = new MOCompleteIdType();
            mo2.setCompEstab(compEstab2);
            mo2.setChrono(chrono2);
            moResp.setOrigin(mo2);

            // Out batch
            moResp.setOutBatch(readMoResponse.getLot());

            // Primary resource
            moResp.setPrimaryResource(readMoResponse.getRessprim());

            // Priority
            moResp.setPriority(readMoResponse.getPrior());

            // Production date
            moResp.setProductionDate(getXMLGregorianCalendar(readMoResponse.getDatprod(), 0));

            // Reference item
            moResp.setRefItem(readMoResponse.getCarts());

            // Sectors list
            SectorListType sectorList = new SectorListType();
            String[] sectors = readMoResponse.getLstsect().split(",");
            for (String sector : sectors) {
                sectorList.getSector().add(sector);
            }
            moResp.setSectorsList(sectorList);

            // Team
            moResp.setTeam(readMoResponse.getCequip());

            // Theorical qty
            QuantityType qty5 = new QuantityType();
            qty5.setQty(readMoResponse.getQtet());
            qty5.setUnit(readMoResponse.getCunite());
            moResp.setTheoreticalQty(qty5);

            // Use
            moResp.setUse(UseType.valueOf(readMoResponse.getCofuse()));

            // Criteria
            List<SoamoPPOTtOutcriteria> listCriteria = ttOutcriteria.getListValue();
            MultiValueCriteriaType critList = new MultiValueCriteriaType();
            if (listCriteria != null) {
                for (SoamoPPOTtOutcriteria criterion : listCriteria) {
                    MultiValueCriterionType crit = new MultiValueCriterionType();
                    crit.setCriterionCode(criterion.getCcri());
                    crit.setCriterionType(criterion.getTypcri());
                    crit.setCriterionEntryMode(criterion.getMsaisie());
                    crit.setCriterionMinValue(criterion.getValmin());
                    crit.setCriterionMaxValue(criterion.getValmax());
                    critList.getMultiValueCriterion().add(crit);
                }
            }
            moResp.setMoCriteria(critList);

            // FILLS THE RESPONSE
            moRespList.getMoResponse().add(moResp);
        }

        ReadMosResponseType resp = new ReadMosResponseType();
        resp.setMoResponses(moRespList);

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertSOAReadResponseToReadResponse(ttMoresponse=" + ttMoresponse + ", ttOutcriteria="
                    + ttOutcriteria + ")=" + resp);
        }
        return resp;
    }

    /**
     * Convert Delete StatusResponseHolder to StandardResponseType.
     * 
     * @param statusResponseHolder Delete status response holder
     * @return StandardResponseType
     */
    public StandardResponseType convertStatusDeleteResponseToStandardResponse(
            final TempTableHolder<SoamoPPOTtDeleteresponse> statusResponseHolder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertStatusDeleteResponseToStandardResponse(statusResponseHolder="
                    + statusResponseHolder + ")");
        }

        StandardResponseType ret = new StandardResponseType();

        if (statusResponseHolder != null) {
            List<SoamoPPOTtDeleteresponse> statusResponseList = statusResponseHolder.getListValue();
            if (statusResponseList != null && statusResponseList.size() > 0) {
                SoamoPPOTtDeleteresponse statusResponse = statusResponseList.get(0);
                ret.setMessage(statusResponse.getMsg());
                ret.setStatus(StatusType.fromValue(statusResponse.getCstatus()));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertStatusDeleteResponseToStandardResponse(statusResponseHolder="
                    + statusResponseHolder + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert Insert StatusResponseHolder to StandardResponseType.
     * 
     * @param statusResponseHolder Insert status response holder
     * @return StandardResponseType
     */
    public StandardResponseType convertStatusInsertResponseToStandardResponse(
            final TempTableHolder<SoamoPPOTtInsertresponse> statusResponseHolder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertStatusInsertResponseToStandardResponse(statusResponseHolder="
                    + statusResponseHolder + ")");
        }

        StandardResponseType ret = new StandardResponseType();

        if (statusResponseHolder != null) {
            List<SoamoPPOTtInsertresponse> statusResponseList = statusResponseHolder.getListValue();
            if (statusResponseList != null && statusResponseList.size() > 0) {
                SoamoPPOTtInsertresponse statusResponse = statusResponseList.get(0);
                ret.setMessage(statusResponse.getMsg());
                ret.setStatus(StatusType.fromValue(statusResponse.getCstatus()));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertStatusInsertResponseToStandardResponse(statusResponseHolder="
                    + statusResponseHolder + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert Read StatusResponseHolder to StandardResponseType.
     * 
     * @param statusResponseHolder Read status response holder
     * @return StandardResponseType
     */
    public StandardResponseType convertStatusReadResponseToStandardResponse(
            final TempTableHolder<SoamoPPOTtReadresponse> statusResponseHolder) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertStatusDeleteResponseToStandardResponse(statusResponseHolder="
                    + statusResponseHolder + ")");
        }

        StandardResponseType ret = new StandardResponseType();

        if (statusResponseHolder != null) {
            List<SoamoPPOTtReadresponse> statusResponseList = statusResponseHolder.getListValue();
            if (statusResponseList != null && statusResponseList.size() > 0) {
                SoamoPPOTtReadresponse statusResponse = statusResponseList.get(0);
                ret.setMessage(statusResponse.getMsg());
                ret.setStatus(StatusType.fromValue(statusResponse.getCstatus()));
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertStatusReadResponseToStandardResponse(statusResponseHolder=" + statusResponseHolder
                    + ")=" + ret);
        }
        return ret;
    }

}

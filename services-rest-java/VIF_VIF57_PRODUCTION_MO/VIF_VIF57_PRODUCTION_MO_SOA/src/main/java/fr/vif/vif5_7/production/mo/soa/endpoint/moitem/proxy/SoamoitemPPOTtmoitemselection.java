/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoitemPPOTtmoitemselection.java,v $
 * Created on 24 avr. 2014 by vl
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.moitem.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamoitempxy.p.xml.
 * 
 * @author vl
 */
public class SoamoitemPPOTtmoitemselection extends AbstractTempTable implements Serializable {

    private String cact     = "";

    private String cartprev = "";

    private String cetab    = "";

    private int    chrono   = 0;

    private String cofuse   = "";

    private String csoc     = "";

    private String cuser    = "";

    private Date   datprod  = null;

    private int    ni1      = 0;

    private int    ni2      = 0;

    private int    ni3      = 0;

    private String prechro  = "";

    private String typacta  = "";

    /**
     * Default Constructor.
     * 
     */
    public SoamoitemPPOTtmoitemselection() {
        super();
    }

    /**
     * Gets the cact.
     * 
     * @category getter
     * @return the cact.
     */
    public final String getCact() {
        return cact;
    }

    /**
     * Gets the cartprev.
     * 
     * @category getter
     * @return the cartprev.
     */
    public final String getCartprev() {
        return cartprev;
    }

    /**
     * Gets the cetab.
     * 
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Gets the chrono.
     * 
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Gets the cofuse.
     * 
     * @category getter
     * @return the cofuse.
     */
    public final String getCofuse() {
        return cofuse;
    }

    /**
     * Gets the csoc.
     * 
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Gets the cuser.
     * 
     * @category getter
     * @return the cuser.
     */
    public final String getCuser() {
        return cuser;
    }

    /**
     * Gets the datprod.
     * 
     * @category getter
     * @return the datprod.
     */
    public final Date getDatprod() {
        return datprod;
    }

    /**
     * Gets the ni1.
     * 
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Gets the ni2.
     * 
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Gets the ni3.
     * 
     * @category getter
     * @return the ni3.
     */
    public final int getNi3() {
        return ni3;
    }

    /**
     * Gets the prechro.
     * 
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Gets the typacta.
     * 
     * @category getter
     * @return the typacta.
     */
    public final String getTypacta() {
        return typacta;
    }

    /**
     * Sets the cact.
     * 
     * @category setter
     * @param cact cact.
     */
    public final void setCact(final String cact) {
        this.cact = cact;
    }

    /**
     * Sets the cartprev.
     * 
     * @category setter
     * @param cartprev cartprev.
     */
    public final void setCartprev(final String cartprev) {
        this.cartprev = cartprev;
    }

    /**
     * Sets the cetab.
     * 
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Sets the chrono.
     * 
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Sets the cofuse.
     * 
     * @category setter
     * @param cofuse cofuse.
     */
    public final void setCofuse(final String cofuse) {
        this.cofuse = cofuse;
    }

    /**
     * Sets the csoc.
     * 
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Sets the cuser.
     * 
     * @category setter
     * @param cuser cuser.
     */
    public final void setCuser(final String cuser) {
        this.cuser = cuser;
    }

    /**
     * Sets the datprod.
     * 
     * @category setter
     * @param datprod datprod.
     */
    public final void setDatprod(final Date datprod) {
        this.datprod = datprod;
    }

    /**
     * Sets the ni1.
     * 
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Sets the ni2.
     * 
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Sets the ni3.
     * 
     * @category setter
     * @param ni3 ni3.
     */
    public final void setNi3(final int ni3) {
        this.ni3 = ni3;
    }

    /**
     * Sets the prechro.
     * 
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

    /**
     * Sets the typacta.
     * 
     * @category setter
     * @param typacta typacta.
     */
    public final void setTypacta(final String typacta) {
        this.typacta = typacta;
    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on Jul 19, 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils.error;


/**
 * Error object to send on javascript client.
 * 
 * 
 * @author ac
 */
public class BusinessError {

    private String message = "";
    private String value   = "";

    /**
     * Constructor.
     * 
     * @param value error value.
     * @param message error message.
     */
    public BusinessError(final String value, final String message) {
        super();
        this.message = message;
    }

    /**
     * Gets the message.
     * 
     * @return the message.
     */
    public String getMessage() {
        return message;
    }

    /**
     * Gets the key.
     * 
     * @return the key.
     */
    public String getValue() {
        return value;
    }

}

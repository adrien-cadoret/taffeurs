/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile$
 * Created on 26 juil. 2016 by ac
 */
package fr.vif.vif5_7.production.mo.soa.rest.utils;


import java.util.Date;
import java.util.GregorianCalendar;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.WebServiceException;

import fr.vif.vif5_7.production.mo.ChronoType;
import fr.vif.vif5_7.production.mo.CompEstabType;
import fr.vif.vif5_7.production.mo.InsertMOItemDataType;
import fr.vif.vif5_7.production.mo.ItemMOSingleIdType;
import fr.vif.vif5_7.production.mo.MOItemSelectionType;
import fr.vif.vif5_7.production.mo.MOItemSingleIdType;
import fr.vif.vif5_7.production.mo.NiMOItemIdType;
import fr.vif.vif5_7.production.mo.QuantitiesType;
import fr.vif.vif5_7.production.mo.QuantityType;
import fr.vif.vif5_7.production.mo.UniqueValueCriteriaType;
import fr.vif.vif5_7.production.mo.UseType;
import fr.vif.vif5_7.production.mo.WareLocType;
import fr.vif.vif5_7.production.mo.dao.kota.Kota;
import fr.vif.vif5_7.production.mo.dao.kotad.Kotad;
import fr.vif.vif5_7.production.mo.soa.rest.moitem.KotaDTO;
import fr.vif.vif5_7.production.mo.soa.rest.moitem.KotadDTO;


/**
 * Class which converts objects in other objects.
 *
 * @author ac
 */
public class Converter {

    /**
     * Convert a Date to an XMLGregorianCalendar object.
     * 
     * @param date the date to be converted
     * @return an XMLGregorianCalendar
     */
    public static XMLGregorianCalendar convertDateToXMLGregorianCalendar(final Date date) {
        GregorianCalendar gregory = new GregorianCalendar();
        gregory.setTime(date);
        XMLGregorianCalendar calendar = null;

        try {
            calendar = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregory);
        } catch (DatatypeConfigurationException e) {
            throw new WebServiceException(e);
        }
        return calendar;
    }

    /**
     * Convert a kotad to a kotad DTO.
     * 
     * @param kotad kotad
     * @return dto
     */
    public static KotadDTO convertDetailedMOItemToDTO(final Kotad kotad) {
        KotadDTO dto = new KotadDTO();

        if (kotad != null) {

            dto.setCart(kotad.getCart());
            dto.setCdepot(kotad.getCdepot());
            dto.setCemp(kotad.getCemp());
            dto.setCetab(kotad.getCetab());
            dto.setChrono(kotad.getChrono());
            dto.setCsoc(kotad.getCsoc());
            dto.setChronoct(kotad.getChronoct());
            dto.setCmonnaie(kotad.getCmonnaie());
            dto.setCmotmvk(kotad.getCmotmvk());
            dto.setCnatstk(kotad.getCnatstk());
            dto.setCont(kotad.getCont());
            dto.setQte1(kotad.getQte1());
            dto.setQte2(kotad.getQte2());
            dto.setQte3(kotad.getQte3());
            dto.setCu1(kotad.getCu1());
            dto.setCu2(kotad.getCu2());
            dto.setCu3(kotad.getCu3());
            dto.setCuprix(kotad.getCuprix());
            dto.setCuser(kotad.getCuser());
            dto.setCv(kotad.getCv());
            dto.setDatheurgen(kotad.getDatheurgen());
            dto.setDatheurmvt(kotad.getDatheurmvt());
            dto.setNi1(kotad.getNi1());
            dto.setNi1or(kotad.getNi1or());
            dto.setNi2(kotad.getNi2());
            dto.setNi2or(kotad.getNi2or());
            dto.setLot(kotad.getLot());
            dto.setMt1(kotad.getMt1());
            dto.setNi3(kotad.getNi3());
            dto.setNi3or(kotad.getNi3or());
            dto.setNi4or(kotad.getNi4or());
            dto.setNlig(kotad.getNlig());
            dto.setPrechroct(kotad.getPrechroct());
            dto.setPrix(kotad.getPrix());
            dto.setTare(kotad.getTare());
            dto.setTstock(kotad.getTstock());
            dto.setTypor(kotad.getTypor());
            dto.setPrechro(kotad.getPrechro());
            dto.setValmt(kotad.getValmt());
            dto.setValmt1(kotad.getValmt1());
            dto.setValmt2(kotad.getValmt2());
            dto.setValmt3(kotad.getValmt3());
            dto.setValmt4(kotad.getValmt4());
            dto.setValmt5(kotad.getValmt5());
            dto.setValprix(kotad.getValprix());
            dto.setValprix1(kotad.getValprix1());
            dto.setValprix2(kotad.getValprix2());
            dto.setValprix3(kotad.getValprix3());
            dto.setValprix4(kotad.getValprix4());
            dto.setValprix5(kotad.getValprix5());
            dto.setTypacta(kotad.getTypacta());
        }

        return dto;
    }

    // /**
    // * Convets a comment DTO to a comment.
    // *
    // * @param commentDTO the dto
    // * @return the result
    // */
    // public static Comment convertDTOToComment(final CommentDTO commentDTO) {
    // Comment comment = new Comment();
    //
    // comment.setCom(commentDTO.getCom());
    // comment.setCupdateUser(commentDTO.getCupdateUser());
    // comment.setKey(commentDTO.getKey());
    // comment.setUpdateDate(commentDTO.getUpdateDate());
    //
    // return comment;
    // }

    /**
     * Convert a DTO to an insert mo item data.
     * 
     * @param kotadDto the dto
     * @return the data
     */
    public static InsertMOItemDataType convertDTOToInsertMOItemData(final KotadDTO kotadDto) {
        InsertMOItemDataType data = new InsertMOItemDataType();

        // Company, establishment
        CompEstabType compEstab = new CompEstabType();
        compEstab.setCompany(kotadDto.getCsoc());
        compEstab.setEstablishment(kotadDto.getCetab());
        data.setCompEstab(compEstab);

        // Batch and item
        data.setBatch(kotadDto.getLot());
        data.setEffectiveItem(kotadDto.getCart());

        // Quantities and units
        QuantitiesType quantitiesType = new QuantitiesType();

        QuantityType q1 = new QuantityType();
        QuantityType q2 = new QuantityType();
        QuantityType q3 = new QuantityType();

        q1.setQty(kotadDto.getQte1());
        q1.setUnit(kotadDto.getCu1());
        q2.setQty(kotadDto.getQte2());
        q2.setUnit(kotadDto.getCu2());
        q3.setQty(kotadDto.getQte3());
        q3.setUnit(kotadDto.getCu3());

        quantitiesType.getQuantity().add(q1);
        quantitiesType.getQuantity().add(q2);
        quantitiesType.getQuantity().add(q3);

        data.setEffectiveQties(quantitiesType);

        // Prechro and chrono
        ChronoType chronoType = new ChronoType();
        chronoType.setPrechro(kotadDto.getPrechro());
        chronoType.setChrono(kotadDto.getChrono());
        data.setMoChrono(chronoType);

        // Internal numbers and item type
        MOItemSingleIdType moItemSingleIdType = new MOItemSingleIdType();
        ItemMOSingleIdType itemMOSingleIdType = new ItemMOSingleIdType();
        itemMOSingleIdType.setItemType(kotadDto.getTypacta());

        moItemSingleIdType.setItemMOId(itemMOSingleIdType);

        NiMOItemIdType niMOItemIdType = new NiMOItemIdType();
        niMOItemIdType.setNi1(kotadDto.getNi1());
        niMOItemIdType.setNi2(kotadDto.getNi2());
        moItemSingleIdType.setNiMOItemId(niMOItemIdType);

        data.setMoItemId(moItemSingleIdType);
        data.setBatchCriteria(new UniqueValueCriteriaType());

        // Mvt Dates
        data.setMvtDateTime(convertDateToXMLGregorianCalendar(kotadDto.getDatheurmvt()));

        // Warehouse and location
        WareLocType wareLocType = new WareLocType();
        wareLocType.setLocation(kotadDto.getCemp());
        wareLocType.setWarehouse(kotadDto.getCdepot());
        data.setWareLoc(wareLocType);
        data.setUse(UseType.SST);

        return data;
    }

    // /**
    // * Converts an MOBean to a MO DTO.
    // *
    // * @param mo the bean
    // * @return the dto.
    // */
    // public static MODTO convertMOBeanToDTO(final MOBean mo) {
    // MODTO dto = new MODTO();
    //
    // dto.setChronoOrig(mo.getChronoOrig());
    // dto.setEffectiveBeginDateTime(mo.getEffectiveBeginDateTime());
    // dto.setEffectiveEndDateTime(mo.getEffectiveEndDateTime());
    // dto.setFabrication(mo.getFabrication());
    // dto.setFlowType(mo.getFlowType());
    // dto.setForecastBeginDateTime(mo.getForecastBeginDateTime());
    // dto.setForecastEndDateTime(mo.getForecastEndDateTime());
    // dto.setItemCLs(mo.getItemCLs());
    // dto.setLabel(mo.getLabel());
    // dto.setMoKey(mo.getMoKey());
    // dto.setMoQuantityUnit(mo.getMoQuantityUnit());
    // dto.setMoState(mo.getMoState());
    // dto.setMoType(mo.getMoType());
    // dto.setMoUse(mo.getMoUse());
    // dto.setPrimaryRessource(mo.getPrimaryRessource());
    // dto.setPriority(mo.getPriority());
    // dto.setShortLabel(mo.getShortLabel());
    // dto.setValueCalculatedCrit1(mo.getValueCalculatedCrit1());
    // dto.setValueCalculatedCrit2(mo.getValueCalculatedCrit2());
    // dto.setValueCalculatedCrit3(mo.getValueCalculatedCrit3());
    // dto.setValueCalculatedCrit4(mo.getValueCalculatedCrit4());
    // dto.setValueCalculatedCrit5(mo.getValueCalculatedCrit5());
    // dto.setValueCrit1(mo.getValueCrit1());
    // dto.setValueCrit2(mo.getValueCrit2());
    // dto.setValueCrit3(mo.getValueCrit3());
    // dto.setValueCrit4(mo.getValueCrit4());
    // dto.setValueCrit5(mo.getValueCrit5());
    // dto.setSubcontractor(mo.getSubcontractor());
    //
    // return dto;
    // }

    /**
     * Convert a kota plc to a dto.
     * 
     * @param kota the plc
     * @return a dto
     */
    public static KotaDTO convertMOItemToDTO(final Kota kota) {
        KotaDTO dto = new KotaDTO();

        dto.setCart(kota.getCart());
        dto.setCavanc(kota.getCavanc());
        dto.setCdepot(kota.getCdepot());
        dto.setCemp(kota.getCemp());
        dto.setCetab(kota.getCetab());
        dto.setCformu(kota.getCformu());
        dto.setChrono(kota.getChrono());
        dto.setClenrj(kota.getClenrj());
        dto.setCoefcout(kota.getCoefcout());
        dto.setCprop(kota.getCprop());
        dto.setCrges(kota.getCrges());
        dto.setCsoc(kota.getCsoc());
        dto.setCu2(kota.getCu2());
        dto.setCu2theo(kota.getCu2theo());
        dto.setCunite(kota.getCunite());
        dto.setCv(kota.getCv());
        dto.setDatheurmvt(kota.getDatheurmvt());
        dto.setLotq(kota.getLotq());
        dto.setLotq2(kota.getLotq2());
        dto.setLotqi(kota.getLotqi());
        dto.setLotsor(kota.getLotsor());
        dto.setNi1(kota.getNi1());
        dto.setNi1acta(kota.getNi1acta());
        dto.setNi1or(kota.getNi1or());
        dto.setNi2(kota.getNi2());
        dto.setNi2opinf(kota.getNi2opinf());
        dto.setNi2or(kota.getNi2or());
        dto.setOrig(kota.getOrig());
        dto.setPrechro(kota.getPrechro());
        dto.setQ2theo(kota.getQ2theo());
        dto.setQte(kota.getQte());
        dto.setQte1(kota.getQte1());
        dto.setQte2(kota.getQte2());
        dto.setQte3(kota.getQte3());
        dto.setQtedec(kota.getQtedec());
        dto.setQtefr(kota.getQtefr());
        dto.setQtefr1(kota.getQtefr1());
        dto.setQtefr2(kota.getQtefr2());
        dto.setQtefr3(kota.getQtefr3());
        dto.setQtestk(kota.getQtestk());
        dto.setTauto(kota.getTauto());
        dto.setTctnul(kota.getTctnul());
        dto.setTeprod(kota.getTeprod());
        dto.setToartgen(kota.getToartgen());
        dto.setTqconst(kota.getTqconst());
        dto.setTqtedif(kota.getTqtedif());
        dto.setTrdtdyn(kota.getTrdtdyn());
        dto.setTrdtm(kota.getTrdtm());
        dto.setTxfr(kota.getTxfr());
        dto.setTypacta(kota.getTypacta());
        dto.setTypdecatl(kota.getTypdecatl());

        return dto;
    }

    /**
     * Create an {@link MOItemSelectionType} with several parameters.
     * 
     * @param company company
     * @param establishment establishment
     * @param prechro prechro
     * @param chrono chrono
     * @param cofuse cofuse
     * @param ni1 ni1
     * @param ni2 ni2
     * @param ni3 ni3
     * @return the {@link MOItemSelectionType}
     */
    public static MOItemSelectionType createMOItemSelectionType(final String company, final String establishment,
            final String prechro, final int chrono, final UseType cofuse, final int ni1, final int ni2, final int ni3) {

        MOItemSelectionType moItemSelectionType = new MOItemSelectionType();

        // Company & Establishment
        CompEstabType compEstab = new CompEstabType();
        compEstab.setCompany(company);
        compEstab.setEstablishment(establishment);
        moItemSelectionType.setCompEstab(compEstab);

        // Prechro & Chrono
        ChronoType chronoType = new ChronoType();
        chronoType.setPrechro(prechro);
        chronoType.setChrono(chrono);
        moItemSelectionType.setMoChrono(chronoType);

        moItemSelectionType.setNi1(ni1);
        moItemSelectionType.setNi2(ni2);
        moItemSelectionType.setNi3(ni3);

        moItemSelectionType.setUse(cofuse);

        return moItemSelectionType;
    }

}

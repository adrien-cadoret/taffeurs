/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SOA
 * File : $RCSfile: SoamoPPOTtMoselection.java,v $
 * Created on 19 nov. 2014 by nle
 * Revision: $Revision: 1.3 $ $Date: 2014/12/29 10:23:19 $ $Author: nle $
 */
package fr.vif.vif5_7.production.mo.soa.endpoint.mo.proxy;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/soamopxy.p.xml.
 * 
 * @author nle
 * @version $Revision: 1.3 $, $Date: 2014/12/29 10:23:19 $
 */
public class SoamoPPOTtMoselection extends AbstractTempTable implements Serializable {


    private String cart = "";

    private String cequip = "";

    private String cetab = "";

    private int chrono = 0;

    private String cres = "";

    private String csoc = "";

    private Date datprod = null;

    private String lstcetat = "";

    private String lstcofuse = "";

    private String lstcsecteur = "";

    private String prechro = "";

    private String typof = "";


    /**
     * Default Constructor.
     *
     */
    public SoamoPPOTtMoselection() {
        super();
    }

    /**
     * Gets the cart.
     *
     * @category getter
     * @return the cart.
     */
    public final String getCart() {
        return cart;
    }

    /**
     * Sets the cart.
     *
     * @category setter
     * @param cart cart.
     */
    public final void setCart(final String cart) {
        this.cart = cart;
    }

    /**
     * Gets the cequip.
     *
     * @category getter
     * @return the cequip.
     */
    public final String getCequip() {
        return cequip;
    }

    /**
     * Sets the cequip.
     *
     * @category setter
     * @param cequip cequip.
     */
    public final void setCequip(final String cequip) {
        this.cequip = cequip;
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the chrono.
     *
     * @category getter
     * @return the chrono.
     */
    public final int getChrono() {
        return chrono;
    }

    /**
     * Sets the chrono.
     *
     * @category setter
     * @param chrono chrono.
     */
    public final void setChrono(final int chrono) {
        this.chrono = chrono;
    }

    /**
     * Gets the cres.
     *
     * @category getter
     * @return the cres.
     */
    public final String getCres() {
        return cres;
    }

    /**
     * Sets the cres.
     *
     * @category setter
     * @param cres cres.
     */
    public final void setCres(final String cres) {
        this.cres = cres;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the datprod.
     *
     * @category getter
     * @return the datprod.
     */
    public final Date getDatprod() {
        return datprod;
    }

    /**
     * Sets the datprod.
     *
     * @category setter
     * @param datprod datprod.
     */
    public final void setDatprod(final Date datprod) {
        this.datprod = datprod;
    }

    /**
     * Gets the lstcetat.
     *
     * @category getter
     * @return the lstcetat.
     */
    public final String getLstcetat() {
        return lstcetat;
    }

    /**
     * Sets the lstcetat.
     *
     * @category setter
     * @param lstcetat lstcetat.
     */
    public final void setLstcetat(final String lstcetat) {
        this.lstcetat = lstcetat;
    }

    /**
     * Gets the lstcofuse.
     *
     * @category getter
     * @return the lstcofuse.
     */
    public final String getLstcofuse() {
        return lstcofuse;
    }

    /**
     * Sets the lstcofuse.
     *
     * @category setter
     * @param lstcofuse lstcofuse.
     */
    public final void setLstcofuse(final String lstcofuse) {
        this.lstcofuse = lstcofuse;
    }

    /**
     * Gets the lstcsecteur.
     *
     * @category getter
     * @return the lstcsecteur.
     */
    public final String getLstcsecteur() {
        return lstcsecteur;
    }

    /**
     * Sets the lstcsecteur.
     *
     * @category setter
     * @param lstcsecteur lstcsecteur.
     */
    public final void setLstcsecteur(final String lstcsecteur) {
        this.lstcsecteur = lstcsecteur;
    }

    /**
     * Gets the prechro.
     *
     * @category getter
     * @return the prechro.
     */
    public final String getPrechro() {
        return prechro;
    }

    /**
     * Sets the prechro.
     *
     * @category setter
     * @param prechro prechro.
     */
    public final void setPrechro(final String prechro) {
        this.prechro = prechro;
    }

    /**
     * Gets the typof.
     *
     * @category getter
     * @return the typof.
     */
    public final String getTypof() {
        return typof;
    }

    /**
     * Sets the typof.
     *
     * @category setter
     * @param typof typof.
     */
    public final void setTypof(final String typof) {
        this.typof = typof;
    }

}

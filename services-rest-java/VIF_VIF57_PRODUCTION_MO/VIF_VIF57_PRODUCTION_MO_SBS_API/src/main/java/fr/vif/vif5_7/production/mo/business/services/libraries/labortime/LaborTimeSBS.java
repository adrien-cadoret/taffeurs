/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: LaborTimeSBS.java,v $
 * Created on 11 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.labortime;


import java.util.Date;
import java.util.List;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTime;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;


/**
 * the KTPSX SBS.
 * 
 * @author nle
 */
public interface LaborTimeSBS {

    /**
     * After a closed period, opens a new one at the current date-hour and with the staff that were there before the
     * period was closed.
     * 
     * @param ctx : the context
     * @param ltk : the labor time key
     * @throws ServerBusinessException if error occurs
     */
    public void backToWork(final VIFContext ctx, final LaborTimeKey ltk) throws ServerBusinessException;

    /**
     * Deletes an existing KTPSX.
     * 
     * @param ctx VIF Context
     * @param ktpsx the KTPSX
     * @throws ServerBusinessException if error occurs
     */
    public void deleteLaborTime(final VIFContext ctx, final LaborTime ktpsx) throws ServerBusinessException;

    /**
     * Closes an open period.
     * 
     * @param ctx : the context
     * @param ltk : the labor time key
     * @throws ServerBusinessException if error occurs
     */
    public void endOfWork(final VIFContext ctx, final LaborTimeKey ltk) throws ServerBusinessException;

    /**
     * Gets a KTPSX bean.
     * 
     * @param ctx VIF Context
     * @param ktpsxKey the KTPSX key
     * @return the KTPSX bean requested
     * @throws ServerBusinessException if error occurs
     */
    public LaborTime getLaborTime(final VIFContext ctx, final LaborTimeKey ktpsxKey) throws ServerBusinessException;

    /**
     * {@inheritDoc}
     */
    public String getResourceLabel(final VIFContext ctx, final EstablishmentKey estKey, final String resourceCode)
            throws ServerBusinessException;

    /**
     * Inserts a new KTPSX.
     * 
     * @param ctx VIF Context
     * @param ktpsx the KTPSX
     * @return the LaborTime inserted
     * @throws ServerBusinessException if error occurs
     */
    public LaborTime insertLaborTime(final VIFContext ctx, final LaborTime ktpsx) throws ServerBusinessException;

    /**
     * Inserts a new KTPSX at the end of the list.
     * 
     * @param ctx VIF Context
     * @param ktpsx the KTPSX
     * @return the ni1 of the LaborTime inserted
     * @throws ServerBusinessException if error occurs
     */
    public int insertLastLaborTime(final VIFContext ctx, final LaborTime ktpsx) throws ServerBusinessException;

    /**
     * Lists all the KTPSX corresponding to the selection.
     * 
     * @param ctx VIF context
     * @param laborTimeKey the KTPSX key
     * @return a list of KTPSX beans
     * @throws ServerBusinessException if error occurs
     * 
     */
    public List<LaborTime> listLaborTime(final VIFContext ctx, final LaborTimeKey laborTimeKey)
            throws ServerBusinessException;

    /**
     * Lists all the KTPSX corresponding to the selection.
     * 
     * @param ctx VIF context
     * @param laborTimeKey the KTPSX key
     * @return a list of KTPSX beans
     * @throws ServerBusinessException if error occurs
     * 
     */
    public List<LaborTime> listLaborTimeMod(final VIFContext ctx, final LaborTimeKey laborTimeKey)
            throws ServerBusinessException;

    /**
     * After a closed period, opens a new one at the current date-hour and with the staff that were there before the
     * period was closed. It's done for the production line given, or, if not, the one of the first MO of the list (NOT
     * ALL OF THEM FOR NOW).
     * 
     * @param ctx : the context
     * @param estKey : the establishment key
     * @param chronoList : the list of MO prechros-chronos
     * @param productionDate : the production date
     * @param productionLine : the production line
     * @param eventDateTime : the date and hour where the event occurs
     * @throws ServerBusinessException if error occurs
     */
    public void srvBackToWork(final VIFContext ctx, final EstablishmentKey estKey, final List<Chrono> chronoList,
            final Date productionDate, final String productionLine, final Date eventDateTime)
            throws ServerBusinessException;

    /**
     * Closes an open period for the production line given, or, if not, the one of the first MO of the list (NOT ALL OF
     * THEM FOR NOW).
     * 
     * @param ctx : the context
     * @param estKey : the establishment key
     * @param chronoList : the list of MO prechros-chronos
     * @param productionDate : the production date
     * @param productionLine : the production line
     * @param eventDateTime : the date and hour where the event occurs
     * @throws ServerBusinessException if error occurs
     */
    public void srvEndOfWork(final VIFContext ctx, final EstablishmentKey estKey, final List<Chrono> chronoList,
            final Date productionDate, final String productionLine, final Date eventDateTime)
            throws ServerBusinessException;

    /**
     * Opens the production line of a MO.
     * 
     * @param ctx : the context
     * @param estKey : the establishment key
     * @param chrono : the MO prechro-chrono
     * @param productionDate : the production date
     * @param productionLine : the production line
     * @param eventDateTime : the date and hour where the event occurs
     */
    public void srvMOBegins(final VIFContext ctx, final EstablishmentKey estKey, final Chrono chrono,
            final Date productionDate, final String productionLine, final Date eventDateTime);

    /**
     * Closes the production line of a MO.
     * 
     * @param ctx : the context
     * @param estKey : the establishment key
     * @param chrono : the MO prechro-chrono
     * @param productionDate : the production date
     * @param productionLine : the production line
     * @param eventDateTime : the date and hour where the event occurs
     */
    public void srvMOCloses(final VIFContext ctx, final EstablishmentKey estKey, final Chrono chrono,
            final Date productionDate, final String productionLine, final Date eventDateTime);

    /**
     * Updates an existing KTPSX.
     * 
     * @param ctx VIF Context
     * @param ktpsx the KTPSX
     * @return the KTPSX bean updated
     * @throws ServerBusinessException if error occurs
     */
    public LaborTime updateLaborTime(final VIFContext ctx, final LaborTime ktpsx) throws ServerBusinessException;

    /**
     * Updates the ni1.
     * 
     * @param ctx the context
     * @param laborTime the labor time
     * @param newNi1 the new ni1
     * @return the number of ktpsx updated
     * @throws ServerBusinessException if error occurs
     */
    public int updateLaborTimeNi1(final VIFContext ctx, final LaborTime laborTime, final int newNi1)
            throws ServerBusinessException;
}

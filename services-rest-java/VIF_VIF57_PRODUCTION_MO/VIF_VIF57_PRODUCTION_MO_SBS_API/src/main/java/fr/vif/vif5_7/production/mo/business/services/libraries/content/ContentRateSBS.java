/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: ContentRateSBS.java,v $
 * Created on 16 Jul 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.content;


import java.util.List;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.gen.location.business.beans.common.location.LocationKey;
import fr.vif.vif5_7.production.mo.business.beans.common.content.ContentRateBatchBean;
import fr.vif.vif5_7.production.mo.business.beans.common.content.ContentRatePOHeader;


/**
 * Interface for Content SBS.
 * 
 * @author cj
 */
public interface ContentRateSBS {

    /**
     * Gets the po informations.
     * 
     * @param ctx the ctx
     * @param initialBean the initial bean
     * @return the ContentRatePOHeader bean
     * @throws ServerBusinessException if error occurs
     */
    public ContentRatePOHeader getContentRatePOHeader(final VIFContext ctx, final ContentRatePOHeader initialBean)
            throws ServerBusinessException;

    /**
     * List batch from po informations.
     * 
     * @param ctx the ctx
     * @param sBean the selection bean
     * @param user the current user
     * @param workstation the workstation
     * @param location the location (warehouse and place)
     * @return the list of ContentRateBatchBean
     * @throws ServerBusinessException if error occurs
     */
    public List<ContentRateBatchBean> listBatch(final VIFContext ctx, final ContentRatePOHeader sBean,
            final String user, final String workstation, final LocationKey location) throws ServerBusinessException;

    /**
     * List updated batch after calcul.
     * 
     * @param ctx the ctx
     * @param sBean the selection bean
     * @param listBean the list bean to update
     * @return the list of ContentRateBatchBean
     * @throws ServerBusinessException if error occurs
     */
    public List<ContentRateBatchBean> listUpdatedBatch(final VIFContext ctx, final ContentRatePOHeader sBean,
            final List<ContentRateBatchBean> listBean) throws ServerBusinessException;
}

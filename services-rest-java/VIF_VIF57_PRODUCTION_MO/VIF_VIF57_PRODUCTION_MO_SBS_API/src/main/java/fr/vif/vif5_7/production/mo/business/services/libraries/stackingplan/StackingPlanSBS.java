/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: StackingPlanSBS.java,v $
 * Created on 17 Apr. 2013 by xg
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.stackingplan;


import java.util.List;
import java.util.Map;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.keys.ContainerKey;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.StackingPlanInfoBean;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;


/**
 * Libraries of methods about Stacking plan.
 * 
 * @author xg
 */
public interface StackingPlanSBS {

    /**
     * Gets the first stacking plan info. Return the first stacking plan bean where we have a place to put the qty.
     * 
     * @param ctx the context
     * @param moKey the mo key
     * @param stackingPlanCode the stacking plan id
     * @param itemId the item id
     * @param qty the qty to insert
     * @return the stacking plan info
     * @throws ServerBusinessException if errors occur
     */
    public StackingPlanInfoBean getFirstStackingPlanInfo(final VIFContext ctx, final MOKey moKey,
            final String stackingPlanCode, final String itemId, final double qty) throws ServerBusinessException;

    /**
     * Gets the inner from the item and mo.
     * 
     * @param ctx the context
     * @param moKey the mokey
     * @param stackingPlanCode the stacking plan id
     * @param itemId the item id
     * @return the string corresponding to the stacking plan code
     * @throws ServerBusinessException if errors occur
     * @deprecated use getFirstStackingPlanInfo(...) instead
     */
    @Deprecated
    public QtyUnit getInnerMOStackingPlan(final VIFContext ctx, final MOKey moKey, final String stackingPlanCode,
            final String itemId) throws ServerBusinessException;

    /**
     * Return the logical number from a container.
     * 
     * @param ctx the context
     * @param establishmentKey the establishment key
     * @param nsc the container id
     * @return the logical number
     * @throws ServerBusinessException if error occurs
     */
    public int getLogicalNumberFromNSC(final VIFContext ctx, final EstablishmentKey establishmentKey, final String nsc)
            throws ServerBusinessException;

    /**
     * Gets the stacking plan codelabel.
     * 
     * @param ctx the context
     * @param moKey the mokey
     * @return the string corresponding to the stacking plan code
     * @throws ServerBusinessException if errors occurs
     */
    public CodeLabel getMOStackingPlanCode(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException;

    /**
     * Return the packaging from a container.
     * 
     * @param ctx the context
     * @param establishmentKey the establishment key
     * @param nsc the container id
     * @return the packaging id
     * @throws ServerBusinessException if error occurs
     */
    public String getPackagingFromNSC(final VIFContext ctx, final EstablishmentKey establishmentKey, final String nsc)
            throws ServerBusinessException;

    /**
     * Gets the packaging id from the stacking plan.
     * 
     * @param ctx the context
     * @param moKey the mokey
     * @param itemId the item id
     * @param qty the qty to insert (or just inserted)
     * @return the packaging information
     * @throws ServerBusinessException if errors occur
     */
    public CEntityBean getPackagingId(final VIFContext ctx, final MOKey moKey, final String itemId, final double qty)
            throws ServerBusinessException;

    /**
     * Is the stacking plan for the current container full ? <br/>
     * There is a matter with the database cache. The just inserted item is not in the stacking plan.
     * 
     * @param ctx the context
     * @param containerKey the container key with the current NSC
     * @param itemCL the item to insert (or just insert)
     * @param qtyToInsert the qty to insert (first qty with the same unit in the stacking plan)
     * @return true, if the current container is full
     * @throws ServerBusinessException if error occurs
     */
    public boolean isCurrentContainerFull(final VIFContext ctx, final ContainerKey containerKey,
            final CodeLabels itemCL, final QtyUnit qtyToInsert) throws ServerBusinessException;

    /**
     * Is the stacking plan for the current container full ?.
     * 
     * @param context the context
     * @param containerKey the container key with the current NSC
     * @param qtyNeeded the quantity needed
     * @param moKey moKey
     * @param cart cart
     * @return true, if the current container has at least one place wih this qtyNeeded available
     * @throws ServerBusinessException if error occurs
     */
    public boolean isThereContainerWithThisQty(VIFContext context, ContainerKey containerKey, QtyUnit qtyNeeded,
            MOKey moKey, String cart) throws ServerBusinessException;

    /**
     * Return the containers list of a stacking plan.
     * 
     * @param ctx the context
     * @param moKey the mokey
     * @param itemId the item code
     * @param notShowEmpty if container are empty, should we return the container number ?
     * @return list with container number
     * @throws ServerBusinessException if error occurs
     */
    public List<String> listContainerNumber(final VIFContext ctx, final MOKey moKey, final String itemId,
            final boolean notShowEmpty) throws ServerBusinessException;

    /**
     * List the stacking plan info for all items from the mo.
     * 
     * @param ctx the context
     * @param moKey the mokey
     * @param stackingPlanCode the stacking plan id
     * @return the StackingPlanInfoBean corresponding to the stacking plan code
     * @throws ServerBusinessException if errors occur
     */
    public Map<String, StackingPlanInfoBean> listInfoBeanFromMOStackingPlan(final VIFContext ctx, final MOKey moKey,
            final String stackingPlanCode) throws ServerBusinessException;

    /**
     * List the inner for all items from the mo.
     * 
     * @param ctx the context
     * @param moKey the mokey
     * @param stackingPlanCode the stakcing plan id
     * @return the string corresponding to the stacking plan code
     * @throws ServerBusinessException if errors occur
     * @deprecated use listInfoBeanFromMOStackingPlan(...) instead
     */
    @Deprecated
    public Map<String, QtyUnit> listInnerMOStackingPlan(final VIFContext ctx, final MOKey moKey,
            final String stackingPlanCode) throws ServerBusinessException;

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: POItemSBS.java,v $
 * Created on 20 Aug 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.poitem;


import java.util.Date;
import java.util.Map;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;


/**
 * SBS interface for PO Item SBS.
 * 
 * @author cj
 */
public interface POItemSBS {

    /**
     * Return the progress for each item from the PO.
     * 
     * @param ctx VIF Context.
     * @param poKey the po key.
     * @param type input or output item
     * @return a amp with all items and there first and second qty.
     * @throws ServerBusinessException if error occurs
     */
    public Map<OperationItemKey, OperationItemQuantityUnit[]> getPOItemListQuantityFromMo(VIFContext ctx, POKey poKey,
            ActivityItemType type) throws ServerBusinessException;

    /**
     * Gets the PO item 2 quantity.
     * 
     * @param ctx VIF Context.
     * @param itemKey the full item key.
     * @return the second OperationItemQuantityUnit
     * @throws ServerBusinessException if errors occur
     */
    public OperationItemQuantityUnit getPOItemQuantity2(final VIFContext ctx, final OperationItemKey itemKey)
            throws ServerBusinessException;

    /**
     * Gets the PO item quantities.
     * 
     * @param ctx VIF Context.
     * @param poKey the po key.
     * @param type input or output item
     * @return the OperationItemQuantityUnit 1 and 2
     * @throws ServerBusinessException if errors occur
     */
    public OperationItemQuantityUnit[] getPOItemQuantityFromMo(final VIFContext ctx, final POKey poKey,
            final ActivityItemType type) throws ServerBusinessException;

    /**
     * Update actual time of the PO item.
     * 
     * @param ctx VIF Context.
     * @param sCsoc the company code.
     * @param sCetab the establishment code
     * @param sRessourceId the ressource Id
     * @param iChrono the chrono number
     * @param sCarts the item code to process
     * @param dActualDate the date to update
     * @param sEvent the name of the event (start or stop)
     * @throws ServerBusinessException if errors occur
     */
    public void updatePOActualTime(final VIFContext ctx, final String sCsoc, final String sCetab,
            final String sRessourceId, final int iChrono, final String sCarts, final Date dActualDate,
            final String sEvent) throws ServerBusinessException;
}

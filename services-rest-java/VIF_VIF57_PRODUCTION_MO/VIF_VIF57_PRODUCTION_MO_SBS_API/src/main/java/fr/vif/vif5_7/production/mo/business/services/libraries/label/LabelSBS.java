/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: LabelSBS.java,v $
 * Created on 18 nov. 08 by gv
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.label;


import java.util.List;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.SearchLabel;
import fr.vif.vif5_7.production.mo.business.beans.common.labels.LabelEvent;


/**
 * The label SBS class.
 * 
 * @author gv
 */
public interface LabelSBS {

    /**
     * run the print label procedure.
     * 
     * @param ctx the vif ctx
     * @param labelEvent the label event source
     * @param searchLabel thge search Label response
     * @param copiesNumber the number of labels to print
     * @throws ServerBusinessException e
     */
    public void runPrintLabel(final VIFContext ctx, LabelEvent labelEvent, SearchLabel searchLabel, int copiesNumber)
            throws ServerBusinessException;

    /**
     * 
     * search the print labels associated to the event.
     * 
     * @param ctx the vif ctx
     * @param labelEvent the label event
     * @throws ServerBusinessException e
     * @return the list of responses to the event
     */
    public List<SearchLabel> searchPrintLabels(final VIFContext ctx, final LabelEvent labelEvent)
            throws ServerBusinessException;
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: MarkingSBS.java,v $
 * Created on 16 Jul 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.marking;


import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.production.mo.business.beans.common.marking.MarkingBean;


/**
 * Interface for Marking SBS.
 * 
 * @author mnt
 */
public interface MarkingSBS {

    /**
     * Start the printing for a documentId.
     * 
     * @param ctx The vifCtx.
     * @param markingbean The marking bean
     * @throws ServerBusinessException if error occurs.
     */
    public void startPrinting(final VIFContext ctx, final MarkingBean markingbean) throws ServerBusinessException;

    /**
     * Stop the printing.
     * 
     * @param ctx The vifCtx.
     * @param markingbean The marking bean
     * @throws ServerBusinessException if error occurs.
     */
    public void stopPrinting(final VIFContext ctx, final MarkingBean markingbean) throws ServerBusinessException;

}

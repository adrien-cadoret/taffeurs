/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: IndicatorMOSBS.java,v $
 * Created on 22 nov. 07 by glc
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.indicators;


import java.util.List;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOSelectionBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;


/**
 * MO business services interface.
 * 
 * @author glc
 */
public interface IndicatorMOSBS {

    /**
     * Get an indicator by selection.
     * 
     * @param ctx context
     * @param poKey key of the process order to get
     * @return a list of indicator
     * @throws ServerBusinessException if error occurs.
     */
    public IndicatorMO getIndicatorMO(VIFContext ctx, POKey poKey) throws ServerBusinessException;

    /**
     * List the indicators by selection.
     * 
     * @param ctx context
     * @param selection a selection
     * @return a list of indicator
     * @throws ServerBusinessException if error occurs.
     */
    public List<IndicatorMO> listBySelection(VIFContext ctx, IndicatorMOSelectionBean selection)
            throws ServerBusinessException;

}

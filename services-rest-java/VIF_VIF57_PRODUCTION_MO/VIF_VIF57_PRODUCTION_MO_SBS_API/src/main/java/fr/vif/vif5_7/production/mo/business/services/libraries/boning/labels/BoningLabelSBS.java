/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: BoningLabelSBS.java,v $
 * Created on 13 Jun 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.boning.labels;


import java.util.List;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.boning.labels.LabelBean;


/**
 * SBS interface for label for boning features.
 * 
 * @author cj
 */
public interface BoningLabelSBS {

    /**
     * List all label from the current workstation.
     * 
     * @param ctx the ctx
     * @param establishmentKey the establishment key
     * @param workstationId the workstation id
     * @return the list of code label
     * @throws ServerBusinessException if error occurs
     */
    public List<LabelBean> listLabelsByWorkstation(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String workstationId) throws ServerBusinessException;
}

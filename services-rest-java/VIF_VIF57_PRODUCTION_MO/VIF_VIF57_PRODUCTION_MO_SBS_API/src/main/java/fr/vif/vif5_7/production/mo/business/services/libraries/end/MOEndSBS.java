/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: MOEndSBS.java,v $
 * Created on 26 déc. 07 by ped
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.end;


import java.util.List;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.constants.Mnemos.SplitControl;


/**
 * 
 * Business services interface for end operations on manufacturing order.
 * 
 * @author ped
 */
public interface MOEndSBS {

    /**
     * tests the split between to do and done.
     * 
     * @param ctx current context
     * @param operationItemKey the key of the operation
     * @param workstationId the ws
     * @param qty1 the first quantity of the new movement if the movement is not saved, null elsewere
     * @param qty2 the second quantity of the new movement if the movement is not saved, null elsewere
     * @param splitControlToDo what is the control to do ?
     * @param newMovement is there a new movement ?
     * @return the message if there a question
     * @throws ServerBusinessException if the control is fatal
     */
    public String checkSplit(final VIFContext ctx, final OperationItemKey operationItemKey, final String workstationId,
            QuantityUnit qty1, QuantityUnit qty2, SplitControl splitControlToDo, boolean newMovement)
                    throws ServerBusinessException;

    /**
     * 
     * Finishes all operation item.
     * 
     * @param ctx current context
     * @param listOperationItemKey the list of operation item key to finish
     * @param workstationId workstation which finishes the operation item
     * @param activityItemType activity item type
     * @throws ServerBusinessException if error occurs
     * @return an information message
     */
    public String finishAllOperationItem(final VIFContext ctx, final List<OperationItemKey> listOperationItemKey,
            final String workstationId, final ActivityItemType activityItemType) throws ServerBusinessException;

    /**
     * Finishes the marking for the MO.
     * 
     * @param ctx current context
     * @param establishmentKey the establishment key
     * @param workstation current workstation
     * @param userId current user
     * @param moChrono the MO chrono
     * @throws ServerBusinessException if error occurs.
     */
    public void finishMarking(VIFContext ctx, EstablishmentKey establishmentKey, String workstation, String userId,
            Chrono moChrono) throws ServerBusinessException;

    /**
     * 
     * Finishes an operation item.
     * 
     * @param ctx current context
     * @param operationItemKey the key of the operation item to finish
     * @param workstationId workstation which finishes the operation item
     * @param activityItemType activity item type
     * @throws ServerBusinessException if error occurs
     * @return an information message
     */
    public String finishOperationItem(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId, final ActivityItemType activityItemType) throws ServerBusinessException;

    /**
     * 
     * Finishes an Origin MO.
     * 
     * @param ctx current context
     * @param moBean the MO key
     * @throws ServerBusinessException if error occurs
     * @return an information message
     */
    public String finishOriginMO(final VIFContext ctx, final MOBean moBean) throws ServerBusinessException;

    /**
     * Returns true if all the items coresponding to the activityItemType and the workstation for the given MO are
     * finished.
     * 
     * @param ctx the context
     * @param key the MO key
     * @param activityItemType the fabrication item type (input, output, both)
     * @param workstationId the id of the workstation
     * @return the boolean
     * @throws ServerBusinessException if error occurs
     */
    public boolean isFinishedMO(VIFContext ctx, MOKey key, ActivityItemType activityItemType, String workstationId)
            throws ServerBusinessException;

    /**
     * 
     * Resumes an operation item.
     * 
     * @param ctx current context
     * @param operationItemKey the key of the operation item to resume
     * @param workstationId workstation which resumes the operation item
     * @throws ServerBusinessException if error occurs
     */
    public void resumeOperationItem(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId) throws ServerBusinessException;

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: LaborStaffSBS.java,v $
 * Created on 11 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.labortime;


import java.util.Date;
import java.util.List;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborStaff;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborStaffKey;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;


/**
 * the Labor Staff SBS.
 * 
 * @author nle
 */
public interface LaborStaffSBS {
    /**
     * 
     * Breakdowns the time on a resource for different labors.
     * 
     * @param ctx VIF Context
     * @param establishmentKey the establishment key
     * @param proddate the production date
     * @param resourceId the resource id
     * @param laborId list of labor id
     * @param creason the reason id
     * @return the chrono
     * @throws ServerBusinessException if error occurs
     */
    public Chrono breakdownTime(final VIFContext ctx, final EstablishmentKey establishmentKey, final Date proddate,
            final String resourceId, final List<String> laborId, final String creason) throws ServerBusinessException;

    /**
     * Deletes an existing Labor Staff.
     * 
     * @param ctx VIF Context
     * @param laborStaff the Labor Staff
     * @throws ServerBusinessException if error occurs
     */
    public void deleteLaborStaff(final VIFContext ctx, final LaborStaff laborStaff) throws ServerBusinessException;

    /**
     * Gets a Labor Staff bean.
     * 
     * @param ctx VIF Context
     * @param laborStaffKey the Labor Staff key
     * @return the Labor Staff bean requested
     * @throws ServerBusinessException if error occurs
     */
    public LaborStaff getLaborStaff(final VIFContext ctx, final LaborStaffKey laborStaffKey)
            throws ServerBusinessException;

    /**
     * Inserts a new Labor Staff.
     * 
     * @param ctx VIF Context
     * @param laborStaff the Labor Staff
     * @return the Labor Staff bean inserted
     * @throws ServerBusinessException if error occurs
     */
    public LaborStaff insertLaborStaff(final VIFContext ctx, final LaborStaff laborStaff)
            throws ServerBusinessException;

    /**
     * Lists all the Labor Staff corresponding to the selection (using the I2 index).
     * 
     * @param ctx VIF context
     * @param establishmentKey the establishment key
     * @param productionDate the production date
     * @param resourceId the resource id
     * @param laborId the labor id
     * @return a list of Labor Staff beans
     * @throws ServerBusinessException if error occurs
     */
    public List<LaborStaff> listLaborStaff(VIFContext ctx, EstablishmentKey establishmentKey, Date productionDate,
            String resourceId, String laborId) throws ServerBusinessException;

    /**
     * Lists all the Labor Staff corresponding to the selection.
     * 
     * @param ctx VIF context
     * @param laborStaffKey the Labor Staff key
     * @return a list of Labor Staff beans
     * @throws ServerBusinessException if error occurs
     * 
     */
    public List<LaborStaff> listLaborStaff(final VIFContext ctx, final LaborStaffKey laborStaffKey)
            throws ServerBusinessException;

    /**
     * Lists all the Labor Staff corresponding to the selection.
     * 
     * @param ctx VIF context
     * @param ltBBean FLaborTimeBBean
     * @return a list of Labor Time beans
     * @throws ServerBusinessException if error occurs
     * 
     */
    public List<LaborStaff> listLaborStaffWithLaborTimeBean(final VIFContext ctx, final FLaborTimeBBean ltBBean)
            throws ServerBusinessException;

    /**
     * Lists all the Labor Staff corresponding to the selection.
     * 
     * @param ctx VIF context
     * @param establishmentKey the establishment Key
     * @param prechro the prechro
     * @param listChrono the list of chronos
     * @param datprod the production date
     * @param ligne the production line
     * @return a list of Labor Staff beans
     * @throws ServerBusinessException if error occurs
     * 
     */
    public List<LaborStaff> listLaborStaffWithListMO(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String prechro, final List<Integer> listChrono, final Date datprod, final String ligne)
                    throws ServerBusinessException;

    /**
     * 
     * Returns the period without working mo.
     * 
     * @param ctx VIF Context
     * @param establishmentKey the establishment Key
     * @param proddate the production date
     * @return the chrono
     * @throws ServerBusinessException if error occurs
     */
    public Chrono periodWithoutMO(final VIFContext ctx, final EstablishmentKey establishmentKey, final Date proddate)
            throws ServerBusinessException;

    /**
     * Updates an existing Labor Staff.
     * 
     * @param ctx VIF Context
     * @param laborStaff the Labor Staff
     * @throws ServerBusinessException if error occurs
     */
    public void updateLaborStaff(final VIFContext ctx, final LaborStaff laborStaff) throws ServerBusinessException;

    /**
     * Updates the ni1.
     * 
     * @param ctx the context
     * @param laborStaff the labor staff
     * @param newNi1 the new ni1
     * @return the number of ktpsxd updated
     * @throws ServerBusinessException if error occurs
     */
    public int updateLaborStaffNi1(final VIFContext ctx, final LaborStaff laborStaff, final int newNi1)
            throws ServerBusinessException;
}

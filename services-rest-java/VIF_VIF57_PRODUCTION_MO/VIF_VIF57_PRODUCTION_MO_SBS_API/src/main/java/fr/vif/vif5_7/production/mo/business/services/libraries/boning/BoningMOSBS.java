/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: BoningMOSBS.java,v $
 * Created on 15 Mar 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.boning;


import java.util.Date;
import java.util.List;
import java.util.Map;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.vif5_7.activities.activities.business.beans.common.fabrication.Fabrication;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaCompleteBean;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.boning.labels.LabelBean;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputItemParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputUpdateReturnBean;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputWorkstationParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.SearchLabel;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBoningBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.StackingPlanInfoBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;


/**
 * SBS Interface to get datas for boning mo.
 * 
 * @author xg
 */
public interface BoningMOSBS extends MOSBS {
    /**
     * Create a started boning MSO.
     * 
     * @param ctx the VIF context
     * @param msoKey the MSO key
     * @param effectiveBeginDateTime the effective begin date and time
     * @param fabrication the fabrication
     * @param quantityUnit the quantity and unit
     * @param stackingPlanCL the staking plan code/label
     * @param userId the user id
     * @param originMOKey the origin MO key
     * @return the created bean
     * @throws ServerBusinessException if error occurs
     */
    public MOBoningBean createStartedBoningMSO(VIFContext ctx, MOKey msoKey, Date effectiveBeginDateTime,
            Fabrication fabrication, QuantityUnit quantityUnit, CodeLabel stackingPlanCL, String userId,
            MOKey originMOKey) throws ServerBusinessException;

    /**
     * Get a FProductionBBean.
     * 
     * @param ctx VIF context
     * @param operationItemBean operation item bean
     * @param bbean browser bean
     * @param hchyValue hchyValue
     * @param stackingplanId the stacking plan id for the current mo
     * @param doneQtyList map with the already done quantities (for each item)
     * @param mapItemInner map with inner for each item
     * @return FProductionBBean
     * @throws ServerBusinessException if error occurs
     */
    public FBoningOutputBBean getBBean(final VIFContext ctx, final AbstractOperationItemBean operationItemBean,
            final FBoningOutputBBean bbean, final String hchyValue, final String stackingplanId,
            final Map<OperationItemKey, QtyUnit> doneQtyList, final Map<String, StackingPlanInfoBean> mapItemInner)
                    throws ServerBusinessException;

    /**
     * Return the criteria $CLIENT which contains the customer name.
     * 
     * @param ctx VIFContext
     * @param moKey the MO Key
     * @return the customer name
     * @throws ServerBusinessException if errors occurs
     */
    public String getMOCustomer(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException;

    /**
     * Return the specification CL from the value of the $CC criteria $CC.
     * 
     * @param ctx VIFContext
     * @param moKey the MO Key
     * @return the specification CL
     * @throws ServerBusinessException if errors occurs
     */
    public CodeLabel getMOSpecification(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException;

    /**
     * Get Output parameters.
     * 
     * @param ctx VIF Context
     * @param establishmentKey establishment key
     * @param itemId item id
     * @param workstationId workstaztion id
     * @return output parameters bean
     * @throws ServerBusinessException if error occurs
     */
    public OutputItemParameters getOutputItemParameters(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstationId) throws ServerBusinessException;

    /**
     * return the qty from the reference PO.
     * 
     * @param ctx the VIF context
     * @param moKey the MO key
     * @return the first qty
     * @throws ServerBusinessException if error occurs
     */
    public QtyUnit getRealQtyFromPO(VIFContext ctx, MOKey moKey) throws ServerBusinessException;

    /**
     * List criteria from order line.
     * 
     * @param ctx the VIF context
     * @param establishmentKey the establishment key
     * @param orderChrono the order chrono
     * @param orderline the order ni1
     * @return list with the orderline criteria
     * @throws ServerBusinessException if error occurs
     */
    public List<CriteriaCompleteBean> listCriteriaFromOrderLine(final VIFContext ctx,
            final EstablishmentKey establishmentKey, final Chrono orderChrono, final int orderline)
                    throws ServerBusinessException;

    /**
     * List the done quantities for each item from a MO.
     * 
     * @param ctx VIF context
     * @param moKey the mo key
     * @param itemType the item type (Input or Output). If null, retrieve all items
     * @return the map with qty and item key
     * @throws ServerBusinessException if error occurs
     */
    public Map<OperationItemKey, QtyUnit> listDoneQty(final VIFContext ctx, final MOKey moKey,
            final ActivityItemType itemType) throws ServerBusinessException;

    /**
     * Reprint act.
     * 
     * @param ctx VIF context
     * @param moKey the mo key
     * @param workstationId the workstation id
     * @param userId the user id
     * @param bBean FBoningDataEntryListInputBBean
     * @return list of unprinted documents
     * @throws ServerBusinessException if error occured
     */
    public List<SearchLabel> printAct(final VIFContext ctx, final MOKey moKey, final String workstationId,
            final String userId, final FBoningDataEntryListInputBBean bBean) throws ServerBusinessException;

    /**
     * Print all labels after an input or output declaration.
     * 
     * @param ctx current context
     * @param operationItemKey the operation item key
     * @param itemId the item id
     * @param mvtRowid the rowid for the xmvtm
     * @param itemType the activity item type
     * @param labels the current labels to print
     * @return the list of documents
     * @throws ServerBusinessException if error occurs
     */
    public List<PrintDocument> printAllLabels(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String itemId, final String mvtRowid, final ActivityItemType itemType, final List<LabelBean> labels)
                    throws ServerBusinessException;

    /**
     * Print the roll label.
     * 
     * @param ctx VIF context
     * @param establishmentKey the establishment key
     * @param workstationId the workstation id
     * @param userId the user id
     * @param containerNb the NSC
     * @throws ServerBusinessException if error occured
     */
    public void printRollLabel(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String workstationId, final String userId, final String containerNb) throws ServerBusinessException;

    /**
     * Saves an output entityBean.
     * 
     * @param ctx current context
     * @param operationItemKey operation item key
     * @param entity entity to validate
     * @param outputWorkstationParameters workstation parameters
     * @param outputItemParameters item parameters
     * @param labels the current labels
     * @param stackingPlanInfoBean the stacking plan bean
     * @return OutputUpdateReturnBean with validation infos
     * @throws ServerBusinessException if error occurs
     */
    public OutputUpdateReturnBean saveOutputEntity(final VIFContext ctx, final OperationItemKey operationItemKey,
            final CEntityBean entity, final OutputWorkstationParameters outputWorkstationParameters,
            final OutputItemParameters outputItemParameters, final List<LabelBean> labels,
            final StackingPlanInfoBean stackingPlanInfoBean) throws ServerBusinessException;

    /**
     * Add a tattooed number for all movements and for the nsc.
     * 
     * @param ctx VIF context
     * @param moKey the moKey
     * @param entityBean the current packaging with NSC and tattooed number
     * @param workstationId the workstation id
     * @param userId the user id
     * @throws ServerBusinessException if errors occurs
     */
    public void updateTattooedNb(final VIFContext ctx, final MOKey moKey, final CEntityBean entityBean,
            final String workstationId, final String userId) throws ServerBusinessException;
}

/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: MoResourceSBS.java,v $
 * Created on 13 févr. 09 by gv
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.resources;


import java.util.List;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceBean;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceInputTimeBean;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceInputTimeSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceKey;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceSBean;


/**
 * MO Resource.
 * 
 * @author gv
 */
public interface MoResourceSBS {

    /**
     * 
     * Gets the MO resource bean.
     * 
     * @param vifContext VIF Context
     * @param moResourceKey the resource Key
     * @return the MOResourceBean
     * @throws ServerBusinessException if error occurs
     */
    public MOResourceBean getMOResourceBean(final VIFContext vifContext, final MOResourceKey moResourceKey)
            throws ServerBusinessException;

    /**
     * 
     * Gets the MO resource time input bean.
     * 
     * @param vifContext VIF Context
     * @param moResourceInputTime the resource input time
     * @return the MOResourceInputTimeBean
     * @throws ServerBusinessException if error occurs
     */
    public MOResourceInputTimeBean getMOResourceInputTimeBean(final VIFContext vifContext,
            final MOResourceInputTimeBean moResourceInputTime) throws ServerBusinessException;

    /**
     * 
     * List resources.
     * 
     * @param vifContext VIF Context
     * @param selection the MOResourceSBean
     * @return a list of MOResourceBean
     * @throws ServerBusinessException if error occurs
     */
    public List<MOResourceBean> listResources(VIFContext vifContext, MOResourceSBean selection)
            throws ServerBusinessException;

    /**
     * 
     * List time inputs.
     * 
     * @param vifContext VIF Context
     * @param selection the MOResourceInputTimeSBean
     * @return a list of MOResourceInputTimeBean
     * @throws ServerBusinessException if error occurs
     */
    public List<MOResourceInputTimeBean> listTimeInput(final VIFContext vifContext,
            final MOResourceInputTimeSBean selection) throws ServerBusinessException;

    /**
     * 
     * Save the MO resource time input bean.
     * 
     * @param vifContext VIF Context
     * @param bean the MOResourceInputTimeBean
     * @return the MOResourceInputTimeBean
     * @throws ServerBusinessException if error occurs
     */
    public MOResourceInputTimeBean saveMOResourceInputTime(final VIFContext vifContext,
            final MOResourceInputTimeBean bean) throws ServerBusinessException;
}

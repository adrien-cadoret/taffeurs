/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: MoProfileSBS.java,v $
 * Created on 18 Mar 2016 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.moprofile;


import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputItemParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputItemParameters;


/**
 * Interface for production profiles SBS.
 *
 * @author cj
 */
public interface MoProfileSBS {

    /**
     * Retrieves the input profile.
     * 
     *
     * @param ctx VIF Context
     * @param establishmentKey the establishment key
     * @param itemId the item id
     * @param workstation the workstation
     * @return the input profile
     * @throws ServerBusinessException if error occurs
     */
    public InputItemParameters getInputProfile(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstation) throws ServerBusinessException;

    /**
     * Retrieves the input profile id.
     *
     * @param ctx VIF Context
     * @param establishmentKey the establishment key
     * @param itemId the item id
     * @param workstation the workstation
     * @return the input profile id
     * @throws ServerBusinessException if error occurs
     */
    public String getInputProfileId(final VIFContext ctx, final EstablishmentKey establishmentKey, final String itemId,
            final String workstation) throws ServerBusinessException;

    /**
     * Retrieves the output profile.
     *
     * @param ctx VIF Context
     * @param establishmentKey the establishment key
     * @param itemId the item id
     * @param workstation the workstation
     * @return the output profile
     * @throws ServerBusinessException if error occurs
     */
    public OutputItemParameters getOutputProfile(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstation) throws ServerBusinessException;

    /**
     * Retrieves the output profile id.
     *
     * @param ctx VIF Context
     * @param establishmentKey the establishment key
     * @param itemId the item id
     * @param workstation the workstation
     * @return the output profile id
     * @throws ServerBusinessException if error occurs
     */
    public String getOutputProfileId(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstation) throws ServerBusinessException;

}

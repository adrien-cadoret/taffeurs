/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: MOSBS.java,v $
 * Created on 22 nov. 07 by glc
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.mo;


import java.util.Date;
import java.util.List;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.vif5_7.activities.activities.business.beans.common.fabrication.Fabrication;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.comment.business.beans.common.Comment;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaCompleteBean;
import fr.vif.vif5_7.gen.item.business.beans.features.fitem.FItemSBean;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.ktpsx.KtpsxBean;
import fr.vif.vif5_7.production.mo.business.beans.common.marking.MOMarkingBean;
import fr.vif.vif5_7.production.mo.business.beans.common.marking.MOMarkingSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBoningBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;
import fr.vif.vif5_7.production.mo.business.beans.common.xpiloia.XpiloiaBean;


/**
 * MO business services interface.
 * 
 * @author glc
 */
public interface MOSBS {

    /**
     * Create a started MO.
     * 
     * @param ctx the vif context
     * @param bean the bean for the MO to create
     * @return the bean created.
     * @throws ServerBusinessException e
     */
    public MOBean createStartedMO(final VIFContext ctx, final MOBean bean) throws ServerBusinessException;

    /**
     * Create a started MO, considering the stacking plan and operating process.
     * 
     * @param ctx the vif context
     * @param bean the bean for the MO to create
     * @return the bean created.
     * @throws ServerBusinessException e
     */
    public MOBean createStartedMOStackingPlan(final VIFContext ctx, final MOBoningBean bean)
            throws ServerBusinessException;

    /**
     * Create a started MSO.
     * 
     * @param ctx the VIF context
     * @param msoKey the MSO key
     * @param effectiveBeginDateTime the effective begin date and time
     * @param fabrication the fabrication
     * @param quantityUnit the quantity and unit
     * @param userId the user id
     * @param originMOKey the origin MOM key
     * @return the created bean
     * @throws ServerBusinessException if error occurs
     */
    public MOBean createStartedMSO(final VIFContext ctx, final MOKey msoKey, final Date effectiveBeginDateTime,
            final Fabrication fabrication, final QuantityUnit quantityUnit, final String userId, final MOKey originMOKey)
            throws ServerBusinessException;

    /**
     * Returns the activity family from the PO.
     * 
     * @param ctx the VIF context
     * @param poKey the PO key
     * @return the activity family code
     * @throws ServerBusinessException if error occurs
     */
    public String getActivityFamilyFromPO(final VIFContext ctx, final POKey poKey) throws ServerBusinessException;

    /**
     * Returns the comment associated to the object key.
     * 
     * @param ctx the VIF context
     * @param key the key
     * @return the comment
     * @throws ServerBusinessException if error occurs
     */
    public Comment getComment(final VIFContext ctx, final Object key) throws ServerBusinessException;

    /**
     * Returns a MOBean by its primary key.
     * 
     * @param ctx the VIF context
     * @param moKey the MO key
     * @return the MOBean
     * @throws ServerBusinessException if error occurs
     */
    public MOBean getMOBeanByKey(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException;

    /**
     * Returns a POBean by its key.
     * 
     * @param ctx the VIF context
     * @param poKey the PO key
     * @return the POBean
     * @throws ServerBusinessException if error occurs
     */
    public POBean getPOBeanByKey(final VIFContext ctx, final POKey poKey) throws ServerBusinessException;

    /**
     * Initializes the printer.
     * 
     * @param ctx the VIF context
     * @param functionId function which is used
     * @param workstationId workstation on which labels are parametered
     * @throws ServerBusinessException if error occurs
     */
    public void initPrinter(final VIFContext ctx, final String functionId, final String workstationId)
            throws ServerBusinessException;

    /**
     * Return all the criterion for the MO define in parameter.
     * 
     * @param ctx the vif context
     * @param bean MOBean for which its list of criterion will be loaded.
     * @return a list of criterion of the MO. (CriteriaCompleteBean)
     * @throws ServerBusinessException if error occurs
     */
    public List<CriteriaCompleteBean> listCriterionMO(final VIFContext ctx, final MOBean bean)
            throws ServerBusinessException;

    /**
     * get a list of item families from a workstation.
     * 
     * @param ctx the context
     * @param companyId the company id
     * @param establishmentId the establishmentId id
     * @param workstationId the workstation Id
     * @param basicItemSelection the selection default value
     * @return the list of families codes
     * @throws ServerBusinessException e
     */
    public List<CodeLabels> listItemCLsForFabrication(final VIFContext ctx, final String companyId,
            final String establishmentId, final String workstationId, final FItemSBean basicItemSelection)
            throws ServerBusinessException;

    /**
     * get a list of item families from a workstation.
     * 
     * @param ctx the context
     * @param companyId the company id
     * @param establishmentId the establishmentId id
     * @param workstationId the workstation Id
     * @param basicItemSelection the selection default value
     * @param startIndex the index to begin query
     * @param rowNumber the number of rows to get
     * @return the list of families codes
     * @throws ServerBusinessException e
     */
    public List<CodeLabels> listItemCLsForFabrication(final VIFContext ctx, final String companyId,
            final String establishmentId, final String workstationId, final FItemSBean basicItemSelection,
            final int startIndex, final int rowNumber) throws ServerBusinessException;

    /**
     * 
     * Retrieves the KtspxBean list for a ctx and a {@link EndDayMOSBean} selection given.
     * 
     * @param ctx ctx
     * @param selection selection
     * @return {@link KtpsxBean} list
     * @throws ServerBusinessException the {@link ServerBusinessException} if error occurs
     */
    public List<KtpsxBean> listKtpsxBean(final VIFContext ctx, final EndDayMOSBean selection)
            throws ServerBusinessException;

    /**
     * Returns the MOBean list corresponding the selection.
     * 
     * @param ctx the VIF context
     * @param selection the MO selection
     * @param activityItemTypeList the activity item types (input, output, cost), all types if null or empty
     * @param showFinished return finished MO
     * @param startIndex the index to begin query
     * @param rowNumber the number of rows to get
     * @return the MO beans list
     * @throws ServerBusinessException if error occurs
     */
    public List<MOBean> listMOBean(final VIFContext ctx, final MOSBean selection,
            final List<ActivityItemType> activityItemTypeList, final boolean showFinished, final int startIndex,
            final int rowNumber) throws ServerBusinessException;

    /**
     * Returns the MOMarkingBean list corresponding the selection.
     * 
     * @param ctx the VIF context
     * @param moSelection marking manufacturing orders
     * @param startIndex the index to begin query
     * @param rowNumber the number of rows to get
     * @return a list of MODeclarationBean
     * @throws ServerBusinessException if error occurs
     */
    public List<MOMarkingBean> listMOMarkingBean(final VIFContext ctx, final MOMarkingSBean moSelection,
            final int startIndex, final int rowNumber) throws ServerBusinessException;

    /**
     * Returns a list of POBean from the MO chrono.
     * 
     * @param ctx the VIF context
     * @param moKey the MO key
     * @return the POBean list
     * @throws ServerBusinessException if error occurs
     */
    public List<POBean> listPOBeanByKey(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException;

    /**
     * 
     * Retrieves the XpiloiaBean list for a ctx and a {@link EndDayMOSBean} selection given.
     * 
     * @param ctx ctx
     * @param selection selection
     * @return {@link XpiloiaBean} list
     * @throws ServerBusinessException the {@link ServerBusinessException} if error occurs
     */
    public List<XpiloiaBean> listXpiloiaBean(final VIFContext ctx, final EndDayMOSBean selection)
            throws ServerBusinessException;

    /**
     * get a chrono vif.
     * 
     * @param ctx the vifcontext.
     * @param moKey the mo key
     * @return the complete key
     * @throws ServerBusinessException if pb.
     */
    public MOKey takeMOChrono(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException;

}

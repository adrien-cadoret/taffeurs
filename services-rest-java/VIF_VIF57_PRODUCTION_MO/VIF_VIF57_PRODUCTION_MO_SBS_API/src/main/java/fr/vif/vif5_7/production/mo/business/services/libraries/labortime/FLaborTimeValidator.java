/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS_API
 * File : $RCSfile: FLaborTimeValidator.java,v $
 * Created on 12 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.labortime;


import org.springframework.validation.Errors;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;


/**
 * Labor time validator.
 * 
 * @author nle
 */
public interface FLaborTimeValidator {
    /**
     * Validate the bean.
     * 
     * @param context VIFContext
     * @param insertMode boolean
     * @param target FLaborTimeVBean
     * @param errors Errors
     * @throws ServerBusinessException if check failed
     */
    public void validate(final VIFContext context, final boolean insertMode, final FLaborTimeBBean target,
            final Errors errors) throws ServerBusinessException;

}

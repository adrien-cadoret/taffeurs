/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FMOCreationCBSTest.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmocreation;


import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import junit.framework.TestCase;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.vif5_7.activities.activities.business.beans.common.fabrication.Fabrication;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FabricationType;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FlowType;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaCompleteBean;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaReturnInitBean;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FMOCreationVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FabricationInfos;


/**
 * Test for FMOCreation CBS.
 * 
 * @author glc
 */
public class FMOCreationCBSTest extends TestCase {
    private BeanFactoryReference bfr;
    private FMOCreationCBS       cbs;
    private IdContext            idCtx;

    /**
     * Test isAutoOpeningCriteria.
     */
    public void isGetCriteriaReturnInitBean() throws Exception {
        CriteriaReturnInitBean ret = cbs.getCriteriaReturnInitBean(idCtx, "ZZ", "01", "VIF", "0001");
        assertNotNull(ret);
        assertFalse(ret.getAutoOpening());
    }

    /**
     * Test createStartedMO.
     */
    public void testCreateStartedMO() throws Exception {
        idCtx = new IdContext("ZZ", "01", Locale.FRANCE, "VIF", "");
        FMOCreationVBean vbean = new FMOCreationVBean();
        vbean.setItem("0001");
        vbean.setFlowType(FlowType.PULLED);
        Fabrication fabrication = new Fabrication();
        fabrication.setFabricationType(FabricationType.ACTIVITY);
        fabrication.setFabricationCLs(new CodeLabels("0001", "", ""));
        vbean.setFabrication(fabrication);
        vbean.setQuantityUnit(new QuantityUnit(1, "Poc"));
        vbean.setReturnInitBean(cbs.getCriteriaReturnInitBean(idCtx, "ZZ", "01", "VIF", "0001"));
        MOBean moBean = cbs.createStartedMO(idCtx, vbean);
        assertNotNull(moBean);
        assertEquals("0001", moBean.getItemCLs().getCode());
        assertEquals(1.0, moBean.getMoQuantityUnit().getToDoQuantity());
        assertEquals("U", moBean.getMoQuantityUnit().getUnit());
        assertEquals(FlowType.PULLED, moBean.getFlowType());
        assertEquals(FabricationType.ACTIVITY, moBean.getFabrication().getFabricationType());
        assertEquals("0001", moBean.getFabrication().getFabricationCLs().getCode());
    }

    /**
     * Test getFabricationsInfos.
     */
    public void testGetFabricationInfos() throws Exception {
        FabricationInfos fabricationInfos = cbs.getFabricationInfos(idCtx, "ZZ", "01", "DED", "VMUSCLE003",
                new ArrayList<CriteriaCompleteBean>());
        assertNotNull(fabricationInfos);
        assertEquals(FlowType.PULLED, fabricationInfos.getFlowType());
        assertEquals(FabricationType.ACTIVITY, fabricationInfos.getFabrication().getFabricationType());
        assertEquals("DESOSG", fabricationInfos.getFabrication().getFabricationCLs().getCode());
        assertEquals("P", fabricationInfos.getQuantityUnit().getUnit());
    }

    /**
     * Test queryElements.
     */
    public void testQueryElements() throws Exception {
        List<CodeLabel> list = cbs.queryElements(idCtx, "ZZ", "01", "DED");
        assertNotNull(list);
    }

    /**
     * Test queryElements with startIndex and rowNumber.
     */
    public void testQueryElements2() throws Exception {
        List<CodeLabel> list = cbs.queryElements(idCtx, "ZZ", "01", "DED", 2, 5);
        assertNotNull(list);
        assertEquals(5, list.size());
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();
        cbs = (FMOCreationCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmocreation.FMOCreationCBS");
        idCtx = new IdContext("ZZ", "01", Locale.FRANCE, "VIF");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FSubstituteItemCBSTest.java,v $
 * Created on 19 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fsubstituteitem;


import java.util.List;

import junit.framework.TestCase;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fsubstituteitem.FSubstituteItemSBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;


/**
 * FSubstituteItem CBS Test.
 * 
 * @author vr
 */
public class FSubstituteItemCBSTest extends TestCase {

    private BeanFactoryReference bfr;
    private FSubstituteItemCBS   cbs;
    private IdContext            idContext;

    public void testQueryElements01() {
        List<CodeLabel> lst;
        FSubstituteItemSBean sBean = new FSubstituteItemSBean();

        OperationItemKey oik = new OperationItemKey();
        oik.setEstablishmentKey(new EstablishmentKey("TG", "01"));
        oik.setChrono(new Chrono("1OF", 212));
        oik.setCounter1(1);
        oik.setCounter2(1);
        oik.setCounter3(1);
        oik.setCounter4(1);
        oik.setManagementType(ManagementType.REAL);
        sBean.setWorkstationId("$$");
        sBean.setAll(false);
        InputOperationItemBean bean = new InputOperationItemBean();
        bean.setOperationItemKey(oik);
        bean.setItemCL(new CodeLabels("TSTGP1", "Poulet entier", "Poulet"));
        bean.setActivityItemType(ActivityItemType.INPUT);
        sBean.setInputOperationItemBean(bean);
        try {
            lst = cbs.queryElements(idContext, sBean, 0, 10000);
            assertNotNull(lst);
            assertTrue(lst.size() == 2);
            assertEquals("TSTGP1", lst.get(0).getCode());
            assertEquals("Poulet entier", lst.get(0).getLabel());            
            assertEquals("TSTGP11", lst.get(1).getCode());
            assertEquals("Poulet entier - substitution", lst.get(1).getLabel());
        } catch (BusinessException e) {
            fail("Error : " + e);
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();

        cbs = (FSubstituteItemCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fsubstituteitem.FSubstituteItemCBS");
        idContext = new IdContext("10", "01", I18nServerManager.FRANCE, "vif");

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FDataEntryListCBSTest.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fdataentrylist;


import java.util.List;

import junit.framework.TestCase;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fdataentrylist.FDataEntryListSBean;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;


public class FDataEntryListCBSTest extends TestCase {
    String                       menuString;

    private BeanFactoryReference bfr;
    private FDataEntryListCBS    cbs;
    private IdContext            idContext;

    public void testQueryElements() throws Exception {
        FDataEntryListSBean sel = new FDataEntryListSBean();
        OperationItemKey oik = new OperationItemKey();
        oik.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        oik.setChrono(new Chrono("1OF", 1));
        oik.setCounter1(1);
        oik.setCounter2(1);
        oik.setCounter3(1);
        oik.setCounter4(4);
        oik.setManagementType(ManagementType.REAL);
        sel.setOperationItemKey(oik);
        List<FDataEntryListBBean> lst = cbs.queryElements(idContext, sel, 0, 100000);
        assertNotNull(lst);

    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();
        cbs = (FDataEntryListCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fdataentrylist.FDataEntryListCBS");
        idContext = new IdContext("VR", "08", I18nServerManager.FRANCE, "SAU01");
        idContext.setWorkstation("SAU01");

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}

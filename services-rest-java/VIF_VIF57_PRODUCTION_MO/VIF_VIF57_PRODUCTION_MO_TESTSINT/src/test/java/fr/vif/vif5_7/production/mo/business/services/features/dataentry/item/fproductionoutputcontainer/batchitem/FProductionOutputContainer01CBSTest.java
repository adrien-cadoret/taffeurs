/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FProductionOutputContainer01CBSTest.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcontainer.batchitem;


import java.util.List;

import junit.framework.TestCase;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputTareType;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerWorkBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBS;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerCBS;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;


public class FProductionOutputContainer01CBSTest extends TestCase {

    private BeanFactoryReference               bfr;
    private FProductionOutputContainerCBS      cbs;
    private FProductionCBS                     fProductionCBS;
    private IdContext                          idContext;
    private FProductionOutputContainerWorkBean workBean;

    public void testConvert01() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        moKey.setChrono(new Chrono("1OF", 1));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("SAU01");
        sBean.setAll(false);
        sBean.setActivityItemType(ActivityItemType.OUTPUT);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("SAU01");
        workBean.setLogicalWorkstationId("SAU01");
        FProductionOutputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);
        assertNotNull(vBean);
        CEntityBean entity = vBean.getEntity();
        assertEquals("08200001", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("KG".toLowerCase(), entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit()
                .toLowerCase());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
        assertEquals("SAUMURE01", vBean.getOutputItemParameters().getProfileId());
    }

    public void testGetWorkBean01() throws Exception {

        FProductionOutputContainerWorkBean workBean = cbs.getWorkBean(idContext);

        assertNotNull(workBean);
        assertEquals(OutputTareType.MODE_3, workBean.getOutputWorkstationParameters().getOutputTareType());
        assertEquals("FICBASC2", workBean.getScale().getIdDevice());
    }

    public void testGetWorkBean02() throws Exception {
        idContext.setWorkstation("SAU02");
        FProductionOutputContainerWorkBean workBean = cbs.getWorkBean(idContext);
        assertEquals(OutputTareType.MODE_5, workBean.getOutputWorkstationParameters().getOutputTareType());
        assertNull(workBean.getScale());
    }

    public void testUpdate01() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        moKey.setChrono(new Chrono("1OF", 1));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("SAU01");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        sBean.setActivityItemType(ActivityItemType.OUTPUT);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("SAU01");
        workBean.setLogicalWorkstationId("SAU01");
        FProductionOutputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);
        assertNotNull(vBean);
        vBean.getEntity().getStockItemBean().getStockItem().setBatch("NOUVEAU");
        vBean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty().setQty(0.021);
        vBean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty().setUnit("KG");
        vBean = cbs.updateBean(idContext, vBean, workBean);
        assertTrue(vBean.getAutoInsert());
        cbs.deleteLast(idContext, vBean.getEntity().getEntityLocationBean().getEntityLocationKey(),
                vBean.getPreviousMovementAct());
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();
        cbs = (FProductionOutputContainerCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerCBS");
        fProductionCBS = (FProductionCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBS");
        idContext = new IdContext("VR", "08", I18nServerManager.FRANCE, "SAP01");
        idContext.setWorkstation("SAU01");
        workBean = new FProductionOutputContainerWorkBean();

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    // private FProductionInputContainerVBean getVBean01() throws Exception {
    // List<FProductionBBean> lst;
    // MOKey moKey = new MOKey();
    // moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
    // moKey.setChrono(new Chrono("1OF", 1));
    //
    // FProductionSBean sBean = new FProductionSBean();
    // sBean.setWorkstationId("SAU01");
    // sBean.setAll(false);
    // sBean.setMoKey(moKey);
    // lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
    // assertNotNull(lst);
    // workBean.setRealWorkstationId("SAU01");
    // workBean.setLogicalWorkstationId("SAU01");
    // FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);
    // return vBean;
    // }
}

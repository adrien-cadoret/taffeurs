/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MOItemContainerSBSTest.java,v $
 * Created on 26 nov. 07 by ped
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.operationitem;


import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.util.test.SBSTestCase;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.services.libraries.chrono.ChronoSBS;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputItemParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputItemParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputBatchType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputContainerCapacityClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputContainerClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputUpperContainerCapacityClosingType;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.production.mo.business.services.libraries.moprofile.MoProfileSBS;


public class MOItemContainerSBSTest extends SBSTestCase {

    private BeanFactoryReference bfr;
    private ChronoSBS            chronoSBS;
    private MOItemSBS            moItemSBS;
    private MOSBS                moSBS;
    private MoProfileSBS         moProfileSBS;

    /**
     * Gets the chronoSBS.
     * 
     * @category getter
     * @return the chronoSBS.
     */
    public ChronoSBS getChronoSBS() {
        return chronoSBS;
    }

    /**
     * Gets the moItemSBS.
     * 
     * @category getter
     * @return the moItemSBS.
     */
    public MOItemSBS getMOItemSBS() {
        return moItemSBS;
    }

    /**
     * Gets the moProfileSBS.
     *
     * @return the moProfileSBS.
     */
    public MoProfileSBS getMoProfileSBS() {
        return moProfileSBS;
    }

    /**
     * Gets the moSBS.
     * 
     * @category getter
     * @return the moSBS.
     */
    public MOSBS getMOSBS() {
        return moSBS;
    }

    /**
     * Sets the chronoSBS.
     * 
     * @category setter
     * @param chronoSBS chronoSBS.
     */
    public void setChronoSBS(final ChronoSBS chronoSBS) {
        this.chronoSBS = chronoSBS;
    }

    /**
     * Sets the moItemSBS.
     * 
     * @category setter
     * @param moItemSBS moItemSBS.
     */
    public void setMoItemSBS(final MOItemSBS moItemSBS) {
        this.moItemSBS = moItemSBS;
    }

    /**
     * Sets the moProfileSBS.
     *
     * @param moProfileSBS moProfileSBS.
     */
    public void setMoProfileSBS(final MoProfileSBS moProfileSBS) {
        this.moProfileSBS = moProfileSBS;
    }

    /**
     * Sets the moSBS.
     * 
     * @category setter
     * @param moSBS moSBS.
     */
    public void setMoSBS(final MOSBS moSBS) {
        this.moSBS = moSBS;
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();

        chronoSBS = (ChronoSBS) bf.getBean("chronoSBSImpl");
        moSBS = (MOSBS) bf.getBean("MOSBSImpl");
        moItemSBS = (MOItemSBS) bf.getBean("MOItemSBSImpl");
        moProfileSBS = (MoProfileSBS) bf.getBean("moProfileSBSImpl");
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        bfr.release();
    }

    @Test
    public void testGetInputParameters01() {
        try {
            InputItemParameters param = moItemSBS.getInputItemParameters(getCtx(), new EstablishmentKey("VR", "08"),
                    "08101001", "SAU01");
            assertNotNull(param);
            assertEquals(InputEntityType.BATCH_ITEM, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals("SAUMURE01", param.getProfileId());
            assertEquals(false, param.getInterpretQty());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }

    @Test
    public void testGetInputParameters01_fullJava() {
        try {
            InputItemParameters param = moProfileSBS.getInputProfile(getCtx(), new EstablishmentKey("VR", "08"),
                    "08101001", "SAU01");
            assertNotNull(param);
            assertEquals(InputEntityType.BATCH_ITEM, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals("SAUMURE01", param.getProfileId());
            assertEquals(false, param.getInterpretQty());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }

    @Test
    public void testGetInputParameters02() {
        try {
            InputItemParameters param = moItemSBS.getInputItemParameters(getCtx(), new EstablishmentKey("VR", "08"),
                    "08101004", "SAU01");
            assertNotNull(param);
            assertEquals(InputEntityType.CONTAINER, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals("SAUMURE02", param.getProfileId());
            assertEquals(false, param.getInterpretQty());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }

    @Test
    public void testGetInputParameters02_fullJava() {
        try {
            InputItemParameters param = moProfileSBS.getInputProfile(getCtx(), new EstablishmentKey("VR", "08"),
                    "08101004", "SAU01");
            assertNotNull(param);
            assertEquals(InputEntityType.CONTAINER, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals("SAUMURE02", param.getProfileId());
            assertEquals(false, param.getInterpretQty());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }

    @Test
    public void testGetOutputParameters01() {
        try {
            OutputItemParameters param = moItemSBS.getOutputItemParameters(getCtx(), new EstablishmentKey("VR", "08"),
                    "08200001", "SAU01");
            assertNotNull(param);
            assertEquals(OutputEntityType.BATCH_ITEM, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals(0d, param.getForcedSecondQty());
            assertEquals("SAUMURE01", param.getProfileId());
            assertEquals("", param.getContainerPackagingId());
            assertEquals(OutputContainerClosingType.DISABLED, param.getContainerClosingType());
            assertEquals(OutputContainerCapacityClosingType.DISABLED, param.getContainerCapacityClosingType());
            assertEquals("", param.getUpperContainerPackagingId());
            assertEquals(OutputContainerClosingType.DISABLED, param.getUpperContainerClosingType());
            assertEquals(OutputUpperContainerCapacityClosingType.DISABLED, param.getUpperContainerCapacityClosingType());
            assertEquals(OutputBatchType.AUTOMATIC_GENERATION, param.getBatchType());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }

    @Test
    public void testGetOutputParameters01_fullJava() {
        try {
            OutputItemParameters param = moProfileSBS.getOutputProfile(getCtx(), new EstablishmentKey("VR", "08"),
                    "08200001", "SAU01");
            assertNotNull(param);
            assertEquals(OutputEntityType.BATCH_ITEM, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals(0d, param.getForcedSecondQty());
            assertEquals("SAUMURE01", param.getProfileId());
            assertEquals("", param.getContainerPackagingId());
            assertEquals(OutputContainerClosingType.DISABLED, param.getContainerClosingType());
            assertEquals(OutputContainerCapacityClosingType.DISABLED, param.getContainerCapacityClosingType());
            assertEquals("", param.getUpperContainerPackagingId());
            assertEquals(OutputContainerClosingType.DISABLED, param.getUpperContainerClosingType());
            assertEquals(OutputUpperContainerCapacityClosingType.DISABLED, param.getUpperContainerCapacityClosingType());
            assertEquals(OutputBatchType.AUTOMATIC_GENERATION, param.getBatchType());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }

    @Test
    public void testGetOutputParameters02() {
        try {
            OutputItemParameters param = moItemSBS.getOutputItemParameters(getCtx(), new EstablishmentKey("VR", "08"),
                    "08200002", "SAU01");
            assertNotNull(param);
            assertEquals(OutputEntityType.CONTAINER, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals("SAUMURE02", param.getProfileId());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals(0d, param.getForcedSecondQty());
            assertEquals("SAC", param.getContainerPackagingId());
            assertEquals(OutputContainerClosingType.AUTOMATIC, param.getContainerClosingType());
            assertEquals(OutputContainerCapacityClosingType.DISABLED, param.getContainerCapacityClosingType());
            assertEquals("", param.getUpperContainerPackagingId());
            assertEquals(OutputContainerClosingType.DISABLED, param.getUpperContainerClosingType());
            assertEquals(OutputUpperContainerCapacityClosingType.DISABLED, param.getUpperContainerCapacityClosingType());
            assertEquals(OutputBatchType.AUTOMATIC_GENERATION, param.getBatchType());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }

    @Test
    public void testGetOutputParameters02_fullJava() {
        try {
            OutputItemParameters param = moProfileSBS.getOutputProfile(getCtx(), new EstablishmentKey("VR", "08"),
                    "08200002", "SAU01");
            assertNotNull(param);
            assertEquals(OutputEntityType.CONTAINER, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals("SAUMURE02", param.getProfileId());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals(0d, param.getForcedSecondQty());
            assertEquals("SAC", param.getContainerPackagingId());
            assertEquals(OutputContainerClosingType.AUTOMATIC, param.getContainerClosingType());
            assertEquals(OutputContainerCapacityClosingType.DISABLED, param.getContainerCapacityClosingType());
            assertEquals("", param.getUpperContainerPackagingId());
            assertEquals(OutputContainerClosingType.DISABLED, param.getUpperContainerClosingType());
            assertEquals(OutputUpperContainerCapacityClosingType.DISABLED, param.getUpperContainerCapacityClosingType());
            assertEquals(OutputBatchType.AUTOMATIC_GENERATION, param.getBatchType());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }

    @Test
    public void testGetOutputParameters03() {
        try {
            OutputItemParameters param = moItemSBS.getOutputItemParameters(getCtx(), new EstablishmentKey("VR", "08"),
                    "08200004", "SAU01");
            assertNotNull(param);
            assertEquals(OutputEntityType.CONTAINER_UPPER_CONTAINER, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals(0d, param.getForcedSecondQty());
            assertEquals("SAC", param.getContainerPackagingId());

            assertEquals(OutputContainerClosingType.AUTOMATIC, param.getContainerClosingType());
            assertEquals(OutputContainerCapacityClosingType.DISABLED, param.getContainerCapacityClosingType());
            assertEquals("PAL", param.getUpperContainerPackagingId());
            assertEquals(OutputContainerClosingType.MANUAL, param.getUpperContainerClosingType());
            assertEquals(OutputUpperContainerCapacityClosingType.DISABLED, param.getUpperContainerCapacityClosingType());
            assertEquals(OutputBatchType.AUTOMATIC_GENERATION, param.getBatchType());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }

    @Test
    public void testGetOutputParameters03_fullJava() {
        try {
            OutputItemParameters param = moProfileSBS.getOutputProfile(getCtx(), new EstablishmentKey("VR", "08"),
                    "08200004", "SAU01");
            assertNotNull(param);
            assertEquals(OutputEntityType.CONTAINER_UPPER_CONTAINER, param.getEntityType());
            assertEquals("", param.getForcedFirstUnit());
            assertEquals("", param.getForcedSecondUnit());
            assertEquals(0d, param.getForcedFirstQty());
            assertEquals(0d, param.getForcedSecondQty());
            assertEquals("SAC", param.getContainerPackagingId());

            assertEquals(OutputContainerClosingType.AUTOMATIC, param.getContainerClosingType());
            assertEquals(OutputContainerCapacityClosingType.DISABLED, param.getContainerCapacityClosingType());
            assertEquals("PAL", param.getUpperContainerPackagingId());
            assertEquals(OutputContainerClosingType.MANUAL, param.getUpperContainerClosingType());
            assertEquals(OutputUpperContainerCapacityClosingType.DISABLED, param.getUpperContainerCapacityClosingType());
            assertEquals(OutputBatchType.AUTOMATIC_GENERATION, param.getBatchType());
        } catch (ServerBusinessException e) {
            fail("Error" + e);
        }
    }
}

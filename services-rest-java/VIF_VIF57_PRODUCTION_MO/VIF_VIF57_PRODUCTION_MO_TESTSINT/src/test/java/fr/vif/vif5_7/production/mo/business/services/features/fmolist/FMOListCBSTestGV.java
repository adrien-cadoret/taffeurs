/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FMOListCBSTestGV.java,v $
 * Created on 6 nov. 08 by alb
 */
package fr.vif.vif5_7.production.mo.business.services.features.fmolist;


import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

import javax.naming.InitialContext;
import javax.sql.DataSource;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.access.BeanFactoryReference;

import fr.vif.jtech.common.IdContext;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.common.fmolist.FMOListCBSImpl;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionmo.FProductionMOListCBSImpl;


/**
 * Tests for MOListCBS.
 * 
 * @author alb
 */
public class FMOListCBSTestGV extends TestCase {
    private static final Logger      LOGGER           = Logger.getLogger(FMOListCBSTestGV.class);

    private BeanFactoryReference     bfr;

    private EstablishmentKey         establishmentKey = new EstablishmentKey("TG", "01");
    private IdContext                idCtx;

    /* CHECKSTYLE:OFF */
    /**
     * The Lib.
     */
    private FMOListCBSImpl           listCBSImpl      = null;
    private FProductionMOListCBSImpl moCBSImpl        = null;

    /**
     * Gets the listCBSImpl.
     * 
     * @category getter
     * @return the listCBSImpl.
     */
    public final FMOListCBSImpl getListCBSImpl() {
        return listCBSImpl;
    }

    /**
     * Gets the moCBSImpl.
     *
     * @category getter
     * @return the moCBSImpl.
     */
    public FProductionMOListCBSImpl getMoCBSImpl() {
        return moCBSImpl;
    }

    /**
     * Sets the listCBSImpl.
     * 
     * @category setter
     * @param listCBSImpl listCBSImpl.
     */
    public final void setListCBSImpl(final FMOListCBSImpl listCBSImpl) {
        this.listCBSImpl = listCBSImpl;
    }

    /**
     * Sets the moCBSImpl.
     *
     * @category setter
     * @param moCBSImpl moCBSImpl.
     */
    public void setMoCBSImpl(final FProductionMOListCBSImpl moCBSImpl) {
        this.moCBSImpl = moCBSImpl;
    }

    public void testCeciestuntestaaz() {
        try {

            InitialContext ctx = new InitialContext();

            DataSource dsora = (DataSource) ctx.lookup("java:/Vif5_7DS");
            Connection cnx = dsora.getConnection();
            Statement st = cnx.createStatement();
            st.execute("select count(1) from zart;");

            ResultSet rs = st.getResultSet();
            int a = rs.getInt(0);

        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }
}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: LabelSBSTest.java,v $
 * Created on 8 déc. 08 by gv
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.label;


import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.util.test.SBSTestCase;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.services.libraries.chrono.ChronoSBS;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.ItemMovementBean;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.RowidKey;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.SearchLabel;
import fr.vif.vif5_7.production.mo.business.beans.common.labels.LabelEvent;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.production.mo.business.services.libraries.operationitem.MOItemSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.PrechroId;


/**
 * The label SBS test class.
 *
 * @author gv
 */
public class LabelSBSTest extends SBSTestCase {
    private BeanFactoryReference bfr;
    LabelSBS                     labelSBS  = null;
    MOSBS                        mOSBS     = null;
    ChronoSBS                    chronoSBS = null;
    MOItemSBS                    mOItemSBS = null;

    /**
     * Gets the chronoSBS.
     *
     * @category getter
     * @return the chronoSBS.
     */
    public ChronoSBS getChronoSBS() {
        return chronoSBS;
    }

    /**
     * Gets the labelSBS.
     *
     * @category getter
     * @return the labelSBS.
     */
    public LabelSBS getLabelSBS() {
        return labelSBS;
    }

    /**
     * Gets the mOItemSBS.
     *
     * @category getter
     * @return the mOItemSBS.
     */
    public MOItemSBS getMOItemSBS() {
        return mOItemSBS;
    }

    /**
     * Gets the mOSBS.
     *
     * @category getter
     * @return the mOSBS.
     */
    public MOSBS getMOSBS() {
        return mOSBS;
    }

    /**
     * Sets the chronoSBS.
     *
     * @category setter
     * @param chronoSBS chronoSBS.
     */
    public void setChronoSBS(final ChronoSBS chronoSBS) {
        this.chronoSBS = chronoSBS;
    }

    /**
     * Sets the labelSBS.
     *
     * @category setter
     * @param labelSBS labelSBS.
     */
    public void setLabelSBS(final LabelSBS labelSBS) {
        this.labelSBS = labelSBS;
    }

    /**
     * Sets the mOItemSBS.
     *
     * @category setter
     * @param itemSBS mOItemSBS.
     */
    public void setMOItemSBS(final MOItemSBS itemSBS) {
        mOItemSBS = itemSBS;
    }

    /**
     * Sets the mOSBS.
     *
     * @category setter
     * @param mosbs mOSBS.
     */
    public void setMOSBS(final MOSBS mosbs) {
        mOSBS = mosbs;
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();

        chronoSBS = (ChronoSBS) bf.getBean("chronoSBSImpl");
        mOSBS = (MOSBS) bf.getBean("MOSBSImpl");
        mOItemSBS = (MOItemSBS) bf.getBean("MOItemSBSImpl");
        labelSBS = (LabelSBS) bf.getBean("labelSBSImpl");
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        bfr.release();
    }

    @Test
    public void testRunPrintActLabel() throws Exception {
        {
            // shouldn't print anything but should give chice of nb.
            getMOSBS().initPrinter(getCtx(), "ENTFABJ", "PENUNIT2");

            MOKey moKey = new MOKey();
            moKey.setChrono(new Chrono("1OF", 1048));
            moKey.setEstablishmentKey(new EstablishmentKey("TG", "01"));

            LabelEvent labelEvent = new LabelEvent();
            labelEvent.setActivityItemType(ActivityItemType.INPUT);
            labelEvent.setEventType(LabelingEventType.MO_CREATION);
            labelEvent.setFeatureId("ENTFABJ");
            labelEvent.setMoKey(moKey);
            labelEvent.setRowIdKey(new RowidKey());
            labelEvent.setWorkstationId("PENUNIT2");

            List<SearchLabel> listsearchLabel = getLabelSBS().searchPrintLabels(getCtx(), labelEvent);
            assertNotNull(listsearchLabel);
            assertTrue(listsearchLabel.size() > 0);
            assertEquals(true, listsearchLabel.get(0).getAskForCopiesNumber());
            assertEquals("OFG", listsearchLabel.get(0).getLabelId());
            assertEquals("ehplaofg", listsearchLabel.get(0).getPrintProgramId());

            getLabelSBS().runPrintLabel(getCtx(), labelEvent, listsearchLabel.get(0), 1);
        }
    }

    @Test
    public void testSearchPrintMovementLabel() {
        MOKey moKey = new MOKey();

        moKey.setEstablishmentKey(new EstablishmentKey("TG", "01"));
        moKey.setChrono(new Chrono());
        try {
            getMOSBS().initPrinter(getCtx(), "SORFABJ", "PSORUNIT");

            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 161), 1, 2,
                    1, 2);

            List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);

            LabelEvent labelEvent = new LabelEvent();

            labelEvent.setActivityItemType(ActivityItemType.OUTPUT);
            labelEvent.setEventType(LabelingEventType.MO_FIRST_DECLARATION);
            labelEvent.setFeatureId("SORFABJ");
            labelEvent.setMoKey(moKey);
            labelEvent.setRowIdKey(new RowidKey(listItemMovement.get(0).getItemMovementKey().getRowidKey()));
            labelEvent.setWorkstationId("PSORUNIT");

            List<SearchLabel> listsearchLabel = getLabelSBS().searchPrintLabels(getCtx(), labelEvent);
            assertNotNull(listsearchLabel);
            assertTrue(listsearchLabel.size() > 0);
            assertEquals(false, listsearchLabel.get(0).getAskForCopiesNumber());
            assertEquals("", listsearchLabel.get(0).getLabelId());
            assertEquals("", listsearchLabel.get(0).getPrintProgramId());

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on searchPrintMovementLabel");
        }

        try {
            getMOSBS().initPrinter(getCtx(), "ENTFABJ", "PENTUNIT");

            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 161), 1, 2,
                    1, 2);

            List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);

            LabelEvent labelEvent = new LabelEvent();
            labelEvent.setActivityItemType(ActivityItemType.INPUT);
            labelEvent.setEventType(LabelingEventType.MO_FIRST_DECLARATION);
            labelEvent.setFeatureId("ENTFABJ");
            labelEvent.setMoKey(moKey);
            labelEvent.setRowIdKey(new RowidKey(listItemMovement.get(0).getItemMovementKey().getRowidKey()));
            labelEvent.setWorkstationId("PENTUNIT");

            List<SearchLabel> listsearchLabel = getLabelSBS().searchPrintLabels(getCtx(), labelEvent);
            assertNotNull(listsearchLabel);
            assertTrue(listsearchLabel.size() > 0);
            assertEquals(true, listsearchLabel.get(1).getAskForCopiesNumber());
            assertEquals("MVG", listsearchLabel.get(1).getLabelId());
            assertEquals("ehplamvg", listsearchLabel.get(1).getPrintProgramId());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on searchPrintMovementLabel");
        }

        try {
            getMOSBS().initPrinter(getCtx(), "SORFABJ", "PSORUNI2");

            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 161), 1, 2,
                    1, 2);

            List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);

            LabelEvent labelEvent = new LabelEvent();
            labelEvent.setActivityItemType(ActivityItemType.OUTPUT);
            labelEvent.setEventType(LabelingEventType.MO_FIRST_DECLARATION);
            labelEvent.setFeatureId("SORFABJ");
            labelEvent.setMoKey(moKey);
            labelEvent.setRowIdKey(new RowidKey(listItemMovement.get(0).getItemMovementKey().getRowidKey()));
            labelEvent.setWorkstationId("PSORUNI2");

            List<SearchLabel> listsearchLabel = getLabelSBS().searchPrintLabels(getCtx(), labelEvent);
            assertNotNull(listsearchLabel);
            assertTrue(listsearchLabel.size() > 0);
            assertEquals(true, listsearchLabel.get(0).getAskForCopiesNumber());
            assertEquals(true, listsearchLabel.get(0).getAskForCopiesNumber());
            assertEquals("PED", listsearchLabel.get(0).getLabelId());
            assertEquals("einf4ped", listsearchLabel.get(0).getPrintProgramId());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on searchPrintMovementLabel");
        }

        // le code suivant m'a servi à appeler la lib ds le cas d'un test d'integ
        // try {
        //
        // TestCaseContextAdapter adapter = new TestCaseContextAdapter();
        // VIFContext ctx = adapter.createVIFContext(new IdContext("SA", "01", Locale.FRENCH, "TESTUNIT"));
        // getMOSBS().initPrinter(ctx, "ENTFABJ", "DED");
        //
        // MOKey moKey = new MOKey();
        // moKey.setChrono(new Chrono("1OF", 680));
        // moKey.setEstablishmentKey(new EstablishmentKey("SA", "01"));
        // OperationItemSBean moItSBean = new OperationItemSBean();
        // moItSBean.setAll(true);
        // List<AbstractOperationItemBean> list = getMOItemSBS().listOperationItem(ctx, moKey, moItSBean);
        //
        // OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("SA",
        // "01"), new Chrono(getChronoSBS().getPrechro(ctx, PrechroId.PRODUCTION.getValue()), 680), 1, 1, 1, 2);
        //
        // List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(ctx, operationItemKey,
        // ActivityItemType.INPUT);
        //
        // SearchLabel searchLabel = getMOItemSBS().searchPrintMovementLabel(ctx,
        // new RowidKey(listItemMovement.get(1).getItemMovementKey().getRowidKey()), "DED",
        // ActivityItemType.INPUT);
        // assertNotNull(searchLabel);
        // assertEquals(true, searchLabel.getAskForCopiesNumber());
        // assertEquals("MVG", searchLabel.getLabelId());
        // assertEquals("ehplamvg", searchLabel.getPrintProgramId());
        // } catch (ServerBusinessException e) {
        // fail("ServerBusinessException on searchPrintMovementLabel");
        // } catch (BusinessException e) {
        // fail("BusinessException on searchPrintMovementLabel");
        // }
    }

}

/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FProductionInputContainer02CBSTest.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.container;


import java.util.List;

import junit.framework.TestCase;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputBarcodeType;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerWorkBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBS;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerCBS;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;


public class FProductionInputContainer02CBSTest extends TestCase {

    private BeanFactoryReference              bfr;
    private FProductionInputContainerCBS      cbs;
    private FProductionCBS                    fProductionCBS;
    private IdContext                         idContext;
    private FProductionInputContainerWorkBean workBean;

    public void testAnalyseBarcode01() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        moKey.setChrono(new Chrono("1OF", 2));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("SAU01");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("SAU01");
        workBean.setLogicalWorkstationId("SAU01");
        workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(2), workBean);
        assertNotNull(vBean);
        vBean = cbs.analyseBarcode(idContext, "]C1915100001000000239", vBean, sBean, workBean);
        assertNotNull(vBean);
        assertFalse(vBean.getAutoInsert());
        CEntityBean entity = vBean.getEntity();
        assertEquals("08101005", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals("891070010", entity.getStockItemBean().getStockItem().getBatch());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("KG".toLowerCase(), entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit()
                .toLowerCase());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
    }

    public void testAnalyseBarcode02() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        moKey.setChrono(new Chrono("1OF", 2));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("SAU01");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("SAU01");
        workBean.setLogicalWorkstationId("SAU01");
        workBean.setBarcodeType(InputBarcodeType.KEEP_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(2), workBean);
        assertNotNull(vBean);
        vBean = cbs.analyseBarcode(idContext, "]C1915100001000000239", vBean, sBean, workBean);
        assertNotNull(vBean);
        assertFalse(vBean.getAutoInsert());
        CEntityBean entity = vBean.getEntity();
        assertEquals("08101005", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals("891070010", entity.getStockItemBean().getStockItem().getBatch());
        assertEquals(2.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("KG".toLowerCase(), entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit()
                .toLowerCase());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
        if (vBean.getPreviousMovementAct() != null) {
            cbs.deleteLast(idContext, entity.getEntityLocationBean().getEntityLocationKey(),
                    vBean.getPreviousMovementAct());
        }
    }

    public void testAnalyseContainer01() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        moKey.setChrono(new Chrono("1OF", 2));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("SAU01");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("SAU01");
        workBean.setLogicalWorkstationId("SAU01");
        workBean.setBarcodeType(InputBarcodeType.KEEP_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(2), workBean);
        assertNotNull(vBean);
        vBean = cbs.analyseContainer(idContext, "1000000239", vBean, sBean, workBean, true);
        assertNotNull(vBean);
        assertFalse(vBean.getAutoInsert());
        CEntityBean entity = vBean.getEntity();
        assertEquals("08101005", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals("891070010", entity.getStockItemBean().getStockItem().getBatch());
        assertEquals(2.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("KG".toLowerCase(), entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit()
                .toLowerCase());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
        if (vBean.getPreviousMovementAct() != null) {
            cbs.deleteLast(idContext, entity.getEntityLocationBean().getEntityLocationKey(),
                    vBean.getPreviousMovementAct());
        }
    }

    public void testConvert01() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        moKey.setChrono(new Chrono("1OF", 2));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("SAU01");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("SAU01");
        workBean.setLogicalWorkstationId("SAU01");
        workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(2), workBean);
        assertNotNull(vBean);
        CEntityBean entity = vBean.getEntity();
        assertEquals("08101005", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("KG".toLowerCase(), entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit()
                .toLowerCase());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
    }

    public void testUpdate01() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        moKey.setChrono(new Chrono("1OF", 1));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("SAU01");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("SAU01");
        workBean.setLogicalWorkstationId("SAU01");
        workBean.setBarcodeType(InputBarcodeType.KEEP_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);

        assertNotNull(vBean);
        vBean.getEntity().getStockItemBean().getStockItem().setBatch("891070001");
        vBean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty().setQty(0.021);
        vBean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty().setUnit("KG");
        vBean = cbs.updateBean(idContext, vBean, workBean);
        assertTrue(vBean.getAutoInsert());
        cbs.deleteLast(idContext, vBean.getEntity().getEntityLocationBean().getEntityLocationKey(),
                vBean.getPreviousMovementAct());
    }

    // public void testAnalyseBarcode02() throws Exception {
    // List<FProductionBBean> lst;
    // MOKey moKey = new MOKey();
    // moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
    // moKey.setChrono(new Chrono("1OF", 1));
    //
    // FProductionSBean sBean = new FProductionSBean();
    // sBean.setWorkstationId("SAU01");
    // sBean.setAll(false);
    // sBean.setMoKey(moKey);
    // lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
    // assertNotNull(lst);
    // workBean.setRealWorkstationId("SAU01");
    // workBean.setLogicalWorkstationId("SAU01");
    // FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);
    // assertNotNull(vBean);
    // vBean = cbs.analyseBarcode(idContext, "]C191600008101001|10890910001|3103000022", vBean, sBean, workBean);
    // assertNotNull(vBean);
    // assertTrue(vBean.getAutoInsert());
    // // cbs.deleteLast(idContext, vBean.getPreviousMovementAct(), "SAU01");
    // }
    //
    // public void testAnalyseBarcode03() {
    // List<FProductionBBean> lst;
    // MOKey moKey = new MOKey();
    // moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
    // moKey.setChrono(new Chrono("1OF", 1));
    //
    // FProductionSBean sBean = new FProductionSBean();
    // sBean.setWorkstationId("SAU01");
    // sBean.setAll(false);
    // sBean.setMoKey(moKey);
    // FProductionInputContainerVBean vBean = null;
    // try {
    // lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
    // assertNotNull(lst);
    //
    // workBean.setRealWorkstationId("SAU02");
    // workBean.setLogicalWorkstationId("SAU01");
    // vBean = cbs.convert(idContext, lst.get(0), workBean);
    // assertNotNull(vBean);
    // } catch (BusinessException e1) {
    // fail("Error " + e1);
    // }
    // try {
    // vBean = cbs.analyseBarcode(idContext, "]C19160000810100hjkh1|1jkhjk0001|310300hjkhjkh0001", vBean, sBean,
    // workBean);
    // fail("Error");
    // } catch (BusinessException e) {
    // } catch (BusinessErrorsException e) {
    // }
    // }

    // public void testAnalyseBarcode04() {
    // List<FProductionBBean> lst;
    // MOKey moKey = new MOKey();
    // moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
    // moKey.setChrono(new Chrono("1OF", 1));
    //
    // FProductionSBean sBean = new FProductionSBean();
    // sBean.setWorkstationId("SAU01");
    // sBean.setAll(false);
    // sBean.setMoKey(moKey);
    // FProductionInputContainerVBean vBean = null;
    // try {
    // lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
    // assertNotNull(lst);
    // workBean.setRealWorkstationId("SAU11");
    // workBean.setLogicalWorkstationId("SAU11");
    // workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
    // vBean = cbs.convert(idContext, lst.get(0), workBean);
    // assertNotNull(vBean);
    // vBean = cbs.analyseBarcode(idContext, "]C1915100001000000001", vBean, sBean, workBean);
    // } catch (BusinessException e) {
    // fail("Error " + e);
    // } catch (BusinessErrorsException e) {
    // fail("Error " + e);
    // }
    // assertNotNull(vBean);
    // assertTrue(vBean.getAutoInsert());
    // // cbs.deleteLast(idContext, vBean.getPreviousItemMovementKey(), "SAU11");
    // }

    // public void testAnalyseBarcode05() {
    // List<FProductionBBean> lst;
    // MOKey moKey = new MOKey();
    // moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
    // moKey.setChrono(new Chrono("1OF", 1));
    //
    // FProductionSBean sBean = new FProductionSBean();
    // sBean.setWorkstationId("SAU01");
    // sBean.setAll(false);
    // sBean.setMoKey(moKey);
    // FProductionInputContainerVBean vBean = null;
    // try {
    // lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
    // assertNotNull(lst);
    // workBean.setRealWorkstationId("SAU11");
    // workBean.setLogicalWorkstationId("SAU11");
    // workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
    // vBean = cbs.convert(idContext, lst.get(0), workBean);
    // assertNotNull(vBean);
    // vBean = cbs.analyseBarcode(idContext, "]C1915100001000000001", vBean, sBean, workBean);
    // } catch (BusinessException e) {
    // fail("Error " + e);
    // } catch (BusinessErrorsException e) {
    // fail("Error " + e);
    // }
    // assertNotNull(vBean);
    // assertFalse(vBean.getAutoInsert());
    // assertEquals("002", vBean.getEntity().getStockItemBean().getStockItem().getBatch());
    // }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();
        cbs = (FProductionInputContainerCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerCBS");
        fProductionCBS = (FProductionCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBS");
        idContext = new IdContext("VR", "08", I18nServerManager.FRANCE, "SAU01");
        idContext.setWorkstation("SAU01");
        workBean = new FProductionInputContainerWorkBean();

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private FProductionInputContainerVBean getVBean01() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        moKey.setChrono(new Chrono("1OF", 1));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("SAU01");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("SAU01");
        workBean.setLogicalWorkstationId("SAU01");
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);
        return vBean;
    }
}

/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MOEndSBSTest.java,v $
 * Created on 26 déc. 07 by ped
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.end;


import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.util.test.SBSTestCase;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.services.libraries.chrono.ChronoSBS;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.operationitem.MOItemSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.PrechroId;


public class MOEndSBSTest extends SBSTestCase {

    private BeanFactoryReference bfr;

    private ChronoSBS            chronoSBS;
    private MOEndSBS             moEndSBS;
    private MOItemSBS            moItemSBS;

    /**
     * Gets the chronoSBS.
     * 
     * @category getter
     * @return the chronoSBS.
     */
    public ChronoSBS getChronoSBS() {
        return chronoSBS;
    }

    /**
     * Gets the moEndSBS.
     * 
     * @category getter
     * @return the moEndSBS.
     */
    public MOEndSBS getMOEndSBS() {
        return moEndSBS;
    }

    /**
     * Gets the moItemSBS.
     * 
     * @category getter
     * @return the moItemSBS.
     */
    public MOItemSBS getMOItemSBS() {
        return moItemSBS;
    }

    /**
     * Sets the chronoSBS.
     * 
     * @category setter
     * @param chronoSBS chronoSBS.
     */
    public void setChronoSBS(final ChronoSBS chronoSBS) {
        this.chronoSBS = chronoSBS;
    }

    /**
     * Sets the moEndSBS.
     * 
     * @category setter
     * @param moEndSBS moEndSBS.
     */
    public void setMOEndSBS(final MOEndSBS moEndSBS) {
        this.moEndSBS = moEndSBS;
    }

    /**
     * Sets the moItemSBS.
     * 
     * @category setter
     * @param moItemSBS moItemSBS.
     */
    public void setMOItemSBS(final MOItemSBS moItemSBS) {
        this.moItemSBS = moItemSBS;
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();

        moEndSBS = (MOEndSBS) bf.getBean("MOEndSBSImpl");
        moItemSBS = (MOItemSBS) bf.getBean("MOItemSBSImpl");
        chronoSBS = (ChronoSBS) bf.getBean("chronoSBSImpl");
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        this.bfr.release();
    }

    @Test
    public void testFinishAndResumeOperationItem() throws DAOException, ServerBusinessException {

        int chono = 114;

        // Operation item finishing

        OperationItemKey operationItemKey1 = new OperationItemKey(ManagementType.REAL,
                new EstablishmentKey("TG", "01"), new Chrono(chronoSBS.getPrechro(getCtx(),
                        PrechroId.PRODUCTION.getValue()), chono), 1, 1, 1, 1);

        AbstractOperationItemBean operationItemBean1 = getMOItemSBS().getOperationItem(getCtx(), operationItemKey1);
        ManagementType managementTypeBeforeFinish = operationItemBean1.getOperationItemKey().getManagementType();

        String information = getMOEndSBS().finishOperationItem(getCtx(), operationItemKey1, "PENTUNIT",
                ActivityItemType.INPUT);

        evictAll();

        VIFContext vifContext = getCtx();
        // To avoid funtions' rights problems
        vifContext.getIdCtx().setLogin("VIF");

        OperationItemKey operationItemKey2 = new OperationItemKey(ManagementType.ARCHIVED, new EstablishmentKey("TG",
                "01"), new Chrono(chronoSBS.getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), chono), 1, 1, 1, 1);
        AbstractOperationItemBean operationItemBean2 = getMOItemSBS().getOperationItem(vifContext, operationItemKey2);
        ManagementType managementTypeAfterFinish = operationItemBean2.getOperationItemKey().getManagementType();

        assertEquals("A bad information is given by the finishing.", "", information);
        assertNotSame("Finishing hasn't changed the management type.", managementTypeBeforeFinish,
                managementTypeAfterFinish);

        // Operation item resumption
        getMOEndSBS().resumeOperationItem(vifContext, operationItemKey2, "PENTUNIT");

        evictAll();

        AbstractOperationItemBean operationItemBean3 = getMOItemSBS().getOperationItem(vifContext, operationItemKey1);
        ManagementType managementTypeAfterResume = operationItemBean3.getOperationItemKey().getManagementType();

        assertNotSame("Resumption hasn't changed the management type.", managementTypeAfterFinish,
                managementTypeAfterResume);

    }

    @Test
    public void testIsFinishedMO() {
        MOKey key = new MOKey();
        key.setEstablishmentKey(new EstablishmentKey("TG", "01"));

        try {
            key.setChrono(new Chrono("1OF", 20));
            boolean ret = getMOEndSBS().isFinishedMO(getCtx(), key, ActivityItemType.INPUT, "PENTUNIT");
            assertFalse(ret);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on isFinishedMO");
        }

        try {
            key.setChrono(new Chrono("1OF", 19));
            boolean ret = getMOEndSBS().isFinishedMO(getCtx(), key, ActivityItemType.INPUT, "PENTUNIT");
            assertTrue(ret);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on isFinishedMO");
        }
    }
}

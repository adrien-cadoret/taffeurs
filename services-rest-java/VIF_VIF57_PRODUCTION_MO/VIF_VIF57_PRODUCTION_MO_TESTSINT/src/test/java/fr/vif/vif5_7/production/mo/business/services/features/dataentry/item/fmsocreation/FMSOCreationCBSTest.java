/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FMSOCreationCBSTest.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmsocreation;


import java.util.Locale;

import junit.framework.TestCase;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FlowType;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaReturnInitBean;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.unit.MOQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FabricationInfos;


/**
 * Test for FMOCreation CBS.
 * 
 * @author glc
 */
public class FMSOCreationCBSTest extends TestCase {
    private BeanFactoryReference bfr;
    private FMSOCreationCBS      cbs;
    private IdContext            idCtx;

    /**
     * Test createStartedMSO.
     */
    // public void testCreateStartedMO() throws Exception {
    // MOKey originMOKey = new MOKey();
    // originMOKey.setEstablishmentKey(new EstablishmentKey("ZZ", "01"));
    // originMOKey.setChrono(new Chrono("1OF", 1982));
    // Fabrication fabrication = new Fabrication();
    // fabrication.setFabricationType(FabricationType.ACTIVITY);
    // fabrication.setFabricationCLs(new CodeLabels("DESOSG", "", ""));
    //
    // FMSOCreationVBean vbean = new FMSOCreationVBean(originMOKey, "", new CodeLabel("VMUSCLE003", ""), fabrication,
    // new QuantityUnit(1, "P"), new CriteriaReturnInitBean());
    // vbean.setReturnInitBean(cbs.getCriteriaReturnInitBean(idCtx, originMOKey));
    // MOBean moBean = cbs.createStartedMSO(idCtx, vbean);
    // assertNotNull(moBean);
    // assertEquals(1.0, moBean.getMoQuantityUnit().getToDoQuantity());
    // assertEquals("P", moBean.getMoQuantityUnit().getUnit());
    // assertEquals(fabrication.getFabricationType(), moBean.getFabrication().getFabricationType());
    // assertEquals(fabrication.getFabricationCLs().getCode(), moBean.getFabrication().getFabricationCLs().getCode());
    // }
    /**
     * Test getCriteriaReturnInitBean.
     */
    public void testGetCriteriaReturnInitBean() throws Exception {
        MOKey originMOKey = new MOKey();
        originMOKey.setEstablishmentKey(new EstablishmentKey("ZZ", "01"));
        originMOKey.setChrono(new Chrono("1OF", 2119));
        CriteriaReturnInitBean criteriaReturnInitBean = cbs.getCriteriaReturnInitBean(idCtx, originMOKey);
        assertNotNull(criteriaReturnInitBean);
    }

    /**
     * Test getFabricationInfos.
     */
    public void testGetFabricationInfos() throws Exception {
        MOBean originMOBean = new MOBean();
        MOKey originMOKey = new MOKey();
        originMOKey.setEstablishmentKey(new EstablishmentKey("ZZ", "01"));
        originMOKey.setChrono(new Chrono("1OF", 1982));
        originMOBean.setMoKey(originMOKey);
        MOQuantityUnit moQuantityUnit = new MOQuantityUnit();
        moQuantityUnit.setToDoQuantity(20);
        moQuantityUnit.setUnit("P");
        originMOBean.setMoQuantityUnit(moQuantityUnit);
        originMOBean.setFlowType(FlowType.PULLED);
        originMOBean.setItemCLs(new CodeLabels("VMUSCLE003", "", ""));
        originMOBean.getFabrication().setFamilyCL(new CodeLabel("actfam1", "label"));
        FabricationInfos fabricationInfos = cbs.getFabricationInfos(idCtx, originMOBean);
        assertNotNull(fabricationInfos);
        System.out.println(fabricationInfos);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();
        cbs = (FMSOCreationCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmsocreation.FMSOCreationCBS");
        idCtx = new IdContext("ZZ", "01", Locale.FRANCE, "VIF");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}

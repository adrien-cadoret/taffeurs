/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FMOCreationCBSTest02.java,v $
 * Created on 24 nov. 08 by glc
 */
package fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmocreation;


import java.util.List;
import java.util.Locale;

import junit.framework.TestCase;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.vif5_7.activities.activities.business.beans.common.fabrication.Fabrication;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FabricationType;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FlowType;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fmocreation.FMOCreationVBean;


/**
 * Test for FMOCreation CBS.
 * 
 * @author glc
 */
public class FMOCreationCBSTest02 extends TestCase {
    private BeanFactoryReference bfr;
    private FMOCreationCBS       cbs;
    private IdContext            idCtx;

    /**
     * Test createStartedMO.
     */
    public void testCreateStartedMO() throws Exception {
        idCtx = new IdContext("VR", "08", Locale.FRANCE, "VIF", "");
        FMOCreationVBean vbean = new FMOCreationVBean();
        vbean.setItem("08200001");
        vbean.setFlowType(FlowType.PULLED);
        Fabrication fabrication = new Fabrication();
        fabrication.setFabricationType(FabricationType.ACTIVITY);
        fabrication.setFabricationCLs(new CodeLabels("08200001", "", ""));
        vbean.setFabrication(fabrication);
        vbean.setQuantityUnit(new QuantityUnit(150, "Kg"));
        vbean.setReturnInitBean(cbs.getCriteriaReturnInitBean(idCtx, "VR", "08", "VIF", "08200001"));
        MOBean moBean = cbs.createStartedMO(idCtx, vbean);
        assertNotNull(moBean);
        assertEquals("08200001", moBean.getItemCLs().getCode());
        assertEquals(150.0, moBean.getMoQuantityUnit().getToDoQuantity());
        assertEquals("KG", moBean.getMoQuantityUnit().getUnit());
        assertEquals(FlowType.PULLED, moBean.getFlowType());
        assertEquals(FabricationType.ACTIVITY, moBean.getFabrication().getFabricationType());
        assertEquals("08200001", moBean.getFabrication().getFabricationCLs().getCode());
        assertTrue(moBean.getMoKey().getChrono().getChrono() > 0);
    }

    //
    // /**
    // * Test getFabricationsInfos.
    // */
    // public void testGetFabricationInfos() throws Exception {
    // FabricationInfos fabricationInfos = cbs.getFabricationInfos(idCtx, "ZZ", "01", "DED", "VMUSCLE003",
    // new ArrayList<CriteriaCompleteBean>());
    // assertNotNull(fabricationInfos);
    // assertEquals(FlowType.PULLED, fabricationInfos.getFlowType());
    // assertEquals(FabricationType.ACTIVITY, fabricationInfos.getFabrication().getFabricationType());
    // assertEquals("DESOSG", fabricationInfos.getFabrication().getFabricationCLs().getCode());
    // assertEquals("P", fabricationInfos.getQuantityUnit().getUnit());
    // }

    /**
     * Test queryElements.
     */
    public void testQueryElements() throws Exception {
        List<CodeLabel> list = cbs.queryElements(idCtx, "VR", "18", "SAU01");
        assertNotNull(list);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();
        cbs = (FMOCreationCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmocreation.FMOCreationCBS");
        idCtx = new IdContext("VR", "08", Locale.FRANCE, "VIF");
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}

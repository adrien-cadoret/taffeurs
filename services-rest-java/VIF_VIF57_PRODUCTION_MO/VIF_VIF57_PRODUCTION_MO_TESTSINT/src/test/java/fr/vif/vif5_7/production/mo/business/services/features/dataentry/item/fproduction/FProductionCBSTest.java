/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FProductionCBSTest.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction;


import java.util.List;

import junit.framework.TestCase;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;


public class FProductionCBSTest extends TestCase {
    String                       menuString;

    private BeanFactoryReference bfr;
    private FProductionCBS       cbs;
    private IdContext            idContext;

    public void testQueryElements01() {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("TG", "04"));
        moKey.setChrono(new Chrono("1OF", 1));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("NONO");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        try {
            lst = cbs.queryElements(idContext, sBean, 0, 10000);
            assertNotNull(lst);
            assertTrue(lst.size() == 0);
        } catch (BusinessException e) {
            fail("Error querying inputoperationitem : " + e);
        }
    }

    public void testQueryElements02() {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("TG", "04"));
        moKey.setChrono(new Chrono("1OF", 1));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        try {
            lst = cbs.queryElements(idContext, sBean, 0, 10000);
            assertNotNull(lst);
            assertTrue(lst.size() == 1);
        } catch (BusinessException e) {
            fail("Error querying inputoperationitem : " + e);
        }
    }

    public void testQueryElements03() {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("TG", "04"));
        moKey.setChrono(new Chrono("1OF", 1));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("404");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        try {
            lst = cbs.queryElements(idContext, sBean, 0, 10000);
            assertNotNull(lst);
            assertTrue(lst.size() == 1);
        } catch (BusinessException e) {
            fail("Error querying inputoperationitem : " + e);
        }
    }

    public void testQueryElements04() {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("TG", "04"));
        moKey.setChrono(new Chrono("1OF", 25));
        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("404");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        try {
            lst = cbs.queryElements(idContext, sBean, 0, 10000);
            assertNotNull(lst);
            assertTrue(lst.size() == 0);
        } catch (BusinessException e) {
            fail("Error querying inputoperationitem : " + e);
        }

        sBean.setAll(true);
        try {
            lst = cbs.queryElements(idContext, sBean, 0, 10000);
            assertNotNull(lst);
            assertTrue(lst.size() == 1);
        } catch (BusinessException e) {
            fail("Error querying inputoperationitem : " + e);
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();
        cbs = (FProductionCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBS");
        idContext = new IdContext("TG", "01", I18nServerManager.FRANCE, "vif");

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

}

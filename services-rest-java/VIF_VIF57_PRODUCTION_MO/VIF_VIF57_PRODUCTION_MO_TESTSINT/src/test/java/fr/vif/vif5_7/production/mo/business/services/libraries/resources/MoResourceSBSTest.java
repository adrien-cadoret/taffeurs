/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MoResourceSBSTest.java,v $
 * Created on 13 févr. 09 by gv
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.resources;


import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.util.test.SBSTestCase;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceBean;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceSBean;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;


/**
 * The MO resource SBS test class.
 *
 * @author gv
 */
public class MoResourceSBSTest extends SBSTestCase {
    private BeanFactoryReference bfr;

    private MOSBS                moSBS;
    MoResourceSBS                moResourceSBS = null;

    @Override
    public void setUp() throws Exception {
        super.setUp();

        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();

        moResourceSBS = (MoResourceSBS) bf.getBean("moResourceSBSImpl");
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        bfr.release();
    }

    public void testListResources() throws Exception {

        MOResourceSBean selection = new MOResourceSBean();

        selection.setEstablishmentKey(new EstablishmentKey("SA", "01"));
        selection.setSelectionOn(MOResourceSBean.SelectionOn.MO);
        selection.getLstChrono().add(new Chrono("1OF", 37));
        selection.getLstChrono().add(new Chrono("1OF", 38));

        List<MOResourceBean> lst = moResourceSBS.listResources(getCtx(), selection);

        assertNotNull(lst);
        assertEquals(4, lst.size());

    }
}

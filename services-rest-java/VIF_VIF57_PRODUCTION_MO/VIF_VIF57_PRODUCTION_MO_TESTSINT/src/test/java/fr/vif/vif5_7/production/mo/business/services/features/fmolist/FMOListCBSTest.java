/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FMOListCBSTest.java,v $
 * Created on 6 nov. 08 by alb
 */
package fr.vif.vif5_7.production.mo.business.services.features.fmolist;


import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import junit.framework.TestCase;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.common.fmolist.FMOListSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionmolist.FMOListVBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.common.fmolist.FMOListCBSImpl;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionmo.FProductionMOListCBSImpl;


/**
 * Tests for MOListCBS.
 * 
 * @author alb
 */
public class FMOListCBSTest extends TestCase {
    private static final Logger      LOGGER           = Logger.getLogger(FMOListCBSTest.class);

    private BeanFactoryReference     bfr;

    private EstablishmentKey         establishmentKey = new EstablishmentKey("TG", "01");
    private IdContext                idCtx;

    /* CHECKSTYLE:OFF */
    /**
     * The Lib.
     */
    private FMOListCBSImpl           listCBSImpl      = null;
    private FProductionMOListCBSImpl moCBSImpl        = null;

    /**
     * Gets the listCBSImpl.
     * 
     * @category getter
     * @return the listCBSImpl.
     */
    public final FMOListCBSImpl getListCBSImpl() {
        return listCBSImpl;
    }

    /**
     * Gets the moCBSImpl.
     * 
     * @category getter
     * @return the moCBSImpl.
     */
    public FProductionMOListCBSImpl getMoCBSImpl() {
        return moCBSImpl;
    }

    /**
     * Sets the listCBSImpl.
     * 
     * @category setter
     * @param listCBSImpl listCBSImpl.
     */
    public final void setListCBSImpl(final FMOListCBSImpl listCBSImpl) {
        this.listCBSImpl = listCBSImpl;
    }

    /**
     * Sets the moCBSImpl.
     * 
     * @category setter
     * @param moCBSImpl moCBSImpl.
     */
    public void setMoCBSImpl(final FProductionMOListCBSImpl moCBSImpl) {
        this.moCBSImpl = moCBSImpl;
    }

    // @SuppressWarnings("deprecation")
    // public void testGetComment() {
    // MOKey key = new MOKey();
    // key.setEstablishmentKey(establishmentKey);
    // key.setChrono(new Chrono("1OF", 1372));
    // try {
    // Comment comment = listCBSImpl.getComment(idCtx, establishmentKey.getCsoc(), establishmentKey.getCetab(),
    // key);
    // assertEquals(comment.getCom(), "TEST-JUNIT");
    // } catch (BusinessException e) {
    // fail(e.getMessage());
    // }
    // }

    public void testConvert() {
        FMOListSBean selection = new FMOListSBean();
        selection.setCompanyId(establishmentKey.getCsoc());
        selection.setEstablishmentId(establishmentKey.getCetab());
        selection.setChrono(1372);
        List<FMOListBBean> list2 = new ArrayList<FMOListBBean>();
        try {
            list2 = listCBSImpl.queryElements(idCtx, selection, 0, 50);
            FMOListVBean bean = moCBSImpl.convert(idCtx, establishmentKey.getCsoc(), "404",
                    establishmentKey.getCetab(), list2.get(0));
            assertNotNull(bean);
            // TODO Test unitaires sur les critères...
        } catch (BusinessException e) {
            fail(e.getMessage());
        }
    }

    // @SuppressWarnings("deprecation")
    // public void testFinishMO() {
    // // TODO Lancer ce test unitaire lorsqu'il sera possible de déquittancer un OF
    // try {
    // MOKey key = new MOKey();
    // key.setEstablishmentKey(new EstablishmentKey("AF", "02"));
    // key.setChrono(new Chrono("1OF", 19));
    // String information = moCBSImpl.finishResumeMO(idCtx, key, "401", null);
    // assertEquals("", information);
    // moCBSImpl.finishResumeMO(idCtx, key, "401", null);
    // } catch (BusinessException e) {
    // fail(e.getMessage());
    // }
    // }

    public void testQueryElements1() {
        FMOListSBean selection = new FMOListSBean();
        selection.setCompanyId(establishmentKey.getCsoc());
        selection.setEstablishmentId(establishmentKey.getCetab());
        Date date = new GregorianCalendar(2007, 10, 27).getTime();
        selection.setMoDate(date);
        try {
            List<FMOListBBean> list = listCBSImpl.queryElements(idCtx, selection, 0, 50);
            assertEquals(50, list.size());
        } catch (BusinessException e) {
            fail(e.getMessage());
        }
    }

    public void testQueryElements2() {
        FMOListSBean selection = new FMOListSBean();
        selection.setCompanyId(establishmentKey.getCsoc());
        selection.setEstablishmentId(establishmentKey.getCetab());
        Date date = new GregorianCalendar(2007, 10, 27).getTime();
        selection.setMoDate(date);
        selection.setWorkstationId("PENTUNIT");
        try {

            List<FMOListBBean> list = listCBSImpl.queryElements(idCtx, selection, 0, 300);
            assertEquals(58, list.size());
        } catch (BusinessException e) {
            fail(e.getMessage());
        }
    }

    public void testQueryElements3() {
        FMOListSBean selection = new FMOListSBean();
        selection.setCompanyId(establishmentKey.getCsoc());
        selection.setEstablishmentId(establishmentKey.getCetab());
        Date date = new GregorianCalendar(2008, 0, 11).getTime();
        selection.setMoDate(date);
        selection.setWorkstationId("PENTUNIT");
        selection.setShowFinished(true);
        try {

            List<FMOListBBean> list = listCBSImpl.queryElements(idCtx, selection, 0, 300);
            assertEquals(26, list.size());
        } catch (BusinessException e) {
            fail(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();

        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();

        listCBSImpl = (FMOListCBSImpl) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.common.fmolist.FMOListCBS");
        idCtx = new IdContext("TG", "01", I18nServerManager.FRANCE, "ALB");

        moCBSImpl = (FProductionMOListCBSImpl) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionmo.FProductionMOListCBS");
        idCtx = new IdContext("TG", "01", I18nServerManager.FRANCE, "ALB");

    }

    /**
     * {@inheritDoc}
     */
    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        this.bfr.release();
    }
}

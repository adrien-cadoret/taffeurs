/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: ProductionMOSBSTestSuite.java,v $
 * Created on 26 nov. 07 by ped
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.vif.vif5_7.production.mo.business.services.libraries.end.MOEndSBSTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.label.LabelSBSTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.operationitem.MOItemContainerSBSTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.operationitem.MOItemSBSTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.resources.MoResourceSBSTest;


/**
 * SBS Test suite for manufacturing order packet.
 * 
 * @author ped
 */
@RunWith(Suite.class)
@SuiteClasses({ MOEndSBSTest.class, //
        LabelSBSTest.class, //
        // MOSBSTest.class, //
        MOItemSBSTest.class, //
        MOItemContainerSBSTest.class, //
        MoResourceSBSTest.class //
})
public class ProductionMOSBSTestSuite {

}

/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MOItemSBSTest.java,v $
 * Created on 26 nov. 07 by ped
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.operationitem;


import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.AbstractVIFContextAdapter;
import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.util.test.SBSTestCase;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.services.libraries.chrono.ChronoSBS;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.BatchCheckBean;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.ItemMovementBean;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.ItemMovementKey;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.ItemMovementQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.MOFinishedBean;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.MOStockCheckBean;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.RowidKey;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.SearchLabel;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.PrechroId;
import fr.vif.vif5_7.production.mo.constants.Mnemos.Progress;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ReasonType;


public class MOItemSBSTest extends SBSTestCase {

    /**
     * 
     * VifContextAdapater for testcase.
     * 
     * @author fl
     * @version $Revision: 1.10 $, $Date: 2016/02/17 08:07:20 $
     */
    class TestCaseContextAdapter extends AbstractVIFContextAdapter {
        /**
         * {@inheritDoc}
         */
        @Override
        protected void close(final VIFContext ctx) throws BusinessException {
            super.close(ctx);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void commit(final VIFContext ctx) throws DAOException {
            super.commit(ctx);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected VIFContext createVIFContext(final IdContext idCtx) throws BusinessException {
            return super.createVIFContext(idCtx);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void evictAll(final VIFContext ctx) throws DAOException {
            super.evictAll(ctx);
        }

        /**
         * {@inheritDoc}
         */
        @Override
        protected void rollback(final VIFContext ctx) throws DAOException {
            super.rollback(ctx);
        }
    }

    private BeanFactoryReference bfr;
    private ChronoSBS            chronoSBS;
    private MOItemSBS            moItemSBS;

    private MOSBS                moSBS;

    /**
     * Gets the chronoSBS.
     * 
     * @category getter
     * @return the chronoSBS.
     */
    public ChronoSBS getChronoSBS() {
        return chronoSBS;
    }

    /**
     * Gets the moItemSBS.
     * 
     * @category getter
     * @return the moItemSBS.
     */
    public MOItemSBS getMOItemSBS() {
        return moItemSBS;
    }

    /**
     * Gets the moSBS.
     * 
     * @category getter
     * @return the moSBS.
     */
    public MOSBS getMOSBS() {
        return moSBS;
    }

    /**
     * Sets the chronoSBS.
     * 
     * @category setter
     * @param chronoSBS chronoSBS.
     */
    public void setChronoSBS(final ChronoSBS chronoSBS) {
        this.chronoSBS = chronoSBS;
    }

    /**
     * Sets the moItemSBS.
     * 
     * @category setter
     * @param moItemSBS moItemSBS.
     */
    public void setMoItemSBS(final MOItemSBS moItemSBS) {
        this.moItemSBS = moItemSBS;
    }

    /**
     * Sets the moSBS.
     * 
     * @category setter
     * @param moSBS moSBS.
     */
    public void setMoSBS(final MOSBS moSBS) {
        this.moSBS = moSBS;
    }

    @Override
    public void setUp() throws Exception {
        super.setUp();

        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();

        chronoSBS = (ChronoSBS) bf.getBean("chronoSBSImpl");
        moSBS = (MOSBS) bf.getBean("MOSBSImpl");
        moItemSBS = (MOItemSBS) bf.getBean("MOItemSBSImpl");
    }

    @Override
    public void tearDown() throws Exception {
        super.tearDown();
        bfr.release();
    }

    @Test
    public void testCheckBatch() {
        try {
            // Test on a good batch without numerotation
            ItemMovementBean itemMovementBean1 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP2", "Blanc de poulet"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean1 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean1,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean1);
            assertEquals("Return code isn't 00.", "00", batchCheckBean1.getReturnCode());

            // Test on a good batch with numerotation
            ItemMovementBean itemMovementBean2 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "80230001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("06110294", "Boc180 TERRINE CONFIT OIE ST"), new CodeLabel("",
                                            ""), new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean2 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean2,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean2);
            assertEquals("Return code isn't 00.", "00", batchCheckBean2.getReturnCode());

            // Test on an unexisting item
            ItemMovementBean itemMovementBean3 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("ZZFALSEITEM", "False item for PED"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean3 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean3,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean3);
            assertEquals("Return code isn't 10.", "10", batchCheckBean3.getReturnCode());

            // Test on an item not managed by batch
            ItemMovementBean itemMovementBean4 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("06209103", "TERRINE RECT 200G ROUGIE VIDE"), new CodeLabel("",
                                            ""), new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean4 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean4,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean4);
            assertEquals("Return code isn't 11.", "11", batchCheckBean4.getReturnCode());

            // Test on an item with a bad group
            ItemMovementBean itemMovementBean5 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("ZZTESTPED", "Test Pierre-Etienne"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean5 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean5,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean5);

            // Test on an item with an unexisting reference item
            ItemMovementBean itemMovementBean6 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("ZZTESTPED2", "Test Pierre-Etienne 2"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean6 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean6,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean6);
            assertEquals("Return code isn't 13.", "13", batchCheckBean6.getReturnCode());

            // Test on a depository not managed by batch
            ItemMovementBean itemMovementBean7 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("999",
                                    "Test dépôt extérieur"), new CodeLabel("TSTGP2", "Blanc de poulet"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean7 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean7,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean7);
            assertEquals("Return code isn't 14.", "14", batchCheckBean7.getReturnCode());

            // Test with no batch
            ItemMovementBean itemMovementBean8 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP2", "Blanc de poulet"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean8 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean8,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean8);
            assertEquals("Return code isn't 20.", "20", batchCheckBean8.getReturnCode());

            // Test with no batch
            ItemMovementBean itemMovementBean9 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest002", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP2", "Blanc de poulet"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean9 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean9,
                    ActivityItemType.INPUT, "PENTUNIT");
            assertNotNull("Bean returned is null.", batchCheckBean9);
            assertEquals("Return code isn't 21.", "21", batchCheckBean9.getReturnCode());

            // Test with a bad numerotation on the batch
            ItemMovementBean itemMovementBean10 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("04208108", "Coeur de Volaille"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean10 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean10,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean10);
            // Change from 22 to 21 after changes in PROGRESS proxies
            // => parameter OFDEC2[19] is not read anymore for this procedure
            assertEquals("Return code isn't 21.", "21", batchCheckBean10.getReturnCode());

            // Test with an already existing batch
            ItemMovementBean itemMovementBean11 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest003", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP4", "Blanc de poulet 2"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean11 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean11,
                    ActivityItemType.OUTPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean11);
            assertEquals("Return code isn't 23.", "23", batchCheckBean11.getReturnCode());

            // Test with an already existing batch but with another reference item
            ItemMovementBean itemMovementBean12 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP2", "Blanc de poulet"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean12 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean12,
                    ActivityItemType.OUTPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean12);
            assertEquals("Return code isn't 24.", "24", batchCheckBean12.getReturnCode());

            // Test with an unaccording batch with day batches
            ItemMovementBean itemMovementBean13 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "80250002", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("ZZTESTPED5", "Test Pierre-Etienne 5"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
            BatchCheckBean batchCheckBean13 = getMOItemSBS().checkBatch(getCtx(), itemMovementBean13,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", batchCheckBean13);
            // Change from 25 to 00 after changes in PROGRESS proxies
            // => parameter OFDEC2[19] is not read anymore for this procedure
            assertEquals("Return code isn't 00.", "00", batchCheckBean13.getReturnCode());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on checkBatch");
        }
    }

    @Test
    public void testCheckDepositoryClosing() {
        try {
            // Test with an unclosed depository
            ItemMovementBean itemMovementBean1 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 164), 1, 2, 1, 2, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));

            getMOItemSBS().checkDepositoryClosing(getCtx(), itemMovementBean1, ActivityItemType.INPUT, "PENTUNIT");

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on checkDepositoryClosing");
        }

        try {
            // Test with a closed depository
            ItemMovementBean itemMovementBean1 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 164), 1, 2, 1, 2, ""), "lotTest001", new CodeLabel("292",
                                    "Test Pierre-Etienne"), new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));

            getMOItemSBS().checkDepositoryClosing(getCtx(), itemMovementBean1, ActivityItemType.INPUT, "PENTUNIT");

            fail("Depository 292 must be closed at 11/01/2008 00:01");
        } catch (ServerBusinessException e) {
        }
    }

    @Test
    public void testCheckDowngradedItem() {
        try {
            // Test with a good downgraded item
            AbstractOperationItemBean operationItemBean1 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 164), 1, 2, 1, 2, "lotTest001", new CodeLabels(
                                    "ZZTESTPED2", "Test Pierre-Etienne 2", "Article VIF"), new CodeLabel("211",
                                            "PROD - Semi finis"), new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0,
                                                    0, 0, ""), false);

            getMOItemSBS().checkDowngradedItem(getCtx(), operationItemBean1, "PENTUNIT");

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on checkDowngradedItem");
        }

        try {
            // Test with a bad downgraded item
            AbstractOperationItemBean operationItemBean2 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 164), 1, 2, 1, 2, "lotTest001", new CodeLabels(
                                    "ZZTESTPED5", "Test Pierre-Etienne 5", "Article VIF"), new CodeLabel("211",
                                            "PROD - Semi finis"), new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0,
                                                    0, 0, ""), false);

            getMOItemSBS().checkDowngradedItem(getCtx(), operationItemBean2, "PENTUNIT");

            fail("Item ZZTESTPED mustn't be a downgraded item of item TSTGP2");

        } catch (ServerBusinessException e) {
        }
    }

    @Test
    public void testCheckItemMovement() throws ServerBusinessException {

        OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG", "01"),
                new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 89), 1, 1, 1, 1);
        try {
            MOStockCheckBean mOStockCheckBean1 = getMOItemSBS().checkItemMovement(
                    getCtx(),
                    new ItemMovementBean(new ItemMovementKey(operationItemKey.getManagementType(), operationItemKey
                            .getEstablishmentKey(), operationItemKey.getChrono(), operationItemKey.getCounter1(),
                            operationItemKey.getCounter2(), operationItemKey.getCounter3(), operationItemKey
                            .getCounter4(), ""), "Lot001", new CodeLabel("211", "PROD - Semi finis"),
                            new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""), new CodeLabel("", ""),
                            new Date(), new ItemMovementQuantityUnit(50, "Kg", 0, "", 0, "")), ActivityItemType.INPUT,
                    "PENTUNIT");

            assertEquals("Checking isn't OK.", true, mOStockCheckBean1.getIsOK());
            assertEquals("Checking has returned some errors.", 0, mOStockCheckBean1.getErrors().getErrorCount());

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on checkItemMovement");
        }

        try {
            // Test with a stock unknown batch
            MOStockCheckBean mOStockCheckBean2 = getMOItemSBS().checkItemMovement(
                    getCtx(),
                    new ItemMovementBean(new ItemMovementKey(operationItemKey.getManagementType(), operationItemKey
                            .getEstablishmentKey(), operationItemKey.getChrono(), operationItemKey.getCounter1(),
                            operationItemKey.getCounter2(), operationItemKey.getCounter3(), operationItemKey
                            .getCounter4(), ""), "LotPEDTestUnitUnKnown", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(50, "Kg", 0, "", 0, "")),
                                    ActivityItemType.INPUT, "PENTUNIT");

            // assertEquals("Checking isn't OK.", true, mOStockCheckBean2.getIsOK());
            // assertEquals("Checking hasn't returned one error.", 1, mOStockCheckBean2.getErrors().getErrorCount());
            fail("Checking hasn't returned one error.");
        } catch (ServerBusinessException e) {
            assertTrue("Error message isn't OK.", e.getMessage().contains("(018207)"));
            // fail("ServerBusinessException on checkItemMovement");
        }

        try {
            // Test with an stock unavailable quantity
            MOStockCheckBean mOStockCheckBean3 = getMOItemSBS().checkItemMovement(
                    getCtx(),
                    new ItemMovementBean(new ItemMovementKey(operationItemKey.getManagementType(), operationItemKey
                            .getEstablishmentKey(), operationItemKey.getChrono(), operationItemKey.getCounter1(),
                            operationItemKey.getCounter2(), operationItemKey.getCounter3(), operationItemKey
                            .getCounter4(), ""), "Lot001", new CodeLabel("211", "PROD - Semi finis"),
                            new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""), new CodeLabel("", ""),
                            new Date(), new ItemMovementQuantityUnit(65000, "Kg", 0, "", 0, "")),
                            ActivityItemType.INPUT, "PENTUNIT");

            // assertEquals("Checking isn't OK.", true, mOStockCheckBean3.getIsOK());
            // assertEquals("Checking hasn't returned one error.", 1, mOStockCheckBean3.getErrors().getErrorCount());
            fail("Checking hasn't returned one error.");
        } catch (ServerBusinessException e) {
            assertTrue("Error message isn't OK.", e.getMessage().contains("(018974)"));
            // fail("ServerBusinessException on checkItemMovement " + e.getMessage());
        }
    }

    @Test
    public void testCheckSubstituteItem() {
        try {
            // Test with a substitute item on the activity
            AbstractOperationItemBean operationItemBean1 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 164), 1, 1, 1, 1, "lotTest001", new CodeLabels("TSTGP1",
                                    "Poulet entier", "Poulet"), new CodeLabel("211", "PROD - Semi finis"),
                                    new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0, 0, 0, ""), false);

            getMOItemSBS().checkSubstituteItem(getCtx(), operationItemBean1, "TSTGP11", "PENTUNIT");

            // Test with a substitute item on the item
            AbstractOperationItemBean operationItemBean2 = new OutputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 164), 1, 2, 1, 2, "lotTest001", new CodeLabels("TSTGP2",
                                    "Blanc de Poulet", "Blanc Poulet"), new CodeLabel("211", "PROD - Semi finis"),
                                    new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0, 0, 0, ""));

            getMOItemSBS().checkSubstituteItem(getCtx(), operationItemBean2, "ZZTESTPED", "PENTUNIT");

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on checkSubstituteItem");
        }

        try {
            // Test with a bad substitute item
            AbstractOperationItemBean operationItemBean3 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 164), 1, 1, 1, 1, "lotTest001", new CodeLabels("TSTGP1",
                                    "Poulet entier", "Poulet"), new CodeLabel("211", "PROD - Semi finis"),
                                    new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0, 0, 0, ""), false);

            getMOItemSBS().checkSubstituteItem(getCtx(), operationItemBean3, "ZZTESTPED2", "PENTUNIT");

            fail("Item ZZTESTPED2 mustn't be a substitute item of item TSTGP2");

        } catch (ServerBusinessException e) {
        }
    }

    @Test
    public void testDeleteItemMovement() {
        try {
            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 89), 1, 1,
                    1, 1);
            double qtyBeforeDeleting = getMOItemSBS().getOperationItem(getCtx(), operationItemKey)
                    .getOperationItemQuantityUnit().getCompletedQuantity();
            List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);
            int numberOfMovementsBeforeDeleting = listItemMovement.size();
            double qtyToDelete = listItemMovement.get(0).getItemMovementQuantityUnit().getFirstQuantity();
            String rowidMovement = listItemMovement.get(0).getItemMovementKey().getRowidKey();

            getMOItemSBS().deleteItemMovement(
                    getCtx(),
                    new ItemMovementKey(operationItemKey.getManagementType(), operationItemKey.getEstablishmentKey(),
                            operationItemKey.getChrono(), operationItemKey.getCounter1(), operationItemKey
                            .getCounter2(), operationItemKey.getCounter3(), operationItemKey.getCounter4(),
                            rowidMovement), "PENTUNIT");

            try {
                evictAll();
            } catch (DAOException e) {
                fail("DAOException on testDeleteItemMovement");
            }

            double qtyAfterDeleting = getMOItemSBS().getOperationItem(getCtx(), operationItemKey)
                    .getOperationItemQuantityUnit().getCompletedQuantity();
            int numberOfMovementsAfterDeleting = getMOItemSBS().listItemMovement(getCtx(), operationItemKey).size();

            assertEquals("Deleting hasn't deleted the good quantity.", qtyBeforeDeleting - qtyToDelete,
                    qtyAfterDeleting);
            assertEquals("Deleting hasn't deleted the item movement.", numberOfMovementsBeforeDeleting - 1,
                    numberOfMovementsAfterDeleting);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on deleteItemMovement");
        }
    }

    @Test
    public void testDeleteLast() {
        try {
            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 89), 1, 1,
                    1, 1);
            double qtyBeforeDeleting = getMOItemSBS().getOperationItem(getCtx(), operationItemKey)
                    .getOperationItemQuantityUnit().getCompletedQuantity();
            List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);
            int numberOfMovementsBeforeDeleting = listItemMovement.size();
            double qtyToDelete = listItemMovement.get(0).getItemMovementQuantityUnit().getFirstQuantity();
            String rowidMovement = listItemMovement.get(0).getItemMovementKey().getRowidKey();

            getMOItemSBS().deleteLast(
                    getCtx(),
                    new ItemMovementKey(operationItemKey.getManagementType(), operationItemKey.getEstablishmentKey(),
                            operationItemKey.getChrono(), operationItemKey.getCounter1(), operationItemKey
                            .getCounter2(), operationItemKey.getCounter3(), operationItemKey.getCounter4(),
                            rowidMovement), "PENTUNIT");

            try {
                evictAll();
            } catch (DAOException e) {
                fail("DAOException on testDeleteItemMovement");
            }

            double qtyAfterDeleting = getMOItemSBS().getOperationItem(getCtx(), operationItemKey)
                    .getOperationItemQuantityUnit().getCompletedQuantity();
            int numberOfMovementsAfterDeleting = getMOItemSBS().listItemMovement(getCtx(), operationItemKey).size();

            assertEquals("Deleting hasn't deleted the good quantity.", qtyBeforeDeleting - qtyToDelete,
                    qtyAfterDeleting);
            assertEquals("Deleting hasn't deleted the item movement.", numberOfMovementsBeforeDeleting - 1,
                    numberOfMovementsAfterDeleting);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on deleteLast" + e);
        }
    }

    @Test
    public void testGetOperationItem() {
        try {
            OperationItemKey operationItemKey1 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 21), 1, 1,
                    1, 1);

            AbstractOperationItemBean operationItemBean1 = getMOItemSBS().getOperationItem(getCtx(), operationItemKey1);

            assertNotNull("operationItemBean is null.", operationItemBean1);
            assertEquals("Chrono of the bean isn't 21.", 21, operationItemBean1.getOperationItemKey().getChrono()
                    .getChrono());
            assertEquals("Counter1 of the bean isn't 1.", 1, operationItemBean1.getOperationItemKey().getCounter1());
            assertEquals("Depository of the bean isn't 211.", "211", operationItemBean1.getDepotCL().getCode());

            OperationItemKey operationItemKey2 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 21), 1, 2,
                    1, 2);

            AbstractOperationItemBean operationItemBean2 = getMOItemSBS().getOperationItem(getCtx(), operationItemKey2);

            assertNotNull("operationItemBean is null.", operationItemBean2);
            assertEquals("Depository of the bean isn't 211.", "211", operationItemBean2.getDepotCL().getCode());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on getOperationItem");
        }
    }

    @Test
    public void testInitQuantityWithLeftToDoQuantity() {
        try {
            // Test on a good operation item
            AbstractOperationItemBean operationItemBean1 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 270), 1, 1, 1, 1, "lotTest017", new CodeLabels("TSTGP1",
                                    "Poulet entier", "Poulet"), new CodeLabel("211", "PROD - Semi finis"),
                                    new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0, 0, 0, ""), false);

            ItemMovementQuantityUnit itemMovementQuantityUnit1 = getMOItemSBS().initQuantityWithLeftToDoQuantity(
                    getCtx(), operationItemBean1);

            assertNotNull("List returned is null.", itemMovementQuantityUnit1);
            assertEquals("First quantity returned isn't 8,83 KG.", 8.83, itemMovementQuantityUnit1.getFirstQuantity());
            assertEquals("First quantity returned isn't 8,83 KG.", "KG", itemMovementQuantityUnit1.getFirstUnit()
                    .toUpperCase());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException oninitQuantityWithLeftToDoQuantity");
        }
    }

    @Test
    public void testInitQuantityWithStock() {
        try {
            // Test on item and batch in stock
            AbstractOperationItemBean operationItemBean1 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, "lotTest017", new CodeLabels("TSTGP2",
                                    "Blanc de Poulet", "Blanc Poulet"), new CodeLabel("211", "PROD - Semi finis"),
                                    new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0, 0, 0, ""), false);

            ItemMovementQuantityUnit itemMovementQuantityUnit1 = getMOItemSBS().initQuantityWithStock(getCtx(),
                    operationItemBean1);

            assertNotNull("List returned is null.", itemMovementQuantityUnit1);
            assertEquals("First quantity returned isn't 25,4 KG.", 25.4, itemMovementQuantityUnit1.getFirstQuantity());
            assertEquals("First quantity returned isn't 25,4 KG.", "KG", itemMovementQuantityUnit1.getFirstUnit());

            // Test on item and batch not in stock
            AbstractOperationItemBean operationItemBean2 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, "lotTest002", new CodeLabels(
                                    "04109102", "GRAISSE CAN DESOD FON", "NEU-TL"), new CodeLabel("211", "PROD - Semi finis"),
                                    new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0, 0, 0, ""), false);

            ItemMovementQuantityUnit itemMovementQuantityUnit2 = getMOItemSBS().initQuantityWithStock(getCtx(),
                    operationItemBean2);

            assertNotNull("List returned is null.", itemMovementQuantityUnit2);
            assertEquals("First quantity returned isn't 0 PAL.", 0.0, itemMovementQuantityUnit2.getFirstQuantity());
            assertEquals("First quantity returned isn't 0 PAL.", "PAL", itemMovementQuantityUnit2.getFirstUnit());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on initQuantityWithStock");
        }
    }

    @Test
    public void testInterpretBarcode() {
        try {
            // Test with a good barcode
            AbstractOperationItemBean operationItemBean1 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 168), 1, 1, 1, 1, "", new CodeLabels("TSTGP1",
                                    "Poulet entier", "Poulet"), new CodeLabel("211", "PROD - Semi finis"),
                                    new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0, 0, 0, ""), false);

            List<String> expectedUnitsList = new ArrayList<String>();
            expectedUnitsList.add("Kg");
            ItemMovementBean itemMovementBean1 = getMOItemSBS().interpretBarcode(getCtx(),
                    "]c1916000TSTGP1|10LotTest068|3103015000", operationItemBean1, expectedUnitsList, "PENTUNIT");

            assertNotNull("Bean returned is null.", itemMovementBean1);
            assertEquals("Batch in the barcode isn't interpreted.", "LotTest068", itemMovementBean1.getBatch());
            assertEquals("Quantity in the barcode isn't interpreted.", 15.0, itemMovementBean1
                    .getItemMovementQuantityUnit().getFirstQuantity());
            assertEquals("Quantity in the barcode isn't interpreted.", "Kg", itemMovementBean1
                    .getItemMovementQuantityUnit().getFirstUnit());

            // Test with a bad barcode
            AbstractOperationItemBean operationItemBean3 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 168), 1, 1, 1, 1, "", new CodeLabels("TSTGP1",
                                    "Poulet entier", "Poulet"), new CodeLabel("211", "PROD - Semi finis"),
                                    new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0, 0, 0, ""), false);

            ItemMovementBean itemMovementBean3 = getMOItemSBS().interpretBarcode(getCtx(),
                    "]c191600TSTGP1|0LotTe068|310301500", operationItemBean3, expectedUnitsList, "PENTUNIT");

            assertNotNull("Bean returned is null.", itemMovementBean3);
            assertEquals("A batch has been interpreted in the barcode.", "", itemMovementBean3.getBatch());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on interpretCode");
        }
    }

    @Test
    public void testInterpretItemFromBarcode() {
        try {
            String itemId = getMOItemSBS().interpretItemFromBarcode(getCtx(),
                    "]c1916000TSTGP1|10LotTest068|3103015000", "TG", "01", "PENTUNIT");

            assertNotNull(itemId);
            assertEquals("TSTGP1", itemId);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on interpretCode");
        }

        try {
            String itemId = getMOItemSBS().interpretItemFromBarcode(getCtx(), "]c191600TSTGP1|0LotTe068|310301500",
                    "TG", "01", "PENTUNIT");

            assertNotNull(itemId);
            assertEquals("", itemId);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on interpretCode");
        }
    }

    @Test
    public void testIsExistingBatchQuality() {
        EstablishmentKey establishmentKey = new EstablishmentKey("TG", "01");

        try {
            // Test on a batch with quality
            getMOItemSBS().isExistingBatchQuality(getCtx(), establishmentKey, "lotTest001", "TSTGP2", "PENTUNIT");
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on isExistingBatchQuality");
        }

        try {
            // Test on a batch without quality
            getMOItemSBS().isExistingBatchQuality(getCtx(), establishmentKey, "lotTest002", "TSTGP2", "PENTUNIT");

            fail("lotTest002 hasn't got quality.");
        } catch (ServerBusinessException e) {
        }
    }

    @Test
    public void testIsOperationItemFinished() {
        try {
            // Test on a good operation item
            ItemMovementBean itemMovementBean1 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(10.80, "Kg", 0, "", 0, ""));

            MOFinishedBean moFinishedBean1 = getMOItemSBS().isOperationItemFinished(getCtx(), itemMovementBean1,
                    ActivityItemType.INPUT, "PENTUNIT");

            assertNotNull("Bean returned is null.", moFinishedBean1);
            assertEquals("Operation item is considered as not finished.", true, moFinishedBean1.getIsFinished());
            assertTrue(moFinishedBean1.getQuestion().contains("10"));
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on isOperationItemFinished");
        }

        try {
            // Test with a too big quantity
            ItemMovementBean itemMovementBean2 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP2", "Blanc de poulet"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(200.0, "Kg", 0, "", 0, ""));

            getMOItemSBS().isOperationItemFinished(getCtx(), itemMovementBean2, ActivityItemType.INPUT, "PENTUNIT");

            fail("200 Kg mustn't be declared.");
        } catch (ServerBusinessException e) {
        }
    }

    @Test
    public void testListDowngradedItem() {
        try {
            // Test on an operation item which has a downgraded item
            OperationItemKey operationItemKey1 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 212), 1, 1,
                    1, 1);

            List<CodeLabel> listDowngradedItem1 = getMOItemSBS().listDowngradedItem(getCtx(), operationItemKey1,
                    "PENTUNIT", ActivityItemType.INPUT);

            assertNotNull("listDowngradedItem is null.", listDowngradedItem1);
            assertEquals("List of downgraded items returned hasn't 1 element.", 1, listDowngradedItem1.size());
            assertEquals("First item of downgraded items list isn't TSTGP11 - Poulet entier - substitution", "TSTGP11",
                    listDowngradedItem1.get(0).getCode());
            assertEquals("First item of downgraded items list isn't TSTGP11 - Poulet entier - substitution",
                    "Poulet entier - substitution", listDowngradedItem1.get(0).getLabel());

            // Test on an operation item which hasn't a downgraded item
            OperationItemKey operationItemKey2 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 212), 1, 2,
                    1, 3);

            List<CodeLabel> listDowngradedItem2 = getMOItemSBS().listDowngradedItem(getCtx(), operationItemKey2,
                    "PENTUNIT", ActivityItemType.OUTPUT);

            assertNotNull("listDowngradedItem is null.", listDowngradedItem2);
            assertEquals("List of substitute items returned has some elements.", 0, listDowngradedItem2.size());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on listDowngradedItem");
        }

        try {
            // Test on a bad operation item
            OperationItemKey operationItemKey3 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 212), 1, 2,
                    1, 4);

            getMOItemSBS().listDowngradedItem(getCtx(), operationItemKey3, "PENTUNIT", ActivityItemType.OUTPUT);

            fail("Operation item R-TG-01-1OF-212-1-1-1-1 doesn't exist");
        } catch (ServerBusinessException e) {
        }
    }

    @Test
    public void testListItemMovement() {
        try {
            // Movements list of existing operation items
            List<ItemMovementBean> listItemMovement1 = getMOItemSBS().listItemMovement(
                    getCtx(),
                    new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG", "01"), new Chrono(
                            getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 89), 1, 2, 1, 2));

            assertNotNull("listItemMovement is null.", listItemMovement1);
            assertEquals("List of item movements returned hasn't 3 elements.", 3, listItemMovement1.size());
            assertEquals("Batch of first element of item movements list isn't LotPEDTestUnit", "LotPEDTestUnit",
                    listItemMovement1.get(0).getBatch());
            assertEquals("Movement date of first element of item movements list isn't 17/12/2007",
                    new GregorianCalendar(2007, 11, 17, 15, 8).getTime(), listItemMovement1.get(0)
                    .getMovementDateTime());

            List<ItemMovementBean> listItemMovement2 = getMOItemSBS().listItemMovement(
                    getCtx(),
                    new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG", "01"), new Chrono(
                            getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 89), 1, 2, 1, 3));

            assertNotNull("listItemMovement2 is null.", listItemMovement2);
            assertEquals("List of item movements returned has some elements.", 0, listItemMovement2.size());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on listItemMovement");
        }

        try {
            // Movements list of an unexisting operation item
            getMOItemSBS().listItemMovement(
                    getCtx(),
                    new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG", "01"), new Chrono(
                            getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 89), 1, 2, 1, 17));

            fail("Record(s) found on an unexisting operation item");
        } catch (ServerBusinessException e) {
        }
    }

    @Test
    public void testListMOBatch() {
        try {
            AbstractOperationItemBean operationItemBean1 = new InputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 166), 1, 1, 1, 1, "", new CodeLabels("TSTGP1",
                                    "Poulet entier", "Poulet"), new CodeLabel("211", "PROD - Semi finis"),
                                    new CodeLabel("", ""), new Date(), new OperationItemQuantityUnit(0, 0, 0, ""), false);

            List<String> listMOBatch1 = getMOItemSBS().listMOBatch(getCtx(), operationItemBean1);

            assertNotNull("listMOBatch is null.", listMOBatch1);
            assertEquals("List of MO batches returned hasn't 1 element.", 1, listMOBatch1.size());
            assertEquals("First batch of the list isn't Lot001", "Lot001", listMOBatch1.get(0));

            AbstractOperationItemBean operationItemBean2 = new OutputOperationItemBean(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 166), 1, 2, 1, 2, "", new CodeLabels("", "", ""),
                            new CodeLabel("211", "PROD - Semi finis"), new CodeLabel("", ""), new Date(),
                            new OperationItemQuantityUnit(0, 0, 0, ""));

            List<String> listMOBatch2 = getMOItemSBS().listMOBatch(getCtx(), operationItemBean2);

            assertNotNull("listMOBatch is null.", listMOBatch2);
            assertEquals("List of MO batches returned hasn't 3 elements.", 3, listMOBatch2.size());
            assertEquals("Second batch of the list isn't LotT002", "lotT002", listMOBatch2.get(2));
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on listMOBatch");
        }
    }

    @Test
    public void testListOperationItem1() {
        try {
            MOKey moKey = new MOKey();
            moKey.setEstablishmentKey(new EstablishmentKey("TG", "01"));
            moKey.setChrono(new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 20));

            List<AbstractOperationItemBean> listOperationItemBean = getMOItemSBS().listOperationItem(
                    getCtx(),
                    moKey,
                    new OperationItemSBean(false, new ArrayList<ManagementType>(), null, ActivityItemType.OUTPUT,
                            "PSORUNIT"));

            assertNotNull("listOperationItemBean is null.", listOperationItemBean);
            assertEquals("List of OperationBean returned hasn't 2 elements.", 2, listOperationItemBean.size());
            assertEquals("First element of OperationItemBean list hasn't 100 as remaining quantity.",
                    Double.valueOf(100), listOperationItemBean.get(0).getOperationItemQuantityUnit()
                    .getLeftToDoQuantity());
            assertEquals("First element of OperationItemBean list hasn't KG as unitID", "KG", listOperationItemBean
                    .get(0).getOperationItemQuantityUnit().getUnit());

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on listOperationItem");
        }
    }

    @Test
    public void testListOperationItem2() {
        try {
            List<Chrono> listMoChrono = new ArrayList<Chrono>();
            listMoChrono.add(new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 20));
            listMoChrono.add(new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 21));

            List<AbstractOperationItemBean> listOperationItemBean = getMOItemSBS().listOperationItem(
                    getCtx(),
                    new EstablishmentKey("TG", "01"),
                    listMoChrono,
                    new OperationItemSBean(false, new ArrayList<ManagementType>(), null, ActivityItemType.OUTPUT,
                            "PSORUNIT"));

            assertNotNull("listOperationItemBean is null.", listOperationItemBean);
            assertEquals("List of OperationBean returned hasn't 2 elements.", 2, listOperationItemBean.size());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on listOperationItem");
        }
    }

    @Test
    public void testListProgressingOperationItem() {
        try {
            List<AbstractOperationItemBean> listOperationItemBean = getMOItemSBS().listProgressingOperationItem(
                    getCtx(), new EstablishmentKey("TG", "01"), ActivityItemType.INPUT, "PENTUNIT");
            assertNotNull("listProgressingOperationItemBean is null.", listOperationItemBean);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on listOperationItem");
        }
    }

    @Test
    public void testListReasonByType() {
        try {
            List<CodeLabel> listReason = getMOItemSBS().listReasonByType(getCtx(), "TG", ReasonType.STOCK);

            assertNotNull("listReason is null.", listReason);
            assertEquals("List of reasons returned hasn't 2 elements.", 2, listReason.size());
            assertEquals("Second reason of reasons list isn't STKVEN - stock vente", "STKVEN", listReason.get(1)
                    .getCode());
            assertEquals("Second reason of reasons list isn't STKVEN - stock vente", "stock vente", listReason.get(1)
                    .getLabel());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on listReasons");
        }
    }

    @Test
    public void testListSubstituteItem() {
        try {
            // Test on an operation item which has a substitute item
            OperationItemKey operationItemKey1 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 212), 1, 1,
                    1, 1);

            List<CodeLabel> listSubstituteItem1 = getMOItemSBS().listSubstituteItem(getCtx(), operationItemKey1,
                    "PENTUNIT", ActivityItemType.INPUT);

            assertNotNull("listSubstituteItem is null.", listSubstituteItem1);
            assertEquals("List of substitute items returned hasn't 1 element.", 1, listSubstituteItem1.size());
            assertEquals("First item of substitute items list isn't TSTGP11 - Poulet entier - substitution", "TSTGP11",
                    listSubstituteItem1.get(0).getCode());
            assertEquals("First item of substitute items list isn't TSTGP11 - Poulet entier - substitution",
                    "Poulet entier - substitution", listSubstituteItem1.get(0).getLabel());

            // Test on an operation item which hasn't a substitute item
            OperationItemKey operationItemKey2 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 212), 1, 2,
                    1, 3);

            List<CodeLabel> listSubstituteItem2 = getMOItemSBS().listSubstituteItem(getCtx(), operationItemKey2,
                    "PENTUNIT", ActivityItemType.OUTPUT);

            assertNotNull("listSubstituteItem is null.", listSubstituteItem2);
            assertEquals("List of substitute items returned has some elements.", 0, listSubstituteItem2.size());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on listSubstituteItem");
        }

        try {
            // Test on a bad operation item
            OperationItemKey operationItemKey3 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 212), 1, 2,
                    1, 4);

            getMOItemSBS().listSubstituteItem(getCtx(), operationItemKey3, "PENTUNIT", ActivityItemType.OUTPUT);

            fail("Operation item R-TG-01-1OF-212-1-1-1-1 doesn't exist");
        } catch (ServerBusinessException e) {
        }
    }

    // @Test
    // public void testPrintMovementLabel() {
    // try {
    // getMOSBS().initPrinter(getCtx(), "SORFABJ", "PSORUNIT");
    //
    // OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
    // "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 161), 1, 2,
    // 1, 2);
    //
    // List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);
    //
    // getMOItemSBS().printMovementLabel(getCtx(),
    // new RowidKey(listItemMovement.get(0).getItemMovementKey().getRowidKey()), "PSORUNIT",
    // ActivityItemType.OUTPUT);
    // } catch (ServerBusinessException e) {
    // fail("ServerBusinessException on printMovementLabel " + e);
    // }
    // }

    @Test
    public void testRunPrintActLabel() throws Exception {
        {
            // shouldn't print anything but should give chice of nb.
            getMOSBS().initPrinter(getCtx(), "ENTFABJ", "PENUNIT2");

            MOKey moKey = new MOKey();
            moKey.setChrono(new Chrono("1OF", 1048));
            moKey.setEstablishmentKey(new EstablishmentKey("TG", "01"));

            List<SearchLabel> searchLabel = getMOItemSBS().searchPrintActLabel(getCtx(), moKey, new RowidKey(),
                    "PENUNIT2", ActivityItemType.INPUT, LabelingEventType.MO_CREATION);
            assertNotNull(searchLabel);
            assertEquals(true, searchLabel.get(0).getAskForCopiesNumber());
            assertEquals("OFG", searchLabel.get(0).getLabelId());
            assertEquals("ehplaofg", searchLabel.get(0).getPrintProgramId());

            getMOItemSBS().runPrintActLabel(getCtx(), moKey, "PENUNIT2", ActivityItemType.INPUT, "OFG", "ehplaofg", 1);
        }
    }

    @Test
    public void testRunPrintMovementLabel() {
        try {
            getMOSBS().initPrinter(getCtx(), "SORFABJ", "PSORUNIT");

            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 161), 1, 2,
                    1, 2);

            List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);

            getMOItemSBS().runPrintMovementLabel(getCtx(),
                    new RowidKey(listItemMovement.get(0).getItemMovementKey().getRowidKey()), "PSORUNIT",
                    ActivityItemType.OUTPUT, "PED", "einf4ped", 1);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on runPrintMovementLabel");
        }
    }

    @Test
    public void testSaveItemMovement() {
        try {
            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 89), 1, 1,
                    1, 1);
            int numberOfMovementsBeforeSave = getMOItemSBS().listItemMovement(getCtx(), operationItemKey).size();
            double completedQuantityBeforeSave = getMOItemSBS().getOperationItem(getCtx(), operationItemKey)
                    .getOperationItemQuantityUnit().getCompletedQuantity();

            getMOItemSBS().saveItemMovement(
                    getCtx(),
                    new ItemMovementBean(new ItemMovementKey(operationItemKey.getManagementType(), operationItemKey
                            .getEstablishmentKey(), operationItemKey.getChrono(), operationItemKey.getCounter1(),
                            operationItemKey.getCounter2(), operationItemKey.getCounter3(), operationItemKey
                            .getCounter4(), ""), "LotPEDTestUnit", new CodeLabel("211", "PROD - Semi finis"),
                            new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""), new CodeLabel("", ""),
                            new Date(), new ItemMovementQuantityUnit(100, "Kg", 0, "", 0, "")), ActivityItemType.INPUT,
                    "PENTUNIT");
            try {
                evictAll();
            } catch (DAOException e) {
                fail("DAOException on testSaveItemMovement");
            }

            int numberOfMovementsAfterSave = getMOItemSBS().listItemMovement(getCtx(), operationItemKey).size();
            double completedQuantityAfterSave = getMOItemSBS().getOperationItem(getCtx(), operationItemKey)
                    .getOperationItemQuantityUnit().getCompletedQuantity();

            assertEquals("There is no new movement added to the operation item.", numberOfMovementsBeforeSave + 1,
                    numberOfMovementsAfterSave);
            assertEquals("100 Kg hasn't been added to the operation item.", completedQuantityBeforeSave + 100,
                    completedQuantityAfterSave);

            // Test with a substitute item
            getMOItemSBS().saveItemMovement(
                    getCtx(),
                    new ItemMovementBean(new ItemMovementKey(operationItemKey.getManagementType(), operationItemKey
                            .getEstablishmentKey(), operationItemKey.getChrono(), operationItemKey.getCounter1(),
                            operationItemKey.getCounter2(), operationItemKey.getCounter3(), operationItemKey
                            .getCounter4(), ""), "LotPEDTestUnit", new CodeLabel("211", "PROD - Semi finis"),
                            new CodeLabel("TSTGP11", "Poulet entier - substitution"), new CodeLabel("", ""),
                            new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(120, "Kg", 0, "", 0, "")),
                            ActivityItemType.INPUT, "PENTUNIT");

            try {
                evictAll();
            } catch (DAOException e) {
                fail("DAOException on testSaveItemMovement");
            }

            double completedQuantityAfterSecondSave = getMOItemSBS().getOperationItem(getCtx(), operationItemKey)
                    .getOperationItemQuantityUnit().getCompletedQuantity();
            assertEquals("120 Kg hasn't been added to the operation item.", completedQuantityAfterSave + 120,
                    completedQuantityAfterSecondSave);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on saveItemMovement");
        }
    }

    @Test
    public void testSearchBatchByDestockingModel() {
        try {
            // Test without batch
            ItemMovementBean itemMovementBean1 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "Kg", 0, "Kg", 0, "Kg"));

            String batchFound1 = getMOItemSBS().searchBatchByDestockingModel(getCtx(), itemMovementBean1, "PENTUNIT");

            assertNotNull("No batch has been found.", batchFound1);
            assertEquals("Batch found isn't lot001.", "Lot001", batchFound1);

            // Test with a good batch
            ItemMovementBean itemMovementBean2 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lot001", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "Kg", 0, "Kg", 0, "Kg"));

            String batchFound2 = getMOItemSBS().searchBatchByDestockingModel(getCtx(), itemMovementBean2, "PENTUNIT");

            assertNotNull("No batch has been found.", batchFound2);
            assertEquals("Batch found isn't lot001.", "Lot001", batchFound2);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on searchBatchByDestockingModel");
        }

        try {
            // Test with a bad batch
            ItemMovementBean itemMovementBean3 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 162), 1, 1, 1, 1, ""), "lotTest017", new CodeLabel("211",
                                    "PROD - Semi finis"), new CodeLabel("TSTGP2", "Poulet entier"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(0, "Kg", 0, "Kg", 0, "Kg"));

            getMOItemSBS().searchBatchByDestockingModel(getCtx(), itemMovementBean3, "PENTUNIT");

            fail("Batch lotTest017 mustn't be OK.");
        } catch (ServerBusinessException e) {
        }
    }

    @Test
    public void testSearchFirstInputBatch() {
        try {
            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 161), 1, 1,
                    1, 1);

            String batchFound = getMOItemSBS().searchFirstInputBatch(getCtx(), operationItemKey, "PSORUNIT");

            assertNotNull("No batch has been found.", batchFound);
            assertEquals("Batch found isn't Lot001.", "Lot001", batchFound);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on searchFirstInputBatch");
        }
    }

    @Test
    public void testSearchPrintActLabel() throws ServerBusinessException {
        {
            // this code should'nt print anything (cause there is no declaration on Manufacturing order 1048)
            getMOSBS().initPrinter(getCtx(), "ENTFABJ", "PENTUNIT");

            MOKey moKey = new MOKey();
            moKey.setChrono(new Chrono("1OF", 1048));
            moKey.setEstablishmentKey(new EstablishmentKey("TG", "01"));

            List<SearchLabel> searchLabel = getMOItemSBS().searchPrintActLabel(getCtx(), moKey, new RowidKey(),
                    "PENTUNIT", ActivityItemType.INPUT, LabelingEventType.MO_FIRST_DECLARATION);
            assertNotNull(searchLabel);
            assertEquals(0, searchLabel.size());
        }

        {
            // this should print something
            getMOSBS().initPrinter(getCtx(), "ENTFABJ", "PENTUNIT");

            MOKey moKey = new MOKey();
            moKey.setChrono(new Chrono("1OF", 1049));
            moKey.setEstablishmentKey(new EstablishmentKey("TG", "01"));

            List<SearchLabel> searchLabel = getMOItemSBS().searchPrintActLabel(getCtx(), moKey, new RowidKey(),
                    "PENTUNIT", ActivityItemType.INPUT, LabelingEventType.MO_FIRST_DECLARATION);
            assertNotNull(searchLabel);
            assertEquals(true, searchLabel.get(0).getAskForCopiesNumber());
            assertEquals("OFG", searchLabel.get(0).getLabelId());
            assertEquals("ehplaofg", searchLabel.get(0).getPrintProgramId());
        }
        {
            // shouldn't print anything but should give chice of nb.
            getMOSBS().initPrinter(getCtx(), "ENTFABJ", "PENUNIT2");

            MOKey moKey = new MOKey();
            moKey.setChrono(new Chrono("1OF", 1048));
            moKey.setEstablishmentKey(new EstablishmentKey("TG", "01"));

            List<SearchLabel> searchLabel = getMOItemSBS().searchPrintActLabel(getCtx(), moKey, new RowidKey(),
                    "PENUNIT2", ActivityItemType.INPUT, LabelingEventType.MO_CREATION);
            assertNotNull(searchLabel);
            assertEquals(true, searchLabel.get(0).getAskForCopiesNumber());
            assertEquals("OFG", searchLabel.get(0).getLabelId());
            assertEquals("ehplaofg", searchLabel.get(0).getPrintProgramId());
        }

    }

    @Test
    public void testSearchPrintMovementLabel() {
        try {
            getMOSBS().initPrinter(getCtx(), "SORFABJ", "PSORUNIT");

            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 161), 1, 2,
                    1, 2);

            List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);

            SearchLabel searchLabel = getMOItemSBS().searchPrintMovementLabel(getCtx(),
                    new RowidKey(listItemMovement.get(0).getItemMovementKey().getRowidKey()), "PSORUNIT",
                    ActivityItemType.OUTPUT);
            assertNotNull(searchLabel);
            assertEquals(false, searchLabel.getAskForCopiesNumber());
            assertEquals("", searchLabel.getLabelId());
            assertEquals("", searchLabel.getPrintProgramId());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on searchPrintMovementLabel");
        }

        try {
            getMOSBS().initPrinter(getCtx(), "ENTFABJ", "PENTUNIT");

            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 161), 1, 2,
                    1, 2);

            List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);

            SearchLabel searchLabel = getMOItemSBS().searchPrintMovementLabel(getCtx(),
                    new RowidKey(listItemMovement.get(0).getItemMovementKey().getRowidKey()), "PENTUNIT",
                    ActivityItemType.INPUT);
            assertNotNull(searchLabel);
            assertEquals(true, searchLabel.getAskForCopiesNumber());
            assertEquals("MVG", searchLabel.getLabelId());
            assertEquals("ehplamvg", searchLabel.getPrintProgramId());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on searchPrintMovementLabel");
        }

        try {
            getMOSBS().initPrinter(getCtx(), "SORFABJ", "PSORUNI2");

            OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 161), 1, 2,
                    1, 2);

            List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(getCtx(), operationItemKey);

            SearchLabel searchLabel = getMOItemSBS().searchPrintMovementLabel(getCtx(),
                    new RowidKey(listItemMovement.get(0).getItemMovementKey().getRowidKey()), "PSORUNI2",
                    ActivityItemType.OUTPUT);
            assertNotNull(searchLabel);
            assertEquals(true, searchLabel.getAskForCopiesNumber());
            assertEquals("PED", searchLabel.getLabelId());
            assertEquals("einf4ped", searchLabel.getPrintProgramId());
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on searchPrintMovementLabel");
        }

        // le code suivant m'a servi à appeler la lib ds le cas d'un test d'integ
        // try {
        //
        // TestCaseContextAdapter adapter = new TestCaseContextAdapter();
        // VIFContext ctx = adapter.createVIFContext(new IdContext("SA", "01", Locale.FRENCH, "TESTUNIT"));
        // getMOSBS().initPrinter(ctx, "ENTFABJ", "DED");
        //
        // MOKey moKey = new MOKey();
        // moKey.setChrono(new Chrono("1OF", 680));
        // moKey.setEstablishmentKey(new EstablishmentKey("SA", "01"));
        // OperationItemSBean moItSBean = new OperationItemSBean();
        // moItSBean.setAll(true);
        // List<AbstractOperationItemBean> list = getMOItemSBS().listOperationItem(ctx, moKey, moItSBean);
        //
        // OperationItemKey operationItemKey = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("SA",
        // "01"), new Chrono(getChronoSBS().getPrechro(ctx, PrechroId.PRODUCTION.getValue()), 680), 1, 1, 1, 2);
        //
        // List<ItemMovementBean> listItemMovement = getMOItemSBS().listItemMovement(ctx, operationItemKey,
        // ActivityItemType.INPUT);
        //
        // SearchLabel searchLabel = getMOItemSBS().searchPrintMovementLabel(ctx,
        // new RowidKey(listItemMovement.get(1).getItemMovementKey().getRowidKey()), "DED",
        // ActivityItemType.INPUT);
        // assertNotNull(searchLabel);
        // assertEquals(true, searchLabel.getAskForCopiesNumber());
        // assertEquals("MVG", searchLabel.getLabelId());
        // assertEquals("ehplamvg", searchLabel.getPrintProgramId());
        // } catch (ServerBusinessException e) {
        // fail("ServerBusinessException on searchPrintMovementLabel");
        // } catch (BusinessException e) {
        // fail("BusinessException on searchPrintMovementLabel");
        // }
    }

    @Test
    public void testUpdateAdministrativeStructure() {
        try {
            // Test on a good xpiloia
            ItemMovementBean itemMovementBean1 = new ItemMovementBean(new ItemMovementKey(ManagementType.REAL,
                    new EstablishmentKey("TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(),
                            PrechroId.PRODUCTION.getValue()), 89), 1, 1, 1, 1, ""), "LotPEDTestUnit", new CodeLabel(
                                    "211", "PROD - Semi finis"), new CodeLabel("TSTGP1", "Poulet entier"), new CodeLabel("", ""),
                                    new CodeLabel("", ""), new Date(), new ItemMovementQuantityUnit(1, "Kg", 0, "", 0, ""));

            getMOItemSBS().saveItemMovement(getCtx(), itemMovementBean1, ActivityItemType.INPUT, "PENTUNIT");
            getMOItemSBS().updateAdministrativeStructure(getCtx(), itemMovementBean1.getItemMovementKey(), "PENTUNIT");
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on updateAdministrativeStructure");
        }

        try {
            // Test on a bad xpiloia
            OperationItemKey operationItemKey2 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 89), 1, 1,
                    1, 4);

            getMOItemSBS().updateAdministrativeStructure(getCtx(), operationItemKey2, "PENTUNIT");

            fail("Updates mustn't be OK.");
        } catch (ServerBusinessException e) {
        }
    }

    @Test
    public void testUpdateWorkshopStructure() {
        try {
            // Test on a good xpiloia
            OperationItemKey operationItemKey1 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 157), 1, 1,
                    1, 1);

            getMOItemSBS().updateWorkshopStructure(getCtx(), operationItemKey1, "PENTUNIT", Progress.PROGRESSING);
            getMOItemSBS().updateWorkshopStructure(getCtx(), operationItemKey1, "PENTUNIT", Progress.SUSPENDED);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException on updateWorkshopStructure");
        }

        try {
            // Test on a bad xpiloia
            OperationItemKey operationItemKey2 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 157), 0, 0,
                    1, 1);

            getMOItemSBS().updateWorkshopStructure(getCtx(), operationItemKey2, "PENTUNIT", Progress.AWAITED);

            fail("Updates mustn't be OK.");
        } catch (ServerBusinessException e) {
        }

        try {
            // Test on a xpiloia with bad management type and progress
            OperationItemKey operationItemKey3 = new OperationItemKey(ManagementType.ARCHIVED, new EstablishmentKey(
                    "TG", "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 157),
                    1, 1, 1, 1);

            getMOItemSBS().updateWorkshopStructure(getCtx(), operationItemKey3, "PENTUNIT", Progress.SUSPENDED);

            fail("Updates mustn't be OK.");
        } catch (ServerBusinessException e) {
        }

        try {
            // Test on a xpiloia with a bad kota
            OperationItemKey operationItemKey4 = new OperationItemKey(ManagementType.REAL, new EstablishmentKey("TG",
                    "01"), new Chrono(getChronoSBS().getPrechro(getCtx(), PrechroId.PRODUCTION.getValue()), 158), 1, 1,
                    1, 1);

            getMOItemSBS().updateWorkshopStructure(getCtx(), operationItemKey4, "PENTUNIT", Progress.AWAITED);

            fail("Updates mustn't be OK.");
        } catch (ServerBusinessException e) {
        }
    }
}

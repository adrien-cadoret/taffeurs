/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: FProductionInputContainer03CBSTest.java,v $
 * Created on 17 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.batchitem;


import java.util.List;

import junit.framework.TestCase;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.access.BeanFactoryLocator;
import org.springframework.beans.factory.access.BeanFactoryReference;
import org.springframework.context.access.ContextSingletonBeanFactoryLocator;

import fr.vif.jtech.business.exceptions.BusinessErrorsException;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputBarcodeType;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproduction.FProductionSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerVBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerWorkBean;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBS;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerCBS;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityEnum;


public class FProductionInputContainer03CBSTest extends TestCase {

    private BeanFactoryReference              bfr;
    private FProductionInputContainerCBS      cbs;
    private FProductionCBS                    fProductionCBS;
    private IdContext                         idContext;
    private FProductionInputContainerWorkBean workBean;

    public void testConvert01() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "06"));
        moKey.setChrono(new Chrono("1OF", 1));
        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("603");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("603");
        workBean.setLogicalWorkstationId("603");
        workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);
        assertNotNull(vBean);
        CEntityBean entity = vBean.getEntity();
        assertEquals("06102101", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("BAC", entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("KG", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
        assertEquals("611", entity.getEntityLocationBean().getEntityLocationKey().getCdepot());
        assertEquals("", entity.getEntityLocationBean().getEntityLocationKey().getCemp());
    }

    public void testConvert02() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "06"));
        moKey.setChrono(new Chrono("1OF", 1));
        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("603");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("603");
        workBean.setLogicalWorkstationId("603");
        workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(1), workBean);
        assertNotNull(vBean);
        CEntityBean entity = vBean.getEntity();
        assertEquals("06102102", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("P", entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
        assertEquals("611", entity.getEntityLocationBean().getEntityLocationKey().getCdepot());
        assertEquals("", entity.getEntityLocationBean().getEntityLocationKey().getCemp());
    }

    public void testConvert03() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "06"));
        moKey.setChrono(new Chrono("1OF", 1));
        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("603");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("603");
        workBean.setLogicalWorkstationId("603");
        workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(2), workBean);
        assertNotNull(vBean);
        CEntityBean entity = vBean.getEntity();
        assertEquals("06104101", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("KG", entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
        assertEquals("661", entity.getEntityLocationBean().getEntityLocationKey().getCdepot());
        assertEquals("", entity.getEntityLocationBean().getEntityLocationKey().getCemp());
    }

    public void testConvert04() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "06"));
        moKey.setChrono(new Chrono("1OF", 4));
        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("613");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("613");
        workBean.setLogicalWorkstationId("613");
        workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);
        assertNotNull(vBean);
        CEntityBean entity = vBean.getEntity();
        assertEquals("06108101", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("BAC", entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("KG", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
        assertEquals("661", entity.getEntityLocationBean().getEntityLocationKey().getCdepot());
        assertEquals("", entity.getEntityLocationBean().getEntityLocationKey().getCemp());
    }

    public void testConvert05() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "06"));
        moKey.setChrono(new Chrono("1OF", 4));
        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("613");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        idContext.setWorkstation("613");
        workBean.setRealWorkstationId("613");
        workBean.setLogicalWorkstationId("613");
        workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(1), workBean);
        assertNotNull(vBean);
        CEntityBean entity = vBean.getEntity();
        assertEquals("06206101", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("U", entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("KG", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
        assertEquals("621", entity.getEntityLocationBean().getEntityLocationKey().getCdepot());
        assertEquals("", entity.getEntityLocationBean().getEntityLocationKey().getCemp());
    }

    public void testConvert06() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "06"));
        moKey.setChrono(new Chrono("1OF", 4));
        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("623");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("623");
        workBean.setLogicalWorkstationId("623");
        workBean.setBarcodeType(InputBarcodeType.IGNORE_QTIES);
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);
        assertNotNull(vBean);
        CEntityBean entity = vBean.getEntity();
        assertEquals("06206102", entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty());
        assertEquals("U", entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty());
        assertEquals("KG", entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit());
        assertEquals(0.0, entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty());
        assertEquals("", entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit());
        assertEquals("621", entity.getEntityLocationBean().getEntityLocationKey().getCdepot());
        assertEquals("", entity.getEntityLocationBean().getEntityLocationKey().getCemp());
    }

    public void testUpdate01() {
        List<FProductionBBean> lst = null;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "06"));
        moKey.setChrono(new Chrono("1OF", 1));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("603");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        try {
            lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        } catch (BusinessException e) {
            fail("Error " + e);
        }
        assertNotNull(lst);
        workBean.setRealWorkstationId("603");
        workBean.setLogicalWorkstationId("603");
        workBean.setBarcodeType(InputBarcodeType.KEEP_QTIES);
        FProductionInputContainerVBean vBean = null;
        try {
            vBean = cbs.convert(idContext, lst.get(0), workBean);
        } catch (BusinessException e) {
            fail("Error " + e);
        }
        assertNotNull(vBean);

        try {
            vBean.getEntity().getStockItemBean().getStockItem().setBatch("64001");
            vBean.getEntity().getStockItemBean().getKeyboardQties().getFirstQty().setQty(1);
            vBean.getEntity().getStockItemBean().getKeyboardQties().getSecondQty().setQty(3.181);
            cbs.validateEntityFields(idContext, CEntityEnum.BATCH, vBean, workBean);
            vBean.setCriteriaWindowOpened(true);
            vBean.getBeanCriteria().getListCriteria().get(0).setValueNumMin(2);
            vBean.getBeanCriteria().getListCriteria().get(0).setValueNumMax(2);
            vBean.getBeanCriteria().getListCriteria().get(0).setValueMin(2);
            vBean.getBeanCriteria().getListCriteria().get(0).setValueMax(2);
            vBean = cbs.updateBean(idContext, vBean, workBean);
            cbs.deleteLast(idContext, new EstablishmentKey(vBean.getEntity().getEntityLocationBean()
                    .getEntityLocationKey().getCsoc(), vBean.getEntity().getEntityLocationBean().getEntityLocationKey()
                    .getCetab()), vBean.getPreviousMovementAct());
        } catch (BusinessException e) {
            fail("Error " + e);
        } catch (BusinessErrorsException e) {
            fail("Error " + e);
        }
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        BeanFactoryLocator bfl = ContextSingletonBeanFactoryLocator.getInstance();
        bfr = bfl.useBeanFactory("fr.vif.vif5_7.cbs.factory");
        BeanFactory bf = bfr.getFactory();
        cbs = (FProductionInputContainerCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerCBS");
        fProductionCBS = (FProductionCBS) bf
                .getBean("fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBS");
        idContext = new IdContext("VR", "06", I18nServerManager.FRANCE, "SAU01");
        idContext.setWorkstation("603");
        workBean = new FProductionInputContainerWorkBean();

    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
    }

    private FProductionInputContainerVBean getVBean01() throws Exception {
        List<FProductionBBean> lst;
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey("VR", "08"));
        moKey.setChrono(new Chrono("1OF", 1));

        FProductionSBean sBean = new FProductionSBean();
        sBean.setWorkstationId("SAU01");
        sBean.setAll(false);
        sBean.setMoKey(moKey);
        lst = fProductionCBS.queryElements(idContext, sBean, 0, 10000);
        assertNotNull(lst);
        workBean.setRealWorkstationId("SAU01");
        workBean.setLogicalWorkstationId("SAU01");
        FProductionInputContainerVBean vBean = cbs.convert(idContext, lst.get(0), workBean);
        return vBean;
    }
}

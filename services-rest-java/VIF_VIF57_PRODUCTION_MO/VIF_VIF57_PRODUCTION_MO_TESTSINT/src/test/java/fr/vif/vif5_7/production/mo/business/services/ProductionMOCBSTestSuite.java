/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_CBS
 * File : $RCSfile: ProductionMOCBSTestSuite.java,v $
 * Created on 19 nov. 08 by vr
 */
package fr.vif.vif5_7.production.mo.business.services;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.vif.vif5_7.production.mo.business.services.composites.csubstituteitem.CSubstituteItemCBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fdataentrylist.FDataEntryListCBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmocreation.FMOCreationCBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmocreation.FMOCreationCBSTest02;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fmsocreation.FMSOCreationCBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproduction.FProductionCBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.batchitem.FProductionInputContainer01CBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.batchitem.FProductionInputContainer03CBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductioninputcontainer.container.FProductionInputContainer02CBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcontainer.batchitem.FProductionOutputContainer01CBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fproductionoutputcontainer.container.FProductionOutputContainer02CBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.dataentry.item.fsubstituteitem.FSubstituteItemCBSTest;
import fr.vif.vif5_7.production.mo.business.services.features.fmolist.FMOListCBSTest;


/**
 * Production Mo CBS Test Suite
 * 
 * @author vr
 */
@RunWith(Suite.class)
@SuiteClasses({ CSubstituteItemCBSTest.class, //
        FDataEntryListCBSTest.class, //
        FMOCreationCBSTest.class, //
        FMOCreationCBSTest02.class, //
        FMSOCreationCBSTest.class, //
        FProductionCBSTest.class, //
        FProductionInputContainer01CBSTest.class, //
        FProductionInputContainer02CBSTest.class, //
        FProductionInputContainer03CBSTest.class, //
        FProductionOutputContainer01CBSTest.class, //
        FProductionOutputContainer02CBSTest.class, //
        FSubstituteItemCBSTest.class, //
        FMOListCBSTest.class })
public class ProductionMOCBSTestSuite {

}

package fr.vif.vif5_7.production.mo.business.services.libraries.indicators;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.VIFContextMockManager;
import fr.vif.jtech.business.exceptions.FatalException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.util.cache.ehcache.EhcacheManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.dao.AbstractDBContext;
import fr.vif.jtech.dao.DaoConfigurationJmockTest;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.jmock.JMockContext;
import fr.vif.vif5_7.gen.item.business.beans.common.GeneralItemInfos;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOSelectionBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;
import fr.vif.vif5_7.production.mo.dao.kfabind.Kfabind;
import fr.vif.vif5_7.production.mo.dao.kfabind.KfabindDAO;


/**
 * The JMock indicator MO SBS implementation test class.
 * 
 * @author jd
 */
public class JMockIndicatorMOSBSImplTest {
    /**
     * Main JMock context.
     */
    public static Mockery                   context;

    private static EstablishmentKey         establishmentKey = new EstablishmentKey("TG", "04");

    private static IdContext                idContext;
    private static ChronoPO                 chronoPO         = new ChronoPO("prechoPO", 2, 5);
    private static IndicatorMOSelectionBean selection;
    private static KfabindDAO               kfabindDAO       = null;
    private static Date                     selectionDate    = new Date();
    private static IndicatorMOSBSImpl       sbs              = null;
    private static String                   REGION_NAME      = "fr.vif.vif5_7.production.mo.business.services.libraries.indicators";

    /**
     * Runs once before all tests.
     * 
     * @throws Exception if error occurs
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        context = JMockContext.getNewContext();

        /* initialize the sbs under test */
        sbs = new IndicatorMOSBSImpl();
        sbs.setCache(new EhcacheManager());
        sbs.setRegionName(REGION_NAME);
        sbs.setLoader(new IndicatorMOLoader());

        /* Sample : Configure DAO retrieving mock objects */
        kfabindDAO = DaoConfigurationJmockTest.addMockToDAO(KfabindDAO.class);
    }

    /**
     * Reload all the data tests.
     * 
     * @throws ServerBusinessException
     */
    public void reloadDataTests() throws ServerBusinessException {
        selection = new IndicatorMOSelectionBean();
        selection.setEstablishmentKey(establishmentKey);
        selection.setPoDate(selectionDate);
        selection.setPoKey(new POKey(establishmentKey, chronoPO));
        sbs.getCache().clear(REGION_NAME);
        sbs.getCache().putCacheObject(selection, new ArrayList<IndicatorMO>());
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        reloadDataTests();
    }

    /**
     * Test listIndicatorBySelection method for simple test
     */
    @Test
    public void testSelection_01() {

        final List<Kfabind> list = new ArrayList<Kfabind>();
        Kfabind k = new Kfabind();
        k.setCsoc("TG");
        k.setCetab("04");
        k.setCart("TOTO");
        list.add(k);

        try {
            context.checking(new Expectations() {
                {
                    oneOf(kfabindDAO).listBySelection(with(any(AbstractDBContext.class)), with(selection));
                    will(returnValue(list));

                }
            });
        } catch (DAOException e) {
            Assert.fail(e.getMessage());

        }

        try {

            // IndicatorMOSelectionBean selection = new IndicatorMOSelectionBean();
            List<IndicatorMO> ret = sbs.listBySelection(getCtx(), selection);
            Assert.assertEquals(1, ret.size());

        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }

    }

    /**
     * Test listIndicatorBySelection method for throw server exception returned
     */
    @Test
    public void testSelection_02() {

        final List<Kfabind> list = new ArrayList<Kfabind>();
        Kfabind k = new Kfabind();
        k.setCsoc("TG");
        k.setCetab("04");
        k.setCart("TOTO");
        list.add(k);

        try {
            context.checking(new Expectations() {
                {
                    oneOf(kfabindDAO).listBySelection(with(any(AbstractDBContext.class)),
                            with(any(IndicatorMOSelectionBean.class)));
                    will(returnValue(list));

                }
            });
        } catch (DAOException e) {
            Assert.fail(e.getMessage());
        }

        try {

            IndicatorMOSelectionBean selection = new IndicatorMOSelectionBean();
            sbs.listBySelection(getCtx(), selection);

        } catch (ServerBusinessException e) {
            Assert.fail("No exception received");
        }

    }

    /**
     * Test listIndicatorBySelection method for throw dao exception returned
     */
    @Test
    public void testSelection_02_bis() {

        selection.getLines().add("L1MA");
        try {
            context.checking(new Expectations() {
                {
                    exactly(1).of(kfabindDAO).listBySelection(with(any(AbstractDBContext.class)), with(selection));
                    will(throwException(new DAOException(new FatalException(""))));
                }
            });
        } catch (DAOException e) {
            Assert.fail(e.getMessage());
        }

        try {
            sbs.listBySelection(getCtx(), selection);
            Assert.fail("No exception received");
        } catch (ServerBusinessException e) {
            // Assert.fail(e.getMessage());
        }

    }

    /**
     * Test listIndicatorBySelection method for simple with item key without label
     */
    @Test
    public void testSelection_03() {

        final List<Kfabind> list = new ArrayList<Kfabind>();
        Kfabind k = new Kfabind();
        k.setCsoc("TG");
        k.setCetab("04");
        k.setCart("TOTO");
        list.add(k);

        try {
            context.checking(new Expectations() {
                {
                    one(kfabindDAO).listBySelection(with(any(AbstractDBContext.class)),
                            with(any(IndicatorMOSelectionBean.class)));
                    will(returnValue(list));

                }
            });
        } catch (DAOException e) {
            Assert.fail(e.getMessage());
        }

        try {

            IndicatorMOSelectionBean selection = new IndicatorMOSelectionBean();
            List<IndicatorMO> ret = sbs.listBySelection(getCtx(), selection);
            Assert.assertEquals(1, ret.size());

        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }

    }

    /**
     * Test listIndicatorBySelection method for simple with item key with label
     */
    @Test
    public void testSelection_04() {

        final List<Kfabind> list = new ArrayList<Kfabind>();
        Kfabind k = new Kfabind();
        k.setCsoc("TG");
        k.setCetab("04");
        k.setCart("TOTO");
        list.add(k);

        final GeneralItemInfos item = new GeneralItemInfos();
        item.setItemCL(new CodeLabels("ART001", "Escal", "Hop de poulet"));

        try {
            context.checking(new Expectations() {
                {
                    oneOf(kfabindDAO).listBySelection(with(any(AbstractDBContext.class)),
                            with(any(IndicatorMOSelectionBean.class)));
                    will(returnValue(list));
                }
            });
        } catch (DAOException e) {
            Assert.fail(e.getMessage());
        }

        try {

            IndicatorMOSelectionBean selection = new IndicatorMOSelectionBean();
            List<IndicatorMO> ret = sbs.listBySelection(getCtx(), selection);
            Assert.assertEquals(1, ret.size());

        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }

    }

    /**
     * Test listIndicatorBySelection method for simple with 2 items
     */
    @Test
    public void testSelection_05() {

        final List<Kfabind> list = new ArrayList<Kfabind>();
        Kfabind k = new Kfabind();
        k.setCsoc("TG");
        k.setCetab("04");
        k.setCart("TOTO");
        list.add(k);
        Kfabind k2 = new Kfabind();
        k2.setCsoc("TG");
        k2.setCetab("04");
        k2.setCart("TOUTI");
        list.add(k2);

        final GeneralItemInfos item = new GeneralItemInfos();
        item.setItemCL(new CodeLabels("ART001", "Escal", "Hop de poulet"));

        try {
            context.checking(new Expectations() {
                {
                    atLeast(2).of(kfabindDAO).listBySelection(with(any(AbstractDBContext.class)), with(selection));
                    will(returnValue(list));

                }
            });
        } catch (DAOException e) {
            Assert.fail(e.getMessage());
        }

        try {
            sbs.getCache().clear(REGION_NAME);
            sbs.getCache().putCacheObject(selection, new ArrayList<IndicatorMO>());
            List<IndicatorMO> ret = sbs.listBySelection(getCtx(), selection);
            Assert.assertEquals(2, ret.size());

        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }

    }

    /**
     * Get the VIFContext.
     * 
     * @return
     */
    private VIFContext getCtx() {
        return VIFContextMockManager.getVIFContext();
    }

}

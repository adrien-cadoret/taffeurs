/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MoItemSBSImplTest.java,v $
 * Created on 15 avr. 09 by vr
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.operationitem;


import java.util.ArrayList;
import java.util.Arrays;

import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.VIFContextMockManager;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.dao.AbstractDBContext;
import fr.vif.jtech.dao.DaoConfigurationJmockTest;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.jmock.JMockContext;
import fr.vif.vif57.stock.entity.business.services.constant.EntityEnum.Management;
import fr.vif.vif57.stock.entity.business.services.libraries.EntitySBS;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.UniqueItem;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityLocationKey;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.Quantities;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.Movement;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementSelection;
import fr.vif.vif5_7.activities.activities.business.services.libraries.activities.ActivitiesSBS;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.admin.config.business.services.libraries.parameter.ParameterSBS;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemKey;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.item.business.services.libraries.item.ItemSBS;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.location.business.beans.common.warehouse.WareHouseKey;
import fr.vif.vif5_7.gen.location.business.beans.common.warehouse.WareHouseProperties;
import fr.vif.vif5_7.gen.location.business.services.libraries.warehouse.WareHouseSBS;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPO;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.production.mo.business.services.libraries.moprofile.MoProfileSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.ProductionMOParamEnum;
import fr.vif.vif5_7.production.mo.dao.xpiloia.Xpiloia;
import fr.vif.vif5_7.production.mo.dao.xpiloia.XpiloiaDAO;


/**
 * Mock test for MoItemSBSImpl.
 * 
 * @author vr
 */
@RunWith(JMock.class)
public class MoItemSBSImplTest {

    /** The JMock context. */
    public static Mockery             context;

    /** The SBS under test. */
    private static MOItemSBSImpl      sbs                  = null;

    /** DAO & SBS. */
    private static XpiloiaDAO         xpiloiaDAO           = null;
    /** Test's constants. */
    private static final String       COMPANY_CODE         = "TG";
    private static final String       ESTABLISHEMENT_CODE  = "01";
    private static final String       WAREHOUSE1_CODE      = "411";
    private static final String       LOCATION1_CODE       = "A01";
    private static final String       LOCATION2_CODE       = "A02";
    private static final String       WAREHOUSE2_CODE      = "221";
    private static final String       ITEM1_CODE           = "cumart01";

    private static final String       ITEM2_CODE           = "cumart02";
    private static final String       ITEM3_CODE           = "cumart03";
    private static final String       BATCH01_CODE         = "batch01";
    private static final String       BATCH04_CODE         = "batch04";
    private static final String       BATCH02_CODE_1       = "batch02-1";
    private static final String       BATCH02_CODE_2       = "batch02-2";
    private static final String       BATCH02_CODE_3       = "batch02-3";
    private static final double       PRIMARY_QTY1_DEFAULT = 10.0;
    private static final double       PRIMARY_QTY2_DEFAULT = 15.0;
    private static final String       PRIMARY_UNIT_DEFAULT = "Kgu";
    private static final String       UPPER_CONTAINER_1    = "1130099000";
    private static final String       CHILD_CONTAINER_1    = "1130099001";
    private static final String       WORKSTATION          = "F01";
    private static final String       PRECHRO              = "1OF";
    private static final int          CHRONO_MO            = 23;
    /** Test's data. */
    private static MOKey              moKey;
    private static MovementSelection  movementSelection;
    private static OperationItemKey   oik;
    private static Xpiloia            xpiloia;
    private static OperationItemSBean sBean;
    private static EstablishmentKey   establishmentKey     = new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE);
    private static ItemKey            itemKey              = new ItemKey(COMPANY_CODE, ITEM1_CODE);

    private EntitySBS                 entitySBS;
    private ItemSBS                   itemSBS;
    private MoProfileSBS              moProfileSBS;
    private MOSBS                     moSBS;
    private ParameterSBS              parameterSBS;
    private ActivitiesSBS             activitiesSBS;
    private WareHouseSBS              wareHouseSBS;

    // private XxfabPPO xxfabPPO;

    /**
     * Reload all the data tests.
     */
    public static void reloadDataTests() {
        xpiloia = new Xpiloia();
        xpiloia.setCart(ITEM1_CODE);
        xpiloia.setCdepot(WAREHOUSE1_CODE);
        xpiloia.setCemp(LOCATION1_CODE);
        xpiloia.setTypacta(ActivityItemType.INPUT.getValue());
        xpiloia.setCsoc(COMPANY_CODE);
        xpiloia.setCetab(ESTABLISHEMENT_CODE);
        xpiloia.setPrechrof(PRECHRO);
        xpiloia.setChronof(CHRONO_MO);
        xpiloia.setLot(BATCH04_CODE);
        xpiloia.setNi1(5);
        xpiloia.setTypgest("R");

        sBean = new OperationItemSBean();
        sBean.setActivityItemType(ActivityItemType.INPUT);
        sBean.setWorkstationId(WORKSTATION);
        moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE));
        moKey.setChrono(new Chrono(PRECHRO, CHRONO_MO));
        oik = new OperationItemKey();
        oik.setChrono(new Chrono(PRECHRO, CHRONO_MO));
        oik.setEstablishmentKey(new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE));
        oik.setCounter1(5);
        oik.setManagementType(ManagementType.REAL);
    }

    /**
     * Runs once before all tests.
     * 
     * @throws Exception if error occurs
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {

    }

    /**
     * 
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        reloadDataTests();
        context = JMockContext.getNewContext();

        itemSBS = context.mock(ItemSBS.class);
        moProfileSBS = context.mock(MoProfileSBS.class);
        moSBS = context.mock(MOSBS.class);
        parameterSBS = context.mock(ParameterSBS.class);
        entitySBS = context.mock(EntitySBS.class);
        // xxfabPPO = context.mock(XxfabPPO.class);
        wareHouseSBS = context.mock(WareHouseSBS.class);
        activitiesSBS = context.mock(ActivitiesSBS.class);

        /* initialize the sbs under test */
        sbs = new MOItemSBSImpl();
        sbs.setEntitySBS(entitySBS);
        sbs.setItemSBS(itemSBS);
        sbs.setMoProfileSBS(moProfileSBS);
        sbs.setMoSBS(moSBS);
        sbs.setParameterSBS(parameterSBS);
        sbs.setWarehouseSBS(wareHouseSBS);
        sbs.setActivitiesSBS(activitiesSBS);

        xpiloiaDAO = DaoConfigurationJmockTest.addMockToDAO(XpiloiaDAO.class);

    }

    /**
     * Test method for
     */
    @Test
    public void testGetOperationItem() {

        // INPUT
        try {

            context.checking(new Expectations() {
                {
                    one(xpiloiaDAO).getOperationItem(with(any(AbstractDBContext.class)), with(oik));
                    will(returnValue(xpiloia));
                }
            });
        } catch (DAOException e1) {
            Assert.fail(e1.getMessage());
        }
        try {
            AbstractOperationItemBean result = sbs.getOperationItem(getCtx(), oik);
            Assert.assertNotNull("Result should not be null", result);
            Assert.assertTrue(result instanceof InputOperationItemBean);
            Assert.assertEquals(oik, result.getOperationItemKey());
            Assert.assertEquals(WAREHOUSE1_CODE, result.getDepotCL().getCode());
            Assert.assertEquals(LOCATION1_CODE, result.getLocationCL().getCode());
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }

        // OUTPUT
        xpiloia.setTypacta(ActivityItemType.OUTPUT.getValue());
        WareHouseProperties whp = new WareHouseProperties();
        whp.setToptimise(true);
        try {

            context.checking(new Expectations() {
                {
                    one(xpiloiaDAO).getOperationItem(with(any(AbstractDBContext.class)), with(oik));
                    will(returnValue(xpiloia));

                    oneOf(wareHouseSBS).getProperties(with(any(VIFContext.class)),
                            with(new WareHouseKey(COMPANY_CODE, ESTABLISHEMENT_CODE, WAREHOUSE1_CODE)));
                    will(returnValue(whp));
                }
            });
        } catch (DAOException | ServerBusinessException e1) {
            Assert.fail(e1.getMessage());
        }
        try {
            AbstractOperationItemBean result = sbs.getOperationItem(getCtx(), oik);
            Assert.assertNotNull("Result should not be null", result);
            Assert.assertTrue(result instanceof OutputOperationItemBean);
            Assert.assertEquals(oik, result.getOperationItemKey());
            Assert.assertEquals(WAREHOUSE1_CODE, result.getDepotCL().getCode());
            Assert.assertEquals(LOCATION1_CODE, result.getLocationCL().getCode());
            Assert.assertTrue(((OutputOperationItemBean) result).getToptimise());
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testGetOperationItem_Downgraded() {
        WareHouseProperties whp = new WareHouseProperties();
        whp.setToptimise(true);
        xpiloia.setTypacta(ActivityItemType.OUTPUT.getValue());
        MovementSelection movementSelection = new MovementSelection();
        MovementAct movementAct = new MovementAct();
        movementAct.setPrechro(PRECHRO);
        movementAct.setChrono(CHRONO_MO);
        movementAct.setNumber1(xpiloia.getNi1());
        movementAct.setNumber2(xpiloia.getNi2());
        movementAct.setNumber3(xpiloia.getNi3());
        movementAct.setNumber4(xpiloia.getNi4());
        movementSelection.setMovementAct(movementAct);
        movementSelection.setManagements(Arrays.asList(Management.REAL));
        movementSelection.setLocation(new EntityLocationKey(new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE)));
        movementSelection.setStockNature(MONatureComplementaryWorkshop.DOWNGRADED.getValue());
        StockItem stockItem = new StockItem(ITEM2_CODE, null);
        movementSelection.setStockItem(stockItem);

        // item 2
        Movement mvt_2_1 = new Movement();
        mvt_2_1.setStockItem(new UniqueItem("", ITEM2_CODE, BATCH02_CODE_1, new Quantities(new QtyUnit(2, "P"))));
        mvt_2_1.getState().setStockNature(MONatureComplementaryWorkshop.MANUFACTURING.getValue());
        Movement mvt_2_2 = new Movement();
        mvt_2_2.setStockItem(new UniqueItem("", ITEM2_CODE, BATCH02_CODE_2, new Quantities(new QtyUnit(4, "P"))));
        mvt_2_2.getState().setStockNature(MONatureComplementaryWorkshop.SCRAP.getValue());
        Movement mvt_2_3 = new Movement();
        mvt_2_3.setStockItem(new UniqueItem("", ITEM2_CODE, BATCH02_CODE_3, new Quantities(new QtyUnit(6, "P"))));
        mvt_2_3.getState().setStockNature(MONatureComplementaryWorkshop.DOWNGRADED.getValue());

        // item 3
        Movement mvt_3_1 = new Movement();
        mvt_3_1.setStockItem(new UniqueItem("", ITEM2_CODE, BATCH02_CODE_1, new Quantities(new QtyUnit(3, "P"))));
        mvt_3_1.getState().setStockNature(MONatureComplementaryWorkshop.MANUFACTURING.getValue());
        Movement mvt_3_2 = new Movement();
        mvt_3_2.setStockItem(new UniqueItem("", ITEM2_CODE, BATCH02_CODE_2, new Quantities(new QtyUnit(6, "P"))));
        mvt_3_2.getState().setStockNature(MONatureComplementaryWorkshop.SCRAP.getValue());
        Movement mvt_3_3 = new Movement();
        mvt_3_3.setStockItem(new UniqueItem("", ITEM2_CODE, BATCH02_CODE_3, new Quantities(new QtyUnit(10, "P"))));
        mvt_3_3.getState().setStockNature(MONatureComplementaryWorkshop.DOWNGRADED.getValue());
        try {
            MovementSelection movementSelection3 = new MovementSelection();
            BeanUtils.copyProperties(movementSelection, movementSelection3);
            movementSelection3.setStockItem(new StockItem(ITEM3_CODE, null));

            context.checking(new Expectations() {
                {
                    one(xpiloiaDAO).getOperationItem(with(any(AbstractDBContext.class)), with(oik));
                    will(returnValue(xpiloia));

                    oneOf(activitiesSBS).listCodeByArticleReference(with(any(VIFContext.class)),
                            with(establishmentKey), with(ITEM1_CODE));
                    will(returnValue(Arrays.asList(ITEM2_CODE, ITEM3_CODE)));
                    // item 2
                    oneOf(itemSBS).getItemCLByPK(with(any(VIFContext.class)),
                            with(new ItemKey(COMPANY_CODE, ITEM2_CODE)));
                    will(returnValue(new CodeLabels(ITEM2_CODE, "item2_lbl", "item2_sbl")));
                    oneOf(entitySBS).listMovementBySelectionOrderByDescending(with(any(VIFContext.class)),
                            with(movementSelection), with(0), with(100));
                    will(returnValue(Arrays.asList(mvt_2_1, mvt_2_2, mvt_2_3)));

                    oneOf(moProfileSBS).getOutputProfile(with(any(VIFContext.class)),
                            with(any(EstablishmentKey.class)), with(any(String.class)), with(any(String.class)));

                    // item 3
                    oneOf(itemSBS).getItemCLByPK(with(any(VIFContext.class)),
                            with(new ItemKey(COMPANY_CODE, ITEM3_CODE)));
                    will(returnValue(new CodeLabels(ITEM3_CODE, "item3_lbl", "item3_sbl")));
                    oneOf(entitySBS).listMovementBySelectionOrderByDescending(with(any(VIFContext.class)),
                            with(movementSelection3), with(0), with(100));
                    will(returnValue(Arrays.asList(mvt_3_1, mvt_3_2, mvt_3_3)));

                    oneOf(moProfileSBS).getOutputProfile(with(any(VIFContext.class)),
                            with(any(EstablishmentKey.class)), with(any(String.class)), with(any(String.class)));

                    oneOf(wareHouseSBS).getProperties(with(any(VIFContext.class)),
                            with(new WareHouseKey(COMPANY_CODE, ESTABLISHEMENT_CODE, WAREHOUSE1_CODE)));
                    will(returnValue(whp));
                }
            });
        } catch (DAOException | ServerBusinessException e1) {
            Assert.fail(e1.getMessage());
        }

        try {
            AbstractOperationItemBean result = sbs.getOutputOperationItem(getCtx(), oik, null,
                    MONatureComplementaryWorkshop.DOWNGRADED);
            Assert.assertNotNull("Result should not be null", result);
            Assert.assertTrue(result instanceof OutputOperationItemBean);
            Assert.assertEquals(oik, result.getOperationItemKey());
            Assert.assertEquals(WAREHOUSE1_CODE, result.getDepotCL().getCode());
            Assert.assertEquals(LOCATION1_CODE, result.getLocationCL().getCode());
            Assert.assertTrue(((OutputOperationItemBean) result).getToptimise());
            Assert.assertEquals(ITEM2_CODE, result.getItemCL().getCode());
            Assert.assertEquals("", result.getForecastBatch());
            Assert.assertEquals(8d, result.getOperationItemQuantityUnit().getCompletedQuantity());
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testGetOperationItem_Downgraded_2() {
        WareHouseProperties whp = new WareHouseProperties();
        whp.setToptimise(true);
        xpiloia.setTypacta(ActivityItemType.OUTPUT.getValue());
        MovementSelection movementSelection = new MovementSelection();
        MovementAct movementAct = new MovementAct();
        movementAct.setPrechro(PRECHRO);
        movementAct.setChrono(CHRONO_MO);
        movementAct.setNumber1(xpiloia.getNi1());
        movementAct.setNumber2(xpiloia.getNi2());
        movementAct.setNumber3(xpiloia.getNi3());
        movementAct.setNumber4(xpiloia.getNi4());
        movementSelection.setMovementAct(movementAct);
        movementSelection.setManagements(Arrays.asList(Management.REAL));
        movementSelection.setLocation(new EntityLocationKey(new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE)));
        movementSelection.setStockNature(MONatureComplementaryWorkshop.DOWNGRADED.getValue());
        StockItem stockItem = new StockItem(ITEM1_CODE, null);
        movementSelection.setStockItem(stockItem);

        // item 2
        Movement mvt_2_1 = new Movement();
        mvt_2_1.setStockItem(new UniqueItem("", ITEM1_CODE, BATCH02_CODE_1, new Quantities(new QtyUnit(2, "P"))));
        mvt_2_1.getState().setStockNature(MONatureComplementaryWorkshop.MANUFACTURING.getValue());
        Movement mvt_2_2 = new Movement();
        mvt_2_2.setStockItem(new UniqueItem("", ITEM1_CODE, BATCH02_CODE_2, new Quantities(new QtyUnit(4, "P"))));
        mvt_2_2.getState().setStockNature(MONatureComplementaryWorkshop.SCRAP.getValue());
        Movement mvt_2_3 = new Movement();
        mvt_2_3.setStockItem(new UniqueItem("", ITEM1_CODE, BATCH02_CODE_3, new Quantities(new QtyUnit(6, "P"))));
        mvt_2_3.getState().setStockNature(MONatureComplementaryWorkshop.DOWNGRADED.getValue());

        try {

            context.checking(new Expectations() {
                {
                    one(xpiloiaDAO).getOperationItem(with(any(AbstractDBContext.class)), with(oik));
                    will(returnValue(xpiloia));

                    // item 2
                    oneOf(itemSBS).getItemCLByPK(with(any(VIFContext.class)),
                            with(new ItemKey(COMPANY_CODE, ITEM1_CODE)));
                    will(returnValue(new CodeLabels(ITEM1_CODE, "item1_lbl", "item1_sbl")));
                    oneOf(entitySBS).listMovementBySelectionOrderByDescending(with(any(VIFContext.class)),
                            with(movementSelection), with(0), with(100));
                    will(returnValue(Arrays.asList(mvt_2_1, mvt_2_2, mvt_2_3)));

                    oneOf(moProfileSBS).getOutputProfile(with(any(VIFContext.class)),
                            with(any(EstablishmentKey.class)), with(any(String.class)), with(any(String.class)));

                    oneOf(wareHouseSBS).getProperties(with(any(VIFContext.class)),
                            with(new WareHouseKey(COMPANY_CODE, ESTABLISHEMENT_CODE, WAREHOUSE1_CODE)));
                    will(returnValue(whp));

                }
            });
        } catch (DAOException | ServerBusinessException e1) {
            Assert.fail(e1.getMessage());
        }

        try {
            AbstractOperationItemBean result = sbs.getOutputOperationItem(getCtx(), oik, itemKey,
                    MONatureComplementaryWorkshop.DOWNGRADED);
            Assert.assertNotNull("Result should not be null", result);
            Assert.assertTrue(result instanceof OutputOperationItemBean);
            Assert.assertEquals(oik, result.getOperationItemKey());
            Assert.assertEquals(WAREHOUSE1_CODE, result.getDepotCL().getCode());
            Assert.assertEquals(LOCATION1_CODE, result.getLocationCL().getCode());
            Assert.assertTrue(((OutputOperationItemBean) result).getToptimise());
            Assert.assertEquals(ITEM1_CODE, result.getItemCL().getCode());
            Assert.assertEquals("", result.getForecastBatch());
            Assert.assertEquals(8d, result.getOperationItemQuantityUnit().getCompletedQuantity());
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testGetOperationItem_Manufacturing() {
        WareHouseProperties whp = new WareHouseProperties();
        whp.setToptimise(true);
        xpiloia.setTypacta(ActivityItemType.OUTPUT.getValue());
        try {

            context.checking(new Expectations() {
                {
                    one(xpiloiaDAO).getOperationItem(with(any(AbstractDBContext.class)), with(oik));
                    will(returnValue(xpiloia));

                    oneOf(wareHouseSBS).getProperties(with(any(VIFContext.class)),
                            with(new WareHouseKey(COMPANY_CODE, ESTABLISHEMENT_CODE, WAREHOUSE1_CODE)));
                    will(returnValue(whp));

                }
            });
        } catch (DAOException | ServerBusinessException e1) {
            Assert.fail(e1.getMessage());
        }

        try {
            AbstractOperationItemBean result = sbs.getOutputOperationItem(getCtx(), oik, itemKey,
                    MONatureComplementaryWorkshop.MANUFACTURING);
            Assert.assertNotNull("Result should not be null", result);
            Assert.assertTrue(result instanceof OutputOperationItemBean);
            Assert.assertEquals(oik, result.getOperationItemKey());
            Assert.assertEquals(WAREHOUSE1_CODE, result.getDepotCL().getCode());
            Assert.assertEquals(LOCATION1_CODE, result.getLocationCL().getCode());
            Assert.assertTrue(((OutputOperationItemBean) result).getToptimise());
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    public void testGetOperationItem_Scrap() {
        WareHouseProperties whp = new WareHouseProperties();
        whp.setToptimise(true);
        xpiloia.setTypacta(ActivityItemType.OUTPUT.getValue());
        MovementSelection movementSelection = new MovementSelection();
        MovementAct movementAct = new MovementAct();
        movementAct.setPrechro(PRECHRO);
        movementAct.setChrono(CHRONO_MO);
        movementAct.setNumber1(xpiloia.getNi1());
        movementAct.setNumber2(xpiloia.getNi2());
        movementAct.setNumber3(xpiloia.getNi3());
        movementAct.setNumber4(xpiloia.getNi4());
        movementSelection.setMovementAct(movementAct);
        movementSelection.setManagements(Arrays.asList(Management.REAL));
        movementSelection.setLocation(new EntityLocationKey(new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE)));
        movementSelection.setStockNature(MONatureComplementaryWorkshop.SCRAP.getValue());
        StockItem stockItem = new StockItem(ITEM1_CODE, null);
        movementSelection.setStockItem(stockItem);

        POBean poBean = new POBean();
        poBean.setActivityFamilyId("FAM_ACT");

        try {

            context.checking(new Expectations() {
                {
                    one(xpiloiaDAO).getOperationItem(with(any(AbstractDBContext.class)), with(oik));
                    will(returnValue(xpiloia));

                    // item 2
                    oneOf(entitySBS).listMovementBySelectionOrderByDescending(with(any(VIFContext.class)),
                            with(movementSelection), with(0), with(100));
                    will(returnValue(null));

                    oneOf(moSBS).getPOBeanByKey(with(any(VIFContext.class)),
                            with(new POKey(establishmentKey, new ChronoPO(moKey.getChrono(), 5))));
                    will(returnValue(poBean));

                    oneOf(parameterSBS).readStringParameter(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE),
                            with(ProductionMOParamEnum.DECLARATION_BY_ACTIVITY_FAMILY.getCode()), with(26),
                            with("FAM_ACT"));
                    will(returnValue(PRIMARY_UNIT_DEFAULT));

                    oneOf(wareHouseSBS).getProperties(with(any(VIFContext.class)),
                            with(new WareHouseKey(COMPANY_CODE, ESTABLISHEMENT_CODE, WAREHOUSE1_CODE)));
                    will(returnValue(whp));

                }
            });
        } catch (DAOException | ServerBusinessException e1) {
            Assert.fail(e1.getMessage());
        }

        try {
            AbstractOperationItemBean result = sbs.getOutputOperationItem(getCtx(), oik, itemKey,
                    MONatureComplementaryWorkshop.SCRAP);
            Assert.assertNotNull("Result should not be null", result);
            Assert.assertTrue(result instanceof OutputOperationItemBean);
            Assert.assertEquals(oik, result.getOperationItemKey());
            Assert.assertEquals(WAREHOUSE1_CODE, result.getDepotCL().getCode());
            Assert.assertEquals(LOCATION1_CODE, result.getLocationCL().getCode());
            Assert.assertTrue(((OutputOperationItemBean) result).getToptimise());
            Assert.assertEquals(ITEM1_CODE, result.getItemCL().getCode());
            Assert.assertEquals(BATCH04_CODE, result.getForecastBatch());
            Assert.assertEquals(PRIMARY_UNIT_DEFAULT, result.getOperationItemQuantityUnit().getUnit());
            Assert.assertEquals(0.0, result.getOperationItemQuantityUnit().getCompletedQuantity());
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }
    }

    /**
     * Test method for
     */
    @Test
    public void testListOpenedContainers() {
        sBean = new OperationItemSBean();
        sBean.setActivityItemType(ActivityItemType.INPUT);
        sBean.setWorkstationId(WORKSTATION);
        moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE));
        moKey.setChrono(new Chrono(PRECHRO, 1));
        try {
            context.checking(new Expectations() {
                {
                    oneOf(entitySBS).listOpenedContainers(with(any(VIFContext.class)),
                            with(any(MovementSelection.class)));
                    returnValue(new ArrayList<String>());
                }
            });
        } catch (ServerBusinessException e1) {
            Assert.fail(e1.getMessage());
        }
        try {
            sbs.listOpenedContainers(getCtx(), moKey, WORKSTATION);
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }
    }

    /**
     * Test method for
     */
    @Test
    public void testListOperationItem() {
        sBean = new OperationItemSBean();
        sBean.setActivityItemType(ActivityItemType.INPUT);
        sBean.setWorkstationId(WORKSTATION);
        moKey = new MOKey();
        moKey.setEstablishmentKey(new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE));
        moKey.setChrono(new Chrono(PRECHRO, 1));

        try {
            context.checking(new Expectations() {
                {
                    one(xpiloiaDAO).listOperationItems(with(any(AbstractDBContext.class)), with(moKey), with(sBean));
                    will(returnValue(new ArrayList<Xpiloia>()));
                }
            });
        } catch (DAOException e1) {
            Assert.fail(e1.getMessage());
        }
        try {
            sbs.listOperationItem(getCtx(), moKey, sBean);
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }
    }

    @Test
    @Ignore
    // We don't know how to test with proxy
    public void testUpdateAdministrativeStructure() {
        XxfabPPO xxfabPPO = context.mock(XxfabPPO.class);
        try {

            context.checking(new Expectations() {
                {
                    one(xxfabPPO).prRemonteeAdmin(with(COMPANY_CODE), with(ESTABLISHEMENT_CODE), with(WORKSTATION),
                            with(getCtx().getIdCtx().getLogin()), with("R"), with(PRECHRO), with(CHRONO_MO), with(5),
                            with(0), with(0), with(0), with(any(BooleanHolder.class)), with(any(StringHolder.class)));
                }
            });

            sbs.updateAdministrativeStructure(getCtx(), oik, WORKSTATION);
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage());
        }
    }

    /**
     * Get the VIFContext.
     * 
     * @return
     */
    private VIFContext getCtx() {
        return VIFContextMockManager.getVIFContext();
    }

}

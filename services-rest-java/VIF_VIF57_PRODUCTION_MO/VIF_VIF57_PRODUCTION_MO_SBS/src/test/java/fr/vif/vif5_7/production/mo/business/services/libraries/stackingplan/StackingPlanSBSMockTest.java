/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: StackingPlanSBSMockTest.java,v $
 * Created on 18 avr. 2013 by xg
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.stackingplan;


import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.VIFContextMockManager;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.dao.AbstractDBContext;
import fr.vif.jtech.dao.DaoConfigurationJmockTest;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.jmock.JMockContext;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.keys.ContainerKey;
import fr.vif.vif5_7.activities.activities.business.beans.common.stackingplan.StackingPlan;
import fr.vif.vif5_7.activities.activities.business.services.libraries.stackingplan.StackingPlanSBS;
import fr.vif.vif5_7.activities.activities.dao.xplanr.Xplanr;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.common.RandomizeUtil;
import fr.vif.vif5_7.production.mo.dao.xplanrm.Xplanrm;
import fr.vif.vif5_7.production.mo.dao.xplanrm.XplanrmDAO;
import fr.vif.vif5_7.production.mo.dao.xplanrmd.Xplanrmd;
import fr.vif.vif5_7.production.mo.dao.xplanrmd.XplanrmdDAO;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;


/**
 * StackingPlanSBSImpl test.
 * 
 * @author xg
 */
@RunWith(JMock.class)
@SuppressWarnings("static-access")
public class StackingPlanSBSMockTest {

    /** Main JMock context. */
    public static Mockery                                                                                       context;

    /** SBS under test. */
    private static StackingPlanSBSImpl                                                                          sbsUT;

    /** Mock object. */
    private static XplanrmDAO                                                                                   xplanrmDAO;
    private static XplanrmdDAO                                                                                  xplanrmdDAO;
    private static fr.vif.vif5_7.activities.activities.business.services.libraries.stackingplan.StackingPlanSBS stackingPlanSBS;

    /** Test data. */
    private static MOKey                                                                                        moKey;
    private static String                                                                                       itemId;
    private static List<Xplanrm>                                                                                lXplanrm;
    private static List<Xplanrmd>                                                                               lstXplanrmd;
    private static Xplanr                                                                                       xplanr;
    private static StackingPlan                                                                                 stackingPlan;

    /** Reload all the data tests. */
    public static void reloadDataTests() {
        itemId = "itemId";

        moKey = new MOKey();
        moKey.setChrono(new Chrono("", 1));
        moKey.setEstablishmentKey(new EstablishmentKey("SA", "01"));

        lXplanrm = new ArrayList<Xplanrm>();
        Xplanrm xplanrm = new Xplanrm();
        xplanrm.setCplanr("dfb");
        lXplanrm.add(xplanrm);

        lstXplanrmd = new ArrayList<Xplanrmd>();
        Xplanrmd xplanrmd = new Xplanrmd();
        xplanrmd.setCplanr("dfb");
        xplanrmd.setCuart(itemId);
        xplanrmd.setCart(itemId);
        xplanrmd.setQtefait(1);
        xplanrmd.setQteprev(2);
        xplanrmd.setSpcb(1);
        xplanrmd.setTplein(false);
        xplanrmd.setNsc("nsc");
        lstXplanrmd.add(xplanrmd);

        xplanr = new Xplanr();
        xplanr.setLplanr("sfb");

        stackingPlan = new StackingPlan();
        stackingPlan.setStackingPlanCL(new CodeLabel("dfb", "sfb"));
    }

    /**
     * Runs once before all tests.
     * 
     * @throws Exception if error occurs
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        context = JMockContext.getNewContext();
        /* initialize the sbs under test */
        sbsUT = new StackingPlanSBSImpl();

        /* Sample : Configure DAO retrieving mock objects */
        xplanrmDAO = DaoConfigurationJmockTest.addMockToDAO(XplanrmDAO.class);
        xplanrmdDAO = DaoConfigurationJmockTest.addMockToDAO(XplanrmdDAO.class);

        stackingPlanSBS = context.mock(StackingPlanSBS.class);
        sbsUT.setStackingPlanSBS(stackingPlanSBS);

        reloadDataTests();
    }

    @Test
    public void testGetInnerMOStackingPlan() {

        try {

            sbsUT.getLogger().setLevel(Level.DEBUG);

            lXplanrm = null;

            context.checking(new Expectations() {
                {
                    oneOf(xplanrmdDAO).listByPK(with(any(AbstractDBContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(moKey.getChrono().getPrechro()), with(moKey.getChrono().getChrono()),
                            with(aNull(String.class)), with(itemId), with(aNull(String.class)),
                            with(aNull(Integer.class)), with(aNull(String.class)), with(true));
                    will(returnValue(lstXplanrmd));
                }
            });

            QtyUnit qu = sbsUT.getInnerMOStackingPlan(getCtx(), moKey, null, itemId);

            Assert.assertNotNull(qu);
            Assert.assertNotNull(qu.getUnit());
            Assert.assertTrue(qu.getQty() == 1);

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        } catch (DAOException e) {
            fail("DAOException thrown");
        }
    }

    @Test
    public void testGetMOStackingPlanCode_DataNull() {

        try {

            sbsUT.getLogger().setLevel(Level.DEBUG);

            lXplanrm = null;

            context.checking(new Expectations() {
                {
                    oneOf(xplanrmDAO).listByPK(with(any(AbstractDBContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(moKey.getChrono().getPrechro()), with(moKey.getChrono().getChrono()),
                            with(aNull(String.class)), with(false));
                    will(returnValue(lXplanrm));
                }
            });

            CodeLabel cl = sbsUT.getMOStackingPlanCode(getCtx(), moKey);

            Assert.assertNull(cl.getCode());

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        } catch (DAOException e) {
            fail("DAOException thrown");
        }
    }

    @Test
    public void testGetMOStackingPlanCode_MissingData() {

        try {

            sbsUT.getLogger().setLevel(Level.INFO);

            lXplanrm = new ArrayList<Xplanrm>();

            context.checking(new Expectations() {
                {
                    oneOf(xplanrmDAO).listByPK(with(any(AbstractDBContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(moKey.getChrono().getPrechro()), with(moKey.getChrono().getChrono()),
                            with(aNull(String.class)), with(false));
                    will(returnValue(lXplanrm));
                }
            });

            CodeLabel cl = sbsUT.getMOStackingPlanCode(getCtx(), moKey);

            Assert.assertNull(cl.getCode());

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        } catch (DAOException e) {
            fail("DAOException thrown");
        }
    }

    @Test
    public void testGetMOStackingPlanCode_NominalCase() {

        try {

            sbsUT.getLogger().setLevel(Level.DEBUG);

            context.checking(new Expectations() {
                {
                    oneOf(xplanrmDAO).listByPK(with(any(AbstractDBContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(moKey.getChrono().getPrechro()), with(moKey.getChrono().getChrono()),
                            with(aNull(String.class)), with(false));
                    will(returnValue(lXplanrm));
                }
            });

            context.checking(new Expectations() {
                {
                    oneOf(stackingPlanSBS).getStackingPlanByCode(with(any(VIFContext.class)),
                            with(moKey.getEstablishmentKey()), with(lXplanrm.get(0).getCplanr()));
                    will(returnValue(stackingPlan));
                }
            });

            CodeLabel cl = sbsUT.getMOStackingPlanCode(getCtx(), moKey);

            Assert.assertNotNull(cl);

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        } catch (DAOException e) {
            fail("DAOException thrown");
        }
    }

    @Test(expected = ServerBusinessException.class)
    public void testGetMOStackingPlanCode_ThrowsDAOExc() throws Exception {

        sbsUT.getLogger().setLevel(Level.INFO);

        context.checking(new Expectations() {
            {
                oneOf(xplanrmDAO).listByPK(with(any(AbstractDBContext.class)),
                        with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                        with(moKey.getChrono().getPrechro()), with(moKey.getChrono().getChrono()),
                        with(aNull(String.class)), with(false));
                will(throwException(new DAOException("Test")));
            }
        });

        sbsUT.getMOStackingPlanCode(getCtx(), moKey);

    }

    @Test
    public void testGetPackagingId_stackingPlanFULL() {

        try {

            lstXplanrmd.get(0).setTplein(true);

            sbsUT.getLogger().setLevel(Level.DEBUG);

            context.checking(new Expectations() {
                {
                    oneOf(xplanrmdDAO).listByPK(with(any(AbstractDBContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(moKey.getChrono().getPrechro()), with(moKey.getChrono().getChrono()),
                            with(aNull(String.class)), with(aNull(String.class)), with(aNull(String.class)),
                            with(aNull(Integer.class)), with(aNull(String.class)), with(true));
                    will(returnValue(lstXplanrmd));
                }
            });

            CEntityBean cl = sbsUT.getPackagingId(getCtx(), moKey, itemId, 0);

            Assert.assertNotNull(cl);

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        } catch (DAOException e) {
            fail("DAOException thrown");
        }
    }

    @Test
    public void testGetPackagingId_stackingPlanNOTFULL() {

        try {

            sbsUT.getLogger().setLevel(Level.DEBUG);

            context.checking(new Expectations() {
                {
                    oneOf(xplanrmdDAO).listByPK(with(any(AbstractDBContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(moKey.getChrono().getPrechro()), with(moKey.getChrono().getChrono()),
                            with(aNull(String.class)), with(aNull(String.class)), with(aNull(String.class)),
                            with(aNull(Integer.class)), with(aNull(String.class)), with(true));
                    will(returnValue(lstXplanrmd));
                }
            });

            CEntityBean cl = sbsUT.getPackagingId(getCtx(), moKey, itemId, 0);

            Assert.assertNotNull(cl);

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        } catch (DAOException e) {
            fail("DAOException thrown");
        }
    }

    @Test
    public void testIsCurrentContainerFull() {

        final ContainerKey containerKey = new ContainerKey(new EstablishmentKey(RandomizeUtil.generateRandomString(2),
                RandomizeUtil.generateRandomString(2)), RandomizeUtil.generateRandomString(2));
        final CodeLabels itemCL = new CodeLabels(RandomizeUtil.generateRandomString(2),
                RandomizeUtil.generateRandomString(2), RandomizeUtil.generateRandomString(2));
        final QtyUnit qtyToInsert = new QtyUnit(1, RandomizeUtil.generateRandomString(2));
        final List<Xplanrmd> lstI4 = new ArrayList<Xplanrmd>();
        Xplanrmd xplanrmd = new Xplanrmd();
        xplanrmd.setCsoc(RandomizeUtil.generateRandomString(2));
        xplanrmd.setCetab(RandomizeUtil.generateRandomString(2));
        xplanrmd.setPrechrof(RandomizeUtil.generateRandomString(2));
        xplanrmd.setChronof(RandomizeUtil.generateRandomUnsignedInt());
        xplanrmd.setCplanr(RandomizeUtil.generateRandomString(2));
        xplanrmd.setCcond(RandomizeUtil.generateRandomString(2));
        xplanrmd.setNologm(RandomizeUtil.generateRandomUnsignedInt());
        lstI4.add(xplanrmd);

        final List<Xplanrmd> lstI2 = new ArrayList<Xplanrmd>();
        xplanrmd = new Xplanrmd();
        xplanrmd.setTplein(false);
        xplanrmd.setCart(RandomizeUtil.generateRandomString(2));
        xplanrmd.setQteprev(RandomizeUtil.generateRandomUnsignedDouble());
        xplanrmd.setQtefait(xplanrmd.getQteprev() - 1);
        lstI2.add(xplanrmd);

        try {

            sbsUT.getLogger().setLevel(Level.DEBUG);

            context.checking(new Expectations() {
                {
                    oneOf(xplanrmdDAO).listByI4(with(any(AbstractDBContext.class)),
                            with(containerKey.getEstablishmentKey().getCsoc()),
                            with(containerKey.getEstablishmentKey().getCetab()),
                            with(containerKey.getContainerNumber()), with(false));
                    will(returnValue(lstI4));

                    oneOf(xplanrmdDAO).listByPK(with(any(AbstractDBContext.class)), with(lstI4.get(0).getCsoc()),
                            with(lstI4.get(0).getCetab()), with(lstI4.get(0).getPrechrof()),
                            with(lstI4.get(0).getChronof()), with(lstI4.get(0).getCplanr()), with(aNull(String.class)),
                            with(lstI4.get(0).getCcond()), with(lstI4.get(0).getNologm()), with(aNull(String.class)),
                            with(false));
                    will(returnValue(lstI2));
                }
            });

            boolean ret = sbsUT.isCurrentContainerFull(getCtx(), containerKey, itemCL, qtyToInsert);

            Assert.assertFalse(ret);

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        } catch (DAOException e) {
            fail("DAOException thrown");
        }
    }

    @Test
    public void testIsThereContainerWithThisQty() {

        final ContainerKey containerKey = new ContainerKey(new EstablishmentKey(RandomizeUtil.generateRandomString(2),
                RandomizeUtil.generateRandomString(2)), RandomizeUtil.generateRandomString(2));
        final MOKey moKey = new MOKey();
        moKey.setChrono(new Chrono(RandomizeUtil.generateRandomString(2), RandomizeUtil.generateRandomUnsignedInt()));
        moKey.setEstablishmentKey(new EstablishmentKey(RandomizeUtil.generateRandomString(2), RandomizeUtil
                .generateRandomString(2)));
        final String cart = RandomizeUtil.generateRandomString(5);

        final List<Xplanrmd> lstI2 = new ArrayList<Xplanrmd>();
        Xplanrmd xplanrmd = new Xplanrmd();
        xplanrmd.setTplein(false);
        xplanrmd.setCart(RandomizeUtil.generateRandomString(2));
        xplanrmd.setQteprev(RandomizeUtil.generateRandomUnsignedDouble());
        xplanrmd.setQtefait(xplanrmd.getQteprev() - 1);
        lstI2.add(xplanrmd);

        final QtyUnit qtyNeeded = new QtyUnit(xplanrmd.getQtefait() + xplanrmd.getQteprev() - 1,
                RandomizeUtil.generateRandomString(2));

        try {

            sbsUT.getLogger().setLevel(Level.DEBUG);

            context.checking(new Expectations() {
                {
                    oneOf(xplanrmdDAO).listByI2(with(any(AbstractDBContext.class)),
                            with(containerKey.getEstablishmentKey().getCsoc()),
                            with(containerKey.getEstablishmentKey().getCetab()), with(moKey.getChrono().getPrechro()),
                            with(moKey.getChrono().getChrono()), with(aNull(String.class)), with(false), with(cart),
                            with(aNull(String.class)), with(aNull(Integer.class)), with(aNull(String.class)),
                            with(false));
                    will(returnValue(lstI2));
                }
            });

            boolean ret = sbsUT.isThereContainerWithThisQty(getCtx(), containerKey, qtyNeeded, moKey, cart);

            Assert.assertTrue(ret);

        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        } catch (DAOException e) {
            fail("DAOException thrown");
        }
    }

    /**
     * Get the VIFContext.
     * 
     * @return
     */
    private VIFContext getCtx() {
        return VIFContextMockManager.getVIFContext();
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: RandomizeUtil.java,v $
 * Created on 29 août 2013 by xg
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.common;


import java.util.Date;
import java.util.Random;

import fr.vif.vif5_7.activities.activities.constants.Mnemos.DeclarationMode;


/**
 * RandomizeUtil.
 * 
 * @author xg
 */
public class RandomizeUtil {

    /**
     * ctor.
     */
    private RandomizeUtil() {
    }

    /**
     * Generate a random boolean.
     * 
     * @return a boolean
     */
    public static boolean generateRandomBoolean() {
        Random rd = new Random();
        return rd.nextBoolean();
    }

    /**
     * Generate a random java.util.Date.
     * 
     * @return a Date
     */
    public static Date generateRandomDate() {
        return new Date(Math.abs(System.currentTimeMillis() - new Random().nextLong()));
    }

    /**
     * Generates a random DeclarationMode.
     * 
     * @return a random DeclarationMode
     */
    public static DeclarationMode generateRandomDeclarationMode() {
        int pick = new Random().nextInt(DeclarationMode.values().length);
        return DeclarationMode.values()[pick];
    }

    /**
     * Generate a random signed double.
     * 
     * @return a double value
     */
    public static double generateRandomSignedDouble() {
        Random rd = new Random();
        return rd.nextDouble();
    }

    /**
     * Generate a random signed int.
     * 
     * @return an int value
     */
    public static int generateRandomSignedInt() {
        Random rd = new Random();
        return rd.nextInt();
    }

    /**
     * Generate a random String, with the given length.
     * 
     * @param length the wished length
     * @return the random string
     */
    public static String generateRandomString(final int length) {
        String chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuffer str = new StringBuffer();
        for (int x = 0; x < length; x++) {
            int i = (int) Math.floor(Math.random() * (chars.length() - 1));
            str.append(chars.charAt(i));
        }
        return str.toString();
    }

    /**
     * Generate a random signed double.
     * 
     * @return a double value
     */
    public static double generateRandomUnsignedDouble() {
        return Math.abs(generateRandomSignedDouble());
    }

    /**
     * Generate a random signed int.
     * 
     * @return an int value
     */
    public static int generateRandomUnsignedInt() {
        return Math.abs(generateRandomSignedInt());
    }

}

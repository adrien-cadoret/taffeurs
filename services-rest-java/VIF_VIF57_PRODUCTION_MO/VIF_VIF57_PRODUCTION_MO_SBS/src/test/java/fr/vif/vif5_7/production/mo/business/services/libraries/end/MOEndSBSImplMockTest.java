/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS-HEAD
 * File : $RCSfile: MOEndSBSImplMockTest.java,v $
 * Created on Mar 11, 2016 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.end;


import junit.framework.Assert;

import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.VIFContextMockManager;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.dao.AbstractDBContext;
import fr.vif.jtech.dao.DaoConfigurationJmockTest;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.jmock.JMockContext;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.common.RandomizeUtil;
import fr.vif.vif5_7.production.mo.dao.xpiloia.XpiloiaDAO;


/**
 * Mock test for MOEndSBSImplMock.
 *
 * @author cj
 */
@RunWith(JMock.class)
public class MOEndSBSImplMockTest {
    /** Main JMock context. */
    public static Mockery           context;

    /** SBS under test. */
    private static MOEndSBSImpl     sbsUT;

    /** Mock object. */
    private static XpiloiaDAO       xpiloiaDAO;

    /** Test's constants. */
    private static final String     COMPANY_CODE        = "TG";
    private static final String     ESTABLISHEMENT_CODE = "01";
    private static final String     PRECHRO             = "pre3";
    private static final int        CHRONO              = RandomizeUtil.generateRandomUnsignedInt();
    private static final String     WORKSTATION         = "wstaID";

    /** Test's datas. */
    private static EstablishmentKey establishmentKey    = new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE);
    private static MOKey            moKey               = new MOKey(establishmentKey, new Chrono(PRECHRO, CHRONO));

    /** Reload all the data tests. */
    public static void reloadDataTests() {
        establishmentKey = new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE);
        moKey = new MOKey(establishmentKey, new Chrono(PRECHRO, CHRONO));
    }

    /**
     * Runs once before all tests.
     * 
     * @throws Exception if error occurs
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        context = JMockContext.getNewContext();
        /* initialize the sbs under test */
        sbsUT = new MOEndSBSImpl();

        /* Configure DAO retrieving mock objects */
        xpiloiaDAO = DaoConfigurationJmockTest.addMockToDAO(XpiloiaDAO.class);
        reloadDataTests();
    }

    /**
     * Get the VIFContext.
     * 
     * @return
     */
    private VIFContext getCtx() {
        return VIFContextMockManager.getVIFContext();
    }

    /**
     * Test method for
     * {@link fr.vif.vif5_7.production.mo.business.services.libraries.end.MOEndSBSImpl#isFinishedMO(fr.vif.jtech.business.VIFContext, fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey, fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType, java.lang.String)}
     * .
     */
    @Test
    public void testIsFinishedMO() {
        try {
            context.checking(new Expectations() {
                {
                    oneOf(xpiloiaDAO).isFinishedMO(with(any(AbstractDBContext.class)), with(moKey),
                            with(ActivityItemType.INPUT), with(WORKSTATION));
                    will(returnValue(true));
                }
            });

            boolean result = sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, WORKSTATION);
            Assert.assertTrue(result);
        } catch (ServerBusinessException | DAOException e) {
            Assert.fail(e.getMessage()); // no exception excepted
        }

        try {
            sbsUT.isFinishedMO(getCtx(), null, ActivityItemType.INPUT, WORKSTATION);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }

        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }

    @Test
    public void testIsFinishedMO_Error_1() {
        moKey.getChrono().setChrono(0);
        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }

    @Test
    public void testIsFinishedMO_Error_2() {
        moKey.setEstablishmentKey(null);
        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }

    @Test
    public void testIsFinishedMO_Error_3() {
        moKey.getEstablishmentKey().setCsoc(null);
        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }

    @Test
    public void testIsFinishedMO_Error_4() {
        moKey.getEstablishmentKey().setCsoc("");
        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }

    @Test
    public void testIsFinishedMO_Error_5() {
        moKey.getEstablishmentKey().setCetab(null);
        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }

    @Test
    public void testIsFinishedMO_Error_6() {
        moKey.getEstablishmentKey().setCetab("");
        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }

    @Test
    public void testIsFinishedMO_Error_7() {
        moKey.setChrono(null);
        ;
        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }

    @Test
    public void testIsFinishedMO_Error_8() {
        moKey.getChrono().setPrechro(null);
        ;
        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }

    @Test
    public void testIsFinishedMO_Error_9() {
        moKey.getChrono().setPrechro("");
        ;
        try {
            sbsUT.isFinishedMO(getCtx(), moKey, ActivityItemType.INPUT, null);
            Assert.fail("Exception expected");
        } catch (ServerBusinessException e) {
            // exception excepted
        }
    }
}

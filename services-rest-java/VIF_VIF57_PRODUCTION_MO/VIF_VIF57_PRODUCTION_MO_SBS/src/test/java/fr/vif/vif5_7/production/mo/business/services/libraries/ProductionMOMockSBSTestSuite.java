/*
 * Copyright (c) 2012 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: ProductionMOMockSBSTestSuite.java,v $
 * Created on 4 oct. 2012 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import fr.vif.vif5_7.production.mo.business.services.libraries.boning.BoningMOSBSMockTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.end.MOEndSBSImplMockTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.indicators.JMockIndicatorMOSBSImplTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.labortime.LaborTimeSBSMockTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBSImplMockTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.operationitem.MoItemSBSImplTest;
import fr.vif.vif5_7.production.mo.business.services.libraries.stackingplan.StackingPlanSBSMockTest;


/**
 * Launch the suite of tests.
 * 
 * @author cj
 */
@RunWith(Suite.class)
@SuiteClasses(value = { BoningMOSBSMockTest.class, //
        MOEndSBSImplMockTest.class, //
        JMockIndicatorMOSBSImplTest.class, //
        LaborTimeSBSMockTest.class, //
        MOSBSImplMockTest.class, //
        MoItemSBSImplTest.class, //
        BoningMOSBSMockTest.class, //
        StackingPlanSBSMockTest.class })
public class ProductionMOMockSBSTestSuite {

}

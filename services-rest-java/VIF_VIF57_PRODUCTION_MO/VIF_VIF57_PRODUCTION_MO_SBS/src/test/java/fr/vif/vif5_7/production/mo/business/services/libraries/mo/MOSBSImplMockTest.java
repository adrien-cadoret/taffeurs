/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS-HEAD
 * File : $RCSfile: MOSBSImplMockTest.java,v $
 * Created on Mar 11, 2016 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.mo;


import static org.junit.Assert.fail;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.VIFContextMockManager;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.ObjectHelper;
import fr.vif.jtech.dao.AbstractDBContext;
import fr.vif.jtech.dao.DaoConfigurationJmockTest;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.jmock.JMockContext;
import fr.vif.vif5_7.activities.activities.business.beans.common.activity.ActivityHeader;
import fr.vif.vif5_7.activities.activities.business.beans.common.soa.Soa;
import fr.vif.vif5_7.activities.activities.business.services.libraries.activities.ActivitiesSBS;
import fr.vif.vif5_7.activities.activities.business.services.libraries.soa.SoaSBS;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.admin.config.business.services.libraries.parameter.ParameterSBS;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.comment.business.beans.common.Comment;
import fr.vif.vif5_7.gen.comment.business.beans.common.CommentKey;
import fr.vif.vif5_7.gen.comment.business.services.libraries.simplecomment.SimpleCommentSBS;
import fr.vif.vif5_7.gen.comment.constants.Mnemos.CommentDomain;
import fr.vif.vif5_7.gen.hierarchy.business.beans.common.pyramid.PyramidFastSelection;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemKey;
import fr.vif.vif5_7.gen.item.business.beans.features.fitem.FItemSBean;
import fr.vif.vif5_7.gen.item.business.services.libraries.item.ItemSBS;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.services.libraries.chrono.ChronoSBS;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBoningBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOSimpleSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.common.RandomizeUtil;
import fr.vif.vif5_7.production.mo.business.services.libraries.stackingplan.StackingPlanSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOUse;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.Progress;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.Sorts;
import fr.vif.vif5_7.production.mo.dao.kof.Kof;
import fr.vif.vif5_7.production.mo.dao.kof.KofDAO;
import fr.vif.vif5_7.production.mo.dao.kot.Kot;
import fr.vif.vif5_7.production.mo.dao.kot.KotDAO;


/**
 * Mock test for MOSBSImpl.
 *
 * @author cj
 */
@RunWith(JMock.class)
public class MOSBSImplMockTest {
    private class FItemSBeanMatch extends TypeSafeMatcher<FItemSBean> {
        private int        marging;
        private Date       compareDate;
        private FItemSBean fitemSBean;

        /**
         * @param compareDate
         * @param marging
         */
        public FItemSBeanMatch(final Date compareDate, final int marging, final FItemSBean fitemSBean) {
            super();
            this.compareDate = compareDate;
            this.marging = marging;
            this.fitemSBean = fitemSBean;
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public void describeTo(final Description arg0) {
        }

        /**
         * {@inheritDoc}
         */
        @Override
        public boolean matchesSafely(final FItemSBean arg0) {
            if (arg0 == null) {
                return false;
            } else if (fitemSBean.getClass().equals(arg0.getClass())) {
                return ObjectHelper.equalsArray(getPropertiesArray(fitemSBean), getPropertiesArray(arg0))
                        && Math.abs(arg0.getValidityDate().getTime() - compareDate.getTime()) < marging;
            } else {
                return false;
            }
        }

        /**
         * Used to retrieve properties array.
         * 
         * @param bean the bean.
         * @return the array.
         */
        private Object[] getPropertiesArray(final FItemSBean bean) {
            return new Object[] { bean.getAll(), bean.getCetab(), bean.getCsoc(), bean.getExtcodes(), bean.getFree(),
                    bean.getHoldInStock(), bean.getItemGroup(), bean.getKindOfItem(), bean.getMade(),
                    bean.getOneMoreSelectionDone(), bean.getPurchased(), bean.getPyramid(), bean.getSold(),
                    bean.getSort(), bean.getValidity(), bean.getSavePermanentSelection() };
        }
    }

    /** Main JMock context. */
    public static Mockery           context;

    /** SBS under test. */
    private static MOSBSImpl        sbsUT;

    /** Mock object. */
    private static KofDAO           kofDAO;
    private static KotDAO           kotDAO;
    private static SimpleCommentSBS commentSBS;
    private static ItemSBS          itemSBS;
    private static ParameterSBS     parameterSBS;
    private static ActivitiesSBS    activitiesSBS;
    private static StackingPlanSBS  stackingPlanSBS;
    private static ChronoSBS        chronoSBS;
    private static SoaSBS           soaSBS;

    /** Test's constants. */
    private static final String     COMPANY_CODE        = "TG";
    private static final String     ESTABLISHEMENT_CODE = "01";
    private static final String     WORKSTATION         = RandomizeUtil.generateRandomString(3);
    private static final String     PRECHRO             = RandomizeUtil.generateRandomString(3);
    private static final int        CHRONO_MO           = RandomizeUtil.generateRandomUnsignedInt();
    private static final int        NI_1                = RandomizeUtil.generateRandomUnsignedInt();
    private static final int        NI_2                = RandomizeUtil.generateRandomUnsignedInt();
    private static final int        NI_3                = RandomizeUtil.generateRandomUnsignedInt();
    private static final int        NI_4                = RandomizeUtil.generateRandomUnsignedInt();
    private static final double     QTY_1               = RandomizeUtil.generateRandomUnsignedDouble();
    private static final double     QTY_2               = RandomizeUtil.generateRandomUnsignedDouble();
    private static final double     QTY_3               = RandomizeUtil.generateRandomUnsignedDouble();
    private static final double     QTY_4               = RandomizeUtil.generateRandomUnsignedDouble();
    private static final double     QTY_5               = RandomizeUtil.generateRandomUnsignedDouble();
    private static final double     QTY_6               = RandomizeUtil.generateRandomUnsignedDouble();
    private static final String     UNIT_1              = RandomizeUtil.generateRandomString(3);
    private static final String     UNIT_2              = RandomizeUtil.generateRandomString(3);
    private static final String     UNIT_3              = RandomizeUtil.generateRandomString(3);
    private static final String     ACT_ID              = RandomizeUtil.generateRandomString(5);
    private static final String     SOA_ID              = RandomizeUtil.generateRandomString(5);
    private static final String     ACT_FAM_ID          = RandomizeUtil.generateRandomString(3);
    private static final String     COMMENT             = RandomizeUtil.generateRandomString(30);
    private static final String     COMMENT_2           = RandomizeUtil.generateRandomString(30);
    private static final String     ITEM_ID             = RandomizeUtil.generateRandomString(5);
    private static final String     ITEM_ID_2           = RandomizeUtil.generateRandomString(5);
    private static final String     STACKING_PLAN_ID    = RandomizeUtil.generateRandomString(10);
    private static final String     RESOURCE_ID         = RandomizeUtil.generateRandomString(10);

    /** Test's data. */
    private static MOKey            moKey;
    private static MOBean           moBean;
    private static EstablishmentKey establishmentKey;
    private static Kof              kof;
    private static Kot              kot;
    private static POKey            poKey;
    private static OperationItemKey oik;
    private static ActivityHeader   actHeader;
    private static Soa              soaHeader;

    /** Reload all the data tests. */
    public static void reloadDataTests() {
        establishmentKey = new EstablishmentKey(COMPANY_CODE, ESTABLISHEMENT_CODE);
        moKey = new MOKey(establishmentKey, new Chrono(PRECHRO, CHRONO_MO));
        poKey = new POKey(establishmentKey, new ChronoPO(PRECHRO, CHRONO_MO, NI_1));
        oik = new OperationItemKey(ManagementType.REAL, establishmentKey, new Chrono(PRECHRO, CHRONO_MO), NI_1, NI_2,
                NI_3, NI_4);

        moBean = new MOBean();
        moBean.setMoKey(moKey);
        moBean.getMoQuantityUnit().setCompletedQuantity(QTY_1);
        moBean.getMoQuantityUnit().setEffectiveQuantity(QTY_2);
        moBean.getMoQuantityUnit().setEffectiveUnit(UNIT_1);
        moBean.getMoQuantityUnit().setTheoricQuantity(QTY_3);
        moBean.getMoQuantityUnit().setToDoQuantity(QTY_4);
        moBean.getMoQuantityUnit().setUnit(UNIT_2);

        kof = new Kof();
        kof.setCact(ACT_ID);
        kof.setCalauto(RandomizeUtil.generateRandomString(5));
        kof.setCart(RandomizeUtil.generateRandomString(5));
        kof.setCarts(ITEM_ID);
        kof.setCavanc(RandomizeUtil.generateRandomString(5));
        kof.setCequip(RandomizeUtil.generateRandomString(5));
        kof.setCetab(ESTABLISHEMENT_CODE);
        kof.setCetabor(RandomizeUtil.generateRandomString(5));
        kof.setCetat(RandomizeUtil.generateRandomString(5));
        kof.setCformus(RandomizeUtil.generateRandomString(5));
        kof.setChrono(CHRONO_MO);
        kof.setChronocr(RandomizeUtil.generateRandomUnsignedInt());
        kof.setChronor(RandomizeUtil.generateRandomUnsignedInt());
        kof.setCiti(SOA_ID);
        kof.setCnatieor(RandomizeUtil.generateRandomString(5));
        kof.setCofuse(RandomizeUtil.generateRandomString(5));
        kof.setCprop(RandomizeUtil.generateRandomString(5));
        kof.setCres(RandomizeUtil.generateRandomString(5));
        kof.setCresprim(RandomizeUtil.generateRandomString(5));
        kof.setCrges(RandomizeUtil.generateRandomString(5));
        kof.setCsce(RandomizeUtil.generateRandomString(5));
        kof.setCsit(RandomizeUtil.generateRandomString(5));
        kof.setCsoc(COMPANY_CODE);
        kof.setCsocor(RandomizeUtil.generateRandomString(5));
        kof.setCtieor(RandomizeUtil.generateRandomString(5));
        kof.setCu2(UNIT_2);
        kof.setCunite(UNIT_1);
        kof.setCuser(RandomizeUtil.generateRandomString(5));
        kof.setCustk2(RandomizeUtil.generateRandomString(5));
        kof.setCv(RandomizeUtil.generateRandomString(5));
        kof.setCvs(RandomizeUtil.generateRandomString(5));
        kof.setDuree(RandomizeUtil.generateRandomUnsignedInt());
        kof.setDuree2(RandomizeUtil.generateRandomUnsignedDouble());
        kof.setDureer(RandomizeUtil.generateRandomUnsignedInt());
        kof.setHeurdeb(RandomizeUtil.generateRandomUnsignedInt());
        kof.setHeurdebr(RandomizeUtil.generateRandomUnsignedInt());
        kof.setHeurfin(RandomizeUtil.generateRandomUnsignedInt());
        kof.setHeurfinr(RandomizeUtil.generateRandomUnsignedInt());
        kof.setLof(RandomizeUtil.generateRandomString(5));
        kof.setLot(RandomizeUtil.generateRandomString(5));
        kof.setLotent(RandomizeUtil.generateRandomString(5));
        kof.setLotq(RandomizeUtil.generateRandomUnsignedDouble());
        kof.setLotq2(RandomizeUtil.generateRandomUnsignedDouble());
        kof.setNi1or(RandomizeUtil.generateRandomUnsignedInt());
        kof.setNi2artref(RandomizeUtil.generateRandomUnsignedInt());
        kof.setNi2or(RandomizeUtil.generateRandomUnsignedInt());
        kof.setOrig(RandomizeUtil.generateRandomString(5));
        kof.setPrechro(PRECHRO);
        kof.setPrechrocr(RandomizeUtil.generateRandomString(5));
        kof.setPrechror(RandomizeUtil.generateRandomString(5));
        kof.setPrior(RandomizeUtil.generateRandomUnsignedInt());
        kof.setQte1(QTY_1);
        kof.setQte2(QTY_2);
        kof.setQte3(QTY_3);
        kof.setQtefr1(QTY_4);
        kof.setQtefr2(QTY_5);
        kof.setQtefr3(QTY_6);
        kof.setQtestk(RandomizeUtil.generateRandomUnsignedDouble());
        kof.setQtestk2(RandomizeUtil.generateRandomUnsignedDouble());
        kof.setRof(RandomizeUtil.generateRandomString(5));
        kof.setTxfr(RandomizeUtil.generateRandomUnsignedDouble());
        kof.setTypflux("POU");
        kof.setTypof("ORI");
        kof.setNi1artref(NI_1);

        kot = new Kot();
        kot.setCsoc(COMPANY_CODE);
        kot.setCetab(ESTABLISHEMENT_CODE);
        kot.setQtestk(QTY_5);
        kot.setCunite(UNIT_3);
        kot.setCfmact(ACT_FAM_ID);
        kot.setPrechro(PRECHRO);
        kot.setChrono(CHRONO_MO);
        kot.setNi1(NI_1);
        kot.setCres(RESOURCE_ID);
        kot.setQte1(QTY_1);
        kot.setQte2(QTY_2);
        kot.setQte3(QTY_3);
        kot.setCart(ITEM_ID);
        kot.setCact(ACT_ID);
        kot.setCavanc("TEC");

        actHeader = new ActivityHeader();
        actHeader.setActivityCL(new CodeLabels(ACT_ID, "lact", "shortLabelAct"));
        actHeader.setActivityFamilyCL(new CodeLabel(ACT_FAM_ID, "fam_act_label"));

        soaHeader = new Soa();
        soaHeader.setSoaCL(new CodeLabels(SOA_ID, "labelSOa", "shortLabelSOA"));
    }

    /**
     * Runs once before all tests.
     * 
     * @throws Exception if error occurs
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        context = JMockContext.getNewContext();
        /* initialize the sbs under test */
        sbsUT = new MOSBSImpl();

        /* Configure DAO retrieving mock objects */
        kofDAO = DaoConfigurationJmockTest.addMockToDAO(KofDAO.class);
        kotDAO = DaoConfigurationJmockTest.addMockToDAO(KotDAO.class);

        commentSBS = context.mock(SimpleCommentSBS.class);
        sbsUT.setCommentSBS(commentSBS);

        itemSBS = context.mock(ItemSBS.class);
        sbsUT.setItemSBS(itemSBS);

        activitiesSBS = context.mock(ActivitiesSBS.class);
        sbsUT.setActivitiesSBS(activitiesSBS);

        stackingPlanSBS = context.mock(StackingPlanSBS.class);
        sbsUT.setStackingPlanSBS(stackingPlanSBS);

        parameterSBS = context.mock(ParameterSBS.class);
        sbsUT.setParameterSBS(parameterSBS);

        chronoSBS = context.mock(ChronoSBS.class);
        sbsUT.setChronoSBS(chronoSBS);

        soaSBS = context.mock(SoaSBS.class);
        sbsUT.setSoaSBS(soaSBS);

        reloadDataTests();
    }

    @Test
    public void testCalculateQuantityLeftOnOriginMO() {
        try {

            context.checking(new Expectations() {
                {
                    oneOf(kotDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(PRECHRO), with(CHRONO_MO), with(NI_1));
                    will(returnValue(kot));
                }
            });
            sbsUT.calculateQuantityLeftOnOriginMO(getCtx(), moBean, kof);
            Assert.assertNotNull(moBean);
            Assert.assertEquals(QTY_5, moBean.getMoQuantityUnit().getEffectiveQuantity(), 0);
            Assert.assertEquals(UNIT_3, moBean.getMoQuantityUnit().getEffectiveUnit());
        } catch (ServerBusinessException | DAOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetActivityFamilyFromPO() {
        try {
            context.checking(new Expectations() {
                {
                    oneOf(kotDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(PRECHRO), with(CHRONO_MO), with(NI_1));
                    will(returnValue(kot));
                }
            });
            String result = sbsUT.getActivityFamilyFromPO(getCtx(), poKey);
            Assert.assertNotNull(result);
            Assert.assertEquals(ACT_FAM_ID, result);
        } catch (ServerBusinessException | DAOException e) {
            fail(e.getMessage());
        }
        // Exception 1
        try {
            sbsUT.getActivityFamilyFromPO(getCtx(), null);
            fail("Exception expected");
        } catch (ServerBusinessException e) {
            // expected
        }
        // Exception 2
        try {
            sbsUT.getActivityFamilyFromPO(getCtx(), new POKey(null, null));
            fail("Exception expected");
        } catch (ServerBusinessException e) {
            // expected
        }
        // Exception 3
        try {
            sbsUT.getActivityFamilyFromPO(getCtx(), new POKey(establishmentKey, null));
            fail("Exception expected");
        } catch (ServerBusinessException e) {
            // expected
        }
    }

    @Test
    public void testGetComment_MO() {
        try {

            context.checking(new Expectations() {
                {
                    oneOf(commentSBS).getComment(with(any(VIFContext.class)), with(CommentDomain.PRODUCTION),
                            with(new CommentKey(COMPANY_CODE, ESTABLISHEMENT_CODE, "COM", PRECHRO + "," + CHRONO_MO)));
                    will(returnValue(new Comment(COMPANY_CODE, ESTABLISHEMENT_CODE, "COM", PRECHRO + "," + CHRONO_MO,
                            COMMENT, new Date(), "")));
                }
            });
            Comment result = sbsUT.getComment(getCtx(), moKey);
            Assert.assertNotNull(result);
            Assert.assertEquals(COMMENT, result.getCom());
            Assert.assertEquals(new CommentKey(COMPANY_CODE, ESTABLISHEMENT_CODE, "COM", PRECHRO + "," + CHRONO_MO),
                    result.getKey());
        } catch (ServerBusinessException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetComment_OIK() {
        try {

            context.checking(new Expectations() {
                {
                    oneOf(commentSBS).getComment(
                            with(any(VIFContext.class)),
                            with(CommentDomain.PRODUCTION),
                            with(new CommentKey(COMPANY_CODE, ESTABLISHEMENT_CODE, "OT", PRECHRO + "," + CHRONO_MO
                                    + "," + NI_1 + "," + NI_4)));
                    will(returnValue(new Comment(COMPANY_CODE, ESTABLISHEMENT_CODE, "OT", PRECHRO + "," + CHRONO_MO
                            + "," + NI_1 + "," + NI_4, COMMENT, new Date(), "")));

                    oneOf(commentSBS).getComment(
                            with(any(VIFContext.class)),
                            with(CommentDomain.PRODUCTION),
                            with(new CommentKey(COMPANY_CODE, ESTABLISHEMENT_CODE, "AAO", PRECHRO + "," + CHRONO_MO
                                    + "," + NI_1 + "," + NI_4)));
                    will(returnValue(new Comment(COMPANY_CODE, ESTABLISHEMENT_CODE, "OT", PRECHRO + "," + CHRONO_MO
                            + "," + NI_1 + "," + NI_4, COMMENT_2, new Date(), "")));
                }
            });
            Comment result = sbsUT.getComment(getCtx(), oik);
            Assert.assertNotNull(result);
            String msg = I18nServerManager.translate(getCtx().getIdCtx().getLocale(), false, ProductionMo.T34548);
            Assert.assertEquals(COMMENT + "   " + msg + ": " + COMMENT_2, result.getCom());
            Assert.assertEquals(new CommentKey(COMPANY_CODE, ESTABLISHEMENT_CODE, "OT", PRECHRO + "," + CHRONO_MO + ","
                    + NI_1 + "," + NI_4), result.getKey());
        } catch (ServerBusinessException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetComment_OT() {
        try {

            context.checking(new Expectations() {
                {
                    oneOf(commentSBS).getComment(
                            with(any(VIFContext.class)),
                            with(CommentDomain.PRODUCTION),
                            with(new CommentKey(COMPANY_CODE, ESTABLISHEMENT_CODE, "OT", PRECHRO + "," + CHRONO_MO
                                    + "," + NI_1)));
                    will(returnValue(new Comment(COMPANY_CODE, ESTABLISHEMENT_CODE, "OT", PRECHRO + "," + CHRONO_MO
                            + "," + NI_1, COMMENT, new Date(), "")));
                }
            });
            Comment result = sbsUT.getComment(getCtx(), poKey);
            Assert.assertNotNull(result);
            Assert.assertEquals(COMMENT, result.getCom());
            Assert.assertEquals(new CommentKey(COMPANY_CODE, ESTABLISHEMENT_CODE, "OT", PRECHRO + "," + CHRONO_MO + ","
                    + NI_1), result.getKey());
        } catch (ServerBusinessException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetMOBeanByKey() {
        ActivityHeader actHeader = new ActivityHeader();
        actHeader.setActivityCL(new CodeLabels(ACT_ID, "lact", "shortLabelAct"));
        actHeader.setActivityFamilyCL(new CodeLabel(ACT_FAM_ID, "fam_act_label"));
        try {

            context.checking(new Expectations() {
                {
                    oneOf(kofDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(PRECHRO), with(CHRONO_MO));
                    will(returnValue(kof));

                    oneOf(itemSBS).getItemCLByPK(with(any(VIFContext.class)), with(new ItemKey(COMPANY_CODE, ITEM_ID)));
                    will(returnValue(new CodeLabels(ITEM_ID, "lbl-item", "ritem")));

                    oneOf(activitiesSBS).getActivityHeader(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(ACT_ID));
                    will(returnValue(actHeader));
                }
            });
            MOBean result = sbsUT.getMOBeanByKey(getCtx(), moKey);
            Assert.assertNotNull(result);
            Assert.assertEquals(moKey, result.getMoKey());
            Assert.assertEquals(ACT_ID, result.getFabrication().getFabricationCLs().getCode());
            Assert.assertEquals(ACT_FAM_ID, result.getFabrication().getFamilyCL().getCode());
            Assert.assertEquals(ITEM_ID, result.getItemCLs().getCode());
        } catch (ServerBusinessException | DAOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetMOBoningBeanByKey() {

        try {

            context.checking(new Expectations() {
                {
                    oneOf(kofDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(PRECHRO), with(CHRONO_MO));
                    will(returnValue(kof));

                    oneOf(itemSBS).getItemCLByPK(with(any(VIFContext.class)), with(new ItemKey(COMPANY_CODE, ITEM_ID)));
                    will(returnValue(new CodeLabels(ITEM_ID, "lbl-item", "ritem")));

                    oneOf(activitiesSBS).getActivityHeader(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(ACT_ID));
                    will(returnValue(actHeader));

                    oneOf(stackingPlanSBS).getMOStackingPlanCode(with(any(VIFContext.class)), with(moKey));
                    will(returnValue(new CodeLabel(STACKING_PLAN_ID, "stak_plan-lbl")));
                }
            });
            MOBoningBean result = sbsUT.getMOBoningBeanByKey(getCtx(), moKey);
            Assert.assertNotNull(result);
            Assert.assertEquals(moKey, result.getMoKey());
            Assert.assertEquals(ACT_ID, result.getFabrication().getFabricationCLs().getCode());
            Assert.assertEquals(ACT_FAM_ID, result.getFabrication().getFamilyCL().getCode());
            Assert.assertEquals(ITEM_ID, result.getItemCLs().getCode());
            Assert.assertEquals(STACKING_PLAN_ID, result.getStackingPlan().getCode());
        } catch (ServerBusinessException | DAOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testGetPOBeanByKey() {
        try {

            context.checking(new Expectations() {
                {
                    oneOf(kotDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(PRECHRO), with(CHRONO_MO), with(NI_1));
                    will(returnValue(kot));

                    oneOf(itemSBS).getItemCLByPK(with(any(VIFContext.class)), with(new ItemKey(COMPANY_CODE, ITEM_ID)));
                    will(returnValue(new CodeLabels(ITEM_ID, "lbl-item", "ritem")));

                    oneOf(activitiesSBS).getActivityCLs(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(ACT_ID));
                    will(returnValue(new CodeLabels(ACT_ID, "lact", "shortLabelAct")));
                }
            });
            POBean result = sbsUT.getPOBeanByKey(getCtx(), poKey);
            Assert.assertNotNull(result);
            Assert.assertEquals(poKey, result.getPoKey());
            Assert.assertEquals(ACT_ID, result.getActivityCLs().getCode());
            Assert.assertEquals(ACT_FAM_ID, result.getActivityFamilyId());
            Assert.assertEquals(ITEM_ID, result.getItemCLs().getCode());
            Assert.assertEquals(Progress.PROGRESSING, result.getProgress());
        } catch (ServerBusinessException | DAOException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testListItemCLsForFabrication() {
        FItemSBean basicItemSelection = new FItemSBean(COMPANY_CODE, ESTABLISHEMENT_CODE, false);
        String groupId = RandomizeUtil.generateRandomString(7);
        try {

            context.checking(new Expectations() {
                {
                    oneOf(parameterSBS).readFieldValue(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with("X-CREAOF"), with(4), with(WORKSTATION));
                    will(returnValue(null));

                    oneOf(parameterSBS).readStringParameter(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with("X-CREAOF"), with(3), with(WORKSTATION));
                    will(returnValue(groupId));

                    oneOf(itemSBS).listItemCL(with(any(VIFContext.class)), with(any(PyramidFastSelection.class)),
                            with(basicItemSelection));
                    will(returnValue(Arrays.asList(ITEM_ID, ITEM_ID_2)));
                }
            });
            List<CodeLabels> result = sbsUT.listItemCLsForFabrication(getCtx(), COMPANY_CODE, ESTABLISHEMENT_CODE,
                    WORKSTATION, basicItemSelection);
            Assert.assertNotNull(result);
            Assert.assertEquals(2, result.size());
        } catch (ServerBusinessException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testListItemCLsForFabrication_2() {
        FItemSBean basicItemSelection = new FItemSBean(COMPANY_CODE, ESTABLISHEMENT_CODE, false);
        String groupId = RandomizeUtil.generateRandomString(7);
        try {

            context.checking(new Expectations() {
                {
                    oneOf(parameterSBS).readFieldValue(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with("X-CREAOF"), with(4), with(WORKSTATION));
                    will(returnValue(null));

                    oneOf(parameterSBS).readStringParameter(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with("X-CREAOF"), with(3), with(WORKSTATION));
                    will(returnValue(groupId));

                    oneOf(itemSBS).listItemCL(with(any(VIFContext.class)), with(any(PyramidFastSelection.class)),
                            with(basicItemSelection), with(3), with(19));
                    will(returnValue(Arrays.asList(ITEM_ID, ITEM_ID_2)));
                }
            });
            List<CodeLabels> result = sbsUT.listItemCLsForFabrication(getCtx(), COMPANY_CODE, ESTABLISHEMENT_CODE,
                    WORKSTATION, basicItemSelection, 3, 19);
            Assert.assertNotNull(result);
            Assert.assertEquals(2, result.size());
        } catch (ServerBusinessException e) {
            fail(e.getMessage());
        }
    }

    @Test
    public void testListMOBean() {
        MOSBean moSBean = new MOSBean();
        moSBean.setCompanyId(COMPANY_CODE);
        moSBean.setEstablishmentId(ESTABLISHEMENT_CODE);
        moSBean.setChrono(CHRONO_MO);
        moSBean.setItemCL(new CodeLabel(ITEM_ID, ""));
        moSBean.setWorkstationCL(new CodeLabel(WORKSTATION, ""));
        moSBean.setMoUses(Arrays.asList(MOUse.PRODUCTION));

        MOSimpleSBean sbean = new MOSimpleSBean();
        sbean.setCompanyId(COMPANY_CODE);
        sbean.setEstablishmentId(ESTABLISHEMENT_CODE);
        sbean.setWorkstationId(WORKSTATION);
        sbean.setSortMoList(Sorts.MO_LIST_DEFAULT_SORT);
        sbean.setPrechro("5OF");
        sbean.setChrono(CHRONO_MO);

        try {

            context.checking(new Expectations() {
                {
                    oneOf(chronoSBS).getPrechro(with(any(VIFContext.class)), with("OF"));
                    will(returnValue("5OF"));

                    oneOf(kofDAO).listKof(with(any(AbstractDBContext.class)), with(sbean),
                            with(Arrays.asList(ActivityItemType.INPUT)), with(true), with(3), with(19));
                    will(returnValue(Arrays.asList(kof)));

                    oneOf(itemSBS).listItemCL(with(any(VIFContext.class)),
                            with(new FItemSBeanMatch(new Date(), 250, new FItemSBean(COMPANY_CODE, null, true))),
                            with(Arrays.asList(ITEM_ID)));
                    will(returnValue(Arrays.asList(new CodeLabels(ITEM_ID, "lbl-item", "ritem"))));

                    oneOf(activitiesSBS).getActivityHeader(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(ACT_ID));
                    will(returnValue(actHeader));

                    oneOf(soaSBS).getSOAHeader(with(any(VIFContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(SOA_ID), with(DateHelper.getTodayAtMidnight().getTime()));
                    will(returnValue(soaHeader));

                    exactly(2).of(kotDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(PRECHRO), with(CHRONO_MO), with(NI_1));
                    will(returnValue(kot));

                }
            });
            List<MOBean> result = sbsUT.listMOBean(getCtx(), moSBean, Arrays.asList(ActivityItemType.INPUT), true, 3,
                    19);
            Assert.assertNotNull(result);
            Assert.assertEquals(1, result.size());
        } catch (ServerBusinessException | DAOException e) {
            fail(e.getMessage());
        }
    }

    /**
     * Get the VIFContext.
     * 
     * @return
     */
    private VIFContext getCtx() {
        return VIFContextMockManager.getVIFContext();
    }

}

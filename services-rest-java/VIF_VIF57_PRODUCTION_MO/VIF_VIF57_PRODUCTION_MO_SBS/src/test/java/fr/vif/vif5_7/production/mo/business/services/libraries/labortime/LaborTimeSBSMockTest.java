/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: LaborTimeSBSMockTest.java,v $
 * Created on 16 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.labortime;


import static org.junit.Assert.fail;

import java.util.Date;
import java.util.GregorianCalendar;

import junit.framework.Assert;

import org.apache.log4j.Level;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.VIFContextMockManager;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.dao.AbstractDBContext;
import fr.vif.jtech.dao.DaoConfigurationJmockTest;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.jmock.JMockContext;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTime;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;
import fr.vif.vif5_7.production.mo.dao.ktpsx.Ktpsx;
import fr.vif.vif5_7.production.mo.dao.ktpsx.KtpsxDAO;


/**
 * Labor Time Mock Class
 * 
 * @author nle
 */
@RunWith(JMock.class)
public class LaborTimeSBSMockTest {
    /**
     * Main JMock context.
     */
    public static Mockery           context;

    private static final String     COMPANY_CODE       = "03";
    private static final String     ESTABLISHMENT_CODE = "01";
    private static final String     CRES               = "M/O FAB";
    private static final String     CUSER              = "VIF";
    private static final int        NI1                = 1;
    private static Ktpsx            ktpsx;
    private static KtpsxDAO         ktpsxDAO;
    private static LaborTime        laborTime;
    private static LaborTimeSBS     laborTimeSBS;
    private static LaborTimeKey     laborTimeKey, laborTimeKey2;
    private static Date             DATPROD            = new GregorianCalendar(2012, 01, 01).getTime();

    /**
     * SBS under test.
     */
    private static LaborTimeSBSImpl sbsUT;

    /**
     * Reload all the data tests.
     */
    public static void reloadDataTests() {
        sbsUT.LOGGER.setLevel(Level.DEBUG);

        ktpsx = new Ktpsx();

        ktpsx.setCsoc(COMPANY_CODE);
        ktpsx.setCetab(ESTABLISHMENT_CODE);
        ktpsx.setDatprod(DATPROD);
        ktpsx.setCres(CRES);
        ktpsx.setNi1(NI1);
        ktpsx.setDatheurdebr(DATPROD);
        ktpsx.setDatheurfinr(DATPROD);
        ktpsx.setDatmod(DATPROD);
        ktpsx.setHeurmod(DATPROD);
        ktpsx.setCuser(CUSER);

        laborTimeKey = new LaborTimeKey(COMPANY_CODE, ESTABLISHMENT_CODE, DATPROD, CRES, NI1);

        laborTimeKey2 = new LaborTimeKey("", "", DATPROD, "", 0);

        laborTime = new LaborTime();
        laborTime.setLaborTimeKey(laborTimeKey);
        laborTime.setBegDateHour(DATPROD);
        laborTime.setEndDateHour(DATPROD);
        laborTime.setModificationDate(DATPROD);
        laborTime.setModificationUser(CUSER);
    }

    /**
     * Runs once before all tests.
     * 
     * @throws Exception if error occurs
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        context = JMockContext.getNewContext();

        /* initialize the sbs under test */
        sbsUT = new LaborTimeSBSImpl();

        /* Configure DAO retrieving mock objects */
        ktpsxDAO = DaoConfigurationJmockTest.addMockToDAO(KtpsxDAO.class);
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        reloadDataTests();
    }

    /**
     * 
     * @throws DAOException
     */
    @Test
    public void testGetLaborTime() throws DAOException {
        try {
            context.checking(new Expectations() {
                {
                    oneOf(ktpsxDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHMENT_CODE), with(DATPROD), with(CRES), with(NI1));
                    will(returnValue(ktpsx));
                }
            });
            LaborTime lt = sbsUT.getLaborTime(getCtx(), laborTimeKey);
            Assert.assertEquals(laborTime, lt);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        }
    }

    @Test
    public void testGetLaborTimeNull() throws DAOException {
        try {
            context.checking(new Expectations() {
                {
                    oneOf(ktpsxDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHMENT_CODE), with(DATPROD), with(CRES), with(NI1));
                    will(returnValue(null));
                }
            });
            LaborTime lt = sbsUT.getLaborTime(getCtx(), laborTimeKey);
            Assert.assertEquals(null, lt);
        } catch (ServerBusinessException e) {
            fail("ServerBusinessException thrown");
        }
    }

    @Test
    public void testGetLaborTimeWithError() throws DAOException {
        try {
            context.checking(new Expectations() {
                {
                    oneOf(ktpsxDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHMENT_CODE), with(DATPROD), with(CRES), with(NI1));
                    will(throwException(new DAOException("DAO_EXCEPTION_TEST")));

                }
            });
            sbsUT.getLaborTime(getCtx(), laborTimeKey);
            fail("ServerBusinessException is expected");
        } catch (ServerBusinessException e) {
            // Expected
        }
    }

    @Test
    public void testGetLaborTimeWithParamNull() {
        try {
            sbsUT.getLaborTime(getCtx(), laborTimeKey2);
            fail("ServerBusinessException is expected");

        } catch (ServerBusinessException e) {
            // expected
        }
    }

    /**
     * Get the VIFContext.
     * 
     * @return
     */
    private VIFContext getCtx() {
        return VIFContextMockManager.getVIFContext();
    }
}

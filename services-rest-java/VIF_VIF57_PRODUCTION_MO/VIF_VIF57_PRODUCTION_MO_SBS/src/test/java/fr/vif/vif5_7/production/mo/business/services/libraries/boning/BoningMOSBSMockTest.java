/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: BoningMOSBSMockTest.java,v $
 * Created on 18 avr. 2013 by xg
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.boning;


import java.util.Arrays;
import java.util.Date;
import java.util.Map;

import junit.framework.Assert;

import org.apache.log4j.Level;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.integration.junit4.JMock;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.VIFContextMockManager;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.dao.AbstractDBContext;
import fr.vif.jtech.dao.DaoConfigurationJmockTest;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.jtech.jmock.JMockContext;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.criteria.business.services.libraries.criteria.SpecificationSBS;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemKey;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.gen.service.business.services.libraries.unit.UnitSBS;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.StackingPlanInfoBean;
import fr.vif.vif5_7.production.mo.business.services.libraries.common.RandomizeUtil;
import fr.vif.vif5_7.production.mo.business.services.libraries.poitem.POItemSBS;
import fr.vif.vif5_7.production.mo.business.services.libraries.stackingplan.StackingPlanSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.PrechroId;
import fr.vif.vif5_7.production.mo.dao.kcriv.Kcriv;
import fr.vif.vif5_7.production.mo.dao.kcriv.KcrivDAO;
import fr.vif.vif5_7.production.mo.dao.kof.Kof;
import fr.vif.vif5_7.production.mo.dao.kof.KofDAO;
import fr.vif.vif5_7.production.mo.dao.kot.Kot;
import fr.vif.vif5_7.production.mo.dao.kot.KotDAO;
import fr.vif.vif5_7.production.mo.dao.kotad.Kotad;
import fr.vif.vif5_7.production.mo.dao.kotad.KotadDAO;


/**
 * 
 * @author xg
 */
@RunWith(JMock.class)
public class BoningMOSBSMockTest {

    /** Main JMock context. */
    public static Mockery                    context;

    /** SBS under test. */
    private static BoningMOSBSImpl           sbsUT;

    /** Mock object. */
    private static KcrivDAO                  kcrivDAO;
    private static KofDAO                    kofDAO;
    private static KotDAO                    kotDAO;
    private static KotadDAO                  kotadDAO;
    private static SpecificationSBS          specificationSBS;
    private static POItemSBS                 poItemSBS;
    private static UnitSBS                   unitSBS;
    private static StackingPlanSBS           stackingPlanSBS;

    /** Test's constants. */
    private static final String              COMPANY_CODE        = "TG";
    private static final String              ESTABLISHEMENT_CODE = "01";
    private static final String              ITEM1_CODE          = "cumart01";
    private static final String              ITEM1_LBL           = "cumart01";
    private static final String              CRITERIA_1_CODE     = "crit01";
    private static final String              CRITERIA_1_VALUE    = "customCritVal";
    private static final String              BATCH               = "batch32";
    private static final String              PRECHRO             = "pre3";
    private static final int                 CHRONO              = RandomizeUtil.generateRandomUnsignedInt();
    private static final int                 INNER               = RandomizeUtil.generateRandomUnsignedInt();
    private static final Date                mvtDate             = new Date();
    private static final double              QTY_1               = 10.10;
    private static final double              QTY_2               = 22.2;
    private static final double              QTY_3               = 3.333;
    private static final double              QTY_4               = 4.0;
    private static final double              QTY_5               = 0.5;
    private static final double              QTY_6               = 66.6;
    private static final String              UNIT                = "U3";
    private static final String              HCHY_VALUE          = "groupId";
    private static final String              STACKING_PLAN_ID    = "plrgt";

    /** Test's datas. */
    private static EstablishmentKey          establishmentKey    = new EstablishmentKey(COMPANY_CODE,
            ESTABLISHEMENT_CODE);
    private static ItemKey                   itemKey             = new ItemKey(COMPANY_CODE, ITEM1_CODE);
    private static CodeLabels                itemCL              = new CodeLabels(ITEM1_CODE, ITEM1_LBL, "rItem");
    private static MOKey                     moKey               = new MOKey(establishmentKey, new Chrono(PRECHRO,
            CHRONO));
    private static Kcriv                     cri;
    private static InputOperationItemBean    inputOperationItemBean;
    private static FBoningOutputBBean        bBean;
    private static OperationItemKey          oik;
    private static StackingPlanInfoBean      stackingPlanInfoBean;
    private static OperationItemQuantityUnit opItemQuantityUnit  = new OperationItemQuantityUnit(QTY_1, QTY_2, QTY_3,
            UNIT);

    /** Reload all the data tests. */
    public static void reloadDataTests() {
        cri = new Kcriv();
        cri.setValcmin(CRITERIA_1_CODE);

        oik = new OperationItemKey(ManagementType.REAL, establishmentKey, new Chrono(PRECHRO, CHRONO), 9, 7, 5, 3);

        inputOperationItemBean = new InputOperationItemBean(ManagementType.REAL, establishmentKey, new Chrono(PRECHRO,
                CHRONO), 9, 7, 5, 3, BATCH, itemCL, null, null, mvtDate, opItemQuantityUnit, false);

        bBean = new FBoningOutputBBean();
        stackingPlanInfoBean = new StackingPlanInfoBean();
        stackingPlanInfoBean.setStackingPlanCL(new CodeLabel(STACKING_PLAN_ID, "stpl_lbl"));
        stackingPlanInfoBean.setInner(INNER);
        stackingPlanInfoBean.setUnit(UNIT);
    }

    /**
     * Runs once before all tests.
     * 
     * @throws Exception if error occurs
     */
    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
    }

    /**
     * 
     * {@inheritDoc}
     */
    @Before
    public void setUp() throws Exception {
        context = JMockContext.getNewContext();

        /* initialize the sbs under test */
        sbsUT = new BoningMOSBSImpl();

        /* Configure DAO retrieving mock objects */
        kcrivDAO = DaoConfigurationJmockTest.addMockToDAO(KcrivDAO.class);
        kofDAO = DaoConfigurationJmockTest.addMockToDAO(KofDAO.class);
        kotDAO = DaoConfigurationJmockTest.addMockToDAO(KotDAO.class);
        kotadDAO = DaoConfigurationJmockTest.addMockToDAO(KotadDAO.class);

        specificationSBS = context.mock(SpecificationSBS.class);
        sbsUT.setSpecificationSBS(specificationSBS);

        unitSBS = context.mock(UnitSBS.class);
        sbsUT.setUnitSBS(unitSBS);

        poItemSBS = context.mock(POItemSBS.class);
        sbsUT.setPoItemSBS(poItemSBS);

        stackingPlanSBS = context.mock(StackingPlanSBS.class);
        sbsUT.setStackingPlanSBS(stackingPlanSBS);

        reloadDataTests();
    }

    @Test
    public void testGetBBean() {
        try {

            context.checking(new Expectations() {
                {
                    oneOf(unitSBS).formatQty(with(any(VIFContext.class)), with(new QuantityUnit(QTY_2, UNIT)));
                    will(returnValue(String.valueOf(QTY_2) + UNIT));

                    oneOf(unitSBS).formatQty(with(any(VIFContext.class)), with(new QuantityUnit(QTY_1, UNIT)));
                    will(returnValue(String.valueOf(QTY_1) + UNIT));

                    oneOf(poItemSBS).getPOItemQuantity2(with(any(VIFContext.class)), with(oik));
                    will(returnValue(new OperationItemQuantityUnit(QTY_4, QTY_5, QTY_6, UNIT)));
                    oneOf(unitSBS).formatQty(with(any(VIFContext.class)), with(new QuantityUnit(QTY_5, UNIT)));
                    will(returnValue(String.valueOf(QTY_5) + UNIT));

                    oneOf(stackingPlanSBS).getFirstStackingPlanInfo(with(any(VIFContext.class)), with(moKey),
                            with(STACKING_PLAN_ID), with(ITEM1_CODE), with(1.0));
                    will(returnValue(stackingPlanInfoBean));
                    oneOf(unitSBS).formatQtyUnit(with(any(VIFContext.class)), with(new QuantityUnit(INNER, UNIT)));
                    will(returnValue(String.valueOf(INNER) + UNIT));
                }
            });
            FBoningOutputBBean resultBBean = sbsUT.getBBean(getCtx(), inputOperationItemBean, bBean, HCHY_VALUE,
                    STACKING_PLAN_ID, null, null);
            Assert.assertNotNull(resultBBean);
            Assert.assertEquals(String.valueOf(INNER) + UNIT, resultBBean.getInnerStr());
            Assert.assertEquals(true, resultBBean.getHasStackingplan());
            Assert.assertEquals(stackingPlanInfoBean, resultBBean.getStackingPlanInfoBean());
            Assert.assertEquals(UNIT, resultBBean.getItemQtty2Unit());
            Assert.assertEquals(QTY_5, resultBBean.getItemQtty2Done());
            Assert.assertEquals(new QtyUnit(QTY_2, UNIT), resultBBean.getItemQttyDoneInRefUnitQU());
            Assert.assertEquals(QTY_2 + UNIT, resultBBean.getItemQttyDoneInRefUnit());
            Assert.assertEquals(new QtyUnit(QTY_1, UNIT), resultBBean.getItemQttyToDoInRefUnitQU());
            Assert.assertEquals(QTY_1 + UNIT, resultBBean.getItemQttyToDoInRefUnit());
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage()); // no exception excepted
        }
    }

    @Test
    public void testGetBBean_2() {
        try {

            context.checking(new Expectations() {
                {
                    oneOf(unitSBS).formatQty(with(any(VIFContext.class)), with(new QuantityUnit(QTY_2, UNIT)));
                    will(returnValue(String.valueOf(QTY_2) + UNIT));

                    oneOf(unitSBS).formatQty(with(any(VIFContext.class)), with(new QuantityUnit(QTY_1, UNIT)));
                    will(returnValue(String.valueOf(QTY_1) + UNIT));

                    oneOf(poItemSBS).getPOItemQuantity2(with(any(VIFContext.class)), with(oik));
                    will(returnValue(new OperationItemQuantityUnit(QTY_4, QTY_5, QTY_6, UNIT)));
                    oneOf(unitSBS).formatQty(with(any(VIFContext.class)), with(new QuantityUnit(QTY_5, UNIT)));
                    will(returnValue(String.valueOf(QTY_5) + UNIT));

                    oneOf(stackingPlanSBS).getFirstStackingPlanInfo(with(any(VIFContext.class)), with(moKey),
                            with(STACKING_PLAN_ID), with(ITEM1_CODE), with(1.0));
                    will(returnValue(null));
                    oneOf(stackingPlanSBS).getFirstStackingPlanInfo(with(any(VIFContext.class)), with(moKey),
                            with(STACKING_PLAN_ID), with(ITEM1_CODE), with(0.0));
                    will(returnValue(stackingPlanInfoBean));

                    oneOf(unitSBS).formatQtyUnit(with(any(VIFContext.class)), with(new QuantityUnit(INNER, UNIT)));
                    will(returnValue(String.valueOf(INNER) + UNIT));
                }
            });
            FBoningOutputBBean resultBBean = sbsUT.getBBean(getCtx(), inputOperationItemBean, bBean, HCHY_VALUE,
                    STACKING_PLAN_ID, null, null);
            Assert.assertNotNull(resultBBean);
            Assert.assertEquals(String.valueOf(INNER) + UNIT, resultBBean.getInnerStr());
            Assert.assertEquals(true, resultBBean.getHasStackingplan());
            Assert.assertEquals(stackingPlanInfoBean, resultBBean.getStackingPlanInfoBean());
            Assert.assertEquals(UNIT, resultBBean.getItemQtty2Unit());
            Assert.assertEquals(QTY_5, resultBBean.getItemQtty2Done());
            Assert.assertEquals(new QtyUnit(QTY_2, UNIT), resultBBean.getItemQttyDoneInRefUnitQU());
            Assert.assertEquals(QTY_2 + UNIT, resultBBean.getItemQttyDoneInRefUnit());
            Assert.assertEquals(new QtyUnit(QTY_1, UNIT), resultBBean.getItemQttyToDoInRefUnitQU());
            Assert.assertEquals(QTY_1 + UNIT, resultBBean.getItemQttyToDoInRefUnit());
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage()); // no exception excepted
        }
    }

    @Test
    public void testGetBBean_3() {
        try {

            context.checking(new Expectations() {
                {
                    oneOf(unitSBS).formatQty(with(any(VIFContext.class)), with(new QuantityUnit(QTY_2, UNIT)));
                    will(returnValue(String.valueOf(QTY_2) + UNIT));

                    oneOf(unitSBS).formatQty(with(any(VIFContext.class)), with(new QuantityUnit(QTY_1, UNIT)));
                    will(returnValue(String.valueOf(QTY_1) + UNIT));

                    oneOf(poItemSBS).getPOItemQuantity2(with(any(VIFContext.class)), with(oik));
                    will(returnValue(new OperationItemQuantityUnit(QTY_4, QTY_5, QTY_6, UNIT)));
                    oneOf(unitSBS).formatQty(with(any(VIFContext.class)), with(new QuantityUnit(QTY_5, UNIT)));
                    will(returnValue(String.valueOf(QTY_5) + UNIT));

                    oneOf(stackingPlanSBS).getFirstStackingPlanInfo(with(any(VIFContext.class)), with(moKey),
                            with(STACKING_PLAN_ID), with(ITEM1_CODE), with(1.0));
                    will(returnValue(null));
                    oneOf(stackingPlanSBS).getFirstStackingPlanInfo(with(any(VIFContext.class)), with(moKey),
                            with(STACKING_PLAN_ID), with(ITEM1_CODE), with(0.0));
                    will(returnValue(null));
                }
            });
            FBoningOutputBBean resultBBean = sbsUT.getBBean(getCtx(), inputOperationItemBean, bBean, HCHY_VALUE,
                    STACKING_PLAN_ID, null, null);
            Assert.assertNotNull(resultBBean);
            Assert.assertEquals("", resultBBean.getInnerStr());
            Assert.assertEquals(false, resultBBean.getHasStackingplan());
            Assert.assertEquals(new StackingPlanInfoBean(), resultBBean.getStackingPlanInfoBean());
            Assert.assertEquals(UNIT, resultBBean.getItemQtty2Unit());
            Assert.assertEquals(QTY_5, resultBBean.getItemQtty2Done());
            Assert.assertEquals(new QtyUnit(QTY_2, UNIT), resultBBean.getItemQttyDoneInRefUnitQU());
            Assert.assertEquals(QTY_2 + UNIT, resultBBean.getItemQttyDoneInRefUnit());
            Assert.assertEquals(new QtyUnit(QTY_1, UNIT), resultBBean.getItemQttyToDoInRefUnitQU());
            Assert.assertEquals(QTY_1 + UNIT, resultBBean.getItemQttyToDoInRefUnit());
        } catch (ServerBusinessException e) {
            Assert.fail(e.getMessage()); // no exception excepted
        }
    }

    @Test
    public void testGetMOCustomer() {

        Kcriv cri = new Kcriv();
        cri.setValcmin(CRITERIA_1_VALUE);

        try {

            context.checking(new Expectations() {
                {
                    oneOf(kcrivDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with("OF"), with(PRECHRO + "," + CHRONO), with("$CLIENT"));
                    will(returnValue(cri));
                }
            });

            String customer = sbsUT.getMOCustomer(getCtx(), moKey);
            Assert.assertNotNull(customer);
            Assert.assertEquals(CRITERIA_1_VALUE, customer);

            context.checking(new Expectations() {
                {
                    oneOf(kcrivDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with("OF"), with(PRECHRO + "," + CHRONO), with("$CLIENT"));
                    will(returnValue(null));
                }
            });

            String customer2 = sbsUT.getMOCustomer(getCtx(), moKey);
            Assert.assertEquals("", customer2);
        } catch (ServerBusinessException | DAOException e) {
            Assert.fail(e.getMessage()); // no exception excepted
        }
    }

    @Test
    public void testGetRealQtyFromPO() {

        Kof kof = new Kof();
        kof.setNi1artref(33);
        Kot kot = new Kot();
        kot.setQte1(QTY_1);
        kot.setCunite(UNIT);

        try {

            context.checking(new Expectations() {
                {

                    oneOf(kofDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(PRECHRO), with(CHRONO));
                    will(returnValue(kof));
                    oneOf(kotDAO).getByPK(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(PRECHRO), with(CHRONO), with(33));
                    will(returnValue(kot));
                }
            });

            QtyUnit result = sbsUT.getRealQtyFromPO(getCtx(), moKey);
            Assert.assertNotNull(result);
            Assert.assertEquals(QTY_1, result.getQty());
            Assert.assertEquals(UNIT, result.getUnit());
        } catch (ServerBusinessException | DAOException e) {
            Assert.fail(e.getMessage()); // no exception excepted
        }
    }

    @Test
    public void testListDoneQty() {

        Kotad kotad1 = new Kotad();
        kotad1.setTypacta("ENT");
        kotad1.setNi1(11);
        kotad1.setNi2(12);
        kotad1.setQte2(QTY_1);
        kotad1.setCu2(UNIT);

        Kotad kotad2 = new Kotad();
        kotad2.setTypacta("ART");
        kotad2.setNi1(21);
        kotad2.setNi2(22);
        kotad2.setQte2(QTY_2);
        kotad2.setCu2(UNIT);

        Kotad kotad3 = new Kotad();
        kotad3.setTypacta("SOR");
        kotad3.setNi1(31);
        kotad3.setNi2(32);
        kotad3.setQte2(QTY_3);
        kotad3.setCu2(UNIT);

        Kotad kotad4 = new Kotad();
        kotad4.setTypacta("ENT");
        kotad4.setNi1(41);
        kotad4.setNi2(42);
        kotad4.setQte2(QTY_4);
        kotad4.setCu2(UNIT);

        Kotad kotad5 = new Kotad();
        kotad5.setTypacta("ENT");
        kotad5.setNi1(11);
        kotad5.setNi2(12);
        kotad5.setQte2(QTY_5);
        kotad5.setCu2(UNIT);

        try {

            context.checking(new Expectations() {
                {

                    oneOf(kotadDAO).listByI3(with(any(AbstractDBContext.class)), with(COMPANY_CODE),
                            with(ESTABLISHEMENT_CODE), with(PRECHRO), with(CHRONO), with(aNull(Integer.class)),
                            with(aNull(Integer.class)), with(aNull(Integer.class)), with(true));
                    will(returnValue(Arrays.asList(kotad1, kotad2, kotad3, kotad4, kotad5)));
                }
            });

            Map<OperationItemKey, QtyUnit> result = sbsUT.listDoneQty(getCtx(), moKey, ActivityItemType.INPUT);
            Assert.assertNotNull(result);
            Assert.assertEquals(2, result.size());
            Assert.assertNotNull(result.get(new OperationItemKey(ManagementType.REAL, establishmentKey, new Chrono(
                    PRECHRO, CHRONO), 11, 0, 0, 12)));
            Assert.assertEquals(
                    QTY_1 + QTY_5,
                    result.get(
                            new OperationItemKey(ManagementType.REAL, establishmentKey, new Chrono(PRECHRO, CHRONO),
                                    11, 0, 0, 12)).getQty());
            Assert.assertNull(result.get(new OperationItemKey(ManagementType.REAL, establishmentKey, new Chrono(
                    PRECHRO, CHRONO), 31, 0, 0, 32)));
            Assert.assertNotNull(result.get(new OperationItemKey(ManagementType.REAL, establishmentKey, new Chrono(
                    PRECHRO, CHRONO), 41, 0, 0, 42)));
            Assert.assertEquals(
                    QTY_4,
                    result.get(
                            new OperationItemKey(ManagementType.REAL, establishmentKey, new Chrono(PRECHRO, CHRONO),
                                    41, 0, 0, 42)).getQty());

        } catch (ServerBusinessException | DAOException e) {
            Assert.fail(e.getMessage()); // no exception excepted
        }
    }

    @SuppressWarnings("static-access")
    @Test
    public void testGetMOSpecification_DataNull() {
        try {

            sbsUT.getLogger().setLevel(Level.INFO);

            cri = null;

            context.checking(new Expectations() {
                {
                    String key = new StringBuffer().append(moKey.getChrono().getPrechro()).append(",")
                            .append(moKey.getChrono().getChrono()).toString();

                    oneOf(kcrivDAO).getByPK(with(any(AbstractDBContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(PrechroId.PRODUCTION.getValue()), with(key), with("$CC"));
                    will(returnValue(cri));
                }
            });

            CodeLabel cl = sbsUT.getMOSpecification(getCtx(), moKey);

            Assert.assertNull(cl);

        } catch (ServerBusinessException e) {
            Assert.fail(); // we should not fall in exception in that case of execution
        } catch (DAOException e) {
            Assert.fail(); // we should not fall in exception in that case of execution
        }
    }

    @SuppressWarnings("static-access")
    @Test
    public void testGetMOSpecification_MissingData() {
        try {

            sbsUT.getLogger().setLevel(Level.INFO);

            cri.setValcmin("");

            context.checking(new Expectations() {
                {
                    String key = new StringBuffer().append(moKey.getChrono().getPrechro()).append(",")
                            .append(moKey.getChrono().getChrono()).toString();

                    oneOf(kcrivDAO).getByPK(with(any(AbstractDBContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(PrechroId.PRODUCTION.getValue()), with(key), with("$CC"));
                    will(returnValue(cri));
                }
            });

            CodeLabel cl = sbsUT.getMOSpecification(getCtx(), moKey);

            Assert.assertNull(cl);

        } catch (ServerBusinessException e) {
            Assert.fail(); // we should not fall in exception in that case of execution
        } catch (DAOException e) {
            Assert.fail(); // we should not fall in exception in that case of execution
        }
    }

    @SuppressWarnings("static-access")
    @Test
    public void testGetMOSpecification_NominalCase() {
        try {

            sbsUT.getLogger().setLevel(Level.DEBUG);

            context.checking(new Expectations() {
                {
                    String key = new StringBuffer().append(moKey.getChrono().getPrechro()).append(",")
                            .append(moKey.getChrono().getChrono()).toString();

                    oneOf(kcrivDAO).getByPK(with(any(AbstractDBContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(PrechroId.PRODUCTION.getValue()), with(key), with("$CC"));
                    will(returnValue(cri));
                }
            });

            context.checking(new Expectations() {
                {
                    String retValue = "Test";
                    oneOf(specificationSBS).getSpecificationCL(with(any(VIFContext.class)),
                            with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                            with(cri.getValcmin()));
                    will(returnValue(retValue));
                }
            });

            CodeLabel cl = sbsUT.getMOSpecification(getCtx(), moKey);

            Assert.assertNotNull(cl);

        } catch (ServerBusinessException e) {
            Assert.fail(); // we should not fall in exception in that case of execution
        } catch (DAOException e) {
            Assert.fail(); // we should not fall in exception in that case of execution
        }
    }

    @SuppressWarnings("static-access")
    @Test(expected = ServerBusinessException.class)
    public void testGetMOSpecification_ThrowsServerBusinessEx() throws Exception {

        sbsUT.getLogger().setLevel(Level.DEBUG);
        final DAOException e = new DAOException("test");
        context.checking(new Expectations() {
            {
                String key = new StringBuffer().append(moKey.getChrono().getPrechro()).append(",")
                        .append(moKey.getChrono().getChrono()).toString();

                oneOf(kcrivDAO).getByPK(with(any(AbstractDBContext.class)),
                        with(moKey.getEstablishmentKey().getCsoc()), with(moKey.getEstablishmentKey().getCetab()),
                        with(PrechroId.PRODUCTION.getValue()), with(key), with("$CC"));
                will(throwException(e));
            }
        });

        sbsUT.getMOSpecification(getCtx(), moKey);

    }

    /**
     * Get the VIFContext.
     * 
     * @return
     */
    private VIFContext getCtx() {
        return VIFContextMockManager.getVIFContext();
    }
}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPOTtLocaldocprg.java,v $
 * Created on 24 mars 2016 by presta15
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:tempxxfabpxy.p.xml.
 * 
 * @author presta15
 */
public class XxfabPPOTtLocaldocprg extends AbstractTempTable implements Serializable {


    private String cdoc = "";

    private String event = "";

    private int nbdoc = 0;

    private String prgedi = "";


    /**
     * Default Constructor.
     *
     */
    public XxfabPPOTtLocaldocprg() {
        super();
    }

    /**
     * Gets the cdoc.
     *
     * @category getter
     * @return the cdoc.
     */
    public final String getCdoc() {
        return cdoc;
    }

    /**
     * Sets the cdoc.
     *
     * @category setter
     * @param cdoc cdoc.
     */
    public final void setCdoc(final String cdoc) {
        this.cdoc = cdoc;
    }

    /**
     * Gets the event.
     *
     * @category getter
     * @return the event.
     */
    public final String getEvent() {
        return event;
    }

    /**
     * Sets the event.
     *
     * @category setter
     * @param event event.
     */
    public final void setEvent(final String event) {
        this.event = event;
    }

    /**
     * Gets the nbdoc.
     *
     * @category getter
     * @return the nbdoc.
     */
    public final int getNbdoc() {
        return nbdoc;
    }

    /**
     * Sets the nbdoc.
     *
     * @category setter
     * @param nbdoc nbdoc.
     */
    public final void setNbdoc(final int nbdoc) {
        this.nbdoc = nbdoc;
    }

    /**
     * Gets the prgedi.
     *
     * @category getter
     * @return the prgedi.
     */
    public final String getPrgedi() {
        return prgedi;
    }

    /**
     * Sets the prgedi.
     *
     * @category setter
     * @param prgedi prgedi.
     */
    public final void setPrgedi(final String prgedi) {
        this.prgedi = prgedi;
    }

}

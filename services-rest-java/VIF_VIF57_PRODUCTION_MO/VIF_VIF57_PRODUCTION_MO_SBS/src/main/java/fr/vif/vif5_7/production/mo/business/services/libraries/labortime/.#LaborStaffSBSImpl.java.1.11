/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: LaborStaffSBSImpl.java,v $
 * Created on 12 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.labortime;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.activities.activities.business.beans.common.resourcecategory.ResourceCategory;
import fr.vif.vif5_7.activities.activities.business.beans.common.resourcecategory.ResourceCategoryKey;
import fr.vif.vif5_7.activities.activities.business.services.libraries.resourcecategory.ResourceCategorySBS;
import fr.vif.vif5_7.activities.activities.business.services.libraries.resources.ProductionResourcesSBS;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.hierarchy.business.services.libraries.hierarchy.HierarchySBS;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborStaff;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborStaffKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.dao.kotod.Kotod;
import fr.vif.vif5_7.production.mo.dao.kotod.KotodDAO;
import fr.vif.vif5_7.production.mo.dao.kotod.KotodDAOFactory;
import fr.vif.vif5_7.production.mo.dao.ktpsx.Ktpsx;
import fr.vif.vif5_7.production.mo.dao.ktpsx.KtpsxDAO;
import fr.vif.vif5_7.production.mo.dao.ktpsx.KtpsxDAOFactory;
import fr.vif.vif5_7.production.mo.dao.ktpsxd.Ktpsxd;
import fr.vif.vif5_7.production.mo.dao.ktpsxd.KtpsxdDAO;
import fr.vif.vif5_7.production.mo.dao.ktpsxd.KtpsxdDAOFactory;


/**
 * the Labor Staff SBS.
 * 
 * @author nle
 */
@Component
public class LaborStaffSBSImpl implements LaborStaffSBS {

    protected static final Logger  LOGGER = Logger.getLogger(LaborStaffSBSImpl.class);

    @Autowired
    private HierarchySBS           hierarchySBS;

    @Autowired
    private ResourceCategorySBS    resourceCategorySBS;

    @Autowired
    private ProductionResourcesSBS productionResourcesSBS;

    public Chrono breakdownTime(final VIFContext ctx, final EstablishmentKey establishmentKey, final Date proddate,
            final String resourceId, final List<String> laborId, final String creason) throws ServerBusinessException {

        String laborIdList = "";
        StringHolder prechro = new StringHolder();
        IntegerHolder chrono = new IntegerHolder();
        StringHolder msg = new StringHolder();
        BooleanHolder ret = new BooleanHolder();
        for (String labor : laborId) {
            if (laborIdList.length() > 0) {
                laborIdList += ",";
            }
            laborIdList += labor;
        }
        new VentiltpsPPO(ctx).ventilerMoJour(establishmentKey.getCsoc(), establishmentKey.getCetab(), proddate,
                resourceId, laborIdList, creason, ctx.getIdCtx().getLogin(), prechro, chrono, ret, msg);

        if (!ret.getBooleanValue()) {
            throw new ServerBusinessException(ProductionMo.T4700, new VarParamTranslation(msg.getValue()));
        }

        return new Chrono(prechro.getStringValue(), chrono.getIntegerValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteLaborStaff(final VIFContext ctx, final LaborStaff laborStaff) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteLaborStaff(" + laborStaff + ")");
        }
        LaborStaffKey laborStaffKey = laborStaff.getLaborStaffKey();
        if (!isValid(laborStaff.getLaborStaffKey())) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborStaff.toString()));
        }
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("P - Try to delete laborStaff for key " + laborStaffKey);
            }
            LaborTimeKey laborTimeKey = new LaborTimeKey(laborStaffKey);
            Ktpsxd ktpsxd = KtpsxdDAOFactory.getDAO().getByPKForUpdate(ctx.getDbCtx(),
                    laborTimeKey.getEstKey().getCsoc(), laborTimeKey.getEstKey().getCetab(),
                    laborTimeKey.getProductionDate(), laborTimeKey.getResourceCode(), laborTimeKey.getNi1(),
                    laborStaffKey.getStaffResourceCode(), laborStaff.getAdmstamp());
            KtpsxdDAOFactory.getDAO().delete(ctx.getDbCtx(), convertLaborStaffToKtpsxd(laborStaff, ktpsxd));
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("Unable to delete the labor staff corresponding to the key " + laborStaffKey, e);
            }
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteLaborStaff(" + laborStaffKey + ")");
        }
    }

    /**
     * Gets the hierarchySBS.
     * 
     * @return the hierarchySBS.
     */
    public HierarchySBS getHierarchySBS() {
        return hierarchySBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LaborStaff getLaborStaff(final VIFContext ctx, final LaborStaffKey laborStaffKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLaborStaff(ctx=" + ctx + ", laborStaffKey=" + laborStaffKey + ")");
        }
        LaborStaff laborStaff = null;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLaborStaff(" + laborStaffKey + ")");
        }
        if (!isValid(laborStaffKey)) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborStaffKey.toString()));
        }
        try {
            LaborTimeKey laborTimeKey = new LaborTimeKey(laborStaffKey);
            laborStaff = convertKtpsxdToLaborStaff(
                    ctx,
                    KtpsxdDAOFactory.getDAO()
                    .getByPK(ctx.getDbCtx(), laborTimeKey.getEstKey().getCsoc(),
                            laborTimeKey.getEstKey().getCetab(), laborTimeKey.getProductionDate(),
                            laborTimeKey.getResourceCode(), laborTimeKey.getNi1(),
                            laborStaffKey.getStaffResourceCode()));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getLaborStaff(" + laborStaffKey + ") : " + laborStaff);
        }
        return laborStaff;
    }

    /**
     * Gets the productionResourcesSBS.
     * 
     * @return the productionResourcesSBS.
     */
    public ProductionResourcesSBS getProductionResourcesSBS() {
        return productionResourcesSBS;
    }

    /**
     * Gets the resourceCategorySBS.
     * 
     * @return the resourceCategorySBS.
     */
    public ResourceCategorySBS getResourceCategorySBS() {
        return resourceCategorySBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LaborStaff insertLaborStaff(final VIFContext ctx, final LaborStaff laborStaff)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertLaborStaff(ctx=" + ctx + ", laborStaff=" + laborStaff + ")");
        }
        LaborStaff laborStaffAfterInsert = null;
        LaborStaffKey laborStaffKey = new LaborStaffKey(laborStaff);
        if (!isValid(laborStaffKey)) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborStaff.toString()));
        }
        try {
            LaborTimeKey laborTimeKey = new LaborTimeKey(laborStaffKey);
            // insert
            KtpsxdDAOFactory.getDAO().insert(ctx.getDbCtx(), convertLaborStaffToKtpsxd(laborStaff, new Ktpsxd()));
            laborStaffAfterInsert = convertKtpsxdToLaborStaff(
                    ctx,
                    KtpsxdDAOFactory.getDAO()
                    .getByPK(ctx.getDbCtx(), laborTimeKey.getEstKey().getCsoc(),
                            laborTimeKey.getEstKey().getCetab(), laborTimeKey.getProductionDate(),
                            laborTimeKey.getResourceCode(), laborTimeKey.getNi1(),
                            laborStaffKey.getStaffResourceCode()));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertLaborStaff(" + laborStaff + ") : " + laborStaffAfterInsert);
        }
        return laborStaffAfterInsert;
    }

    @Override
    public List<LaborStaff> listLaborStaff(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final Date productionDate, final String resourceId, final String laborId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listLaborStaff(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", productionDate=" + productionDate + ", resourceId=" + resourceId + ", laborId=" + laborId
                    + ")");
        }

        List<LaborStaff> laborStaffList = new ArrayList<LaborStaff>();
        List<Ktpsxd> plcList;
        try {
            plcList = KtpsxdDAOFactory.getDAO().listByI2(ctx.getDbCtx(), establishmentKey.getCsoc(),
                    establishmentKey.getCetab(), productionDate, resourceId, laborId, true);
            laborStaffList = convertKtpsxdListToLaborStaffList(ctx, plcList);
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listLaborStaff(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", productionDate=" + productionDate + ", resourceId=" + resourceId + ", laborId=" + laborId
                    + ")=" + laborStaffList);
        }
        return laborStaffList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LaborStaff> listLaborStaff(final VIFContext ctx, final LaborStaffKey laborStaffKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listLaborStaff(" + laborStaffKey + ")");
        }
        // not !isValid because the cresmo must be null for a list request
        LaborTimeKey laborTimeKey = new LaborTimeKey(laborStaffKey);
        if (laborStaffKey == null || laborTimeKey.getEstKey().getCsoc() == null
                || laborTimeKey.getEstKey().getCetab() == null || laborTimeKey.getProductionDate() == null
                || laborTimeKey.getResourceCode() == null || laborStaffKey.getLaborTimeKey().getNi1() == 0) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborStaffKey.toString()));
        }
        List<LaborStaff> list = null;
        try {
            KtpsxdDAO ktpsxdDAO = KtpsxdDAOFactory.getDAO();
            list = convertKtpsxdListToLaborStaffList(ctx, ktpsxdDAO.listByPK(ctx.getDbCtx(), laborTimeKey.getEstKey()
                    .getCsoc(), laborTimeKey.getEstKey().getCetab(), laborTimeKey.getProductionDate(), laborTimeKey
                    .getResourceCode(), laborTimeKey.getNi1(), null, // cresmo
                    // must be
                    // null to
                    // have a
                    // list and
                    // not a
                    // singleton
                    true));
            if (list != null) {
                // searches and feeds the lres
                for (LaborStaff laborStaff : list) {
                    CodeLabel resLbl = getProductionResourcesSBS().getResourceCL(ctx,
                            laborStaff.getLaborStaffKey().getLaborTimeKey().getEstKey().getCsoc(),
                            laborStaff.getLaborStaffKey().getLaborTimeKey().getEstKey().getCetab(),
                            laborStaff.getLaborStaffKey().getStaffResourceCode());
                    if (resLbl != null) {
                        laborStaff.setResourceLabel(resLbl.getLabel());
                    }
                }
            }
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listLaborStaff(" + laborStaffKey + ") : " + list);
        }
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LaborStaff> listLaborStaffWithLaborTimeBean(final VIFContext ctx, final FLaborTimeBBean ltBBean)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listLaborStaffWithLaborTimeBean(" + ltBBean + ")");
        }
        List<LaborStaff> listLaborStaff = new ArrayList<LaborStaff>();
        KtpsxdDAO ktpsxdDAO;
        try {
            ktpsxdDAO = KtpsxdDAOFactory.getDAO();
            // builds a key without the ni1
            LaborStaffKey laborStaffKey = new LaborStaffKey();
            laborStaffKey.setLaborTimeKey(ltBBean.getLaborTimeKey());
            laborStaffKey.setStaffResourceCode(null);
            // gets a list of all Ktpsxd from csoc, cetab, datprod, cres, ni1
            List<Ktpsxd> listKtpsxd = ktpsxdDAO.list(ctx.getDbCtx(), laborStaffKey, true);
            for (Ktpsxd ktpsxd : listKtpsxd) {
                LaborStaff ls = convertKtpsxdToLaborStaff(ctx, ktpsxd);
                listLaborStaff.add(ls);
            }
        } catch (DAOException e) {
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listLaborStaffWithLaborTimeBean(" + ltBBean + ") : " + listLaborStaff);
        }
        return listLaborStaff;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LaborStaff> listLaborStaffWithListMO(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String prechro, final List<Integer> listChrono, final Date datprod, final String ligne)
                    throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listLaborStaffWithLstMO(" + listChrono + ")");
        }
        List<LaborStaff> listLaborStaff = null;
        try {
            KotodDAO kotodDAO = KotodDAOFactory.getDAO();
            List<Kotod> listKotod = new ArrayList<Kotod>();
            listKotod = kotodDAO.listKotodWithListChrono(ctx.getDbCtx(), establishmentKey, prechro, listChrono, false,
                    false);
            if (listKotod != null) {
                // keep only the resources that are "M/O" and NOT part of the "MONITOR" hierarchy
                Iterator<Kotod> itr = listKotod.iterator();
                while (itr.hasNext()) {
                    Kotod kotod = itr.next();

                    // The kotod has a crub. From this crub we find the krub record and we only keep the cres of which
                    // krub.cnatres = "M/O"
                    ResourceCategory resCat = getResourceCategorySBS().getResourceCategory(ctx,
                            new ResourceCategoryKey(establishmentKey.getCsoc(), kotod.getCrub()));
                    if (!"M/O".equals(resCat.getCnatRes())) {
                        itr.remove();
                    } else {
                        // // The kotod.cres must NOT be part of the MONITOR resource hierarchy
                        // HchyKey hchyKey = new HchyKey(establishmentKey.getCsoc(), "RES", "MONITOR");
                        // if (getHierarchySBS().getFirstLevelValue(ctx, hchyKey, kotod.getCres()) != null) {
                        // itr.remove();
                        // }
                    }
                }
                // converts the cresmo and squeezes the duplicates
                listLaborStaff = convertKotodListToLaborStaffList(ctx, listKotod, datprod, ligne);
                // searches and feeds the lres
                for (LaborStaff laborStaff : listLaborStaff) {
                    CodeLabel resCL = getProductionResourcesSBS().getResourceCL(ctx,
                            laborStaff.getLaborStaffKey().getLaborTimeKey().getEstKey().getCsoc(),
                            laborStaff.getLaborStaffKey().getLaborTimeKey().getEstKey().getCetab(),
                            laborStaff.getLaborStaffKey().getStaffResourceCode());
                    if (resCL != null) {
                        laborStaff.setResourceLabel(resCL.getLabel());
                    }
                }
                // overwrites the nbr with the ones of KTPSXD (the last one for each cresmo)
                KtpsxdDAO ktpsxdDAO = KtpsxdDAOFactory.getDAO();
                KtpsxDAO ktpsxDAO = KtpsxDAOFactory.getDAO();
                for (LaborStaff laborStaff : listLaborStaff) {
                    // builds a key without the ni1
                    LaborStaffKey laborStaffKey = new LaborStaffKey();
                    laborStaffKey.setLaborTimeKey(laborStaff.getLaborStaffKey().getLaborTimeKey());
                    laborStaffKey.getLaborTimeKey().setResourceCode(
                            laborStaff.getLaborStaffKey().getLaborTimeKey().getResourceCode());
                    laborStaffKey.setStaffResourceCode(laborStaff.getLaborStaffKey().getStaffResourceCode());
                    laborStaffKey.getLaborTimeKey().setNi1(0); // in order not to be in the WHERE clause

                    // gets a list of all Ktpsxd from csoc, cetab, datprod, cres, cresmo BY ni1
                    List<Ktpsxd> listKtpsxd = ktpsxdDAO.list(ctx.getDbCtx(), laborStaffKey, true);
                    int ni1 = 0;

                    // being sorted by ni1 ASC, if the same cresmo exists with a superior ni1, overwrites the inferior
                    // one !
                    // we take the nbr of the last occurrence, unless the end date-hour is set : in this case, the
                    // number of people is zero
                    for (Ktpsxd ktpsxd : listKtpsxd) {
                        if (ktpsxd.getNi1() > ni1) {
                            laborStaff.setStaff(ktpsxd.getNbr());
                            laborStaff.getLaborStaffKey().getLaborTimeKey().setNi1(ktpsxd.getNi1());
                            ni1 = ktpsxd.getNi1();
                        }
                    }
                    if (ni1 != 0) {
                        Ktpsx ktpsx = ktpsxDAO.getByPK(ctx.getDbCtx(), establishmentKey.getCsoc(),
                                establishmentKey.getCetab(), datprod, ligne, ni1);
                        if (ktpsx.getDatheurfinr() != null) {
                            laborStaff.setStaff(0);
                        }
                    }
                }
            }
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listLaborStaffWithLstMO(" + listChrono + ") : " + listChrono);
        }
        return listLaborStaff;
    }

    public Chrono periodWithoutMO(final VIFContext ctx, final EstablishmentKey establishmentKey, final Date proddate)
            throws ServerBusinessException {

        StringHolder prechro = new StringHolder();
        IntegerHolder chrono = new IntegerHolder();
        StringHolder msg = new StringHolder();
        BooleanHolder ret = new BooleanHolder();
        new VentiltpsPPO(ctx).periodesSansOf(establishmentKey.getCsoc(), establishmentKey.getCetab(), proddate,
                prechro, chrono, ret, msg);

        if (!ret.getBooleanValue()) {
            throw new ServerBusinessException(Jtech.T24594, new VarParamTranslation(msg.getValue()));
        }

        return new Chrono(prechro.getStringValue(), chrono.getIntegerValue());
    }

    /**
     * Sets the hierarchySBS.
     * 
     * @param hierarchySBS hierarchySBS.
     */
    public void setHierarchySBS(final HierarchySBS hierarchySBS) {
        this.hierarchySBS = hierarchySBS;
    }

    /**
     * Sets the productionResourcesSBS.
     * 
     * @param productionResourcesSBS productionResourcesSBS.
     */
    public void setProductionResourcesSBS(final ProductionResourcesSBS productionResourcesSBS) {
        this.productionResourcesSBS = productionResourcesSBS;
    }

    /**
     * Sets the resourceCategorySBS.
     * 
     * @param resourceCategorySBS resourceCategorySBS.
     */
    public void setResourceCategorySBS(final ResourceCategorySBS resourceCategorySBS) {
        this.resourceCategorySBS = resourceCategorySBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateLaborStaff(final VIFContext ctx, final LaborStaff laborStaff) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateLaborStaff(ctx=" + ctx + ", laborStaff=" + laborStaff + ")");
        }
        LaborStaffKey laborStaffKey = new LaborStaffKey(laborStaff);
        if (!isValid(laborStaffKey)) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborStaff.toString()));
        }
        try {
            LaborTimeKey laborTimeKey = new LaborTimeKey(laborStaffKey);
            Ktpsxd ktpsxd = KtpsxdDAOFactory.getDAO().getByPKForUpdate(ctx.getDbCtx(),
                    laborTimeKey.getEstKey().getCsoc(), laborTimeKey.getEstKey().getCetab(),
                    laborTimeKey.getProductionDate(), laborTimeKey.getResourceCode(), laborTimeKey.getNi1(),
                    laborStaffKey.getStaffResourceCode(), laborStaff.getAdmstamp());
            KtpsxdDAOFactory.getDAO().update(ctx.getDbCtx(), convertLaborStaffToKtpsxd(laborStaff, ktpsxd));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateLaborStaff(" + laborStaff + ") : " + laborStaff);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int updateLaborStaffNi1(final VIFContext ctx, final LaborStaff laborStaff, final int newNi1)
            throws ServerBusinessException {
        int nb = 0;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateLaborStaffNi1(" + laborStaff.getLaborStaffKey() + ")");
        }
        if (!isValid(laborStaff.getLaborStaffKey())) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborStaff.getLaborStaffKey()
                    .toString()));
        }
        try {
            // no update because ni1 is part of the primary key : insert new one and delete old one
            LaborStaff laborStaffIns = new LaborStaff();
            laborStaffIns.setLaborStaffKey(laborStaff.getLaborStaffKey());
            laborStaffIns.setResourceLabel(laborStaff.getResourceLabel());
            laborStaffIns.setStaff(laborStaff.getStaff());
            laborStaffIns.setAdmstamp(laborStaff.getAdmstamp());
            LaborStaff laborStaffDel = new LaborStaff();
            LaborStaffKey lsKey = new LaborStaffKey();
            lsKey.getLaborTimeKey().setEstKey(laborStaff.getLaborStaffKey().getLaborTimeKey().getEstKey());
            lsKey.getLaborTimeKey().setProductionDate(
                    laborStaff.getLaborStaffKey().getLaborTimeKey().getProductionDate());
            lsKey.getLaborTimeKey().setResourceCode(laborStaff.getLaborStaffKey().getLaborTimeKey().getResourceCode());
            lsKey.getLaborTimeKey().setNi1(laborStaff.getLaborStaffKey().getLaborTimeKey().getNi1());
            lsKey.setStaffResourceCode(laborStaff.getLaborStaffKey().getStaffResourceCode());
            laborStaffDel.setLaborStaffKey(lsKey);
            laborStaffDel.setAdmstamp(laborStaff.getAdmstamp());
            laborStaffIns.getLaborStaffKey().getLaborTimeKey().setNi1(newNi1);
            // insert new one
            KtpsxdDAOFactory.getDAO().insert(ctx.getDbCtx(), convertLaborStaffToKtpsxd(laborStaffIns, new Ktpsxd()));
            Ktpsxd ktpsxd = KtpsxdDAOFactory.getDAO().getByPKForUpdate(ctx.getDbCtx(),
                    laborStaffDel.getLaborStaffKey().getLaborTimeKey().getEstKey().getCsoc(),
                    laborStaffDel.getLaborStaffKey().getLaborTimeKey().getEstKey().getCetab(),
                    laborStaffDel.getLaborStaffKey().getLaborTimeKey().getProductionDate(),
                    laborStaffDel.getLaborStaffKey().getLaborTimeKey().getResourceCode(),
                    laborStaffDel.getLaborStaffKey().getLaborTimeKey().getNi1(),
                    laborStaffDel.getLaborStaffKey().getStaffResourceCode(), laborStaffDel.getAdmstamp());
            // delete old one
            KtpsxdDAOFactory.getDAO().delete(ctx.getDbCtx(), convertLaborStaffToKtpsxd(laborStaffDel, ktpsxd));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateLaborStaffNi1(" + laborStaff.getLaborStaffKey() + ")");
        }
        return nb;
    }

    /**
     * Convert a list of Kotodd plc into a list of LaborStaff bean.
     * 
     * @param ctx the context
     * @param kotodList list of plc to be converted
     * @param datprod the production date
     * @param ligne the production line
     * @return the corresponding list of LaborStaff
     */
    private List<LaborStaff> convertKotodListToLaborStaffList(final VIFContext ctx, final List<Kotod> kotodList,
            final Date datprod, final String ligne) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertKotodListToLaborStaffList");
        }
        List<LaborStaff> laborStaffList = new ArrayList<LaborStaff>(kotodList.size());
        // removes duplicate cres
        for (Kotod kotod : kotodList) {
            boolean alreadyThere = false;
            for (LaborStaff ls : laborStaffList) {
                if (ls.getLaborStaffKey().getStaffResourceCode().equals(kotod.getCres())) {
                    // the cresmo of ktpsxd
                    alreadyThere = true;
                    break;
                }
            }
            if (!alreadyThere) {
                LaborStaff laborStaff = convertKotodToLaborStaff(ctx, kotod, datprod, ligne);
                laborStaffList.add(laborStaff);
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertKotodListToLaborStaffList(ctx=" + ctx + ", kotodList=" + kotodList + ", datprod="
                    + datprod + ", ligne=" + ligne + ")=" + laborStaffList);
        }
        return laborStaffList;
    }

    /**
     * Convert the plc Kotodd bean into the LaborStaff bean.
     * 
     * @param ctx the context
     * @param kotod plc to be converted
     * @param datprod the production date
     * @param ligne the production line
     * @return the LaborStaff bean that corresponds to the kotod
     */
    private LaborStaff convertKotodToLaborStaff(final VIFContext ctx, final Kotod kotod, final Date datprod,
            final String ligne) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertKotodToLaborStaff");
        }
        LaborStaff laborStaff = null;
        if (kotod != null) {
            laborStaff = new LaborStaff();
            LaborTimeKey laborTimeKey = new LaborTimeKey(kotod.getCsoc(), kotod.getCetab(), datprod, ligne, 0);
            LaborStaffKey laborStaffKey = new LaborStaffKey(laborTimeKey, kotod.getCres());
            laborStaff.setLaborStaffKey(laborStaffKey);
            laborStaff.setStaff(kotod.getNb());
            // forecast number, not effective number
            laborStaff.setAdmstamp(kotod.getAdmstamp());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertKotodToLaborStaff(ctx=" + ctx + ", kotod=" + kotod + ", datprod=" + datprod
                    + ", ligne=" + ligne + ")=" + laborStaff);
        }
        return laborStaff;
    }

    /**
     * Convert a list of Ktpsxd plc into a list of LaborStaff bean.
     * 
     * @param ctx the context
     * @param ktpsxdList list of plc to be converted
     * @return the corresponding list of LaborStaff
     */
    private List<LaborStaff> convertKtpsxdListToLaborStaffList(final VIFContext ctx, final List<Ktpsxd> ktpsxdList)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertKtpsxdListToLaborStaffList");
        }
        List<LaborStaff> laborStaffList = new ArrayList<LaborStaff>(ktpsxdList.size());
        for (Iterator<Ktpsxd> iterator = ktpsxdList.iterator(); iterator.hasNext();) {
            Ktpsxd ktpsxd = iterator.next();
            laborStaffList.add(convertKtpsxdToLaborStaff(ctx, ktpsxd));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertKtpsxdListToLaborStaffList(ktpsxdList=" + ktpsxdList + ")=" + laborStaffList);
        }
        return laborStaffList;
    }

    /**
     * Convert the plc Ktpsxd bean into the LaborStaff bean.
     * 
     * @param ctx the context
     * @param ktpsxd plc to be converted
     * @return the LaborStaff bean that corresponds to the Ktpsxd
     */
    private LaborStaff convertKtpsxdToLaborStaff(final VIFContext ctx, final Ktpsxd ktpsxd)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertKtpsxdToLaborStaff");
        }
        LaborStaff laborStaff = null;
        if (ktpsxd != null) {
            laborStaff = new LaborStaff();
            LaborTimeKey laborTimeKey = new LaborTimeKey(ktpsxd.getCsoc(), ktpsxd.getCetab(), ktpsxd.getDatprod(),
                    ktpsxd.getCres(), ktpsxd.getNi1());
            LaborStaffKey laborStaffKey = new LaborStaffKey(laborTimeKey, ktpsxd.getCresmo());
            laborStaff.setLaborStaffKey(laborStaffKey);
            laborStaff.setStaff(ktpsxd.getNbr());
            laborStaff.setAdmstamp(ktpsxd.getAdmstamp());
            // searches and feeds the lres
            CodeLabel resCL = getProductionResourcesSBS().getResourceCL(ctx, laborTimeKey.getEstKey().getCsoc(),
                    laborTimeKey.getEstKey().getCetab(), laborStaffKey.getStaffResourceCode());
            if (resCL != null) {
                laborStaff.setResourceLabel(resCL.getLabel());
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertKtpsxdToLaborStaff(ktpsxd=" + ktpsxd + ")=" + laborStaff);
        }
        return laborStaff;
    }

    /**
     * Convert a LaborStaff bean to a Ktpsxd PLC.
     * 
     * @param laborStaff the LaborStaff
     * @param ktpsxd Ktpsxd PLC
     * @return Ktpsxd PLC
     */
    private Ktpsxd convertLaborStaffToKtpsxd(final LaborStaff laborStaff, final Ktpsxd ktpsxd) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertLaborStaffToKtpsxd");
        }
        Ktpsxd ktpsxdConverted = null;
        if (ktpsxd == null) {
            ktpsxdConverted = new Ktpsxd();
        } else {
            ktpsxdConverted = ktpsxd;
        }
        LaborStaffKey laborStaffKey = new LaborStaffKey(laborStaff);
        LaborTimeKey laborTimeKey = new LaborTimeKey(laborStaffKey);
        ktpsxdConverted.setCsoc(laborTimeKey.getEstKey().getCsoc());
        ktpsxdConverted.setCetab(laborTimeKey.getEstKey().getCetab());
        ktpsxdConverted.setDatprod(laborTimeKey.getProductionDate());
        ktpsxdConverted.setCres(laborTimeKey.getResourceCode());
        ktpsxdConverted.setNi1(laborTimeKey.getNi1());
        ktpsxdConverted.setCresmo(laborStaffKey.getStaffResourceCode());
        ktpsxdConverted.setNbr(laborStaff.getStaff());
        ktpsxdConverted.setAdmstamp(laborStaff.getAdmstamp());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertLaborStaffToKtpsxd(laborStaff=" + laborStaff + ", ktpsxd=" + ktpsxd + ")="
                    + ktpsxdConverted);
        }
        return ktpsxdConverted;
    }

    /**
     * Is a LaborStaff key valid ?
     * 
     * @param laborStaffKey Labor Staff key
     * @return true if valid, false otherwise
     */
    private boolean isValid(final LaborStaffKey laborStaffKey) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isValid(laborStaffKey=" + laborStaffKey + ")");
        }
        boolean isValid = true;
        LaborTimeKey laborTimeKey = new LaborTimeKey(laborStaffKey);
        if (laborStaffKey == null || laborTimeKey.getEstKey().getCsoc() == null
                || laborTimeKey.getEstKey().getCetab() == null || laborTimeKey.getProductionDate() == null
                || laborTimeKey.getResourceCode() == null || laborTimeKey.getNi1() == 0) {
            isValid = false;
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isValid(laborStaffKey=" + laborStaffKey + ")=" + isValid);
        }
        return isValid;
    }
}

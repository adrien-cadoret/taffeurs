/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabrecPPOTtLocaldocprg.java,v $
 * Created on 16 mai 2014 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.boning;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/xxfabrecpxy.p.xml.
 * 
 * @author cj
 */
public class XxfabrecPPOTtLocaldocprg extends AbstractTempTable implements Serializable {

    private String cdoc   = "";

    private String event  = "";

    private int    nbdoc  = 0;

    private String prgedi = "";

    /**
     * Default Constructor.
     * 
     */
    public XxfabrecPPOTtLocaldocprg() {
        super();
    }

    /**
     * Gets the cdoc.
     * 
     * @category getter
     * @return the cdoc.
     */
    public final String getCdoc() {
        return cdoc;
    }

    /**
     * Gets the event.
     * 
     * @category getter
     * @return the event.
     */
    public final String getEvent() {
        return event;
    }

    /**
     * Gets the nbdoc.
     * 
     * @category getter
     * @return the nbdoc.
     */
    public final int getNbdoc() {
        return nbdoc;
    }

    /**
     * Gets the prgedi.
     * 
     * @category getter
     * @return the prgedi.
     */
    public final String getPrgedi() {
        return prgedi;
    }

    /**
     * Sets the cdoc.
     * 
     * @category setter
     * @param cdoc cdoc.
     */
    public final void setCdoc(final String cdoc) {
        this.cdoc = cdoc;
    }

    /**
     * Sets the event.
     * 
     * @category setter
     * @param event event.
     */
    public final void setEvent(final String event) {
        this.event = event;
    }

    /**
     * Sets the nbdoc.
     * 
     * @category setter
     * @param nbdoc nbdoc.
     */
    public final void setNbdoc(final int nbdoc) {
        this.nbdoc = nbdoc;
    }

    /**
     * Sets the prgedi.
     * 
     * @category setter
     * @param prgedi prgedi.
     */
    public final void setPrgedi(final String prgedi) {
        this.prgedi = prgedi;
    }

}

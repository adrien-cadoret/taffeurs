/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: FLaborTimeValidatorImpl.java,v $
 * Created on 12 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.labortime;


import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.TranslatedException;
import fr.vif.jtech.business.util.validator.AbstractVIFValidator;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.dao.ktpsx.Ktpsx;
import fr.vif.vif5_7.production.mo.dao.ktpsx.KtpsxDAOFactory;


/**
 * Labor Time validator SBS.
 * 
 * @author nle
 */
@Component
public class FLaborTimeValidatorImpl extends AbstractVIFValidator<FLaborTimeBBean> implements FLaborTimeValidator {

    private static final Logger LOGGER = Logger.getLogger(LaborTimeSBSImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final VIFContext context, final boolean insertMode, final FLaborTimeBBean target,
            final Errors errors) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - validate( FLaborTimeVbean = " + target + " )");
        }
        // Error if record exists in insertMode.
        if (insertMode) {
            LaborTimeKey laborTimeKey = target.getLaborTimeKey();
            if (laborTimeKey.getEstKey().getCsoc() == null || laborTimeKey.getEstKey().getCsoc().equals("")) {
                rejectValue(context, errors, "laborTimeKey.estKey.csoc", new TranslatedException(Generic.T12640,
                        GenKernel.T27957));
            }
            if (laborTimeKey.getEstKey().getCetab() == null || laborTimeKey.getEstKey().getCetab().equals("")) {
                rejectValue(context, errors, "laborTimeKey.estKey.cetab", new TranslatedException(Generic.T12640,
                        GenKernel.T189));
            }
            if (laborTimeKey.getProductionDate() == null) {
                rejectValue(context, errors, "laborTimeKey.productionDate", new TranslatedException(Generic.T12640,
                        ProductionMo.T34694));
            }
            if (laborTimeKey.getResourceCode() == null || laborTimeKey.getResourceCode().equals("")) {
                rejectValue(context, errors, "laborTimeKey.resourceCode", new TranslatedException(Generic.T12640,
                        ProductionMo.T32440));
            }
            if (laborTimeKey.getNi1() == 0) {
                rejectValue(context, errors, "laborTimeKey.ni1", new TranslatedException(Generic.T12640, Generic.T1755));
            }
            try {
                Ktpsx ktpsx = KtpsxDAOFactory.getDAO().getByPK(context.getDbCtx(), laborTimeKey.getEstKey().getCsoc(),
                        laborTimeKey.getEstKey().getCetab(), laborTimeKey.getProductionDate(),
                        laborTimeKey.getResourceCode(), laborTimeKey.getNi1());
                if ((ktpsx != null) && (ktpsx.getDatheurdebr() != null)) {
                    reject(context, errors,
                            new TranslatedException(Jtech.T24683, new VarParamTranslation(ktpsx.getDatheurdebr())));
                }
            } catch (DAOException e) {
                LOGGER.error("Unable to validate the labor time corresponding to the bean " + target, e);
            }
        }

    }
}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPOTtartremp.java,v $
 * Created on 24 mars 2016 by presta15
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:tempxxfabpxy.p.xml.
 * 
 * @author presta15
 */
public class XxfabPPOTtartremp extends AbstractTempTable implements Serializable {


    private String cArt = "";

    private String lArt = "";


    /**
     * Default Constructor.
     *
     */
    public XxfabPPOTtartremp() {
        super();
    }

    /**
     * Gets the cArt.
     *
     * @category getter
     * @return the cArt.
     */
    public final String getCArt() {
        return cArt;
    }

    /**
     * Sets the cArt.
     *
     * @category setter
     * @param cArt cArt.
     */
    public final void setCArt(final String cArt) {
        this.cArt = cArt;
    }

    /**
     * Gets the lArt.
     *
     * @category getter
     * @return the lArt.
     */
    public final String getLArt() {
        return lArt;
    }

    /**
     * Sets the lArt.
     *
     * @category setter
     * @param lArt lArt.
     */
    public final void setLArt(final String lArt) {
        this.lArt = lArt;
    }

}

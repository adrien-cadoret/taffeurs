/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabrecPPOTtcrivproxy.java,v $
 * Created on 16 mai 2014 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.boning;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/xxfabrecpxy.p.xml.
 * 
 * @author cj
 */
public class XxfabrecPPOTtcrivproxy extends AbstractTempTable implements Serializable {

    private String  ccri      = "";

    private String  cetab     = "";

    private String  cle       = "";

    private String  corig     = "";

    private String  csoc      = "";

    private String  cstade    = "";

    private String  cunite    = "";

    private Date    datem     = null;

    private int     heurem    = 0;

    private String  infoStade = "";

    private String  lcri      = "";

    private String  lformat   = "";

    private String  libaff    = "";

    private String  msaisie   = "";

    private int     nlig      = 0;

    private String  refval    = "";

    private boolean texclus   = false;

    private boolean tsaimodif = false;

    private boolean tsaioblig = false;

    private boolean tsaizero  = false;

    private String  typcle    = "";

    private String  type      = "";

    private String  valcmax   = "";

    private String  valcmin   = "";

    private Date    valdmax   = null;

    private Date    valdmin   = null;

    private double  valnmax   = 0;

    private double  valnmin   = 0;

    /**
     * Default Constructor.
     * 
     */
    public XxfabrecPPOTtcrivproxy() {
        super();
    }

    /**
     * Gets the ccri.
     * 
     * @category getter
     * @return the ccri.
     */
    public final String getCcri() {
        return ccri;
    }

    /**
     * Gets the cetab.
     * 
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Gets the cle.
     * 
     * @category getter
     * @return the cle.
     */
    public final String getCle() {
        return cle;
    }

    /**
     * Gets the corig.
     * 
     * @category getter
     * @return the corig.
     */
    public final String getCorig() {
        return corig;
    }

    /**
     * Gets the csoc.
     * 
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Gets the cstade.
     * 
     * @category getter
     * @return the cstade.
     */
    public final String getCstade() {
        return cstade;
    }

    /**
     * Gets the cunite.
     * 
     * @category getter
     * @return the cunite.
     */
    public final String getCunite() {
        return cunite;
    }

    /**
     * Gets the datem.
     * 
     * @category getter
     * @return the datem.
     */
    public final Date getDatem() {
        return datem;
    }

    /**
     * Gets the heurem.
     * 
     * @category getter
     * @return the heurem.
     */
    public final int getHeurem() {
        return heurem;
    }

    /**
     * Gets the infoStade.
     * 
     * @category getter
     * @return the infoStade.
     */
    public final String getInfoStade() {
        return infoStade;
    }

    /**
     * Gets the lcri.
     * 
     * @category getter
     * @return the lcri.
     */
    public final String getLcri() {
        return lcri;
    }

    /**
     * Gets the lformat.
     * 
     * @category getter
     * @return the lformat.
     */
    public final String getLformat() {
        return lformat;
    }

    /**
     * Gets the libaff.
     * 
     * @category getter
     * @return the libaff.
     */
    public final String getLibaff() {
        return libaff;
    }

    /**
     * Gets the msaisie.
     * 
     * @category getter
     * @return the msaisie.
     */
    public final String getMsaisie() {
        return msaisie;
    }

    /**
     * Gets the nlig.
     * 
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Gets the refval.
     * 
     * @category getter
     * @return the refval.
     */
    public final String getRefval() {
        return refval;
    }

    /**
     * Gets the texclus.
     * 
     * @category getter
     * @return the texclus.
     */
    public final boolean getTexclus() {
        return texclus;
    }

    /**
     * Gets the tsaimodif.
     * 
     * @category getter
     * @return the tsaimodif.
     */
    public final boolean getTsaimodif() {
        return tsaimodif;
    }

    /**
     * Gets the tsaioblig.
     * 
     * @category getter
     * @return the tsaioblig.
     */
    public final boolean getTsaioblig() {
        return tsaioblig;
    }

    /**
     * Gets the tsaizero.
     * 
     * @category getter
     * @return the tsaizero.
     */
    public final boolean getTsaizero() {
        return tsaizero;
    }

    /**
     * Gets the typcle.
     * 
     * @category getter
     * @return the typcle.
     */
    public final String getTypcle() {
        return typcle;
    }

    /**
     * Gets the type.
     * 
     * @category getter
     * @return the type.
     */
    public final String getType() {
        return type;
    }

    /**
     * Gets the valcmax.
     * 
     * @category getter
     * @return the valcmax.
     */
    public final String getValcmax() {
        return valcmax;
    }

    /**
     * Gets the valcmin.
     * 
     * @category getter
     * @return the valcmin.
     */
    public final String getValcmin() {
        return valcmin;
    }

    /**
     * Gets the valdmax.
     * 
     * @category getter
     * @return the valdmax.
     */
    public final Date getValdmax() {
        return valdmax;
    }

    /**
     * Gets the valdmin.
     * 
     * @category getter
     * @return the valdmin.
     */
    public final Date getValdmin() {
        return valdmin;
    }

    /**
     * Gets the valnmax.
     * 
     * @category getter
     * @return the valnmax.
     */
    public final double getValnmax() {
        return valnmax;
    }

    /**
     * Gets the valnmin.
     * 
     * @category getter
     * @return the valnmin.
     */
    public final double getValnmin() {
        return valnmin;
    }

    /**
     * Sets the ccri.
     * 
     * @category setter
     * @param ccri ccri.
     */
    public final void setCcri(final String ccri) {
        this.ccri = ccri;
    }

    /**
     * Sets the cetab.
     * 
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Sets the cle.
     * 
     * @category setter
     * @param cle cle.
     */
    public final void setCle(final String cle) {
        this.cle = cle;
    }

    /**
     * Sets the corig.
     * 
     * @category setter
     * @param corig corig.
     */
    public final void setCorig(final String corig) {
        this.corig = corig;
    }

    /**
     * Sets the csoc.
     * 
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Sets the cstade.
     * 
     * @category setter
     * @param cstade cstade.
     */
    public final void setCstade(final String cstade) {
        this.cstade = cstade;
    }

    /**
     * Sets the cunite.
     * 
     * @category setter
     * @param cunite cunite.
     */
    public final void setCunite(final String cunite) {
        this.cunite = cunite;
    }

    /**
     * Sets the datem.
     * 
     * @category setter
     * @param datem datem.
     */
    public final void setDatem(final Date datem) {
        this.datem = datem;
    }

    /**
     * Sets the heurem.
     * 
     * @category setter
     * @param heurem heurem.
     */
    public final void setHeurem(final int heurem) {
        this.heurem = heurem;
    }

    /**
     * Sets the infoStade.
     * 
     * @category setter
     * @param infoStade infoStade.
     */
    public final void setInfoStade(final String infoStade) {
        this.infoStade = infoStade;
    }

    /**
     * Sets the lcri.
     * 
     * @category setter
     * @param lcri lcri.
     */
    public final void setLcri(final String lcri) {
        this.lcri = lcri;
    }

    /**
     * Sets the lformat.
     * 
     * @category setter
     * @param lformat lformat.
     */
    public final void setLformat(final String lformat) {
        this.lformat = lformat;
    }

    /**
     * Sets the libaff.
     * 
     * @category setter
     * @param libaff libaff.
     */
    public final void setLibaff(final String libaff) {
        this.libaff = libaff;
    }

    /**
     * Sets the msaisie.
     * 
     * @category setter
     * @param msaisie msaisie.
     */
    public final void setMsaisie(final String msaisie) {
        this.msaisie = msaisie;
    }

    /**
     * Sets the nlig.
     * 
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Sets the refval.
     * 
     * @category setter
     * @param refval refval.
     */
    public final void setRefval(final String refval) {
        this.refval = refval;
    }

    /**
     * Sets the texclus.
     * 
     * @category setter
     * @param texclus texclus.
     */
    public final void setTexclus(final boolean texclus) {
        this.texclus = texclus;
    }

    /**
     * Sets the tsaimodif.
     * 
     * @category setter
     * @param tsaimodif tsaimodif.
     */
    public final void setTsaimodif(final boolean tsaimodif) {
        this.tsaimodif = tsaimodif;
    }

    /**
     * Sets the tsaioblig.
     * 
     * @category setter
     * @param tsaioblig tsaioblig.
     */
    public final void setTsaioblig(final boolean tsaioblig) {
        this.tsaioblig = tsaioblig;
    }

    /**
     * Sets the tsaizero.
     * 
     * @category setter
     * @param tsaizero tsaizero.
     */
    public final void setTsaizero(final boolean tsaizero) {
        this.tsaizero = tsaizero;
    }

    /**
     * Sets the typcle.
     * 
     * @category setter
     * @param typcle typcle.
     */
    public final void setTypcle(final String typcle) {
        this.typcle = typcle;
    }

    /**
     * Sets the type.
     * 
     * @category setter
     * @param type type.
     */
    public final void setType(final String type) {
        this.type = type;
    }

    /**
     * Sets the valcmax.
     * 
     * @category setter
     * @param valcmax valcmax.
     */
    public final void setValcmax(final String valcmax) {
        this.valcmax = valcmax;
    }

    /**
     * Sets the valcmin.
     * 
     * @category setter
     * @param valcmin valcmin.
     */
    public final void setValcmin(final String valcmin) {
        this.valcmin = valcmin;
    }

    /**
     * Sets the valdmax.
     * 
     * @category setter
     * @param valdmax valdmax.
     */
    public final void setValdmax(final Date valdmax) {
        this.valdmax = valdmax;
    }

    /**
     * Sets the valdmin.
     * 
     * @category setter
     * @param valdmin valdmin.
     */
    public final void setValdmin(final Date valdmin) {
        this.valdmin = valdmin;
    }

    /**
     * Sets the valnmax.
     * 
     * @category setter
     * @param valnmax valnmax.
     */
    public final void setValnmax(final double valnmax) {
        this.valnmax = valnmax;
    }

    /**
     * Sets the valnmin.
     * 
     * @category setter
     * @param valnmin valnmin.
     */
    public final void setValnmin(final double valnmin) {
        this.valnmin = valnmin;
    }

}

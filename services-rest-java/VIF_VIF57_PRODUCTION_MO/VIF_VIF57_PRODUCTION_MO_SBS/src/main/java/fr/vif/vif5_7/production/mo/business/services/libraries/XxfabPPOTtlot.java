/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPOTtlot.java,v $
 * Created on 24 mars 2016 by presta15
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:tempxxfabpxy.p.xml.
 * 
 * @author presta15
 */
public class XxfabPPOTtlot extends AbstractTempTable implements Serializable {


    private String lot = "";


    /**
     * Default Constructor.
     *
     */
    public XxfabPPOTtlot() {
        super();
    }

    /**
     * Gets the lot.
     *
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Sets the lot.
     *
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

}

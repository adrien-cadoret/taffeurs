/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxoiaPPO.java,v $
 * Created on 22 janv. 2008 by ped
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.operationitem;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.StringHolder;


/**
 * Generated class from xxoiapxy.p.
 * 
 * @author ped
 */
public class XxoiaPPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(XxoiaPPO.class);

    /**
     * Context.
     */
    private VIFContext          ctx;

    /**
     * Default Constructor.
     * 
     * @param ctx Database Context.
     */
    public XxoiaPPO(final VIFContext ctx) {
        super();
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - XxoiaPPO(ctx=" + ctx + ")");
        }

        this.ctx = ctx;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - XxoiaPPO(ctx=" + ctx + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "xxoiapxy.p";
    }

    /**
     * Generated Class prMajCpropCavanc.
     * 
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @param iTypgest String INPUT
     * @param iPrechrof String INPUT
     * @param iChronof int INPUT
     * @param iNi1 int INPUT
     * @param iNi2 int INPUT
     * @param iNi3 int INPUT
     * @param iNi4 int INPUT
     * @param iCavanc String INPUT
     * @param oTactesel boolean OUTPUT
     * @param oTret boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prMajCpropCavanc(final String iCsoc, final String iCetab, final String iCposte, final String iCuser,
            final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2,
            final int iNi3, final int iNi4, final String iCavanc, final BooleanHolder oTactesel,
            final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - prMajCpropCavanc(iCsoc=" + iCsoc + ", iCetab=" + iCetab + ", iCposte=" + iCposte
                    + ", iCuser=" + iCuser + ", iTypgest=" + iTypgest + ", iPrechrof=" + iPrechrof + ", iChronof="
                    + iChronof + ", iNi1=" + iNi1 + ", iNi2=" + iNi2 + ", iNi3=" + iNi3 + ", iNi4=" + iNi4
                    + ", iCavanc=" + iCavanc + ", oTactesel=" + oTactesel + ", oTret=" + oTret + ", oMsg=" + oMsg + ")");
        }

        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTactesel == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prMajCpropCavanc values :" + "oTactesel = " + oTactesel + "oTret = " + oTret
                        + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iCavanc, ParamArrayMode.INPUT);
            params.addLogical(12, null, ParamArrayMode.OUTPUT);
            params.addLogical(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Maj-Cprop-Cavanc", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oTactesel.setBooleanValue((Boolean) params.getOutputParameter(12));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(13));
            oMsg.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - prMajCpropCavanc(iCsoc=" + iCsoc + ", iCetab=" + iCetab + ", iCposte=" + iCposte
                    + ", iCuser=" + iCuser + ", iTypgest=" + iTypgest + ", iPrechrof=" + iPrechrof + ", iChronof="
                    + iChronof + ", iNi1=" + iNi1 + ", iNi2=" + iNi2 + ", iNi3=" + iNi3 + ", iNi4=" + iNi4
                    + ", iCavanc=" + iCavanc + ", oTactesel=" + oTactesel + ", oTret=" + oTret + ", oMsg=" + oMsg + ")");
        }
    }

}

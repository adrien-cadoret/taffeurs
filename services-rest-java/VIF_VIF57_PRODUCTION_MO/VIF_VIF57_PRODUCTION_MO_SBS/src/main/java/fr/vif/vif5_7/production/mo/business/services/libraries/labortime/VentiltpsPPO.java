/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: VentiltpsPPO.java,v $
 * Created on 21 nov. 2014 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.labortime;


import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.StringHolder;


/**
 * Generated class from ventiltpspxy.p.
 * 
 * @author cj
 * @version $Revision: 1.1 $, $Date: 2015/06/11 09:43:50 $
 */
public class VentiltpsPPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(VentiltpsPPO.class);

    /**
     * Context.
     */
    private VIFContext          ctx;

    /**
     * Default Constructor.
     *
     * @param ctx Database Context.
     */
    public VentiltpsPPO(final VIFContext ctx) {
        super();
        this.ctx = ctx;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "ventiltpspxy.p";
    }

    /**
     * Generated Class periodesSansOf.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iDatprod Date INPUT
     * @param oPrechroCR String OUTPUT
     * @param oChronoCR int OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void periodesSansOf(final String iCsoc, final String iCetab, final Date iDatprod,
            final StringHolder oPrechroCR, final IntegerHolder oChronoCR, final BooleanHolder oRet,
            final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oPrechroCR == null || oChronoCR == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : periodesSansOf values :" + "oPrechroCR = " + oPrechroCR + "oChronoCR = "
                        + oChronoCR + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method periodesSansOf " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iDatprod = "
                    + iDatprod + " oPrechroCR = " + oPrechroCR + " oChronoCR = " + oChronoCR + " oRet = " + oRet
                    + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(7);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addDate(2, dateToGreg(iDatprod), ParamArrayMode.INPUT);
            params.addCharacter(3, null, ParamArrayMode.OUTPUT);
            params.addInteger(4, null, ParamArrayMode.OUTPUT);
            params.addLogical(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Periodes-sans-of", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oPrechroCR.setStringValue((String) params.getOutputParameter(3));
            oChronoCR.setIntegerValue((Integer) params.getOutputParameter(4));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(5));
            oMsg.setStringValue((String) params.getOutputParameter(6));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class ventilerMoJour.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iDatprod Date INPUT
     * @param iLigne String INPUT
     * @param iLstCresMO String INPUT
     * @param iCmotmvk String INPUT
     * @param iCuser String INPUT
     * @param oPrechroCR String OUTPUT
     * @param oChronoCR int OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void ventilerMoJour(final String iCsoc, final String iCetab, final Date iDatprod, final String iLigne,
            final String iLstCresMO, final String iCmotmvk, final String iCuser, final StringHolder oPrechroCR,
            final IntegerHolder oChronoCR, final BooleanHolder oRet, final StringHolder oMsg)
                    throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oPrechroCR == null || oChronoCR == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : ventilerMoJour values :" + "oPrechroCR = " + oPrechroCR + "oChronoCR = "
                        + oChronoCR + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method ventilerMoJour " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iDatprod = "
                    + iDatprod + " iLigne = " + iLigne + " iLstCresMO = " + iLstCresMO + " iCmotmvk = " + iCmotmvk
                    + " iCuser = " + iCuser + " oPrechroCR = " + oPrechroCR + " oChronoCR = " + oChronoCR + " oRet = "
                    + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(11);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addDate(2, dateToGreg(iDatprod), ParamArrayMode.INPUT);
            params.addCharacter(3, iLigne, ParamArrayMode.INPUT);
            params.addCharacter(4, iLstCresMO, ParamArrayMode.INPUT);
            params.addCharacter(5, iCmotmvk, ParamArrayMode.INPUT);
            params.addCharacter(6, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
            params.addInteger(8, null, ParamArrayMode.OUTPUT);
            params.addLogical(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Ventiler-MO-Jour", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oPrechroCR.setStringValue((String) params.getOutputParameter(7));
            oChronoCR.setIntegerValue((Integer) params.getOutputParameter(8));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(9));
            oMsg.setStringValue((String) params.getOutputParameter(10));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

}

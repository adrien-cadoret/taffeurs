/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: BoningLabelSBSImpl.java,v $
 * Created on 13 Jun 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.boning.labels;


import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.boning.labels.LabelBean;
import fr.vif.vif5_7.production.mo.business.services.libraries.boning.XxfabrecPPO;
import fr.vif.vif5_7.production.mo.business.services.libraries.boning.XxfabrecPPOTtXetiq;
import fr.vif.vif5_7.workshop.workstation.business.beans.common.WorkStation;
import fr.vif.vif5_7.workshop.workstation.business.beans.common.WorkStationKey;
import fr.vif.vif5_7.workshop.workstation.business.services.libraries.workstation.WorkStationSBS;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;


/**
 * Implementation for boning label.
 * 
 * @author cj
 */
@Component
public class BoningLabelSBSImpl implements BoningLabelSBS {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(BoningLabelSBSImpl.class);

    @Autowired
    private WorkStationSBS      workStationSBS;

    /**
     * Gets the workStationSBS.
     * 
     * @return the workStationSBS.
     */
    public WorkStationSBS getWorkStationSBS() {
        return workStationSBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LabelBean> listLabelsByWorkstation(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listLabelsByWorkstation(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", workstationId=" + workstationId + ")");
        }
        List<LabelBean> lst = new ArrayList<LabelBean>();
        TempTableHolder<XxfabrecPPOTtXetiq> labels = new TempTableHolder<XxfabrecPPOTtXetiq>();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        XxfabrecPPO xxfabrecPPO = new XxfabrecPPO(ctx);
        xxfabrecPPO.prListeEtiquette(establishmentKey.getCsoc(), establishmentKey.getCetab(), workstationId, labels,
                oRet, oMsg);
        // get the printers list for the current workstation
        WorkStation ws = getWorkStationSBS().getWorkStation(ctx, new WorkStationKey(establishmentKey, workstationId));
        if (ws != null && ws.getListPrinters() != null) {
            for (XxfabrecPPOTtXetiq etiq : labels.getListValue()) {
                // keep the label if it uses a printer linked to the workstation
                if (ws.getListPrinters().contains(etiq.getCimput())) {
                    LabelBean bean = new LabelBean();
                    bean.setCode(etiq.getCetiq());
                    bean.setLabel(etiq.getLetiq());
                    bean.setEdiPrg(etiq.getPrgedi());
                    bean.setType(etiq.getCtypdoc());
                    lst.add(bean);
                }
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listLabelsByWorkstation(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", workstationId=" + workstationId + ")=" + lst);
        }
        return lst;
    }

    /**
     * Sets the workStationSBS.
     * 
     * @param workStationSBS workStationSBS.
     */
    public void setWorkStationSBS(final WorkStationSBS workStationSBS) {
        this.workStationSBS = workStationSBS;
    }
}

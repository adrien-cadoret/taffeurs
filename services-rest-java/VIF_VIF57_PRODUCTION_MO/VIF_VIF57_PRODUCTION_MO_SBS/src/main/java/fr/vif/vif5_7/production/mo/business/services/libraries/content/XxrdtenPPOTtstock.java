/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxrdtenPPOTtstock.java,v $
 * Created on 16 juil. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.content;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/xxrdtenpxy.p.xml.
 * 
 * @author cj
 */
public class XxrdtenPPOTtstock extends AbstractTempTable implements Serializable {

    private String cArt      = "";

    private String cArtPrev  = "";

    private String cuCri     = "";

    private String cunite    = "";

    private String lot       = "";

    private int    nOrdre    = 0;

    private double qteAfer   = 0;

    private double qteStk    = 0;

    private double valCri    = 0;

    private double valCriRes = 0;

    /**
     * Default Constructor.
     * 
     */
    public XxrdtenPPOTtstock() {
        super();
    }

    /**
     * Gets the cArt.
     * 
     * @category getter
     * @return the cArt.
     */
    public final String getCArt() {
        return cArt;
    }

    /**
     * Gets the cArtPrev.
     * 
     * @category getter
     * @return the cArtPrev.
     */
    public final String getCArtPrev() {
        return cArtPrev;
    }

    /**
     * Gets the cuCri.
     * 
     * @category getter
     * @return the cuCri.
     */
    public final String getCuCri() {
        return cuCri;
    }

    /**
     * Gets the cunite.
     * 
     * @category getter
     * @return the cunite.
     */
    public final String getCunite() {
        return cunite;
    }

    /**
     * Gets the lot.
     * 
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Gets the nOrdre.
     * 
     * @category getter
     * @return the nOrdre.
     */
    public final int getNOrdre() {
        return nOrdre;
    }

    /**
     * Gets the qteAfer.
     * 
     * @category getter
     * @return the qteAfer.
     */
    public final double getQteAfer() {
        return qteAfer;
    }

    /**
     * Gets the qteStk.
     * 
     * @category getter
     * @return the qteStk.
     */
    public final double getQteStk() {
        return qteStk;
    }

    /**
     * Gets the valCri.
     * 
     * @category getter
     * @return the valCri.
     */
    public final double getValCri() {
        return valCri;
    }

    /**
     * Gets the valCriRes.
     * 
     * @category getter
     * @return the valCriRes.
     */
    public final double getValCriRes() {
        return valCriRes;
    }

    /**
     * Sets the cArt.
     * 
     * @category setter
     * @param cart cArt.
     */
    public final void setCArt(final String cart) {
        this.cArt = cart;
    }

    /**
     * Sets the cArtPrev.
     * 
     * @category setter
     * @param cartPrev cArtPrev.
     */
    public final void setCArtPrev(final String cartPrev) {
        this.cArtPrev = cartPrev;
    }

    /**
     * Sets the cuCri.
     * 
     * @category setter
     * @param cuCri cuCri.
     */
    public final void setCuCri(final String cuCri) {
        this.cuCri = cuCri;
    }

    /**
     * Sets the cunite.
     * 
     * @category setter
     * @param cunite cunite.
     */
    public final void setCunite(final String cunite) {
        this.cunite = cunite;
    }

    /**
     * Sets the lot.
     * 
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Sets the nOrdre.
     * 
     * @category setter
     * @param nordre nOrdre.
     */
    public final void setNOrdre(final int nordre) {
        this.nOrdre = nordre;
    }

    /**
     * Sets the qteAfer.
     * 
     * @category setter
     * @param qteAfer qteAfer.
     */
    public final void setQteAfer(final double qteAfer) {
        this.qteAfer = qteAfer;
    }

    /**
     * Sets the qteStk.
     * 
     * @category setter
     * @param qteStk qteStk.
     */
    public final void setQteStk(final double qteStk) {
        this.qteStk = qteStk;
    }

    /**
     * Sets the valCri.
     * 
     * @category setter
     * @param valCri valCri.
     */
    public final void setValCri(final double valCri) {
        this.valCri = valCri;
    }

    /**
     * Sets the valCriRes.
     * 
     * @category setter
     * @param valCriRes valCriRes.
     */
    public final void setValCriRes(final double valCriRes) {
        this.valCriRes = valCriRes;
    }

}

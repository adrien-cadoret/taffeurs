/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPOTtCrivHandle.java,v $
 * Created on 24 mars 2016 by presta15
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:tempxxfabpxy.p.xml.
 * 
 * @author presta15
 */
public class XxfabPPOTtCrivHandle extends AbstractTempTable implements Serializable {


    private String ccri = "";

    private String cetab = "";

    private String cle = "";

    private String cmotmvk = "";

    private String corig = "";

    private String crifaInfo = "";

    private String csoc = "";

    private String cstade = "";

    private String cunite = "";

    private String libaff = "";

    private String msaisie = "";

    private String msgctrl = "";

    private int ni1 = 0;

    private int nlig = 0;

    private boolean tanomotif = false;

    private boolean texclus = false;

    private boolean tsaimodif = false;

    private boolean tsaioblig = false;

    private boolean tsaizero = false;

    private String typcle = "";

    private String type = "";

    private String valcmax = "";

    private String valcmin = "";

    private Date valdmax = null;

    private Date valdmin = null;

    private String[] valmotif;

    private double valnmax = 0;

    private double valnmin = 0;


    /**
     * Default Constructor.
     *
     */
    public XxfabPPOTtCrivHandle() {
        super();
    }

    /**
     * Gets the ccri.
     *
     * @category getter
     * @return the ccri.
     */
    public final String getCcri() {
        return ccri;
    }

    /**
     * Sets the ccri.
     *
     * @category setter
     * @param ccri ccri.
     */
    public final void setCcri(final String ccri) {
        this.ccri = ccri;
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the cle.
     *
     * @category getter
     * @return the cle.
     */
    public final String getCle() {
        return cle;
    }

    /**
     * Sets the cle.
     *
     * @category setter
     * @param cle cle.
     */
    public final void setCle(final String cle) {
        this.cle = cle;
    }

    /**
     * Gets the cmotmvk.
     *
     * @category getter
     * @return the cmotmvk.
     */
    public final String getCmotmvk() {
        return cmotmvk;
    }

    /**
     * Sets the cmotmvk.
     *
     * @category setter
     * @param cmotmvk cmotmvk.
     */
    public final void setCmotmvk(final String cmotmvk) {
        this.cmotmvk = cmotmvk;
    }

    /**
     * Gets the corig.
     *
     * @category getter
     * @return the corig.
     */
    public final String getCorig() {
        return corig;
    }

    /**
     * Sets the corig.
     *
     * @category setter
     * @param corig corig.
     */
    public final void setCorig(final String corig) {
        this.corig = corig;
    }

    /**
     * Gets the crifaInfo.
     *
     * @category getter
     * @return the crifaInfo.
     */
    public final String getCrifaInfo() {
        return crifaInfo;
    }

    /**
     * Sets the crifaInfo.
     *
     * @category setter
     * @param crifaInfo crifaInfo.
     */
    public final void setCrifaInfo(final String crifaInfo) {
        this.crifaInfo = crifaInfo;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the cstade.
     *
     * @category getter
     * @return the cstade.
     */
    public final String getCstade() {
        return cstade;
    }

    /**
     * Sets the cstade.
     *
     * @category setter
     * @param cstade cstade.
     */
    public final void setCstade(final String cstade) {
        this.cstade = cstade;
    }

    /**
     * Gets the cunite.
     *
     * @category getter
     * @return the cunite.
     */
    public final String getCunite() {
        return cunite;
    }

    /**
     * Sets the cunite.
     *
     * @category setter
     * @param cunite cunite.
     */
    public final void setCunite(final String cunite) {
        this.cunite = cunite;
    }

    /**
     * Gets the libaff.
     *
     * @category getter
     * @return the libaff.
     */
    public final String getLibaff() {
        return libaff;
    }

    /**
     * Sets the libaff.
     *
     * @category setter
     * @param libaff libaff.
     */
    public final void setLibaff(final String libaff) {
        this.libaff = libaff;
    }

    /**
     * Gets the msaisie.
     *
     * @category getter
     * @return the msaisie.
     */
    public final String getMsaisie() {
        return msaisie;
    }

    /**
     * Sets the msaisie.
     *
     * @category setter
     * @param msaisie msaisie.
     */
    public final void setMsaisie(final String msaisie) {
        this.msaisie = msaisie;
    }

    /**
     * Gets the msgctrl.
     *
     * @category getter
     * @return the msgctrl.
     */
    public final String getMsgctrl() {
        return msgctrl;
    }

    /**
     * Sets the msgctrl.
     *
     * @category setter
     * @param msgctrl msgctrl.
     */
    public final void setMsgctrl(final String msgctrl) {
        this.msgctrl = msgctrl;
    }

    /**
     * Gets the ni1.
     *
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Sets the ni1.
     *
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Gets the nlig.
     *
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Sets the nlig.
     *
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Gets the tanomotif.
     *
     * @category getter
     * @return the tanomotif.
     */
    public final boolean getTanomotif() {
        return tanomotif;
    }

    /**
     * Sets the tanomotif.
     *
     * @category setter
     * @param tanomotif tanomotif.
     */
    public final void setTanomotif(final boolean tanomotif) {
        this.tanomotif = tanomotif;
    }

    /**
     * Gets the texclus.
     *
     * @category getter
     * @return the texclus.
     */
    public final boolean getTexclus() {
        return texclus;
    }

    /**
     * Sets the texclus.
     *
     * @category setter
     * @param texclus texclus.
     */
    public final void setTexclus(final boolean texclus) {
        this.texclus = texclus;
    }

    /**
     * Gets the tsaimodif.
     *
     * @category getter
     * @return the tsaimodif.
     */
    public final boolean getTsaimodif() {
        return tsaimodif;
    }

    /**
     * Sets the tsaimodif.
     *
     * @category setter
     * @param tsaimodif tsaimodif.
     */
    public final void setTsaimodif(final boolean tsaimodif) {
        this.tsaimodif = tsaimodif;
    }

    /**
     * Gets the tsaioblig.
     *
     * @category getter
     * @return the tsaioblig.
     */
    public final boolean getTsaioblig() {
        return tsaioblig;
    }

    /**
     * Sets the tsaioblig.
     *
     * @category setter
     * @param tsaioblig tsaioblig.
     */
    public final void setTsaioblig(final boolean tsaioblig) {
        this.tsaioblig = tsaioblig;
    }

    /**
     * Gets the tsaizero.
     *
     * @category getter
     * @return the tsaizero.
     */
    public final boolean getTsaizero() {
        return tsaizero;
    }

    /**
     * Sets the tsaizero.
     *
     * @category setter
     * @param tsaizero tsaizero.
     */
    public final void setTsaizero(final boolean tsaizero) {
        this.tsaizero = tsaizero;
    }

    /**
     * Gets the typcle.
     *
     * @category getter
     * @return the typcle.
     */
    public final String getTypcle() {
        return typcle;
    }

    /**
     * Sets the typcle.
     *
     * @category setter
     * @param typcle typcle.
     */
    public final void setTypcle(final String typcle) {
        this.typcle = typcle;
    }

    /**
     * Gets the type.
     *
     * @category getter
     * @return the type.
     */
    public final String getType() {
        return type;
    }

    /**
     * Sets the type.
     *
     * @category setter
     * @param type type.
     */
    public final void setType(final String type) {
        this.type = type;
    }

    /**
     * Gets the valcmax.
     *
     * @category getter
     * @return the valcmax.
     */
    public final String getValcmax() {
        return valcmax;
    }

    /**
     * Sets the valcmax.
     *
     * @category setter
     * @param valcmax valcmax.
     */
    public final void setValcmax(final String valcmax) {
        this.valcmax = valcmax;
    }

    /**
     * Gets the valcmin.
     *
     * @category getter
     * @return the valcmin.
     */
    public final String getValcmin() {
        return valcmin;
    }

    /**
     * Sets the valcmin.
     *
     * @category setter
     * @param valcmin valcmin.
     */
    public final void setValcmin(final String valcmin) {
        this.valcmin = valcmin;
    }

    /**
     * Gets the valdmax.
     *
     * @category getter
     * @return the valdmax.
     */
    public final Date getValdmax() {
        return valdmax;
    }

    /**
     * Sets the valdmax.
     *
     * @category setter
     * @param valdmax valdmax.
     */
    public final void setValdmax(final Date valdmax) {
        this.valdmax = valdmax;
    }

    /**
     * Gets the valdmin.
     *
     * @category getter
     * @return the valdmin.
     */
    public final Date getValdmin() {
        return valdmin;
    }

    /**
     * Sets the valdmin.
     *
     * @category setter
     * @param valdmin valdmin.
     */
    public final void setValdmin(final Date valdmin) {
        this.valdmin = valdmin;
    }

    /**
     * Gets the valmotif.
     *
     * @category getter
     * @return the valmotif.
     */
    public final String[] getValmotif() {
        return valmotif;
    }

    /**
     * Sets the valmotif.
     *
     * @category setter
     * @param valmotif valmotif.
     */
    public final void setValmotif(final String[] valmotif) {
        this.valmotif = valmotif;
    }

    /**
     * Gets the valnmax.
     *
     * @category getter
     * @return the valnmax.
     */
    public final double getValnmax() {
        return valnmax;
    }

    /**
     * Sets the valnmax.
     *
     * @category setter
     * @param valnmax valnmax.
     */
    public final void setValnmax(final double valnmax) {
        this.valnmax = valnmax;
    }

    /**
     * Gets the valnmin.
     *
     * @category getter
     * @return the valnmin.
     */
    public final double getValnmin() {
        return valnmin;
    }

    /**
     * Sets the valnmin.
     *
     * @category setter
     * @param valnmin valnmin.
     */
    public final void setValnmin(final double valnmin) {
        this.valnmin = valnmin;
    }

}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPOTtxmvtm.java,v $
 * Created on 24 mars 2016 by presta15
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:tempxxfabpxy.p.xml.
 * 
 * @author presta15
 */
public class XxfabPPOTtxmvtm extends AbstractTempTable implements Serializable {


    private String cArt = "";

    private String cDepot = "";

    private String cEmp = "";

    private String cMotMvk = "";

    private String cPoste = "";

    private String cU1 = "";

    private String cU2 = "";

    private String cU3 = "";

    private Date datMvt = null;

    private int heurMvt = 0;

    private String lArt = "";

    private String lDepot = "";

    private String lEmp = "";

    private String lMotMvk = "";

    private String lot = "";

    private int nlig = 0;

    private double qte1 = 0;

    private double qte2 = 0;

    private double qte3 = 0;

    private String rowidMVT = "";


    /**
     * Default Constructor.
     *
     */
    public XxfabPPOTtxmvtm() {
        super();
    }

    /**
     * Gets the cArt.
     *
     * @category getter
     * @return the cArt.
     */
    public final String getCArt() {
        return cArt;
    }

    /**
     * Sets the cArt.
     *
     * @category setter
     * @param cArt cArt.
     */
    public final void setCArt(final String cArt) {
        this.cArt = cArt;
    }

    /**
     * Gets the cDepot.
     *
     * @category getter
     * @return the cDepot.
     */
    public final String getCDepot() {
        return cDepot;
    }

    /**
     * Sets the cDepot.
     *
     * @category setter
     * @param cDepot cDepot.
     */
    public final void setCDepot(final String cDepot) {
        this.cDepot = cDepot;
    }

    /**
     * Gets the cEmp.
     *
     * @category getter
     * @return the cEmp.
     */
    public final String getCEmp() {
        return cEmp;
    }

    /**
     * Sets the cEmp.
     *
     * @category setter
     * @param cEmp cEmp.
     */
    public final void setCEmp(final String cEmp) {
        this.cEmp = cEmp;
    }

    /**
     * Gets the cMotMvk.
     *
     * @category getter
     * @return the cMotMvk.
     */
    public final String getCMotMvk() {
        return cMotMvk;
    }

    /**
     * Sets the cMotMvk.
     *
     * @category setter
     * @param cMotMvk cMotMvk.
     */
    public final void setCMotMvk(final String cMotMvk) {
        this.cMotMvk = cMotMvk;
    }

    /**
     * Gets the cPoste.
     *
     * @category getter
     * @return the cPoste.
     */
    public final String getCPoste() {
        return cPoste;
    }

    /**
     * Sets the cPoste.
     *
     * @category setter
     * @param cPoste cPoste.
     */
    public final void setCPoste(final String cPoste) {
        this.cPoste = cPoste;
    }

    /**
     * Gets the cU1.
     *
     * @category getter
     * @return the cU1.
     */
    public final String getCU1() {
        return cU1;
    }

    /**
     * Sets the cU1.
     *
     * @category setter
     * @param cU1 cU1.
     */
    public final void setCU1(final String cU1) {
        this.cU1 = cU1;
    }

    /**
     * Gets the cU2.
     *
     * @category getter
     * @return the cU2.
     */
    public final String getCU2() {
        return cU2;
    }

    /**
     * Sets the cU2.
     *
     * @category setter
     * @param cU2 cU2.
     */
    public final void setCU2(final String cU2) {
        this.cU2 = cU2;
    }

    /**
     * Gets the cU3.
     *
     * @category getter
     * @return the cU3.
     */
    public final String getCU3() {
        return cU3;
    }

    /**
     * Sets the cU3.
     *
     * @category setter
     * @param cU3 cU3.
     */
    public final void setCU3(final String cU3) {
        this.cU3 = cU3;
    }

    /**
     * Gets the datMvt.
     *
     * @category getter
     * @return the datMvt.
     */
    public final Date getDatMvt() {
        return datMvt;
    }

    /**
     * Sets the datMvt.
     *
     * @category setter
     * @param datMvt datMvt.
     */
    public final void setDatMvt(final Date datMvt) {
        this.datMvt = datMvt;
    }

    /**
     * Gets the heurMvt.
     *
     * @category getter
     * @return the heurMvt.
     */
    public final int getHeurMvt() {
        return heurMvt;
    }

    /**
     * Sets the heurMvt.
     *
     * @category setter
     * @param heurMvt heurMvt.
     */
    public final void setHeurMvt(final int heurMvt) {
        this.heurMvt = heurMvt;
    }

    /**
     * Gets the lArt.
     *
     * @category getter
     * @return the lArt.
     */
    public final String getLArt() {
        return lArt;
    }

    /**
     * Sets the lArt.
     *
     * @category setter
     * @param lArt lArt.
     */
    public final void setLArt(final String lArt) {
        this.lArt = lArt;
    }

    /**
     * Gets the lDepot.
     *
     * @category getter
     * @return the lDepot.
     */
    public final String getLDepot() {
        return lDepot;
    }

    /**
     * Sets the lDepot.
     *
     * @category setter
     * @param lDepot lDepot.
     */
    public final void setLDepot(final String lDepot) {
        this.lDepot = lDepot;
    }

    /**
     * Gets the lEmp.
     *
     * @category getter
     * @return the lEmp.
     */
    public final String getLEmp() {
        return lEmp;
    }

    /**
     * Sets the lEmp.
     *
     * @category setter
     * @param lEmp lEmp.
     */
    public final void setLEmp(final String lEmp) {
        this.lEmp = lEmp;
    }

    /**
     * Gets the lMotMvk.
     *
     * @category getter
     * @return the lMotMvk.
     */
    public final String getLMotMvk() {
        return lMotMvk;
    }

    /**
     * Sets the lMotMvk.
     *
     * @category setter
     * @param lMotMvk lMotMvk.
     */
    public final void setLMotMvk(final String lMotMvk) {
        this.lMotMvk = lMotMvk;
    }

    /**
     * Gets the lot.
     *
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Sets the lot.
     *
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Gets the nlig.
     *
     * @category getter
     * @return the nlig.
     */
    public final int getNlig() {
        return nlig;
    }

    /**
     * Sets the nlig.
     *
     * @category setter
     * @param nlig nlig.
     */
    public final void setNlig(final int nlig) {
        this.nlig = nlig;
    }

    /**
     * Gets the qte1.
     *
     * @category getter
     * @return the qte1.
     */
    public final double getQte1() {
        return qte1;
    }

    /**
     * Sets the qte1.
     *
     * @category setter
     * @param qte1 qte1.
     */
    public final void setQte1(final double qte1) {
        this.qte1 = qte1;
    }

    /**
     * Gets the qte2.
     *
     * @category getter
     * @return the qte2.
     */
    public final double getQte2() {
        return qte2;
    }

    /**
     * Sets the qte2.
     *
     * @category setter
     * @param qte2 qte2.
     */
    public final void setQte2(final double qte2) {
        this.qte2 = qte2;
    }

    /**
     * Gets the qte3.
     *
     * @category getter
     * @return the qte3.
     */
    public final double getQte3() {
        return qte3;
    }

    /**
     * Sets the qte3.
     *
     * @category setter
     * @param qte3 qte3.
     */
    public final void setQte3(final double qte3) {
        this.qte3 = qte3;
    }

    /**
     * Gets the rowidMVT.
     *
     * @category getter
     * @return the rowidMVT.
     */
    public final String getRowidMVT() {
        return rowidMVT;
    }

    /**
     * Sets the rowidMVT.
     *
     * @category setter
     * @param rowidMVT rowidMVT.
     */
    public final void setRowidMVT(final String rowidMVT) {
        this.rowidMVT = rowidMVT;
    }

}

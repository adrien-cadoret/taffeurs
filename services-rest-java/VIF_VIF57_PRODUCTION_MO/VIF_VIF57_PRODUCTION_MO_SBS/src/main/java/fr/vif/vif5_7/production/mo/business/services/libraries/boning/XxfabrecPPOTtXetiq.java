/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabrecPPOTtXetiq.java,v $
 * Created on 16 mai 2014 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.boning;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/xxfabrecpxy.p.xml.
 * 
 * @author cj
 */
public class XxfabrecPPOTtXetiq extends AbstractTempTable implements Serializable {

    private String cetab   = "";

    private String cetiq   = "";

    private String cimput  = "";

    private String cposte  = "";

    private String csoc    = "";

    private String ctypdoc = "";

    private String letiq   = "";

    private String prgedi  = "";

    /**
     * Default Constructor.
     * 
     */
    public XxfabrecPPOTtXetiq() {
        super();
    }

    /**
     * Gets the cetab.
     * 
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Gets the cetiq.
     * 
     * @category getter
     * @return the cetiq.
     */
    public final String getCetiq() {
        return cetiq;
    }

    /**
     * Gets the cimput.
     * 
     * @category getter
     * @return the cimput.
     */
    public final String getCimput() {
        return cimput;
    }

    /**
     * Gets the cposte.
     * 
     * @category getter
     * @return the cposte.
     */
    public final String getCposte() {
        return cposte;
    }

    /**
     * Gets the csoc.
     * 
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Gets the ctypdoc.
     * 
     * @category getter
     * @return the ctypdoc.
     */
    public final String getCtypdoc() {
        return ctypdoc;
    }

    /**
     * Gets the letiq.
     * 
     * @category getter
     * @return the letiq.
     */
    public final String getLetiq() {
        return letiq;
    }

    /**
     * Gets the prgedi.
     * 
     * @category getter
     * @return the prgedi.
     */
    public final String getPrgedi() {
        return prgedi;
    }

    /**
     * Sets the cetab.
     * 
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Sets the cetiq.
     * 
     * @category setter
     * @param cetiq cetiq.
     */
    public final void setCetiq(final String cetiq) {
        this.cetiq = cetiq;
    }

    /**
     * Sets the cimput.
     * 
     * @category setter
     * @param cimput cimput.
     */
    public final void setCimput(final String cimput) {
        this.cimput = cimput;
    }

    /**
     * Sets the cposte.
     * 
     * @category setter
     * @param cposte cposte.
     */
    public final void setCposte(final String cposte) {
        this.cposte = cposte;
    }

    /**
     * Sets the csoc.
     * 
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Sets the ctypdoc.
     * 
     * @category setter
     * @param ctypdoc ctypdoc.
     */
    public final void setCtypdoc(final String ctypdoc) {
        this.ctypdoc = ctypdoc;
    }

    /**
     * Sets the letiq.
     * 
     * @category setter
     * @param letiq letiq.
     */
    public final void setLetiq(final String letiq) {
        this.letiq = letiq;
    }

    /**
     * Sets the prgedi.
     * 
     * @category setter
     * @param prgedi prgedi.
     */
    public final void setPrgedi(final String prgedi) {
        this.prgedi = prgedi;
    }

}

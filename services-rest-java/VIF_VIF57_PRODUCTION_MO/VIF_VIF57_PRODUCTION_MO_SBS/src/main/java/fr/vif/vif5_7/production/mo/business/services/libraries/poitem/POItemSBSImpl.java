/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: POItemSBSImpl.java,v $
 * Created on 20 Aug 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.poitem;


import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.BatchSBSImpl;
import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.TimeHelper;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.admin.config.business.services.libraries.parameter.ParameterSBS;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPO;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.dao.kof.Kof;
import fr.vif.vif5_7.production.mo.dao.kof.KofDAOFactory;
import fr.vif.vif5_7.production.mo.dao.kot.Kot;
import fr.vif.vif5_7.production.mo.dao.kot.KotDAOFactory;
import fr.vif.vif5_7.production.mo.dao.kota.Kota;
import fr.vif.vif5_7.production.mo.dao.kota.KotaDAOFactory;
import fr.vif.vif5_7.production.mo.dao.kotad.Kotad;
import fr.vif.vif5_7.production.mo.dao.kotad.KotadDAOFactory;


/**
 * SBS implementation for PO information.
 * 
 * @author cj
 */
@Component
public class POItemSBSImpl extends BatchSBSImpl implements POItemSBS {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(POItemSBSImpl.class);

    @Autowired
    private ParameterSBS        parameterSBS;

    /**
     * Gets the parameterSBS.
     *
     * @return the parameterSBS.
     */
    public ParameterSBS getParameterSBS() {
        return parameterSBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<OperationItemKey, OperationItemQuantityUnit[]> getPOItemListQuantityFromMo(final VIFContext ctx,
            final POKey poKey, final ActivityItemType type) throws ServerBusinessException {
        Map<OperationItemKey, OperationItemQuantityUnit[]> qtyItemList = new HashMap<OperationItemKey, OperationItemQuantityUnit[]>();
        List<Kotad> lstKotad;
        List<Kota> lstKota;
        OperationItemQuantityUnit oiqu1 = null;
        OperationItemQuantityUnit oiqu2 = null;
        try {
            lstKota = KotaDAOFactory.getDAO().listByI3(ctx.getDbCtx(), poKey.getEstablishmentKey().getCsoc(),
                    poKey.getEstablishmentKey().getCetab(), poKey.getChrono().getPrechro(),
                    poKey.getChrono().getChrono(), poKey.getChrono().getCounter1(), type.getValue(), null, true);
            lstKotad = KotadDAOFactory.getDAO().listByI3(ctx.getDbCtx(), poKey.getEstablishmentKey().getCsoc(),
                    poKey.getEstablishmentKey().getCetab(), poKey.getChrono().getPrechro(),
                    poKey.getChrono().getChrono(), poKey.getChrono().getCounter1(), null, null, true);
            String unit1 = "";
            String unit2 = "";
            // gets the toDo qty in first unit
            for (Kota kota : lstKota) {
                OperationItemKey oikey = new OperationItemKey(ManagementType.REAL, poKey.getEstablishmentKey(),
                        new Chrono(poKey.getChrono().getPrechro(), poKey.getChrono().getChrono()), kota.getNi1(),
                        kota.getNi2(), 0, 0);
                oiqu1 = new OperationItemQuantityUnit(kota.getQte()[0], 0, kota.getQte()[0], kota.getCunite());
                oiqu2 = new OperationItemQuantityUnit(0, 0, 0, "");
                OperationItemQuantityUnit[] qties = { oiqu1, oiqu2 };
                qtyItemList.put(oikey, qties);
            }
            // get the doneQty and first and second units
            for (Kotad kotad : lstKotad) {
                if (type.getValue().equals(kotad.getTypacta()) && "REAT".equals(kotad.getTypor())) {
                    OperationItemKey oikey = new OperationItemKey(ManagementType.REAL, poKey.getEstablishmentKey(),
                            new Chrono(poKey.getChrono().getPrechro(), poKey.getChrono().getChrono()), kotad.getNi1(),
                            kotad.getNi2(), 0, 0);
                    if (unit1.isEmpty()) {
                        unit1 = kotad.getCu1();
                    }
                    if (unit2.isEmpty()) {
                        unit2 = kotad.getCu2();
                    }
                    OperationItemQuantityUnit[] qties = qtyItemList.get(oikey);
                    qties[0].setCompletedQuantity(qties[0].getCompletedQuantity() + kotad.getQte1());
                    qties[1].setCompletedQuantity(qties[1].getCompletedQuantity() + kotad.getQte2());
                    qties[0].setUnit(unit1);
                    qties[1].setUnit(unit2);
                    qties[0].setLeftToDoQuantity(qties[0].getToDoQuantity() - qties[0].getCompletedQuantity());
                    qties[1].setLeftToDoQuantity(qties[1].getToDoQuantity() - qties[1].getCompletedQuantity());
                }
            }
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                LOGGER.error("getPOItemQuantityFromMo(ctx, poKey, type)Exception occured", e);
            }
            throw new ServerBusinessException(e);
        }
        return qtyItemList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationItemQuantityUnit getPOItemQuantity2(final VIFContext ctx, final OperationItemKey itemKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getPOItemQuantity(ctx=" + ctx + ", itemKey=" + itemKey + ")");
        }
        List<Kotad> lstKotad;
        OperationItemQuantityUnit oiqu2 = null;
        try {
            lstKotad = KotadDAOFactory.getDAO().listByI3(ctx.getDbCtx(), itemKey.getEstablishmentKey().getCsoc(),
                    itemKey.getEstablishmentKey().getCetab(), itemKey.getChrono().getPrechro(),
                    itemKey.getChrono().getChrono(), itemKey.getCounter1(), itemKey.getCounter4(), null, true);
            double toDoQuantity = 0;
            double completedQuantity2 = 0;
            double leftToDoQuantity = 0;
            String unit2 = "";
            if (lstKotad != null && !lstKotad.isEmpty()) {
                unit2 = lstKotad.get(0).getCu2();
            }
            for (Kotad kotad : lstKotad) {
                completedQuantity2 += kotad.getQte2();
            }
            oiqu2 = new OperationItemQuantityUnit(toDoQuantity, completedQuantity2, leftToDoQuantity, unit2);
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getPOItemQuantity(ctx=" + ctx + ", itemKey=" + itemKey + ")=" + oiqu2);
        }
        return oiqu2;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OperationItemQuantityUnit[] getPOItemQuantityFromMo(final VIFContext ctx, final POKey poKey,
            final ActivityItemType type) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getPOItemQuantityFromMo(ctx=" + ctx + ", poKey=" + poKey + ", type=" + type + ")");
        }
        List<Kotad> lstKotad;
        List<Kota> lstKota;
        OperationItemQuantityUnit oiqu1 = null;
        OperationItemQuantityUnit oiqu2 = null;
        try {
            lstKota = KotaDAOFactory.getDAO().listByI3(ctx.getDbCtx(), poKey.getEstablishmentKey().getCsoc(),
                    poKey.getEstablishmentKey().getCetab(), poKey.getChrono().getPrechro(),
                    poKey.getChrono().getChrono(), poKey.getChrono().getCounter1(), type.getValue(), null, true);
            lstKotad = KotadDAOFactory.getDAO().listByI3(ctx.getDbCtx(), poKey.getEstablishmentKey().getCsoc(),
                    poKey.getEstablishmentKey().getCetab(), poKey.getChrono().getPrechro(),
                    poKey.getChrono().getChrono(), poKey.getChrono().getCounter1(), null, null, true);
            double toDoQuantity = 0;
            double completedQuantity1 = 0;
            double completedQuantity2 = 0;
            double leftToDoQuantity = 0;
            String unit1 = "";
            String unit2 = "";
            // gets the toDo qty in first unit
            for (Kota kota : lstKota) {
                toDoQuantity += kota.getQte()[0];
            }
            // get the doneQty and first and second units
            for (Kotad kotad : lstKotad) {
                if (type.getValue().equals(kotad.getTypacta()) && "REAT".equals(kotad.getTypor())) {
                    if (unit1.isEmpty()) {
                        unit1 = kotad.getCu1();
                    }
                    if (unit2.isEmpty()) {
                        unit2 = kotad.getCu2();
                    }
                    completedQuantity1 += kotad.getQte1();
                    completedQuantity2 += kotad.getQte2();
                }
            }
            leftToDoQuantity = toDoQuantity - completedQuantity1;
            oiqu1 = new OperationItemQuantityUnit(toDoQuantity, completedQuantity1, leftToDoQuantity, unit1);
            oiqu2 = new OperationItemQuantityUnit(0, completedQuantity2, 0, unit2);
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                LOGGER.error("getPOItemQuantityFromMo(ctx, poKey, type)Exception occured", e);
            }
            throw new ServerBusinessException(e);
        }
        OperationItemQuantityUnit[] ret = { oiqu1, oiqu2 };
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getPOItemQuantityFromMo(ctx=" + ctx + ", poKey=" + poKey + ", type=" + type + ")=" + ret);
        }
        return ret;
    }

    /**
     * Sets the parameterSBS.
     *
     * @param parameterSBS parameterSBS.
     */
    public void setParameterSBS(final ParameterSBS parameterSBS) {
        this.parameterSBS = parameterSBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updatePOActualTime(final VIFContext ctx, final String sCsoc, final String sCetab,
            final String sRessourceId, final int iChrono, final String sCarts, final Date dActualDate,
            final String sEvent) throws ServerBusinessException {
        final int mille = 1000;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updatePOActualTime(ctx=" + ctx + ", sCsoc=" + sCsoc + ", sCetab=" + sCetab + ", iChrono="
                    + iChrono + ", dActualDate=" + dActualDate + ", sEvent=" + sEvent + ")");
        }

        try {
            Boolean setDateHourOnKOF = false;

            /* Update KOT */
            Kot kot = KotDAOFactory.getDAO().getByItem(ctx.getDbCtx(), sCsoc, sCetab, "1OF", iChrono, sCarts);

            if (sEvent.equals("START")) {
                /* update start date */
                if (!kot.getTdatdebr() || //
                        (dateHour1InfDateHour2(dActualDate, TimeHelper.getTime(dActualDate), //
                                kot.getDatdebr(), kot.getHeurdebr()))) {
                    kot.setDatdebr(dActualDate);
                    kot.setHeurdebr(TimeHelper.getTime(dActualDate));
                    kot.setTdatdebr(true);
                    setDateHourOnKOF = true;
                }
            } else if (sEvent.equals("STOP")) {
                /* update end date */
                if (!kot.getTdatfinr() || //
                        (dateHour1SupDateHour2(dActualDate, TimeHelper.getTime(dActualDate), //
                                kot.getDatfinr(), kot.getHeurfinr()))) {
                    kot.setDatfinr(dActualDate);
                    kot.setHeurfinr(TimeHelper.getTime(dActualDate));
                    kot.setTdatfinr(true);
                    setDateHourOnKOF = true;
                }
            }

            if (setDateHourOnKOF) {
                // Correction of incoherences
                if (dateHour1SupDateHour2(kot.getDatdebr(), kot.getHeurdebr(), kot.getDatfinr(), kot.getHeurfinr())) {
                    if (kot.getTdatdebr() && !kot.getTdatfinr()) {
                        kot.setDatfinr(kot.getDatdebr());
                        kot.setHeurfinr(kot.getHeurdebr());
                        kot.setTdatfinr(false);
                        kot.setDureetr(0);
                    } else if (!kot.getTdatdebr() && kot.getTdatfinr()) {
                        kot.setDatdebr(kot.getDatfinr());
                        kot.setHeurdebr(kot.getHeurfinr());
                        kot.setTdatdebr(false);
                        kot.setDureetr(0);
                    }
                }

                // Recalculates the real length
                if (dateHour1InfDateHour2(kot.getDatdebr(), kot.getHeurdebr(), kot.getDatfinr(), kot.getHeurfinr())) {
                    Date dh1 = DateHelper.getDateTime(kot.getDatdebr(), kot.getHeurdebr());
                    Date dh2 = DateHelper.getDateTime(kot.getDatfinr(), kot.getHeurfinr());
                    long length = (dh2.getTime() - dh1.getTime());

                    kot.setDureetr((int) length / mille);

                } else {
                    kot.setDureetr(0);
                }

                // Sends the date-hour at the KOF level
                updateMOActualTime(ctx, sCsoc, sCetab, sRessourceId, iChrono, sCarts, dActualDate, sEvent);
            }

            kot.setCcrif("TRS");

            KotDAOFactory.getDAO().update(ctx.getDbCtx(), kot);

            /* Update KFABEVT */
            XxfabPPO xxfabPPO = new XxfabPPO(ctx);
            xxfabPPO.prGestionfabevt(kot.getCsoc(), kot.getCetab(), kot.getPrechro(), kot.getChrono(), kot.getNi1());
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                LOGGER.error("updatePOActualTime(ctx, poKey, iLineNumber, dActualDate, iActualTime)Exception occured",
                        e);
            }
            throw new ServerBusinessException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updatePOActualTime(ctx=" + ctx + ", sCsoc=" + sCsoc + ", sCetab=" + sCetab + ", iChrono="
                    + iChrono + ", dActualDate=" + dActualDate + ", sEvent=" + sEvent + ")");
        }
    }

    /**
     * Compares 2 date-hour : returns true if the first one is inferior to the second one.
     *
     * @param date1 the first date.
     * @param hour1 the first hour.
     * @param date2 the first date.
     * @param hour2 the first hour.
     * @return true if the first date-hour is inferior to the second one.
     */
    private Boolean dateHour1InfDateHour2(final Date date1, final int hour1, final Date date2, final int hour2) {
        final Boolean result;

        if (date1 == null || date2 == null) {
            result = false;
        } else if (DateHelper.compareDate(date1, date2) < 0) {
            result = true;
        } else if (DateHelper.compareDate(date1, date2) > 0) {
            result = false;
        } else {
            result = hour1 < hour2;
        }

        return result;
    }

    /**
     * Compares 2 date-hour : returns true if the first one is superior to the second one.
     *
     * @param date1 the first date.
     * @param hour1 the first hour.
     * @param date2 the first date.
     * @param hour2 the first hour.
     * @return true if the first date-hour is superior to the second one.
     */
    private Boolean dateHour1SupDateHour2(final Date date1, final int hour1, final Date date2, final int hour2) {
        final Boolean result;

        if (date1 == null || date2 == null) {
            result = false;
        } else if (DateHelper.compareDate(date1, date2) > 0) {
            result = true;
        } else if (DateHelper.compareDate(date1, date2) < 0) {
            result = false;
        } else {
            result = hour1 > hour2;
        }

        return result;
    }

    /**
     * Update actual time of the MO item.
     * 
     * @param ctx VIF Context.
     * @param sCsoc the company code.
     * @param sCetab the establishment code
     * @param sRessourceId the ressource Id
     * @param iChrono the chrono number
     * @param sCarts the item code to process
     * @param dActualDate the date to update
     * @param sEvent the name of the event (start or stop)
     * @throws ServerBusinessException if errors occur
     */
    private void updateMOActualTime(final VIFContext ctx, final String sCsoc, final String sCetab,
            final String sRessourceId, final int iChrono, final String sCarts, final Date dActualDate,
            final String sEvent) throws ServerBusinessException {
        final int mille = 1000;

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateMOActualTime(ctx=" + ctx + ", sCsoc=" + sCsoc + ", sCetab=" + sCetab + ", iChrono="
                    + iChrono + ", dActualDate=" + dActualDate + ", sEvent=" + sEvent + ")");
        }

        try {
            Boolean modification = false;

            /* Update KOF */
            Kof kof2 = KofDAOFactory.getDAO().getByPK(ctx.getDbCtx(), sCsoc, sCetab, "1OF", iChrono);
            int admstamp = kof2.getAdmstamp();
            Kof kof = KofDAOFactory.getDAO().getByPKForUpdate(ctx.getDbCtx(), sCsoc, sCetab, "1OF", iChrono, admstamp);

            if (sEvent.equals("START")) {
                /* update start date */
                if (!kof.getTdatdebr() || //
                        (dateHour1InfDateHour2(dActualDate, TimeHelper.getTime(dActualDate), //
                                kof.getDatdebr(), kof.getHeurdebr()))) {
                    kof.setDatdebr(dActualDate);
                    kof.setHeurdebr(TimeHelper.getTime(dActualDate));
                    kof.setTdatdebr(true);
                    modification = true;
                }
            } else if (sEvent.equals("STOP")) {
                /* update end date */
                if (!kof.getTdatfinr() || //
                        (dateHour1SupDateHour2(dActualDate, TimeHelper.getTime(dActualDate), //
                                kof.getDatfinr(), kof.getHeurfinr()))) {
                    kof.setDatfinr(dActualDate);
                    kof.setHeurfinr(TimeHelper.getTime(dActualDate));
                    kof.setTdatfinr(true);
                    modification = true;
                }
            }

            // Sends the date-hour at the KOF level
            if (modification) {
                // Correction of incoherences
                if (dateHour1SupDateHour2(kof.getDatdebr(), kof.getHeurdebr(), kof.getDatfinr(), kof.getHeurfinr())) {
                    if (kof.getTdatdebr() && !kof.getTdatfinr()) {
                        kof.setDatfinr(kof.getDatdebr());
                        kof.setHeurfinr(kof.getHeurdebr());
                        kof.setTdatfinr(false);
                        kof.setDuree2(0);
                    } else if (!kof.getTdatdebr() && kof.getTdatfinr()) {
                        kof.setDatdebr(kof.getDatfinr());
                        kof.setHeurdebr(kof.getHeurfinr());
                        kof.setTdatdebr(false);
                        kof.setDuree2(0);
                    }
                }

                // Recalculates the real length
                if (dateHour1InfDateHour2(kof.getDatdebr(), kof.getHeurdebr(), kof.getDatfinr(), kof.getHeurfinr())) {
                    Date dh1 = DateHelper.getDateTime(kof.getDatdebr(), kof.getHeurdebr());
                    Date dh2 = DateHelper.getDateTime(kof.getDatfinr(), kof.getHeurfinr());
                    long length = (dh2.getTime() - dh1.getTime());

                    kof.setDuree2((int) length / mille);

                } else {
                    kof.setDuree2(0);
                }
            }

            kof.setAdmstamp(admstamp + 1);

            KofDAOFactory.getDAO().update(ctx.getDbCtx(), kof);
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(org.apache.log4j.Level.ERROR)) {
                LOGGER.error("updateMOActualTime(ctx, poKey, iLineNumber, dActualDate, iActualTime)Exception occured",
                        e);
            }
            throw new ServerBusinessException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updatePOActualTime(ctx=" + ctx + ", sCsoc=" + sCsoc + ", sCetab=" + sCetab + ", iChrono="
                    + iChrono + ", dActualDate=" + dActualDate + ", sEvent=" + sEvent + ")");
        }
    }
}

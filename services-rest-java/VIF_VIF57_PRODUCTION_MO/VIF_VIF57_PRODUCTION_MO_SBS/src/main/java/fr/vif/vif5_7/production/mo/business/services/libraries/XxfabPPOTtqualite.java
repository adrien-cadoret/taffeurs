/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPOTtqualite.java,v $
 * Created on 24 mars 2016 by presta15
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.io.Serializable;
import java.util.Date;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:tempxxfabpxy.p.xml.
 * 
 * @author presta15
 */
public class XxfabPPOTtqualite extends AbstractTempTable implements Serializable {


    private String ccri = "";

    private String cetab = "";

    private String csoc = "";

    private String valcmax = "";

    private String valcmin = "";

    private Date valdmax = null;

    private Date valdmin = null;

    private double valnmax = 0;

    private double valnmin = 0;


    /**
     * Default Constructor.
     *
     */
    public XxfabPPOTtqualite() {
        super();
    }

    /**
     * Gets the ccri.
     *
     * @category getter
     * @return the ccri.
     */
    public final String getCcri() {
        return ccri;
    }

    /**
     * Sets the ccri.
     *
     * @category setter
     * @param ccri ccri.
     */
    public final void setCcri(final String ccri) {
        this.ccri = ccri;
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the valcmax.
     *
     * @category getter
     * @return the valcmax.
     */
    public final String getValcmax() {
        return valcmax;
    }

    /**
     * Sets the valcmax.
     *
     * @category setter
     * @param valcmax valcmax.
     */
    public final void setValcmax(final String valcmax) {
        this.valcmax = valcmax;
    }

    /**
     * Gets the valcmin.
     *
     * @category getter
     * @return the valcmin.
     */
    public final String getValcmin() {
        return valcmin;
    }

    /**
     * Sets the valcmin.
     *
     * @category setter
     * @param valcmin valcmin.
     */
    public final void setValcmin(final String valcmin) {
        this.valcmin = valcmin;
    }

    /**
     * Gets the valdmax.
     *
     * @category getter
     * @return the valdmax.
     */
    public final Date getValdmax() {
        return valdmax;
    }

    /**
     * Sets the valdmax.
     *
     * @category setter
     * @param valdmax valdmax.
     */
    public final void setValdmax(final Date valdmax) {
        this.valdmax = valdmax;
    }

    /**
     * Gets the valdmin.
     *
     * @category getter
     * @return the valdmin.
     */
    public final Date getValdmin() {
        return valdmin;
    }

    /**
     * Sets the valdmin.
     *
     * @category setter
     * @param valdmin valdmin.
     */
    public final void setValdmin(final Date valdmin) {
        this.valdmin = valdmin;
    }

    /**
     * Gets the valnmax.
     *
     * @category getter
     * @return the valnmax.
     */
    public final double getValnmax() {
        return valnmax;
    }

    /**
     * Sets the valnmax.
     *
     * @category setter
     * @param valnmax valnmax.
     */
    public final void setValnmax(final double valnmax) {
        this.valnmax = valnmax;
    }

    /**
     * Gets the valnmin.
     *
     * @category getter
     * @return the valnmin.
     */
    public final double getValnmin() {
        return valnmin;
    }

    /**
     * Sets the valnmin.
     *
     * @category setter
     * @param valnmin valnmin.
     */
    public final void setValnmin(final double valnmin) {
        this.valnmin = valnmin;
    }

}

/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabrecPPO.java,v $
 * Created on 16 mai 2014 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.boning;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.Parameter;
import com.progress.open4gl.ProResultSet;
import com.progress.open4gl.ProResultSetMetaDataImpl;
import com.progress.open4gl.ProSQLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.DoubleHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.ProInputTempTable;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.business.progress.TempTableHolder;


/**
 * Generated class from xxfabrecpxy.p.
 * 
 * @author cj
 */
public class XxfabrecPPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger             LOGGER                = Logger.getLogger(XxfabrecPPO.class);

    /**
     * ProResultSetMetaDataImpl for the temp-table ttCrivPROXY.
     */
    private static ProResultSetMetaDataImpl metadatattCrivPROXY   = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttLocaldocprg.
     */
    private static ProResultSetMetaDataImpl metadatattLocaldocprg = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttXetiq.
     */
    private static ProResultSetMetaDataImpl metadatattXetiq       = null;

    /**
     * Context.
     */
    private VIFContext                      ctx;

    /**
     * Default Constructor.
     *
     * @param ctx Database Context.
     */
    public XxfabrecPPO(final VIFContext ctx) {
        super();
        this.ctx = ctx;
    }

    /**
     * Generated Class prEditeEtiquettesEntreeSortie.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @param iPrechrof String INPUT
     * @param iChronof int INPUT
     * @param iCart String INPUT
     * @param iRowidMvt String INPUT
     * @param iTypacta String INPUT
     * @param ttXetiq TABLE INPUT
     * @param ttLocaldocprgHolder TABLE OUTPUT
     * @param oTret boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prEditeEtiquettesEntreeSortie(final String iCsoc, final String iCetab, final String iCposte,
            final String iCuser, final String iPrechrof, final int iChronof, final String iCart,
            final String iRowidMvt, final String iTypacta, final List<XxfabrecPPOTtXetiq> ttXetiq,
            final TempTableHolder<XxfabrecPPOTtLocaldocprg> ttLocaldocprgHolder, final BooleanHolder oTret,
            final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocprgHolder == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prEditeEtiquettesEntreeSortie values :" + "ttLocaldocprgHolder = "
                        + ttLocaldocprgHolder + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prEditeEtiquettesEntreeSortie " + " iCsoc = " + iCsoc + " iCetab = " + iCetab
                    + " iCposte = " + iCposte + " iCuser = " + iCuser + " iPrechrof = " + iPrechrof + " iChronof = "
                    + iChronof + " iCart = " + iCart + " iRowidMvt = " + iRowidMvt + " iTypacta = " + iTypacta
                    + " ttXetiq = " + ttXetiq + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oTret = " + oTret
                    + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(13);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addCharacter(6, iCart, ParamArrayMode.INPUT);
            params.addCharacter(7, iRowidMvt, ParamArrayMode.INPUT);
            params.addCharacter(8, iTypacta, ParamArrayMode.INPUT);
            // ------------------------------------------
            // Make the temp-table ttXetiq
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttXetiq");
            }
            params.addTable(9, convertTempTablettXetiq(ttXetiq), ParamArrayMode.INPUT, getMetaDatattXetiq());
            params.addTable(10, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addLogical(11, null, ParamArrayMode.OUTPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Edite-Etiquettes-Entree-Sortie", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttLocaldocprg

            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params, 10));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(11));
            oMsg.setStringValue((String) params.getOutputParameter(12));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGetOutputParametersBoning.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iCposte String INPUT
     * @param iCart String INPUT
     * @param oCpfsor String OUTPUT
     * @param oEntityType int OUTPUT
     * @param oCCond String OUTPUT
     * @param oCcondp String OUTPUT
     * @param oTypFerm int OUTPUT
     * @param oTypFermCapa int OUTPUT
     * @param oTypFermp int OUTPUT
     * @param oTypFermpCapa int OUTPUT
     * @param oQte1 double OUTPUT
     * @param oCu1 String OUTPUT
     * @param oQte2 double OUTPUT
     * @param oCu2 String OUTPUT
     * @param oCdepot String OUTPUT
     * @param oCemp String OUTPUT
     * @param oBatch String OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGetOutputParametersBoning(final String iCsoc, final String iCetab, final String iCposte,
            final String iCart, final StringHolder oCpfsor, final IntegerHolder oEntityType, final StringHolder oCCond,
            final StringHolder oCcondp, final IntegerHolder oTypFerm, final IntegerHolder oTypFermCapa,
            final IntegerHolder oTypFermp, final IntegerHolder oTypFermpCapa, final DoubleHolder oQte1,
            final StringHolder oCu1, final DoubleHolder oQte2, final StringHolder oCu2, final StringHolder oCdepot,
            final StringHolder oCemp, final StringHolder oBatch, final BooleanHolder oRet, final StringHolder oMsg)
            throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCpfsor == null || oEntityType == null || oCCond == null || oCcondp == null || oTypFerm == null
                || oTypFermCapa == null || oTypFermp == null || oTypFermpCapa == null || oQte1 == null || oCu1 == null
                || oQte2 == null || oCu2 == null || oCdepot == null || oCemp == null || oBatch == null || oRet == null
                || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prGetOutputParametersBoning values :" + "oCpfsor = " + oCpfsor
                        + "oEntityType = " + oEntityType + "oCCond = " + oCCond + "oCcondp = " + oCcondp
                        + "oTypFerm = " + oTypFerm + "oTypFermCapa = " + oTypFermCapa + "oTypFermp = " + oTypFermp
                        + "oTypFermpCapa = " + oTypFermpCapa + "oQte1 = " + oQte1 + "oCu1 = " + oCu1 + "oQte2 = "
                        + oQte2 + "oCu2 = " + oCu2 + "oCdepot = " + oCdepot + "oCemp = " + oCemp + "oBatch = " + oBatch
                        + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGetOutputParametersBoning " + " iCsoc = " + iCsoc + " iCetab = " + iCetab
                    + " iCposte = " + iCposte + " iCart = " + iCart + " oCpfsor = " + oCpfsor + " oEntityType = "
                    + oEntityType + " oCCond = " + oCCond + " oCcondp = " + oCcondp + " oTypFerm = " + oTypFerm
                    + " oTypFermCapa = " + oTypFermCapa + " oTypFermp = " + oTypFermp + " oTypFermpCapa = "
                    + oTypFermpCapa + " oQte1 = " + oQte1 + " oCu1 = " + oCu1 + " oQte2 = " + oQte2 + " oCu2 = " + oCu2
                    + " oCdepot = " + oCdepot + " oCemp = " + oCemp + " oBatch = " + oBatch + " oRet = " + oRet
                    + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(21);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addInteger(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
            params.addInteger(8, null, ParamArrayMode.OUTPUT);
            params.addInteger(9, null, ParamArrayMode.OUTPUT);
            params.addInteger(10, null, ParamArrayMode.OUTPUT);
            params.addInteger(11, null, ParamArrayMode.OUTPUT);
            params.addDecimal(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
            params.addDecimal(14, null, ParamArrayMode.OUTPUT);
            params.addCharacter(15, null, ParamArrayMode.OUTPUT);
            params.addCharacter(16, null, ParamArrayMode.OUTPUT);
            params.addCharacter(17, null, ParamArrayMode.OUTPUT);
            params.addCharacter(18, null, ParamArrayMode.OUTPUT);
            params.addLogical(19, null, ParamArrayMode.OUTPUT);
            params.addCharacter(20, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Get-Output-Parameters-Boning", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oCpfsor.setStringValue((String) params.getOutputParameter(4));
            oEntityType.setIntegerValue((Integer) params.getOutputParameter(5));
            oCCond.setStringValue((String) params.getOutputParameter(6));
            oCcondp.setStringValue((String) params.getOutputParameter(7));
            oTypFerm.setIntegerValue((Integer) params.getOutputParameter(8));
            oTypFermCapa.setIntegerValue((Integer) params.getOutputParameter(9));
            oTypFermp.setIntegerValue((Integer) params.getOutputParameter(10));
            oTypFermpCapa.setIntegerValue((Integer) params.getOutputParameter(11));
            oQte1.setDoubleValue(((BigDecimal) params.getOutputParameter(12)).doubleValue());
            oCu1.setStringValue((String) params.getOutputParameter(13));
            oQte2.setDoubleValue(((BigDecimal) params.getOutputParameter(14)).doubleValue());
            oCu2.setStringValue((String) params.getOutputParameter(15));
            oCdepot.setStringValue((String) params.getOutputParameter(16));
            oCemp.setStringValue((String) params.getOutputParameter(17));
            oBatch.setStringValue((String) params.getOutputParameter(18));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(19));
            oMsg.setStringValue((String) params.getOutputParameter(20));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGetSalesCriteria.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iNi1 int INPUT
     * @param ttCrivPROXYHolder TABLE OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGetSalesCriteria(final String iCsoc, final String iCetab, final String iPrechro, final int iChrono,
            final int iNi1, final TempTableHolder<XxfabrecPPOTtcrivproxy> ttCrivPROXYHolder, final BooleanHolder oRet,
            final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttCrivPROXYHolder == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prGetSalesCriteria values :" + "ttCrivPROXYHolder = " + ttCrivPROXYHolder
                        + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGetSalesCriteria " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = "
                    + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1 + " ttCrivPROXYHolder = "
                    + ttCrivPROXYHolder + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(8);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addTable(5, null, ParamArrayMode.OUTPUT, getMetaDatattCrivPROXY());
            params.addLogical(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Get-Sales-Criteria", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttCrivPROXY

            ttCrivPROXYHolder.setListValue(convertRecordttCrivPROXY(params, 5));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(6));
            oMsg.setStringValue((String) params.getOutputParameter(7));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prListeEtiquette.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iCposte String INPUT
     * @param ttXetiqHolder TABLE OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prListeEtiquette(final String iCsoc, final String iCetab, final String iCposte,
            final TempTableHolder<XxfabrecPPOTtXetiq> ttXetiqHolder, final BooleanHolder oRet, final StringHolder oMsg)
            throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttXetiqHolder == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prListeEtiquette values :" + "ttXetiqHolder = " + ttXetiqHolder + "oRet = "
                        + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prListeEtiquette " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = "
                    + iCposte + " ttXetiqHolder = " + ttXetiqHolder + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(6);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addTable(3, null, ParamArrayMode.OUTPUT, getMetaDatattXetiq());
            params.addLogical(4, null, ParamArrayMode.OUTPUT);
            params.addCharacter(5, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Liste-Etiquette", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttXetiq

            ttXetiqHolder.setListValue(convertRecordttXetiq(params, 3));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(4));
            oMsg.setStringValue((String) params.getOutputParameter(5));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prMajNotat.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iNsc String INPUT
     * @param iCcond String INPUT
     * @param iNoTat String INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prMajNotat(final String iCsoc, final String iCetab, final String iPrechro, final int iChrono,
            final String iNsc, final String iCcond, final String iNoTat, final String iCposte, final String iCuser,
            final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prMajNotat values :" + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prMajNotat " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = "
                    + iPrechro + " iChrono = " + iChrono + " iNsc = " + iNsc + " iCcond = " + iCcond + " iNoTat = "
                    + iNoTat + " iCposte = " + iCposte + " iCuser = " + iCuser + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(11);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addCharacter(4, iNsc, ParamArrayMode.INPUT);
            params.addCharacter(5, iCcond, ParamArrayMode.INPUT);
            params.addCharacter(6, iNoTat, ParamArrayMode.INPUT);
            params.addCharacter(7, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(8, iCuser, ParamArrayMode.INPUT);
            params.addLogical(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Maj-NoTat", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oRet.setBooleanValue((Boolean) params.getOutputParameter(9));
            oMsg.setStringValue((String) params.getOutputParameter(10));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prPrintFichePere.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iNsc String INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prPrintFichePere(final String iCsoc, final String iCetab, final String iNsc, final String iCposte,
            final String iCuser) throws ProgressBusinessException {
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prPrintFichePere " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iNsc = "
                    + iNsc + " iCposte = " + iCposte + " iCuser = " + iCuser);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iNsc, ParamArrayMode.INPUT);
            params.addCharacter(3, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(4, iCuser, ParamArrayMode.INPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Print-Fiche-Pere", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prPrintFicheRoll.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iNsc String INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prPrintFicheRoll(final String iCsoc, final String iCetab, final String iNsc, final String iCposte,
            final String iCuser) throws ProgressBusinessException {
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prPrintFicheRoll " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iNsc = "
                    + iNsc + " iCposte = " + iCposte + " iCuser = " + iCuser);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iNsc, ParamArrayMode.INPUT);
            params.addCharacter(3, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(4, iCuser, ParamArrayMode.INPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Print-Fiche-Roll", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prPrintMultiEtiquetteActeEntrant.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @param iCpf String INPUT
     * @param iRowId String INPUT
     * @param ttLocaldocprgHolder TABLE OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prPrintMultiEtiquetteActeEntrant(final String iCsoc, final String iCetab, final String iPrechro,
            final int iChrono, final String iCposte, final String iCuser, final String iCpf, final String iRowId,
            final TempTableHolder<XxfabrecPPOTtLocaldocprg> ttLocaldocprgHolder, final BooleanHolder oRet,
            final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocprgHolder == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prPrintMultiEtiquetteActeEntrant values :" + "ttLocaldocprgHolder = "
                        + ttLocaldocprgHolder + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prPrintMultiEtiquetteActeEntrant " + " iCsoc = " + iCsoc + " iCetab = " + iCetab
                    + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iCposte = " + iCposte + " iCuser = "
                    + iCuser + " iCpf = " + iCpf + " iRowId = " + iRowId + " ttLocaldocprgHolder = "
                    + ttLocaldocprgHolder + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(11);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addCharacter(4, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(5, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(6, iCpf, ParamArrayMode.INPUT);
            params.addCharacter(7, iRowId, ParamArrayMode.INPUT);
            params.addTable(8, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addLogical(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Print-Multi-Etiquette-Acte-Entrant", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttLocaldocprg

            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params, 8));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(9));
            oMsg.setStringValue((String) params.getOutputParameter(10));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prRechercheEditeMultiEtiquetteActe2.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @param iTypacta String INPUT
     * @param iCevtDoc String INPUT
     * @param iCpf String INPUT
     * @param iRowId String INPUT
     * @param ttLocaldocprgHolder TABLE OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prRechercheEditeMultiEtiquetteActe2(final String iCsoc, final String iCetab, final String iPrechro,
            final int iChrono, final String iCposte, final String iCuser, final String iTypacta, final String iCevtDoc,
            final String iCpf, final String iRowId,
            final TempTableHolder<XxfabrecPPOTtLocaldocprg> ttLocaldocprgHolder, final BooleanHolder oRet,
            final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocprgHolder == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prRechercheEditeMultiEtiquetteActe2 values :" + "ttLocaldocprgHolder = "
                        + ttLocaldocprgHolder + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prRechercheEditeMultiEtiquetteActe2 " + " iCsoc = " + iCsoc + " iCetab = " + iCetab
                    + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iCposte = " + iCposte + " iCuser = "
                    + iCuser + " iTypacta = " + iTypacta + " iCevtDoc = " + iCevtDoc + " iCpf = " + iCpf + " iRowId = "
                    + iRowId + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(13);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addCharacter(4, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(5, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(6, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(7, iCevtDoc, ParamArrayMode.INPUT);
            params.addCharacter(8, iCpf, ParamArrayMode.INPUT);
            params.addCharacter(9, iRowId, ParamArrayMode.INPUT);
            params.addTable(10, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addLogical(11, null, ParamArrayMode.OUTPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Recherche-Edite-Multi-Etiquette-Acte2", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttLocaldocprg

            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params, 10));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(11));
            oMsg.setStringValue((String) params.getOutputParameter(12));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prValidateMvtSortieReco.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @param iTypgest String INPUT
     * @param iPrechrof String INPUT
     * @param iChronof int INPUT
     * @param iNi1 int INPUT
     * @param iNi2 int INPUT
     * @param iNi3 int INPUT
     * @param iNi4 int INPUT
     * @param iTypacta String INPUT
     * @param iCcond String INPUT
     * @param iNotat String INPUT
     * @param iTare double INPUT
     * @param iCunitar String INPUT
     * @param iCcondp String INPUT
     * @param iNotatp String INPUT
     * @param iTarep double INPUT
     * @param iCunitarp String INPUT
     * @param iCart String INPUT
     * @param iLot String INPUT
     * @param iCdepot String INPUT
     * @param iCemp String INPUT
     * @param iCmotmvk String INPUT
     * @param iQte1 double INPUT
     * @param iCu1 String INPUT
     * @param iQte2 double INPUT
     * @param iCu2 String INPUT
     * @param iQte3 double INPUT
     * @param iCu3 String INPUT
     * @param iTypFerm int INPUT
     * @param iTypFermCapa int INPUT
     * @param iTypFermp int INPUT
     * @param iTypFermpCapa int INPUT
     * @param iGestNsp boolean INPUT
     * @param iTypTare int INPUT
     * @param ttXetiq TABLE INPUT
     * @param ioNsc String INPUT_OUTPUT
     * @param ioNscp String INPUT_OUTPUT
     * @param ttLocaldocprgHolder TABLE OUTPUT
     * @param oRowidMvtStr String OUTPUT
     * @param oNscClosed boolean OUTPUT
     * @param oNscpClosed boolean OUTPUT
     * @param oNlig int OUTPUT
     * @param oTret boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prValidateMvtSortieReco(final String iCsoc, final String iCetab, final String iCposte,
            final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1,
            final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final String iCcond,
            final String iNotat, final double iTare, final String iCunitar, final String iCcondp, final String iNotatp,
            final double iTarep, final String iCunitarp, final String iCart, final String iLot, final String iCdepot,
            final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2,
            final String iCu2, final double iQte3, final String iCu3, final int iTypFerm, final int iTypFermCapa,
            final int iTypFermp, final int iTypFermpCapa, final boolean iGestNsp, final int iTypTare,
            final List<XxfabrecPPOTtXetiq> ttXetiq, final StringHolder ioNsc, final StringHolder ioNscp,
            final TempTableHolder<XxfabrecPPOTtLocaldocprg> ttLocaldocprgHolder, final StringHolder oRowidMvtStr,
            final BooleanHolder oNscClosed, final BooleanHolder oNscpClosed, final IntegerHolder oNlig,
            final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ioNsc == null || ioNscp == null || ttLocaldocprgHolder == null || oRowidMvtStr == null
                || oNscClosed == null || oNscpClosed == null || oNlig == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prValidateMvtSortieReco values :" + "ioNsc = " + ioNsc + "ioNscp = " + ioNscp
                        + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oRowidMvtStr = " + oRowidMvtStr
                        + "oNscClosed = " + oNscClosed + "oNscpClosed = " + oNscpClosed + "oNlig = " + oNlig
                        + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prValidateMvtSortieReco " + " iCsoc = " + iCsoc + " iCetab = " + iCetab
                    + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = "
                    + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3
                    + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " iCcond = " + iCcond + " iNotat = " + iNotat
                    + " iTare = " + iTare + " iCunitar = " + iCunitar + " iCcondp = " + iCcondp + " iNotatp = "
                    + iNotatp + " iTarep = " + iTarep + " iCunitarp = " + iCunitarp + " iCart = " + iCart + " iLot = "
                    + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = "
                    + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3
                    + " iCu3 = " + iCu3 + " iTypFerm = " + iTypFerm + " iTypFermCapa = " + iTypFermCapa
                    + " iTypFermp = " + iTypFermp + " iTypFermpCapa = " + iTypFermpCapa + " iGestNsp = " + iGestNsp
                    + " iTypTare = " + iTypTare + " ttXetiq = " + ttXetiq + " ioNsc = " + ioNsc + " ioNscp = " + ioNscp
                    + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRowidMvtStr = " + oRowidMvtStr
                    + " oNscClosed = " + oNscClosed + " oNscpClosed = " + oNscpClosed + " oNlig = " + oNlig
                    + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(47);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, iCcond, ParamArrayMode.INPUT);
            params.addCharacter(13, iNotat, ParamArrayMode.INPUT);
            params.addDecimal(14, new BigDecimal(iTare).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(15, iCunitar, ParamArrayMode.INPUT);
            params.addCharacter(16, iCcondp, ParamArrayMode.INPUT);
            params.addCharacter(17, iNotatp, ParamArrayMode.INPUT);
            params.addDecimal(18, new BigDecimal(iTarep).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(19, iCunitarp, ParamArrayMode.INPUT);
            params.addCharacter(20, iCart, ParamArrayMode.INPUT);
            params.addCharacter(21, iLot, ParamArrayMode.INPUT);
            params.addCharacter(22, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(23, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(24, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(25, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(26, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(27, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(28, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(29, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(30, iCu3, ParamArrayMode.INPUT);
            params.addInteger(31, new Integer(iTypFerm), ParamArrayMode.INPUT);
            params.addInteger(32, new Integer(iTypFermCapa), ParamArrayMode.INPUT);
            params.addInteger(33, new Integer(iTypFermp), ParamArrayMode.INPUT);
            params.addInteger(34, new Integer(iTypFermpCapa), ParamArrayMode.INPUT);
            params.addLogical(35, new Boolean(iGestNsp), ParamArrayMode.INPUT);
            params.addInteger(36, new Integer(iTypTare), ParamArrayMode.INPUT);
            // ------------------------------------------
            // Make the temp-table ttXetiq
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttXetiq");
            }
            params.addTable(37, convertTempTablettXetiq(ttXetiq), ParamArrayMode.INPUT, getMetaDatattXetiq());
            params.addCharacter(38, ioNsc.getStringValue(), ParamArrayMode.INPUT_OUTPUT);
            params.addCharacter(39, ioNscp.getStringValue(), ParamArrayMode.INPUT_OUTPUT);
            params.addTable(40, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addCharacter(41, null, ParamArrayMode.OUTPUT);
            params.addLogical(42, null, ParamArrayMode.OUTPUT);
            params.addLogical(43, null, ParamArrayMode.OUTPUT);
            params.addInteger(44, null, ParamArrayMode.OUTPUT);
            params.addLogical(45, null, ParamArrayMode.OUTPUT);
            params.addCharacter(46, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Validate-Mvt-Sortie-Reco", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            ioNsc.setStringValue((String) params.getOutputParameter(38));
            ioNscp.setStringValue((String) params.getOutputParameter(39));
            // Make the temp-table ttLocaldocprg

            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params, 40));
            oRowidMvtStr.setStringValue((String) params.getOutputParameter(41));
            oNscClosed.setBooleanValue((Boolean) params.getOutputParameter(42));
            oNscpClosed.setBooleanValue((Boolean) params.getOutputParameter(43));
            oNlig.setIntegerValue((Integer) params.getOutputParameter(44));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(45));
            oMsg.setStringValue((String) params.getOutputParameter(46));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prValidateMvtSortieReco2.
     *
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @param iTypgest String INPUT
     * @param iPrechrof String INPUT
     * @param iChronof int INPUT
     * @param iNi1 int INPUT
     * @param iNi2 int INPUT
     * @param iNi3 int INPUT
     * @param iNi4 int INPUT
     * @param iTypacta String INPUT
     * @param iCcond String INPUT
     * @param iNotat String INPUT
     * @param iTare double INPUT
     * @param iCunitar String INPUT
     * @param iCcondp String INPUT
     * @param iNotatp String INPUT
     * @param iTarep double INPUT
     * @param iCunitarp String INPUT
     * @param iCart String INPUT
     * @param iLot String INPUT
     * @param iCdepot String INPUT
     * @param iCemp String INPUT
     * @param iCmotmvk String INPUT
     * @param iQte1 double INPUT
     * @param iCu1 String INPUT
     * @param iQte2 double INPUT
     * @param iCu2 String INPUT
     * @param iQte3 double INPUT
     * @param iCu3 String INPUT
     * @param iTypFerm int INPUT
     * @param iTypFermCapa int INPUT
     * @param iTypFermp int INPUT
     * @param iTypFermpCapa int INPUT
     * @param iGestNsp boolean INPUT
     * @param iCplanr String INPUT
     * @param iNoLogm int INPUT
     * @param iCsubdiv String INPUT
     * @param iTypTare int INPUT
     * @param ttXetiq TABLE INPUT
     * @param ioNsc String INPUT_OUTPUT
     * @param ioNscp String INPUT_OUTPUT
     * @param ttLocaldocprgHolder TABLE OUTPUT
     * @param oRowidMvtStr String OUTPUT
     * @param oNscClosed boolean OUTPUT
     * @param oNscpClosed boolean OUTPUT
     * @param oNlig int OUTPUT
     * @param oTret boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prValidateMvtSortieReco2(final String iCsoc, final String iCetab, final String iCposte,
            final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1,
            final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final String iCcond,
            final String iNotat, final double iTare, final String iCunitar, final String iCcondp, final String iNotatp,
            final double iTarep, final String iCunitarp, final String iCart, final String iLot, final String iCdepot,
            final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2,
            final String iCu2, final double iQte3, final String iCu3, final int iTypFerm, final int iTypFermCapa,
            final int iTypFermp, final int iTypFermpCapa, final boolean iGestNsp, final String iCplanr,
            final int iNoLogm, final String iCsubdiv, final int iTypTare, final List<XxfabrecPPOTtXetiq> ttXetiq,
            final StringHolder ioNsc, final StringHolder ioNscp,
            final TempTableHolder<XxfabrecPPOTtLocaldocprg> ttLocaldocprgHolder, final StringHolder oRowidMvtStr,
            final BooleanHolder oNscClosed, final BooleanHolder oNscpClosed, final IntegerHolder oNlig,
            final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ioNsc == null || ioNscp == null || ttLocaldocprgHolder == null || oRowidMvtStr == null
                || oNscClosed == null || oNscpClosed == null || oNlig == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prValidateMvtSortieReco2 values :" + "ioNsc = " + ioNsc + "ioNscp = "
                        + ioNscp + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oRowidMvtStr = " + oRowidMvtStr
                        + "oNscClosed = " + oNscClosed + "oNscpClosed = " + oNscpClosed + "oNlig = " + oNlig
                        + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prValidateMvtSortieReco2 " + " iCsoc = " + iCsoc + " iCetab = " + iCetab
                    + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = "
                    + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3
                    + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " iCcond = " + iCcond + " iNotat = " + iNotat
                    + " iTare = " + iTare + " iCunitar = " + iCunitar + " iCcondp = " + iCcondp + " iNotatp = "
                    + iNotatp + " iTarep = " + iTarep + " iCunitarp = " + iCunitarp + " iCart = " + iCart + " iLot = "
                    + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = "
                    + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3
                    + " iCu3 = " + iCu3 + " iTypFerm = " + iTypFerm + " iTypFermCapa = " + iTypFermCapa
                    + " iTypFermp = " + iTypFermp + " iTypFermpCapa = " + iTypFermpCapa + " iGestNsp = " + iGestNsp
                    + " iCplanr = " + iCplanr + " iNoLogm = " + iNoLogm + " iCsubdiv = " + iCsubdiv + " iTypTare = "
                    + iTypTare + " ttXetiq = " + ttXetiq + " ioNsc = " + ioNsc + " ioNscp = " + ioNscp
                    + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRowidMvtStr = " + oRowidMvtStr
                    + " oNscClosed = " + oNscClosed + " oNscpClosed = " + oNscpClosed + " oNlig = " + oNlig
                    + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(50);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, iCcond, ParamArrayMode.INPUT);
            params.addCharacter(13, iNotat, ParamArrayMode.INPUT);
            params.addDecimal(14, new BigDecimal(iTare).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(15, iCunitar, ParamArrayMode.INPUT);
            params.addCharacter(16, iCcondp, ParamArrayMode.INPUT);
            params.addCharacter(17, iNotatp, ParamArrayMode.INPUT);
            params.addDecimal(18, new BigDecimal(iTarep).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(19, iCunitarp, ParamArrayMode.INPUT);
            params.addCharacter(20, iCart, ParamArrayMode.INPUT);
            params.addCharacter(21, iLot, ParamArrayMode.INPUT);
            params.addCharacter(22, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(23, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(24, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(25, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(26, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(27, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(28, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(29, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(30, iCu3, ParamArrayMode.INPUT);
            params.addInteger(31, new Integer(iTypFerm), ParamArrayMode.INPUT);
            params.addInteger(32, new Integer(iTypFermCapa), ParamArrayMode.INPUT);
            params.addInteger(33, new Integer(iTypFermp), ParamArrayMode.INPUT);
            params.addInteger(34, new Integer(iTypFermpCapa), ParamArrayMode.INPUT);
            params.addLogical(35, new Boolean(iGestNsp), ParamArrayMode.INPUT);
            params.addCharacter(36, iCplanr, ParamArrayMode.INPUT);
            params.addInteger(37, new Integer(iNoLogm), ParamArrayMode.INPUT);
            params.addCharacter(38, iCsubdiv, ParamArrayMode.INPUT);
            params.addInteger(39, new Integer(iTypTare), ParamArrayMode.INPUT);
            // ------------------------------------------
            // Make the temp-table ttXetiq
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttXetiq");
            }
            params.addTable(40, convertTempTablettXetiq(ttXetiq), ParamArrayMode.INPUT, getMetaDatattXetiq());
            params.addCharacter(41, ioNsc.getStringValue(), ParamArrayMode.INPUT_OUTPUT);
            params.addCharacter(42, ioNscp.getStringValue(), ParamArrayMode.INPUT_OUTPUT);
            params.addTable(43, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addCharacter(44, null, ParamArrayMode.OUTPUT);
            params.addLogical(45, null, ParamArrayMode.OUTPUT);
            params.addLogical(46, null, ParamArrayMode.OUTPUT);
            params.addInteger(47, null, ParamArrayMode.OUTPUT);
            params.addLogical(48, null, ParamArrayMode.OUTPUT);
            params.addCharacter(49, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Validate-Mvt-Sortie-Reco2", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            ioNsc.setStringValue((String) params.getOutputParameter(41));
            ioNscp.setStringValue((String) params.getOutputParameter(42));
            // Make the temp-table ttLocaldocprg

            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params, 43));
            oRowidMvtStr.setStringValue((String) params.getOutputParameter(44));
            oNscClosed.setBooleanValue((Boolean) params.getOutputParameter(45));
            oNscpClosed.setBooleanValue((Boolean) params.getOutputParameter(46));
            oNlig.setIntegerValue((Integer) params.getOutputParameter(47));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(48));
            oMsg.setStringValue((String) params.getOutputParameter(49));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generate the MetaData of tempTable ttLocaldocprg.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttLocaldocprg.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattLocaldocprg() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttLocaldocprg");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattLocaldocprg == null) {
            metadatattLocaldocprg = new ProResultSetMetaDataImpl(4);
            metadatattLocaldocprg.setFieldMetaData(1, "cdoc", 0, Parameter.PRO_CHARACTER);
            metadatattLocaldocprg.setFieldMetaData(2, "nbdoc", 0, Parameter.PRO_INTEGER);
            metadatattLocaldocprg.setFieldMetaData(3, "event", 0, Parameter.PRO_CHARACTER);
            metadatattLocaldocprg.setFieldMetaData(4, "prgedi", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattLocaldocprg;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabrecPPOTtLocaldocprg&gt; Convert a record set to a list of XxfabrecPPOTtLocaldocprg
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabrecPPOTtLocaldocprg> convertRecordttLocaldocprg(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabrecPPOTtLocaldocprg");
        }

        ProResultSet rs = null;
        List<XxfabrecPPOTtLocaldocprg> list = new ArrayList<XxfabrecPPOTtLocaldocprg>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabrecPPOTtLocaldocprg tempTable = new XxfabrecPPOTtLocaldocprg();
                /* CHECKSTYLE:OFF */
                tempTable.setCdoc(rs.getString("cdoc"));
                tempTable.setNbdoc(rs.getInt("nbdoc"));
                tempTable.setEvent(rs.getString("event"));
                tempTable.setPrgedi(rs.getString("prgedi"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttLocaldocprg List of XxfabrecPPOTtLocaldocprg
     * @return ProInputTempTable Convert a record set to a list of XxfabrecPPOTtLocaldocprg
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettLocaldocprg(final List<XxfabrecPPOTtLocaldocprg> ttLocaldocprg)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabrecPPOTtLocaldocprg");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<XxfabrecPPOTtLocaldocprg> iter = ttLocaldocprg.iterator();
        while (iter.hasNext()) {
            XxfabrecPPOTtLocaldocprg row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCdoc());
            list.add(row.getNbdoc());
            list.add(row.getEvent());
            list.add(row.getPrgedi());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttCrivPROXY.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttCrivPROXY.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattCrivPROXY() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttCrivPROXY");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattCrivPROXY == null) {
            metadatattCrivPROXY = new ProResultSetMetaDataImpl(28);
            metadatattCrivPROXY.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(3, "typcle", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(4, "cle", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(5, "ccri", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(6, "cunite", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(7, "nlig", 0, Parameter.PRO_INTEGER);
            metadatattCrivPROXY.setFieldMetaData(8, "tsaimodif", 0, Parameter.PRO_LOGICAL);
            metadatattCrivPROXY.setFieldMetaData(9, "valdmin", 0, Parameter.PRO_DATE);
            metadatattCrivPROXY.setFieldMetaData(10, "valdmax", 0, Parameter.PRO_DATE);
            metadatattCrivPROXY.setFieldMetaData(11, "valnmin", 0, Parameter.PRO_DECIMAL);
            metadatattCrivPROXY.setFieldMetaData(12, "valnmax", 0, Parameter.PRO_DECIMAL);
            metadatattCrivPROXY.setFieldMetaData(13, "valcmin", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(14, "valcmax", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(15, "libaff", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(16, "msaisie", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(17, "texclus", 0, Parameter.PRO_LOGICAL);
            metadatattCrivPROXY.setFieldMetaData(18, "tsaioblig", 0, Parameter.PRO_LOGICAL);
            metadatattCrivPROXY.setFieldMetaData(19, "corig", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(20, "type", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(21, "tsaizero", 0, Parameter.PRO_LOGICAL);
            metadatattCrivPROXY.setFieldMetaData(22, "lcri", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(23, "lformat", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(24, "refval", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(25, "cstade", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(26, "InfoStade", 0, Parameter.PRO_CHARACTER);
            metadatattCrivPROXY.setFieldMetaData(27, "datem", 0, Parameter.PRO_DATE);
            metadatattCrivPROXY.setFieldMetaData(28, "heurem", 0, Parameter.PRO_INTEGER);
        }
        /* CHECKSTYLE:ON */
        return metadatattCrivPROXY;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabrecPPOTtcrivproxy&gt; Convert a record set to a list of XxfabrecPPOTtcrivproxy
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabrecPPOTtcrivproxy> convertRecordttCrivPROXY(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabrecPPOTtcrivproxy");
        }

        ProResultSet rs = null;
        List<XxfabrecPPOTtcrivproxy> list = new ArrayList<XxfabrecPPOTtcrivproxy>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabrecPPOTtcrivproxy tempTable = new XxfabrecPPOTtcrivproxy();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setTypcle(rs.getString("typcle"));
                tempTable.setCle(rs.getString("cle"));
                tempTable.setCcri(rs.getString("ccri"));
                tempTable.setCunite(rs.getString("cunite"));
                tempTable.setNlig(rs.getInt("nlig"));
                tempTable.setTsaimodif(rs.getBoolean("tsaimodif"));
                GregorianCalendar gvaldmin = rs.getGregorianCalendar("valdmin");
                tempTable.setValdmin(gvaldmin != null ? gvaldmin.getTime() : null);
                GregorianCalendar gvaldmax = rs.getGregorianCalendar("valdmax");
                tempTable.setValdmax(gvaldmax != null ? gvaldmax.getTime() : null);
                tempTable.setValnmin(rs.getDouble("valnmin"));
                tempTable.setValnmax(rs.getDouble("valnmax"));
                tempTable.setValcmin(rs.getString("valcmin"));
                tempTable.setValcmax(rs.getString("valcmax"));
                tempTable.setLibaff(rs.getString("libaff"));
                tempTable.setMsaisie(rs.getString("msaisie"));
                tempTable.setTexclus(rs.getBoolean("texclus"));
                tempTable.setTsaioblig(rs.getBoolean("tsaioblig"));
                tempTable.setCorig(rs.getString("corig"));
                tempTable.setType(rs.getString("type"));
                tempTable.setTsaizero(rs.getBoolean("tsaizero"));
                tempTable.setLcri(rs.getString("lcri"));
                tempTable.setLformat(rs.getString("lformat"));
                tempTable.setRefval(rs.getString("refval"));
                tempTable.setCstade(rs.getString("cstade"));
                tempTable.setInfoStade(rs.getString("InfoStade"));
                GregorianCalendar gdatem = rs.getGregorianCalendar("datem");
                tempTable.setDatem(gdatem != null ? gdatem.getTime() : null);
                tempTable.setHeurem(rs.getInt("heurem"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttCrivPROXY List of XxfabrecPPOTtcrivproxy
     * @return ProInputTempTable Convert a record set to a list of XxfabrecPPOTtcrivproxy
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettCrivPROXY(final List<XxfabrecPPOTtcrivproxy> ttCrivPROXY)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabrecPPOTtcrivproxy");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<XxfabrecPPOTtcrivproxy> iter = ttCrivPROXY.iterator();
        while (iter.hasNext()) {
            XxfabrecPPOTtcrivproxy row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getTypcle());
            list.add(row.getCle());
            list.add(row.getCcri());
            list.add(row.getCunite());
            list.add(row.getNlig());
            list.add(row.getTsaimodif());
            list.add(dateToGreg(row.getValdmin()));
            list.add(dateToGreg(row.getValdmax()));
            list.add(new BigDecimal(row.getValnmin()).setScale(5, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getValnmax()).setScale(5, RoundingMode.HALF_EVEN));
            list.add(row.getValcmin());
            list.add(row.getValcmax());
            list.add(row.getLibaff());
            list.add(row.getMsaisie());
            list.add(row.getTexclus());
            list.add(row.getTsaioblig());
            list.add(row.getCorig());
            list.add(row.getType());
            list.add(row.getTsaizero());
            list.add(row.getLcri());
            list.add(row.getLformat());
            list.add(row.getRefval());
            list.add(row.getCstade());
            list.add(row.getInfoStade());
            list.add(dateToGreg(row.getDatem()));
            list.add(row.getHeurem());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttXetiq.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttXetiq.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattXetiq() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttXetiq");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattXetiq == null) {
            metadatattXetiq = new ProResultSetMetaDataImpl(8);
            metadatattXetiq.setFieldMetaData(1, "cetiq", 0, Parameter.PRO_CHARACTER);
            metadatattXetiq.setFieldMetaData(2, "letiq", 0, Parameter.PRO_CHARACTER);
            metadatattXetiq.setFieldMetaData(3, "prgedi", 0, Parameter.PRO_CHARACTER);
            metadatattXetiq.setFieldMetaData(4, "cposte", 0, Parameter.PRO_CHARACTER);
            metadatattXetiq.setFieldMetaData(5, "cimput", 0, Parameter.PRO_CHARACTER);
            metadatattXetiq.setFieldMetaData(6, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattXetiq.setFieldMetaData(7, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattXetiq.setFieldMetaData(8, "ctypdoc", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattXetiq;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabrecPPOTtXetiq&gt; Convert a record set to a list of XxfabrecPPOTtXetiq
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabrecPPOTtXetiq> convertRecordttXetiq(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabrecPPOTtXetiq");
        }

        ProResultSet rs = null;
        List<XxfabrecPPOTtXetiq> list = new ArrayList<XxfabrecPPOTtXetiq>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabrecPPOTtXetiq tempTable = new XxfabrecPPOTtXetiq();
                /* CHECKSTYLE:OFF */
                tempTable.setCetiq(rs.getString("cetiq"));
                tempTable.setLetiq(rs.getString("letiq"));
                tempTable.setPrgedi(rs.getString("prgedi"));
                tempTable.setCposte(rs.getString("cposte"));
                tempTable.setCimput(rs.getString("cimput"));
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setCtypdoc(rs.getString("ctypdoc"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttXetiq List of XxfabrecPPOTtXetiq
     * @return ProInputTempTable Convert a record set to a list of XxfabrecPPOTtXetiq
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettXetiq(final List<XxfabrecPPOTtXetiq> ttXetiq)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabrecPPOTtXetiq");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<XxfabrecPPOTtXetiq> iter = ttXetiq.iterator();
        while (iter.hasNext()) {
            XxfabrecPPOTtXetiq row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCetiq());
            list.add(row.getLetiq());
            list.add(row.getPrgedi());
            list.add(row.getCposte());
            list.add(row.getCimput());
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getCtypdoc());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "xxfabrecpxy.p";
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxrdtenPPO.java,v $
 * Created on 16 juil. 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.content;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.Parameter;
import com.progress.open4gl.ProResultSet;
import com.progress.open4gl.ProResultSetMetaDataImpl;
import com.progress.open4gl.ProSQLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.DateHolder;
import fr.vif.jtech.business.progress.DoubleHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.ProInputTempTable;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.business.progress.TempTableHolder;


/**
 * Generated class from xxrdtenpxy.p.
 * 
 * @author cj
 */
public class XxrdtenPPO extends AbstractPPO {

    /** LOGGER. */
    private static final Logger             LOGGER          = Logger.getLogger(XxrdtenPPO.class);

    /**
     * ProResultSetMetaDataImpl for the temp-table ttStock.
     */
    private static ProResultSetMetaDataImpl metadatattStock = null;

    /**
     * Context.
     */
    private VIFContext                      ctx;

    /**
     * Default Constructor.
     * 
     * @param ctx Database Context.
     */
    public XxrdtenPPO(final VIFContext ctx) {
        super();
        this.ctx = ctx;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttStock List of XxrdtenPPOTtstock
     * @return ProInputTempTable Convert a record set to a list of XxrdtenPPOTtstock
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettStock(final List<XxrdtenPPOTtstock> ttStock)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Generate a ProInputTempTable from the record set for XxrdtenPPOTtstock");
            }
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<XxrdtenPPOTtstock> iter = ttStock.iterator();
        while (iter.hasNext()) {
            XxrdtenPPOTtstock row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getNOrdre());
            list.add(row.getCArtPrev());
            list.add(row.getCArt());
            list.add(row.getLot());
            list.add(new BigDecimal(row.getQteStk()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCunite());
            list.add(new BigDecimal(row.getValCri()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCuCri());
            list.add(new BigDecimal(row.getQteAfer()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getValCriRes()).setScale(4, RoundingMode.HALF_EVEN));
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttStock.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttStock.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattStock() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Generate the metadata of the temp table ttStock");
            }
        }
        /* CHECKSTYLE:OFF */
        if (metadatattStock == null) {
            metadatattStock = new ProResultSetMetaDataImpl(10);
            metadatattStock.setFieldMetaData(1, "nOrdre", 0, Parameter.PRO_INTEGER);
            metadatattStock.setFieldMetaData(2, "cArtPrev", 0, Parameter.PRO_CHARACTER);
            metadatattStock.setFieldMetaData(3, "cArt", 0, Parameter.PRO_CHARACTER);
            metadatattStock.setFieldMetaData(4, "lot", 0, Parameter.PRO_CHARACTER);
            metadatattStock.setFieldMetaData(5, "qteStk", 0, Parameter.PRO_DECIMAL);
            metadatattStock.setFieldMetaData(6, "cunite", 0, Parameter.PRO_CHARACTER);
            metadatattStock.setFieldMetaData(7, "valCri", 0, Parameter.PRO_DECIMAL);
            metadatattStock.setFieldMetaData(8, "cuCri", 0, Parameter.PRO_CHARACTER);
            metadatattStock.setFieldMetaData(9, "qteAfer", 0, Parameter.PRO_DECIMAL);
            metadatattStock.setFieldMetaData(10, "valCriRes", 0, Parameter.PRO_DECIMAL);
        }
        /* CHECKSTYLE:ON */
        return metadatattStock;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "xxrdtenpxy.p";
    }

    /**
     * Generated Class pListerLots.
     * 
     * @param icSoc String INPUT
     * @param icEtab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iNi1 int INPUT
     * @param icUser String INPUT
     * @param icPoste String INPUT
     * @param icDepot String INPUT
     * @param icEmp String INPUT
     * @param ttStockHolder TABLE OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void pListerLots(final String icSoc, final String icEtab, final String iPrechro, final int iChrono,
            final int iNi1, final String icUser, final String icPoste, final String icDepot, final String icEmp,
            final TempTableHolder<XxrdtenPPOTtstock> ttStockHolder, final BooleanHolder oRet, final StringHolder oMsg)
            throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttStockHolder == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : pListerLots values :" + "ttStockHolder = " + ttStockHolder + "oRet = " + oRet
                        + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("In Method pListerLots " + " icSoc = " + icSoc + " icEtab = " + icEtab + " iPrechro = "
                        + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1 + " icUser = " + icUser
                        + " icPoste = " + icPoste + " icDepot = " + icDepot + " icEmp = " + icEmp + " ttStockHolder = "
                        + ttStockHolder + " oRet = " + oRet + " oMsg = " + oMsg);
            }
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(12);

        try {
            params.addCharacter(0, icSoc, ParamArrayMode.INPUT);
            params.addCharacter(1, icEtab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addCharacter(5, icUser, ParamArrayMode.INPUT);
            params.addCharacter(6, icPoste, ParamArrayMode.INPUT);
            params.addCharacter(7, icDepot, ParamArrayMode.INPUT);
            params.addCharacter(8, icEmp, ParamArrayMode.INPUT);
            params.addTable(9, null, ParamArrayMode.OUTPUT, getMetaDatattStock());
            params.addLogical(10, null, ParamArrayMode.OUTPUT);
            params.addCharacter(11, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "P-Lister-Lots", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttStock

            ttStockHolder.setListValue(convertRecordttStock(params, 9));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(10));
            oMsg.setStringValue((String) params.getOutputParameter(11));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class pProposerLots.
     * 
     * @param icSoc String INPUT
     * @param icEtab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iNi1 int INPUT
     * @param iValTenReel double INPUT
     * @param ttStock TABLE INPUT
     * @param ttStockHolder TABLE OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void pProposerLots(final String icSoc, final String icEtab, final String iPrechro, final int iChrono,
            final int iNi1, final double iValTenReel, final List<XxrdtenPPOTtstock> ttStock,
            final TempTableHolder<XxrdtenPPOTtstock> ttStockHolder, final BooleanHolder oRet, final StringHolder oMsg)
            throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttStockHolder == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : pProposerLots values :" + "ttStockHolder = " + ttStockHolder + "oRet = "
                        + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("In Method pProposerLots " + " icSoc = " + icSoc + " icEtab = " + icEtab + " iPrechro = "
                        + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1 + " iValTenReel = " + iValTenReel
                        + " ttStockHolder = " + ttStockHolder + " oRet = " + oRet + " oMsg = " + oMsg);
            }
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(9);

        try {
            params.addCharacter(0, icSoc, ParamArrayMode.INPUT);
            params.addCharacter(1, icEtab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addDecimal(5, new BigDecimal(iValTenReel).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            // ------------------------------------------
            // Make the temp-table ttStock
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("Make the input temp-table ttStock");
                }
            }
            params.addTable(6, convertTempTablettStock(ttStock), ParamArrayMode.INPUT_OUTPUT, getMetaDatattStock());
            params.addLogical(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "P-Proposer-Lots", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttStock

            ttStockHolder.setListValue(convertRecordttStock(params, 6));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(7));
            oMsg.setStringValue((String) params.getOutputParameter(8));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class pRechercherDonneesOt.
     * 
     * @param icSoc String INPUT
     * @param icEtab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iNi1 int INPUT
     * @param ocCriTen String OUTPUT
     * @param oFormatTen String OUTPUT
     * @param oValTenTheo double OUTPUT
     * @param ocUniteTen String OUTPUT
     * @param oLstArtTen String OUTPUT
     * @param oQteReste double OUTPUT
     * @param ocUnite String OUTPUT
     * @param oDateMvt Date OUTPUT
     * @param oHeureMvt int OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void pRechercherDonneesOt(final String icSoc, final String icEtab, final String iPrechro, final int iChrono,
            final int iNi1, final StringHolder ocCriTen, final StringHolder oFormatTen, final DoubleHolder oValTenTheo,
            final StringHolder ocUniteTen, final StringHolder oLstArtTen, final DoubleHolder oQteReste,
            final StringHolder ocUnite, final DateHolder oDateMvt, final IntegerHolder oHeureMvt)
            throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ocCriTen == null || oFormatTen == null || oValTenTheo == null || ocUniteTen == null || oLstArtTen == null
                || oQteReste == null || ocUnite == null || oDateMvt == null || oHeureMvt == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : pRechercherDonneesOt values :" + "ocCriTen = " + ocCriTen + "oFormatTen = "
                        + oFormatTen + "oValTenTheo = " + oValTenTheo + "ocUniteTen = " + ocUniteTen + "oLstArtTen = "
                        + oLstArtTen + "oQteReste = " + oQteReste + "ocUnite = " + ocUnite + "oDateMvt = " + oDateMvt
                        + "oHeureMvt = " + oHeureMvt);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("In Method pRechercherDonneesOt " + " icSoc = " + icSoc + " icEtab = " + icEtab
                        + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1 + " ocCriTen = "
                        + ocCriTen + " oFormatTen = " + oFormatTen + " oValTenTheo = " + oValTenTheo + " ocUniteTen = "
                        + ocUniteTen + " oLstArtTen = " + oLstArtTen + " oQteReste = " + oQteReste + " ocUnite = "
                        + ocUnite + " oDateMvt = " + oDateMvt + " oHeureMvt = " + oHeureMvt);
            }
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(14);

        try {
            params.addCharacter(0, icSoc, ParamArrayMode.INPUT);
            params.addCharacter(1, icEtab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addCharacter(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
            params.addDecimal(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);
            params.addCharacter(9, null, ParamArrayMode.OUTPUT);
            params.addDecimal(10, null, ParamArrayMode.OUTPUT);
            params.addCharacter(11, null, ParamArrayMode.OUTPUT);
            params.addDate(12, null, ParamArrayMode.OUTPUT);
            params.addInteger(13, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "P-Rechercher-Donnees-OT", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            ocCriTen.setStringValue((String) params.getOutputParameter(5));
            oFormatTen.setStringValue((String) params.getOutputParameter(6));
            oValTenTheo.setDoubleValue(((BigDecimal) params.getOutputParameter(7)).doubleValue());
            ocUniteTen.setStringValue((String) params.getOutputParameter(8));
            oLstArtTen.setStringValue((String) params.getOutputParameter(9));
            oQteReste.setDoubleValue(((BigDecimal) params.getOutputParameter(10)).doubleValue());
            ocUnite.setStringValue((String) params.getOutputParameter(11));
            oDateMvt.setDateValue((params.getOutputParameter(12) == null ? null : ((GregorianCalendar) params
                    .getOutputParameter(12)).getTime()));
            oHeureMvt.setIntegerValue((Integer) params.getOutputParameter(13));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxrdtenPPOTtstock&gt; Convert a record set to a list of XxrdtenPPOTtstock
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxrdtenPPOTtstock> convertRecordttStock(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("Generate a list from the record set for XxrdtenPPOTtstock");
            }
        }

        ProResultSet rs = null;
        List<XxrdtenPPOTtstock> list = new ArrayList<XxrdtenPPOTtstock>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxrdtenPPOTtstock tempTable = new XxrdtenPPOTtstock();
                /* CHECKSTYLE:OFF */
                tempTable.setNOrdre(rs.getInt("nOrdre"));
                tempTable.setCArtPrev(rs.getString("cArtPrev"));
                tempTable.setCArt(rs.getString("cArt"));
                tempTable.setLot(rs.getString("lot"));
                tempTable.setQteStk(rs.getDouble("qteStk"));
                tempTable.setCunite(rs.getString("cunite"));
                tempTable.setValCri(rs.getDouble("valCri"));
                tempTable.setCuCri(rs.getString("cuCri"));
                tempTable.setQteAfer(rs.getDouble("qteAfer"));
                tempTable.setValCriRes(rs.getDouble("valCriRes"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(e.getMessage());
                }
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug(e.getMessage());
                }
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    if (LOGGER.isDebugEnabled()) {
                        LOGGER.debug(e.getMessage());
                    }
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

}

/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MOItemSBSImpl.java,v $
 * Created on 26 nov. 07 by ped
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.operationitem;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProxyBusinessException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.DoubleHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.business.util.validator.VIFBindException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.TimeHelper;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif57.stock.entity.business.services.constant.EntityEnum.Management;
import fr.vif.vif57.stock.entity.business.services.constant.MovementEnum.Sens;
import fr.vif.vif57.stock.entity.business.services.constant.MovementEnum.StockValidation;
import fr.vif.vif57.stock.entity.business.services.libraries.EntitySBS;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.StockItem;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityLocationKey;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.Movement;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementSelection;
import fr.vif.vif5_7.activities.activities.business.services.libraries.activities.ActivitiesSBS;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.admin.config.business.services.libraries.parameter.ParameterSBS;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaCompleteBean;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaReturnInitBean;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemKey;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemProcessKey;
import fr.vif.vif5_7.gen.item.business.beans.common.StockItemInfos;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.item.business.services.libraries.item.ItemSBS;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.location.business.beans.common.warehouse.WareHouseKey;
import fr.vif.vif5_7.gen.location.business.beans.common.warehouse.WareHouseProperties;
import fr.vif.vif5_7.gen.location.business.services.libraries.warehouse.WareHouseSBS;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputItemParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputUpdateReturnBean;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputWorkstationParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputItemParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputBatchType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputContainerCapacityClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputContainerClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputUpperContainerCapacityClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputUpdateReturnBean;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputWorkstationParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.BatchCheckBean;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.ItemMovementBean;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.ItemMovementKey;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.ItemMovementQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.MOFinishedBean;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.MOStockCheckBean;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.RowidKey;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.SearchLabel;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.InputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OutputOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductioninputcontainer.FProductionInputContainerValidatorBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcomp.FProductionOutputCompSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.fproductionoutputcontainer.FProductionOutputContainerValidatorBean;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPO;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtLocaldoc;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtLocaldocprg;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtartremp;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtcontenu;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtlot;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtmotmvk;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtqualite;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtxmvtm;
import fr.vif.vif5_7.production.mo.business.services.libraries.doc.OfliedocPPO;
import fr.vif.vif5_7.production.mo.business.services.libraries.doc.OfliedocPPOTtdocument;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.production.mo.business.services.libraries.moprofile.MoProfileSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MONatureComplementaryWorkshop;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.PrintEvent;
import fr.vif.vif5_7.production.mo.constants.Mnemos.Progress;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ReasonType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.TouchDeclarationType;
import fr.vif.vif5_7.production.mo.constants.ProductionMOParamEnum;
import fr.vif.vif5_7.production.mo.dao.xpiloia.Xpiloia;
import fr.vif.vif5_7.production.mo.dao.xpiloia.XpiloiaDAOFactory;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;


/**
 * Business services implementation for the operation items of manufacturing orders.
 * 
 * @author ped
 */
@Component
public class MOItemSBSImpl implements MOItemSBS {

    private static final String KILOGRAM                                                   = "KG";

    private static final Logger LOGGER                                                     = Logger.getLogger(MOItemSBSImpl.class);

    private static final int    PARAM_FIELD_DECLARATION_BY_ACTIVITY_FAMILY_INIT_LEFT_TO_DO = 1;

    private static final int    PARAM_FIELD_DECLARATION_BY_ITEM_FAMILY_INIT_STK            = 1;

    private static final int    PARAM_FIELD_FABRICATION_INPUT_DESTOCKING                   = 45;

    private static final int    PARAM_FIELD_FABRICATION_INPUT_EXISTINGQUALITY              = 46;

    private static final int    PARAM_FIELD_FABRICATION_INPUT_PROFILE                      = 1;

    private static final int    PARAM_FIELD_FABRICATION_OUTPUT_PROFILE                     = 1;

    private static final String SEPARATOR                                                  = "§";

    /** Parameter OFDEC[26]: Definition of the unit of Scrap item. */
    private static final int    DECLARATION_BY_ACTIVITY_FAMILY_COUNTER_26                  = 26;

    private static final int    MAX_ROW                                                    = 100;

    @Autowired
    private EntitySBS           entitySBS;

    @Autowired
    private ItemSBS             itemSBS;

    @Autowired
    private MoProfileSBS        moProfileSBS;

    @Autowired
    @Qualifier("MOSBSImpl")
    private MOSBS               moSBS;

    @Autowired
    private ActivitiesSBS       activitiesSBS;

    @Autowired
    private ParameterSBS        parameterSBS;

    @Autowired
    private WareHouseSBS        warehouseSBS;

    /**
     * {@inheritDoc}
     */
    @Override
    public BatchCheckBean checkBatch(final VIFContext ctx, final CEntityBean entity,
            final OperationItemKey operationItemKey, final ActivityItemType activityItemType, final String workstationId)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkBatch(ctx=" + ctx + ", entity=" + entity + ", operationItemKey=" + operationItemKey
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        StringHolder oBatch = new StringHolder();
        oBatch.setStringValue(entity.getStockItemBean().getStockItem().getBatch());
        StringHolder oReturnCode = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCtrlDelivLot(entity.getEntityLocationBean().getEntityLocationKey().getCsoc(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCetab(), //
                workstationId, //
                entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCdepot(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCemp(), operationItemKey.getChrono()
                        .getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                activityItemType.getValue(), oBatch, oReturnCode);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkBatch(ctx=" + ctx + ", entity=" + entity + ", operationItemKey=" + operationItemKey
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        return new BatchCheckBean(oBatch.getStringValue(), oReturnCode.getStringValue());
    }

    /**
     * {@inheritDoc}
     */
    public BatchCheckBean checkBatch(final VIFContext ctx, final ItemMovementBean itemMovementBean,
            final ActivityItemType activityItemType, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkBatch(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean + ", activityItemType="
                    + activityItemType + ", workstationId=" + workstationId + ")");
        }
        StringHolder oBatch = new StringHolder();
        oBatch.setStringValue(itemMovementBean.getBatch());
        StringHolder oReturnCode = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCtrlDelivLot(itemMovementBean.getItemMovementKey().getEstablishmentKey().getCsoc(), itemMovementBean
                .getItemMovementKey().getEstablishmentKey().getCetab(), workstationId, itemMovementBean.getItemCL()
                .getCode(), itemMovementBean.getDepotCL().getCode(), itemMovementBean.getLocationCL().getCode(),
                itemMovementBean.getItemMovementKey().getChrono().getPrechro(), itemMovementBean.getItemMovementKey()
                        .getChrono().getChrono(), activityItemType.getValue(), oBatch, oReturnCode);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkBatch(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean + ", activityItemType="
                    + activityItemType + ", workstationId=" + workstationId + ")");
        }
        return new BatchCheckBean(oBatch.getStringValue(), oReturnCode.getStringValue());
    }

    /**
     * {@inheritDoc}
     */
    public void checkDepositoryClosing(final VIFContext ctx, final ItemMovementBean itemMovementBean,
            final ActivityItemType activityItemType, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkDepositoryClosing(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCtrlDepotCloture(itemMovementBean.getItemMovementKey().getEstablishmentKey().getCsoc(),
                itemMovementBean.getItemMovementKey().getEstablishmentKey().getCetab(), itemMovementBean
                        .getItemMovementKey().getChrono().getPrechro(), itemMovementBean.getItemMovementKey()
                        .getChrono().getChrono(), itemMovementBean.getItemCL().getCode(), itemMovementBean.getDepotCL()
                        .getCode(), activityItemType.getValue(), workstationId, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkDepositoryClosing(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void checkDowngradedItem(final VIFContext ctx, final AbstractOperationItemBean operationItemBean,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkDowngradedItem(ctx=" + ctx + ", operationItemBean=" + operationItemBean
                    + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCtrlArtDec(operationItemBean.getOperationItemKey().getEstablishmentKey().getCsoc(),
                operationItemBean.getOperationItemKey().getEstablishmentKey().getCetab(), workstationId, ctx.getIdCtx()
                        .getLogin(), operationItemBean.getOperationItemKey().getManagementType().getValue(),
                operationItemBean.getOperationItemKey().getChrono().getPrechro(), operationItemBean
                        .getOperationItemKey().getChrono().getChrono(), operationItemBean.getOperationItemKey()
                        .getCounter1(), operationItemBean.getOperationItemKey().getCounter2(), operationItemBean
                        .getOperationItemKey().getCounter3(), operationItemBean.getOperationItemKey().getCounter4(),
                operationItemBean.getItemCL().getCode(), operationItemBean.getActivityItemType().getValue(), oRet,
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkDowngradedItem(ctx=" + ctx + ", operationItemBean=" + operationItemBean
                    + ", workstationId=" + workstationId + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FProductionInputContainerValidatorBean checkInputAll(final VIFContext ctx, final CEntityBean entity,
            final OperationItemKey operationItemKey, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkInputAll(ctx=" + ctx + ", entity=" + entity + ", operationItemKey="
                    + operationItemKey + ", workstationId=" + workstationId + ")");
        }
        FProductionInputContainerValidatorBean validatorBean = new FProductionInputContainerValidatorBean();
        BooleanHolder oTRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        BooleanHolder oTLigTer = new BooleanHolder();
        BooleanHolder oTCtrlStk = new BooleanHolder();
        StringHolder oQuestionLigTer = new StringHolder();
        StringHolder oQuestionSplit = new StringHolder();
        StringHolder oQuestionDestocking = new StringHolder();
        StringHolder oLstQuestionCtrlStk = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCtrlEntree(entity.getEntityLocationBean().getEntityLocationKey().getCsoc(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCetab(), workstationId, //
                ctx.getIdCtx().getLogin(), //
                operationItemKey.getManagementType().getValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), //
                operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4(), //
                entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode(), //
                entity.getStockItemBean().getStockItem().getBatch(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCdepot(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCemp(), //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit(), //
                entity.getEntityLocationBean().getEntityLocationKey().getContainerNumber(), //
                ActivityItemType.INPUT.getValue(), //
                "", //
                oTLigTer, //
                oQuestionLigTer, //
                oQuestionSplit, //
                oTCtrlStk, //
                oLstQuestionCtrlStk, //
                oQuestionDestocking, //
                oTRet, //
                oWstr);
        if (!oTRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (!oQuestionLigTer.getStringValue().isEmpty()) {
            validatorBean.setFinishQuestionNeeded(true);
        } else {
            validatorBean.setFinishQuestionNeeded(false);
        }
        validatorBean.setToFinish(oTLigTer.getBooleanValue());
        if (!oQuestionSplit.getStringValue().isEmpty()) {
            validatorBean.getQuestions().add(
                    oQuestionSplit.getStringValue() + "\n"
                            + I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, ProductionMo.T29570));
        }
        if (oLstQuestionCtrlStk.getValue() != null && !oLstQuestionCtrlStk.getValue().equals("")) {
            String outputQuestions = oLstQuestionCtrlStk.getStringValue();
            String[] outputQuestionTab = outputQuestions.split(SEPARATOR);
            for (String message : outputQuestionTab) {
                validatorBean.getQuestions().add(message);
            }
        }
        if (!oQuestionDestocking.getStringValue().isEmpty()) {
            validatorBean.getQuestions().add(
                    oQuestionDestocking.getStringValue() + "\r"
                            + I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, ProductionMo.T29570));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkInputAll(ctx=" + ctx + ", entity=" + entity + ", operationItemKey="
                    + operationItemKey + ", workstationId=" + workstationId + ")=" + validatorBean);
        }
        return validatorBean;
    }

    /**
     * {@inheritDoc}
     */
    public MOStockCheckBean checkItemMovement(final VIFContext ctx, final ItemMovementBean itemMovementBean,
            final ActivityItemType activityItemType, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkItemMovement(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        StringHolder oCtrlStk = new StringHolder();
        StringHolder oQuestions = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCtrlStock(ctx.getIdCtx().getCompany(), ctx.getIdCtx().getEstablishment(), workstationId, ctx
                .getIdCtx().getLogin(), itemMovementBean.getItemMovementKey().getChrono().getPrechro(),
                itemMovementBean.getItemMovementKey().getChrono().getChrono(), activityItemType.getValue(),
                itemMovementBean.getItemCL().getCode(), itemMovementBean.getBatch(), itemMovementBean.getDepotCL()
                        .getCode(), ((itemMovementBean.getLocationCL().getCode() != null) ? itemMovementBean
                        .getLocationCL().getCode() : ""),
                ((itemMovementBean.getReasonCL().getCode() != null) ? itemMovementBean.getReasonCL().getCode() : ""),
                itemMovementBean.getItemMovementQuantityUnit().getFirstQuantity(), itemMovementBean
                        .getItemMovementQuantityUnit().getFirstUnit(), itemMovementBean.getItemMovementQuantityUnit()
                        .getSecondQuantity(), itemMovementBean.getItemMovementQuantityUnit().getSecondUnit(), oCtrlStk,
                oQuestions, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        Errors errors = new VIFBindException(itemMovementBean);
        if (oQuestions.getValue() != null && !oQuestions.getValue().equals("")) {
            String outputQuestions = oQuestions.getStringValue();
            String[] outputQuestionTab = outputQuestions.split(SEPARATOR);
            for (String message : outputQuestionTab) {
                errors.reject("", I18nServerManager.translate(ctx.getIdCtx().getLocale(), true,
                        new ProxyBusinessException(message)));
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkItemMovement(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        return new MOStockCheckBean(oCtrlStk.getStringValue(), errors, oRet.getBooleanValue(), oWstr.getStringValue());
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public FProductionOutputContainerValidatorBean checkOutputAll(final VIFContext ctx, final CEntityBean entity,
            final OperationItemKey operationItemKey, final String workstationId,
            final OutputWorkstationParameters workstationParameters, final OutputItemParameters itemParameters)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkOutputAll(ctx=" + ctx + ", entity=" + entity + ", operationItemKey="
                    + operationItemKey + ", workstationId=" + workstationId + ", workstationParameters="
                    + workstationParameters + ", itemParameters=" + itemParameters + ")");
        }
        FProductionOutputContainerValidatorBean validatorBean = new FProductionOutputContainerValidatorBean();
        BooleanHolder oTRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        BooleanHolder oTLigTer = new BooleanHolder();
        BooleanHolder oTCtrlStk = new BooleanHolder();
        StringHolder oQuestionLigTer = new StringHolder();
        StringHolder oQuestionSplit = new StringHolder();
        StringHolder oLstQuestionCtrlStk = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCtrlSortie(entity.getEntityLocationBean().getEntityLocationKey().getCsoc(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCetab(), workstationId, //
                ctx.getIdCtx().getLogin(), //
                operationItemKey.getManagementType().getValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), //
                operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4(), //
                entity.getStockItemBean().getGeneralItemInfos().getItemCL().getCode(), //
                entity.getStockItemBean().getStockItem().getBatch(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCdepot(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCemp(), //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit(), //
                entity.getEntityLocationBean().getEntityLocationKey().getContainerNumber(), //
                ActivityItemType.OUTPUT.getValue(), //
                "", //
                workstationParameters.getOutputTareType().getValue(), //
                oTLigTer, //
                oQuestionLigTer, //
                oQuestionSplit, //
                oTCtrlStk, //
                oLstQuestionCtrlStk, //
                oTRet, //
                oWstr);
        if (!oTRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (!oQuestionLigTer.getStringValue().isEmpty()) {
            validatorBean.setFinishQuestionNeeded(true);
        } else {
            validatorBean.setFinishQuestionNeeded(false);
        }
        validatorBean.setToFinish(oTLigTer.getBooleanValue());
        if (!oQuestionSplit.getStringValue().isEmpty()) {
            // CHU QA7370 - complete question by concatenation of oQuestionSplit and oQuestionLigTer informations
            if (!oQuestionLigTer.getStringValue().isEmpty()) {
                oQuestionSplit.setStringValue(oQuestionLigTer.getStringValue().concat(" ")
                        .concat(oQuestionSplit.getStringValue()));
            }
            validatorBean.getQuestions().add(
                    oQuestionSplit.getStringValue() + "\r"
                            + I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, ProductionMo.T29570));
        }
        if (!oLstQuestionCtrlStk.getStringValue().isEmpty()) {
            validatorBean.getQuestions().add(oLstQuestionCtrlStk.getStringValue());
        }
        validatorBean.setToFinish(oTLigTer.getBooleanValue());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkOutputAll(ctx=" + ctx + ", entity=" + entity + ", operationItemKey="
                    + operationItemKey + ", workstationId=" + workstationId + ", workstationParameters="
                    + workstationParameters + ", itemParameters=" + itemParameters + ")=" + validatorBean);
        }
        return validatorBean;
    }

    /**
     * {@inheritDoc}
     */
    public void checkSubstituteItem(final VIFContext ctx, final AbstractOperationItemBean operationItemBean,
            final String item, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkSubstituteItem(ctx=" + ctx + ", operationItemBean=" + operationItemBean + ", item="
                    + item + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCtrlArtRemp(operationItemBean.getOperationItemKey().getEstablishmentKey().getCsoc(),
                operationItemBean.getOperationItemKey().getEstablishmentKey().getCetab(), workstationId, ctx.getIdCtx()
                        .getLogin(), operationItemBean.getOperationItemKey().getManagementType().getValue(),
                operationItemBean.getOperationItemKey().getChrono().getPrechro(), operationItemBean
                        .getOperationItemKey().getChrono().getChrono(), operationItemBean.getOperationItemKey()
                        .getCounter1(), operationItemBean.getOperationItemKey().getCounter2(), operationItemBean
                        .getOperationItemKey().getCounter3(), operationItemBean.getOperationItemKey().getCounter4(),
                item, operationItemBean.getActivityItemType().getValue(), oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkSubstituteItem(ctx=" + ctx + ", operationItemBean=" + operationItemBean + ", item="
                    + item + ", workstationId=" + workstationId + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteInputMovement(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final MovementAct movementAct, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteInputMovement(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", movementAct=" + movementAct + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prDeleteMvtEntree(
                establishmentKey.getCsoc(), //
                establishmentKey.getCetab(), //
                workstationId, //
                ctx.getIdCtx().getLogin(), //
                ManagementType.REAL.getValue(), //
                movementAct.getPrechro(), //
                movementAct.getChrono(), //
                movementAct.getNumber1(), movementAct.getNumber2(), movementAct.getNumber3(), movementAct.getNumber4(),
                movementAct.getLineNumber(), //
                false, //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteInputMovement(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", movementAct=" + movementAct + ", workstationId=" + workstationId + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void deleteItemMovement(final VIFContext ctx, final ItemMovementKey itemMovementKey,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteItemMovement(ctx=" + ctx + ", itemMovementKey=" + itemMovementKey
                    + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prDeleteMvt(itemMovementKey.getEstablishmentKey().getCsoc(), itemMovementKey.getEstablishmentKey()
                .getCetab(), workstationId, ctx.getIdCtx().getLogin(), itemMovementKey.getRowidKey(), itemMovementKey
                .getManagementType().getValue(), itemMovementKey.getChrono().getPrechro(), itemMovementKey.getChrono()
                .getChrono(), itemMovementKey.getCounter1(), itemMovementKey.getCounter2(), itemMovementKey
                .getCounter3(), itemMovementKey.getCounter4(), // "false" means trnscsa of xpfent : profile information
                // - "Entité contenant reconduite"
                false, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteItemMovement(ctx=" + ctx + ", itemMovementKey=" + itemMovementKey
                    + ", workstationId=" + workstationId + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteLast(final VIFContext ctx, final EstablishmentKey establishmentKey, final MovementAct movementAct)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteLast(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", movementAct="
                    + movementAct + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prDeleteLastNew(ctx.getIdCtx().getWorkstation(), //
                ctx.getIdCtx().getLogin(), establishmentKey.getCsoc(), //
                establishmentKey.getCetab(), //
                movementAct.getPrechro(), //
                movementAct.getChrono(), //
                movementAct.getNumber1(), //
                movementAct.getNumber2(), //
                movementAct.getNumber3(), //
                movementAct.getNumber4(), //
                movementAct.getLineNumber(), //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteLast(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", movementAct="
                    + movementAct + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteLast(final VIFContext ctx, final ItemMovementKey itemMovementKey, final String workstationId)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteLast(ctx=" + ctx + ", itemMovementKey=" + itemMovementKey + ", workstationId="
                    + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prDeleteLast(workstationId, ctx.getIdCtx().getLogin(), itemMovementKey.getRowidKey(), oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteLast(ctx=" + ctx + ", itemMovementKey=" + itemMovementKey + ", workstationId="
                    + workstationId + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteOutputMovement(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final MovementAct movementAct, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteOutputMovement(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", movementAct=" + movementAct + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prDeleteMvtSortie(establishmentKey.getCsoc(), //
                establishmentKey.getCetab(), //
                workstationId, //
                ctx.getIdCtx().getLogin(), //
                ManagementType.REAL.getValue(), //
                movementAct.getPrechro(), //
                movementAct.getChrono(), //
                movementAct.getNumber1(), //
                movementAct.getNumber2(), //
                movementAct.getNumber3(), //
                movementAct.getNumber4(), //
                movementAct.getLineNumber(), //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteOutputMovement(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", movementAct=" + movementAct + ", workstationId=" + workstationId + ")");
        }
    }

    /**
     * Gets the activitiesSBS.
     * 
     * @return the activitiesSBS.
     */
    public ActivitiesSBS getActivitiesSBS() {
        return activitiesSBS;
    }

    /**
     * Gets the entitySBS.
     * 
     * @category getter
     * @return the entitySBS.
     */
    public EntitySBS getEntitySBS() {
        return entitySBS;
    }

    /**
     * {@inheritDoc}
     * 
     * @deprecated Replace by MoProfileSBS.getInputProfile(...)
     */
    @Override
    @Deprecated
    public InputItemParameters getInputItemParameters(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getInputItemParameters(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", itemId=" + itemId + ", workstationId=" + workstationId + ")");
        }
        InputItemParameters inputItemParameters = new InputItemParameters();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        IntegerHolder oEntityType = new IntegerHolder();
        DoubleHolder oQte1 = new DoubleHolder();
        StringHolder oProfileId = new StringHolder();
        StringHolder oProfileLabel = new StringHolder();
        StringHolder oCu1 = new StringHolder();
        StringHolder oCu2 = new StringHolder();
        StringHolder oCu3 = new StringHolder();
        BooleanHolder oInterpretQty = new BooleanHolder();

        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prGetInputParameters4(establishmentKey.getCsoc(), //
                establishmentKey.getCetab(), //
                workstationId, //
                itemId, //
                oProfileId, //
                oProfileLabel, //
                oEntityType, //
                oQte1, //
                oCu1, //
                oCu2, //
                oCu3, //
                oInterpretQty, //
                oRet, //
                oMsg);
        //
        inputItemParameters.setEntityType(InputParametersEnums.InputEntityType.getInputEntityType(oEntityType
                .getIntValue()));
        inputItemParameters.setProfileId(oProfileId.getStringValue());
        inputItemParameters.setProfileLabel(oProfileLabel.getStringValue());
        inputItemParameters.setForcedFirstQty(oQte1.getDoubleValue());
        inputItemParameters.setForcedFirstUnit(oCu1.getStringValue());
        inputItemParameters.setForcedSecondUnit(oCu2.getStringValue());
        inputItemParameters.setInterpretQty(oInterpretQty.getBooleanValue());
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMsg.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getInputItemParameters(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", itemId=" + itemId + ", workstationId=" + workstationId + ")=" + inputItemParameters);
        }
        return inputItemParameters;
    }

    /**
     * Gets the itemSBS.
     * 
     * @category getter
     * @return the itemSBS.
     */
    public ItemSBS getItemSBS() {
        return itemSBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getLocationOptimize(final VIFContext ctx, final CEntityBean entity,
            final CriteriaReturnInitBean beanCriteria) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLocationOptimize(ctx=" + ctx + ", entity=" + entity + ", beanCriteria=" + beanCriteria
                    + ")");
        }
        String sEmpOpt = null;
        List<String> listLocationOptimizes = this.getLocationsOptimize(ctx, entity, beanCriteria);
        if (listLocationOptimizes != null && !listLocationOptimizes.isEmpty()) {
            sEmpOpt = listLocationOptimizes.get(0);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getLocationOptimize(ctx=" + ctx + ", entity=" + entity + ", beanCriteria=" + beanCriteria
                    + ")=" + sEmpOpt);
        }
        return sEmpOpt;
    }

    @Override
    public List<String> getLocationsOptimize(final VIFContext ctx, final CEntityBean entity,
            final CriteriaReturnInitBean beanCriteria) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLocationsOptimize(ctx=" + ctx + ", entity=" + entity + ", beanCriteria="
                    + beanCriteria + ")");
        }
        List<String> listLocationOptimizes = this.listLocationOptimizes(ctx, entity, beanCriteria);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getLocationsOptimize(ctx=" + ctx + ", entity=" + entity + ", beanCriteria="
                    + beanCriteria + ")=" + listLocationOptimizes);
        }
        return listLocationOptimizes;
    }

    /**
     * Gets the moProfileSBS.
     *
     * @return the moProfileSBS.
     */
    public MoProfileSBS getMoProfileSBS() {
        return moProfileSBS;
    }

    /**
     * Gets the moSBS.
     * 
     * @category getter
     * @return the moSBS.
     */
    public MOSBS getMoSBS() {
        return moSBS;
    }

    /**
     * {@inheritDoc}
     */
    public AbstractOperationItemBean getOperationItem(final VIFContext ctx, final OperationItemKey operationItemKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ")");
        }
        AbstractOperationItemBean operationItemBean = null;
        operationItemBean = this.getOperationItem(ctx, operationItemKey, null);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ")="
                    + operationItemBean);
        }
        return operationItemBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public AbstractOperationItemBean getOperationItem(final VIFContext ctx, final OperationItemKey operationItemKey,
            final ItemKey itemKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", itemKey="
                    + itemKey + ")");
        }
        AbstractOperationItemBean operationItemBean = null;
        WareHouseProperties wareHouseProperties = null;
        try {
            Xpiloia xpiloia = XpiloiaDAOFactory.getDAO().getOperationItem(ctx.getDbCtx(), operationItemKey);
            if (xpiloia == null) {
                throw new ServerBusinessException(Generic.T17702, new VarParamTranslation(null));
            }
            CodeLabel depositoryCL = this.getDepositOfXpiloia(ctx, xpiloia, itemKey);
            CodeLabel locationCL = this.getLocationOfXpiloia(ctx, xpiloia, itemKey);
            if (ActivityItemType.INPUT.getValue().equals(xpiloia.getTypacta())) {
                operationItemBean = new InputOperationItemBean(ManagementType.getManagementType(xpiloia.getTypgest()),
                        new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(xpiloia.getPrechrof(),
                                xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia.getNi3(),
                        xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia.getLart(),
                                xpiloia.getRart()), depositoryCL, locationCL, new Date(),
                        new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                .getQtartrest1(), xpiloia.getCuart1()), xpiloia.getTauto(), xpiloia.getLstnatstk(),
                        TouchDeclarationType.WITHOUT_QTY_TOUCH_DECLARATION.getValue().equals(xpiloia.getTypdecatl()),
                        TouchDeclarationType.getTouchDeclarationType(xpiloia.getTypdecatl()));
            } else {
                operationItemBean = new OutputOperationItemBean(ManagementType.getManagementType(xpiloia.getTypgest()),
                        new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(xpiloia.getPrechrof(),
                                xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia.getNi3(),
                        xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia.getLart(),
                                xpiloia.getRart()), depositoryCL, locationCL, new Date(),
                        new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                .getQtartrest1(), xpiloia.getCuart1()), xpiloia.getTauto(), xpiloia.getLstnatstk(),
                        TouchDeclarationType.getTouchDeclarationType(xpiloia.getTypdecatl()));
                // DP1689 : Load the properties of the deposit. Use to Location Optimise.
                wareHouseProperties = warehouseSBS.getProperties(ctx,
                        new WareHouseKey(xpiloia.getCsoc(), xpiloia.getCetab(), depositoryCL.getCode()));
                if (wareHouseProperties != null) {
                    ((OutputOperationItemBean) operationItemBean).setToptimise(wareHouseProperties.getToptimise());
                }
            }
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(
                        new StringBuilder().append("Unable to get the operation item ")
                                .append(operationItemKey.getEstablishmentKey().getCsoc())
                                .append(operationItemKey.getEstablishmentKey().getCetab()).append("-")
                                .append(operationItemKey.getChrono().getPrechro()).append("-")
                                .append(operationItemKey.getChrono().getChrono()).append("-")
                                .append(operationItemKey.getCounter1()).append("-")
                                .append(operationItemKey.getCounter2()).append("-")
                                .append(operationItemKey.getCounter3()).append("-")
                                .append(operationItemKey.getCounter4()).toString(), e);
            }
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ")="
                    + operationItemBean);
        }
        return operationItemBean;
    }

    /**
     * {@inheritDoc}
     * 
     * @deprecated Replace by MoProfileSBS.getOutputProfile(...)
     */
    @Override
    @Deprecated
    public OutputItemParameters getOutputItemParameters(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getOutputItemParameters(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", itemId=" + itemId + ", workstationId=" + workstationId + ")");
        }
        OutputItemParameters outputItemParameters = new OutputItemParameters();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        IntegerHolder oEntityType = new IntegerHolder();
        StringHolder oProfileId = new StringHolder();
        StringHolder oProfileLabel = new StringHolder();
        StringHolder oCcond = new StringHolder();
        StringHolder oCcondp = new StringHolder();
        IntegerHolder oTypFerm = new IntegerHolder();
        IntegerHolder oTypFermCapa = new IntegerHolder();
        IntegerHolder oTypFermp = new IntegerHolder();
        IntegerHolder oTypFermpCapa = new IntegerHolder();
        DoubleHolder oQte1 = new DoubleHolder();
        StringHolder oCu1 = new StringHolder();
        DoubleHolder oQte2 = new DoubleHolder();
        StringHolder oCu2 = new StringHolder();
        StringHolder oCdepot = new StringHolder();
        StringHolder oCemp = new StringHolder();
        StringHolder oBatch = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prGetOutputParameters3(establishmentKey.getCsoc(), //
                establishmentKey.getCetab(), //
                workstationId, //
                itemId, //
                oProfileId, //
                oEntityType, //
                oCcond, //
                oCcondp, //
                oTypFerm, //
                oTypFermCapa, //
                oTypFermp, //
                oTypFermpCapa, //
                oQte1, //
                oCu1, //
                oQte2, //
                oCu2, //
                oCdepot, //
                oCemp, //
                oBatch, //
                oProfileLabel, //
                oRet, //
                oMsg);
        //
        outputItemParameters.setProfileId(oProfileId.getStringValue());
        outputItemParameters.setProfileLabel(oProfileLabel.getStringValue());
        outputItemParameters.setContainerPackagingId(oCcond.getStringValue());
        outputItemParameters.setUpperContainerPackagingId(oCcondp.getStringValue());
        outputItemParameters.setEntityType(OutputEntityType.getOutputEntityType(oEntityType.getIntegerValue()));
        outputItemParameters.setContainerClosingType(OutputContainerClosingType.getOutputContainerClosingType(oTypFerm
                .getIntegerValue()));
        outputItemParameters.setUpperContainerClosingType(OutputContainerClosingType
                .getOutputContainerClosingType(oTypFermp.getIntegerValue()));
        // Units
        outputItemParameters.setForcedFirstUnit(oCu1.getStringValue());
        outputItemParameters.setForcedSecondUnit(oCu2.getStringValue());
        // Quantities
        if (oCu1.getStringValue() != null && !oCu1.getStringValue().isEmpty()) {
            outputItemParameters.setForcedFirstQty(oQte1.getDoubleValue());
            if (oCu2.getStringValue() != null && !oCu2.getStringValue().isEmpty()) {
                // Second quantity obtained from conversion
                ItemKey itemKey = new ItemKey(establishmentKey.getCsoc(), itemId);
                QtyUnit firstQtyUnit = new QtyUnit(oQte1.getDoubleValue(), oCu1.getStringValue());
                QtyUnit secondQtyUnit = getItemSBS().convertQty(ctx, itemKey, firstQtyUnit, oCu2.getStringValue(),
                        false);
                if (secondQtyUnit != null) {
                    outputItemParameters.setForcedSecondQty(secondQtyUnit.getQty());
                }
            }
        }
        outputItemParameters.setContainerCapacityClosingType(OutputContainerCapacityClosingType
                .getOutputContainerCapacityClosingType(oTypFermCapa.getIntegerValue()));
        outputItemParameters.setUpperContainerCapacityClosingType(OutputUpperContainerCapacityClosingType
                .getOutputUpperContainerCapacityClosingType(oTypFermpCapa.getIntegerValue()));
        outputItemParameters.setBatchType(OutputBatchType.getOutputBatchType(oBatch.getStringValue()));
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMsg.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getOutputItemParameters(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", itemId=" + itemId + ", workstationId=" + workstationId + ")=" + outputItemParameters);
        }
        return outputItemParameters;
    }

    /**
     * {@inheritDoc}
     */
    public OutputItemParameters getOutputItemParameters(final VIFContext ctx, final OperationItemKey operationItemKey,
            final MONatureComplementaryWorkshop natureComplementaryWorkshop,
            final OutputItemParameters outputItemParameters) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getOutputItemParameters(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", natureComplementaryWorkshop=" + natureComplementaryWorkshop + ", outputItemParameters="
                    + outputItemParameters + ")");
        }
        OutputItemParameters outputParameter = null;
        if (outputItemParameters == null) {
            outputParameter = new OutputItemParameters();
        } else {
            outputParameter = outputItemParameters;
        }
        if (MONatureComplementaryWorkshop.SCRAP.equals(natureComplementaryWorkshop)) {
            // Load the POBean to get the family activity (kot.cfam)
            POKey poKey = new POKey(operationItemKey.getEstablishmentKey(), new ChronoPO(operationItemKey.getChrono(),
                    operationItemKey.getCounter1()));
            POBean poBean = getMoSBS().getPOBeanByKey(ctx, poKey);
            // The unit of the item is set in OFDEC[26].
            String unit = getParameterSBS().readStringParameter(ctx, poKey.getEstablishmentKey().getCsoc(),
                    poKey.getEstablishmentKey().getCetab(),
                    ProductionMOParamEnum.DECLARATION_BY_ACTIVITY_FAMILY.getCode(),
                    DECLARATION_BY_ACTIVITY_FAMILY_COUNTER_26, poBean == null ? "" : poBean.getActivityFamilyId());
            if (StringUtils.isNotEmpty(unit)) {
                outputParameter.setEntityType(OutputEntityType.BATCH_ITEM);
                outputParameter.setForcedFirstUnit(unit);
                outputParameter.setForcedFirstQty(0.0);
                outputParameter.setForcedSecondQty(0.0);
                outputParameter.setForcedSecondUnit("");
                outputParameter.setContainerClosingType(OutputContainerClosingType.DISABLED);
                outputParameter.setContainerCapacityClosingType(OutputContainerCapacityClosingType.DISABLED);
            } else {
                ServerBusinessException e = new ServerBusinessException(ProductionMo.T14544, new VarParamTranslation(
                        DECLARATION_BY_ACTIVITY_FAMILY_COUNTER_26), new VarParamTranslation(
                                ProductionMOParamEnum.DECLARATION_BY_ACTIVITY_FAMILY.getCode()));
                LOGGER.error(" getOutputItemParameters(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ") - "
                        + e);
                throw e;
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getOutputItemParameters(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", natureComplementaryWorkshop=" + natureComplementaryWorkshop + ", outputItemParameters="
                    + outputItemParameters + ")=" + outputParameter);
        }
        return outputParameter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OutputOperationItemBean getOutputOperationItem(final VIFContext ctx,
            final OperationItemKey operationItemKey, final ItemKey itemKeyDowngraded,
            final MONatureComplementaryWorkshop natureComplementaryWorkshop) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", itemKeyDowngraded=" + itemKeyDowngraded + ", natureComplementaryWorkshop="
                    + natureComplementaryWorkshop + ")");
        }
        List<OutputOperationItemBean> operationItemBeans = null;
        OutputOperationItemBean outputOperationItemBean = null;
        try {
            Xpiloia xpiloia = XpiloiaDAOFactory.getDAO().getOperationItem(ctx.getDbCtx(), operationItemKey);
            if (xpiloia == null) {
                throw new ServerBusinessException(Generic.T17702, new VarParamTranslation(null));
            }
            List<AbstractOperationItemBean> absOpItemList = this.getOperationItem(ctx, xpiloia,
                    natureComplementaryWorkshop, itemKeyDowngraded);
            if (absOpItemList != null) {
                operationItemBeans = new ArrayList<OutputOperationItemBean>();
                for (AbstractOperationItemBean abstractOperationItemBean : absOpItemList) {
                    if (abstractOperationItemBean instanceof OutputOperationItemBean) {
                        operationItemBeans.add((OutputOperationItemBean) abstractOperationItemBean);
                    }
                }
            }
            if (operationItemBeans != null && !operationItemBeans.isEmpty()) {
                outputOperationItemBean = operationItemBeans.get(0);
                CodeLabel depositoryCL = this.getDepositOfXpiloia(ctx, xpiloia, itemKeyDowngraded);
                CodeLabel locationCL = this.getLocationOfXpiloia(ctx, xpiloia, itemKeyDowngraded);
                outputOperationItemBean.setDepotCL(depositoryCL);
                outputOperationItemBean.setLocationCL(locationCL);
                // DP1689 : Load the properties of the deposit. Use to Location Optimise.
                WareHouseProperties wareHouseProperties = warehouseSBS.getProperties(ctx,
                        new WareHouseKey(xpiloia.getCsoc(), xpiloia.getCetab(), depositoryCL.getCode()));
                if (wareHouseProperties != null) {
                    outputOperationItemBean.setToptimise(wareHouseProperties.getToptimise());
                }
            }
        } catch (DAOException e) {
            LOGGER.error("getOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", itemKeyDowngraded=" + itemKeyDowngraded + ", natureComplementaryWorkshop="
                    + natureComplementaryWorkshop + ") - " + e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", itemKeyDowngraded=" + itemKeyDowngraded + ", natureComplementaryWorkshop="
                    + natureComplementaryWorkshop + ")=" + outputOperationItemBean);
        }
        return outputOperationItemBean;
    }

    /**
     * Gets the parameterSBS.
     * 
     * @category getter
     * @return the parameterSBS.
     */
    public ParameterSBS getParameterSBS() {
        return parameterSBS;
    }

    /**
     * {@inheritDoc}
     * 
     * @deprecated
     */
    @Deprecated
    public String getPdfFileOfCurrentItem(final VIFContext ctx, final MOItemKey moItemKey)
            throws ServerBusinessException {
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("getPdfFileOfCurrentItem() moItemKey=" + moItemKey);
        }
        if (moItemKey == null || moItemKey.getEstablishmentKey() == null
                || moItemKey.getEstablishmentKey().getCsoc() == null
                || moItemKey.getEstablishmentKey().getCsoc().equals("")
                || moItemKey.getEstablishmentKey().getCetab() == null
                || moItemKey.getEstablishmentKey().getCetab().equals("") || moItemKey.getChrono() == null
                || moItemKey.getChrono().getPrechro() == null || moItemKey.getChrono().getPrechro().equals("")
                || moItemKey.getChrono().getChrono() == 0) {
            /* || moItemKey.getCounter1() == 0 */
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        StringHolder oDocument = new StringHolder();
        OfliedocPPO ppo = new OfliedocPPO(ctx);
        ppo.getDocument(moItemKey.getEstablishmentKey().getCsoc(), //
                moItemKey.getEstablishmentKey().getCetab(), //
                moItemKey.getChrono().getPrechro(), //
                moItemKey.getChrono().getChrono(), //
                moItemKey.getCounter1(), ctx.getIdCtx().getLogin(), oDocument, oRet, oMsg);
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("getPdfFileOfCurrentItem() oDocument=" + oDocument);
        }
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMsg.getStringValue());
        }
        return oDocument != null ? oDocument.getStringValue() : "";
    }

    /**
     * {@inheritDoc}
     */
    public List<PDFFile> getPdfFilesOfCurrentItem(final VIFContext ctx, final MOItemKey moItemKey)
            throws ServerBusinessException {
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("getPdfFileOfCurrentItem() moItemKey=" + moItemKey);
        }
        List<PDFFile> ret = new ArrayList<PDFFile>();
        if (moItemKey == null || moItemKey.getEstablishmentKey() == null
                || moItemKey.getEstablishmentKey().getCsoc() == null
                || moItemKey.getEstablishmentKey().getCsoc().equals("")
                || moItemKey.getEstablishmentKey().getCetab() == null
                || moItemKey.getEstablishmentKey().getCetab().equals("") || moItemKey.getChrono() == null
                || moItemKey.getChrono().getPrechro() == null || moItemKey.getChrono().getPrechro().equals("")
                || moItemKey.getChrono().getChrono() == 0) {
            /* || moItemKey.getCounter1() == 0 */
            String error = I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, ProductionMo.T30838) + " - "
                    + moItemKey;
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(error));
        }
        TempTableHolder<OfliedocPPOTtdocument> tt = new TempTableHolder<OfliedocPPOTtdocument>();
        OfliedocPPO ppo = new OfliedocPPO(ctx);
        ppo.getDocuments(moItemKey.getEstablishmentKey().getCsoc(), //
                moItemKey.getEstablishmentKey().getCetab(), //
                moItemKey.getChrono().getPrechro(), //
                moItemKey.getChrono().getChrono(), //
                moItemKey.getCounter1(), ctx.getIdCtx().getLogin(), tt);
        for (OfliedocPPOTtdocument rec : tt.getListValue()) {
            ret.add(new PDFFile(rec.getTitre(), rec.getPath()));
        }
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("getPdfFilesOfCurrentItem() return=" + ret);
        }
        return ret;
    }

    /**
     * 
     * search for the repository of an item in an operation.
     * 
     * @param ctx thge vif context
     * @param operationItemKey operation key
     * @param itemId item
     * @return the Code label of depository.
     * @throws ServerBusinessException e
     */
    public CodeLabel getWareHouse(final VIFContext ctx, final OperationItemKey operationItemKey, final String itemId)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getWareHouse(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", itemId="
                    + itemId + ")");
        }
        CodeLabel retVal = null;
        try {
            Xpiloia xpiloia = XpiloiaDAOFactory.getDAO().getByPK(ctx.getDbCtx(),
                    operationItemKey.getManagementType().getValue(), operationItemKey.getEstablishmentKey().getCsoc(),
                    operationItemKey.getEstablishmentKey().getCetab(), operationItemKey.getChrono().getPrechro(),
                    operationItemKey.getChrono().getChrono(), operationItemKey.getCounter1(),
                    operationItemKey.getCounter2(), operationItemKey.getCounter3(), operationItemKey.getCounter4());
            if (xpiloia.getCdepot() == null || "".equals(xpiloia.getCdepot())) {
                BooleanHolder oRet = new BooleanHolder();
                StringHolder oWstr = new StringHolder();
                StringHolder oCdepot = new StringHolder();
                StringHolder oLdepot = new StringHolder();
                XxfabPPO xxfabPPO = new XxfabPPO(ctx);
                xxfabPPO.prDepotArticle(xpiloia.getCsoc(), xpiloia.getCetab(), itemId, oCdepot, oLdepot, oRet, oWstr);
                if (!oRet.getBooleanValue()) {
                    throw new ProxyBusinessException(oWstr.getStringValue());
                }
                retVal = new CodeLabel(oCdepot.getStringValue(), oLdepot.getStringValue());
            } else {
                retVal = new CodeLabels(xpiloia.getCdepot(), xpiloia.getLdepot(), xpiloia.getRdepot());
            }
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(
                        new StringBuilder().append("Unable to get operation item of manufacturing order ")
                                .append(operationItemKey.getEstablishmentKey().getCsoc()).append("-")
                                .append(operationItemKey).toString(), e);
            }
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getWareHouse(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", itemId="
                    + itemId + ")=" + retVal);
        }
        return retVal;
    }

    /**
     * Gets the warehouseSBS.
     * 
     * @return the warehouseSBS.
     */
    public WareHouseSBS getWarehouseSBS() {
        return warehouseSBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean hasStatementScrap(final VIFContext ctx, final MOKey key, final String workstationId)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - hasStatementScrap(ctx=" + ctx + ", key=" + key + ", workstationId=" + workstationId + ")");
        }
        boolean hasStatement = false;
        try {
            hasStatement = XpiloiaDAOFactory.getDAO().hasStatementScrap(ctx.getDbCtx(), key, workstationId);
        } catch (DAOException e) {
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - hasStatementScrap(ctx=" + ctx + ", key=" + key + ", workstationId=" + workstationId
                    + ")=" + hasStatement);
        }
        return hasStatement;
    }

    /**
     * {@inheritDoc}
     */
    public ItemMovementQuantityUnit initQuantityWithLeftToDoQuantity(final VIFContext ctx,
            final AbstractOperationItemBean operationItemBean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initQuantityWithLeftToDoQuantity(ctx=" + ctx + ", operationItemBean=" + operationItemBean
                    + ")");
        }
        ItemMovementQuantityUnit itemMovementQuantityUnit = null;
        DoubleHolder oFirstQuantity = new DoubleHolder();
        StringHolder oFirstUnit = new StringHolder();
        DoubleHolder oSecondQuantity = new DoubleHolder();
        StringHolder oSecondUnit = new StringHolder();
        DoubleHolder oThirdQuantity = new DoubleHolder();
        StringHolder oThirdUnit = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        // Reading parameter to know if we must initialize
        POKey poKey = new POKey();
        poKey.setEstablishmentKey(operationItemBean.getOperationItemKey().getEstablishmentKey());
        poKey.setChrono(new ChronoPO(operationItemBean.getOperationItemKey().getChrono(), operationItemBean
                .getOperationItemKey().getCounter1()));
        String activityFamily = getMoSBS().getPOBeanByKey(ctx, poKey).getActivityFamilyId();
        boolean initQuantityWithLeftToDoQuantity = parameterSBS.readBooleanParameter(ctx, ctx.getIdCtx().getCompany(),
                ctx.getIdCtx().getEstablishment(), ProductionMOParamEnum.DECLARATION_BY_ACTIVITY_FAMILY.getCode(),
                PARAM_FIELD_DECLARATION_BY_ACTIVITY_FAMILY_INIT_LEFT_TO_DO, activityFamily);
        // Call of the service
        if (initQuantityWithLeftToDoQuantity) {
            double leftToDoQuantity = getOperationItem(ctx, operationItemBean.getOperationItemKey())
                    .getOperationItemQuantityUnit().getLeftToDoQuantity();
            String unit = getOperationItem(ctx, operationItemBean.getOperationItemKey()).getOperationItemQuantityUnit()
                    .getUnit();
            XxfabPPO xxfabPPO = new XxfabPPO(ctx);
            xxfabPPO.prInitQteOf(operationItemBean.getOperationItemKey().getEstablishmentKey().getCsoc(),
                    operationItemBean.getItemCL().getCode(), leftToDoQuantity, unit, "", "", "", oFirstQuantity,
                    oFirstUnit, oSecondQuantity, oSecondUnit, oThirdQuantity, oThirdUnit, oRet, oWstr);
            itemMovementQuantityUnit = new ItemMovementQuantityUnit();
            itemMovementQuantityUnit.setFirstQuantity(oFirstQuantity.getDoubleValue());
            itemMovementQuantityUnit.setFirstUnit(oFirstUnit.getStringValue());
            itemMovementQuantityUnit.setSecondQuantity(oSecondQuantity.getDoubleValue());
            itemMovementQuantityUnit.setSecondUnit(oSecondUnit.getStringValue());
            itemMovementQuantityUnit.setThirdQuantity(oThirdQuantity.getDoubleValue());
            itemMovementQuantityUnit.setThirdUnit(oThirdUnit.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initQuantityWithLeftToDoQuantity(ctx=" + ctx + ", operationItemBean=" + operationItemBean
                    + ")=" + itemMovementQuantityUnit);
        }
        return itemMovementQuantityUnit;
    }

    /**
     * {@inheritDoc}
     */
    public ItemMovementQuantityUnit initQuantityWithStock(final VIFContext ctx,
            final AbstractOperationItemBean operationItemBean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initQuantityWithStock(ctx=" + ctx + ", operationItemBean=" + operationItemBean + ")");
        }
        ItemMovementQuantityUnit itemMovementQuantityUnit = null;
        DoubleHolder oFirstQuantity = new DoubleHolder();
        StringHolder oFirstUnit = new StringHolder();
        DoubleHolder oSecondQuantity = new DoubleHolder();
        StringHolder oSecondUnit = new StringHolder();
        DoubleHolder oThirdQuantity = new DoubleHolder();
        StringHolder oThirdUnit = new StringHolder();
        // Reading parameter to know if we must initialize
        String itemFamily = getItemSBS().getGeneralInfos(
                ctx,
                new ItemKey(operationItemBean.getOperationItemKey().getEstablishmentKey().getCsoc(), operationItemBean
                        .getItemCL().getCode())).getCfam();
        boolean initQuantityWithStock = parameterSBS.readBooleanParameter(ctx, ctx.getIdCtx().getCompany(), ctx
                .getIdCtx().getEstablishment(), ProductionMOParamEnum.DECLARATION_BY_ITEM_FAMILY.getCode(),
                PARAM_FIELD_DECLARATION_BY_ITEM_FAMILY_INIT_STK, itemFamily);
        // Call of the service
        if (initQuantityWithStock) {
            XxfabPPO xxfabPPO = new XxfabPPO(ctx);
            xxfabPPO.prInitQteStklot(operationItemBean.getOperationItemKey().getEstablishmentKey().getCsoc(),
                    operationItemBean.getOperationItemKey().getEstablishmentKey().getCetab(), operationItemBean
                            .getItemCL().getCode(), operationItemBean.getForecastBatch(), operationItemBean
                            .getDepotCL().getCode(), operationItemBean.getLocationCL().getCode(), oFirstQuantity,
                    oFirstUnit, oSecondQuantity, oSecondUnit, oThirdQuantity, oThirdUnit);
            itemMovementQuantityUnit = new ItemMovementQuantityUnit();
            itemMovementQuantityUnit.setFirstQuantity(oFirstQuantity.getDoubleValue());
            itemMovementQuantityUnit.setFirstUnit(oFirstUnit.getStringValue());
            itemMovementQuantityUnit.setSecondQuantity(oSecondQuantity.getDoubleValue());
            itemMovementQuantityUnit.setSecondUnit(oSecondUnit.getStringValue());
            itemMovementQuantityUnit.setThirdQuantity(oThirdQuantity.getDoubleValue());
            itemMovementQuantityUnit.setThirdUnit(oThirdUnit.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initQuantityWithStock(ctx=" + ctx + ", operationItemBean=" + operationItemBean + ")="
                    + itemMovementQuantityUnit);
        }
        return itemMovementQuantityUnit;
    }

    /**
     * {@inheritDoc}
     */
    public ItemMovementBean interpretBarcode(final VIFContext ctx, final String barcode,
            final AbstractOperationItemBean operationItemBean, final List<String> expectedUnitList,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - interpretBarcode(ctx=" + ctx + ", barcode=" + barcode + ", operationItemBean="
                    + operationItemBean + ", expectedUnitList=" + expectedUnitList + ", workstationId=" + workstationId
                    + ")");
        }
        ItemMovementBean itemMovementBean = new ItemMovementBean();
        itemMovementBean.setBatch(operationItemBean.getForecastBatch());
        itemMovementBean.setItemCL(operationItemBean.getItemCL());
        itemMovementBean.setItemMovementKey(new ItemMovementKey(operationItemBean.getOperationItemKey()
                .getManagementType(), operationItemBean.getOperationItemKey().getEstablishmentKey(), operationItemBean
                .getOperationItemKey().getChrono(), operationItemBean.getOperationItemKey().getCounter1(),
                operationItemBean.getOperationItemKey().getCounter2(), operationItemBean.getOperationItemKey()
                        .getCounter3(), operationItemBean.getOperationItemKey().getCounter4(), ""));
        itemMovementBean.setItemMovementQuantityUnit(new ItemMovementQuantityUnit(0, "", 0, "", 0, ""));
        itemMovementBean.setMovementDateTime(operationItemBean.getMovementDateTime());
        itemMovementBean.setReasonCL(new CodeLabel("", ""));
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        TempTableHolder<XxfabPPOTtcontenu> ttContents = new TempTableHolder<XxfabPPOTtcontenu>();
        xxfabPPO.prAnalyseCabEntrant(operationItemBean.getOperationItemKey().getEstablishmentKey().getCsoc(),
                operationItemBean.getOperationItemKey().getEstablishmentKey().getCetab(), workstationId,
                operationItemBean.getOperationItemKey().getChrono().getPrechro(), operationItemBean
                        .getOperationItemKey().getChrono().getChrono(), operationItemBean.getItemCL().getCode(),
                operationItemBean.getActivityItemType().getValue(), barcode, ttContents);
        Iterator<XxfabPPOTtcontenu> iter = ttContents.getListValue().iterator();
        XxfabPPOTtcontenu content = iter.next();
        itemMovementBean.setItemCL(new CodeLabels(content.getCart(), content.getLart(), ""));
        itemMovementBean.setBatch(content.getLot());
        ItemMovementQuantityUnit itemMovementQuantityUnit = new ItemMovementQuantityUnit();
        for (int i = 0; i < expectedUnitList.size(); i++) {
            double quantity = 0;
            if (expectedUnitList.get(i).equalsIgnoreCase(KILOGRAM)) {
                quantity = content.getPdsnet();
            } else if (expectedUnitList.get(i).equals(content.getCug())) {
                quantity = content.getQug();
            } else if (expectedUnitList.get(i).equals(content.getCus())) {
                quantity = content.getQus();
            } else if (expectedUnitList.get(i).equals(content.getCut())) {
                quantity = content.getQut();
            }
            switch (i) {
                case 0:
                    itemMovementQuantityUnit.setFirstUnit(expectedUnitList.get(i));
                    itemMovementQuantityUnit.setFirstQuantity(quantity);
                    break;
                case 1:
                    itemMovementQuantityUnit.setSecondUnit(expectedUnitList.get(i));
                    itemMovementQuantityUnit.setSecondQuantity(quantity);
                    break;
                case 2:
                    itemMovementQuantityUnit.setThirdUnit(expectedUnitList.get(i));
                    itemMovementQuantityUnit.setThirdQuantity(quantity);
                    break;
                default:
                    break;
            }
        }
        CodeLabel stockLocation = getWareHouse(ctx, operationItemBean.getOperationItemKey(), itemMovementBean
                .getItemCL().getCode());
        itemMovementBean.setDepotCL(stockLocation);
        itemMovementBean.setLocationCL(new CodeLabel("", ""));
        itemMovementBean.setItemMovementQuantityUnit(itemMovementQuantityUnit);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - interpretBarcode(ctx=" + ctx + ", barcode=" + barcode + ", operationItemBean="
                    + operationItemBean + ", expectedUnitList=" + expectedUnitList + ", workstationId=" + workstationId
                    + ")=" + itemMovementBean);
        }
        return itemMovementBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String interpretItemFromBarcode(final VIFContext ctx, final String barcode, final String companyId,
            final String establishmentId, final String workstationId) throws ServerBusinessException {
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug(new StringBuilder().append("interpretItemFromBarcode(ctx=)").append(ctx).append(", barcode=")
                    .append(barcode).append(", companyId=").append(companyId).append(", establishmentId=")
                    .append(establishmentId).append(", workstationId=").append(workstationId).append(")").toString());
        }
        StringHolder itemId = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prAnalyseCab(barcode, companyId, establishmentId, workstationId, itemId);
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug(new StringBuilder().append("interpretItemFromBarcode returns ").append(itemId).toString());
        }
        return itemId.getStringValue();
    }

    /**
     * {@inheritDoc}
     */
    public void isExistingBatchQuality(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String batch, final String itemCode, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isExistingBatchQuality(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", batch=" + batch + ", itemCode=" + itemCode + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        double isExistingBatchQuality = parameterSBS.readDoubleParameter(ctx, ctx.getIdCtx().getCompany(), ctx
                .getIdCtx().getEstablishment(), ProductionMOParamEnum.FABRICATION_INPUT.getCode(),
                PARAM_FIELD_FABRICATION_INPUT_EXISTINGQUALITY, workstationId);
        if (isExistingBatchQuality != 0) {
            XxfabPPO xxfabPPO = new XxfabPPO(ctx);
            xxfabPPO.prTestExistenceLot(establishmentKey.getCsoc(), establishmentKey.getCetab(), batch, itemCode, oRet,
                    oWstr);
            if (!oRet.getBooleanValue()) {
                throw new ProxyBusinessException(oWstr.getStringValue());
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isExistingBatchQuality(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", batch=" + batch + ", itemCode=" + itemCode + ", workstationId=" + workstationId + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public MOFinishedBean isOperationItemFinished(final VIFContext ctx, final ItemMovementBean itemMovementBean,
            final ActivityItemType activityItemType, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isOperationItemFinished(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oIsFinished = new BooleanHolder();
        StringHolder oQuestion = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prTestSaisieQtes(itemMovementBean.getItemMovementKey().getEstablishmentKey().getCsoc(), //
                itemMovementBean.getItemMovementKey().getEstablishmentKey().getCetab(), //
                workstationId, //
                ctx.getIdCtx().getLogin(), //
                itemMovementBean.getItemMovementKey().getManagementType().getValue(), //
                itemMovementBean.getItemMovementKey().getChrono().getPrechro(), //
                itemMovementBean.getItemMovementKey().getChrono().getChrono(), //
                itemMovementBean.getItemMovementKey().getCounter1(), //
                itemMovementBean.getItemMovementKey().getCounter2(), //
                itemMovementBean.getItemMovementKey().getCounter3(), //
                itemMovementBean.getItemMovementKey().getCounter4(), //
                itemMovementBean.getItemCL().getCode(), //
                itemMovementBean.getItemMovementQuantityUnit().getFirstQuantity(), //
                itemMovementBean.getItemMovementQuantityUnit().getFirstUnit(), //
                itemMovementBean.getItemMovementQuantityUnit().getSecondQuantity(), //
                itemMovementBean.getItemMovementQuantityUnit().getSecondUnit(), //
                itemMovementBean.getItemMovementQuantityUnit().getThirdQuantity(), //
                itemMovementBean.getItemMovementQuantityUnit().getThirdUnit(), //
                "", //
                activityItemType.getValue(), //
                oIsFinished, oQuestion, //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue() && (!oWstr.getStringValue().isEmpty())) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isOperationItemFinished(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        return new MOFinishedBean(oIsFinished.getBooleanValue(), oQuestion.getStringValue());
    }

    /**
     * {@inheritDoc}
     */
    public List<CodeLabel> listDowngradedItem(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId, final ActivityItemType activityItemType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listDowngradedItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        TempTableHolder<XxfabPPOTtartremp> ttDowngradedItem = new TempTableHolder<XxfabPPOTtartremp>();
        xxfabPPO.prListeArticleRemplacement(operationItemKey.getEstablishmentKey().getCsoc(), operationItemKey
                .getEstablishmentKey().getCetab(), workstationId, ctx.getIdCtx().getLogin(), operationItemKey
                .getManagementType().getValue(), operationItemKey.getChrono().getPrechro(), operationItemKey
                .getChrono().getChrono(), operationItemKey.getCounter1(), operationItemKey.getCounter2(),
                operationItemKey.getCounter3(), operationItemKey.getCounter4(), activityItemType.getValue(),
                ttDowngradedItem, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        Iterator<XxfabPPOTtartremp> iter = ttDowngradedItem.getListValue().iterator();
        List<CodeLabel> listDowngradedItem = new ArrayList<CodeLabel>();
        CodeLabel downgradedItemCL = null;
        while (iter.hasNext()) {
            XxfabPPOTtartremp downgradedItem = iter.next();
            downgradedItemCL = new CodeLabel(downgradedItem.getCArt(), downgradedItem.getLArt());
            listDowngradedItem.add(downgradedItemCL);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listDowngradedItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
        }
        return listDowngradedItem;
    }

    /**
     * {@inheritDoc}
     */
    public List<ItemMovementBean> listItemMovement(final VIFContext ctx, final OperationItemKey operationItemKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listItemMovement(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        TempTableHolder<XxfabPPOTtxmvtm> ttMovement = new TempTableHolder<XxfabPPOTtxmvtm>();
        xxfabPPO.prListeMvts(operationItemKey.getEstablishmentKey().getCsoc(), operationItemKey.getEstablishmentKey()
                .getCetab(), ctx.getIdCtx().getLogin(), operationItemKey.getManagementType().getValue(),
                operationItemKey.getChrono().getPrechro(), operationItemKey.getChrono().getChrono(), operationItemKey
                        .getCounter1(), operationItemKey.getCounter2(), operationItemKey.getCounter3(),
                operationItemKey.getCounter4(), ttMovement, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        Iterator<XxfabPPOTtxmvtm> iter = ttMovement.getListValue().iterator();
        List<ItemMovementBean> listItemMovement = new ArrayList<ItemMovementBean>();
        ItemMovementBean itemMovementBean = null;
        Date movementDate = null;
        while (iter.hasNext()) {
            XxfabPPOTtxmvtm itemMovement = iter.next();
            movementDate = itemMovement.getDatMvt();
            DateHelper.changeTime(movementDate, TimeHelper.parseTime(itemMovement.getHeurMvt()));
            itemMovementBean = new ItemMovementBean(new ItemMovementKey(operationItemKey.getManagementType(),
                    operationItemKey.getEstablishmentKey(), operationItemKey.getChrono(),
                    operationItemKey.getCounter1(), operationItemKey.getCounter2(), operationItemKey.getCounter3(),
                    operationItemKey.getCounter4(), itemMovement.getRowidMVT(), itemMovement.getNlig()),
                    itemMovement.getLot(), new CodeLabel(itemMovement.getCDepot(), itemMovement.getLDepot()),
                    new CodeLabel(itemMovement.getCArt(), itemMovement.getLArt()), new CodeLabel(
                            itemMovement.getCEmp(), itemMovement.getLEmp()), new CodeLabel(itemMovement.getCMotMvk(),
                            itemMovement.getLMotMvk()), movementDate, new ItemMovementQuantityUnit(
                            itemMovement.getQte1(), itemMovement.getCU1(), itemMovement.getQte2(),
                            itemMovement.getCU2(), itemMovement.getQte3(), itemMovement.getCU3()));
            listItemMovement.add(itemMovementBean);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listItemMovement(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ")");
        }
        return listItemMovement;
    }

    /**
     * {@inheritDoc}
     * 
     * @deprecated
     */
    @Deprecated
    public List<ItemMovementBean> listItemMovement(final VIFContext ctx, final OperationItemKey operationItemKey,
            final ActivityItemType activityItemType) throws ServerBusinessException {
        return listItemMovement(ctx, operationItemKey);
    }

    /**
     * {@inheritDoc}
     */
    public List<String> listMOBatch(final VIFContext ctx, final AbstractOperationItemBean operationItemBean)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listMOBatch(ctx=" + ctx + ", operationItemBean=" + operationItemBean + ")");
        }
        List<String> listMOBatch = new ArrayList<String>();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        TempTableHolder<XxfabPPOTtlot> ttTOItemDetail = new TempTableHolder<XxfabPPOTtlot>();
        xxfabPPO.prListeLotsOf(operationItemBean.getOperationItemKey().getEstablishmentKey().getCsoc(),
                operationItemBean.getOperationItemKey().getEstablishmentKey().getCetab(), operationItemBean
                        .getOperationItemKey().getChrono().getPrechro(), operationItemBean.getOperationItemKey()
                        .getChrono().getChrono(), operationItemBean.getOperationItemKey().getCounter1(),
                operationItemBean.getActivityItemType().getValue(), operationItemBean.getItemCL().getCode(),
                operationItemBean.getDepotCL().getCode(), operationItemBean.getLocationCL().getCode(), ttTOItemDetail);
        Iterator<XxfabPPOTtlot> iter = ttTOItemDetail.getListValue().iterator();
        while (iter.hasNext()) {
            XxfabPPOTtlot toItemDetail = iter.next();
            listMOBatch.add(toItemDetail.getLot());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listMOBatch(ctx=" + ctx + ", operationItemBean=" + operationItemBean + ")");
        }
        return listMOBatch;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Movement> listMvtWithOpenedContainers(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listMvtWithOpenedContainers(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        List<Movement> lst = new ArrayList<Movement>();
        List<Management> l = new ArrayList<Management>();
        l.add(Management.REAL);
        MovementSelection sel = new MovementSelection();
        sel.setManagements(l);
        sel.getLocation().setCsoc(operationItemKey.getEstablishmentKey().getCsoc());
        sel.getLocation().setCetab(operationItemKey.getEstablishmentKey().getCetab());
        sel.getMovementAct().setPrechro(operationItemKey.getChrono().getPrechro());
        sel.getMovementAct().setChrono(operationItemKey.getChrono().getChrono());
        sel.getMovementAct().setNumber1(operationItemKey.getCounter1());
        sel.getMovementAct().setNumber2(operationItemKey.getCounter2());
        sel.getMovementAct().setNumber3(operationItemKey.getCounter3());
        sel.getMovementAct().setNumber4(operationItemKey.getCounter4());
        sel.getSens().clear();
        sel.getSens().add(Sens.OUTPUT);
        sel.setWorkStationId(workstationId);
        List<StockValidation> l2 = new ArrayList<StockValidation>();
        l2.add(StockValidation.TO_VALIDATE);
        l2.add(StockValidation.VALIDATED);
        sel.setStockValidation(l2);
        lst = getEntitySBS().listMvtWithOpenedContainers(ctx, sel);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listMvtWithOpenedContainers(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        return lst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<Movement> listMvtWithOpenedUpperContainers(final VIFContext ctx,
            final OperationItemKey operationItemKey, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listMvtWithOpenedUpperContainers(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        List<Management> l = new ArrayList<Management>();
        l.add(Management.REAL);
        MovementSelection sel = new MovementSelection();
        sel.setManagements(l);
        sel.getLocation().setCsoc(operationItemKey.getEstablishmentKey().getCsoc());
        sel.getLocation().setCetab(operationItemKey.getEstablishmentKey().getCetab());
        sel.getMovementAct().setPrechro(operationItemKey.getChrono().getPrechro());
        sel.getMovementAct().setChrono(operationItemKey.getChrono().getChrono());
        sel.getMovementAct().setNumber1(operationItemKey.getCounter1());
        sel.getMovementAct().setNumber2(operationItemKey.getCounter2());
        sel.getMovementAct().setNumber3(operationItemKey.getCounter3());
        sel.getMovementAct().setNumber4(operationItemKey.getCounter4());
        sel.getSens().clear();
        sel.getSens().add(Sens.OUTPUT);
        sel.setWorkStationId(workstationId);
        List<StockValidation> l2 = new ArrayList<StockValidation>();
        l2.add(StockValidation.TO_VALIDATE);
        l2.add(StockValidation.VALIDATED);
        sel.setStockValidation(l2);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listMvtWithOpenedUpperContainers(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        return getEntitySBS().listMvtWithOpenedUpperContainers(ctx, sel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> listOpenedContainers(final VIFContext ctx, final MOKey moKey, final String workstationId)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listOpenedContainers(ctx=" + ctx + ", moKey=" + moKey + ", workstationId="
                    + workstationId + ")");
        }
        List<Management> l = new ArrayList<Management>();
        l.add(Management.REAL);
        MovementSelection sel = new MovementSelection();
        sel.setManagements(l);
        sel.getLocation().setCsoc(moKey.getEstablishmentKey().getCsoc());
        sel.getLocation().setCetab(moKey.getEstablishmentKey().getCetab());
        sel.getMovementAct().setPrechro(moKey.getChrono().getPrechro());
        sel.getMovementAct().setChrono(moKey.getChrono().getChrono());
        List<StockValidation> l2 = new ArrayList<StockValidation>();
        l2.add(StockValidation.TO_VALIDATE);
        l2.add(StockValidation.VALIDATED);
        sel.setStockValidation(l2);
        sel.getSens().clear();
        sel.getSens().add(Sens.OUTPUT);
        sel.setWorkStationId(workstationId);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listOpenedContainers(ctx=" + ctx + ", moKey=" + moKey + ", workstationId="
                    + workstationId + ")");
        }
        return getEntitySBS().listOpenedContainers(ctx, sel);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> listOpenedContainers(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listOpenedContainers(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        List<String> lst = new ArrayList<String>();
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug(new StringBuilder().append("listOpenedContainers(ctx, operationItemKey = ")
                    .append(operationItemKey).append(", workstationId = ").append(workstationId).append(")").toString());
        }
        List<Management> l = new ArrayList<Management>();
        l.add(Management.REAL);
        MovementSelection sel = new MovementSelection();
        sel.setManagements(l);
        sel.getLocation().setCsoc(operationItemKey.getEstablishmentKey().getCsoc());
        sel.getLocation().setCetab(operationItemKey.getEstablishmentKey().getCetab());
        sel.getMovementAct().setPrechro(operationItemKey.getChrono().getPrechro());
        sel.getMovementAct().setChrono(operationItemKey.getChrono().getChrono());
        sel.getMovementAct().setNumber1(operationItemKey.getCounter1());
        sel.getMovementAct().setNumber2(operationItemKey.getCounter2());
        sel.getMovementAct().setNumber3(operationItemKey.getCounter3());
        sel.getMovementAct().setNumber4(operationItemKey.getCounter4());
        sel.getSens().clear();
        sel.getSens().add(Sens.OUTPUT);
        sel.setWorkStationId(workstationId);
        List<StockValidation> l2 = new ArrayList<StockValidation>();
        l2.add(StockValidation.TO_VALIDATE);
        l2.add(StockValidation.VALIDATED);
        sel.setStockValidation(l2);
        lst = getEntitySBS().listOpenedContainers(ctx, sel);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listOpenedContainers(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        return lst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> listOpenedUpperContainers(final VIFContext ctx, final MOKey moKey, final String workstationId)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listOpenedUpperContainers(ctx=" + ctx + ", moKey=" + moKey + ", workstationId="
                    + workstationId + ")");
        }
        List<String> lst = new ArrayList<String>();
        List<Management> l = new ArrayList<Management>();
        l.add(Management.REAL);
        MovementSelection sel = new MovementSelection();
        sel.setManagements(l);
        sel.getLocation().setCsoc(moKey.getEstablishmentKey().getCsoc());
        sel.getLocation().setCetab(moKey.getEstablishmentKey().getCetab());
        sel.getMovementAct().setPrechro(moKey.getChrono().getPrechro());
        sel.getMovementAct().setChrono(moKey.getChrono().getChrono());
        sel.getSens().clear();
        sel.getSens().add(Sens.OUTPUT);
        sel.setWorkStationId(workstationId);
        List<StockValidation> l2 = new ArrayList<StockValidation>();
        l2.add(StockValidation.TO_VALIDATE);
        l2.add(StockValidation.VALIDATED);
        sel.setStockValidation(l2);
        lst = getEntitySBS().listOpenedUpperContainers(ctx, sel);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listOpenedUpperContainers(ctx=" + ctx + ", moKey=" + moKey + ", workstationId="
                    + workstationId + ")");
        }
        return lst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> listOpenedUpperContainers(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listOpenedUpperContainers(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        List<Management> l = new ArrayList<Management>();
        l.add(Management.REAL);
        MovementSelection sel = new MovementSelection();
        sel.setManagements(l);
        sel.getLocation().setCsoc(operationItemKey.getEstablishmentKey().getCsoc());
        sel.getLocation().setCetab(operationItemKey.getEstablishmentKey().getCetab());
        sel.getMovementAct().setPrechro(operationItemKey.getChrono().getPrechro());
        sel.getMovementAct().setChrono(operationItemKey.getChrono().getChrono());
        sel.getMovementAct().setNumber1(operationItemKey.getCounter1());
        sel.getMovementAct().setNumber2(operationItemKey.getCounter2());
        sel.getMovementAct().setNumber3(operationItemKey.getCounter3());
        sel.getMovementAct().setNumber4(operationItemKey.getCounter4());
        sel.getSens().clear();
        sel.getSens().add(Sens.OUTPUT);
        sel.setWorkStationId(workstationId);
        List<StockValidation> l2 = new ArrayList<StockValidation>();
        l2.add(StockValidation.TO_VALIDATE);
        l2.add(StockValidation.VALIDATED);
        sel.setStockValidation(l2);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listOpenedUpperContainers(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        return getEntitySBS().listOpenedUpperContainers(ctx, sel);
    }

    /**
     * {@inheritDoc}
     */
    public List<AbstractOperationItemBean> listOperationItem(final VIFContext ctx,
            final EstablishmentKey establishmentKey, final List<Chrono> listMoChrono, final OperationItemSBean selection)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listOperationItem(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", listMoChrono=" + listMoChrono + ", selection=" + selection + ")");
        }
        List<AbstractOperationItemBean> listOperationItem = new ArrayList<AbstractOperationItemBean>();
        try {
            List<Xpiloia> listXpiloia = XpiloiaDAOFactory.getDAO().listOperationItems(ctx.getDbCtx(), establishmentKey,
                    listMoChrono, selection);
            for (Xpiloia xpiloia : listXpiloia) {
                if (ActivityItemType.INPUT.getValue().equals(xpiloia.getTypacta())) {
                    listOperationItem.add(new InputOperationItemBean(ManagementType.getManagementType(xpiloia
                            .getTypgest()), new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(
                                    xpiloia.getPrechrof(), xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia
                                    .getNi3(), xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia
                                            .getLart(), xpiloia.getRart()), new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                                            new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(),
                                            new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                    .getQtartrest1(), xpiloia.getCuart1()),
                                                    TouchDeclarationType.WITHOUT_QTY_TOUCH_DECLARATION.getValue()
                                    .equals(xpiloia.getTypdecatl())));
                } else {
                    listOperationItem.add(new OutputOperationItemBean(ManagementType.getManagementType(xpiloia
                            .getTypgest()), new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(
                            xpiloia.getPrechrof(), xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia
                            .getNi3(), xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia
                            .getLart(), xpiloia.getRart()), new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                            new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(),
                            new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                    .getQtartrest1(), xpiloia.getCuart1())));
                }
            }
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(new StringBuilder().append("Unable to list operation items of manufacturing orders ")
                        .append(establishmentKey.getCsoc()).append("-").append(establishmentKey.getCetab()).append("-")
                        .append(listMoChrono).toString(), e);
            }
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listOperationItem(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", listMoChrono=" + listMoChrono + ", selection=" + selection + ")");
        }
        return listOperationItem;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AbstractOperationItemBean> listOperationItem(final VIFContext ctx,
            final EstablishmentKey establishmentKey, final OperationItemSBean selection) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listOperationItem(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", selection="
                    + selection + ")");
        }
        List<AbstractOperationItemBean> listOperationItemBean = new ArrayList<AbstractOperationItemBean>();
        try {
            List<Xpiloia> listXpiloia = XpiloiaDAOFactory.getDAO().listOperationItems(ctx.getDbCtx(), establishmentKey,
                    selection);
            for (Xpiloia xpiloia : listXpiloia) {
                if (ActivityItemType.INPUT.getValue().equals(xpiloia.getTypacta())) {
                    listOperationItemBean.add(new InputOperationItemBean(ManagementType.getManagementType(xpiloia
                            .getTypgest()), new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(
                                    xpiloia.getPrechrof(), xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia
                                    .getNi3(), xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia
                                            .getLart(), xpiloia.getRart()), new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                                            new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(),
                                            new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                                    .getQtartrest1(), xpiloia.getCuart1()),
                                                    TouchDeclarationType.WITHOUT_QTY_TOUCH_DECLARATION.getValue()
                                                    .equals(xpiloia.getTypdecatl())));

                } else {
                    listOperationItemBean.add(new OutputOperationItemBean(ManagementType.getManagementType(xpiloia
                            .getTypgest()), new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(
                            xpiloia.getPrechrof(), xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia
                            .getNi3(), xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia
                            .getLart(), xpiloia.getRart()), new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                            new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(),
                            new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                                    .getQtartrest1(), xpiloia.getCuart1())));
                }
            }
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(new StringBuilder().append("Unable to list operation items of manufacturing order ")
                        .toString(), e);
            }
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listOperationItem(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", selection="
                    + selection + ")");
        }
        return listOperationItemBean;
    }

    /**
     * {@inheritDoc}
     */
    public List<AbstractOperationItemBean> listOperationItem(final VIFContext ctx, final MOKey moKey,
            final OperationItemSBean selection) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listOperationItem(ctx=" + ctx + ", moKey=" + moKey + ", selection=" + selection + ")");
        }
        List<AbstractOperationItemBean> listOperationItemBean = new ArrayList<AbstractOperationItemBean>();
        try {
            List<Xpiloia> listXpiloia = XpiloiaDAOFactory.getDAO().listOperationItems(ctx.getDbCtx(), moKey, selection);
            for (Xpiloia xpiloia : listXpiloia) {
                // prxy
                if (ActivityItemType.INPUT.getValue().equals(xpiloia.getTypacta())) {
                    listOperationItemBean.add(new InputOperationItemBean(ManagementType.getManagementType(xpiloia
                            .getTypgest()), new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(
                            xpiloia.getPrechrof(), xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia
                            .getNi3(), xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia
                            .getLart(), xpiloia.getRart()), new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                            new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(),
                            new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                    .getQtartrest1(), xpiloia.getCuart1()), xpiloia.getTauto(), xpiloia.getLstnatstk(),
                            TouchDeclarationType.WITHOUT_QTY_TOUCH_DECLARATION.getValue()
                                    .equals(xpiloia.getTypdecatl()), TouchDeclarationType
                                    .getTouchDeclarationType(xpiloia.getTypdecatl())));
                } else {
                    listOperationItemBean.add(new OutputOperationItemBean(ManagementType.getManagementType(xpiloia
                            .getTypgest()), new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(
                            xpiloia.getPrechrof(), xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia
                            .getNi3(), xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia
                            .getLart(), xpiloia.getRart()), new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                            new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(),
                            new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                    .getQtartrest1(), xpiloia.getCuart1()), xpiloia.getTauto(), xpiloia.getLstnatstk(),
                            TouchDeclarationType.getTouchDeclarationType(xpiloia.getTypdecatl())));
                }
            }
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(
                        new StringBuilder().append("Unable to list operation items of manufacturing order ")
                                .append(moKey.getEstablishmentKey().getCsoc()).append("-")
                                .append(moKey.getEstablishmentKey().getCetab()).append("-")
                                .append(moKey.getChrono().getPrechro()).append("-")
                                .append(moKey.getChrono().getChrono()).toString(), e);
            }
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listOperationItem(ctx=" + ctx + ", moKey=" + moKey + ", selection=" + selection + ")");
        }
        return listOperationItemBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AbstractOperationItemBean> listOperationItem(final VIFContext ctx, final MOKey moKey,
            final OperationItemSBean selection, final FProductionOutputCompSBean selectionCompSBean)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listOperationItem(ctx=" + ctx + ", moKey=" + moKey + ", selection=" + selection
                    + ", selectionCompSBean=" + selectionCompSBean + ")");
        }
        List<AbstractOperationItemBean> listOperationItemBean = new ArrayList<AbstractOperationItemBean>();
        try {
            // --------------------------------------
            // Load the xpiloia
            // --------------------------------------
            List<Xpiloia> listXpiloia = XpiloiaDAOFactory.getDAO().listOperationItems(ctx.getDbCtx(), moKey, selection,
                    MONatureComplementaryWorkshop.getMONatureComplementaryWorkshop(selectionCompSBean.getState()));
            if (listXpiloia != null && !listXpiloia.isEmpty()) {
                for (Xpiloia xpiloia : listXpiloia) {
                    listOperationItemBean.addAll(this.getOperationItem(ctx, xpiloia, MONatureComplementaryWorkshop
                            .getMONatureComplementaryWorkshop(selectionCompSBean.getState()), null));
                }
            }
        } catch (DAOException e) {
            LOGGER.error("getOperationItem(ctx=" + ctx + ", moKey=" + moKey + ", selection=" + selection
                    + ", selectionCompSBean=" + selectionCompSBean + ") - " + e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listOperationItem(ctx=" + ctx + ", moKey=" + moKey + ", selection=" + selection
                    + ", selectionCompSBean=" + selectionCompSBean + ")=" + null);
        }
        return listOperationItemBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<AbstractOperationItemBean> listProgressingOperationItem(final VIFContext ctx,
            final EstablishmentKey establishmentKey, final ActivityItemType activityItemType, final String workstationId)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listProgressingOperationItem(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        List<AbstractOperationItemBean> listOperationItemBean = new ArrayList<AbstractOperationItemBean>();
        try {
            List<Xpiloia> listXpiloia = XpiloiaDAOFactory.getDAO().listProgressingOperationItems(ctx.getDbCtx(),
                    establishmentKey, activityItemType, workstationId);
            for (Xpiloia xpiloia : listXpiloia) {
                if (ActivityItemType.INPUT.getValue().equals(xpiloia.getTypacta())) {
                    listOperationItemBean.add(new InputOperationItemBean(ManagementType.getManagementType(xpiloia
                            .getTypgest()), new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(
                            xpiloia.getPrechrof(), xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia
                            .getNi3(), xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia
                            .getLart(), xpiloia.getRart()), new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                            new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(),
                            new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                    .getQtartrest1(), xpiloia.getCuart1()),
                            TouchDeclarationType.WITHOUT_QTY_TOUCH_DECLARATION.getValue()
                                    .equals(xpiloia.getTypdecatl())));
                } else {
                    listOperationItemBean.add(new OutputOperationItemBean(ManagementType.getManagementType(xpiloia
                            .getTypgest()), new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(
                            xpiloia.getPrechrof(), xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia
                            .getNi3(), xpiloia.getNi4(), xpiloia.getLot(), new CodeLabels(xpiloia.getCart(), xpiloia
                            .getLart(), xpiloia.getRart()), new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                            new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(),
                            new OperationItemQuantityUnit(xpiloia.getQtartafer1(), xpiloia.getQtartfait1(), xpiloia
                                    .getQtartrest1(), xpiloia.getCuart1())));
                }
            }
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(
                        new StringBuilder().append("Unable to listProgressingOperationItem(ctx, establishmentKey = ")
                                .append(establishmentKey).append(", activityItemType = ").append(activityItemType)
                                .append(", workstationId = ").append(workstationId).append(")").toString(), e);
            }
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listProgressingOperationItem(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        return listOperationItemBean;
    }

    /**
     * {@inheritDoc}
     */
    public List<CodeLabel> listReasonByType(final VIFContext ctx, final String companyId, final ReasonType reasonType)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listReasonByType(ctx=" + ctx + ", companyId=" + companyId + ", reasonType=" + reasonType
                    + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        TempTableHolder<XxfabPPOTtmotmvk> ttReason = new TempTableHolder<XxfabPPOTtmotmvk>();
        xxfabPPO.prListeMotifsParNature(companyId, reasonType.getValue(), ttReason, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        Iterator<XxfabPPOTtmotmvk> iter = ttReason.getListValue().iterator();
        List<CodeLabel> listReason = new ArrayList<CodeLabel>();
        CodeLabel reasonCL = null;
        while (iter.hasNext()) {
            XxfabPPOTtmotmvk reason = iter.next();
            reasonCL = new CodeLabel(reason.getCMotMvk(), reason.getLMotMvk());
            listReason.add(reasonCL);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listReasonByType(ctx=" + ctx + ", companyId=" + companyId + ", reasonType=" + reasonType
                    + ")");
        }
        return listReason;
    }

    /**
     * {@inheritDoc}
     */
    public List<CodeLabel> listSubstituteItem(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId, final ActivityItemType activityItemType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listSubstituteItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        TempTableHolder<XxfabPPOTtartremp> ttSubstituteItem = new TempTableHolder<XxfabPPOTtartremp>();
        xxfabPPO.prListeArticleRemplacement(operationItemKey.getEstablishmentKey().getCsoc(), operationItemKey
                .getEstablishmentKey().getCetab(), workstationId, ctx.getIdCtx().getLogin(), operationItemKey
                .getManagementType().getValue(), operationItemKey.getChrono().getPrechro(), operationItemKey
                .getChrono().getChrono(), operationItemKey.getCounter1(), operationItemKey.getCounter2(),
                operationItemKey.getCounter3(), operationItemKey.getCounter4(), activityItemType.getValue(),
                ttSubstituteItem, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        Iterator<XxfabPPOTtartremp> iter = ttSubstituteItem.getListValue().iterator();
        List<CodeLabel> listSubstituteItem = new ArrayList<CodeLabel>();
        CodeLabel substituteItemCL = null;
        while (iter.hasNext()) {
            XxfabPPOTtartremp substituteItem = iter.next();
            substituteItemCL = new CodeLabel(substituteItem.getCArt(), substituteItem.getLArt());
            listSubstituteItem.add(substituteItemCL);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listSubstituteItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
        }
        return listSubstituteItem;
    }

    @Override
    public List<PrintDocument> printAllLabels(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String itemId, final String mvtRowid, final ActivityItemType itemType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - printAllInputLabels(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", itemId="
                    + itemId + ", mvtRowid=" + mvtRowid + ", itemType=" + itemType + ")");
        }
        List<PrintDocument> documents = new ArrayList<PrintDocument>();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        TempTableHolder<XxfabPPOTtLocaldocprg> ttUnprintedHolder = new TempTableHolder<XxfabPPOTtLocaldocprg>();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prEditeEtiquettesEntreeSortie(operationItemKey.getEstablishmentKey().getCsoc(), //
                operationItemKey.getEstablishmentKey().getCetab(), //
                ctx.getIdCtx().getWorkstation(), //
                ctx.getIdCtx().getLogin(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                itemId, //
                mvtRowid, //
                itemType.getValue(), ttUnprintedHolder, //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        for (XxfabPPOTtLocaldocprg doc : ttUnprintedHolder.getListValue()) {
            documents.add(new PrintDocument(doc.getCdoc(), PrintEvent.getPrintEvent(doc.getEvent()), doc.getNbdoc()));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - printAllInputLabels(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", itemId="
                    + itemId + ", mvtRowid=" + mvtRowid + ", itemType=" + itemType + ")=" + documents);
        }
        return documents;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<PrintDocument> printMovement(final VIFContext ctx, final EstablishmentKey estblishmentKey,
            final MovementAct movementAct) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - printMovement(ctx=" + ctx + ", estblishmentKey=" + estblishmentKey + ", movementAct="
                    + movementAct + ")");
        }
        List<PrintDocument> lst = new ArrayList<PrintDocument>();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        TempTableHolder<XxfabPPOTtLocaldoc> ttUnprintedHolder = new TempTableHolder<XxfabPPOTtLocaldoc>();
        xxfabPPO.prPrintfindMovement(estblishmentKey.getCsoc(), //
                estblishmentKey.getCetab(), //
                ctx.getIdCtx().getWorkstation(), //
                ctx.getIdCtx().getLogin(), //
                movementAct.getPrechro(), //
                movementAct.getChrono(), //
                movementAct.getNumber1(), //
                movementAct.getNumber2(), //
                movementAct.getNumber3(), //
                movementAct.getNumber4(), //
                movementAct.getLineNumber(), //
                ttUnprintedHolder, oRet, //
                oWstr);
        //
        if (!oRet.getBooleanValue()) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("printMovementDocument :" + oWstr.getStringValue());
            }
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        for (XxfabPPOTtLocaldoc xxfabPPOTtLocaldoc : ttUnprintedHolder.getListValue()) {
            lst.add(new PrintDocument(xxfabPPOTtLocaldoc.getCdoc(), PrintEvent.getPrintEvent(xxfabPPOTtLocaldoc
                    .getEvent()), xxfabPPOTtLocaldoc.getNbdoc()));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - printMovement(ctx=" + ctx + ", estblishmentKey=" + estblishmentKey + ", movementAct="
                    + movementAct + ")");
        }
        return lst;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void printMovement(final VIFContext ctx, final EstablishmentKey establishmentKey, final MovementAct act,
            final List<PrintDocument> documents) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - printMovement(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", act=" + act
                    + ", documents=" + documents + ")");
        }
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        List<XxfabPPOTtLocaldoc> docs = new ArrayList<XxfabPPOTtLocaldoc>();
        for (PrintDocument document : documents) {
            XxfabPPOTtLocaldoc e = new XxfabPPOTtLocaldoc();
            e.setCdoc(document.getDocumentId());
            e.setEvent(document.getPrintEvent().getValue());
            e.setNbdoc(document.getDocumentCount());
            docs.add(e);
        }
        xxfabPPO.prPrintMovement(ctx.getIdCtx().getWorkstation(), //
                ctx.getIdCtx().getLogin(), //
                establishmentKey.getCsoc(), //
                establishmentKey.getCetab(), //
                act.getPrechro(), //
                act.getChrono(), //
                act.getNumber1(), //
                act.getNumber2(), //
                act.getNumber3(), //
                act.getNumber4(), //
                act.getLineNumber(), //
                docs, //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue()) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("printMovement :" + oWstr.getStringValue());
            }
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - printMovement(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", act=" + act
                    + ", documents=" + documents + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public void printMovementLabel(final VIFContext ctx, final RowidKey rowidKey, final String workstationId,
            final ActivityItemType activityItemType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - printMovementLabel(ctx=" + ctx + ", rowidKey=" + rowidKey + ", workstationId="
                    + workstationId + ", activityItemType=" + activityItemType + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prEditeEtiquette(ctx.getIdCtx().getCompany(), ctx.getIdCtx().getEstablishment(), workstationId, ctx
                .getIdCtx().getLogin(), rowidKey.getRowid(), activityItemType.getValue(), oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - printMovementLabel(ctx=" + ctx + ", rowidKey=" + rowidKey + ", workstationId="
                    + workstationId + ", activityItemType=" + activityItemType + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void runPrintActLabel(final VIFContext ctx, final MOKey moKey, final String workstationId,
            final ActivityItemType activityItemType, final String labelId, final String printProgramId,
            final int copiesNumber) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - runPrintActLabel(ctx=" + ctx + ", moKey=" + moKey + ", workstationId=" + workstationId
                    + ", activityItemType=" + activityItemType + ", labelId=" + labelId + ", printProgramId="
                    + printProgramId + ", copiesNumber=" + copiesNumber + ")");
        }
        BooleanHolder oReturnValue = new BooleanHolder();
        StringHolder oReturnMessage = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prLanceEditeEtiquetteActe(moKey.getEstablishmentKey().getCsoc(), moKey.getEstablishmentKey()
                .getCetab(), workstationId, ctx.getIdCtx().getLogin(), moKey.getChrono().getPrechro(), moKey
                .getChrono().getChrono(), activityItemType.getValue(), labelId, printProgramId, copiesNumber,
                oReturnValue, oReturnMessage);
        if (!oReturnValue.getBooleanValue()) {
            throw new ProxyBusinessException(oReturnMessage.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - runPrintActLabel(ctx=" + ctx + ", moKey=" + moKey + ", workstationId=" + workstationId
                    + ", activityItemType=" + activityItemType + ", labelId=" + labelId + ", printProgramId="
                    + printProgramId + ", copiesNumber=" + copiesNumber + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void runPrintMovementLabel(final VIFContext ctx, final RowidKey rowidKey, final String workstationId,
            final ActivityItemType activityItemType, final String labelId, final String printProgramId,
            final int copiesNumber) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - runPrintMovementLabel(ctx=" + ctx + ", rowidKey=" + rowidKey + ", workstationId="
                    + workstationId + ", activityItemType=" + activityItemType + ", labelId=" + labelId
                    + ", printProgramId=" + printProgramId + ", copiesNumber=" + copiesNumber + ")");
        }
        BooleanHolder oReturnValue = new BooleanHolder();
        StringHolder oReturnMessage = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prLanceEditeEtiquette(ctx.getIdCtx().getCompany(), ctx.getIdCtx().getEstablishment(), workstationId,
                ctx.getIdCtx().getLogin(), rowidKey.getRowid(), activityItemType.getValue(), labelId, printProgramId,
                copiesNumber, oReturnValue, oReturnMessage);
        if (!oReturnValue.getBooleanValue()) {
            throw new ProxyBusinessException(oReturnMessage.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - runPrintMovementLabel(ctx=" + ctx + ", rowidKey=" + rowidKey + ", workstationId="
                    + workstationId + ", activityItemType=" + activityItemType + ", labelId=" + labelId
                    + ", printProgramId=" + printProgramId + ", copiesNumber=" + copiesNumber + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputUpdateReturnBean saveInputEntity(final VIFContext ctx, final OperationItemKey operationItemKey,
            final CEntityBean entity, final InputWorkstationParameters inputWorkstationParameters,
            final InputItemParameters inputItemParameters) throws ServerBusinessException {
        return this.saveInputEntity(ctx, operationItemKey, entity, inputWorkstationParameters, inputItemParameters, "");
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public InputUpdateReturnBean saveInputEntity(final VIFContext ctx, final OperationItemKey operationItemKey,
            final CEntityBean entity, final InputWorkstationParameters inputWorkstationParameters,
            final InputItemParameters inputItemParameters, final String workshopType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveInputEntity(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", entity="
                    + entity + ", inputWorkstationParameters=" + inputWorkstationParameters + ", inputItemParameters="
                    + inputItemParameters + ")");
        }
        InputUpdateReturnBean ret = new InputUpdateReturnBean();
        CEntityBean e = entity;
        StringHolder oRowid = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        StringHolder ioNSC = new StringHolder();
        StringHolder ioNSCP = new StringHolder();
        IntegerHolder oNlig = new IntegerHolder();
        ioNSC.setStringValue(entity.getEntityLocationBean().getEntityLocationKey().getContainerNumber());
        ioNSCP.setStringValue(entity.getEntityLocationBean().getEntityLocationKey().getUpperContainerNumber());

        TempTableHolder<XxfabPPOTtLocaldocprg> ttUnprintedHolder = new TempTableHolder<XxfabPPOTtLocaldocprg>();

        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prValidateMvtEntree3(entity.getEntityLocationBean().getEntityLocationKey().getCsoc(), entity
                .getEntityLocationBean().getEntityLocationKey().getCetab(), //
                ctx.getIdCtx().getWorkstation(), //
                ctx.getIdCtx().getLogin(), //
                operationItemKey.getManagementType().getValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), //
                operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4(), //
                ActivityItemType.INPUT.getValue(), //
                entity.getEntityLocationBean().getEntityLocationKey().getContainerNumber(), //
                entity.getEntityLocationBean().getContainerPackaging().getPackagingId(), //
                entity.getEntityLocationBean().getEntityLocationKey().getUpperContainerNumber(), //
                entity.getEntityLocationBean().getContainerPackaging().getUnitPackagingId(), //
                entity.getStockItemBean().getStockItem().getUniqueNumber(), //
                entity.getStockItemBean().getStockItem().getItemId(), //
                entity.getStockItemBean().getStockItem().getBatch(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCdepot(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCemp(), //
                "", //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit(), //
                inputWorkstationParameters.getInputTareType().getValue(), //
                workshopType, //
                ttUnprintedHolder, //
                oRowid, //
                oNlig, //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        MovementAct movementAct = new MovementAct(oNlig.getIntegerValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4());
        ret.setMovementAct(movementAct);
        e.getEntityLocationBean().getEntityLocationKey().setContainerNumber(ioNSC.getStringValue());
        e.getEntityLocationBean().getEntityLocationKey().setUpperContainerNumber(ioNSCP.getStringValue());
        ret.setEntity(e);
        ret.setMvtRowid(oRowid.getStringValue());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveInputEntity(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", entity="
                    + entity + ", inputWorkstationParameters=" + inputWorkstationParameters + ", inputItemParameters="
                    + inputItemParameters + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Deprecated
    public String saveItemMovement(final VIFContext ctx, final ItemMovementBean itemMovementBean,
            final ActivityItemType activityItemType, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveItemMovement(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        StringHolder oRowid = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prValidateMvt(
                ctx.getIdCtx().getCompany(),
                ctx.getIdCtx().getEstablishment(),
                workstationId,
                ctx.getIdCtx().getLogin(),
                itemMovementBean.getItemMovementKey().getManagementType().getValue(),
                itemMovementBean.getItemMovementKey().getChrono().getPrechro(),
                itemMovementBean.getItemMovementKey().getChrono().getChrono(),
                itemMovementBean.getItemMovementKey().getCounter1(),
                itemMovementBean.getItemMovementKey().getCounter2(),
                itemMovementBean.getItemMovementKey().getCounter3(),
                itemMovementBean.getItemMovementKey().getCounter4(),
                activityItemType.getValue(),
                itemMovementBean.getItemCL().getCode(),
                itemMovementBean.getBatch(),
                itemMovementBean.getDepotCL().getCode(),
                ((itemMovementBean.getLocationCL().getCode() != null) ? itemMovementBean.getLocationCL().getCode() : ""),
                ((itemMovementBean.getReasonCL().getCode() != null) ? itemMovementBean.getReasonCL().getCode() : ""),
                itemMovementBean.getItemMovementQuantityUnit().getFirstQuantity(), itemMovementBean
                        .getItemMovementQuantityUnit().getFirstUnit(), itemMovementBean.getItemMovementQuantityUnit()
                        .getSecondQuantity(), itemMovementBean.getItemMovementQuantityUnit().getSecondUnit(),
                itemMovementBean.getItemMovementQuantityUnit().getThirdQuantity(), itemMovementBean
                        .getItemMovementQuantityUnit().getThirdUnit(), oRowid, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveItemMovement(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        return oRowid.getStringValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String saveItemMovementComplete(final VIFContext ctx, final ItemMovementBean itemMovementBean,
            final ActivityItemType activityItemType, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveItemMovementComplete(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        StringHolder oRowid = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prValidateMvtComplete(
                ctx.getIdCtx().getCompany(),
                ctx.getIdCtx().getEstablishment(),
                workstationId,
                ctx.getIdCtx().getLogin(),
                itemMovementBean.getItemMovementKey().getManagementType().getValue(),
                itemMovementBean.getItemMovementKey().getChrono().getPrechro(),
                itemMovementBean.getItemMovementKey().getChrono().getChrono(),
                itemMovementBean.getItemMovementKey().getCounter1(),
                itemMovementBean.getItemMovementKey().getCounter2(),
                itemMovementBean.getItemMovementKey().getCounter3(),
                itemMovementBean.getItemMovementKey().getCounter4(),
                activityItemType.getValue(),
                itemMovementBean.getItemCL().getCode(),
                itemMovementBean.getBatch(),
                itemMovementBean.getDepotCL().getCode(),
                ((itemMovementBean.getLocationCL().getCode() != null) ? itemMovementBean.getLocationCL().getCode() : ""),
                ((itemMovementBean.getReasonCL().getCode() != null) ? itemMovementBean.getReasonCL().getCode() : ""),
                itemMovementBean.getItemMovementQuantityUnit().getFirstQuantity(), itemMovementBean
                        .getItemMovementQuantityUnit().getFirstUnit(), itemMovementBean.getItemMovementQuantityUnit()
                        .getSecondQuantity(), itemMovementBean.getItemMovementQuantityUnit().getSecondUnit(),
                itemMovementBean.getItemMovementQuantityUnit().getThirdQuantity(), itemMovementBean
                        .getItemMovementQuantityUnit().getThirdUnit(), oRowid, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveItemMovementComplete(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", activityItemType=" + activityItemType + ", workstationId=" + workstationId + ")");
        }
        return oRowid.getStringValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OutputUpdateReturnBean saveOutputEntity(final VIFContext ctx, final OperationItemKey operationItemKey,
            final CEntityBean entity, final OutputWorkstationParameters outputWorkstationParameters,
            final OutputItemParameters outputItemParameters, final CodeLabel motifCL, final Chrono chronoDestination)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveOutputEntity(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", entity="
                    + entity + ", outputWorkstationParameters=" + outputWorkstationParameters
                    + ", outputItemParameters=" + outputItemParameters + ")");
        }
        OutputUpdateReturnBean ret = new OutputUpdateReturnBean();
        CEntityBean e = entity;
        StringHolder oRowid = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        StringHolder ioNSC = new StringHolder();
        StringHolder ioNSCP = new StringHolder();
        IntegerHolder oNLig = new IntegerHolder();
        String motif = (motifCL != null && StringUtils.isNotEmpty(motifCL.getCode())) ? motifCL.getCode() : "";
        BooleanHolder oNscClosed = new BooleanHolder();
        BooleanHolder oNscpClosed = new BooleanHolder();
        ioNSC.setStringValue(entity.getEntityLocationBean().getEntityLocationKey().getContainerNumber());
        ioNSCP.setStringValue(entity.getEntityLocationBean().getEntityLocationKey().getUpperContainerNumber());
        TempTableHolder<XxfabPPOTtLocaldocprg> ttUnprintedHolder = new TempTableHolder<XxfabPPOTtLocaldocprg>();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prValidateMvtSortie(entity.getEntityLocationBean().getEntityLocationKey().getCsoc(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCetab(), //
                ctx.getIdCtx().getWorkstation(), //
                ctx.getIdCtx().getLogin(), //
                operationItemKey.getManagementType().getValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), //
                operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4(), //
                ActivityItemType.OUTPUT.getValue(), //
                entity.getEntityLocationBean().getContainerPackaging().getPackagingId(), //
                entity.getEntityLocationBean().getContainerPackaging().getTattooedId(), //
                0.0, //
                "", //
                entity.getEntityLocationBean().getUpperPackaging().getPackagingId(), //
                entity.getEntityLocationBean().getUpperPackaging().getTattooedId(), //
                0.0, //
                "", //
                entity.getStockItemBean().getStockItem().getItemId(), //
                entity.getStockItemBean().getStockItem().getBatch(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCdepot(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCemp(), //
                motif, //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit(), //
                outputItemParameters.getContainerClosingType().getValue(), //
                outputItemParameters.getContainerCapacityClosingType().getValue(), //
                outputItemParameters.getUpperContainerClosingType().getValue(), //
                outputItemParameters.getUpperContainerCapacityClosingType().getValue(), //
                outputWorkstationParameters.getOutputTareType().getValue(), //
                chronoDestination.getPrechro(), // Prechro of the MO destination UNUSED for the moment
                chronoDestination.getChrono(), // MO destination UNUSED for the moment
                ioNSC, //
                ioNSCP, //
                ttUnprintedHolder, //
                oRowid, //
                oNscClosed, //
                oNscpClosed, //
                oNLig, //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        e.getEntityLocationBean().getEntityLocationKey().setContainerNumber(ioNSC.getStringValue());
        e.getEntityLocationBean().getEntityLocationKey().setUpperContainerNumber(ioNSCP.getStringValue());
        for (XxfabPPOTtLocaldocprg doc : ttUnprintedHolder.getListValue()) {
            ret.getDocuments().add(
                    new PrintDocument(doc.getCdoc(), PrintEvent.getPrintEvent(doc.getEvent()), doc.getNbdoc()));
        }
        ret.setContainerClosed(oNscClosed.getBooleanValue());
        ret.setUpperContainerClosed(oNscpClosed.getBooleanValue());
        // Last data entry
        MovementAct movementAct = new MovementAct(oNLig.getIntegerValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4());
        ret.setMovementAct(movementAct);
        ret.setEntity(e);
        ret.setMvtRowid(oRowid.getStringValue());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveOutputEntity(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", entity="
                    + entity + ", outputWorkstationParameters=" + outputWorkstationParameters
                    + ", outputItemParameters=" + outputItemParameters + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OutputUpdateReturnBean saveOutputEntity(final VIFContext ctx, final OperationItemKey operationItemKey,
            final CEntityBean entity, final OutputWorkstationParameters outputWorkstationParameters,
            final OutputItemParameters outputItemParameters, final CodeLabel motifCL, final Chrono chronoDestination,
            final MONatureComplementaryWorkshop natureComplementaryWorkshop) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveOutputEntity(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", entity="
                    + entity + ", outputWorkstationParameters=" + outputWorkstationParameters
                    + ", outputItemParameters=" + outputItemParameters + ", natureComplementaryWorkshop="
                    + natureComplementaryWorkshop + ")");
        }
        // Parameters.
        OutputUpdateReturnBean ret = new OutputUpdateReturnBean();
        CEntityBean cEntityBean = entity;
        StringHolder oRowid = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        StringHolder ioNSC = new StringHolder();
        StringHolder ioNSCP = new StringHolder();
        IntegerHolder oNLig = new IntegerHolder();
        BooleanHolder oNscClosed = new BooleanHolder();
        BooleanHolder oNscpClosed = new BooleanHolder();
        String motif = (motifCL != null && StringUtils.isNotEmpty(motifCL.getCode())) ? motifCL.getCode() : "";
        ioNSC.setStringValue(entity.getEntityLocationBean().getEntityLocationKey().getContainerNumber());
        ioNSCP.setStringValue(entity.getEntityLocationBean().getEntityLocationKey().getUpperContainerNumber());
        TempTableHolder<XxfabPPOTtLocaldocprg> ttUnprintedHolder = new TempTableHolder<XxfabPPOTtLocaldocprg>();
        // Call Proxy.
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prValidateMvtSortieRebut(entity.getEntityLocationBean().getEntityLocationKey().getCsoc(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCetab(), //
                ctx.getIdCtx().getWorkstation(), //
                ctx.getIdCtx().getLogin(), //
                operationItemKey.getManagementType().getValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), //
                operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4(), //
                ActivityItemType.OUTPUT.getValue(), //
                entity.getEntityLocationBean().getContainerPackaging().getPackagingId(), //
                "", //
                0.0, //
                "", //
                entity.getEntityLocationBean().getUpperPackaging().getPackagingId(), //
                "", //
                0.0, //
                "", //
                entity.getStockItemBean().getStockItem().getItemId(), //
                entity.getStockItemBean().getStockItem().getBatch(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCdepot(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCemp(), //
                motif, //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit(), //
                outputItemParameters.getContainerClosingType().getValue(), //
                outputItemParameters.getContainerCapacityClosingType().getValue(), //
                outputItemParameters.getUpperContainerClosingType().getValue(), //
                outputItemParameters.getUpperContainerCapacityClosingType().getValue(), //
                outputWorkstationParameters.getOutputTareType().getValue(), //
                natureComplementaryWorkshop.getValue(), // Nature of the item.
                chronoDestination.getPrechro(), // Prechro of the MO destination UNUSED for the moment
                chronoDestination.getChrono(), // MO destination UNUSED for the moment
                ioNSC, //
                ioNSCP, //
                ttUnprintedHolder, //
                oRowid, //
                oNscClosed, //
                oNscpClosed, //
                oNLig, //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        cEntityBean.getEntityLocationBean().getEntityLocationKey().setContainerNumber(ioNSC.getStringValue());
        cEntityBean.getEntityLocationBean().getEntityLocationKey().setUpperContainerNumber(ioNSCP.getStringValue());
        for (XxfabPPOTtLocaldocprg doc : ttUnprintedHolder.getListValue()) {
            ret.getDocuments().add(
                    new PrintDocument(doc.getCdoc(), PrintEvent.getPrintEvent(doc.getEvent()), doc.getNbdoc()));
        }
        ret.setContainerClosed(oNscClosed.getBooleanValue());
        ret.setUpperContainerClosed(oNscpClosed.getBooleanValue());
        // Last data entry
        MovementAct movementAct = new MovementAct(oNLig.getIntegerValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4());
        ret.setMovementAct(movementAct);
        ret.setEntity(cEntityBean);
        ret.setMvtRowid(oRowid.getStringValue()); // CHU - QATEST 8028
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveOutputEntity(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", entity="
                    + entity + ", outputWorkstationParameters=" + outputWorkstationParameters
                    + ", outputItemParameters=" + outputItemParameters + ", natureComplementaryWorkshop="
                    + natureComplementaryWorkshop + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    public String searchBatchByDestockingModel(final VIFContext ctx, final ItemMovementBean itemMovementBean,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - searchBatchByDestockingModel(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", workstationId=" + workstationId + ")");
        }
        StringHolder oLot = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        double searchBatchByDestockingModel = parameterSBS.readDoubleParameter(ctx, ctx.getIdCtx().getCompany(), ctx
                .getIdCtx().getEstablishment(), ProductionMOParamEnum.FABRICATION_INPUT.getCode(),
                PARAM_FIELD_FABRICATION_INPUT_DESTOCKING, workstationId);
        if (searchBatchByDestockingModel != 0) {
            XxfabPPO xxfabPPO = new XxfabPPO(ctx);
            xxfabPPO.prRechLotDestockage(itemMovementBean.getItemMovementKey().getEstablishmentKey().getCsoc(),
                    itemMovementBean.getItemMovementKey().getEstablishmentKey().getCetab(), ctx.getIdCtx().getLogin(),
                    itemMovementBean.getItemMovementKey().getManagementType().getValue(), itemMovementBean
                            .getItemMovementKey().getChrono().getPrechro(), itemMovementBean.getItemMovementKey()
                            .getChrono().getChrono(), itemMovementBean.getItemMovementKey().getCounter1(),
                    itemMovementBean.getItemMovementKey().getCounter2(), itemMovementBean.getItemMovementKey()
                            .getCounter3(), itemMovementBean.getItemMovementKey().getCounter4(), itemMovementBean
                            .getDepotCL().getCode(), itemMovementBean.getLocationCL().getCode(), itemMovementBean
                            .getBatch(), oLot, oRet, oWstr);
            if (!oRet.getBooleanValue()) {
                throw new ProxyBusinessException(oWstr.getStringValue());
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - searchBatchByDestockingModel(ctx=" + ctx + ", itemMovementBean=" + itemMovementBean
                    + ", workstationId=" + workstationId + ")");
        }
        return oLot.getStringValue();
    }

    /**
     * {@inheritDoc}
     */
    public String searchFirstInputBatch(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - searchFirstInputBatch(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        StringHolder oLot = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prRechLotPremierEntrant(operationItemKey.getEstablishmentKey().getCsoc(), operationItemKey
                .getEstablishmentKey().getCetab(), workstationId, ctx.getIdCtx().getLogin(), operationItemKey
                .getManagementType().getValue(), operationItemKey.getChrono().getPrechro(), operationItemKey
                .getChrono().getChrono(), operationItemKey.getCounter1(), operationItemKey.getCounter2(),
                operationItemKey.getCounter3(), operationItemKey.getCounter4(), oLot, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - searchFirstInputBatch(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        return oLot.getStringValue();
    }

    /**
     * {@inheritDoc}
     */
    public List<SearchLabel> searchPrintActLabel(final VIFContext ctx, final MOKey moKey, final RowidKey rowidKey,
            final String workstationId, final ActivityItemType activityItemType,
            final LabelingEventType labelingEventType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - searchPrintActLabel(ctx=" + ctx + ", moKey=" + moKey + ", rowidKey=" + rowidKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType
                    + ", labelingEventType=" + labelingEventType + ")");
        }
        List<SearchLabel> lstResult = new ArrayList<SearchLabel>();
        BooleanHolder oReturnValue = new BooleanHolder();
        StringHolder oReturnMessage = new StringHolder();
        // StringHolder oPrintProgramId = new StringHolder();
        TempTableHolder<XxfabPPOTtLocaldocprg> ttLocaldocprgHolder = new TempTableHolder<XxfabPPOTtLocaldocprg>();
        ProductionMOParamEnum param = null;
        int paramNb = -1;
        // get the profile on workstation
        if (activityItemType.equals(ActivityItemType.OUTPUT)) {
            param = ProductionMOParamEnum.FABRICATION_OUTPUT;
            paramNb = PARAM_FIELD_FABRICATION_OUTPUT_PROFILE;
        } else {
            param = ProductionMOParamEnum.FABRICATION_INPUT;
            paramNb = PARAM_FIELD_FABRICATION_INPUT_PROFILE;
        }
        String codeProfile = getParameterSBS().readStringParameter(ctx, moKey.getEstablishmentKey().getCsoc(),
                moKey.getEstablishmentKey().getCetab(), param.getCode(), paramNb, workstationId);
        // Log
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prRechercheEditeMultiEtiquetteActe(moKey.getEstablishmentKey().getCsoc(), moKey.getEstablishmentKey()
                .getCetab(), moKey.getChrono().getPrechro(), moKey.getChrono().getChrono(), workstationId, ctx
                .getIdCtx().getLogin(), activityItemType.getValue(), labelingEventType.getValue(), codeProfile,
                rowidKey.getRowid(), ttLocaldocprgHolder, oReturnValue, oReturnMessage);
        if (!oReturnValue.getBooleanValue()) {
            throw new ProxyBusinessException(oReturnMessage.getStringValue());
        }
        for (XxfabPPOTtLocaldocprg doc : ttLocaldocprgHolder.getListValue()) {
            SearchLabel searchLabel = new SearchLabel(doc.getNbdoc() == 0, doc.getCdoc(), doc.getPrgedi());
            lstResult.add(searchLabel);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - searchPrintActLabel(ctx=" + ctx + ", moKey=" + moKey + ", rowidKey=" + rowidKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType
                    + ", labelingEventType=" + labelingEventType + ")=" + lstResult);
        }
        return lstResult;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public SearchLabel searchPrintMovementLabel(final VIFContext ctx, final RowidKey rowidKey,
            final String workstationId, final ActivityItemType activityItemType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - searchPrintMovementLabel(ctx=" + ctx + ", rowidKey=" + rowidKey + ", workstationId="
                    + workstationId + ", activityItemType=" + activityItemType + ")");
        }
        BooleanHolder oReturnValue = new BooleanHolder();
        StringHolder oReturnMessage = new StringHolder();
        BooleanHolder oAsfForCopiesNumber = new BooleanHolder();
        StringHolder oLabelId = new StringHolder();
        StringHolder oPrintProgramId = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prRechercheEditeEtiquette(ctx.getIdCtx().getCompany(), ctx.getIdCtx().getEstablishment(),
                workstationId, ctx.getIdCtx().getLogin(), rowidKey.getRowid(), activityItemType.getValue(),
                oReturnValue, oReturnMessage, oAsfForCopiesNumber, oLabelId, oPrintProgramId);
        if (!oReturnValue.getBooleanValue()) {
            throw new ProxyBusinessException(oReturnMessage.getStringValue());
        }
        SearchLabel searchLabel = new SearchLabel(oAsfForCopiesNumber.getBooleanValue(), oLabelId.getStringValue(),
                oPrintProgramId.getStringValue());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - searchPrintMovementLabel(ctx=" + ctx + ", rowidKey=" + rowidKey + ", workstationId="
                    + workstationId + ", activityItemType=" + activityItemType + ")=" + searchLabel);
        }
        return searchLabel;
    }

    /**
     * Sets the activitiesSBS.
     * 
     * @param activitiesSBS activitiesSBS.
     */
    public void setActivitiesSBS(final ActivitiesSBS activitiesSBS) {
        this.activitiesSBS = activitiesSBS;
    }

    /**
     * Sets the entitySBS.
     * 
     * @category setter
     * @param entitySBS entitySBS.
     */
    public void setEntitySBS(final EntitySBS entitySBS) {
        this.entitySBS = entitySBS;
    }

    /**
     * Sets the itemSBS.
     * 
     * @category setter
     * @param itemSBS itemSBS.
     */
    public void setItemSBS(final ItemSBS itemSBS) {
        this.itemSBS = itemSBS;
    }

    /**
     * Sets the moProfileSBS.
     *
     * @param moProfileSBS moProfileSBS.
     */
    public void setMoProfileSBS(final MoProfileSBS moProfileSBS) {
        this.moProfileSBS = moProfileSBS;
    }

    /**
     * Sets the moSBS.
     * 
     * @category setter
     * @param moSBS moSBS.
     */
    public void setMoSBS(final MOSBS moSBS) {
        this.moSBS = moSBS;
    }

    /**
     * Sets the parameterSBS.
     * 
     * @category setter
     * @param parameterSBS parameterSBS.
     */
    public void setParameterSBS(final ParameterSBS parameterSBS) {
        this.parameterSBS = parameterSBS;
    }

    /**
     * Sets the warehouseSBS.
     * 
     * @param warehouseSBS warehouseSBS.
     */
    public void setWarehouseSBS(final WareHouseSBS warehouseSBS) {
        this.warehouseSBS = warehouseSBS;
    }

    /**
     * {@inheritDoc}
     */
    public void updateAdministrativeStructure(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateAdministrativeStructure(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug(new StringBuilder().append("updateAdministrativeStructure(ctx, operationItemKey = ")
                    .append(operationItemKey).append(", workstationId = ").append(workstationId).append(")").toString());
        }
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prRemonteeAdmin(operationItemKey.getEstablishmentKey().getCsoc(), operationItemKey
                .getEstablishmentKey().getCetab(), workstationId, ctx.getIdCtx().getLogin(), operationItemKey
                .getManagementType().getValue(), operationItemKey.getChrono().getPrechro(), operationItemKey
                .getChrono().getChrono(), operationItemKey.getCounter1(), operationItemKey.getCounter2(),
                operationItemKey.getCounter3(), operationItemKey.getCounter4(), oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateAdministrativeStructure(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public boolean updateWorkshopStructure(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId, final Progress progress) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateWorkshopStructure(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ", progress=" + progress + ")");
        }
        BooleanHolder oPrintMOLabel = new BooleanHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxoiaPPO xxoiaPPO = new XxoiaPPO(ctx);
        xxoiaPPO.prMajCpropCavanc(operationItemKey.getEstablishmentKey().getCsoc(), operationItemKey
                .getEstablishmentKey().getCetab(), workstationId, ctx.getIdCtx().getLogin(), operationItemKey
                .getManagementType().getValue(), operationItemKey.getChrono().getPrechro(), operationItemKey
                .getChrono().getChrono(), operationItemKey.getCounter1(), operationItemKey.getCounter2(),
                operationItemKey.getCounter3(), operationItemKey.getCounter4(), progress.getValue(), oPrintMOLabel,
                oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateWorkshopStructure(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ", progress=" + progress + ")");
        }
        return oPrintMOLabel.getBooleanValue();
    }

    /**
     * If natureComplementaryWorkshop is equals to MONatureComplementaryWorkshop.DOWNGRADED, <b>the downgradedItemInfo
     * and quantityDone have to be not null</b>.
     * 
     * @param xpiloia Bean use to create OperationItemBean.
     * @param natureComplementaryWorkshop nature of the item
     * @param downgradedItemCLs code label of the downgraded item.
     * @param quantityDone quantity of the downgraded item.
     * @param batch batch id of the item downgraded or scrap or inprogress.
     * @param unitItemScrap the unit item scrap
     * @return OutputOperationItemBean or InputOperationItemBean
     */
    private AbstractOperationItemBean createOperationItemBean(final Xpiloia xpiloia,
            final MONatureComplementaryWorkshop natureComplementaryWorkshop, final CodeLabels downgradedItemCLs,
            final double quantityDone, final String batch, final String unitItemScrap) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createOutputOperationItemBean(xpiloia=" + xpiloia + ", natureComplementaryWorkshop="
                    + natureComplementaryWorkshop + ", downgradedItemCLs=" + downgradedItemCLs + ", quantityDone="
                    + quantityDone + ", batch=" + batch + ")");
        }
        CodeLabels itemCls = null;
        OperationItemQuantityUnit operationItemQuantityUnit = null;
        String batchItem = xpiloia.getLot();
        if (!MONatureComplementaryWorkshop.DOWNGRADED.equals(natureComplementaryWorkshop)) {
            itemCls = new CodeLabels(xpiloia.getCart(), xpiloia.getLart(), xpiloia.getRart());
            if (MONatureComplementaryWorkshop.MANUFACTURING.equals(natureComplementaryWorkshop)) {
                operationItemQuantityUnit = new OperationItemQuantityUnit(xpiloia.getQtartafer1(),
                        xpiloia.getQtartfait1(), xpiloia.getQtartrest1(), xpiloia.getCuart1());
            } else if (ActivityItemType.OUTPUT.getValue().equals(xpiloia.getTypacta())) {
                operationItemQuantityUnit = new OperationItemQuantityUnit(0.0, quantityDone, 0.0, xpiloia.getCuart1());
                if (MONatureComplementaryWorkshop.SCRAP.equals(natureComplementaryWorkshop)) {
                    operationItemQuantityUnit.setUnit(unitItemScrap);
                }
                batchItem = batch;
            } else if (ActivityItemType.INPUT.getValue().equals(xpiloia.getTypacta())) {
                operationItemQuantityUnit = new OperationItemQuantityUnit(0.0, xpiloia.getQtartfait1(), 0.0,
                        xpiloia.getCuart1());
                batchItem = batch;
            }
        } else {
            if (downgradedItemCLs != null) {
                itemCls = downgradedItemCLs;
                operationItemQuantityUnit = new OperationItemQuantityUnit(0.0, quantityDone, 0.0, xpiloia.getCuart1());
                batchItem = batch;
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createOutputOperationItemBean(xpiloia=" + xpiloia + ", natureComplementaryWorkshop="
                    + natureComplementaryWorkshop + ", downgradedItemCLs=" + downgradedItemCLs + ", quantityDone="
                    + quantityDone + ", batch=" + batch + ")");
        }
        AbstractOperationItemBean operationItemBean = null;
        if (ActivityItemType.OUTPUT.getValue().equals(xpiloia.getTypacta())) {
            operationItemBean = new OutputOperationItemBean(ManagementType.getManagementType(xpiloia.getTypgest()),
                    new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(xpiloia.getPrechrof(),
                            xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia.getNi3(),
                    xpiloia.getNi4(), batchItem, itemCls, new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                    new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(), operationItemQuantityUnit);

        } else if (ActivityItemType.INPUT.getValue().equals(xpiloia.getTypacta())) {
            operationItemBean = new InputOperationItemBean(ManagementType.getManagementType(xpiloia.getTypgest()),
                    new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), new Chrono(xpiloia.getPrechrof(),
                            xpiloia.getChronof()), xpiloia.getNi1(), xpiloia.getNi2(), xpiloia.getNi3(),
                    xpiloia.getNi4(), batchItem, itemCls, new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot()),
                    new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp()), xpiloia.getDatcre(),
                    operationItemQuantityUnit, TouchDeclarationType.WITHOUT_QTY_TOUCH_DECLARATION.getValue().equals(
                            xpiloia.getTypdecatl()));
        }
        return operationItemBean;
    }

    /**
     * Initialize the deposit of the xpiloia set in parameter.
     * 
     * @param ctx Vif Context
     * @param xpiloia xpiloia for which the treatment is done
     * @param itemKey Replacement item or Downgraded Item.
     * @return CodeLabel
     * @throws ServerBusinessException ServerBusinessException
     */
    private CodeLabel getDepositOfXpiloia(final VIFContext ctx, final Xpiloia xpiloia, final ItemKey itemKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getDepositOfXpiloia(ctx=" + ctx + ", xpiloia=" + xpiloia + ", itemKey=" + itemKey + ")");
        }
        if (xpiloia == null) {
            throw new ServerBusinessException(Generic.T17702, new VarParamTranslation(null));
        }
        CodeLabel depositoryCL = null;
        StringHolder oCdepot = new StringHolder();
        StringHolder oLdepot = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        // The OperationItem has a deposit
        if (StringUtils.isNotEmpty(xpiloia.getCdepot())) {
            depositoryCL = new CodeLabel(xpiloia.getCdepot(), xpiloia.getLdepot());
        } else {
            // Reference Item
            String item = xpiloia.getCart();
            // If the Remplacement Item Has bean flashed.
            if (itemKey != null) {
                item = itemKey.getCItem();
            }
            XxfabPPO xxfabPPO = new XxfabPPO(ctx);
            xxfabPPO.prDepotArticle(xpiloia.getCsoc(), xpiloia.getCetab(), item, oCdepot, oLdepot, oRet, oWstr);
            /********************** Get the location of the item **********************/
            if (oCdepot != null) {
                // get the location of the depot of the item
                StockItemInfos stockItemInfos = getItemSBS().getStockItemInfosByKey(ctx,
                        new ItemProcessKey(xpiloia.getCsoc(), xpiloia.getCetab(), item));
                if (stockItemInfos != null && StringUtils.isNotEmpty(stockItemInfos.getLocationId())) {
                    xpiloia.setCemp(stockItemInfos.getLocationId());
                }
            }
            if (!oRet.getBooleanValue()) {
                throw new ProxyBusinessException(oWstr.getStringValue());
            }
            depositoryCL = new CodeLabel(oCdepot.getStringValue(), oLdepot.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getDepositOfXpiloia(ctx=" + ctx + ", xpiloia=" + xpiloia + ", itemKey=" + itemKey + ")="
                    + depositoryCL);
        }
        return depositoryCL;
    }

    /**
     * Initialize the location of the xpiloia set in parameter.
     * 
     * @param ctx Vif Context
     * @param xpiloia xpiloia for which the treatment is done
     * @param itemKey Replacement item or Downgraded Item.
     * @return CodeLabel
     * @throws ServerBusinessException ServerBusinessException
     */
    private CodeLabel getLocationOfXpiloia(final VIFContext ctx, final Xpiloia xpiloia, final ItemKey itemKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLocationOfXpiloia(ctx=" + ctx + ", xpiloia=" + xpiloia + ", itemKey=" + itemKey + ")");
        }
        CodeLabel locationCL = new CodeLabel("", "");
        if (xpiloia == null) {
            throw new ServerBusinessException(Generic.T17702, new VarParamTranslation(null));
        }
        if (StringUtils.isNotEmpty(xpiloia.getCdepot())) {
            locationCL = new CodeLabel(xpiloia.getCemp(), xpiloia.getLemp());
        } else {
            // Reference Item
            String item = xpiloia.getCart();
            // If the Remplacement Item Has bean flashed.
            if (itemKey != null) {
                item = itemKey.getCItem();
            }
            // get the location of the depot of the item
            StockItemInfos stockItemInfos = getItemSBS().getStockItemInfosByKey(ctx,
                    new ItemProcessKey(xpiloia.getCsoc(), xpiloia.getCetab(), item));
            if (stockItemInfos != null && StringUtils.isNotEmpty(stockItemInfos.getLocationId())) {
                locationCL = new CodeLabel(stockItemInfos.getLocationId(), null);
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getLocationOfXpiloia(ctx=" + ctx + ", xpiloia=" + xpiloia + ", itemKey=" + itemKey + ")="
                    + locationCL);
        }
        return locationCL;
    }

    /**
     * Return a list of OutputOperationItemBean. This list is initialize according the natureComplementaryWorkshop
     * parameter. If natureComplementaryWorkshop has the value :
     * <ul>
     * <li><b>MANUFACTURING :</b></li> Standard loading
     * <li><b>DOWNGRADED :</b></li> If itemKeyDowngraded is null, load all the downgraded items of the xpiloia in
     * parameter. <br>
     * If itemKeyDowngraded is not null, load the item downgraded set in itemKeyDowngraded. In any case, the quantity
     * done is calculated with the sum of xmtvm, the quantities done and lefttodo are initialized to zero. *
     * <li><b>SCRAP/INPROGRESS :</b></li> Standard loading except the quantity done which is calculated with the sum of
     * xmtvm and the quantities done and lefttodo which are initialized to zero.
     * </ul>
     * 
     * @param ctx VifContext
     * @param xpiloia used to initialize the OutputOperationItemBean
     * @param natureComplementaryWorkshop determines the treatment to do
     * @param itemKeyDowngraded item Downgraded to load. (Not null only when it's a convert call method).
     * @return List of OutputOperationItemBean
     * @throws ServerBusinessException ServerBusinessException
     */
    private List<AbstractOperationItemBean> getOperationItem(final VIFContext ctx, final Xpiloia xpiloia,
            final MONatureComplementaryWorkshop natureComplementaryWorkshop, final ItemKey itemKeyDowngraded)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getOperationItem(ctx=" + ctx + ", xpiloia=" + xpiloia + ", natureComplementaryWorkshop="
                    + natureComplementaryWorkshop + ", itemKeyDowngraded=" + itemKeyDowngraded + ")");
        }
        List<AbstractOperationItemBean> operationItemBeans = new ArrayList<AbstractOperationItemBean>();
        String batchItem = null;
        String unitItemScrap = null;
        // ----------------------------------------------------------------------------------------------------------------------------------
        // If the item to load are not Manufacturing, we have to load their movement (xmvtm) to initialize the
        // xpiloia.qtartfait1 in the bean.
        // ----------------------------------------------------------------------------------------------------------------------------------
        if (!MONatureComplementaryWorkshop.MANUFACTURING.equals(natureComplementaryWorkshop)) {
            List<Movement> listMovement = null;

            // ----------------------------------------------------------------------------
            // If the item to load are Downgraded, we have to load the zartd of the xiploia.
            // ----------------------------------------------------------------------------
            if (MONatureComplementaryWorkshop.DOWNGRADED.equals(natureComplementaryWorkshop)) {
                // If itemKeyDowngraded is null, loading of all the downgraded item. (QueryElement call)
                if (itemKeyDowngraded == null) {
                    List<String> listCodeArtDec = getActivitiesSBS().listCodeByArticleReference(ctx,
                            new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()), xpiloia.getCart());
                    // ----------------------------------------------------------------------------
                    // Then, create the OutputOperationItemBean list with generalItemInfos .
                    // ----------------------------------------------------------------------------
                    if (listCodeArtDec != null && !listCodeArtDec.isEmpty()) {
                        for (String cartd : listCodeArtDec) {
                            CodeLabels downgradedItemCLS = getItemSBS().getItemCLByPK(ctx,
                                    new ItemKey(xpiloia.getCsoc(), cartd));
                            if (downgradedItemCLS != null) {
                                listMovement = listMovementByNature(ctx, xpiloia, natureComplementaryWorkshop,
                                        downgradedItemCLS);
                                if (listMovement != null && !listMovement.isEmpty()) {
                                    batchItem = listMovement.get(0).getStockItem().getBatch();
                                }
                                // ----------------------------------------------------------------------------
                                // Create the OutputOperationItemBean.
                                // IMPORTANT : The quantities in OperationItemQuantityUnit
                                // are 0 to toDoQuantity, leftToDoQuantity and the completedQuantity is
                                // calculated with addition of the movement(xmvtm) of the item
                                // ----------------------------------------------------------------------------
                                operationItemBeans.add(createOperationItemBean(xpiloia, natureComplementaryWorkshop,
                                        downgradedItemCLS, this.getQuantityDoneByNature(listMovement), batchItem,
                                        unitItemScrap));
                                // Clean the variable
                                batchItem = null;
                            }
                        }
                    }
                } else {
                    // If itemKeyDowngraded is not null, loading of all the selected downgraded item. (Convert call)
                    CodeLabels downgradedItemCLS = getItemSBS().getItemCLByPK(ctx,
                            new ItemKey(xpiloia.getCsoc(), itemKeyDowngraded.getCItem()));
                    if (downgradedItemCLS != null) {
                        listMovement = listMovementByNature(ctx, xpiloia, natureComplementaryWorkshop,
                                downgradedItemCLS);
                        if (listMovement != null && !listMovement.isEmpty()) {
                            batchItem = listMovement.get(0).getStockItem().getBatch();
                        }
                        // ----------------------------------------------------------------------------
                        // Create the OutputOperationItemBean.
                        // IMPORTANT : The quantities in OperationItemQuantityUnit
                        // are 0 to toDoQuantity, leftToDoQuantity and the completedQuantity is
                        // calculated with addition of the movement(xmvtm) of the item
                        // ----------------------------------------------------------------------------
                        operationItemBeans
                                .add(createOperationItemBean(xpiloia, natureComplementaryWorkshop, downgradedItemCLS,
                                        this.getQuantityDoneByNature(listMovement), batchItem, unitItemScrap));
                    }
                }
            } else {
                listMovement = listMovementByNature(ctx, xpiloia, natureComplementaryWorkshop, null);
                if (listMovement != null && !listMovement.isEmpty()) {
                    batchItem = listMovement.get(0).getStockItem().getBatch();
                    if (MONatureComplementaryWorkshop.SCRAP.equals(natureComplementaryWorkshop)) {
                        unitItemScrap = listMovement.get(0).getKeyboardQties().getFirstQty().getUnit();
                        // QA8421 For scrap nature the batch of xpiloia is the priority
                        if (StringUtils.isNotEmpty(xpiloia.getLot())) {
                            batchItem = xpiloia.getLot();
                        }
                    }
                } else if (MONatureComplementaryWorkshop.SCRAP.equals(natureComplementaryWorkshop)) {
                    OperationItemKey operationItemKey = new OperationItemKey();
                    operationItemKey.setEstablishmentKey(new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab()));
                    operationItemKey.setChrono(new Chrono(xpiloia.getPrechrof(), xpiloia.getChronof()));
                    operationItemKey.setCounter1(xpiloia.getNi1());
                    OutputItemParameters outputItemParameters = this.getOutputItemParameters(ctx, operationItemKey,
                            natureComplementaryWorkshop, null);
                    batchItem = xpiloia.getLot();
                    unitItemScrap = outputItemParameters.getForcedFirstUnit();
                }
                // ---------------------------------------------
                // Items have Nature Scrap or InProgress.
                // Create the OutputOperationItemBean.
                // IMPORTANT : The quantities in OperationItemQuantityUnit
                // are 0 to toDoQuantity, leftToDoQuantity and the completedQuantity is
                // calculated with addition of the movement(xmvtm) of the item
                // ----------------------------------------------------------------------------
                operationItemBeans.add(createOperationItemBean(xpiloia, natureComplementaryWorkshop, null,
                        this.getQuantityDoneByNature(listMovement), batchItem, unitItemScrap));
            }
        } else {
            // ----------------------------------------------------------------------------
            // Transform the xpiloia (Manufacturing) list in a OutputOperationItemBean list.
            // ----------------------------------------------------------------------------
            operationItemBeans.add(createOperationItemBean(xpiloia, natureComplementaryWorkshop, null, 0.0, null,
                    unitItemScrap));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getOperationItem(ctx=" + ctx + ", xpiloia=" + xpiloia + ", natureComplementaryWorkshop="
                    + natureComplementaryWorkshop + ", itemKeyDowngraded=" + itemKeyDowngraded + ")="
                    + operationItemBeans);
        }

        // If DOWNGRADED item, we re-check the profile to force the new batch if needed
        if (MONatureComplementaryWorkshop.DOWNGRADED.equals(natureComplementaryWorkshop)
                || MONatureComplementaryWorkshop.INPROGRESS.equals(natureComplementaryWorkshop)) {
            for (AbstractOperationItemBean absOperationItemBean : operationItemBeans) {
                if (absOperationItemBean instanceof OutputOperationItemBean) {
                    OutputOperationItemBean outputOperationItemBean = (OutputOperationItemBean) absOperationItemBean;
                    OperationItemKey operationItemKey = new OperationItemKey();
                    operationItemKey.setChrono(new Chrono(xpiloia.getPrechrof(), xpiloia.getChronof()));
                    operationItemKey.setCounter1(xpiloia.getNi1());
                    // QA8421 Very awkward code : the getOutputItemParameters method return every time the same
                    // object, so the batch is always set to empty
                    OutputItemParameters outputItemParameters = getMoProfileSBS().getOutputProfile(ctx,
                            outputOperationItemBean.getOperationItemKey().getEstablishmentKey(),
                            outputOperationItemBean.getItemCL().getCode(), ctx.getIdCtx().getWorkstation());

                    if (outputItemParameters != null
                            && !OutputEntityType.BATCH_ITEM.equals(outputItemParameters.getEntityType())) {
                        outputOperationItemBean.setForecastBatch("");
                    }
                }
            }
        }

        return operationItemBeans;
    }

    /**
     * Calculate the quantity done based on the movements in parameter..
     * 
     * @param listMovements list of movements
     * @return The quantity done.
     * @throws ServerBusinessException ServerBusinessException
     */
    private double getQuantityDoneByNature(final List<Movement> listMovements) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getQuantityDoneByNature(listMovements=" + listMovements + ")");
        }
        double quantityDone = 0.0;
        // If there is movements, add the qty of first qty.
        if (listMovements != null && !listMovements.isEmpty()) {
            for (Movement movement : listMovements) {
                if (MONatureComplementaryWorkshop.SCRAP.getValue().equals(movement.getState().getStockNature())) {
                    quantityDone += movement.getKeyboardQties().getFirstQty().getQty();
                } else {
                    quantityDone += movement.getStockItem().getQuantities().getFirstQty().getQty();
                }
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getQuantityDoneByNature(listMovements=" + listMovements + ")=" + quantityDone);
        }
        return quantityDone;
    }

    /**
     * Return the List Of Location Optimizes of a deposit.
     * 
     * @param ctx current context
     * @param entity entity to validate
     * @param beanCriteria criteria
     * @return List of Location Optimize
     * @throws ServerBusinessException if error occurs
     */
    private List<String> listLocationOptimizes(final VIFContext ctx, final CEntityBean entity,
            final CriteriaReturnInitBean beanCriteria) throws ServerBusinessException {
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMess = new StringHolder();
        StringHolder oCemp = new StringHolder();
        StringHolder oNsc = new StringHolder();
        // Temp Table contains the criteria about the O.F
        List<XxfabPPOTtqualite> listTt = new ArrayList<XxfabPPOTtqualite>();
        for (CriteriaCompleteBean criteriaCompleteBean : beanCriteria.getListCriteria()) {
            XxfabPPOTtqualite ttQualite = new XxfabPPOTtqualite();
            ttQualite.setCsoc(criteriaCompleteBean.getCompanyId());
            ttQualite.setCetab(criteriaCompleteBean.getEstablishmentId());
            ttQualite.setCcri(criteriaCompleteBean.getCriteriaId());
            // Valmin valmax
            ttQualite.setValcmin(criteriaCompleteBean.getValueCarMin());
            ttQualite.setValcmax(criteriaCompleteBean.getValueCarMax());
            ttQualite.setValdmax(criteriaCompleteBean.getValueDateMax());
            ttQualite.setValdmin(criteriaCompleteBean.getValueDateMin());
            ttQualite.setValnmax(criteriaCompleteBean.getValueNumMax());
            ttQualite.setValnmin(criteriaCompleteBean.getValueNumMin());
            listTt.add(ttQualite);
        }
        String sCcond = entity.getEntityLocationBean().getContainerPackaging().getPackagingId();
        // Need a conditionnement
        if (StringUtils.isEmpty(sCcond)) {
            sCcond = entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit();
        }
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prRechercheEmpOptimise("", entity.getEntityLocationBean().getEntityLocationKey().getCsoc(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCetab(), //
                entity.getStockItemBean().getStockItem().getItemId(), // Cart
                entity.getStockItemBean().getStockItem().getBatch(), // Lot
                entity.getEntityLocationBean().getEntityLocationKey().getCdepot(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCemp(), //
                sCcond, // Ccond
                beanCriteria.getInternalInfo().getInfoCriteriaStade(), // cleCrifa
                beanCriteria.getInternalInfo().getInfoAct(), // InfoActe
                beanCriteria.getInternalInfo().getInfoCriteriaKey(), // CleActe
                listTt, // Table temporaire contenant les Critères venant d'être saisis
                oCemp, // Emplacement Optimise
                oNsc, // Nb Emplacement Libre
                oRet, //
                oMess);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMess.getStringValue());
        }
        return Arrays.asList(StringUtils.splitPreserveAllTokens(oCemp.getStringValue(), ","));
    }

    /**
     * Search all the movement real for an item which have a Nature Downgraded, Scrap or In Progress.<br>
     * 
     * @param ctx Vif Context
     * @param xpiloia Item for which the seatch is done.
     * @param natureComplementaryWorkshop Nature of the item.
     * @param downgradedItemCLs Item Downgraded code labels. If it not null, we load it xmvtm.
     * @return List of Movement to this item on the O.F in progress.
     * @throws ServerBusinessException ServerBusinessException
     */
    @SuppressWarnings("deprecation")
    private List<Movement> listMovementByNature(final VIFContext ctx, final Xpiloia xpiloia,
            final MONatureComplementaryWorkshop natureComplementaryWorkshop, final CodeLabels downgradedItemCLs)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listMovementByNature(ctx=" + ctx + ", xpiloia=" + xpiloia
                    + ", natureComplementaryWorkshop=" + natureComplementaryWorkshop + ", downgradedItemCLs="
                    + downgradedItemCLs + ")");
        }
        MovementSelection movementSelection = new MovementSelection();
        MovementAct movementAct = new MovementAct();
        movementAct.setPrechro(xpiloia.getPrechrof());
        movementAct.setChrono(xpiloia.getChronof());
        movementAct.setNumber1(xpiloia.getNi1());
        movementAct.setNumber2(xpiloia.getNi2());
        movementAct.setNumber3(xpiloia.getNi3());
        movementAct.setNumber4(xpiloia.getNi4());
        movementSelection.setMovementAct(movementAct);
        movementSelection.setManagements(Arrays.asList(Management.REAL));
        movementSelection
                .setLocation(new EntityLocationKey(new EstablishmentKey(xpiloia.getCsoc(), xpiloia.getCetab())));
        movementSelection.setStockNature(natureComplementaryWorkshop.getValue());
        StockItem stockItem = new StockItem(xpiloia.getCart(), null);
        if (downgradedItemCLs != null) {
            stockItem.setItemId(downgradedItemCLs.getCode());
        }
        movementSelection.setStockItem(stockItem);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listMovementByNature(ctx=" + ctx + ", xpiloia=" + xpiloia
                    + ", natureComplementaryWorkshop=" + natureComplementaryWorkshop + ", downgradedItemCLs="
                    + downgradedItemCLs + ")");
        }
        return getEntitySBS().listMovementBySelectionOrderByDescending(ctx, movementSelection, 0, MAX_ROW);
    }
}

/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MOSBSImpl.java,v $
 * Created on 22 nov. 07 by glc
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.mo;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProxyBusinessException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.beans.BoundBean;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.common.beans.DateBoundBean;
import fr.vif.jtech.common.beans.FlipFlopBean;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.StringHelper;
import fr.vif.jtech.common.util.TimeHelper;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.activities.activities.business.beans.common.activity.ActivityHeader;
import fr.vif.vif5_7.activities.activities.business.beans.common.fabrication.Fabrication;
import fr.vif.vif5_7.activities.activities.business.beans.common.resources.ProductionResourceBean;
import fr.vif.vif5_7.activities.activities.business.beans.common.soa.Soa;
import fr.vif.vif5_7.activities.activities.business.services.libraries.activities.ActivitiesSBS;
import fr.vif.vif5_7.activities.activities.business.services.libraries.resources.ProductionResourcesSBS;
import fr.vif.vif5_7.activities.activities.business.services.libraries.soa.SoaSBS;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FabricationType;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FlowType;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ResourceType;
import fr.vif.vif5_7.admin.config.business.beans.ParamFieldValue;
import fr.vif.vif5_7.admin.config.business.services.libraries.parameter.ParameterSBS;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.comment.business.beans.common.Comment;
import fr.vif.vif5_7.gen.comment.business.beans.common.CommentKey;
import fr.vif.vif5_7.gen.comment.business.services.libraries.simplecomment.SimpleCommentSBS;
import fr.vif.vif5_7.gen.comment.constants.Mnemos.CommentDomain;
import fr.vif.vif5_7.gen.comment.constants.Mnemos.SimpleCommentKeyType;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaCompleteBean;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaValueBean;
import fr.vif.vif5_7.gen.criteria.business.constants.CriterionConstants.Mode;
import fr.vif.vif5_7.gen.criteria.business.services.libraries.criteria.CriteriaSBS;
import fr.vif.vif5_7.gen.hierarchy.business.beans.common.pyramid.HchyLstValue;
import fr.vif.vif5_7.gen.hierarchy.business.beans.common.pyramid.PyramidDeepSelection;
import fr.vif.vif5_7.gen.hierarchy.business.beans.common.pyramid.PyramidDeepValue;
import fr.vif.vif5_7.gen.hierarchy.business.beans.common.pyramid.PyramidFastSelection;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemKey;
import fr.vif.vif5_7.gen.item.business.beans.features.fitem.FItemSBean;
import fr.vif.vif5_7.gen.item.business.services.libraries.item.ItemSBS;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.gen.service.business.services.libraries.chrono.ChronoSBS;
import fr.vif.vif5_7.production.mo.business.beans.common.endday.EndDayMOSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.ktpsx.KtpsxBean;
import fr.vif.vif5_7.production.mo.business.beans.common.marking.MOMarkingBean;
import fr.vif.vif5_7.production.mo.business.beans.common.marking.MOMarkingSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBoningBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOSimpleSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.pdffile.PDFFile;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;
import fr.vif.vif5_7.production.mo.business.beans.common.unit.MOQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.xpiloia.XpiloiaBean;
import fr.vif.vif5_7.production.mo.business.beans.common.xpiloia.XpiloiaKey;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModel;
import fr.vif.vif5_7.production.mo.business.beans.features.ftablelook.touchmodel.TouchModelArray;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPO;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtCrivHandle;
import fr.vif.vif5_7.production.mo.business.services.libraries.doc.OfliedocPPO;
import fr.vif.vif5_7.production.mo.business.services.libraries.doc.OfliedocPPOTtdocument;
import fr.vif.vif5_7.production.mo.business.services.libraries.stackingplan.StackingPlanSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOState;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.MOUse;
import fr.vif.vif5_7.production.mo.constants.Mnemos.PrechroId;
import fr.vif.vif5_7.production.mo.constants.Mnemos.Progress;
import fr.vif.vif5_7.production.mo.constants.ProductionMOParamEnum;
import fr.vif.vif5_7.production.mo.constants.features.tablelook.Sorts;
import fr.vif.vif5_7.production.mo.dao.kof.Kof;
import fr.vif.vif5_7.production.mo.dao.kof.KofDAOFactory;
import fr.vif.vif5_7.production.mo.dao.kot.Kot;
import fr.vif.vif5_7.production.mo.dao.kot.KotDAOFactory;
import fr.vif.vif5_7.production.mo.dao.kotod.Kotod;
import fr.vif.vif5_7.production.mo.dao.kotod.KotodDAOFactory;
import fr.vif.vif5_7.production.mo.dao.ktpsx.Ktpsx;
import fr.vif.vif5_7.production.mo.dao.ktpsx.KtpsxDAOFactory;
import fr.vif.vif5_7.production.mo.dao.xpiloia.Xpiloia;
import fr.vif.vif5_7.production.mo.dao.xpiloia.XpiloiaDAOFactory;


/**
 * MO business services implementation.
 * 
 * @author glc
 */
@Component
public class MOSBSImpl implements MOSBS {

    private static final String    ITEM_FAMILY_HIERARCHY                 = "$FAMART";

    private static final Logger    LOGGER                                = Logger.getLogger(MOSBSImpl.class);

    private static final int       PYRAMID_WORKSTATION_PARAM_FAMILIES_NB = 4;

    private static final int       PYRAMID_WORKSTATION_PARAM_GROUPID_NB  = 3;

    private static final int       SECONDS_IN_ONE_DAY                    = 86400;
    private static final int       SECONDS_IN_ONE_HOUR                   = 3600;
    private static final int       SECONDS_IN_ONE_MINUTE                 = 60;

    @Autowired
    private ActivitiesSBS          activitiesSBS                         = null;

    @Autowired
    private ChronoSBS              chronoSBS                             = null;

    @Autowired
    private SimpleCommentSBS       commentSBS                            = null;

    @Autowired
    private ItemSBS                itemSBS                               = null;

    @Autowired
    private ParameterSBS           parameterSBS                          = null;

    @Autowired
    private StackingPlanSBS        stackingPlanSBS                       = null;

    // SBS activities sequence
    @Autowired
    private SoaSBS                 soaSBS                                = null;

    @Autowired
    private ProductionResourcesSBS productionResourcesSBS                = null;

    @Autowired
    private CriteriaSBS            criteriaSBS                           = null;

    /**
     * On Origin MO, there is a different way to get ToDo quantity.
     * 
     * @param vifCtx ctx
     * @param bean the bean
     * @param plc the kof plc
     * @throws ServerBusinessException if error occurs
     */
    public void calculateQuantityLeftOnOriginMO(final VIFContext vifCtx, final MOBean bean, final Kof plc)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - calculateQuantityLeftOnOriginMO(vifCtx=" + vifCtx + ", bean=" + bean + ", plc=" + plc
                    + ")");
        }
        try {
            Kot kot = KotDAOFactory.getDAO().getByPK(vifCtx.getDbCtx(),
                    bean.getMoKey().getEstablishmentKey().getCsoc(), bean.getMoKey().getEstablishmentKey().getCetab(),
                    bean.getMoKey().getChrono().getPrechro(), bean.getMoKey().getChrono().getChrono(),
                    plc.getNi1artref());

            // We retrieve effective quantity from KOT and we force the unit to KOT's unit in order to work on activity
            // unit
            bean.getMoQuantityUnit().setEffectiveQuantity(kot.getQtestk());
            bean.getMoQuantityUnit().setEffectiveUnit(kot.getCunite());
        } catch (DAOException e) {
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - calculateQuantityLeftOnOriginMO(vifCtx=" + vifCtx + ", bean=" + bean + ", plc=" + plc
                    + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public MOBean createStartedMO(final VIFContext ctx, final MOBean bean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createStartedMO(ctx=" + ctx + ", bean=" + bean + ")");
        }
        XxcreaofPPO ppo = new XxcreaofPPO(ctx);
        BooleanHolder ret = new BooleanHolder();
        StringHolder retMsg = new StringHolder();
        ppo.prCreateOf(bean.getMoKey().getEstablishmentKey().getCsoc(), bean.getMoKey().getEstablishmentKey()
                .getCetab(), bean.getMoKey().getChrono().getPrechro(), bean.getMoKey().getChrono().getChrono(), true,
                bean.getEffectiveBeginDateTime(), TimeHelper.getTime(bean.getEffectiveBeginDateTime()), MOState.STARTED
                        .getValue(), bean.getItemCLs().getCode(),
                bean.getFabrication().getFabricationType().getValue(), bean.getFabrication().getFabricationCLs()
                        .getCode(), bean.getFlowType().getValue(), "", // TODO input batch
                bean.getMoQuantityUnit().getToDoQuantity(), bean.getMoQuantityUnit().getUnit(), bean
                        .getMoQuantityUnit().getToDoQuantity(), // TODO qte
                bean.getMoQuantityUnit().getToDoQuantity(), // TODO qte ds unite 2
                bean.getMoQuantityUnit().getUnit(), 0, // TODO freinte
                ctx.getIdCtx().getLogin(), true, "", ret, retMsg);
        if (!ret.getBooleanValue()) {
            throw new ProxyBusinessException(retMsg.getStringValue());
        }
        ppo.release(ctx.getDbCtx());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createStartedMO(ctx=" + ctx + ", bean=" + bean + ")");
        }
        return getMOBeanByKey(ctx, bean.getMoKey());
    }

    /**
     * {@inheritDoc}
     */
    public MOBoningBean createStartedMOStackingPlan(final VIFContext ctx, final MOBoningBean bean)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createStartedMOStackingPlan(ctx=" + ctx + ", bean=" + bean + ")");
        }
        XxcreaofPPO ppo = new XxcreaofPPO(ctx);
        BooleanHolder ret = new BooleanHolder();
        StringHolder retMsg = new StringHolder();
        String stackingplan = bean.getStackingPlan().getCode() == null ? "" : bean.getStackingPlan().getCode();
        String operatingProcess = bean.getOperatingProcess().getCode() == null ? "" : bean.getOperatingProcess()
                .getCode();
        ChronoPO originPO = bean.getOriginChrono() == null ? new ChronoPO("", 0, 0) : bean.getOriginChrono();

        ppo.prCreateOfPlanrgt(bean.getMoKey().getEstablishmentKey().getCsoc(), bean.getMoKey().getEstablishmentKey()
                .getCetab(), bean.getMoKey().getChrono().getPrechro(), bean.getMoKey().getChrono().getChrono(),
                originPO.getPrechro(), originPO.getChrono(), originPO.getCounter1(), true, bean
                        .getEffectiveBeginDateTime(), TimeHelper.getTime(bean.getEffectiveBeginDateTime()),
                MOState.STARTED.getValue(), bean.getItemCLs().getCode(), bean.getFabrication().getFabricationType()
                        .getValue(), bean.getFabrication().getFabricationCLs().getCode(),
                bean.getFlowType().getValue(), "", bean.getMoQuantityUnit().getToDoQuantity(), bean.getMoQuantityUnit()
                        .getUnit(), bean.getMoQuantityUnit().getToDoQuantity(), bean.getMoQuantityUnit()
                        .getToDoQuantity(), bean.getMoQuantityUnit().getUnit(), 0, ctx.getIdCtx().getLogin(), true, "",
                stackingplan, operatingProcess, ret, retMsg);

        if (!ret.getBooleanValue()) {
            throw new ProxyBusinessException(retMsg.getStringValue());
        }
        ppo.release(ctx.getDbCtx());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createStartedMOStackingPlan(ctx=" + ctx + ", bean=" + bean + ")");
        }
        return getMOBoningBeanByKey(ctx, bean.getMoKey());
    }

    /**
     * {@inheritDoc}
     */
    public MOBean createStartedMSO(final VIFContext ctx, final MOKey msoKey, final Date effectiveBeginDateTime,
            final Fabrication fabrication, final QuantityUnit quantityUnit, final String userId, final MOKey originMOKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createStartedMSO(ctx=" + ctx + ", msoKey=" + msoKey + ", effectiveBeginDateTime="
                    + effectiveBeginDateTime + ", fabrication=" + fabrication + ", quantityUnit=" + quantityUnit
                    + ", userId=" + userId + ", originMOKey=" + originMOKey + ")");
        }
        XxcreaofPPO ppo = new XxcreaofPPO(ctx);
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        ppo.prCreateOfTechnique(msoKey.getEstablishmentKey().getCsoc(), msoKey.getEstablishmentKey().getCetab(), msoKey
                .getChrono().getPrechro(), msoKey.getChrono().getChrono(), effectiveBeginDateTime, TimeHelper
                .getTime(effectiveBeginDateTime), fabrication.getFabricationType().getValue(), fabrication
                .getFabricationCLs().getCode(), quantityUnit.getQty(), quantityUnit.getUnit(), userId, originMOKey
                .getChrono().getPrechro(), originMOKey.getChrono().getChrono(), oRet, oMsg);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMsg.getStringValue());
        }
        ppo.release(ctx.getDbCtx());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createStartedMSO(ctx=" + ctx + ", msoKey=" + msoKey + ", effectiveBeginDateTime="
                    + effectiveBeginDateTime + ", fabrication=" + fabrication + ", quantityUnit=" + quantityUnit
                    + ", userId=" + userId + ", originMOKey=" + originMOKey + ")");
        }
        return getMOBeanByKey(ctx, msoKey);
    }

    /**
     * Gets the activitiesSBS.
     * 
     * @category getter
     * @return the activitiesSBS.
     */
    public ActivitiesSBS getActivitiesSBS() {
        return activitiesSBS;
    }

    @Override
    public String getActivityFamilyFromPO(final VIFContext ctx, final POKey poKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getActivityFamilyFromPO(ctx=" + ctx + ", poKey=" + poKey + ")");
        }

        String actFam = null;
        if (poKey == null || poKey.getEstablishmentKey() == null || poKey.getEstablishmentKey().getCsoc() == null
                || poKey.getEstablishmentKey().getCsoc().equals("") || poKey.getEstablishmentKey().getCetab() == null
                || poKey.getEstablishmentKey().getCetab().equals("") || poKey.getChrono() == null
                || poKey.getChrono().getPrechro() == null || poKey.getChrono().getPrechro().equals("")
                || poKey.getChrono().getChrono() == 0) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
        }
        try {
            Kot plc = KotDAOFactory.getDAO().getByPK(ctx.getDbCtx(), poKey.getEstablishmentKey().getCsoc(),
                    poKey.getEstablishmentKey().getCetab(), poKey.getChrono().getPrechro(),
                    poKey.getChrono().getChrono(), poKey.getChrono().getCounter1());
            if (plc != null) {
                actFam = plc.getCfmact();
            }
        } catch (DAOException e) {
            LOGGER.error("Error while getting getActivityFamilyFromPO");
            throw new ServerBusinessException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getActivityFamilyFromPO(ctx=" + ctx + ", poKey=" + poKey + ")=" + actFam);
        }
        return actFam;
    }

    /**
     * Gets the chronoSBS.
     * 
     * @category getter
     * @return the chronoSBS.
     */
    public ChronoSBS getChronoSBS() {
        return chronoSBS;
    }

    /**
     * {@inheritDoc}
     */
    public Comment getComment(final VIFContext ctx, final Object key) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getComment(ctx=" + ctx + ", key=" + key + ")");
        }
        Comment comment = null;
        CommentKey commentKey = getCommentKey(key);
        comment = getCommentSBS().getComment(ctx, CommentDomain.PRODUCTION, commentKey);
        // if it's an OTA comment, add the OTA AFFECT comment associated
        if (OperationItemKey.class.isInstance(key)) {
            // searches the OTA AFFECT comment associated
            OperationItemKey moItemKey = (OperationItemKey) key;
            commentKey.setCsoc(moItemKey.getEstablishmentKey().getCsoc());
            commentKey.setCetab(moItemKey.getEstablishmentKey().getCetab());
            commentKey.setKeyType(SimpleCommentKeyType.PROCESSING_ORDER_ITEM.getKey());
            // commentKey.setKeyType("AAO");
            commentKey.setKey(StringHelper
                    .concat(moItemKey.getChrono().getPrechro())
                    .concat(",")
                    .concat(Integer.toString(moItemKey.getChrono().getChrono()))
                    .concat(",")
                    .concat(Integer.toString(moItemKey.getCounter1()).concat(",")
                            .concat(Integer.toString(moItemKey.getCounter4()))));
            Comment commentAff = null;
            commentAff = getCommentSBS().getComment(ctx, CommentDomain.PRODUCTION, commentKey);
            // concats the 2 comments
            String msg = I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, ProductionMo.T34548);
            // "Lots affectés"
            if (commentAff != null) {
                if (comment == null) {
                    comment = new Comment();
                }
                comment.setCom(comment.getCom().concat("   ".concat(msg.concat(": ".concat(commentAff.getCom())))));
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getComment(ctx=" + ctx + ", key=" + key + ")=" + comment);
        }
        return comment;
    }

    /**
     * Gets the commentSBS.
     * 
     * @category getter
     * @return the commentSBS.
     */
    public SimpleCommentSBS getCommentSBS() {
        return commentSBS;
    }

    /**
     * Gets the criteriaSBS.
     * 
     * @return the criteriaSBS.
     */
    public CriteriaSBS getCriteriaSBS() {
        return criteriaSBS;
    }

    /**
     * 
     * Gets the itemSBS.
     * 
     * @category getter
     * @return the itemSBS.
     */
    public ItemSBS getItemSBS() {
        return itemSBS;
    }

    /**
     * {@inheritDoc}
     */
    public MOBean getMOBeanByKey(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getMOBeanByKey(ctx=" + ctx + ", moKey=" + moKey + ")");
        }
        if (moKey == null || moKey.getEstablishmentKey() == null || moKey.getEstablishmentKey().getCsoc() == null
                || moKey.getEstablishmentKey().getCsoc().equals("") || moKey.getEstablishmentKey().getCetab() == null
                || moKey.getEstablishmentKey().getCetab().equals("") || moKey.getChrono() == null
                || moKey.getChrono().getPrechro() == null || moKey.getChrono().getPrechro().equals("")
                || moKey.getChrono().getChrono() == 0) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
        }
        Kof plc = getKofByKey(ctx, moKey);
        MOBean bean = null;
        if (plc != null) {
            bean = plc2MOBean(ctx, plc);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getMOBeanByKey(ctx=" + ctx + ", moKey=" + moKey + ")=" + bean);
        }
        return bean;
    }

    /**
     * {@inheritDoc}
     */
    public MOBoningBean getMOBoningBeanByKey(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getMOBoningBeanByKey(ctx=" + ctx + ", moKey=" + moKey + ")");
        }
        if (moKey == null || moKey.getEstablishmentKey() == null || moKey.getEstablishmentKey().getCsoc() == null
                || moKey.getEstablishmentKey().getCsoc().equals("") || moKey.getEstablishmentKey().getCetab() == null
                || moKey.getEstablishmentKey().getCetab().equals("") || moKey.getChrono() == null
                || moKey.getChrono().getPrechro() == null || moKey.getChrono().getPrechro().equals("")
                || moKey.getChrono().getChrono() == 0) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
        }
        MOBoningBean bean = null;
        Kof plc = getKofByKey(ctx, moKey);
        if (plc != null) {
            bean = plc2MOBoningBean(ctx, plc);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getMOBoningBeanByKey(ctx=" + ctx + ", moKey=" + moKey + ")=" + bean);
        }
        return bean;
    }

    /**
     * Gets the parameterSBS.
     * 
     * @category getter
     * @return the parameterSBS.
     */
    public ParameterSBS getParameterSBS() {
        return parameterSBS;
    }

    /**
     * {@inheritDoc}
     * 
     * @deprecated
     */
    @Deprecated
    public String getPdfFileOfCurrentItem(final VIFContext ctx, final MOItemKey moItemKey)
            throws ServerBusinessException {
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("getPdfFileOfCurrentItem() moItemKey=" + moItemKey);
        }
        if (moItemKey == null || moItemKey.getEstablishmentKey() == null
                || moItemKey.getEstablishmentKey().getCsoc() == null
                || moItemKey.getEstablishmentKey().getCsoc().equals("")
                || moItemKey.getEstablishmentKey().getCetab() == null
                || moItemKey.getEstablishmentKey().getCetab().equals("") || moItemKey.getChrono() == null
                || moItemKey.getChrono().getPrechro() == null || moItemKey.getChrono().getPrechro().equals("")
                || moItemKey.getChrono().getChrono() == 0 || //
                moItemKey.getCounter1() == 0) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        StringHolder oDocument = new StringHolder();
        OfliedocPPO ppo = new OfliedocPPO(ctx);
        ppo.getDocument(moItemKey.getEstablishmentKey().getCsoc(), //
                moItemKey.getEstablishmentKey().getCetab(), //
                moItemKey.getChrono().getPrechro(), //
                moItemKey.getChrono().getChrono(), //
                moItemKey.getCounter1(), ctx.getIdCtx().getLogin(), oDocument, oRet, oMsg);
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("getPdfFileOfCurrentItem() oDocument=" + oDocument);
        }
        return oDocument != null ? oDocument.getStringValue() : "";
    }

    /**
     * {@inheritDoc}
     * 
     * 
     */
    public List<PDFFile> getPdfFilesOfCurrentItem(final VIFContext ctx, final MOItemKey moItemKey)
            throws ServerBusinessException {
        List<PDFFile> ret = new ArrayList<PDFFile>();
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("getPdfFilesOfCurrentItem() moItemKey=" + moItemKey);
        }
        if (moItemKey == null || moItemKey.getEstablishmentKey() == null
                || moItemKey.getEstablishmentKey().getCsoc() == null
                || moItemKey.getEstablishmentKey().getCsoc().equals("")
                || moItemKey.getEstablishmentKey().getCetab() == null
                || moItemKey.getEstablishmentKey().getCetab().equals("") || moItemKey.getChrono() == null
                || moItemKey.getChrono().getPrechro() == null || moItemKey.getChrono().getPrechro().equals("")
                || moItemKey.getChrono().getChrono() == 0 || //
                moItemKey.getCounter1() == 0) {
            String error = I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, ProductionMo.T30838) + " - "
                    + moItemKey;
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(error));
        }
        OfliedocPPO ppo = new OfliedocPPO(ctx);
        TempTableHolder<OfliedocPPOTtdocument> tt = new TempTableHolder<OfliedocPPOTtdocument>();
        ppo.getDocuments(moItemKey.getEstablishmentKey().getCsoc(), //
                moItemKey.getEstablishmentKey().getCetab(), //
                moItemKey.getChrono().getPrechro(), //
                moItemKey.getChrono().getChrono(), //
                moItemKey.getCounter1(), ctx.getIdCtx().getLogin(), tt);
        for (OfliedocPPOTtdocument rec : tt.getListValue()) {
            ret.add(new PDFFile(rec.getTitre(), rec.getPath()));
        }
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("getPdfFilesOfCurrentItem() return=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("deprecation")
    public POBean getPOBeanByKey(final VIFContext ctx, final POKey poKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getPOBeanByKey(ctx=" + ctx + ", poKey=" + poKey + ")");
        }
        POBean bean = null;
        if (poKey == null || poKey.getEstablishmentKey() == null || poKey.getEstablishmentKey().getCsoc() == null
                || poKey.getEstablishmentKey().getCsoc().equals("") || poKey.getEstablishmentKey().getCetab() == null
                || poKey.getEstablishmentKey().getCetab().equals("") || poKey.getChrono() == null
                || poKey.getChrono().getPrechro() == null || poKey.getChrono().getPrechro().equals("")
                || poKey.getChrono().getChrono() == 0) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
        }
        try {
            Kot plc = null;
            plc = KotDAOFactory.getDAO().getByPK(ctx.getDbCtx(), poKey.getEstablishmentKey().getCsoc(),
                    poKey.getEstablishmentKey().getCetab(), poKey.getChrono().getPrechro(),
                    poKey.getChrono().getChrono(), poKey.getCounter1());
            bean = plc2POBean(ctx, plc);
        } catch (DAOException e) {
            LOGGER.error("Error while getting getPOBeanByKey");
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getPOBeanByKey(ctx=" + ctx + ", poKey=" + poKey + ")=" + bean);
        }
        return bean;
    }

    /**
     * Gets the productionResourcesSBS.
     * 
     * @return the productionResourcesSBS.
     */
    public ProductionResourcesSBS getProductionResourcesSBS() {
        return productionResourcesSBS;
    }

    /**
     * Gets the soaSBS.
     * 
     * @return the soaSBS.
     */
    public SoaSBS getSoaSBS() {
        return soaSBS;
    }

    /**
     * Gets the stackingPlanSBS.
     * 
     * @return the stackingPlanSBS.
     */
    public StackingPlanSBS getStackingPlanSBS() {
        return stackingPlanSBS;
    }

    /**
     * {@inheritDoc}
     */
    public void initPrinter(final VIFContext ctx, final String functionId, final String workstationId)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - initPrinter(ctx=" + ctx + ", functionId=" + functionId + ", workstationId="
                    + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug(new StringBuilder().append("initPrinter(ctx, functionId = ").append(functionId)
                    .append(", workstationId = ").append(workstationId).append(")").toString());
        }
        // TODO : Appeler le chargement des formats standards quand le paquet sera créé (GEN_???)
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prChargementFormat(functionId, ctx.getIdCtx().getCompany(), ctx.getIdCtx().getEstablishment(),
                workstationId, ctx.getIdCtx().getLogin(), oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - initPrinter(ctx=" + ctx + ", functionId=" + functionId + ", workstationId="
                    + workstationId + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<CriteriaCompleteBean> listCriterionMO(final VIFContext ctx, final MOBean bean)
            throws ServerBusinessException {
        LOGGER.debug("B - listCriterionMO(ctx=" + ctx + ", bean=" + bean + ")");
        List<CriteriaCompleteBean> list = null;
        if (bean == null || bean.getMoKey() == null || bean.getMoKey().getEstablishmentKey() == null
                || StringUtils.isEmpty(bean.getMoKey().getEstablishmentKey().getCsoc())
                || StringUtils.isEmpty(bean.getMoKey().getEstablishmentKey().getCetab())
                || StringUtils.isEmpty(bean.getMoKey().getChrono().getPrechro())) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(
                    "MoSBSImpl.listCriterionMO, bean or bean.Mokey is null"));
        }
        TempTableHolder<XxfabPPOTtCrivHandle> ttCrivHandleHolder = new TempTableHolder<XxfabPPOTtCrivHandle>();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCriteresof(bean.getMoKey().getEstablishmentKey().getCsoc(), //
                bean.getMoKey().getEstablishmentKey().getCetab(), //
                bean.getMoKey().getChrono().getPrechro(), //
                bean.getMoKey().getChrono().getChrono(), //
                ttCrivHandleHolder);
        List<XxfabPPOTtCrivHandle> lTtCrivHandles = null;
        if (ttCrivHandleHolder.getListValue() != null && !ttCrivHandleHolder.getListValue().isEmpty()) {
            lTtCrivHandles = ttCrivHandleHolder.getListValue();
            list = new ArrayList<CriteriaCompleteBean>();
            for (XxfabPPOTtCrivHandle ttCrivHandle : lTtCrivHandles) {
                list.add(convertTempTableToCriteriaCompleteBean(ttCrivHandle));
            }
        }
        LOGGER.debug("E - listCriterionMO(ctx=" + ctx + ", bean=" + bean + ")=" + list);
        return list;
    }

    /**
     * {@inheritDoc}
     */
    public List<CodeLabels> listItemCLsForFabrication(final VIFContext ctx, final String companyId,
            final String establishmentId, final String workstationId, final FItemSBean basicItemSelection)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Begin listItemCLsForFabrication :ctx=" + ctx + ";companyId=" + companyId
                    + ";establishmentId=" + establishmentId + ";workstationId=" + workstationId
                    + ";basicItemSelection=" + basicItemSelection);
        }
        ParamFieldValue listFamiliesObject = getParameterSBS().readFieldValue(ctx, companyId, establishmentId,
                ProductionMOParamEnum.WORKSTATION_MO_CREATION.getCode(), PYRAMID_WORKSTATION_PARAM_FAMILIES_NB,
                workstationId);
        List<CodeLabels> itemListFromFamily = null;
        List<CodeLabels> itemListFromGroupId = null;
        List<CodeLabels> listCLsItem = null;
        if (listFamiliesObject != null) {
            Object[] array = listFamiliesObject.getValues();
            List<String> listFamilies = new ArrayList<String>();
            for (Object object : array) {
                if (!((String) object).equals("")) {
                    listFamilies.add((String) object);
                }
            }
            if (listFamilies.size() != 0) {
                List<CodeLabel> listCLsFamilies = new ArrayList<CodeLabel>();
                for (String family : listFamilies) {
                    CodeLabel cls = new CodeLabel();
                    cls.setCode(family);
                    listCLsFamilies.add(cls);
                }
                PyramidDeepSelection pyramidDeepSelection = new PyramidDeepSelection();
                PyramidDeepValue pyramidDeepValue = new PyramidDeepValue();
                HchyLstValue hchyLstValue = new HchyLstValue();
                hchyLstValue.setIncluded(true);
                hchyLstValue.setLevel(1);
                hchyLstValue.setHierarchy(new CodeLabel(ITEM_FAMILY_HIERARCHY, ""));
                FlipFlopBean flipflopHchyValue = new FlipFlopBean();
                flipflopHchyValue.setLstResu(listCLsFamilies);
                hchyLstValue.setFlipflopHchyValue(flipflopHchyValue);
                pyramidDeepValue.setHchyLstValue(hchyLstValue);
                List<PyramidDeepValue> listDetail = new ArrayList<PyramidDeepValue>();
                listDetail.add(pyramidDeepValue);
                pyramidDeepSelection.setListDetail(listDetail);
                itemListFromFamily = getItemSBS().listItemCL(ctx, pyramidDeepSelection, basicItemSelection);
            }
        }
        String groupId = getParameterSBS().readStringParameter(ctx, companyId, establishmentId,
                ProductionMOParamEnum.WORKSTATION_MO_CREATION.getCode(), PYRAMID_WORKSTATION_PARAM_GROUPID_NB,
                workstationId);
        if (groupId != null && !"".equals(groupId)) {
            PyramidFastSelection pyramidFastSelection = new PyramidFastSelection(new CodeLabel(groupId, ""), 1, null,
                    null);
            itemListFromGroupId = getItemSBS().listItemCL(ctx, pyramidFastSelection, basicItemSelection);
        }
        if (itemListFromFamily != null && itemListFromGroupId != null) {
            listCLsItem = new ArrayList<CodeLabels>();
            for (CodeLabels codeLabels : itemListFromFamily) {
                if (itemListFromGroupId.indexOf(codeLabels) != -1) {
                    listCLsItem.add(codeLabels);
                }
            }
        } else if (itemListFromFamily != null) {
            listCLsItem = itemListFromFamily;
        } else if (itemListFromGroupId != null) {
            listCLsItem = itemListFromGroupId;
        } else {
            listCLsItem = new ArrayList<CodeLabels>();
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("End listItemCLsForFabrication");
        }
        return listCLsItem;
    }

    /**
     * {@inheritDoc}
     */
    public List<CodeLabels> listItemCLsForFabrication(final VIFContext ctx, final String companyId,
            final String establishmentId, final String workstationId, final FItemSBean basicItemSelection,
            final int startIndex, final int rowNumber) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("Begin listItemCLsForFabrication :ctx=" + ctx + ";companyId=" + companyId
                    + ";establishmentId=" + establishmentId + ";workstationId=" + workstationId
                    + ";basicItemSelection=" + basicItemSelection);
        }
        ParamFieldValue listFamiliesObject = getParameterSBS().readFieldValue(ctx, companyId, establishmentId,
                ProductionMOParamEnum.WORKSTATION_MO_CREATION.getCode(), PYRAMID_WORKSTATION_PARAM_FAMILIES_NB,
                workstationId);
        List<CodeLabels> itemListFromFamily = null;
        List<CodeLabels> itemListFromGroupId = null;
        List<CodeLabels> listCLsItem = null;
        if (listFamiliesObject != null) {
            Object[] array = listFamiliesObject.getValues();
            List<String> listFamilies = new ArrayList<String>();
            for (Object object : array) {
                if (!((String) object).equals("")) {
                    listFamilies.add((String) object);
                }
            }
            if (listFamilies.size() != 0) {
                List<CodeLabel> listCLsFamilies = new ArrayList<CodeLabel>();
                for (String family : listFamilies) {
                    CodeLabel cls = new CodeLabel();
                    cls.setCode(family);
                    listCLsFamilies.add(cls);
                }
                PyramidDeepSelection pyramidDeepSelection = new PyramidDeepSelection();
                PyramidDeepValue pyramidDeepValue = new PyramidDeepValue();
                HchyLstValue hchyLstValue = new HchyLstValue();
                hchyLstValue.setIncluded(true);
                hchyLstValue.setLevel(1);
                hchyLstValue.setHierarchy(new CodeLabel(ITEM_FAMILY_HIERARCHY, ""));
                FlipFlopBean flipflopHchyValue = new FlipFlopBean();
                flipflopHchyValue.setLstResu(listCLsFamilies);
                hchyLstValue.setFlipflopHchyValue(flipflopHchyValue);
                pyramidDeepValue.setHchyLstValue(hchyLstValue);
                List<PyramidDeepValue> listDetail = new ArrayList<PyramidDeepValue>();
                listDetail.add(pyramidDeepValue);
                pyramidDeepSelection.setListDetail(listDetail);
                itemListFromFamily = getItemSBS().listItemCL(ctx, pyramidDeepSelection, basicItemSelection, startIndex,
                        rowNumber);
            }
        }
        String groupId = getParameterSBS().readStringParameter(ctx, companyId, establishmentId,
                ProductionMOParamEnum.WORKSTATION_MO_CREATION.getCode(), PYRAMID_WORKSTATION_PARAM_GROUPID_NB,
                workstationId);
        if (groupId != null && !"".equals(groupId)) {
            PyramidFastSelection pyramidFastSelection = new PyramidFastSelection(new CodeLabel(groupId, ""), 1, null,
                    null);
            itemListFromGroupId = getItemSBS().listItemCL(ctx, pyramidFastSelection, basicItemSelection, startIndex,
                    rowNumber);
        }
        if (itemListFromFamily != null && itemListFromGroupId != null) {
            listCLsItem = new ArrayList<CodeLabels>();
            for (CodeLabels codeLabels : itemListFromFamily) {
                if (itemListFromGroupId.indexOf(codeLabels) != -1) {
                    listCLsItem.add(codeLabels);
                }
            }
        } else if (itemListFromFamily != null) {
            listCLsItem = itemListFromFamily;
        } else if (itemListFromGroupId != null) {
            listCLsItem = itemListFromGroupId;
        } else {
            listCLsItem = new ArrayList<CodeLabels>();
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("End listItemCLsForFabrication");
        }
        return listCLsItem;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<KtpsxBean> listKtpsxBean(final VIFContext ctx, final EndDayMOSBean selection)
            throws ServerBusinessException {

        List<Ktpsx> ktpsxPlcs = new ArrayList<Ktpsx>();
        List<KtpsxBean> ktpsxsBeans = new ArrayList<KtpsxBean>();
        List<ProductionResourceBean> productionResourceBeans = new ArrayList<ProductionResourceBean>();
        try {

            // Retrieving of all production resources of area
            productionResourceBeans = getProductionResourcesSBS().listResource(ctx, selection.getCompanyId(),
                    selection.getEstablishmentId(), selection.getSector().getCode(),
                    ResourceType.ELEMENTARY_RESOURCE.getValue().toString(), ResourceType.MACHINE.getValue().toString());

            BoundBean timeBound = new BoundBean();
            DateBoundBean dateBound = selection.getDateBoundBean();
            if (selection.isTimeWindowContext()) {
                Calendar minTime = Calendar.getInstance();
                minTime.setTime(dateBound.getMinBound());
                Calendar maxTime = Calendar.getInstance();
                maxTime.setTime(dateBound.getMaxBound());
                timeBound = new BoundBean(minTime.get(Calendar.HOUR_OF_DAY) * SECONDS_IN_ONE_HOUR
                        + minTime.get(Calendar.MINUTE) * SECONDS_IN_ONE_MINUTE, maxTime.get(Calendar.HOUR_OF_DAY)
                        * SECONDS_IN_ONE_HOUR + maxTime.get(Calendar.MINUTE) * SECONDS_IN_ONE_MINUTE);
            }

            Map<Date, BoundBean> dates = getDatesBetweenTwoDates(dateBound, timeBound);

            // Retrieving of all unclosed measures for each production resource on the period.
            for (ProductionResourceBean productionResourceBean : productionResourceBeans) {

                if (selection.isTimeWindowContext()) {

                    for (Map.Entry<Date, BoundBean> entry : dates.entrySet()) {
                        ktpsxPlcs.addAll(KtpsxDAOFactory.getDAO().list(ctx.getDbCtx(),
                                new EstablishmentKey(selection.getCompanyId(), selection.getEstablishmentId()),
                                productionResourceBean.getCode(), entry.getKey(), entry.getValue(), true));
                    }

                } else {
                    // When we are not in a time window context, the dateBoundBean contains two same dates, so we can
                    // take the min or the max.
                    ktpsxPlcs.addAll(KtpsxDAOFactory.getDAO().list(ctx.getDbCtx(),
                            new EstablishmentKey(selection.getCompanyId(), selection.getEstablishmentId()),
                            productionResourceBean.getCode(), selection.getDateBoundBean().getMinBound(), null, false));
                }

            }

            if (ktpsxPlcs != null) {
                for (Ktpsx ktpsx : ktpsxPlcs) {
                    ktpsxsBeans.add(plc2KtpsxBean(ctx, ktpsx));
                }
            }

        } catch (DAOException e) {
            LOGGER.error("Error while getting listOperationItems");
            throw new ServerBusinessException(e);
        }

        return ktpsxsBeans;
    }

    /**
     * {@inheritDoc}
     */
    public List<MOBean> listMOBean(final VIFContext ctx, final MOSBean selection,
            final List<ActivityItemType> activityItemTypeList, final boolean showFinished, final int startIndex,
            final int rowNumber) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listMOBean(ctx=" + ctx + ", selection=" + selection + ", activityItemTypeList="
                    + activityItemTypeList + ", showFinished=" + showFinished + ", startIndex=" + startIndex
                    + ", rowNumber=" + rowNumber + ")");
        }
        List<MOBean> beanList = new ArrayList<MOBean>();
        List<Kof> plcList = null;
        MOSimpleSBean sbean = null;
        try {
            sbean = checkSelection(ctx, selection);
            plcList = KofDAOFactory.getDAO().listKof(ctx.getDbCtx(), sbean, activityItemTypeList, showFinished,
                    startIndex, rowNumber);
        } catch (DAOException e) {
            LOGGER.error("Error while getting listMOBean");
            throw new ServerBusinessException(e);
        }
        beanList = listPlc2MOBean(ctx, plcList, sbean);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listMOBean(ctx=" + ctx + ", selection=" + selection + ", activityItemTypeList="
                    + activityItemTypeList + ", showFinished=" + showFinished + ", startIndex=" + startIndex
                    + ", rowNumber=" + rowNumber + ")");
        }
        return beanList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MOMarkingBean> listMOMarkingBean(final VIFContext ctx, final MOMarkingSBean moSelection,
            final int startIndex, final int rowNumber) throws ServerBusinessException {
        List<MOMarkingBean> listXpiloia;
        try {
            listXpiloia = XpiloiaDAOFactory.getDAO().listOperationItemsMarking(ctx.getDbCtx(), moSelection, startIndex,
                    rowNumber);
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(
                        new StringBuilder().append("Unable to list operation items of marking manufacturing orders ")
                                .append("-").append(moSelection).toString(), e);
            }
            throw new ServerBusinessException(e);
        }
        return listXpiloia;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<POBean> listPOBeanByKey(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listPOBeanByKey(ctx=" + ctx + ", moKey=" + moKey + ")");
        }
        List<POBean> poBeanList = new ArrayList<POBean>();
        if (moKey == null || moKey.getEstablishmentKey() == null || moKey.getEstablishmentKey().getCsoc() == null
                || moKey.getEstablishmentKey().getCsoc().equals("") || moKey.getEstablishmentKey().getCetab() == null
                || moKey.getEstablishmentKey().getCetab().equals("") || moKey.getChrono() == null
                || moKey.getChrono().getPrechro() == null || moKey.getChrono().getPrechro().equals("")
                || moKey.getChrono().getChrono() == 0) {
            throw new ServerBusinessException(Generic.T5815, ProductionMo.T3881);
        }
        try {
            List<Kot> plcList = KotDAOFactory.getDAO().listByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), moKey.getChrono().getPrechro(),
                    moKey.getChrono().getChrono(), null, false);
            for (Kot kot : plcList) {
                poBeanList.add(plc2POBean(ctx, kot));
            }
        } catch (DAOException e) {
            LOGGER.error("Error while getting getPOBeanByKey");
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listPOBeanByKey(ctx=" + ctx + ", moKey=" + moKey + ")=" + poBeanList);
        }
        return poBeanList;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<XpiloiaBean> listXpiloiaBean(final VIFContext ctx, final EndDayMOSBean selection)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listXpiloiaBean(ctx=" + ctx + ", selection=" + selection + ")");
        }

        List<XpiloiaBean> xpiloias = new ArrayList<XpiloiaBean>();
        List<Xpiloia> plcList = new ArrayList<Xpiloia>();

        try {

            BoundBean timeBound = new BoundBean();
            if (selection.isTimeWindowContext()) {
                Calendar minTime = Calendar.getInstance();
                minTime.setTime(selection.getDateBoundBean().getMinBound());
                Calendar maxTime = Calendar.getInstance();
                maxTime.setTime(selection.getDateBoundBean().getMaxBound());
                timeBound = new BoundBean(minTime.get(Calendar.HOUR_OF_DAY) * SECONDS_IN_ONE_HOUR
                        + minTime.get(Calendar.MINUTE) * SECONDS_IN_ONE_MINUTE, maxTime.get(Calendar.HOUR_OF_DAY)
                        * SECONDS_IN_ONE_HOUR + maxTime.get(Calendar.MINUTE) * SECONDS_IN_ONE_MINUTE);
            }

            Map<Date, BoundBean> dates = getDatesBetweenTwoDates(selection.getDateBoundBean(), timeBound);

            if (selection.isTimeWindowContext()) {

                for (Map.Entry<Date, BoundBean> entry : dates.entrySet()) {
                    plcList.addAll(XpiloiaDAOFactory.getDAO().listOperationItems(ctx.getDbCtx(),
                            new EstablishmentKey(selection.getCompanyId(), selection.getEstablishmentId()),
                            selection.getSector().getCode(), entry.getKey(), entry.getValue(), true));
                }

            } else {
                // When we are not in a time window context, the dateBoundBean contains two same dates, so we can
                // take the min or the max.
                plcList.addAll(XpiloiaDAOFactory.getDAO().listOperationItems(ctx.getDbCtx(),
                        new EstablishmentKey(selection.getCompanyId(), selection.getEstablishmentId()),
                        selection.getSector().getCode(), selection.getDateBoundBean().getMinBound(), null, false));

            }

            if (plcList != null) {
                xpiloias = new ArrayList<XpiloiaBean>();
                for (Xpiloia xpiloia : plcList) {
                    xpiloias.add(plc2XpiloiaBean(ctx, xpiloia));
                }
            }

        } catch (DAOException e) {
            LOGGER.error("Error while getting listOperationItems");
            throw new ServerBusinessException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listXpiloiaBean(ctx=" + ctx + ", selection=" + selection + ")=" + xpiloias);
        }
        return xpiloias;
    }

    /**
     * Sets the activitiesSBS.
     * 
     * @category setter
     * @param activitiesSBS activitiesSBS.
     */
    public void setActivitiesSBS(final ActivitiesSBS activitiesSBS) {
        this.activitiesSBS = activitiesSBS;
    }

    /**
     * Sets the chronoSBS.
     * 
     * @category setter
     * @param chronoSBS chronoSBS.
     */
    public void setChronoSBS(final ChronoSBS chronoSBS) {
        this.chronoSBS = chronoSBS;
    }

    /**
     * Sets the commentSBS.
     * 
     * @category setter
     * @param commentSBS commentSBS.
     */
    public void setCommentSBS(final SimpleCommentSBS commentSBS) {
        this.commentSBS = commentSBS;
    }

    /**
     * Sets the criteriaSBS.
     * 
     * @param criteriaSBS criteriaSBS.
     */
    public void setCriteriaSBS(final CriteriaSBS criteriaSBS) {
        this.criteriaSBS = criteriaSBS;
    }

    /**
     * Sets the itemSBS.
     * 
     * @category setter
     * @param itemSBS itemSBS.
     */
    public void setItemSBS(final ItemSBS itemSBS) {
        this.itemSBS = itemSBS;
    }

    /**
     * Sets the parameterSBS.
     * 
     * @category setter
     * @param parameterSBS parameterSBS.
     */
    public void setParameterSBS(final ParameterSBS parameterSBS) {
        this.parameterSBS = parameterSBS;
    }

    /**
     * Sets the productionResourcesSBS.
     * 
     * @param productionResourcesSBS productionResourcesSBS.
     */
    public void setProductionResourcesSBS(final ProductionResourcesSBS productionResourcesSBS) {
        this.productionResourcesSBS = productionResourcesSBS;
    }

    /**
     * Sets the soaSBS.
     * 
     * @param soaSBS soaSBS.
     */
    public void setSoaSBS(final SoaSBS soaSBS) {
        this.soaSBS = soaSBS;
    }

    /**
     * Sets the stackingPlanSBS.
     * 
     * @param stackingPlanSBS stackingPlanSBS.
     */
    public void setStackingPlanSBS(final StackingPlanSBS stackingPlanSBS) {
        this.stackingPlanSBS = stackingPlanSBS;
    }

    /**
     * {@inheritDoc}
     */
    public MOKey takeMOChrono(final VIFContext ctx, final MOKey key) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - takeMOChrono(ctx=" + ctx + ", key=" + key + ")");
        }
        XxcreaofPPO ppo = new XxcreaofPPO(ctx);
        IntegerHolder retChrono = new IntegerHolder();
        ppo.prChronoOf(key.getEstablishmentKey().getCsoc(), key.getEstablishmentKey().getCetab(), key.getChrono()
                .getPrechro(), retChrono);
        key.getChrono().setChrono(retChrono.getIntegerValue());
        ppo.release(ctx.getDbCtx());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - takeMOChrono(ctx=" + ctx + ", key=" + key + ")=" + key);
        }
        return key;
    }

    /**
     * 
     * Return the MOSimpleSBean corresponding to the MOSbean.
     * 
     * @param ctx the VIF context
     * @param selection the selection
     * @return the selection bean
     * @throws ServerBusinessException if error occurs
     */
    private MOSimpleSBean checkSelection(final VIFContext ctx, final MOSBean selection) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkSelection(ctx=" + ctx + ", selection=" + selection + ")");
        }
        MOSimpleSBean sbean = null;
        if (selection == null || selection.getCompanyId() == null || selection.getEstablishmentId() == null) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
        } else {
            sbean = new MOSimpleSBean();
            sbean.setCompanyId(selection.getCompanyId());
            sbean.setEstablishmentId(selection.getEstablishmentId());
            if (selection.getWorkstationCL() != null) {
                sbean.setWorkstationId(selection.getWorkstationCL().getCode());
            }
            // Sort of the Mo List
            if (selection.getSortMoList() != null) {
                sbean.setSortMoList(selection.getSortMoList());
            } else {
                sbean.setSortMoList(Sorts.MO_LIST_DEFAULT_SORT);
            }
            sbean.setAreaIdList(selection.getAreaIdList());
            if (selection.getChrono() != 0) {
                // Selection with chrono
                if (selection.getMoUses() == null || selection.getMoUses().size() != 1) {
                    throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
                } else {
                    PrechroId prechroId = null;
                    switch (selection.getMoUses().get(0)) {
                        case CLASSIFICATION:
                            prechroId = PrechroId.CLASSIFICATION;
                            break;
                        case FORECAST:
                            prechroId = PrechroId.FORECAST;
                            break;
                        case MIXING:
                            prechroId = PrechroId.MIXING;
                            break;
                        case PICKING:
                            prechroId = PrechroId.PICKING;
                            break;
                        case PRODUCTION:
                            prechroId = PrechroId.PRODUCTION;
                            break;
                        case TRANSFER:
                            prechroId = PrechroId.TRANSFERT;
                            break;
                        default:
                            break;
                    }
                    sbean.setPrechro(getChronoSBS().getPrechro(ctx, prechroId.getValue()));
                    sbean.setChrono(selection.getChrono());
                }
            } else {
                // Selection without chrono

                // Selection with date bounds
                if (selection.getMoDateBoundSBean() != null) {
                    if (selection.getMoDateBoundSBean().getTimeBounds() != null
                            && (selection.getMoDateBoundSBean().getMoBoundReferenceType() == null || selection
                            .getMoDateBoundSBean().getDateBounds() == null)) {
                        throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
                    } else {
                        switch (selection.getMoDateBoundSBean().getMoBoundReferenceType()) {
                            case EFFECTIVE_BEGIN:
                                sbean.setEffectiveBeginDateBounds(selection.getMoDateBoundSBean().getDateBounds());
                                sbean.setEffectiveBeginTimeBounds(selection.getMoDateBoundSBean().getTimeBounds());
                                break;
                            case EFFECTIVE_END:
                                sbean.setEffectiveEndDateBounds(selection.getMoDateBoundSBean().getDateBounds());
                                sbean.setEffectiveEndTimeBounds(selection.getMoDateBoundSBean().getTimeBounds());
                                break;
                            case FORECAST_BEGIN:
                                sbean.setForecastBeginDateBounds(selection.getMoDateBoundSBean().getDateBounds());
                                sbean.setForecastBeginTimeBounds(selection.getMoDateBoundSBean().getTimeBounds());
                            case PRODUCTION_BEGIN:
                                // DP 1689: Add Search O.F by production day.
                                sbean.setProductionDateBounds(selection.getMoDateBoundSBean().getDateBounds());
                                break;
                            default:
                                break;
                        }
                    }
                }
                // Selection with states or uses
                if (selection.getMoUses() != null || selection.getMoStates() != null) {
                    if (selection.getMoStates() != null && selection.getMoUses() == null) {
                        throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
                    } else {
                        sbean.setMoUses(selection.getMoUses());
                        sbean.setMoStates(selection.getMoStates());
                    }
                }
                // Selection with team
                if (selection.getTeamCL() != null) {
                    sbean.setTeamId(selection.getTeamCL().getCode());
                }
                // Selection with production resource
                if (selection.getProductionResourceCL() != null) {
                    sbean.setProductionResourceId(selection.getProductionResourceCL().getCode());
                }
                sbean.setListCriterionToLoad(selection.getListCriterionToLoad());
                sbean.setLoadPrimaryResource(selection.getLoadPrimaryResource());
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkSelection(ctx=" + ctx + ", selection=" + selection + ")=" + sbean);
        }
        return sbean;
    }

    /**
     * Return a date initialized with date and time parameters.
     * 
     * @param date the date
     * @param time the time in type integer in ms
     * @return Date the dateTime
     */
    private Date computeDateTime(final Date date, final int time) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - computeDateTime(date=" + date + ", time=" + time + ")");
        }
        // OM - 17/10/2012 M123526 Bug on mapping of hour field which use in request
        Date result = DateHelper.getDateTime(date, time);
        /*
         * GregorianCalendar gcDateTime = new GregorianCalendar(); gcDateTime.setTime(time); GregorianCalendar gcDate =
         * new GregorianCalendar(); gcDate.setTime(date); gcDateTime.set(GregorianCalendar.YEAR,
         * gcDate.get(GregorianCalendar.YEAR)); gcDateTime.set(GregorianCalendar.MONTH,
         * gcDate.get(GregorianCalendar.MONTH)); gcDateTime.set(GregorianCalendar.DAY_OF_MONTH,
         * gcDate.get(GregorianCalendar.DAY_OF_MONTH));
         * 
         * return new Date(gcDateTime.getTimeInMillis());
         */
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - computeDateTime(date=" + date + ", time=" + time + ")=" + result);
        }
        return result;
    }

    /**
     * Convert the temp-table xxfabPPOTtCrivHandle into a CriteriaCompleteBean.
     * 
     * @param xxfabPPOTtCrivHandle Temp table
     * @return CriteriaCompleteBean
     */
    private CriteriaCompleteBean convertTempTableToCriteriaCompleteBean(final XxfabPPOTtCrivHandle xxfabPPOTtCrivHandle) {
        CriteriaCompleteBean criteriaCompleteBean = new CriteriaCompleteBean();
        criteriaCompleteBean.setCompanyId(xxfabPPOTtCrivHandle.getCsoc());
        criteriaCompleteBean.setEstablishmentId(xxfabPPOTtCrivHandle.getCetab());
        criteriaCompleteBean.setCriteriaId(xxfabPPOTtCrivHandle.getCcri());
        criteriaCompleteBean.setValueCarMax(xxfabPPOTtCrivHandle.getValcmax());
        criteriaCompleteBean.setValueCarMin(xxfabPPOTtCrivHandle.getValcmin());
        criteriaCompleteBean.setValueDateMax(xxfabPPOTtCrivHandle.getValdmax());
        criteriaCompleteBean.setValueDateMin(xxfabPPOTtCrivHandle.getValdmin());
        criteriaCompleteBean.setValueNumMax(xxfabPPOTtCrivHandle.getValnmax());
        criteriaCompleteBean.setValueNumMin(xxfabPPOTtCrivHandle.getValnmin());
        criteriaCompleteBean.setSeizureMode(xxfabPPOTtCrivHandle.getMsaisie());
        return criteriaCompleteBean;
    }

    /**
     * Return the comment key of the given object.
     * 
     * @param key the key
     * @return the comment key
     */
    @SuppressWarnings("deprecation")
    private CommentKey getCommentKey(final Object key) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getCommentKey(key=" + key + ")");
        }
        CommentKey commentKey = new CommentKey();
        if (MOKey.class.isInstance(key)) {
            MOKey moKey = (MOKey) key;
            commentKey.setCsoc(moKey.getEstablishmentKey().getCsoc());
            commentKey.setCetab(moKey.getEstablishmentKey().getCetab());
            commentKey.setKeyType(SimpleCommentKeyType.MANUFACTURING_ORDER.getKey());
            commentKey.setKey(StringHelper.concat(moKey.getChrono().getPrechro()).concat(",")
                    .concat(Integer.toString(moKey.getChrono().getChrono())));
        } else if (POKey.class.isInstance(key)) {
            POKey poKey = (POKey) key;
            commentKey.setCsoc(poKey.getEstablishmentKey().getCsoc());
            commentKey.setCetab(poKey.getEstablishmentKey().getCetab());
            commentKey.setKeyType(SimpleCommentKeyType.PROCESSING_ORDER.getKey());
            commentKey.setKey(StringHelper.concat(poKey.getChrono().getPrechro()).concat(",")
                    .concat(Integer.toString(poKey.getChrono().getChrono())).concat(",")
                    .concat(Integer.toString(poKey.getCounter1())));
        } else if (OperationItemKey.class.isInstance(key)) {
            OperationItemKey moItemKey = (OperationItemKey) key;
            commentKey.setCsoc(moItemKey.getEstablishmentKey().getCsoc());
            commentKey.setCetab(moItemKey.getEstablishmentKey().getCetab());
            commentKey.setKeyType(SimpleCommentKeyType.PROCESSING_ORDER.getKey());
            commentKey.setKey(StringHelper
                    .concat(moItemKey.getChrono().getPrechro())
                    .concat(",")
                    .concat(Integer.toString(moItemKey.getChrono().getChrono()))
                    .concat(",")
                    .concat(Integer.toString(moItemKey.getCounter1()).concat(",")
                            .concat(Integer.toString(moItemKey.getCounter4()))));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getCommentKey(key=" + key + ")=" + commentKey);
        }
        return commentKey;
    }

    /**
     * 
     * Get list of dates between two dates.
     * 
     * @param dateBoundBean date bound
     * @param timeBound time bound
     * @return a {@link Map} of Dates and time bounds
     */
    private Map<Date, BoundBean> getDatesBetweenTwoDates(final DateBoundBean dateBoundBean, final BoundBean timeBound) {
        List<Date> dates = new ArrayList<Date>();
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(dateBoundBean.getMinBound());
        while (calendar.getTime().before(dateBoundBean.getMaxBound())) {
            Date result = calendar.getTime();
            dates.add(result);
            calendar.add(Calendar.DATE, 1);
        }

        Map<Date, BoundBean> datesAndBounds = null;
        datesAndBounds = new HashMap<Date, BoundBean>();
        if (dates.size() != 0) {
            if (dates.size() == 1) {
                datesAndBounds.put(dates.get(0), new BoundBean(timeBound.getMinBound(), timeBound.getMaxBound()));
            } else if (dates.size() == 2) {
                datesAndBounds.put(dates.get(0), new BoundBean(timeBound.getMinBound(), SECONDS_IN_ONE_DAY));
                datesAndBounds.put(dates.get(1), new BoundBean(0, timeBound.getMaxBound()));
            } else {
                for (Date date : dates) {
                    if (dates.indexOf(date) == 0) {
                        datesAndBounds.put(date, new BoundBean(timeBound.getMinBound(), SECONDS_IN_ONE_DAY));
                    } else if (dates.indexOf(date) == dates.size() - 1) {
                        datesAndBounds.put(date, new BoundBean(0, timeBound.getMaxBound()));
                    } else {
                        datesAndBounds.put(date, new BoundBean(0, SECONDS_IN_ONE_DAY));

                    }
                }
            }
        }
        return datesAndBounds;
    }

    /**
     * Gets a Kof in database.
     * 
     * @param ctx ctx
     * @param moKey moKey
     * @return the Kof
     * @throws ServerBusinessException if errors occurs
     */
    private Kof getKofByKey(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getKofByKey(ctx=" + ctx + ", moKey=" + moKey + ")");
        }
        Kof plc = null;
        try {
            plc = KofDAOFactory.getDAO().getByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), moKey.getChrono().getPrechro(),
                    moKey.getChrono().getChrono());
        } catch (DAOException e) {
            LOGGER.error("Error while getting gettMOBeanByKey");
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getKofByKey(ctx=" + ctx + ", moKey=" + moKey + ")=" + plc);
        }
        return plc;
    }

    /**
     * Check if the code resource set in parameter is in the list of Primary Resource in parameter.
     * 
     * @param listPrimaryResources list of primary resource.
     * @param cres code resource to check
     * @return the primary resource as a CodeLabel Bean.
     */
    private CodeLabel isPrimaryRessource(final List<ProductionResourceBean> listPrimaryResources, final String cres) {
        CodeLabel primaryResourceCL = null;
        for (ProductionResourceBean resourceBean : listPrimaryResources) {
            if (resourceBean.getCode().equals(cres)) {
                primaryResourceCL = new CodeLabel(resourceBean.getCode(), resourceBean.getLabel());
                break;
            }
        }
        return primaryResourceCL;
    }

    /**
     * Convert a list of PLC to a list of MOBean.
     * 
     * @param ctx the VIF context
     * @param plcList the list of PLC bean to convert
     * @param sbean Selection (use the primaryRessource Id)
     * @return ListMOBean the list of MOBean corresponding to the PLC
     * @throws ServerBusinessException if error occurs
     */
    private List<MOBean> listPlc2MOBean(final VIFContext ctx, final List<Kof> plcList, final MOSimpleSBean sbean)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - plc2MOBean(ctx=" + ctx + ")");
        }
        Set<String> itemSet = new HashSet<String>();
        Set<String> activitySet = new HashSet<String>();
        Set<String> activitiesSequenceSet = new HashSet<String>();
        String companyId = null;
        String establishmentId = null;
        for (Kof kof : plcList) {
            itemSet.add(kof.getCarts());
            if (!kof.getCact().isEmpty()) {
                activitySet.add(kof.getCact());
            }
            if (!kof.getCiti().isEmpty()) {
                activitiesSequenceSet.add(kof.getCiti());
            }
            companyId = kof.getCsoc();
            establishmentId = kof.getCetab();
        }
        List<CodeLabels> itemCLsList = getItemSBS().listItemCL(ctx, new FItemSBean(companyId, null, true),
                new ArrayList<String>(itemSet));
        Map<String, CodeLabels> itemCLsMap = new HashMap<String, CodeLabels>();
        for (CodeLabels labels : itemCLsList) {
            itemCLsMap.put(labels.getCode(), labels);
        }
        // OM : 03/01/2014 DP1879 Manage family code assign
        Map<String, CodeLabels> activityCLsMap = new HashMap<String, CodeLabels>();
        Map<String, CodeLabel> activityFamilyCLMap = new HashMap<String, CodeLabel>();
        for (String cact : activitySet) {
            ActivityHeader actHeader = getActivitiesSBS().getActivityHeader(ctx, companyId, establishmentId, cact);
            // ALB: Q07305 24/07/2014 If activity has been deleted, anyway we have to be able to do Input/output
            // dataentry on
            // the manufacturing order
            if (actHeader != null) {
                activityCLsMap.put(cact, actHeader.getActivityCL());
                activityFamilyCLMap.put(actHeader.getActivityFamilyCL().getCode(), actHeader.getActivityFamilyCL());
            } else {
                activityCLsMap.put(cact, new CodeLabels(cact, "", ""));
            }
        }
        Map<String, CodeLabels> activitiesSequenceCLsMap = new HashMap<String, CodeLabels>();
        for (String citi : activitiesSequenceSet) {
            Soa actSequence = getSoaSBS().getSOAHeader(ctx, companyId, establishmentId, citi,
                    DateHelper.getTodayAtMidnight().getTime()); // no effect date to header info
            // ALB: Q07305 24/07/2014 If activities sequence has been deleted, anyway we have to be able to do
            // Input/output dataentry on
            // the manufacturing order
            if (actSequence != null) {
                activitiesSequenceCLsMap.put(citi, actSequence.getSoaCL());
            } else {
                activitiesSequenceCLsMap.put(citi, new CodeLabels(citi, "", ""));
            }
        }
        List<MOBean> beanList = new ArrayList<MOBean>();
        List<ProductionResourceBean> listPrimaryResources = null;
        // -- Primary resource is define in the model of the screen, so load all the primary resource --//
        if (sbean.getLoadPrimaryResource()) {
            listPrimaryResources = getProductionResourcesSBS().listResource(ctx, companyId, establishmentId, null,
                    ResourceType.ELEMENTARY_RESOURCE.getValue(), null, 1, null);
        }
        for (Kof plc : plcList) {
            if (plc != null) {
                MOBean bean = new MOBean();
                // MOKey
                MOKey moKey = new MOKey();
                moKey.setEstablishmentKey(new EstablishmentKey(plc.getCsoc(), plc.getCetab()));
                moKey.setChrono(new Chrono(plc.getPrechro(), plc.getChrono()));
                bean.setMoKey(moKey);
                // Other properties
                bean.setLabel(plc.getLof());
                bean.setShortLabel(plc.getRof());
                bean.setPriority(plc.getPrior());
                bean.setMoState(MOState.getMOState(plc.getCetat()));
                bean.setMoUse(MOUse.getMOUse(plc.getCofuse()));
                bean.setAdmstamp(plc.getAdmstamp());
                // Dates
                bean.setEffectiveBeginDateTime(computeDateTime(plc.getDatdebr(), plc.getHeurdebr()));
                bean.setEffectiveEndDateTime(computeDateTime(plc.getDatfinr(), plc.getHeurfinr()));
                bean.setForecastBeginDateTime(computeDateTime(plc.getDatdeb(), plc.getHeurdeb()));
                bean.setForecastEndDateTime(computeDateTime(plc.getDatfin(), plc.getHeurfin()));
                // Quantities and units
                MOQuantityUnit moQuantityUnit = new MOQuantityUnit();
                moQuantityUnit.setToDoQuantity(plc.getQte1());
                moQuantityUnit.setCompletedQuantity(plc.getQte2());
                moQuantityUnit.setTheoricQuantity(plc.getQte3());
                moQuantityUnit.setUnit(plc.getCunite());
                moQuantityUnit.setEffectiveQuantity(plc.getQtestk2());
                moQuantityUnit.setEffectiveUnit(plc.getCustk2());
                bean.setMoQuantityUnit(moQuantityUnit);
                bean.setFlowType(FlowType.getFlowType(plc.getTypflux()));
                // ItemCLs
                bean.setItemCLs(itemCLsMap.get(plc.getCarts()));
                bean.setMoType(MOType.getMOType(plc.getTypof()));
                beanList.add(bean);
                // Fabrication
                Fabrication fabrication = new Fabrication();
                // OM : 03/01/2014 DP1879 Manage family code assign
                if (plc.getCact() != null && !("".equals(plc.getCact()))) {
                    fabrication.setFabricationType(FabricationType.ACTIVITY);
                    fabrication.setFabricationCLs(activityCLsMap.get(plc.getCact()));
                } else {
                    fabrication.setFabricationType(FabricationType.ACTIVITIES_SEQUENCE);
                    fabrication.setFabricationCLs(activitiesSequenceCLsMap.get(plc.getCiti()));
                }

                // set the activity family. Should use the kot.cfmact
                String actFam = getActivityFamilyFromPO(ctx,
                        new POKey(moKey.getEstablishmentKey(), new ChronoPO(moKey.getChrono(), plc.getNi1artref())));
                if (actFam != null) {
                    if (activityFamilyCLMap.get(actFam) != null) {
                        fabrication.setFamilyCL(activityFamilyCLMap.get(actFam));
                    } else {
                        fabrication.setFamilyCL(new CodeLabel(actFam, ""));
                    }
                }
                bean.setFabrication(fabrication);
                if (bean.getMoType().equals(MOType.ORIGIN)) {
                    calculateQuantityLeftOnOriginMO(ctx, bean, plc);
                }

                // DP1931
                // ----------------------//
                // -- Chrono origin -- //
                // ----------------------//
                bean.setChronoOrig(new Chrono(plc.getPrechror(), plc.getChronor()));

                // -----------------------------//
                // --Load primary resource -- //
                // ---------------------------//
                // -- Case 1: The user define the primary in the model, and make a selection by primary resource -- //
                if (StringUtils.isNotEmpty(sbean.getProductionResourceId())) {
                    bean.setPrimaryRessource(getProductionResourcesSBS().getResourceCL(ctx, companyId, establishmentId,
                            sbean.getProductionResourceId()));
                } else if (sbean.getLoadPrimaryResource()) {
                    // -- Case 2: The user define the primary in the model, but don't made the selection by primary
                    // resource. -- //
                    try {
                        List<Kotod> listKotod = KotodDAOFactory.getDAO().listKotodWithListChrono(ctx.getDbCtx(),
                                new EstablishmentKey(companyId, establishmentId), plc.getPrechro(),
                                Arrays.asList(plc.getChrono()), null, null);
                        String codeResource = null;
                        CodeLabel primaryResrouceCL = null;
                        if (listKotod != null && !listKotod.isEmpty()) {
                            for (Kotod kotod : listKotod) {
                                codeResource = kotod.getCres();
                                if (StringUtils.isNotEmpty(codeResource)) {
                                    primaryResrouceCL = isPrimaryRessource(listPrimaryResources, codeResource);
                                    if (primaryResrouceCL != null) {
                                        bean.setPrimaryRessource(primaryResrouceCL);
                                        break;
                                    }
                                }
                            }
                        }
                    } catch (DAOException e) {
                        throw new ServerBusinessException(e);
                    }
                }

                // ----------------------------------------------------------------------------------------//
                // --If there is some criterion define in the model, load all the criterion for this MO --//
                // ---------------------------------------------------------------------------------------//
                setCriterionValues(ctx, bean, sbean, plc);
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - plc2MOBean(ctx=" + ctx + ")");
        }
        return beanList;
    }

    /**
     * 
     * To convert a Ktpsx plc to an Ktpsx Bean.
     * 
     * @param ctx the context
     * @param plc the ktpsx
     * @return KtpsxBean the converted plc
     * @throws ServerBusinessException the {@link ServerBusinessException} if error occurs
     */
    private KtpsxBean plc2KtpsxBean(final VIFContext ctx, final Ktpsx plc) throws ServerBusinessException {

        KtpsxBean bean = null;
        if (plc != null) {
            bean = new KtpsxBean();
            bean.setAdmstamp(plc.getAdmstamp());
            bean.setBeginDate(plc.getDatheurdebr());
            bean.setRessource(new CodeLabel(plc.getCres(), null));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - plc2XpiloiaBean(ctx=" + ctx + ", plc=" + plc + ")=" + bean);
        }
        return bean;
    }

    /**
     * Convert a PLC to a MOBean.
     * 
     * @param ctx the VIF context
     * @param plc the PLC bean to convert
     * @return MOBean the MOBean corresponding to the PLC
     * @throws ServerBusinessException if error occurs
     */
    private MOBean plc2MOBean(final VIFContext ctx, final Kof plc) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - plc2MOBean(ctx=" + ctx + ", plc=" + plc + ")");
        }
        MOBean bean = null;
        if (plc != null) {
            bean = new MOBean();
            // MOKey
            MOKey moKey = new MOKey();
            moKey.setEstablishmentKey(new EstablishmentKey(plc.getCsoc(), plc.getCetab()));
            moKey.setChrono(new Chrono(plc.getPrechro(), plc.getChrono()));
            bean.setMoKey(moKey);
            // Other properties
            bean.setLabel(plc.getLof());
            bean.setShortLabel(plc.getRof());
            bean.setPriority(plc.getPrior());
            bean.setMoState(MOState.getMOState(plc.getCetat()));
            bean.setMoUse(MOUse.getMOUse(plc.getCofuse()));
            bean.setMoType(MOType.getMOType(plc.getTypof())); /* A008374 M168648 ag 07/09/2015 */
            bean.setAdmstamp(plc.getAdmstamp());
            // Dates
            bean.setEffectiveBeginDateTime(computeDateTime(plc.getDatdebr(), plc.getHeurdebr()));
            bean.setEffectiveEndDateTime(computeDateTime(plc.getDatfinr(), plc.getHeurfinr()));
            bean.setForecastBeginDateTime(computeDateTime(plc.getDatdeb(), plc.getHeurdeb()));
            bean.setForecastEndDateTime(computeDateTime(plc.getDatfin(), plc.getHeurfin()));
            // Quantities and units
            MOQuantityUnit moQuantityUnit = new MOQuantityUnit();
            moQuantityUnit.setToDoQuantity(plc.getQte1());
            moQuantityUnit.setCompletedQuantity(plc.getQte2());
            moQuantityUnit.setTheoricQuantity(plc.getQte3());
            moQuantityUnit.setUnit(plc.getCunite());
            moQuantityUnit.setEffectiveQuantity(plc.getQtestk2());
            moQuantityUnit.setEffectiveUnit(plc.getCustk2());
            bean.setMoQuantityUnit(moQuantityUnit);
            bean.setFlowType(FlowType.getFlowType(plc.getTypflux()));
            // ItemCLs
            bean.setItemCLs(getItemSBS().getItemCLByPK(ctx, new ItemKey(plc.getCsoc(), plc.getCarts())));
            // Fabrication
            Fabrication fabrication = new Fabrication();
            // OM : 03/01/2014 DP1879 Manage family code assign
            if (plc.getCact() != null && !("".equals(plc.getCact()))) {
                fabrication.setFabricationType(FabricationType.ACTIVITY);
                ActivityHeader actHeader = getActivitiesSBS().getActivityHeader(ctx, plc.getCsoc(), plc.getCetab(),
                        plc.getCact());
                fabrication.setFabricationCLs(actHeader.getActivityCL());
                fabrication.setFamilyCL(actHeader.getActivityFamilyCL());
            } else {
                fabrication.setFabricationType(FabricationType.ACTIVITIES_SEQUENCE);
                Soa actSequence = getSoaSBS().getSOA(ctx, plc.getCsoc(), plc.getCetab(), plc.getCiti(),
                        bean.getEffectiveBeginDateTime());
                fabrication.setFabricationCLs(actSequence.getSoaCL());
                fabrication.setFamilyCL(actSequence.getSoaFamilyCL());
            }
            bean.setFabrication(fabrication);
            CodeLabel cresprim = new CodeLabel();
            cresprim.setCode(plc.getCresprim());
            bean.setPrimaryRessource(cresprim);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - plc2MOBean(ctx=" + ctx + ", plc=" + plc + ")=" + bean);
        }
        return bean;
    }

    /**
     * Convert a PLC to a MOBoningBean.
     * 
     * @param ctx ctx
     * @param plc plc
     * @return MOBoningBean
     * @throws ServerBusinessException if errors occurs
     */
    private MOBoningBean plc2MOBoningBean(final VIFContext ctx, final Kof plc) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - plc2MOBoningBean(ctx=" + ctx + ", plc=" + plc + ")");
        }
        MOBean moBean = plc2MOBean(ctx, plc);
        MOBoningBean moBoningBean = new MOBoningBean(moBean);
        moBoningBean.setOperatingProcess(new CodeLabel(plc.getCres(), ""));
        CodeLabel cl = getStackingPlanSBS().getMOStackingPlanCode(ctx, moBean.getMoKey());
        if (cl != null && cl.getCode() != null) {
            moBoningBean.setStackingPlan(cl);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - plc2MOBoningBean(ctx=" + ctx + ", plc=" + plc + ")=" + moBoningBean);
        }
        return moBoningBean;
    }

    /**
     * Convert a PLC to a POBean.
     * 
     * @param ctx the VIF context
     * @param plc the PLC bean to convert
     * @return POBean the POBean corresponding to the PLC
     * @throws ServerBusinessException if error occurs
     */
    private POBean plc2POBean(final VIFContext ctx, final Kot plc) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - plc2POBean(ctx=" + ctx + ", plc=" + plc + ")");
        }
        POBean bean = null;
        if (plc != null) {
            bean = new POBean();
            // POKey
            POKey poKey = new POKey();
            poKey.setEstablishmentKey(new EstablishmentKey(plc.getCsoc(), plc.getCetab()));
            poKey.setChrono(new ChronoPO(plc.getPrechro(), plc.getChrono(), plc.getNi1()));
            bean.setPoKey(poKey);
            bean.setResourceId(plc.getCres());
            // Dates
            bean.setEffectiveBeginDateTime(computeDateTime(plc.getDatdebr(), plc.getHeurdebr()));
            bean.setEffectiveEndDateTime(computeDateTime(plc.getDatfinr(), plc.getHeurfinr()));
            bean.setForecastBeginDateTime(computeDateTime(plc.getDatdeb(), plc.getHeurdeb()));
            bean.setForecastEndDateTime(computeDateTime(plc.getDatfin(), plc.getHeurfin()));
            // Quantities and units
            MOQuantityUnit moQuantityUnit = new MOQuantityUnit();
            moQuantityUnit.setToDoQuantity(plc.getQte1());
            moQuantityUnit.setCompletedQuantity(plc.getQte2());
            moQuantityUnit.setTheoricQuantity(plc.getQte3());
            moQuantityUnit.setUnit(plc.getCunite());
            moQuantityUnit.setEffectiveQuantity(plc.getQtestk());
            moQuantityUnit.setEffectiveUnit(plc.getCunite());
            bean.setMoQuantityUnit(moQuantityUnit);
            // ItemCLs
            bean.setItemCLs(getItemSBS().getItemCLByPK(ctx, new ItemKey(plc.getCsoc(), plc.getCart())));
            // ActivityCLs
            bean.setActivityCLs(getActivitiesSBS().getActivityCLs(ctx, plc.getCsoc(), plc.getCetab(), plc.getCact()));
            // Other properties
            bean.setProgress(Progress.getProgress(plc.getCavanc()));
            bean.setActivityFamilyId(plc.getCfmact());
            bean.setReferenceItemCounter2(plc.getNi2artref());
            bean.setAdmstamp(plc.getAdmstamp());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - plc2POBean(ctx=" + ctx + ", plc=" + plc + ")=" + bean);
        }
        return bean;
    }

    /**
     * 
     * To convert a Xpiloia plc to an Xpiloia Bean.
     * 
     * @param ctx the context
     * @param plc the xpiloia
     * @return XpiloiaBean the converted plc
     * @throws ServerBusinessException the {@link ServerBusinessException} if error occurs
     */
    private XpiloiaBean plc2XpiloiaBean(final VIFContext ctx, final Xpiloia plc) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - plc2XpiloiaBean(ctx=" + ctx + ", plc=" + plc + ")");
        }

        XpiloiaBean bean = null;
        if (plc != null) {
            bean = new XpiloiaBean();
            EstablishmentKey establishmentKey = new EstablishmentKey(plc.getCsoc(), plc.getCetab());
            Chrono chrono = new Chrono(plc.getPrechrof(), plc.getChronof());
            // XpiloiaKey
            XpiloiaKey xpiloiaKey = new XpiloiaKey();
            xpiloiaKey.setEstablishmentKey(establishmentKey);
            xpiloiaKey.setChrono(chrono);
            xpiloiaKey.setTypGest(plc.getTypgest());
            bean.setXpiloiaKey(xpiloiaKey);
            bean.setAdmstamp(plc.getAdmstamp());
            bean.setArt(new CodeLabel(plc.getCart(), plc.getLart()));
            bean.setPoste(new CodeLabel(plc.getCposte(), plc.getLposte()));
            bean.setTypacta(plc.getTypacta());
            MOKey moKey = new MOKey(establishmentKey, chrono);
            bean.setLine(getMOBeanByKey(ctx, moKey).getPrimaryRessource().getCode());
            bean.setLot(plc.getLot());
            bean.setClDepot(new CodeLabel(plc.getCdepot(), plc.getLdepot()));
            bean.setQtartafer1(plc.getQtartafer1());
            bean.setQtartfait1(plc.getQtartfait1());
            bean.setCuart1(plc.getCuart1());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - plc2XpiloiaBean(ctx=" + ctx + ", plc=" + plc + ")=" + bean);
        }
        return bean;
    }

    /**
     * Set the values and the calculated values of the criterion define in touch screen model.
     * 
     * @param ctx Vif Context
     * @param bean Bean for which values and values calculated will be set.
     * @param sbean Selection bean
     * @param kof Mo in progress
     * @throws ServerBusinessException if erros occurs
     */
    private void setCriterionValues(final VIFContext ctx, final MOBean bean, final MOSimpleSBean sbean, final Kof kof)
            throws ServerBusinessException {
        if (sbean.getListCriterionToLoad() != null && !sbean.getListCriterionToLoad().isEmpty()) {

            // --Example : The user define a model like this : --//
            // --criterion : $DLUO --> will be set in bean.setValueCrit1--//
            // --criterion to restituate : $DATEFAB will be set in bean.setValueCalculatedCrit1--//
            List<TouchModelArray> listCriterionInModel = sbean.getListCriterionToLoad();
            List<CriteriaCompleteBean> listCriterionMo = listCriterionMO(ctx, bean);
            if (listCriterionMo != null && !listCriterionMo.isEmpty()) {
                int nbValueCrit = 0;
                int nbValueCalculatedCrit = 0;
                for (TouchModelArray touchModelArray : listCriterionInModel) {
                    for (CriteriaCompleteBean criteriaCompleteBean : listCriterionMo) {
                        if (touchModelArray.getCodeCritere().equals(criteriaCompleteBean.getCriteriaId())) {
                            Mode mode = Mode.VALUE_CRIT;
                            if (touchModelArray.isValueRestituee()) {
                                mode = Mode.CALCULATED_VALUE_CRIT;
                            }
                            CriteriaValueBean criteriaValueBean = getCriteriaSBS().getLabelValueCalculatedValue(ctx,
                                    mode, criteriaCompleteBean, kof.getLot() + "|" + kof.getCarts());
                            if (criteriaValueBean != null) {
                                if (touchModelArray.isValueRestituee()) {
                                    nbValueCalculatedCrit = nbValueCalculatedCrit + 1;
                                    if (nbValueCalculatedCrit == TouchModel.FIRST_CRITERION) {
                                        bean.setValueCalculatedCrit1(criteriaValueBean.getValueUnformatted());
                                    } else if (nbValueCalculatedCrit == TouchModel.SECOND_CRITERION) {
                                        bean.setValueCalculatedCrit2(criteriaValueBean.getValueUnformatted());
                                    } else if (nbValueCalculatedCrit == TouchModel.THIRD_CRITERION) {
                                        bean.setValueCalculatedCrit3(criteriaValueBean.getValueUnformatted());
                                    } else if (nbValueCalculatedCrit == TouchModel.FOURTH_CRITERION) {
                                        bean.setValueCalculatedCrit4(criteriaValueBean.getValueUnformatted());
                                    } else if (nbValueCalculatedCrit == TouchModel.FIFTH_CRITERION) {
                                        bean.setValueCalculatedCrit5(criteriaValueBean.getValueUnformatted());
                                    }
                                } else {
                                    nbValueCrit = nbValueCrit + 1;
                                    if (nbValueCrit == TouchModel.FIRST_CRITERION) {
                                        bean.setValueCrit1(criteriaValueBean.getValueCharacter());
                                    } else if (nbValueCrit == TouchModel.SECOND_CRITERION) {
                                        bean.setValueCrit2(criteriaValueBean.getValueCharacter());
                                    } else if (nbValueCrit == TouchModel.THIRD_CRITERION) {
                                        bean.setValueCrit3(criteriaValueBean.getValueCharacter());
                                    } else if (nbValueCrit == TouchModel.FOURTH_CRITERION) {
                                        bean.setValueCrit4(criteriaValueBean.getValueCharacter());
                                    } else if (nbValueCrit == TouchModel.FIFTH_CRITERION) {
                                        bean.setValueCrit5(criteriaValueBean.getValueCharacter());
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

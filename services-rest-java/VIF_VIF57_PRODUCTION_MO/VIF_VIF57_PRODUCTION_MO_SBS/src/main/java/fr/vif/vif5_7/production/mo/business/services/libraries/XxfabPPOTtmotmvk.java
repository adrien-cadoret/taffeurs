/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPOTtmotmvk.java,v $
 * Created on 24 mars 2016 by presta15
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:tempxxfabpxy.p.xml.
 * 
 * @author presta15
 */
public class XxfabPPOTtmotmvk extends AbstractTempTable implements Serializable {


    private String cMotMvk = "";

    private String lMotMvk = "";


    /**
     * Default Constructor.
     *
     */
    public XxfabPPOTtmotmvk() {
        super();
    }

    /**
     * Gets the cMotMvk.
     *
     * @category getter
     * @return the cMotMvk.
     */
    public final String getCMotMvk() {
        return cMotMvk;
    }

    /**
     * Sets the cMotMvk.
     *
     * @category setter
     * @param cMotMvk cMotMvk.
     */
    public final void setCMotMvk(final String cMotMvk) {
        this.cMotMvk = cMotMvk;
    }

    /**
     * Gets the lMotMvk.
     *
     * @category getter
     * @return the lMotMvk.
     */
    public final String getLMotMvk() {
        return lMotMvk;
    }

    /**
     * Sets the lMotMvk.
     *
     * @category setter
     * @param lMotMvk lMotMvk.
     */
    public final void setLMotMvk(final String lMotMvk) {
        this.lMotMvk = lMotMvk;
    }

}

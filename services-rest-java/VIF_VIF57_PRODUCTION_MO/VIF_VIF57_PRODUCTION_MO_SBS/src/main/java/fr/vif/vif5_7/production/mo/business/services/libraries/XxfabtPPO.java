/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabtPPO.java,v $
 * Created on 17 mars 2009 by gp
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.StringHolder;


/**
 * Generated class from xxfabtpxy.p.
 * 
 * @author gp
 */
public class XxfabtPPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(XxfabtPPO.class);

    /**
     * Context.
     */
    private VIFContext          ctx;

    /**
     * Default Constructor.
     * 
     * @param ctx Database Context.
     */
    public XxfabtPPO(final VIFContext ctx) {
        super();
        this.ctx = ctx;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "xxfabtpxy.p";
    }

    /**
     * Generated Class prMajSaisieTemps.
     * 
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iNi1 int INPUT
     * @param iNi2 int INPUT
     * @param iNlig int INPUT
     * @param iNi4 int INPUT
     * @param oMsg String OUTPUT
     * @param oRet boolean OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prMajSaisieTemps(final String iCsoc, final String iCetab, final String iPrechro, final int iChrono,
            final int iNi1, final int iNi2, final int iNlig, final int iNi4, final StringHolder oMsg,
            final BooleanHolder oRet) throws ProgressBusinessException {

        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oMsg == null || oRet == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prMajSaisieTemps values :" + "oMsg = " + oMsg + "oRet = " + oRet);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - prMajSaisieTemps(iCsoc=" + iCsoc + ", iCetab=" + iCetab + ", iPrechro=" + iPrechro
                    + ", iChrono=" + iChrono + ", iNi1=" + iNi1 + ", iNi2=" + iNi2 + ", iNlig=" + iNlig + ", iNi4="
                    + iNi4 + ", oMsg=" + oMsg + ", oRet=" + oRet + ")");
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(10);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iNlig), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);
            params.addLogical(9, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-MAJ-saisie-temps", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oMsg.setStringValue((String) params.getOutputParameter(8));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(9));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - prMajSaisieTemps(iCsoc=" + iCsoc + ", iCetab=" + iCetab + ", iPrechro=" + iPrechro
                    + ", iChrono=" + iChrono + ", iNi1=" + iNi1 + ", iNi2=" + iNi2 + ", iNlig=" + iNlig + ", iNi4="
                    + iNi4 + ", oMsg=" + oMsg + ", oRet=" + oRet + ")");
        }
    }

}

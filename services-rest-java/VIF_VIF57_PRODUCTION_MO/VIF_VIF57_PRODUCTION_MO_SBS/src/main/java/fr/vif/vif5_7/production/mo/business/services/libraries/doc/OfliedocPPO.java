/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: OfliedocPPO.java,v $
 * Created on 01 août 2013 by vr
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.doc;


import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.Parameter;
import com.progress.open4gl.ProResultSet;
import com.progress.open4gl.ProResultSetMetaDataImpl;
import com.progress.open4gl.ProSQLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.ProInputTempTable;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.business.progress.TempTableHolder;


/**
 * Generated class from ofliedocpxy.p.
 * 
 * @author vr
 */
public class OfliedocPPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger             LOGGER             = Logger.getLogger(OfliedocPPO.class);

    /**
     * ProResultSetMetaDataImpl for the temp-table ttDocument.
     */
    private static ProResultSetMetaDataImpl metadatattDocument = null;

    /**
     * Context.
     */
    private VIFContext                      ctx;

    /**
     * Default Constructor.
     * 
     * @param ctx Database Context.
     */
    public OfliedocPPO(final VIFContext ctx) {
        super();
        this.ctx = ctx;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttDocument List of OfliedocPPOTtdocument
     * @return ProInputTempTable Convert a record set to a list of OfliedocPPOTtdocument
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettDocument(final List<OfliedocPPOTtdocument> ttDocument)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for OfliedocPPOTtdocument");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<OfliedocPPOTtdocument> iter = ttDocument.iterator();
        while (iter.hasNext()) {
            OfliedocPPOTtdocument row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getTitre());
            list.add(row.getPath());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttDocument.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttDocument.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattDocument() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttDocument");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattDocument == null) {
            metadatattDocument = new ProResultSetMetaDataImpl(2);
            metadatattDocument.setFieldMetaData(1, "titre", 0, Parameter.PRO_CHARACTER);
            metadatattDocument.setFieldMetaData(2, "path", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattDocument;
    }

    /**
     * Generated Class getDocument.
     * 
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iNi1 int INPUT
     * @param iCuser String INPUT
     * @param oDocument String OUTPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void getDocument(final String iCsoc, final String iCetab, final String iPrechro, final int iChrono,
            final int iNi1, final String iCuser, final StringHolder oDocument, final BooleanHolder oRet,
            final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oDocument == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : getDocument values :" + "oDocument = " + oDocument + "oRet = " + oRet
                        + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method getDocument " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = "
                    + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1 + " iCuser = " + iCuser + " oDocument = "
                    + oDocument + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(9);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addCharacter(5, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
            params.addLogical(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "getDocument", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oDocument.setStringValue((String) params.getOutputParameter(6));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(7));
            oMsg.setStringValue((String) params.getOutputParameter(8));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class getDocuments.
     * 
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iNi1 int INPUT
     * @param iCuser String INPUT
     * @param ttDocumentHolder TABLE OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void getDocuments(final String iCsoc, final String iCetab, final String iPrechro, final int iChrono,
            final int iNi1, final String iCuser, final TempTableHolder<OfliedocPPOTtdocument> ttDocumentHolder)
            throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttDocumentHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : getDocuments values :" + "ttDocumentHolder = " + ttDocumentHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method getDocuments " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = "
                    + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1 + " iCuser = " + iCuser
                    + " ttDocumentHolder = " + ttDocumentHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(7);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addCharacter(5, iCuser, ParamArrayMode.INPUT);
            params.addTable(6, null, ParamArrayMode.OUTPUT, getMetaDatattDocument());

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "getDocuments", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            // Make the temp-table ttDocument

            ttDocumentHolder.setListValue(convertRecordttDocument(params, 6));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "ofliedocpxy.p";
    }

    /**
     * convert a record set to a Temp table.
     * 
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;OfliedocPPOTtdocument&gt; Convert a record set to a list of OfliedocPPOTtdocument
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<OfliedocPPOTtdocument> convertRecordttDocument(final ParamArray params, final int nbParam)
            throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for OfliedocPPOTtdocument");
        }

        ProResultSet rs = null;
        List<OfliedocPPOTtdocument> list = new ArrayList<OfliedocPPOTtdocument>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                OfliedocPPOTtdocument tempTable = new OfliedocPPOTtdocument();
                /* CHECKSTYLE:OFF */
                tempTable.setTitre(rs.getString("titre"));
                tempTable.setPath(rs.getString("path"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }

        return list;
    }

}

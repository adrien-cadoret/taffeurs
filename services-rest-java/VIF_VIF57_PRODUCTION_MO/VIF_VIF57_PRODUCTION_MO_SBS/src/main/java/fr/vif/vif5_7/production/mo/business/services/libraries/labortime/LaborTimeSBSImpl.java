/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: LaborTimeSBSImpl.java,v $
 * Created on 12 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.labortime;


import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.BusinessException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.util.i18n.I18nServerManager;
import fr.vif.jtech.common.IdContext;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.activities.activities.business.beans.common.resources.ProductionResourceBean;
import fr.vif.vif5_7.activities.activities.business.services.libraries.resources.ProductionResourcesSBS;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborStaff;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborStaffKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTime;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceBean;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceSBean;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborTimeBBean;
import fr.vif.vif5_7.production.mo.business.services.libraries.resources.MoResourceSBS;
import fr.vif.vif5_7.production.mo.dao.ktpsx.Ktpsx;
import fr.vif.vif5_7.production.mo.dao.ktpsx.KtpsxDAO;
import fr.vif.vif5_7.production.mo.dao.ktpsx.KtpsxDAOFactory;


/**
 * the Labor Time SBS.
 * 
 * @author nle
 */
@Component
public class LaborTimeSBSImpl implements LaborTimeSBS {

    protected static final Logger  LOGGER       = Logger.getLogger(LaborTimeSBSImpl.class);

    private static final String    DATE_PATTERN = "dd/MM/yy HH:mm";

    @Autowired
    private LaborStaffSBS          laborstaffSBS;

    @Autowired
    private ProductionResourcesSBS productionResourcesSBS;

    @Autowired
    private MoResourceSBS          moResourceSBS;

    @Override
    public void backToWork(final VIFContext ctx, final LaborTimeKey ltk) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - backToWork(ctx=" + ctx + ", ltk=" + ltk + ")");
        }
        FLaborTimeBBean lastLaborTime = getLastLaborTimeFromKey(ctx, ltk);
        // the last period must be closed (the end date-hour must not be empty)
        if (lastLaborTime != null && lastLaborTime.getEndDateHour() != null) {
            // gets the current date-hour
            Date currentDate = DateHelper.getTodayTime().getTime();
            Date datdebr = currentDate;
            Date heurdebr = currentDate;
            // creates a list of database labor staffs for the labor time key
            LaborStaffKey lsk = new LaborStaffKey(ltk, "");
            List<LaborStaff> lsList = null;
            try {
                lsList = getLaborstaffSBS().listLaborStaff(ctx, lsk);
            } catch (ServerBusinessException e) {
                LOGGER.error(I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, Jtech.T16809,
                        new VarParamTranslation(ltk)), e);
                throw e;
            }
            if (lsList != null) {
                LaborTime laborTime = new LaborTime();
                laborTime.setLaborTimeKey(ltk);
                laborTime.setBegDateHour(DateHelper.getDate(datdebr, heurdebr));
                laborTime.setEndDateHour(null);
                laborTime.setModificationDate(new Date());
                laborTime.setModificationUser(ctx.getIdCtx().getLogin());
                // inserts 1 LaborTime with the new date-hour
                int ni1 = 0;
                try {
                    ni1 = insertLastLaborTime(ctx, laborTime);
                } catch (ServerBusinessException e) {
                    LOGGER.error(I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, Jtech.T16809,
                            new VarParamTranslation(ltk)), e);
                    throw e;
                }
                // inserts N LaborStaff with the new ni1
                if (ni1 > 0) {
                    for (LaborStaff laborStaff : lsList) {
                        laborStaff.getLaborStaffKey().getLaborTimeKey().setNi1(ni1);
                        try {
                            // inserts
                            getLaborstaffSBS().insertLaborStaff(ctx, laborStaff);
                        } catch (ServerBusinessException e) {
                            LOGGER.error(I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, Jtech.T16809,
                                    new VarParamTranslation(ltk)), e);
                            throw e;
                        }
                    }
                }
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - backToWork(ctx=" + ctx + ", ltk=" + ltk + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void deleteLaborTime(final VIFContext ctx, final LaborTime laborTime) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - deleteLaborTime(" + laborTime + ")");
        }
        LaborTimeKey laborTimeKey = laborTime.getLaborTimeKey();
        if (!isValid(laborTimeKey)) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborTime.toString()));
        }
        try {
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("P - Try to delete laborTime for key " + laborTimeKey);
            }
            Ktpsx ktpsx = KtpsxDAOFactory.getDAO().getByPKForUpdate(ctx.getDbCtx(), laborTimeKey.getEstKey().getCsoc(),
                    laborTimeKey.getEstKey().getCetab(), laborTimeKey.getProductionDate(),
                    laborTimeKey.getResourceCode(), laborTimeKey.getNi1(), laborTime.getAdmstamp());
            KtpsxDAOFactory.getDAO().delete(ctx.getDbCtx(), convertLaborTimeToKtpsx(laborTime, ktpsx));
        } catch (DAOException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("Unable to delete the labor time corresponding to the key " + laborTimeKey, e);
            }
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - deleteLaborTime(" + laborTimeKey + ")");
        }
    }

    @Override
    public void endOfWork(final VIFContext ctx, final LaborTimeKey ltk) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - endOfWork(ctx=" + ctx + ", ltk=" + ltk + ")");
        }
        FLaborTimeBBean lastLT = getLastLaborTimeFromKey(ctx, ltk);
        // the last period must be open (the end date-hour must be empty)
        if (lastLT != null && lastLT.getEndDateHour() == null) {
            // converts and sets the current date-hour to String
            lastLT.setEndDateHour(DateHelper.getTodayTime().getTime());

            LaborTime lt = convertFLaborTimeBBeanToLaborTime(ctx.getIdCtx(), lastLT);
            try {
                // updates
                updateLaborTime(ctx, lt);
            } catch (ServerBusinessException e) {
                LOGGER.error(I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, Jtech.T16809,
                        new VarParamTranslation(ltk)), e);
                throw e;
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - endOfWork(ctx=" + ctx + ", ltk=" + ltk + ")");
        }
    }

    /**
     * Gets the laborStaffSBS.
     * 
     * @return the laborStaffSBS.
     */
    public LaborStaffSBS getLaborstaffSBS() {
        return laborstaffSBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LaborTime getLaborTime(final VIFContext ctx, final LaborTimeKey laborTimeKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLaborTime(ctx=" + ctx + ", laborTimeKey=" + laborTimeKey + ")");
        }
        LaborTime laborTime = null;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLaborTime(" + laborTimeKey + ")");
        }
        if (!isValid(laborTimeKey)) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborTimeKey.toString()));
        }
        try {
            laborTime = convertKtpsxToLaborTime(KtpsxDAOFactory.getDAO().getByPK(ctx.getDbCtx(),
                    laborTimeKey.getEstKey().getCsoc(), laborTimeKey.getEstKey().getCetab(),
                    laborTimeKey.getProductionDate(), laborTimeKey.getResourceCode(), laborTimeKey.getNi1()));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getLaborTime(" + laborTimeKey + ") : " + laborTime);
        }
        return laborTime;
    }

    /**
     * Gets the moResourceSBS.
     * 
     * @return the moResourceSBS.
     */
    public MoResourceSBS getMoResourceSBS() {
        return moResourceSBS;
    }

    /**
     * Finds the production line of the first Mo of a list.
     * 
     * @param ctx : the context
     * @param estKey : the establishment key
     * @param chronoList : the list of MO prechros-chronos
     * @return : the production line
     * @throws ServerBusinessException if fails
     */
    public String getProductionLine(final VIFContext ctx, final EstablishmentKey estKey, final List<Chrono> chronoList)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getProductionLine(ctx=" + ctx + ", estKey=" + estKey + ", chronoList=" + chronoList + ")");
        }
        String productionLine = "";
        // At least one MO in the list.
        if (chronoList != null && !chronoList.isEmpty() && chronoList.get(0) != null) {
            // For now, we only use the production line of the first MO of the list.
            Chrono myChrono = chronoList.get(0);

            // The production line is the primary resource of the MO (among its KOTODs).

            // List of the KOTODs of the MO.
            MOResourceSBean selection = new MOResourceSBean();
            selection.setEstablishmentKey(estKey);
            List<Chrono> lstChrono = new ArrayList<Chrono>();
            lstChrono.add(myChrono);
            selection.setLstChrono(lstChrono);
            selection.setSelectionOn(MOResourceSBean.SelectionOn.MO);
            List<MOResourceBean> lstMORes = new ArrayList<MOResourceBean>();
            try {
                lstMORes = getMoResourceSBS().listResources(ctx, selection);
            } catch (ServerBusinessException e) {
                throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(selection));
            }
            // List of the primary resources of the establishment.
            List<ProductionResourceBean> lstProdRes = new ArrayList<ProductionResourceBean>();
            try {
                lstProdRes = getProductionResourcesSBS().listResource(ctx, estKey.getCsoc(), estKey.getCetab(), null,
                        "RE", null, 1, null);
            } catch (ServerBusinessException e) {
                I18nServerManager.translate(ctx.getIdCtx().getLocale(), false, Generic.T5815, new VarParamTranslation(
                        estKey));
                throw e;
            }
            // the PRODUCTION LINE is the resource among the KOTODs that's part of the primary resources
            for (MOResourceBean kotod : lstMORes) {
                for (ProductionResourceBean prBean : lstProdRes) {
                    if (prBean.getCode().equals((kotod.getCres()))) {
                        productionLine = kotod.getCres();
                        break;
                    }
                }
                if (!"".equals(productionLine)) {
                    break;
                }
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getProductionLine(ctx=" + ctx + ", estKey=" + estKey + ", chronoList=" + chronoList
                    + ")=" + productionLine);
        }
        return productionLine;
    }

    /**
     * Gets the productionResourcesSBS.
     * 
     * @return the productionResourcesSBS.
     */
    public ProductionResourcesSBS getProductionResourcesSBS() {
        return productionResourcesSBS;
    }

    /**
     * {@inheritDoc}
     */
    public String getResourceLabel(final VIFContext ctx, final EstablishmentKey estKey, final String resourceCode)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getResourceLabel(ctx=" + ctx + ", estKey=" + estKey + ", resourceCode=" + resourceCode
                    + ")");
        }
        // searches and feeds the lres
        String result = "";
        CodeLabel resCL = getProductionResourcesSBS().getResourceCL(ctx, estKey.getCsoc(), estKey.getCetab(),
                resourceCode);
        if (resCL != null) {
            result = resCL.getLabel();
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getResourceLabel(ctx=" + ctx + ", estKey=" + estKey + ", resourceCode=" + resourceCode
                    + ")=" + result);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LaborTime insertLaborTime(final VIFContext ctx, final LaborTime laborTime) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertLaborTime(" + laborTime + ")");
        }
        LaborTime laborTimeAfterInsert = null;
        LaborTimeKey laborTimeKey = new LaborTimeKey(laborTime);
        if (!isValid(laborTimeKey)) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborTime.toString()));
        }
        try {
            // insert
            KtpsxDAOFactory.getDAO().insert(ctx.getDbCtx(), convertLaborTimeToKtpsx(laborTime, new Ktpsx()));
            laborTimeAfterInsert = convertKtpsxToLaborTime(KtpsxDAOFactory.getDAO().getByPK(ctx.getDbCtx(),
                    laborTimeKey.getEstKey().getCsoc(), laborTimeKey.getEstKey().getCetab(),
                    laborTimeKey.getProductionDate(), laborTimeKey.getResourceCode(), laborTimeKey.getNi1()));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertLaborTime(" + laborTime + ") : " + laborTimeAfterInsert);
        }
        return laborTimeAfterInsert;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int insertLastLaborTime(final VIFContext ctx, final LaborTime laborTime) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - insertLastLaborTime(" + laborTime + ")");
        }
        LaborTime laborTimeConverted = null;
        int ni1 = 0;
        Ktpsx lastKtpsx = null;
        // finds the next ni1
        try {
            List<Ktpsx> lstKtpsx = KtpsxDAOFactory.getDAO().list(ctx.getDbCtx(), laborTime.getLaborTimeKey(), true);
            for (Ktpsx ktpsx : lstKtpsx) {
                if (ktpsx.getNi1() > ni1) {
                    ni1 = ktpsx.getNi1();
                    lastKtpsx = ktpsx;
                }
            }
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborTime.toString()));
        }

        // controls
        //
        // example 1 : lastKtpsx : beginning 10h00 - end empty :
        // insert 9h00 : KO, 10h00 : KO, 11h00 : OK, 12h00 : OK, 13h00 : OK
        //
        // example 2 : lastKtpsx : beginning 10h00 - end 12h00 :
        // insert 9h00 : KO, 10h00 : KO, 11h00 : KO, 12h00 : OK, 13h00 : OK
        if (lastKtpsx != null) {
            if (lastKtpsx.getDatheurdebr() != null && (lastKtpsx.getDatheurdebr().after(laborTime.getBegDateHour()))
                    || lastKtpsx.getDatheurdebr().equals(laborTime.getBegDateHour())) {
                throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(I18nServerManager.translate(
                        ctx.getIdCtx().getLocale(), false, ProductionMo.T10182)));
            }
            if (lastKtpsx.getDatheurfinr() != null && lastKtpsx.getDatheurfinr().after(laborTime.getBegDateHour())) {
                throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(I18nServerManager.translate(
                        ctx.getIdCtx().getLocale(), false, ProductionMo.T10182)));
            }
        }

        // Updates the last KTPSX in order to feed its datfinr and heurfinr with the datdebr and heurdebr of the new one
        // created after, unless the begin date-time of the last KTPSX is after the ones on screen.
        // And only if the last end date-hour is null
        if (lastKtpsx != null && lastKtpsx.getDatheurfinr() == null) {
            Date datheurfinr = new Date();
            datheurfinr = laborTime.getBegDateHour();
            if (lastKtpsx.getDatheurdebr().after(laborTime.getBegDateHour())) {
                throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(I18nServerManager.translate(
                        ctx.getIdCtx().getLocale(), false, ProductionMo.T10182)));
            } else {
                lastKtpsx.setDatheurfinr(datheurfinr);
                try {
                    KtpsxDAOFactory.getDAO().update(ctx.getDbCtx(), lastKtpsx);
                } catch (DAOException e) {
                    LOGGER.error("", e);
                    throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(lastKtpsx.toString()));
                }
            }
        }
        ni1++;
        laborTime.getLaborTimeKey().setNi1(ni1);
        LaborTimeKey laborTimeKey = new LaborTimeKey(laborTime);
        if (!isValid(laborTimeKey)) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborTime.toString()));
        }
        try {
            KtpsxDAOFactory.getDAO().insert(ctx.getDbCtx(), convertLaborTimeToKtpsx(laborTime, new Ktpsx()));
            laborTimeConverted = convertKtpsxToLaborTime(KtpsxDAOFactory.getDAO().getByPK(ctx.getDbCtx(),
                    laborTimeKey.getEstKey().getCsoc(), laborTimeKey.getEstKey().getCetab(),
                    laborTimeKey.getProductionDate(), laborTimeKey.getResourceCode(), laborTimeKey.getNi1()));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - insertLastLaborTime(" + laborTime + ") : " + laborTimeConverted);
        }
        return ni1;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LaborTime> listLaborTime(final VIFContext ctx, final LaborTimeKey laborTimeKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listLaborTime(" + laborTimeKey + ")");
        }
        // not !isValid because the ni1 must be null for a list request
        if (laborTimeKey == null || laborTimeKey.getEstKey().getCsoc() == null
                || laborTimeKey.getEstKey().getCetab() == null || laborTimeKey.getProductionDate() == null
                || laborTimeKey.getResourceCode() == null) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborTimeKey.toString()));
        }
        List<LaborTime> list = null;
        try {
            KtpsxDAO ktpsxDAO = KtpsxDAOFactory.getDAO();
            list = convertKtpsxListToLaborTimeList(ktpsxDAO.listByPK(ctx.getDbCtx(),
                    laborTimeKey.getEstKey().getCsoc(), laborTimeKey.getEstKey().getCetab(),
                    laborTimeKey.getProductionDate(), laborTimeKey.getResourceCode(), null, // ni1 must be null to have
                    // a list and not a
                    // singleton
                    true));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listLaborTime(" + laborTimeKey + ") : " + list);
        }
        return list;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<LaborTime> listLaborTimeMod(final VIFContext ctx, final LaborTimeKey laborTimeKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listLaborTimeMod(" + laborTimeKey + ")");
        }
        if (!isValid(laborTimeKey)) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborTimeKey.toString()));
        }
        List<LaborTime> list = null;
        try {
            KtpsxDAO ktpsxDAO = KtpsxDAOFactory.getDAO();
            list = convertKtpsxListToLaborTimeList(ktpsxDAO.listByPK(ctx.getDbCtx(),
                    laborTimeKey.getEstKey().getCsoc(), laborTimeKey.getEstKey().getCetab(),
                    laborTimeKey.getProductionDate(), laborTimeKey.getResourceCode(), laborTimeKey.getNi1(), true));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listLaborTimeMod(" + laborTimeKey + ") : " + list);
        }
        return list;
    }

    /**
     * Sets the laborStaffSBS.
     * 
     * @param laborstaffSBS the labor staff SBS.
     */
    public void setLaborstaffSBS(final LaborStaffSBS laborstaffSBS) {
        this.laborstaffSBS = laborstaffSBS;
    }

    /**
     * Sets the moResourceSBS.
     * 
     * @param moResourceSBS moResourceSBS.
     */
    public void setMoResourceSBS(final MoResourceSBS moResourceSBS) {
        this.moResourceSBS = moResourceSBS;
    }

    /**
     * Sets the productionResourcesSBS.
     * 
     * @param productionResourcesSBS productionResourcesSBS.
     */
    public void setProductionResourcesSBS(final ProductionResourcesSBS productionResourcesSBS) {
        this.productionResourcesSBS = productionResourcesSBS;
    }

    @Override
    public void srvBackToWork(final VIFContext ctx, final EstablishmentKey estKey, final List<Chrono> chronoList,
            final Date productionDate, final String productionLine, final Date eventDateTime)
                    throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - srvBackToWork(ctx=" + ctx + ", estKey=" + estKey + ", chronoList=" + chronoList
                    + ", productionDate=" + productionDate + ", productionLine=" + productionLine + ", eventDateTime="
                    + eventDateTime + ")");
        }
        String resourceCode = productionLine;
        if (resourceCode == null) {
            try {
                // Gets the production line of the first MO of the list (NOT ALL OF THEM FOR NOW).
                resourceCode = getProductionLine(ctx, estKey, chronoList);
            } catch (ServerBusinessException e) {
                LOGGER.error("", e);
                throw new ServerBusinessException(Jtech.T16809, new VarParamTranslation(chronoList));
            }
        }
        if (resourceCode == null) {
            throw new ServerBusinessException(Generic.T19885, new VarParamTranslation(I18nServerManager.translate(ctx
                    .getIdCtx().getLocale(), false, ProductionMo.T32440)), new VarParamTranslation(null));
        } else {
            LaborTimeKey ltk = new LaborTimeKey();
            ltk.setEstKey(estKey);
            ltk.setProductionDate(productionDate);
            ltk.setResourceCode(resourceCode);
            FLaborTimeBBean lastLT = getLastLaborTimeFromKey(ctx, ltk);
            // the last period must be closed (the end date-hour must not be empty)
            if (lastLT != null && lastLT.getEndDateHour() != null) {
                // creates a list of database labor staffs for the labor time key
                LaborStaffKey lsk = new LaborStaffKey(ltk, "");
                List<LaborStaff> lsList = null;
                try {
                    lsList = getLaborstaffSBS().listLaborStaff(ctx, lsk);
                } catch (ServerBusinessException e) {
                    LOGGER.error("", e);
                    throw new ServerBusinessException(Jtech.T16809, new VarParamTranslation(ltk));
                }
                if (lsList != null) {
                    LaborTime laborTime = new LaborTime();
                    laborTime.setLaborTimeKey(ltk);
                    laborTime.setBegDateHour(DateHelper.getDate(eventDateTime, eventDateTime));
                    laborTime.setEndDateHour(null);
                    laborTime.setModificationDate(new Date());
                    laborTime.setModificationUser(ctx.getIdCtx().getLogin());
                    // inserts 1 LaborTime with the new date-hour
                    int ni1 = 0;
                    try {
                        ni1 = insertLastLaborTime(ctx, laborTime);
                    } catch (ServerBusinessException e) {
                        LOGGER.error("", e);
                        throw new ServerBusinessException(Jtech.T16809, new VarParamTranslation(ltk));
                    }
                    // inserts N LaborStaff with the new ni1
                    if (ni1 > 0) {
                        for (LaborStaff laborStaff : lsList) {
                            laborStaff.getLaborStaffKey().getLaborTimeKey().setNi1(ni1);
                            try {
                                // inserts
                                getLaborstaffSBS().insertLaborStaff(ctx, laborStaff);
                            } catch (ServerBusinessException e) {
                                LOGGER.error("", e);
                                throw new ServerBusinessException(Jtech.T16809, new VarParamTranslation(ltk));
                            }
                        }
                    }
                }
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - srvBackToWork(ctx=" + ctx + ", estKey=" + estKey + ", chronoList=" + chronoList
                    + ", productionDate=" + productionDate + ", productionLine=" + productionLine + ", eventDateTime="
                    + eventDateTime + ")");
        }
    }

    /**
     * Closes an open period for the production line given, or, if not, the one of the first MO of the list (NOT ALL OF
     * THEM FOR NOW).
     * 
     * @param ctx : the context
     * @param estKey : the establishment key
     * @param chronoList : the list of MO prechros-chronos
     * @param productionDate : the production date
     * @param productionLine : the production line
     * @param eventDateTime : the date and hour where the event occurs
     * @throws ServerBusinessException if error occurs
     */
    public void srvEndOfWork(final VIFContext ctx, final EstablishmentKey estKey, final List<Chrono> chronoList,
            final Date productionDate, final String productionLine, final Date eventDateTime)
                    throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - srvEndOfWork(ctx=" + ctx + ", estKey=" + estKey + ", chronoList=" + chronoList
                    + ", productionDate=" + productionDate + ", productionLine=" + productionLine + ", eventDateTime="
                    + eventDateTime + ")");
        }
        String resourceCode = productionLine;
        if (resourceCode == null) {
            try {
                // Gets the production line of the first MO of the list (NOT ALL OF THEM FOR NOW).
                resourceCode = getProductionLine(ctx, estKey, chronoList);
            } catch (ServerBusinessException e) {
                LOGGER.error("", e);
                throw new ServerBusinessException(Jtech.T16809, new VarParamTranslation(chronoList));
            }
        }
        if (resourceCode == null) {
            throw new ServerBusinessException(Generic.T19885, new VarParamTranslation(I18nServerManager.translate(ctx
                    .getIdCtx().getLocale(), false, ProductionMo.T32440)), new VarParamTranslation(null));
        } else {
            LaborTimeKey ltk = new LaborTimeKey();
            ltk.setEstKey(estKey);
            ltk.setProductionDate(productionDate);
            ltk.setResourceCode(resourceCode);
            FLaborTimeBBean lastLT = getLastLaborTimeFromKey(ctx, ltk);
            // the last period must be open (the end date-hour must be empty)
            if (lastLT != null && lastLT.getEndDateHour() == null) {
                // converts and sets the current date-hour to String
                lastLT.setEndDateHour(eventDateTime);

                LaborTime lt = convertFLaborTimeBBeanToLaborTime(ctx.getIdCtx(), lastLT);
                try {
                    // updates
                    updateLaborTime(ctx, lt);
                } catch (ServerBusinessException e) {
                    LOGGER.error("", e);
                    throw new ServerBusinessException(Jtech.T16809, new VarParamTranslation(ltk));
                }
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - srvEndOfWork(ctx=" + ctx + ", estKey=" + estKey + ", chronoList=" + chronoList
                    + ", productionDate=" + productionDate + ", productionLine=" + productionLine + ", eventDateTime="
                    + eventDateTime + ")");
        }
    }

    @Override
    public void srvMOBegins(final VIFContext ctx, final EstablishmentKey estKey, final Chrono chrono,
            final Date productionDate, final String productionLine, final Date eventDateTime) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - srvMOBegins(ctx=" + ctx + ", estKey=" + estKey + ", chrono=" + chrono
                    + ", productionDate=" + productionDate + ", productionLine=" + productionLine + ", eventDateTime="
                    + eventDateTime + ")");
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - srvMOBegins(ctx=" + ctx + ", estKey=" + estKey + ", chrono=" + chrono
                    + ", productionDate=" + productionDate + ", productionLine=" + productionLine + ", eventDateTime="
                    + eventDateTime + ")=");
        }
    }

    @Override
    public void srvMOCloses(final VIFContext ctx, final EstablishmentKey estKey, final Chrono chrono,
            final Date productionDate, final String productionLine, final Date eventDateTime) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - srvMOCloses(ctx=" + ctx + ", estKey=" + estKey + ", chrono=" + chrono
                    + ", productionDate=" + productionDate + ", productionLine=" + productionLine + ", eventDateTime="
                    + eventDateTime + ")");
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - srvMOCloses(ctx=" + ctx + ", estKey=" + estKey + ", chrono=" + chrono
                    + ", productionDate=" + productionDate + ", productionLine=" + productionLine + ", eventDateTime="
                    + eventDateTime + ")=");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public LaborTime updateLaborTime(final VIFContext ctx, final LaborTime laborTime) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateLaborTime(ctx=" + ctx + ", laborTime=" + laborTime + ")");
        }
        LaborTime laborTimeConverted = null;
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateLaborTime(" + laborTime + ")");
        }
        LaborTimeKey laborTimeKey = new LaborTimeKey(laborTime);
        if (!isValid(laborTimeKey)) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborTime.toString()));
        }
        try {
            Ktpsx ktpsx = KtpsxDAOFactory.getDAO().getByPKForUpdate(ctx.getDbCtx(), laborTimeKey.getEstKey().getCsoc(),
                    laborTimeKey.getEstKey().getCetab(), laborTimeKey.getProductionDate(),
                    laborTimeKey.getResourceCode(), laborTimeKey.getNi1(), laborTime.getAdmstamp());
            // update
            KtpsxDAOFactory.getDAO().update(ctx.getDbCtx(), convertLaborTimeToKtpsx(laborTime, ktpsx));
            laborTimeConverted = convertKtpsxToLaborTime(KtpsxDAOFactory.getDAO().getByPK(ctx.getDbCtx(),
                    laborTimeKey.getEstKey().getCsoc(), laborTimeKey.getEstKey().getCetab(),
                    laborTimeKey.getProductionDate(), laborTimeKey.getResourceCode(), laborTimeKey.getNi1()));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateLaborTime(" + laborTime + ") : " + laborTimeConverted);
        }
        return laborTimeConverted;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int updateLaborTimeNi1(final VIFContext ctx, final LaborTime laborTime, final int newNi1)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateLaborTimeNi1(" + laborTime + ")");
        }
        int nb = 0;
        if (!isValid(laborTime.getLaborTimeKey())) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(laborTime.toString()));
        }
        try {
            // no update because ni1 is part of the primary key : insert new one and delete old one
            LaborTime laborTimeIns = new LaborTime();
            laborTimeIns.setLaborTimeKey(laborTime.getLaborTimeKey());
            laborTimeIns.setBegDateHour(laborTime.getBegDateHour());
            laborTimeIns.setEndDateHour(laborTime.getEndDateHour());
            LaborTime laborTimeDel = new LaborTime();
            LaborTimeKey ltKey = new LaborTimeKey();
            ltKey.setEstKey(laborTime.getLaborTimeKey().getEstKey());
            ltKey.setProductionDate(laborTime.getLaborTimeKey().getProductionDate());
            ltKey.setResourceCode(laborTime.getLaborTimeKey().getResourceCode());
            ltKey.setNi1(laborTime.getLaborTimeKey().getNi1());
            laborTimeDel.setLaborTimeKey(ltKey);
            laborTimeDel.setAdmstamp(laborTime.getAdmstamp());
            laborTimeIns.getLaborTimeKey().setNi1(newNi1);
            // insert new one
            KtpsxDAOFactory.getDAO().insert(ctx.getDbCtx(), convertLaborTimeToKtpsx(laborTimeIns, new Ktpsx()));
            Ktpsx ktpsx = KtpsxDAOFactory.getDAO().getByPKForUpdate(ctx.getDbCtx(),
                    laborTimeDel.getLaborTimeKey().getEstKey().getCsoc(),
                    laborTimeDel.getLaborTimeKey().getEstKey().getCetab(),
                    laborTimeDel.getLaborTimeKey().getProductionDate(),
                    laborTimeDel.getLaborTimeKey().getResourceCode(), laborTimeDel.getLaborTimeKey().getNi1(),
                    laborTimeDel.getAdmstamp());
            // delete old one
            KtpsxDAOFactory.getDAO().delete(ctx.getDbCtx(), convertLaborTimeToKtpsx(laborTimeDel, ktpsx));
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateLaborTimeNi1(" + laborTime + ")");
        }
        return nb;
    }

    /**
     * Convert a FLaborTimeBBean to a LaborTime bean.
     * 
     * @param idCtx : the context.
     * @param fltbb : the FLaborTimeBBean to convert.
     * @return LaborTime.
     */
    private LaborTime convertFLaborTimeBBeanToLaborTime(final IdContext idCtx, final FLaborTimeBBean fltbb) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertFLaborTimeBBeanToLaborTime = " + fltbb);
        }
        LaborTime lt = new LaborTime();
        lt.setLaborTimeKey(fltbb.getLaborTimeKey());

        lt.setBegDateHour(fltbb.getBegDateHour());
        lt.setEndDateHour(fltbb.getEndDateHour());

        lt.setAdmstamp(fltbb.getAdmstamp());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertFLaborTimeBBeanToLaborTime(idCtx=" + idCtx + ", FLaborTimeBBean=" + fltbb + ")="
                    + lt);
        }
        return lt;
    }

    /**
     * Convert a list of Ktpsx plc into a list of LaborTime bean.
     * 
     * @param ktpsxList list of plc to be converted
     * @return the corresponding list of LaborTime
     */
    private List<LaborTime> convertKtpsxListToLaborTimeList(final List<Ktpsx> ktpsxList) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertKtpsxListToLaborTimeList");
        }
        List<LaborTime> laborTimeList = new ArrayList<LaborTime>(ktpsxList.size());
        for (Iterator<Ktpsx> iterator = ktpsxList.iterator(); iterator.hasNext();) {
            Ktpsx ktpsx = iterator.next();
            laborTimeList.add(convertKtpsxToLaborTime(ktpsx));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertKtpsxListToLaborTimeList(ktpsxList=" + ktpsxList + ")=" + laborTimeList);
        }
        return laborTimeList;
    }

    /**
     * Convert the plc Ktpsx bean into the LaborTime bean.
     * 
     * @param ktpsx plc to be converted
     * @return the LaborTime bean that corresponds to the Ktpsx
     */
    private LaborTime convertKtpsxToLaborTime(final Ktpsx ktpsx) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertKtpsxToLaborTime");
        }
        LaborTime laborTime = null;
        if (ktpsx != null) {
            laborTime = new LaborTime();
            LaborTimeKey laborTimeKey = new LaborTimeKey(ktpsx.getCsoc(), ktpsx.getCetab(), ktpsx.getDatprod(),
                    ktpsx.getCres(), ktpsx.getNi1());
            laborTime.setLaborTimeKey(laborTimeKey);
            laborTime.setBegDateHour(ktpsx.getDatheurdebr());
            laborTime.setEndDateHour(ktpsx.getDatheurfinr());
            laborTime.setModificationDate(ktpsx.getDatmod());
            laborTime.setModificationUser(ktpsx.getCuser());
            laborTime.setAdmstamp(ktpsx.getAdmstamp());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertKtpsxToLaborTime(ktpsx=" + ktpsx + ")=" + laborTime);
        }
        return laborTime;
    }

    /**
     * Convert a labor time bean to a FLaborTimeBBean.
     * 
     * @param idCtx : the context.
     * @param laborTime : labor time bean to convert.
     * @return FLaborTimeBBean.
     */
    private FLaborTimeBBean convertLaborTimeToFLaborTimeBBean(final IdContext idCtx, final LaborTime laborTime) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("convertLaborTimeToFLaborTimeBBean = " + laborTime);
        }
        FLaborTimeBBean bBean = new FLaborTimeBBean();
        bBean.setLaborTimeKey(laborTime.getLaborTimeKey());
        bBean.setBegDateHour(laborTime.getBegDateHour());
        if (laborTime.getEndDateHour() != null) {
            bBean.setEndDateHour(laborTime.getEndDateHour());
        }
        bBean.setAdmstamp(laborTime.getAdmstamp());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertLaborTimeToFLaborTimeBBean(idCtx=" + idCtx + ", laborTime=" + laborTime + ")="
                    + bBean);
        }
        return bBean;
    }

    /**
     * Convert a LaborTime bean to a Ktpsx PLC.
     * 
     * @param laborTime the LaborTime
     * @param ktpsx Ktpsx PLC
     * @return Ktpsx PLC
     */
    private Ktpsx convertLaborTimeToKtpsx(final LaborTime laborTime, final Ktpsx ktpsx) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertLaborTimeToKtpsx");
        }
        Ktpsx ktpsxConverted = null;
        if (ktpsx == null) {
            ktpsxConverted = new Ktpsx();
        } else {
            ktpsxConverted = ktpsx;
        }
        ktpsxConverted.setCsoc(laborTime.getLaborTimeKey().getEstKey().getCsoc());
        ktpsxConverted.setCetab(laborTime.getLaborTimeKey().getEstKey().getCetab());
        ktpsxConverted.setDatprod(laborTime.getLaborTimeKey().getProductionDate());
        ktpsxConverted.setCres(laborTime.getLaborTimeKey().getResourceCode());
        ktpsxConverted.setNi1(laborTime.getLaborTimeKey().getNi1());
        ktpsxConverted.setDatheurdebr(laborTime.getBegDateHour());
        ktpsxConverted.setDatheurfinr(laborTime.getEndDateHour());
        ktpsxConverted.setDatmod(laborTime.getModificationDate());
        ktpsxConverted.setCuser(laborTime.getModificationUser());
        ktpsxConverted.setAdmstamp(laborTime.getAdmstamp());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertLaborTimeToKtpsx(laborTime=" + laborTime + ", ktpsx=" + ktpsx + ")="
                    + ktpsxConverted);
        }
        return ktpsxConverted;
    }

    /**
     * Get the last labor time from a labor time key.
     * 
     * @param ctx the context
     * @param ltk the labor time key
     * @return the bean
     */
    private FLaborTimeBBean getLastLaborTimeFromKey(final VIFContext ctx, final LaborTimeKey ltk) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getLastLaborTimeFromKey(ctx=" + ctx + ", ltk=" + ltk + ")");
        }
        FLaborTimeBBean lastLaborTime = null;
        FLaborTimeBBean ltbb = new FLaborTimeBBean();
        ltk.setNi1(0);
        // in order to get all the periods
        ltbb.setLaborTimeKey(ltk);
        try {
            List<LaborStaff> laborStaffList = new ArrayList<LaborStaff>();
            try {
                laborStaffList = getLaborstaffSBS().listLaborStaffWithLaborTimeBean(ctx, ltbb);
            } catch (ServerBusinessException e) {
                LOGGER.error("", e);
                throw new BusinessException(e);
            }
            int lastNi1 = 0;
            for (LaborStaff ls : laborStaffList) {
                int ni1 = ls.getLaborStaffKey().getLaborTimeKey().getNi1();
                if (ni1 > lastNi1) {
                    lastNi1 = ni1;
                }
            }
            if (lastNi1 > 0) {
                ltbb.getLaborTimeKey().setNi1(lastNi1);
                // re-uses this bean with the last ni1
                try {
                    LaborTime laborTime = getLaborTime(ctx, ltbb.getLaborTimeKey());
                    if (laborTime == null) {
                        throw new BusinessException(Generic.T1401, new VarParamTranslation(ltbb.getLaborTimeKey()));
                    } else {
                        lastLaborTime = convertLaborTimeToFLaborTimeBBean(ctx.getIdCtx(), laborTime);
                    }
                } catch (ServerBusinessException e) {
                    if (LOGGER.isEnabledFor(Level.ERROR)) {
                        LOGGER.error("Unable to get the  of the FLaborTimeVBean corresponding to the labor time key "
                                + ltbb.getLaborTimeKey(), e);
                    }
                    throw new BusinessException(e);
                }
            }
        } catch (BusinessException e) {
            LOGGER.error("", e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getLastLaborTimeFromKey(ctx=" + ctx + ", ltk=" + ltk + ")=" + lastLaborTime);
        }
        return lastLaborTime;
    }

    /**
     * Is a Labor Time key valid ?
     * 
     * @param laborTimeKey the labor time key
     * @return true if valid, false otherwise
     */
    private boolean isValid(final LaborTimeKey laborTimeKey) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isValid(laborTimeKey=" + laborTimeKey + ")");
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isValid(laborTimeKey=" + laborTimeKey + ")");
        }
        return (laborTimeKey != null && laborTimeKey.getEstKey().getCsoc() != null
                && laborTimeKey.getEstKey().getCetab() != null && laborTimeKey.getProductionDate() != null
                && laborTimeKey.getResourceCode() != null && laborTimeKey.getNi1() != 0);
    }
}

/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MoProfileSBSImpl.java,v $
 * Created on 18 Mar 2016 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.moprofile;


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.admin.config.business.services.libraries.parameter.ParameterSBS;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemKey;
import fr.vif.vif5_7.gen.item.business.beans.common.item.manufacturingdata.ManufacturingData;
import fr.vif.vif5_7.gen.item.business.beans.common.item.manufacturingdata.ManufacturingDataKey;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.item.business.services.libraries.item.ItemSBS;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputItemParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.input.InputParametersEnums.InputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputItemParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputBatchType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputContainerCapacityClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputContainerClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputUpperContainerCapacityClosingType;
import fr.vif.vif5_7.production.mo.constants.ProductionMOParamEnum;
import fr.vif.vif5_7.production.mo.dao.xpfent.Xpfent;
import fr.vif.vif5_7.production.mo.dao.xpfent.XpfentDAOFactory;
import fr.vif.vif5_7.production.mo.dao.xpfsor.Xpfsor;
import fr.vif.vif5_7.production.mo.dao.xpfsor.XpfsorDAOFactory;


/**
 * SBS implementation for mo input and output profile.
 *
 * @author cj
 */
@Component
public class MoProfileSBSImpl implements MoProfileSBS {
    /** LOGGER. */
    private static final Logger LOGGER                    = Logger.getLogger(MoProfileSBSImpl.class);
    private static final int    OUTPUT_NSC_CLOSING_PARAM  = 36;
    private static final int    OUTPUT_NSCP_CLOSING_PARAM = 46;

    @Autowired
    private ItemSBS             itemSBS;

    @Autowired
    private ParameterSBS        parameterSBS;

    @Override
    public InputItemParameters getInputProfile(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstation) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getInputProfile(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", workstation="
                    + workstation + ", itemId=" + itemId + ")");
        }

        InputItemParameters inputItemParameters = new InputItemParameters();

        String profileId = getProfileId(ctx, establishmentKey, ActivityItemType.INPUT, itemId, workstation);

        if (StringUtils.isNotEmpty(profileId)) {
            try {

                String company = establishmentKey.getCsoc();
                String establishment = establishmentKey.getCetab();

                if (profileId.length() > 1 && "$".equals(profileId.substring(0, 1))) {
                    company = "$$";
                    establishment = "$$";
                }

                Xpfent plc = XpfentDAOFactory.getDAO().getByPK(ctx.getDbCtx(), company, establishment, profileId);
                if (plc != null) {
                    inputItemParameters.setProfileId(plc.getCpfent());
                    inputItemParameters.setProfileLabel(plc.getLpfent());
                    inputItemParameters.setForcedFirstQty(plc.getEqte1());
                    inputItemParameters.setForcedFirstUnit(plc.getEcu1());
                    inputItemParameters.setForcedSecondUnit(plc.getEcu2());

                    if (plc.getTnscsa()) {
                        inputItemParameters.setEntityType(InputEntityType.CONTAINER);
                    } else if (plc.getTartlosa()) {
                        inputItemParameters.setEntityType(InputEntityType.BATCH_ITEM);
                    } else {
                        throw new ServerBusinessException(ProductionMo.T39489, new VarParamTranslation(profileId));
                    }
                    inputItemParameters.setInterpretQty(plc.getTicbqug() || plc.getTicbqus() || plc.getTicbqut());
                } else {
                    throw new ServerBusinessException(ProductionMo.T39488, new VarParamTranslation(profileId));
                }
            } catch (DAOException e) {
                LOGGER.error(e);
                throw new ServerBusinessException(e);
            }
        } else {
            throw new ServerBusinessException(ProductionMo.T16729);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getInputProfile(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", workstation="
                    + workstation + ", itemId=" + itemId + ")=" + inputItemParameters);
        }
        return inputItemParameters;
    }

    @Override
    public String getInputProfileId(final VIFContext ctx, final EstablishmentKey establishmentKey, final String itemId,
            final String workstation) throws ServerBusinessException {
        return getProfileId(ctx, establishmentKey, ActivityItemType.INPUT, itemId, workstation);
    }

    /**
     * Gets the itemSBS.
     *
     * @return the itemSBS.
     */
    public ItemSBS getItemSBS() {
        return itemSBS;
    }

    @Override
    public OutputItemParameters getOutputProfile(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstation) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getOutputProfile(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", workstation=" + workstation + ", itemId=" + itemId + ")");
        }

        OutputItemParameters outputItemParameters = new OutputItemParameters();

        String profileId = getProfileId(ctx, establishmentKey, ActivityItemType.OUTPUT, itemId, workstation);

        if (StringUtils.isNotEmpty(profileId)) {
            try {

                String company = establishmentKey.getCsoc();
                String establishment = establishmentKey.getCetab();

                if (profileId.length() > 1 && "$".equals(profileId.substring(0, 1))) {
                    company = "$$";
                    establishment = "$$";
                }

                Xpfsor plc = XpfsorDAOFactory.getDAO().getByPK(ctx.getDbCtx(), company, establishment, profileId);
                if (plc != null) {
                    outputItemParameters.setProfileId(plc.getCpfsor());
                    outputItemParameters.setProfileLabel(plc.getLpfsor());

                    // Units
                    outputItemParameters.setForcedFirstUnit(plc.getScu1());
                    outputItemParameters.setForcedSecondUnit(plc.getScu2());

                    // Quantities
                    if (plc.getScu1() != null && !plc.getScu1().isEmpty()) {
                        outputItemParameters.setForcedFirstQty(plc.getSqte1());
                        if (plc.getScu2() != null && !plc.getScu2().isEmpty()) {
                            // Second quantity obtained from conversion
                            ItemKey itemKey = new ItemKey(establishmentKey.getCsoc(), itemId);
                            QtyUnit firstQtyUnit = new QtyUnit(plc.getSqte1(), plc.getScu1());
                            QtyUnit secondQtyUnit = getItemSBS().convertQty(ctx, itemKey, firstQtyUnit, plc.getScu2(),
                                    false);
                            if (secondQtyUnit != null) {
                                outputItemParameters.setForcedSecondQty(secondQtyUnit.getQty());
                            }
                        }
                    }

                    outputItemParameters.setBatchType(OutputBatchType.getOutputBatchType(plc.getSnumlot()));

                    // Entity type
                    if (plc.getTnscco() && plc.getTnscpco()) {
                        outputItemParameters.setEntityType(OutputEntityType.CONTAINER_UPPER_CONTAINER);
                    } else if (plc.getTnscco() && !plc.getTnscpco()) {
                        outputItemParameters.setEntityType(OutputEntityType.CONTAINER);
                    } else if (!plc.getTnscco() && !plc.getTnscpco() && plc.getTartloco()) {
                        outputItemParameters.setEntityType(OutputEntityType.BATCH_ITEM);
                    } else if (plc.getTnspco()) {
                        outputItemParameters.setEntityType(OutputEntityType.EU);
                    } else {
                        throw new ServerBusinessException(ProductionMo.T39490, new VarParamTranslation(profileId));
                    }

                    // Container
                    if (plc.getTnscco()) {
                        if ("NVS".equals(plc.getGnscco())) {
                            outputItemParameters.setContainerClosingType(OutputContainerClosingType.AUTOMATIC);
                        } else if ("USC".equals(plc.getGnscco())) {
                            // CSORFAB - 36 is a string parameter (with int values inside)
                            int closeParam = Integer.valueOf(
                                    getParameterSBS().readStringParameter(ctx, establishmentKey.getCsoc(),
                                            establishmentKey.getCetab(),
                                            ProductionMOParamEnum.FABRICATION_INPUT.getCode(),
                                            OUTPUT_NSC_CLOSING_PARAM, workstation)).intValue();
                            if (closeParam == 0) {
                                outputItemParameters.setContainerClosingType(OutputContainerClosingType.MANUAL);
                            } else {
                                outputItemParameters.setContainerClosingType(OutputContainerClosingType.CAPACITY);
                                if (closeParam == 1) {
                                    outputItemParameters
                                            .setContainerCapacityClosingType(OutputContainerCapacityClosingType.AUTOMATIC_IF_IN_RANGE);
                                } else if (closeParam == 2) {
                                    outputItemParameters
                                            .setContainerCapacityClosingType(OutputContainerCapacityClosingType.AUTOMATIC_IF_MORE_CAPACITY);
                                } else {
                                    outputItemParameters
                                            .setContainerCapacityClosingType(OutputContainerCapacityClosingType
                                            .getOutputContainerCapacityClosingType(closeParam));
                                }
                            }
                        }

                        outputItemParameters.setContainerPackagingId(plc.getCcond());
                        if (StringUtils.isEmpty(plc.getCcond())) {
                            throw new ServerBusinessException(ProductionMo.T39492, new VarParamTranslation(profileId),
                                    ProductionMo.T39175);
                        }
                    }

                    // upper container
                    if (plc.getTnscpco()) {
                        if ("NVS".equals(plc.getGnscpco())) {
                            outputItemParameters.setUpperContainerClosingType(OutputContainerClosingType.AUTOMATIC);
                        } else if ("USC".equals(plc.getGnscpco())) {
                            int closeParam = (int) getParameterSBS().readDoubleParameter(ctx,
                                    establishmentKey.getCsoc(), establishmentKey.getCetab(),
                                    ProductionMOParamEnum.FABRICATION_INPUT.getCode(), OUTPUT_NSCP_CLOSING_PARAM,
                                    workstation);
                            if (closeParam == 0) {
                                outputItemParameters.setUpperContainerClosingType(OutputContainerClosingType.MANUAL);
                            } else {
                                outputItemParameters.setUpperContainerClosingType(OutputContainerClosingType.CAPACITY);
                                if (closeParam == 1) {
                                    outputItemParameters
                                            .setUpperContainerCapacityClosingType(OutputUpperContainerCapacityClosingType.AUTOMATIC_IF_MORE_CAPACITY);
                                } else {
                                    outputItemParameters
                                            .setUpperContainerCapacityClosingType(OutputUpperContainerCapacityClosingType
                                                    .getOutputUpperContainerCapacityClosingType(closeParam));
                                }
                            }
                        }

                        outputItemParameters.setUpperContainerPackagingId(plc.getCcondp());
                        if (StringUtils.isEmpty(plc.getCcondp())) {
                            throw new ServerBusinessException(ProductionMo.T39492, new VarParamTranslation(profileId),
                                    new VarParamTranslation(ProductionMo.T39176));
                        }
                    }

                    if (plc.getTnscco() && plc.getTnscpco() && "NVS".equals(plc.getGnscco())
                            && "NVS".equals(plc.getGnscpco())) {
                        throw new ServerBusinessException(ProductionMo.T39492, new VarParamTranslation(profileId),
                                ProductionMo.T39495);
                    }
                    if ((plc.getTnscco() && "PLS".equals(plc.getGnscco()))
                            || (plc.getTnscpco() && "PLS".equals(plc.getGnscpco()))) {
                        throw new ServerBusinessException(ProductionMo.T39492, new VarParamTranslation(profileId),
                                ProductionMo.T39494);
                    }
                } else {
                    throw new ServerBusinessException(ProductionMo.T39491, new VarParamTranslation(profileId));
                }
            } catch (DAOException e) {
                LOGGER.error(e);
                throw new ServerBusinessException(e);
            }
        } else {
            throw new ServerBusinessException(ProductionMo.T39493);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getOutputProfile(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", workstation=" + workstation + ", itemId=" + itemId + ")=" + outputItemParameters);
        }
        return outputItemParameters;
    }

    @Override
    public String getOutputProfileId(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstation) throws ServerBusinessException {
        return getProfileId(ctx, establishmentKey, ActivityItemType.OUTPUT, itemId, workstation);
    }

    /**
     * Gets the parameterSBS.
     *
     * @return the parameterSBS.
     */
    public ParameterSBS getParameterSBS() {
        return parameterSBS;
    }

    /**
     * Sets the itemSBS.
     *
     * @param itemSBS itemSBS.
     */
    public void setItemSBS(final ItemSBS itemSBS) {
        this.itemSBS = itemSBS;
    }

    /**
     * Sets the parameterSBS.
     *
     * @param parameterSBS parameterSBS.
     */
    public void setParameterSBS(final ParameterSBS parameterSBS) {
        this.parameterSBS = parameterSBS;
    }

    /**
     * Gets the profile id. Search the item profile first. If any, search the workstation profile.
     *
     * @param ctx VIF Context
     * @param establishmentKey the establishment key
     * @param itemType the activity item type
     * @param workstation the workstation
     * @param itemId the item id
     * @return the profile id
     * @throws ServerBusinessException if error occurs
     */
    private String getProfileId(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final ActivityItemType itemType, final String itemId, final String workstation)
                    throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getProfileId(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", itemType="
                    + itemType + ", workstation=" + workstation + ", itemId=" + itemId + ")");
        }

        String profileId = null;
        ManufacturingData manufacturingData = getItemSBS().getManufacturingData(ctx,
                new ManufacturingDataKey(establishmentKey, itemId));

        // Search the profile on the item informations
        if (manufacturingData != null) {
            if (ActivityItemType.INPUT.equals(itemType)) {
                profileId = manufacturingData.getCodeWorkshopInputProfile();
            } else if (ActivityItemType.OUTPUT.equals(itemType)) {
                profileId = manufacturingData.getCodeWorkshopOutputProfile();
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("P - getProfileId(). Item profile = " + profileId);
        }
        // else search the workstation profile
        if (StringUtils.isEmpty(profileId)) {
            if (ActivityItemType.INPUT.equals(itemType)) {
                profileId = getParameterSBS().readStringParameter(ctx, establishmentKey.getCsoc(),
                        establishmentKey.getCetab(), ProductionMOParamEnum.FABRICATION_INPUT.getCode(), 1, workstation);
            } else if (ActivityItemType.OUTPUT.equals(itemType)) {
                profileId = getParameterSBS()
                        .readStringParameter(ctx, establishmentKey.getCsoc(), establishmentKey.getCetab(),
                                ProductionMOParamEnum.FABRICATION_OUTPUT.getCode(), 1, workstation);
            }
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getProfileId(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", itemType="
                    + itemType + ", workstation=" + workstation + ", itemId=" + itemId + ")=" + profileId);
        }
        return profileId;
    }
}

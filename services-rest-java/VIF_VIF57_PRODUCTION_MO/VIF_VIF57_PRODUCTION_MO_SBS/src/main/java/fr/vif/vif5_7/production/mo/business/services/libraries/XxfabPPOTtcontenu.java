/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPOTtcontenu.java,v $
 * Created on 24 mars 2016 by presta15
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:tempxxfabpxy.p.xml.
 * 
 * @author presta15
 */
public class XxfabPPOTtcontenu extends AbstractTempTable implements Serializable {


    private String cart = "";

    private String cetab = "";

    private int chronof = 0;

    private String csoc = "";

    private String cug = "";

    private String cus = "";

    private String cut = "";

    private String lart = "";

    private String lot = "";

    private int ni1 = 0;

    private int ni2 = 0;

    private int ni3 = 0;

    private int ni4 = 0;

    private double pdsbrut = 0;

    private double pdsnet = 0;

    private String prechrof = "";

    private double quantu = 0;

    private double qug = 0;

    private double qus = 0;

    private double qut = 0;


    /**
     * Default Constructor.
     *
     */
    public XxfabPPOTtcontenu() {
        super();
    }

    /**
     * Gets the cart.
     *
     * @category getter
     * @return the cart.
     */
    public final String getCart() {
        return cart;
    }

    /**
     * Sets the cart.
     *
     * @category setter
     * @param cart cart.
     */
    public final void setCart(final String cart) {
        this.cart = cart;
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the chronof.
     *
     * @category getter
     * @return the chronof.
     */
    public final int getChronof() {
        return chronof;
    }

    /**
     * Sets the chronof.
     *
     * @category setter
     * @param chronof chronof.
     */
    public final void setChronof(final int chronof) {
        this.chronof = chronof;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the cug.
     *
     * @category getter
     * @return the cug.
     */
    public final String getCug() {
        return cug;
    }

    /**
     * Sets the cug.
     *
     * @category setter
     * @param cug cug.
     */
    public final void setCug(final String cug) {
        this.cug = cug;
    }

    /**
     * Gets the cus.
     *
     * @category getter
     * @return the cus.
     */
    public final String getCus() {
        return cus;
    }

    /**
     * Sets the cus.
     *
     * @category setter
     * @param cus cus.
     */
    public final void setCus(final String cus) {
        this.cus = cus;
    }

    /**
     * Gets the cut.
     *
     * @category getter
     * @return the cut.
     */
    public final String getCut() {
        return cut;
    }

    /**
     * Sets the cut.
     *
     * @category setter
     * @param cut cut.
     */
    public final void setCut(final String cut) {
        this.cut = cut;
    }

    /**
     * Gets the lart.
     *
     * @category getter
     * @return the lart.
     */
    public final String getLart() {
        return lart;
    }

    /**
     * Sets the lart.
     *
     * @category setter
     * @param lart lart.
     */
    public final void setLart(final String lart) {
        this.lart = lart;
    }

    /**
     * Gets the lot.
     *
     * @category getter
     * @return the lot.
     */
    public final String getLot() {
        return lot;
    }

    /**
     * Sets the lot.
     *
     * @category setter
     * @param lot lot.
     */
    public final void setLot(final String lot) {
        this.lot = lot;
    }

    /**
     * Gets the ni1.
     *
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Sets the ni1.
     *
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Gets the ni2.
     *
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Sets the ni2.
     *
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Gets the ni3.
     *
     * @category getter
     * @return the ni3.
     */
    public final int getNi3() {
        return ni3;
    }

    /**
     * Sets the ni3.
     *
     * @category setter
     * @param ni3 ni3.
     */
    public final void setNi3(final int ni3) {
        this.ni3 = ni3;
    }

    /**
     * Gets the ni4.
     *
     * @category getter
     * @return the ni4.
     */
    public final int getNi4() {
        return ni4;
    }

    /**
     * Sets the ni4.
     *
     * @category setter
     * @param ni4 ni4.
     */
    public final void setNi4(final int ni4) {
        this.ni4 = ni4;
    }

    /**
     * Gets the pdsbrut.
     *
     * @category getter
     * @return the pdsbrut.
     */
    public final double getPdsbrut() {
        return pdsbrut;
    }

    /**
     * Sets the pdsbrut.
     *
     * @category setter
     * @param pdsbrut pdsbrut.
     */
    public final void setPdsbrut(final double pdsbrut) {
        this.pdsbrut = pdsbrut;
    }

    /**
     * Gets the pdsnet.
     *
     * @category getter
     * @return the pdsnet.
     */
    public final double getPdsnet() {
        return pdsnet;
    }

    /**
     * Sets the pdsnet.
     *
     * @category setter
     * @param pdsnet pdsnet.
     */
    public final void setPdsnet(final double pdsnet) {
        this.pdsnet = pdsnet;
    }

    /**
     * Gets the prechrof.
     *
     * @category getter
     * @return the prechrof.
     */
    public final String getPrechrof() {
        return prechrof;
    }

    /**
     * Sets the prechrof.
     *
     * @category setter
     * @param prechrof prechrof.
     */
    public final void setPrechrof(final String prechrof) {
        this.prechrof = prechrof;
    }

    /**
     * Gets the quantu.
     *
     * @category getter
     * @return the quantu.
     */
    public final double getQuantu() {
        return quantu;
    }

    /**
     * Sets the quantu.
     *
     * @category setter
     * @param quantu quantu.
     */
    public final void setQuantu(final double quantu) {
        this.quantu = quantu;
    }

    /**
     * Gets the qug.
     *
     * @category getter
     * @return the qug.
     */
    public final double getQug() {
        return qug;
    }

    /**
     * Sets the qug.
     *
     * @category setter
     * @param qug qug.
     */
    public final void setQug(final double qug) {
        this.qug = qug;
    }

    /**
     * Gets the qus.
     *
     * @category getter
     * @return the qus.
     */
    public final double getQus() {
        return qus;
    }

    /**
     * Sets the qus.
     *
     * @category setter
     * @param qus qus.
     */
    public final void setQus(final double qus) {
        this.qus = qus;
    }

    /**
     * Gets the qut.
     *
     * @category getter
     * @return the qut.
     */
    public final double getQut() {
        return qut;
    }

    /**
     * Sets the qut.
     *
     * @category setter
     * @param qut qut.
     */
    public final void setQut(final double qut) {
        this.qut = qut;
    }

}

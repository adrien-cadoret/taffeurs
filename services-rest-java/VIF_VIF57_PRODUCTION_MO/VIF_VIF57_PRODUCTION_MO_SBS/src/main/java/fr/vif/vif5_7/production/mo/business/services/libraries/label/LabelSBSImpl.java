/*
 * Copyright (c) 2008 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: LabelSBSImpl.java,v $
 * Created on 18 nov. 08 by gv
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.label;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.SearchLabel;
import fr.vif.vif5_7.production.mo.business.beans.common.labels.LabelEvent;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBS;
import fr.vif.vif5_7.production.mo.business.services.libraries.operationitem.MOItemSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;


/**
 * class for labeling in production mo.
 * 
 * @author gv
 */
@Component
public class LabelSBSImpl implements LabelSBS {

    /** LOGGER. */
    private static final Logger LOGGER    = Logger.getLogger(LabelSBSImpl.class);

    @Autowired
    private MOItemSBS           moItemSBS = null;

    @Autowired
    @Qualifier("MOSBSImpl")
    private MOSBS               moSBS     = null;

    /**
     * defualt ctor.
     */
    public LabelSBSImpl() {
    }

    /**
     * Gets the moItemSBS.
     * 
     * @category getter
     * @return the moItemSBS.
     */
    public MOItemSBS getMoItemSBS() {
        return moItemSBS;
    }

    /**
     * Gets the moSBS.
     * 
     * @category getter
     * @return the moSBS.
     */
    public MOSBS getMoSBS() {
        return moSBS;
    }

    /**
     * {@inheritDoc}
     */
    public void runPrintLabel(final VIFContext ctx, final LabelEvent labelEvent, final SearchLabel searchLabel,
            final int copiesNumber) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - runPrintLabel(ctx=" + ctx + ", labelEvent=" + labelEvent + ", searchLabel=" + searchLabel
                    + ", copiesNumber=" + copiesNumber + ")");
        }
        if (LabelType.LABEL_ACT.equals(searchLabel.getLabelType())) {
            runPrintActLabel(ctx, labelEvent, searchLabel, copiesNumber);
        } else if (LabelType.LABEL_MVT.equals(searchLabel.getLabelType())) {
            runPrintMovementLabel(ctx, labelEvent, searchLabel, copiesNumber);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - runPrintLabel(ctx=" + ctx + ", labelEvent=" + labelEvent + ", searchLabel=" + searchLabel
                    + ", copiesNumber=" + copiesNumber + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public List<SearchLabel> searchPrintLabels(final VIFContext ctx, final LabelEvent labelEvent)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - searchPrintLabels(ctx=" + ctx + ", labelEvent=" + labelEvent + ")");
        }
        List<SearchLabel> lstResponse = new ArrayList<SearchLabel>();
        if (labelEvent.getEventType().equals(LabelingEventType.MO_CREATION)) {
            List<SearchLabel> listSearchLabel = searchPrintActLabel(ctx, labelEvent);
            for (SearchLabel searchLabel : listSearchLabel) {
                searchLabel.setLabelType(LabelType.LABEL_ACT);
                lstResponse.add(searchLabel);
            }
        } else if (labelEvent.getEventType().equals(LabelingEventType.MO_FIRST_DECLARATION)) {
            if ((labelEvent.getActivityItemType().equals(ActivityItemType.INPUT))) {
                List<SearchLabel> listSearchLabel = searchPrintActLabel(ctx, labelEvent);
                for (SearchLabel searchLabel : listSearchLabel) {
                    searchLabel.setLabelType(LabelType.LABEL_ACT);
                    lstResponse.add(searchLabel);
                }
            }
            SearchLabel searchLabel = searchPrintMovementLabel(ctx, labelEvent);
            searchLabel.setLabelType(LabelType.LABEL_MVT);
            lstResponse.add(searchLabel);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - searchPrintLabels(ctx=" + ctx + ", labelEvent=" + labelEvent + ")");
        }
        return lstResponse;
    }

    /**
     * Sets the moItemSBS.
     * 
     * @category setter
     * @param moItemSBS moItemSBS.
     */
    public void setMoItemSBS(final MOItemSBS moItemSBS) {
        this.moItemSBS = moItemSBS;
    }

    /**
     * Sets the moSBS.
     * 
     * @category setter
     * @param moSBS moSBS.
     */
    public void setMoSBS(final MOSBS moSBS) {
        this.moSBS = moSBS;
    }

    /**
     * run the print for Acts.
     * 
     * @param vifCtx the vif context
     * @param labelEvent the label event source
     * @param labelEventResponse the response to events
     * @param copiesNumber the nb of labels to print
     * @throws ServerBusinessException e
     */
    private void runPrintActLabel(final VIFContext vifCtx, final LabelEvent labelEvent,
            final SearchLabel labelEventResponse, final int copiesNumber) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - runPrintActLabel(vifCtx=" + vifCtx + ", labelEvent=" + labelEvent
                    + ", labelEventResponse=" + labelEventResponse + ", copiesNumber=" + copiesNumber + ")");
        }
        // Prints a movement label
        getMoItemSBS().runPrintActLabel(vifCtx, labelEvent.getMoKey(), labelEvent.getWorkstationId(),
                labelEvent.getActivityItemType(), labelEventResponse.getLabelId(),
                labelEventResponse.getPrintProgramId(), copiesNumber);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - runPrintActLabel(vifCtx=" + vifCtx + ", labelEvent=" + labelEvent
                    + ", labelEventResponse=" + labelEventResponse + ", copiesNumber=" + copiesNumber + ")");
        }
    }

    /**
     * run the print for Acts.
     * 
     * @param vifCtx the vif context
     * @param labelEvent the label event source
     * @param labelEventResponse the response to events
     * @param copiesNumber the nb of labels to print
     * @throws ServerBusinessException e
     */
    private void runPrintMovementLabel(final VIFContext vifCtx, final LabelEvent labelEvent,
            final SearchLabel labelEventResponse, final int copiesNumber) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - runPrintMovementLabel(vifCtx=" + vifCtx + ", labelEvent=" + labelEvent
                    + ", labelEventResponse=" + labelEventResponse + ", copiesNumber=" + copiesNumber + ")");
        }
        // Prints a movement label
        getMoItemSBS().runPrintMovementLabel(vifCtx, labelEvent.getRowIdKey(), labelEvent.getWorkstationId(),
                labelEvent.getActivityItemType(), labelEventResponse.getLabelId(),
                labelEventResponse.getPrintProgramId(), copiesNumber);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - runPrintMovementLabel(vifCtx=" + vifCtx + ", labelEvent=" + labelEvent
                    + ", labelEventResponse=" + labelEventResponse + ", copiesNumber=" + copiesNumber + ")");
        }
    }

    /**
     * 
     * search the label for act events.
     * 
     * @param ctx c
     * @param labelEvent a
     * @return list of search label
     * @throws ServerBusinessException e
     */
    private List<SearchLabel> searchPrintActLabel(final VIFContext ctx, final LabelEvent labelEvent)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - searchPrintActLabel(ctx=" + ctx + ", labelEvent=" + labelEvent + ")");
        }
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(labelEvent.getMoKey().getEstablishmentKey());
        moKey.setChrono(labelEvent.getMoKey().getChrono());
        List<SearchLabel> listSearchLabel = getMoItemSBS().searchPrintActLabel(ctx, moKey, labelEvent.getRowIdKey(),
                labelEvent.getWorkstationId(), labelEvent.getActivityItemType(), labelEvent.getEventType());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - searchPrintActLabel(ctx=" + ctx + ", labelEvent=" + labelEvent + ")=" + listSearchLabel);
        }
        return listSearchLabel;
    }

    /**
     * 
     * search the labels from movement events.
     * 
     * @param ctx c
     * @param labelEvent the event for labeling
     * @return s
     * @throws ServerBusinessException e
     */
    private SearchLabel searchPrintMovementLabel(final VIFContext ctx, final LabelEvent labelEvent)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - searchPrintMovementLabel(ctx=" + ctx + ", labelEvent=" + labelEvent + ")");
        }
        SearchLabel searchLabel = getMoItemSBS().searchPrintMovementLabel(ctx, labelEvent.getRowIdKey(),
                labelEvent.getWorkstationId(), labelEvent.getActivityItemType());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - searchPrintMovementLabel(ctx=" + ctx + ", labelEvent=" + labelEvent + ")=" + searchLabel);
        }
        return searchLabel;
    }
}

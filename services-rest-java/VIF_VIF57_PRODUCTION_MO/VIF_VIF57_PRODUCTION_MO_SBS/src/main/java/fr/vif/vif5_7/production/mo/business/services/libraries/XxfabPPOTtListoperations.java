/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPOTtListoperations.java,v $
 * Created on 10 mai 2016 by presta14
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:tempxxfabpxy.p.xml.
 * 
 * @author presta14
 */
public class XxfabPPOTtListoperations extends AbstractTempTable implements Serializable {


    private String cetab = "";

    private int chronof = 0;

    private String csoc = "";

    private int ni1 = 0;

    private int ni2 = 0;

    private int ni3 = 0;

    private int ni4 = 0;

    private String prechrof = "";

    private String typGest = "";


    /**
     * Default Constructor.
     *
     */
    public XxfabPPOTtListoperations() {
        super();
    }

    /**
     * Gets the cetab.
     *
     * @category getter
     * @return the cetab.
     */
    public final String getCetab() {
        return cetab;
    }

    /**
     * Sets the cetab.
     *
     * @category setter
     * @param cetab cetab.
     */
    public final void setCetab(final String cetab) {
        this.cetab = cetab;
    }

    /**
     * Gets the chronof.
     *
     * @category getter
     * @return the chronof.
     */
    public final int getChronof() {
        return chronof;
    }

    /**
     * Sets the chronof.
     *
     * @category setter
     * @param chronof chronof.
     */
    public final void setChronof(final int chronof) {
        this.chronof = chronof;
    }

    /**
     * Gets the csoc.
     *
     * @category getter
     * @return the csoc.
     */
    public final String getCsoc() {
        return csoc;
    }

    /**
     * Sets the csoc.
     *
     * @category setter
     * @param csoc csoc.
     */
    public final void setCsoc(final String csoc) {
        this.csoc = csoc;
    }

    /**
     * Gets the ni1.
     *
     * @category getter
     * @return the ni1.
     */
    public final int getNi1() {
        return ni1;
    }

    /**
     * Sets the ni1.
     *
     * @category setter
     * @param ni1 ni1.
     */
    public final void setNi1(final int ni1) {
        this.ni1 = ni1;
    }

    /**
     * Gets the ni2.
     *
     * @category getter
     * @return the ni2.
     */
    public final int getNi2() {
        return ni2;
    }

    /**
     * Sets the ni2.
     *
     * @category setter
     * @param ni2 ni2.
     */
    public final void setNi2(final int ni2) {
        this.ni2 = ni2;
    }

    /**
     * Gets the ni3.
     *
     * @category getter
     * @return the ni3.
     */
    public final int getNi3() {
        return ni3;
    }

    /**
     * Sets the ni3.
     *
     * @category setter
     * @param ni3 ni3.
     */
    public final void setNi3(final int ni3) {
        this.ni3 = ni3;
    }

    /**
     * Gets the ni4.
     *
     * @category getter
     * @return the ni4.
     */
    public final int getNi4() {
        return ni4;
    }

    /**
     * Sets the ni4.
     *
     * @category setter
     * @param ni4 ni4.
     */
    public final void setNi4(final int ni4) {
        this.ni4 = ni4;
    }

    /**
     * Gets the prechrof.
     *
     * @category getter
     * @return the prechrof.
     */
    public final String getPrechrof() {
        return prechrof;
    }

    /**
     * Sets the prechrof.
     *
     * @category setter
     * @param prechrof prechrof.
     */
    public final void setPrechrof(final String prechrof) {
        this.prechrof = prechrof;
    }

    /**
     * Gets the typGest.
     *
     * @category getter
     * @return the typGest.
     */
    public final String getTypGest() {
        return typGest;
    }

    /**
     * Sets the typGest.
     *
     * @category setter
     * @param typGest typGest.
     */
    public final void setTypGest(final String typGest) {
        this.typGest = typGest;
    }

}

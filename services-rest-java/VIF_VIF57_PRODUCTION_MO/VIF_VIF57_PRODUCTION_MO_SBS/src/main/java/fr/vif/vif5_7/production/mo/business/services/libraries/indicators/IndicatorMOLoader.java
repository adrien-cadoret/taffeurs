/*
 * Copyright (c) 2011 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: IndicatorMOLoader.java,v $
 * Created on 2 nov. 2011 by jd
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.indicators;


import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;
import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.util.cache.ICacheLoader;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOSelectionBean;
import fr.vif.vif5_7.production.mo.dao.kfabind.Kfabind;
import fr.vif.vif5_7.production.mo.dao.kfabind.KfabindDAOFactory;
import org.springframework.stereotype.Component;


/**
 * Cache loader of the productivities values.
 * 
 * @author ag
 */
@Component
public class IndicatorMOLoader implements ICacheLoader {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(IndicatorMOLoader.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public Object loadCacheObject(final VIFContext ctx, final Object cacheKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - loadCacheObject(ctx=" + ctx + ", cacheKey=" + cacheKey + ")");
        }
        List<IndicatorMO> ret = new ArrayList<IndicatorMO>();
        if (cacheKey instanceof IndicatorMOSelectionBean) {
            IndicatorMOSelectionBean selection = (IndicatorMOSelectionBean) cacheKey;
            List<Kfabind> plcs = new ArrayList<Kfabind>();
            try {
                plcs = KfabindDAOFactory.getDAO().listBySelection(ctx.getDbCtx(), selection);
                ret = IndicatorMOSBSImpl.convertPlcToIndicators(plcs);
            } catch (DAOException e) {
                throw new ServerBusinessException(e);
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - loadCacheObject(ctx=" + ctx + ", cacheKey=" + cacheKey + ")=" + ret);
        }
        return ret;
    }
}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: FLaborStaffValidatorImpl.java,v $
 * Created on 13 déc. 2013 by nle
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.labortime;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.TranslatedException;
import fr.vif.jtech.business.util.validator.AbstractVIFValidator;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.common.util.i18n.jtech.Jtech;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.common.util.i18n.gen.kernel.GenKernel;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborStaffKey;
import fr.vif.vif5_7.production.mo.business.beans.common.labortime.LaborTimeKey;
import fr.vif.vif5_7.production.mo.business.beans.features.flabortime.FLaborStaffBBean;
import fr.vif.vif5_7.production.mo.dao.ktpsxd.Ktpsxd;
import fr.vif.vif5_7.production.mo.dao.ktpsxd.KtpsxdDAOFactory;


/**
 * Labor Staff validator SBS.
 * 
 * @author nle
 */
@Component
public class FLaborStaffValidatorImpl extends AbstractVIFValidator<FLaborStaffBBean> implements FLaborStaffValidator {

    private static final Logger LOGGER = Logger.getLogger(LaborStaffSBSImpl.class);

    /**
     * Labor Staff's SBS.
     */
    @Autowired
    private LaborStaffSBS       laborStaffSBS;

    /**
     * Gets the LaborStaffSBS.
     * 
     * @return the LaborStaffSBS.
     */
    public LaborStaffSBS getLaborStaffSBS() {
        return laborStaffSBS;
    }

    /**
     * Sets the LaborStaffSBS.
     * 
     * @param iLaborStaffSBS LaborStaffSBS.
     */
    public void setLaborStaffSBS(final LaborStaffSBS iLaborStaffSBS) {
        this.laborStaffSBS = iLaborStaffSBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void validate(final VIFContext context, final boolean insertMode, final FLaborStaffBBean target,
            final Errors errors) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - validate( FLaborStaffVbean = " + target + " )");
        }

        // Error if record exists in insertMode.
        if (insertMode) {
            LaborStaffKey laborStaffKey = target.getLaborStaff().getLaborStaffKey();
            LaborTimeKey laborTimeKey = laborStaffKey.getLaborTimeKey();
            if (laborTimeKey.getEstKey().getCsoc() == null || laborTimeKey.getEstKey().getCsoc().isEmpty()) {
                rejectValue(context, errors, "laborStaff.laborStaffKey.laborTimeKey.estKey.csoc",
                        new TranslatedException(Generic.T12640, GenKernel.T27957));
            }
            if (laborTimeKey.getEstKey().getCetab() == null || laborTimeKey.getEstKey().getCetab().isEmpty()) {
                rejectValue(context, errors, "laborStaff.laborStaffKey.laborTimeKey.estKey.cetab",
                        new TranslatedException(Generic.T12640, GenKernel.T189));
            }
            if (laborTimeKey.getProductionDate() == null) {
                rejectValue(context, errors, "laborStaff.laborStaffKey.laborTimeKey.productionDate",
                        new TranslatedException(Generic.T12640, ProductionMo.T34694));
            }
            if (laborTimeKey.getResourceCode() == null || laborTimeKey.getResourceCode().isEmpty()) {
                rejectValue(context, errors, "laborStaff.laborStaffKey.laborTimeKey.resourceCode",
                        new TranslatedException(Generic.T12640, ProductionMo.T32440));
            }
            if (laborTimeKey.getNi1() == 0) {
                rejectValue(context, errors, "laborStaff.laborStaffKey.laborTimeKey.ni1", new TranslatedException(
                        Generic.T12640, Generic.T1755));
            }
            if (laborStaffKey.getStaffResourceCode() == null || laborStaffKey.getStaffResourceCode().isEmpty()) {
                rejectValue(context, errors, "laborStaff.laborStaffKey.staffResourceCode", new TranslatedException(
                        Generic.T12640, ProductionMo.T34758));
            }
            Ktpsxd ktpsxd;
            try {
                ktpsxd = KtpsxdDAOFactory.getDAO().getByPK(context.getDbCtx(), laborTimeKey.getEstKey().getCsoc(),
                        laborTimeKey.getEstKey().getCetab(), laborTimeKey.getProductionDate(),
                        laborTimeKey.getResourceCode(), laborTimeKey.getNi1(), laborStaffKey.getStaffResourceCode());
                if ((ktpsxd != null) && (ktpsxd.getCresmo() != null)) {
                    reject(context, errors,
                            new TranslatedException(Jtech.T24683, new VarParamTranslation(ktpsxd.getCresmo())));
                }
            } catch (DAOException e) {
                LOGGER.error("Unable to validate the Labor Staff corresponding to the bean " + target, e);
            }
        }
    }

}

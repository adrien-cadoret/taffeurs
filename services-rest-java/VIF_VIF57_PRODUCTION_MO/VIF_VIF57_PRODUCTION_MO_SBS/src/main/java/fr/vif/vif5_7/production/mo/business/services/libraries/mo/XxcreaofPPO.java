/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxcreaofPPO.java,v $
 * Created on 08 août 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.mo;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.StringHolder;


/**
 * Generated class from xxcreaofpxy.p.
 * 
 * @author cj
 */
public class XxcreaofPPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(XxcreaofPPO.class);

    /**
     * Context.
     */
    private VIFContext          ctx;

    /**
     * Default Constructor.
     * 
     * @param ctx Database Context.
     */
    public XxcreaofPPO(final VIFContext ctx) {
        super();
        this.ctx = ctx;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "xxcreaofpxy.p";
    }

    /**
     * Generated Class prChronoOf.
     * 
     * @param icsoc String INPUT
     * @param icetab String INPUT
     * @param iprechro String INPUT
     * @param ochronoof int OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prChronoOf(final String icsoc, final String icetab, final String iprechro, final IntegerHolder ochronoof)
            throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ochronoof == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prChronoOf values :" + "ochronoof = " + ochronoof);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prChronoOf " + " icsoc = " + icsoc + " icetab = " + icetab + " iprechro = "
                    + iprechro + " ochronoof = " + ochronoof);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(4);

        try {
            params.addCharacter(0, icsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, icetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iprechro, ParamArrayMode.INPUT);
            params.addInteger(3, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Chrono-OF", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            ochronoof.setIntegerValue((Integer) params.getOutputParameter(3));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCreateOf.
     * 
     * @param icsoc String INPUT
     * @param icetab String INPUT
     * @param iprechro String INPUT
     * @param ichronoof int INPUT
     * @param traisdat boolean INPUT
     * @param datedeb Date INPUT
     * @param heurdeb int INPUT
     * @param cetat String INPUT
     * @param carts String INPUT
     * @param typeItiAct String INPUT
     * @param itiAct String INPUT
     * @param typflux String INPUT
     * @param lot String INPUT
     * @param qte double INPUT
     * @param cunite String INPUT
     * @param qtestk double INPUT
     * @param qtestk2 double INPUT
     * @param custk2 String INPUT
     * @param freinte double INPUT
     * @param cUser String INPUT
     * @param tReconst boolean INPUT
     * @param lotent String INPUT
     * @param retour boolean OUTPUT
     * @param msg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCreateOf(final String icsoc, final String icetab, final String iprechro, final int ichronoof,
            final boolean traisdat, final Date datedeb, final int heurdeb, final String cetat, final String carts,
            final String typeItiAct, final String itiAct, final String typflux, final String lot, final double qte,
            final String cunite, final double qtestk, final double qtestk2, final String custk2, final double freinte,
            final String cUser, final boolean tReconst, final String lotent, final BooleanHolder retour,
            final StringHolder msg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (retour == null || msg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCreateOf values :" + "retour = " + retour + "msg = " + msg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCreateOf " + " icsoc = " + icsoc + " icetab = " + icetab + " iprechro = "
                    + iprechro + " ichronoof = " + ichronoof + " traisdat = " + traisdat + " datedeb = " + datedeb
                    + " heurdeb = " + heurdeb + " cetat = " + cetat + " carts = " + carts + " typeItiAct = "
                    + typeItiAct + " itiAct = " + itiAct + " typflux = " + typflux + " lot = " + lot + " qte = " + qte
                    + " cunite = " + cunite + " qtestk = " + qtestk + " qtestk2 = " + qtestk2 + " custk2 = " + custk2
                    + " freinte = " + freinte + " cUser = " + cUser + " tReconst = " + tReconst + " lotent = " + lotent
                    + " retour = " + retour + " msg = " + msg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(24);

        try {
            params.addCharacter(0, icsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, icetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iprechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(ichronoof), ParamArrayMode.INPUT);
            params.addLogical(4, new Boolean(traisdat), ParamArrayMode.INPUT);
            params.addDate(5, dateToGreg(datedeb), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(heurdeb), ParamArrayMode.INPUT);
            params.addCharacter(7, cetat, ParamArrayMode.INPUT);
            params.addCharacter(8, carts, ParamArrayMode.INPUT);
            params.addCharacter(9, typeItiAct, ParamArrayMode.INPUT);
            params.addCharacter(10, itiAct, ParamArrayMode.INPUT);
            params.addCharacter(11, typflux, ParamArrayMode.INPUT);
            params.addCharacter(12, lot, ParamArrayMode.INPUT);
            params.addDecimal(13, new BigDecimal(qte).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(14, cunite, ParamArrayMode.INPUT);
            params.addDecimal(15, new BigDecimal(qtestk).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addDecimal(16, new BigDecimal(qtestk2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(17, custk2, ParamArrayMode.INPUT);
            params.addDecimal(18, new BigDecimal(freinte).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(19, cUser, ParamArrayMode.INPUT);
            params.addLogical(20, new Boolean(tReconst), ParamArrayMode.INPUT);
            params.addCharacter(21, lotent, ParamArrayMode.INPUT);
            params.addLogical(22, null, ParamArrayMode.OUTPUT);
            params.addCharacter(23, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Create-OF", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            retour.setBooleanValue((Boolean) params.getOutputParameter(22));
            msg.setStringValue((String) params.getOutputParameter(23));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCreateOfPlanrgt.
     * 
     * @param icsoc String INPUT
     * @param icetab String INPUT
     * @param iprechro String INPUT
     * @param ichronoof int INPUT
     * @param iprechror String INPUT
     * @param ichronor int INPUT
     * @param ini1or int INPUT
     * @param traisdat boolean INPUT
     * @param datedeb Date INPUT
     * @param heurdeb int INPUT
     * @param cetat String INPUT
     * @param carts String INPUT
     * @param typeItiAct String INPUT
     * @param itiAct String INPUT
     * @param typflux String INPUT
     * @param lot String INPUT
     * @param qte double INPUT
     * @param cunite String INPUT
     * @param qtestk double INPUT
     * @param qtestk2 double INPUT
     * @param custk2 String INPUT
     * @param freinte double INPUT
     * @param cUser String INPUT
     * @param tReconst boolean INPUT
     * @param lotent String INPUT
     * @param cplanrgt String INPUT
     * @param cres String INPUT
     * @param retour boolean OUTPUT
     * @param msg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCreateOfPlanrgt(final String icsoc, final String icetab, final String iprechro, final int ichronoof,
            final String iprechror, final int ichronor, final int ini1or, final boolean traisdat, final Date datedeb,
            final int heurdeb, final String cetat, final String carts, final String typeItiAct, final String itiAct,
            final String typflux, final String lot, final double qte, final String cunite, final double qtestk,
            final double qtestk2, final String custk2, final double freinte, final String cUser,
            final boolean tReconst, final String lotent, final String cplanrgt, final String cres,
            final BooleanHolder retour, final StringHolder msg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (retour == null || msg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCreateOfPlanrgt values :" + "retour = " + retour + "msg = " + msg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCreateOfPlanrgt " + " icsoc = " + icsoc + " icetab = " + icetab + " iprechro = "
                    + iprechro + " ichronoof = " + ichronoof + " iprechror = " + iprechror + " ichronor = " + ichronor
                    + " ini1or = " + ini1or + " traisdat = " + traisdat + " datedeb = " + datedeb + " heurdeb = "
                    + heurdeb + " cetat = " + cetat + " carts = " + carts + " typeItiAct = " + typeItiAct
                    + " itiAct = " + itiAct + " typflux = " + typflux + " lot = " + lot + " qte = " + qte
                    + " cunite = " + cunite + " qtestk = " + qtestk + " qtestk2 = " + qtestk2 + " custk2 = " + custk2
                    + " freinte = " + freinte + " cUser = " + cUser + " tReconst = " + tReconst + " lotent = " + lotent
                    + " cplanrgt = " + cplanrgt + " cres = " + cres + " retour = " + retour + " msg = " + msg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(29);

        try {
            params.addCharacter(0, icsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, icetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iprechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(ichronoof), ParamArrayMode.INPUT);
            params.addCharacter(4, iprechror, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(ichronor), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(ini1or), ParamArrayMode.INPUT);
            params.addLogical(7, new Boolean(traisdat), ParamArrayMode.INPUT);
            params.addDate(8, dateToGreg(datedeb), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(heurdeb), ParamArrayMode.INPUT);
            params.addCharacter(10, cetat, ParamArrayMode.INPUT);
            params.addCharacter(11, carts, ParamArrayMode.INPUT);
            params.addCharacter(12, typeItiAct, ParamArrayMode.INPUT);
            params.addCharacter(13, itiAct, ParamArrayMode.INPUT);
            params.addCharacter(14, typflux, ParamArrayMode.INPUT);
            params.addCharacter(15, lot, ParamArrayMode.INPUT);
            params.addDecimal(16, new BigDecimal(qte).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(17, cunite, ParamArrayMode.INPUT);
            params.addDecimal(18, new BigDecimal(qtestk).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addDecimal(19, new BigDecimal(qtestk2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(20, custk2, ParamArrayMode.INPUT);
            params.addDecimal(21, new BigDecimal(freinte).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(22, cUser, ParamArrayMode.INPUT);
            params.addLogical(23, new Boolean(tReconst), ParamArrayMode.INPUT);
            params.addCharacter(24, lotent, ParamArrayMode.INPUT);
            params.addCharacter(25, cplanrgt, ParamArrayMode.INPUT);
            params.addCharacter(26, cres, ParamArrayMode.INPUT);
            params.addLogical(27, null, ParamArrayMode.OUTPUT);
            params.addCharacter(28, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Create-OF-planrgt", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            retour.setBooleanValue((Boolean) params.getOutputParameter(27));
            msg.setStringValue((String) params.getOutputParameter(28));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCreateOfTechnique.
     * 
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iDatedeb Date INPUT
     * @param iHeurdeb int INPUT
     * @param iTypeItiAct String INPUT
     * @param iItiAct String INPUT
     * @param iQte double INPUT
     * @param iCunite String INPUT
     * @param iCuser String INPUT
     * @param iPrechror String INPUT
     * @param iChronor int INPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCreateOfTechnique(final String iCsoc, final String iCetab, final String iPrechro, final int iChrono,
            final Date iDatedeb, final int iHeurdeb, final String iTypeItiAct, final String iItiAct, final double iQte,
            final String iCunite, final String iCuser, final String iPrechror, final int iChronor,
            final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCreateOfTechnique values :" + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCreateOfTechnique " + " iCsoc = " + iCsoc + " iCetab = " + iCetab
                    + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iDatedeb = " + iDatedeb + " iHeurdeb = "
                    + iHeurdeb + " iTypeItiAct = " + iTypeItiAct + " iItiAct = " + iItiAct + " iQte = " + iQte
                    + " iCunite = " + iCunite + " iCuser = " + iCuser + " iPrechror = " + iPrechror + " iChronor = "
                    + iChronor + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addDate(4, dateToGreg(iDatedeb), ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iHeurdeb), ParamArrayMode.INPUT);
            params.addCharacter(6, iTypeItiAct, ParamArrayMode.INPUT);
            params.addCharacter(7, iItiAct, ParamArrayMode.INPUT);
            params.addDecimal(8, new BigDecimal(iQte).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(9, iCunite, ParamArrayMode.INPUT);
            params.addCharacter(10, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(11, iPrechror, ParamArrayMode.INPUT);
            params.addInteger(12, new Integer(iChronor), ParamArrayMode.INPUT);
            params.addLogical(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Create-OF-Technique", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oRet.setBooleanValue((Boolean) params.getOutputParameter(13));
            oMsg.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCreateOfTechniquePlanrgt.
     * 
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iPrechro String INPUT
     * @param iChrono int INPUT
     * @param iDatedeb Date INPUT
     * @param iHeurdeb int INPUT
     * @param iTypeItiAct String INPUT
     * @param iItiAct String INPUT
     * @param iQte double INPUT
     * @param iCunite String INPUT
     * @param iCuser String INPUT
     * @param iPrechror String INPUT
     * @param iChronor int INPUT
     * @param cplanrgt String INPUT
     * @param oRet boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCreateOfTechniquePlanrgt(final String iCsoc, final String iCetab, final String iPrechro,
            final int iChrono, final Date iDatedeb, final int iHeurdeb, final String iTypeItiAct, final String iItiAct,
            final double iQte, final String iCunite, final String iCuser, final String iPrechror, final int iChronor,
            final String cplanrgt, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCreateOfTechniquePlanrgt values :" + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCreateOfTechniquePlanrgt " + " iCsoc = " + iCsoc + " iCetab = " + iCetab
                    + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iDatedeb = " + iDatedeb + " iHeurdeb = "
                    + iHeurdeb + " iTypeItiAct = " + iTypeItiAct + " iItiAct = " + iItiAct + " iQte = " + iQte
                    + " iCunite = " + iCunite + " iCuser = " + iCuser + " iPrechror = " + iPrechror + " iChronor = "
                    + iChronor + " cplanrgt = " + cplanrgt + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(16);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addDate(4, dateToGreg(iDatedeb), ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iHeurdeb), ParamArrayMode.INPUT);
            params.addCharacter(6, iTypeItiAct, ParamArrayMode.INPUT);
            params.addCharacter(7, iItiAct, ParamArrayMode.INPUT);
            params.addDecimal(8, new BigDecimal(iQte).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(9, iCunite, ParamArrayMode.INPUT);
            params.addCharacter(10, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(11, iPrechror, ParamArrayMode.INPUT);
            params.addInteger(12, new Integer(iChronor), ParamArrayMode.INPUT);
            params.addCharacter(13, cplanrgt, ParamArrayMode.INPUT);
            params.addLogical(14, null, ParamArrayMode.OUTPUT);
            params.addCharacter(15, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Create-OF-Technique-Planrgt", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oRet.setBooleanValue((Boolean) params.getOutputParameter(14));
            oMsg.setStringValue((String) params.getOutputParameter(15));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

}

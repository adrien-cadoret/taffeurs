/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MOEndSBSImpl.java,v $
 * Created on 26 déc. 07 by ped
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.end;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProxyBusinessException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.common.util.i18n.VarParamTranslation;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPO;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabPPOTtListoperations;
import fr.vif.vif5_7.production.mo.constants.Mnemos.SplitControl;
import fr.vif.vif5_7.production.mo.dao.xpiloia.XpiloiaDAOFactory;


/**
 * Business services implementation for end operations on manufacturing order.
 * 
 * @author ped
 */
@Component
public class MOEndSBSImpl implements MOEndSBS {

    private static final Logger LOGGER = Logger.getLogger(MOEndSBSImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public String checkSplit(final VIFContext ctx, final OperationItemKey operationItemKey, final String workstationId,
            final QuantityUnit qty1, final QuantityUnit qty2, final SplitControl splitControlToDo,
            final boolean newMovement) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkSplit(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", workstationId="
                    + workstationId + ", qty1=" + qty1 + ", qty2=" + qty2 + ", splitControlToDo=" + splitControlToDo
                    + ", newMovement=" + newMovement + ")");
        }
        StringHolder strMsg = new StringHolder();
        StringHolder strInfo = new StringHolder();
        BooleanHolder bRet = new BooleanHolder();
        QuantityUnit quant1 = (qty1 == null) ? (new QuantityUnit(0, "")) : qty1;
        QuantityUnit quant2 = (qty2 == null) ? (new QuantityUnit(0, "")) : qty2;
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prCtrlEcart(operationItemKey.getEstablishmentKey().getCsoc(), operationItemKey.getEstablishmentKey()
                .getCetab(), workstationId, operationItemKey.getManagementType().getValue(), operationItemKey
                .getChrono().getPrechro(), operationItemKey.getChrono().getChrono(), operationItemKey.getCounter1(),
                operationItemKey.getCounter2(), operationItemKey.getCounter3(), operationItemKey.getCounter4(), quant1
                        .getQty(), quant1.getUnit(), quant2.getQty(), quant2.getUnit(), !newMovement, splitControlToDo
                        .getValue(), strInfo, bRet, strMsg);
        if (!bRet.getBooleanValue()) {
            throw new ProxyBusinessException(strMsg.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkSplit(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", workstationId="
                    + workstationId + ", qty1=" + qty1 + ", qty2=" + qty2 + ", splitControlToDo=" + splitControlToDo
                    + ", newMovement=" + newMovement + ")");
        }
        return strInfo.getStringValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String finishAllOperationItem(final VIFContext ctx, final List<OperationItemKey> listOperationItemKey,
            final String workstationId, final ActivityItemType activityItemType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - finishOperationItem(ctx=" + ctx + ", listOperationItemKey=" + listOperationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
        }

        StringHolder oInfo = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();

        XxfabPPO xxfabPPO = new XxfabPPO(ctx);

        List<XxfabPPOTtListoperations> listXxfabPPOTtListoperations = new ArrayList<XxfabPPOTtListoperations>();

        for (OperationItemKey operationItemKey : listOperationItemKey) {
            XxfabPPOTtListoperations xxfabPPOTtListoperations = new XxfabPPOTtListoperations();
            xxfabPPOTtListoperations.setCsoc(operationItemKey.getEstablishmentKey().getCsoc());
            xxfabPPOTtListoperations.setCetab(operationItemKey.getEstablishmentKey().getCetab());
            xxfabPPOTtListoperations.setTypGest(operationItemKey.getManagementType().getValue());
            xxfabPPOTtListoperations.setPrechrof(operationItemKey.getChrono().getPrechro());
            xxfabPPOTtListoperations.setChronof(operationItemKey.getChrono().getChrono());
            xxfabPPOTtListoperations.setNi1(operationItemKey.getCounter1());
            xxfabPPOTtListoperations.setNi2(operationItemKey.getCounter2());
            xxfabPPOTtListoperations.setNi3(operationItemKey.getCounter3());
            xxfabPPOTtListoperations.setNi4(operationItemKey.getCounter4());

            listXxfabPPOTtListoperations.add(xxfabPPOTtListoperations);
        }

        xxfabPPO.prQuittancementTout(listXxfabPPOTtListoperations, workstationId, ctx.getIdCtx().getLogin(),
                activityItemType.getValue(), oInfo, oRet, oWstr);

        if (!oRet.getBooleanValue()) {
            LOGGER.error("Error - finishOperationItem(ctx=" + ctx + ", listOperationItemKey=" + listOperationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
            throw new ProxyBusinessException(oWstr.getStringValue() + "\n" + oInfo.getStringValue());
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - finishOperationItem(ctx=" + ctx + ", listOperationItemKey=" + listOperationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
        }
        return oInfo.getStringValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void finishMarking(final VIFContext ctx, final EstablishmentKey establishmentKey, final String workstation,
            final String userId, final Chrono moChrono) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - finishMarking(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", workstation="
                    + workstation + ", userId=" + userId + ", moChrono=" + moChrono + ")");
        }
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prQuittancementmarquage(establishmentKey.getCsoc(), establishmentKey.getCetab(), workstation, userId,
                moChrono.getPrechro(), moChrono.getChrono());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - finishMarking(ctx=" + ctx + ", establishmentKey=" + establishmentKey + ", workstation="
                    + workstation + ", userId=" + userId + ", moChrono=" + moChrono + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    public String finishOperationItem(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId, final ActivityItemType activityItemType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - finishOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
        }
        StringHolder oInfo = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prQuittancementLigne(operationItemKey.getEstablishmentKey().getCsoc(), operationItemKey
                .getEstablishmentKey().getCetab(), workstationId, ctx.getIdCtx().getLogin(), operationItemKey
                .getManagementType().getValue(), operationItemKey.getChrono().getPrechro(), operationItemKey
                .getChrono().getChrono(), operationItemKey.getCounter1(), operationItemKey.getCounter2(),
                operationItemKey.getCounter3(), operationItemKey.getCounter4(), activityItemType.getValue(), oInfo,
                oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            LOGGER.error("Error - finishOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
            throw new ProxyBusinessException(oWstr.getStringValue() + "\n" + oInfo.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - finishOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ", activityItemType=" + activityItemType + ")");
        }
        return oInfo.getStringValue();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String finishOriginMO(final VIFContext ctx, final MOBean moBean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - finishOriginMO(ctx=" + ctx + ", moBean=" + moBean + ")");
        }
        StringHolder oInfo = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prQuittancementof(moBean.getMoKey().getEstablishmentKey().getCsoc(), moBean.getMoKey()
                .getEstablishmentKey().getCetab(), ctx.getIdCtx().getWorkstation(), ctx.getIdCtx().getLogin(), moBean
                .getMoKey().getChrono().getPrechro(), moBean.getMoKey().getChrono().getChrono(), oInfo, oRet, oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue() + "\n" + oInfo.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - finishOriginMO(ctx=" + ctx + ", moBean=" + moBean + ")");
        }
        return oInfo.getStringValue();
    }

    /**
     * {@inheritDoc}
     */
    public boolean isFinishedMO(final VIFContext ctx, final MOKey key, final ActivityItemType activityItemType,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isFinishedMO(ctx=" + ctx + ", key=" + key + ", activityItemType=" + activityItemType
                    + ", workstationId=" + workstationId + ")");
        }
        if (key == null || key.getEstablishmentKey() == null || key.getEstablishmentKey().getCsoc() == null
                || key.getEstablishmentKey().getCsoc().equals("") || key.getEstablishmentKey().getCetab() == null
                || key.getEstablishmentKey().getCetab().equals("") || key.getChrono() == null
                || key.getChrono().getPrechro() == null || key.getChrono().getPrechro().equals("")
                || key.getChrono().getChrono() == 0 || workstationId == null) {
            throw new ServerBusinessException(Generic.T5815, new VarParamTranslation(null));
        }
        boolean ret = false;
        try {
            ret = XpiloiaDAOFactory.getDAO().isFinishedMO(ctx.getDbCtx(), key, activityItemType, workstationId);
        } catch (DAOException e) {
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isFinishedMO(ctx=" + ctx + ", key=" + key + ", activityItemType=" + activityItemType
                    + ", workstationId=" + workstationId + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    public void resumeOperationItem(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - resumeOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        // Log
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug(new StringBuilder().append("resumeOperationItem(ctx, operationItemKey = ")
                    .append(operationItemKey).append(", workstationId = ").append(workstationId).append(")").toString());
        }
        XxfabPPO xxfabPPO = new XxfabPPO(ctx);
        xxfabPPO.prDequittancementLigne(operationItemKey.getEstablishmentKey().getCsoc(), operationItemKey
                .getEstablishmentKey().getCetab(), workstationId, ctx.getIdCtx().getLogin(), operationItemKey
                .getChrono().getPrechro(), operationItemKey.getChrono().getChrono(), operationItemKey.getCounter1(),
                operationItemKey.getCounter2(), operationItemKey.getCounter3(), operationItemKey.getCounter4(), oRet,
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - resumeOperationItem(ctx=" + ctx + ", operationItemKey=" + operationItemKey
                    + ", workstationId=" + workstationId + ")");
        }
    }
}

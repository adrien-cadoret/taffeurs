/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: StackingPlanSBSImpl.java,v $
 * Created on 17 Apr 2013 by xg
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.stackingplan;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.common.EntityLocationKey;
import fr.vif.vif57.stock.entity.business.services.libraries.entities.keys.ContainerKey;
import fr.vif.vif5_7.activities.activities.business.beans.common.stackingplan.StackingPlan;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.StackingPlanInfoBean;
import fr.vif.vif5_7.production.mo.dao.xplanrm.Xplanrm;
import fr.vif.vif5_7.production.mo.dao.xplanrm.XplanrmDAOFactory;
import fr.vif.vif5_7.production.mo.dao.xplanrmd.Xplanrmd;
import fr.vif.vif5_7.production.mo.dao.xplanrmd.XplanrmdDAOFactory;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;


/**
 * SBS implementation for stacking plan.
 * 
 * @author xg
 */
@Component
public class StackingPlanSBSImpl implements StackingPlanSBS {

    /** LOGGER. */
    private static final Logger                                                                          LOGGER = Logger.getLogger(StackingPlanSBSImpl.class);

    @Autowired
    @Qualifier("fr.vif.vif5_7.activities.activities.business.services.libraries.stackingplan.StackingPlanSBS")
    private fr.vif.vif5_7.activities.activities.business.services.libraries.stackingplan.StackingPlanSBS stackingPlanSBS;

    /**
     * Gets the logger.
     * 
     * @return the logger.
     */
    protected static Logger getLogger() {
        return LOGGER;
    }

    @Override
    public StackingPlanInfoBean getFirstStackingPlanInfo(final VIFContext ctx, final MOKey moKey,
            final String stackingPlanCode, final String itemId, final double qty) throws ServerBusinessException {
        StackingPlanInfoBean stackingPlanInfoBean = null;

        try {
            // ordered list to get the nologm order
            List<Xplanrmd> lst = XplanrmdDAOFactory.getDAO().listByPK(ctx.getDbCtx(),
                    moKey.getEstablishmentKey().getCsoc(), moKey.getEstablishmentKey().getCetab(),
                    moKey.getChrono().getPrechro(), moKey.getChrono().getChrono(), stackingPlanCode, null, null, null,
                    null, true);

            Xplanrmd xplanItem = null;
            if (lst != null && lst.size() > 0) {
                // get the first nolog and noplan where there is place to put the item (not completed)
                for (Xplanrmd xplanrmd : lst) {
                    if (itemId.equals(xplanrmd.getCart()) && (xplanrmd.getQtefait() + qty) <= xplanrmd.getQteprev()) {
                        xplanItem = xplanrmd;
                        break;
                    }
                }

                // if the current xplanrmd is not full and has a nsc, we use it
                if (xplanItem != null && !xplanItem.getTplein() && xplanItem.getNsc() != null
                        && !xplanItem.getNsc().isEmpty()) {
                    stackingPlanInfoBean = convertIntoStackingPlanInfoBean(xplanItem);
                } else if (xplanItem != null) {
                    // is there already a packaging in the same nolog for the same packaging
                    for (Xplanrmd xplanrmd : lst) {
                        if (xplanItem.getCcond() == xplanrmd.getCcond()
                                && xplanItem.getNologm() == xplanrmd.getNologm()
                                && xplanItem.getNoplan() == xplanrmd.getNoplan() && xplanrmd.getNsc() != null
                                && !xplanrmd.getNsc().isEmpty()) {
                            stackingPlanInfoBean = convertIntoStackingPlanInfoBean(xplanrmd);
                            break;
                        } else if (xplanItem.getCcond() == xplanrmd.getCcond()
                                && xplanItem.getNologm() == xplanrmd.getNologm()
                                && xplanItem.getNoplan() == xplanrmd.getNoplan()) {
                            // if the nsc is null, we just use the ccond
                            stackingPlanInfoBean = convertIntoStackingPlanInfoBean(xplanrmd);
                            stackingPlanInfoBean.setContainerNumber("");
                        }
                    }
                }
            }
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }

        return stackingPlanInfoBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public QtyUnit getInnerMOStackingPlan(final VIFContext ctx, final MOKey moKey, final String stackingPlanCode,
            final String itemId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getInnerMOStackingPlan(ctx=" + ctx + ", moKey=" + moKey + ", stackingPlanCode="
                    + stackingPlanCode + ", itemId=" + itemId + ")");
        }
        QtyUnit inner = null;
        List<Xplanrmd> lstXplanrmd;
        try {
            lstXplanrmd = XplanrmdDAOFactory.getDAO().listByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), moKey.getChrono().getPrechro(),
                    moKey.getChrono().getChrono(), stackingPlanCode, itemId, null, null, null, true);
            if (lstXplanrmd != null && !lstXplanrmd.isEmpty()) {
                for (Xplanrmd xplanrmd : lstXplanrmd) {
                    // takes the first inner qty for not full xplanrmd
                    if (!xplanrmd.getTplein()) {
                        break;
                    }
                }
                if (inner == null) {
                    // if all xplarmd are full, takes the last
                    inner = new QtyUnit(lstXplanrmd.get(lstXplanrmd.size() - 1).getSpcb(), lstXplanrmd.get(0)
                            .getCuart());
                }
            }
        } catch (DAOException e) {
            LOGGER.error(e.getMessage());
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getInnerMOStackingPlan(ctx=" + ctx + ", moKey=" + moKey + ", stackingPlanCode="
                    + stackingPlanCode + ", itemId=" + itemId + ")=" + inner);
        }
        return inner;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public int getLogicalNumberFromNSC(final VIFContext ctx, final EstablishmentKey establishmentKey, final String nsc)
            throws ServerBusinessException {
        int ret = 0;
        try {
            List<Xplanrmd> lStackingPlan = XplanrmdDAOFactory.getDAO().listByI4(ctx.getDbCtx(),
                    establishmentKey.getCsoc(), establishmentKey.getCetab(), nsc, true);
            if (lStackingPlan != null && lStackingPlan.size() > 0) {
                ret = lStackingPlan.get(0).getNologm();
            } else {
                LOGGER.error("", new ServerBusinessException(Generic.T17702, ProductionMo.T34513));
            }
        } catch (DAOException e) {
            LOGGER.error(e.getMessage());
            throw new ServerBusinessException(e);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CodeLabel getMOStackingPlanCode(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getMOStackingPlan(ctx=" + ctx + ", moKey=" + moKey + ")");
        }
        CodeLabel stackingPlan = new CodeLabel();
        try {
            List<Xplanrm> lStackingPlan = XplanrmDAOFactory.getDAO().listByPK(ctx.getDbCtx(),
                    moKey.getEstablishmentKey().getCsoc(), moKey.getEstablishmentKey().getCetab(),
                    moKey.getChrono().getPrechro(), moKey.getChrono().getChrono(), null, false);
            if (lStackingPlan != null && !lStackingPlan.isEmpty()) {
                stackingPlan.setCode(lStackingPlan.get(0).getCplanr());
                StackingPlan sp = getStackingPlanSBS().getStackingPlanByCode(ctx, moKey.getEstablishmentKey(),
                        lStackingPlan.get(0).getCplanr());
                if (sp != null && sp.getStackingPlanCL() != null && sp.getStackingPlanCL().getLabel() != null
                        && !sp.getStackingPlanCL().getLabel().isEmpty()) {
                    stackingPlan.setLabel(sp.getStackingPlanCL().getLabel());
                }
            }
        } catch (DAOException e) {
            LOGGER.error(e.getMessage());
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getMOStackingPlan(ctx=" + ctx + ", moKey=" + moKey + ")=" + stackingPlan);
        }
        return stackingPlan;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getPackagingFromNSC(final VIFContext ctx, final EstablishmentKey establishmentKey, final String nsc)
            throws ServerBusinessException {
        String ret = null;
        try {
            List<Xplanrmd> lStackingPlan = XplanrmdDAOFactory.getDAO().listByI4(ctx.getDbCtx(),
                    establishmentKey.getCsoc(), establishmentKey.getCetab(), nsc, true);
            if (lStackingPlan != null && lStackingPlan.size() > 0) {
                ret = lStackingPlan.get(0).getCcond();
            } else {
                throw new ServerBusinessException(Generic.T17702, ProductionMo.T34513);
            }
        } catch (DAOException e) {
            LOGGER.error(e.getMessage());
            throw new ServerBusinessException(e);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CEntityBean getPackagingId(final VIFContext ctx, final MOKey moKey, final String itemId, final double qty)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getPackagingId(ctx=" + ctx + ", moKey=" + moKey + ", itemId=" + itemId + ")");
        }
        CEntityBean packaging = null;
        try {
            // ordered list to get the nologm order
            List<Xplanrmd> lst = XplanrmdDAOFactory.getDAO().listByPK(ctx.getDbCtx(),
                    moKey.getEstablishmentKey().getCsoc(), moKey.getEstablishmentKey().getCetab(),
                    moKey.getChrono().getPrechro(), moKey.getChrono().getChrono(), null, null, null, null, null, true);
            Xplanrmd xplanItem = null;
            if (lst != null && lst.size() > 0) {
                // get the first nolog and noplan where there is place to put the item (not completed)
                for (Xplanrmd xplanrmd : lst) {
                    if (itemId.equals(xplanrmd.getCart()) && (xplanrmd.getQtefait() + qty) <= xplanrmd.getQteprev()) {
                        xplanItem = xplanrmd;
                        break;
                    }
                }
                // if the current xplanrmd is not full and has a nsc, we use it
                if (xplanItem != null && !xplanItem.getTplein() && xplanItem.getNsc() != null
                        && !xplanItem.getNsc().isEmpty()) {
                    EntityLocationKey locKey = new EntityLocationKey(new EstablishmentKey(xplanItem.getCsoc(),
                            xplanItem.getCetab()));
                    locKey.setContainerNumber(xplanItem.getNsc());
                    packaging = new CEntityBean();
                    packaging.getEntityLocationBean().setEntityLocationKey(locKey);
                    packaging.getEntityLocationBean().getContainerPackaging().setPackagingId(xplanItem.getCcond());
                } else if (xplanItem != null) {
                    // is there already a packaging in the same nolog for the same packaging
                    for (Xplanrmd xplanrmd : lst) {
                        if (xplanItem.getCcond() == xplanrmd.getCcond()
                                && xplanItem.getNologm() == xplanrmd.getNologm()
                                && xplanItem.getNoplan() == xplanrmd.getNoplan() && xplanrmd.getNsc() != null
                                && !xplanrmd.getNsc().isEmpty()) {
                            EntityLocationKey locKey = new EntityLocationKey(new EstablishmentKey(xplanrmd.getCsoc(),
                                    xplanrmd.getCetab()));
                            locKey.setContainerNumber(xplanrmd.getNsc());
                            packaging = new CEntityBean();
                            packaging.getEntityLocationBean().setEntityLocationKey(locKey);
                            packaging.getEntityLocationBean().getContainerPackaging()
                                    .setPackagingId(xplanrmd.getCcond());
                            break;
                        } else if (xplanItem.getCcond() == xplanrmd.getCcond()
                                && xplanItem.getNologm() == xplanrmd.getNologm()
                                && xplanItem.getNoplan() == xplanrmd.getNoplan()) {
                            // if the nsc is null, we just use the ccond
                            packaging = new CEntityBean();
                            packaging.getEntityLocationBean().getContainerPackaging()
                                    .setPackagingId(xplanrmd.getCcond());
                        }
                    }
                }
            }
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getPackagingId(ctx=" + ctx + ", moKey=" + moKey + ", itemId=" + itemId + ")=" + packaging);
        }
        return packaging;
    }

    /**
     * Gets the stackingPlanSBS.
     * 
     * @return the stackingPlanSBS.
     */
    public fr.vif.vif5_7.activities.activities.business.services.libraries.stackingplan.StackingPlanSBS getStackingPlanSBS() {
        return stackingPlanSBS;
    }

    /**
     * {@inheritDoc}
     * 
     */
    @Override
    public boolean isCurrentContainerFull(final VIFContext ctx, final ContainerKey containerKey,
            final CodeLabels itemCL, final QtyUnit qtyToInsert) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isCurrentContainerFull(ctx=" + ctx + ", containerKey=" + containerKey + ")");
        }
        boolean ret = true;
        boolean qtyIsUsed = false;
        try {
            List<Xplanrmd> lst = XplanrmdDAOFactory.getDAO().listByI4(ctx.getDbCtx(),
                    containerKey.getEstablishmentKey().getCsoc(), containerKey.getEstablishmentKey().getCetab(),
                    containerKey.getContainerNumber(), false);
            if (lst != null && lst.size() > 0) {
                Xplanrmd planRef = lst.get(0);
                List<Xplanrmd> lst2 = XplanrmdDAOFactory.getDAO().listByPK(ctx.getDbCtx(), planRef.getCsoc(),
                        planRef.getCetab(), planRef.getPrechrof(), planRef.getChronof(), planRef.getCplanr(), null,
                        planRef.getCcond(), planRef.getNologm(), null, false);
                for (Xplanrmd xplanrmd : lst2) {
                    if (!xplanrmd.getTplein()) {
                        // it's ok when :
                        // 1. the qty is not already use
                        // 2. the item to insert matchs
                        // 3. the qty to insert equals the qty to do
                        // 4. the itemCL is null => we check that all xplanrmd are full
                        if (itemCL == null) {
                            ret = false;
                            break;
                        } else if (!qtyIsUsed && itemCL.getCode().equals(xplanrmd.getCart())
                                && qtyToInsert.getQty() == (xplanrmd.getQteprev() - xplanrmd.getQtefait())) {
                            qtyIsUsed = true;
                        } else {
                            ret = false;
                            break;
                        }
                    }
                }
            }
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isCurrentContainerFull(ctx=" + ctx + ", containerKey=" + containerKey + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public boolean isThereContainerWithThisQty(final VIFContext context, final ContainerKey containerKey,
            final QtyUnit qtyNeeded, final MOKey moKey, final String cart) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - isThereContainerWithThisQty(context=" + context + ", containerKey=" + containerKey
                    + ", qtyNeeded=" + qtyNeeded + ", moKey=" + moKey + ", cart=" + cart + ")");
        }
        boolean ret = false;
        try {
            List<Xplanrmd> lst = XplanrmdDAOFactory.getDAO().listByI2(context.getDbCtx(),
                    containerKey.getEstablishmentKey().getCsoc(), containerKey.getEstablishmentKey().getCetab(),
                    moKey.getChrono().getPrechro(), moKey.getChrono().getChrono(), null, false, cart, null, null, null,
                    false);
            if (lst != null && lst.size() > 0) {
                for (Xplanrmd xplanrmd : lst) {
                    // double availableQty = xplanrmd.getQtefait() - xplanrmd.getQteprev();
                    if (xplanrmd.getQtefait() + qtyNeeded.getQty() <= xplanrmd.getQteprev()) {
                        ret = true;
                        break;
                    }
                }
            }
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - isThereContainerWithThisQty(context=" + context + ", containerKey=" + containerKey
                    + ", qtyNeeded=" + qtyNeeded + ", moKey=" + moKey + ", cart=" + cart + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<String> listContainerNumber(final VIFContext ctx, final MOKey moKey, final String itemId,
            final boolean notShowEmpty) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listContainerNumber(ctx=" + ctx + ", moKey=" + moKey + ", itemId=" + itemId
                    + ", notShowEmpty=" + notShowEmpty + ")");
        }
        List<String> res = new ArrayList<String>();
        try {
            List<Xplanrmd> lStackingPlan = XplanrmdDAOFactory.getDAO()
                    .listByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                            moKey.getEstablishmentKey().getCetab(), moKey.getChrono().getPrechro(),
                            moKey.getChrono().getChrono(), null, itemId, null, null, null, true);
            for (Xplanrmd xplanrmd : lStackingPlan) {
                if (!res.contains(xplanrmd.getNsc()) && (!notShowEmpty || xplanrmd.getTplein())) {
                    res.add(xplanrmd.getNsc());
                }
            }
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listContainerNumber(ctx=" + ctx + ", moKey=" + moKey + ", itemId=" + itemId
                    + ", notShowEmpty=" + notShowEmpty + ")=" + res);
        }
        return res;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, StackingPlanInfoBean> listInfoBeanFromMOStackingPlan(final VIFContext ctx, final MOKey moKey,
            final String stackingPlanCode) throws ServerBusinessException {
        Map<String, StackingPlanInfoBean> mapItemInner = new HashMap<String, StackingPlanInfoBean>();
        List<Xplanrmd> lstXplanrmd;
        try {
            lstXplanrmd = XplanrmdDAOFactory.getDAO().listByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), moKey.getChrono().getPrechro(),
                    moKey.getChrono().getChrono(), stackingPlanCode, null, null, null, null, true);
            if (lstXplanrmd != null && !lstXplanrmd.isEmpty()) {
                for (Xplanrmd xplanrmd : lstXplanrmd) {
                    // first I put all inner qty for not full xplanrmd
                    if (!xplanrmd.getTplein() && mapItemInner.get(xplanrmd.getCart()) == null) {
                        mapItemInner.put(xplanrmd.getCart(), convertIntoStackingPlanInfoBean(xplanrmd));
                    }
                }

                // second, I add the inner qty for item which are completely done (all xplanrmd are full), I have to
                // display the last inner qty (change the list order)
                for (int i = lstXplanrmd.size() - 1; i >= 0; i--) {
                    for (Xplanrmd xplanrmd : lstXplanrmd) {
                        if (mapItemInner.get(xplanrmd.getCart()) == null) {
                            mapItemInner.put(xplanrmd.getCart(), convertIntoStackingPlanInfoBean(xplanrmd));
                        }
                    }
                }
            }
        } catch (DAOException e) {
            LOGGER.error(e.getMessage());
            throw new ServerBusinessException(e);
        }
        return mapItemInner;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<String, QtyUnit> listInnerMOStackingPlan(final VIFContext ctx, final MOKey moKey,
            final String stackingPlanCode) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getInnerMOStackingPlan(ctx=" + ctx + ", moKey=" + moKey + ", stackingPlanCode="
                    + stackingPlanCode + ")");
        }
        Map<String, QtyUnit> mapItemInner = new HashMap<String, QtyUnit>();
        List<Xplanrmd> lstXplanrmd;
        try {
            lstXplanrmd = XplanrmdDAOFactory.getDAO().listByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), moKey.getChrono().getPrechro(),
                    moKey.getChrono().getChrono(), stackingPlanCode, null, null, null, null, true);
            if (lstXplanrmd != null && !lstXplanrmd.isEmpty()) {
                for (Xplanrmd xplanrmd : lstXplanrmd) {
                    // first I put all inner qty for not full xplanrmd
                    if (!xplanrmd.getTplein() && mapItemInner.get(xplanrmd.getCart()) == null) {
                        mapItemInner.put(xplanrmd.getCart(), new QtyUnit(xplanrmd.getSpcb(), xplanrmd.getCuart()));
                    }
                }

                // second, I add the inner qty for item which are completely done (all xplanrmd are full), I have to
                // display the last inner qty (change the list order)
                for (int i = lstXplanrmd.size() - 1; i >= 0; i--) {
                    for (Xplanrmd xplanrmd : lstXplanrmd) {
                        if (mapItemInner.get(xplanrmd.getCart()) == null) {
                            mapItemInner.put(xplanrmd.getCart(), new QtyUnit(xplanrmd.getSpcb(), xplanrmd.getCuart()));
                        }
                    }
                }
            }
        } catch (DAOException e) {
            LOGGER.error(e.getMessage());
            throw new ServerBusinessException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getInnerMOStackingPlan(ctx=" + ctx + ", moKey=" + moKey + ", stackingPlanCode="
                    + stackingPlanCode + ")=" + mapItemInner);
        }
        return mapItemInner;
    }

    /**
     * Sets the stackingPlanSBS.
     * 
     * @param stackingPlanSBS stackingPlanSBS.
     */
    public void setStackingPlanSBS(
            final fr.vif.vif5_7.activities.activities.business.services.libraries.stackingplan.StackingPlanSBS stackingPlanSBS) {
        this.stackingPlanSBS = stackingPlanSBS;
    }

    /**
     * Convert a xplanrmd into a new StackingPlanInfoBean.
     * 
     * @param xplanItem the plc
     * @return the info bean
     */
    private StackingPlanInfoBean convertIntoStackingPlanInfoBean(final Xplanrmd xplanItem) {
        StackingPlanInfoBean ret = new StackingPlanInfoBean();
        ret.setContainerNumber(xplanItem.getNsc());
        ret.setFull(xplanItem.getTplein());
        ret.setInner(xplanItem.getSpcb());
        ret.setLogicNumber(xplanItem.getNologm());
        ret.setPackagingId(xplanItem.getCcond());
        ret.setQtyDone(xplanItem.getQtefait());
        ret.setQtyToDo(xplanItem.getQteprev());
        ret.setRank(xplanItem.getNlig());
        ret.setStackingNumber(xplanItem.getNoplan());
        ret.setStackingPlanCL(new CodeLabel(xplanItem.getCplanr(), ""));
        ret.setSubdivision(xplanItem.getCsubdiv());
        ret.setUnit(xplanItem.getCuart());
        return ret;
    }
}

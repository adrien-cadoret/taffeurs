/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: ContentRateSBSImpl.java,v $
 * Created on 16 Jul 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.content;


import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.apache.log4j.Logger;
import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProxyBusinessException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.DateHolder;
import fr.vif.jtech.business.progress.DoubleHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.common.util.DateHelper;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.location.business.beans.common.location.LocationKey;
import fr.vif.vif5_7.production.mo.business.beans.common.content.ContentRateBatchBean;
import fr.vif.vif5_7.production.mo.business.beans.common.content.ContentRatePOHeader;
import org.springframework.stereotype.Component;


/**
 * SBS for content feature.
 * 
 * @author cj
 */
@Component
public class ContentRateSBSImpl implements ContentRateSBS {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(ContentRateSBSImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public ContentRatePOHeader getContentRatePOHeader(final VIFContext ctx, final ContentRatePOHeader initialBean)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getContentRatePOHeader(ctx=" + ctx + ", initialBean=" + initialBean + ")");
        }
        ContentRatePOHeader result = initialBean;
        XxrdtenPPO ppo = new XxrdtenPPO(ctx);
        StringHolder ocCriTen = new StringHolder();
        StringHolder oFormatTen = new StringHolder();
        DoubleHolder oValTenTheo = new DoubleHolder();
        StringHolder ocUniteTen = new StringHolder();
        StringHolder oLstArtTen = new StringHolder();
        DoubleHolder oQteReste = new DoubleHolder();
        StringHolder ocUnite = new StringHolder();
        DateHolder oDateMvt = new DateHolder();
        IntegerHolder oHeureMvt = new IntegerHolder();
        ppo.pRechercherDonneesOt(initialBean.getMoKey().getEstablishmentKey().getCsoc(), initialBean.getMoKey()
                .getEstablishmentKey().getCetab(), initialBean.getMoKey().getChrono().getPrechro(), initialBean
                .getMoKey().getChrono().getChrono(), initialBean.getMoKey().getCounter1(), ocCriTen, oFormatTen,
                oValTenTheo, ocUniteTen, oLstArtTen, oQteReste, ocUnite, oDateMvt, oHeureMvt);
        result.setCriteriaId(ocCriTen.getStringValue());
        result.setQtyTarget(new QtyUnit(oValTenTheo.getDoubleValue(), ocUniteTen.getStringValue()));
        result.setQtyCritCurrent(new QtyUnit(0, ocUniteTen.getStringValue()));
        result.setQtyTodo(new QtyUnit(oQteReste.getDoubleValue(), ocUnite.getStringValue()));
        if (oDateMvt != null && oHeureMvt != null) {
            Date movementDate = DateHelper.getDateTime(oDateMvt.getDateValue().getTime(), oHeureMvt.getIntegerValue());
            result.setMovementDate(movementDate);
        }
        String[] itemLst = oLstArtTen.getStringValue().split(";");
        result.setItemLst(new ArrayList<String>());
        for (String item : itemLst) {
            result.getItemLst().add(item);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getContentRatePOHeader(ctx=" + ctx + ", initialBean=" + initialBean + ")=" + result);
        }
        return result;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ContentRateBatchBean> listBatch(final VIFContext ctx, final ContentRatePOHeader sBean,
            final String user, final String workstation, final LocationKey location) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listBatch(ctx=" + ctx + ", sBean=" + sBean + ", user=" + user + ", workstation="
                    + workstation + ", location=" + location + ")");
        }
        List<ContentRateBatchBean> ret = new ArrayList<ContentRateBatchBean>();
        XxrdtenPPO ppo = new XxrdtenPPO(ctx);
        TempTableHolder<XxrdtenPPOTtstock> ttStockHolder = new TempTableHolder<XxrdtenPPOTtstock>();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        ppo.pListerLots(sBean.getMoKey().getEstablishmentKey().getCsoc(), sBean.getMoKey().getEstablishmentKey()
                .getCetab(), sBean.getMoKey().getChrono().getPrechro(), sBean.getMoKey().getChrono().getChrono(), sBean
                .getMoKey().getCounter1(), user, workstation, location.getCdepot(), location.getCemp(), ttStockHolder,
                oRet, oMsg);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMsg.getStringValue());
        }
        for (XxrdtenPPOTtstock ttStock : ttStockHolder.getListValue()) {
            ret.add(convertTTStock2ContentRateBatchBean(ttStock));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listBatch(ctx=" + ctx + ", sBean=" + sBean + ", user=" + user + ", workstation="
                    + workstation + ", location=" + location + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<ContentRateBatchBean> listUpdatedBatch(final VIFContext ctx, final ContentRatePOHeader sBean,
            final List<ContentRateBatchBean> listBean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listUpdatedBatch(ctx=" + ctx + ", sBean=" + sBean + ", listBean=" + listBean + ")");
        }
        List<ContentRateBatchBean> ret = new ArrayList<ContentRateBatchBean>();
        XxrdtenPPO ppo = new XxrdtenPPO(ctx);
        List<XxrdtenPPOTtstock> ittStock = new ArrayList<XxrdtenPPOTtstock>();
        for (ContentRateBatchBean bean : listBean) {
            ittStock.add(convertContentRateBatchBean2TTStock(bean));
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        TempTableHolder<XxrdtenPPOTtstock> ttStockHolder = new TempTableHolder<XxrdtenPPOTtstock>();
        ppo.pProposerLots(sBean.getMoKey().getEstablishmentKey().getCsoc(), sBean.getMoKey().getEstablishmentKey()
                .getCetab(), sBean.getMoKey().getChrono().getPrechro(), sBean.getMoKey().getChrono().getChrono(), sBean
                .getMoKey().getCounter1(), sBean.getQtyCritCurrent().getQty(), ittStock, ttStockHolder, oRet, oMsg);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMsg.getStringValue());
        }
        for (XxrdtenPPOTtstock ttStock : ttStockHolder.getListValue()) {
            ret.add(convertTTStock2ContentRateBatchBean(ttStock));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listUpdatedBatch(ctx=" + ctx + ", sBean=" + sBean + ", listBean=" + listBean + ")=" + ret);
        }
        return ret;
    }

    /**
     * Convert a ContentRateBatchBean into a XxrdtenPPOTtstock.
     * 
     * @param bean the sbs bean
     * @return the proxy bean
     */
    private XxrdtenPPOTtstock convertContentRateBatchBean2TTStock(final ContentRateBatchBean bean) {
        XxrdtenPPOTtstock ttStock = new XxrdtenPPOTtstock();
        ttStock.setCArt(bean.getItem());
        ttStock.setCArtPrev(bean.getExpectedItem());
        ttStock.setCuCri(bean.getCritQtyUnit().getUnit());
        ttStock.setCunite(bean.getStockQtyUnit().getUnit());
        ttStock.setLot(bean.getBatch());
        ttStock.setNOrdre(bean.getOrder());
        ttStock.setQteAfer(bean.getTodoQty().getQty());
        ttStock.setQteStk(bean.getStockQtyUnit().getQty());
        ttStock.setValCri(bean.getCritQtyUnit().getQty());
        ttStock.setValCriRes(bean.getCritQtyUnitCalculated().getQty());
        return ttStock;
    }

    /**
     * Convert a XxrdtenPPOTtstock into a ContentRateBatchBean.
     * 
     * @param ttStock the proxy object
     * @return the sbs bean
     */
    private ContentRateBatchBean convertTTStock2ContentRateBatchBean(final XxrdtenPPOTtstock ttStock) {
        ContentRateBatchBean ret = new ContentRateBatchBean();
        ret.setBatch(ttStock.getLot());
        ret.setCritQtyUnit(new QtyUnit(ttStock.getValCri(), ttStock.getCuCri()));
        ret.setCritQtyUnitCalculated(new QtyUnit(ttStock.getValCriRes(), ttStock.getCuCri()));
        ret.setExpectedItem(ttStock.getCArtPrev());
        ret.setItem(ttStock.getCArt());
        ret.setOrder(ttStock.getNOrdre());
        ret.setStockQtyUnit(new QtyUnit(ttStock.getQteStk(), ttStock.getCunite()));
        ret.setTodoQty(new QtyUnit(ttStock.getQteAfer(), ttStock.getCunite()));
        return ret;
    }
}

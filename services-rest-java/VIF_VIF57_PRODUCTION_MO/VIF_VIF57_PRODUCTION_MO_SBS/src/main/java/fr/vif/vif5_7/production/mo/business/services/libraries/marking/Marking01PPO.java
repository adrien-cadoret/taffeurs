/*
 * Copyright (c) 2014 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: Marking01PPO.java,v $
 * Created on 06 févr. 2014 by mnt
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.marking;


import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.StringHolder;


/**
 * Generated class from marking01pxy.p.
 * 
 * @author mnt
 */
public class Marking01PPO extends AbstractPPO {

    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(Marking01PPO.class);

    /**
     * Context.
     */
    private VIFContext          ctx;

    /**
     * Default Constructor.
     * 
     * @param ctx Database Context.
     */
    public Marking01PPO(final VIFContext ctx) {
        super();
        this.ctx = ctx;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "marking01pxy.p";
    }

    /**
     * Generated Class startPrinting.
     * 
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param iTypgest String INPUT
     * @param iPrechrof String INPUT
     * @param iChronof int INPUT
     * @param iNi1 int INPUT
     * @param iNi2 int INPUT
     * @param iNi3 int INPUT
     * @param iNi4 int INPUT
     * @param icUser String INPUT
     * @param icPoste String INPUT
     * @param icFonct String INPUT
     * @param icTypeMat String INPUT
     * @param icCodeMat String INPUT
     * @param icDocId String INPUT
     * @param oRet boolean OUTPUT
     * @param oMess String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void startPrinting(final String iCsoc, final String iCetab, final String iTypgest, final String iPrechrof,
            final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String icUser,
            final String icPoste, final String icFonct, final String icTypeMat, final String icCodeMat,
            final String icDocId, final BooleanHolder oRet, final StringHolder oMess) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMess == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : startPrinting values :" + "oRet = " + oRet + "oMess = " + oMess);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method startPrinting " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iTypgest = "
                    + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1
                    + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " icUser = " + icUser + " icPoste = "
                    + icPoste + " icFonct = " + icFonct + " icTypeMat = " + icTypeMat + " icCodeMat = " + icCodeMat
                    + " icDocId = " + icDocId + " oRet = " + oRet + " oMess = " + oMess);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(17);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(3, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(9, icUser, ParamArrayMode.INPUT);
            params.addCharacter(10, icPoste, ParamArrayMode.INPUT);
            params.addCharacter(11, icFonct, ParamArrayMode.INPUT);
            params.addCharacter(12, icTypeMat, ParamArrayMode.INPUT);
            params.addCharacter(13, icCodeMat, ParamArrayMode.INPUT);
            params.addCharacter(14, icDocId, ParamArrayMode.INPUT);
            params.addLogical(15, null, ParamArrayMode.OUTPUT);
            params.addCharacter(16, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "startPrinting", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oRet.setBooleanValue((Boolean) params.getOutputParameter(15));
            oMess.setStringValue((String) params.getOutputParameter(16));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class stopPrinting.
     * 
     * @param iCsoc String INPUT
     * @param iCetab String INPUT
     * @param icUser String INPUT
     * @param icPoste String INPUT
     * @param icFonct String INPUT
     * @param icTypeMat String INPUT
     * @param icCodeMat String INPUT
     * @param oRet boolean OUTPUT
     * @param oMess String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void stopPrinting(final String iCsoc, final String iCetab, final String icUser, final String icPoste,
            final String icFonct, final String icTypeMat, final String icCodeMat, final BooleanHolder oRet,
            final StringHolder oMess) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMess == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : stopPrinting values :" + "oRet = " + oRet + "oMess = " + oMess);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method stopPrinting " + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " icUser = "
                    + icUser + " icPoste = " + icPoste + " icFonct = " + icFonct + " icTypeMat = " + icTypeMat
                    + " icCodeMat = " + icCodeMat + " oRet = " + oRet + " oMess = " + oMess);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(9);

        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, icUser, ParamArrayMode.INPUT);
            params.addCharacter(3, icPoste, ParamArrayMode.INPUT);
            params.addCharacter(4, icFonct, ParamArrayMode.INPUT);
            params.addCharacter(5, icTypeMat, ParamArrayMode.INPUT);
            params.addCharacter(6, icCodeMat, ParamArrayMode.INPUT);
            params.addLogical(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "stopPrinting", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oRet.setBooleanValue((Boolean) params.getOutputParameter(7));
            oMess.setStringValue((String) params.getOutputParameter(8));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

}

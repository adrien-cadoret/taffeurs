/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: OfliedocPPOTtdocument.java,v $
 * Created on 01 août 2013 by vr
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.doc;


import java.io.Serializable;

import fr.vif.jtech.business.progress.AbstractTempTable;


/**
 * Generated class from c:temp/ofliedocpxy.p.xml.
 * 
 * @author vr
 */
public class OfliedocPPOTtdocument extends AbstractTempTable implements Serializable {

    private String path  = "";

    private String titre = "";

    /**
     * Default Constructor.
     * 
     */
    public OfliedocPPOTtdocument() {
        super();
    }

    /**
     * Gets the path.
     * 
     * @category getter
     * @return the path.
     */
    public final String getPath() {
        return path;
    }

    /**
     * Gets the titre.
     * 
     * @category getter
     * @return the titre.
     */
    public final String getTitre() {
        return titre;
    }

    /**
     * Sets the path.
     * 
     * @category setter
     * @param path path.
     */
    public final void setPath(final String path) {
        this.path = path;
    }

    /**
     * Sets the titre.
     * 
     * @category setter
     * @param titre titre.
     */
    public final void setTitre(final String titre) {
        this.titre = titre;
    }

}

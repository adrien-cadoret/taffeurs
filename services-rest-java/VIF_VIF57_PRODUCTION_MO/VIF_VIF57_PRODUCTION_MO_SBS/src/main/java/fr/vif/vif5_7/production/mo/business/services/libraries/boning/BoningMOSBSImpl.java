/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: BoningMOSBSImpl.java,v $
 * Created on 15 Mar 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.boning;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProxyBusinessException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.DoubleHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.business.progress.TempTableHolder;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.common.beans.CodeLabels;
import fr.vif.jtech.common.util.TimeHelper;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif57.stock.entity.business.services.libraries.movements.MovementAct;
import fr.vif.vif5_7.activities.activities.business.beans.common.fabrication.Fabrication;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.ActivityItemType;
import fr.vif.vif5_7.gen.criteria.business.beans.common.CriteriaCompleteBean;
import fr.vif.vif5_7.gen.criteria.business.services.libraries.criteria.SpecificationSBS;
import fr.vif.vif5_7.gen.item.business.beans.common.ItemKey;
import fr.vif.vif5_7.gen.item.business.beans.common.unit.QtyUnit;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.gen.service.business.beans.common.unit.QuantityUnit;
import fr.vif.vif5_7.gen.service.business.services.libraries.unit.UnitSBS;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.boning.labels.LabelBean;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputItemParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputBatchType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputContainerCapacityClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputContainerClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputEntityType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputParametersEnums.OutputUpperContainerCapacityClosingType;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputUpdateReturnBean;
import fr.vif.vif5_7.production.mo.business.beans.common.dataentry.item.output.OutputWorkstationParameters;
import fr.vif.vif5_7.production.mo.business.beans.common.itemmovement.SearchLabel;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOBoningBean;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.AbstractOperationItemBean;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemKey;
import fr.vif.vif5_7.production.mo.business.beans.common.operationitem.OperationItemQuantityUnit;
import fr.vif.vif5_7.production.mo.business.beans.common.printdocument.PrintDocument;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.FBoningOutputBBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fboningoutput.StackingPlanInfoBean;
import fr.vif.vif5_7.production.mo.business.beans.features.dataentry.item.boning.fdataentrylist.FBoningDataEntryListInputBBean;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.MOSBSImpl;
import fr.vif.vif5_7.production.mo.business.services.libraries.mo.XxcreaofPPO;
import fr.vif.vif5_7.production.mo.business.services.libraries.poitem.POItemSBS;
import fr.vif.vif5_7.production.mo.constants.Mnemos.LabelingEventType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.ManagementType;
import fr.vif.vif5_7.production.mo.constants.Mnemos.PrechroId;
import fr.vif.vif5_7.production.mo.constants.Mnemos.PrintEvent;
import fr.vif.vif5_7.production.mo.constants.ProductionMOParamEnum;
import fr.vif.vif5_7.production.mo.constants.features.dataentry.item.boning.Mnemos.BoningConstant;
import fr.vif.vif5_7.production.mo.dao.kcriv.Kcriv;
import fr.vif.vif5_7.production.mo.dao.kcriv.KcrivDAOFactory;
import fr.vif.vif5_7.production.mo.dao.kof.Kof;
import fr.vif.vif5_7.production.mo.dao.kof.KofDAOFactory;
import fr.vif.vif5_7.production.mo.dao.kot.Kot;
import fr.vif.vif5_7.production.mo.dao.kot.KotDAOFactory;
import fr.vif.vif5_7.production.mo.dao.kotad.Kotad;
import fr.vif.vif5_7.production.mo.dao.kotad.KotadDAOFactory;
import fr.vif.vif5_7.stock.entity.business.beans.composites.centity.CEntityBean;


/**
 * SBS Implementation to get datas for boning mo.
 * 
 * @author cj
 */
@Component
public class BoningMOSBSImpl extends MOSBSImpl implements BoningMOSBS {

    /** LOGGER. */
    private static final Logger LOGGER                                = Logger.getLogger(BoningMOSBSImpl.class);

    private static final int    PARAM_FIELD_FABRICATION_INPUT_PROFILE = 1;

    @Autowired
    private SpecificationSBS    specificationSBS;

    @Autowired
    private UnitSBS             unitSBS;

    @Autowired
    private POItemSBS           poItemSBS;

    @Override
    public List<PrintDocument> printAllLabels(final VIFContext ctx, final OperationItemKey operationItemKey,
            final String itemId, final String mvtRowid, final ActivityItemType itemType, final List<LabelBean> labels)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - printAllInputLabels(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", itemId="
                    + itemId + ", mvtRowid=" + mvtRowid + ", itemType=" + itemType + ")");
        }
        List<PrintDocument> documents = new ArrayList<PrintDocument>();

        List<XxfabrecPPOTtXetiq> ttLabel = new ArrayList<XxfabrecPPOTtXetiq>();
        for (LabelBean labelBean : labels) {
            XxfabrecPPOTtXetiq xetiq = new XxfabrecPPOTtXetiq();
            xetiq.setCsoc(operationItemKey.getEstablishmentKey().getCsoc());
            xetiq.setCetab(operationItemKey.getEstablishmentKey().getCetab());
            xetiq.setCetiq(labelBean.getCode());
            xetiq.setCtypdoc(labelBean.getType());
            xetiq.setPrgedi(labelBean.getEdiPrg());
            ttLabel.add(xetiq);
        }

        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        TempTableHolder<XxfabrecPPOTtLocaldocprg> ttUnprintedHolder = new TempTableHolder<XxfabrecPPOTtLocaldocprg>();

        XxfabrecPPO xxfabrecPPO = new XxfabrecPPO(ctx);
        xxfabrecPPO.prEditeEtiquettesEntreeSortie(operationItemKey.getEstablishmentKey().getCsoc(), //
                operationItemKey.getEstablishmentKey().getCetab(), //
                ctx.getIdCtx().getWorkstation(), //
                ctx.getIdCtx().getLogin(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                itemId, //
                mvtRowid, //
                itemType.getValue(), //
                ttLabel, //
                ttUnprintedHolder, //
                oRet, //
                oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        for (XxfabrecPPOTtLocaldocprg doc : ttUnprintedHolder.getListValue()) {
            documents.add(new PrintDocument(doc.getCdoc(), PrintEvent.getPrintEvent(doc.getEvent()), doc.getNbdoc()));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - printAllInputLabels(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", itemId="
                    + itemId + ", mvtRowid=" + mvtRowid + ", itemType=" + itemType + ")=" + documents);
        }
        return documents;
    }

    /**
     * Gets the logger.
     * 
     * @return the logger.
     */
    protected static Logger getLogger() {
        return LOGGER;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public MOBoningBean createStartedBoningMSO(final VIFContext ctx, final MOKey msoKey,
            final Date effectiveBeginDateTime, final Fabrication fabrication, final QuantityUnit quantityUnit,
            final CodeLabel stackingPlanCL, final String userId, final MOKey originMOKey)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - createStartedBoningMSO(ctx=" + ctx + ", msoKey=" + msoKey + ", effectiveBeginDateTime="
                    + effectiveBeginDateTime + ", fabrication=" + fabrication + ", quantityUnit=" + quantityUnit
                    + ", stackingPlanCL=" + stackingPlanCL + ", userId=" + userId + ", originMOKey=" + originMOKey
                    + ")");
        }
        XxcreaofPPO ppo = new XxcreaofPPO(ctx);
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        ppo.prCreateOfTechniquePlanrgt(msoKey.getEstablishmentKey().getCsoc(), msoKey.getEstablishmentKey().getCetab(),
                msoKey.getChrono().getPrechro(), msoKey.getChrono().getChrono(), effectiveBeginDateTime, TimeHelper
                        .getTime(effectiveBeginDateTime), fabrication.getFabricationType().getValue(), fabrication
                        .getFabricationCLs().getCode(), quantityUnit.getQty(), quantityUnit.getUnit(), userId,
                originMOKey.getChrono().getPrechro(), originMOKey.getChrono().getChrono(), stackingPlanCL.getCode(),
                oRet, oMsg);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMsg.getStringValue());
        }
        ppo.release(ctx.getDbCtx());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - createStartedBoningMSO(ctx=" + ctx + ", msoKey=" + msoKey + ", effectiveBeginDateTime="
                    + effectiveBeginDateTime + ", fabrication=" + fabrication + ", quantityUnit=" + quantityUnit
                    + ", stackingPlanCL=" + stackingPlanCL + ", userId=" + userId + ", originMOKey=" + originMOKey
                    + ")");
        }
        return getMOBoningBeanByKey(ctx, msoKey);
    }

    @Override
    public FBoningOutputBBean getBBean(final VIFContext ctx, final AbstractOperationItemBean operationItemBean,
            final FBoningOutputBBean bbean, final String hchyValue, final String stackingplanId,
            final Map<OperationItemKey, QtyUnit> doneQtyList, final Map<String, StackingPlanInfoBean> mapItemQty)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getBBean(ctx=" + ctx + ", operationItemBean=" + operationItemBean + ", bbean=" + bbean
                    + ")");
        }
        MOKey moKey = new MOKey();
        moKey.setEstablishmentKey(operationItemBean.getOperationItemKey().getEstablishmentKey());
        moKey.setChrono(operationItemBean.getOperationItemKey().getChrono());

        FBoningOutputBBean ret;
        if (bbean != null) {
            ret = bbean;
        } else {
            ret = new FBoningOutputBBean();
        }
        ret.setItemCLs(new CodeLabels(operationItemBean.getItemCL().getCode(),
                operationItemBean.getItemCL().getLabel(), operationItemBean.getItemCL().getShortLabel()));
        // ret.setOperationItemKey(operationItemBean.getOperationItemKey());
        ret.setOperationItemBean(operationItemBean);
        // Quantities
        ret.setItemQttyDoneInRefUnit(getUnitSBS().formatQty(
                ctx,
                new QuantityUnit(operationItemBean.getOperationItemQuantityUnit().getCompletedQuantity(),
                        operationItemBean.getOperationItemQuantityUnit().getUnit())));

        ret.setItemQttyToDoInRefUnit(getUnitSBS().formatQty(
                ctx,
                new QuantityUnit(operationItemBean.getOperationItemQuantityUnit().getToDoQuantity(), operationItemBean
                        .getOperationItemQuantityUnit().getUnit())));
        ret.setItemRefUnit(operationItemBean.getOperationItemQuantityUnit().getUnit());

        ret.setItemQttyDoneInRefUnitQU(new QtyUnit(operationItemBean.getOperationItemQuantityUnit()
                .getCompletedQuantity(), operationItemBean.getOperationItemQuantityUnit().getUnit()));
        ret.setItemQttyToDoInRefUnitQU(new QtyUnit(operationItemBean.getOperationItemQuantityUnit().getToDoQuantity(),
                operationItemBean.getOperationItemQuantityUnit().getUnit()));

        if (doneQtyList != null) {
            if (doneQtyList.get(new OperationItemKey(ManagementType.REAL, moKey.getEstablishmentKey(), moKey
                    .getChrono(), operationItemBean.getOperationItemKey().getCounter1(), 0, 0, operationItemBean
                    .getOperationItemKey().getCounter4())) != null) {
                QtyUnit oiqu = doneQtyList.get(new OperationItemKey(ManagementType.REAL, moKey.getEstablishmentKey(),
                        moKey.getChrono(), operationItemBean.getOperationItemKey().getCounter1(), 0, 0,
                        operationItemBean.getOperationItemKey().getCounter4()));
                ret.setItemQtty2DoneStr(getUnitSBS().formatQty(ctx, oiqu));
                ret.setItemQtty2Done(oiqu.getQty());
                ret.setItemQtty2Unit(oiqu.getUnit());
            }
        } else {
            OperationItemQuantityUnit oiqu = getPoItemSBS().getPOItemQuantity2(ctx,
                    operationItemBean.getOperationItemKey());
            ret.setItemQtty2DoneStr(getUnitSBS().formatQty(ctx,
                    new QuantityUnit(oiqu.getCompletedQuantity(), oiqu.getUnit())));
            ret.setItemQtty2Done(oiqu.getCompletedQuantity());
            ret.setItemQtty2Unit(oiqu.getUnit());
        }

        if (hchyValue != null) {
            ret.setHierarchy(hchyValue);
        }

        // get the Stacking plan info
        StackingPlanInfoBean spinfo = null;
        if (mapItemQty == null) {
            spinfo = getStackingPlanSBS().getFirstStackingPlanInfo(ctx, moKey, stackingplanId,
                    operationItemBean.getItemCL().getCode(), 1);
        } else {
            spinfo = mapItemQty.get(operationItemBean.getItemCL().getCode());
        }

        if (spinfo == null) {
            // is the item finish ?
            spinfo = getStackingPlanSBS().getFirstStackingPlanInfo(ctx, moKey, stackingplanId,
                    operationItemBean.getItemCL().getCode(), 0);
            if (spinfo == null) {
                ret.setStackingPlanInfoBean(new StackingPlanInfoBean());
                ret.setHasStackingplan(false);
                ret.setInnerStr("");
            } else {
                ret.setStackingPlanInfoBean(spinfo);
                ret.setHasStackingplan(true);
                ret.setInnerStr(getUnitSBS().formatQtyUnit(ctx, new QuantityUnit(spinfo.getInner(), spinfo.getUnit())));
            }
        } else {
            ret.setStackingPlanInfoBean(spinfo);
            ret.setHasStackingplan(true);
            ret.setInnerStr(getUnitSBS().formatQtyUnit(ctx, new QuantityUnit(spinfo.getInner(), spinfo.getUnit())));
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getBBean(ctx=" + ctx + ", operationItemBean=" + operationItemBean + ", bbean=" + bbean
                    + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getMOCustomer(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getMOCustomer(ctx=" + ctx + ", moKey=" + moKey + ")");
        }
        final String customerCriteria = BoningConstant.CUSTOMER_CRITERIA;
        String customer = "";
        String key = new StringBuffer().append(moKey.getChrono().getPrechro()).append(",")
                .append(moKey.getChrono().getChrono()).toString();
        try {
            Kcriv cri = KcrivDAOFactory.getDAO().getByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), PrechroId.PRODUCTION.getValue(), key, customerCriteria);
            if (cri != null) {
                customer = cri.getValcmin();
            }
        } catch (DAOException e) {
            LOGGER.error(e.getMessage());
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getMOCustomer(ctx=" + ctx + ", moKey=" + moKey + ")=" + customer);
        }
        return customer;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public CodeLabel getMOSpecification(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getMOSpecification(ctx=" + ctx + ", moKey=" + moKey + ")");
        }
        CodeLabel specificationCL = null;
        String key = new StringBuffer().append(moKey.getChrono().getPrechro()).append(",")
                .append(moKey.getChrono().getChrono()).toString();
        try {
            Kcriv cri = KcrivDAOFactory.getDAO().getByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), PrechroId.PRODUCTION.getValue(), key,
                    BoningConstant.SPEC_CRITERIA);
            if (cri != null && !cri.getValcmin().isEmpty()) {
                specificationCL = new CodeLabel(cri.getValcmin(), "");
                specificationCL.setLabel(getSpecificationSBS()
                        .getSpecificationCL(ctx, moKey.getEstablishmentKey().getCsoc(),
                                moKey.getEstablishmentKey().getCetab(), cri.getValcmin()));
            }
        } catch (DAOException e) {
            LOGGER.error(e.getMessage());
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getMOSpecification(ctx=" + ctx + ", moKey=" + moKey + ")=" + specificationCL);
        }
        return specificationCL;
    }

    /**
     * {@inheritDoc}
     * 
     * @deprecated Replace by MoProfileSBS.getOutputProfile(...)
     */
    @Deprecated
    @Override
    public OutputItemParameters getOutputItemParameters(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String itemId, final String workstationId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getOutputItemParameters(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", itemId=" + itemId + ", workstationId=" + workstationId + ")");
        }
        OutputItemParameters outputItemParameters = new OutputItemParameters();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        IntegerHolder oEntityType = new IntegerHolder();
        StringHolder oProfileId = new StringHolder();
        StringHolder oCcond = new StringHolder();
        StringHolder oCcondp = new StringHolder();
        IntegerHolder oTypFerm = new IntegerHolder();
        IntegerHolder oTypFermCapa = new IntegerHolder();
        IntegerHolder oTypFermp = new IntegerHolder();
        IntegerHolder oTypFermpCapa = new IntegerHolder();
        DoubleHolder oQte1 = new DoubleHolder();
        StringHolder oCu1 = new StringHolder();
        DoubleHolder oQte2 = new DoubleHolder();
        StringHolder oCu2 = new StringHolder();
        StringHolder oCdepot = new StringHolder();
        StringHolder oCemp = new StringHolder();
        StringHolder oBatch = new StringHolder();
        XxfabrecPPO xxfabrec = new XxfabrecPPO(ctx);
        xxfabrec.prGetOutputParametersBoning(establishmentKey.getCsoc(), //
                establishmentKey.getCetab(), //
                workstationId, //
                itemId, //
                oProfileId, //
                oEntityType, //
                oCcond, //
                oCcondp, //
                oTypFerm, //
                oTypFermCapa, //
                oTypFermp, //
                oTypFermpCapa, //
                oQte1, //
                oCu1, //
                oQte2, //
                oCu2, //
                oCdepot, //
                oCemp, //
                oBatch, //
                oRet, //
                oMsg); //

        outputItemParameters.setProfileId(oProfileId.getStringValue());
        outputItemParameters.setContainerPackagingId(oCcond.getStringValue());
        outputItemParameters.setUpperContainerPackagingId(oCcondp.getStringValue());
        outputItemParameters.setEntityType(OutputEntityType.getOutputEntityType(oEntityType.getIntegerValue()));
        outputItemParameters.setContainerClosingType(OutputContainerClosingType.getOutputContainerClosingType(oTypFerm
                .getIntegerValue()));
        outputItemParameters.setUpperContainerClosingType(OutputContainerClosingType
                .getOutputContainerClosingType(oTypFermp.getIntegerValue()));
        // Units
        outputItemParameters.setForcedFirstUnit(oCu1.getStringValue());
        outputItemParameters.setForcedSecondUnit(oCu2.getStringValue());
        // Quantities
        if (oCu1.getStringValue() != null && !oCu1.getStringValue().isEmpty()) {
            outputItemParameters.setForcedFirstQty(oQte1.getDoubleValue());
            if (oCu2.getStringValue() != null && !oCu2.getStringValue().isEmpty()) {
                // Second quantity obtained from conversion
                ItemKey itemKey = new ItemKey(establishmentKey.getCsoc(), itemId);
                QtyUnit firstQtyUnit = new QtyUnit(oQte1.getDoubleValue(), oCu1.getStringValue());
                QtyUnit secondQtyUnit = getItemSBS().convertQty(ctx, itemKey, firstQtyUnit, oCu2.getStringValue(),
                        false);
                if (secondQtyUnit != null) {
                    outputItemParameters.setForcedSecondQty(secondQtyUnit.getQty());
                }
            }
        }
        outputItemParameters.setContainerCapacityClosingType(OutputContainerCapacityClosingType
                .getOutputContainerCapacityClosingType(oTypFermCapa.getIntegerValue()));
        outputItemParameters.setUpperContainerCapacityClosingType(OutputUpperContainerCapacityClosingType
                .getOutputUpperContainerCapacityClosingType(oTypFermpCapa.getIntegerValue()));
        outputItemParameters.setBatchType(OutputBatchType.getOutputBatchType(oBatch.getStringValue()));
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMsg.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getOutputItemParameters(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", itemId=" + itemId + ", workstationId=" + workstationId + ")=" + outputItemParameters);
        }
        return outputItemParameters;
    }

    /**
     * Gets the poItemSBS.
     * 
     * @return the poItemSBS.
     */
    public POItemSBS getPoItemSBS() {
        return poItemSBS;
    }

    @Override
    public QtyUnit getRealQtyFromPO(final VIFContext ctx, final MOKey moKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - getRealQtyFromPO(ctx=" + ctx + ", moKey=" + moKey + ")");
        }
        QtyUnit qty = null;
        try {
            Kof kof = KofDAOFactory.getDAO().getByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), moKey.getChrono().getPrechro(),
                    moKey.getChrono().getChrono());
            Kot kot = KotDAOFactory.getDAO().getByPK(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), moKey.getChrono().getPrechro(),
                    moKey.getChrono().getChrono(), kof.getNi1artref());
            qty = new QtyUnit(kot.getQte1(), kot.getCunite());
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - getRealQtyFromPO(ctx=" + ctx + ", moKey=" + moKey + ")=" + qty);
        }
        return qty;
    }

    /**
     * Gets the specificationSBS.
     * 
     * @return the specificationSBS.
     */
    public SpecificationSBS getSpecificationSBS() {
        return specificationSBS;
    }

    /**
     * Gets the unitSBS.
     * 
     * @return the unitSBS.
     */
    public UnitSBS getUnitSBS() {
        return unitSBS;
    }

    @Override
    public List<CriteriaCompleteBean> listCriteriaFromOrderLine(final VIFContext ctx,
            final EstablishmentKey establishmentKey, final Chrono orderChrono, final int orderline)
            throws ServerBusinessException {
        List<CriteriaCompleteBean> listCriteriaCompleteBean = new ArrayList<CriteriaCompleteBean>();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        TempTableHolder<XxfabrecPPOTtcrivproxy> ttCrivPROXYHolder = new TempTableHolder<XxfabrecPPOTtcrivproxy>();
        XxfabrecPPO xxfabrec = new XxfabrecPPO(ctx);
        xxfabrec.prGetSalesCriteria(establishmentKey.getCsoc(), establishmentKey.getCetab(), orderChrono.getPrechro(),
                orderChrono.getChrono(), orderline, ttCrivPROXYHolder, oRet, oMsg);
        /* TtCriv -> List<CriteriaCompleteBean> */
        listCriteriaCompleteBean = ttHolderToListCriteriaBean(ttCrivPROXYHolder);
        return listCriteriaCompleteBean;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public Map<OperationItemKey, QtyUnit> listDoneQty(final VIFContext ctx, final MOKey moKey,
            final ActivityItemType itemType) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listDoneQty(ctx=" + ctx + ", moKey=" + moKey + ", itemType=" + itemType + ")");
        }

        List<Kotad> lstKotad = new ArrayList<Kotad>();
        Map<OperationItemKey, QtyUnit> mapItemQtyDone = new HashMap<OperationItemKey, QtyUnit>();
        try {
            lstKotad = KotadDAOFactory.getDAO().listByI3(ctx.getDbCtx(), moKey.getEstablishmentKey().getCsoc(),
                    moKey.getEstablishmentKey().getCetab(), moKey.getChrono().getPrechro(),
                    moKey.getChrono().getChrono(), null, null, null, true);

            for (Kotad kotad : lstKotad) {
                if (itemType == null || itemType.getValue().equals(kotad.getTypacta())) {
                    OperationItemKey itemkey = new OperationItemKey(ManagementType.REAL, moKey.getEstablishmentKey(),
                            moKey.getChrono(), kotad.getNi1(), 0, 0, kotad.getNi2());
                    QtyUnit qty = mapItemQtyDone.get(itemkey);
                    if (qty == null) {
                        qty = new QtyUnit(kotad.getQte2(), kotad.getCu2());
                    } else {
                        qty.setQty(qty.getQty() + kotad.getQte2());
                    }
                    mapItemQtyDone.put(itemkey, qty);
                }
            }
        } catch (DAOException e) {
            LOGGER.error("", e);
            throw new ServerBusinessException(e);
        }

        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listDoneQty(ctx=" + ctx + ", moKey=" + moKey + ", itemType=" + itemType + ")="
                    + mapItemQtyDone);
        }
        return mapItemQtyDone;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<SearchLabel> printAct(final VIFContext ctx, final MOKey moKey, final String workstationId,
            final String userId, final FBoningDataEntryListInputBBean bBean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - printAct(ctx=" + ctx + ", moKey=" + moKey + ", workstationId=" + workstationId
                    + ", userId=" + userId + ", bBean=" + bBean + ")");
        }
        List<SearchLabel> ret = new ArrayList<SearchLabel>();
        String codeProfile = getParameterSBS().readStringParameter(ctx, moKey.getEstablishmentKey().getCsoc(),
                moKey.getEstablishmentKey().getCetab(), ProductionMOParamEnum.FABRICATION_INPUT.getCode(),
                PARAM_FIELD_FABRICATION_INPUT_PROFILE, workstationId);
        TempTableHolder<XxfabrecPPOTtLocaldocprg> docs = new TempTableHolder<XxfabrecPPOTtLocaldocprg>();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        XxfabrecPPO xxfabrec = new XxfabrecPPO(ctx);
        xxfabrec.prRechercheEditeMultiEtiquetteActe2(moKey.getEstablishmentKey().getCsoc(), //
                moKey.getEstablishmentKey().getCetab(), //
                moKey.getChrono().getPrechro(), //
                moKey.getChrono().getChrono(), //
                workstationId, //
                userId, //
                ActivityItemType.INPUT.getValue(), //
                LabelingEventType.MO_FIRST_DECLARATION.getValue(), //
                codeProfile, //
                bBean.getItemMovementBean().getItemMovementKey().getRowidKey(), //
                docs, //
                oRet, oMsg);
        for (XxfabrecPPOTtLocaldocprg doc : docs.getListValue()) {
            SearchLabel searchLabel = new SearchLabel(doc.getNbdoc() == 0, doc.getCdoc(), doc.getPrgedi());
            ret.add(searchLabel);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - printAct(ctx=" + ctx + ", moKey=" + moKey + ", workstationId=" + workstationId
                    + ", userId=" + userId + ", bBean=" + bBean + ")=" + ret);
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void printRollLabel(final VIFContext ctx, final EstablishmentKey establishmentKey,
            final String workstationId, final String userId, final String containerNb) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - printRollLabel(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", workstationId=" + workstationId + ", userId=" + userId + ", containerNb=" + containerNb + ")");
        }
        XxfabrecPPO xxfabrecPPO = new XxfabrecPPO(ctx);
        xxfabrecPPO.prPrintFicheRoll(establishmentKey.getCsoc(), establishmentKey.getCetab(), containerNb,
                workstationId, userId);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - printRollLabel(ctx=" + ctx + ", establishmentKey=" + establishmentKey
                    + ", workstationId=" + workstationId + ", userId=" + userId + ", containerNb=" + containerNb + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public OutputUpdateReturnBean saveOutputEntity(final VIFContext ctx, final OperationItemKey operationItemKey,
            final CEntityBean entity, final OutputWorkstationParameters outputWorkstationParameters,
            final OutputItemParameters outputItemParameters, final List<LabelBean> labels,
            final StackingPlanInfoBean stackingPlanInfoBean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveOutputEntity(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", entity="
                    + entity + ", outputWorkstationParameters=" + outputWorkstationParameters
                    + ", outputItemParameters=" + outputItemParameters + ", labels=" + labels + ")");
        }
        OutputUpdateReturnBean ret = new OutputUpdateReturnBean();
        CEntityBean e = entity;
        StringHolder oRowid = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oWstr = new StringHolder();
        StringHolder ioNSC = new StringHolder();
        StringHolder ioNSCP = new StringHolder();
        IntegerHolder oNLig = new IntegerHolder();
        BooleanHolder oNscClosed = new BooleanHolder();
        BooleanHolder oNscpClosed = new BooleanHolder();
        ioNSC.setStringValue(entity.getEntityLocationBean().getEntityLocationKey().getContainerNumber());
        ioNSCP.setStringValue(entity.getEntityLocationBean().getEntityLocationKey().getUpperContainerNumber());
        List<XxfabrecPPOTtXetiq> ttLabel = new ArrayList<XxfabrecPPOTtXetiq>();
        for (LabelBean labelBean : labels) {
            XxfabrecPPOTtXetiq xetiq = new XxfabrecPPOTtXetiq();
            xetiq.setCsoc(entity.getEntityLocationBean().getEntityLocationKey().getCsoc());
            xetiq.setCetab(entity.getEntityLocationBean().getEntityLocationKey().getCetab());
            xetiq.setCetiq(labelBean.getCode());
            xetiq.setCtypdoc(labelBean.getType());
            xetiq.setPrgedi(labelBean.getEdiPrg());
            ttLabel.add(xetiq);
        }
        TempTableHolder<XxfabrecPPOTtLocaldocprg> ttUnprintedHolder = new TempTableHolder<XxfabrecPPOTtLocaldocprg>();
        XxfabrecPPO xxfabrecPPO = new XxfabrecPPO(ctx);
        xxfabrecPPO
        .prValidateMvtSortieReco2(
                entity.getEntityLocationBean().getEntityLocationKey().getCsoc(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCetab(), //
                ctx.getIdCtx().getWorkstation(), //
                ctx.getIdCtx().getLogin(), //
                operationItemKey.getManagementType().getValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), //
                operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4(), //
                ActivityItemType.OUTPUT.getValue(), //
                entity.getEntityLocationBean().getContainerPackaging().getPackagingId(), //
                entity.getEntityLocationBean().getContainerPackaging().getTattooedId(), //
                0.0, //
                "", //
                entity.getEntityLocationBean().getUpperPackaging().getPackagingId(), //
                "", //
                0.0, //
                "", //
                entity.getStockItemBean().getStockItem().getItemId(), //
                entity.getStockItemBean().getStockItem().getBatch(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCdepot(), //
                entity.getEntityLocationBean().getEntityLocationKey().getCemp(), //
                "", //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getFirstQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getSecondQty().getUnit(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getQty(), //
                entity.getStockItemBean().getKeyboardQties().getThirdQty().getUnit(), //
                outputItemParameters.getContainerClosingType().getValue(), //
                outputItemParameters.getContainerCapacityClosingType().getValue(), //
                outputItemParameters.getUpperContainerClosingType().getValue(), //
                outputItemParameters.getUpperContainerCapacityClosingType().getValue(), //
                outputItemParameters.getEntityType().equals(OutputEntityType.EU), //
                (stackingPlanInfoBean != null ? (stackingPlanInfoBean.getStackingPlanCL() != null ? stackingPlanInfoBean
                        .getStackingPlanCL().getCode() : "")
                        : ""), //
                        stackingPlanInfoBean != null ? stackingPlanInfoBean.getLogicNumber() : 0, //
                                stackingPlanInfoBean != null ? (stackingPlanInfoBean.getSubdivision() != null ? stackingPlanInfoBean
                                        .getSubdivision() : "")
                                        : "", //
                        outputWorkstationParameters.getOutputTareType().getValue(), //
                                        ttLabel, //
                                        ioNSC, //
                                        ioNSCP, //
                                        ttUnprintedHolder, //
                                        oRowid, //
                                        oNscClosed, //
                                        oNscpClosed, //
                                        oNLig, //
                                        oRet, //
                                        oWstr);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oWstr.getStringValue());
        }
        e.getEntityLocationBean().getEntityLocationKey().setContainerNumber(ioNSC.getStringValue());
        e.getEntityLocationBean().getEntityLocationKey().setUpperContainerNumber(ioNSCP.getStringValue());
        for (XxfabrecPPOTtLocaldocprg doc : ttUnprintedHolder.getListValue()) {
            ret.getDocuments().add(
                    new PrintDocument(doc.getCdoc(), PrintEvent.getPrintEvent(doc.getEvent()), doc.getNbdoc()));
        }
        ret.setContainerClosed(oNscClosed.getBooleanValue());
        ret.setUpperContainerClosed(oNscpClosed.getBooleanValue());
        // Last data entry
        MovementAct movementAct = new MovementAct(oNLig.getIntegerValue(), //
                operationItemKey.getChrono().getPrechro(), //
                operationItemKey.getChrono().getChrono(), //
                operationItemKey.getCounter1(), operationItemKey.getCounter2(), //
                operationItemKey.getCounter3(), //
                operationItemKey.getCounter4());
        ret.setMovementAct(movementAct);
        ret.setEntity(e);
        ret.setMvtRowid(oRowid.getStringValue());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - saveOutputEntity(ctx=" + ctx + ", operationItemKey=" + operationItemKey + ", entity="
                    + entity + ", outputWorkstationParameters=" + outputWorkstationParameters
                    + ", outputItemParameters=" + outputItemParameters + ", labels=" + labels + ")=" + ret);
        }
        return ret;
    }

    /**
     * Sets the poItemSBS.
     * 
     * @param poItemSBS poItemSBS.
     */
    public void setPoItemSBS(final POItemSBS poItemSBS) {
        this.poItemSBS = poItemSBS;
    }

    /**
     * Sets the specificationSBS.
     * 
     * @param specificationSBS specificationSBS.
     */
    public void setSpecificationSBS(final SpecificationSBS specificationSBS) {
        this.specificationSBS = specificationSBS;
    }

    /**
     * Sets the unitSBS.
     * 
     * @param unitSBS unitSBS.
     */
    public void setUnitSBS(final UnitSBS unitSBS) {
        this.unitSBS = unitSBS;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void updateTattooedNb(final VIFContext ctx, final MOKey moKey, final CEntityBean entityBean,
            final String workstationId, final String userId) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - updateTattooedNb(ctx=" + ctx + ", moKey=" + moKey + ", entityBean=" + entityBean
                    + ", workstationId=" + workstationId + ", userId=" + userId + ")");
        }
        BooleanHolder oRet = new BooleanHolder();
        StringHolder oMsg = new StringHolder();
        XxfabrecPPO xxfabrecPPO = new XxfabrecPPO(ctx);
        xxfabrecPPO.prMajNotat(moKey.getEstablishmentKey().getCsoc(), //
                moKey.getEstablishmentKey().getCetab(), //
                moKey.getChrono().getPrechro(), //
                moKey.getChrono().getChrono(), //
                entityBean.getEntityLocationBean().getEntityLocationKey().getContainerNumber(), //
                entityBean.getEntityLocationBean().getContainerPackaging().getPackagingId(), //
                entityBean.getEntityLocationBean().getContainerPackaging().getTattooedId(), //
                workstationId, //
                userId, //
                oRet, //
                oMsg);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMsg.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - updateTattooedNb(ctx=" + ctx + ", moKey=" + moKey + ", entityBean=" + entityBean
                    + ", workstationId=" + workstationId + ", userId=" + userId + ")");
        }
    }

    /**
     * 
     * Build the list of PROXY for criteria.
     * 
     * @param ttCrivPROXYHolder list of Criteria
     * @return list of criteria
     */
    private List<CriteriaCompleteBean> ttHolderToListCriteriaBean(
            final TempTableHolder<XxfabrecPPOTtcrivproxy> ttCrivPROXYHolder) {
        /* TtCriv -> List<CriteriaCompleteBean> */
        List<CriteriaCompleteBean> listCriteriaCompleteBean = new ArrayList<CriteriaCompleteBean>();
        Iterator<XxfabrecPPOTtcrivproxy> iter = ttCrivPROXYHolder.getListValue().iterator();
        while (iter.hasNext()) {
            XxfabrecPPOTtcrivproxy content = iter.next();
            CriteriaCompleteBean oneCriteria = new CriteriaCompleteBean(content.getCsoc(), content.getCetab(),
                    content.getCcri(), content.getType(), content.getCunite(), content.getLcri(), content.getLformat(),
                    content.getRefval(), content.getNlig(), content.getTsaimodif(), content.getTsaioblig(),
                    content.getMsaisie(), content.getValdmin(), content.getValdmax(), content.getValcmin(),
                    content.getValcmax(), content.getValnmin(), content.getValnmax(), content.getTexclus(),
                    content.getCorig(), content.getTsaizero(), content.getTypcle(), content.getCle(),
                    content.getCstade(), content.getInfoStade(), content.getDatem(), content.getHeurem());
            // oneCriteria.setListToDisplay(content.getListToDisplay());
            listCriteriaCompleteBean.add(oneCriteria);
        }
        return listCriteriaCompleteBean;
    }
}

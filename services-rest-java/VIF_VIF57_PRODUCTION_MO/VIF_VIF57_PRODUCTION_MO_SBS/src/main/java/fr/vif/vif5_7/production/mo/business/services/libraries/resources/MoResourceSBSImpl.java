/*
 * Copyright (c) 2009 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MoResourceSBSImpl.java,v $
 * Created on 13 févr. 09 by gv
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.resources;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.common.util.i18n.production.mo.ProductionMo;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.gen.service.business.beans.chrono.Chrono;
import fr.vif.vif5_7.production.mo.business.beans.common.mo.MOKey;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceBean;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceInputTimeBean;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceInputTimeSBean;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceKey;
import fr.vif.vif5_7.production.mo.business.beans.common.moresource.MOResourceSBean;
import fr.vif.vif5_7.production.mo.business.services.libraries.XxfabtPPO;
import fr.vif.vif5_7.production.mo.dao.kotod.Kotod;
import fr.vif.vif5_7.production.mo.dao.kotod.KotodDAOFactory;
import fr.vif.vif5_7.production.mo.dao.kotodd.Kotodd;
import fr.vif.vif5_7.production.mo.dao.kotodd.KotoddDAOFactory;


/**
 * impl.
 * 
 * @author gv
 */
@Component
public class MoResourceSBSImpl implements MoResourceSBS {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(MoResourceSBSImpl.class);

    /**
     * ctor.
     */
    public MoResourceSBSImpl() {
    }

    /**
     * {@inheritDoc}
     */
    public MOResourceBean getMOResourceBean(final VIFContext vifContext, final MOResourceKey moResourceKey)
            throws ServerBusinessException {
        try {
            return kotodToMOResourceBean(KotodDAOFactory.getDAO().getByPK(vifContext.getDbCtx(),
                    moResourceKey.getMoKey().getEstablishmentKey().getCsoc(),
                    moResourceKey.getMoKey().getEstablishmentKey().getCetab(),
                    moResourceKey.getMoKey().getChrono().getPrechro(),
                    moResourceKey.getMoKey().getChrono().getChrono(), moResourceKey.getCounter1(),
                    moResourceKey.getCounter2(), moResourceKey.getCounter3()));
        } catch (DAOException e) {
            throw new ServerBusinessException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    public MOResourceInputTimeBean getMOResourceInputTimeBean(final VIFContext vifContext,
            final MOResourceInputTimeBean moResourceInputTime) throws ServerBusinessException {
        MOResourceKey moResourceKey = moResourceInputTime.getMoResourceKey();
        try {
            return kotoddToMOResourceInputTimeBean(KotoddDAOFactory.getDAO().getByPK(vifContext.getDbCtx(),
                    moResourceKey.getMoKey().getEstablishmentKey().getCsoc(),
                    moResourceKey.getMoKey().getEstablishmentKey().getCsoc(),
                    moResourceKey.getMoKey().getChrono().getPrechro(),
                    moResourceKey.getMoKey().getChrono().getChrono(), moResourceKey.getCounter1(),
                    moResourceKey.getCounter2(), moResourceKey.getCounter3(),
                    moResourceInputTime.getInputKeyComplement()));
        } catch (DAOException e) {
            throw new ServerBusinessException(e);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public List<MOResourceBean> listResources(final VIFContext vifContext, final MOResourceSBean selection)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listResources(vifContext=" + vifContext + ", selection=" + selection + ")");
        }
        List<MOResourceBean> retList = new ArrayList<MOResourceBean>();
        checkSelection(selection);
        if (MOResourceSBean.SelectionOn.MO.equals(selection.getSelectionOn())) {
            try {
                Collections.sort(selection.getLstChrono(), new Comparator<Chrono>() {

                    @Override
                    public int compare(final Chrono o1, final Chrono o2) {
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("B - $Comparator<Chrono>.compare(o1=" + o1 + ", o2=" + o2 + ")");
                        }
                        if (LOGGER.isDebugEnabled()) {
                            LOGGER.debug("E - $Comparator<Chrono>.compare(o1=" + o1 + ", o2=" + o2 + ")");
                        }
                        return (o1.getPrechro().compareTo(o2.getPrechro()));
                    }
                });
                Map<String, List<Integer>> mapChrono = new HashMap<String, List<Integer>>();
                String curPrechro = "";
                List<Integer> curLst = null;
                for (Chrono chrono : selection.getLstChrono()) {
                    if (!curPrechro.equals(chrono.getPrechro())) {
                        curPrechro = chrono.getPrechro();
                        curLst = new ArrayList<Integer>();
                        curLst.add(chrono.getChrono());
                        mapChrono.put(curPrechro, curLst);
                    } else {
                        curLst.add(chrono.getChrono());
                    }
                }
                Iterator<Map.Entry<String, List<Integer>>> it = mapChrono.entrySet().iterator();
                while (it.hasNext()) {
                    Map.Entry<String, List<Integer>> entry = it.next();
                    List<Kotod> lstKotod = KotodDAOFactory.getDAO().listKotodWithListChrono(vifContext.getDbCtx(),
                            selection.getEstablishmentKey(), entry.getKey(), entry.getValue(), null, null);
                    for (Kotod kotod : lstKotod) {
                        retList.add(kotodToMOResourceBean(kotod));
                    }
                }
            } catch (DAOException e) {
                throw new ServerBusinessException(e);
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listResources(vifContext=" + vifContext + ", selection=" + selection + ")");
        }
        return retList;
    }

    /**
     * {@inheritDoc}
     */
    public List<MOResourceInputTimeBean> listTimeInput(final VIFContext vifContext,
            final MOResourceInputTimeSBean selection) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listTimeInput(vifContext=" + vifContext + ", selection=" + selection + ")");
        }
        checkSelection(selection);
        List<Kotodd> lst;
        try {
            switch (selection.getSelectionOn()) {
                case MO_RESOURCE:
                    lst = KotoddDAOFactory.getDAO().listKotoddWithMoResource(vifContext.getDbCtx(),
                            selection.getMoResourceKey());
                    break;
                case MO:
                    lst = KotoddDAOFactory.getDAO().listKotoddWithListChrono(vifContext.getDbCtx(),
                            selection.getEstablishmentKey(), "1OF", chronoToInt(selection.getListChrono()));
                    break;
                case NONE:
                    // TODO
                    lst = new ArrayList<Kotodd>();
                    break;
                default:
                    // nothing todo
                    lst = new ArrayList<Kotodd>();
                    break;
            }
        } catch (DAOException e) {
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listTimeInput(vifContext=" + vifContext + ", selection=" + selection + ")");
        }
        return kotoddToMOResourceInputTimeBean(lst);
    }

    /**
     * {@inheritDoc}
     */
    public MOResourceInputTimeBean saveMOResourceInputTime(final VIFContext vifContext,
            final MOResourceInputTimeBean bean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - saveMOResourceInputTime(vifContext=" + vifContext + ", bean=" + bean + ")");
        }
        Kotodd kotodd = null;
        try {
            if (bean.getInputKeyComplement() == 0) {
                // new
                List<Kotodd> lst = KotoddDAOFactory.getDAO().listKotoddWithMoResource(vifContext.getDbCtx(),
                        bean.getMoResourceKey());
                int ni4 = maxNi4(lst) + 1;
                kotodd = moResourceInputTimeBeanToKotodd(vifContext, bean);
                kotodd.setNi4(ni4);
                kotodd.setAdmstamp(0);
                KotoddDAOFactory.getDAO().insert(vifContext.getDbCtx(), kotodd);
            } else {
                // update
                kotodd = KotoddDAOFactory.getDAO().getByPKForUpdate(vifContext.getDbCtx(),
                        bean.getMoResourceKey().getMoKey().getEstablishmentKey().getCsoc(),
                        bean.getMoResourceKey().getMoKey().getEstablishmentKey().getCetab(),
                        bean.getMoResourceKey().getMoKey().getChrono().getPrechro(),
                        bean.getMoResourceKey().getMoKey().getChrono().getChrono(),
                        bean.getMoResourceKey().getCounter1(), bean.getMoResourceKey().getCounter2(),
                        bean.getMoResourceKey().getCounter3(), bean.getInputKeyComplement(), bean.getAdmstamp());
                kotodd.setCuser(vifContext.getIdCtx().getLogin());
                kotodd.setDatheurdebr(bean.getBeginDateTime());
                kotodd.setDatheurfinr(bean.getEndDateTime());
                if (bean.getEndDateTime() != null && bean.getBeginDateTime() != null) {
                    kotodd.setDuree2((bean.getEndDateTime().getTime() - bean.getBeginDateTime().getTime()) / 1000);
                }
                kotodd.setNbr(bean.getNumber());
                KotoddDAOFactory.getDAO().update(vifContext.getDbCtx(), kotodd);
            }
            if (bean.getEndDateTime() != null) {
                StringHolder oMsg = new StringHolder();
                BooleanHolder oRet = new BooleanHolder();
                String iCsoc = vifContext.getIdCtx().getCompany();
                String iCetab = vifContext.getIdCtx().getEstablishment();
                String iPrechro = bean.getMoResourceKey().getMoKey().getChrono().getPrechro();
                int iChrono = bean.getMoResourceKey().getMoKey().getChrono().getChrono();
                int iNi1 = bean.getMoResourceKey().getCounter1();
                int iNi2 = bean.getMoResourceKey().getCounter2();
                int iNlig = bean.getMoResourceKey().getCounter3();
                int iNi4 = bean.getInputKeyComplement();
                new XxfabtPPO(vifContext).prMajSaisieTemps(iCsoc, iCetab, iPrechro, iChrono, iNi1, iNi2, iNlig, iNi4,
                        oMsg, oRet);
            }
            kotodd = KotoddDAOFactory.getDAO().getByPK(vifContext.getDbCtx(), kotodd.getCsoc(), kotodd.getCetab(),
                    kotodd.getPrechro(), kotodd.getChrono(), kotodd.getNi1(), kotodd.getNi2(), kotodd.getNlig(),
                    kotodd.getNi4());
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("E - saveMOResourceInputTime(vifContext=" + vifContext + ", bean=" + bean + ")");
            }
            return kotoddToMOResourceInputTimeBean(kotodd);
        } catch (DAOException e) {
            throw new ServerBusinessException(e);
        }
    }

    /**
     * 
     * Check the selection bean.
     * 
     * @param selection the selection bean (MOResourceInputTimeSBean)
     * @throws ServerBusinessException if error occurs
     */
    private void checkSelection(final MOResourceInputTimeSBean selection) throws ServerBusinessException {
    }

    /**
     * check the selection.
     * 
     * @param selection s
     * @throws ServerBusinessException e
     */
    private void checkSelection(final MOResourceSBean selection) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - checkSelection(selection=" + selection + ")");
        }
        if (MOResourceSBean.SelectionOn.NONE.equals(selection.getSelectionOn())) {
            throw new ServerBusinessException(ProductionMo.T29623);
        } else if (MOResourceSBean.SelectionOn.MO.equals(selection.getSelectionOn())) {
            if (selection.getLstChrono().size() == 0) {
                throw new ServerBusinessException(ProductionMo.T29623);
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - checkSelection(selection=" + selection + ")");
        }
    }

    /**
     * Convert a chrono list to an integer list.
     * 
     * @param listChrono the list of chrono to convert
     * @return the list of integer
     */
    private List<Integer> chronoToInt(final List<Chrono> listChrono) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - chronoToInt(listChrono=" + listChrono + ")");
        }
        List<Integer> lst = new ArrayList<Integer>(listChrono.size());
        for (Iterator<Chrono> iterator = listChrono.iterator(); iterator.hasNext();) {
            lst.add(new Integer(iterator.next().getChrono()));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - chronoToInt(listChrono=" + listChrono + ")");
        }
        return lst;
    }

    /**
     * 
     * Convert Kotodd to bean.
     * 
     * @param kotodd the Kotodd to convert
     * @return the MOResourceInputTimeBean
     */
    private MOResourceInputTimeBean kotoddToMOResourceInputTimeBean(final Kotodd kotodd) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - kotoddToMOResourceInputTimeBean(kotodd=" + kotodd + ")");
        }
        MOResourceInputTimeBean bean = new MOResourceInputTimeBean();
        MOKey moKey = new MOKey();
        moKey.setChrono(new Chrono(kotodd.getPrechro(), kotodd.getChrono()));
        moKey.setEstablishmentKey(new EstablishmentKey(kotodd.getCsoc(), kotodd.getCetab()));
        MOResourceKey moResourceKey = new MOResourceKey();
        moResourceKey.setCounter1(kotodd.getNi1());
        moResourceKey.setCounter2(kotodd.getNi2());
        moResourceKey.setCounter3(kotodd.getNlig());
        moResourceKey.setMoKey(moKey);
        bean.setMoResourceKey(moResourceKey);
        bean.setNumber(kotodd.getNbr());
        bean.setInputKeyComplement(kotodd.getNi4());
        bean.setBeginDateTime(kotodd.getDatheurdebr());
        bean.setEndDateTime(kotodd.getDatheurfinr());
        bean.setAdmstamp(kotodd.getAdmstamp());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - kotoddToMOResourceInputTimeBean(kotodd=" + kotodd + ")=" + bean);
        }
        return bean;
    }

    /**
     * 
     * Convert a Kotodd list to a bean list.
     * 
     * @param kotodd the list kotodd to convert
     * @return the list of MOResourceInputTimeBean
     */
    private List<MOResourceInputTimeBean> kotoddToMOResourceInputTimeBean(final List<Kotodd> kotodd) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - kotoddToMOResourceInputTimeBean(kotodd=" + kotodd + ")");
        }
        List<MOResourceInputTimeBean> lst = new ArrayList<MOResourceInputTimeBean>(kotodd.size());
        for (Iterator<Kotodd> iterator = kotodd.iterator(); iterator.hasNext();) {
            lst.add(kotoddToMOResourceInputTimeBean(iterator.next()));
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - kotoddToMOResourceInputTimeBean(kotodd=" + kotodd + ")");
        }
        return lst;
    }

    /**
     * convert.
     * 
     * @param kotod k
     * @return b
     */
    private MOResourceBean kotodToMOResourceBean(final Kotod kotod) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - kotodToMOResourceBean(kotod=" + kotod + ")");
        }
        MOResourceBean bean = new MOResourceBean();
        if (kotod != null) {
            bean.setCres(kotod.getCres());
            MOResourceKey moResourceKey = new MOResourceKey();
            MOKey moKey = new MOKey();
            moKey.setChrono(new Chrono(kotod.getPrechro(), kotod.getChrono()));
            moKey.setEstablishmentKey(new EstablishmentKey(kotod.getCsoc(), kotod.getCetab()));
            moResourceKey.setMoKey(moKey);
            moResourceKey.setCounter1(kotod.getNi1());
            moResourceKey.setCounter2(kotod.getNi2());
            moResourceKey.setCounter3(kotod.getNlig());
            bean.setMoResourceKey(moResourceKey);
            bean.setNbFixe(kotod.getNbfix());
            bean.setNbMax(kotod.getNbmax());
            bean.setNbMin(kotod.getNbmin());
            bean.setNbTot(kotod.getNb());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - kotodToMOResourceBean(kotod=" + kotod + ")=" + bean);
        }
        return bean;
    }

    /**
     * calculates the biggest ni4.
     * 
     * @param lst l
     * @return i
     */
    private int maxNi4(final List<Kotodd> lst) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - maxNi4(lst=" + lst + ")");
        }
        int ret = 0;
        for (Kotodd kotodd : lst) {
            if (kotodd.getNi4() > ret) {
                ret = kotodd.getNi4();
            }
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - maxNi4(lst)=" + ret);
        }
        return ret;
    }

    /**
     * Convert bean to Kotodd.
     * 
     * @param vifContext the VIF Context
     * @param bean the MOResourceInputTimeBean to convert
     * @return the kotodd
     */
    private Kotodd moResourceInputTimeBeanToKotodd(final VIFContext vifContext, final MOResourceInputTimeBean bean) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - moResourceInputTimeBeanToKotodd(vifContext=" + vifContext + ", bean=" + bean + ")");
        }
        Kotodd kotodd = new Kotodd();
        kotodd.setCetab(bean.getMoResourceKey().getMoKey().getEstablishmentKey().getCetab());
        kotodd.setCsoc(bean.getMoResourceKey().getMoKey().getEstablishmentKey().getCsoc());
        kotodd.setChrono(bean.getMoResourceKey().getMoKey().getChrono().getChrono());
        kotodd.setPrechro(bean.getMoResourceKey().getMoKey().getChrono().getPrechro());
        kotodd.setNi1(bean.getMoResourceKey().getCounter1());
        kotodd.setNi2(bean.getMoResourceKey().getCounter2());
        kotodd.setNlig(bean.getMoResourceKey().getCounter3());
        kotodd.setNi4(bean.getInputKeyComplement());
        kotodd.setCuser(vifContext.getIdCtx().getLogin());
        kotodd.setDatheurdebr(bean.getBeginDateTime());
        kotodd.setDatheurfinr(bean.getEndDateTime());
        if (bean.getEndDateTime() != null && bean.getBeginDateTime() != null) {
            kotodd.setDuree2((bean.getEndDateTime().getTime() - bean.getBeginDateTime().getTime()) / 1000);
        }
        kotodd.setNbr(bean.getNumber());
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - moResourceInputTimeBeanToKotodd(vifContext=" + vifContext + ", bean=" + bean + ")="
                    + kotodd);
        }
        return kotodd;
    }
}

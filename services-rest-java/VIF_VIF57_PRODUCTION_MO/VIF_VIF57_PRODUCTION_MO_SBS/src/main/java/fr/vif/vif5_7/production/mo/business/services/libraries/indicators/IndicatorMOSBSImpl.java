/*
 * Copyright (c) 2007 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: IndicatorMOSBSImpl.java,v $
 * Created on 22 nov. 07 by glc
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.indicators;


import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import fr.vif.jtech.business.CachedSBSImpl;
import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.util.cache.ICacheLoader;
import fr.vif.jtech.common.beans.CodeLabel;
import fr.vif.jtech.dao.exceptions.DAOException;
import fr.vif.vif5_7.activities.activities.constants.Mnemos.FlowType;
import fr.vif.vif5_7.common.util.i18n.generic.Generic;
import fr.vif.vif5_7.gen.location.business.beans.common.establishment.EstablishmentKey;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMO;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOInfo;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOQties;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOQty;
import fr.vif.vif5_7.production.mo.business.beans.common.indicators.IndicatorMOSelectionBean;
import fr.vif.vif5_7.production.mo.business.beans.common.po.ChronoPO;
import fr.vif.vif5_7.production.mo.business.beans.common.po.POKey;
import fr.vif.vif5_7.production.mo.constants.Mnemos;
import fr.vif.vif5_7.production.mo.dao.kfabind.Kfabind;
import fr.vif.vif5_7.production.mo.dao.kfabind.KfabindDAOFactory;


/**
 * Indicator MO business services implementation.
 * 
 * @author ag
 */
@Component
public class IndicatorMOSBSImpl extends CachedSBSImpl implements IndicatorMOSBS {

    private static final Logger LOGGER        = Logger.getLogger(IndicatorMOSBSImpl.class);

    private static final int    MILLISECONDES = 1000;

    private static final String SECONDES      = "s";

    public IndicatorMOSBSImpl() {
    }

    @Autowired
    public IndicatorMOSBSImpl(@Qualifier("indicatorMOLoader") final ICacheLoader loader) {
        super();
        setLoader(loader);
        setRegionName("fr.vif.vif5_7.production.mo.business.services.libraries.indicators");
    }

    /**
     * 
     * Convert PLC Kfabind into Indicator bean.
     * 
     * @param plcList plc list
     * @return List<Indicator>
     * @throws ServerBusinessException e
     */
    public static List<IndicatorMO> convertPlcToIndicators(final List<Kfabind> plcList) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertPlcToIndicator(plcList=" + plcList + ")");
        }
        List<IndicatorMO> indicators = new ArrayList<IndicatorMO>();
        for (Kfabind p : plcList) {
            IndicatorMO indicator = convertPlcToIndicator(p);
            indicators.add(indicator);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertPlcToIndicator(plcList=" + plcList + ")=" + indicators);
        }
        return indicators;
    }

    /**
     * Convert PLC to IndicatorMO.
     * 
     * @param kfabind Kfabind
     * @return IndicatorMO
     */
    private static IndicatorMO convertPlcToIndicator(final Kfabind kfabind) {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - convertPlcToIndicator(kfabind=" + kfabind + ")");
        }
        // Keys
        POKey poKey = new POKey();
        poKey.setChrono(new ChronoPO(kfabind.getPrechro(), kfabind.getChrono(), kfabind.getNi1()));
        poKey.setEstablishmentKey(new EstablishmentKey(kfabind.getCsoc(), kfabind.getCetab()));
        // Constructor
        IndicatorMO indicator = new IndicatorMO(kfabind.getJourfab(), poKey);
        // Infos
        IndicatorMOInfo infos = new IndicatorMOInfo(Mnemos.POIndicatorState.getPOIndicatorState(kfabind.getCetatot()),
                FlowType.getFlowType(kfabind.getTypflux()));
        infos.setItemCL(new CodeLabel(kfabind.getCart(), kfabind.getLart()));
        infos.setSectorCL(new CodeLabel(kfabind.getCsecteur(), kfabind.getLsecteur()));
        infos.setHchyValueCL(new CodeLabel(kfabind.getCategart(), kfabind.getLcategart()));
        infos.setLineCL(new CodeLabel(kfabind.getLgnfab(), kfabind.getLlgnfab()));
        infos.setTeamCL(new CodeLabel(kfabind.getCequip(), kfabind.getLequip()));
        infos.setExpectedBeginDate(kfabind.getDatheurdeb());
        infos.setExpectedEndDate(kfabind.getDatheurfin());
        infos.setEffectiveBeginDate(kfabind.getDatheurdebr());
        infos.setEffectiveEndDate(kfabind.getDatheurfinr());
        infos.setPriority(kfabind.getPrior());
        indicator.setInfos(infos);
        // Quantities
        IndicatorMOQties quantities = new IndicatorMOQties();
        quantities.setNbProcessOrder(1); // FIXME JD Not the right place to do that ?! But very convenient

        // weight unit
        quantities.setInputQtyWU(new IndicatorMOQty(kfabind.getQterent(), kfabind.getQtepent(), kfabind.getCunite()));
        quantities.setOutputQtyWU(new IndicatorMOQty(kfabind.getQtersor(), kfabind.getQtepsor(), kfabind.getCunite()));
        quantities.setGarbageQtyWU(new IndicatorMOQty(kfabind.getQterdec(), kfabind.getQtepdec(), kfabind.getCunite()));
        // efficiency unit
        quantities.setProcessQtyEU(new IndicatorMOQty(kfabind.getQter(), kfabind.getQtep(), kfabind.getAddeddata1()));
        quantities.setInputQtyEU(new IndicatorMOQty(kfabind.getData4(), kfabind.getData3(), kfabind.getAddeddata1()));
        quantities.setOutputQtyEU(new IndicatorMOQty(kfabind.getData6(), kfabind.getData5(), kfabind.getAddeddata1()));
        // 1st statistics unit
        quantities.setProcessQtySU(new IndicatorMOQty(kfabind.getData2(), kfabind.getData1(), kfabind.getAddeddata2()));
        if (kfabind.getDatheurdeb() != null && kfabind.getDatheurdebr() != null && kfabind.getDatheurfin() != null
                && kfabind.getDatheurfinr() != null) {
            quantities.setTimes(new IndicatorMOQty((kfabind.getDatheurfinr().getTime() - kfabind.getDatheurdebr()
                    .getTime()) / MILLISECONDES,
                    (kfabind.getDatheurfin().getTime() - kfabind.getDatheurdeb().getTime()) / MILLISECONDES, SECONDES));
        }
        indicator.setQties(quantities);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - convertPlcToIndicator(kfabind=" + kfabind + ")=" + indicator);
        }
        return indicator;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public IndicatorMO getIndicatorMO(final VIFContext ctx, final POKey poKey) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listIndicatorBySelection(ctx=" + ctx + ", poKey=" + poKey + ")");
        }
        IndicatorMO ret = new IndicatorMO();
        Kfabind plcs = null;
        try {
            plcs = KfabindDAOFactory.getDAO().get(ctx.getDbCtx(), poKey.getEstablishmentKey().getCsoc(),
                    poKey.getEstablishmentKey().getCetab(), poKey.getChrono().getPrechro(),
                    poKey.getChrono().getChrono(), poKey.getChrono().getCounter1());
            if (plcs == null) {
                LOGGER.error("Value is null (ctx=" + ctx + ", poKey=" + poKey + ")");
                throw new ServerBusinessException(Generic.T5815);
            }
            ret = convertPlcToIndicator(plcs);
        } catch (DAOException e) {
            throw new ServerBusinessException(e);
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listIndicatorBySelection(ctx=" + ctx + ", poKey=" + poKey + ")");
        }
        return ret;
    }

    /**
     * {@inheritDoc}
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<IndicatorMO> listBySelection(final VIFContext ctx, final IndicatorMOSelectionBean selection)
            throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - listIndicatorBySelection(ctx=" + ctx + ", selection=" + selection + ")");
        }
        List<IndicatorMO> ret = new ArrayList<IndicatorMO>();
        ret = (List<IndicatorMO>) getCacheObject(ctx, selection);
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - listIndicatorBySelection(ctx=" + ctx + ", selection=" + selection + ")");
        }
        return ret;
    }
}

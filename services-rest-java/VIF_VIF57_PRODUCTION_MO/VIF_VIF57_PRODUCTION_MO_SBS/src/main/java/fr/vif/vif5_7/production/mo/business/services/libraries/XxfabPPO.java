/*
 * Copyright (c) 2016 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO/VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: XxfabPPO.java,v $
 * Created on 24 mars 2016 by presta15
 */
package fr.vif.vif5_7.production.mo.business.services.libraries;


import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Level;
import org.apache.log4j.Logger;

import com.progress.open4gl.Open4GLException;
import com.progress.open4gl.Parameter;
import com.progress.open4gl.ProResultSet;
import com.progress.open4gl.ProResultSetMetaDataImpl;
import com.progress.open4gl.ProSQLException;
import com.progress.open4gl.javaproxy.ParamArray;
import com.progress.open4gl.javaproxy.ParamArrayMode;

import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProgressBusinessException;
import fr.vif.jtech.business.progress.AbstractPPO;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.DoubleHolder;
import fr.vif.jtech.business.progress.IntegerHolder;
import fr.vif.jtech.business.progress.ProInputTempTable;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.jtech.business.progress.TempTableHolder;


/**
 * Generated class from xxfabpxy.p.
 * 
 * @author presta15
 */
public class XxfabPPO extends AbstractPPO {


    /**
     * Logger.
     */
    private static final Logger LOGGER = Logger.getLogger(XxfabPPO.class);

    /**
     * ProResultSetMetaDataImpl for the temp-table tTLocaldoc.
     */
    private static ProResultSetMetaDataImpl metadatatTLocaldoc = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table tTQualite.
     */
    private static ProResultSetMetaDataImpl metadatatTQualite = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttArtRemp.
     */
    private static ProResultSetMetaDataImpl metadatattArtRemp = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttContenu.
     */
    private static ProResultSetMetaDataImpl metadatattContenu = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttCrivHandle.
     */
    private static ProResultSetMetaDataImpl metadatattCrivHandle = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttListoperations.
     */
    private static ProResultSetMetaDataImpl metadatattListoperations = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttLocaldocprg.
     */
    private static ProResultSetMetaDataImpl metadatattLocaldocprg = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttLot.
     */
    private static ProResultSetMetaDataImpl metadatattLot = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttMotMvk.
     */
    private static ProResultSetMetaDataImpl metadatattMotMvk = null;

    /**
     * ProResultSetMetaDataImpl for the temp-table ttXmvtm.
     */
    private static ProResultSetMetaDataImpl metadatattXmvtm = null;

    /**
     * Context.
     */
    private VIFContext ctx;


    /**
     * Default Constructor.
     *
     * @param ctx Database Context.
     */
    public XxfabPPO(final VIFContext ctx) {
        super();
        this.ctx = ctx;
    }

    /**
     * Generated Class prAnalyseCab.
     *
     * @param iLecture  String INPUT
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param oCart  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prAnalyseCab(final String iLecture, final String iCsoc, final String iCetab, final String iCposte, final StringHolder oCart) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCart == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prAnalyseCab values :" + "oCart = " + oCart);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prAnalyseCab "  + " iLecture = " + iLecture + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " oCart = " + oCart);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);
        
        try {
            params.addCharacter(0, iLecture, ParamArrayMode.INPUT);
            params.addCharacter(1, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(2, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(3, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Analyse-CAB", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCart.setStringValue((String) params.getOutputParameter(4));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prAnalyseCabEntrant.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iCart  String INPUT
     * @param iTypacta  String INPUT
     * @param iLecture  String INPUT
     * @param ttContenuHolder  TABLE OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prAnalyseCabEntrant(final String iCsoc, final String iCetab, final String iCposte, final String iPrechrof, final int iChronof, final String iCart, final String iTypacta, final String iLecture, final TempTableHolder<XxfabPPOTtcontenu> ttContenuHolder) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttContenuHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prAnalyseCabEntrant values :" + "ttContenuHolder = " + ttContenuHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prAnalyseCabEntrant "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iCart = " + iCart + " iTypacta = " + iTypacta + " iLecture = " + iLecture + " ttContenuHolder = " + ttContenuHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(9);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addCharacter(5, iCart, ParamArrayMode.INPUT);
            params.addCharacter(6, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(7, iLecture, ParamArrayMode.INPUT);
            params.addTable(8, null, ParamArrayMode.OUTPUT, getMetaDatattContenu());
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Analyse-CAB-Entrant", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttContenu
        
            ttContenuHolder.setListValue(convertRecordttContenu(params,8));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prChargementFormat.
     *
     * @param iCfonct  String INPUT
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prChargementFormat(final String iCfonct, final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prChargementFormat values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prChargementFormat "  + " iCfonct = " + iCfonct + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(7);
        
        try {
            params.addCharacter(0, iCfonct, ParamArrayMode.INPUT);
            params.addCharacter(1, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(2, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(3, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(4, iCuser, ParamArrayMode.INPUT);
            params.addLogical(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Chargement-Format", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(5));
            oMsg.setStringValue((String) params.getOutputParameter(6));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCriteresof.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  double INPUT
     * @param ttCrivHandleHolder  TABLE OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCriteresof(final String iCsoc, final String iCetab, final String iPrechro, final double iChrono, final TempTableHolder<XxfabPPOTtCrivHandle> ttCrivHandleHolder) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttCrivHandleHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCriteresof values :" + "ttCrivHandleHolder = " + ttCrivHandleHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCriteresof "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " ttCrivHandleHolder = " + ttCrivHandleHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addDecimal(3, new BigDecimal(iChrono).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addTable(4, null, ParamArrayMode.OUTPUT, getMetaDatattCrivHandle());
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-CriteresOF", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttCrivHandle
        
            ttCrivHandleHolder.setListValue(convertRecordttCrivHandle(params,4));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCtrlArtDec.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iCartDec  String INPUT
     * @param iTypacta  String INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCtrlArtDec(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iCartDec, final String iTypacta, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCtrlArtDec values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCtrlArtDec "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iCartDec = " + iCartDec + " iTypacta = " + iTypacta + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iCartDec, ParamArrayMode.INPUT);
            params.addCharacter(12, iTypacta, ParamArrayMode.INPUT);
            params.addLogical(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Ctrl-Art-Dec", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(13));
            oMsg.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCtrlArtRemp.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iCartRemp  String INPUT
     * @param iTypacta  String INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCtrlArtRemp(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iCartRemp, final String iTypacta, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCtrlArtRemp values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCtrlArtRemp "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iCartRemp = " + iCartRemp + " iTypacta = " + iTypacta + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iCartRemp, ParamArrayMode.INPUT);
            params.addCharacter(12, iTypacta, ParamArrayMode.INPUT);
            params.addLogical(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Ctrl-Art-Remp", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(13));
            oMsg.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCtrlDelivLot.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCart  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iTypacta  String INPUT
     * @param ioLot  String INPUT_OUTPUT
     * @param oCodeRetour  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCtrlDelivLot(final String iCsoc, final String iCetab, final String iCposte, final String iCart, final String iCdepot, final String iCemp, final String iPrechrof, final int iChronof, final String iTypacta, final StringHolder ioLot, final StringHolder oCodeRetour) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ioLot == null || oCodeRetour == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCtrlDelivLot values :" + "ioLot = " + ioLot + "oCodeRetour = " + oCodeRetour);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCtrlDelivLot "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCart = " + iCart + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iTypacta = " + iTypacta + " ioLot = " + ioLot + " oCodeRetour = " + oCodeRetour);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(11);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(5, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(6, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addCharacter(8, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(9, ioLot.getStringValue(), ParamArrayMode.INPUT_OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Ctrl-Deliv-Lot", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            ioLot.setStringValue((String) params.getOutputParameter(9));
            oCodeRetour.setStringValue((String) params.getOutputParameter(10));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCtrlDepotCloture.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iCart  String INPUT
     * @param iCdepot  String INPUT
     * @param iTypacta  String INPUT
     * @param iCposte  String INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCtrlDepotCloture(final String iCsoc, final String iCetab, final String iPrechrof, final int iChronof, final String iCart, final String iCdepot, final String iTypacta, final String iCposte, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCtrlDepotCloture values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCtrlDepotCloture "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iCart = " + iCart + " iCdepot = " + iCdepot + " iTypacta = " + iTypacta + " iCposte = " + iCposte + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(10);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addCharacter(4, iCart, ParamArrayMode.INPUT);
            params.addCharacter(5, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(6, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(7, iCposte, ParamArrayMode.INPUT);
            params.addLogical(8, null, ParamArrayMode.OUTPUT);
            params.addCharacter(9, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Ctrl-Depot-Cloture", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(8));
            oMsg.setStringValue((String) params.getOutputParameter(9));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCtrlEcart.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iTypGest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iqte1  double INPUT
     * @param iU1  String INPUT
     * @param iqte2  double INPUT
     * @param iU2  String INPUT
     * @param ibMvtEnBase  boolean INPUT
     * @param iControlType  int INPUT
     * @param oInfo  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCtrlEcart(final String iCsoc, final String iCetab, final String iCposte, final String iTypGest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final double iqte1, final String iU1, final double iqte2, final String iU2, final boolean ibMvtEnBase, final int iControlType, final StringHolder oInfo, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oInfo == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCtrlEcart values :" + "oInfo = " + oInfo + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCtrlEcart "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iTypGest = " + iTypGest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iqte1 = " + iqte1 + " iU1 = " + iU1 + " iqte2 = " + iqte2 + " iU2 = " + iU2 + " ibMvtEnBase = " + ibMvtEnBase + " iControlType = " + iControlType + " oInfo = " + oInfo + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(19);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iTypGest, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addDecimal(10, new BigDecimal(iqte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(11, iU1, ParamArrayMode.INPUT);
            params.addDecimal(12, new BigDecimal(iqte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(13, iU2, ParamArrayMode.INPUT);
            params.addLogical(14, new Boolean(ibMvtEnBase), ParamArrayMode.INPUT);
            params.addInteger(15, new Integer(iControlType), ParamArrayMode.INPUT);
            params.addCharacter(16, null, ParamArrayMode.OUTPUT);
            params.addLogical(17, null, ParamArrayMode.OUTPUT);
            params.addCharacter(18, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Ctrl-Ecart", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oInfo.setStringValue((String) params.getOutputParameter(16));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(17));
            oMsg.setStringValue((String) params.getOutputParameter(18));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCtrlEntree.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param iNsc  String INPUT
     * @param iTypacta  String INPUT
     * @param iCmotmvk  String INPUT
     * @param oTLigTer  boolean OUTPUT
     * @param oQuestionLigTer  String OUTPUT
     * @param oQuestionSplit  String OUTPUT
     * @param oTCtrlStk  boolean OUTPUT
     * @param oLstQuestionCtrlStk  String OUTPUT
     * @param oQuestionDestockCtrl  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCtrlEntree(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iCart, final String iLot, final String iCdepot, final String iCemp, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final String iNsc, final String iTypacta, final String iCmotmvk, final BooleanHolder oTLigTer, final StringHolder oQuestionLigTer, final StringHolder oQuestionSplit, final BooleanHolder oTCtrlStk, final StringHolder oLstQuestionCtrlStk, final StringHolder oQuestionDestockCtrl, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTLigTer == null || oQuestionLigTer == null || oQuestionSplit == null || oTCtrlStk == null || oLstQuestionCtrlStk == null || oQuestionDestockCtrl == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCtrlEntree values :" + "oTLigTer = " + oTLigTer + "oQuestionLigTer = " + oQuestionLigTer + "oQuestionSplit = " + oQuestionSplit + "oTCtrlStk = " + oTCtrlStk + "oLstQuestionCtrlStk = " + oLstQuestionCtrlStk + "oQuestionDestockCtrl = " + oQuestionDestockCtrl + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCtrlEntree "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " iNsc = " + iNsc + " iTypacta = " + iTypacta + " iCmotmvk = " + iCmotmvk + " oTLigTer = " + oTLigTer + " oQuestionLigTer = " + oQuestionLigTer + " oQuestionSplit = " + oQuestionSplit + " oTCtrlStk = " + oTCtrlStk + " oLstQuestionCtrlStk = " + oLstQuestionCtrlStk + " oQuestionDestockCtrl = " + oQuestionDestockCtrl + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(32);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iCart, ParamArrayMode.INPUT);
            params.addCharacter(12, iLot, ParamArrayMode.INPUT);
            params.addCharacter(13, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(14, iCemp, ParamArrayMode.INPUT);
            params.addDecimal(15, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(16, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(17, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(18, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(19, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(20, iCu3, ParamArrayMode.INPUT);
            params.addCharacter(21, iNsc, ParamArrayMode.INPUT);
            params.addCharacter(22, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(23, iCmotmvk, ParamArrayMode.INPUT);
            params.addLogical(24, null, ParamArrayMode.OUTPUT);
            params.addCharacter(25, null, ParamArrayMode.OUTPUT);
            params.addCharacter(26, null, ParamArrayMode.OUTPUT);
            params.addLogical(27, null, ParamArrayMode.OUTPUT);
            params.addCharacter(28, null, ParamArrayMode.OUTPUT);
            params.addCharacter(29, null, ParamArrayMode.OUTPUT);
            params.addLogical(30, null, ParamArrayMode.OUTPUT);
            params.addCharacter(31, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Ctrl-Entree", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTLigTer.setBooleanValue((Boolean) params.getOutputParameter(24));
            oQuestionLigTer.setStringValue((String) params.getOutputParameter(25));
            oQuestionSplit.setStringValue((String) params.getOutputParameter(26));
            oTCtrlStk.setBooleanValue((Boolean) params.getOutputParameter(27));
            oLstQuestionCtrlStk.setStringValue((String) params.getOutputParameter(28));
            oQuestionDestockCtrl.setStringValue((String) params.getOutputParameter(29));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(30));
            oMsg.setStringValue((String) params.getOutputParameter(31));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCtrlSortie.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param iNsc  String INPUT
     * @param iTypacta  String INPUT
     * @param iCmotmvk  String INPUT
     * @param iTypTare  int INPUT
     * @param oTLigTer  boolean OUTPUT
     * @param oQuestionLigTer  String OUTPUT
     * @param oQuestionSplit  String OUTPUT
     * @param oTCtrlStk  boolean OUTPUT
     * @param oLstQuestionCtrlStk  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCtrlSortie(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iCart, final String iLot, final String iCdepot, final String iCemp, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final String iNsc, final String iTypacta, final String iCmotmvk, final int iTypTare, final BooleanHolder oTLigTer, final StringHolder oQuestionLigTer, final StringHolder oQuestionSplit, final BooleanHolder oTCtrlStk, final StringHolder oLstQuestionCtrlStk, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTLigTer == null || oQuestionLigTer == null || oQuestionSplit == null || oTCtrlStk == null || oLstQuestionCtrlStk == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCtrlSortie values :" + "oTLigTer = " + oTLigTer + "oQuestionLigTer = " + oQuestionLigTer + "oQuestionSplit = " + oQuestionSplit + "oTCtrlStk = " + oTCtrlStk + "oLstQuestionCtrlStk = " + oLstQuestionCtrlStk + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCtrlSortie "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " iNsc = " + iNsc + " iTypacta = " + iTypacta + " iCmotmvk = " + iCmotmvk + " iTypTare = " + iTypTare + " oTLigTer = " + oTLigTer + " oQuestionLigTer = " + oQuestionLigTer + " oQuestionSplit = " + oQuestionSplit + " oTCtrlStk = " + oTCtrlStk + " oLstQuestionCtrlStk = " + oLstQuestionCtrlStk + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(32);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iCart, ParamArrayMode.INPUT);
            params.addCharacter(12, iLot, ParamArrayMode.INPUT);
            params.addCharacter(13, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(14, iCemp, ParamArrayMode.INPUT);
            params.addDecimal(15, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(16, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(17, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(18, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(19, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(20, iCu3, ParamArrayMode.INPUT);
            params.addCharacter(21, iNsc, ParamArrayMode.INPUT);
            params.addCharacter(22, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(23, iCmotmvk, ParamArrayMode.INPUT);
            params.addInteger(24, new Integer(iTypTare), ParamArrayMode.INPUT);
            params.addLogical(25, null, ParamArrayMode.OUTPUT);
            params.addCharacter(26, null, ParamArrayMode.OUTPUT);
            params.addCharacter(27, null, ParamArrayMode.OUTPUT);
            params.addLogical(28, null, ParamArrayMode.OUTPUT);
            params.addCharacter(29, null, ParamArrayMode.OUTPUT);
            params.addLogical(30, null, ParamArrayMode.OUTPUT);
            params.addCharacter(31, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Ctrl-Sortie", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTLigTer.setBooleanValue((Boolean) params.getOutputParameter(25));
            oQuestionLigTer.setStringValue((String) params.getOutputParameter(26));
            oQuestionSplit.setStringValue((String) params.getOutputParameter(27));
            oTCtrlStk.setBooleanValue((Boolean) params.getOutputParameter(28));
            oLstQuestionCtrlStk.setStringValue((String) params.getOutputParameter(29));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(30));
            oMsg.setStringValue((String) params.getOutputParameter(31));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prCtrlStock.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iTypacta  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iCmotmvk  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param oCtrlStk  String OUTPUT
     * @param oLstQuestions  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prCtrlStock(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iPrechrof, final int iChronof, final String iTypacta, final String iCart, final String iLot, final String iCdepot, final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final StringHolder oCtrlStk, final StringHolder oLstQuestions, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCtrlStk == null || oLstQuestions == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prCtrlStock values :" + "oCtrlStk = " + oCtrlStk + "oLstQuestions = " + oLstQuestions + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prCtrlStock "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iTypacta = " + iTypacta + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " oCtrlStk = " + oCtrlStk + " oLstQuestions = " + oLstQuestions + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(20);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addCharacter(6, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(7, iCart, ParamArrayMode.INPUT);
            params.addCharacter(8, iLot, ParamArrayMode.INPUT);
            params.addCharacter(9, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(10, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(11, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(12, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(13, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(14, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(15, iCu2, ParamArrayMode.INPUT);
            params.addCharacter(16, null, ParamArrayMode.OUTPUT);
            params.addCharacter(17, null, ParamArrayMode.OUTPUT);
            params.addLogical(18, null, ParamArrayMode.OUTPUT);
            params.addCharacter(19, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Ctrl-Stock", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCtrlStk.setStringValue((String) params.getOutputParameter(16));
            oLstQuestions.setStringValue((String) params.getOutputParameter(17));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(18));
            oMsg.setStringValue((String) params.getOutputParameter(19));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prDeleteLast.
     *
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iRowidXmvtm  String INPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prDeleteLast(final String iCposte, final String iCuser, final String iRowidXmvtm, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prDeleteLast values :" + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prDeleteLast "  + " iCposte = " + iCposte + " iCuser = " + iCuser + " iRowidXmvtm = " + iRowidXmvtm + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);
        
        try {
            params.addCharacter(0, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(1, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(2, iRowidXmvtm, ParamArrayMode.INPUT);
            params.addLogical(3, null, ParamArrayMode.OUTPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Delete-Last", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oRet.setBooleanValue((Boolean) params.getOutputParameter(3));
            oMsg.setStringValue((String) params.getOutputParameter(4));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prDeleteLastNew.
     *
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iNlig  int INPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prDeleteLastNew(final String iCposte, final String iCuser, final String iCsoc, final String iCetab, final String iPrechro, final int iChrono, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final int iNlig, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prDeleteLastNew values :" + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prDeleteLastNew "  + " iCposte = " + iCposte + " iCuser = " + iCuser + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iNlig = " + iNlig + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(13);
        
        try {
            params.addCharacter(0, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(1, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(2, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(3, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNlig), ParamArrayMode.INPUT);
            params.addLogical(11, null, ParamArrayMode.OUTPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Delete-Last-New", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oRet.setBooleanValue((Boolean) params.getOutputParameter(11));
            oMsg.setStringValue((String) params.getOutputParameter(12));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prDeleteMvt.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iRowidXmvtm  String INPUT
     * @param iTypgestXpiloia  String INPUT
     * @param iPrechroXpiloia  String INPUT
     * @param iChronoXpiloia  int INPUT
     * @param iNi1Xpiloia  int INPUT
     * @param iNi2Xpiloia  int INPUT
     * @param iNi3Xpiloia  int INPUT
     * @param iNi4Xpiloia  int INPUT
     * @param iTrnscsa  boolean INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prDeleteMvt(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iRowidXmvtm, final String iTypgestXpiloia, final String iPrechroXpiloia, final int iChronoXpiloia, final int iNi1Xpiloia, final int iNi2Xpiloia, final int iNi3Xpiloia, final int iNi4Xpiloia, final boolean iTrnscsa, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prDeleteMvt values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prDeleteMvt "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iRowidXmvtm = " + iRowidXmvtm + " iTypgestXpiloia = " + iTypgestXpiloia + " iPrechroXpiloia = " + iPrechroXpiloia + " iChronoXpiloia = " + iChronoXpiloia + " iNi1Xpiloia = " + iNi1Xpiloia + " iNi2Xpiloia = " + iNi2Xpiloia + " iNi3Xpiloia = " + iNi3Xpiloia + " iNi4Xpiloia = " + iNi4Xpiloia + " iTrnscsa = " + iTrnscsa + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iRowidXmvtm, ParamArrayMode.INPUT);
            params.addCharacter(5, iTypgestXpiloia, ParamArrayMode.INPUT);
            params.addCharacter(6, iPrechroXpiloia, ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iChronoXpiloia), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi1Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi2Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi3Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(11, new Integer(iNi4Xpiloia), ParamArrayMode.INPUT);
            params.addLogical(12, new Boolean(iTrnscsa), ParamArrayMode.INPUT);
            params.addLogical(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Delete-Mvt", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(13));
            oMsg.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prDeleteMvtEntree.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgestXpiloia  String INPUT
     * @param iPrechroXpiloia  String INPUT
     * @param iChronoXpiloia  int INPUT
     * @param iNi1Xpiloia  int INPUT
     * @param iNi2Xpiloia  int INPUT
     * @param iNi3Xpiloia  int INPUT
     * @param iNi4Xpiloia  int INPUT
     * @param iNlig  int INPUT
     * @param iTrnscsa  boolean INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prDeleteMvtEntree(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgestXpiloia, final String iPrechroXpiloia, final int iChronoXpiloia, final int iNi1Xpiloia, final int iNi2Xpiloia, final int iNi3Xpiloia, final int iNi4Xpiloia, final int iNlig, final boolean iTrnscsa, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prDeleteMvtEntree values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prDeleteMvtEntree "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgestXpiloia = " + iTypgestXpiloia + " iPrechroXpiloia = " + iPrechroXpiloia + " iChronoXpiloia = " + iChronoXpiloia + " iNi1Xpiloia = " + iNi1Xpiloia + " iNi2Xpiloia = " + iNi2Xpiloia + " iNi3Xpiloia = " + iNi3Xpiloia + " iNi4Xpiloia = " + iNi4Xpiloia + " iNlig = " + iNlig + " iTrnscsa = " + iTrnscsa + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgestXpiloia, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechroXpiloia, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronoXpiloia), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(11, new Integer(iNlig), ParamArrayMode.INPUT);
            params.addLogical(12, new Boolean(iTrnscsa), ParamArrayMode.INPUT);
            params.addLogical(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Delete-Mvt-Entree", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(13));
            oMsg.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prDeleteMvtSortie.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgestXpiloia  String INPUT
     * @param iPrechroXpiloia  String INPUT
     * @param iChronoXpiloia  int INPUT
     * @param iNi1Xpiloia  int INPUT
     * @param iNi2Xpiloia  int INPUT
     * @param iNi3Xpiloia  int INPUT
     * @param iNi4Xpiloia  int INPUT
     * @param iNlig  int INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prDeleteMvtSortie(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgestXpiloia, final String iPrechroXpiloia, final int iChronoXpiloia, final int iNi1Xpiloia, final int iNi2Xpiloia, final int iNi3Xpiloia, final int iNi4Xpiloia, final int iNlig, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prDeleteMvtSortie values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prDeleteMvtSortie "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgestXpiloia = " + iTypgestXpiloia + " iPrechroXpiloia = " + iPrechroXpiloia + " iChronoXpiloia = " + iChronoXpiloia + " iNi1Xpiloia = " + iNi1Xpiloia + " iNi2Xpiloia = " + iNi2Xpiloia + " iNi3Xpiloia = " + iNi3Xpiloia + " iNi4Xpiloia = " + iNi4Xpiloia + " iNlig = " + iNlig + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(14);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgestXpiloia, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechroXpiloia, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronoXpiloia), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4Xpiloia), ParamArrayMode.INPUT);
            params.addInteger(11, new Integer(iNlig), ParamArrayMode.INPUT);
            params.addLogical(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Delete-Mvt-Sortie", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(12));
            oMsg.setStringValue((String) params.getOutputParameter(13));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prDepotArticle.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCart  String INPUT
     * @param oCdepot  String OUTPUT
     * @param oLdepot  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prDepotArticle(final String iCsoc, final String iCetab, final String iCart, final StringHolder oCdepot, final StringHolder oLdepot, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCdepot == null || oLdepot == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prDepotArticle values :" + "oCdepot = " + oCdepot + "oLdepot = " + oLdepot + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prDepotArticle "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCart = " + iCart + " oCdepot = " + oCdepot + " oLdepot = " + oLdepot + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(7);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCart, ParamArrayMode.INPUT);
            params.addCharacter(3, null, ParamArrayMode.OUTPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addLogical(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Depot-Article", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCdepot.setStringValue((String) params.getOutputParameter(3));
            oLdepot.setStringValue((String) params.getOutputParameter(4));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(5));
            oMsg.setStringValue((String) params.getOutputParameter(6));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prDequittancementLigne.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prDequittancementLigne(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prDequittancementLigne values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prDequittancementLigne "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(12);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addLogical(10, null, ParamArrayMode.OUTPUT);
            params.addCharacter(11, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Dequittancement-Ligne", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(10));
            oMsg.setStringValue((String) params.getOutputParameter(11));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prEditeEtiquette.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iRowid  String INPUT
     * @param iTypacta  String INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prEditeEtiquette(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iRowid, final String iTypacta, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prEditeEtiquette values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prEditeEtiquette "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iRowid = " + iRowid + " iTypacta = " + iTypacta + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(8);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iRowid, ParamArrayMode.INPUT);
            params.addCharacter(5, iTypacta, ParamArrayMode.INPUT);
            params.addLogical(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Edite-Etiquette", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(6));
            oMsg.setStringValue((String) params.getOutputParameter(7));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prEditeEtiquetteActe.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @param iTypacta  String INPUT
     * @param iTypEvt  String INPUT
     * @param iCpf  String INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prEditeEtiquetteActe(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iPrechro, final int iChrono, final String iTypacta, final String iTypEvt, final String iCpf, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prEditeEtiquetteActe values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prEditeEtiquetteActe "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iTypacta = " + iTypacta + " iTypEvt = " + iTypEvt + " iCpf = " + iCpf + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(11);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addCharacter(6, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(7, iTypEvt, ParamArrayMode.INPUT);
            params.addCharacter(8, iCpf, ParamArrayMode.INPUT);
            params.addLogical(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Edite-Etiquette-Acte", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(9));
            oMsg.setStringValue((String) params.getOutputParameter(10));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prEditeEtiquettesEntreeSortie.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iCart  String INPUT
     * @param iRowidMvt  String INPUT
     * @param iTypacta  String INPUT
     * @param ttLocaldocprgHolder  TABLE OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prEditeEtiquettesEntreeSortie(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iPrechrof, final int iChronof, final String iCart, final String iRowidMvt, final String iTypacta, final TempTableHolder<XxfabPPOTtLocaldocprg> ttLocaldocprgHolder, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocprgHolder == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prEditeEtiquettesEntreeSortie values :" + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prEditeEtiquettesEntreeSortie "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iCart = " + iCart + " iRowidMvt = " + iRowidMvt + " iTypacta = " + iTypacta + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(12);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addCharacter(6, iCart, ParamArrayMode.INPUT);
            params.addCharacter(7, iRowidMvt, ParamArrayMode.INPUT);
            params.addCharacter(8, iTypacta, ParamArrayMode.INPUT);
            params.addTable(9, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addLogical(10, null, ParamArrayMode.OUTPUT);
            params.addCharacter(11, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Edite-Etiquettes-Entree-Sortie", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttLocaldocprg
        
            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params,9));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(10));
            oMsg.setStringValue((String) params.getOutputParameter(11));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGestionfabevt.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @param iNi1  int INPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGestionfabevt(final String iCsoc, final String iCetab, final String iPrechro, final int iChrono, final int iNi1) throws ProgressBusinessException {
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGestionfabevt "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iNi1), ParamArrayMode.INPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-GestionFabEvt", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGetInputParameters.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCart  String INPUT
     * @param oCpfent  String OUTPUT
     * @param oEntityType  int OUTPUT
     * @param oCu1  String OUTPUT
     * @param oCu2  String OUTPUT
     * @param oCu3  String OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGetInputParameters(final String iCsoc, final String iCetab, final String iCposte, final String iCart, final StringHolder oCpfent, final IntegerHolder oEntityType, final StringHolder oCu1, final StringHolder oCu2, final StringHolder oCu3, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCpfent == null || oEntityType == null || oCu1 == null || oCu2 == null || oCu3 == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prGetInputParameters values :" + "oCpfent = " + oCpfent + "oEntityType = " + oEntityType + "oCu1 = " + oCu1 + "oCu2 = " + oCu2 + "oCu3 = " + oCu3 + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGetInputParameters "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCart = " + iCart + " oCpfent = " + oCpfent + " oEntityType = " + oEntityType + " oCu1 = " + oCu1 + " oCu2 = " + oCu2 + " oCu3 = " + oCu3 + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(11);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addInteger(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);
            params.addLogical(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Get-Input-Parameters", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCpfent.setStringValue((String) params.getOutputParameter(4));
            oEntityType.setIntegerValue((Integer) params.getOutputParameter(5));
            oCu1.setStringValue((String) params.getOutputParameter(6));
            oCu2.setStringValue((String) params.getOutputParameter(7));
            oCu3.setStringValue((String) params.getOutputParameter(8));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(9));
            oMsg.setStringValue((String) params.getOutputParameter(10));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGetInputParameters2.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCart  String INPUT
     * @param oCpfent  String OUTPUT
     * @param oEntityType  int OUTPUT
     * @param oQte1  double OUTPUT
     * @param oCu1  String OUTPUT
     * @param oCu2  String OUTPUT
     * @param oCu3  String OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGetInputParameters2(final String iCsoc, final String iCetab, final String iCposte, final String iCart, final StringHolder oCpfent, final IntegerHolder oEntityType, final DoubleHolder oQte1, final StringHolder oCu1, final StringHolder oCu2, final StringHolder oCu3, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCpfent == null || oEntityType == null || oQte1 == null || oCu1 == null || oCu2 == null || oCu3 == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prGetInputParameters2 values :" + "oCpfent = " + oCpfent + "oEntityType = " + oEntityType + "oQte1 = " + oQte1 + "oCu1 = " + oCu1 + "oCu2 = " + oCu2 + "oCu3 = " + oCu3 + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGetInputParameters2 "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCart = " + iCart + " oCpfent = " + oCpfent + " oEntityType = " + oEntityType + " oQte1 = " + oQte1 + " oCu1 = " + oCu1 + " oCu2 = " + oCu2 + " oCu3 = " + oCu3 + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(12);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addInteger(5, null, ParamArrayMode.OUTPUT);
            params.addDecimal(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);
            params.addCharacter(9, null, ParamArrayMode.OUTPUT);
            params.addLogical(10, null, ParamArrayMode.OUTPUT);
            params.addCharacter(11, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Get-Input-Parameters2", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCpfent.setStringValue((String) params.getOutputParameter(4));
            oEntityType.setIntegerValue((Integer) params.getOutputParameter(5));
            oQte1.setDoubleValue(((BigDecimal) params.getOutputParameter(6)).doubleValue());
            oCu1.setStringValue((String) params.getOutputParameter(7));
            oCu2.setStringValue((String) params.getOutputParameter(8));
            oCu3.setStringValue((String) params.getOutputParameter(9));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(10));
            oMsg.setStringValue((String) params.getOutputParameter(11));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGetInputParameters3.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCart  String INPUT
     * @param oCpfent  String OUTPUT
     * @param oLpfent  String OUTPUT
     * @param oEntityType  int OUTPUT
     * @param oQte1  double OUTPUT
     * @param oCu1  String OUTPUT
     * @param oCu2  String OUTPUT
     * @param oCu3  String OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGetInputParameters3(final String iCsoc, final String iCetab, final String iCposte, final String iCart, final StringHolder oCpfent, final StringHolder oLpfent, final IntegerHolder oEntityType, final DoubleHolder oQte1, final StringHolder oCu1, final StringHolder oCu2, final StringHolder oCu3, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCpfent == null || oLpfent == null || oEntityType == null || oQte1 == null || oCu1 == null || oCu2 == null || oCu3 == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prGetInputParameters3 values :" + "oCpfent = " + oCpfent + "oLpfent = " + oLpfent + "oEntityType = " + oEntityType + "oQte1 = " + oQte1 + "oCu1 = " + oCu1 + "oCu2 = " + oCu2 + "oCu3 = " + oCu3 + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGetInputParameters3 "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCart = " + iCart + " oCpfent = " + oCpfent + " oLpfent = " + oLpfent + " oEntityType = " + oEntityType + " oQte1 = " + oQte1 + " oCu1 = " + oCu1 + " oCu2 = " + oCu2 + " oCu3 = " + oCu3 + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(13);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addCharacter(5, null, ParamArrayMode.OUTPUT);
            params.addInteger(6, null, ParamArrayMode.OUTPUT);
            params.addDecimal(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);
            params.addCharacter(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);
            params.addLogical(11, null, ParamArrayMode.OUTPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Get-Input-Parameters3", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCpfent.setStringValue((String) params.getOutputParameter(4));
            oLpfent.setStringValue((String) params.getOutputParameter(5));
            oEntityType.setIntegerValue((Integer) params.getOutputParameter(6));
            oQte1.setDoubleValue(((BigDecimal) params.getOutputParameter(7)).doubleValue());
            oCu1.setStringValue((String) params.getOutputParameter(8));
            oCu2.setStringValue((String) params.getOutputParameter(9));
            oCu3.setStringValue((String) params.getOutputParameter(10));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(11));
            oMsg.setStringValue((String) params.getOutputParameter(12));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGetInputParameters4.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCart  String INPUT
     * @param oCpfent  String OUTPUT
     * @param oLpfent  String OUTPUT
     * @param oEntityType  int OUTPUT
     * @param oQte1  double OUTPUT
     * @param oCu1  String OUTPUT
     * @param oCu2  String OUTPUT
     * @param oCu3  String OUTPUT
     * @param oInterpretQty  boolean OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGetInputParameters4(final String iCsoc, final String iCetab, final String iCposte, final String iCart, final StringHolder oCpfent, final StringHolder oLpfent, final IntegerHolder oEntityType, final DoubleHolder oQte1, final StringHolder oCu1, final StringHolder oCu2, final StringHolder oCu3, final BooleanHolder oInterpretQty, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCpfent == null || oLpfent == null || oEntityType == null || oQte1 == null || oCu1 == null || oCu2 == null || oCu3 == null || oInterpretQty == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prGetInputParameters4 values :" + "oCpfent = " + oCpfent + "oLpfent = " + oLpfent + "oEntityType = " + oEntityType + "oQte1 = " + oQte1 + "oCu1 = " + oCu1 + "oCu2 = " + oCu2 + "oCu3 = " + oCu3 + "oInterpretQty = " + oInterpretQty + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGetInputParameters4 "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCart = " + iCart + " oCpfent = " + oCpfent + " oLpfent = " + oLpfent + " oEntityType = " + oEntityType + " oQte1 = " + oQte1 + " oCu1 = " + oCu1 + " oCu2 = " + oCu2 + " oCu3 = " + oCu3 + " oInterpretQty = " + oInterpretQty + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(14);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addCharacter(5, null, ParamArrayMode.OUTPUT);
            params.addInteger(6, null, ParamArrayMode.OUTPUT);
            params.addDecimal(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);
            params.addCharacter(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);
            params.addLogical(11, null, ParamArrayMode.OUTPUT);
            params.addLogical(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Get-Input-Parameters4", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCpfent.setStringValue((String) params.getOutputParameter(4));
            oLpfent.setStringValue((String) params.getOutputParameter(5));
            oEntityType.setIntegerValue((Integer) params.getOutputParameter(6));
            oQte1.setDoubleValue(((BigDecimal) params.getOutputParameter(7)).doubleValue());
            oCu1.setStringValue((String) params.getOutputParameter(8));
            oCu2.setStringValue((String) params.getOutputParameter(9));
            oCu3.setStringValue((String) params.getOutputParameter(10));
            oInterpretQty.setBooleanValue((Boolean) params.getOutputParameter(11));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(12));
            oMsg.setStringValue((String) params.getOutputParameter(13));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGetOutputParameters.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCart  String INPUT
     * @param oCpfsor  String OUTPUT
     * @param oEntityType  int OUTPUT
     * @param oCCond  String OUTPUT
     * @param oCcondp  String OUTPUT
     * @param oTypFerm  int OUTPUT
     * @param oTypFermCapa  int OUTPUT
     * @param oTypFermp  int OUTPUT
     * @param oTypFermpCapa  int OUTPUT
     * @param oCu1  String OUTPUT
     * @param oCu2  String OUTPUT
     * @param oCu3  String OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGetOutputParameters(final String iCsoc, final String iCetab, final String iCposte, final String iCart, final StringHolder oCpfsor, final IntegerHolder oEntityType, final StringHolder oCCond, final StringHolder oCcondp, final IntegerHolder oTypFerm, final IntegerHolder oTypFermCapa, final IntegerHolder oTypFermp, final IntegerHolder oTypFermpCapa, final StringHolder oCu1, final StringHolder oCu2, final StringHolder oCu3, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCpfsor == null || oEntityType == null || oCCond == null || oCcondp == null || oTypFerm == null || oTypFermCapa == null || oTypFermp == null || oTypFermpCapa == null || oCu1 == null || oCu2 == null || oCu3 == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prGetOutputParameters values :" + "oCpfsor = " + oCpfsor + "oEntityType = " + oEntityType + "oCCond = " + oCCond + "oCcondp = " + oCcondp + "oTypFerm = " + oTypFerm + "oTypFermCapa = " + oTypFermCapa + "oTypFermp = " + oTypFermp + "oTypFermpCapa = " + oTypFermpCapa + "oCu1 = " + oCu1 + "oCu2 = " + oCu2 + "oCu3 = " + oCu3 + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGetOutputParameters "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCart = " + iCart + " oCpfsor = " + oCpfsor + " oEntityType = " + oEntityType + " oCCond = " + oCCond + " oCcondp = " + oCcondp + " oTypFerm = " + oTypFerm + " oTypFermCapa = " + oTypFermCapa + " oTypFermp = " + oTypFermp + " oTypFermpCapa = " + oTypFermpCapa + " oCu1 = " + oCu1 + " oCu2 = " + oCu2 + " oCu3 = " + oCu3 + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(17);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addInteger(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
            params.addInteger(8, null, ParamArrayMode.OUTPUT);
            params.addInteger(9, null, ParamArrayMode.OUTPUT);
            params.addInteger(10, null, ParamArrayMode.OUTPUT);
            params.addInteger(11, null, ParamArrayMode.OUTPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);
            params.addLogical(15, null, ParamArrayMode.OUTPUT);
            params.addCharacter(16, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Get-Output-Parameters", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCpfsor.setStringValue((String) params.getOutputParameter(4));
            oEntityType.setIntegerValue((Integer) params.getOutputParameter(5));
            oCCond.setStringValue((String) params.getOutputParameter(6));
            oCcondp.setStringValue((String) params.getOutputParameter(7));
            oTypFerm.setIntegerValue((Integer) params.getOutputParameter(8));
            oTypFermCapa.setIntegerValue((Integer) params.getOutputParameter(9));
            oTypFermp.setIntegerValue((Integer) params.getOutputParameter(10));
            oTypFermpCapa.setIntegerValue((Integer) params.getOutputParameter(11));
            oCu1.setStringValue((String) params.getOutputParameter(12));
            oCu2.setStringValue((String) params.getOutputParameter(13));
            oCu3.setStringValue((String) params.getOutputParameter(14));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(15));
            oMsg.setStringValue((String) params.getOutputParameter(16));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGetOutputParameters2.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCart  String INPUT
     * @param oCpfsor  String OUTPUT
     * @param oEntityType  int OUTPUT
     * @param oCCond  String OUTPUT
     * @param oCcondp  String OUTPUT
     * @param oTypFerm  int OUTPUT
     * @param oTypFermCapa  int OUTPUT
     * @param oTypFermp  int OUTPUT
     * @param oTypFermpCapa  int OUTPUT
     * @param oQte1  double OUTPUT
     * @param oCu1  String OUTPUT
     * @param oQte2  double OUTPUT
     * @param oCu2  String OUTPUT
     * @param oCdepot  String OUTPUT
     * @param oCemp  String OUTPUT
     * @param oBatch  String OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGetOutputParameters2(final String iCsoc, final String iCetab, final String iCposte, final String iCart, final StringHolder oCpfsor, final IntegerHolder oEntityType, final StringHolder oCCond, final StringHolder oCcondp, final IntegerHolder oTypFerm, final IntegerHolder oTypFermCapa, final IntegerHolder oTypFermp, final IntegerHolder oTypFermpCapa, final DoubleHolder oQte1, final StringHolder oCu1, final DoubleHolder oQte2, final StringHolder oCu2, final StringHolder oCdepot, final StringHolder oCemp, final StringHolder oBatch, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCpfsor == null || oEntityType == null || oCCond == null || oCcondp == null || oTypFerm == null || oTypFermCapa == null || oTypFermp == null || oTypFermpCapa == null || oQte1 == null || oCu1 == null || oQte2 == null || oCu2 == null || oCdepot == null || oCemp == null || oBatch == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prGetOutputParameters2 values :" + "oCpfsor = " + oCpfsor + "oEntityType = " + oEntityType + "oCCond = " + oCCond + "oCcondp = " + oCcondp + "oTypFerm = " + oTypFerm + "oTypFermCapa = " + oTypFermCapa + "oTypFermp = " + oTypFermp + "oTypFermpCapa = " + oTypFermpCapa + "oQte1 = " + oQte1 + "oCu1 = " + oCu1 + "oQte2 = " + oQte2 + "oCu2 = " + oCu2 + "oCdepot = " + oCdepot + "oCemp = " + oCemp + "oBatch = " + oBatch + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGetOutputParameters2 "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCart = " + iCart + " oCpfsor = " + oCpfsor + " oEntityType = " + oEntityType + " oCCond = " + oCCond + " oCcondp = " + oCcondp + " oTypFerm = " + oTypFerm + " oTypFermCapa = " + oTypFermCapa + " oTypFermp = " + oTypFermp + " oTypFermpCapa = " + oTypFermpCapa + " oQte1 = " + oQte1 + " oCu1 = " + oCu1 + " oQte2 = " + oQte2 + " oCu2 = " + oCu2 + " oCdepot = " + oCdepot + " oCemp = " + oCemp + " oBatch = " + oBatch + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(21);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addInteger(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
            params.addInteger(8, null, ParamArrayMode.OUTPUT);
            params.addInteger(9, null, ParamArrayMode.OUTPUT);
            params.addInteger(10, null, ParamArrayMode.OUTPUT);
            params.addInteger(11, null, ParamArrayMode.OUTPUT);
            params.addDecimal(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
            params.addDecimal(14, null, ParamArrayMode.OUTPUT);
            params.addCharacter(15, null, ParamArrayMode.OUTPUT);
            params.addCharacter(16, null, ParamArrayMode.OUTPUT);
            params.addCharacter(17, null, ParamArrayMode.OUTPUT);
            params.addCharacter(18, null, ParamArrayMode.OUTPUT);
            params.addLogical(19, null, ParamArrayMode.OUTPUT);
            params.addCharacter(20, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Get-Output-Parameters2", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCpfsor.setStringValue((String) params.getOutputParameter(4));
            oEntityType.setIntegerValue((Integer) params.getOutputParameter(5));
            oCCond.setStringValue((String) params.getOutputParameter(6));
            oCcondp.setStringValue((String) params.getOutputParameter(7));
            oTypFerm.setIntegerValue((Integer) params.getOutputParameter(8));
            oTypFermCapa.setIntegerValue((Integer) params.getOutputParameter(9));
            oTypFermp.setIntegerValue((Integer) params.getOutputParameter(10));
            oTypFermpCapa.setIntegerValue((Integer) params.getOutputParameter(11));
            oQte1.setDoubleValue(((BigDecimal) params.getOutputParameter(12)).doubleValue());
            oCu1.setStringValue((String) params.getOutputParameter(13));
            oQte2.setDoubleValue(((BigDecimal) params.getOutputParameter(14)).doubleValue());
            oCu2.setStringValue((String) params.getOutputParameter(15));
            oCdepot.setStringValue((String) params.getOutputParameter(16));
            oCemp.setStringValue((String) params.getOutputParameter(17));
            oBatch.setStringValue((String) params.getOutputParameter(18));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(19));
            oMsg.setStringValue((String) params.getOutputParameter(20));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prGetOutputParameters3.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCart  String INPUT
     * @param oCpfsor  String OUTPUT
     * @param oEntityType  int OUTPUT
     * @param oCCond  String OUTPUT
     * @param oCcondp  String OUTPUT
     * @param oTypFerm  int OUTPUT
     * @param oTypFermCapa  int OUTPUT
     * @param oTypFermp  int OUTPUT
     * @param oTypFermpCapa  int OUTPUT
     * @param oQte1  double OUTPUT
     * @param oCu1  String OUTPUT
     * @param oQte2  double OUTPUT
     * @param oCu2  String OUTPUT
     * @param oCdepot  String OUTPUT
     * @param oCemp  String OUTPUT
     * @param oBatch  String OUTPUT
     * @param oLpfsor  String OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prGetOutputParameters3(final String iCsoc, final String iCetab, final String iCposte, final String iCart, final StringHolder oCpfsor, final IntegerHolder oEntityType, final StringHolder oCCond, final StringHolder oCcondp, final IntegerHolder oTypFerm, final IntegerHolder oTypFermCapa, final IntegerHolder oTypFermp, final IntegerHolder oTypFermpCapa, final DoubleHolder oQte1, final StringHolder oCu1, final DoubleHolder oQte2, final StringHolder oCu2, final StringHolder oCdepot, final StringHolder oCemp, final StringHolder oBatch, final StringHolder oLpfsor, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCpfsor == null || oEntityType == null || oCCond == null || oCcondp == null || oTypFerm == null || oTypFermCapa == null || oTypFermp == null || oTypFermpCapa == null || oQte1 == null || oCu1 == null || oQte2 == null || oCu2 == null || oCdepot == null || oCemp == null || oBatch == null || oLpfsor == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prGetOutputParameters3 values :" + "oCpfsor = " + oCpfsor + "oEntityType = " + oEntityType + "oCCond = " + oCCond + "oCcondp = " + oCcondp + "oTypFerm = " + oTypFerm + "oTypFermCapa = " + oTypFermCapa + "oTypFermp = " + oTypFermp + "oTypFermpCapa = " + oTypFermpCapa + "oQte1 = " + oQte1 + "oCu1 = " + oCu1 + "oQte2 = " + oQte2 + "oCu2 = " + oCu2 + "oCdepot = " + oCdepot + "oCemp = " + oCemp + "oBatch = " + oBatch + "oLpfsor = " + oLpfsor + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prGetOutputParameters3 "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCart = " + iCart + " oCpfsor = " + oCpfsor + " oEntityType = " + oEntityType + " oCCond = " + oCCond + " oCcondp = " + oCcondp + " oTypFerm = " + oTypFerm + " oTypFermCapa = " + oTypFermCapa + " oTypFermp = " + oTypFermp + " oTypFermpCapa = " + oTypFermpCapa + " oQte1 = " + oQte1 + " oCu1 = " + oCu1 + " oQte2 = " + oQte2 + " oCu2 = " + oCu2 + " oCdepot = " + oCdepot + " oCemp = " + oCemp + " oBatch = " + oBatch + " oLpfsor = " + oLpfsor + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(22);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addInteger(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
            params.addInteger(8, null, ParamArrayMode.OUTPUT);
            params.addInteger(9, null, ParamArrayMode.OUTPUT);
            params.addInteger(10, null, ParamArrayMode.OUTPUT);
            params.addInteger(11, null, ParamArrayMode.OUTPUT);
            params.addDecimal(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
            params.addDecimal(14, null, ParamArrayMode.OUTPUT);
            params.addCharacter(15, null, ParamArrayMode.OUTPUT);
            params.addCharacter(16, null, ParamArrayMode.OUTPUT);
            params.addCharacter(17, null, ParamArrayMode.OUTPUT);
            params.addCharacter(18, null, ParamArrayMode.OUTPUT);
            params.addCharacter(19, null, ParamArrayMode.OUTPUT);
            params.addLogical(20, null, ParamArrayMode.OUTPUT);
            params.addCharacter(21, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Get-Output-Parameters3", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCpfsor.setStringValue((String) params.getOutputParameter(4));
            oEntityType.setIntegerValue((Integer) params.getOutputParameter(5));
            oCCond.setStringValue((String) params.getOutputParameter(6));
            oCcondp.setStringValue((String) params.getOutputParameter(7));
            oTypFerm.setIntegerValue((Integer) params.getOutputParameter(8));
            oTypFermCapa.setIntegerValue((Integer) params.getOutputParameter(9));
            oTypFermp.setIntegerValue((Integer) params.getOutputParameter(10));
            oTypFermpCapa.setIntegerValue((Integer) params.getOutputParameter(11));
            oQte1.setDoubleValue(((BigDecimal) params.getOutputParameter(12)).doubleValue());
            oCu1.setStringValue((String) params.getOutputParameter(13));
            oQte2.setDoubleValue(((BigDecimal) params.getOutputParameter(14)).doubleValue());
            oCu2.setStringValue((String) params.getOutputParameter(15));
            oCdepot.setStringValue((String) params.getOutputParameter(16));
            oCemp.setStringValue((String) params.getOutputParameter(17));
            oBatch.setStringValue((String) params.getOutputParameter(18));
            oLpfsor.setStringValue((String) params.getOutputParameter(19));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(20));
            oMsg.setStringValue((String) params.getOutputParameter(21));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prInitQteOf.
     *
     * @param iCsoc  String INPUT
     * @param iCart  String INPUT
     * @param iQteRestante  double INPUT
     * @param iCuRestante  String INPUT
     * @param iCu1  String INPUT
     * @param iCu2  String INPUT
     * @param iCu3  String INPUT
     * @param oQte1  double OUTPUT
     * @param oCu1  String OUTPUT
     * @param oQte2  double OUTPUT
     * @param oCu2  String OUTPUT
     * @param oQte3  double OUTPUT
     * @param oCu3  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prInitQteOf(final String iCsoc, final String iCart, final double iQteRestante, final String iCuRestante, final String iCu1, final String iCu2, final String iCu3, final DoubleHolder oQte1, final StringHolder oCu1, final DoubleHolder oQte2, final StringHolder oCu2, final DoubleHolder oQte3, final StringHolder oCu3, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oQte1 == null || oCu1 == null || oQte2 == null || oCu2 == null || oQte3 == null || oCu3 == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prInitQteOf values :" + "oQte1 = " + oQte1 + "oCu1 = " + oCu1 + "oQte2 = " + oQte2 + "oCu2 = " + oCu2 + "oQte3 = " + oQte3 + "oCu3 = " + oCu3 + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prInitQteOf "  + " iCsoc = " + iCsoc + " iCart = " + iCart + " iQteRestante = " + iQteRestante + " iCuRestante = " + iCuRestante + " iCu1 = " + iCu1 + " iCu2 = " + iCu2 + " iCu3 = " + iCu3 + " oQte1 = " + oQte1 + " oCu1 = " + oCu1 + " oQte2 = " + oQte2 + " oCu2 = " + oCu2 + " oQte3 = " + oQte3 + " oCu3 = " + oCu3 + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCart, ParamArrayMode.INPUT);
            params.addDecimal(2, new BigDecimal(iQteRestante).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(3, iCuRestante, ParamArrayMode.INPUT);
            params.addCharacter(4, iCu1, ParamArrayMode.INPUT);
            params.addCharacter(5, iCu2, ParamArrayMode.INPUT);
            params.addCharacter(6, iCu3, ParamArrayMode.INPUT);
            params.addDecimal(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);
            params.addDecimal(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);
            params.addDecimal(11, null, ParamArrayMode.OUTPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);
            params.addLogical(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Init-Qte-OF", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oQte1.setDoubleValue(((BigDecimal) params.getOutputParameter(7)).doubleValue());
            oCu1.setStringValue((String) params.getOutputParameter(8));
            oQte2.setDoubleValue(((BigDecimal) params.getOutputParameter(9)).doubleValue());
            oCu2.setStringValue((String) params.getOutputParameter(10));
            oQte3.setDoubleValue(((BigDecimal) params.getOutputParameter(11)).doubleValue());
            oCu3.setStringValue((String) params.getOutputParameter(12));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(13));
            oMsg.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prInitQteStklot.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param oQte1  double OUTPUT
     * @param oCu1  String OUTPUT
     * @param oQte2  double OUTPUT
     * @param oCu2  String OUTPUT
     * @param oQte3  double OUTPUT
     * @param oCu3  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prInitQteStklot(final String iCsoc, final String iCetab, final String iCart, final String iLot, final String iCdepot, final String iCemp, final DoubleHolder oQte1, final StringHolder oCu1, final DoubleHolder oQte2, final StringHolder oCu2, final DoubleHolder oQte3, final StringHolder oCu3) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oQte1 == null || oCu1 == null || oQte2 == null || oCu2 == null || oQte3 == null || oCu3 == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prInitQteStklot values :" + "oQte1 = " + oQte1 + "oCu1 = " + oCu1 + "oQte2 = " + oQte2 + "oCu2 = " + oCu2 + "oQte3 = " + oQte3 + "oCu3 = " + oCu3);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prInitQteStklot "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " oQte1 = " + oQte1 + " oCu1 = " + oCu1 + " oQte2 = " + oQte2 + " oCu2 = " + oCu2 + " oQte3 = " + oQte3 + " oCu3 = " + oCu3);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(12);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCart, ParamArrayMode.INPUT);
            params.addCharacter(3, iLot, ParamArrayMode.INPUT);
            params.addCharacter(4, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(5, iCemp, ParamArrayMode.INPUT);
            params.addDecimal(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
            params.addDecimal(8, null, ParamArrayMode.OUTPUT);
            params.addCharacter(9, null, ParamArrayMode.OUTPUT);
            params.addDecimal(10, null, ParamArrayMode.OUTPUT);
            params.addCharacter(11, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Init-Qte-StkLot", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oQte1.setDoubleValue(((BigDecimal) params.getOutputParameter(6)).doubleValue());
            oCu1.setStringValue((String) params.getOutputParameter(7));
            oQte2.setDoubleValue(((BigDecimal) params.getOutputParameter(8)).doubleValue());
            oCu2.setStringValue((String) params.getOutputParameter(9));
            oQte3.setDoubleValue(((BigDecimal) params.getOutputParameter(10)).doubleValue());
            oCu3.setStringValue((String) params.getOutputParameter(11));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prLanceEditeEtiquette.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iRowid  String INPUT
     * @param iTypacta  String INPUT
     * @param iCdoc  String INPUT
     * @param iPrgedi  String INPUT
     * @param iNbdoc  int INPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prLanceEditeEtiquette(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iRowid, final String iTypacta, final String iCdoc, final String iPrgedi, final int iNbdoc, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prLanceEditeEtiquette values :" + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prLanceEditeEtiquette "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iRowid = " + iRowid + " iTypacta = " + iTypacta + " iCdoc = " + iCdoc + " iPrgedi = " + iPrgedi + " iNbdoc = " + iNbdoc + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(11);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iRowid, ParamArrayMode.INPUT);
            params.addCharacter(5, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(6, iCdoc, ParamArrayMode.INPUT);
            params.addCharacter(7, iPrgedi, ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNbdoc), ParamArrayMode.INPUT);
            params.addLogical(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Lance-Edite-Etiquette", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oRet.setBooleanValue((Boolean) params.getOutputParameter(9));
            oMsg.setStringValue((String) params.getOutputParameter(10));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prLanceEditeEtiquetteActe.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @param iTypacta  String INPUT
     * @param iCdoc  String INPUT
     * @param iPrgedi  String INPUT
     * @param iNbdoc  int INPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prLanceEditeEtiquetteActe(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iPrechro, final int iChrono, final String iTypacta, final String iCdoc, final String iPrgedi, final int iNbdoc, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prLanceEditeEtiquetteActe values :" + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prLanceEditeEtiquetteActe "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iTypacta = " + iTypacta + " iCdoc = " + iCdoc + " iPrgedi = " + iPrgedi + " iNbdoc = " + iNbdoc + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(12);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addCharacter(6, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(7, iCdoc, ParamArrayMode.INPUT);
            params.addCharacter(8, iPrgedi, ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNbdoc), ParamArrayMode.INPUT);
            params.addLogical(10, null, ParamArrayMode.OUTPUT);
            params.addCharacter(11, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Lance-Edite-Etiquette-Acte", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oRet.setBooleanValue((Boolean) params.getOutputParameter(10));
            oMsg.setStringValue((String) params.getOutputParameter(11));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prListeArticleRemplacement.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iTypacta  String INPUT
     * @param ttArtRempHolder  TABLE OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prListeArticleRemplacement(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final TempTableHolder<XxfabPPOTtartremp> ttArtRempHolder, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttArtRempHolder == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prListeArticleRemplacement values :" + "ttArtRempHolder = " + ttArtRempHolder + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prListeArticleRemplacement "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " ttArtRempHolder = " + ttArtRempHolder + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addTable(12, null, ParamArrayMode.OUTPUT, getMetaDatattArtRemp());
            params.addLogical(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Liste-Article-Remplacement", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttArtRemp
        
            ttArtRempHolder.setListValue(convertRecordttArtRemp(params,12));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(13));
            oMsg.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prListeLotsOf.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iTypacta  String INPUT
     * @param iCart  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param ttLotHolder  TABLE OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prListeLotsOf(final String iCsoc, final String iCetab, final String iPrechrof, final int iChronof, final int iNi1, final String iTypacta, final String iCart, final String iCdepot, final String iCemp, final TempTableHolder<XxfabPPOTtlot> ttLotHolder) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLotHolder == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prListeLotsOf values :" + "ttLotHolder = " + ttLotHolder);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prListeLotsOf "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iTypacta = " + iTypacta + " iCart = " + iCart + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " ttLotHolder = " + ttLotHolder);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(10);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(4, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addCharacter(5, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(6, iCart, ParamArrayMode.INPUT);
            params.addCharacter(7, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(8, iCemp, ParamArrayMode.INPUT);
            params.addTable(9, null, ParamArrayMode.OUTPUT, getMetaDatattLot());
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Liste-Lots-OF", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttLot
        
            ttLotHolder.setListValue(convertRecordttLot(params,9));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prListeMotifsParNature.
     *
     * @param iCsoc  String INPUT
     * @param iCnatmot  String INPUT
     * @param ttMotmvkHolder  TABLE OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prListeMotifsParNature(final String iCsoc, final String iCnatmot, final TempTableHolder<XxfabPPOTtmotmvk> ttMotmvkHolder, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttMotmvkHolder == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prListeMotifsParNature values :" + "ttMotmvkHolder = " + ttMotmvkHolder + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prListeMotifsParNature "  + " iCsoc = " + iCsoc + " iCnatmot = " + iCnatmot + " ttMotmvkHolder = " + ttMotmvkHolder + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(5);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCnatmot, ParamArrayMode.INPUT);
            params.addTable(2, null, ParamArrayMode.OUTPUT, getMetaDatattMotMvk());
            params.addLogical(3, null, ParamArrayMode.OUTPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Liste-Motifs-Par-Nature", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttMotmvk
        
            ttMotmvkHolder.setListValue(convertRecordttMotMvk(params,2));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(3));
            oMsg.setStringValue((String) params.getOutputParameter(4));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prListeMvts.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param ttXmvtmHolder  TABLE OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prListeMvts(final String iCsoc, final String iCetab, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final TempTableHolder<XxfabPPOTtxmvtm> ttXmvtmHolder, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttXmvtmHolder == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prListeMvts values :" + "ttXmvtmHolder = " + ttXmvtmHolder + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prListeMvts "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " ttXmvtmHolder = " + ttXmvtmHolder + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(13);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(3, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addTable(10, null, ParamArrayMode.OUTPUT, getMetaDatattXmvtm());
            params.addLogical(11, null, ParamArrayMode.OUTPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Liste-Mvts", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttXmvtm
        
            ttXmvtmHolder.setListValue(convertRecordttXmvtm(params,10));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(11));
            oMsg.setStringValue((String) params.getOutputParameter(12));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prPrintMovement.
     *
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iNlig  int INPUT
     * @param ttLocaldoc  TABLE INPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prPrintMovement(final String iCposte, final String iCuser, final String iCsoc, final String iCetab, final String iPrechro, final int iChrono, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final int iNlig, final List<XxfabPPOTtLocaldoc> ttLocaldoc, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prPrintMovement values :" + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prPrintMovement "  + " iCposte = " + iCposte + " iCuser = " + iCuser + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iNlig = " + iNlig + " ttLocaldoc = " + ttLocaldoc + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(14);
        
        try {
            params.addCharacter(0, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(1, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(2, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(3, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNlig), ParamArrayMode.INPUT);
            // ------------------------------------------
            // Make the temp-table ttLocaldoc
            // ------------------------------------------
        
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttLocaldoc");
            }
            params.addTable(11, convertTempTabletTLocaldoc(ttLocaldoc), ParamArrayMode.INPUT, getMetaDatatTLocaldoc());
            params.addLogical(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Print-Movement", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oRet.setBooleanValue((Boolean) params.getOutputParameter(12));
            oMsg.setStringValue((String) params.getOutputParameter(13));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prPrintfindMovement.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iNlig  int INPUT
     * @param ttLocaldocHolder  TABLE OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prPrintfindMovement(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iPrechro, final int iChrono, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final int iNlig, final TempTableHolder<XxfabPPOTtLocaldoc> ttLocaldocHolder, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocHolder == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prPrintfindMovement values :" + "ttLocaldocHolder = " + ttLocaldocHolder + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prPrintfindMovement "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iNlig = " + iNlig + " ttLocaldocHolder = " + ttLocaldocHolder + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(14);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNlig), ParamArrayMode.INPUT);
            params.addTable(11, null, ParamArrayMode.OUTPUT, getMetaDatatTLocaldoc());
            params.addLogical(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-PrintFind-Movement", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttLocaldoc
        
            ttLocaldocHolder.setListValue(convertRecordtTLocaldoc(params,11));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(12));
            oMsg.setStringValue((String) params.getOutputParameter(13));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prQuittancementLigne.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypGest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iTypacta  String INPUT
     * @param oInfo  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prQuittancementLigne(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypGest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final StringHolder oInfo, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oInfo == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prQuittancementLigne values :" + "oInfo = " + oInfo + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prQuittancementLigne "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypGest = " + iTypGest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " oInfo = " + oInfo + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypGest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);
            params.addLogical(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Quittancement-Ligne", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oInfo.setStringValue((String) params.getOutputParameter(12));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(13));
            oMsg.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prQuittancementmarquage.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prQuittancementmarquage(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iPrechro, final int iChrono) throws ProgressBusinessException {
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prQuittancementmarquage "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iPrechro = " + iPrechro + " iChrono = " + iChrono);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(6);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChrono), ParamArrayMode.INPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-QuittancementMarquage", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prQuittancementof.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @param oInfo  String OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prQuittancementof(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iPrechro, final int iChrono, final StringHolder oInfo, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oInfo == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prQuittancementof values :" + "oInfo = " + oInfo + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prQuittancementof "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " oInfo = " + oInfo + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(9);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);
            params.addLogical(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-QuittancementOF", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oInfo.setStringValue((String) params.getOutputParameter(6));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(7));
            oMsg.setStringValue((String) params.getOutputParameter(8));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prQuittancementTout.
     *
     * @param ttListoperations TABLE INPUT
     * @param iCposte String INPUT
     * @param iCuser String INPUT
     * @param iTypacta String INPUT
     * @param oInfo String OUTPUT
     * @param oTret boolean OUTPUT
     * @param oMsg String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prQuittancementTout(final List<XxfabPPOTtListoperations> ttListoperations, final String iCposte,
            final String iCuser, final String iTypacta, final StringHolder oInfo, final BooleanHolder oTret,
            final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oInfo == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prQuittancementTout values :" + "oInfo = " + oInfo + "oTret = " + oTret
                        + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prQuittancementTout " + " ttListoperations = " + ttListoperations + " iCposte = "
                    + iCposte + " iCuser = " + iCuser + " iTypacta = " + iTypacta + " oInfo = " + oInfo + " oTret = "
                    + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------

        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(7);

        try {
            // ------------------------------------------
            // Make the temp-table ttListoperations
            // ------------------------------------------

            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table ttListoperations");
            }
            params.addTable(0, convertTempTablettListoperations(ttListoperations), ParamArrayMode.INPUT,
                    getMetaDatattListoperations());
            params.addCharacter(1, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(2, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(3, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(4, null, ParamArrayMode.OUTPUT);
            params.addLogical(5, null, ParamArrayMode.OUTPUT);
            params.addCharacter(6, null, ParamArrayMode.OUTPUT);

            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Quittancement-Tout", params);

            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */

            oInfo.setStringValue((String) params.getOutputParameter(4));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(5));
            oMsg.setStringValue((String) params.getOutputParameter(6));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prRechLotDestockage.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iLot  String INPUT
     * @param oLotPrevu  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prRechLotDestockage(final String iCsoc, final String iCetab, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iCdepot, final String iCemp, final String iLot, final StringHolder oLotPrevu, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oLotPrevu == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prRechLotDestockage values :" + "oLotPrevu = " + oLotPrevu + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prRechLotDestockage "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iLot = " + iLot + " oLotPrevu = " + oLotPrevu + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(16);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(3, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(4, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(5, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(10, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(11, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(12, iLot, ParamArrayMode.INPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
            params.addLogical(14, null, ParamArrayMode.OUTPUT);
            params.addCharacter(15, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Rech-Lot-Destockage", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oLotPrevu.setStringValue((String) params.getOutputParameter(13));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(14));
            oMsg.setStringValue((String) params.getOutputParameter(15));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prRechLotPremierEntrant.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param oLot  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prRechLotPremierEntrant(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final StringHolder oLot, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oLot == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prRechLotPremierEntrant values :" + "oLot = " + oLot + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prRechLotPremierEntrant "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " oLot = " + oLot + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(14);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, null, ParamArrayMode.OUTPUT);
            params.addLogical(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Rech-Lot-Premier-Entrant", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oLot.setStringValue((String) params.getOutputParameter(11));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(12));
            oMsg.setStringValue((String) params.getOutputParameter(13));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prRechercheEditeEtiquette.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iRowid  String INPUT
     * @param iTypacta  String INPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @param oTNbDoc  boolean OUTPUT
     * @param oCdoc  String OUTPUT
     * @param oPrgedi  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prRechercheEditeEtiquette(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iRowid, final String iTypacta, final BooleanHolder oRet, final StringHolder oMsg, final BooleanHolder oTNbDoc, final StringHolder oCdoc, final StringHolder oPrgedi) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null || oTNbDoc == null || oCdoc == null || oPrgedi == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prRechercheEditeEtiquette values :" + "oRet = " + oRet + "oMsg = " + oMsg + "oTNbDoc = " + oTNbDoc + "oCdoc = " + oCdoc + "oPrgedi = " + oPrgedi);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prRechercheEditeEtiquette "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iRowid = " + iRowid + " iTypacta = " + iTypacta + " oRet = " + oRet + " oMsg = " + oMsg + " oTNbDoc = " + oTNbDoc + " oCdoc = " + oCdoc + " oPrgedi = " + oPrgedi);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(11);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iRowid, ParamArrayMode.INPUT);
            params.addCharacter(5, iTypacta, ParamArrayMode.INPUT);
            params.addLogical(6, null, ParamArrayMode.OUTPUT);
            params.addCharacter(7, null, ParamArrayMode.OUTPUT);
            params.addLogical(8, null, ParamArrayMode.OUTPUT);
            params.addCharacter(9, null, ParamArrayMode.OUTPUT);
            params.addCharacter(10, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Recherche-Edite-Etiquette", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oRet.setBooleanValue((Boolean) params.getOutputParameter(6));
            oMsg.setStringValue((String) params.getOutputParameter(7));
            oTNbDoc.setBooleanValue((Boolean) params.getOutputParameter(8));
            oCdoc.setStringValue((String) params.getOutputParameter(9));
            oPrgedi.setStringValue((String) params.getOutputParameter(10));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prRechercheEditeEtiquetteActe.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypacta  String INPUT
     * @param iCevtDoc  String INPUT
     * @param iCpf  String INPUT
     * @param iRowId  String INPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @param oTNbDoc  boolean OUTPUT
     * @param oCdoc  String OUTPUT
     * @param oPrgedi  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prRechercheEditeEtiquetteActe(final String iCsoc, final String iCetab, final String iPrechro, final int iChrono, final String iCposte, final String iCuser, final String iTypacta, final String iCevtDoc, final String iCpf, final String iRowId, final BooleanHolder oRet, final StringHolder oMsg, final BooleanHolder oTNbDoc, final StringHolder oCdoc, final StringHolder oPrgedi) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRet == null || oMsg == null || oTNbDoc == null || oCdoc == null || oPrgedi == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prRechercheEditeEtiquetteActe values :" + "oRet = " + oRet + "oMsg = " + oMsg + "oTNbDoc = " + oTNbDoc + "oCdoc = " + oCdoc + "oPrgedi = " + oPrgedi);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prRechercheEditeEtiquetteActe "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypacta = " + iTypacta + " iCevtDoc = " + iCevtDoc + " iCpf = " + iCpf + " iRowId = " + iRowId + " oRet = " + oRet + " oMsg = " + oMsg + " oTNbDoc = " + oTNbDoc + " oCdoc = " + oCdoc + " oPrgedi = " + oPrgedi);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(15);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addCharacter(4, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(5, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(6, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(7, iCevtDoc, ParamArrayMode.INPUT);
            params.addCharacter(8, iCpf, ParamArrayMode.INPUT);
            params.addCharacter(9, iRowId, ParamArrayMode.INPUT);
            params.addLogical(10, null, ParamArrayMode.OUTPUT);
            params.addCharacter(11, null, ParamArrayMode.OUTPUT);
            params.addLogical(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
            params.addCharacter(14, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Recherche-Edite-Etiquette-Acte", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oRet.setBooleanValue((Boolean) params.getOutputParameter(10));
            oMsg.setStringValue((String) params.getOutputParameter(11));
            oTNbDoc.setBooleanValue((Boolean) params.getOutputParameter(12));
            oCdoc.setStringValue((String) params.getOutputParameter(13));
            oPrgedi.setStringValue((String) params.getOutputParameter(14));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prRechercheEditeMultiEtiquette.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iRowid  String INPUT
     * @param iTypacta  String INPUT
     * @param ttLocaldocprgHolder  TABLE OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prRechercheEditeMultiEtiquette(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iRowid, final String iTypacta, final TempTableHolder<XxfabPPOTtLocaldocprg> ttLocaldocprgHolder, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocprgHolder == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prRechercheEditeMultiEtiquette values :" + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prRechercheEditeMultiEtiquette "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iRowid = " + iRowid + " iTypacta = " + iTypacta + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(9);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iRowid, ParamArrayMode.INPUT);
            params.addCharacter(5, iTypacta, ParamArrayMode.INPUT);
            params.addTable(6, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addLogical(7, null, ParamArrayMode.OUTPUT);
            params.addCharacter(8, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Recherche-Edite-Multi-Etiquette", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttLocaldocprg
        
            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params,6));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(7));
            oMsg.setStringValue((String) params.getOutputParameter(8));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prRechercheEditeMultiEtiquetteActe.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iPrechro  String INPUT
     * @param iChrono  int INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypacta  String INPUT
     * @param iCevtDoc  String INPUT
     * @param iCpf  String INPUT
     * @param iRowId  String INPUT
     * @param ttLocaldocprgHolder  TABLE OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prRechercheEditeMultiEtiquetteActe(final String iCsoc, final String iCetab, final String iPrechro, final int iChrono, final String iCposte, final String iCuser, final String iTypacta, final String iCevtDoc, final String iCpf, final String iRowId, final TempTableHolder<XxfabPPOTtLocaldocprg> ttLocaldocprgHolder, final BooleanHolder oRet, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocprgHolder == null || oRet == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prRechercheEditeMultiEtiquetteActe values :" + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oRet = " + oRet + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prRechercheEditeMultiEtiquetteActe "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iPrechro = " + iPrechro + " iChrono = " + iChrono + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypacta = " + iTypacta + " iCevtDoc = " + iCevtDoc + " iCpf = " + iCpf + " iRowId = " + iRowId + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRet = " + oRet + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(13);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iPrechro, ParamArrayMode.INPUT);
            params.addInteger(3, new Integer(iChrono), ParamArrayMode.INPUT);
            params.addCharacter(4, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(5, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(6, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(7, iCevtDoc, ParamArrayMode.INPUT);
            params.addCharacter(8, iCpf, ParamArrayMode.INPUT);
            params.addCharacter(9, iRowId, ParamArrayMode.INPUT);
            params.addTable(10, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addLogical(11, null, ParamArrayMode.OUTPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Recherche-Edite-Multi-Etiquette-Acte", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttLocaldocprg
        
            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params,10));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(11));
            oMsg.setStringValue((String) params.getOutputParameter(12));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prRechercheEmpOptimise.
     *
     * @param iMode  String INPUT
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iCcond  String INPUT
     * @param iCleCrifa  String INPUT
     * @param iInfoActe  String INPUT
     * @param iCleActe  String INPUT
     * @param tTQualite  TABLE INPUT
     * @param oCemp  String OUTPUT
     * @param oNsc  String OUTPUT
     * @param oRet  boolean OUTPUT
     * @param oMess  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prRechercheEmpOptimise(final String iMode, final String iCsoc, final String iCetab, final String iCart, final String iLot, final String iCdepot, final String iCemp, final String iCcond, final String iCleCrifa, final String iInfoActe, final String iCleActe, final List<XxfabPPOTtqualite> tTQualite, final StringHolder oCemp, final StringHolder oNsc, final BooleanHolder oRet, final StringHolder oMess) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oCemp == null || oNsc == null || oRet == null || oMess == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prRechercheEmpOptimise values :" + "oCemp = " + oCemp + "oNsc = " + oNsc + "oRet = " + oRet + "oMess = " + oMess);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prRechercheEmpOptimise "  + " iMode = " + iMode + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCcond = " + iCcond + " iCleCrifa = " + iCleCrifa + " iInfoActe = " + iInfoActe + " iCleActe = " + iCleActe + " tTQualite = " + tTQualite + " oCemp = " + oCemp + " oNsc = " + oNsc + " oRet = " + oRet + " oMess = " + oMess);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(16);
        
        try {
            params.addCharacter(0, iMode, ParamArrayMode.INPUT);
            params.addCharacter(1, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(2, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addCharacter(4, iLot, ParamArrayMode.INPUT);
            params.addCharacter(5, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(6, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(7, iCcond, ParamArrayMode.INPUT);
            params.addCharacter(8, iCleCrifa, ParamArrayMode.INPUT);
            params.addCharacter(9, iInfoActe, ParamArrayMode.INPUT);
            params.addCharacter(10, iCleActe, ParamArrayMode.INPUT);
            // ------------------------------------------
            // Make the temp-table tTQualite
            // ------------------------------------------
        
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug("Make the input temp-table tTQualite");
            }
            params.addTable(11, convertTempTabletTQualite(tTQualite), ParamArrayMode.INPUT, getMetaDatatTQualite());
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);
            params.addCharacter(13, null, ParamArrayMode.OUTPUT);
            params.addLogical(14, null, ParamArrayMode.OUTPUT);
            params.addCharacter(15, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Recherche-Emp-Optimise", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oCemp.setStringValue((String) params.getOutputParameter(12));
            oNsc.setStringValue((String) params.getOutputParameter(13));
            oRet.setBooleanValue((Boolean) params.getOutputParameter(14));
            oMess.setStringValue((String) params.getOutputParameter(15));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prRemonteeAdmin.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prRemonteeAdmin(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prRemonteeAdmin values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prRemonteeAdmin "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(13);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addLogical(11, null, ParamArrayMode.OUTPUT);
            params.addCharacter(12, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Remontee-Admin", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(11));
            oMsg.setStringValue((String) params.getOutputParameter(12));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prTestExistenceLot.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iLot  String INPUT
     * @param iCart  String INPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prTestExistenceLot(final String iCsoc, final String iCetab, final String iLot, final String iCart, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prTestExistenceLot values :" + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prTestExistenceLot "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iLot = " + iLot + " iCart = " + iCart + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(6);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iLot, ParamArrayMode.INPUT);
            params.addCharacter(3, iCart, ParamArrayMode.INPUT);
            params.addLogical(4, null, ParamArrayMode.OUTPUT);
            params.addCharacter(5, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Test-Existence-Lot", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oTret.setBooleanValue((Boolean) params.getOutputParameter(4));
            oMsg.setStringValue((String) params.getOutputParameter(5));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prTestSaisieQtes.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iCart  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param iNsc  String INPUT
     * @param iTypacta  String INPUT
     * @param oLigneTerminee  boolean OUTPUT
     * @param oQuestion  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prTestSaisieQtes(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iCart, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final String iNsc, final String iTypacta, final BooleanHolder oLigneTerminee, final StringHolder oQuestion, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oLigneTerminee == null || oQuestion == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prTestSaisieQtes values :" + "oLigneTerminee = " + oLigneTerminee + "oQuestion = " + oQuestion + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prTestSaisieQtes "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iCart = " + iCart + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " iNsc = " + iNsc + " iTypacta = " + iTypacta + " oLigneTerminee = " + oLigneTerminee + " oQuestion = " + oQuestion + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(24);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iCart, ParamArrayMode.INPUT);
            params.addDecimal(12, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(13, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(14, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(15, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(16, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(17, iCu3, ParamArrayMode.INPUT);
            params.addCharacter(18, iNsc, ParamArrayMode.INPUT);
            params.addCharacter(19, iTypacta, ParamArrayMode.INPUT);
            params.addLogical(20, null, ParamArrayMode.OUTPUT);
            params.addCharacter(21, null, ParamArrayMode.OUTPUT);
            params.addLogical(22, null, ParamArrayMode.OUTPUT);
            params.addCharacter(23, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Test-Saisie-Qtes", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oLigneTerminee.setBooleanValue((Boolean) params.getOutputParameter(20));
            oQuestion.setStringValue((String) params.getOutputParameter(21));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(22));
            oMsg.setStringValue((String) params.getOutputParameter(23));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prValidateMvt.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iTypacta  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iCmotmvk  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param oRowidMvt  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prValidateMvt(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final String iCart, final String iLot, final String iCdepot, final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final StringHolder oRowidMvt, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRowidMvt == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prValidateMvt values :" + "oRowidMvt = " + oRowidMvt + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prValidateMvt "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " oRowidMvt = " + oRowidMvt + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(26);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, iCart, ParamArrayMode.INPUT);
            params.addCharacter(13, iLot, ParamArrayMode.INPUT);
            params.addCharacter(14, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(15, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(16, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(17, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(18, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(19, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(20, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(21, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(22, iCu3, ParamArrayMode.INPUT);
            params.addCharacter(23, null, ParamArrayMode.OUTPUT);
            params.addLogical(24, null, ParamArrayMode.OUTPUT);
            params.addCharacter(25, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Validate-Mvt", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oRowidMvt.setStringValue((String) params.getOutputParameter(23));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(24));
            oMsg.setStringValue((String) params.getOutputParameter(25));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prValidateMvtComplete.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iTypacta  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iCmotmvk  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param oRowidMvt  String OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prValidateMvtComplete(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final String iCart, final String iLot, final String iCdepot, final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final StringHolder oRowidMvt, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (oRowidMvt == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prValidateMvtComplete values :" + "oRowidMvt = " + oRowidMvt + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prValidateMvtComplete "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " oRowidMvt = " + oRowidMvt + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(26);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, iCart, ParamArrayMode.INPUT);
            params.addCharacter(13, iLot, ParamArrayMode.INPUT);
            params.addCharacter(14, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(15, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(16, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(17, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(18, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(19, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(20, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(21, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(22, iCu3, ParamArrayMode.INPUT);
            params.addCharacter(23, null, ParamArrayMode.OUTPUT);
            params.addLogical(24, null, ParamArrayMode.OUTPUT);
            params.addCharacter(25, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Validate-Mvt-Complete", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            oRowidMvt.setStringValue((String) params.getOutputParameter(23));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(24));
            oMsg.setStringValue((String) params.getOutputParameter(25));
            /* CHECKSTYLE:ON */
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prValidateMvtEntree.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iTypacta  String INPUT
     * @param iNsc  String INPUT
     * @param iCcond  String INPUT
     * @param iNscp  String INPUT
     * @param iCcondp  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iCmotmvk  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param iTypTare  int INPUT
     * @param ttLocaldocprgHolder  TABLE OUTPUT
     * @param oRowidMvt  String OUTPUT
     * @param oNlig  int OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prValidateMvtEntree(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final String iNsc, final String iCcond, final String iNscp, final String iCcondp, final String iCart, final String iLot, final String iCdepot, final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final int iTypTare, final TempTableHolder<XxfabPPOTtLocaldocprg> ttLocaldocprgHolder, final StringHolder oRowidMvt, final IntegerHolder oNlig, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocprgHolder == null || oRowidMvt == null || oNlig == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prValidateMvtEntree values :" + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oRowidMvt = " + oRowidMvt + "oNlig = " + oNlig + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prValidateMvtEntree "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " iNsc = " + iNsc + " iCcond = " + iCcond + " iNscp = " + iNscp + " iCcondp = " + iCcondp + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " iTypTare = " + iTypTare + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRowidMvt = " + oRowidMvt + " oNlig = " + oNlig + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(33);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, iNsc, ParamArrayMode.INPUT);
            params.addCharacter(13, iCcond, ParamArrayMode.INPUT);
            params.addCharacter(14, iNscp, ParamArrayMode.INPUT);
            params.addCharacter(15, iCcondp, ParamArrayMode.INPUT);
            params.addCharacter(16, iCart, ParamArrayMode.INPUT);
            params.addCharacter(17, iLot, ParamArrayMode.INPUT);
            params.addCharacter(18, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(19, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(20, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(21, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(22, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(23, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(24, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(25, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(26, iCu3, ParamArrayMode.INPUT);
            params.addInteger(27, new Integer(iTypTare), ParamArrayMode.INPUT);
            params.addTable(28, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addCharacter(29, null, ParamArrayMode.OUTPUT);
            params.addInteger(30, null, ParamArrayMode.OUTPUT);
            params.addLogical(31, null, ParamArrayMode.OUTPUT);
            params.addCharacter(32, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Validate-Mvt-Entree", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttLocaldocprg
        
            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params,28));
            oRowidMvt.setStringValue((String) params.getOutputParameter(29));
            oNlig.setIntegerValue((Integer) params.getOutputParameter(30));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(31));
            oMsg.setStringValue((String) params.getOutputParameter(32));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prValidateMvtEntree2.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iTypacta  String INPUT
     * @param iNsc  String INPUT
     * @param iCcond  String INPUT
     * @param iNscp  String INPUT
     * @param iCcondp  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iCmotmvk  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param iTypTare  int INPUT
     * @param iCnatStk  String INPUT
     * @param ttLocaldocprgHolder  TABLE OUTPUT
     * @param oRowidMvt  String OUTPUT
     * @param oNlig  int OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prValidateMvtEntree2(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final String iNsc, final String iCcond, final String iNscp, final String iCcondp, final String iCart, final String iLot, final String iCdepot, final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final int iTypTare, final String iCnatStk, final TempTableHolder<XxfabPPOTtLocaldocprg> ttLocaldocprgHolder, final StringHolder oRowidMvt, final IntegerHolder oNlig, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocprgHolder == null || oRowidMvt == null || oNlig == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prValidateMvtEntree2 values :" + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oRowidMvt = " + oRowidMvt + "oNlig = " + oNlig + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prValidateMvtEntree2 "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " iNsc = " + iNsc + " iCcond = " + iCcond + " iNscp = " + iNscp + " iCcondp = " + iCcondp + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " iTypTare = " + iTypTare + " iCnatStk = " + iCnatStk + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRowidMvt = " + oRowidMvt + " oNlig = " + oNlig + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(34);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, iNsc, ParamArrayMode.INPUT);
            params.addCharacter(13, iCcond, ParamArrayMode.INPUT);
            params.addCharacter(14, iNscp, ParamArrayMode.INPUT);
            params.addCharacter(15, iCcondp, ParamArrayMode.INPUT);
            params.addCharacter(16, iCart, ParamArrayMode.INPUT);
            params.addCharacter(17, iLot, ParamArrayMode.INPUT);
            params.addCharacter(18, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(19, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(20, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(21, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(22, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(23, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(24, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(25, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(26, iCu3, ParamArrayMode.INPUT);
            params.addInteger(27, new Integer(iTypTare), ParamArrayMode.INPUT);
            params.addCharacter(28, iCnatStk, ParamArrayMode.INPUT);
            params.addTable(29, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addCharacter(30, null, ParamArrayMode.OUTPUT);
            params.addInteger(31, null, ParamArrayMode.OUTPUT);
            params.addLogical(32, null, ParamArrayMode.OUTPUT);
            params.addCharacter(33, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Validate-Mvt-Entree2", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttLocaldocprg
        
            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params,29));
            oRowidMvt.setStringValue((String) params.getOutputParameter(30));
            oNlig.setIntegerValue((Integer) params.getOutputParameter(31));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(32));
            oMsg.setStringValue((String) params.getOutputParameter(33));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prValidateMvtEntree3.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iTypacta  String INPUT
     * @param iNsc  String INPUT
     * @param iCcond  String INPUT
     * @param iNscp  String INPUT
     * @param iCcondp  String INPUT
     * @param iNsp  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iCmotmvk  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param iTypTare  int INPUT
     * @param iCnatStk  String INPUT
     * @param ttLocaldocprgHolder  TABLE OUTPUT
     * @param oRowidMvt  String OUTPUT
     * @param oNlig  int OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prValidateMvtEntree3(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final String iNsc, final String iCcond, final String iNscp, final String iCcondp, final String iNsp, final String iCart, final String iLot, final String iCdepot, final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final int iTypTare, final String iCnatStk, final TempTableHolder<XxfabPPOTtLocaldocprg> ttLocaldocprgHolder, final StringHolder oRowidMvt, final IntegerHolder oNlig, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ttLocaldocprgHolder == null || oRowidMvt == null || oNlig == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prValidateMvtEntree3 values :" + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oRowidMvt = " + oRowidMvt + "oNlig = " + oNlig + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prValidateMvtEntree3 "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " iNsc = " + iNsc + " iCcond = " + iCcond + " iNscp = " + iNscp + " iCcondp = " + iCcondp + " iNsp = " + iNsp + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " iTypTare = " + iTypTare + " iCnatStk = " + iCnatStk + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRowidMvt = " + oRowidMvt + " oNlig = " + oNlig + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(35);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, iNsc, ParamArrayMode.INPUT);
            params.addCharacter(13, iCcond, ParamArrayMode.INPUT);
            params.addCharacter(14, iNscp, ParamArrayMode.INPUT);
            params.addCharacter(15, iCcondp, ParamArrayMode.INPUT);
            params.addCharacter(16, iNsp, ParamArrayMode.INPUT);
            params.addCharacter(17, iCart, ParamArrayMode.INPUT);
            params.addCharacter(18, iLot, ParamArrayMode.INPUT);
            params.addCharacter(19, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(20, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(21, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(22, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(23, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(24, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(25, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(26, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(27, iCu3, ParamArrayMode.INPUT);
            params.addInteger(28, new Integer(iTypTare), ParamArrayMode.INPUT);
            params.addCharacter(29, iCnatStk, ParamArrayMode.INPUT);
            params.addTable(30, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addCharacter(31, null, ParamArrayMode.OUTPUT);
            params.addInteger(32, null, ParamArrayMode.OUTPUT);
            params.addLogical(33, null, ParamArrayMode.OUTPUT);
            params.addCharacter(34, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Validate-Mvt-Entree3", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
        // Make the temp-table ttLocaldocprg
        
            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params,30));
            oRowidMvt.setStringValue((String) params.getOutputParameter(31));
            oNlig.setIntegerValue((Integer) params.getOutputParameter(32));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(33));
            oMsg.setStringValue((String) params.getOutputParameter(34));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prValidateMvtSortie.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iTypacta  String INPUT
     * @param iCcond  String INPUT
     * @param iNotat  String INPUT
     * @param iTare  double INPUT
     * @param iCunitar  String INPUT
     * @param iCcondp  String INPUT
     * @param iNotatp  String INPUT
     * @param iTarep  double INPUT
     * @param iCunitarp  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iCmotmvk  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param iTypFerm  int INPUT
     * @param iTypFermCapa  int INPUT
     * @param iTypFermp  int INPUT
     * @param iTypFermpCapa  int INPUT
     * @param iTypTare  int INPUT
     * @param iPrechroDestination  String INPUT
     * @param iChronoDestination  int INPUT
     * @param ioNsc  String INPUT_OUTPUT
     * @param ioNscp  String INPUT_OUTPUT
     * @param ttLocaldocprgHolder  TABLE OUTPUT
     * @param oRowidMvtStr  String OUTPUT
     * @param oNscClosed  boolean OUTPUT
     * @param oNscpClosed  boolean OUTPUT
     * @param oNlig  int OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prValidateMvtSortie(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final String iCcond, final String iNotat, final double iTare, final String iCunitar, final String iCcondp, final String iNotatp, final double iTarep, final String iCunitarp, final String iCart, final String iLot, final String iCdepot, final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final int iTypFerm, final int iTypFermCapa, final int iTypFermp, final int iTypFermpCapa, final int iTypTare, final String iPrechroDestination, final int iChronoDestination, final StringHolder ioNsc, final StringHolder ioNscp, final TempTableHolder<XxfabPPOTtLocaldocprg> ttLocaldocprgHolder, final StringHolder oRowidMvtStr, final BooleanHolder oNscClosed, final BooleanHolder oNscpClosed, final IntegerHolder oNlig, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ioNsc == null || ioNscp == null || ttLocaldocprgHolder == null || oRowidMvtStr == null || oNscClosed == null || oNscpClosed == null || oNlig == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prValidateMvtSortie values :" + "ioNsc = " + ioNsc + "ioNscp = " + ioNscp + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oRowidMvtStr = " + oRowidMvtStr + "oNscClosed = " + oNscClosed + "oNscpClosed = " + oNscpClosed + "oNlig = " + oNlig + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prValidateMvtSortie "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " iCcond = " + iCcond + " iNotat = " + iNotat + " iTare = " + iTare + " iCunitar = " + iCunitar + " iCcondp = " + iCcondp + " iNotatp = " + iNotatp + " iTarep = " + iTarep + " iCunitarp = " + iCunitarp + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " iTypFerm = " + iTypFerm + " iTypFermCapa = " + iTypFermCapa + " iTypFermp = " + iTypFermp + " iTypFermpCapa = " + iTypFermpCapa + " iTypTare = " + iTypTare + " iPrechroDestination = " + iPrechroDestination + " iChronoDestination = " + iChronoDestination + " ioNsc = " + ioNsc + " ioNscp = " + ioNscp + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRowidMvtStr = " + oRowidMvtStr + " oNscClosed = " + oNscClosed + " oNscpClosed = " + oNscpClosed + " oNlig = " + oNlig + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(47);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, iCcond, ParamArrayMode.INPUT);
            params.addCharacter(13, iNotat, ParamArrayMode.INPUT);
            params.addDecimal(14, new BigDecimal(iTare).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(15, iCunitar, ParamArrayMode.INPUT);
            params.addCharacter(16, iCcondp, ParamArrayMode.INPUT);
            params.addCharacter(17, iNotatp, ParamArrayMode.INPUT);
            params.addDecimal(18, new BigDecimal(iTarep).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(19, iCunitarp, ParamArrayMode.INPUT);
            params.addCharacter(20, iCart, ParamArrayMode.INPUT);
            params.addCharacter(21, iLot, ParamArrayMode.INPUT);
            params.addCharacter(22, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(23, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(24, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(25, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(26, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(27, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(28, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(29, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(30, iCu3, ParamArrayMode.INPUT);
            params.addInteger(31, new Integer(iTypFerm), ParamArrayMode.INPUT);
            params.addInteger(32, new Integer(iTypFermCapa), ParamArrayMode.INPUT);
            params.addInteger(33, new Integer(iTypFermp), ParamArrayMode.INPUT);
            params.addInteger(34, new Integer(iTypFermpCapa), ParamArrayMode.INPUT);
            params.addInteger(35, new Integer(iTypTare), ParamArrayMode.INPUT);
            params.addCharacter(36, iPrechroDestination, ParamArrayMode.INPUT);
            params.addInteger(37, new Integer(iChronoDestination), ParamArrayMode.INPUT);
            params.addCharacter(38, ioNsc.getStringValue(), ParamArrayMode.INPUT_OUTPUT);
            params.addCharacter(39, ioNscp.getStringValue(), ParamArrayMode.INPUT_OUTPUT);
            params.addTable(40, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addCharacter(41, null, ParamArrayMode.OUTPUT);
            params.addLogical(42, null, ParamArrayMode.OUTPUT);
            params.addLogical(43, null, ParamArrayMode.OUTPUT);
            params.addInteger(44, null, ParamArrayMode.OUTPUT);
            params.addLogical(45, null, ParamArrayMode.OUTPUT);
            params.addCharacter(46, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Validate-Mvt-Sortie", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            ioNsc.setStringValue((String) params.getOutputParameter(38));
            ioNscp.setStringValue((String) params.getOutputParameter(39));
        // Make the temp-table ttLocaldocprg
        
            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params,40));
            oRowidMvtStr.setStringValue((String) params.getOutputParameter(41));
            oNscClosed.setBooleanValue((Boolean) params.getOutputParameter(42));
            oNscpClosed.setBooleanValue((Boolean) params.getOutputParameter(43));
            oNlig.setIntegerValue((Integer) params.getOutputParameter(44));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(45));
            oMsg.setStringValue((String) params.getOutputParameter(46));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generated Class prValidateMvtSortieRebut.
     *
     * @param iCsoc  String INPUT
     * @param iCetab  String INPUT
     * @param iCposte  String INPUT
     * @param iCuser  String INPUT
     * @param iTypgest  String INPUT
     * @param iPrechrof  String INPUT
     * @param iChronof  int INPUT
     * @param iNi1  int INPUT
     * @param iNi2  int INPUT
     * @param iNi3  int INPUT
     * @param iNi4  int INPUT
     * @param iTypacta  String INPUT
     * @param iCcond  String INPUT
     * @param iNotat  String INPUT
     * @param iTare  double INPUT
     * @param iCunitar  String INPUT
     * @param iCcondp  String INPUT
     * @param iNotatp  String INPUT
     * @param iTarep  double INPUT
     * @param iCunitarp  String INPUT
     * @param iCart  String INPUT
     * @param iLot  String INPUT
     * @param iCdepot  String INPUT
     * @param iCemp  String INPUT
     * @param iCmotmvk  String INPUT
     * @param iQte1  double INPUT
     * @param iCu1  String INPUT
     * @param iQte2  double INPUT
     * @param iCu2  String INPUT
     * @param iQte3  double INPUT
     * @param iCu3  String INPUT
     * @param iTypFerm  int INPUT
     * @param iTypFermCapa  int INPUT
     * @param iTypFermp  int INPUT
     * @param iTypFermpCapa  int INPUT
     * @param iTypTare  int INPUT
     * @param iCnatStk  String INPUT
     * @param iPrechroDestination  String INPUT
     * @param iChronoDestination  int INPUT
     * @param ioNsc  String INPUT_OUTPUT
     * @param ioNscp  String INPUT_OUTPUT
     * @param ttLocaldocprgHolder  TABLE OUTPUT
     * @param oRowidMvtStr  String OUTPUT
     * @param oNscClosed  boolean OUTPUT
     * @param oNscpClosed  boolean OUTPUT
     * @param oNlig  int OUTPUT
     * @param oTret  boolean OUTPUT
     * @param oMsg  String OUTPUT
     * @throws ProgressBusinessException Progress Error thrown.
     */
    public void prValidateMvtSortieRebut(final String iCsoc, final String iCetab, final String iCposte, final String iCuser, final String iTypgest, final String iPrechrof, final int iChronof, final int iNi1, final int iNi2, final int iNi3, final int iNi4, final String iTypacta, final String iCcond, final String iNotat, final double iTare, final String iCunitar, final String iCcondp, final String iNotatp, final double iTarep, final String iCunitarp, final String iCart, final String iLot, final String iCdepot, final String iCemp, final String iCmotmvk, final double iQte1, final String iCu1, final double iQte2, final String iCu2, final double iQte3, final String iCu3, final int iTypFerm, final int iTypFermCapa, final int iTypFermp, final int iTypFermpCapa, final int iTypTare, final String iCnatStk, final String iPrechroDestination, final int iChronoDestination, final StringHolder ioNsc, final StringHolder ioNscp, final TempTableHolder<XxfabPPOTtLocaldocprg> ttLocaldocprgHolder, final StringHolder oRowidMvtStr, final BooleanHolder oNscClosed, final BooleanHolder oNscpClosed, final IntegerHolder oNlig, final BooleanHolder oTret, final StringHolder oMsg) throws ProgressBusinessException {
        // ------------------------------------------
        // Verif is null
        // ------------------------------------------
        if (ioNsc == null || ioNscp == null || ttLocaldocprgHolder == null || oRowidMvtStr == null || oNscClosed == null || oNscpClosed == null || oNlig == null || oTret == null || oMsg == null) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error("In method : prValidateMvtSortieRebut values :" + "ioNsc = " + ioNsc + "ioNscp = " + ioNscp + "ttLocaldocprgHolder = " + ttLocaldocprgHolder + "oRowidMvtStr = " + oRowidMvtStr + "oNscClosed = " + oNscClosed + "oNscpClosed = " + oNscpClosed + "oNlig = " + oNlig + "oTret = " + oTret + "oMsg = " + oMsg);
            }
            throw new ProgressBusinessException("Parameter has a null value !!");
        }
         
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("In Method prValidateMvtSortieRebut "  + " iCsoc = " + iCsoc + " iCetab = " + iCetab + " iCposte = " + iCposte + " iCuser = " + iCuser + " iTypgest = " + iTypgest + " iPrechrof = " + iPrechrof + " iChronof = " + iChronof + " iNi1 = " + iNi1 + " iNi2 = " + iNi2 + " iNi3 = " + iNi3 + " iNi4 = " + iNi4 + " iTypacta = " + iTypacta + " iCcond = " + iCcond + " iNotat = " + iNotat + " iTare = " + iTare + " iCunitar = " + iCunitar + " iCcondp = " + iCcondp + " iNotatp = " + iNotatp + " iTarep = " + iTarep + " iCunitarp = " + iCunitarp + " iCart = " + iCart + " iLot = " + iLot + " iCdepot = " + iCdepot + " iCemp = " + iCemp + " iCmotmvk = " + iCmotmvk + " iQte1 = " + iQte1 + " iCu1 = " + iCu1 + " iQte2 = " + iQte2 + " iCu2 = " + iCu2 + " iQte3 = " + iQte3 + " iCu3 = " + iCu3 + " iTypFerm = " + iTypFerm + " iTypFermCapa = " + iTypFermCapa + " iTypFermp = " + iTypFermp + " iTypFermpCapa = " + iTypFermpCapa + " iTypTare = " + iTypTare + " iCnatStk = " + iCnatStk + " iPrechroDestination = " + iPrechroDestination + " iChronoDestination = " + iChronoDestination + " ioNsc = " + ioNsc + " ioNscp = " + ioNscp + " ttLocaldocprgHolder = " + ttLocaldocprgHolder + " oRowidMvtStr = " + oRowidMvtStr + " oNscClosed = " + oNscClosed + " oNscpClosed = " + oNscpClosed + " oNlig = " + oNlig + " oTret = " + oTret + " oMsg = " + oMsg);
        }
        // ------------------------------------------
        // Set the value of ParamArray
        // ------------------------------------------
        
        /* CHECKSTYLE:OFF */
        ParamArray params = new ParamArray(48);
        
        try {
            params.addCharacter(0, iCsoc, ParamArrayMode.INPUT);
            params.addCharacter(1, iCetab, ParamArrayMode.INPUT);
            params.addCharacter(2, iCposte, ParamArrayMode.INPUT);
            params.addCharacter(3, iCuser, ParamArrayMode.INPUT);
            params.addCharacter(4, iTypgest, ParamArrayMode.INPUT);
            params.addCharacter(5, iPrechrof, ParamArrayMode.INPUT);
            params.addInteger(6, new Integer(iChronof), ParamArrayMode.INPUT);
            params.addInteger(7, new Integer(iNi1), ParamArrayMode.INPUT);
            params.addInteger(8, new Integer(iNi2), ParamArrayMode.INPUT);
            params.addInteger(9, new Integer(iNi3), ParamArrayMode.INPUT);
            params.addInteger(10, new Integer(iNi4), ParamArrayMode.INPUT);
            params.addCharacter(11, iTypacta, ParamArrayMode.INPUT);
            params.addCharacter(12, iCcond, ParamArrayMode.INPUT);
            params.addCharacter(13, iNotat, ParamArrayMode.INPUT);
            params.addDecimal(14, new BigDecimal(iTare).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(15, iCunitar, ParamArrayMode.INPUT);
            params.addCharacter(16, iCcondp, ParamArrayMode.INPUT);
            params.addCharacter(17, iNotatp, ParamArrayMode.INPUT);
            params.addDecimal(18, new BigDecimal(iTarep).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(19, iCunitarp, ParamArrayMode.INPUT);
            params.addCharacter(20, iCart, ParamArrayMode.INPUT);
            params.addCharacter(21, iLot, ParamArrayMode.INPUT);
            params.addCharacter(22, iCdepot, ParamArrayMode.INPUT);
            params.addCharacter(23, iCemp, ParamArrayMode.INPUT);
            params.addCharacter(24, iCmotmvk, ParamArrayMode.INPUT);
            params.addDecimal(25, new BigDecimal(iQte1).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(26, iCu1, ParamArrayMode.INPUT);
            params.addDecimal(27, new BigDecimal(iQte2).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(28, iCu2, ParamArrayMode.INPUT);
            params.addDecimal(29, new BigDecimal(iQte3).setScale(4, RoundingMode.HALF_EVEN), ParamArrayMode.INPUT);
            params.addCharacter(30, iCu3, ParamArrayMode.INPUT);
            params.addInteger(31, new Integer(iTypFerm), ParamArrayMode.INPUT);
            params.addInteger(32, new Integer(iTypFermCapa), ParamArrayMode.INPUT);
            params.addInteger(33, new Integer(iTypFermp), ParamArrayMode.INPUT);
            params.addInteger(34, new Integer(iTypFermpCapa), ParamArrayMode.INPUT);
            params.addInteger(35, new Integer(iTypTare), ParamArrayMode.INPUT);
            params.addCharacter(36, iCnatStk, ParamArrayMode.INPUT);
            params.addCharacter(37, iPrechroDestination, ParamArrayMode.INPUT);
            params.addInteger(38, new Integer(iChronoDestination), ParamArrayMode.INPUT);
            params.addCharacter(39, ioNsc.getStringValue(), ParamArrayMode.INPUT_OUTPUT);
            params.addCharacter(40, ioNscp.getStringValue(), ParamArrayMode.INPUT_OUTPUT);
            params.addTable(41, null, ParamArrayMode.OUTPUT, getMetaDatattLocaldocprg());
            params.addCharacter(42, null, ParamArrayMode.OUTPUT);
            params.addLogical(43, null, ParamArrayMode.OUTPUT);
            params.addLogical(44, null, ParamArrayMode.OUTPUT);
            params.addInteger(45, null, ParamArrayMode.OUTPUT);
            params.addLogical(46, null, ParamArrayMode.OUTPUT);
            params.addCharacter(47, null, ParamArrayMode.OUTPUT);
        
            /* CHECKSTYLE:ON */
            // ------------------------------------------
            // Call the progress procedure by the App server
            // ------------------------------------------
            runProc(ctx, "Pr-Validate-Mvt-Sortie-Rebut", params);
        
            // ------------------------------------------
            // Set the returns values
            // ------------------------------------------
            /* CHECKSTYLE:OFF */
        
            ioNsc.setStringValue((String) params.getOutputParameter(39));
            ioNscp.setStringValue((String) params.getOutputParameter(40));
        // Make the temp-table ttLocaldocprg
        
            ttLocaldocprgHolder.setListValue(convertRecordttLocaldocprg(params,41));
            oRowidMvtStr.setStringValue((String) params.getOutputParameter(42));
            oNscClosed.setBooleanValue((Boolean) params.getOutputParameter(43));
            oNscpClosed.setBooleanValue((Boolean) params.getOutputParameter(44));
            oNlig.setIntegerValue((Integer) params.getOutputParameter(45));
            oTret.setBooleanValue((Boolean) params.getOutputParameter(46));
            oMsg.setStringValue((String) params.getOutputParameter(47));
            /* CHECKSTYLE:ON */
        } catch (ProSQLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.ERROR)) {
                LOGGER.error(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        }
    }

    /**
     * Generate the MetaData of tempTable ttXmvtm.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttXmvtm.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattXmvtm() throws ProSQLException {
        
        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttXmvtm");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattXmvtm == null) {
            metadatattXmvtm = new ProResultSetMetaDataImpl(20);
            metadatattXmvtm.setFieldMetaData(1, "Nlig", 0, Parameter.PRO_INTEGER);
            metadatattXmvtm.setFieldMetaData(2, "cArt", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(3, "lArt", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(4, "cDepot", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(5, "lDepot", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(6, "cEmp", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(7, "lEmp", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(8, "Lot", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(9, "Qte1", 0, Parameter.PRO_DECIMAL);
            metadatattXmvtm.setFieldMetaData(10, "cU1", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(11, "Qte2", 0, Parameter.PRO_DECIMAL);
            metadatattXmvtm.setFieldMetaData(12, "cU2", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(13, "Qte3", 0, Parameter.PRO_DECIMAL);
            metadatattXmvtm.setFieldMetaData(14, "cU3", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(15, "cPoste", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(16, "cMotMvk", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(17, "lMotMvk", 0, Parameter.PRO_CHARACTER);
            metadatattXmvtm.setFieldMetaData(18, "DatMvt", 0, Parameter.PRO_DATE);
            metadatattXmvtm.setFieldMetaData(19, "HeurMvt", 0, Parameter.PRO_INTEGER);
            metadatattXmvtm.setFieldMetaData(20, "RowidMVT", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattXmvtm;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabPPOTtxmvtm&gt; Convert a record set to a list of XxfabPPOTtxmvtm
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabPPOTtxmvtm> convertRecordttXmvtm(final ParamArray params, final int nbParam) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabPPOTtxmvtm");
        }
        
        ProResultSet rs = null;
        List<XxfabPPOTtxmvtm> list = new ArrayList<XxfabPPOTtxmvtm>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabPPOTtxmvtm tempTable = new XxfabPPOTtxmvtm();
                /* CHECKSTYLE:OFF */
                tempTable.setNlig(rs.getInt("Nlig"));
                tempTable.setCArt(rs.getString("cArt"));
                tempTable.setLArt(rs.getString("lArt"));
                tempTable.setCDepot(rs.getString("cDepot"));
                tempTable.setLDepot(rs.getString("lDepot"));
                tempTable.setCEmp(rs.getString("cEmp"));
                tempTable.setLEmp(rs.getString("lEmp"));
                tempTable.setLot(rs.getString("Lot"));
                tempTable.setQte1(rs.getDouble("Qte1"));
                tempTable.setCU1(rs.getString("cU1"));
                tempTable.setQte2(rs.getDouble("Qte2"));
                tempTable.setCU2(rs.getString("cU2"));
                tempTable.setQte3(rs.getDouble("Qte3"));
                tempTable.setCU3(rs.getString("cU3"));
                tempTable.setCPoste(rs.getString("cPoste"));
                tempTable.setCMotMvk(rs.getString("cMotMvk"));
                tempTable.setLMotMvk(rs.getString("lMotMvk"));
                GregorianCalendar gdatMvt = rs.getGregorianCalendar("DatMvt");
                tempTable.setDatMvt(gdatMvt != null ? gdatMvt.getTime() : null);
                tempTable.setHeurMvt(rs.getInt("HeurMvt"));
                tempTable.setRowidMVT(rs.getString("RowidMVT"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }
         
        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttXmvtm List of XxfabPPOTtxmvtm
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtxmvtm
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettXmvtm(final List<XxfabPPOTtxmvtm> ttXmvtm) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtxmvtm");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list 
        Iterator<XxfabPPOTtxmvtm> iter = ttXmvtm.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtxmvtm row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getNlig());
            list.add(row.getCArt());
            list.add(row.getLArt());
            list.add(row.getCDepot());
            list.add(row.getLDepot());
            list.add(row.getCEmp());
            list.add(row.getLEmp());
            list.add(row.getLot());
            list.add(new BigDecimal(row.getQte1()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCU1());
            list.add(new BigDecimal(row.getQte2()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCU2());
            list.add(new BigDecimal(row.getQte3()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCU3());
            list.add(row.getCPoste());
            list.add(row.getCMotMvk());
            list.add(row.getLMotMvk());
            list.add(dateToGreg(row.getDatMvt()));
            list.add(row.getHeurMvt());
            list.add(row.getRowidMVT());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }
         
        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttArtRemp.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttArtRemp.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattArtRemp() throws ProSQLException {
        
        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttArtRemp");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattArtRemp == null) {
            metadatattArtRemp = new ProResultSetMetaDataImpl(2);
            metadatattArtRemp.setFieldMetaData(1, "cArt", 0, Parameter.PRO_CHARACTER);
            metadatattArtRemp.setFieldMetaData(2, "lArt", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattArtRemp;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabPPOTtartremp&gt; Convert a record set to a list of XxfabPPOTtartremp
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabPPOTtartremp> convertRecordttArtRemp(final ParamArray params, final int nbParam) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabPPOTtartremp");
        }
        
        ProResultSet rs = null;
        List<XxfabPPOTtartremp> list = new ArrayList<XxfabPPOTtartremp>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabPPOTtartremp tempTable = new XxfabPPOTtartremp();
                /* CHECKSTYLE:OFF */
                tempTable.setCArt(rs.getString("cArt"));
                tempTable.setLArt(rs.getString("lArt"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }
         
        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttArtRemp List of XxfabPPOTtartremp
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtartremp
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettArtRemp(final List<XxfabPPOTtartremp> ttArtRemp) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtartremp");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list 
        Iterator<XxfabPPOTtartremp> iter = ttArtRemp.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtartremp row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCArt());
            list.add(row.getLArt());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }
         
        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttLot.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttLot.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattLot() throws ProSQLException {
        
        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttLot");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattLot == null) {
            metadatattLot = new ProResultSetMetaDataImpl(1);
            metadatattLot.setFieldMetaData(1, "lot", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattLot;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabPPOTtlot&gt; Convert a record set to a list of XxfabPPOTtlot
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabPPOTtlot> convertRecordttLot(final ParamArray params, final int nbParam) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabPPOTtlot");
        }
        
        ProResultSet rs = null;
        List<XxfabPPOTtlot> list = new ArrayList<XxfabPPOTtlot>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabPPOTtlot tempTable = new XxfabPPOTtlot();
                /* CHECKSTYLE:OFF */
                tempTable.setLot(rs.getString("lot"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }
         
        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttLot List of XxfabPPOTtlot
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtlot
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettLot(final List<XxfabPPOTtlot> ttLot) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtlot");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list 
        Iterator<XxfabPPOTtlot> iter = ttLot.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtlot row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getLot());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }
         
        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable tTQualite.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table tTQualite.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatatTQualite() throws ProSQLException {
        
        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table tTQualite");
        }
        /* CHECKSTYLE:OFF */
        if (metadatatTQualite == null) {
            metadatatTQualite = new ProResultSetMetaDataImpl(9);
            metadatatTQualite.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatatTQualite.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatatTQualite.setFieldMetaData(3, "ccri", 0, Parameter.PRO_CHARACTER);
            metadatatTQualite.setFieldMetaData(4, "valdmin", 0, Parameter.PRO_DATE);
            metadatatTQualite.setFieldMetaData(5, "valdmax", 0, Parameter.PRO_DATE);
            metadatatTQualite.setFieldMetaData(6, "valnmin", 0, Parameter.PRO_DECIMAL);
            metadatatTQualite.setFieldMetaData(7, "valnmax", 0, Parameter.PRO_DECIMAL);
            metadatatTQualite.setFieldMetaData(8, "valcmin", 0, Parameter.PRO_CHARACTER);
            metadatatTQualite.setFieldMetaData(9, "valcmax", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatatTQualite;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabPPOTtqualite&gt; Convert a record set to a list of XxfabPPOTtqualite
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabPPOTtqualite> convertRecordtTQualite(final ParamArray params, final int nbParam) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabPPOTtqualite");
        }
        
        ProResultSet rs = null;
        List<XxfabPPOTtqualite> list = new ArrayList<XxfabPPOTtqualite>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabPPOTtqualite tempTable = new XxfabPPOTtqualite();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setCcri(rs.getString("ccri"));
                GregorianCalendar gvaldmin = rs.getGregorianCalendar("valdmin");
                tempTable.setValdmin(gvaldmin != null ? gvaldmin.getTime() : null);
                GregorianCalendar gvaldmax = rs.getGregorianCalendar("valdmax");
                tempTable.setValdmax(gvaldmax != null ? gvaldmax.getTime() : null);
                tempTable.setValnmin(rs.getDouble("valnmin"));
                tempTable.setValnmax(rs.getDouble("valnmax"));
                tempTable.setValcmin(rs.getString("valcmin"));
                tempTable.setValcmax(rs.getString("valcmax"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }
         
        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param tTQualite List of XxfabPPOTtqualite
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtqualite
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTabletTQualite(final List<XxfabPPOTtqualite> tTQualite) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtqualite");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list 
        Iterator<XxfabPPOTtqualite> iter = tTQualite.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtqualite row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getCcri());
            list.add(dateToGreg(row.getValdmin()));
            list.add(dateToGreg(row.getValdmax()));
            list.add(new BigDecimal(row.getValnmin()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getValnmax()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getValcmin());
            list.add(row.getValcmax());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }
         
        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttContenu.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttContenu.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattContenu() throws ProSQLException {
        
        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttContenu");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattContenu == null) {
            metadatattContenu = new ProResultSetMetaDataImpl(20);
            metadatattContenu.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattContenu.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattContenu.setFieldMetaData(3, "prechrof", 0, Parameter.PRO_CHARACTER);
            metadatattContenu.setFieldMetaData(4, "chronof", 0, Parameter.PRO_INTEGER);
            metadatattContenu.setFieldMetaData(5, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattContenu.setFieldMetaData(6, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattContenu.setFieldMetaData(7, "ni3", 0, Parameter.PRO_INTEGER);
            metadatattContenu.setFieldMetaData(8, "ni4", 0, Parameter.PRO_INTEGER);
            metadatattContenu.setFieldMetaData(9, "cart", 0, Parameter.PRO_CHARACTER);
            metadatattContenu.setFieldMetaData(10, "lart", 0, Parameter.PRO_CHARACTER);
            metadatattContenu.setFieldMetaData(11, "lot", 0, Parameter.PRO_CHARACTER);
            metadatattContenu.setFieldMetaData(12, "qug", 0, Parameter.PRO_DECIMAL);
            metadatattContenu.setFieldMetaData(13, "qus", 0, Parameter.PRO_DECIMAL);
            metadatattContenu.setFieldMetaData(14, "qut", 0, Parameter.PRO_DECIMAL);
            metadatattContenu.setFieldMetaData(15, "cug", 0, Parameter.PRO_CHARACTER);
            metadatattContenu.setFieldMetaData(16, "cus", 0, Parameter.PRO_CHARACTER);
            metadatattContenu.setFieldMetaData(17, "cut", 0, Parameter.PRO_CHARACTER);
            metadatattContenu.setFieldMetaData(18, "pdsbrut", 0, Parameter.PRO_DECIMAL);
            metadatattContenu.setFieldMetaData(19, "pdsnet", 0, Parameter.PRO_DECIMAL);
            metadatattContenu.setFieldMetaData(20, "quantu", 0, Parameter.PRO_DECIMAL);
        }
        /* CHECKSTYLE:ON */
        return metadatattContenu;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabPPOTtcontenu&gt; Convert a record set to a list of XxfabPPOTtcontenu
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabPPOTtcontenu> convertRecordttContenu(final ParamArray params, final int nbParam) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabPPOTtcontenu");
        }
        
        ProResultSet rs = null;
        List<XxfabPPOTtcontenu> list = new ArrayList<XxfabPPOTtcontenu>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabPPOTtcontenu tempTable = new XxfabPPOTtcontenu();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setPrechrof(rs.getString("prechrof"));
                tempTable.setChronof(rs.getInt("chronof"));
                tempTable.setNi1(rs.getInt("ni1"));
                tempTable.setNi2(rs.getInt("ni2"));
                tempTable.setNi3(rs.getInt("ni3"));
                tempTable.setNi4(rs.getInt("ni4"));
                tempTable.setCart(rs.getString("cart"));
                tempTable.setLart(rs.getString("lart"));
                tempTable.setLot(rs.getString("lot"));
                tempTable.setQug(rs.getDouble("qug"));
                tempTable.setQus(rs.getDouble("qus"));
                tempTable.setQut(rs.getDouble("qut"));
                tempTable.setCug(rs.getString("cug"));
                tempTable.setCus(rs.getString("cus"));
                tempTable.setCut(rs.getString("cut"));
                tempTable.setPdsbrut(rs.getDouble("pdsbrut"));
                tempTable.setPdsnet(rs.getDouble("pdsnet"));
                tempTable.setQuantu(rs.getDouble("quantu"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }
         
        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttContenu List of XxfabPPOTtcontenu
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtcontenu
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettContenu(final List<XxfabPPOTtcontenu> ttContenu) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtcontenu");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list 
        Iterator<XxfabPPOTtcontenu> iter = ttContenu.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtcontenu row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getPrechrof());
            list.add(row.getChronof());
            list.add(row.getNi1());
            list.add(row.getNi2());
            list.add(row.getNi3());
            list.add(row.getNi4());
            list.add(row.getCart());
            list.add(row.getLart());
            list.add(row.getLot());
            list.add(new BigDecimal(row.getQug()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getQus()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getQut()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getCug());
            list.add(row.getCus());
            list.add(row.getCut());
            list.add(new BigDecimal(row.getPdsbrut()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getPdsnet()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getQuantu()).setScale(4, RoundingMode.HALF_EVEN));
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }
         
        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttListoperations.
     * 
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttListoperations.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattListoperations() throws ProSQLException {

        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttListoperations");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattListoperations == null) {
            metadatattListoperations = new ProResultSetMetaDataImpl(9);
            metadatattListoperations.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattListoperations.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattListoperations.setFieldMetaData(3, "TypGest", 0, Parameter.PRO_CHARACTER);
            metadatattListoperations.setFieldMetaData(4, "Prechrof", 0, Parameter.PRO_CHARACTER);
            metadatattListoperations.setFieldMetaData(5, "Chronof", 0, Parameter.PRO_INTEGER);
            metadatattListoperations.setFieldMetaData(6, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattListoperations.setFieldMetaData(7, "ni2", 0, Parameter.PRO_INTEGER);
            metadatattListoperations.setFieldMetaData(8, "ni3", 0, Parameter.PRO_INTEGER);
            metadatattListoperations.setFieldMetaData(9, "ni4", 0, Parameter.PRO_INTEGER);
        }
        /* CHECKSTYLE:ON */
        return metadatattListoperations;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     * 
     * @param ttListoperations List of XxfabPPOTtListoperations
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtListoperations
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettListoperations(
            final List<XxfabPPOTtListoperations> ttListoperations) throws ProgressBusinessException {

        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------

        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtListoperations");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list
        Iterator<XxfabPPOTtListoperations> iter = ttListoperations.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtListoperations row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getTypGest());
            list.add(row.getPrechrof());
            list.add(row.getChronof());
            list.add(row.getNi1());
            list.add(row.getNi2());
            list.add(row.getNi3());
            list.add(row.getNi4());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }

        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable tTLocaldoc.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table tTLocaldoc.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatatTLocaldoc() throws ProSQLException {
        
        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table tTLocaldoc");
        }
        /* CHECKSTYLE:OFF */
        if (metadatatTLocaldoc == null) {
            metadatatTLocaldoc = new ProResultSetMetaDataImpl(3);
            metadatatTLocaldoc.setFieldMetaData(1, "cdoc", 0, Parameter.PRO_CHARACTER);
            metadatatTLocaldoc.setFieldMetaData(2, "nbdoc", 0, Parameter.PRO_INTEGER);
            metadatatTLocaldoc.setFieldMetaData(3, "event", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatatTLocaldoc;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabPPOTtLocaldoc&gt; Convert a record set to a list of XxfabPPOTtLocaldoc
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabPPOTtLocaldoc> convertRecordtTLocaldoc(final ParamArray params, final int nbParam) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabPPOTtLocaldoc");
        }
        
        ProResultSet rs = null;
        List<XxfabPPOTtLocaldoc> list = new ArrayList<XxfabPPOTtLocaldoc>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabPPOTtLocaldoc tempTable = new XxfabPPOTtLocaldoc();
                /* CHECKSTYLE:OFF */
                tempTable.setCdoc(rs.getString("cdoc"));
                tempTable.setNbdoc(rs.getInt("nbdoc"));
                tempTable.setEvent(rs.getString("event"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }
         
        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param tTLocaldoc List of XxfabPPOTtLocaldoc
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtLocaldoc
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTabletTLocaldoc(final List<XxfabPPOTtLocaldoc> tTLocaldoc) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtLocaldoc");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list 
        Iterator<XxfabPPOTtLocaldoc> iter = tTLocaldoc.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtLocaldoc row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCdoc());
            list.add(row.getNbdoc());
            list.add(row.getEvent());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }
         
        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttLocaldocprg.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttLocaldocprg.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattLocaldocprg() throws ProSQLException {
        
        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttLocaldocprg");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattLocaldocprg == null) {
            metadatattLocaldocprg = new ProResultSetMetaDataImpl(4);
            metadatattLocaldocprg.setFieldMetaData(1, "cdoc", 0, Parameter.PRO_CHARACTER);
            metadatattLocaldocprg.setFieldMetaData(2, "nbdoc", 0, Parameter.PRO_INTEGER);
            metadatattLocaldocprg.setFieldMetaData(3, "event", 0, Parameter.PRO_CHARACTER);
            metadatattLocaldocprg.setFieldMetaData(4, "prgedi", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattLocaldocprg;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabPPOTtLocaldocprg&gt; Convert a record set to a list of XxfabPPOTtLocaldocprg
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabPPOTtLocaldocprg> convertRecordttLocaldocprg(final ParamArray params, final int nbParam) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabPPOTtLocaldocprg");
        }
        
        ProResultSet rs = null;
        List<XxfabPPOTtLocaldocprg> list = new ArrayList<XxfabPPOTtLocaldocprg>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabPPOTtLocaldocprg tempTable = new XxfabPPOTtLocaldocprg();
                /* CHECKSTYLE:OFF */
                tempTable.setCdoc(rs.getString("cdoc"));
                tempTable.setNbdoc(rs.getInt("nbdoc"));
                tempTable.setEvent(rs.getString("event"));
                tempTable.setPrgedi(rs.getString("prgedi"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }
         
        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttLocaldocprg List of XxfabPPOTtLocaldocprg
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtLocaldocprg
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettLocaldocprg(final List<XxfabPPOTtLocaldocprg> ttLocaldocprg) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtLocaldocprg");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list 
        Iterator<XxfabPPOTtLocaldocprg> iter = ttLocaldocprg.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtLocaldocprg row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCdoc());
            list.add(row.getNbdoc());
            list.add(row.getEvent());
            list.add(row.getPrgedi());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }
         
        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttMotMvk.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttMotMvk.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattMotMvk() throws ProSQLException {
        
        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttMotMvk");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattMotMvk == null) {
            metadatattMotMvk = new ProResultSetMetaDataImpl(2);
            metadatattMotMvk.setFieldMetaData(1, "cMotMvk", 0, Parameter.PRO_CHARACTER);
            metadatattMotMvk.setFieldMetaData(2, "lMotMvk", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattMotMvk;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabPPOTtmotmvk&gt; Convert a record set to a list of XxfabPPOTtmotmvk
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabPPOTtmotmvk> convertRecordttMotMvk(final ParamArray params, final int nbParam) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabPPOTtmotmvk");
        }
        
        ProResultSet rs = null;
        List<XxfabPPOTtmotmvk> list = new ArrayList<XxfabPPOTtmotmvk>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabPPOTtmotmvk tempTable = new XxfabPPOTtmotmvk();
                /* CHECKSTYLE:OFF */
                tempTable.setCMotMvk(rs.getString("cMotMvk"));
                tempTable.setLMotMvk(rs.getString("lMotMvk"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }
         
        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttMotMvk List of XxfabPPOTtmotmvk
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtmotmvk
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettMotMvk(final List<XxfabPPOTtmotmvk> ttMotMvk) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtmotmvk");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list 
        Iterator<XxfabPPOTtmotmvk> iter = ttMotMvk.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtmotmvk row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCMotMvk());
            list.add(row.getLMotMvk());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }
         
        return tempTable;
    }

    /**
     * Generate the MetaData of tempTable ttCrivHandle.
     *
     * @return ProResultSetMetaDataImpl The metadata of the temp-table ttCrivHandle.
     * @throws ProSQLException Exception SQL
     */
    private static ProResultSetMetaDataImpl getMetaDatattCrivHandle() throws ProSQLException {
        
        // ------------------------------------------
        // Generate the metadata of the temp table
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate the metadata of the temp table ttCrivHandle");
        }
        /* CHECKSTYLE:OFF */
        if (metadatattCrivHandle == null) {
            metadatattCrivHandle = new ProResultSetMetaDataImpl(28);
            metadatattCrivHandle.setFieldMetaData(1, "csoc", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(2, "cetab", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(3, "typcle", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(4, "cle", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(5, "ccri", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(6, "cunite", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(7, "ni1", 0, Parameter.PRO_INTEGER);
            metadatattCrivHandle.setFieldMetaData(8, "nlig", 0, Parameter.PRO_INTEGER);
            metadatattCrivHandle.setFieldMetaData(9, "tsaimodif", 0, Parameter.PRO_LOGICAL);
            metadatattCrivHandle.setFieldMetaData(10, "valdmin", 0, Parameter.PRO_DATE);
            metadatattCrivHandle.setFieldMetaData(11, "valdmax", 0, Parameter.PRO_DATE);
            metadatattCrivHandle.setFieldMetaData(12, "valnmin", 0, Parameter.PRO_DECIMAL);
            metadatattCrivHandle.setFieldMetaData(13, "valnmax", 0, Parameter.PRO_DECIMAL);
            metadatattCrivHandle.setFieldMetaData(14, "valcmin", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(15, "valcmax", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(16, "libaff", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(17, "msaisie", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(18, "texclus", 0, Parameter.PRO_LOGICAL);
            metadatattCrivHandle.setFieldMetaData(19, "tsaioblig", 0, Parameter.PRO_LOGICAL);
            metadatattCrivHandle.setFieldMetaData(20, "corig", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(21, "tanomotif", 0, Parameter.PRO_LOGICAL);
            metadatattCrivHandle.setFieldMetaData(22, "msgctrl", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(23, "cmotmvk", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(24, "valmotif", 2, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(25, "type", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(26, "tsaizero", 0, Parameter.PRO_LOGICAL);
            metadatattCrivHandle.setFieldMetaData(27, "crifaInfo", 0, Parameter.PRO_CHARACTER);
            metadatattCrivHandle.setFieldMetaData(28, "cstade", 0, Parameter.PRO_CHARACTER);
        }
        /* CHECKSTYLE:ON */
        return metadatattCrivHandle;
    }

    /**
     * convert a record set to a Temp table.
     *
     * @param params Progress ParamArray containing parameters
     * @param nbParam Current param number
     * @return List&lt;XxfabPPOTtCrivHandle&gt; Convert a record set to a list of XxfabPPOTtCrivHandle
     * @throws ProgressBusinessException Thrown if any exception has been catched
     */
    private List<XxfabPPOTtCrivHandle> convertRecordttCrivHandle(final ParamArray params, final int nbParam) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a list from the record set for XxfabPPOTtCrivHandle");
        }
        
        ProResultSet rs = null;
        List<XxfabPPOTtCrivHandle> list = new ArrayList<XxfabPPOTtCrivHandle>();
        try {
            rs = (ProResultSet) params.getOutputParameter(nbParam);
            while (rs.next()) {
                XxfabPPOTtCrivHandle tempTable = new XxfabPPOTtCrivHandle();
                /* CHECKSTYLE:OFF */
                tempTable.setCsoc(rs.getString("csoc"));
                tempTable.setCetab(rs.getString("cetab"));
                tempTable.setTypcle(rs.getString("typcle"));
                tempTable.setCle(rs.getString("cle"));
                tempTable.setCcri(rs.getString("ccri"));
                tempTable.setCunite(rs.getString("cunite"));
                tempTable.setNi1(rs.getInt("ni1"));
                tempTable.setNlig(rs.getInt("nlig"));
                tempTable.setTsaimodif(rs.getBoolean("tsaimodif"));
                GregorianCalendar gvaldmin = rs.getGregorianCalendar("valdmin");
                tempTable.setValdmin(gvaldmin != null ? gvaldmin.getTime() : null);
                GregorianCalendar gvaldmax = rs.getGregorianCalendar("valdmax");
                tempTable.setValdmax(gvaldmax != null ? gvaldmax.getTime() : null);
                tempTable.setValnmin(rs.getDouble("valnmin"));
                tempTable.setValnmax(rs.getDouble("valnmax"));
                tempTable.setValcmin(rs.getString("valcmin"));
                tempTable.setValcmax(rs.getString("valcmax"));
                tempTable.setLibaff(rs.getString("libaff"));
                tempTable.setMsaisie(rs.getString("msaisie"));
                tempTable.setTexclus(rs.getBoolean("texclus"));
                tempTable.setTsaioblig(rs.getBoolean("tsaioblig"));
                tempTable.setCorig(rs.getString("corig"));
                tempTable.setTanomotif(rs.getBoolean("tanomotif"));
                tempTable.setMsgctrl(rs.getString("msgctrl"));
                tempTable.setCmotmvk(rs.getString("cmotmvk"));
                 // Extent valmotif
                String[] valmotif = new String[2];
                for (int extent = 0; extent < 2; extent++) {
                    valmotif[extent] = rs.getString("valmotif", extent + 1);
                }
                tempTable.setValmotif(valmotif);
                tempTable.setType(rs.getString("type"));
                tempTable.setTsaizero(rs.getBoolean("tsaizero"));
                tempTable.setCrifaInfo(rs.getString("crifaInfo"));
                tempTable.setCstade(rs.getString("cstade"));
                /* CHECKSTYLE:ON */
                list.add(tempTable);
            }
        } catch (SQLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } catch (Open4GLException e) {
            if (LOGGER.isEnabledFor(Level.DEBUG)) {
                LOGGER.debug(e.getMessage());
            }
            throw new ProgressBusinessException(e.getMessage());
        } finally {
            try {
                rs.close();
            } catch (SQLException e) {
                if (LOGGER.isEnabledFor(Level.DEBUG)) {
                    LOGGER.debug(e.getMessage());
                }
                throw new ProgressBusinessException(e.getMessage());
            }
        }
         
        return list;
    }

    /**
     * Convert a list of TempTable to a RecordSet.
     *
     * @param ttCrivHandle List of XxfabPPOTtCrivHandle
     * @return ProInputTempTable Convert a record set to a list of XxfabPPOTtCrivHandle
     * @throws ProgressBusinessException Progress Exception
     */
    private static ProInputTempTable convertTempTablettCrivHandle(final List<XxfabPPOTtCrivHandle> ttCrivHandle) throws ProgressBusinessException {
        
        // ------------------------------------------
        // Generate a list from the record set
        // ------------------------------------------
        
        if (LOGGER.isEnabledFor(Level.DEBUG)) {
            LOGGER.debug("Generate a ProInputTempTable from the record set for XxfabPPOTtCrivHandle");
        }
        ProInputTempTable tempTable = new ProInputTempTable();
        // add element of the list 
        Iterator<XxfabPPOTtCrivHandle> iter = ttCrivHandle.iterator();
        while (iter.hasNext()) {
            XxfabPPOTtCrivHandle row = iter.next();
            List<Object> list = new ArrayList<Object>();
            // Add each field to row
            /* CHECKSTYLE:OFF */
            list.add(row.getCsoc());
            list.add(row.getCetab());
            list.add(row.getTypcle());
            list.add(row.getCle());
            list.add(row.getCcri());
            list.add(row.getCunite());
            list.add(row.getNi1());
            list.add(row.getNlig());
            list.add(row.getTsaimodif());
            list.add(dateToGreg(row.getValdmin()));
            list.add(dateToGreg(row.getValdmax()));
            list.add(new BigDecimal(row.getValnmin()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(new BigDecimal(row.getValnmax()).setScale(4, RoundingMode.HALF_EVEN));
            list.add(row.getValcmin());
            list.add(row.getValcmax());
            list.add(row.getLibaff());
            list.add(row.getMsaisie());
            list.add(row.getTexclus());
            list.add(row.getTsaioblig());
            list.add(row.getCorig());
            list.add(row.getTanomotif());
            list.add(row.getMsgctrl());
            list.add(row.getCmotmvk());
            // Extent valmotif
            for (int extent = 0; extent < 2; extent++) {
                list.add(row.getValmotif()[extent]);
            }
            list.add(row.getType());
            list.add(row.getTsaizero());
            list.add(row.getCrifaInfo());
            list.add(row.getCstade());
            /* CHECKSTYLE:ON */
            tempTable.addRow(list);
        }
         
        return tempTable;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public String getProgName() {
        return "xxfabpxy.p";
    }

}

/*
 * Copyright (c) 2013 by VIF (Vignon Informatique France)
 * Project : VIF_VIF57_PRODUCTION_MO_SBS
 * File : $RCSfile: MarkingSBSImpl.java,v $
 * Created on 16 Jul 2013 by cj
 */
package fr.vif.vif5_7.production.mo.business.services.libraries.marking;


import org.apache.log4j.Logger;
import fr.vif.jtech.business.VIFContext;
import fr.vif.jtech.business.exceptions.ProxyBusinessException;
import fr.vif.jtech.business.exceptions.ServerBusinessException;
import fr.vif.jtech.business.progress.BooleanHolder;
import fr.vif.jtech.business.progress.StringHolder;
import fr.vif.vif5_7.production.mo.business.beans.common.marking.MarkingBean;
import org.springframework.stereotype.Component;


/**
 * SBS for marking feature.
 * 
 * @author mnt
 */
@Component
public class MarkingSBSImpl implements MarkingSBS {

    /** LOGGER. */
    private static final Logger LOGGER = Logger.getLogger(MarkingSBSImpl.class);

    /**
     * {@inheritDoc}
     */
    @Override
    public void startPrinting(final VIFContext ctx, final MarkingBean markingbean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - startPrinting(ctx=" + ctx + ", markingbean=" + markingbean + ")");
        }
        StringHolder oMess = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        Marking01PPO ppo = new Marking01PPO(ctx);
        ppo.startPrinting(markingbean.getOperationItemKey().getEstablishmentKey().getCsoc(), //
                markingbean.getOperationItemKey().getEstablishmentKey().getCetab(), //
                markingbean.getOperationItemKey().getManagementType().getValue(), // typgest
                markingbean.getOperationItemKey().getChrono().getPrechro(), // prechrof
                markingbean.getOperationItemKey().getChrono().getChrono(), // chronof
                markingbean.getOperationItemKey().getCounter1(), // ni1
                markingbean.getOperationItemKey().getCounter2(), // ni2
                markingbean.getOperationItemKey().getCounter3(), // ni3
                markingbean.getOperationItemKey().getCounter4(), // ni4
                ctx.getIdCtx().getLogin(), //
                markingbean.getWorkstationId(), //
                markingbean.getFeatureId(), //
                markingbean.getHardwareKeyType().getValue(), //
                markingbean.getDeviceId(), //
                markingbean.getDocumentId(), //
                oRet, oMess);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMess.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - startPrinting(ctx=" + ctx + ", markingbean=" + markingbean + ")");
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void stopPrinting(final VIFContext ctx, final MarkingBean markingbean) throws ServerBusinessException {
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("B - stopPrinting(ctx=" + ctx + ", markingbean=" + markingbean + ")");
        }
        StringHolder oMess = new StringHolder();
        BooleanHolder oRet = new BooleanHolder();
        Marking01PPO ppo = new Marking01PPO(ctx);
        ppo.stopPrinting(markingbean.getOperationItemKey().getEstablishmentKey().getCsoc(), //
                markingbean.getOperationItemKey().getEstablishmentKey().getCetab(), //
                ctx.getIdCtx().getLogin(), //
                markingbean.getWorkstationId(), //
                markingbean.getFeatureId(), //
                markingbean.getHardwareKeyType().getValue(), //
                markingbean.getDeviceId(), //
                oRet, oMess);
        if (!oRet.getBooleanValue()) {
            throw new ProxyBusinessException(oMess.getStringValue());
        }
        if (LOGGER.isDebugEnabled()) {
            LOGGER.debug("E - stopPrinting(ctx=" + ctx + ", markingbean=" + markingbean + ")");
        }
    }
}

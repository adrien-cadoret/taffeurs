&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Tablejf _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actsstjf.w
 *
 * Description  : SmartFonction pour la gestion de la sous-traitance d'une activit�
 * 
 * Template     : fonction.w - SmartFonction 
 * Type Objet   : SmartFonction 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 11/02/2016 � 09:17 par AC
 *
 * HISTORIQUE DES MODIFICATIONS :
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME ACT

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Tablejf _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Tablejf 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/
/*** Constante pour Fonctions ***/
/* Nombre de pages � initialiser d�s le d�but de la fonction */  
&SCOPED-DEFINE Nombre-Pages 0
/* Titre de la Fonction */
&SCOPED-DEFINE TitreFrame "Sans Titre":T
/* Confirmation de la validation des modifications */
&SCOPED-DEFINE Verif-Modifs-Validees YES
/* Tester si record courant non modifi� avant appel sousfct */
&SCOPED-DEFINE EL-SousFct-CurrentChanged NO
/* Ecran Graphique susceptible d'�tre Tactile OU Ecran Tactile*/
&SCOPED-DEFINE EcranTactile NO
/* Autorise la retaille de l'�cran */
&SCOPED-DEFINE FctResizable NO
/* Nom de programme � afficher en bas de fen�tre */
/* &SCOPED-DEFINE NomProg-A-Afficher   */ 


/*** Variables globales ***/
{variable.i " "}
{admvif/template/fonction.i "Definitions"}

&IF DEFINED(UIB_is_Running) NE 0 &THEN
/* Attributs de depart pour test SmartFonction */
Attributs-De-Depart = "".
&ENDIF

/*----------------------------------------------------------------------*/

/* D�finition des variables globales */
/* DEFINE VARIABLE vgCsoc AS CHARACTER NO-UNDO. */

/* D�finition des param�tres globaux */
 /*DEFINE INPUT PARAMETER i-csoc AS CHARACTER NO-UNDO.
 DEFINE INPUT PARAMETER i-cetab AS CHARACTER NO-UNDO.
 DEFINE INPUT PARAMETER i-cact AS CHARACTER NO-UNDO.*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" Tablejf _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartFonction
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER FRAME

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Custom List Definitions                                              */
/* Btn-SF,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_actsstjv AS HANDLE NO-UNDO.

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 159.2 BY 22.05
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartFonction
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Container Links: Page-Target,Record-Source,Record-Target,Navigation-Source,Navigation-Target,TableIO-Source,TableIO-Target
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Tablejf ASSIGN
         HEIGHT             = 22.19
         WIDTH              = 159.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "SmartFrameCues" Tablejf _INLINE
/* Actions: adecomm/_so-cue.w ? adecomm/_so-cued.p ? adecomm/_so-cuew.p */
/* SmartFonction,uib,49268
Destroy on next read */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" Tablejf _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Tablejf 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_fonct.i}
{src/adm/method/containr.i}
{admvif/method/ap_fonct.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW Tablejf
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE                                                          */
ASSIGN 
       FRAME F-Main:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main Tablejf
ON HELP OF FRAME F-Main
DO:
   /* Gestion standard de l'aide en ligne */
   {admvif/template/tinclude.i "Help"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Tablejf 


{admvif/template/fonction.i "Triggers"}
{admvif/template/fonction.i "Main-Block"}
{admvif/template/fonction.i "Procedures-Internes"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects Tablejf  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actsstjv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Hide-Links-With-Object = No,
                     Initial-Lock = Default,
                     Dern-Champ-Go = No':U ,
             OUTPUT h_actsstjv ).
       RUN set-position IN h_actsstjv ( 5.86 , 4.20 ) NO-ERROR.
       RUN set-size IN h_actsstjv ( 11.43 , 156.00 ) NO-ERROR.

       /* Adjust the tab order of the smart objects. */
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available Tablejf  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Tablejf  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Tablejf  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-entree Tablejf 
PROCEDURE local-adv-entree :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-entree':U ) .
  
  /*MESSAGE "Params = " + i-csoc + " " + i-cetab + " " + i-cact.*/

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PrAncrageWidget Tablejf 
PROCEDURE PrAncrageWidget :
/*------------------------------------------------------------------------------
  Purpose:     Ancrage de tous les Widgets (Fiil-In, Combo...) pos�s sur la frame
               du container.
  Parameters:  <none>
  Notes:      Pour les SmartObjects (SmartBrowser, SmartViewer il faut passer par les instances 
              d'attributs 
------------------------------------------------------------------------------*/
/* DO WITH FRAME {&FRAME-NAME}:                                         */
/*    Pour donner la possibilit� de r�duire la taille mini d'un browse  */
/*    {admvif/method/fctresizerun.i &Proc="SmartBrwMinimize"            */
/*                                  &I-SmartBrowser="HBrw:Handle"}      */
/*    {admvif/method/fctresizerun.i &Proc="AttachObject"                */
/*                                  &I-Handle="widget:Handle"           */
/*                                  &I-Attach="'horizontal,vertical':U" */
/*                                  &I-Width-Resizable=Yes/NO           */
/*                                  &I-Height-Resizable=Yes/NO          */
/*                                  &I-GroupName="'NomGroupe':U"}       */
/* END. */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records Tablejf  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartFonction, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed Tablejf 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.
  
  CASE p-state:
      /* Placez vos cases ici pour remplacez les states standard ou
         en mettre des nouveaux */

      /* Cases standard des fonction */
     {admvif/template/fonction.i "Fctstates"}
         
      /* Cases standard des container */
      {admvif/template/cinclude.i "cstates"}
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


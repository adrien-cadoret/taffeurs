&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialsai
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" D-Dialsai _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\resgjd.w
 *
 * Description  : Affectation d'un groupe � une ressource
 * 
 * Template     : Dialsai.w - Smartdialog de saisie 
 * Type Objet   : SmartDialogsai 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 12/06/1997 � 12:12 par DB
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 06/10/2009 - SLE     : P_1076:Modularit� : Modif r�pertoire
 *                              20091006_0001 (17079)|P_1076|Modularit� : Modif r�pertoire|SLE|06/10/09 14:56:43 
 *                               
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME ACT

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" D-Dialsai _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialsai 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

/* Constantes de la dialogue-box */
&Scoped-Define Confirme-Escape No

{admvif/template/dialsai.i "Definitions" }

/* Parameters Definitions ---                                           */

/* Variables globales */
{variable.i " "}

&Scoped-define Temp-Table Tt-res

Define Shared Temp-Table {&Temp-Table} No-Undo
  Field cres  Like kres.cres
  Field lres  Like kres.lres
  Field cresm Like kres.cresm
  /* Ajout SLE 18/01/05 */
  FIELD lotd  Like kres.lotd
  FIELD lotq  Like kres.lotq
  FIELD lotqu Like kres.lotqu.
  
/* Local Variable Definitions ---                                       */
Define Input        Parameter i-typres    Like kres.typres  No-Undo.
Define Input        Parameter i-cnatres   Like Kres.cnatres No-Undo.
Define Input-Output Parameter i-o-wres    Like kres.cres    No-Undo.
Define Output       Parameter o-ret       As Logical        No-Undo.
Define Output       Parameter o-lres      Like kres.lres    No-Undo.
Define Output       Parameter o-cresm     Like kres.cresm   No-Undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" D-Dialsai _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialogSai
&Scoped-define DB-AWARE no

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME D-Dialsai

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES kres

/* Definitions for DIALOG-BOX D-Dialsai                                 */
&Scoped-define QUERY-STRING-D-Dialsai FOR EACH kres NO-LOCK
&Scoped-define OPEN-QUERY-D-Dialsai OPEN QUERY D-Dialsai FOR EACH kres NO-LOCK.
&Scoped-define TABLES-IN-QUERY-D-Dialsai kres
&Scoped-define FIRST-TABLE-IN-QUERY-D-Dialsai kres


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS fl-cres B_AID- Btn_OK Btn_Annuler Btn_Aide 
&Scoped-Define DISPLAYED-OBJECTS fl-cres 

/* Custom List Definitions                                              */
/* List-1,List-2,L-Oblig,L-Field,L-Majuscule,List-6                     */
&Scoped-define L-Field fl-cres 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Auto-Definitions" D-Dialsai _INLINE
/* Actions: ? ? ? ? admvif/xftr/wrdef.p */

/* ********** Preprocessor Definitions VIF ***********/
&Scoped-Define List-Champs-Auto fl-cres
&If Defined(List-Champs-Auto) <> 0 &Then
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&List-Champs-Auto},{&User-List-Champs}
   &Else
      &Scoped-Define List-champs {&List-Champs-Auto}
   &Endif
&Else
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&User-List-Champs}
   &Endif
&Endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Aide 
     LABEL "Ai&de" 
     SIZE 15 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON Btn_Annuler 
     LABEL "Annuler" 
     SIZE 15 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON B_AID- 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE VARIABLE fl-cres AS CHARACTER FORMAT "X(10)":U 
     LABEL "Ressource" 
     VIEW-AS FILL-IN 
     SIZE 18.4 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY D-Dialsai FOR 
      kres SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialsai
     fl-cres AT ROW 1.52 COL 13.4 COLON-ALIGNED
     B_AID- AT ROW 1.52 COL 34.6
     kres.lres AT ROW 1.52 COL 37.4 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 52.8 BY 1
     Btn_OK AT ROW 3 COL 43.4
     Btn_Annuler AT ROW 3 COL 59.4
     Btn_Aide AT ROW 3 COL 75.4
     SPACE(2.00) SKIP(0.48)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 6
         TITLE "Ressource"
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialogSai
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialsai 
/* ************************* Included-Libraries *********************** */

{admvif/method/dialsaim.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX D-Dialsai
                                                                        */
ASSIGN 
       FRAME D-Dialsai:SCROLLABLE       = FALSE
       FRAME D-Dialsai:HIDDEN           = TRUE.

ASSIGN 
       B_AID-:PRIVATE-DATA IN FRAME D-Dialsai     = 
                "Fonction=Combo-Aide".

/* SETTINGS FOR FILL-IN fl-cres IN FRAME D-Dialsai
   4                                                                    */
/* SETTINGS FOR FILL-IN kres.lres IN FRAME D-Dialsai
   NO-DISPLAY NO-ENABLE                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialsai
/* Query rebuild information for DIALOG-BOX D-Dialsai
     _TblList          = "vif5_7.kres"
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialsai */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" D-Dialsai _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialsai
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialsai D-Dialsai
ON END-ERROR OF FRAME D-Dialsai /* Ressource */
DO:
   {admvif/template/dialsai.i "End-Error"}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialsai D-Dialsai
ON ENDKEY OF FRAME D-Dialsai /* Ressource */
DO:
  {admvif/template/dialsai.i "End-Key"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialsai D-Dialsai
ON GO OF FRAME D-Dialsai /* Ressource */
DO:
  {admvif/template/dialsai.i "Go"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialsai D-Dialsai
ON WINDOW-CLOSE OF FRAME D-Dialsai /* Ressource */
DO:  
  /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
  APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Aide
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Aide D-Dialsai
ON CHOOSE OF Btn_Aide IN FRAME D-Dialsai /* Aide */
OR HELP OF FRAME {&FRAME-NAME}
DO: /* Call Help Function (or a simple message). */
  {admvif/template/tinclude.i "Help" }
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Annuler
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Annuler D-Dialsai
ON CHOOSE OF Btn_Annuler IN FRAME D-Dialsai /* Annuler */
DO:
  {admvif/template/dialsai.i "Choose-Annuler"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID- D-Dialsai
ON CHOOSE OF B_AID- IN FRAME D-Dialsai /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID- D-Dialsai
ON ENTRY OF B_AID- IN FRAME D-Dialsai /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialsai 


/* ***************************  Main Block  *************************** */

{admvif/template/dialsai.i "Triggers" }

{admvif/template/dialsai.i "Main-Block" }

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialsai 
PROCEDURE adm-create-objects :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialsai 
PROCEDURE adm-row-available :
/* -----------------------------------------------------------
  Purpose:     Ask if there are rows available.
  Parameters:  <none>      
-------------------------------------------------------------*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Aide-Sur-Champ D-Dialsai 
PROCEDURE Aide-Sur-Champ :
/*------------------------------------------------------------------------------
  Purpose:     Aide (liste de choix) Sur les Champs
  Parameters:  Handle du widget active
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Champ  As Widget-Handle No-Undo.
    Define Variable ChoixEffectue As Character No-Undo.

    Case Champ:Name :
       When "fl-cres":U Then Do :
          {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("resjf":U,             /* code de la fonction */
                       "resjb":U, "typres=":U + i-typres + ",cnatres=":U +
                                  i-cnatres + ",cresm=TOU,cetat=,cresg=":U,   /* Nom browser et attributs */
                       " ":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       No,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
          If ChoixEffectue <> ? Then 
             Champ:Screen-Value = ChoixEffectue.
       End.

       Otherwise ChoixEffectue = ?.
    End Case.

    If ChoixEffectue <> ? Then 
       Apply "Tab":U To Champ.
    Else
       Apply "Entry":U To Champ.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialsai  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialsai.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialsai  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY fl-cres 
      WITH FRAME D-Dialsai.
  ENABLE fl-cres B_AID- Btn_OK Btn_Annuler Btn_Aide 
      WITH FRAME D-Dialsai.
  VIEW FRAME D-Dialsai.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialsai}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-Assign-Statement D-Dialsai 
PROCEDURE local-Assign-Statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  Assign i-o-wres = fl-cres:Screen-Value In Frame {&Frame-Name}
         o-ret    = Yes.
 
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'Assign-Statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-Display-Fields D-Dialsai 
PROCEDURE local-Display-Fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'Display-Fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  {rescc.i fl-cres no yes kres.lres 1}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialsai 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  FIND FIRST {&TEMP-TABLE} WHERE {&TEMP-TABLE}.cres = i-o-wres NO-ERROR.
  Assign fl-cres = i-o-wres.
  
  FOR EACH {&TEMP-TABLE} :
   
   MESSAGE "available" + {&TEMP-TABLE}.cres.
  
  END.
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Test-Saisie D-Dialsai 
PROCEDURE Local-Test-Saisie :
/*------------------------------------------------------------------------------
  Purpose:     Tester la saisie des champ 
  Parameters:  - Test Global, ou Pour un widget 
               - Handle du widget � tester.
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Test-Global As Logical       No-Undo.
    Define Input Parameter Widget-Test As Widget-Handle No-Undo.

    DEFINE BUFFER B-{&TEMP-TABLE} FOR {&TEMP-TABLE}.
    
    If Test-Global 
    Or Widget-Test = fl-cres:Handle In Frame {&Frame-Name} 
    Then Do :
       Assign o-lres = "".
       {rescc.i fl-cres yes yes kres.lres 1}
       If Available buf1-kres Then Do:
         
         If buf1-kres.typres <> i-typres Or buf1-kres.cnatres <> i-cnatres Then Do:
           {affichemsg.i   
               &Message = "Get-TradMsg (004293, 'Votre ressource n''est pas coh�rente avec le groupe':T)"
               &TypeMsg = {&MsgWarning}
           }
           Apply "Entry":U To fl-cres In Frame {&Frame-Name}.
           Return "Adm-Error":U.
         End.
         
         If buf1-kres.cresg <> "" Then Do:
           {affichemsg.i   
               &Message = "Get-TradMsg (004305, 'Cette ressource appartient d�j� � un autre groupe':T)"
               &TypeMsg = {&MsgWarning}
           }
           Apply "Entry":U To fl-cres In Frame {&Frame-Name}.
           Return "Adm-Error":U.
         End.
         /* Pas deux ressources dans le groupe avec la m�me ressource m�re */
         If buf1-kres.cresm <> "" And 
            CAN-FIND (First B-{&Temp-Table} Where B-{&Temp-Table}.cresm = buf1-kres.cresm)
         Then Do:
           {affichemsg.i   
               &Message = "Get-TradMsg (004309, 'Il ne peut pas y avoir deux ressources avec la | m�me ressource m�re dans un m�me groupe':T)"
               &TypeMsg = {&MsgWarning}
           }
           Apply "Entry":U To fl-cres In Frame {&Frame-Name}.
           Return "Adm-Error":U.
         End.
       End.

       If CAN-FIND (First B-{&Temp-Table} Where B-{&Temp-Table}.cres = fl-cres:Screen-Value In Frame {&Frame-Name}) 
       Then Do:
         {affichemsg.i   
               &Message = "Get-TradMsg (004324, 'Vous avez d�j� mentionn� cette ressource pour ce groupe':T)"
               &TypeMsg = {&MsgWarning}
           }
         Apply "Entry":U To fl-cres In Frame {&Frame-Name}.
         Return "Adm-Error":U.
       End.
       
       Assign o-lres  = buf1-kres.lres
              o-cresm = buf1-kres.cresm.
       
       /* SLE 18/01/05 */
       IF Test-Global AND AVAILABLE buf1-kres THEN
         ASSIGN {&TEMP-TABLE}.lotq  = buf1-kres.lotq
                {&TEMP-TABLE}.lotqu = buf1-kres.lotqu
                {&TEMP-TABLE}.lotd  = buf1-kres.lotd.
    End.
    
    Return.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialsai 
PROCEDURE send-records :
/* -----------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i      
-------------------------------------------------------------*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialsai 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


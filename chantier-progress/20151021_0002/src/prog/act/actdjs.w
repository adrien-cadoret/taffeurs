&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actdjs.w
 *
 * Description  : SmartSousFct sur Activit� par date d'effet
 * 
 * Template     : sousfct.w - SmartSous-Fonction  
 * Type Objet   : SmartSousFct 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 01/08/1996 � 00:00 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 22/12/2014 - MB      : DP001776:Activit�s : dates d'effet "historiques" � ignorer
 *                              20140623_0008 (25599)|DP001776|Activit�s : dates d'effet "historiques" � ignorer|MB|22/12/14 09:36:11 
 *                               
 *  - Le 18/08/2014 - SLE     : A006751:Gestion des PJ en copie d'activit�
 *                              20130827_0010 (23986)|A006751|Gestion des PJ en copie d'activit�|SLE|18/08/14 16:44:54 
 *                               M139113
 *                              
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" F-Frame-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/standard.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS F-Frame-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

/* Boite de dialogue pour les attributs */

/* Parameters Definitions ---                                           */

/* Constantes propres � la fonction */
&Scoped-Define TitreFrame "Dates d'effet des activit�s":T
&Scoped-Define Nombre-Pages 2

/* N� Contexte Aide en ligne */

/* Variables globales */
{variable.i " "}
{vif4bibi.i} /* contr�le droit fonction conditionnelle */

/* Local Variable Definitions ---                                       */
{admvif\template\sousfct.i "Definitions"}

/* Sous-Fonction de saisie fiche (dont le browse est sur la fct appelante) */
&Global-Define Gestion-Saisie-Fiche No
/* Faut-il garder en memoire la sous-fonction apres le 1er appel ? */
/* Ne modifier que si gestion-saisie-fiche = no */
&Global-Define Garder-En-Memoire Yes

DEFINE VARIABLE vgCact AS CHARACTER NO-UNDO.
/* MB P2374 */
DEFINE VARIABLE vgcArtRef AS CHARACTER NO-UNDO.
/* MB M139113 20130827_0010 */
DEFINE VARIABLE vgModefct AS CHARACTER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" F-Frame-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartSousFct
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER FRAME

&Scoped-define ADM-SUPPORTED-LINKS Record-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-SousFct

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kact
&Scoped-define FIRST-EXTERNAL-TABLE kact


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kact.
/* Custom List Definitions                                              */
/* Btn-SF,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_actd2jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actd3jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actd4jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actdjb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actdjv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_com1jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_criv2jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_criv2jv-2 AS HANDLE NO-UNDO.
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-updsav AS HANDLE NO-UNDO.

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-SousFct
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 159.2 BY 15.86
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartSousFct
   External Tables: vif5_7.kact
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Container Links: Page-Target,Record-Source,Record-Target,Navigation-Source,Navigation-Target,TableIO-Source,TableIO-Target
   Design Page: 3
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW F-Frame-Win ASSIGN
         HEIGHT             = 16.19
         WIDTH              = 160.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB F-Frame-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_sfct.i}
{src/adm/method/containr.i}
{admvif/method/ap_sfct.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW F-Frame-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-SousFct
   NOT-VISIBLE FRAME-NAME                                               */
ASSIGN 
       FRAME F-SousFct:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-SousFct
/* Query rebuild information for FRAME F-SousFct
     _Options          = ""
     _Query            is NOT OPENED
*/  /* FRAME F-SousFct */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-SousFct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-SousFct F-Frame-Win
ON HELP OF FRAME F-SousFct
DO:
  /* Gestion standard de l'aide en ligne */
  {admvif/template/tinclude.i "Help"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK F-Frame-Win 


{admvif\template\sousfct.i "Triggers"}

{admvif\template\sousfct.i "Main-Block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects F-Frame-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actdjb.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ':U ,
             OUTPUT h_actdjb ).
       RUN set-position IN h_actdjb ( 1.00 , 67.40 ) NO-ERROR.
       RUN set-size IN h_actdjb ( 4.14 , 21.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/p-updsav.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'SmartPanelType = Save,
                     Edge-Pixels = 0,
                     SaveFunction = Normal,
                     NameBtnAction = ,
                     Hide-Save = no,
                     Hide-Add = no,
                     Hide-Copy = no,
                     Hide-Delete = no,
                     Hide-cancel = no,
                     Hide-Print = yes,
                     Hide-Documents = no,
                     PrintFunction = Normale,
                     Layout-Options = "Master Layout,Standard Character",
                     Default-Layout = Master Layout,
                     OrientPanel = Horizontal':U ,
             OUTPUT h_p-updsav ).
       RUN set-position IN h_p-updsav ( 1.29 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 1.86 , 8.20 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/folder.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'FOLDER-TAB-TYPE = 2,
                     Folder-Labels = ':U + 'G�n�ral|Temps|Rdt dynamique' + ',
                     Ancrage = LEFT�BOTTOM�NO�NO�':U ,
             OUTPUT h_folder ).
       RUN set-position IN h_folder ( 4.14 , 1.00 ) NO-ERROR.
       RUN set-size IN h_folder ( 12.38 , 159.20 ) NO-ERROR.

       /* Links to SmartBrowser h_actdjb. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Record':U , h_actdjb ).

       /* Links to SmartPanel h_p-updsav. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Panel':U , h_p-updsav ).

       /* Links to SmartFolder h_folder. */
       RUN add-link IN adm-broker-hdl ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_folder ,
             h_actdjb , 'AFTER':U ).
    END. /* Page 0 */
    WHEN 1 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actd3jv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Initial-Lock = Default,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non':U ,
             OUTPUT h_actd3jv ).
       RUN set-position IN h_actd3jv ( 6.00 , 24.00 ) NO-ERROR.
       RUN set-size IN h_actd3jv ( 1.24 , 41.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'com1jv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'cetab = ,
                     csoc = ,
                     Titre = Commentaires,
                     typcle = {&INIT-COM-ACT},
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_com1jv ).
       RUN set-position IN h_com1jv ( 7.43 , 33.40 ) NO-ERROR.
       RUN set-size IN h_com1jv ( 7.38 , 82.40 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actd2jv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Initial-Lock = Default,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non':U ,
             OUTPUT h_actd2jv ).
       RUN set-position IN h_actd2jv ( 14.81 , 85.80 ) NO-ERROR.
       RUN set-size IN h_actd2jv ( 1.43 , 74.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'criv2jv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Layout-Options = "Master Layout,Standard Character",
                     aff-statut = {&INIT-ZCRI-AFF-STATUT-NON},
                     longueur = 300,
                     mode = {&INIT-ZCRI-MODE-MODIF},
                     mode-statut = {&INIT-ZCRI-MODE-STATUT-CONSULT},
                     table = kactcriv,
                     tqualite = OUI,
                     valeur-attendue = {&INIT-ZCRI-VALEUR-ATTENDUE-NON},
                     Hide-Links-With-Object = No,
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Default-Layout = Master Layout,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_criv2jv ).
       RUN set-position IN h_criv2jv ( 14.86 , 17.80 ) NO-ERROR.
       RUN set-size IN h_criv2jv ( 1.19 , 6.20 ) NO-ERROR.

       /* Links to SmartViewer h_actd3jv. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actd3jv ).
       RUN add-link IN adm-broker-hdl ( h_p-updsav , 'TableIO':U , h_actd3jv ).

       /* Links to SmartViewer h_com1jv. */
       RUN add-link IN adm-broker-hdl ( h_actd3jv , 'Group-Assign':U , h_com1jv ).
       RUN add-link IN adm-broker-hdl ( h_actd3jv , 'info2':U , h_com1jv ).
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_com1jv ).

       /* Links to SmartViewer h_actd2jv. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actd2jv ).

       /* Links to SmartViewerSpe h_criv2jv. */
       RUN add-link IN adm-broker-hdl ( h_actd3jv , 'criteres':U , h_criv2jv ).
       RUN add-link IN adm-broker-hdl ( h_actd3jv , 'Group-Assign':U , h_criv2jv ).
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_criv2jv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actd3jv ,
             h_folder , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_com1jv ,
             h_actd3jv , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_actd2jv ,
             h_com1jv , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_criv2jv ,
             h_actd2jv , 'AFTER':U ).
    END. /* Page 1 */
    WHEN 2 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actdjv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ':U ,
             OUTPUT h_actdjv ).
       RUN set-position IN h_actdjv ( 6.71 , 30.00 ) NO-ERROR.
       RUN set-size IN h_actdjv ( 8.81 , 108.20 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartViewer h_actdjv. */
       RUN add-link IN adm-broker-hdl ( h_actd3jv , 'Group-Assign':U , h_actdjv ).
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actdjv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actdjv ,
             h_folder , 'AFTER':U ).
    END. /* Page 2 */
    WHEN 3 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actd4jv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Initial-Lock = Default,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non':U ,
             OUTPUT h_actd4jv ).
       RUN set-position IN h_actd4jv ( 7.19 , 18.40 ) NO-ERROR.
       RUN set-size IN h_actd4jv ( 2.71 , 73.80 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'criv2jv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Layout-Options = "Master Layout,Standard Character",
                     aff-statut = {&INIT-ZCRI-AFF-STATUT-NON},
                     longueur = 300,
                     mode = {&INIT-ZCRI-MODE-MODIF},
                     mode-statut = {&INIT-ZCRI-MODE-STATUT-CONSULT},
                     table = kactcriv,
                     tqualite = OUI,
                     valeur-attendue = {&INIT-ZCRI-VALEUR-ATTENDUE-NON},
                     Hide-Links-With-Object = No,
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Default-Layout = Master Layout,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_criv2jv-2 ).
       RUN set-position IN h_criv2jv-2 ( 11.00 , 44.80 ) NO-ERROR.
       RUN set-size IN h_criv2jv-2 ( 1.19 , 6.20 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartViewer h_actd4jv. */
       RUN add-link IN adm-broker-hdl ( h_actd3jv , 'Group-Assign':U , h_actd4jv ).
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actd4jv ).
       RUN add-link IN adm-broker-hdl ( h_p-updsav , 'TableIO':U , h_actd4jv ).

       /* Links to SmartViewerSpe h_criv2jv-2. */
       RUN add-link IN adm-broker-hdl ( h_actd4jv , 'criteres':U , h_criv2jv-2 ).
       RUN add-link IN adm-broker-hdl ( h_actd4jv , 'Group-Assign':U , h_criv2jv-2 ).
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_criv2jv-2 ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actd4jv ,
             h_folder , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_criv2jv-2 ,
             h_actd4jv , 'AFTER':U ).
    END. /* Page 3 */

  END CASE.
  /* Select a Startup page. */
  IF adm-current-page eq 0 
  THEN RUN select-page IN THIS-PROCEDURE ( 1 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available F-Frame-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kact"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kact"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI F-Frame-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-SousFct.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI F-Frame-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  {&OPEN-BROWSERS-IN-QUERY-F-SousFct}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-donnees F-Frame-Win 
PROCEDURE envoi-donnees :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter w-rowid as rowid   no-undo.

define variable w-list-rowid    as character format "x(20)" no-undo.

    run send-records in h_actdjb ("kactd":U, output w-list-rowid).
    w-rowid = to-rowid(w-list-rowid).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-close F-Frame-Win 
PROCEDURE local-adv-close :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-close':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  if return-value <> "adm-error":U then 
    Run new-State('fin-dates-effet,container-source':U).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available F-Frame-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  define variable w-rowid-kactd as rowid no-undo.
  
  If Return-Value <> "Adm-Error":U And 
     Valid-Handle(Fonction-Source-Hdl) Then Do :
    run envoi-donnees in Fonction-Source-Hdl (output w-rowid-kactd).
    if w-rowid-kactd <> ? then
        run reposition-browse in h_actdjb (input w-rowid-kactd).
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-view F-Frame-Win 
PROCEDURE local-view :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vDroitFct   AS LOGICAL   NO-UNDO.
  DEFINE VARIABLE vTyplux     AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vCfmact     AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vtcFmRdtDyn AS LOGICAL   NO-UNDO.

  DEFINE BUFFER bKfmact FOR kfmact.

  /* Code placed here will execute PRIOR to standard behavior. */
  Run Get-Info ("F-cact":u, "Info-Source":U).
  vgCact = Return-Value.
  If vgCact <> "" AND 
     vgCact <> ?  Then
     Run SetTitle( Get-TradTxt (000321, 'Dates d''effet de l''activit�':T)
                   + ' ':U + vgCact).
  /* MB P2374 Recherche de l'art. r�f. pour envoi au viewer de rdt dynamique */
   Run Get-Info ("F-cartvu":U, "Info-Source":U).                 
   ASSIGN vgcArtRef = RETURN-VALUE.
   
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'view':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  Run Get-Info ("ModeFct":U, "Container-Source":U).
  /* MB M139113 20130827_0010 */
  ASSIGN vgModeFct = Return-Value.
  Run Set-Link-Attribute In Adm-Broker-Hdl (This-Procedure,'Panel-Target':U,
                         'Mode-Consult=':U + (If vgModeFct = "Consult":U Then "Oui":U Else "Non":U)).
  Run New-State ("Reinit-Panel,Panel-Target":U).

  Run Get-Info ("TypFlux":u, "Info-Source":U).
  ASSIGN vTyplux = Return-Value.
  Run Get-Info ("kact.cfmact":u, "Info-Source":U).
  ASSIGN vcfmact = Return-Value.
  

  /* Code placed here will execute AFTER standard behavior.    */
  
/*   CD DP2374 controle acc�s au rendement dynamique                                    */
/*   gestion rendement dynamique sur famille activit� et droit fonction conditionnelle  */
  FOR FIRST bKfmact FIELDS (tRdtDyn)
                     WHERE bKfmact.csoc   = wcsoc
                       AND bKfmact.cetab  = wcetab
                       AND bKfmact.cfmact = vcfmact
                     NO-LOCK:
     ASSIGN vtcFmRdtDyn = bKfmact.trdtdyn.                 
  END.
  {&Ctrl-Acces-Fonsoc}('&KRDTDYN':U, wcsoc, OUTPUT vDroitFct).
  IF vtcFmRdtDyn AND vDroitFct THEN
     RUN Enable-Folder-Page  in h_folder(3).
  ELSE run Disable-Folder-Page in h_folder(3).
  
  /* CD 03/07/2013 : V1 = que la teneur, que en flux tir� */
  IF vTyplux = {&TYPFLUX-POUSSE} THEN DO WITH FRAME {&FRAME-NAME}:
     run Disable-Folder-Page in h_folder(3).
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info F-Frame-Win 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.
   
   DEFINE VARIABLE vModeFct AS CHARACTER NO-UNDO.

   Case Name-Info :
       /* Ins�rer ici vos propres infos � recevoir */
       
       /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
       WHEN "DatefHisto":U THEN DO:
          Run Get-Info ("ModeFct":U, "Container-Source":U).
          ASSIGN vModefct = RETURN-VALUE.
          
          IF Valeur-Info BEGINS "o":U AND vModefct <> "Consult":U THEN DO:
             /* Il faut pouvoir supprimer et copier les dates d'effet "historiques" */
             RUN New-State ("ActiveOnly-Supprimer-Copier-Documents,Panel-Target":U).
          END.
          ELSE DO:
             /* On force la r�-activation de tous les boutons d�finis dans la sous-fonction */
             RUN New-State ("ActiveOnly-Valider-Nouveau-Abandon-Supprimer-Copier-Documents,Panel-Target":U).
             /* On grise tout seulement si on est en mode "Consultation" */
             Run Set-Link-Attribute In Adm-Broker-Hdl (This-Procedure,'Panel-Target':U,
                                                       'Mode-Consult=':U + (If vModefct = "Consult":U 
                                                                            Then "Oui":U Else "Non":U)).
             RUN New-State ("Reinit-Panel,Panel-Target":U).
          END.
       END.
       /* FIN DP001776 */

       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info F-Frame-Win 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
       /* Ins�rer ici vos propres infos � envoyer */
       WHEN 'CACT':U THEN 
          ASSIGN Valeur-Info = vgCact.
       
       WHEN 'cacttmp':U THEN 
          ASSIGN valeur-info = (If Available kact 
                                   THEN kact.cact 
                                   ELSE "":U).
       
       WHEN 'typflux':U THEN DO:
         Run Get-Info ("TypFlux":u, "Info-Source":U).
         ASSIGN Valeur-Info = Return-Value.
       END.
       
       WHEN 'cfmact':U THEN DO:
         Run Get-Info ("cfmact":u, "Info-Source":U).
         ASSIGN Valeur-Info = Return-Value.
       END.
       
       WHEN 'HANDLE':U THEN DO:
/*           /* vient de actjf.w */                                    */
/*           Run Get-Info ("HANDLE-SF-DATEF":u, "Container-Source":U). */
/*                                                                     */
/*           /* va � actd3jv.w */                                      */
/*           Valeur-Info = STRING(Return-Value).                       */
           Valeur-Info = STRING(THIS-PROCEDURE:HANDLE).
       END.
      
      /* MB P2374 Rdt dynamique */ 
      WHEN "cartref":U THEN
         ASSIGN Valeur-Info = vgcArtRef.
      
      /* MB M139113 20130827_0010 */
      WHEN "ModeFct":U THEN DO:
         ASSIGN Valeur-Info = vgModefct. 
      END.
      
       Otherwise 
          /* Par d�faut, envoi de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Send-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records F-Frame-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kact"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed F-Frame-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Placez vos cases ici pour remplacez les states standard ou
         en mettre des nouveaux */
         
      
      /* Cas standard des sous-fonctions */
      {admvif/template/sousfct.i "sfctstates"}
               
      /* Cases standard des container */
      {admvif/template/cinclude.i "cstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r2 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Tablejb _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actsstjb.w
 *
 * Description  : SmartBrowser sur les sous-traitants d'une activit�
 * 
 * Template     : browser.w - SmartBrowser basique 
 * Type Objet   : SmartBrowser 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 04/02/2016 � 14:32 par AC
 *
 * HISTORIQUE DES MODIFICATIONS :
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME ACT

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Tablejb _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/browserd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Tablejb 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/
/* Variables globales */
{variable.i " "}

/* constantes de pre-compilation */
&GLOBAL-DEFINE TABLE kactsst
&GLOBAL-DEFINE ltable Sous-traitants par activit�



/* Constantes pour calcul taille du browse */
&SCOPED-DEFINE Calcul-Size-Auto YES
&SCOPED-DEFINE Calcul-Avec-ScrollH NO
&SCOPED-DEFINE Browse-Avec-Selection NO
/* Constantes pour browse retaillable */
&SCOPED-DEFINE FctResizable NO
/* Autres constantes */
/* Repositionnement record toujours en bas du browse (par defaut : milieu) */
&SCOPED-DEFINE Position-Record-Down NO
  
/* Variable utilis�es pour le template des browser */
{admvif/template/binclude.i "definitions"}

/*----------------------------------------------------------------------*/

/* D�finition des variables globales */

DEFINE VARIABLE ModeFct AS CHARACTER NO-UNDO.

DEFINE VARIABLE w-cact AS CHARACTER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" Tablejb _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES {&TABLE} ksst

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table kactsst.csst ksst.lsst kactsst.priorite   
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table   
&Scoped-define SELF-NAME br_table
&Scoped-define QUERY-STRING-br_table FOR EACH {&TABLE} WHERE {&TABLE}.csoc = wcsoc             AND {&TABLE}.cetab = wcetab             AND {&TABLE}.cact = w-cact, ~
           EACH ksst OF kactsst      NO-LOCK  BY {&TABLE}.priorite
&Scoped-define OPEN-QUERY-br_table OPEN QUERY {&SELF-NAME} FOR EACH {&TABLE} WHERE {&TABLE}.csoc = wcsoc             AND {&TABLE}.cetab = wcetab             AND {&TABLE}.cact = w-cact, ~
           EACH ksst OF kactsst      NO-LOCK  BY {&TABLE}.priorite.
&Scoped-define TABLES-IN-QUERY-br_table {&TABLE} ksst
&Scoped-define FIRST-TABLE-IN-QUERY-br_table {&TABLE}
&Scoped-define SECOND-TABLE-IN-QUERY-br_table ksst


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Browse-TTY" Tablejb _INLINE
/* Actions: ? admvif/xftr/ettybr.w ? ? ? */
/********************** TAILLE DU BROWSE EN TTY **********/
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Complement-Display-Browse" Tablejb _INLINE
/* Actions: ? admvif/xftr/ecompbr.w ? ? admvif/xftr/wcompbr.p */
/********************** COMPLEMENT DISPLAY BROWSE **********/
/* Automatique=Oui*/
/* Pas de compl�ment-display-browse */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-br_table 
       MENU-ITEM m_Recherche-Code LABEL "&Recherche par le ????".


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      {&TABLE}, 
      ksst SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table Tablejb _FREEFORM
  QUERY br_table NO-LOCK DISPLAY
      kactsst.csst FORMAT "X(10)":u
ksst.lsst FORMAT "x(30)":u
kactsst.priorite FORMAT 99
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN NO-ROW-MARKERS SEPARATORS SIZE 137.8 BY 8.19
         FONT 6 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Tablejb ASSIGN
         HEIGHT             = 8.19
         WIDTH              = 137.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Tablejb 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_brows.i}
{src/adm/method/browser.i}
{admvif/method/ap_brows.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW Tablejb
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE Size-to-Fit                                              */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:POPUP-MENU IN FRAME F-Main             = MENU POPUP-MENU-br_table:HANDLE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _START_FREEFORM
OPEN QUERY {&SELF-NAME}
FOR EACH {&TABLE} WHERE {&TABLE}.csoc = wcsoc
            AND {&TABLE}.cetab = wcetab
            AND {&TABLE}.cact = w-cact,
    EACH ksst OF kactsst
     NO-LOCK
 BY {&TABLE}.priorite.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" Tablejb _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table Tablejb
ON MOUSE-SELECT-DBLCLICK OF br_table IN FRAME F-Main
DO:
   /* code standard pour le double-click, en cas de browse d'aide */
   {admvif/template/binclude.i "brsdbclick"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table Tablejb
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
   /* This code displays initial values for newly added or copied rows. */
   {admvif/template/binclude.i "brsentry"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table Tablejb
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
   {admvif/template/binclude.i "brsleave"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table Tablejb
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
   /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
   {admvif/template/binclude.i "brschnge"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Recherche-Code
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Recherche-Code Tablejb
ON CHOOSE OF MENU-ITEM m_Recherche-Code /* Recherche par le ???? */
DO:
   /* Recherche standard par le code */
   RUN dispatch IN THIS-PROCEDURE ('Adv-Recherche-Code':U).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Tablejb 


/* ***************************  Main Block  *************************** */
{admvif/template/binclude.i "triggers"}

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

{admvif/template/binclude.i "main-block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available Tablejb  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Tablejb  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-reopen-query Tablejb 
PROCEDURE local-adv-reopen-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-reopen-query':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  MESSAGE "query reopened".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize Tablejb 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  /* TODO Enlever ce truc l� : �a doit �tre dynamique */ 
    ASSIGN ModeFct = "Modifier".
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Position-Record Tablejb 
PROCEDURE Position-Record :
/*------------------------------------------------------------------------------
  Purpose:     Postionnement du browse en fonction d'un cle d'entree
  Parameters:  Cle d'entree (List Progress, avec comme separteur '|').
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER CleEntree AS CHARACTER NO-UNDO.

/*{admvif/template/binclude.i "Position-Record" "CleEntree" "{&Table}"
                            "{&Table}.csoc = wcsoc 
                            AND {&TABLE}.cle >= ENTRY(1,CleEntree,'|':U)"}*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PrAncrageWidget Tablejb 
PROCEDURE PrAncrageWidget :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* DO WITH FRAME {&FRAME-NAME}:                                             */
/*    {admvif/method/objresizerun.i &I-Proc="&AttachObjects"                */
/*                                  &I-Hansle="widget:handle"               */
/*                                  &I-Attach="'horiz�vert�height�width':U" */
/*                                  &I-Height-Resizable="YES/NO"            */
/*                                  &I-Width-Resizable="YES/NO"             */
/*                                  &I-Proc-Resize="proc"                   */
/*                                  &I-GroupName="'nomgroupe':U"}           */
/* END.                                                                     */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info Tablejb 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
   
      WHEN "Current-Cact" THEN DO:
        ASSIGN w-cact = valeur-info.
        
      END.
      WHEN "State" THEN DO:
        IF(valeur-info = "allow-adding") THEN DO :
           
        END.
        ELSE IF(valeur-info = "get-fields") THEN DO :
             
        END.
        
      END.

       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Cle-Record Tablejb 
PROCEDURE Send-Cle-Record :
/*------------------------------------------------------------------------------
  Purpose:     Renvoie la cle du record courant en une liste Progress
               separe par '|'
  Parameters:  <none>
  Notes:       Cette Procedure est surtout utilise quand le browse sert d'aide a
               la saisie.
------------------------------------------------------------------------------*/
DEFINE OUTPUT PARAMETER oCleRecord AS CHARACTER NO-UNDO.
     
/*IF AVAILABLE {&TABLE} THEN DO:
  oCleRecord = {&TABLE}.cle.
  RETURN.
END.

oCleRecord = ?.
RETURN.*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records Tablejb  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "{&TABLE}"}
  {src/adm/template/snd-list.i "ksst"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed Tablejb 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
     {admvif/template/binclude.i "bstates"}
  END CASE.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


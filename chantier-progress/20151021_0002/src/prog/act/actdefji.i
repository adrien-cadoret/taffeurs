&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v7r11
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Include _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actdefji.i
 *
 * Description  : Mise � jour des activit�s : include de d�finitions
 * 
 * Template     : include.i - Structured Include File 
 * Type Objet   : Include 
 * Compilable   : __COMPILE__NON 
 * Cr�� le      : 24/01/2005 � 17:36 par GLC
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 31/07/2013 - CBA     : P2374:Rendements dynamiques - ACT
 *                              20130416_0013 (23313)|P2374|Rendements dynamiques - ACT|CBA|31/07/13 11:56:47 
 *                               
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Include _INLINE
/* Actions: ? ? ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*----------------------------------------------------------------------*/
/*          Definitions de l'include                                    */
/*----------------------------------------------------------------------*/

&IF DEFINED (VAR-ACTJDEF) = 0 &THEN
   &GLOBAL-DEFINE VAR-ACTJDEF YES
   DEFINE TEMP-TABLE ttKact-APRES             NO-UNDO LIKE kact.
   DEFINE TEMP-TABLE ttKactsst-APRES          NO-UNDO LIKE kactsst.
   DEFINE TEMP-TABLE ttKactd-APRES            NO-UNDO LIKE kactd.
   DEFINE TEMP-TABLE ttKactdpop-APRES         NO-UNDO LIKE Kactdpop.
   DEFINE TEMP-TABLE ttKacta-APRES            NO-UNDO LIKE kacta.
   DEFINE TEMP-TABLE ttKactapop-APRES         NO-UNDO LIKE Kactapop.
   DEFINE TEMP-TABLE ttKactapopd-APRES        NO-UNDO LIKE Kactapopd.
   DEFINE TEMP-TABLE ttKactartr-APRES         NO-UNDO LIKE kactartr.
   DEFINE TEMP-TABLE ttKacto-APRES            NO-UNDO LIKE kacto.
   DEFINE TEMP-TABLE ttKactod-APRES           NO-UNDO LIKE kactod.
   DEFINE TEMP-TABLE ttKactctxu-APRES         NO-UNDO LIKE kactctxu.
   DEFINE TEMP-TABLE ttKcriv-APRES            NO-UNDO LIKE kactcriv.
   DEFINE TEMP-TABLE ttKcriv-Dat-APRES        NO-UNDO LIKE kactcriv.
   DEFINE TEMP-TABLE ttKcriv-Dat-RdtDyn-APRES NO-UNDO LIKE kactcriv.
   DEFINE TEMP-TABLE ttKcom-Dat-APRES         NO-UNDO LIKE kcom.
   DEFINE TEMP-TABLE ttKcom-Art-APRES         NO-UNDO LIKE kcom.
&ENDIF

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 4.57
         WIDTH              = 44.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 



/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



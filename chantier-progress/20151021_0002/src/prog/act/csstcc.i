&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Include _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\csstcc.i
 *
 * Description  : Contr�le include sur le csst de la table ksst
 * 
 * Template     : itable.i - Include pour libell� table 
 * Type Objet   : Include 
 * Compilable   : __COMPILE__NON 
 * Cr�� le      : 27/10/2015 � 17:03 par AC
 *
 * HISTORIQUE DES MODIFICATIONS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Include _INLINE
/* Actions: ? ? ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*----------------------------------------------------------------------*/
/*          Definitions de l'include                                    */
/*----------------------------------------------------------------------*/

&If Defined(CTRL-FRAME) = 0 &Then
    &If "{&Controle-In-Browse}":U = "yes":U &Then
       &Scoped-Define CTRL-FRAME Browse {&Browse-Name}
    &Else
       &Scoped-Define CTRL-FRAME Frame {&Frame-Name}
    &Endif
&Endif

&If Defined(CTRL-WCSOC) = 0 &Then
   &Scoped-Define CTRL-WCSOC wcsoc
&Endif 

&If Defined(CTRL-WCETAB) = 0 &Then
   &Scoped-Define CTRL-WCETAB wcetab
&Endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 2
         WIDTH              = 40.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* ************************ Main-Block ********************************** */
/*------------------ PARAMETRE d'ENTREE ---------------------------------*/
/*
{1} : nom de la zone a tester
{2} : oui faire le test d'existence fichier, non sinon
{3} : oui affichage du libelle complementaire, non sinon
{4} : nom du libelle complementaire a afficher
{5} : Numero de buffer  (en cas d'utilisation pour 2 zones ou + dans le meme prog)
... Cles complementaires
*/

/*&IF (NOT {2} AND NOT {3}) &THEN
   RUN AddLinkFeature IN adv-outils-hdl ({1}:HANDLE IN {&CTRL-FRAME},"VIF.FMACTJ":U) NO-ERROR.
&ENDIF*/

/* lecture dans le fichier */ 
&IF {2} or {3} &THEN 
Define Buffer buf{5}-ksst For ksst.

Find buf{5}-ksst Where buf{5}-ksst.csoc = {&CTRL-WCSOC}
              And buf{5}-ksst.cetab = {&CTRL-WCETAB}  
              And buf{5}-ksst.csst = {1}:Screen-Value in {&CTRL-FRAME}
          No-lock No-error.
&ENDIF.


/* affichage du libelle complementaire */        
&IF {3} &THEN 
If Available buf{5}-ksst
   Then Assign {4}:screen-value in {&CTRL-FRAME} = buf{5}-ksst.lsst.
   Else Assign {4}:screen-value in {&CTRL-FRAME} = "" . 
&ENDIF.

/* affichage de l'erreur si besoin */         
&IF {2} &THEN
If Not Available buf{5}-ksst Then Do :
   Run Test-Champ-Vide ({1}:Handle In {&CTRL-FRAME}).
   If Return-Value <> "Ok":U Then Do :
      Run msgaffcp.p ("Ksst":u ,{1}:Screen-Value in {&CTRL-FRAME} , "":u).
      APPLY "entry":U to {1} in {&CTRL-FRAME}.
      Return "Adm-Error":U.
  End.
End.
Else Do :
   /* On ne change pas la valeur de l'attribut Modified du handle */
   If {1}:Modified In {&CTRL-FRAME} Then
      {1}:Screen-Value in {&CTRL-FRAME} = buf{5}-Ksst.csst.
   Else 
      Assign {1}:Screen-Value in {&CTRL-FRAME} = buf{5}-Ksst.csst
             {1}:Modified In {&CTRL-FRAME} = No.

End.
&ENDIF.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



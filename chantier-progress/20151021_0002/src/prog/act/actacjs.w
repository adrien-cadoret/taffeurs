&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actacjs.w
 *
 * Description  : SmartSousFct sur art. de co�t
 * 
 * Template     : sousfct.w - SmartSous-Fonction  
 * Type Objet   : SmartSousFct 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 14/04/1998 � 15:23 par ED
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 22/12/2014 - MB      : DP001776:Activit�s : dates d'effet "historiques" � ignorer
 *                              20140623_0008 (25599)|DP001776|Activit�s : dates d'effet "historiques" � ignorer|MB|22/12/14 09:36:11 
 *                               
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" F-Frame-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/standard.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS F-Frame-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

&GLOBAL-DEFINE adv-user-attr-globaux "typacta":U

/* Boite de dialogue pour les attributs */

/* Parameters Definitions ---                                           */

/* Constantes propres � la fonction */
&Scoped-Define Nombre-Pages 0

/* N� Contexte Aide en ligne */

/* Variables globales */
{variable.i " "}

/* Local Variable Definitions ---                                       */
{admvif\template\sousfct.i "Definitions"}

/* Sous-Fonction de saisie fiche (dont le browse est sur la fct appelante) */
&Global-Define Gestion-Saisie-Fiche No
/* Faut-il garder en memoire la sous-fonction apres le 1er appel ? */
/* Ne modifier que si gestion-saisie-fiche = no */
&Global-Define Garder-En-Memoire Yes

&Scoped-Define Test Message "Available kactd" available kactd.

Define Variable w-titre As Character No-Undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" F-Frame-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartSousFct
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER FRAME

&Scoped-define ADM-SUPPORTED-LINKS Record-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-SousFct

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kactd
&Scoped-define FIRST-EXTERNAL-TABLE kactd


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kactd.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS R-ValidF R-ValidP RECT-168 RECT-1 

/* Custom List Definitions                                              */
/* Btn-SF,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_actajb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actajv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-updsav AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE VARIABLE F-ValidF AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 21.2 BY .62 NO-UNDO.

DEFINE VARIABLE F-ValidP AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 21.2 BY .62 NO-UNDO.

DEFINE VARIABLE FILL-IN-3 AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 12.4 BY .62
     FONT 15 NO-UNDO.

DEFINE RECTANGLE R-ValidF
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 2.4 BY .57.

DEFINE RECTANGLE R-ValidP
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 2.4 BY .57.

DEFINE RECTANGLE RECT-1
     EDGE-PIXELS 2 GRAPHIC-EDGE    
     SIZE 145.2 BY 8.33
     BGCOLOR 31 .

DEFINE RECTANGLE RECT-168
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 27.4 BY 2.1.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-SousFct
     FILL-IN-3 AT ROW 5.38 COL 125.8 COLON-ALIGNED NO-LABEL
     F-ValidP AT ROW 6.14 COL 128.6 COLON-ALIGNED NO-LABEL
     F-ValidF AT ROW 6.91 COL 128.6 COLON-ALIGNED NO-LABEL
     R-ValidF AT ROW 6.95 COL 127
     R-ValidP AT ROW 6.14 COL 127
     RECT-168 AT ROW 5.71 COL 125.8
     RECT-1 AT ROW 8.33 COL 8
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 15.8 ROW 1
         SIZE 168.8 BY 15.86
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartSousFct
   External Tables: vif5_7.kactd
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Container Links: Page-Target,Record-Source,Record-Target,Navigation-Source,Navigation-Target,TableIO-Source,TableIO-Target
   Design Page: 1
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW F-Frame-Win ASSIGN
         HEIGHT             = 16.05
         WIDTH              = 201.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB F-Frame-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_sfct.i}
{src/adm/method/containr.i}
{admvif/method/ap_sfct.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW F-Frame-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-SousFct
   NOT-VISIBLE FRAME-NAME                                               */
ASSIGN 
       FRAME F-SousFct:HIDDEN           = TRUE.

/* SETTINGS FOR FILL-IN F-ValidF IN FRAME F-SousFct
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       F-ValidF:PRIVATE-DATA IN FRAME F-SousFct     = 
                "init-txt=Validit� future".

/* SETTINGS FOR FILL-IN F-ValidP IN FRAME F-SousFct
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       F-ValidP:PRIVATE-DATA IN FRAME F-SousFct     = 
                "init-txt=Validit� pass�e".

/* SETTINGS FOR FILL-IN FILL-IN-3 IN FRAME F-SousFct
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       FILL-IN-3:PRIVATE-DATA IN FRAME F-SousFct     = 
                "init-txt= L�gende".

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-SousFct
/* Query rebuild information for FRAME F-SousFct
     _Options          = ""
     _Query            is NOT OPENED
*/  /* FRAME F-SousFct */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-SousFct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-SousFct F-Frame-Win
ON HELP OF FRAME F-SousFct
DO:
  /* Gestion standard de l'aide en ligne */
  {admvif/template/tinclude.i "Help"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK F-Frame-Win 


{admvif\template\sousfct.i "Triggers"}

{admvif\template\sousfct.i "Main-Block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects F-Frame-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actajb.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     Suppression-Message = ,
                     typacta = {&INIT-TYPACTA-ART-COUT}':U ,
             OUTPUT h_actajb ).
       RUN set-position IN h_actajb ( 1.00 , 42.00 ) NO-ERROR.
       RUN set-size IN h_actajb ( 6.86 , 76.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/p-updsav.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'SmartPanelType = Save,
                     Edge-Pixels = 2,
                     SaveFunction = AddMultiple,
                     NameBtnAction = ,
                     Hide-Save = No,
                     Hide-Add = No,
                     Hide-Copy = No,
                     Hide-Delete = No,
                     Hide-cancel = No,
                     Hide-Print = Yes,
                     Hide-Documents = Yes,
                     PrintFunction = Normale,
                     Layout-Options = "Master Layout,Standard Character",
                     Default-Layout = Master Layout':U ,
             OUTPUT h_p-updsav ).
       RUN set-position IN h_p-updsav ( 1.71 , 8.60 ) NO-ERROR.
       /* Size in UIB:  ( 1.86 , 9.20 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actajv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Layout-Options = "Master Layout,cout",
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Default-Layout = cout,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     Suppression-Message = ,
                     typacta = {&INIT-TYPACTA-ART-COUT}':U ,
             OUTPUT h_actajv ).
       RUN set-position IN h_actajv ( 8.52 , 9.00 ) NO-ERROR.
       RUN set-size IN h_actajv ( 7.19 , 143.60 ) NO-ERROR.

       /* Links to SmartBrowser h_actajb. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Record':U , h_actajb ).

       /* Links to SmartPanel h_p-updsav. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Panel':U , h_p-updsav ).

       /* Links to SmartViewer h_actajv. */
       RUN add-link IN adm-broker-hdl ( h_actajb , 'Record':U , h_actajv ).
       RUN add-link IN adm-broker-hdl ( h_p-updsav , 'TableIO':U , h_actajv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actajv ,
             h_actajb , 'AFTER':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available F-Frame-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kactd"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kactd"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI F-Frame-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-SousFct.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI F-Frame-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE R-ValidF R-ValidP RECT-168 RECT-1 
      WITH FRAME F-SousFct.
  {&OPEN-BROWSERS-IN-QUERY-F-SousFct}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-donnees F-Frame-Win 
PROCEDURE envoi-donnees :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter w-rowid as rowid   no-undo.

define variable w-list-rowid    as character format "x(20)":u no-undo.

    run send-records in h_actajb ("kacta":U, output w-list-rowid).
    w-rowid = to-rowid(w-list-rowid).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-close F-Frame-Win 
PROCEDURE local-adv-close :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  Define Variable Tot-Coef Like kacta.coefcout No-Undo.

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-close':U ) .
  
  
  /* Code placed here will execute AFTER standard behavior.    */
  if return-value = "adm-error":U then Return "Adm-Error":U.
 
  run get-attribute("titre":U).
  
  if Return-Value = "Articles de co�t":U 
     then Run new-State('fin-articlescout,container-source':U).   
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize F-Frame-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  ASSIGN w-titre = Get-TradTxt (003994, 'Articles de co�t':T).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ("initialize":U).

  /* Code placed here will execute AFTER standard behavior.    */
  R-ValidP:Bgcolor In Frame {&Frame-name} = {&COLOR-WARNING2-BG}.
  R-ValidF:Bgcolor In Frame {&Frame-name} = {&COLOR-WARNING-BG}.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available F-Frame-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  define variable w-rowid-kacta as rowid no-undo.

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If Return-Value <> "Adm-Error":U And 
     Valid-Handle(Fonction-Source-Hdl) Then Do :
    run envoi-donnees in Fonction-Source-Hdl (output w-rowid-kacta).
    if w-rowid-kacta <> ? then
        run reposition-browse in h_actajb (input w-rowid-kacta).
  end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-view F-Frame-Win 
PROCEDURE local-view :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vtDatefHisto AS LOGICAL   NO-UNDO. /* MB DP001776 */
/* -------------------------------------------------------------------------- */  

  /* Code placed here will execute PRIOR to standard behavior. */
  /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
  RUN Get-Info ("DatefHisto":U, "Container-Source":U).
  IF RETURN-VALUE <> ? THEN
     ASSIGN vtDatefHisto = (RETURN-VALUE = "oui":U).
 
  IF vtDatefHisto THEN
     RUN Set-Link-Attribute In Adm-Broker-Hdl (This-Procedure,'Panel-Target':U,
                                              'Mode-Consult=Oui':U ).
  ELSE DO:
  /* Fin DP001776 */
      Run Get-Info ("ModeFct":U, "Container-Source":U).
      Run Set-Link-Attribute In Adm-Broker-Hdl (This-Procedure,'Panel-Target':U,
                            'Mode-Consult=':U + (If Return-Value = "Consult":U Then "Oui":U Else "Non":U)).
  END.
  Run New-State ("Reinit-Panel,Panel-Target":U).
  
  Run Get-Info ("F-cact":U, "Info-Source":U).
  
  Run SetTitle ( w-titre + 
                 (if Return-Value ne ""  
                    then " " + Get-TradTxt (005157, 'de l''activit�':T) + " " + Return-Value
                    else "")).  

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'view':U ).

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info F-Frame-Win 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
       /* Ins�rer ici vos propres infos � recevoir */
       
       /* L�gende en gras */
       When "Valid":U Then
           CASE Valeur-Info :
               WHEN "Future":U THEN
                   ASSIGN F-ValidP:Font IN Frame {&Frame-Name} = ?
                          F-ValidF:Font IN Frame {&Frame-Name} = 15.
               WHEN "Passee":U THEN
                   ASSIGN F-ValidP:Font IN Frame {&Frame-Name} = 15
                          F-ValidF:Font IN Frame {&Frame-Name} = ?.
               WHEN "Valide":U THEN
                   ASSIGN F-ValidP:Font IN Frame {&Frame-Name} = ?
                          F-ValidF:Font IN Frame {&Frame-Name} = ?.
           END.

       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records F-Frame-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kactd"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed F-Frame-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Placez vos cases ici pour remplacez les states standard ou
         en mettre des nouveaux */
         
 
      /* Cas standard des sous-fonctions */
      {admvif/template/sousfct.i "sfctstates"}
               
      /* Cases standard des container */
      {admvif/template/cinclude.i "cstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


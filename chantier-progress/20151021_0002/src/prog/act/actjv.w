&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" V-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actjv.w
 *
 * Description  : SmartViewer sur Activit�s
 * 
 * Template     : VIEWER.W - SmartViewer basique 
 * Type Objet   : SmartViewer 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 01/08/1996 � 00:00 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 03/04/2015 - MB      : DP001776:Bug en cr�ation d'activit�
 *                              20150119_0017 (26543)|DP001776|Bug en cr�ation d'activit�|MB|03/04/15 10:18:30 
 *                               Q07846 bug KACTARTR
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" V-table-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/viewerd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

/* Parameters Definitions ---                                           */

/* Variables globales */
{variable.i " "}
{unitebibli.i}
{actbibli.i}
{crivi.i}
{cristabibli.i} 
{actdefji.i}     /* GLC 08/02/2005 : Traces Activit�s */
{liedoclib.i}
{bsff01l.i}  /* SLe 01/10/09 Modularit� des co�ts */

/* constantes de pre-compilation */
&GLOBAL-DEFINE table kact
&GLOBAL-DEFINE ltable

/* Variable pour template viewer */
{admvif/template/vinclude.i "Definitions"}

/* Local Variable Definitions ---                                       */
 define variable w-cact like kact.cact no-undo.

Define variable temp-cact    Like kact.cact  No-Undo.
Define Variable Reset-Modif  As Logical      No-Undo.
Define variable Modefct      As character    No-Undo Init "Consult":U.
Define Variable Rowid-Temp   As Rowid        No-Undo.

Define Buffer R-kact     For kact.    /* buffer reel de mise � jour */
Define Buffer B-kact     for kact.
Define Buffer B-kactsst  for kactsst.
Define Buffer B-kactd    for kactd.
Define Buffer B-kacto    for kacto.
Define Buffer B-kactod   for kactod.
Define Buffer B-kacta    for kacta.
Define Buffer B-kactartr for kactartr.
Define Buffer B-kactctxu for kactctxu.
Define Buffer B1-ktact   For ktact.
Define Buffer B2-ktact   For ktact.
Define Buffer B-kcom     For kcom.

/* Variable pour le param�tre ACTIVITE champ saisie simplifi�e d'une activit� */
Define Variable wtactart  As   logical            No-undo.
Define Variable W-T-Duree As   logical            No-undo.

/* Pour un test dans l'end-update, on a besoin de la famille �ltaire art.
 * ... Pas besoin de rechercher zart car on a d�j� cherch� buf-zart dans test-saisie
 */
Define Variable w-cfam Like zart.cfam No-Undo.

/* NLE 02/12/2002 */
Define Variable w-cunite    Like kact.cunite  No-Undo.

/* CLE 03/09/2003 fiche 922 */
Define Variable w-cart Like kact.cart  No-Undo.
Define Variable w-itichanged As Character No-Undo.
&SCOPED-DEFINE lg-iti 70 /*longueur max pour d�couper la chaine des itin�raires modifi�s pour l'affichage */

/* SLE 28/06/04 Sp�cificit� pour le Add-Record (vu avec les m�thodes) */
Define Variable w-Specif-Add As Logical No-Undo.

/* GLC 14/01/2005 Traces Activit�s : type de maj */
DEFINE VARIABLE vTypMaj AS CHARACTER NO-UNDO.

/* MB G7_1659 08/11/05 : T�moin "importation" du contexte Reconstitu� sur la soc/�tab courante */ 
DEFINE VARIABLE vgtCtxRec AS LOGICAL   NO-UNDO.

/* APO Process op�ratoires : Process operatoire regroupant les op�r�ration de l'activit� (Kacto, Kactod)*/
&SCOPED-DEFINE PROCESS-OP-ACTIVITE '$POPACT':U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" V-table-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kact
&Scoped-define FIRST-EXTERNAL-TABLE kact


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kact.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS kact.lact kact.ract kact.cfmact kact.csecteur ~
kact.cunite 
&Scoped-define ENABLED-TABLES kact
&Scoped-define FIRST-ENABLED-TABLE kact
&Scoped-Define ENABLED-OBJECTS B-cact B-cart B_AID-4 Cb-Typflux B_AID-2 ~
B_AID-7 RECT-86 
&Scoped-Define DISPLAYED-FIELDS kact.lact kact.ract kact.cfmact ~
kact.csecteur kact.cunite 
&Scoped-define DISPLAYED-TABLES kact
&Scoped-define FIRST-DISPLAYED-TABLE kact
&Scoped-Define DISPLAYED-OBJECTS F-cact F-cartvu Cb-Typflux 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,L-Oblig,L-Field,L-Majuscule,List-6 */
&Scoped-define ADM-CREATE-FIELDS F-cact F-cartvu 
&Scoped-define ADM-ASSIGN-FIELDS Cb-Typflux 
&Scoped-define L-Oblig F-cact F-cartvu kact.cfmact 
&Scoped-define L-Field F-cartvu Cb-Typflux 
&Scoped-define L-Majuscule F-cact 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Auto-Definitions" V-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wrdef.p */

/* ********** Preprocessor Definitions VIF ***********/
&Scoped-Define Trait-External-Tables ~{&TP1} kact ~{&TP2} 
&Scoped-Define List-Champs-Auto F-cact,kact.lact,kact.ract,F-cartvu,kact.cfmact,Cb-Typflux,kact.csecteur,kact.cunite
&If Defined(List-Champs-Auto) <> 0 &Then
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&List-Champs-Auto},{&User-List-Champs}
   &Else
      &Scoped-Define List-champs {&List-Champs-Auto}
   &Endif
&Else
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&User-List-Champs}
   &Endif
&Endif

&Scoped-Define List-Combos Cb-Typflux
&Scoped-Define Clear-Combos Assign Cb-Typflux:screen-value in Frame F-Main = Cb-Typflux:Entry(1)
&Scoped-Define Initialize-Combos ~
  Run Init-Cb-Rs In Adm-broker-hdl (Cb-Typflux:handle in Frame F-Main).
&Scoped-Define FIELD-PAIRS~
 ~{&FP1}lact ~{&FP2}lact ~{&FP3}~
 ~{&FP1}ract ~{&FP2}ract ~{&FP3}~
 ~{&FP1}cfmact ~{&FP2}cfmact ~{&FP3}~
 ~{&FP1}csecteur ~{&FP2}csecteur ~{&FP3}~
 ~{&FP1}cunite ~{&FP2}cunite ~{&FP3}
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD FormatVersart V-table-Win 
FUNCTION FormatVersart RETURNS INTEGER
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GetVERSART V-table-Win 
FUNCTION GetVERSART RETURNS INTEGER PRIVATE
  ( INPUT iCsoc   AS CHARACTER,
    INPUT iCetab  AS CHARACTER,
    INPUT iCact   AS CHARACTER,
    INPUT iDatef  AS DATE)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON B-cact 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B-cart 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-2 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-4 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-7 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE VARIABLE Cb-Typflux AS CHARACTER FORMAT "X(256)":U 
     LABEL "Type de flux" 
     VIEW-AS COMBO-BOX INNER-LINES 5
     DROP-DOWN-LIST
     SIZE 19.2 BY 1 NO-UNDO.

DEFINE VARIABLE F-cact AS CHARACTER FORMAT "X(15)" 
     LABEL "Activit�" 
     VIEW-AS FILL-IN 
     SIZE 28.2 BY 1 NO-UNDO.

DEFINE VARIABLE F-cartvu AS CHARACTER FORMAT "X(15)" 
     LABEL "Article de r�f�rence" 
     VIEW-AS FILL-IN 
     SIZE 28.2 BY 1 NO-UNDO.

DEFINE RECTANGLE RECT-86
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 134.2 BY 6.81.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     F-cact AT ROW 1.38 COL 21.4 COLON-ALIGNED
     B-cact AT ROW 2.38 COL 51.6
     kact.lact AT ROW 1.38 COL 54 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 52.8 BY 1
     kact.ract AT ROW 1.38 COL 107 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 22.8 BY 1
     F-cartvu AT ROW 2.38 COL 21.4 COLON-ALIGNED
     B-cart AT ROW 2.38 COL 51.6
     zart.lart AT ROW 2.38 COL 54 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 52.8 BY 1
     zart.rart AT ROW 2.38 COL 107 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 22.8 BY 1
     kact.cfmact AT ROW 3.38 COL 21.4 COLON-ALIGNED
          LABEL "Famille d'activit�s"
          VIEW-AS FILL-IN 
          SIZE 15.6 BY 1
     B_AID-4 AT ROW 3.38 COL 39
     kfmact.lfmact AT ROW 3.38 COL 54 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 52.8 BY 1
     Cb-Typflux AT ROW 4.38 COL 21.4 COLON-ALIGNED
     kact.csecteur AT ROW 5.38 COL 21.4 COLON-ALIGNED
          LABEL "Secteur"
          VIEW-AS FILL-IN 
          SIZE 15.6 BY 1
     B_AID-2 AT ROW 5.38 COL 39
     zsecteur.lsecteur AT ROW 5.38 COL 54 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 52.8 BY 1
     kact.cunite AT ROW 6.38 COL 21.4 COLON-ALIGNED
          LABEL "Unit� r�f�rence"
          VIEW-AS FILL-IN 
          SIZE 15.6 BY 1
     B_AID-7 AT ROW 6.38 COL 39
     RECT-86 AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: vif5_7.kact
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 8.38
         WIDTH              = 167.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_viewe.i}
{src/adm/method/viewer.i}
{admvif/method/ap_viewe.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit Custom                            */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       B-cact:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B-cart:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-2:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-4:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-7:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

/* SETTINGS FOR COMBO-BOX Cb-Typflux IN FRAME F-Main
   2 4                                                                  */
ASSIGN 
       Cb-Typflux:PRIVATE-DATA IN FRAME F-Main     = 
                "Liste-valeur={&LCODE-TYPFLUX}|
Liste-Libell={&LCLAIR-TYPFLUX}|
Mnemoid=TYPFLUX".

/* SETTINGS FOR FILL-IN kact.cfmact IN FRAME F-Main
   3 EXP-LABEL                                                          */
/* SETTINGS FOR FILL-IN kact.csecteur IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN kact.cunite IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN F-cact IN FRAME F-Main
   NO-ENABLE 1 3 5                                                      */
/* SETTINGS FOR FILL-IN F-cartvu IN FRAME F-Main
   NO-ENABLE 1 3 4                                                      */
/* SETTINGS FOR FILL-IN kact.lact IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN zart.lart IN FRAME F-Main
   NO-DISPLAY NO-ENABLE EXP-LABEL                                       */
/* SETTINGS FOR FILL-IN kfmact.lfmact IN FRAME F-Main
   NO-DISPLAY NO-ENABLE EXP-LABEL                                       */
/* SETTINGS FOR FILL-IN zsecteur.lsecteur IN FRAME F-Main
   NO-DISPLAY NO-ENABLE EXP-LABEL                                       */
/* SETTINGS FOR FILL-IN kact.ract IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN zart.rart IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" V-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME B-cact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B-cact V-table-Win
ON CHOOSE OF B-cact IN FRAME F-Main /* ? */
DO :

  /* Appliquer la r�gle standard du choose sur le bouton */
 If wtactart Then do:
     {admvif/template/tinclude.i "choose-combo-aide"}
 End.
 Else 
   Apply "choose":u To B-cart.
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B-cact V-table-Win
ON ENTRY OF B-cact IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B-cart
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B-cart V-table-Win
ON CHOOSE OF B-cart IN FRAME F-Main /* ? */
DO :

  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B-cart V-table-Win
ON ENTRY OF B-cart IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-2 V-table-Win
ON CHOOSE OF B_AID-2 IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-2 V-table-Win
ON ENTRY OF B_AID-2 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-4 V-table-Win
ON CHOOSE OF B_AID-4 IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-4 V-table-Win
ON ENTRY OF B_AID-4 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-7 V-table-Win
ON CHOOSE OF B_AID-7 IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-7 V-table-Win
ON ENTRY OF B_AID-7 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME F-cact
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-cact V-table-Win
ON LEAVE OF F-cact IN FRAME F-Main /* Activit� */
DO:
  Run Notify ('Enable-Buttons,InfoACT-Target':U).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */
/*** Triggers Generaux Du viewer */
{admvif/template/vinclude.i "triggers"}

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         

{admvif/template/vinclude.i "main-block"}
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kact"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kact"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Aide-Sur-Champ V-table-Win 
PROCEDURE Aide-Sur-Champ :
/*------------------------------------------------------------------------------
  Purpose:     Aide (liste de choix) Sur les Champs
  Parameters:  Handle du widget active
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Champ  As Widget-Handle No-Undo.
    Define Variable ChoixEffectue As Character No-Undo.
   
    define variable w-cart like zart.cart no-undo.
    define variable w-cug  like zart.cug  no-undo.
    define variable w-cus  like zart.cus  no-undo.

    Case Champ:Name :
       When "F-cact":U Then Do:
          If wtactart Then do:
             {&Run-prog} "admvif/objects/a-standa.w":U 
                         ("artj":U,             /* Code fonction pour les droits */
                          "artjb":U, Substitute('typart=&1,tfab=oui':U, 
                                                  {&TYPART-MARCHANDISE}),      /* Nom browser et attributs */
                          "":U, "":U,           /* Nom viewer et attributs */
                          Champ:Label,          /* Titre ecran d'aide */
                          no,                   /* Acces bouton nouveau */
                          Champ:Screen-Value,   /* Valeur de depart */
                          Output ChoixEffectue).

             If ChoixEffectue <> ? Then 
                Champ:Screen-Value = ChoixEffectue.
          End.         
       End.

       When "csecteur":U Then Do :
          {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("secteuj":U,             /* Code fonction pour les droits */
                       "secteujb":U, "":U,   /* Nom browser et attributs */
                       "secteujv":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
          If ChoixEffectue <> ? Then 
             Champ:Screen-Value = ChoixEffectue.
       End.
              
       When "cfmact":U Then Do :
          {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("fmactj":U,             /* Code fonction pour les droits */
                       "fmactjb":U, "":U,   /* Nom browser et attributs */
                       "fmactjv":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
          If ChoixEffectue <> ? Then 
             Champ:Screen-Value = ChoixEffectue.
       End.
       
       When "F-cartvu":U Then Do :
          {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("artj":U,             /* Code fonction pour les droits */
                       "artjb":U, Substitute('typart=&1,tfab=oui':U, 
                                                  {&TYPART-MARCHANDISE}),   /* Nom browser et attributs */
                       "":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
          If ChoixEffectue <> ? Then 
             Champ:Screen-Value = ChoixEffectue.
       End.
              
       When "cunite":U Then Do :
          {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("artj":U,             /* Code fonction pour les droits */
                       "artulb":U, "att-wcart=":U + F-cartvu:screen-value in frame {&Frame-name},   /* Nom browser et attributs */
                       "":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
          If ChoixEffectue <> ? Then 
             Champ:Screen-Value = ChoixEffectue.
       End.     
       
       Otherwise ChoixEffectue = ?.
    End Case.

    If ChoixEffectue <> ? Then 
       Apply "Tab":U To Champ.
    Else
       Apply "Entry":U To Champ.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Annuler-Buffer-Temp V-table-Win 
PROCEDURE Annuler-Buffer-Temp :
/*------------------------------------------------------------------------------
  Purpose:     Annulation buffer temporaire
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
Define Variable w-ok        As Logical      No-Undo.
Define Variable w-msg-err   As Character    No-Undo.
DEFINE VARIABLE vNbEnrSup   AS INTEGER      NO-UNDO. /*AMA 23/02/2006 */

DEFINE BUFFER bKactdpop     FOR Kactdpop.
DEFINE BUFFER bKactapopd     FOR Kactapopd.

Define Buffer bsup-kact     For kact.
Define Buffer bsup-kactsst  For kactsst.
Define Buffer bsup-kactd    For kactd.
DEFINE BUFFER bsup-Kactdpop FOR Kactdpop.
Define Buffer bsup-kacto    For kacto.
Define Buffer bsup-kactod   For kactod.
Define Buffer bsup-kacta    For kacta.
DEFINE BUFFER bsup-Kactapopd FOR Kactapopd.
Define Buffer bsup-kactartr For kactartr.
Define Buffer bsup-kactctxu For kactctxu.
Define Buffer bsup-kcom     For kcom.

   {&SetCurs-Wait}
   Assign w-ok = Yes.
   Tr_Annuler:
   Do Transaction :
      /* ACA Suppression des sous-traitants de l'activit�*/
      For each B-kactsst Where B-kactsst.csoc  = wcsoc
                        And B-kactsst.cetab = wcetab
                        And B-kactsst.cact  = temp-cact 
                      Exclusive-lock :
         Delete B-kactsst.
      End.
      For each b-kactd FIELDS({&EmptyFields})
                        Where b-kactd.csoc  = wcsoc
                          And b-kactd.cetab = wcetab
                          And b-kactd.cact  = temp-cact No-lock :
          Find First bsup-kactd Where Rowid(bsup-kactd) = Rowid(b-kactd)
          Exclusive-Lock No-Wait No-Error.
          If Locked( bsup-kactd )
          Then Do:
             Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'kactd':U )
                    w-ok      = No.
             Undo, Leave Tr_Annuler.
          End.
          Else Do:
            /* SLe 05/12/2012 */
            Run SupCrit( Input  temp-cact + ',':U + STRING(bsup-kactd.datef, '99/99/9999':U)
                       , Output w-ok
                       , Output w-msg-err ).
            If Not w-ok Then Undo, Leave Tr_Annuler.
            Assign w-ok = Yes. 
            
            /* NLE 26/07/2005 : Suppression des liens du kactd vers les documents */
            {&Run-LIEDOC-Suppression} (INPUT wcsoc,
                                       INPUT wcetab,
                                       INPUT {&ZLIEDOC-TYPCLE-FAB},
                                       INPUT bsup-kactd.cact + ':':U + STRING(bsup-kactd.datef, '99/99/9999':U),
                                       OUTPUT vNbEnrSup). /* AMA 23/02/2006 */
            Delete bsup-kactd.
          End.          
      End.      
      /* Lien version d'activit� - Process op�ratoire */
      FOR EACH bKactdpop FIELDS({&Emptyfields})
                          WHERE bKactdpop.csoc  = wcsoc
                            AND bKactdpop.cetab = wcetab
                            AND bKactdpop.cact  = temp-cact
                         NO-LOCK:
          FIND FIRST bsup-Kactdpop WHERE ROWID(bsup-Kactdpop) = ROWID(bKactdpop)
                                   EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
          IF LOCKED (bsup-Kactdpop) THEN DO:
             ASSIGN w-msg-err = SUBSTITUTE(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'Kactdpop':U )
                    w-ok      = NO.
             Undo, Leave Tr_Annuler.          
          END.
          ELSE 
             DELETE bsup-Kactdpop.
      END.
      For each b-kacto FIELDS({&EmptyFields})
                        Where b-kacto.csoc  = wcsoc
                          And b-kacto.cetab = wcetab
                          And b-kacto.cact  = temp-cact No-lock :                                  
          Find First bsup-kacto Where Rowid(bsup-kacto) = Rowid(b-kacto)
          Exclusive-Lock No-Wait No-Error.
          If Locked( bsup-kacto )
          Then Do:
             Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'kacto':U )
                    w-ok      = No.
             Undo, Leave Tr_Annuler.
          End.
          Else Do:
            Delete bsup-kacto.
          End.
      End.
      For each b-kactod FIELDS({&EmptyFields})
                         Where b-kactod.csoc  = wcsoc
                           And b-kactod.cetab = wcetab
                           And b-kactod.cact  = temp-cact No-lock :
          Find First bsup-kactod Where Rowid(bsup-kactod) = Rowid(b-kactod)
          Exclusive-Lock No-Wait No-Error.
          If Locked( bsup-kactod )
          Then Do:
             Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'kactod':U )
                    w-ok      = No.
             Undo, Leave Tr_Annuler.
          End.
          Else Do:
            Delete bsup-kactod.
          End.
      End.
      For each b-kacta FIELDS({&EmptyFields})
                        Where b-kacta.csoc  = wcsoc
                          And b-kacta.cetab = wcetab
                          And b-kacta.cact  = temp-cact No-lock :
          Find First bsup-kacta Where Rowid(bsup-kacta) = Rowid(b-kacta)
          Exclusive-Lock No-Wait No-Error.
          If Locked( bsup-kacta )
          Then Do:
             Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'kacta':U )
                    w-ok      = No.
             Undo, Leave Tr_Annuler.
          End.
          Else Do:
            Delete bsup-kacta.
          End.
      End.
      /* Lien article d'activit� - Op�ration de Process op�ratoire */
      FOR EACH bKactapopd FIELDS({&EmptyFields})
                          WHERE bKactapopd.csoc  = wcsoc
                            AND bKactapopd.cetab = wcetab
                            AND bKactapopd.cact  = temp-cact
                         NO-LOCK:
          FIND FIRST bsup-Kactapopd WHERE ROWID(bsup-Kactapopd) = ROWID(bKactapopd)
                                   EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
          IF LOCKED (bsup-Kactapopd) THEN DO:
             ASSIGN w-msg-err = SUBSTITUTE(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'Kactapopd':U )
                    w-ok      = NO.
             Undo, Leave Tr_Annuler.          
          END.
          ELSE 
             DELETE bsup-Kactapopd.
      END.
      For each b-kcom FIELDS({&EmptyFields})
                       Where b-kcom.csoc   = wcsoc
                         And b-kcom.cetab  = wcetab
                         And b-kcom.typcle = {&TYPCLE-COM-ACT}
                         And b-kcom.cle Begins ( temp-cact + ',':U ) No-lock :
          Find First bsup-kcom Where Rowid(bsup-kcom) = Rowid(b-kcom)
          Exclusive-Lock No-Wait No-Error.
          If Locked( bsup-kcom )
          Then Do:
             Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'kcom':U )
                    w-ok      = No.
             Undo, Leave Tr_Annuler.
          End.
          Else Do:
            Delete bsup-kcom.
          End.
      End.
      For each b-kactartr FIELDS({&Emptyfields})
                           Where b-kactartr.csoc  = wcsoc
                             And b-kactartr.cetab = wcetab
                             And b-kactartr.cact  = temp-cact No-lock : /* CBR 19/04/04 */
          Find First bsup-kactartr Where Rowid(bsup-kactartr) = Rowid(b-kactartr)
          Exclusive-Lock No-Wait No-Error.
          If Locked( bsup-kactartr )
          Then Do:
             Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'kactartr':U )
                    w-ok      = No.
             Undo, Leave Tr_Annuler.
          End.
          Else Do:
            Delete bsup-kactartr.
          End.
      End.
      /* Rajout CBR le 22/02/2002 */
      For each b-kactctxu FIELDS({&EmptyFields})
                           Where b-kactctxu.csoc   = wcsoc
                             And b-kactctxu.cetab  = wcetab
                             And b-kactctxu.typcle = {&KACTCTXU-TYPCLE-ACTIVITE}
                             And b-kactctxu.cle    = temp-cact No-lock : /* CBR 19/04/04 */
          Find First bsup-kactctxu Where Rowid(bsup-kactctxu) = Rowid(b-kactctxu)
          Exclusive-Lock No-Wait No-Error.
          If Locked( bsup-kactctxu )
          Then Do:
             Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'kactctxu':U )
                    w-ok      = No.
             Undo, Leave Tr_Annuler.
          End.
          Else Do:
            Delete bsup-kactctxu.
          End.
      End.
      
      /* YG le 30/04/2002: gestion de l'acc�s concurrentiel en multi-utilisateur */
      Find First B1-ktact Where B1-ktact.csoc    = wcsoc
                            And B1-ktact.cetab   = wcetab
                            And B1-ktact.typcle  = {&KTACT-TYPCLE-ACTIVITE}
                            And B1-ktact.cle     = temp-cact
                            And B1-ktact.ctemoin = {&KTACT-CTEMOIN-MAJ-ACTIVITE}
                          No-Lock No-Error.
      If Available B1-ktact Then Do:
         Find First B2-ktact Where B2-ktact.csoc    = wcsoc
                               And B2-ktact.cetab   = wcetab
                               And B2-ktact.typcle  = {&KTACT-TYPCLE-ACTIVITE}
                               And B2-ktact.cle     = B1-ktact.zalpha
                               And B2-ktact.ctemoin = {&KTACT-CTEMOIN-MAJ-ACTIVITE}
                             No-Lock No-Error.      
         If Available B2-ktact Then Do:
            Find Current B2-ktact Exclusive-Lock No-Wait No-Error.
            If Locked (B2-ktact) Then Do:
                Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                             , B1-ktact.zalpha, 'ktact':U )
                       w-ok      = No.
                Undo, Leave Tr_Annuler.
            End.
            Else Do:
                Delete B2-ktact.
            End.
         End.
         Find Current B1-ktact Exclusive-Lock No-Wait No-Error.
         If Locked (B1-ktact) Then Do:
            Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                         , temp-cact, 'ktact':U )
                   w-ok      = No.
            Undo, Leave Tr_Annuler.
         End.
         Else Do:
            Delete B1-ktact.
         End.
      End.

      Find b-kact where Rowid(b-kact) = Rowid-Temp No-Lock No-Error.
      If Available b-kact Then Do:
         Find bsup-kact where Rowid(bsup-kact) = Rowid(b-kact) Exclusive-Lock No-Wait No-Error.
         If Locked (bsup-kact)
         Then Do:
             Assign w-msg-err = Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , temp-cact, 'kact':U )
                    w-ok      = No.
             Undo, Leave Tr_Annuler.
         End.
         Else Do:
            Delete bsup-kact.
         End.
      End.
      Run SupCrit( Input  temp-cact
                 , Output w-ok
                 , Output w-msg-err ).
      If Not w-ok Then Undo, Leave Tr_Annuler.
      Assign w-ok = Yes.
   End.
   If Not w-ok
   Then Do:
      {affichemsg.i &Message="w-msg-err"}
   End.
   If available R-kact Then Do :
      /* lecture du vrai kact en no-lock */
      Find kact Where Rowid(kact) = Rowid(R-kact) No-lock No-Error.
      Release R-kact.
   End.

  {&SetCurs-NoWait}

   Run Notify ('Adv-Clear-Modified,Self,Group-Assign-Target':U).
   Run Set-Info("Rowid-Temp=?":U, "Record-Source":U).
   ModeFct = "Consult":U.
   Run Set-Info("ModeFct=":U + Modefct, "Container-Source":U).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Assign-Buffer-Temp V-table-Win 
PROCEDURE Assign-Buffer-Temp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Run get-valeur-combo(cb-typflux:handle in frame {&FRAME-NAME},
                       output kact.typflux).

/*  SLE 07/08/02 typactiti => contextes
 *   Run Get-Info ("typactiti":u, "typactiti-target":u).
 *   kact.typactiti = Return-Value.  */
                      
   Assign kact.cart = F-cartvu:Screen-Value In Frame {&Frame-Name}.
   
    RUN Adm-Assign-Statement.   /* Run direct de l'assign statement pour assigner les screen-values */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Assign-tt-traces V-table-Win 
PROCEDURE Assign-tt-traces :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
   DEFINE INPUT PARAMETER iDatefEnCours AS DATE      NO-UNDO.

   DEFINE BUFFER   bKactdpop FOR Kactdpop.
   DEFINE BUFFER   bKactapopd FOR Kactapopd.
   
   IF AVAILABLE kact 
            AND F-cact:Screen-Value In Frame {&Frame-Name} <> "":U
   THEN DO :
      /*-- On vide les tables temporaires avant de les initialiser --*/
      RUN Raz-tt-traces.

      /*-- Ent�te. --*/
      CREATE ttKact-APRES.
      BUFFER-COPY kact EXCEPT cact TO ttKact-APRES.
      ASSIGN ttKact-APRES.cact = F-cact:Screen-Value In Frame {&Frame-Name}.

      /*-- Dates d'effets. --*/
      FOR EACH kactd WHERE kactd.csoc  = kact.csoc
                       AND kactd.cetab = kact.cetab
                       AND kactd.cact  = kact.cact
                       AND kactd.datef >= iDatefEnCours /* MB DP001776 */
                     NO-LOCK :
          CREATE ttKactd-APRES.
          BUFFER-COPY kactd EXCEPT cact TO ttKactd-APRES.
          ASSIGN ttKactd-APRES.cact = F-cact:Screen-Value In Frame {&Frame-Name}.
      END.
      /* Sous-traitants */
      FOR EACH kactsst WHERE kactsst.csoc  = kact.csoc
                       AND kactsst.cetab = kact.cetab
                       AND kactsst.cact  = kact.cact
                     NO-LOCK :
          CREATE ttKactsst-APRES.
          BUFFER-COPY kactsst EXCEPT cact TO ttKactsst-APRES.
          ASSIGN ttKactsst-APRES.cact = F-cact:Screen-Value In Frame {&Frame-Name}.
      END.
      
         
      /* Lien version d'activit� - Process op�ratoire */     
      FOR EACH bKactdpop WHERE bKactdpop.csoc  = kact.csoc 
                           AND bKactdpop.cetab = kact.cetab
                           AND bKactdpop.cact  = kact.cact 
                           AND bKactdpop.datef >= iDatefEnCours /* MB DP001776 */
                         NO-LOCK:     
          CREATE ttKactdpop-APRES.
          BUFFER-COPY bKactdpop EXCEPT cact TO ttKactdpop-APRES.
          ASSIGN ttKactdpop-APRES.cact = F-cact:Screen-Value In Frame {&Frame-Name}.
      END.
      
      /*-- Entrants, sortants et articles de co�t. --*/
      FOR EACH kacta WHERE kacta.csoc  = kact.csoc
                       AND kacta.cetab = kact.cetab
                       AND kacta.cact  = kact.cact
                       AND kacta.datef >= iDatefEnCours /* MB DP001776 */
                     NO-LOCK :         
          CREATE ttKacta-APRES.
          BUFFER-COPY kacta EXCEPT cact TO ttKacta-APRES.
          ASSIGN ttKacta-APRES.cact = F-cact:Screen-Value In Frame {&Frame-Name}.        
      END.
      
      /* Lien article d'activit� - Op�ration de Process op�ratoire */      
      FOR EACH bKactapopd WHERE bKactapopd.csoc  = kact.csoc 
                           AND bKactapopd.cetab = kact.cetab
                           AND bKactapopd.cact  = kact.cact 
                           AND bKactapopd.datef >= iDatefEnCours /* MB DP001776 */
                         NO-LOCK:     
          CREATE ttKactapopd-APRES.
          BUFFER-COPY bKactapopd EXCEPT cact TO ttKactapopd-APRES.
          ASSIGN ttKactapopd-APRES.cact = F-cact:Screen-Value In Frame {&Frame-Name}.
      END.
      
      /*-- Articles de remplacement. --*/
      FOR EACH kactartr WHERE kactartr.csoc  = kact.csoc
                          AND kactartr.cetab = kact.cetab
                          AND kactartr.cact  = kact.cact
                          AND kactartr.datef >= iDatefEnCours /* MB DP001776 */
                        NO-LOCK :         
         CREATE ttkactartr-APRES.
         BUFFER-COPY kactartr EXCEPT cact TO ttKactartr-APRES.
         ASSIGN ttKactartr-APRES.cact = F-cact:Screen-Value In Frame {&Frame-Name}.
      END.

      /*-- Op�rations. --*/
      FOR EACH kacto WHERE kacto.csoc  = kact.csoc
                       AND kacto.cetab = kact.cetab
                       AND kacto.cact  = kact.cact
                       AND kacto.datef >= iDatefEnCours /* MB DP001776 */
                     NO-LOCK :
         CREATE ttkacto-APRES.    
         BUFFER-COPY kacto EXCEPT cact TO ttkacto-APRES.
         ASSIGN ttKacto-APRES.cact = F-cact:Screen-Value In Frame {&Frame-Name}.
      END.

      /*-- D�tail des op�rations. --*/
      FOR EACH kactod WHERE kactod.csoc  = kact.csoc
                        AND kactod.cetab = kact.cetab
                        AND kactod.cact  = kact.cact
                        AND kactod.datef >= iDatefEnCours /* MB DP001776 */
                      NO-LOCK :
          CREATE ttkactod-APRES.          
          BUFFER-COPY kactod EXCEPT cact TO ttkactod-APRES.
          ASSIGN ttKactod-APRES.cact = F-cact:Screen-Value In Frame {&Frame-Name}.
      END.

      /*-- Contextes --*/
      FOR EACH kactctxu WHERE kactctxu.csoc   = kact.csoc
                          AND kactctxu.cetab  = kact.cetab
                          AND kactctxu.typcle = {&KACTCTXU-TYPCLE-ACTIVITE}
                          AND kactctxu.cle    = kact.cact
                      NO-LOCK :
         CREATE ttKactctxu-APRES.          
         BUFFER-COPY kactctxu EXCEPT cle TO ttKactctxu-APRES.
         ASSIGN ttKactctxu-APRES.cle = F-cact:Screen-Value In Frame {&Frame-Name}.
      END.

      /*-- Crit�res --*/
      FOR EACH kactcriv WHERE kactcriv.csoc   = kact.csoc
                          AND kactcriv.cetab  = kact.cetab
                          AND kactcriv.typcle = {&KCRIV-TYPCLE-ACT}
                          AND kactcriv.cle    = kact.cact
                        NO-LOCK :
         CREATE ttkcriv-APRES.          
         BUFFER-COPY kactcriv EXCEPT cle TO ttkcriv-APRES.
         ASSIGN ttKcriv-APRES.cle = F-cact:Screen-Value In Frame {&Frame-Name}.
      END.
      BOUCLE-KACTCRIV-ACT:
      FOR EACH kactcriv WHERE kactcriv.csoc   = kact.csoc
                          AND kactcriv.cetab  = kact.cetab
                          AND kactcriv.typcle = {&KCRIV-TYPCLE-ACT}
                          AND kactcriv.cle BEGINS kact.cact + ',':u
                        NO-LOCK :
         /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
         IF DATE(ENTRY(2,kactcriv.cle,",":U)) < iDatefEnCours THEN
            NEXT BOUCLE-KACTCRIV-ACT.
         CREATE ttKcriv-Dat-APRES.          
         BUFFER-COPY kactcriv EXCEPT cle TO ttKcriv-Dat-APRES.
         ASSIGN ttKcriv-Dat-APRES.cle = F-cact:Screen-Value In Frame {&Frame-Name} + SUBSTRING(kactcriv.cle,LENGTH(ENTRY(1,kactcriv.cle)) + 1).
      END.      
      BOUCLE-KACTCRIV-ACT-RD:
      FOR EACH kactcriv WHERE kactcriv.csoc   = kact.csoc
                          AND kactcriv.cetab  = kact.cetab
                          AND kactcriv.typcle = {&KACTCRIV-TYPCLE-ACT-RD}
                          AND kactcriv.cle BEGINS kact.cact + ',':u
                        NO-LOCK :
         /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
         IF DATE(ENTRY(2,kactcriv.cle,",":U)) < iDatefEnCours THEN
            NEXT BOUCLE-KACTCRIV-ACT-RD.
         CREATE ttKcriv-Dat-RdtDyn-APRES.          
         BUFFER-COPY kactcriv EXCEPT cle TO ttKcriv-Dat-RdtDyn-APRES.
         ASSIGN ttKcriv-Dat-RdtDyn-APRES.cle = F-cact:Screen-Value In Frame {&Frame-Name} + SUBSTRING(kactcriv.cle,LENGTH(ENTRY(1,kactcriv.cle)) + 1).
      END.
      
      /*-- Commentaires --*/
      BOUCLE-COM:
      FOR EACH kcom WHERE kcom.csoc         = kact.csoc
                      AND kcom.cetab        = kact.cetab
                      AND kcom.typcle       = {&TYPCLE-COM-ACT}
                      AND kcom.cle     BEGINS kact.cact + ',':U      /* NLE 06/07/2006 */
                    NO-LOCK :
         /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
         IF DATE(ENTRY(2,kcom.cle,",":U)) < iDatefEnCours THEN
            NEXT BOUCLE-COM.
         CASE NUM-ENTRIES(kcom.cle) :
            WHEN 2 THEN DO: /* cact,datef */
               CREATE ttKcom-Dat-APRES.          
               BUFFER-COPY kcom EXCEPT cle TO ttKcom-Dat-APRES.
               ASSIGN ttKcom-Dat-APRES.cle = F-cact:Screen-Value In Frame {&Frame-Name} + SUBSTRING(kcom.cle,LENGTH(ENTRY(1,kcom.cle)) + 1).
            END.
            WHEN 4 THEN DO: /* cact,datef,typacta,ni1 */ 
               CREATE ttKcom-Art-APRES.          
               BUFFER-COPY kcom EXCEPT cle TO ttKcom-Art-APRES.
               ASSIGN ttKcom-Art-APRES.cle = F-cact:Screen-Value In Frame {&Frame-Name} + SUBSTRING(kcom.cle,LENGTH(ENTRY(1,kcom.cle)) + 1).
            END.
         END CASE.
      END.
   END.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Copie-Buffers V-table-Win 
PROCEDURE Copie-Buffers :
/*------------------------------------------------------------------------------
  Purpose:     Copie des buffers
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  Define Input Parameter Mode   As Character No-Undo.
  Define Input Parameter majdoc As LOGICAL   No-Undo.
  
  DEFINE VARIABLE vToday AS  DATE      NO-UNDO.
  DEFINE VARIABLE vTime  AS  INTEGER   NO-UNDO.
  
  DEFINE BUFFER bKactdpop   FOR Kactdpop.
  DEFINE BUFFER bKactapopd   FOR Kactapopd.
  
  /* mise � jour */
  DEFINE BUFFER bufKtact    FOR ktact.
  DEFINE BUFFER bufKactdpop FOR Kactdpop.
  DEFINE BUFFER bufKactapopd FOR Kactapopd.
  
  Temp-cact = "$":u + wcuser.
  
  CASE Mode:
     WHEN "Ajouter" THEN DO :
           Run Set-Info("Current-Cact=":U + Temp-cact , "Group-Assign-Target":u).
     END.
     WHEN "Copier" THEN DO :
           Run Set-Info("Current-Cact=":U + kact.cact, "Group-Assign-Target":u).
     END.
     WHEN "Modifier" THEN DO :   
           Run Set-Info("Current-Cact=":U + kact.cact, "Group-Assign-Target":u).
     END.
  END CASE.
  


  Find b-kact Where b-kact.csoc  = wcsoc
                And b-kact.cetab = wcetab
                And b-kact.cact  = Temp-cact No-Lock No-Error.
  If Available b-kact Then Do :
     {affichemsg.i       &Message = "Get-TradMsg (002094, 'Vous �tes d�j� en modification d''activit� sur une autre session.|Demandez � votre administrateur de vous d�bloquer gr�ce aux utilitaires.':T)"}
     Return "Adm-Error":U.
  End.

  /* YG le 30/04/2002: gestion de l'acc�s concurrentiel en multi-utilisateur */
  If Mode = "Modifier":U Then Do:  
     {&Run-Actbib-Test-Blocage-Act} (Input  {&Table}.csoc,
                                     Input  {&Table}.cetab,
                                     Input  {&Table}.cact,
                                     Output ret,
                                     Output wstr).
                                     
                                     
     If Ret Then Do:
         /* OM : 17/07/2003 Afin de limiter le nb de message on divise ce roman en trois ce qui permet d'en supprimer 10 autres */
         /* {affichemsg.i       &Message = "Substitute(Get-TradMsg (002100, 'L''utilisateur &1 est d�j� en modification sur cette activit�.|Si le blocage persiste alors que cet utilisateur est sorti de la fiche Activit�,|demandez � votre administrateur de vous d�bloquer gr�ce aux utilitaires.':T), wstr)"} */
         {affichemsg.i       &Message = "Substitute(Get-TradTxt (000962, 'L''utilisateur &1 est d�j� en modification sur cette activit�.':T), wstr)                   + '|':U +
                                         Substitute(Get-TradTxt (002100, 'Attendez que cet utilisateur sorte de la fiche &1.':T), Get-TradTxt (000345, 'Activit�':T)) + '|':U +
                                         Get-TradTxt (003634, 'Si le blocage persiste, demandez � votre administrateur de vous d�bloquer gr�ce aux utilitaires.':T)"}                     
        Return "Adm-Error":U.
     End.
  End.

  Do transaction:
     Create b-kact. 
     Rowid-Temp = Rowid(b-kact).

     If Mode = "Ajouter":U Then Do :
        Assign b-kact.csoc  = wcsoc
               b-kact.cetab = wcetab
               b-kact.cact  = temp-cact
               b-kact.cetat = {&CETAT-ACTIF}.
        Validate b-kact.
     End.
     Else Do :
        Find R-kact Where Rowid(R-kact) = Rowid(kact) No-Lock No-Error.

        If Error-Status:Error Then Do :
           Run dispatch ('Show-Errors':U).
           Return "Adm-Error":U.
        End.

        Assign B-kact.cact = temp-cact.
        
        /* JS 04/02/2004 : Ajout du No-Error */
        /* Permet de rafraichir l'enregistrement avant copie */
        Find current kact No-Lock No-Error.
        
        Buffer-Copy kact Except cact To b-kact.
        Release b-kact.
        
        For Each kactd of kact No-Lock :
           Create b-kactd.
           Assign B-kactd.cact = temp-cact.
           Buffer-Copy kactd Except cact To b-kactd.
           Release b-kactd.
           
           /* SLe 05/12/2012 Crit�res ent�te datef */
           Run DupCrit (Input kact.cact + ',':u + STRING(kactd.datef, '99/99/9999':U),
                        Input temp-cact + ',':u + STRING(kactd.datef, '99/99/9999':U)).

           /* NLE 26/07/2005 : Copie de ses liens vers documents */
           {&Run-LIEDOC-Copie}      ( INPUT kactd.csoc,
                                      INPUT kactd.cetab,
                                      INPUT {&ZLIEDOC-TYPCLE-FAB},
                                      INPUT kact.cact + ':':U + STRING(kactd.datef, '99/99/9999':U),
                                      INPUT temp-cact + ':':U + STRING(kactd.datef, '99/99/9999':U),
                                      INPUT majdoc).           
        End.
        
        /* Copie des sous-traitants de l'activit� */   
        For Each kactsst of kact No-Lock :
        
           Create B-kactsst.
           Assign B-kactsst.cact = temp-cact.
           Buffer-Copy kactsst Except cact To B-kactsst.
           Release b-kactsst.
        End.

        
        For Each kacto of kact No-Lock :
           Create b-kacto.
           Assign B-kacto.cact = temp-cact.
           Buffer-Copy kacto Except cact To b-kacto.
           Release b-kacto.
        End.
        
        For Each kactod of kact No-Lock :
           Create b-kactod.
           Assign B-kactod.cact = temp-cact.
           Buffer-Copy kactod Except cact To b-kactod.
           Release b-kactod.
        End.
        
        For Each kacta of kact No-Lock :
           Create b-kacta.
           Assign B-kacta.cact = temp-cact.
           Buffer-Copy kacta Except cact To b-kacta.
           Release b-kacta.
        End.        
        
        /* Lien version d'activit� - Process op�ratoire */
        FOR EACH bKactdpop WHERE bKactdpop.csoc  = Kact.csoc
                             AND bKactdpop.cetab = Kact.cetab
                             AND bKactdpop.cact  = Kact.cact
                             AND bKactdpop.cpope <> {&PROCESS-OP-ACTIVITE}
                           NO-LOCK:
            CREATE bufKactdpop.
            ASSIGN bufKactdpop.cact = temp-cact.
            BUFFER-COPY bKactdpop EXCEPT cact TO bufKactdpop.
            RELEASE bufKactdpop.                     
        END.
        
        /* Lien article d'activit� - Op�ration de Process op�ratoire */
        FOR EACH bKactapopd WHERE bKactapopd.csoc  = Kact.csoc
                             AND bKactapopd.cetab = Kact.cetab
                             AND bKactapopd.cact  = Kact.cact
                           NO-LOCK:
            CREATE bufKactapopd.
            ASSIGN bufKactapopd.cact = temp-cact.
            BUFFER-COPY bKactapopd EXCEPT cact TO bufKactapopd.
            RELEASE bufKactapopd.                     
        END.

        For Each kcom Where kcom.csoc = kact.csoc
                        And kcom.cetab = kact.cetab
                        And kcom.typcle = {&TYPCLE-COM-ACT}
                        And kcom.cle Begins ( kact.cact + ',':U ) No-Lock :
           Create b-kcom.
           Assign B-kcom.cle = kcom.cle.
           Assign Entry(1, B-kcom.cle, ',':U ) = temp-cact.
           Buffer-Copy kcom Except cle To b-kcom.
           Release b-kcom.
        End.

        For Each kactartr of kact No-Lock :
           Create b-kactartr.
           Assign B-kactartr.cact = temp-cact.
           Buffer-Copy kactartr Except cact To b-kactartr.
           Release b-kactartr.
        End.       

        /* Rajout CBR le 22/02/2002 pour contextes*/
        For Each kactctxu Where kactctxu.csoc   = kact.csoc
                            And kactctxu.cetab  = kact.cetab
                            And kactctxu.typcle = {&KACTCTXU-TYPCLE-ACTIVITE}
                            And kactctxu.cle    = kact.cact
                          No-Lock :
           Create b-kactctxu.
           Assign B-kactctxu.cle = temp-cact.
           Buffer-Copy kactctxu Except cle To b-kactctxu.
           Release b-kactctxu.
        End.   

        /* YG le 30/04/2002: gestion de l'acc�s concurrentiel en multi-utilisateur */
        If Mode = "Modifier":U Then Do:           
           Create bufKtact.
           Assign vToday            = TODAY
                  vTime             = TIME
                  bufKtact.csoc     = kact.csoc
                  bufKtact.cetab    = kact.cetab
                  bufKtact.typcle   = {&KTACT-TYPCLE-ACTIVITE}
                  bufKtact.cle      = kact.cact
                  bufKtact.ctemoin  = {&KTACT-CTEMOIN-MAJ-ACTIVITE}
                  bufKtact.zalpha   = temp-cact
                  bufKtact.datcre   = vToday
                  bufKtact.heurcre  = vTime
                  bufKtact.cusercre = wcuser
                  bufKtact.datmod   = vToday
                  bufKtact.heurmod  = vTime
                  bufKtact.cusermod = wcuser.
           Release bufKtact.
           Create bufKtact.
           Assign bufKtact.csoc     = kact.csoc
                  bufKtact.cetab    = kact.cetab
                  bufKtact.typcle   = {&KTACT-TYPCLE-ACTIVITE}
                  bufKtact.cle      = temp-cact
                  bufKtact.ctemoin  = {&KTACT-CTEMOIN-MAJ-ACTIVITE}
                  bufKtact.zalpha   = kact.cact
                  bufKtact.datcre   = vToday
                  bufKtact.heurcre  = vTime
                  bufKtact.cusercre = wcuser
                  bufKtact.datmod   = vToday
                  bufKtact.heurmod  = vTime
                  bufKtact.cusermod = wcuser.
           Release bufKtact.
        End.
        
        Run DupCrit (Input kact.cact,
                     Input temp-cact).
     End. /* Pas 'Ajouter' */

  End. /* Transaction */

  Modefct = Mode.
  If Mode = "Ajouter":U Then Assign w-Specif-Add = True. /* SLE 28/06/04 cf. Row-Available */
  Run Set-Info("Rowid-Temp=":U + String(Rowid-Temp), "Record-Source":u).
  Assign w-Specif-Add = False.  /* SLE 28/06/04 cf. Row-Available */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE DupCrit V-table-Win 
PROCEDURE DupCrit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  l'ancienne et la nouvelle cl�s
  Notes:       
------------------------------------------------------------------------------*/
   Define Input Parameter I-Cle1 AS CHARACTER No-Undo.
   Define Input Parameter I-Cle2 AS CHARACTER No-Undo.
   
   Define Buffer bKactcriv   For kactcriv.
   DEFINE BUFFER b2Kactcriv  FOR kactcriv.
   Define Buffer bufKactcriv For kactcriv.

   For Each kactcriv Fields (ccri)
                     Where kactcriv.csoc   = wcsoc
                       And kactcriv.cetab  = wcetab
                       And kactcriv.typcle = {&KCRIV-TYPCLE-ACT}
                       And kactcriv.cle    = I-Cle2
   No-Lock:
      If Not Can-Find (First bKactcriv Where bKactcriv.csoc   = wcsoc
                                         And bKactcriv.cetab  = wcetab
                                         And bKactcriv.typcle = {&KCRIV-TYPCLE-ACT}
                                         And bKactcriv.cle    = I-Cle1
                                         And bKactcriv.ccri   = kactcriv.ccri
                                       No-Lock)
      Then Do:
         Find First bufKactcriv Where Rowid(bufKactcriv) = Rowid(kactcriv)
         Exclusive-Lock No-Wait No-Error.
         If Locked (bufKactcriv)
         Then Do:
            {affichemsg.i &Message = "Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                                , ( I-cle2 + ',':U + kactcriv.ccri ) , 'kactcriv':U )"}
         End.
         Else Do:
            Delete bufKactcriv.
         End.
      End.
   End.

   For Each bKactcriv Where bKactcriv.csoc   = wcsoc
                        And bKactcriv.cetab  = wcetab
                        And bKactcriv.typcle = {&KCRIV-TYPCLE-ACT}
                        And bKactcriv.cle    = I-Cle1                    
                      No-Lock:
      Find First bufKactcriv Where bufKactcriv.csoc   = wcsoc
                               And bufKactcriv.cetab  = wcetab
                               And bufKactcriv.typcle = {&KCRIV-TYPCLE-ACT}
                               And bufKactcriv.cle    = I-Cle2
                               And bufKactcriv.ccri   = bKactcriv.ccri
                             Exclusive-Lock No-Error.
      If Not Available bufKactcriv Then 
         Create bufKactcriv.

      Assign bufKactcriv.cle = I-Cle2.
      Buffer-Copy bKactcriv Except cle To bufKactcriv.
      Release bufKactcriv.
   End.
   
   /* CD DP2374 duplication crit�re rendement dynamique */
   IF NUM-ENTRIES(I-cle1, ',':U) = 2 THEN DO:
      For Each b2Kactcriv Fields (ccri)
                          Where b2Kactcriv.csoc   = wcsoc
                            And b2Kactcriv.cetab  = wcetab
                            And b2Kactcriv.typcle = {&KACTCRIV-TYPCLE-ACT-RD}
                            And b2kactcriv.cle    = I-Cle2
                          No-Lock:
         If Not Can-Find (First bKactcriv Where bKactcriv.csoc   = wcsoc
                                            And bKactcriv.cetab  = wcetab
                                            And bKactcriv.typcle = {&KACTCRIV-TYPCLE-ACT-RD}
                                            And bKactcriv.cle    = I-Cle1
                                            And bKactcriv.ccri   = b2Kactcriv.ccri
                                          No-Lock)
         Then Do:
            Find First bufKactcriv Where Rowid(bufKactcriv) = Rowid(b2Kactcriv)
            Exclusive-Lock No-Wait No-Error.
            If Locked (bufKactcriv)
            Then Do:
               {affichemsg.i &Message = "Substitute(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                                   , ( I-cle2 + ',':U + b2Kactcriv.ccri ) , 'kactcriv':U )"}
            End.
            Else Do:
               Delete bufKactcriv.
            End.
         End.
      End.
   
      For Each bKactcriv Where bKactcriv.csoc   = wcsoc
                           And bKactcriv.cetab  = wcetab
                           And bKactcriv.typcle = {&KACTCRIV-TYPCLE-ACT-RD}
                           And bKactcriv.cle    = I-Cle1                    
                         No-Lock:
         Find First bufKactcriv Where bufKactcriv.csoc   = wcsoc
                                  And bufKactcriv.cetab  = wcetab
                                  And bufKactcriv.typcle = {&KACTCRIV-TYPCLE-ACT-RD}
                                  And bufKactcriv.cle    = I-Cle2
                                  And bufKactcriv.ccri   = bKactcriv.ccri
                                Exclusive-Lock No-Error.
         If Not Available bufKactcriv Then 
            Create bufKactcriv.
   
         Assign bufKactcriv.cle = I-Cle2.
         Buffer-Copy bKactcriv Except cle To bufKactcriv.
         Release bufKactcriv.
      End.
   END.
   /* fin CD DP2374 */ 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LinkFeatureID-Initialize V-table-Win 
PROCEDURE LinkFeatureID-Initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   { artcc.i f-cartvu NO NO }
   { fmactcc.i kact.cfmact NO NO }
   { secteucc.i kact.csecteur NO NO }
   { unitecc.i kact.cunite NO NO }
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record V-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN vTypMaj = "ADD":u. /* GLC 14/01/2005 */

  /* Code placed here will execute PRIOR to standard behavior. */
  {&SetCurs-Wait}
  Run Copie-Buffers ("Ajouter":U, NO).
  {&SetCurs-NoWait}
  If Return-Value = "Adm-Error":U Then Return "Adm-Error":U.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If Return-Value <> 'Adm-Error':U Then do:
    
    Run set-Info('Modefct=':U + Modefct ,'Container-Source':U).

    Run Dispatch ('adv-Clear-Modified':U).

    Run Maj-Buffer-Temp.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/ 

   DEFINE BUFFER bKactd      FOR kactd.
   DEFINE BUFFER b2Kactd     FOR Kactd.
   DEFINE BUFFER bKactdpop   FOR Kactdpop.
   DEFINE BUFFER bKactapopd  FOR Kactapopd.
   DEFINE BUFFER bufKactd    FOR kactd.
   DEFINE BUFFER bufKactdpop FOR Kactdpop.
   DEFINE BUFFER bufKacto    FOR kacto.
   DEFINE BUFFER bufKactod   FOR kactod.
   DEFINE BUFFER bufKacta    FOR kacta.
   DEFINE BUFFER bufKactapopd FOR Kactapopd.
   DEFINE BUFFER bufKactctxu FOR kactctxu.
   DEFINE BUFFER bufKactartr FOR kactartr.
   DEFINE BUFFER bufKcom     FOR kcom.
   DEFINE BUFFER bufKactsst  FOR kactsst.
   DEFINE VARIABLE w-cle     AS CHARACTER NO-UNDO.
   Define Variable w-nb      As Integer   No-Undo.
   Define variable vMess     As Character No-Undo.
   DEFINE VARIABLE vTypacta  AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vDatef    AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vVersart  AS INTEGER   NO-UNDO.
   DEFINE VARIABLE vVersartTmp AS INTEGER   NO-UNDO.
   DEFINE VARIABLE vFormatVersart AS INTEGER   NO-UNDO.
   DEFINE VARIABLE vNbEnrSup AS INTEGER   NO-UNDO.
   DEFINE VARIABLE vQte      AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vRet      AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vDatefEnCours AS DATE      NO-UNDO. /* MB DP001776 */
   
   /* R-kact est l'activit� en base / kact est l'activit� en cours de modification */

   /* Dispatch standard ADM method.                             */
   RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
   
   /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
   RUN P-TestDatefEnVigueur (INPUT ?, INPUT R-kact.cact, 
                             OUTPUT vDatefEnCours,
                             OUTPUT vRet, OUTPUT vMess).
   /* Cas de la cr�ation d'une activit� (ou modif tant que aucune date d'effet n'a �t� saisie) */
   IF vDatefEncours = ? THEN  
      ASSIGN vDatefEnCours = 01/01/1900.
   /* Fin DP001776 */
  
   /* RENSEIGNEMENT de TYPFLUX */
   Case kact.typflux:
      When "" Then Do:
      /* L'utilisateur n'a pas renseign� la zone : renseignement auto de typflux */
         Assign R-kact.typflux = {&TYPFLUX-POUSSE}.                         
         Find First kacta Where kacta.csoc  = R-kact.csoc
                            And kacta.cetab = R-kact.cetab 
                            And kacta.cact  = R-kact.cact
                            And kacta.cart  = R-kact.cart
                          No-Lock No-Error.
         If Available kacta Then Do:
            If kacta.typacta = {&TYPACTA-ENTRANT}
               Then R-kact.typflux = {&TYPFLUX-POUSSE}.
               Else R-kact.typflux = {&TYPFLUX-TIRE}.
         End.
         Assign kact.typflux = R-kact.typflux.
      End.
      When {&TYPFLUX-POUSSE} Or
      When {&TYPFLUX-TIRE} Then 
      /* L'utilisateur a lui-m�me renseign� la zone */ 
         Assign R-kact.typflux = kact.typflux.
     
   End Case.
   /* MAJ de la qte de la date d'effet par celle des entrants/sortants 
    * si d�crit par les op�rations */
   If W-T-Duree THEN
      For Each bKactd Where bKactd.csoc  = kact.csoc
                       And bKactd.cetab = kact.cetab
                       And bKactd.cact  = kact.cact
                       /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
                       AND bKactd.datef >= vDatefEnCours 
                     NO-LOCK:
         RUN Pr-CalculQte (bKactd.csoc,bKactd.cetab,bKactd.cact,bKactd.datef,
                           (if kact.typflux = {&TYPFLUX-TIRE} 
                               then {&TYPACTA-SORTANT}
                               else {&TYPACTA-ENTRANT}),
                           kact.cart,
                           OUTPUT vQte).
         /* maj qt� de kactd si elle ne correspond pas � celle de l'article de r�f�rence de l'activit� */
         IF vQte <> bKactd.lotq THEN DO:
            FIND FIRST b2Kactd WHERE ROWID (b2Kactd) = ROWID (bKactd) EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
            IF AVAILABLE b2Kactd THEN b2Kactd.lotq = vQte.
         END.
      End.

  /*------ Contr�le Global de l'activit� -------*/   
  /* CHU 20100519_0003 - QA002973 - AM67780 - nouvelle version du prg de contr�le g�n�ral de l'activit� */
  RUN actgen01cp.p ( BUFFER kact ,
                     TRUE,/* Test Global : test sur les co�ts */
                     OUTPUT vRet, 
                     OUTPUT vMess).
  IF vMess NE "":U THEN DO:
     IF NOT vRet THEN DO: /* un contr�le bloquant a �chou� */                
        ASSIGN Adv-MsgErreur = vMess.
        RETURN "Adm-Error":U.
     END.
     ELSE DO: /* un contr�le non bloquant a �chou� - est-ce qu'on continue ? */
        {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (022573, 'Confirmez-vous la modification (&1) ?':T), vMess)"
                      &TypeBtn = {&MsgOuiNon}
                      &BtnDef = 2}
        /*  arr�t de la mise � jour */
        IF ChoixMsg = 2 THEN RETURN "Adm-Error":U.
     END.
  END.       
  
   /* Traces Activit�s */
   RUN Assign-tt-traces (INPUT vDatefEnCours). /* MB DP001776 */
   
   /* G�n�ration de la trace */
   IF AVAILABLE ttKact-APRES THEN DO :
    {actji.i &CSOC     = "ttKact-APRES.csoc"
             &CETAB    = "ttKact-APRES.cetab"
             &CACT     = "ttKact-APRES.cact"
             &TYPCLEO  = "{&ZTRAMOD-TYPCLEO-ACT-MANUJ}"
             &TMAJALL  = "(vTypMaj = 'MOD':U OR vTypMaj = 'COP':U)"
             &NEW = "'V1'"}
   END.
  
  /*------- Recopie des buffers temporaires dans les r�els -------*/
  Assign R-kact.cact = F-cact No-Error.
  Buffer-Copy kact Except csoc cetab cact To R-kact.
  
  /* Recopie KACTD */ 
  /* MB DP001776 - Bloc inchang� car il faut pouvoir supprimer des dates d'effet "historiques" */ 
  For Each bKactd Of R-kact 
                  NO-LOCK :
     /* Suppression des liens du kactd vers les documents */
     {&Run-LIEDOC-Suppression} (wcsoc,
                                wcetab,
                                {&ZLIEDOC-TYPCLE-FAB},
                                bKactd.cact + ':':U + STRING(bKactd.datef, '99/99/9999':U),
                                OUTPUT vNbEnrSup).
  End.
  For Each bKactd Of R-kact 
                  WHERE Not Can-Find (First b-kactd Of kact 
                                      Where b-kactd.datef = bKactd.datef 
                                      No-Lock) 
                  EXCLUSIVE-LOCK :
     /* MB DP001776 - Oubli */
     RUN SupCrit (bKactd.cact + ',':U + STRING(bKactd.datef, '99/99/9999':U),
                  OUTPUT vRet, OUTPUT vMess).
     Delete bKactd.
  End.
  For each bKactd Of kact 
                  No-lock :
     Find bufKactd Of R-kact
                   Where bufKactd.datef = bKactd.datef 
                   Exclusive-Lock No-Error No-Wait.
     If Not Available bufKactd Then Create bufKactd.
     Assign bufKactd.cact = R-kact.cact.
     Buffer-Copy bKactd Except cact To bufKactd.
     ASSIGN vDatef = STRING(bKactd.datef, '99/99/9999':U).
     Run DupCrit (temp-cact + ',':u + vDatef,
                  F-cact:Screen-Value In Frame {&Frame-Name} + ',':u + vDatef).
     /* Copie de ses liens vers documents */
     {&Run-LIEDOC-Copie} (bKactd.csoc,bKactd.cetab,{&ZLIEDOC-TYPCLE-FAB},
                          bKactd.cact + ':':U + vDatef,
                          R-kact.cact + ':':U + vDatef,
                          YES).                        
  End.
  Release bufKactd. 
  /* Recopie KACTDPOP */
  FOR EACH bKactdpop OF R-kact
                     WHERE NOT CAN-FIND(FIRST Kactdpop OF Kact
                                        WHERE Kactdpop.datef = bKactdpop.datef
                                          AND Kactdpop.cpope = bKactdpop.cpope NO-LOCK)
                     NO-LOCK:
     FIND FIRST bufKactdpop WHERE ROWID(bufKactdpop) = ROWID(bKactdpop)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT LOCKED(bufKactdpop) THEN DELETE bufKactdpop.                 
  END.
  FOR EACH bKactdpop OF Kact
                     WHERE bKactdpop.datef >= vDatefEnCours  /* MB DP001776 */
                     NO-LOCK:          
     FIND FIRST bufKactdpop OF R-kact
                            WHERE bufKactdpop.datef = bKactdpop.datef
                              AND bufKactdpop.cpope = bKactdpop.cpope
                              
                              
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT AVAILABLE bufKactdpop THEN CREATE bufKactdpop.        
     ASSIGN bufKactdpop.cact = R-Kact.cact.
     BUFFER-COPY bKactdpop EXCEPT cact TO bufKactdpop.                               
  END.
  /* Recopie KACTO */ 
  For Each b-kacto Of R-kact 
                   WHERE Not Can-Find (First kacto Of kact 
                                       Where kacto.datef = b-kacto.datef
                                         And kacto.ni1 = b-kacto.ni1 No-Lock)
                   Exclusive-Lock :
     Delete b-kacto.
  End.
  For each b-kacto Of kact 
                   WHERE b-kacto.datef >= vDatefEnCours  /* MB DP001776 */
                   No-lock :
     Find bufKacto Of R-kact 
                   Where bufKacto.datef = b-kacto.datef 
                     And bufKacto.ni1  = b-kacto.ni1 
                   Exclusive-Lock No-Error No-Wait.
     If Not Available bufKacto Then Create bufKacto.
     Assign bufKacto.cact = R-kact.cact.
     Buffer-Copy b-kacto Except cact To bufKacto.
  End.
  Release bufKacto.
  /* Recopie KACTOD */  
  For Each bufKactod Of R-kact 
                     WHERE Not Can-Find (First b-kactod Of kact 
                                         Where b-kactod.datef = bufKactod.datef
                                           And b-kactod.ni1 = bufKactod.ni1 
                                           And b-kactod.nlig = bufKactod.nlig No-Lock)
                     Exclusive-Lock :
     Delete bufKactod.
  End.
  For each b-kactod Of kact 
                    WHERE b-kactod.datef >= vDatefEnCours  /* MB DP001776 */
                    No-lock :
     Find bufKactod Of R-kact 
                    Where bufKactod.datef = b-kactod.datef 
                      And bufKactod.ni1   = b-kactod.ni1 
                      And bufKactod.nlig  = b-kactod.nlig
                    Exclusive-Lock No-Error No-Wait.
     If Not Available bufKactod Then Create bufKactod.
     Assign bufKactod.cact = R-kact.cact.
     Buffer-Copy b-kactod Except cact To bufKactod.
  End.
  Release bufKactod. 
  /* Recopie KACTA */
  For Each bufKacta Of R-kact 
                    WHERE Not Can-Find (First b-kacta Of kact 
                                        Where b-kacta.datef   = bufKacta.datef 
                                          And b-kacta.typacta = bufKacta.typacta
                                          And b-kacta.ni1     = bufKacta.ni1 No-Lock)
                    Exclusive-Lock :
     Delete bufKacta.
  End.
  For each b-kacta Of kact 
                   WHERE b-kacta.datef >= vDatefEnCours  /* MB DP001776 */
                   No-lock :
     Find bufKacta Of R-kact 
                   Where bufKacta.datef   = b-kacta.datef 
                     And bufKacta.typacta = b-kacta.typacta 
                     And bufKacta.ni1     = b-kacta.ni1
                   Exclusive-Lock No-Error No-Wait.
     If Not Available bufKacta Then Create bufKacta.
     Assign bufKacta.cact = R-kact.cact.
     Buffer-Copy b-kacta Except cact To bufKacta.
  End.
  Release bufKacta. 
  /* Recopie KACTAPOPD */
  FOR EACH bKactapopd OF R-kact
                      WHERE NOT CAN-FIND(FIRST Kactapopd OF Kact
                                         WHERE Kactapopd.datef   = bKactapopd.datef
                                           AND Kactapopd.typacta = bKactapopd.typacta
                                           AND Kactapopd.ni1     = bKactapopd.ni1
                                           AND Kactapopd.cpope   = bKactapopd.cpope
                                           AND Kactapopd.datefpop = bKactapopd.datefpop NO-LOCK)
                   NO-LOCK:
     FIND FIRST bufKactapopd WHERE ROWID(bufKactapopd) = ROWID(bKactapopd)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT locked(bufKactapopd) THEN DELETE bufKactapopd.                 
  END.
  FOR EACH bKactapopd OF Kact 
                      WHERE bKactapopd.datef >= vDatefEnCours  /* MB DP001776 */
                      NO-LOCK:
     FIND FIRST bufKactapopd OF R-kact
                         WHERE bufKactapopd.datef   = bKactapopd.datef
                           AND bufKactapopd.typacta = bKactapopd.typacta
                           AND bufKactapopd.ni1     = bKactapopd.ni1    
                           AND bufKactapopd.cpope   = bKactapopd.cpope
                           AND bufKactapopd.datefpop = bKactapopd.datefpop
                         EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT AVAILABLE bufKactapopd THEN CREATE bufKactapopd.
     ASSIGN bufKactapopd.cact = R-Kact.cact.
     BUFFER-COPY bKactapopd EXCEPT cact TO bufKactapopd.                               
  END.
  /* Recopie KCOM */  
  For Each bufKcom Where bufKcom.csoc = R-kact.csoc
                     And bufKcom.cetab = R-kact.cetab
                     And bufKcom.typcle = {&TYPCLE-COM-ACT}
                     And bufKcom.cle Begins (R-kact.cact + ',':U)
                   EXCLUSIVE-LOCK:
    Assign w-cle = bufKcom.cle.
    Assign Entry(1,w-cle,',':U) = kact.cact.
    Find First b-kcom Where b-kcom.csoc = kact.csoc
                        And b-kcom.cetab = kact.cetab
                        And b-kcom.typcle = {&TYPCLE-COM-ACT}
                        And b-kcom.cle = w-cle
                      Exclusive-Lock No-Error No-Wait.
    If Not Available b-kcom THEN Delete bufKcom.
  End.
  BCLE-KCOM:
  For Each b-kcom Where b-kcom.csoc = kact.csoc
                    And b-kcom.cetab = kact.cetab
                    And b-kcom.typcle = {&TYPCLE-COM-ACT}
                    And b-kcom.cle Begins (kact.cact + ',':U)
                  NO-LOCK:
    IF DATE(ENTRY(2,b-kcom.cle,",":U)) < vDatefEnCours THEN /* MB DP001776 */
       NEXT BCLE-KCOM.              
    Assign w-cle = b-kcom.cle.
    Assign Entry(1,w-cle,',':U) = R-kact.cact.
    Find bufKcom Where bufKcom.csoc = R-kact.csoc
                   And bufKcom.cetab = R-kact.cetab
                   And bufKcom.typcle = {&TYPCLE-COM-ACT}
                   And bufKcom.cle = w-cle
                 Exclusive-Lock No-Error No-Wait.
     If Not Available bufKcom Then Create bufKcom.
     Assign bufKcom.cle = w-cle.
     Buffer-Copy b-kcom Except cle To bufKcom.
  End.
  Release bufKcom.
  /* Recopie KACTARTR */
  For Each b-kactartr OF R-kact
                      WHERE NOT CAN-FIND(FIRST kactartr OF Kact
                                         WHERE kactartr.datef = b-kactartr.datef
                                           AND kactartr.ni1   = b-kactartr.ni1
                                           AND kactartr.cartr = b-kactartr.cartr
                                           AND kactartr.cart  = b-kactartr.cart NO-LOCK)
                      NO-LOCK:
     /* Suppression d'art.de remplacement */
     FIND FIRST bufKactartr WHERE ROWID(bufKactartr) = ROWID(b-kactartr)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT LOCKED(bufKactartr) THEN DELETE bufKactartr.        
 
  End.
  For Each b-kactartr OF kact
                      WHERE b-kactartr.datef >= vDatefEnCours  /* MB DP001776 */
                      No-Lock:
      FIND FIRST bufKactartr OF R-kact /* MB Q07846-DP001776 */
                             WHERE bufKactartr.datef = b-kactartr.datef   
                               AND bufKactartr.ni1   = b-kactartr.ni1
                               AND bufKactartr.cartr = b-kactartr.cartr
                               AND bufKactartr.cart  = b-kactartr.cart           
                             EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      /* Cr�ation - mise � jour d'art. de remplacement */
      IF NOT AVAILABLE(bufKactartr) THEN Create bufKactartr.
      Assign bufKactartr.cact = R-kact.cact.
      Buffer-Copy b-kactartr Except cact To bufKactartr.
  End.
  Release bufKactartr.
  /* Recopie KACTCTXU */
  For Each bufKactctxu Where bufKactctxu.csoc   = R-kact.csoc
                         And bufKactctxu.cetab  = R-kact.cetab
                         And bufKactctxu.typcle = {&KACTCTXU-TYPCLE-ACTIVITE}
                         And bufKactctxu.cle    = R-kact.cact
                       Exclusive-Lock:
     Delete bufKactctxu.
  End.
  For Each B-kactctxu Where B-kactctxu.csoc   = kact.csoc
                        And B-kactctxu.cetab  = kact.cetab
                        And B-kactctxu.typcle = {&KACTCTXU-TYPCLE-ACTIVITE}
                        And B-kactctxu.cle    = kact.cact
                      No-Lock:
     Create bufKactctxu.
     Assign bufKactctxu.cle = R-kact.cact.
     Buffer-Copy B-kactctxu Except cle To bufKactctxu.
  End.
  Release bufKactctxu.

  /* Recopie KACTSST */
  For Each b-kactsst Of R-kact 
                   WHERE Not Can-Find (First kactsst Of kact 
                                       Where kactsst.csst = b-kactsst.csst
                                         No-Lock)
                   Exclusive-Lock :
     Delete b-kactsst.
  End.
  For each b-kactsst Of kact         
                   No-lock :
     Find bufKactsst Of R-kact 
                   Where bufKactsst.csst = b-kactsst.csst 
                   Exclusive-Lock No-Error No-Wait.
     If Not Available bufKactsst Then Create bufKactsst.
     Assign bufKactsst.cact = R-kact.cact.
     Buffer-Copy b-kactsst Except cact To bufKactsst.
  End.
  Release bufKactsst.

   /* Y a-t-il compatibilit� avec toutes les versions de l'activit� ? */
   ASSIGN vVersart = 0
          vFormatVersart = FormatVersart().
   For Each kactd Where kactd.csoc  = R-kact.csoc
                    And kactd.cetab = R-kact.cetab
                    And kactd.cact  = R-kact.cact
                  No-Lock
                  BY kactd.datef:
      Case kact.typflux:
         When {&TYPFLUX-POUSSE} THEN
            ASSIGN vTypacta = {&TYPACTA-ENTRANT}
                   vMess    = Get-TradTxt (002121, 'L''activit� &1 : &2 suit un flux &3, |� la date d''effet &4 l''article de r�f�rence &5 doit �tre un article entrant.':T).
         When {&TYPFLUX-TIRE} THEN
            ASSIGN vTypacta = {&TYPACTA-SORTANT}
                   vMess    = Get-TradTxt (002258, 'L''activit� &1 : &2 suit un flux &3, |� la date d''effet &4 l''article de r�f�rence &5 doit �tre un article sortant.':T). 
      END CASE.
      If Not Can-Find (First kacta Where kacta.csoc    = kactd.csoc
                                     And kacta.cetab   = kactd.cetab
                                     And kacta.cact    = kactd.cact
                                     And kacta.datef   = kactd.datef
                                     And kacta.typacta = vTypacta
                                     And kacta.cart    = R-kact.cart
                                   No-Lock)
      Then Do:
         /* Erreur dans une version */
         Adv-MsgErreur = 
           Substitute(vMess,
                      R-kact.cact,R-kact.lact,Get-TradClair(Input 'TYPFLUX':U,Input R-kact.typflux,Input '{&LCODE-TYPFLUX}':U,Input '{&LCLAIR-TYPFLUX}':U),
                      String(kactd.datef), R-kact.cart). 
         Apply "Entry":U To Cb-Typflux In Frame {&Frame-Name}.
         Return "Adm-Error":U.
      End.
      
      /* SLe 15/01/2013 Contr�le $VERSART */
      ASSIGN vVersartTmp = GetVERSART (kactd.csoc,kactd.cetab,kactd.cact,kactd.datef).
      IF vVersart > vVersartTmp AND NOT (vVersart = vFormatVersart AND vVersartTmp = 1)
      THEN DO:
         Adv-MsgErreur = SUBSTITUTE(Get-TradTxt (005815, 'Donn�es incoh�rentes (&1).':T), SUBSTITUTE(Get-TradTxt (002830, 'Crit�re &1':T), '$VERSART':u) + ' ' + Get-TradTxt (000353, 'Date d''effet':T) + ' ' + String(kactd.datef)).
         Return "Adm-Error":U.
      END.
      ASSIGN vVersart = vVersartTmp.
   End.

   Run DupCrit (Input temp-cact,
                Input F-cact:Screen-Value In Frame {&Frame-Name}).

   /* Quand on modifie l'unit�, il faut aller modifier aussi l'unit� des �ventuels kitidd correspondants */
   /* Quand on modifie l'activit� cela peut avoir des r�percutions sur les itin�raires article et unit�*/
   Run maj-itineraires.

   /* Test 1 seul qtediff par activit� (test fait lors de la cl�ture de l'OF) */
   RUN Pr-TestQtediff ({&Table}.csoc,{&Table}.cetab,{&Table}.cact,
                       OUTPUT vRet, OUTPUT vMess).
   IF NOT vRet THEN DO:
      Adv-MsgErreur = vMess.
      Return "Adm-Error":U.
   End.

   Run Annuler-Buffer-Temp.          

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement V-table-Win 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
/* ATTENTION : dans cette procedure kact contient le buffer de recopie temporaire  !!
               si adm-new-record, kact pointe en d�but de proc�dure sur le record
               cr��, mais on inverse les buffers pour la suite */

  /*------ Lectures des buffers ------*/
  If adm-new-record Then Do :
     Assign kact.csoc   = wcsoc
            kact.cetab  = wcetab
            kact.datcre = Today.

     Find R-kact Where Rowid(R-kact) = Rowid(kact) Exclusive-Lock No-Error.
     {&_proparse_prolint-nowarn(findexcl)}
     Find kact   Where Rowid(kact)   = Rowid-Temp  Exclusive-Lock No-Error.
  End.
  Else Do :
     Find current R-kact Exclusive-Lock No-Error No-Wait.
     If Not Available R-kact Then Do :
        RUN Recup-MsgErreur-Adm.
        Return "Adm-Error":U.
     End.
     If Current-Changed(R-kact) Then Do :
        Adv-MsgErreur = Get-TradTxt (002172, 'D�sol�, l''enregistrement a �t� modifi� par un autre utilisateur.':T).
        Reset-Modif = Yes.   /* Pour faire le reset-record en dehors de la transaction
                              * sinon le delete record temporaire est annul� par le undo
                              * de la transaction */
        Return "Adm-Error":u.
     End. 
  End.

  /*------ Assignation des screen-value dans buffer temporaire ------*/
  Run Assign-Buffer-Temp.
  
  If Adm-New-Record Then 
     Assign kact.datcre = Today. 
  
  Assign kact.datmod = Today
         kact.cuser  = wcuser
         kact.Cart   = F-Cartvu.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-cancel-record V-table-Win 
PROCEDURE local-cancel-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* message de confirmation d'abandon */
  {affichemsg.i       &Message = "Get-TradMsg (002200, 'ATTENTION : Vous allez abandonner TOUTE la saisie sur l''activit�.|Confirmez-vous ?':T)"
         &TypeBtn = {&MsgOuiNon}
         &TypeMsg = {&MsgQuestion}
         &BtnDef = 2
  }

  If ChoixMsg = 2 Then Return "Adm-Error":u.
  
  
 
  Run Annuler-Buffer-Temp.
  Run Dispatch('Adv-Clear-Modified':U).
  If Adm-New-Record Then 
     Run New-State("Cancel-Saisie,Record-Source":U).
  Else
     Run New-State("Fin-Saisie,Record-Source":u).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'cancel-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-copy-record V-table-Win 
PROCEDURE local-copy-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  ASSIGN vTypMaj = "COP":u. /* GLC 14/01/2005 */
  
  /* Code placed here will execute PRIOR to standard behavior. */
  w-cact = kact.cact.    /* pour duplication des crit�res */

  {&SetCurs-Wait}
  Run Copie-Buffers ("Copier":U, YES).
  {&SetCurs-NoWait}
  If Return-Value = "Adm-Error":U Then
     Return "Adm-Error":U.  
       
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'copy-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If Return-Value <> 'Adm-Error':U Then
  
  Run Set-Info('ModeFct=':U + ModeFct ,'container-source':U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record V-table-Win 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  If available kact and
     kact.cetat = {&CETAT-ACTIF} 
  Then do: 
     {affichemsg.i       &Message = "Get-TradMsg (002209, 'L''activit� est � l''�tat Actif (onglet suite 1)  : |sa suppression est impossible.':T)"}
     Return "Adm-Error":U.
  End.

  {&setcurs-wait}

  if Can-Find (first kitidd where kitidd.csoc = wcsoc
                              and kitidd.cetab = wcetab
                              and kitidd.cact = f-cact:screen-value in frame {&FRAME-NAME}
                            use-index i2-itidd
                            no-lock)
  then do:
     {affichemsg.i       &Message = "Get-TradMsg (002214, 'Cette activit� fait partie d''un itin�raire : |sa suppression est impossible.':T)"}
     Return "Adm-Error":U.
  end.

  {&setcurs-nowait}
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .
  IF RETURN-VALUE = 'Adm-Error':U THEN RETURN "ADM-ERROR":U. /* OM : 23/01/2006 */
 
  Run new-State('nouveau,container-source':U).  
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-statement V-table-Win 
PROCEDURE local-delete-statement :
/*------------------------------------------------------------------------------
Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE BUFFER bKactsst      FOR kactsst.
  DEFINE BUFFER bKactd      FOR kactd.
  DEFINE BUFFER bKactdpop   FOR Kactdpop.
  DEFINE BUFFER bKacta      FOR kacta.
  DEFINE BUFFER bKactapopd   FOR Kactapopd.
  DEFINE BUFFER bKacto      FOR kacto.
  DEFINE BUFFER bKactod     FOR kactod.
  DEFINE BUFFER bKactctxu   FOR kactctxu.
  DEFINE BUFFER bKactartr   FOR kactartr.
  DEFINE BUFFER bKactcriv   FOR kactcriv.
  
  /* mise � jour */
  DEFINE BUFFER bufKactsst    FOR kactsst.  
  DEFINE BUFFER bufKactd    FOR kactd.
  DEFINE BUFFER bufKactdpop FOR Kactdpop.
  DEFINE BUFFER bufKacta    FOR kacta.
  DEFINE BUFFER bufKactapopd FOR Kactapopd.
  DEFINE BUFFER bufKacto    FOR kacto.
  DEFINE BUFFER bufKactod   FOR kactod.
  DEFINE BUFFER bufKactctxu FOR kactctxu.
  DEFINE BUFFER bufKactartr FOR kactartr.
  DEFINE BUFFER bufKactcriv FOR kactcriv.
    
  DEFINE VARIABLE vNbEnregSup AS INTEGER   NO-UNDO.  /* AMA 22/02/2006 */

  /* Code placed here will execute PRIOR to standard behavior. */
  
  {&setcurs-wait}  
  
   /* GLC 14/02/2005 : Traces Activit�s */
   RUN Raz-tt-traces.
   
   /* G�n�ration de la trace */
   {actji.i &CSOC     = "wcsoc"
            &CETAB    = "wcetab"
            &CACT     = "kact.cact"
            &TYPCLEO  = "{&ZTRAMOD-TYPCLEO-ACT-MANUJ}"
            &NEW      = "OUI"}

  /*-- Suppression des dates d'effets. --*/
  For Each bKactd FIELDS(cact datef)
                   Where bKactd.csoc  = wcsoc
                     And bKactd.cetab = wcetab
                     And bKactd.cact  = kact.cact
                  NO-LOCK :
      /* NLE 26/07/2005 : Suppression des liens du kactd vers les documents */
      {&Run-LIEDOC-Suppression} (INPUT wcsoc,
                                 INPUT wcetab,
                                 INPUT {&ZLIEDOC-TYPCLE-FAB},
                                 INPUT bKactd.cact + ':':U + STRING(bKactd.datef, '99/99/9999':U),
                                 OUTPUT vNbEnregSup). /* AMA 22/02/2006 */
                                       
      FIND FIRST bufKactd WHERE ROWID(bufKactd) = ROWID(bKactd)
                          EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      IF NOT LOCKED(bufKactd) THEN            
         Delete bufKactd.
  End.  
  
  /* Suppression des liens version d'activit� - Process op�ratoire*/    
  FOR EACH bKactdpop FIELDS({&EmptyFields})
                      WHERE bKactdpop.csoc  = wcsoc
                        AND bKactdpop.cetab = wcetab
                        AND bKactdpop.cact  = kact.cact
                     NO-LOCK:          
     FIND FIRST bufKactdpop WHERE ROWID(bufKactdpop) = ROWID(bKactdpop)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT LOCKED(bufKactdpop) THEN
        DELETE bufKactdpop.
  END.
  
  FOR EACH bKactsst FIELDS({&EmptyFields})
                      WHERE bKactsst.csoc  = wcsoc
                        AND bKactsst.cetab = wcetab
                        AND bKactsst.cact  = kact.cact
                     NO-LOCK:          
     FIND FIRST bufKactsst WHERE ROWID(bufKactsst) = ROWID(bKactsst)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT LOCKED(bufKactsst) THEN
        DELETE bufKactsst.
  END.
  
  /*-- Suppression des entrants, sortants et articles de co�t. --*/
  For Each bKacta FIELDS({&EmptyFields})
                   Where bKacta.csoc  = wcsoc
                     And bKacta.cetab = wcetab
                     And bKacta.cact  = kact.cact
                  NO-LOCK :         
      FIND FIRST bufKacta WHERE ROWID(bufKacta) = ROWID(bKacta)
                          EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      IF NOT LOCKED(bufKacta) THEN 
         Delete bufKacta.
  End.
        
  /* Suppression des liens article d'activit� - Op�ration de Process op�ratoire*/    
  FOR EACH bKactapopd FIELDS({&EmptyFields})
                      WHERE bKactapopd.csoc  = wcsoc
                        AND bKactapopd.cetab = wcetab
                        AND bKactapopd.cact  = kact.cact
                     NO-LOCK:
     FIND FIRST bufKactapopd WHERE ROWID(bufKactapopd) = ROWID(bKactapopd)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT LOCKED(bufKactapopd) THEN
        DELETE bufKactapopd.
  END.
  
  /*-- Suppression des op�rations. --*/
  For Each bKacto FIELDS({&EmptyFields})
                   Where bKacto.csoc  = wcsoc
                     And bKacto.cetab = wcetab
                     And bKacto.cact  = kact.cact
                  NO-LOCK :
     FIND FIRST bufKacto WHERE ROWID(bufKacto) = ROWID(bKacto)
                         EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT LOCKED(bufKacto) THEN
        Delete bufKacto.    
  End.
        
  /*-- Suppression du d�tail des op�rations. --*/
  For Each bKactod FIELDS({&EmptyFields})
                    Where bKactod.csoc  = wcsoc
                      And bKactod.cetab = wcetab
                      And bKactod.cact  = kact.cact
                   NO-LOCK :
      FIND FIRST bufKactod WHERE ROWID(bufKactod) = ROWID(bKactod)
                           EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      IF NOT LOCKED(bufKactod) THEN
         Delete bufKactod.          
  End.

  /*-- Suppression des contextes --*/
  For Each bKactctxu FIELDS({&EmptyFields})
                      Where bKactctxu.csoc   = wcsoc
                        And bKactctxu.cetab  = wcetab
                        And bKactctxu.typcle = {&KACTCTXU-TYPCLE-ACTIVITE}
                        And bKactctxu.cle    = kact.cact
                     NO-LOCK :
      FIND FIRST bufKactctxu WHERE ROWID(bufKactctxu) = ROWID(bKactctxu)
                             EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      IF NOT LOCKED(bufKactctxu) THEN
         Delete bufKactctxu.          
  End.

  /* JS 21/04/2004 */
  /*-- Suppression des articles de remplacement --*/
  For Each bKactartr FIELDS({&EmptyFields}) 
                      Where bKactartr.csoc  = wcsoc
                        And bKactartr.cetab = wcetab
                        And bKactartr.cact  = kact.cact
                     NO-LOCK :
     FIND FIRST bufKactartr WHERE ROWID(bufKactartr) = ROWID(bKactartr)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT LOCKED(bufKactartr) THEN
        Delete bufKactartr.
  End.
  
  /*-- Suppression des restrictions frais generaux / activit� --*/
  {&BS-CTFF-SuppressionFraisActivite} (wcsoc,wcetab,kact.cact).

  /* CD DP2374 suppression des crit�res de toutes les dates d'effet */
  For Each bkactcriv FIELDS({&EmptyFields}) 
                      Where bkactcriv.csoc   = wcsoc
                        And bkactcriv.cetab  = wcetab
                        AND bkactcriv.typcle = {&KACTCRIV-TYPCLE-ACT}
                        AND bkactcriv.cle  BEGINS kact.cact
                        And num-entries(bkactcriv.cle) = 2
                     NO-LOCK :
     FIND FIRST bufKactcriv WHERE ROWID(bufKactcriv) = ROWID(bkactcriv)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT LOCKED(bufKactcriv) THEN
        Delete bufKactcriv.
  End.
  For Each bkactcriv FIELDS({&EmptyFields}) 
                      Where bkactcriv.csoc   = wcsoc
                        And bkactcriv.cetab  = wcetab
                        AND bkactcriv.typcle = {&KACTCRIV-TYPCLE-ACT-RD}
                        AND bkactcriv.cle    BEGINS kact.cact
                        And num-entries(bkactcriv.cle) = 2
                     NO-LOCK :
     FIND FIRST bufKactcriv WHERE ROWID(bufKactcriv) = ROWID(bkactcriv)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF NOT LOCKED(bufKactcriv) THEN
        Delete bufKactcriv.
  End.

  {&setcurs-nowait}
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  ASSIGN w-cunite = "":U
         w-cart = "":U.

  If ModeFct <> "Ajouter":U Then Do :
    if available kact then do:
       w-cunite = kact.cunite.

       If Not kact.cact begins "$":U Then
          F-cact = kact.cact.
       Display F-cact With Frame {&Frame-Name}.
        
       run set-valeur-combo(cb-typflux:handle in frame {&frame-name},kact.typflux).
       assign frame {&FRAME-NAME} cb-typflux.
       
       Run Set-Info('Record-Available=Yes':U,'container-source':U).
     
       Assign F-Cartvu = Kact.Cart
              w-cart   = Kact.Cart.

     end.
     else Run Set-Info('Record-Available=No':U,'container-source':U).  
  End.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  { secteucc.i     kact.csecteur   No Yes zsecteur.lsecteur }
  { fmactcc.i      kact.cfmact     No Yes kfmact.lfmact }
  { artcc.i        F-cartvu        No Yes zart.lart 1 '' '' zart.rart} 
  
  /* L�gende en gras */
  RUN Set-Info('D�sactive=':u + String(Available {&Table} And {&Table}.cetat = {&CETAT-DESACTIF}, 'oui/non':u),'Container-Source':u).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-end-update V-table-Win 
PROCEDURE local-end-update :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       24/04/02 Ajout du test (avant il �tait dans assign-record
------------------------------------------------------------------------------*/
   Define Variable w-lanct Like zstade.cstade No-Undo.

  /* Code placed here will execute PRIOR to standard behavior. */
  
   /* Warning si les crit�res ACTIVITES 
    * non inclus dans les crit�res FAM.ELEMENTAIRES ARTICLES du stade "Lancement"
    */
   If w-cfam = "" Then Do:
      {affichemsg.i &Message = "Get-TradMsg (002240, 'Je ne connais pas la famille �l�mentaire de l''article de r�f�rence.':T)"}
      Apply "Entry":U to f-cartvu In Frame {&Frame-Name}.      
      Return "Adm-Error":U.
   End.
   
   Case {&Table}.typflux:   /* R-kact.typflux */
      When {&TYPFLUX-POUSSE} Then w-lanct = 'LANCTPOU':u.
      When {&TYPFLUX-TIRE}   Then w-lanct = 'LANCTTIR':u.
      OtherWise                   w-lanct = '':u.
   End Case.

   /* SLe 05/12/12 Sciemment, je ne change rien ici et ne teste pas les crit�res � datef (mais � faire + tard) +
    *
    */
   BOUCLE:
   For Each kactcriv Where kactcriv.csoc   = wcsoc
                       And kactcriv.cetab  = wcetab
                       And kactcriv.typcle = {&KCRIV-TYPCLE-ACT}
                       And kactcriv.cle    = {&Table}.cact 
                  No-Lock:
      If Not {&Stade-CritExistence} (wcsoc,wcetab,f-cartvu:Screen-Value In Frame {&Frame-Name},w-lanct,Today,"",kactcriv.ccri)
      Then Do:
         {affichemsg.i  &Message = "SUBSTITUTE(Get-TradMsg (002222, 'Attention ! |Le crit�re &1 n''apparait pas parmi les crit�res du stade Cr�ation O.F.':T), kactcriv.ccri)"}  /* ALB 20140715_0002 Modification message Lancement --> Cr�ation */
         Leave BOUCLE.
      End.
   End.

  If Not Adm-New-Record Then Do :
     Run Dispatch ('adv-clear-modified':U).
     Run New-State ("Fin-saisie,Record-Source":U).
  End.
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'end-update':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If w-itichanged <> "":U Then Do:
     {affichemsg.i       &Message = "Get-TradTxt (021462, 'Certains itin�raires ont �t� modifi�s :':T) + '|':U + String(w-itichanged)" }
     w-itichanged = "":U.
  End.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-hide V-table-Win 
PROCEDURE local-hide :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'hide':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If ModeFct <> "Consult":U Then
     Run Maj-Buffer-Temp.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize V-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   Define Variable w-Liste As Character No-Undo.
   Define Variable w-mess        As Character      No-Undo. 
   Define Variable w-type-01     As Character      No-Undo. 
   Define Variable w-Ret         As Logical        No-Undo. 
   DEFINE BUFFER bKactctx FOR kactctx.   /* MB G7_1659 08/11/05 */

   Run Ajoute-Valeur-Combo 
      (Input cb-typflux:handle in frame {&frame-name},
       Input 'AUC':U,
       Input '.',
       Input yes).
   
   /* Code placed here will execute PRIOR to standard behavior. */
   Display cb-typflux with frame {&Frame-Name}.

  {getparii.i &CPACON="'ACTIVITE':U" 
              &NOCHAMP=3
              &CSOC=wcsoc
              &CETAB=wcetab
              &OUT-valeurs=w-Liste
  }
  wtactart = (vParam-Ok And W-liste = 'Yes':u).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  /* On place le bouton d'aide du code activit� 
   * il est superpos� sur le bouton d'aide de l'article 
   */
  
  If wtactart 
  Then Do with frame {&frame-name} :
     Assign B-cact:Y = F-cact:Y 
            B-cact:X = F-cact:X + F-cact:Width-Pixels
            ret = B-cact:Move-after-tab-item(F-cact:handle). 
  End.
  /* JS 18/11/2003 : Correction Bug (affichage de la boite d'aide � la saisie d'un article lorsque 
                     le param�tre 'Activit�\Mise en place de la saisie simplifi�e des libell�s est � Non') */
  Else
     B-cact:hidden = true.

  {getparii.i &CPACON="'ACTIVITE':U" 
              &NOCHAMP=1
              &CSOC=wcsoc
              &CETAB=wcetab
              &OUT-valeurs=w-Liste
  } 
  W-T-Duree = (vParam-Ok And W-liste = '1':u).
  
  /* MB G7_1659 08/11/05 */
  /* Mise � jour du t�moin indiquant si le contexte Reconstitu� a �t� */
  /* import� � partir des contextes VIF pour cette soci�t� / �tab     */
  /* ---------------------------------------------------------------- */
  ASSIGN vgtCtxRec = CAN-FIND (bKactctx WHERE bKactctx.cSoc    = wcSoc
                                          AND bKactctx.cEtab   = wcEtab
                                          AND bKactctx.cActCtx = {&KACTCTXV-CACTCTX-RECONSTITUE} 
                                        NO-LOCK). 
  /* Fin MB G7_1659 */
  
  
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-modify-record V-table-Win 
PROCEDURE local-modify-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   /* MB DP00001776 */
   DEFINE VARIABLE vDatef        AS DATE      NO-UNDO.
   DEFINE VARIABLE vDatefEnCours AS DATE      NO-UNDO.
   DEFINE VARIABLE vRet          AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vMsg          AS CHARACTER NO-UNDO.
/* -------------------------------------------------------------------------- */  

  ASSIGN vTypMaj = "MOD":u. /* GLC 14/01/2005 */
  
  /* Code placed here will execute PRIOR to standard behavior. */
  /* copie de l'enreg dans un enreg interm�diaire */
  /* SLE 17/01/05 on garde en m�moire la datef sur laquelle on est positionn�,
   * pour bien s'y replacer au terme du modify
   */
  RUN Get-Info ("DatefModify":u,"InfoM-Target":U).
  /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
  IF RETURN-VALUE NE ? THEN ASSIGN vDatef = DATE(RETURN-VALUE).
  RUN P-TestDatefEnVigueur (INPUT vDatef, INPUT {&TABLE}.cact,
                            OUTPUT vDatefEnCours,
                            OUTPUT vRet, OUTPUT vMsg).
  IF NOT vRet THEN DO:
     {affichemsg.i &MESSAGE = "vMsg"}
     RETURN.
  END.
  /* Fin MB DP001776 */
  
  Run Set-Info("DatefModify=":U + (IF RETURN-VALUE NE ? THEN STRING(RETURN-VALUE) ELSE ""), "Container-Source":U).
  /* --- */

  {&SetCurs-Wait}
  Run Copie-Buffers("Modifier":U, NO).
  {&SetCurs-NoWait}
  
  If Return-Value = "adm-error":u Then Return "Adm-Error":U. 
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'modify-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If Return-Value = "Adm-Error":U Then Do :
     RUN Annuler-Buffer-Temp.
     Return "Adm-Error":U.
  End.
               
  Run Set-Info("ModeFct=":U + ModeFct, "Container-Source":U).
  /* SLE 17/01/05 */
  Run Set-Info("DatefModify=":U, "Container-Source":U).
  /* --- */
  Reset-Modif = No.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-reset-record V-table-Win 
PROCEDURE local-reset-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  /* message de confirmation d'abandon */
  {affichemsg.i 
       &Message = "Get-TradMsg (002200, 'ATTENTION : Vous allez abandonner TOUTE la saisie sur l''activit�.|Confirmez-vous ?':T)"
       &TypeMsg={&MsgQuestion}
       &TypeBtn={&MsgOuiNon}
       &BtnDef =2
  }
  If ChoixMsg = 2 Then Return "Adm-Error":u.
  

  Run Annuler-Buffer-Temp.
  Run Dispatch('Adv-Clear-Modified':U).
  Run New-State("Fin-Saisie,Record-Source":U).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'reset-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available V-table-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE VARIABLE vRet AS  LOGICAL   NO-UNDO.
   DEFINE VARIABLE vMsg AS  CHARACTER NO-UNDO.
   
   DEFINE BUFFER   bKactdpop FOR Kactdpop.
/*    DEFINE BUFFER   bKacto   FOR Kacto.  */
/*    DEFINE BUFFER   bKpope   FOR Kpope.  */
/*    DEFINE BUFFER   bufKpope FOR Kpope.  */

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  /* SLe 28/06/04 - Bug quand on faisait nouveau sur un browse vide (car s�lections),
   * ; on annule ; on fait une s�lection pour avoir un browse non vide et on change 
   * de ligne => il nous demande si on veut valider car il a d�tect� des modifs ???
   */
  If Not w-Specif-Add Then
     RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  /* il ne doit y avoir qu'un seul process op�ratoire temporaire */
  /* et ce doit �tre celui de l'activit� courante                                */
  IF CAN-FIND(FIRST bKactdpop WHERE bKactdpop.csoc  = Kact.csoc 
                                AND bKactdpop.cetab = Kact.cetab
                                AND bKactdpop.cpope = {&PROCESS-OP-ACTIVITE}
                                AND bKactdpop.cact  <> Kact.cact                                   
                              NO-LOCK) THEN DO:                                                                                        
     RUN P-Delete-OPACT (OUTPUT vRet, OUTPUT vMsg).
     IF NOT vRet THEN DO:                          
        {affichemsg.i &Message = "vMsg"}           
        RETURN 'Adm-Error':U.                      
     END.                                          
  END.                                                                                                          
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-Test-Saisie V-table-Win 
PROCEDURE local-Test-Saisie :
/*------------------------------------------------------------------------------
  Purpose:     Tester la saisie des variables du viewer
  Parameters:  - Test Global, ou Pour un widget 
               - Handle du widget � tester.
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Test-Global  As Logical          No-Undo.
    Define Input Parameter Widget-Test  As Widget-Handle    No-Undo.

    Define variable w-typact     like kact.typact  no-undo.
    Define Variable W-Cart       Like Zart.Cart    No-Undo.
    Define Variable W-TypfluxAct Like kact.typflux No-Undo.
    
    /* CLE 03/09/2003 */
    /* cdu 15/12/2003 no-undo*/
    Define Variable O-Ret        As Logical        No-Undo.
    Define Variable O-Mes-Ret    As Character      No-Undo.
    
    If (Test-Global 
        Or Widget-Test = F-cact:Handle in Frame {&Frame-Name}) And
       Adm-new-record And
       Can-Find (First kact Where kact.csoc  = wcsoc
                              And kact.cetab = wcetab    
                              And kact.cact  = Input Frame {&Frame-Name} F-cact
                            No-Lock)
    Then Do :
       /*{affichemsg.i       &Message = "Get-TradMsg (002242, 'Cette activit� existe d�j�.':T)"}*/ /* CHU 20090615_0008 - op�ration reprise messages pour 13125 Cette valeur existe d�j� */
       {affichemsg.i &Message="SUBSTITUTE(Get-TradMsg (013125, 'Cette valeur existe d�j� (&1).':T), Get-TradTxt (000345, 'Activit�':T))"}
       Apply "Entry":U to F-cact.
       Return "Adm-Error":U.
    End.

    /* 
     *  Utilisation du param�tre ACTART
     */
    If Widget-Test = F-cact:Handle in Frame {&Frame-Name} And
       wtactart
    Then Do with Frame {&Frame-Name} :
       Find First zart where zart.csoc = wcsoc
                         And zart.cart = F-cact:screen-value
       No-Lock No-Error.
       If available zart then do:
          Assign kact.lact:screen-value = zart.lart
                 kact.ract:screen-value = zart.rart
                 f-cartvu:screen-value  = zart.cart
                 zart.lart:screen-value = zart.lart
                 zart.rart:screen-value = zart.rart.
       End.
    End.

    If Test-Global 
       Or Widget-Test = f-cartvu:Handle in Frame {&Frame-Name} 
    Then Do :
        { artcc.i f-cartvu yes yes zart.lart 1 '' '' zart.rart}     /* NLE 11/07/2002 : ajout rart */
        If kact.cunite:Screen-Value = "" And Available Buf1-Zart Then Do :
            Assign kact.cunite:Screen-Value = buf1-zart.cug.
        End.
        If Available Buf1-Zart then do:
          w-cfam = buf1-zart.cfam.   /* Utile dans l'end-update */
          
          /* JS (30/10/2003) : Si article = ZZARTVIF, on ne v�rifie pas s'il est fabriqu� */
          If F-cartvu:Screen-Value NE "ZZARTVIF":U Then Do:
            Find First zartfab where zartfab.csoc  = Buf1-Zart.csoc
                                 And zartfab.cetab = wcetab
                                 And zartfab.cart  = Buf1-Zart.cart
            No-Lock No-Error.       
            If Not available zartfab Or 
               Not zartfab.Tfab Then Do:
              {affichemsg.i &Message = "Get-TradMsg (002249, 'Cet article n''est pas ''fabriqu�''.':T)"}
              Apply "Entry":U to f-cartvu.
              Return "Adm-Error":U.
            End.
          End.

          Run Get-Valeur-Combo(Cb-typflux:Handle,Output W-TypfluxAct).
          If w-typfluxAct = "" And
             Not Test-Global 
          Then
            Run Set-Valeur-Combo(Cb-typflux:Handle,zartfab.typfluxOF).    
        End.
        
        /* CLE 03/09/2003 */
        If Test-Global Then Do :
           Run test-itineraires (Output O-Ret,
                                 Output O-Mes-Ret).
           If Not O-Ret Then Do :
              if O-Mes-Ret <> "":U Then
                 {affichemsg.i &Message="O-Mes-Ret" }
              Return "Adm-Error":U.
           End.
        End.
    End.

    /* JS (30/10/2003) : Si article = ZZARTVIF, le champ csecteur peut �tre vide */
    If Test-Global 
       Or Widget-Test = kact.csecteur:Handle In Frame {&Frame-Name} Then Do :
      If kact.csecteur:Screen-Value NE "" Then Do:
        { secteucc.i kact.csecteur yes yes zsecteur.lsecteur }
      End.
      Else Do: 
        If F-cartvu:Screen-Value NE "ZZARTVIF":U Then Do:
          {affichemsg.i &Message = "Substitute(Get-TradMsg (000066, 'Cette zone est obligatoire (&1).':T), 'Secteur':U)"}
          Apply "Entry":U to kact.csecteur in Frame {&Frame-Name}.
          Return "Adm-Error":U.
        End.
      End.
    End.  
        
    If Test-Global 
       Or Widget-Test = kact.cfmact:Handle in Frame {&Frame-Name} Then Do :     
       { fmactcc.i kact.cfmact yes yes kfmact.lfmact}
       
       /* Appel de la saisie des crit�res */
       If Not Test-Global Then
          Run New-State ("Critere,Group-Assign-Target":U).
          
    End. /* test sur la famille d'activit�s */


    /*-- L'unite saisie doit �tre une des deux unit�s de l'article ou autres unit�s. --*/
    If (Test-Global 
        Or Widget-Test = kact.cunite:Handle in Frame {&Frame-Name})
    Then Do With Frame {&Frame-Name}:
       {unitecc.i kact.cunite Yes no "" }
       {&Run-unite-Test-Unite-Article} (Input  wcsoc,
                                        Input  F-cartvu:screen-Value,
                                        Input  kact.cunite:Screen-Value,
                                        Output O-unite-retour,
                                        Output O-unite-Mess).
       If Not O-unite-retour Then Do:
          {affichemsg.i
            &Message="O-unite-Mess"
          }
          Apply "Entry":u to kact.cunite in Frame {&Frame-Name}.
          Return "Adm-Error":U.
       End.

/*
 *        Assign W-Cart = Input Frame {&Frame-Name} F-cartvu.
 *        Find zart Where zart.csoc = wcsoc
 *                    And zart.cart = W-Cart
 *                    No-Lock No-Error.
 *        If Available zart And 
 *           kact.cunite:Screen-Value <> zart.cug And 
 *           kact.cunite:Screen-Value <> zart.cus
 *        Then do:
 *          Find First zartu Where zartu.csoc = wcsoc
 *                             And zartu.cart = W-Cart
 *                             And zartu.cunite = kact.cunite:Screen-Value
 *                           No-Lock No-Error.
 *          If not Available zartu Then Do :
 *            {affichemsg.i
 *               &Message="'L''unit� doit �tre l''une des unit�s de l''article.':T100"
 *            }
 *            Apply "Entry":U to kact.cunite.
 *            Return "Adm-Error":U.
 *          End.            
 *        End.  */
    End.
   
    Return.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-update-record V-table-Win 
PROCEDURE local-update-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  run maj-info-atelier.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  /* MB G7_1659 25/10/05 - Si le contexte Reconstitu� a �t� */
  /* "import�" � partir des contextes VIF pour la soc/�tab, */
  /* Alors mise � jour de zartfab.tReconst en fonction de   */
  /* l'existence du contexte reconstitu� pour une activit�  */
  /* � l'�tat ACTIF et ayant pour article sortant le        */
  /* premier de l'activit� courante                         */
  /* ------------------------------------------------------ */
  /* SLe 06/07/06 on teste RETURN-VALUE */
  IF RETURN-VALUE <> "Adm-Error":U AND 
     AVAILABLE kact AND /* SLe 18/09/08 dans le cas o� la nouvelle activit� n'appartient pas � la s�lection en cours */
      vgtCtxRec 
  THEN RUN P-Maj-Article-Reconstitue.
  /* Fin MB 25/10/05 */
    
  If Return-Value = "Adm-Error":U And Modefct = "Modifier":U And Reset-Modif Then Do :
     Run dispatch ('cancel-record':U).
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Maj-Buffer-Temp V-table-Win 
PROCEDURE Maj-Buffer-Temp :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  Do Transaction :
    {&_proparse_prolint-nowarn(findexcl)}
    Find kact Where Rowid(kact) = Rowid-Temp Exclusive-Lock No-Wait No-Error.

    If Available kact Then
       Run Assign-Buffer-Temp.
  End.
  If Available kact Then 
     Find current kact no-lock no-error.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-info-atelier V-table-Win 
PROCEDURE maj-info-atelier :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters: Maj des information kacto.nligoprev Et kacta.nlig
              Permet de controler si l'op�ration a �t� supprimer ....
  Notes:       
------------------------------------------------------------------------------*/
  Define Buffer b-kacta     For kacta.
  Define Buffer b2-kacta    For kacta.

  Define Buffer b-kacto     For kacto.
  Define Buffer b2-kacto    For kacto.
  Define Buffer b-ope-kacto For kacto.

  For each b-kacta Fields (csoc cetab cact datef ni1opinf nligopinf)
                   where b-kacta.csoc      = kact.csoc
                     And b-kacta.cetab     = kact.cetab
                     And b-kacta.cact      = kact.cact
                     And b-kacta.nligopinf <> 0 
                     And b-kacta.ni1opinf  <> 0
                   No-lock :
     
     Find first b-ope-kacto where b-ope-kacto.csoc  = b-kacta.csoc
                              And b-ope-kacto.cetab = b-kacta.cetab
                              And b-ope-kacto.cact  = b-kacta.cact
                              And b-ope-kacto.datef = b-kacta.datef
                              And b-ope-kacto.ni1   = b-kacta.ni1opinf
                           No-lock No-Error.
     If Not Available b-ope-kacto Or
        b-ope-kacto.nlig <> b-kacta.nligopinf
     Then do Transaction :
        Find First b2-kacta where Rowid(b2-kacta) = Rowid(b-kacta)
                            Exclusive-lock No-wait No-error.
        If Available b2-kacta then do:
           If available b-ope-kacto Then
              Assign b2-kacta.nligopinf = b-ope-kacto.nlig
                     b2-kacta.ni1opinf  = b-ope-kacto.ni1.
           Else 
              Assign b2-kacta.nligopinf = 0
                     b2-kacta.ni1opinf  = 0.
        End.
    End.
  End.

  For each b-kacto Fields (csoc cetab cact datef ni1oprevel nligoprevel)
                   where b-kacto.csoc        = kact.csoc
                     And b-kacto.cetab       = kact.cetab
                     And b-kacto.cact        = kact.cact
                     And b-kacto.ni1oprevel  = 0 
                     And b-kacto.nligoprevel = 0
                   No-lock :
     
     Find first b-ope-kacto where b-ope-kacto.csoc  = b-kacto.csoc
                              And b-ope-kacto.cetab = b-kacto.cetab
                              And b-ope-kacto.cact  = b-kacto.cact
                              And b-ope-kacto.datef = b-kacto.datef
                              And b-ope-kacto.ni1   = b-kacto.ni1oprevel
                           No-lock No-Error.
     If Not Available b-ope-kacto Or
        b-ope-kacto.nlig <> b-kacto.nligoprevel
     Then do Transaction :
        Find First b2-kacto where Rowid(b2-kacto) = Rowid(b-kacto)
                            Exclusive-lock No-wait No-error.
        If Available b2-kacto then do:
           If available b-ope-kacto Then
              Assign b2-kacto.nligoprevel = b-ope-kacto.nlig
                     b2-kacto.ni1oprevel  = b-ope-kacto.ni1.
           Else 
              Assign b2-kacto.nligoprevel = 0
                     b2-kacto.ni1oprevel  = 0.
        End.
    End.
  End.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE maj-itineraires V-table-Win 
PROCEDURE maj-itineraires :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  cle le 03/09/2003 
  Notes: mise � jour des itin�raires si l'article de r�f�rence et, ou de leur activit� change       
------------------------------------------------------------------------------*/
        
   Define buffer b-kitidd for kitidd.
   Define buffer b-kiti for kiti.
       
   Assign w-itichanged = "":U
          i            = 1.

   If Not adm-new-record
    And ( ( kact.cunite:Modified In Frame {&Frame-Name} And kact.cunite <> w-cunite )
       Or ( F-cartvu:modified In Frame {&Frame-Name}    And kact.cart <> w-cart ))
      Then Do:
      For Each b-kitidd Where b-kitidd.csoc    = r-kact.csoc
                          And b-kitidd.cetab   = r-kact.cetab
                          And b-kitidd.cact    = r-kact.cact
                        Exclusive-Lock 
                        Use-Index i2-itidd 
                        Break By b-kitidd.csoc
                              By b-kitidd.cetab
                              By b-kitidd.cact
                              By b-kitidd.citi :
        
         Assign b-kitidd.cart   = kact.cart
                b-kitidd.cunite = kact.cunite.
                
         /* r�cup�ration de l'itin�raire */
         If First-OF(b-kitidd.citi) Then Do : 
            For Each b-kiti Where b-kiti.csoc  = b-kitidd.csoc
                              And b-kiti.cetab = b-kitidd.cetab
                              And b-kiti.citi  = b-kitidd.citi
                            Use-Index i1-iti
                            Exclusive-Lock :
                If b-Kiti.typflux = {&TYPFLUX-POUSSE} Then do :
                   Find First Kitidd Where Kitidd.csoc  = b-Kiti.csoc
                                       And Kitidd.cetab = b-Kiti.cetab
                                       And Kitidd.citi  = b-Kiti.citi
                                     Use-Index i1-itidd
                                     No-Lock No-Error.
                   If Available Kitidd And Kitidd.cact = b-Kitidd.cact Then do :
                      b-kiti.cart = kact.cart.
                      
                      If Length ( w-itichanged ) -  ( i - 1) + length ( String (b-kitidd.citi) + ", " ) > i * {&lg-iti} Then Do :
                         Assign w-itichanged = w-itichanged + '|'
                         i          = i + 1.
                      End.
                      w-itichanged = w-itichanged + String(b-kitidd.citi) + ", ":U. 
                   end.
                End.
                Else do : /*flux tir&*/
                   Find Last Kitidd Where Kitidd.csoc  = b-Kiti.csoc
                                       And Kitidd.cetab = b-Kiti.cetab
                                       And Kitidd.citi  = b-Kiti.citi
                                     Use-Index i1-itidd
                                     No-Lock No-Error.
                   If Available Kitidd And Kitidd.cact = b-Kitidd.cact Then do :
                      b-kiti.cart = kact.cart.
                      
                      If Length ( w-itichanged ) -  ( i - 1) + length ( String (b-kitidd.citi) + ", " ) > i * {&lg-iti} Then Do :
                         Assign w-itichanged = w-itichanged + '|'
                         i          = i + 1.
                      End.
                      w-itichanged = w-itichanged + String(b-kitidd.citi) + ", ":U. 
                   end.
                End.
            End.         
         End.       
      End.
   End.
         
   If w-itichanged <> "":U Then
      w-itichanged = substring (w-itichanged, 1, integer (length (w-itichanged)) - length (", "), "character").
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE P-Delete-OPACT V-table-Win 
PROCEDURE P-Delete-OPACT :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: {&PROCESS-OP-ACTIVITE} est le process op�ratoire temporaire cr�� � partir
         des op�rations de l'activit�s (kacto et kactod)
------------------------------------------------------------------------------*/   
   DEFINE OUTPUT PARAMETER oRet AS LOGICAL   NO-UNDO.
   DEFINE OUTPUT PARAMETER oMsg AS CHARACTER NO-UNDO.
   
   DEFINE BUFFER   bKactdpop   FOR Kactdpop.
   DEFINE BUFFER   bKactapopd   FOR Kactapopd.
   
   /*mise � jour*/
   DEFINE BUFFER   bufKactdpop FOR Kactdpop.
   DEFINE BUFFER   bufKactapopd FOR Kactapopd.
   
   ASSIGN oRet = YES
          oMsg = "":U.
            
   /* Liens Process op�ratoire temporaire - Activit� temporaire*/
   FOR EACH bKactdpop WHERE bKactdpop.csoc  = Kact.csoc                 
                        AND bKactdpop.cetab = Kact.cetab                        
                        AND bKactdpop.cpope = {&PROCESS-OP-ACTIVITE}                                   
                      NO-LOCK:                 
      FIND FIRST bufKactdpop WHERE ROWID(bufKactdpop) = ROWID(bKactdpop)
                             EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      IF LOCKED(bufKactdpop) THEN DO:
         ASSIGN oRet = NO
                oMsg = SUBSTITUTE(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                      , {&PROCESS-OP-ACTIVITE}, 'Kactdpop':U ).
                
         RETURN.
      END.
      ELSE
         DELETE bufKactdpop.                  
   END.
   
   /* Lien Article de l'Activit� - Op�rations du process op�ratoire temporaire */   
   FOR EACH bKactapopd WHERE bKactapopd.csoc  = Kact.csoc                 
                        AND bKactapopd.cetab = Kact.cetab                        
                        AND bKactapopd.cpope = {&PROCESS-OP-ACTIVITE}                                   
                      NO-LOCK:                 
      FIND FIRST bufKactapopd WHERE ROWID(bufKactapopd) = ROWID(bKactapopd)
                             EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      IF LOCKED(bufKactapopd) THEN DO:
         ASSIGN oRet = NO
                oMsg = SUBSTITUTE(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                      , {&PROCESS-OP-ACTIVITE}, 'Kactapopd':U ).
                
         RETURN.
      END.
      ELSE
         DELETE bufKactapopd.                  
   END.                                                                                                           

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE P-Maj-Article-Reconstitue V-table-Win 
PROCEDURE P-Maj-Article-Reconstitue :
/*------------------------------------------------------------------------------
  Purpose:     Met � jour zartfab.tReconst pour le premier article sortant 
               de l'activit� selon qu'il existe une activit� � l'�tat ACTIF 
               de contexte reconstitution et ayant parmi ses sortants cet article
  Parameters:  <none>
  Notes:       MB G7_1659 25/10/05 Cr�ation
               MB M028441 18/06/07 Modification
------------------------------------------------------------------------------*/
   DEFINE VARIABLE vRet AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vMsg AS CHARACTER NO-UNDO.
/* -------------------------------------------------------------------------- */
    
   /* Met le sablier */
   {&SetCurs-Wait}
   
   {&RUN-ACTBIB-MAJ-ART-ACT-RECONST} (INPUT wcSoc,
                                      INPUT wcEtab,
                                      INPUT kact.cAct, /* MB M028441 18/06/07 kact.cArt */
                                      OUTPUT vRet,
                                      OUTPUT vMsg).
    
    /* Enl�ve le sablier */
   {&SetCurs-NoWait}

   IF NOT vRet THEN DO:
      {affichemsg.i &Message = "vMsg"}
   END.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE P-TestDatefEnVigueur V-table-Win 
PROCEDURE P-TestDatefEnVigueur :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables
------------------------------------------------------------------------------*/
   DEFINE INPUT        PARAMETER iDatef        AS DATE      NO-UNDO.
   DEFINE INPUT        PARAMETER icAct         AS CHARACTER NO-UNDO.
   DEFINE       OUTPUT PARAMETER oDatefEnCours AS DATE      NO-UNDO.
   DEFINE       OUTPUT PARAMETER oRet          AS LOGICAL   NO-UNDO.
   DEFINE       OUTPUT PARAMETER oMsg          AS CHARACTER NO-UNDO.
   
   DEFINE BUFFER bKactd FOR kactd.
/* -------------------------------------------------------------------------- */ 
    
   /* Recherche de la date d'effet en vigueur */
   FOR LAST bKactd FIELDS (datef)
                    WHERE bKactd.csoc  = wcSoc
                      AND bKactd.cetab = wcEtab
                      AND bKactd.cact  = icact
                      AND bKactd.datef <= TODAY  
                    NO-LOCK:
       ASSIGN oDatefEnCours = bKactd.datef.
   END.
   
   IF iDatef <> ? AND  iDatef < oDatefEncours THEN DO:
      ASSIGN oRet = FALSE
             oMsg = SUBSTITUTE(Get-TradTxt (022493, 'Traitement impossible (&1).':T), 
                    Get-TradMsg (035303, 'Modification d''une date d''effet pass�e':T)).
      RETURN "":U. 
   END.
   
   ASSIGN oRet = TRUE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-CalculQte V-table-Win 
PROCEDURE Pr-CalculQte PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     cumul de la qt� de l'article de r�f�rence de l'activit� (qt� "normale" + �ventuel taux de freinte ou �ventuelle qt� de freinte)
  Parameters:  <none>
  Notes:       Appel� dans Assign-Record (d�coupage SLe 20/05/09)
------------------------------------------------------------------------------*/
   DEFINE INPUT  PARAMETER iCsoc    AS CHARACTER NO-UNDO.
   DEFINE INPUT  PARAMETER iCetab   AS CHARACTER NO-UNDO.
   DEFINE INPUT  PARAMETER iCact    AS CHARACTER NO-UNDO.
   DEFINE INPUT  PARAMETER iDatef   AS DATE      NO-UNDO.
   DEFINE INPUT  PARAMETER iTypacta AS CHARACTER NO-UNDO.
   DEFINE INPUT  PARAMETER iCart    AS CHARACTER NO-UNDO.
   DEFINE OUTPUT PARAMETER oQte     AS DECIMAL   NO-UNDO.
   
   DEFINE BUFFER bKacta FOR kacta.
   
   FOR EACH bKacta FIELDS (qte txfr tligfr)
                   Where bKacta.csoc    = iCsoc
                     And bKacta.cetab   = iCetab
                     And bKacta.cact    = iCact
                     And bKacta.datef   = iDatef
                     And bKacta.typacta = iTypacta
                     And bKacta.cart    = iCart
                   No-Lock :
      /* gv 080708 modif ds le cas de la gestion des temps hors activit� : prise en compte de la freinte pour le sortant de ref */
      IF kact.typflux EQ {&TYPFLUX-TIRE} 
         THEN Assign oQte = oQte + (bKacta.qte * ( 1 / (1 - (bKacta.TxFr / 100)))).
         ELSE IF NOT bKacta.tligfr THEN ASSIGN oQte = oQte + bKacta.qte.
   END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-TestQtediff V-table-Win 
PROCEDURE Pr-TestQtediff PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     Test 1 seul qtediff par activit� (test fait lors de la cl�ture de l'OF)
  Parameters:  <none>
  Notes:       d�coupage SLe 14/01/2013 (assign-record)
------------------------------------------------------------------------------*/
   DEFINE INPUT  PARAMETER iCsoc  AS CHARACTER NO-UNDO.
   DEFINE INPUT  PARAMETER iCetab AS CHARACTER NO-UNDO.
   DEFINE INPUT  PARAMETER iCact  AS CHARACTER NO-UNDO.
   DEFINE OUTPUT PARAMETER oRet   AS LOGICAL   NO-UNDO.
   DEFINE OUTPUT PARAMETER oMsg   AS CHARACTER NO-UNDO.
   
   DEFINE BUFFER bKactd FOR kactd.
   DEFINE VARIABLE vNb  AS INTEGER   NO-UNDO.
   
   For Each bKactd Where bKactd.csoc  = {&Table}.csoc
                     And bKactd.cetab = {&Table}.cetab
                     And bKactd.cact  = {&Table}.cact
                   No-Lock:
      Select count(*) Into vNb From kacta
                               Where kacta.csoc    = bKactd.csoc
                                 And kacta.cetab   = bKactd.cetab
                                 And kacta.cact    = bKactd.cact
                                 And kacta.datef   = bKactd.datef
                                 And kacta.tqtedif = YES. /*CJ 20101220_0001 Q004134 M087778 ajout de l'�galit� (erreur Oracle sinon)*/
      If vNb > 1 Then Do:
         ASSIGN oRet = FALSE
                oMsg = Substitute(Get-TradMsg (003406, 'L''activit� &1 compte plusieurs articles qui demandent � �tre calcul�s automatiquement par diff�rence.':T), Substitute(Get-TradTxt (008555, 'du &1':T), bKactd.datef)).
         Return "":U.
      End.
   End.
   ASSIGN oRet = TRUE.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Raz-tt-traces V-table-Win 
PROCEDURE Raz-tt-traces :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/


   EMPTY TEMP-TABLE ttKact-APRES.
   EMPTY TEMP-TABLE ttKactd-APRES.
   EMPTY TEMP-TABLE ttKactsst-APRES.
   EMPTY TEMP-TABLE ttKactdpop-APRES.
   EMPTY TEMP-TABLE ttKacta-APRES.
   EMPTY TEMP-TABLE ttKactapopd-APRES.
   EMPTY TEMP-TABLE ttKactartr-APRES.
   EMPTY TEMP-TABLE ttkacto-APRES.    
   EMPTY TEMP-TABLE ttkactod-APRES.          
   EMPTY TEMP-TABLE ttKactctxu-APRES.          
   EMPTY TEMP-TABLE ttKcriv-APRES.          
   EMPTY TEMP-TABLE ttKcriv-Dat-APRES. 
   EMPTY TEMP-TABLE ttKcriv-Dat-Rdtdyn-APRES.
   EMPTY TEMP-TABLE ttKcom-Dat-APRES.
   EMPTY TEMP-TABLE ttKcom-Art-APRES.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info V-table-Win 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.
   
   Case Name-Info :
   
       When "F-cact":U Then
          Valeur-Info = F-cact:Screen-Value In Frame {&Frame-Name}.
   
       /* Ins�rer ici vos propres infos � envoyer */
       When "Rowid-Temp":U Then
          Valeur-Info = String(Rowid-Temp).
          
       When "ModeFct":U Then
          Valeur-Info = ModeFct.
          
       /* Pour les crit�res */
       When "Cle-Crit":U Then
          Assign Valeur-Info = wcsoc + "|":U +
                               wcetab + "|":U +
                               {&KCRIV-TYPCLE-ACT} + "|":U +
                               (If Available {&Table} Then {&Table}.cact Else "").

       When "Cle-Crifa":U Then Do:
          Assign Valeur-Info = F-cartvu:Screen-Value In Frame {&Frame-Name}     + '�':U +
                               'ACT':U                                          + '�':U +
                               String(Today, '99/99/9999':U)                    + '�':U +
                               {&Table}.cfmact:Screen-Value In Frame {&Frame-Name} + ',':U + 
                                 F-cact:Screen-Value In Frame {&Frame-Name}.
       End.
       
       When "Init-Crifa":U Then
          Assign Valeur-Info = "Oui":U
                               /* (If Adm-New-Record 
                                   Then (IF Adm-Adding-Record OR ({&Table}.cfmact:MODIFIED In Frame {&Frame-Name}) THEN "Oui":U ELSE "Non":U)
                                   Else "Non":U) SLE 23/12/04 */
                               /*(If (Adm-New-Record And Adm-Adding-Record) 
                                   Then "Oui":U
                                   Else "Non":U) SLE 26/11/04 */
                               + "|":U +
                               (If Available {&Table} Then {&Table}.cact Else "").
       
       When "TypFlux":u Then       
          Run Get-Valeur-Combo (Cb-TypFlux:handle In Frame {&Frame-Name},
                                Output Valeur-Info).              

       When "Cle":U Then /* CBR 22/02/2002 Envoi de la cl� pour viewer Contextes */
          Assign Valeur-Info = (If Available {&Table} Then {&Table}.cact Else "":u).

       WHEN "Cartref":U THEN ASSIGN Valeur-info = F-cartvu:SCREEN-VALUE IN FRAME {&FRAME-NAME}.
       WHEN "famact":U THEN ASSIGN Valeur-info = {&Table}.cfmact:Screen-Value In Frame {&Frame-Name}.
       WHEN "famart":U THEN DO:
          FOR FIRST zart WHERE zart.csoc = wcsoc
                           AND zart.cart = F-cartvu:SCREEN-VALUE IN FRAME {&FRAME-NAME}
                           NO-LOCK:
             ASSIGN valeur-info = F-cartvu:SCREEN-VALUE IN FRAME {&FRAME-NAME}.
          END.
       END.
      
       Otherwise 
          /* Par d�faut, envoi de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Send-Info In Adm-Broker-Hdl (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kact"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  DEFINE VARIABLE vRet AS  LOGICAL   NO-UNDO.
  DEFINE VARIABLE vMsg AS  CHARACTER NO-UNDO.
  
  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
     
      {admvif/template/vinclude.i "vstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE SupCrit V-table-Win 
PROCEDURE SupCrit :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  l'ancienne cl�
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter I-Cle        AS CHARACTER No-Undo.
   Define Output Parameter O-Ret        As Logical   No-Undo.
   Define Output Parameter O-Mes-Err    As Character No-Undo.
   
   Define Buffer B-kactcriv    For kactcriv.
   Define Buffer Bsup-kactcriv For kactcriv.
  
   Assign O-Ret = Yes.
   Tr_Supcrit:
   Do Transaction:
      For Each B-kactcriv Fields (ccri)
                       Where B-kactcriv.csoc   = wcsoc
                         And B-kactcriv.cetab  = wcetab
                         And B-kactcriv.typcle = {&KCRIV-TYPCLE-ACT}
                         And B-kactcriv.cle    = I-Cle
                       No-Lock:
          Find First bsup-kactcriv Where Rowid(bsup-kactcriv) = Rowid(b-kactcriv)
          Exclusive-Lock No-Wait No-Error.
          If Locked( bsup-kactcriv )
          Then Do:
             Assign O-Mes-Err = Substitute( Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                          , (I-cle + ',':U + B-kactcriv.ccri), 'kactcriv':U )
                    O-Ret     = No.
             Undo, Leave Tr_Supcrit.
          End.
          Else 
            Delete bsup-kactcriv.
      End.
      
      /* CD DP2374 suppression crit�re rendement */ 
      IF NUM-ENTRIES(I-cle,",":U) = 2 THEN DO:
         For Each B-kactcriv Fields (ccri)
                             Where B-kactcriv.csoc   = wcsoc
                               And B-kactcriv.cetab  = wcetab
                               And B-kactcriv.typcle = {&KACTCRIV-TYPCLE-ACT-RD}
                               And B-kactcriv.cle    = I-Cle
                             No-Lock:
             Find First bsup-kactcriv Where Rowid(bsup-kactcriv) = Rowid(b-kactcriv)
             Exclusive-Lock No-Wait No-Error.
             If Locked( bsup-kactcriv )
             Then Do:
                Assign O-Mes-Err = Substitute( Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                             , (I-cle + ',':U + B-kactcriv.ccri), 'kactcriv':U )
                       O-Ret     = No.
                Undo, Leave Tr_Supcrit.
             End.
             Else 
               Delete bsup-kactcriv.
         End.
      END.
      
   End. /* Transaction */
   Assign O-Ret = Yes.
                       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE test-itineraires V-table-Win 
PROCEDURE test-itineraires :
/*------------------------------------------------------------------------------
  Purpose:     tests si changement de l'article de ref ne pose pas de probl�me 
               avec les itin�raires
  Parameters:  <none>
  Notes:   CLE le 03/09/2003 modif de l'article de r�f�rence 
     -> impact sur les itin�raires en changeant l'article de r�f�rence sur une 
     activit� la modif doit �tre r�percut�e sur les itin�raires concern�s sauf 
     si cela impacte une activit� particuli�re la premi�re en flux pouss�, 
     la derni�re en flux tir�     
------------------------------------------------------------------------------*/ 
   Define Output Parameter O-Ret        As Logical      No-Undo.
   Define Output Parameter O-Mes-Err    As Character    No-Undo.
   
   Assign O-Ret = Yes.
   
   Define Buffer b-Kitidd  For Kitidd.
   Define Buffer b-Kitidd2 For Kitidd.
   Define Buffer b-Kiti    For Kiti.
   Define Variable ititemp As Character No-Undo.
        
   /* r�cup�rer le citi dans kitidd */
   For Each b-Kitidd Fields (csoc cetab citi cact)
                     Where b-Kitidd.csoc  = wcsoc
                       And b-Kitidd.cetab = wcetab
                       And b-Kitidd.cact  = F-cact:Screen-Value In Frame {&Frame-Name}
                     No-Lock 
                     Break By b-Kitidd.csoc
                           By b-Kitidd.cetab
                           By b-Kitidd.cact
                           By b-Kitidd.citi :
                           
      /* r�cup�ration de l'itin�raire */
      If First-OF(b-Kitidd.citi) Then ititemp = b-Kitidd.citi. 
      
      For First b-Kiti Fields (typflux)
                       Where b-Kiti.csoc  = wcsoc
                         And b-Kiti.cetab = wcetab
                         And b-Kiti.citi  = ititemp
                       No-Lock :

         Case b-Kiti.typflux:
           When {&TYPFLUX-POUSSE} Then
              For First b-Kitidd2 FIELDS ({&EmptyFields})
                                  Where b-Kitidd2.csoc  = wcsoc
                                    And b-Kitidd2.cetab = wcetab
                                    And b-Kitidd2.citi  = ititemp
                                    And b-Kitidd2.cact  = b-Kitidd.cact 
                                    And b-Kitidd2.cart <> F-cartvu:Screen-Value In Frame {&Frame-Name}
                                  Use-Index i1-itidd
                                  No-Lock :
                 {affichemsg.i &Message = "Substitute(Get-TradMsg (001204, 'L''article de r�f�rence de l''activit� &1 ne correspond pas � l''article de r�f�rence de l''itin�raire &2 . Voulez vous aussi modifier l''itin�raire &3 .':T), b-Kitidd.cact, ititemp, ititemp)"
                               &TypeBtn = {&MsgOuiNon}
                               &TypeMsg = {&MsgErreur}
                 }                
                 If ChoixMsg = 2 Then DO :
                    O-Ret = No.
                    Return "":U.
                 End.
              End.
           When {&TYPFLUX-TIRE} Then 
              For Last b-Kitidd2 FIELDS ({&EmptyFields})
                                 Where b-Kitidd2.csoc  = wcsoc
                                   And b-Kitidd2.cetab = wcetab
                                   And b-Kitidd2.citi  = ititemp
                                   And b-Kitidd2.cact  = b-Kitidd.cact 
                                   And b-Kitidd2.cart <> F-cartvu:Screen-Value In Frame {&Frame-Name}
                                 Use-Index i1-itidd
                                 No-Lock :
                 {affichemsg.i &Message = "Substitute(Get-TradMsg (001204, 'L''article de r�f�rence de l''activit� &1 ne correspond pas � l''article de r�f�rence de l''itin�raire &2 . Voulez vous aussi modifier l''itin�raire &3 .':T), b-Kitidd.cact, ititemp, ititemp)"
                               &TypeBtn = {&MsgOuiNon}
                               &TypeMsg = {&MsgErreur}
                 }                 
                 If ChoixMsg = 2 Then DO :
                    O-Ret = No.
                    Return "":U.
                 end.
              End.               
         End Case.
      End.
   End. 
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE zzP-Create-OPACT V-table-Win 
PROCEDURE zzP-Create-OPACT :
/*------------------------------------------------------------------------------
  Purpose: cr� un porcess op�ratoire temporaire comntenant pour les op�rations de l'activit�
  Parameters: Entr�e : cl� Kactd
  Notes: {&PROCESS-OP-ACTIVITE} est le process op�ratoire temporaire cr�� � partir
         des op�rations de l'activit�s (kacto et kactod)
------------------------------------------------------------------------------*/
   DEFINE INPUT  PARAMETER iCact  AS CHARACTER NO-UNDO.
   
   DEFINE VARIABLE vLpope  AS  CHARACTER NO-UNDO.
   
   DEFINE BUFFER   bKacto      FOR Kacto.
   DEFINE BUFFER   bKacta      FOR Kacta.
   DEFINE BUFFER   bKactdpop   FOR Kactdpop.
   DEFINE BUFFER   bKactapopd   FOR Kactapopd.

   /*mise � jour*/
   DEFINE BUFFER   bufKactdpop FOR Kactdpop.
   DEFINE BUFFER   bufKactapopd FOR Kactapopd.
      
   FOR EACH bKacto FIELDS(csoc cetab datef ni1 nlig)
                    WHERE bKacto.csoc  = Kact.csoc 
                      AND bKacto.cetab = Kact.cetab
                      AND bKacto.cact  = iCact     
                   NO-LOCK:
      IF NOT CAN-FIND(FIRST bKactdpop WHERE bKactdpop.csoc  = bKacto.csoc                 
                                        AND bKactdpop.cetab = bKacto.cetab                
                                        AND bKactdpop.cact  = iCact                 
                                        AND bKactdpop.datef = bKacto.datef                
                                        AND bKactdpop.cpope = {&PROCESS-OP-ACTIVITE}
                                      NO-LOCK) THEN DO:            
         /* Lien Process op�ratoire temporaire - Activit� temporaire */
         CREATE bufKactdpop.                           
         ASSIGN bufKactdpop.csoc  = bKacto.csoc 
                bufKactdpop.cetab = bKacto.cetab
                bufKactdpop.cact  = iCact       
                bufKactdpop.datef = bKacto.datef
                bufKactdpop.cpope = {&PROCESS-OP-ACTIVITE}.
      END.      
      
      FOR EACH bKacta FIELDS(csoc cetab datef typacta ni1)
                       WHERE bKacta.csoc  = bKacto.csoc 
                         AND bKacta.cetab = bKacto.cetab
                         AND bKacta.cact  = iCact     
                      NO-LOCK:      
         IF NOT CAN-FIND(FIRST bKactapopd WHERE bKactapopd.csoc    = bKacta.csoc                 
                                           AND bKactapopd.cetab   = bKacta.cetab                
                                           AND bKactapopd.cact    = iCact                 
                                           AND bKactapopd.datef   = bKacta.datef                
                                           AND bKactapopd.typacta = bKacta.typacta
                                           AND bKactapopd.ni1     = bKacta.ni1
                                           AND bKactapopd.cpope   = {&PROCESS-OP-ACTIVITE}
                                         NO-LOCK) THEN DO:            
            /* Lien Article de l'Activit� - Op�rations du process op�ratoire temporaire */
            CREATE bufKactapopd.                           
            ASSIGN bufKactapopd.csoc      = bKacta.csoc 
                   bufKactapopd.cetab     = bKacta.cetab
                   bufKactapopd.cact      = iCact       
                   bufKactapopd.datef     = bKacta.datef
                   bufKactapopd.typacta   = bKacta.typacta
                   bufKactapopd.ni1       = bKacta.ni1                   
                   bufKactapopd.cpope     = {&PROCESS-OP-ACTIVITE}
                   bufKactapopd.ni1opinf  = bKacto.ni1
                   bufKactapopd.nligopinf = bKacto.nlig.
         END.      
      END.
      
   END.
                                                                                                    
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION FormatVersart V-table-Win 
FUNCTION FormatVersart RETURNS INTEGER
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
   DEFINE BUFFER bZcri FOR zcri.
   
   FOR FIRST bZcri WHERE bZcri.csoc = wcsoc
                     AND bZcri.ccri = "$VERSART":U
                   NO-LOCK:
      RETURN INTEGER(bZcri.lformat).   /* Function return value. */
   END.
   RETURN 0. 

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GetVERSART V-table-Win 
FUNCTION GetVERSART RETURNS INTEGER PRIVATE
  ( INPUT iCsoc   AS CHARACTER,
    INPUT iCetab  AS CHARACTER,
    INPUT iCact   AS CHARACTER,
    INPUT iDatef  AS DATE) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  SLe 14/01/2013 D001436
------------------------------------------------------------------------------*/
   DEFINE BUFFER bKactcriv  FOR kactcriv.
   
   FOR FIRST bKactcriv WHERE bKactcriv.csoc   = iCsoc
                         AND bKactcriv.cetab  = iCetab
                         AND bKactcriv.typcle = {&KCRIV-TYPCLE-ACT}
                         AND bKactcriv.cle    = iCact + ',':u + STRING(iDatef,'99/99/9999':u)
                         AND bKactcriv.ccri   = "$VERSART":U
                       NO-LOCK:
      RETURN INTEGER (bKactcriv.valnmin).
   END.
   RETURN 0.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


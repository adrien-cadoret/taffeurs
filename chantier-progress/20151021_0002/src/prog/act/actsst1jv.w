&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actsst1jv.w
 *
 * Description  : SmartViewer des sous-traitants d'une activit�
 * 
 * Template     : viewer.w - SmartViewer basique 
 * Type Objet   : SmartViewer 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 05/02/2016 � 08:43 par AC
 *
 * HISTORIQUE DES MODIFICATIONS :
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME ACT

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Tablejv _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/viewerd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Tablejv 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/
/* Variables globales */
{variable.i " "}
/* constantes de pre-compilation */
&GLOBAL-DEFINE TABLE kactsst
&GLOBAL-DEFINE ltable Sous-traitant par activit�
/* Gestion acces concurrentiel Entete/Ligne */
&SCOPED-DEFINE EL-TableEntete    
&SCOPED-DEFINE EL-LTableEntete   
&SCOPED-DEFINE EL-NiveauDetail   
/* Pas Display-Fields sur la validation */
&SCOPED-DEFINE NO-DISPLAY-IF-NOT-DIFFERENT-ROW Yes
/* Niveaux des champs */
&SCOPED-DEFINE CHAMPS-GENERAL
&SCOPED-DEFINE CHAMPS-SOCIETE
/* Constantes pour la retaille des viewers */
&SCOPED-DEFINE FctResizable NO

{admvif/template/vinclude.i "Definitions"}

/*----------------------------------------------------------------------*/

/* D�finition des variables globales */
/* DEFINE VARIABLE vgCsoc AS CHARACTER NO-UNDO. */

DEFINE VARIABLE state AS CHARACTER NO-UNDO.

Define Variable Rowid-Temp As CHARACTER No-Undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" Tablejv _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kactsst ksst
&Scoped-define FIRST-EXTERNAL-TABLE kactsst


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kactsst, ksst.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS kactsst.csst kactsst.priorite 
&Scoped-define ENABLED-TABLES kactsst
&Scoped-define FIRST-ENABLED-TABLE kactsst
&Scoped-Define ENABLED-OBJECTS B_AID- 
&Scoped-Define DISPLAYED-FIELDS kactsst.csst kactsst.priorite 
&Scoped-define DISPLAYED-TABLES kactsst
&Scoped-define FIRST-DISPLAYED-TABLE kactsst


/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,L-Oblig,L-Field,L-Majuscule,L-CarSpecial */
&Scoped-define L-Oblig kactsst.csst 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Auto-Definitions" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/wrdef.p */

/* ********** Preprocessor Definitions VIF ***********/
&Scoped-Define Trait-External-Tables ~{&TP1} kactsst ~{&TP2} ~
                                     ~{&TP1} ksst ~{&TP2} 

&Scoped-Define List-Champs-Auto kactsst.csst,kactsst.priorite
&If Defined(List-Champs-Auto) <> 0 &Then
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&List-Champs-Auto},{&User-List-Champs}
   &Else
      &Scoped-Define List-champs {&List-Champs-Auto}
   &Endif
&Else
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&User-List-Champs}
   &Endif
&Endif

&Scoped-Define FIELD-PAIRS~
 ~{&FP1}csst ~{&FP2}csst ~{&FP3}~
 ~{&FP1}priorite ~{&FP2}priorite ~{&FP3}
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON B_AID- 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     kactsst.csst AT ROW 2.52 COL 20 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 12 BY 1
     B_AID- AT ROW 2.52 COL 35
     ksst.lsst AT ROW 2.52 COL 37.4 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 32 BY 1
     kactsst.priorite AT ROW 4 COL 27 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 17 BY 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: vif5_7.kactsst,vif5_7.ksst
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Tablejv ASSIGN
         HEIGHT             = 5.86
         WIDTH              = 156.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Tablejv 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_viewe.i}
{src/adm/method/viewer.i}
{admvif/method/ap_viewe.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW Tablejv
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       B_AID-:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

/* SETTINGS FOR FILL-IN kactsst.csst IN FRAME F-Main
   3                                                                    */
/* SETTINGS FOR FILL-IN ksst.lsst IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME B_AID-
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID- Tablejv
ON CHOOSE OF B_AID- IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID- Tablejv
ON ENTRY OF B_AID- IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Tablejv 


/* ***************************  Main Block  *************************** */
{admvif/template/vinclude.i "triggers"}

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF         

{admvif/template/vinclude.i "main-block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available Tablejv  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kactsst"}
  {src/adm/template/row-list.i "ksst"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kactsst"}
  {src/adm/template/row-find.i "ksst"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Aide-Sur-Champ Tablejv 
PROCEDURE Aide-Sur-Champ :
/*------------------------------------------------------------------------------
  Purpose:     Aide (liste de choix) Sur les Champs
  Parameters:  Handle du widget active
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER iChamp AS HANDLE    NO-UNDO.

 DEFINE VARIABLE vChoix AS CHARACTER NO-UNDO.
                                                                  
 CASE iChamp:NAME:                                                                  
    WHEN "csst":U THEN DO:                                                         
       {&Run-prog} "admvif/objects/a-standa.w":U                                    
                         (INPUT "CodeFct":U,        /* Fonction pour les droits */  
                          INPUT "sstjb":U, "":U,  /* Nom browser et attributs */  
                          INPUT "viewer":U, "":U,   /* Nom viewer et attributs */   
                          INPUT iChamp:LABEL,       /* Titre ecran d'aide */        
                          INPUT YES,                /* Acces bouton nouveau */      
                          INPUT iChamp:SCREEN-VALUE, /* Valeur de depart */         
                          OUTPUT vChoix).                                           
       IF vChoix <> ? THEN                                                          
          iChamp:SCREEN-VALUE = vChoix.                                             
    END.                                                                            
    OTHERWISE vChoix = ?.                                                           
 END CASE.                                                                          
                                                                                    
 IF vChoix <> ? THEN                                                                
    APPLY "TAB":U TO iChamp.                                                        
 ELSE                                                                               
    APPLY "ENTRY":U TO iChamp.                                                      

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Tablejv  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LinkFeatureID-Initialize Tablejv 
PROCEDURE LinkFeatureID-Initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   /*{ csstcc.i ksst.csst NO NO }*/
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record Tablejv 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
    /* copier les screen-value dans l'enreg temporaire */
  /*Run dispatch ('Assign-Statement':U).                 */
  
  MESSAGE "Add record".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record Tablejv 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
  
  MESSAGE "Local assign record".

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement Tablejv 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  /*RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .*/
  
  

  
  DEFINE BUFFER bufKactsst FOR Kactsst. /* APO 30/12/08 */
  
  Do Transaction :
  
  MESSAGE "rowid temp " + Rowid-Temp. 
  
  FOR EACH kactsst:
   MESSAGE " rowid " + string(Rowid(kactsst)).
  END.
  
      
  /*MESSAGE "local assign statement".
    Find kactsst where string(Rowid(kactsst)) = Rowid-Temp Exclusive-Lock No-Wait No-Error.
    If Available bufKactsst Then Do:
      MESSAGE "available".
       If Adm-New-Record Then
          MESSAGE "He l'ai ". 
       /* Dispatch standard ADM method.  */                           
       
       RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ).
    End.*/
  End.

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields Tablejv 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */ 

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .
  
  /* Code placed here will execute AFTER standard behavior.    */
  
  
  { csstcc.i kactsst.csst No Yes ksst.lsst}
  
    IF(state = "allow-adding") THEN Do With Frame {&Frame-Name}:
       ASSIGN kactsst.csst:SENSITIVE = YES.
       ASSIGN kactsst.priorite:SENSITIVE = YES.
       /*ASSIGN B_AID-sst:SENSITIVE = YES.*/
       ASSIGN kactsst.csst:SCREEN-VALUE = "".
       ASSIGN kactsst.priorite:SCREEN-VALUE = string(000000000).     

  END.
  
  IF (state = "consult") THEN Do With Frame {&Frame-Name}:
  
       ASSIGN kactsst.csst:SENSITIVE = NO.
       ASSIGN kactsst.priorite:SENSITIVE = NO.
  
  END. 
  
  IF (state = "clear-fields") THEN Do With Frame {&Frame-Name}:
  
       ASSIGN kactsst.csst:SCREEN-VALUE = "".
       ASSIGN kactsst.priorite:SCREEN-VALUE = string(000000000).
  
  END.
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Test-Saisie Tablejv 
PROCEDURE Local-Test-Saisie :
/*------------------------------------------------------------------------------
  Purpose:     Tester la saisie des variables du viewer
  Parameters:  - Test Global, ou Pour un widget 
               - Handle du widget � tester.
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER iTest-Global AS LOGICAL   NO-UNDO.
DEFINE INPUT PARAMETER iWidget-Test AS HANDLE    NO-UNDO.

/* Exemple de test : Test apres la saisie de cle, en creation, si l'enreg n'existe pas d�j� */
 IF iTest-Global OR                                                                                                          
    iWidget-Test = kactsst.csst:HANDLE IN FRAME {&FRAME-NAME} THEN DO:                                                         
    IF CAN-FIND(FIRST kactsst WHERE kactsst.csoc = kactd.csoc
                            AND     kactsst.cetab = kactd.cetab
                            AND     kactsst.cact = kactd.cact                                                                           
                            AND kactsst.csst = INPUT FRAME {&FRAME-NAME} kactsst.csst NO-LOCK) THEN DO:                            
       {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (013125, 'Cette valeur existe d�j� (&1).':T), iWidget-Test:label )"} 
       APPLY "ENTRY":U TO kactsst.csst IN FRAME {&FRAME-NAME}.                                                                  
       RETURN "ADM-ERROR":U.                                                                                                 
    END.                                                                                                                  
 END.                                                                                                                 

/* Autre Exemple : Test libelle complementaire */
 IF iTest-Global OR                                                    
    iWidget-Test = kactsst.csst:HANDLE IN FRAME {&FRAME-NAME} THEN DO: 
     {csstcc.i kactsst.csst YES YES ksst.lsst}                     
 END.                                                                  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PrAncrageWidget Tablejv 
PROCEDURE PrAncrageWidget :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* DO WITH FRAME {&FRAME-NAME}:                                             */
/*    {admvif/method/objresizerun.i &I-Proc="&AttachObjects"                */
/*                                  &I-Hansle="widget:handle"               */
/*                                  &I-Attach="'horiz�vert�height�width':U" */
/*                                  &I-Height-Resizable="YES/NO"            */
/*                                  &I-Width-Resizable="YES/NO"             */
/*                                  &I-Proc-Resize="proc"                   */
/*                                  &I-GroupName="'nomgroupe':U"}           */
/* END.                                                                     */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info Tablejv 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.
   
   DEFINE VARIABLE fields-values AS CHARACTER NO-UNDO.

   Case Name-Info :
       WHEN "State" THEN DO:
        IF(valeur-info = "allow-adding") THEN DO :
            ASSIGN state = "allow-adding".
            RUN local-display-fields.
        END.
        IF(valeur-info = "get-fields") THEN Do With Frame {&Frame-Name} :
            ASSIGN state = "get-fields".
            fields-values = "" + kactsst.csst:SCREEN-VALUE + "|" + kactsst.priorite:SCREEN-VALUE.
            Run Set-Info("fields-values=":U + fields-values, "Info2-Source":U).            
             
         END.
       END.
        WHEN "Rowid-Temp" THEN DO:
            Rowid-Temp = valeur-info.
        END.
        /*IF (valeur-info = "clear-fields") THEN DO:
        
            ASSIGN state = "clear-fields". 
            RUN local-display-fields.
        
        END.*/

       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records Tablejv  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kactsst"}
  {src/adm/template/snd-list.i "ksst"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed Tablejv 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {admvif/template/vinclude.i "vstates"}
  END CASE.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


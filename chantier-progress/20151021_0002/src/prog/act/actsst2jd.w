&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialsai
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" D-Dialsai _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actsst2jd.w
 *
 * Description  : DialogSaisie pour cr�er un sous-traitant pour une activit�
 * 
 * Template     : Dialsai.w - Smartdialog de saisie 
 * Type Objet   : SmartDialogSai 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 11/02/2016 � 12:00 par AC
 *
 * HISTORIQUE DES MODIFICATIONS :
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME ACT

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" D-Dialsai _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialsai 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

/* Constantes de la dialogue-box */
&Scoped-Define Confirme-Escape No

{admvif/template/dialsai.i "Definitions" }

/* Parameters Definitions ---                                           */

Define INPUT       Parameter o-all-temp       As CHARACTER        No-Undo.
Define Output       Parameter o-ret       As Logical        No-Undo.
Define Output       Parameter o-csst      Like kactsst.csst    No-Undo.
Define Output       Parameter o-lsst     Like ksst.lsst   No-Undo.
Define Output       Parameter o-priorite     Like kactsst.priorite   No-Undo.
                               
/* Variables globales */
{variable.i " "}

&Scoped-define Temp-Table Tt-actsst

/* Definition de la table temporaire de travail (Temp-Table ou Work-Table) */
Define New Shared Temp-Table {&Temp-Table} No-Undo
  Field csst  Like kactsst.csst
  Field priorite  Like kactsst.priorite
  Field lsst Like ksst.lsst.
  
/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" D-Dialsai _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialogSai
&Scoped-define DB-AWARE no

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialsai

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES ksst

/* Definitions for DIALOG-BOX D-Dialsai                                 */
&Scoped-define QUERY-STRING-D-Dialsai FOR EACH ksst NO-LOCK
&Scoped-define OPEN-QUERY-D-Dialsai OPEN QUERY D-Dialsai FOR EACH ksst NO-LOCK.
&Scoped-define TABLES-IN-QUERY-D-Dialsai ksst
&Scoped-define FIRST-TABLE-IN-QUERY-D-Dialsai ksst


/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS csst B_AID-csst priorite Btn_OK Btn_Annuler ~
Btn_Aide 
&Scoped-Define DISPLAYED-OBJECTS csst priorite 

/* Custom List Definitions                                              */
/* List-1,List-2,L-Oblig,L-Field,L-Majuscule,L-CarSpecial               */
&Scoped-define L-Field csst priorite 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Auto-Definitions" D-Dialsai _INLINE
/* Actions: ? ? ? ? admvif/xftr/wrdef.p */

/* ********** Preprocessor Definitions VIF ***********/
&Scoped-Define List-Champs-Auto csst,priorite
&If Defined(List-Champs-Auto) <> 0 &Then
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&List-Champs-Auto},{&User-List-Champs}
   &Else
      &Scoped-Define List-champs {&List-Champs-Auto}
   &Endif
&Else
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&User-List-Champs}
   &Endif
&Endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Aide 
     LABEL "Ai&de" 
     SIZE 15 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON Btn_Annuler 
     LABEL "Annuler" 
     SIZE 15 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON B_AID-csst 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE VARIABLE csst AS CHARACTER FORMAT "X(30)":U 
     LABEL "Code sous-traitant" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

DEFINE VARIABLE priorite AS INTEGER FORMAT "->9":U INITIAL 0 
     LABEL "Priorit�" 
     VIEW-AS FILL-IN 
     SIZE 14 BY 1 NO-UNDO.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY D-Dialsai FOR 
      ksst SCROLLING.
&ANALYZE-RESUME

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialsai
     csst AT ROW 6.95 COL 37.8 COLON-ALIGNED
     B_AID-csst AT ROW 6.95 COL 54.4
     ksst.lsst AT ROW 7 COL 56.8 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 32 BY 1
     priorite AT ROW 8.57 COL 38.4 COLON-ALIGNED
     Btn_OK AT ROW 22.43 COL 110
     Btn_Annuler AT ROW 22.43 COL 126
     Btn_Aide AT ROW 22.43 COL 142
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 6
         TITLE "SmartDialog"
         DEFAULT-BUTTON Btn_OK.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialogSai
   Allow: Basic,Browse,DB-Fields,Query
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialsai 
/* ************************* Included-Libraries *********************** */

{admvif/method/dialsaim.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX D-Dialsai
   FRAME-NAME                                                           */
ASSIGN 
       FRAME D-Dialsai:SCROLLABLE       = FALSE
       FRAME D-Dialsai:HIDDEN           = TRUE.

ASSIGN 
       B_AID-csst:PRIVATE-DATA IN FRAME D-Dialsai     = 
                "Fonction=Combo-Aide".

/* SETTINGS FOR FILL-IN csst IN FRAME D-Dialsai
   4                                                                    */
/* SETTINGS FOR FILL-IN ksst.lsst IN FRAME D-Dialsai
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN priorite IN FRAME D-Dialsai
   4                                                                    */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialsai
/* Query rebuild information for DIALOG-BOX D-Dialsai
     _TblList          = "vif5_7.ksst"
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialsai */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" D-Dialsai _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialsai
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialsai D-Dialsai
ON END-ERROR OF FRAME D-Dialsai /* SmartDialog */
DO:
   {admvif/template/dialsai.i "End-Error"}.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialsai D-Dialsai
ON ENDKEY OF FRAME D-Dialsai /* SmartDialog */
DO:
   {admvif/template/dialsai.i "End-Key"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialsai D-Dialsai
ON GO OF FRAME D-Dialsai /* SmartDialog */
DO:
   {admvif/template/dialsai.i "Go"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialsai D-Dialsai
ON WINDOW-CLOSE OF FRAME D-Dialsai /* SmartDialog */
DO:  
   /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
   APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Aide
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Aide D-Dialsai
ON CHOOSE OF Btn_Aide IN FRAME D-Dialsai /* Aide */
OR HELP OF FRAME {&FRAME-NAME}
DO: /* Call Help Function (or a simple message). */
   {admvif/template/tinclude.i "Help" }
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Annuler
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Annuler D-Dialsai
ON CHOOSE OF Btn_Annuler IN FRAME D-Dialsai /* Annuler */
DO:
   {admvif/template/dialsai.i "Choose-Annuler"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-csst
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-csst D-Dialsai
ON CHOOSE OF B_AID-csst IN FRAME D-Dialsai /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-csst D-Dialsai
ON ENTRY OF B_AID-csst IN FRAME D-Dialsai /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialsai 


/* ***************************  Main Block  *************************** */
{admvif/template/dialsai.i "Triggers" }
{admvif/template/dialsai.i "Main-Block" }

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialsai 
PROCEDURE adm-create-objects :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialsai 
PROCEDURE adm-row-available :
/* -----------------------------------------------------------
  Purpose:     Ask if there are rows available.
  Parameters:  <none>      
-------------------------------------------------------------*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Aide-Sur-Champ D-Dialsai 
PROCEDURE Aide-Sur-Champ :
/*------------------------------------------------------------------------------
  Purpose:     Aide (liste de choix) Sur les Champs
  Parameters:  Handle du widget active
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER iChamp AS HANDLE    NO-UNDO.

 DEFINE VARIABLE vChoix AS CHARACTER NO-UNDO.
                                                                  
 CASE iChamp:NAME:                                                                  
    WHEN "csst":u THEN DO:                                                         
       {&Run-prog} "admvif/objects/a-standa.w":U                                    
                         (INPUT "CodeFct":U,        /* Fonction pour les droits */  
                          INPUT "sstjb":U, "":U,  /* Nom browser et attributs */  
                          INPUT "viewer":U, "":U,   /* Nom viewer et attributs */   
                          INPUT iChamp:LABEL,       /* Titre ecran d'aide */        
                          INPUT YES,                /* Acces bouton nouveau */      
                          INPUT iChamp:SCREEN-VALUE, /* Valeur de depart */         
                          OUTPUT vChoix).                                           
       IF vChoix <> ? THEN                                                          
          iChamp:SCREEN-VALUE = vChoix.                                             
    END.                                                                            
    OTHERWISE vChoix = ?.                                                           
 END CASE.                                                                          
                                                                                    
 IF vChoix <> ? THEN                                                                
    APPLY "TAB":U TO iChamp.                                                        
 ELSE                                                                               
    APPLY "ENTRY":U TO iChamp.                                                      

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialsai  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialsai.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialsai  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY csst priorite 
      WITH FRAME D-Dialsai.
  ENABLE csst B_AID-csst priorite Btn_OK Btn_Annuler Btn_Aide 
      WITH FRAME D-Dialsai.
  VIEW FRAME D-Dialsai.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialsai}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-Assign-Statement D-Dialsai 
PROCEDURE local-Assign-Statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  Assign o-csst = csst:Screen-Value In Frame {&Frame-Name}
         o-priorite = integer(priorite:Screen-Value In Frame {&Frame-Name})
         o-ret    = Yes.
         
  IF o-priorite = 0  THEN DO:
         MESSAGE "Veuillez rentrer une bonne priorite".
         Apply "Entry":U To priorite In Frame {&Frame-Name}.
         Return "Adm-Error":U.
  END.
  
  ELSE DO : 
      /* Dispatch standard ADM method.                             */
      RUN dispatch IN THIS-PROCEDURE ( INPUT 'Assign-Statement':U ) .
  END.


  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-Display-Fields D-Dialsai 
PROCEDURE local-Display-Fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'Display-Fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
 { csstcc.i csst No Yes ksst.lsst 1}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialsai 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  DEFINE VARIABLE tmp-index AS INTEGER NO-UNDO.
  
  DEFINE VARIABLE num-temp AS INTEGER NO-UNDO.
  
  num-temp = Num-Entries(o-all-temp,"|").
  tmp-index = 1. 
  
    
  DO WHILE NOT tmp-index = num-temp :
     
   CREATE {&Temp-Table}.
   {&Temp-Table}.csst = Entry(tmp-index,o-all-temp,"|"). 
   tmp-index = tmp-index + 1.
   MESSAGE "temp copi� = " + {&Temp-Table}.csst.
  END.

  
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Test-Saisie D-Dialsai 
PROCEDURE Local-Test-Saisie :
/*------------------------------------------------------------------------------
  Purpose:     Tester la saisie des champ 
  Parameters:  - Test Global, ou Pour un widget 
               - Handle du widget � tester.
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Test-Global As Logical       No-Undo.
    Define Input Parameter Widget-Test As Widget-Handle No-Undo.

    DEFINE BUFFER B-{&TEMP-TABLE} FOR {&TEMP-TABLE}.
    DEFINE BUFFER B-ksst FOR ksst.
    
    If Test-Global 
    Or Widget-Test = csst:Handle In Frame {&Frame-Name} 
    Then Do :
    
        Assign o-lsst = "".
       { csstcc.i csst YES Yes ksst.lsst 1}
       If Available buf1-ksst Then Do:
         MESSAGE "ksst avaailable".
       End.
       
       FOR EACH B-{&Temp-Table}:
         MESSAGE "csst trouve dans temp " + B-{&Temp-Table}.csst.
       END.
    
       If CAN-FIND (First B-{&Temp-Table} Where B-{&Temp-Table}.csst = csst:Screen-Value In Frame {&Frame-Name}) 
       Then Do:
         {affichemsg.i   
               &Message = "Get-TradMsg (004324, 'Vous avez d�j� mentionn� cette ressource pour ce groupe':T)"
               &TypeMsg = {&MsgWarning}
           }
         Apply "Entry":U To csst In Frame {&Frame-Name}.
         Return "Adm-Error":U.
       End.
       
       
       If NOT CAN-FIND (First B-ksst Where B-ksst.csst = csst:Screen-Value In Frame {&Frame-Name}) 
       Then Do:
         /*{affichemsg.i   
               &Message = "Get-TradMsg (004324, 'Vous avez d�j� mentionn� cette ressource pour ce groupe':T)"
               &TypeMsg = {&MsgWarning}
           }*/
           MESSAGE "cette valeur n'existe pas".
         Apply "Entry":U To csst In Frame {&Frame-Name}.
         Return "Adm-Error":U.
       End.
       
       Assign o-lsst  = buf1-ksst.lsst.
       
    End.
    
    Return.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialsai 
PROCEDURE send-records :
/* -----------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i      
-------------------------------------------------------------*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialsai 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


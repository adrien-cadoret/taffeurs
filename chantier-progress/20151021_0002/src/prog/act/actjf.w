&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actjf.w
 *
 * Description  : SmartFonction sur Activit�s
 * 
 * Template     : fonction.w - SmartFonction 
 * Type Objet   : SmartFonction 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 01/08/1996 � 00:00 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 05/01/2015 - CJ      : Q7805:Fonction connexe cadence article
 *                              20150105_0005 (26416)|Q7805|Fonction connexe cadence article|CJ|05/01/15 12:26:47 
 *                               Changement du mot cl� pour la s�lection article dans la fonction connexe des cadences articles (TRS)
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" F-Frame-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS F-Frame-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

/* Constante pour sous-fonctions */
   /* La Saisie se fait-elle dans une sous-fonction  */
&Scoped-Define Saisie-Dans-Sous-fonction yes
   /* Numero de page o� se trouve la sous-fonction de saisie de la table */
&Scoped-Define Page-Saisie-Table   1


/* Constantes propres � la fonction */
/* Fl le 18/10/96 : 
 * le nombre de pages ne contient pas la page d�tail, pour que cette  page soit visualiser
 * en m�me temps que l'initialisation. En effet, pour que le browser des date d'effet
 * fonctionne bien et se positionne correctement sur la date de r�f�rence, il
 * ne faut que l'open-query du browse soit fait avant qu'un premier view n'est �t�
 * effectu� (bug progress sur les browse sans doute) */
&Scoped-Define Nombre-Pages 2

&Scoped-Define Adv-User-Attr-Globaux "cpope":U

/* N� Contexte Aide en ligne */

/* Variables globales */
{variable.i " "}
{unitebibli.i}

{vif4bibi.i} /* CD D001150 ctrl Fonsoc */

{admvif\template\fonction.i "Definitions"}

&if DEFINED(UIB_is_Running) NE 0 &Then
 /* Attributs de depart pour test SmartFonction */
 Attributs-De-Depart = "".
&Endif

/* Local Variable Definitions ---                                       */
Define Variable ModeFct As character No-Undo Init "Consult":u.     /* Modifier, Nouveau, Copier, Consult */
Define Variable Record-Available As Logical No-Undo.
Define Variable Datef-Available  As Logical No-Undo.

Define Variable w-retour-ss-fct As Logical No-Undo.

/* SLE 17/01/05 on garde en m�moire la datef sur laquelle on est positionn�,
   * pour bien s'y replacer au terme du modify */
DEFINE VARIABLE vg-datefModify AS CHARACTER NO-UNDO.

DEFINE VARIABLE vgTInitialize AS LOGICAL   NO-UNDO. /* 06/03/2012 SLe Fonction connexe */

/* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
DEFINE VARIABLE vgtDatefHisto AS LOGICAL   NO-UNDO.

/* CD D001150 Fonctions connexes */
&Global-Define  LSTPARAMFAM        'OFCRE,OFCRE2,OFDEC,OFDEC2,OFMOD2,X-CLOTOF':U
DEFINE VARIABLE hgParametresFam    AS HANDLE    NO-UNDO. /* param�tres famille activit� ou famille article */ 
DEFINE VARIABLE vgLibParamFam      AS CHARACTER NO-UNDO.
DEFINE VARIABLE hgCadArt           AS HANDLE    NO-UNDO. /* cadence par article */
DEFINE VARIABLE vgLibCadArt        AS CHARACTER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" F-Frame-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartFonction
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER FRAME

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS F-Datref B_AIDAT- R-invalide 
&Scoped-Define DISPLAYED-OBJECTS F-Datref 

/* Custom List Definitions                                              */
/* Btn-SF,List-2,List-3,List-4,List-5,List-6                            */
&Scoped-define Btn-SF btn-sst 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_act2jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_acta2jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actacjb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actajb-ent AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actajb-sor AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actajs-cout AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actajs-ent AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actajs-sor AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actctxu01jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actdjb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actdjs AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actjb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actjv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actodjs AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actopopjb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actopopjs AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actsstjs AS HANDLE NO-UNDO.
DEFINE VARIABLE h_crivjv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-updsav AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON btn-cout 
     LABEL "Articles de &co�t" 
     SIZE 21.6 BY 1.05.

DEFINE BUTTON btn-date 
     LABEL "&Dates d'effet" 
     SIZE 17.4 BY 1.05.

DEFINE BUTTON btn-entrant 
     LABEL "&Entrants" 
     SIZE 19.2 BY 1.05.

DEFINE BUTTON btn-operation 
     LABEL "Op�&rations" 
     SIZE 19.2 BY 1.05.

DEFINE BUTTON btn-sortant 
     LABEL "&Sortants" 
     SIZE 19.2 BY 1.05.

DEFINE BUTTON btn-sst 
     LABEL "Sous-Traitance" 
     SIZE 21.2 BY .95.

DEFINE BUTTON B_AIDAT- 
     IMAGE-UP FILE "admvif/images/calendar.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/calendari.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE VARIABLE F-Datref AS DATE FORMAT "99/99/9999":U 
     LABEL "Au" 
     VIEW-AS FILL-IN 
     SIZE 15.6 BY 1 NO-UNDO.

DEFINE VARIABLE F-Desactive AS CHARACTER FORMAT "X(256)":U INITIAL "Activit� d�sactiv�e" 
      VIEW-AS TEXT 
     SIZE 22.6 BY .67 NO-UNDO.

DEFINE RECTANGLE R-invalide
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 2.4 BY .57.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     btn-sst AT ROW 11.1 COL 137.6
     F-Datref AT ROW 1.52 COL 131.4 COLON-ALIGNED
     B_AIDAT- AT ROW 1.52 COL 149.2
     btn-date AT ROW 7.48 COL 6.2
     btn-operation AT ROW 7.48 COL 71.4
     btn-cout AT ROW 7.57 COL 128.6
     F-Desactive AT ROW 4.81 COL 133.4 COLON-ALIGNED NO-LABEL
     btn-entrant AT ROW 13.43 COL 29.6
     btn-sortant AT ROW 13.43 COL 114.8
     R-invalide AT ROW 4.86 COL 132
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 159.2 BY 22.05
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartFonction
   Allow: Basic,Browse,DB-Fields,Smart,Query
   Container Links: Page-Target,Record-Source,Record-Target,Navigation-Source,Navigation-Target,TableIO-Source,TableIO-Target
   Design Page: 2
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW F-Frame-Win ASSIGN
         HEIGHT             = 23.38
         WIDTH              = 161.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB F-Frame-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_fonct.i}
{src/adm/method/containr.i}
{admvif/method/ap_fonct.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW F-Frame-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Custom                                        */
ASSIGN 
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON btn-cout IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       btn-cout:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR BUTTON btn-date IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       btn-date:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR BUTTON btn-entrant IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       btn-entrant:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR BUTTON btn-operation IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       btn-operation:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR BUTTON btn-sortant IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       btn-sortant:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR BUTTON btn-sst IN FRAME F-Main
   NO-ENABLE 1                                                          */
ASSIGN 
       btn-sst:HIDDEN IN FRAME F-Main           = TRUE.

ASSIGN 
       B_AIDAT-:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide|Tooltip=Calendar".

/* SETTINGS FOR FILL-IN F-Desactive IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       F-Desactive:PRIVATE-DATA IN FRAME F-Main     = 
                "Init-Txt=Activit� d�sactiv�e".

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = ""
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main F-Frame-Win
ON HELP OF FRAME F-Main
DO:
  /* Gestion standard de l'aide en ligne */
  {admvif/template/tinclude.i "Help"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-cout
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-cout F-Frame-Win
ON CHOOSE OF btn-cout IN FRAME F-Main /* Articles de co�t */
DO:
   w-retour-ss-fct = No.
   Run Page-SousFct(-15).
   w-retour-ss-fct = Yes.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-date
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-date F-Frame-Win
ON CHOOSE OF btn-date IN FRAME F-Main /* Dates d'effet */
DO:
   w-retour-ss-fct = No.
  Run Page-SousFct(-11).
   w-retour-ss-fct = Yes.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-entrant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-entrant F-Frame-Win
ON CHOOSE OF btn-entrant IN FRAME F-Main /* Entrants */
DO:
   IF VALID-HANDLE(hgParametresFam) THEN DELETE WIDGET hgParametresFam.
   IF VALID-HANDLE(hgCadArt) THEN DELETE WIDGET hgCadArt.
   
   w-retour-ss-fct = No.
   Run Page-SousFct(-13).
   w-retour-ss-fct = Yes.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-operation
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-operation F-Frame-Win
ON CHOOSE OF btn-operation IN FRAME F-Main /* Op�rations */
DO:   
   IF VALID-HANDLE(hgParametresFam) THEN DELETE WIDGET hgParametresFam.
   IF VALID-HANDLE(hgCadArt) THEN DELETE WIDGET hgCadArt.
   
   w-retour-ss-fct = No.
   Run Page-SousFct(-12).
   w-retour-ss-fct = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-sortant
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-sortant F-Frame-Win
ON CHOOSE OF btn-sortant IN FRAME F-Main /* Sortants */
DO:
   IF VALID-HANDLE(hgParametresFam) THEN DELETE WIDGET hgParametresFam.
   IF VALID-HANDLE(hgCadArt) THEN DELETE WIDGET hgCadArt.
   
   w-retour-ss-fct = No.
   Run Page-SousFct(-14).
   w-retour-ss-fct = Yes.   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-sst
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-sst F-Frame-Win
ON CHOOSE OF btn-sst IN FRAME F-Main /* Sous-Traitance */
DO:
   w-retour-ss-fct = No.
   Run Page-SousFct(-17).   
   w-retour-ss-fct = Yes.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AIDAT-
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AIDAT- F-Frame-Win
ON CHOOSE OF B_AIDAT- IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-aide-date"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AIDAT- F-Frame-Win
ON ENTRY OF B_AIDAT- IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-aide-date"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME F-Datref
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Datref F-Frame-Win
ON LEAVE OF F-Datref IN FRAME F-Main /* Au */
DO:
  If Input Frame {&Frame-Name} F-Datref <> F-Datref Then Do :
     Assign Frame {&Frame-Name} F-Datref.
     If Valid-handle (h_actdjb) Then
        Run dispatch In h_actdjb ('Open-query':U).
  End.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK F-Frame-Win 


{admvif\template\fonction.i "Triggers"}

{admvif\template\fonction.i "Main-Block"}

{admvif\template\fonction.i "Procedures-Internes"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects F-Frame-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/p-updsav.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'SmartPanelType = Update,
                     Edge-Pixels = 2,
                     SaveFunction = Normal,
                     NameBtnAction = ,
                     Hide-Save = no,
                     Hide-Add = no,
                     Hide-Copy = no,
                     Hide-Delete = no,
                     Hide-cancel = no,
                     Hide-Print = Yes,
                     Hide-Documents = Yes,
                     PrintFunction = Normale,
                     Layout-Options = "Master Layout,Standard Character",
                     Default-Layout = Master Layout':U ,
             OUTPUT h_p-updsav ).
       RUN set-position IN h_p-updsav ( 1.00 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 1.86 , 8.40 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actjb.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     Suppression-Message = ,
                     activite = ':U ,
             OUTPUT h_actjb ).
       RUN set-position IN h_actjb ( 1.52 , 18.80 ) NO-ERROR.
       RUN set-size IN h_actjb ( 4.14 , 108.60 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/folder.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'FOLDER-TAB-TYPE = 2,
                     FOLDER-LABELS = ':U + 'G�n�ral|Suite|D�tail' ,
             OUTPUT h_folder ).
       RUN set-position IN h_folder ( 6.10 , 1.00 ) NO-ERROR.
       RUN set-size IN h_folder ( 16.95 , 159.20 ) NO-ERROR.

       /* Links to SmartFolder h_folder. */
       RUN add-link IN adm-broker-hdl ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actjb ,
             B_AIDAT-:HANDLE IN FRAME F-Main , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_folder ,
             h_actjb , 'AFTER':U ).
    END. /* Page 0 */
    WHEN 1 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actjv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ':U ,
             OUTPUT h_actjv ).
       RUN set-position IN h_actjv ( 10.19 , 12.80 ) NO-ERROR.
       RUN set-size IN h_actjv ( 6.81 , 134.20 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'criv2jv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Layout-Options = "Master Layout,Standard Character",
                     aff-statut = {&INIT-ZCRI-AFF-STATUT-NON},
                     longueur = 671,
                     mode = ,
                     mode-statut = {&INIT-ZCRI-MODE-STATUT-CONSULT},
                     table = kactcriv,
                     tqualite = OUI,
                     valeur-attendue = {&INIT-ZCRI-VALEUR-ATTENDUE-NON},
                     Hide-Links-With-Object = No,
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Default-Layout = Master Layout,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_crivjv ).
       RUN set-position IN h_crivjv ( 17.91 , 12.60 ) NO-ERROR.
       RUN set-size IN h_crivjv ( 1.19 , 6.20 ) NO-ERROR.

       /* Links to SmartViewer h_actjv. */
       RUN add-link IN adm-broker-hdl ( h_actjb , 'Record':U , h_actjv ).
       RUN add-link IN adm-broker-hdl ( h_p-updsav , 'TableIO':U , h_actjv ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'InfoACT':U , THIS-PROCEDURE ).

       /* Links to SmartViewerSpe h_crivjv. */
       RUN add-link IN adm-broker-hdl ( h_actjb , 'Record':U , h_crivjv ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'criteres':U , h_crivjv ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Group-Assign':U , h_crivjv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actjv ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_crivjv ,
             h_actjv , 'AFTER':U ).
    END. /* Page 1 */
    WHEN 2 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'act2jv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     Suppression-Message = ':U ,
             OUTPUT h_act2jv ).
       RUN set-position IN h_act2jv ( 8.71 , 29.20 ) NO-ERROR.
       RUN set-size IN h_act2jv ( 12.29 , 106.60 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actctxu01jv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'listectx = ,
                     typcle = {&INIT-KACTCTXU-TYPCLE-ACTIVITE},
                     Hide-Links-With-Object = No,
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_actctxu01jv ).
       RUN set-position IN h_actctxu01jv ( 12.57 , 41.60 ) NO-ERROR.
       RUN set-size IN h_actctxu01jv ( 5.00 , 80.60 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1,17':U) NO-ERROR.

       /* Links to SmartViewer h_act2jv. */
       RUN add-link IN adm-broker-hdl ( h_actjb , 'Record':U , h_act2jv ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Group-Assign':U , h_act2jv ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'typactiti':U , h_act2jv ).
       RUN add-link IN adm-broker-hdl ( h_actsstjs , 'Info':U , h_act2jv ).
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'InfoMode':U , h_act2jv ).

       /* Links to SmartViewerSpe h_actctxu01jv. */
       RUN add-link IN adm-broker-hdl ( h_actjb , 'Record':U , h_actctxu01jv ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Group-Assign':U , h_actctxu01jv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_act2jv ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_actctxu01jv ,
             h_act2jv , 'AFTER':U ).
    END. /* Page 2 */
    WHEN 3 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actajb.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     typacta = {&INIT-TYPACTA-SORTANT}':U ,
             OUTPUT h_actajb-sor ).
       RUN set-position IN h_actajb-sor ( 15.10 , 82.20 ) NO-ERROR.
       RUN set-size IN h_actajb-sor ( 6.86 , 76.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actacjb.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_actacjb ).
       RUN set-position IN h_actacjb ( 8.86 , 126.20 ) NO-ERROR.
       RUN set-size IN h_actacjb ( 4.14 , 27.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'acta2jv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Initial-Lock = Default,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non':U ,
             OUTPUT h_acta2jv ).
       RUN set-position IN h_acta2jv ( 22.05 , 54.00 ) NO-ERROR.
       RUN set-size IN h_acta2jv ( 1.00 , 55.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actopopjb.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non':U ,
             OUTPUT h_actopopjb ).
       RUN set-position IN h_actopopjb ( 8.81 , 44.80 ) NO-ERROR.
       RUN set-size IN h_actopopjb ( 4.14 , 74.80 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actdjb.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_actdjb ).
       RUN set-position IN h_actdjb ( 8.81 , 5.40 ) NO-ERROR.
       RUN set-size IN h_actdjb ( 4.14 , 21.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actajb.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     typacta = {&INIT-TYPACTA-ENTRANT}':U ,
             OUTPUT h_actajb-ent ).
       RUN set-position IN h_actajb-ent ( 15.14 , 3.20 ) NO-ERROR.
       RUN set-size IN h_actajb-ent ( 6.86 , 76.00 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartBrowser h_actajb-sor. */
       RUN add-link IN adm-broker-hdl ( h_actajb-ent , 'tab-order':U , h_actajb-sor ).
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actajb-sor ).

       /* Links to SmartBrowser h_actacjb. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actacjb ).

       /* Links to SmartBrowser h_actopopjb. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actopopjb ).
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'tab-order':U , h_actopopjb ).

       /* Links to SmartBrowser h_actdjb. */
       RUN add-link IN adm-broker-hdl ( h_actjb , 'Record':U , h_actdjb ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'InfoM':U , h_actdjb ).
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Info':U , THIS-PROCEDURE ).

       /* Links to SmartBrowser h_actajb-ent. */
       RUN add-link IN adm-broker-hdl ( h_actacjb , 'tab-order':U , h_actajb-ent ).
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actajb-ent ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actajb-sor ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_actacjb ,
             h_actajb-sor , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_acta2jv ,
             h_actacjb , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_actopopjb ,
             h_acta2jv , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_actdjb ,
             h_actopopjb , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_actajb-ent ,
             h_actdjb , 'AFTER':U ).
    END. /* Page 3 */
    WHEN 11 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actdjs.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'TypeClose = Normal':U ,
             OUTPUT h_actdjs ).
       RUN set-position IN h_actdjs ( 6.14 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('3,1':U) NO-ERROR.

       /* Links to SmartSousFct h_actdjs. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actdjs ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Info':U , h_actdjs ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actdjs ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
    END. /* Page 11 */
    WHEN 12 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actopop2js.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  '':U ,
             OUTPUT h_actopopjs ).
       RUN set-position IN h_actopopjs ( 1.00 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('3,1':U) NO-ERROR.

       /* Links to SmartSousFct h_actopopjs. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actopopjs ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Info':U , h_actopopjs ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actopopjs ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
    END. /* Page 12 */
    WHEN 13 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actajs.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Layout-Options = "Master Layout,cout",
                     Default-Layout = Master Layout,
                     Titre = Entrants,
                     typacta = {&INIT-TYPACTA-ENTRANT}':U ,
             OUTPUT h_actajs-ent ).
       RUN set-position IN h_actajs-ent ( 6.05 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('3,1':U) NO-ERROR.

       /* Links to SmartSousFct h_actajs-ent. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actajs-ent ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Info':U , h_actajs-ent ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actajs-ent ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
    END. /* Page 13 */
    WHEN 14 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actajs.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Layout-Options = "Master Layout,cout",
                     Default-Layout = Master Layout,
                     Titre = Sortants,
                     typacta = {&INIT-TYPACTA-SORTANT},
                     TypeClose = Normal':U ,
             OUTPUT h_actajs-sor ).
       RUN set-position IN h_actajs-sor ( 6.05 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('3,1':U) NO-ERROR.

       /* Links to SmartSousFct h_actajs-sor. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actajs-sor ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Info':U , h_actajs-sor ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actajs-sor ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
    END. /* Page 14 */
    WHEN 15 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actacjs.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Layout-Options = "Master Layout,cout",
                     Default-Layout = cout,
                     Titre = Articles de co�t,
                     typacta = {&INIT-TYPACTA-ART-COUT},
                     TypeClose = Normal':U ,
             OUTPUT h_actajs-cout ).
       RUN set-position IN h_actajs-cout ( 6.05 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('3,1':U) NO-ERROR.

       /* Links to SmartSousFct h_actajs-cout. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actajs-cout ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Info':U , h_actajs-cout ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actajs-cout ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
    END. /* Page 15 */
    WHEN 16 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actodjs.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  '':U ,
             OUTPUT h_actodjs ).
       RUN set-position IN h_actodjs ( 1.00 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('3,1':U) NO-ERROR.

       /* Links to SmartSousFct h_actodjs. */
       RUN add-link IN adm-broker-hdl ( h_actdjb , 'Record':U , h_actodjs ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Info':U , h_actodjs ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actodjs ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
    END. /* Page 16 */
    WHEN 17 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actsstjs.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  '':U ,
             OUTPUT h_actsstjs ).
       RUN set-position IN h_actsstjs ( 4.00 , 8.20 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartSousFct h_actsstjs. */
       RUN add-link IN adm-broker-hdl ( h_actjb , 'Record':U , h_actsstjs ).
       RUN add-link IN adm-broker-hdl ( h_actjv , 'Info':U , h_actsstjs ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actsstjs ,
             btn-sortant:HANDLE IN FRAME F-Main , 'AFTER':U ).
    END. /* Page 17 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available F-Frame-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI F-Frame-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Enable-Buttons F-Frame-Win 
PROCEDURE Enable-Buttons :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/ 
 Do With Frame {&Frame-Name} :
    Assign btn-date:sensitive      = (Record-Available Or ModeFct = "Ajouter":U Or 
                                                          ModeFct = "Copier":U)
           btn-operation:sensitive = (btn-date:sensitive And Datef-Available)
           btn-entrant:sensitive   = (btn-date:sensitive And Datef-Available)
           btn-sortant:sensitive   = (btn-date:sensitive And Datef-Available)
           btn-cout:sensitive      = (btn-date:sensitive And Datef-Available)
           btn-sst:sensitive       = (ModeFct = "Ajouter":U Or ModeFct = "Modifier":U OR
                                                          ModeFct = "Copier":U)
                                                          
           /*btn-sst:sensitive      = (btn-date:sensitive And Datef-Available)*/
           F-datref:Sensitive      = (ModeFct = "Consult":U).
          
           
 End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI F-Frame-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  DISPLAY F-Datref 
      WITH FRAME F-Main.
  ENABLE F-Datref B_AIDAT- R-invalide 
      WITH FRAME F-Main.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-donnees F-Frame-Win 
PROCEDURE envoi-donnees :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter w-rowid as rowid no-undo.

    case NumPage-SousFct:
    
        when 11 then 
            run rech-rowid (h_actdjb,"kactd":U,output w-rowid).
        
        when 12 then
            run rech-rowid (h_actopopjb,"TtActopop":U,output w-rowid).
        
        when 13 then 
            run rech-rowid (h_actajb-ent,"kacta":U,output w-rowid).
            
        when 14 then 
            run rech-rowid (h_actajb-sor,"kacta":U,output w-rowid).
           
        when 15 then 
            run rech-rowid (h_actacjb,"kacta":U,output w-rowid).
            
       /* when 17 THEN DO: 
            MESSAGE "Je suis dans l'envoie donne de actjf".
            run rech-rowid (h_actjb,"kact.csst":U,output w-rowid).
               
        END.*/
             
                        
        Otherwise 
             w-rowid = ?.
             
     end. /*case*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRelatedFeatureParams F-Frame-Win 
PROCEDURE GetRelatedFeatureParams :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   {&GET-RELATED-FEATURE-PARAMS-DEFINITION}
  
   DEFINE VARIABLE vCart   AS CHARACTER NO-UNDO. /* CD  D1150 article de r�f�rence */ 
   DEFINE VARIABLE vFamAct AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vFamArt AS CHARACTER NO-UNDO.

   CASE iCfonct:
      WHEN "VIF.PACONUJ":U THEN DO:
         RUN Get-info("famact":U,"Infoact-Source":U).
         IF RETURN-VALUE <> ? THEN vFamAct = RETURN-VALUE.
         RUN Get-info("famart":U,"Infoact-Source":U).
         IF RETURN-VALUE <> ? THEN vFamArt = RETURN-VALUE.
         ASSIGN oParams = "ZPACON-CPACON=":U + {&LSTPARAMFAM} + "," + "|ZPACON-VALLCLE=":U + vFamAct /*+ "," + vFamArt*/.
      END.
      WHEN "VIF.TRSITCAD":U THEN DO:
         RUN Get-info("Cartref":U,"Infoact-Source":U).
         IF RETURN-VALUE <> ? THEN vCart = RETURN-VALUE.
         IF vCart <> "":U THEN ASSIGN oParams = "initItem=":U + vCart. 

      END.
   END CASE.
   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-close F-Frame-Win 
PROCEDURE local-adv-close :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vRet AS  LOGICAL   NO-UNDO.
  DEFINE VARIABLE vMsg AS  CHARACTER NO-UNDO.
  
  /* Code placed here will execute PRIOR to standard behavior. */
  If ModeFct <> "Consult":U Then Do :
     {affichemsg.i       &Message = "Get-TradMsg (002051, 'Veuillez valider ou abandonner la saisie d''activit� en cours.':T)"}
     Return "Adm-Error":U.
  End.
  
  /* Si cela n'est pas d�j� fait, suppression du process op�ratoire temporaire */
  /* et de ses liens cr��s pour les op�rations de l'activit� */
  RUN P-Delete-OPACT(OUTPUT vRet, OUTPUT vMsg).
  IF NOT vRet THEN DO:
     {affichemsg.i &Message = "vMsg"}
     RETURN "Adm-Error":U.
  END.
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-close':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-init-toutes-pages F-Frame-Win 
PROCEDURE local-adv-init-toutes-pages :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       Pour que la modif du kact en ligne soit effectivement r�percut�e
            dans le browse puis redispatch�e aux record-target, il faut que le 
            tab-order des record-target soit particulier :
            1) actjv ...
            Hors si on veut commencer par l'onglet 3, il faut :
            - commencer page 0
            - s�lectionner finalement la page 3
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-init-toutes-pages':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Run Select-Page(3).
  RUN adm-apply-entry IN h_actjb. /* SLe 16/06/09 */
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-change-page F-Frame-Win 
PROCEDURE local-change-page :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/


  /* Code placed here will execute PRIOR to standard behavior. */
  Run get-Attribute('Current-Page':U).
  
  If Integer(Return-Value) <> 3 And Not btn-date:Hidden In Frame {&Frame-Name} THEN DO:
   Assign btn-date:Hidden      In Frame {&Frame-Name} = Yes
            btn-operation:Hidden In Frame {&Frame-Name} = Yes
            btn-entrant:Hidden   In Frame {&Frame-Name} = yes
            btn-sortant:Hidden   In Frame {&Frame-Name} = yes
            btn-cout:Hidden      In Frame {&Frame-Name} = YES.
  END.
  ELSE If Integer(Return-Value) <> 2 THEN
           ASSIGN btn-sst:Hidden      In Frame {&Frame-Name} = YES.
               
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'change-page':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
    Run get-Attribute('Current-Page':U).
  If Integer(Return-Value) = 3 OR Integer(Return-Value) = 2 THEN DO:
      If Integer(Return-Value) = 3 THEN
       Assign btn-date:Hidden      In Frame {&Frame-Name} = No
            btn-operation:Hidden In Frame {&Frame-Name} = No
            btn-entrant:Hidden   In Frame {&Frame-Name} = No
            btn-sortant:Hidden   In Frame {&Frame-Name} = No
            btn-cout:Hidden      In Frame {&Frame-Name} = NO.
      If Integer(Return-Value) = 2 THEN
       Assign btn-sst:Hidden      In Frame {&Frame-Name} = No.
            
  END.
  

            
 /*   Run get-Attribute('Current-Page':U).
  If Integer(Return-Value) = 2 THEN DO:
  MESSAGE "2".
  Assign btn-sst:Hidden      In Frame {&Frame-Name} = NO.
  END.*/
     

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-create-objects F-Frame-Win 
PROCEDURE local-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'create-objects':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Run Get-Attribute ('Current-Page':U).
  If Integer(Return-Value) = 3 Then Do :
     If ModeFct = "consult":U Then
        Run Set-Info("Color-Browse={&COLOR-BTNFACE}":U,"Container-Target":U).
     Else
        Run Set-Info("Color-Browse=?":U,"Container-Target":U).
        
     Run Set-Info ("ModeFct=" + ModeFct,"InfoMode-Target":U).
  End.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize F-Frame-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vCpope AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vFonct AS LOGICAL   NO-UNDO.
  
  /* Code placed here will execute PRIOR to standard behavior. */
  Assign F-Datref = Today.
  
  ASSIGN  vgTInitialize = YES.
  /* Si la fonction est appel�e comme "fonction connexe", on initialise la fen�tre en s�lectionnant le lot pass� en "contexte".
   Pour ne pas faire 2 fois cette s�lection (2e fois dans la proc Traiter-Contexte), on initialize un t�moin vgTInitialize */
  RUN Get-Contexte IN Adv-Outils-Hdl ("cpope":U, OUTPUT vCpope).
  Run Set-Attribute-List ("cpope=":U + vCpope).  

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  R-invalide:Bgcolor In Frame {&Frame-name} = {&COLOR-WARNING2-BG}.
  
  /* CD D001150 fonctions connexes */      
  ASSIGN vgLibParamFam   = Get-TradTxt (015189, 'Param�tres':T).
  RUN AddRelatedFeature IN Adv-outils-Hdl (THIS-PROCEDURE, "VIF.PACONUJ":U, vgLibParamFam, OUTPUT hgParametresFam).


  {&Ctrl-Acces-Fonsoc} ('TRSITCAD':U, wcsoc, OUTPUT vFonct).
  IF vFonct THEN DO:
     ASSIGN vgLibCadArt   = Get-TradTxt (030260, 'Cadence par article':T). 
     RUN AddRelatedFeature IN Adv-outils-Hdl (THIS-PROCEDURE, "VIF.TRSITCAD":U, vgLibCadArt, OUTPUT hgCadArt).
  END.
 
 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-Retour-Sous-Fonction F-Frame-Win 
PROCEDURE local-Retour-Sous-Fonction :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vFonct AS LOGICAL   NO-UNDO.
  
  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'Retour-Sous-Fonction':U ) .

  /* Code placed here will execute AFTER standard behavior.    */ 
  
  If w-retour-ss-fct Then Do:
     w-retour-ss-fct = No.
     Run New-State ('Refresh-Data, Refresh-Target':u).
  End.
  
  
  /* CD D001150 fonctions connexes */      
  IF NOT VALID-HANDLE(hgParametresFam) THEN
     RUN AddRelatedFeature IN Adv-outils-Hdl (THIS-PROCEDURE, "VIF.PACONUJ":U, vgLibParamFam, OUTPUT hgParametresFam).

  {&Ctrl-Acces-Fonsoc} ('TRSITCAD':U, wcsoc, OUTPUT vFonct).
  IF vFonct THEN DO:
     IF NOT VALID-HANDLE(hgCadArt) THEN
        RUN AddRelatedFeature IN Adv-outils-Hdl (THIS-PROCEDURE, "VIF.TRSITCAD":U, vgLibCadArt, OUTPUT hgCadArt).
  END.
  
  /*RUN ToolBarBtnEnable IN Adv-outils-Hdl ({&ToolBarFonct-FonctConnexes}, NO).*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE P-Delete-OPACT F-Frame-Win 
PROCEDURE P-Delete-OPACT :
/*------------------------------------------------------------------------------
  Purpose: Supprimer tous les process op�ratoires temporaires et les liens de l'activit� avec celui-ci    
  Parameters:  <none>
  Notes: {&PROCESS-OP-ACTIVITE} est le process op�ratoire temporaire cr�� � partir
         des op�rations de l'activit�s (kacto et kactod)
------------------------------------------------------------------------------*/   
   DEFINE OUTPUT PARAMETER oRet AS LOGICAL   NO-UNDO.
   DEFINE OUTPUT PARAMETER oMsg AS CHARACTER NO-UNDO.
   
   DEFINE BUFFER   bKactdpop   FOR Kactdpop.
   DEFINE BUFFER   bKactapopd   FOR Kactapopd.
   DEFINE BUFFER   bKpope      FOR Kpope.   
   
   /*mise � jour*/
   DEFINE BUFFER   bufKactdpop FOR Kactdpop.
   DEFINE BUFFER   bufKactapopd FOR Kactapopd.
   DEFINE BUFFER   bufKpope    FOR Kpope.
   
   &SCOPED-DEFINE PROCESS-OP-ACTIVITE '$POPACT':U
   
   ASSIGN oRet = YES
          oMsg = "":U.

      /* Process op�ratoire temporaire */
      IF CAN-FIND(FIRST bKpope WHERE bKpope.csoc  = wcsoc 
                                 AND bKpope.cetab = wcetab
                                 AND bKpope.cpope = {&PROCESS-OP-ACTIVITE}
                               NO-LOCK) THEN DO:     
         FIND FIRST bufKpope WHERE bufKpope.csoc  = wcsoc 
                               AND bufKpope.cetab = wcetab
                               AND bufKpope.cpope = {&PROCESS-OP-ACTIVITE}  
                             EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
         IF LOCKED(bufKpope) THEN DO:
            ASSIGN oRet = NO               
                   oMsg = SUBSTITUTE(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T), 
                                      {&PROCESS-OP-ACTIVITE} ,
                                      'Kpope':U).
            RETURN.                         
         END.   
         ELSE
            DELETE bufKpope.
      END.  
            
      /* Lien Process op�ratoire temporaire - Activit� temporaire*/
      FOR EACH bKactdpop FIELDS(csoc cetab cact datef)
                          WHERE bKactdpop.csoc  = wcsoc                 
                            AND bKactdpop.cetab = wcetab                        
                            AND bKactdpop.cpope = {&PROCESS-OP-ACTIVITE}                                   
                         NO-LOCK:                 
         FIND FIRST bufKactdpop WHERE bufKactdpop.csoc  = bKactdpop.csoc                 
                                  AND bufKactdpop.cetab = bKactdpop.cetab                
                                  AND bufKactdpop.cact  = bKactdpop.cact                 
                                  AND bufKactdpop.datef = bKactdpop.datef                
                                  AND bufKactdpop.cpope = {&PROCESS-OP-ACTIVITE}
                                EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
         IF LOCKED(bufKactdpop) THEN DO:
            ASSIGN oRet = NO
                   oMsg = SUBSTITUTE(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                         , {&PROCESS-OP-ACTIVITE}, 'Kactdpop':U ).
                   
            RETURN.
         END.
         ELSE
            DELETE bufKactdpop.                  
      END.
                                                                                                                       
      /* Lien Articles de l'activit� - Op�rations du Process op�ratoire temporaire*/
      FOR EACH bKactapopd FIELDS(csoc cetab cact datef)
                          WHERE bKactapopd.csoc  = wcsoc                 
                            AND bKactapopd.cetab = wcetab                        
                            AND bKactapopd.cpope = {&PROCESS-OP-ACTIVITE}                                   
                         NO-LOCK:                 
         FIND FIRST bufKactapopd WHERE bufKactapopd.csoc  = bKactapopd.csoc                 
                                  AND bufKactapopd.cetab = bKactapopd.cetab                
                                  AND bufKactapopd.cact  = bKactapopd.cact                 
                                  AND bufKactapopd.datef = bKactapopd.datef                
                                  AND bufKactapopd.cpope = {&PROCESS-OP-ACTIVITE}
                                EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
         IF LOCKED(bufKactapopd) THEN DO:
            ASSIGN oRet = NO
                   oMsg = SUBSTITUTE(Get-TradMsg (017911, 'Blocage sur l''enregistrement &1 de la table &2':T)
                                         , {&PROCESS-OP-ACTIVITE}, 'Kactapopd':U ).
                   
            RETURN.
         END.
         ELSE
            DELETE bufKactapopd.                  
      END.                                                                                                           

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-InfoToolTip F-Frame-Win 
PROCEDURE Pr-InfoToolTip :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE INPUT  PARAMETER iCact   AS CHARACTER NO-UNDO.
   DEFINE INPUT  PARAMETER iDatef  AS DATE      NO-UNDO.
   DEFINE INPUT  PARAMETER iCfmact AS CHARACTER NO-UNDO.
   DEFINE OUTPUT PARAMETER oPop    AS LOGICAL   NO-UNDO.
   DEFINE OUTPUT PARAMETER oCunite AS CHARACTER NO-UNDO.
   DEFINE OUTPUT PARAMETER oPoidsE AS DECIMAL   NO-UNDO EXTENT 2.
   DEFINE OUTPUT PARAMETER oPoidsS AS DECIMAL   NO-UNDO EXTENT 2.
   
   DEFINE BUFFER bKactdpop FOR kactdpop.
   DEFINE BUFFER bKfmact   FOR kfmact.
   DEFINE BUFFER bKacta    FOR kacta.
   &SCOPED-DEFINE PROCESS-OP-ACTIVITE '$POPACT':U   
   
   /* 1- L'activit� est g�r�e via des process op�ratoires ou des op�rations classiques ? */
   ASSIGN oPop = CAN-FIND (FIRST bKactdpop WHERE bKactdpop.csoc  = wcsoc
                                             AND bKactdpop.cetab = wcetab
                                             AND bKactdpop.cact  = iCact
                                             AND bKactdpop.datef = iDatef
                                             AND bKactdpop.cpope <> {&PROCESS-OP-ACTIVITE}
                                           NO-LOCK).
   /* 2- Afficher la somme des entrants et la somme des sortants si l'unit� de rendement de la famille est une unit� de poids */
   IF iCfmact NE ? THEN
      FOR FIRST bKfmact Fields (cu)
                Where bKfmact.csoc   = wcsoc
                  And bKfmact.cetab  = wcetab
                  And bKfmact.cfmact = iCfmact
                No-Lock:
         IF {&UNITE-FAMILLE}(bKfmact.cu) = {&CFAMU-POIDS} THEN
            ASSIGN oCunite = bKfmact.cu.
      End.     
   IF oCunite NE "" THEN DO:
      /* Pour le moment, tr�s classique : pas de conversion */
      FOR EACH bKacta FIELDS (qte tligfr txfr)
                      WHERE bKacta.csoc    = wcsoc
                        AND bKacta.cetab   = wcetab
                        AND bKacta.cact    = iCact
                        AND bKacta.datef   = iDatef
                        AND bKacta.typacta = {&TYPACTA-ENTRANT}
                        AND bKacta.cunite  = oCunite
                      NO-LOCK:
         IF NOT bKacta.tligfr THEN
            ASSIGN oPoidsE[1] = oPoidsE[1] + bKacta.qte.
         ELSE
            ASSIGN oPoidsE[2] = oPoidsE[2] + bKacta.qte.
            
         If ( 1 - ( DECIMAL(bKacta.txfr) / 100 ) ) <> 0 Then
            ASSIGN oPoidsE[2] = bKacta.qte / ( 1 - ( bKacta.txfr / 100 )).
      END.
      FOR EACH bKacta FIELDS (qte)
                      WHERE bKacta.csoc    = wcsoc
                        AND bKacta.cetab   = wcetab
                        AND bKacta.cact    = iCact
                        AND bKacta.datef   = iDatef
                        AND bKacta.typacta = {&TYPACTA-SORTANT}
                        AND bKacta.cunite  = oCunite
                      NO-LOCK:
         ASSIGN oPoidsS[1] = oPoidsS[1] + bKacta.qte.
      END.               
   END.   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reaffiche-tableau F-Frame-Win 
PROCEDURE reaffiche-tableau :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input parameter w-hdl as widget-handle no-undo.
DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.

define variable w-rowid as rowid   no-undo.

    run envoi-donnees in p-issuer-hdl(output w-rowid).
 
    if w-rowid <> ? then do:
         run dispatch in w-hdl ("adv-reopen-query":U).
         run reposition-browse in w-hdl (w-rowid).
    end.
    else run dispatch in w-hdl ("open-query":U).     
       
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info F-Frame-Win 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.
     
   DEFINE VARIABLE vCunite AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vPop    AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vPoidsE AS DECIMAL   NO-UNDO EXTENT 2. /*le 2 est freinte comprise) */
   DEFINE VARIABLE vPoidsS AS DECIMAL   NO-UNDO EXTENT 2. /*le 2 est freinte comprise) */
   
   Case Name-Info :
       /* Ins�rer ici vos propres infos � recevoir */
       
       When "Modefct":U Then Do :
          ModeFct = Valeur-Info.
          Run Enable-Buttons.
          If ModeFct = "consult":U Then
             Run Set-Info("Color-Browse={&COLOR-BTNFACE}":U,"Container-Target":U).
          Else
             Run Set-Info("Color-Browse=?":U,"Container-Target":U).
          Run Set-Info ("ModeFct=" + ModeFct,"InfoMode-Target":U).
       End.
       
       When "Record-Available":U Then Do :
          Record-Available = (Valeur-Info = "Yes":U).
          Run Enable-Buttons.
       End.

       When "Datef-Available":U Then Do :
          Datef-Available = (Valeur-Info = "Yes":U).
          Run Enable-Buttons.
       End.
       
       When "Desactive":U Then
          F-Desactive:Font IN Frame {&Frame-Name} = (if Valeur-Info = 'oui':u then 15 else ?).

       /* SLE 17/01/05 on garde en m�moire la datef sur laquelle on est positionn�,
        * pour bien s'y replacer au terme du modify */
       WHEN "datefModify":u THEN
          ASSIGN vg-datefModify = (IF Valeur-Info NE ? THEN STRING(Valeur-Info) ELSE "").

      WHEN "Cle-Datef":U THEN DO:
         /* SLe 03/04/2012 */
         /* Remettons en cause les infos ToolTip des boutons (suite � row-changed sur datef) */
         RUN Get-Info("kact.cfmact":u,"InfoACT-Source":U).
          
         IF Valeur-info NE ? AND NUM-ENTRIES(Valeur-info) >= 2 THEN DO:
            /* 1- L'activit� est g�r�e via des process op�ratoires ou des op�rations classiques ? */
            /* 2- Afficher la somme des entrants et la somme des sortants si l'unit� de rendement de la famille est une unit� de poids */
            RUN Pr-InfoToolTip (INPUT  ENTRY(1,Valeur-info), DATE (ENTRY(2,Valeur-info)),RETURN-VALUE,
                                OUTPUT vPop,
                                OUTPUT vCunite,
                                OUTPUT vPoidsE,
                                OUTPUT vPoidsS).
         END.
         ASSIGN Btn-operation:TOOLTIP = STRING(vPop,Get-TradTxt (029909, 'Process op�ratoires':T) + "/":u + SUBSTITUTE(Get-TradTxt (000543, 'Op�rations de l''activit� &1':T)))
                btn-entrant:TOOLTIP   = (IF vCunite = "" THEN "" ELSE TRIM({&Unite-Affichage-Qte-format}(vCunite, '>>>,>>>,>>>,>>':u,vPoidsE[1])) 
                                                                        + ' ':U + vCunite)
                btn-sortant:TOOLTIP   = (IF vCunite = "" THEN "" ELSE TRIM({&Unite-Affichage-Qte-format}(vCunite, '>>>,>>>,>>>,>>':u,vPoidsS[1])) 
                                                                        + ' ':U + vCunite).
      END.
      
      /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
      WHEN "DatefHisto":U THEN
         ASSIGN vgtDatefHisto = (Valeur-Info = "oui":U).
      
       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE rech-rowid F-Frame-Win 
PROCEDURE rech-rowid :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define input  parameter w-hdl   as widget-handle            no-undo.
define input  parameter w-fic   as character                no-undo.
define output parameter w-rowid as rowid                    no-undo.

define variable w-list-rowid    as character no-undo format "x(30)":u.

     run send-records in w-hdl (w-fic,output w-list-rowid).
     w-rowid = to-rowid(w-list-rowid).
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info F-Frame-Win 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.
   
   Case Name-Info :
       /* Ins�rer ici vos propres infos � envoyer */
       When "Modefct":U Then
          Valeur-Info = ModeFct.
          
       When "F-Datref":U Then
         /* Valeur-Info = String(F-Datref). */
         /* SLE 17/01/05 on garde en m�moire la datef sur laquelle on est positionn�,
          * pour bien s'y replacer au terme du modify 
          */
         Valeur-Info = (IF vg-datefModify NE "" 
                           THEN vg-datefModify
                           ELSE String(F-Datref)).
                           
       /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
       WHEN "DatefHisto":U THEN
          ASSIGN Valeur-Info = (IF vgtDatefHisto THEN "oui":U ELSE "non":U).

       
/*        /* pour actdjs.w */                                           */
/*        WHEN 'HANDLE-SF-DATEF':U THEN Valeur-Info = STRING(h_actdjs). */
       
       Otherwise 
          /* Par d�faut, envoi de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Send-Info In Adm-Broker-Hdl (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records F-Frame-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartFonction, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed F-Frame-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.
      
  CASE p-state:
      /* Placez vos cases ici pour remplacez les states standard ou
         en mettre des nouveaux */
      
      when "fin-sous-traitance":u then do:
        run reaffiche-tableau (h_actdjb,p-issuer-hdl). 
      END.
      when "fin-dates-effet":u 
        then run reaffiche-tableau (h_actdjb,p-issuer-hdl). 
     
      when "fin-entrants":u 
        then run reaffiche-tableau (h_actajb-ent,p-issuer-hdl).
      
      when "fin-sortants":u 
        then run reaffiche-tableau (h_actajb-sor,p-issuer-hdl).
        
      when "fin-operations":u 
        then run reaffiche-tableau (h_actopopjb,p-issuer-hdl).         
      
      when "fin-articlescout":u 
        then run reaffiche-tableau (h_actacjb,p-issuer-hdl).  
                
      When "Data-Modified":u Then 
          /* Forca data-modified du viewer page 1 � yes */
          Run Set-Info ("Data-Modified=Yes":U,"Page1-Target":U).

      When "DBLCLICK-ACTDJD":u Then Do:
         If Btn-Date:sensitive in frame {&frame-name} then 
            Apply "Choose":u To Btn-Date .
      End.

      When "DBLCLICK-ACTAJS-ENT":u Then Do:
         If btn-entrant:sensitive in frame {&frame-name} then 
            Apply "Choose":u To Btn-entrant .
      End.

      When "DBLCLICK-ACTAJS-SOR":u Then Do:
         If btn-sortant:sensitive in frame {&frame-name} then 
            Apply "Choose":u To Btn-sortant .
      End.

      When "DBLCLICK-ACTOPOPJS":u Then Do:
         If btn-operation:sensitive in frame {&frame-name} then
            Apply "Choose":u To Btn-operation .
      End.

      When "DBLCLICK-ACTACJS":u Then Do:
         If btn-cout:sensitive in frame {&frame-name} then 
            Apply "Choose":u To Btn-cout .
      End.            
      
      /* Cases standard des fonction */
     {admvif\template\fonction.i "Fctstates"}
         
      /* Cases standard des container */
      {admvif/template/cinclude.i "cstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Traiter-Contexte F-Frame-Win 
PROCEDURE Traiter-Contexte :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       Proc�dure appel�e par "Lance-Fonction" m�me si la fonction est d�j� ouverte
------------------------------------------------------------------------------*/

   IF vgTInitialize THEN DO : /*on est pass� dans le local-initialize*/
      ASSIGN vgTInitialize = NO. 
      Run Traiter-Contexte     IN h_actjb(Output wstr).
   END.
   ELSE DO: /*la fonction est d�j� ouverte, on ne passe pas dans le local-initialize, il faut mettre � jour la s�lection*/
     /* SLE 20/01/05 */
     /*Run Get-Contexte In Adv-Outils-Hdl ("kact.datrech":U, Output wstr).
     If wstr <> "":U Then Do:
        ASSIGN f-datref:SCREEN-VALUE IN FRAME {&FRAME-NAME} = wstr.
        Run Set-Contexte In Adv-Outils-Hdl (This-Procedure,"kact.datrech":U, "":U).
     End.*/
     /* --- */
     /* SLe 06/03/2012 Fonction connexe Process op�ratoire */
     /*Run Get-Contexte In Adv-Outils-Hdl ("cpope":U, Output wstr).
     If wstr <> "":U Then Do:
        Run Set-Contexte In Adv-Outils-Hdl (This-Procedure,"cpope":U, wstr).
     End. */
     
     Run Traiter-Contexte     IN h_actjb(Output wstr).      /* Browse des activit�s         */
     Run Traiter-Contexte     IN h_actdjb(Output wstr).     /* Browse des dates d'effet     */
     Run Traiter-Contexte-Ent IN h_actajb-ent(Output wstr). /* Browse des articles entrants */
     Run Traiter-Contexte-Sor IN h_actajb-sor(Output wstr). /* Browse des articles sortants */
   END.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


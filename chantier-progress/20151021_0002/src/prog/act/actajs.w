&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actajs.w
 *
 * Description  : SmartSousFct sur Entrants,sortants,art. de co�t
 * 
 * Template     : sousfct.w - SmartSous-Fonction  
 * Type Objet   : SmartSousFct 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 01/08/1996 � 00:00 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 02/02/2016 - CJ      : DP002511:Consommation d'en-cours au sein d'un itin�raire
 *                              20151130_0012 (28029)|DP002511|Consommation d'en-cours au sein d'un itin�raire|CJ|02/02/16 16:06:12 
 *                               Cr�ation de la fonction consommation d'encours
 *  - Le 05/06/2015 - MMO     : QA8006:Probl�me d'affichage dans les activit�s - onglet A...
 *                              20150408_0005 (27033)|QA8006|Probl�me d'affichage dans les activit�s - onglet Article|MMO|05/06/15 18:01:25 
 *                               
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" F-Frame-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/standard.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS F-Frame-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

&GLOBAL-DEFINE adv-user-attr-globaux "typacta":U

/* Boite de dialogue pour les attributs */

/* Parameters Definitions ---                                           */

/* Constantes propres � la fonction */
&Scoped-Define Nombre-Pages 2

/* N� Contexte Aide en ligne */

/* Variables globales */
{variable.i " "}
{vif4bibi.i} /* enable - disable folder valorisation */

/* Local Variable Definitions ---                                       */
{admvif\template\sousfct.i "Definitions"}

/* Sous-Fonction de saisie fiche (dont le browse est sur la fct appelante) */
&Global-Define Gestion-Saisie-Fiche No
/* Faut-il garder en memoire la sous-fonction apres le 1er appel ? */
/* Ne modifier que si gestion-saisie-fiche = no */
/* QA8006 - passage � No le garder en m�moire car anomalie de perte de page */
&Global-Define Garder-En-Memoire NO

Define Variable vgTitre As Character No-Undo.

Define Variable vgModeFct As character No-undo.
Define Variable vgCact As Character No-Undo.

Define Variable vgTypacta AS CHARACTER No-Undo.

/* NLE 18/11/2003 */
Define Variable vgCacherFreinte   As Logical No-Undo.


/* CD D001150 Fonctions connexes */
&Global-Define  LSTPARAM          'CENTFAB,COTOD,CSORFAB,X-CREAOF':U
DEFINE VARIABLE hgParametres      AS HANDLE    NO-UNDO.
DEFINE VARIABLE vgLibParam        AS CHARACTER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" F-Frame-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartSousFct
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER FRAME

&Scoped-define ADM-SUPPORTED-LINKS Record-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-SousFct

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kactd
&Scoped-define FIRST-EXTERNAL-TABLE kactd


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kactd.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS R-ValidF R-ValidP btn-monter btn-descendre ~
Btn-actartr Btn-Freinte 

/* Custom List Definitions                                              */
/* Btn-SF,List-2,List-3,List-4,List-5,List-6                            */
&Scoped-define Btn-SF btn-monter btn-descendre 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_acta3jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actaajb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actaajv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actajb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actajv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actartjs AS HANDLE NO-UNDO.
DEFINE VARIABLE h_com1jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-updsav AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn-actartr 
     LABEL "&Art. de remplacement" 
     SIZE 25 BY 1.05.

DEFINE BUTTON btn-descendre 
     IMAGE-UP FILE "admvif/images\descendre":U
     IMAGE-INSENSITIVE FILE "admvif/images\descendre-i":U
     LABEL "&D" 
     SIZE 25 BY 1.05 TOOLTIP "Descendre (Alt-D)".

DEFINE BUTTON Btn-Freinte 
     LABEL "&Calcul de la freinte" 
     SIZE 25 BY 1.05.

DEFINE BUTTON btn-monter 
     IMAGE-UP FILE "admvif/images\monter":U
     IMAGE-INSENSITIVE FILE "admvif/images\monter-i":U
     LABEL "&T" 
     SIZE 25 BY 1.05 TOOLTIP "Monter (Alt-T)".

DEFINE VARIABLE F-ValidF AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 21.2 BY .62 NO-UNDO.

DEFINE VARIABLE F-ValidP AS CHARACTER FORMAT "X(256)":U 
      VIEW-AS TEXT 
     SIZE 21.2 BY .62 NO-UNDO.

DEFINE RECTANGLE R-ValidF
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 2.4 BY .57.

DEFINE RECTANGLE R-ValidP
     EDGE-PIXELS 1 GRAPHIC-EDGE    
     SIZE 2.4 BY .57.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-SousFct
     btn-monter AT ROW 1.29 COL 132.4
     btn-descendre AT ROW 2.33 COL 132.4
     Btn-actartr AT ROW 3.38 COL 132.4
     Btn-Freinte AT ROW 4.43 COL 132.4
     F-ValidP AT ROW 6.14 COL 133.8 COLON-ALIGNED NO-LABEL
     F-ValidF AT ROW 6.91 COL 133.8 COLON-ALIGNED NO-LABEL
     R-ValidF AT ROW 6.95 COL 132.2
     R-ValidP AT ROW 6.14 COL 132.2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 159.2 BY 16
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartSousFct
   External Tables: vif5_7.kactd
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Container Links: Page-Target,Record-Source,Record-Target,Navigation-Source,Navigation-Target,TableIO-Source,TableIO-Target
   Design Page: 1
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW F-Frame-Win ASSIGN
         HEIGHT             = 16.1
         WIDTH              = 160.2.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB F-Frame-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_sfct.i}
{src/adm/method/containr.i}
{admvif/method/ap_sfct.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW F-Frame-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-SousFct
   NOT-VISIBLE FRAME-NAME                                               */
ASSIGN 
       FRAME F-SousFct:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON btn-descendre IN FRAME F-SousFct
   1                                                                    */
/* SETTINGS FOR BUTTON btn-monter IN FRAME F-SousFct
   1                                                                    */
/* SETTINGS FOR FILL-IN F-ValidF IN FRAME F-SousFct
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       F-ValidF:PRIVATE-DATA IN FRAME F-SousFct     = 
                "init-txt=Validit� future".

/* SETTINGS FOR FILL-IN F-ValidP IN FRAME F-SousFct
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       F-ValidP:PRIVATE-DATA IN FRAME F-SousFct     = 
                "init-txt=Validit� pass�e".

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-SousFct
/* Query rebuild information for FRAME F-SousFct
     _Options          = ""
     _Query            is NOT OPENED
*/  /* FRAME F-SousFct */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-SousFct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-SousFct F-Frame-Win
ON HELP OF FRAME F-SousFct
DO:
  /* Gestion standard de l'aide en ligne */
  {admvif/template/tinclude.i "Help"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-actartr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-actartr F-Frame-Win
ON CHOOSE OF Btn-actartr IN FRAME F-SousFct /* Art. de remplacement */
DO:
  RUN Page-SousFct (10).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-descendre
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-descendre F-Frame-Win
ON CHOOSE OF btn-descendre IN FRAME F-SousFct /* D */
DO:
  Define Variable List-Hdl   As Character     No-Undo.
  Define Variable Hdl-Browse As Widget-Handle No-Undo.
  
  RUN dispatch IN THIS-PROCEDURE ('adv-modifs-validees':U).
  If Return-Value = "Adm-Error":U
    Then Return No-Apply.
    
  Run Get-Link-Handle In Adm-Broker-Hdl
          (Input This-Procedure, Input 'Ligne-Target':U, Output list-Hdl).
  Assign Hdl-Browse = Widget-Handle(Entry(1,List-Hdl)).
  
  If Valid-Handle(Hdl-Browse)
    Then Run Descendre In Hdl-Browse.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-Freinte
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-Freinte F-Frame-Win
ON CHOOSE OF Btn-Freinte IN FRAME F-SousFct /* Calcul de la freinte */
DO:
   DEFINE VARIABLE vQte     AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vTxFr    AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vCunite  AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vQteIni  AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vTxFrIni AS DECIMAL   NO-UNDO.
   
   Run Get-Info( "qte":U, "Freinte-Source":U ).
   Assign vQte = Decimal(Return-Value)
          vQteIni = vQte.
   Run Get-Info( "cunite":U, "Freinte-Source":U ).
   Assign vCunite = RETURN-VALUE.
   Run Get-Info( "txfr":U, "Freinte-Source":U ).
   Assign vTxfr = Decimal(Return-Value)
          vTxFrIni = vTxFr.
   {&Run-Prog} actafrld.w( INPUT-OUTPUT vQte,
                           INPUT        vCunite,
                           INPUT-OUTPUT vTxfr ).
   
   IF vgModeFct NE "Consult":U AND
      (vQte <> vQteIni OR vTxFr <> vTxFrIni)
   THEN DO:
      {affichemsg.i &Message = "Get-TradMsg (023996, 'Voulez-vous mettre � jour les informations de l''article ?':T)"
          &TypeBtn = {&MsgOuiNon} 
          &BtnDef = 1
      }
      IF ChoixMsg = 1 THEN DO:
         /* Envoi pour mise � jour au viewer */
         Run Set-Info( "qte=":U + STRING(vQte), "Freinte-Source":U ).
         Run Set-Info( "txfr=":U + STRING(vTxfr), "Freinte-Source":U ).      
      END.
   END.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME btn-monter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL btn-monter F-Frame-Win
ON CHOOSE OF btn-monter IN FRAME F-SousFct /* T */
DO:
  Define Variable List-Hdl   As Character     No-Undo.
  Define Variable Hdl-Browse As Widget-Handle No-Undo.
  
  RUN dispatch IN THIS-PROCEDURE ('adv-modifs-validees':U).
  If Return-Value = "Adm-Error":U
    Then Return No-Apply.
    
  Run Get-Link-Handle In Adm-Broker-Hdl
          (Input This-Procedure, Input 'Ligne-Target':U, Output list-Hdl).
  Assign Hdl-Browse = Widget-Handle(Entry(1,List-Hdl)).
  
  If Valid-Handle(Hdl-Browse)
    Then Run Monter In Hdl-Browse.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK F-Frame-Win 


{admvif\template\sousfct.i "Triggers"}

{admvif\template\sousfct.i "Main-Block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects F-Frame-Win  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actajb.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     typacta = {&INIT-TYPACTA-ENTRANT}':U ,
             OUTPUT h_actajb ).
       RUN set-position IN h_actajb ( 1.00 , 39.00 ) NO-ERROR.
       RUN set-size IN h_actajb ( 6.86 , 76.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/p-updsav.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'SmartPanelType = Save,
                     Edge-Pixels = 2,
                     SaveFunction = AddMultiple,
                     NameBtnAction = ,
                     Hide-Save = No,
                     Hide-Add = No,
                     Hide-Copy = No,
                     Hide-Delete = No,
                     Hide-cancel = No,
                     Hide-Print = Yes,
                     Hide-Documents = Yes,
                     PrintFunction = Normale,
                     Layout-Options = "Master Layout,Standard Character",
                     Default-Layout = Master Layout':U ,
             OUTPUT h_p-updsav ).
       RUN set-position IN h_p-updsav ( 1.71 , 10.60 ) NO-ERROR.
       /* Size in UIB:  ( 1.86 , 7.60 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/folder.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'FOLDER-TAB-TYPE = 2,
                     Folder-Labels = ':U + 'Article|Atelier|Commentaire|Valorisation' + ',
                     Ancrage = LEFT�BOTTOM�NO�NO�':U ,
             OUTPUT h_folder ).
       RUN set-position IN h_folder ( 7.81 , 1.00 ) NO-ERROR.
       RUN set-size IN h_folder ( 8.91 , 159.20 ) NO-ERROR.

       /* Links to SmartBrowser h_actajb. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Ligne':U , h_actajb ).
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Record':U , h_actajb ).

       /* Links to SmartPanel h_p-updsav. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Panel':U , h_p-updsav ).

       /* Links to SmartFolder h_folder. */
       RUN add-link IN adm-broker-hdl ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actajb ,
             btn-monter:HANDLE IN FRAME F-SousFct , 'BEFORE':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_folder ,
             Btn-Freinte:HANDLE IN FRAME F-SousFct , 'AFTER':U ).
    END. /* Page 0 */
    WHEN 1 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actajv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Layout-Options = Master Layout,
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Default-Layout = Master-Layout,
                     Suppression-Message = ,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     typacta = {&INIT-TYPACTA-ENTRANT}':U ,
             OUTPUT h_actajv ).
       RUN set-position IN h_actajv ( 8.86 , 10.80 ) NO-ERROR.
       RUN set-size IN h_actajv ( 7.19 , 143.60 ) NO-ERROR.

       /* Links to SmartViewer h_actajv. */
       RUN add-link IN adm-broker-hdl ( h_actajb , 'Record':U , h_actajv ).
       RUN add-link IN adm-broker-hdl ( h_p-updsav , 'TableIO':U , h_actajv ).
       RUN add-link IN adm-broker-hdl ( h_actajv , 'Freinte':U , THIS-PROCEDURE ).
       RUN add-link IN adm-broker-hdl ( h_actajv , 'info3':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actajv ,
             h_folder , 'AFTER':U ).
    END. /* Page 1 */
    WHEN 2 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actaajv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Layout-Options = "Master Layout,cout",
                     Initial-Lock = Default,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Default-Layout = Master Layout':U ,
             OUTPUT h_actaajv ).
       RUN set-position IN h_actaajv ( 9.67 , 22.20 ) NO-ERROR.
       RUN set-size IN h_actaajv ( 2.14 , 96.60 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actaajb.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non':U ,
             OUTPUT h_actaajb ).
       RUN set-position IN h_actaajb ( 12.19 , 23.00 ) NO-ERROR.
       RUN set-size IN h_actaajb ( 3.38 , 92.00 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartViewer h_actaajv. */
       RUN add-link IN adm-broker-hdl ( h_actajb , 'Record':U , h_actaajv ).
       RUN add-link IN adm-broker-hdl ( h_actajv , 'Group-Assign':U , h_actaajv ).

       /* Links to SmartBrowser h_actaajb. */
       RUN add-link IN adm-broker-hdl ( h_actajb , 'Record':U , h_actaajb ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actaajv ,
             h_folder , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_actaajb ,
             h_actaajv , 'AFTER':U ).
    END. /* Page 2 */
    WHEN 3 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'com1jv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'cetab = ,
                     csoc = ,
                     Titre = ,
                     typcle = {&INIT-COM-ACT},
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_com1jv ).
       RUN set-position IN h_com1jv ( 9.19 , 35.80 ) NO-ERROR.
       RUN set-size IN h_com1jv ( 7.38 , 82.40 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartViewer h_com1jv. */
       RUN add-link IN adm-broker-hdl ( h_actajb , 'Record':U , h_com1jv ).
       RUN add-link IN adm-broker-hdl ( h_actajv , 'Group-Assign':U , h_com1jv ).
       RUN add-link IN adm-broker-hdl ( h_actajv , 'info2':U , h_com1jv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_com1jv ,
             h_folder , 'AFTER':U ).
    END. /* Page 3 */
    WHEN 4 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'acta3jv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Initial-Lock = Default,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non':U ,
             OUTPUT h_acta3jv ).
       RUN set-position IN h_acta3jv ( 9.43 , 5.40 ) NO-ERROR.
       RUN set-size IN h_acta3jv ( 3.00 , 101.20 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartViewer h_acta3jv. */
       RUN add-link IN adm-broker-hdl ( h_actajb , 'Record':U , h_acta3jv ).
       RUN add-link IN adm-broker-hdl ( h_actajv , 'Group-Assign':U , h_acta3jv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_acta3jv ,
             h_folder , 'AFTER':U ).
    END. /* Page 4 */
    WHEN 10 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actartjs.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'TypeClose = Normal':U ,
             OUTPUT h_actartjs ).
       RUN set-position IN h_actartjs ( 1.00 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Links to SmartSousFct h_actartjs. */
       RUN add-link IN adm-broker-hdl ( h_actajb , 'Record':U , h_actartjs ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actartjs ,
             btn-monter:HANDLE IN FRAME F-SousFct , 'BEFORE':U ).
    END. /* Page 10 */

  END CASE.
  /* Select a Startup page. */
  IF adm-current-page eq 0 
  THEN RUN select-page IN THIS-PROCEDURE ( 1 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available F-Frame-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kactd"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kactd"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI F-Frame-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-SousFct.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI F-Frame-Win  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE R-ValidF R-ValidP btn-monter btn-descendre Btn-actartr Btn-Freinte 
      WITH FRAME F-SousFct.
  {&OPEN-BROWSERS-IN-QUERY-F-SousFct}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-donnees F-Frame-Win 
PROCEDURE envoi-donnees :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter w-rowid as rowid   no-undo.

MESSAGE "envoi donners".
/*define variable w-list-rowid    as character format "x(20)" no-undo.

    run send-records in h_actajb ("kacta":U, output w-list-rowid).
    w-rowid = to-rowid(w-list-rowid).*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GetRelatedFeatureParams F-Frame-Win 
PROCEDURE GetRelatedFeatureParams :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   {&GET-RELATED-FEATURE-PARAMS-DEFINITION}
  
   DEFINE VARIABLE vCart  AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vDatef AS DATE      NO-UNDO.

   CASE iCfonct:
      WHEN "VIF.PACONUJ":U THEN DO:
         ASSIGN oParams = "ZPACON-CPACON=":U + {&LSTPARAM}.
      END.

   END CASE.
   

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-close F-Frame-Win 
PROCEDURE local-adv-close :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  Define Variable Tot-Coef          AS   DECIMAL        No-Undo.
  Define Variable w-crges           AS   CHARACTER      No-Undo.
  Define Variable w-crges-coef-rep  As   Character      No-Undo.
  Define Variable w-crges-coef-prix As   Character      No-Undo.

  /* Code placed here will execute PRIOR to standard behavior. */
  Run Dispatch("Adv-Modifs-Validees":U).
  if return-value = "adm-error":U then Return "Adm-Error":U.

  /* CD Q7432 20140910_0001 10/09/2014 pour bloquer le passage en mode cr�ation � l'ouverture */
  Run new-State('RAZ-memCart,container-target':U).

  Run Get-Attribute('typacta':U).
  If Return-Value = {&TYPACTA-SORTANT} And Available kactd Then Do :
     Assign Tot-Coef = 0
            w-crges  = "":U.
     For Each kacta Of kactd Where kacta.typacta = {&TYPACTA-SORTANT} No-Lock :
        Tot-Coef = Tot-Coef + kacta.coefcout.
        If kacta.crges = {&KACTA-CRGES-COEFFICIENT-REPARTITION}
        Or kacta.crges = {&KACTA-CRGES-COEFFICIENT-DE-PRIX} Then Do:
            If w-crges = '':U Then Do:
                Assign w-crges = kacta.crges.
            End.
            Else Do:
                If w-crges <> kacta.crges Then Do:
                    Assign w-crges-coef-rep     = Get-TradClair( "KACTA-CRGES":U
                                                               , {&KACTA-CRGES-COEFFICIENT-REPARTITION}
                                                               , "{&LCODE-KACTA-CRGES}":U
                                                               , "{&LCLAIR-KACTA-CRGES}":U )
                           w-crges-coef-prix    = Get-TradClair( "KACTA-CRGES":U
                                                               , {&KACTA-CRGES-COEFFICIENT-DE-PRIX}
                                                               , "{&LCODE-KACTA-CRGES}":U
                                                               , "{&LCLAIR-KACTA-CRGES}":U ).
                    {affichemsg.i &Message="Substitute(Get-TradMsg (021093, 'Il ne peut pas y avoir un &1 et un &2 au niveau des sortants pour la date d''effet &3.&4 Voulez-vous corriger maintenant ?':T)
                                                      , w-crges-coef-rep
                                                      , w-crges-coef-prix
                                                      , String(kactd.datef,'99/99/9999':U)
                                                      , '|':U )"
                                  &TypeBtn = {&MsgOuiNon}
                                  &BtnDef = 1 }
                    If ChoixMsg <> 2 THEN DO:
                       /* MB M148957 20140313_0002 */
                       /* Positionnement sur l'onglet Valorisation */
                       RUN Select-Page IN THIS-PROCEDURE(4).
                       Return "Adm-Error":U.
                    END.   
                End.
            End.
        End.
     End.
     If (Return-Value = {&TYPACTA-SORTANT} OR Return-Value = {&TYPACTA-ENTRANT}) And Available kactd Then Do :
        For Each kacta Of kactd No-Lock :
           /* CJ DP2511 le contr�le doit se faire pour les entrants et les sortants */
           IF kacta.tqtedif AND kacta.lstnatstk <> "" THEN DO:
                    {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (005815, 'Donn�es incoh�rentes (&1).':T), Get-TradMsg (000216, 'Quantit� par diff�rence':T)  + '/':U +  Get-TradMsg (034454, 'Nature compl�mentaire atelier':T))"}
                    Return "Adm-Error":U.
           END.
        END.
     END.
     
     If w-crges = '':U Then
        Assign w-crges = {&KACTA-CRGES-COEFFICIENT-REPARTITION}.
     If Tot-Coef <> 100 And w-crges = {&KACTA-CRGES-COEFFICIENT-REPARTITION} Then do :
        {affichemsg.i &Message = "Get-TradMsg (004908, 'Le total des coefficients de r�partition est diff�rent de 100.|Voulez-vous corriger maintenant ?':T)"
                      &TypeBtn = {&MsgOuiNon}
                      &BtnDef = 1
        }
        If ChoixMsg <> 2 THEN DO:
           /* MB M148957 20140313_0002 */
           /* Positionnement sur l'onglet Valorisation */
           RUN Select-Page IN THIS-PROCEDURE(4). 
           Return "Adm-Error":U.
        END.
     End.
  End.


  /* Suppression des fonctions connexes */
  RUN Pr-CacheFctConnexe (YES).
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-close':U ) .
  
  
  /* Code placed here will execute AFTER standard behavior.    */
  if return-value = "adm-error":U then Return "Adm-Error":U.
 
 
  run get-attribute("titre":U).
  if Return-Value Begins "Entrants":U 
     then Run new-State('fin-entrants,container-source':U).
  if Return-Value Begins "Sortants":U 
     then Run new-State('fin-sortants,container-source':U).   
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-enable-sousfct F-Frame-Win 
PROCEDURE local-adv-enable-sousfct :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  MESSAGE "enable sous fonction".
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-enable-sousfct':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  If vgTypacta = {&TYPACTA-SORTANT} Then 
      Btn-actartr:sensitive In Frame {&Frame-Name} = False. 
  
  /* MB D002845 22/08/07 - Gestion de la freinte aussi pour les sortants (Art.R�f. uniquement) */  
/*Btn-Freinte:Hidden In Frame {&Frame-Name} = (vgTypacta <> {&TYPACTA-ENTRANT} Or vgCacherFreinte). */
  ASSIGN Btn-Freinte:HIDDEN IN FRAME {&FRAME-NAME} = vgCacherFreinte.
  
  /* D�sactivation des boutons dont l'activation va d�pendre de la ligne courante du browse */
  Assign Btn-monter:sensitive in frame {&frame-name}    = No
         Btn-Descendre:sensitive in frame {&frame-name} = No.
  
  /* Activation des boutons ci-dessus en fonction de la ligne courante du browse */       
  If vgModeFct <> "Consult":U Then RUN Enable-buttons IN h_actajb.   
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize F-Frame-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
/* << KL 20130910_0007 DP1662 : Param�tre permettant de g�rer l'affichage du browser actaajb.w */
DEFINE VARIABLE vHidden AS LOGICAL   NO-UNDO. 
DEFINE BUFFER bKacta FOR kacta.
DEFINE BUFFER bKactapopd FOR kactapopd.
/* >> KL 20130910_0007 DP1662 */

DEFINE VARIABLE vFonct AS LOGICAL NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */
  {getparii.i &CPACON="'ACTIVITE':U" 
              &NOCHAMP=2
              &CSOC=wcsoc
              &CETAB=wcetab
  } 
  vgCacherFreinte = (vParam-Ok And vParam-Valeurs = 'Yes':u ).

  run get-attribute("uib-mode":U).
  if return-value <> "design":u then DO WITH FRAME {&FRAME-NAME}:
    run get-attribute("titre":U).
    /* Cas possibles : Entrants ou Sortants ou Articles de co�t */
    /* vgTitre = Return-Value. SLe Q003175 TRAD g�r�e via typacta et non plus l'attribut Titre */
    
    Run Get-Attribute("typacta":U).
    vgTypacta = Return-Value.    
    
    /* Libell� du bouton 'Articles de ... ' */
    Case vgTypacta:
       When {&TYPACTA-ENTRANT} THEN 
          Assign Btn-actartr:Label  = Get-TradTxt (004933, '&Art. de remplacement':T)
                 Btn-Freinte:Hidden = vgCacherFreinte
                 vgTitre            = Get-TradTxt (009399, 'Entrants':T).
  
       /* MB D002845 22/08/07 - gestion de la freinte pour les sortants aussi */
       When {&TYPACTA-SORTANT} THEN
          Assign Btn-actartr:Label  = Get-TradTxt (004936, '&Art. de d�classement':T)
                 Btn-Freinte:Hidden = vgCacherFreinte
                 vgTitre            = Get-TradTxt (009395, 'Sortants':T).

       OTHERWISE
          Assign Btn-actartr:hidden  = TRUE
                 Btn-Freinte:Hidden  = Yes
                 vgTitre             = Get-TradTxt (003994, 'Articles de co�t':T).
     End Case. 
     
     
  end.  
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ("initialize":U).

  /* CD DP2374 activer ou d�sactiver l'onglet valorisation en fonction du typacta */ 
  Case vgTypacta:
     When {&TYPACTA-ENTRANT} THEN DO:
        RUN Disable-Folder-Page in h_folder(4).       
     END.
   
     When {&TYPACTA-SORTANT} THEN DO:   
        RUN Enable-Folder-Page  in h_folder(4).
     END.

     OTHERWISE DO:
        RUN Disable-Folder-Page in h_folder(4). 
     END.
  End Case. 

  /* Code placed here will execute AFTER standard behavior.    */
  R-ValidP:Bgcolor In Frame {&Frame-name} = {&COLOR-WARNING2-BG}.
  R-ValidF:Bgcolor In Frame {&Frame-name} = {&COLOR-WARNING-BG}.
  
      
    /* << KL 20130910_0007 D1662 : Gestion de l'affichage du browser actaajb.w */
  /* Ce browser sera affich� si au moins une op�ration est li�e � un process op�ratoire */                       
  IF VALID-HANDLE(h_actaajb) THEN DO:
      ASSIGN vHidden = TRUE.

      IF AVAILABLE kactd THEN DO:
         BCL:
         FOR EACH bKacta OF kactd NO-LOCK :
            /* Si on trouve un r�sultat, le browser sera affich�. */
            IF CAN-FIND (FIRST bKactapopd WHERE bKactapopd.csoc   = bKacta.csoc
                                           AND bKactapopd.cetab   = bKacta.cetab
                                           AND bKactapopd.cact    = bKacta.cact
                                           AND bKactapopd.datef   = bKacta.datef
                                           AND bKactapopd.typacta = bKacta.typacta
                                           AND bKactapopd.ni1     = bKacta.ni1
                                       NO-LOCK) THEN DO:
               ASSIGN vHidden = FALSE.
            END.
            IF NOT vHidden THEN LEAVE BCL.
         END.
      END.
      ASSIGN h_actaajb:HIDDEN = vHidden.
  END.
  /* >> KL 20130910_0007 D1662 */

  /* CD D001150 fonctions connexes */      
  run get-attribute("uib-mode":U).
  if return-value <> "design":u then DO WITH FRAME {&FRAME-NAME}:
     IF VALID-HANDLE (Adv-outils-Hdl) THEN DO:
        RUN Pr-CacheFctConnexe(NO).
     END.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-Retour-Sous-Fonction F-Frame-Win 
PROCEDURE local-Retour-Sous-Fonction :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'Retour-Sous-Fonction':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  MESSAGE "retour sous fonction".
  RUN Pr-CacheFctConnexe (NO).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available F-Frame-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  define variable w-rowid-kacta as rowid no-undo.

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If Return-Value <> "Adm-Error":U And 
     Valid-Handle(Fonction-Source-Hdl) Then Do :
    run envoi-donnees in Fonction-Source-Hdl (output w-rowid-kacta).
    if w-rowid-kacta <> ? then
        run reposition-browse in h_actajb (input w-rowid-kacta).
  end.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-view F-Frame-Win 
PROCEDURE local-view :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/  
   DEFINE VARIABLE vtDatefHisto AS LOGICAL   NO-UNDO. /* MB DP001776 */
/* -------------------------------------------------------------------------- */  
  
  /* Code placed here will execute PRIOR to standard behavior. */
    
  Run Get-Info ("F-cact":U, "Info-Source":U).
  vgCact = Return-Value.
  
  Run SetTitle (vgTitre +
                (if vgCact ne "" 
                    then " " + Get-TradTxt (004941, 'de l''activit�':T) + " " + vgCact
                    else "")). 
                    
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'view':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
  RUN Get-Info ("DatefHisto":U, "Container-Source":U).
  IF RETURN-VALUE <> ? THEN
     ASSIGN vtDatefHisto = (RETURN-VALUE = "oui":U).
 
  IF vtDatefHisto THEN
     RUN Set-Link-Attribute In Adm-Broker-Hdl (This-Procedure,'Panel-Target':U,
                                              'Mode-Consult=Oui':U ).
  ELSE DO:
  /* Fin DP001776 */
     Run Get-Info ("ModeFct":U, "Container-Source":U).
     vgModeFct = Return-Value.
     Run Set-Link-Attribute In Adm-Broker-Hdl (This-Procedure,'Panel-Target':U,
                            'Mode-Consult=':U + STRING(vgModeFct = "Consult":U,"Oui/Non":U)).
  END.   
  /* modification cle le 17/09/03 */
  /*Assign Btn-monter:sensitive in frame {&frame-name}    =  vgModeFct <> "Consult":U
   *          Btn-Descendre:sensitive in frame {&frame-name} =  vgModeFct <> "Consult":U.*/
   Assign Btn-monter:sensitive in frame {&frame-name}    =  No
          Btn-Descendre:sensitive in frame {&frame-name} =  No.
   If vgModeFct <> "Consult":U
      AND NOT vtDatefHisto /* MB DP001776 */ Then 
      RUN Enable-buttons IN h_actajb.
  
   Run New-State ("Reinit-Panel,Panel-Target":U).

  RUN Pr-CacheFctConnexe (NO).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-CacheFctConnexe F-Frame-Win 
PROCEDURE Pr-CacheFctConnexe :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE INPUT PARAMETER iCache AS LOGICAL   NO-UNDO.
   
   DEFINE VARIABLE vFonct AS LOGICAL   NO-UNDO.
   
   
   IF iCache THEN DO:
      IF VALID-HANDLE(hgParametres) THEN DELETE WIDGET hgParametres.
   END.
   ELSE DO:
     /* RUN ToolBarBtnEnable IN Adv-outils-Hdl ({&ToolBarFonct-FonctConnexes}, YES).*/
      IF NOT VALID-HANDLE(hgParametres) THEN DO:
         ASSIGN vgLibParam   = Get-TradTxt (015189, 'Param�tres':T).
         RUN AddRelatedFeature IN Adv-outils-Hdl (THIS-PROCEDURE, "VIF.PACONUJ":U, vgLibParam, OUTPUT hgParametres). 
      END.
   END.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info F-Frame-Win 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
       /* Ins�rer ici vos propres infos � recevoir */
       /* ajout Cle le 10/09/2003 */
       When "btn-monter:sensitive":U Then Do With Frame {&Frame-Name} :
             If Valeur-Info ="yes":u And vgModeFct <> "Consult":U Then btn-monter:sensitive = yes.
             else btn-monter:sensitive = no.
             End.
       When "btn-descendre:sensitive":U Then Do With Frame {&Frame-Name} :
             If Valeur-Info ="yes":u And vgModeFct <> "Consult":U Then btn-descendre:sensitive = yes.
             else btn-descendre:sensitive = no.
             End.
        /* fin ajout Cle le 10/09/2003 */
       
       /* L�gende en gras */
       When "Valid":U Then
           CASE Valeur-Info :
               WHEN "Future" THEN DO :
                   F-ValidP:Font IN Frame {&Frame-Name} = ?.
                   F-ValidF:Font IN Frame {&Frame-Name} = 15.
               END.
               WHEN "Passee" THEN DO :
                   F-ValidP:Font IN Frame {&Frame-Name} = 15.
                   F-ValidF:Font IN Frame {&Frame-Name} = ?.
               END.
               WHEN "Valide" THEN DO :
                   F-ValidP:Font IN Frame {&Frame-Name} = ?.
                   F-ValidF:Font IN Frame {&Frame-Name} = ?.
               END.
           END.
      
      /* MB D002854 (II) 12/11/07 */     
      /* Pour les sortants non article de r�f�rence, */
      /* on cache le bouton Calcul de la freinte     */
      WHEN "Btn-freinte:hidden":U THEN 
         IF Valeur-Info = "YES":U OR vgCacherFreinte THEN
            ASSIGN Btn-Freinte:HIDDEN = TRUE.
         ELSE 
            ASSIGN Btn-Freinte:HIDDEN = FALSE. 
      
      WHEN "Btn-freinte:sensitive":U THEN 
         IF Valeur-Info = "YES":U THEN
            ASSIGN Btn-Freinte:SENSITIVE = TRUE.
         ELSE 
            ASSIGN Btn-Freinte:SENSITIVE = FALSE. 
      /* Fin MB D002854 (II) */
      
       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info F-Frame-Win 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
       /* Ins�rer ici vos propres infos � envoyer */

       When "ModeFct":U Then Valeur-Info = vgModeFct.

       When "F-cact":U Then Valeur-Info = vgCact. 
       
      WHEN "kact.cfmact":U THEN DO:
           RUN Get-info("kact.cfmact","Info-Source").
           Valeur-Info = RETURN-VALUE.
      END.

       Otherwise 
          /* Par d�faut, envoi de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Send-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records F-Frame-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kactd"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed F-Frame-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Placez vos cases ici pour remplacez les states standard ou
         en mettre des nouveaux */            

      /* Cas standard des sous-fonctions */
      {admvif/template/sousfct.i "sfctstates"}
               
      /* Cases standard des container */
      {admvif/template/cinclude.i "cstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


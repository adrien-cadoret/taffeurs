&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Tablejs _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actsstjs.w
 *
 * Description  : SmartSousFct sur les sous-traitant d'une activit�
 * 
 * Template     : sousfct.w - SmartSous-Fonction 
 * Type Objet   : SmartSousFct 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 03/11/2015 � 10:32 par AC
 *
 * HISTORIQUE DES MODIFICATIONS :
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME ACT

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Tablejs _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/sousfctd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Tablejs 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/
/*** Constantes g�n�rales de la sous-fonction ***/

/*** Variables globales ***/
{variable.i " "}

{admvif/template/sousfct.i "Definitions"}

/* Nombre de pages � initialiser d�s le d�but */
&SCOPED-DEFINE Nombre-Pages 0

/* Sous-Fonction de saisie fiche (dont le browse est sur la fct appelante) */
&Global-Define Gestion-Saisie-Fiche NO
/* Faut-il garder en memoire la sous-fonction apres le 1er appel ? */
/* Ne modifier que si gestion-saisie-fiche = no */
&SCOPED-DEFINE Garder-En-Memoire YES




/*----------------------------------------------------------------------*/

/* D�finition des variables globales */
/* DEFINE VARIABLE vgCsoc AS CHARACTER NO-UNDO. */
DEFINE VARIABLE vgCact AS CHARACTER NO-UNDO.

/* MB M139113 20130827_0010 */
DEFINE VARIABLE vgModefct AS CHARACTER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" Tablejs _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartSousFct
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER FRAME

&Scoped-define ADM-SUPPORTED-LINKS Record-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-SousFct

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kact
&Scoped-define FIRST-EXTERNAL-TABLE kact


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kact.
/* Custom List Definitions                                              */
/* Btn-SF,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_actsstjb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_actsstjv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-updsav AS HANDLE NO-UNDO.

/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-SousFct
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 159.2 BY 17
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartSousFct
   External Tables: vif5_7.kact
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Container Links: Page-Target,Record-Source,Record-Target,Navigation-Source,Navigation-Target,TableIO-Source,TableIO-Target
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Tablejs ASSIGN
         HEIGHT             = 17.1
         WIDTH              = 151.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Tablejs 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_sfct.i}
{src/adm/method/containr.i}
{admvif/method/ap_sfct.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW Tablejs
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-SousFct
   NOT-VISIBLE FRAME-NAME Custom                                        */
ASSIGN 
       FRAME F-SousFct:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-SousFct
/* Query rebuild information for FRAME F-SousFct
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-SousFct */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" Tablejs _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-SousFct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-SousFct Tablejs
ON HELP OF FRAME F-SousFct
DO:
   /* Gestion standard de l'aide en ligne */
   {admvif/template/tinclude.i "Help"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Tablejs 


{admvif/template/sousfct.i "Triggers"}
{admvif/template/sousfct.i "Main-Block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects Tablejs  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actsstjb.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non':U ,
             OUTPUT h_actsstjb ).
       RUN set-position IN h_actsstjb ( 2.33 , 14.80 ) NO-ERROR.
       RUN set-size IN h_actsstjb ( 8.19 , 137.80 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/p-updsav.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'SmartPanelType = Save,
                     Edge-Pixels = 0,
                     SaveFunction = AddMultiple,
                     NameBtnAction = ,
                     Hide-Save = no,
                     Hide-Add = no,
                     Hide-Copy = no,
                     Hide-Delete = no,
                     Hide-cancel = no,
                     Hide-Print = yes,
                     Hide-Documents = yes,
                     PrintFunction = Normale,
                     OrientPanel = Horizontal':U ,
             OUTPUT h_p-updsav ).
       RUN set-position IN h_p-updsav ( 12.05 , 119.60 ) NO-ERROR.
       /* Size in UIB:  ( 1.81 , 20.00 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actsstjv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     DISABLE-ON-INIT = yes,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_actsstjv ).
       RUN set-position IN h_actsstjv ( 11.19 , 36.00 ) NO-ERROR.
       RUN set-size IN h_actsstjv ( 2.33 , 71.00 ) NO-ERROR.

       /* Links to SmartBrowser h_actsstjb. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Record':U , h_actsstjb ).

       /* Links to SmartPanel h_p-updsav. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Panel':U , h_p-updsav ).

       /* Links to SmartViewer h_actsstjv. */
       RUN add-link IN adm-broker-hdl ( h_p-updsav , 'TableIO':U , h_actsstjv ).

       /* Adjust the tab order of the smart objects. */
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available Tablejs  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kact"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kact"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Tablejs  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-SousFct.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Tablejs  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  {&OPEN-BROWSERS-IN-QUERY-F-SousFct}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-donnees Tablejs 
PROCEDURE envoi-donnees :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter w-rowid as rowid   no-undo.

define variable w-list-rowid    as character format "x(30)" no-undo.

    run send-records in h_actsstjb ("kactsst":U, output w-list-rowid).
    w-rowid = to-rowid(w-list-rowid).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-close Tablejs 
PROCEDURE local-adv-close :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-close':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  if return-value <> "adm-error":U then 
    Run new-State('fin-sous-traitance,container-source':U).

/*
  /* Code placed here will execute PRIOR to standard behavior. */

  Run Get-Info ("F-cact":U, "Info-Source":U).
  
  vgCact = RETURN-VALUE. 
  /* Code placed here will execute AFTER standard behavior.    */

DEFINE BUFFER bKactsst FOR kactsst.
DEFINE VARIABLE nb-csst AS INTEGER INITIAL 0 NO-UNDO.
DEFINE VARIABLE csst-rep AS CHARACTER INITIAL "" NO-UNDO.
DEFINE BUFFER bKact FOR kact.
  
FOR EACH bKactsst 
         WHERE bKactsst.csoc = wcsoc
         AND bKactsst.cetab = wcetab
         AND bKactsst.cact = vgCact
         USE-INDEX i02-sstact NO-LOCK:
         
     IF(nb-csst = 0) THEN 
     csst-rep = bKactsst.csst.
     ELSE 
     csst-rep = csst-rep + ", " + bKactsst.csst.
     
     nb-csst = nb-csst + 1.
END.
   

   
FIND FIRST bKact 
     WHERE bKact.csoc = wcsoc
       AND bKact.cetab = wcetab
       AND bKact.cact = vgCact
       USE-INDEX i1-act EXCLUSIVE-LOCK.
     IF AVAILABLE bKact THEN DO:
        IF(nb-csst > 0) THEN 
         ASSIGN bKact.tsstrait = YES.
        ELSE
         ASSIGN bKact.tsstrait = NO. 
        MESSAGE "" + STRING(bKact.tsstrait).
     END.
     
MESSAGE "rep : " + csst-rep.     

    RUN Notify ('local-display-fields,Info-Target':U). 
     
Run Set-Info("csst-rep=":U + csst-rep,"Info-Target":U). 

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-close':U ) .
   */
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available Tablejs 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   define variable w-rowid-kactsst as rowid no-undo.

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
   if return-value <> "adm-error":U then do:
    If Valid-Handle(Fonction-Source-Hdl) Then Do :
       run Envoi-donnees in Fonction-Source-Hdl (output w-rowid-kactsst) no-error.
       if w-rowid-kactsst <> ? then
           run reposition-browse in h_actsstjb (input w-rowid-kactsst).
    End.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-view Tablejs 
PROCEDURE local-view :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  Run Get-Info ("F-cact":u, "Info-Source":U).
  vgCact = Return-Value.
  If vgCact <> "" AND 
     vgCact <> ?  Then
     Run SetTitle( Get-TradTxt (000321, 'Dates d''effet de l''activit�':T)
                   + ' ':U + vgCact).
  
  Run Get-Info ("ModeFct":U, "Container-Source":U).
  Run Set-Link-Attribute In Adm-Broker-Hdl (This-Procedure,'Panel-Target':U,
                         'Mode-Consult=':U + (If Return-Value = "Consult":U Then "Oui":U Else "Non":U)).
  Run New-State ("Reinit-Panel,Panel-Target":U).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'view':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info Tablejs 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   /*Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
       /* Ins�rer ici vos propres infos � envoyer */
       WHEN 'CACT':U THEN 
          ASSIGN Valeur-Info = vgCact.
       
       WHEN 'cacttmp':U THEN 
          ASSIGN valeur-info = (If Available kact 
                                   THEN kact.cact 
                                   ELSE "":U).
       
       
       WHEN 'HANDLE':U THEN DO:
/*           /* vient de actjf.w */                                    */
/*           Run Get-Info ("HANDLE-SF-DATEF":u, "Container-Source":U). */
/*                                                                     */
/*           /* va � actd3jv.w */                                      */
/*           Valeur-Info = STRING(Return-Value).                       */
           Valeur-Info = STRING(THIS-PROCEDURE:HANDLE).
       END.
      
      /* MB M139113 20130827_0010 */
      WHEN "ModeFct":U THEN DO:
         ASSIGN Valeur-Info = vgModefct. 
      END.
      
       Otherwise 
          /* Par d�faut, envoi de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Send-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records Tablejs  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kact"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed Tablejs 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Placez vos cases ici pour remplacez les states standard ou
         en mettre des nouveaux */

      /* Cas standard des sous-fonctions */
      {admvif/template/sousfct.i "sfctstates"}
               
      /* Cases standard des container */
      {admvif/template/cinclude.i "cstates"}
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


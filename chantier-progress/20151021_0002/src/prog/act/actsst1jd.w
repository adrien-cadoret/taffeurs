&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&Scoped-define FRAME-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" D-Dialog _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actsst1jd.w
 *
 * Description  : Dialog permettant la gestion des sous-traitants d'une activit�
 * 
 * Template     : cntnrdlg.w - SmartDialog basique 
 * Type Objet   : SmartDialog 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 08/02/2016 � 08:43 par AC
 *
 * HISTORIQUE DES MODIFICATIONS :
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME ACT

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" D-Dialog _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS D-Dialog 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/
/* Variables globales */
{variable.i " "}

/*** Constantes g�n�rales de la Smart Dialog-Box ***/
/* Confirmer la sortie pat Escape ? */
&SCOPED-DEFINE Confirmer-Escape NO
/*Activation des touches du panel s'il y en a un*/
&SCOPED-DEFINE Active-Touche-Panel NO
/* Confirmation de la validation des modifications */
&SCOPED-DEFINE Verif-Modifs-Validees YES
/* Nom de programme � afficher en bas de fen�tre */
/* &Scoped-Define NomProg-A-Afficher   */ 

{admvif/template/cntnrdlg.i "Definitions"}

/*----------------------------------------------------------------------*/


 DEFINE INPUT PARAMETER i-csoc AS CHARACTER NO-UNDO.
 DEFINE INPUT PARAMETER i-cetab AS CHARACTER NO-UNDO.
 DEFINE INPUT PARAMETER i-cact AS CHARACTER NO-UNDO.
 
 
 DEFINE VARIABLE i-csst AS CHARACTER NO-UNDO.
 DEFINE VARIABLE i-priorite AS INTEGER NO-UNDO.
 DEFINE VARIABLE Mode AS CHARACTER NO-UNDO.
 DEFINE VARIABLE fields-values AS CHARACTER NO-UNDO.
 
 DEFINE BUFFER bufKactsst FOR kactsst.

/* D�finition des variables globales */

&GLOBAL-DEFINE TABLE kactsst
&GLOBAL-DEFINE ltable Sous-traitant par activit�

Define work-table w-actsst like kactsst.

Define Variable Wrowid   As Rowid   No-Undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" D-Dialog _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartDialog
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER DIALOG-BOX

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME D-Dialog

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Btn_OK Btn_Annuler Btn_Aide 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */

/* Define a dialog box                                                  */

/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_actsstjv AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn_Aide 
     LABEL "Ai&de" 
     SIZE 15 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON Btn_Annuler AUTO-END-KEY 
     LABEL "Annuler" 
     SIZE 15 BY 1.05
     BGCOLOR 8 .

DEFINE BUTTON Btn_OK AUTO-GO 
     LABEL "OK" 
     SIZE 15 BY 1.05
     BGCOLOR 8 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME D-Dialog
     Btn_OK AT ROW 19.95 COL 106.8
     Btn_Annuler AT ROW 19.95 COL 122.8
     Btn_Aide AT ROW 19.95 COL 138.8
     SPACE(0.00) SKIP(0.56)
    WITH VIEW-AS DIALOG-BOX KEEP-TAB-ORDER 
         SIDE-LABELS NO-UNDERLINE THREE-D  SCROLLABLE 
         FONT 6
         TITLE "Sous-traitants de l'activit�"
         DEFAULT-BUTTON Btn_OK CANCEL-BUTTON Btn_Annuler.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartDialog
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Other Settings: COMPILE
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB D-Dialog 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_dialo.i}
{src/adm/method/containr.i}
{admvif/method/ap_dialo.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR DIALOG-BOX D-Dialog
   FRAME-NAME                                                           */
ASSIGN 
       FRAME D-Dialog:SCROLLABLE       = FALSE
       FRAME D-Dialog:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK DIALOG-BOX D-Dialog
/* Query rebuild information for DIALOG-BOX D-Dialog
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* DIALOG-BOX D-Dialog */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" D-Dialog _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME D-Dialog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON END-ERROR OF FRAME D-Dialog /* Sous-traitants de l'activit� */
ANYWHERE DO:
   {admvif/template/cntnrdlg.i "End-Error"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON ENDKEY OF FRAME D-Dialog /* Sous-traitants de l'activit� */
ANYWHERE DO:
   {admvif/template/cntnrdlg.i "End-Key"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL D-Dialog D-Dialog
ON WINDOW-CLOSE OF FRAME D-Dialog /* Sous-traitants de l'activit� */
DO:  
   /* Add Trigger to equate WINDOW-CLOSE to END-ERROR. */
   APPLY "END-ERROR":U TO SELF.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Aide
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Aide D-Dialog
ON CHOOSE OF Btn_Aide IN FRAME D-Dialog /* Aide */
OR HELP OF FRAME {&FRAME-NAME}
DO: /* Call Help Function (or a simple message). */
   {admvif/template/tinclude.i "Help" }
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_Annuler
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_Annuler D-Dialog
ON CHOOSE OF Btn_Annuler IN FRAME D-Dialog /* Annuler */
DO:
RUN synchronize-tt IN h_actsstjv
    ( INPUT NO).
   {admvif/template/cntnrdlg.i "Choose-Annuler"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn_OK
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn_OK D-Dialog
ON CHOOSE OF Btn_OK IN FRAME D-Dialog /* OK */
DO:
RUN synchronize-tt IN h_actsstjv
    ( INPUT YES /* LOGICAL */).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK D-Dialog 


/* ***************************  Main Block  *************************** */
{admvif/template/cntnrdlg.i "Triggers"}
{admvif/template/cntnrdlg.i "Main-block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects D-Dialog  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'actsstjv.w':U ,
             INPUT  FRAME D-Dialog:HANDLE ,
             INPUT  'Hide-Links-With-Object = No,
                     Initial-Lock = Default,
                     Dern-Champ-Go = No':U ,
             OUTPUT h_actsstjv ).
       RUN set-position IN h_actsstjv ( 3.00 , 9.60 ) NO-ERROR.
       RUN set-size IN h_actsstjv ( 9.43 , 123.80 ) NO-ERROR.

       /* Links to SmartViewerSpe h_actsstjv. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Info':U , h_actsstjv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_actsstjv ,
             Btn_OK:HANDLE , 'BEFORE':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available D-Dialog  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable-fields D-Dialog 
PROCEDURE disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*IF(Mode = "Ajouter") THEN Do With Frame {&Frame-Name}:

ASSIGN kactsst.csst:SENSITIVE = YES.
ASSIGN kactsst.priorite:SENSITIVE = YES.
ASSIGN B_AID-sst:SENSITIVE = YES.
ASSIGN Btn-supprimer:SENSITIVE = NO.
END.

IF(Mode = "Consult") THEN Do With Frame {&Frame-Name}:

ASSIGN kactsst.csst:SENSITIVE = NO.
ASSIGN kactsst.priorite:SENSITIVE = NO.
ASSIGN B_AID-sst:SENSITIVE = NO.

END.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI D-Dialog  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME D-Dialog.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE empty-fields D-Dialog 
PROCEDURE empty-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

/*Do With Frame {&Frame-Name}:
       ASSIGN kactsst.csst = "".
       ASSIGN kactsst.priorite = 000000000. 

END.

       ASSIGN i-csst = "".
       ASSIGN i-priorite = 0.
*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-fields D-Dialog 
PROCEDURE enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/*
IF(Mode = "Ajouter") THEN Do With Frame {&Frame-Name}:

ASSIGN kactsst.csst:SENSITIVE = YES.
ASSIGN kactsst.priorite:SENSITIVE = YES.
ASSIGN B_AID-sst:SENSITIVE = YES.
ASSIGN Btn-supprimer:SENSITIVE = NO.
END.

IF(Mode = "") THEN Do With Frame {&Frame-Name}:

ASSIGN kactsst.csst:SENSITIVE = YES.
ASSIGN kactsst.priorite:SENSITIVE = YES.
ASSIGN B_AID-sst:SENSITIVE = YES.

END.

*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI D-Dialog  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE Btn_OK Btn_Annuler Btn_Aide 
      WITH FRAME D-Dialog.
  VIEW FRAME D-Dialog.
  {&OPEN-BROWSERS-IN-QUERY-D-Dialog}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize D-Dialog 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  /*ASSIGN Mode = "consult".
  Run Set-Info("State=":U + "consult", "Info2-Target":U).*/
  Run Set-Info("Current-Cact=":U + i-cact, "Info-Target":U).
  
  RUN local-display-fields IN h_actsstjv.
  
 /* For each kactsst WHERE kactsst.csoc = w-csoc 
  AND kactsst.cetab = w-cetab
  AND kactsst.cact = i-cact:
   Create w-actsst.
   Buffer-copy kactsst to w-actsst.
   MESSAGE "csst copi� " + w-actsst.csst.
End.
*/
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-test-saisie D-Dialog 
PROCEDURE local-test-saisie :
/*------------------------------------------------------------------------------
  Purpose:     Tester la saisie des variables du viewer
  Parameters:  - Test Global, ou Pour un widget 
               - Handle du widget � tester.
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER iTest-Global AS LOGICAL   NO-UNDO.
DEFINE INPUT PARAMETER iWidget-Test AS HANDLE    NO-UNDO.

/*DO With Frame {&Frame-Name}:
       ASSIGN i-csst = kactsst.csst:SCREEN-VALUE.
       ASSIGN i-priorite = INTEGER(kactsst.priorite:SCREEN-VALUE). 
END.


       MESSAGE "i-csst " + i-csst + "prio " + string(i-priorite).*/
       

/* Exemple de test : Test apres la saisie de cle, en creation, si l'enreg n'existe pas d�j� */
 /*IF iTest-Global OR                                                                                                          
    iWidget-Test = w-cact.csst:HANDLE IN FRAME {&FRAME-NAME} THEN DO:                                                                                                                                                            
       CAN-FIND(FIRST w-cact WHERE w-cact.csoc = w-csoc
                            AND     w-cact.cetab = w-cetab
                            AND     w-cact.cact = w-cact                                                                          
                            AND w-cact.csst = INPUT FRAME {&FRAME-NAME} w-cact.csst NO-LOCK) THEN DO:                                                 
       {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (013125, 'Cette valeur existe d�j� (&1).':T), iWidget-Test:label )"} 
       APPLY "ENTRY":U TO kactsst.csst IN FRAME {&FRAME-NAME}.                                                                  
       RETURN "ADM-ERROR":U.                                                                                                                                                                                                                 
 END.        */                                                                                                         

/* Autre Exemple : Test libelle complementaire */
/* IF iTest-Global OR                                                    
    iWidget-Test = kactsst.csst:HANDLE IN FRAME {&FRAME-NAME} THEN DO: 
     {csstcc.i kactsst.csst YES YES ksst.lsst}                     
 END. */                                                                 

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info D-Dialog 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
      WHEN "fields-values" THEN DO:
         
         ASSIGN fields-values = valeur-info.
      
      END.

       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records D-Dialog  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartDialog, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed D-Dialog 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
DEFINE INPUT PARAMETER ihIssuer AS HANDLE    NO-UNDO.
DEFINE INPUT PARAMETER iState   AS CHARACTER NO-UNDO.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


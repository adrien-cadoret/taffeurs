&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actsstjv.w
 *
 * Description  : SmartViewer sp� pour la gestion de sous-traitance d'une activit�
 * 
 * Template     : viewspe.w - SmartViewer sp�cial 
 * Type Objet   : SmartViewerSpe 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 11/02/2016 � 08:53 par AC
 *
 * HISTORIQUE DES MODIFICATIONS :
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME ACT

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Tablejv _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/viewerd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Tablejv 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

{admvif/template/viewspe.i "Definitions"}

/* Variables globales */
{variable.i " "}
{unitebibli.i}  /* SLE 18/01/05 */

&Scoped-define Temp-Table Tt-actsst
&Scoped-define Table      kactsst

/* Definition de la table temporaire de travail (Temp-Table ou Work-Table) */
Define New Shared Temp-Table {&Temp-Table} No-Undo
  Field csst  Like kactsst.csst
  Field priorite  Like kactsst.priorite
  Field lsst Like ksst.lsst.

Define Variable Nb-Enreg As Integer No-Undo.
Define Variable Wrowid   As Rowid   No-Undo.

DEFINE VARIABLE current-cact AS CHARACTER NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" Tablejv _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewerSpe
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Target,Tabelio-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME B-Table

/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES {&Temp-Table}

/* Definitions for BROWSE B-Table                                       */
&Scoped-define FIELDS-IN-QUERY-B-Table {&Temp-Table}.csst {&Temp-Table}.lsst {&Temp-Table}.priorite   
&Scoped-define ENABLED-FIELDS-IN-QUERY-B-Table   
&Scoped-define SELF-NAME B-Table
&Scoped-define QUERY-STRING-B-Table FOR EACH {&Temp-Table}.  FOR EACH {&Temp-Table} : MESSAGE "temp " + {&Temp-Table}.csst + " " + STRING({&Temp-Table}.priorite). END.  Run Enable-Buttons
&Scoped-define OPEN-QUERY-B-Table OPEN QUERY {&Browse-Name} FOR EACH {&Temp-Table}.  FOR EACH {&Temp-Table} : MESSAGE "temp " + {&Temp-Table}.csst + " " + STRING({&Temp-Table}.priorite). END.  Run Enable-Buttons.
&Scoped-define TABLES-IN-QUERY-B-Table {&Temp-Table}
&Scoped-define FIRST-TABLE-IN-QUERY-B-Table {&Temp-Table}


/* Definitions for FRAME F-Main                                         */
&Scoped-define OPEN-BROWSERS-IN-QUERY-F-Main ~
    ~{&OPEN-QUERY-B-Table}

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS B-Table Btn-Ajouter Btn-Enlever 

/* Custom List Definitions                                              */
/* List-1,List-2,L-Oblig,L-Field,L-Majuscule,List-6                     */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Auto-Definitions" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/wrdef.p */

/* ********** Preprocessor Definitions VIF ***********/
&If Defined(List-Champs-Auto) <> 0 &Then
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&List-Champs-Auto},{&User-List-Champs}
   &Else
      &Scoped-Define List-champs {&List-Champs-Auto}
   &Endif
&Else
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&User-List-Champs}
   &Endif
&Endif

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn-Ajouter 
     LABEL "&Ajouter" 
     SIZE 13.2 BY 1.05.

DEFINE BUTTON Btn-Enlever 
     LABEL "&Enlever" 
     SIZE 13.2 BY 1.05.

/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY B-Table FOR 
      {&Temp-Table} SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE B-Table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS B-Table Tablejv _FREEFORM
  QUERY B-Table NO-LOCK DISPLAY
      {&Temp-Table}.csst FORMAT "X(10)":u COLUMN-LABEL 'Code sous-traitant':u
      {&Temp-Table}.lsst FORMAT "X(30)":u COLUMN-LABEL 'Libell�':u
      {&Temp-Table}.priorite FORMAT 99 COLUMN-LABEL 'Priorite':u
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ROW-MARKERS SEPARATORS SIZE 95.6 BY 8.19
         FONT 6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     B-Table AT ROW 2.24 COL 7
     Btn-Ajouter AT ROW 4.52 COL 111.6
     Btn-Enlever AT ROW 6.57 COL 110
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewerSpe
   Allow: Basic,Browse,DB-Fields,Query
   Frames: 1
   Add Fields to: External-Tables
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Tablejv ASSIGN
         HEIGHT             = 11.67
         WIDTH              = 156.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Tablejv 
/* ************************* Included-Libraries *********************** */

{admvif/method/viewspem.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW Tablejv
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
/* BROWSE-TAB B-Table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       B-Table:NUM-LOCKED-COLUMNS IN FRAME F-Main     = 1.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE B-Table
/* Query rebuild information for BROWSE B-Table
     _START_FREEFORM
OPEN QUERY {&Browse-Name} FOR EACH {&Temp-Table}.

FOR EACH {&Temp-Table} :
MESSAGE "temp " + {&Temp-Table}.csst + " " + STRING({&Temp-Table}.priorite).
END.

Run Enable-Buttons.
     _END_FREEFORM
     _Options          = "NO-LOCK"
     _Query            is OPENED
*/  /* BROWSE B-Table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME B-Table
&Scoped-define SELF-NAME B-Table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B-Table Tablejv
ON DEFAULT-ACTION OF B-Table IN FRAME F-Main
DO:
  If Btn-Ajouter:Sensitive In Frame {&Frame-Name} Then
     Apply "Choose":U TO Btn-Ajouter.  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-Ajouter
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-Ajouter Tablejv
ON CHOOSE OF Btn-Ajouter IN FRAME F-Main /* Ajouter */
DO:

   DEFINE VARIABLE all-temp AS CHARACTER NO-UNDO.
   
   Assign Ret = Yes.
   
   
   Do While ret :   
     Wrowid = Rowid({&Temp-Table}).
     
     FOR EACH {&Temp-Table} : 
     
     all-temp = {&Temp-Table}.csst  + "|" + all-temp.
     
     END.
     
     MESSAGE all-temp. 
     
     Create {&Temp-Table}.
     /* Dialog-box de saisie d'un enreg */
     {&Run-Prog} actsst2jd.w ( INPUT all-temp,
                            Output Ret,
                            Output {&Temp-Table}.csst,
                            Output {&Temp-Table}.lsst,
                            Output {&Temp-Table}.priorite).
                     
     If Ret Then Do :
       Nb-Enreg = Nb-Enreg + 1.
       Wrowid = Rowid({&Temp-Table}).
       {&OPEN-QUERY-{&BROWSE-NAME}}
       Reposition {&Browse-Name} To Rowid Wrowid. 
       Data-Modified = Yes.
     End.
   End.
   If Not ret Then Do:
      Delete {&Temp-Table}.
      If Wrowid <> ? Then
         Reposition {&Browse-Name} To Rowid Wrowid.
   End.
   
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-Enlever
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-Enlever Tablejv
ON CHOOSE OF Btn-Enlever IN FRAME F-Main /* Enlever */
DO:
  If Available {&Temp-Table} Then Do :
     Delete {&Temp-Table}.
     Ret = Browse {&Browse-Name}:Delete-Current-Row().
     Data-Modified = Yes.
     Nb-Enreg = Nb-Enreg - 1.
     Run Enable-Buttons.
  End.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Tablejv 


/* ***************************  Main Block  *************************** */
{admvif/template/viewspe.i "Triggers"}
{admvif/template/viewspe.i "Main-Block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects Tablejv  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available Tablejv  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Aide-Sur-Champ Tablejv 
PROCEDURE Aide-Sur-Champ :
/*------------------------------------------------------------------------------
  Purpose:     Aide (liste de choix) Sur les Champs
  Parameters:  Handle du widget active
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER iChamp AS HANDLE    NO-UNDO.

DEFINE VARIABLE vChoix AS CHARACTER NO-UNDO.

CASE iChamp:NAME:
   WHEN "champ":U THEN DO:
      {&Run-prog} "admvif/objects/a-standa.w":U
                        (INPUT "CodeFct":U,        /* Fonction pour les droits */
                         INPUT "browser":U, "":U,  /* Nom browser et attributs */
                         INPUT "viewer":U, "":U,   /* Nom viewer et attributs */
                         INPUT iChamp:LABEL,       /* Titre ecran d'aide */
                         INPUT YES,                /* Acces bouton nouveau */
                         INPUT iChamp:SCREEN-VALUE, /* Valeur de depart */
                         OUTPUT vChoix).
      IF vChoix <> ? THEN
         iChamp:SCREEN-VALUE = vChoix.
   END.
   OTHERWISE vChoix = ?.
END CASE.

IF vChoix <> ? THEN
   APPLY "TAB":U TO iChamp.
ELSE
   APPLY "ENTRY":U TO iChamp.                                                     
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Tablejv  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Enable-Buttons Tablejv 
PROCEDURE Enable-Buttons :
/*------------------------------------------------------------------------------
  Purpose:     Enable des boutons de mise � jour
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   /*Do With Frame {&Frame-Name} :
      Browse {&Browse-Name}:Max-Data-Guess = Nb-Enreg.

      Assign Btn-Ajouter:Sensitive  = Adm-Fields-Enabled
             Btn-Enlever:Sensitive  = (Adm-Fields-Enabled And 
                                       Num-Results("{&Browse-Name}":U) > 0).
   End.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Tablejv  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE B-Table Btn-Ajouter Btn-Enlever 
      WITH FRAME F-Main.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Adv-Clear-Fields Tablejv 
PROCEDURE Local-Adv-Clear-Fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       Effacement du viewer
------------------------------------------------------------------------------*/
    {&SetCurs-Wait}
    
    /* Mettre ici le code n�cessaire de l'effacement du viewer */
    EMPTY TEMP-TABLE {&Temp-Table}.
    
    ASSIGN Nb-Enreg = 0.
  /*         F-lotd:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "".*/
    
    {&Open-Query-{&Browse-Name}}
   
    
    {&SetCurs-NoWait}

   /* Remise a zero des flags de modification de saisie */
   Run Dispatch ('Adv-Clear-Modified':U).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement Tablejv 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Assign Record de la table ma�tre
  Notes:       
------------------------------------------------------------------------------*/
   /*DEFINE BUFFER b{&Table} FOR {&Table}.
   
   For Each b{&Table} Where b{&Table}.csoc  = wcsoc
                       And b{&Table}.cetab = wcetab
                       And b{&Table}.cact = current-cact
                       And  Not Can-Find(First {&Temp-Table} Where {&Temp-Table}.csst = b{&Table}.csst) 
   Exclusive-Lock :
     /* Assign b{&Table}.cresg = "".*/
   End.
   
   For Each {&Temp-Table} :
      Find b{&Table} Where b{&Table}.csoc  = wcsoc
                      And b{&Table}.cetab = wcetab
                      And b{&Table}.cres  = {&Temp-Table}.cres 
      Exclusive-Lock No-Error.
      If Available b{&Table} 
        Then Assign b{&Table}.cresg = kresg.cresg.
   End.
  
   Return.*/
   
   MESSAGE "Local assign statement du viewer".
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Disable-Fields Tablejv 
PROCEDURE Local-Disable-Fields :
/*------------------------------------------------------------------------------
  Purpose:     Disable de la saisie
  Notes:       
------------------------------------------------------------------------------*/
    
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'Disable-Fields':U ) .

  If Not Adm-Fields-Enabled Then Do :
     /* Ajouter ici les disable des widgets */
     Run Enable-Buttons.
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields Tablejv 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   {&SetCurs-Wait}
   
                  
   EMPTY TEMP-TABLE {&Temp-Table}.
      
   Nb-Enreg = 0.
   /*If Available kresg Then Do :*/
      For Each {&Table} Where {&Table}.csoc  = wcsoc
                          And {&Table}.cetab = wcetab
                          And {&Table}.cact = current-cact,
      EACH ksst OF kactsst
      No-Lock BY {&TABLE}.priorite:
         Create {&Temp-Table}.
         Buffer-Copy {&Table} To {&Temp-Table}.
                 Buffer-Copy ksst To {&Temp-Table}.
         Assign Nb-Enreg = Nb-Enreg + 1.
      End. /* For Each {&Table} */
   /*End.*/
   
   {&OPEN-QUERY-{&BROWSE-NAME}}
   
   
   {&SetCurs-NoWait}
   

   /* Remise a zero des flags de modification de saisie */
   Run Dispatch ('Adv-Clear-Modified':U).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Enable-Fields Tablejv 
PROCEDURE Local-Enable-Fields :
/*------------------------------------------------------------------------------
  Purpose:     Enable de la saisie
  Notes:       
------------------------------------------------------------------------------*/
    
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'Enable-Fields':U ) .

  If Adm-Fields-Enabled Then Do :
     /* Ajouter ici les enable des widgets desire */
     Run Enable-Buttons.
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Test-Saisie Tablejv 
PROCEDURE Local-Test-Saisie :
/*------------------------------------------------------------------------------
  Purpose:     Tester la saisie des variables du viewer
  Parameters:  - Test Global, ou Pour un widget 
               - Handle du widget � tester.
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Test-Global As Logical       No-Undo.
    Define Input Parameter Widget-Test As Widget-Handle No-Undo.


    
    Return.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-view Tablejv 
PROCEDURE local-view :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'view':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  /* Pour assurer la barre de scroll sur le browse */
  If Num-Results("{&Browse-Name}":U) >= 1 Then
     Ret = Browse {&Browse-Name}:Select-Focused-Row().
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE PrAncrageWidget Tablejv 
PROCEDURE PrAncrageWidget :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* DO WITH FRAME {&FRAME-NAME}:                                             */
/*    {admvif/method/objresizerun.i &I-Proc="&AttachObjects"                */
/*                                  &I-Hansle="widget:handle"               */
/*                                  &I-Attach="'horiz�vert�height�width':U" */
/*                                  &I-Height-Resizable="YES/NO"            */
/*                                  &I-Width-Resizable="YES/NO"             */
/*                                  &I-Proc-Resize="proc"                   */
/*                                  &I-GroupName="'nomgroupe':U"}           */
/* END.                                                                     */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info Tablejv 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
      WHEN "Current-Cact" THEN DO:
         current-cact = valeur-info.
      END.

       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records Tablejv  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "{&Temp-Table}"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed Tablejv 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.
  
   CASE p-state:
        {admvif/template/viewspe.i "vstates"}
   END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE synchronize-tt Tablejv 
PROCEDURE synchronize-tt :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER inclusion AS LOGICAL.

DEFINE BUFFER b-kactsst FOR kactsst.
/*DEFINE BUFFER b-tt-kactsst FOR {&Temp-Table}.*/

IF(inclusion) THEN DO:

/* ACA Suppression des enregistrements de la table temp qui ont �t� supprim�s */
   For Each b-kactsst WHERE b-kactsst.cact = current-cact AND 
                        Not Can-Find (First {&Temp-Table}
                                       Where {&Temp-Table}.csst = b-kactsst.csst
                                         No-Lock)
                   Exclusive-Lock :
     Delete b-kactsst.
   End.
   
   MESSAGE "Fin de la suppression des enregistrements".
   
   FOR EACH b-kactsst Where b-kactsst.cact = current-cact:
      MESSAGE "dan b-kactsst, j'ai " + b-kactsst.csst.
   END.

/* ACA  Ajout des nouveaux enregistrements */ 
   For each {&Temp-Table} No-lock :
        MESSAGE "Je suis sur la temp de " + {&Temp-Table}.csst.
        Find b-kactsst Where b-kactsst.csst = {&Temp-Table}.csst
                       AND b-kactsst.cact = current-cact
                   Exclusive-Lock No-Error No-Wait.
        If Not Available b-kactsst THEN DO :           
            MESSAGE "Je n'ai pas trouv�  dans b-kactsst" + {&Temp-Table}.csst.
            Create b-kactsst.
            Assign b-kactsst.csoc = wcsoc.
            Assign b-kactsst.cetab = wcetab.
            Assign b-kactsst.cact = current-cact.
            Buffer-Copy {&Temp-Table} To b-kactsst.
        END.
       
    End.
  Release {&Temp-Table}.

   MESSAGE "Fin de la ajout des enregistrements".

END.


ELSE DO:
MESSAGE "elever".
END.

/* messaging tmp*/  
For Each b-kactsst WHERE b-kactsst.cact = current-cact :
MESSAGE "temp " + b-kactsst.csoc + " " +  b-kactsst.cetab + " " + b-kactsst.cact + " " + b-kactsst.csst + " " + STRING(b-kactsst.priorite).
END.



END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


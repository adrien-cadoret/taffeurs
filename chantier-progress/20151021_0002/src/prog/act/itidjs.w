&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : SVIF4 - VIF5_7                          
 * Programme    : fab\itidjs.w
 *
 * Description  : SmartSousFct sur Itin�raire par date d'effet
 * 
 * Template     : sousfct.w - SmartSous-Fonction  
 * Type Objet   : SmartSousFct 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 01/08/1996 � 00:00 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 14/10/2004 - Mouli   : V9 Ajout ligne Container links
 *  - Le 08/04/2003 - SLE     : Rectangle autour viewer et non tout simple
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" F-Frame-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/standard.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS F-Frame-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

/* Boite de dialogue pour les attributs */

/* Parameters Definitions ---                                           */

&Scoped-Define TitreFrame "Dates d'effet de l'itin�raire":T

/* Constantes propres � la fonction */
&Scoped-Define Nombre-Pages 0

/* N� Contexte Aide en ligne */

/* Variables globales */
{variable.i " "}

/* Local Variable Definitions ---                                       */
{admvif\template\sousfct.i "Definitions"}

/* Sous-Fonction de saisie fiche (dont le browse est sur la fct appelante) */
&Global-Define Gestion-Saisie-Fiche No
/* Faut-il garder en memoire la sous-fonction apres le 1er appel ? */
/* Ne modifier que si gestion-saisie-fiche = no */
&Global-Define Garder-En-Memoire Yes

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" F-Frame-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartSousFct

&Scoped-define ADM-CONTAINER FRAME

&Scoped-define ADM-SUPPORTED-LINKS Record-Target

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-SousFct

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kiti
&Scoped-define FIRST-EXTERNAL-TABLE kiti


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kiti.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS Rect-Viewer 

/* Custom List Definitions                                              */
/* Btn-SF,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* ***********************  Control Definitions  ********************** */


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_itidjb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_itidjv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-updsav AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE RECTANGLE Rect-Viewer
     EDGE-PIXELS 2 GRAPHIC-EDGE  
     SIZE 56.6 BY 3.67
     BGCOLOR 31 .


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-SousFct
     Rect-Viewer AT ROW 10.57 COL 54.2
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 159.2 BY 15.9
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartSousFct
   External Tables: vif5_7.kiti
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Container Links: Page-Target,Record-Source,Record-Target,Navigation-Source,Navigation-Target,TableIO-Source,TableIO-Target
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT."
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW F-Frame-Win ASSIGN
         HEIGHT             = 16.33
         WIDTH              = 160.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME


/* ***************  Runtime Attributes and UIB Settings  ************** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW F-Frame-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-SousFct
   NOT-VISIBLE                                                          */
ASSIGN 
       FRAME F-SousFct:HIDDEN           = TRUE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-SousFct
/* Query rebuild information for FRAME F-SousFct
     _Options          = ""
     _Query            is NOT OPENED
*/  /* FRAME F-SousFct */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" F-Frame-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB F-Frame-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_sfct.i}
{src/adm/method/containr.i}
{admvif/method/ap_sfct.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-SousFct
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-SousFct F-Frame-Win
ON HELP OF FRAME F-SousFct
DO:
  /* Gestion standard de l'aide en ligne */
  {admvif/template/tinclude.i "Help"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK F-Frame-Win 


{admvif\template\sousfct.i "Triggers"}

{admvif\template\sousfct.i "Main-Block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects F-Frame-Win _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'itidjb.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ':U ,
             OUTPUT h_itidjb ).
       RUN set-position IN h_itidjb ( 1.52 , 70.40 ) NO-ERROR.
       /* Size in UIB:  ( 8.19 , 20.20 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/p-updsav.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'SmartPanelType = Save,
                     Edge-Pixels = 2,
                     SaveFunction = AddMultiple,
                     NameBtnAction = ,
                     Hide-Save = no,
                     Hide-Add = no,
                     Hide-Copy = no,
                     Hide-Delete = no,
                     Hide-cancel = no,
                     Hide-Print = Yes,
                     PrintFunction = Normale,
                     Layout-Options = "Master Layout,Standard Character",
                     Default-Layout = Master Layout':U ,
             OUTPUT h_p-updsav ).
       RUN set-position IN h_p-updsav ( 1.57 , 50.20 ) NO-ERROR.
       /* Size in UIB:  ( 1.86 , 8.20 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'itidjv.w':U ,
             INPUT  FRAME F-SousFct:HANDLE ,
             INPUT  'Initial-Lock = NO-LOCK,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_itidjv ).
       RUN set-position IN h_itidjv ( 11.24 , 58.60 ) NO-ERROR.
       /* Size in UIB:  ( 2.57 , 50.20 ) */

       /* Links to SmartBrowser h_itidjb. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Record':U , h_itidjb ).

       /* Links to SmartPanel h_p-updsav. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Panel':U , h_p-updsav ).

       /* Links to SmartViewer h_itidjv. */
       RUN add-link IN adm-broker-hdl ( h_itidjb , 'Record':U , h_itidjv ).
       RUN add-link IN adm-broker-hdl ( h_p-updsav , 'TableIO':U , h_itidjv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_p-updsav ,
             h_itidjb , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_itidjv ,
             h_p-updsav , 'AFTER':U ).
    END. /* Page 0 */

  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available F-Frame-Win _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kiti"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kiti"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI F-Frame-Win _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-SousFct.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI F-Frame-Win _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE Rect-Viewer 
      WITH FRAME F-SousFct.
  {&OPEN-BROWSERS-IN-QUERY-F-SousFct}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE envoi-donnees F-Frame-Win 
PROCEDURE envoi-donnees :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
define output parameter w-rowid as rowid   no-undo.

define variable w-list-rowid    as character format "x(20)" no-undo.

    run send-records in h_itidjb ("kitid":U, output w-list-rowid).
    w-rowid = to-rowid(w-list-rowid).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-close F-Frame-Win 
PROCEDURE local-adv-close :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-close':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  if return-value <> "adm-error":U then 
    Run new-State('fin-dates-effet,container-source':U).
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available F-Frame-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   define variable w-rowid-kitid as rowid no-undo.

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
   if return-value <> "adm-error":U then do:
    If Valid-Handle(Fonction-Source-Hdl) Then Do :
       run Envoi-donnees in Fonction-Source-Hdl (output w-rowid-kitid) no-error.
       if w-rowid-kitid <> ? then
           run reposition-browse in h_itidjb (input w-rowid-kitid).
    End.
  end.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-view F-Frame-Win 
PROCEDURE local-view :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  Run Get-Info ("ModeFct":U, "Container-Source":U).
  Run Set-Link-Attribute In Adm-Broker-Hdl (This-Procedure,'Panel-Target':U,
                         'Mode-Consult=':U + (If Return-Value = "Consult":U Then "Oui":U Else "Non":U)).
  Run New-State ("Reinit-Panel,Panel-Target":U).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'view':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records F-Frame-Win _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kiti"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed F-Frame-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Placez vos cases ici pour remplacez les states standard ou
         en mettre des nouveaux */
         
       /* Cas standard des sous-fonctions */
      {admvif/template/sousfct.i "sfctstates"}
               
      /* Cases standard des container */
      {admvif/template/cinclude.i "cstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



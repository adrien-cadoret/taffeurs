&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" B-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actdjb.w
 *
 * Description  : SmartBrowser sur Activit� par date d'effet
 * 
 * Template     : BROWSER.W - SmartBrowser basique 
 * Type Objet   : SmartBrowser 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 01/08/1996 � 00:00 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 20/01/2015 - MB      : DP001776:Bug en cr�ation d'activit�
 *                              20150119_0017 (26543)|DP001776|Bug en cr�ation d'activit�|MB|20/01/15 17:37:06 
 *                               
 *  - Le 22/12/2014 - MB      : DP001776:Activit�s : dates d'effet "historiques" � ignorer
 *                              20140623_0008 (25599)|DP001776|Activit�s : dates d'effet "historiques" � ignorer|MB|22/12/14 09:36:11 
 *                               
 *  - Le 19/03/2014 - NLE     : M146531:Anomalie lors de la cr�ation d'une activit� - Date...
 *                              20140318_0004 (25037)|M146531|Anomalie lors de la cr�ation d'une activit� - Date d'effet|NLE|19/03/14 09:47:01 
 *                               
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" B-table-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/browserd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/



/* Parameters Definitions ---                                           */

/* Variables globales */
{variable.i " "}


/* constantes de pre-compilation */
&GLOBAL-DEFINE table kactd
&GLOBAL-DEFINE ltable Activit� par date d'effet

/* Variable utilis�es pour le template des browser */
{admvif/template/binclude.i "definitions"}

/* Local Variable Definitions ---                                       */

DEFINE VARIABLE vgtDatefHisto AS LOGICAL   NO-UNDO. /* MB DP001776 */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" B-table-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kact
&Scoped-define FIRST-EXTERNAL-TABLE kact


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kact.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES kactd

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table kactd.datef 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table 
&Scoped-define QUERY-STRING-br_table FOR EACH kactd OF kact NO-LOCK ~
    BY kactd.datef DESCENDING
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH kactd OF kact NO-LOCK ~
    BY kactd.datef DESCENDING.
&Scoped-define TABLES-IN-QUERY-br_table kactd
&Scoped-define FIRST-TABLE-IN-QUERY-br_table kactd


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Browse-TTY" B-table-Win _INLINE
/* Actions: ? admvif/xftr/ettybr.w ? ? ? */
/********************** TAILLE DU BROWSE EN TTY **********/
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Complement-Display-Browse" B-table-Win _INLINE
/* Actions: ? admvif/xftr/ecompbr.w ? ? admvif/xftr/wcompbr.p */
/********************** COMPLEMENT DISPLAY BROWSE **********/
/* Automatique=Oui*/
/* Pas de compl�ment-display-browse */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-br_table 
       MENU-ITEM m_Recherche-Code LABEL "&Recherche par la date d'effet".


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      kactd SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      kactd.datef COLUMN-LABEL "Date d'effet      ." FORMAT "99/99/9999":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 20.8 BY 4.14
         FONT 6 FIT-LAST-COLUMN.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 21 RIGHT-ALIGNED
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   External Tables: vif5_7.kact
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 4.52
         WIDTH              = 21.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_brows.i}
{src/adm/method/browser.i}
{admvif/method/ap_brows.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR BROWSE br_table IN FRAME F-Main
   ALIGN-R                                                              */
ASSIGN 
       br_table:POPUP-MENU IN FRAME F-Main             = MENU POPUP-MENU-br_table:HANDLE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "vif5_7.kactd OF vif5_7.kact"
     _Options          = "NO-LOCK"
     _OrdList          = "vif5_7.kactd.datef|no"
     _FldNameList[1]   > vif5_7.kactd.datef
"kactd.datef" "Date d'effet      ." ? "date" ? ? ? ? ? ? no ? no no ? yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" B-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON MOUSE-SELECT-DBLCLICK OF br_table IN FRAME F-Main
DO:
  /* code standard pour le double-click, en cas de browse d'aide */
  {admvif/template/binclude.i "brsdbclick"}
  
  /* Pour faire comme si on appuyait sur le bouton date d'effet */
  Run new-state ("DBLCLICK-ACTDJD,Container-source":u).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {admvif/template/binclude.i "brsentry"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
  {admvif/template/binclude.i "brsleave"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {admvif/template/binclude.i "brschnge"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Recherche-Code
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Recherche-Code B-table-Win
ON CHOOSE OF MENU-ITEM m_Recherche-Code /* Recherche par la date d'effet */
DO:
  /* Recherche standard par le code */
  RUN dispatch IN THIS-PROCEDURE ('Adv-Recherche-Code':U).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */
/*** Triggers Generaux ***/
{admvif/template/binclude.i "triggers"}

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

/* Main-block standard browser */
{admvif/template/binclude.i "main-block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kact"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kact"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-reopen-query B-table-Win 
PROCEDURE local-adv-reopen-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-reopen-query':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  if num-results("{&BROWSE-NAME}":U) = 0 
    then Run Set-Info('Datef-Available=no':U,'container-source':U).
    else Run Set-Info('Datef-Available=yes':U,'container-source':U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize B-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  run get-attribute("uib-mode":U).
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query B-table-Win 
PROCEDURE local-open-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-reopen-query':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If num-results("{&BROWSE-NAME}":U) = 0 Or 
     num-results("{&BROWSE-NAME}":U) = ? 
  Then Do :
     Run Set-Info('Datef-Available=no':U,'container-source':U).
     Run dispatch ('Row-Changed':U).    /* Pour avertir les record-target */
  End.
  Else Do :
     Run Set-Info('Datef-Available=yes':U,'container-source':U).
     Run Get-Info("F-datref":U, "Container-Source":U).
     Find Last kactd Where kactd.csoc = wcsoc 
                          and kactd.cetab = wcetab
                          and kactd.cact = kact.cact
                          And kactd.datef <= date(Return-Value)
                        No-Lock No-Error.

     If Not Available kactd Then
        Apply "End":u To Browse {&Browse-Name}.
     Run Reposition-Browse(Rowid(kactd)).

  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-available B-table-Win 
PROCEDURE local-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-available':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  /* KL M146531 : Report correction appel 146581*/
  IF AVAILABLE {&TABLE} THEN DO:
      RUN set-info('CactDate=' + {&TABLE}.cact + '|' + STRING({&TABLE}.datef) ,'SpeKit-Target':U).
  END.
  ELSE DO:
      RUN set-info('CactDate=' + "":U + '|' + STRING(TODAY) ,'SpeKit-Target':U).
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-row-changed B-table-Win 
PROCEDURE local-row-changed :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE VARIABLE vDatefEnCours AS DATE      NO-UNDO. /* MB DP001776 */
   DEFINE BUFFER bKactd FOR kactd.
/* -------------------------------------------------------------------------- */

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'row-changed':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  /* Informons le container qu'il doit rafraichir ses infos ToolTip des boutons Op�rations etc... */
  /* SLe 03/04/2012 */
  RUN Set-Info("Cle-Datef=":U + IF AVAILABLE kactd THEN kactd.cact + ",":u + STRING(kactd.datef) ELSE "","Info-Target":u).
  
  
  /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
  /* Recherche de la date d'effet en vigueur (seulement en modification d'activit�) */
  IF AVAILABLE {&TABLE} THEN DO:    
      FOR LAST bKactd FIELDS (datef)
                      WHERE bKactd.csoc  = wcSoc
                        AND bKactd.cetab = wcEtab
                        AND bKactd.cact  = {&TABLE}.cact
                        AND bKactd.datef <= TODAY  
                      NO-LOCK:
         ASSIGN vDatefEnCours = bKactd.datef.
      END.
      /* Si la date d'effet courante est ant�rieure � la date en vigueur */
      /* alors mise � jour du t�moin qui sera r�cup�r� dans le local-view */
      /* des sous-fonctions appel� par les boutons */
      IF {&TABLE}.datef < vDatefEncours THEN
         ASSIGN vgtDatefHisto = TRUE.
      ELSE 
         ASSIGN vgtDatefHisto = FALSE.   
      
      RUN Set-Info("DatefHisto=" + IF vgtDatefHisto THEN "oui":U ELSE "non":U,"Container-Source":U).
  END.    
  /* Fin DP001776 */
      
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Position-Record B-table-Win 
PROCEDURE Position-Record :
/*------------------------------------------------------------------------------
  Purpose:     Postionnement du browse en fonction d'un cle d'entree
  Parameters:  Cle d'entree (List Progress, avec comme separteur '|').
  Notes:       
------------------------------------------------------------------------------*/
   Define Input Parameter CleEntree As Character No-Undo.

   {admvif/template/binclude.i "Position-Record" "CleEntree" "{&Table}"

                               " {&Table}.csoc = wcsoc 
                         and {&Table}.cetab = wcetab
                         and {&Table}.cact = kact.cact
                         And {&Table}.datef >= date(Entry(1,CleEntree,'|':U))
"}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE reaffiche-browse B-table-Win 
PROCEDURE reaffiche-browse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Ret = Browse {&Browse-Name}:Refresh().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info B-table-Win 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.
   
   Case Name-Info :
       /* Ins�rer ici vos propres infos � recevoir */
       When "Color-Browse":U Then 
          Browse {&Browse-Name}:Bgcolor = Integer(Valeur-Info).
       
       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Cle-Record B-table-Win 
PROCEDURE Send-Cle-Record :
/*------------------------------------------------------------------------------
  Purpose:     Renvoie la cle du record courant en une liste Progress
               separe par '|'
  Parameters:  <none>
  Notes:       Cette Procedure est surtout utilise quand le browse sert d'aide a
               la saisie.
------------------------------------------------------------------------------*/
     Define Output Parameter CleRecord As Character No-Undo.
     
     If Available {&Table} Then Do :
        CleRecord = string({&Table}.datef,"99/99/9999").
        Return.
     End.
     
     CleRecord = ?.
     Return.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info B-table-Win 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
       /* Ins�rer ici vos propres infos � envoyer */
       When "cle-com":U Then
          If Available kactd Then
             Valeur-Info = kactd.cact + ',':U
                         + String(kactd.datef, '99/99/9999':U).
          Else
             Valeur-Info = "":U.

       /* SLE 17/01/05 on garde en m�moire la datef sur laquelle on est positionn�,
        * pour bien s'y replacer au terme du modify */
       WHEN "datefModify":u THEN
          If Available kactd Then
             Valeur-Info = String(kactd.datef, '99/99/9999':U).
          Else
             Valeur-Info = "":U.

       Otherwise 
          /* Par d�faut, envoi de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Send-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kact"}
  {src/adm/template/snd-list.i "kactd"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
     {admvif/template/binclude.i "bstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Traiter-Contexte B-table-Win 
PROCEDURE Traiter-Contexte :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  Define Output Parameter w-val As character No-undo.

  Run Get-Contexte In Adv-Outils-Hdl ("kact.dtef":U, Output w-val).  

  If w-val <> "":U Then Do:
     Run Position-Record (INPUT w-val).
     Run Set-Contexte In Adv-Outils-Hdl (This-Procedure,"kact.dtef":U, "":U).
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" V-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actajv.w
 *
 * Description  : SmartViewer sur Entrants,sortants,art. de co�t
 * 
 * Template     : VIEWER.W - SmartViewer basique 
 * Type Objet   : SmartViewer 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 01/08/1996 � 00:00 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 05/12/2014 - CD      : D001150:Activit�s : fonctions connexes
 *                              20141006_0014 (26015)|D001150|Activit�s : fonctions connexes|CD|05/12/14 12:12:38 
 *                               
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" V-table-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/viewerd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/



/* Parameters Definitions ---                                           */

/* Variables globales */
{variable.i " "}
{unitebibli.i}

{cribibsli.i}
{crivi.i}
{infocribibli.i}
/* MB 20131112_0001 P2374 */
{cribibfpli.i}

/* constantes de pre-compilation */
&GLOBAL-DEFINE table kacta
&GLOBAL-DEFINE ltable

/* Variable pour template viewer */
{admvif/template/vinclude.i "Definitions"}

/* Local Variable Definitions ---                                       */
define variable w-typacta     AS CHARACTER no-undo.
Define Variable w-typart      AS CHARACTER No-undo.
Define Variable vgtCacherFreinte As Logical   No-undo. /* plus causant que W-T-Freinte */
Define Variable W-T-Duree     As Logical   No-undo.
Define Variable W-T-2emeUnite As Logical   No-undo.
/* Define Variable w-cout-prefixe As Character No-Undo. /* SLe 22/12/2010 D000662 Distinction CTART pr�fix� et coeff de prix */ */
/* Define Variable w-cout-coef    As Character No-Undo.                                                                         */
Define Variable w-unit1 As Character No-Undo.
Define Variable w-unit2 As Character No-Undo.

Define Buffer b-Kacto For kacto.

DEFINE VARIABLE vBlockAddRecord AS LOGICAL   NO-UNDO.

/* MB 20131112_0001 P2374 */
DEFINE VARIABLE vgcCriRdtDyn AS CHARACTER NO-UNDO.

DEFINE VARIABLE vgMemCart AS CHARACTER NO-UNDO. /* CD Q7432 20140910_0001 bloquer en mode cr�ation */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" V-table-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define LAYOUT-VARIABLE CURRENT-WINDOW-layout

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kacta kactd
&Scoped-define FIRST-EXTERNAL-TABLE kacta


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kacta, kactd.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS kacta.qte kacta.cunite kacta.qte2 kacta.cu2 ~
kacta.tqconst kacta.tauto kacta.cdepot kacta.trdtm kacta.valcridyn ~
kacta.trdtdyn kacta.teprod kacta.toartgen 
&Scoped-define ENABLED-TABLES kacta
&Scoped-define FIRST-ENABLED-TABLE kacta
&Scoped-Define ENABLED-OBJECTS B_AID- B_AID-7 B_AID-2 B_AID-4 B_AID-cemp ~
F-cuCriDyn 
&Scoped-Define DISPLAYED-FIELDS kacta.qte kacta.cunite kacta.qte2 kacta.cu2 ~
kacta.tligfr kacta.txfr kacta.tqconst kacta.tauto kacta.cdepot kacta.cemp ~
kacta.trdtm kacta.valcridyn kacta.trdtdyn kacta.teprod kacta.toartgen 
&Scoped-define DISPLAYED-TABLES kacta
&Scoped-define FIRST-DISPLAYED-TABLE kacta
&Scoped-Define DISPLAYED-OBJECTS F-cartvu F-cuCriDyn F-CritTen 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,L-Oblig,L-Field,L-Majuscule,List-6 */
&Scoped-define ADM-ASSIGN-FIELDS kacta.tqtedif kacta.trdtdyn 
&Scoped-define L-Oblig F-cartvu 
&Scoped-define L-Field F-cartvu kacta.tligfr kacta.txfr kacta.cemp ~
kacta.toartgen 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Auto-Definitions" V-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wrdef.p */

/* ********** Preprocessor Definitions VIF ***********/
&Scoped-Define Trait-External-Tables ~{&TP1} kacta ~{&TP2} ~
                                     ~{&TP1} kactd ~{&TP2} 

&Scoped-Define List-Champs-Auto F-cartvu,kacta.qte,kacta.cunite,kacta.qte2,kacta.cu2,kacta.tligfr,kacta.txfr,kacta.tqconst,kacta.tauto,kacta.cdepot,kacta.cemp,kacta.trdtm,kacta.valcridyn,F-cuCriDyn,kacta.trdtdyn,kacta.teprod,kacta.toartgen,F-CritTen
&If Defined(List-Champs-Auto) <> 0 &Then
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&List-Champs-Auto},{&User-List-Champs}
   &Else
      &Scoped-Define List-champs {&List-Champs-Auto}
   &Endif
&Else
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&User-List-Champs}
   &Endif
&Endif

&Scoped-Define Special-Pairs  ~
       ~{&SP1}tligfr~{&SP2}tligfr~{&SP3}tligfr~{&SP4} ~
       ~{&SP1}tqconst~{&SP2}tqconst~{&SP3}tqconst~{&SP4} ~
       ~{&SP1}tauto~{&SP2}tauto~{&SP3}tauto~{&SP4} ~
       ~{&SP1}trdtm~{&SP2}trdtm~{&SP3}trdtm~{&SP4} ~
       ~{&SP1}trdtdyn~{&SP2}trdtdyn~{&SP3}trdtdyn~{&SP4} ~
       ~{&SP1}teprod~{&SP2}teprod~{&SP3}teprod~{&SP4} ~
       ~{&SP1}toartgen~{&SP2}toartgen~{&SP3}toartgen~{&SP4}
&Scoped-Define FIELD-PAIRS~
 ~{&FP1}qte ~{&FP2}qte ~{&FP3}~
 ~{&FP1}cunite ~{&FP2}cunite ~{&FP3}~
 ~{&FP1}qte2 ~{&FP2}qte2 ~{&FP3}~
 ~{&FP1}cu2 ~{&FP2}cu2 ~{&FP3}~
 ~{&FP1}cdepot ~{&FP2}cdepot ~{&FP3}~
 ~{&FP1}valcridyn ~{&FP2}valcridyn ~{&FP3}
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Article-de-reference V-table-Win 
FUNCTION Article-de-reference RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* Define a variable to store the name of the active layout.            */
DEFINE VAR CURRENT-WINDOW-layout AS CHAR INITIAL "Master Layout":U NO-UNDO.

/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON B_AID- 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-2 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-4 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-7 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-cemp 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "" 
     SIZE 3.6 BY 1.05.

DEFINE VARIABLE F-% AS CHARACTER FORMAT "X(1)":U INITIAL "%" 
      VIEW-AS TEXT 
     SIZE 2 BY 1 NO-UNDO.

DEFINE VARIABLE F-cartvu AS CHARACTER FORMAT "X(15)" 
     LABEL "Article" 
     VIEW-AS FILL-IN 
     SIZE 31 BY 1 NO-UNDO.

DEFINE VARIABLE F-CritTen AS CHARACTER FORMAT "X(8)":U 
      VIEW-AS TEXT 
     SIZE 16 BY .62 NO-UNDO.

DEFINE VARIABLE F-cuCriDyn AS CHARACTER FORMAT "X(3)":U 
     VIEW-AS FILL-IN 
     SIZE 8.8 BY 1 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     F-cartvu AT ROW 1 COL 29.6 COLON-ALIGNED
     B_AID- AT ROW 1 COL 62.6
     zart.lart AT ROW 1 COL 64 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 52.8 BY 1
     zart.rart AT ROW 1 COL 116.6 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 26 BY 1
     kacta.qte AT ROW 2 COL 29.6 COLON-ALIGNED FORMAT "-z,zzz,zz9.9999"
          VIEW-AS FILL-IN 
          SIZE 23 BY 1
          FONT 8
     kacta.cunite AT ROW 2 COL 52.6 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     B_AID-7 AT ROW 2 COL 62.6
     kacta.qte2 AT ROW 2 COL 68.4 COLON-ALIGNED NO-LABEL FORMAT "-z,zzz,zz9.9999"
          VIEW-AS FILL-IN 
          SIZE 23 BY 1
          FONT 8
     kacta.cu2 AT ROW 2 COL 91.4 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 8 BY 1
     B_AID-2 AT ROW 2 COL 101.4
     kacta.tligfr AT ROW 2 COL 116.6
          LABEL "Ligne de freinte"
          VIEW-AS TOGGLE-BOX
          SIZE 23.6 BY .81
     kacta.txfr AT ROW 3 COL 29.6 COLON-ALIGNED FORMAT "z9.99"
          VIEW-AS FILL-IN 
          SIZE 8.4 BY 1
          FONT 8
     kacta.tqconst AT ROW 3.14 COL 97.6
          LABEL "Quantit� fixe"
          VIEW-AS TOGGLE-BOX
          SIZE 21.6 BY .81
     kacta.tauto AT ROW 3.95 COL 97.6
          LABEL "Mouvement de stock automatique"
          VIEW-AS TOGGLE-BOX
          SIZE 36 BY .81
     kacta.cdepot AT ROW 4.05 COL 29.6 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 19.2 BY 1
     B_AID-4 AT ROW 4.05 COL 50.8
     zdepot.ldepot AT ROW 4.05 COL 52.4 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 42.2 BY 1
     kacta.tqtedif AT ROW 4.76 COL 102.2
          VIEW-AS TOGGLE-BOX
          SIZE 38.8 BY .81
     kacta.cemp AT ROW 5.1 COL 29.6 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 19.2 BY 1
     B_AID-cemp AT ROW 5.1 COL 50.8
     zemp.lemp AT ROW 5.1 COL 52.4 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 42.2 BY 1
     kacta.trdtm AT ROW 5.76 COL 97.6
          VIEW-AS TOGGLE-BOX
          SIZE 37 BY .81
     kacta.valcridyn AT ROW 6.33 COL 69 COLON-ALIGNED NO-LABEL FORMAT "zzz,zz9.99999":U
          VIEW-AS FILL-IN 
          SIZE 16.6 BY 1
          FONT 8
     F-cuCriDyn AT ROW 6.33 COL 85.8 COLON-ALIGNED NO-LABEL
     kacta.trdtdyn AT ROW 6.48 COL 48.4 RIGHT-ALIGNED
          VIEW-AS TOGGLE-BOX
          SIZE 47.4 BY .81
     kacta.teprod AT ROW 6.57 COL 97.6
          VIEW-AS TOGGLE-BOX
          SIZE 41 BY .81
     kacta.toartgen AT ROW 7.38 COL 97.6
          VIEW-AS TOGGLE-BOX
          SIZE 34.2 BY .81
     F-% AT ROW 3 COL 40.8 NO-LABEL
     F-CritTen AT ROW 6.52 COL 69.6 RIGHT-ALIGNED NO-LABEL
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: vif5_7.kacta,vif5_7.kactd
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 7.62
         WIDTH              = 144.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_viewe.i}
{src/adm/method/viewer.i}
{admvif/method/ap_viewe.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       B_AID-:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-2:HIDDEN IN FRAME F-Main           = TRUE
       B_AID-2:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-4:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-7:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-cemp:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

/* SETTINGS FOR FILL-IN kacta.cemp IN FRAME F-Main
   NO-ENABLE 4                                                          */
ASSIGN 
       kacta.cu2:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN kacta.cunite IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN F-% IN FRAME F-Main
   NO-DISPLAY NO-ENABLE ALIGN-L                                         */
ASSIGN 
       F-%:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN F-cartvu IN FRAME F-Main
   NO-ENABLE 3 4                                                        */
/* SETTINGS FOR FILL-IN F-CritTen IN FRAME F-Main
   NO-ENABLE ALIGN-R                                                    */
ASSIGN 
       F-CritTen:READ-ONLY IN FRAME F-Main        = TRUE.

ASSIGN 
       F-cuCriDyn:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN zart.lart IN FRAME F-Main
   NO-DISPLAY NO-ENABLE EXP-LABEL                                       */
/* SETTINGS FOR FILL-IN zdepot.ldepot IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN zemp.lemp IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN kacta.qte IN FRAME F-Main
   EXP-FORMAT                                                           */
/* SETTINGS FOR FILL-IN kacta.qte2 IN FRAME F-Main
   EXP-FORMAT                                                           */
ASSIGN 
       kacta.qte2:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN zart.rart IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR TOGGLE-BOX kacta.tauto IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX kacta.tligfr IN FRAME F-Main
   NO-ENABLE 4 EXP-LABEL                                                */
/* SETTINGS FOR TOGGLE-BOX kacta.toartgen IN FRAME F-Main
   4                                                                    */
/* SETTINGS FOR TOGGLE-BOX kacta.tqconst IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR TOGGLE-BOX kacta.tqtedif IN FRAME F-Main
   NO-DISPLAY NO-ENABLE 2                                               */
/* SETTINGS FOR TOGGLE-BOX kacta.trdtdyn IN FRAME F-Main
   ALIGN-R 2                                                            */
/* SETTINGS FOR FILL-IN kacta.txfr IN FRAME F-Main
   NO-ENABLE 4 EXP-FORMAT                                               */
ASSIGN 
       kacta.txfr:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN kacta.valcridyn IN FRAME F-Main
   EXP-FORMAT                                                           */
ASSIGN 
       kacta.valcridyn:HIDDEN IN FRAME F-Main           = TRUE.


/* _MULTI-LAYOUT-RUN-TIME-ADJUSTMENTS */

/* LAYOUT-NAME: "cout"
   LAYOUT-TYPE: GUI
   EXPRESSION:  
   COMMENT:     
                                                                        */
RUN set-attribute-list ('Layout-Options="Master Layout,cout"':U).

/* END-OF-LAYOUT-DEFINITIONS */

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" V-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME B_AID-
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID- V-table-Win
ON CHOOSE OF B_AID- IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID- V-table-Win
ON ENTRY OF B_AID- IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-2 V-table-Win
ON CHOOSE OF B_AID-2 IN FRAME F-Main /* ? */
DO :
  /*ajout cle le 09/09/2003 */
   Assign kacta.qte2:Format = {&unite-Format-qte} (Input kacta.cu2, Input "z,zzz,zz":U)
          kacta.qte2:Screen-Value="0".  
  
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-2 V-table-Win
ON ENTRY OF B_AID-2 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-4
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-4 V-table-Win
ON CHOOSE OF B_AID-4 IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-4 V-table-Win
ON ENTRY OF B_AID-4 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-7 V-table-Win
ON CHOOSE OF B_AID-7 IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-7 V-table-Win
ON ENTRY OF B_AID-7 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-cemp
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-cemp V-table-Win
ON CHOOSE OF B_AID-cemp IN FRAME F-Main
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-cemp V-table-Win
ON ENTRY OF B_AID-cemp IN FRAME F-Main
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME F-cartvu
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-cartvu V-table-Win
ON LEAVE OF F-cartvu IN FRAME F-Main /* Article */
DO:
  
    /* NLE 12/04/2002 : ajout de */
    /*If W-T-2emeUnite And F-cartvu:screen-value <> '' Then do :
 *        /* Affichage des zones concernant la 2�me unit� si l'article a 
 *        * au moins une unit� � coefficient variable 
 *        */
 *        Run GestionAffichage2emeUnite(F-cartvu:screen-value).
 *     end.
 *     Else 
 *       Run Affichage2emeUnite(No).*/

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME kacta.tauto
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL kacta.tauto V-table-Win
ON VALUE-CHANGED OF kacta.tauto IN FRAME F-Main /* Mouvement de stock automatique */
DO:
   Run enabled-fields.
   If {&Table}.tauto:checked Then {&Table}.teprod:checked = False.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME kacta.tligfr
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL kacta.tligfr V-table-Win
ON VALUE-CHANGED OF kacta.tligfr IN FRAME F-Main /* Ligne de freinte */
DO:

/* DEFINE BUFFER bKact FOR kact.*/
  
  Run Enabled-Fields.
  
/* MB D002845 (II) 19/11/07 - inutile avec mise en place proc. Pr-Gestion-Freinte */
/* If {&Table}.tligfr:Checked 
 *    And (w-typacta = {&TYPACTA-ENTRANT} 
 *        /* MB D002845 11/10/07 - gestion de la freinte pour les sortants (Art. R�f. uniquement) */
 *         OR (w-typacta = {&TYPACTA-SORTANT} AND AVAILABLE kactd 
 *             AND CAN-FIND (FIRST bKact WHERE bKact.cSoc  = kactd.cSoc
 *                                         AND bKact.cEtab = kactd.cEtab
 *                                         AND bKact.cAct  = kactd.cAct  
 *                                         AND bKact.cArt  = F-cArtVu:SCREEN-VALUE
 *                                       NO-LOCK) ))
 * Then Assign {&Table}.txfr:Screen-Value = String(0.0).
 */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME kacta.trdtdyn
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL kacta.trdtdyn V-table-Win
ON VALUE-CHANGED OF kacta.trdtdyn IN FRAME F-Main /* Participe au rendement dynamique */
DO:

  /* CD DP2374 si le rendement dynamique est de type teneur, */ 
  /* limitation � 2 entrants coch�s */
  DEFINE VARIABLE vNumEntrantCoches AS INTEGER NO-UNDO.
  DEFINE VARIABLE vRowidArtRef      AS ROWID   NO-UNDO.
  DEFINE VARIABLE vValcridynRef     AS DECIMAL NO-UNDO.
  DEFINE VARIABLE vTypFlux          AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vcArtRef          AS CHARACTER NO-UNDO.
  
  DEFINE BUFFER bKacta FOR kacta.
  DEFINE BUFFER bKact  FOR kact.

  IF kacta.trdtdyn:CHECKED AND w-typacta = {&TYPACTA-ENTRANT} THEN DO:
     ASSIGN vNumEntrantCoches = 0.
     IF Adm-New-Record THEN DO:
        FOR EACH bKacta FIELDS ({&EmptyFields})
                        WHERE bKacta.csoc    = kactd.csoc
                          AND bKacta.cetab   = kactd.cetab
                          AND bKacta.cact    = kactd.cact
                          AND bKacta.datef   = kactd.datef
                          AND bKacta.typacta = {&TYPACTA-ENTRANT}
                          AND bKacta.trdtdyn = TRUE
                       NO-LOCK:
           ASSIGN vNumEntrantCoches = vNumEntrantCoches + 1.
        END.
     END.
     ELSE DO:
        FOR EACH bKacta FIELDS ({&EmptyFields})
                        WHERE bKacta.csoc    = kactd.csoc
                          AND bKacta.cetab   = kactd.cetab
                          AND bKacta.cact    = kactd.cact
                          AND bKacta.datef   = kactd.datef
                          AND bKacta.typacta = {&TYPACTA-ENTRANT}
                          AND bKacta.ni1     <> kacta.ni1
                          AND bKacta.trdtdyn = TRUE
                       NO-LOCK:
           ASSIGN vNumEntrantCoches = vNumEntrantCoches + 1.
        END.
     END.
     
     IF vNumEntrantCoches >= 2 THEN DO:
        {affichemsg.i &Message="Get-TradMsg (034232, 'Vous ne pouvez pas faire participer plus de 2 entrants au rendement dynamique de type teneur.':T)" }
        ASSIGN kacta.trdtdyn:CHECKED = FALSE.
        RETURN "".
     END.
  END.
  
  /* Recherche de l'article de r�f�rence de l'activit� */
  FOR FIRST bKact FIELDS (typflux cart)
                  WHERE bKact.csoc  = kactd.csoc
                    AND bKact.cetab = kactd.cetab
                    AND bKact.cact  = kactd.cact
                  NO-LOCK:
     ASSIGN vTypFlux = bKact.Typflux
            vcArtRef = bKact.cArt.              
  END.
  FOR FIRST bKacta FIELDS (valcridyn)
                    WHERE bKacta.cSoc    = kactd.cSoc
                      AND bKacta.cEtab   = kactd.cEtab
                      AND bKacta.TypActa = (IF vTypFlux = {&TYPFLUX-TIRE} 
                                               THEN {&TYPACTA-SORTANT} 
                                               ELSE {&TYPACTA-ENTRANT})
                      AND bKacta.cArt    = vcArtRef
                      AND bKacta.cAct    = kactd.cAct
                      AND bKacta.Datef   = kactd.Datef
                      AND bKacta.tLigFr  = FALSE
                    NO-LOCK:
     ASSIGN vRowidArtRef  = ROWID(bKacta)
            vValcridynRef = bKacta.valcridyn.
  END.

  /* l'art ref participe au rendement dynamique et c'est non modifiable */
  IF ((F-cartvu:SCREEN-VALUE IN FRAME {&FRAME-NAME} <> vcArtRef) 
       OR (F-cartvu:SCREEN-VALUE IN FRAME {&FRAME-NAME} = vcArtRef AND kacta.tligfr:CHECKED = TRUE)) THEN DO:
     /* si flux tir� + sortant : alors t�moin dispo mais pas valcridyn */
     ASSIGN kacta.trdtdyn:SENSITIVE = Adm-Fields-Enabled.
     
     IF kacta.trdtdyn:CHECKED THEN DO:
        /* si flux tir� + sortant autre que l'art ref, s'il participe au rendement alors il a la m�me valeur que le sortant */
        IF vTypFlux = {&TYPFLUX-TIRE} AND w-typacta = {&TYPACTA-SORTANT} AND NOT kacta.valcridyn:HIDDEN THEN
           ASSIGN kacta.valcridyn:SCREEN-VALUE IN FRAME {&FRAME-NAME} = STRING(vValcridynRef).
        
        ASSIGN kacta.valcridyn:SENSITIVE = (Adm-Fields-Enabled 
                                            AND NOT kacta.valcridyn:HIDDEN 
                                            AND w-typacta <> {&TYPACTA-SORTANT}).
     END.
     ELSE 
        ASSIGN kacta.valcridyn:SENSITIVE    = FALSE
               kacta.valcridyn:SCREEN-VALUE = STRING(0).
  END.
  
  /* MB 20131112_0001 P2374 */
  /* Initialisation de la valeur � partir de la fiche article */
  /* pour les entrants participant � la teneur */
  IF vTypFlux = {&TYPFLUX-TIRE} AND w-typacta = {&TYPACTA-ENTRANT} AND kacta.trdtdyn:CHECKED THEN
     RUN Init-Rdt-Dynamique.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */

/*** Triggers Generaux Du viewer */
{admvif/template/vinclude.i "triggers"}

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         

{admvif/template/vinclude.i "main-block"}

  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kacta"}
  {src/adm/template/row-list.i "kactd"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kacta"}
  {src/adm/template/row-find.i "kactd"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Affichage2emeUnite V-table-Win 
PROCEDURE Affichage2emeUnite :
/*------------------------------------------------------------------------------
  Purpose: Afficher ou pas les zones concernant la 2�me unit� (quantit�, unit�)
  Parameters:  Affichage : bool�en indiquant si les zones doivent �tre
                           affich�es (Yes) ou cach�es (No)
  Notes: cr�ation : NLE 12/04/2002
------------------------------------------------------------------------------*/

  Define Input Parameter I-Affichage As Logical No-Undo.
  Define Variable w-tunit1ko As Logical No-Undo.
  Define Variable w-tunit2ko As Logical No-Undo.

  Do With Frame {&Frame-Name}:

     Assign kacta.qte2:hidden    = Not I-Affichage
            kacta.qte2:sensitive = Adm-fields-enabled And I-Affichage
           
            kacta.cu2:hidden     = Not I-Affichage
            kacta.cu2:sensitive  = Adm-fields-enabled And I-Affichage
           
            B_AID-2:hidden       = Not I-Affichage
            B_AID-2:sensitive    = Adm-fields-enabled And I-Affichage.

     If Not I-Affichage Then
        Assign kacta.qte2:screen-value = "0":U
               kacta.cu2:screen-value  = "":U. 
    
     /* ajout CLE le 14/08/2003 */
     Assign w-tunit1ko  = yes
            w-tunit2ko  = yes.   
     If Not kacta.qte2:Hidden   /*la deuxi�me quantit� est affich�e*/    
            And Adm-New-Record 
            And w-unit2 <> "":U 
            And w-unit1 <> "":U
        Then Do :
           /*on a cr�� un article avec deux unit�s avant*/ 
           /* v�rification que ces deux unit�s sont possibles avec le nouvel article */
          
           /* test sur les deux premi�res unit�s de l'article */
           For First zart Fields(cug cus) Where zart.csoc = wcsoc
                                            And zart.cart = F-cartvu:Screen-Value
                                            And ( w-tunit2ko Or w-tunit1ko) 
                                          No-Lock :
             
              If zart.cug = w-unit2 Then w-tunit2ko = no.
              If zart.cus = w-unit2 Then w-tunit2ko = no.
                 
              If zart.cug = w-unit1 Then w-tunit1ko = no.
              If zart.cus = w-unit1 Then w-tunit1ko = no.
                
           End.
                
           /* test sur les autres unit�s de l'article */
           For Each zartu Fields (cunite) Where zartu.csoc = wcsoc
                                            And zartu.cart = F-cartvu:Screen-Value
                                            And ( w-tunit2ko Or w-tunit1ko )
                                          No-lock :
                
              If zartu.cunite = w-unit2 Then w-tunit2ko = no.
              If zartu.cunite = w-unit1 Then w-tunit1ko = no.
           End.
                
           /* affectation */
           If Not w-tunit2ko And Not w-tunit1ko Then Do :
              Assign kacta.cunite:Screen-Value = w-unit1
                     kacta.cu2:Screen-Value    = w-unit2.
                    
              kacta.qte:Format = {&Unite-Format-Qte} (Input kacta.cunite:Screen-Value, Input "z,zzz,zz":U). 
           End.
        End.
    
      If I-Affichage And Available kacta And kacta.cu2 <> "":U Then Do:
           /* Gestion du format des quantit�s en fonction de l'unit� */
           /* NLE 11/03/2003 : debut */
      
          If Adm-New-Record Then 
              /* modif CLE 14/08/2003 activite avec deux unites*/
              /*Assign kacta.qte2:Format = substring(string(" ":u, "x(5)":u) ,1,6 - length("9.9999":U)) + "z,zzz,zz9.9999":U.*/
              If kacta.cu2:Screen-Value <> "":U 
                 Then kacta.qte2:Format = {&Unite-Format-Qte} (Input kacta.cu2:Screen-Value, Input "z,zzz,zz":U). 

              Else 
                 Assign kacta.qte2:Format = substring(string(" ":u, "x(5)":u) ,1,6 - length("9.9999":U)) + "z,zzz,zz9.9999":U.
      
           Else
              /* NLE 11/03/2003 : fin */
              /* modif CLE 14/08/2003 activite avec deux unites*/
              Assign kacta.qte2:Format = {&unite-Format-qte} (Input kacta.cu2, Input "z,zzz,zz":U).
      End.
          
      Else Do:
         If Adm-Fields-Enabled 
            And (Available kacta And kacta.qte2 = 0 Or Adm-New-Record)
            Then Assign kacta.qte2:Format = substring(string(" ":u, "x(5)":u) ,1,6 - length("9.9999":U)) + "z,zzz,zz9.9999":U.
         
         Else Assign kacta.qte2:Format = "z,zzz,zzz":U.
     End.
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Aide-Sur-Champ V-table-Win 
PROCEDURE Aide-Sur-Champ :
/*------------------------------------------------------------------------------
  Purpose:     Aide (liste de choix) Sur les Champs
  Parameters:  Handle du widget active
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Champ  As Widget-Handle No-Undo.
    Define Variable ChoixEffectue As Character No-Undo.
    
    Define buffer bKacto for kacto.
    
    Case Champ:Name :
       When "F-cartvu":U Then Do :
          {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("artj":U,             /* Code fonction pour les droits */
                       "artjb":U, ("typart=":U + w-typart + 
                                  (If available kactd then ",Dvalid=":u + string(kactd.datef)  else "")), /* Nom browser et attributs */  
                       "":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
          If ChoixEffectue <> ? Then 
             Champ:Screen-Value = ChoixEffectue.
       End.       
       
       When "cunite":U Or
       When "cu2":U    Then Do :
          {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("artj":U,             /* Code fonction pour les droits */
                       "artulb":U, "att-wcart=":U + F-cartvu:screen-value in frame {&Frame-name},   /* Nom browser et attributs */
                       "":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
          If ChoixEffectue <> ? Then 
             Champ:Screen-Value = ChoixEffectue.
       End.

       When "cdepot":U Then Do :
          {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("depotj":U,              /* Code fonction pour les droits */
                       "depotjb":U, "":U,       /* Nom browser et attributs */
                       "":U, "":U,              /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no,                      /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
          If ChoixEffectue <> ? Then 
             Champ:Screen-Value = ChoixEffectue.
       End.

       When "cemp":U Then Do :
           If kacta.cdepot:screen-value <> "":U Then Do:
                 {&Run-prog} "admvif/objects/a-standa.w":U 
                             ("empj":U,           /* code de la fonction */
                              "empjb":U, "cdepot = ":U + kacta.cdepot:screen-value,   
                                                  /* Nom browser et attributs */
                              "":U, "":U,         /* Nom viewer et attributs */
                              Champ:Label,        /* Titre ecran d'aide */
                              No,                 /* Acces bouton nouveau */
                              Champ:Screen-Value, /* Valeur de depart */
                              Output ChoixEffectue).
                 If ChoixEffectue <> ? Then 
                    Champ:Screen-Value = ChoixEffectue.
           End.   
       End.

       /* << KL 20130910_0007 D1662 : Bloc d�plac� dans actaajv.w (Onglet Atelier) */
/*        When "f-copinf":U Then Do with frame {&frame-name} :                                                     */
/*            Find first bKacto where bKacto.csoc  = wcsoc                                                         */
/*                                 and bKacto.cetab = wcetab                                                       */
/*                                 and bKacto.cact  = kactd.cact                                                   */
/*                                 and bKacto.datef = kactd.datef                                                  */
/*                                 and bKacto.coper = Champ:Screen-value                                           */
/*                               no-lock no-error.                                                                 */
/*                                                                                                                 */
/*           {&Run-prog} "admvif/objects/a-standa.w":U                                                             */
/*                       ("actj":U,             /* Code fonction pour les droits */                                */
/*                        "actojb":U, "Rowid-kactd = ":u + String(Rowid(kactd)) ,   /* Nom browser et attributs */ */
/*                        "":U, "":U,    /* Nom viewer et attributs */                                             */
/*                        Champ:Label,             /* Titre ecran d'aide */                                        */
/*                        no,                     /* Acces bouton nouveau */                                       */
/*                        (If available bKacto                                                                     */
/*                           then string(bKacto.ni1)                                                               */
/*                           Else ""),      /* Valeur de depart */                                                 */
/*                        Output ChoixEffectue).                                                                   */
/*           If ChoixEffectue <> ? Then Do:                                                                        */
/*              Find first bKacto where bKacto.csoc  = wcsoc                                                       */
/*                                   and bKacto.cetab = wcetab                                                     */
/*                                   and bKacto.cact  = kactd.cact                                                 */
/*                                   and bKacto.datef = kactd.datef                                                */
/*                                   and bKacto.ni1   = Integer(ChoixEffectue)                                     */
/*                                 no-lock no-error.                                                               */
/*              Champ:Screen-Value = if available bKacto then bKacto.coper else "".                                */
/*           End.                                                                                                  */
/*        End.                                                                                                     */
       /* >> KL 20130910_0007 D1662 */

       Otherwise ChoixEffectue = ?.
    End Case.

    If ChoixEffectue <> ? Then 
       Apply "Tab":U To Champ.
    Else
       Apply "Entry":U To Champ.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Calcul-quantite V-table-Win 
PROCEDURE Calcul-quantite :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes: Calcul des quantit� vis � vis des conversions entre unit�s
------------------------------------------------------------------------------*/
   Define Variable ret As Logical No-Undo.
   Define Variable mess As Character No-Undo.
   Define Variable qte2temp As Decimal No-Undo.
   
   If not kacta.cu2:Hidden in Frame {&Frame-Name} /*deux unit�s affichables*/
      And not kacta.cunite:Screen-Value = "":U /*premi�re unit� renseign�e*/
      And not kacta.cu2:Screen-Value = "":U /*deuxi�me quantit� renseign�e*/
      
   Then Do With Frame {&Frame-Name} :
      {&Run-unite-Calcul-Qte-Opt} (Input wcsoc,
                                   Input F-Cartvu:Screen-Value,
                                   Input kacta.cunite:Screen-Value,
                                   Input kacta.cu2:Screen-Value,
                                   Input Decimal(kacta.qte:Screen-Value),
                                   Output qte2temp, 
                                   Output ret, 
                                   Output mess).
      if mess <> "":U Then DO:
         {affichemsg.i &Message="mess" }
      END.
      /* SLE 09/12/05 Si la zone est d�j� renseign�e, 
       * demander si maj auto 
       */
      IF DECIMAL (kacta.qte2:Screen-Value) <> qte2temp THEN DO: 
         {affichemsg.i &Message = "SUBSTITUTE(Get-TradTxt (024006, 'A partir des informations article, on obtiendrait &1.':T), {&unite-Affichage-qte}(qte2temp,kacta.cu2:Screen-Value,15) + ' ' + kacta.cu2:Screen-Value) 
                                   + '|':U + 
                                   SUBSTITUTE(Get-TradMsg (024007, 'Voulez-vous mettre � jour la quantit� de l''unit� &1 ?':T), kacta.cu2:Screen-Value)"
                       &TypeBtn = {&MsgOuiNon}
                       &BtnDef  = 1
         }
         IF ChoixMsg = 1 THEN
            ASSIGN kacta.qte2:Screen-Value = String(qte2temp).
      END.

   End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Enable-Rdtdyn V-table-Win 
PROCEDURE Enable-Rdtdyn :
/*------------------------------------------------------------------------------
  Purpose:     CD DP2374 affichage des zones si gestion du rendement pour la famille d'activit� 
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
 
   DEFINE BUFFER bKfmact   FOR kfmact.
   DEFINE BUFFER bKactcriv FOR kactcriv.
   DEFINE BUFFER bKacta    FOR kacta.
   DEFINE BUFFER bKact     FOR kact.
   DEFINE BUFFER bZcri     FOR zcri.
   
   DEFINE VARIABLE vCfmact      AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vRowidArtRef AS ROWID     NO-UNDO.
   DEFINE VARIABLE vTypFlux     AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vcArtRef     AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vtcFmRdtDyn  AS LOGICAL   NO-UNDO.
/* DEFINE VARIABLE vcCriRdtDyn  AS CHARACTER NO-UNDO. */
 
   RUN Get-info("kact.cfmact","Container-Source").
   vCfmact = RETURN-VALUE.

   FOR FIRST bKfmact FIELDS (trdtdyn)
                     WHERE bKfmact.csoc   = wcsoc
                       AND bKfmact.cetab  = wcetab
                       AND bKfmact.cfmact = vCfmact
                     NO-LOCK:
      ASSIGN vtcFmRdtDyn = bKfmact.trdtdyn.
   END.
   
   DO WITH FRAME {&FRAME-NAME}:
      IF vtcFmRdtDyn THEN DO:
         If w-typacta = {&TYPACTA-SORTANT} OR w-typacta = {&TYPACTA-ENTRANT} THEN   
            ASSIGN kacta.trdtdyn:HIDDEN = NO.
      END.
      ELSE ASSIGN kacta.trdtdyn:HIDDEN   = YES
                  kacta.valcridyn:HIDDEN = YES
                  F-CritTen:HIDDEN       = YES.
   END.

   /* affichage libell� participation rendement / teneur ****/
   IF NOT kacta.trdtdyn:HIDDEN THEN DO:   
      IF kactd.rdtdyn = {&RDTDYN-RDT} THEN 
         ASSIGN kacta.trdtdyn:LABEL    = Get-TradTxt (034243, 'Participe au rendement de d�sassemblage':T)
                kacta.valcridyn:HIDDEN = TRUE
                kacta.trdtdyn:CHECKED  = FALSE
                F-CritTen:HIDDEN       = TRUE
                F-cuCriDyn:HIDDEN      = TRUE.
      IF kactd.rdtdyn = {&RDTDYN-TEN} THEN DO: 
         ASSIGN kacta.trdtdyn:LABEL    = Get-TradTxt (034244, 'Participe � la teneur en dynamique':T)
                kacta.valcridyn:HIDDEN = FALSE
                F-CritTen:HIDDEN       = FALSE
                F-cuCriDyn:HIDDEN      = FALSE.
         FOR FIRST bKactcriv FIELDS (ccri)
                             WHERE bKactcriv.csoc   = wcsoc                     
                               And bKactcriv.cetab  = wcetab                    
                               And bKactcriv.typcle = {&KACTCRIV-TYPCLE-ACT-RD} 
                               And bKactcriv.cle    = kacta.cact + ',':u + STRING(kactd.datef, '99/99/9999':U)
                             NO-LOCK:
            ASSIGN vgcCriRdtDyn = bKactcriv.ccri.                                     
         END.

         /* Format de la valeur + unit� du crit�re */
         IF vgcCriRdtDyn  <> "":U THEN 
            ASSIGN F-CritTen:SCREEN-VALUE    = vgcCriRdtDyn + " :":U
                   {&TABLE}.valcridyn:format = REPLACE({&INFOCRI-Caractere}('lformat':U, wcsoc , vgcCriRdtDyn),"z":U, ">":U)
                   F-cuCriDyn:SCREEN-VALUE   = {&INFOCRI-Caractere}('cunite':U, wcsoc , vgcCriRdtDyn).
      END.
      
      /* CD DP2374 Recherche de l'article de r�f�rence de l'activit� */
      FOR FIRST bKact FIELDS (typflux cart)
                      WHERE bKact.csoc  = wcsoc
                        AND bKact.cetab = wcetab
                        AND bKact.cact  = kactd.cact
                      NO-LOCK:
         ASSIGN vTypFlux = bKact.typflux
                vcArtRef = bKact.cart.
      END.
      FOR FIRST bKacta FIELDS ({&EmptyFields})
                       WHERE bKacta.cSoc    = kactd.cSoc
                         AND bKacta.cEtab   = kactd.cEtab
                         AND bKacta.TypActa = (IF vTypFlux = {&TYPFLUX-TIRE} 
                                                  THEN {&TYPACTA-SORTANT} 
                                                  ELSE {&TYPACTA-ENTRANT})
                         AND bKacta.cArt    = vcArtRef
                         AND bKacta.cAct    = kactd.cAct
                         AND bKacta.Datef   = kactd.Datef 
                         AND bKacta.tLigFr  = FALSE
                       NO-LOCK:                    
         ASSIGN vRowidArtRef = ROWID(bKacta).
      END.
      
      /* l'art ref participe au rendement dynamique et c'est non modifiable */
      IF rowid(kacta) = vRowidArtRef AND F-cartvu:SCREEN-VALUE IN FRAME {&frame-name} = vcArtRef THEN
         ASSIGN kacta.trdtdyn:SENSITIVE   = NO
                kacta.valcridyn:SENSITIVE = NO. 
      ELSE DO: 
         /* si flux tir� + sortant : alors t�moin dispo mais pas valcridyn */
         ASSIGN kacta.trdtdyn:SENSITIVE = Adm-Fields-Enabled.
         IF kacta.trdtdyn:CHECKED THEN 
            ASSIGN kacta.valcridyn:SENSITIVE = (Adm-Fields-Enabled 
                                                AND NOT kacta.valcridyn:HIDDEN 
                                                AND w-typacta <> {&TYPACTA-SORTANT}).
         ELSE 
            ASSIGN kacta.valcridyn:SENSITIVE = FALSE.
      END.
   END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Enabled-fields V-table-Win 
PROCEDURE Enabled-fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE VARIABLE vtExistArt AS LOGICAL   NO-UNDO.

   Define Buffer bKacto   For kacto.
   Define Buffer b-zdepot  For zdepot.
   Define Buffer b-zart    For zart.
   DEFINE BUFFER bKact  FOR kact.  /* MB D002845 11/10/07 */
   DEFINE BUFFER bKacta FOR kacta. /* MB D002845 (II) 16/11/07 */
   
IF NOT AVAILABLE {&TABLE} THEN RETURN.
   
Run get-attribute("typacta":u).
w-typacta = return-value.

Do With Frame {&Frame-Name} :

  Case w-typacta :
     When {&TYPACTA-Sortant} Then do:
        Assign kacta.tqtedif:sensitive =  Adm-fields-enabled And
                                          kacta.tauto:checked.                                  
                                          
        If not kacta.tauto:checked then 
           Assign kacta.tqtedif:checked = No.
           /* CD DP2374 D�placement dans nouvel onglet Valorisation (acta3jv.w) */
/*         Assign cb-crges:Hidden = No.                      */
/*         Run Enable-crges( No /* replacement curseur */ ). */
/*         Assign kacta.coefcout:Modified = No.              */
        
     End.
     When {&TYPACTA-Entrant} Then do:
        /* CD DP2374 D�placement dans nouvel onglet Valorisation (acta3jv.w) */
/*         Assign kacta.coefcout:Hidden = True  */
/*                cb-crges:Hidden       = True. */
        Assign kacta.tqtedif:sensitive =  Adm-fields-enabled And
                                          kacta.tauto:checked.
        If not kacta.tauto:checked then 
           Assign kacta.tqtedif:checked = No.
     End.
  End.

  If w-typacta <> {&TYPACTA-ART-COUT} Then do:
     
     IF AVAILABLE kactd THEN /*VH 09/06/09 M_009505 test available sur kactd*/
        Find first bKacto where bKacto.csoc  = kactd.csoc
                             and bKacto.cetab = kactd.cetab
                             and bKacto.cact  = kactd.cact
                             and bKacto.datef = kactd.datef
                           no-lock no-error.
     /* << KL 20130910_0007 D1662 : Bloc d�plac� dans actaajv.w (Onglet Atelier) */                           
/*      Assign F-copinf:sensitive    = Adm-fields-enabled And */
/*                                     Available bKacto       */
/*             F-copinf:hidden       = Not Available bKacto   */
/*             Btn-AID-copinf:hidden = Not Available bKacto   */
/*             kacto.loper:hidden    = Not Available bKacto.  */
     /* >> KL 20130910_0007 D1662  */            
     
     IF AVAILABLE kactd THEN /*VH 09/06/09 M_009505 test available sur kactd*/     
        For First b-zart Fields (csoc cart tgene)
                         Where b-zart.csoc = kactd.csoc
                           And b-zart.cart = F-cartvu:Screen-Value
        No-Lock:
           Assign kacta.toartgen:Sensitive = b-zart.tgene And Adm-Fields-Enabled
                  kacta.toartgen:Hidden    = Not b-zart.tgene.
        End.
  End.

  /* Affichage et activation des zones Taux de freinte et Ligne de freinte */
  /* MB D002845 (II) 16/11/07 */
  /* Le taux de freinte n'est affich� que si l'article n'appara�t     */
  /* pas d�j� en entr�e (resp. sortie) de l'activit�, et inversement, */
  /* le t�moin Ligne de freinte n'est affich� que si l'article        */
  /* appara�t d�j� en entr�e (resp. sortie) de l'activit�             */
  /* Taux de freinte et Ligne de freinte INCOMPATIBLES pour un m�me   */
  /* entrant (resp. sortant si Art.R�f.)                              */
  RUN Pr-Gestion-Freinte (BUFFER kacta, 
                          INPUT w-TypActa, 
                          INPUT F-cArtVu:SCREEN-VALUE).
  /* MB D002845 (II) 19/11/07 */      
  /* Activation des zones Taux de freinte et Ligne de freinte */
  /* Bloc d�plac� dans nouvelle proc. Pr-Gestion-Freinte */
/* If Not {&Table}.tligfr:Hidden Then DO:
     If w-typacta = {&TYPACTA-ENTRANT}
        /* MB D002845 11/10/07 - Gestion de la freinte pour les sortants (Art.R�f. uniquement) */
        OR (w-typacta = {&TYPACTA-SORTANT} 
            AND AVAILABLE kactd
            AND CAN-FIND (FIRST bKact WHERE bKact.cSoc  = wcSoc
                                        AND bKact.cEtab = wcEtab
                                        AND bKact.cAct  = kactd.cAct  
                                        AND bKact.cArt  = F-cArtVu:SCREEN-VALUE
                                      NO-LOCK))
     Then 
        Assign {&Table}.txfr:Sensitive    = Not {&Table}.tligfr:Checked And Adm-Fields-Enabled
               {&Table}.tligfr:Sensitive  = (Decimal({&Table}.txfr:screen-value) = 0) And Adm-Fields-Enabled.
     Else
        If w-typacta = {&TYPACTA-SORTANT} Then
           Assign {&Table}.tligfr:Sensitive  = Adm-Fields-Enabled.
  END.*/
    
  /* Emplacement actif seulement si d�p�t g�r� par emplacement */
  If {&table}.cdepot:screen-value <> "":U Then Do:
    Find First b-zdepot where b-zdepot.csoc   = wcsoc
                          And b-zdepot.cetab  = wcetab
                          And b-zdepot.cdepot = {&Table}.cdepot:screen-value
                        No-Lock No-Error.
    If Available B-zdepot And B-zdepot.temp Then
       /* Le d�p�t est par emplacement */
       Assign {&Table}.cemp:sensitive   = Adm-Fields-Enabled
              B_AID-cemp:sensitive      = {&Table}.cemp:sensitive.
    Else
       Assign {&Table}.cemp:screen-value = "":u
              {&Table}.cemp:sensitive    = False
              B_AID-cemp:sensitive       = {&Table}.cemp:sensitive.

  End.
End.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE GestionAffichage2emeUnite V-table-Win 
PROCEDURE GestionAffichage2emeUnite :
/*------------------------------------------------------------------------------
  Purpose: Affichage des zones concernant la 2�me unit� si le 4�me 
           champ (Gestion d'une 2�me unit�) du param�tre ACTIVITE 
           est Vrai et si l'article a au moins une unit� � coefficient variable.
  Entr�e:  Le code de l'article
  Notes:   cr�ation : NLE 12/04/2002 - Revu SLE 08/04/03
           changement par rapport � la version pr�c�dente : si le zart n'existe pas,
           on lance un Affichage2emeUnite(NO) alors qu'avant on ne lancait rien
------------------------------------------------------------------------------*/
   Define Input Parameter I-cart AS CHARACTER No-Undo.

   Define Buffer B-zart  For zart.
   Define Buffer B-zartu For zartu.
   Define Variable w-tok As Logical No-Undo.
   
   For First B-zart FIELDS ({&EmptyFields})
                    Where B-zart.csoc = wcsoc
                      And B-zart.cart = I-cart
                      And B-zart.cug <> '' 
                      And B-zart.cus <> '' 
                      And Not B-zart.tqconst
                    Use-Index i1-art
                    No-Lock :
                   
      /* Coeff. variable entre UG et US : on affiche */
      Run Affichage2emeUnite(Yes).
      w-tok = True.
   End.
    
   If Not w-tok Then Do:
   
      /* Recherche d'au moins une unit� variable parmi les autres unit�s */     
      For First B-zartu FIELDS ({&EmptyFields})
                        Where B-zartu.csoc = wcsoc
                          And B-zartu.cart = i-cart
                          And Not B-zartu.tqconst
                        No-Lock :
         w-tok = True.
      End.
        
      Run Affichage2emeUnite(w-tok).
   End.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Init-Freinte V-table-Win 
PROCEDURE Init-Freinte :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE VARIABLE vTxFrArt      AS DECIMAL   No-Undo.
   DEFINE VARIABLE vTInitFreinte AS LOGICAL   NO-UNDO. /* Re-initialise-t-on la freinte par la freinte normative ? */ /* OM : 09/08/2006 Q000746 */

   If f-cartvu:Screen-Value IN FRAME {&FRAME-NAME} <> "" And
      Not kacta.txfr:Hidden                              And
      kacta.txfr:Sensitive
   Then Do:
      /* Je calcule la freinte en fonction de la freinte normative de l'article de production */
      For First zartfab Fields (txfr)
                         Where zartfab.csoc  = kactd.csoc
                           And zartfab.cetab = kactd.cetab
                           And zartfab.cart  = f-cartvu:Screen-Value
      No-Lock:
         Assign vTxFrArt = zartfab.txfr.
      End.
      /* On initialise l'unit� de freinte avec la valeur de l'article */
      /* SLE 09/12/05 Si la zone est d�j� renseign�e, 
       * demander si maj auto 
       */         
      IF NOT Adm-new-record AND 
         DECIMAL (kacta.txfr:Screen-Value) <> vTxFrArt THEN DO: 
         {affichemsg.i &Message = "SUBSTITUTE(Get-TradTxt (024006, 'A partir des informations article, on obtiendrait &1.':T), {&unite-Affichage-qte}(vTxFrArt,F-%:Screen-Value,15) + ' ' + F-%:Screen-Value) 
                                   + '|':U + 
                                   Get-TradMsg (024021, 'Voulez-vous mettre � jour la freinte ?':T)"
                       &TypeBtn = {&MsgOuiNon}
                       &BtnDef  = 1
         }
         ASSIGN vTInitFreinte = (ChoixMsg = 1).
      END.
      
      IF Adm-new-record OR   /* En creation, on init la freinte par la freinte normative */ /* OM : 09/08/2006 Q000746 */
         vTInitFreinte  THEN /* Re-Initialise t on la freinte par la freinte normative */   /* OM : 09/08/2006 Q000746 */
         ASSIGN kacta.txfr:Screen-Value = String(vTxFrArt).
   
   End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Init-Rdt-Dynamique V-table-Win 
PROCEDURE Init-Rdt-Dynamique :
/*------------------------------------------------------------------------------
  Purpose:     Initialisation du crit�re de r�f�rence du rendement sur les entrants 
               � partir des crit�res descriptifs article
  Parameters:  <none>
  Notes:       MB 20131112_0001 P2374
------------------------------------------------------------------------------*/
   DEFINE VARIABLE vTxRdtDyn    AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vtInitRdtDyn AS LOGICAL   NO-UNDO.
   
   DEFINE VARIABLE vValCritN    AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vMsaisie     AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vValc        AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vValn        AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vVald        AS DATE      NO-UNDO.
   DEFINE VARIABLE vRet         AS LOGICAL   NO-UNDO.
/* -------------------------------------------------------------------------- */    

   IF F-cArtvu:SCREEN-VALUE IN FRAME {&FRAME-NAME} <> "" AND
      NOT kacta.trdtdyn:HIDDEN                           AND
      kacta.tRdtDyn:SENSITIVE                            AND
      kacta.tRdtDyn:CHECKED = TRUE
   THEN DO:
      /* Recherche de la valeur par d�faut au niveau crit�res descriptifs article */
      /* ------------------------------------------------------------------------ */
      {&RUN-CRITERESFP-RECH-CRIT-ARTICLE} (INPUT  3,  /* D'abord � l'�tab (crit. desc. art. de prod.),
                                                       * puis � la soc. (crit. desc. art.) */
                                           INPUT  kacta.cSoc,
                                           INPUT  kacta.cEtab,
                                           INPUT  F-cArtvu:SCREEN-VALUE,
                                           INPUT  vgcCriRdtDyn,
                                           OUTPUT vMsaisie,
                                           OUTPUT vVald,      /* valdmin */
                                           OUTPUT vVald,      /* valdmax */
                                           OUTPUT vValCritN,  /* valnmin */
                                           OUTPUT vValn,      /* valnmax */
                                           OUTPUT vValc,      /* valcmin */
                                           OUTPUT vValc,      /* valcmax */
                                           OUTPUT vRet).                                   
      ASSIGN vTxRdtDyn = vValCritN.
      
      IF NOT Adm-new-record AND 
         DECIMAL (kacta.valcridyn:SCREEN-VALUE) <> vTxRdtDyn THEN DO: 
         {affichemsg.i &Message = "SUBSTITUTE(Get-TradTxt (024006, 'A partir des informations article, on obtiendrait &1.':T), 
                                               vgcCriRdtDyn + ' : ' + {&unite-Affichage-qte}(vTxRdtDyn,F-cuCriDyn:SCREEN-VALUE,15) + ' ' + F-cuCriDyn:SCREEN-VALUE) 
                                   + '|':U + Get-TradTxt (029885, 'Mise � jour':T) + ' ?':U"
                       &TypeBtn = {&MsgOuiNon}
                       &BtnDef  = 1
         }
         ASSIGN vtInitRdtDyn = (ChoixMsg = 1).
      END.
      
      IF Adm-new-record OR   /* En cr�ation, on initialise la teneur par la fiche article */ 
         vtInitRdtDyn  THEN  /* Re-initialise-t-on la teneur par la fiche article ? */ 
         ASSIGN kacta.valcridyn:SCREEN-VALUE = STRING(vTxRdtDyn).
   
   END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LinkFeatureID-Initialize V-table-Win 
PROCEDURE LinkFeatureID-Initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   {artcc.i F-cartvu NO NO}
   {unitecc.i {&TABLE}.cunite NO NO "" 1}
   {unitecc.i {&TABLE}.cu2 NO NO "" 2}
   {depotcc.i {&Table}.cdepot NO NO}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record V-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
Define Buffer b-kacta for kacta.

  /* Code placed here will execute PRIOR to standard behavior. */
  /* NLE 29/04/2002 */
  Run Affichage2emeUnite(No).
 
  IF vBlockAddRecord THEN DO:
     ASSIGN vBlockAddRecord   = FALSE.
     RETURN.
  END.
  
  ASSIGN vBlockAddRecord = FALSE.  /* CD Q7432 20140910_0001 bloquer en mode cr�ation */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .
  
  /* Code placed here will execute AFTER standard behavior.    */
  If Return-Value <> 'Adm-Error':U Then do:
    Case w-typacta :
      When {&TYPACTA-SORTANT} Then Do :
         /* CD DP2374 D�placement dans nouvel onglet Valorisation (acta3jv.w) */
/*         {getparii.i &CPACON="'ACTIVITE':U"                                        */
/*                     &NOCHAMP=5}                                                   */
/*         If vParam-OK And Integer(vParam-Valeurs) = 2 Then Do:                     */
/*             Run Set-Valeur-Combo( Input cb-crges:Handle In Frame {&Frame-Name}    */
/*                                 , Input {&KACTA-CRGES-COEFFICIENT-DE-PRIX} ).     */
/*         End.                                                                      */
/*         Else Do:                                                                  */
/*             Run Set-Valeur-Combo( Input cb-crges:Handle In Frame {&Frame-Name}    */
/*                                 , Input {&KACTA-CRGES-COEFFICIENT-REPARTITION} ). */
/*             wdec = 100.                                                           */
/*             For each b-kacta where b-kacta.csoc    = kactd.csoc                   */
/*                                And b-kacta.cetab   = kactd.cetab                  */
/*                                And b-kacta.cact    = kactd.cact                   */
/*                                And b-kacta.datef   = kactd.datef                  */
/*                                And b-kacta.typacta = {&TYPACTA-SORTANT}           */
/*                              No-lock :                                            */
/*                wdec = wdec - b-kacta.coefcout.                                    */
/*             End.                                                                  */
/*             if wdec < 0 then wdec = 0.                                            */
/*             Display wdec @ kacta.coefcout With Frame {&Frame-Name}.               */
/*         End.                                                                      */
/*         Run Enable-Crges(No).                                                     */
       
        Assign Kacta.tqtedif:Hidden in Frame {&Frame-Name} = No.
        Run dispatch ('Adv-Clear-Modified':u).
      End.
      /* Ajout SLE 08/04/03 */
      When {&TYPACTA-ENTRANT} Then Do :
        Assign Kacta.tqtedif:Hidden In Frame {&Frame-Name} = No.
        Run dispatch ('Adv-Clear-Modified':u).
      End.
      /* -- */
    End Case.
    Assign kacta.toartgen:Sensitive In Frame {&Frame-Name} = No
           kacta.toartgen:Hidden    In Frame {&Frame-Name} = Yes
           /* MB D002845 (II) 21/11/07 */
           /* On force l'affichage et l'activation du Taux de freinte et le non    */
           /* affichage du t�moin Ligne de freinte, ce n'est que lorsqu'un article */
           /* aura �t� saisi que ces zones s'initialiseront correctement           */
           kacta.TxFr:HIDDEN        IN FRAME {&Frame-Name} = FALSE
           F-%:HIDDEN               IN FRAME {&Frame-Name} = FALSE
           kacta.TxFr:SENSITIVE     IN FRAME {&Frame-Name} = TRUE
           kacta.tLigFr:HIDDEN      IN FRAME {&Frame-Name} = TRUE.       
           
    /***** CD DP2374 affichage des zones si gestion du rendement pour la famille d'activit� */   
    RUN Enable-Rdtdyn.      
           
           
 end.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-Adv-Clear-Fields V-table-Win 
PROCEDURE local-Adv-Clear-Fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'Adv-Clear-Fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Assign F-%:screen-value in frame {&frame-name} = "%":U.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record V-table-Win 
PROCEDURE local-assign-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  /*************************************************************/

  /* Dispatch standard ADM method.                             */
  /*************************************************************/
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  /*************************************************************/
  
  /* MB M148957 20140313_0002 */
  /* Suite � cr�ation de l'onglet Valorisation (acta3jv.w) pour mettre */
  /* � jour la r�gle de valorisation et le coefficient, on teste qu'il */
  /* y a bien une valeur, et on la force � la valeur par d�faut si ce  */
  /* n'est pas le cas afin de d�clencher les contr�les g�n�raux � la */
  /* sortie de actajs.w*/
  IF kacta.typacta = {&TYPACTA-SORTANT} AND kacta.crges = "":U THEN DO:
     /* Adaptation du bloc pr�sent dans le local-add-record de acta3jv */
     {getparii.i &CPACON  = "'ACTIVITE':U"
                 &NOCHAMP = 5}
     IF vParam-OK AND INTEGER(vParam-Valeurs) = 2 THEN 
        ASSIGN kacta.crges    = {&KACTA-CRGES-COEFFICIENT-DE-PRIX}.
     ELSE 
        ASSIGN kacta.crges    = {&KACTA-CRGES-COEFFICIENT-REPARTITION}
               kacta.coefcout = 100.
  END.
  /* Fin MB M148957 */ 
  

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement V-table-Win 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  define buffer bKacta for kacta.
  define buffer bKacto for kacto.
  define buffer bKactd for kactd.
  define buffer bKact  for kact.
  DEFINE BUFFER bKactartr FOR kactartr.
  DEFINE VARIABLE vCartOld AS CHARACTER NO-UNDO.
  
  /* Code placed here will execute PRIOR to standard behavior. */
  /* --------------------------------------------------------- */
  
  /* MB PROLINT 20071228_0001 */
  /* Vu avec les M�thodes : trop co�teux de r�parer ce Find Current... Exclusive-Lock */
  /* Mais il ne faut pas le taguer par un &_PROPARSE_PROLINT-NOWARN(findexcl) pour autant */
  /* M�me si du coup, cela ressortira toujours au contr�le des sources */
  Find Current kactd Exclusive-Lock No-Error.
  Assign kactd.datmod = Today
         kactd.cuser  = wcuser.

  Assign kacta.csoc    = wcsoc
         kacta.cetab   = wcetab
         kacta.cact    = kactd.cact
         kacta.datef   = kactd.datef
         kacta.typacta = w-typacta 
         vCartOld      = kacta.cart        
         kacta.Cart    = TRIM(F-Cartvu:Screen-Value In Frame {&Frame-Name}).
         
  /* CD DP2374 26/07/13 si article ne participe pas au rendement, remettre � 0 la valeur du crit�re de ref */
  IF kacta.trdtdyn:CHECKED = FALSE THEN
     ASSIGN kacta.valcridyn = 0
            kacta.valcridyn:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "0".
     
   /* CD DP2374 D�placement dans nouvel onglet Valorisation (acta3jv.w) */
/*   IF NOT cb-crges:HIDDEN THEN                                            */
/*      Run Get-Valeur-Combo( Input  cb-crges:Handle In Frame {&Frame-Name} */
/*                          , Output kacta.crges ).                         */
/*   ELSE                                                                   */
/*      ASSIGN kacta.crges = "".                                            */
  
   /* << KL 20130910_0007 D1662 : Bloc d�plac� dans actaajv.w (Onglet Atelier) */    
  /*
   * Cas particulier pour l'atelier 
   */
/*   If F-copinf:screen-value in Frame {&Frame-Name} = "" Then do:                           */
/*      Assign kacta.nligopinf = 0                                                           */
/*             kacta.ni1opinf  = 0.                                                          */
/*   End.                                                                                    */
/*   Else Do:                                                                                */
/*      Find first bKacto where bKacto.csoc  = kactd.csoc                                    */
/*                           and bKacto.cetab = kactd.cetab                                  */
/*                           and bKacto.cact  = kactd.cact                                   */
/*                           and bKacto.datef = kactd.datef                                  */
/*                           and bKacto.coper = F-copinf:Screen-value in frame {&frame-name} */
/*                         no-lock no-error.                                                 */
/*      If not available bKacto                                                              */
/*        Then Assign kacta.nligopinf = 0                                                    */
/*                    kacta.ni1opinf  = 0.                                                   */
/*        Else Assign kacta.ni1opinf  = bKacto.ni1                                           */
/*                    kacta.nligopinf = bKacto.nlig.                                         */
/*   End.                                                                                    */
  /* >> KL 20130910_0007 D1662 */
  
  Case kacta.typacta :
     When {&TYPACTA-ART-COUT} Then
        Assign kacta.tauto = Yes.
  End.

  If Adm-New-Record Then Do:
    Find Last bKacta Where bKacta.csoc    = wcsoc
                        And bKacta.cetab   = wcetab
                        And bKacta.cact    = kactd.cact
                        And bKacta.datef   = kactd.datef
                        And bKacta.typacta = w-typacta
                      No-Lock No-Error.
    If Available bKacta Then 
        kacta.ni1 = bKacta.ni1 + 1.
    Else 
        kacta.ni1 = 1.        
    
    Find Last bKacta Where bKacta.csoc    = wcsoc
                        And bKacta.cetab   = wcetab
                        And bKacta.cact    = kactd.cact
                        And bKacta.datef   = kactd.datef
                        And bKacta.typacta = w-typacta
                      Use-Index i3-acta
                      No-Lock No-Error.
    If Available bKacta Then 
        kacta.nlig = bKacta.nlig + 1.
    Else 
        kacta.nlig = 1.
  End.

  /* Dispatch standard ADM method.                             */
  /* --------------------------------------------------------- */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .
  
  /* Code placed here will execute AFTER standard behavior.    */
  /* --------------------------------------------------------- */
 
 
  /* CBA D003444 04/07/08 : d�placement de ce traitement dans le local-end-update */ 
  /* --- TRAITEMENT SPECIFIQUE A L'ARTICLE DE REFERENCE -----  */

  /* Si modif de l'article, mettre � jour les articles de remplacement */
  IF vCartOld <> "" AND vCartOld <> kacta.cart AND 
     CAN-FIND(FIRST bKactartr WHERE bKactartr.csoc  = wcsoc
                                And bKactartr.cetab = wcetab
                                And bKactartr.cact  = kactd.cact
                                And bKactartr.datef = kactd.datef 
                                AND bKactartr.ni1   = kacta.ni1
                                AND bKactartr.cart  = vCartOld) THEN DO:
     FOR EACH bKactartr WHERE bKactartr.csoc  = wcsoc
                          And bKactartr.cetab = wcetab
                          And bKactartr.cact  = kactd.cact
                          And bKactartr.datef = kactd.datef 
                          AND bKactartr.ni1   = kacta.ni1
                          AND bKactartr.cart  = vCartOld
                        EXCLUSIVE-LOCK :
        ASSIGN bKactartr.cart = kacta.cart.
     END.
  END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-cancel-record V-table-Win 
PROCEDURE local-cancel-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'cancel-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
 ASSIGN vBlockAddRecord   = FALSE.  /* CD Q7432 10/09/2014 20140910_0001 ne pas bloquer la cr�ation si on a fait cr�ation puis annulation */
 
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-copy-record V-table-Win 
PROCEDURE local-copy-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       Cr�ation MB D002845 (II) 16/11/07 
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'copy-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */  
  IF AVAILABLE kacta THEN DO WITH FRAME {&FRAME-NAME} :
     RUN Pr-Gestion-Freinte (BUFFER kacta, 
                             INPUT  w-TypActa,
                             INPUT  F-cArtVu:SCREEN-VALUE).
     /* R�initialisation des infos de freintes */
     ASSIGN /* kacta.TxFr:SCREEN-VALUE = STRING(0.0)*/
            kacta.tLigFr:CHECKED = FALSE.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-statement V-table-Win 
PROCEDURE local-delete-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   /* MB D002845 (II) 23/11/07 */
   DEFINE VARIABLE vTypFlux      AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vtMajQteDatef AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vRowidArtRef  AS ROWID     NO-UNDO.

   Define Buffer B-kactartr For kactartr.
   /* MB D002845 (II) 23/11/07 */
   DEFINE BUFFER bKact   FOR kact.
   DEFINE BUFFER bKacta  FOR kacta.
   DEFINE BUFFER bufKcom FOR kcom. 
   DEFINE BUFFER bKactapop FOR Kactapop. /* MMO AT000927 */
   
/* -------------------------------------------------------------------------- */  

   /* Code placed here will execute PRIOR to standard behavior. */
   /* --------------------------------------------------------- */
  
   /* MB D002845 (II) 23/11/07 - Passage sur buffer pour suppression + ajout NO-WAIT */
   Find First bufKcom Where bufKcom.csoc   = {&Table}.csoc
                        And bufKcom.cetab  = {&Table}.cetab
                        And bufKcom.typcle = {&TYPCLE-COM-ACT}
                        And bufKcom.cle    = {&Table}.cact + ',':U
                                           + String({&Table}.datef, '99/99/9999':U) + ',':U
                                           + {&Table}.typacta + ',':U
                                           + String({&Table}.ni1)
                    Exclusive-Lock NO-WAIT No-Error.
   If Available bufKcom Then Delete bufKcom.
  
   /* Destruction des articles de remplacement */
   For Each B-kactartr Where B-kactartr.csoc  = {&Table}.csoc
                         And B-kactartr.cetab = {&Table}.cetab
                         And B-kactartr.cact  = {&Table}.cact
                         And B-kactartr.datef = {&Table}.datef
                         And B-kactartr.ni1   = {&Table}.ni1
                       Exclusive-Lock:
      Delete B-kactartr.
   End.
  
   /* MB D002845 (II) 23/11/07 - Freinte sur les sortants (suite) */
   /* Si flux tir� et si la ligne supprim�e porte sur l'article de r�f�rence (donc  */
   /* un sortant), alors il faut recalculer la qt� de r�f�rence de la date d'effet  */
   /* ----------------------------------------------------------------------------- */
   FOR FIRST bKact FIELDS (cArt TypFlux)
                   WHERE bKact.cSoc  = {&TABLE}.cSoc
                     AND bKact.cEtab = {&TABLE}.cEtab
                     AND bKact.cAct  = {&TABLE}.cAct
                   NO-LOCK:
      ASSIGN vtMajQteDatef = ( {&TABLE}.TypActa = {&TYPACTA-SORTANT}
                               AND bKact.TypFlux = {&TYPFLUX-TIRE}
                               AND bKact.cArt = {&TABLE}.cArt )
             vTypFlux      = bKact.TypFlux.                      
   END.
   
   /* Recherche de la ligne de l'art. de r�f�rence non ligne de freinte */
   IF vtMajQteDatef THEN 
      FOR FIRST bKacta FIELDS ({&EmptyFields})
                       WHERE bKacta.cSoc    = {&TABLE}.cSoc
                         AND bKacta.cEtab   = {&TABLE}.cEtab
                         AND bKacta.TypActa = {&TABLE}.TypActa
                         AND bKacta.cArt    = {&TABLE}.cArt
                         AND bKacta.cAct    = {&TABLE}.cAct
                         AND bKacta.Datef   = {&TABLE}.Datef  /* i2-acta */
                         AND bKacta.tLigFr  = FALSE
                       NO-LOCK:
         ASSIGN vRowidArtRef = ROWID(bKacta).
      END.

      
   /* MMO AT000927 - suppression lien */ 
   FOR FIRST bKactapop  WHERE bKactapop.csoc     = {&TABLE}.csoc    
                          AND bKactapop.cetab    = {&TABLE}.cetab   
                          AND bKactapop.cact     = {&TABLE}.cact    
                          AND bKactapop.datef    = {&TABLE}.datef   
                          AND bKactapop.typacta  = {&TABLE}.typacta 
                          AND bKactapop.ni1      = {&TABLE}.ni1  
                        EXCLUSIVE-LOCK :    
      Delete bKactapop.    
   END.
   /* fin MMO AT000927 - suppression lien */ 
                           
   /* Dispatch standard ADM method.                             */
   /* --------------------------------------------------------- */
   RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-statement':U ) .
   
   /* Code placed here will execute AFTER standard behavior.    */
   /* --------------------------------------------------------- */

   /* MB D002845 (II) 23/11/07 - Freinte sur les sortants (suite) */
   /* Si flux tir� et si la ligne supprim�e porte sur l'article de r�f�rence, */
   /* alors il faut recalculer la qt� de r�f�rence de la date d'effet         */
   /* ----------------------------------------------------------------------- */
   IF vtMajQteDatef AND vRowidArtRef <> ? THEN DO:
      /* Repositionnement sur le ligne non ligne de freinte */
      FIND FIRST kacta WHERE ROWID(kacta) = vRowidArtRef
                       NO-LOCK NO-ERROR.                       
      IF AVAILABLE kacta THEN
         RUN Pr-Maj-QteRef-Datef (BUFFER kacta,
                                  INPUT vTypFlux).
   END.
      
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields V-table-Win 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .
  /* Code placed here will execute AFTER standard behavior.    */
  
   /* CD DP2374 D�placement dans nouvel onglet Valorisation (acta3jv.w) */
/* Assign kacta.coefcout:sensitive in frame {&FRAME-NAME} = no  */
/*        cb-crges:sensitive in frame {&FRAME-NAME}       = no. */

  Run enabled-fields.

  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vRet AS LOGICAL NO-UNDO.
  DEFINE VARIABLE vMsg AS CHARACTER NO-UNDO.
  
  DEFINE BUFFER bKact FOR kact.  /* MB D002845 11/10/07 */
    
  /* CD Q7432 10/09/2014 20140910_0001 d�plac� dans local-display-fields sinon ne fonctionne qu'� la 1�re entr�e */
  IF vgMemCart = "":U THEN ASSIGN vBlockAddRecord = TRUE.
  
  /* CD Q7432 20140910_0001 10/09/2014 pour bloquer le passage en mode cr�ation � l'ouverture */
  if available kacta THEN ASSIGN vgMemCart = kacta.cart.
  
  /* Code placed here will execute PRIOR to standard behavior. */
  /* --------------------------------------------------------- */
  if available kacta then do:
    Assign F-Cartvu = kacta.cart.

    /* Avant, coefcout et tqtedif g�r�s uniquement en SORTANT */
    /* CBA D003444 04/07/08 ajout de kacta.toartgen dans la liste car sinon l'affichage de la valeur du champ bas� �tait al�atoire */ 
    /* CJ Q004989 M107785 21/11/11 ajout champs txfr et F-cartvu pas valeur base dans le screen-value pour conserver l'affichage de la freinte */ 
    Case w-typacta :
       When {&TYPACTA-SORTANT} Then Do:
         Display kacta.tqtedif kacta.toartgen kacta.txfr F-cartvu kacta.valcridyn kacta.trdtdyn With Frame {&Frame-Name}.
         /* Display kacta.coefcout kacta.tqtedif kacta.toartgen kacta.txfr F-cartvu With Frame {&Frame-Name}. */
       End.
       When {&TYPACTA-ENTRANT} Then
         Display kacta.tqtedif kacta.toartgen kacta.txfr F-cartvu kacta.valcridyn kacta.trdtdyn With Frame {&Frame-Name}. 
    End Case.
    /* Gestion du format des quantit�s en fonction de l'unit� */
    Assign Kacta.Qte:Format = {&unite-Format-qte} (Input Kacta.Cunite ,
                                                   Input "z,zzz,zz":U).

    If W-T-2emeUnite Then Do:
      If kacta.cu2 <> "":U Or kacta.qte2 <> 0 Then
        Run Affichage2emeUnite(Yes).
      Else Do:
        /* Affichage des zones concernant la 2�me unit� si l'article a 
         * au moins une unit� � coefficient variable 
         */
        Run GestionAffichage2emeUnite(kacta.cart).
      End.  
    End.
    /* MB D008245 (II) 16/11/07 */
    /* Affichage des zones Taux de freinte et Ligne de freinte en    */
    /* fonction du reste des entrants (resp. sortants) de l'activit� */
    RUN Pr-Gestion-Freinte (BUFFER kacta, INPUT w-TypActa, INPUT kacta.cArt).
  end.

   /* Dispatch standard ADM method.                             */
   /* --------------------------------------------------------- */
   RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

   /* Code placed here will execute AFTER standard behavior.    */
   /* --------------------------------------------------------- */
  If Available {&Table} Then Do:
  
      /* L�gende en gras */
      RUN artfabdfabcp.p ( INPUT {&Table}.csoc,
                           INPUT {&Table}.cetab,
                           INPUT F-cartvu:screen-value in Frame {&Frame-Name},
                           INPUT {&Table}.datef,
                           OUTPUT vRet,
                           OUTPUT vMsg).

      IF NOT vRet THEN
          RUN Set-Info('Valid=Passee':u,'Container-Source':u).
      ELSE IF vMsg <> "" THEN
          RUN Set-Info('Valid=Future':u,'Container-Source':u).
      ELSE
          RUN Set-Info('Valid=Valide':u,'Container-Source':u).
          
     /* CD DP2374 D�placement dans nouvel onglet Valorisation (acta3jv.w) */
/*   If w-typacta = {&TYPACTA-SORTANT} Then                                 */
/*        Run Set-Valeur-Combo( Input cb-crges:Handle In Frame {&Frame-Name} */
/*                            , Input kacta.crges ).                         */
  
      {artcc.i   F-Cartvu      no yes zart.lart 1 w-typart "''" zart.rart}
      {unitecc.i kacta.cunite  no no  ""}
      {depotcc.i kacta.cdepot  no yes zdepot.ldepot}  /* SLE 30/04/03 */
      {empcc.i   kacta.cemp    no yes zemp.lemp 1 kacta.cdepot} /* SLE 22/05/03 */

      /* suppression de {depotcc.i kacta.cdepot no yes zdepot.ldepot} */    
      /* << KL 20130910_0007 D1662 : Bloc d�plac� dans actaajv.w (Onglet Atelier) */
/*       Find first b-kacto where b-kacto.csoc  = kactd.csoc                                                   */
/*                            and b-kacto.cetab = kactd.cetab                                                  */
/*                            and b-kacto.cact  = kactd.cact                                                   */
/*                            and b-kacto.datef = kactd.datef                                                  */
/*                            and b-kacto.ni1   = kacta.ni1opinf                                               */
/*                          no-lock no-error.                                                                  */
/*        F-copinf:screen-value in frame {&frame-name} =                                                       */
/*                        (If available b-kacto then b-kacto.coper else "").                                   */
/*       {acto2cc.i  F-copinf no yes kacto.loper 1 string(kactd.cact) string(kactd.datef)}  /* GLC 28/12/04 */ */
      /* >> KL 20130910_0007 D1662 */
      /***** CD DP2374 affichage des zones si gestion du rendement pour la famille d'activit� */   
      RUN Enable-Rdtdyn.  

  end.
  Run enabled-fields.
  If Not kacta.cu2:Hidden Then w-unit2 = kacta.cu2:Screen-Value.
                          Else w-unit2 = "":U.
  w-unit1 = kacta.cunite:Screen-Value.
 
  Run get-attribute("typacta":u).
  w-typacta = return-value.
  
/** MB D002845 (II) 16/11/07 - d�plac� avant le Run Dispatch Display-Fields dans proc. Pr-Gestion-Freinte
  Do With Frame {&Frame-Name}:
      CASE w-typacta :
         WHEN {&TYPACTA-ENTRANT} Then
            Assign kacta.txfr:Hidden   = vgtCacherFreinte
                   F-%:Hidden          = vgtCacherFreinte
                   kacta.tligfr:Hidden = vgtCacherFreinte.
         WHEN {&TYPACTA-SORTANT} Then DO:
            /* MB D002845 11/10/07 - gestion de la freinte pour les sortants (Art.R�f. uniquement) */
            IF CAN-FIND (FIRST bKact WHERE bKact.cSoc  = kactd.cSoc
                                       AND bKact.cEtab = kactd.cEtab
                                       AND bKact.cAct  = kactd.cAct  
                                       AND bKact.cArt  = F-cArtVu:SCREEN-VALUE
                                     NO-LOCK) THEN
               ASSIGN kacta.txfr:HIDDEN   = vgtCacherFreinte
                      F-%:HIDDEN          = vgtCacherFreinte
                      kacta.tligfr:HIDDEN = vgtCacherFreinte.
            ELSE
               Assign kacta.txfr:Sensitive = No
                      kacta.txfr:Hidden    = Yes /* vgtCacherFreinte */
                      F-%:Hidden           = Yes /* vgtCacherFreinte */
                      kacta.tligfr:Hidden  = vgtCacherFreinte. /* ??? pourquoi pas Yes comme les 2 autres ??? MB */
         END.          
         OTHERWISE
            Assign kacta.txfr:Sensitive = No
                   kacta.txfr:Hidden    = Yes
                   F-%:Hidden           = Yes
                   kacta.tligfr:Hidden  = YES.
      END CASE. 

  End.**/
  
  RUN dispatch IN THIS-PROCEDURE ('adv-clear-modified':U).
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  Case w-typacta:
    When {&TYPACTA-SORTANT} Then 
      Assign /* CD DP2374 D�placement dans nouvel onglet Valorisation (acta3jv.w) */
             /* kacta.coefcout:sensitive in frame {&FRAME-NAME} = Adm-Fields-Enabled */
             /* cb-crges:sensitive in frame {&FRAME-NAME}       = Adm-Fields-Enabled  */ 
             Kacta.tqtedif:sensitive in frame {&FRAME-NAME}  = Adm-Fields-Enabled.

    When {&TYPACTA-SORTANT} Then 
      Assign Kacta.tqtedif:sensitive in frame {&FRAME-NAME}  = Adm-Fields-Enabled.
    /* -- */
  End Case.
  Run enabled-fields.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-end-update V-table-Win 
PROCEDURE local-end-update :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  /* CBA D003444 04/07/08 : d�placement ici du recalcul de la qt� de r�f�rence de l'activit�  */
  /* car sinon marche pas dans l'assign statement : si le kacta en cours concerne  */
  /* l'article de r�f�rence, il n'est pas encore cr�e et donc pas pris en compte dans */
  /* le calcul.                                                                       */
 
 
 
  /* --- TRAITEMENT SPECIFIQUE A L'ARTICLE DE REFERENCE -----
   * Si la dur�e de l'activit� est d�finie par les op�rations 
   * ( param�tre ACTIVITE dur�e de l'activit� = 1, W-T-Duree = True )
   * ou si aucune dur�e de l'activit� n'est renseign�e dans les dates d'effet de l'activit� 
   * alors on r�plique la qte de l'article de r�f�rence d�clar�es au niveau 
   * des entrants/sortants dans celle d�clar�e au niveau des dates d'effet 
   * pour garder la coh�rence entre ces 2 qt�s pour l'article de r�f�rence  
   */
  find kact where kact.csoc  = wcsoc
              and kact.cetab = wcetab
              and kact.cact  = kacta.cact
            no-lock no-error.

  If available kact and 
     available kactd and 
     (Article-de-reference () 
      /* MB D002845 (II) 23/11/07 et m�me la ligne de freinte impacte la qt� ref. en flux tir� */
      OR (kact.TypFlux = {&TYPFLUX-TIRE} AND kact.cArt = kacta.cArt AND kacta.TypActa = {&TYPACTA-SORTANT} )) And 
     ((Kactd.lotde = 0 And
       Kactd.lotds = 0 And
       Kactd.lotdt = 0 ) Or
       W-T-Duree )
  Then Do:
      /* MAJ de la qt� de r�f�rence de l'activit� (kactd.lotq) � partir */
      /* de la qt� de l'activit� pour l'art. de r�f�rence (kacta.qte)   */
      
      /* MB D002845 (II) 23/11/07 - passage sur proc. pour */
      /* calcul de la qt� de r�f�rence de la date d'effet  */ 
      /* ------------------------------------------------- */
      RUN Pr-Maj-QteRef-Datef (BUFFER kacta,
                               INPUT  kact.TypFlux).  

  END.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'end-update':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize V-table-Win 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  /* CD Q7432 10/09/2014 20140910_0001 d�plac� dans local-display-fields sinon ne fonctionne qu'� la 1�re entr�e */
  /* ASSIGN vBlockAddRecord = TRUE.*/
  
  Run get-attribute("typacta":u).
  w-typacta = return-value.
  If w-typacta = {&TYPACTA-ART-COUT} Then
     w-typart = {&TYPART-COUT}.
  Else
     w-typart = {&TYPART-MARCHANDISE}.

  If w-typacta <> {&TYPACTA-ART-COUT} Then Do:
    Run Get-Attribute('Uib-Mode':U).
    If Return-Value <> 'Design':U Then Do With Frame {&Frame-Name} :
      /* Recherche du 4�me champ (Gestion d'une 2�me unit�) du param�tre ACTIVITE */
      {getparii.i &CPACON="'ACTIVITE':U" 
                  &NOCHAMP=4 
      } 
      Assign W-T-2emeUnite = vParam-Ok And vParam-Valeurs = "Yes":U. 
    End.
  End.
  
  /* CD DP2374 D�placement dans nouvel onglet Valorisation (acta3jv.w) */
  /* SLe 22/12/2010 D000662 Distinction CTART pr�fix� et coeff de prix */  
/*   {getparii.i &CPACON="'CTART':U"         */
/*               &NOCHAMP="1"                */
/*               &CSOC=wcsoc                 */
/*               &CETAB=wcetab               */
/*               &OUT-Valeurs=w-cout-prefixe */
/*   }                                       */
/*   {getparii.i &CPACON="'CTART':U"         */
/*               &NOCHAMP="7"                */
/*               &CSOC=wcsoc                 */
/*               &CETAB=wcetab               */
/*               &OUT-Valeurs=w-cout-coef    */
/*   }                                       */
  /* --- */
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .
     
  /* Code placed here will execute AFTER standard behavior.    */
  Run Get-Attribute('Uib-Mode':U).
  If Return-Value <> 'Design':U Then Do With Frame {&Frame-Name} :
     If w-typacta = {&TYPACTA-SORTANT} Or
        w-typacta = {&TYPACTA-ENTRANT}
     Then Assign  /* CD DP2374 D�placement dans nouvel onglet Valorisation (acta3jv.w) */
                 /* kacta.coefcout:Hidden = (w-typacta = {&TYPACTA-SORTANT})
                  * cb-crges:Hidden       = (w-typacta = {&TYPACTA-SORTANT}) */
                 kacta.tqtedif:Hidden  = No /*(w-typacta = {&TYPACTA-SORTANT}) SLE 08/04/03 */.

     If CURRENT-WINDOW-layout = "Master-Layout":u
     Then Do:
        {getparii.i &CPACON="'ACTIVITE':U" 
                    &NOCHAMP=2
                    &CSOC=wcsoc
                    &CETAB=wcetab
        } 
/* SLe D002845 Partout ailleurs la valeur par d�faut est FALSe !? */
/*        If vParam-Ok And vParam-Valeurs <> ? And vParam-Valeurs <> '':u */
/*         Then Do:                                                        */
/*             vgtCacherFreinte = ( vParam-Valeurs = 'Yes':u ) .           */
/*         End.                                                            */
/*         Else Do:                                                        */
/*             vgtCacherFreinte = True .                                   */
/*         End.                                                            */
        ASSIGN vgtCacherFreinte = ( vParam-Ok And vParam-Valeurs = 'Yes':u ).
        
        /* MB D002845 11/10/07 - gestion de la freinte pour les sortants (Art.R�f. uniquement) */
        Assign kacta.txfr:Hidden   = vgtCacherFreinte /* And ( w-typacta <> {&TYPACTA-ENTRANT} )*/
               F-%:Hidden          = vgtCacherFreinte /* And ( w-typacta <> {&TYPACTA-ENTRANT} )*/
               kacta.tligfr:Hidden = vgtCacherFreinte.
     End.
     
     {getparii.i &CPACON="'ACTIVITE':U" 
                 &NOCHAMP=1
                 &CSOC=wcsoc
                 &CETAB=wcetab
     } 
     If vParam-Ok And vParam-Valeurs <> ? And vParam-Valeurs <> '':u 
     Then W-T-Duree = ( vParam-Valeurs = '1':u ).
     Else W-T-Duree = False.
        
     
        
  End.
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Test-Saisie V-table-Win 
PROCEDURE Local-Test-Saisie :
Define Input Parameter Test-Global As Logical       No-Undo.
   Define Input Parameter Widget-Test As Widget-Handle No-Undo.

   Define Variable vCart           As Character No-Undo.
   Define Variable w-init-toartgen As Logical   No-Undo.
   Define Variable wnom-onglet     As Character No-Undo.
   DEFINE VARIABLE vRet AS LOGICAL NO-UNDO.
   DEFINE VARIABLE vMsg AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vTrouve  AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vcUnite  AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vcUnite2 AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vtAnoFreinte AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vtArtRef AS LOGICAL   NO-UNDO.
   DEFINE BUFFER bKact   FOR kact.
   DEFINE BUFFER bKacta  FOR kacta.
   
   Assign vCart       = F-cartvu:screen-value in Frame {&Frame-Name}
          wnom-onglet = Get-Label-Folder(). 

   /* TEST ARTICLE   */
   If Test-Global Or Widget-Test = F-cartvu:Handle in Frame {&Frame-Name}
   Then Do With Frame {&Frame-Name}:
      {artcc.i F-cartvu yes yes zart.lart 1 w-typart "''" zart.rart}
      /* !!!!!! On utilisera par la suite buf1-zart pour cusaisie, tgene */
      
      Find Current kactd No-Lock No-Error.
      /* L'article ne doit pas �tre invalide � la date d'effet de l'activit� */
      /* Contr�le non bloquant pour les articles dont la validit� est � venir
       *  + prise en compte des dates de fabrication en priorit� */
      RUN artfabdfabcp.p ( INPUT kactd.csoc,
                           INPUT kactd.cetab,
                           INPUT vCart,
                           INPUT kactd.datef,
                           OUTPUT vRet,
                           OUTPUT vMsg).
      IF vMsg <> "" THEN DO :
          {affichemsg.i &Message = "vMsg + '|':U + wnom-onglet"}
          Apply "Entry":U to F-Cartvu.
      END.
      IF NOT vRet THEN Return "Adm-Error":U.
      
      Find kact Where kact.csoc  = kactd.csoc
                  And kact.cetab = kactd.cetab
                  And kact.cact  = kactd.cact
                No-Lock No-Error.
      If available kact and available kactd 
         and decimal(kacta.qte:screen-value) = 0 
         and kacta.cunite:screen-value = "":U 
         and kact.cart = vCart 
      Then
         assign kacta.qte:screen-value    =
               (IF NOT CAN-FIND (FIRST bKacta WHERE bKacta.cSoc    = wcSoc
                                                AND bKacta.cEtab   = wcEtab
                                                AND bKacta.TypActa = w-typacta
                                                AND bKacta.cArt    = vcArt
                                                AND bKacta.cAct    = kact.cAct
                                                AND bKacta.Datef   = kactd.Datef /* i2-acta */
                                                AND bKacta.tLigFr  = FALSE
                                              NO-LOCK)
                    THEN STRING(kactd.lotq)
                    ELSE STRING(0.0))
               kacta.cunite:screen-value = kact.cunite
               kacta.Cunite:Modified     = No.

      if kacta.cunite:screen-value = "":U then 
         Assign kacta.cunite:screen-value = buf1-zart.cusaisie.

      /* Gestion du format des quantit�s en fonction de l'unit� */
      Assign Kacta.Qte:Format = {&unite-Format-qte} (Input Kacta.Cunite:SCREEN-VALUE, Input "z,zzz,zz":U)
             Kacta.Qte:Modified = No.

      If w-typacta <> {&TYPACTA-ART-COUT} Then do:
         Assign w-init-toartgen = Not kacta.toartgen:SENSITIVE
                kacta.toartgen:Sensitive = buf1-zart.tgene And Adm-Fields-Enabled
                kacta.toartgen:Hidden    = Not buf1-zart.tgene.
         If buf1-zart.tgene And w-init-toartgen And Adm-New-Record Then
            Assign kacta.toartgen:Checked = Yes.
      End.

      /* Article avec deux unit�s */
      If Not Test-Global Then 
          If W-T-2emeUnite And vcArt <> '' Then
            /* Affichage des zones concernant la 2�me unit� si l'article a au moins une unit� � coefficient variable */
            Run GestionAffichage2emeUnite(vcArt).
          Else 
             Run Affichage2emeUnite(No).

      If Not Test-Global Then Do:
         Run Calcul-quantite.
         /* << DD 20130624_0007 M135468 - Appel Init-Freinte apr�s Pr-GestionFreinte : */
         /*  Si apr�s Pr-Gestion-Freinte, txfr est affich�, alors il faut recalculer la freinte en fonction de l'article car celle-ci a pu �tre mise � z�ro pr�c�demment */
         RUN Pr-Gestion-Freinte (BUFFER kacta, INPUT w-TypActa, INPUT vcArt).
         Run Init-Freinte.
          /* >> DD 20130624_0007 M135468 */
      End.
   End.

   /* TEST GLOBAL    */
   If Test-Global Then DO WITH FRAME {&FRAME-NAME}:
      Find Current kactd No-Lock No-Error.
      Find First kact where kact.csoc  = kactd.csoc
                        and kact.cetab = kactd.cetab
                        and kact.cact  = kactd.cact
                      no-lock no-error.
      /* L'article de r�f�rence et l'article de freinte doivent avoir l'unit� de l'activit� */
      If available kact and available kactd and kact.cart = vCart And
         ((kact.typflux = {&TYPFLUX-TIRE}   And w-typacta = {&TYPACTA-SORTANT}) Or
          (kact.typflux = {&TYPFLUX-POUSSE} And w-typacta = {&TYPACTA-ENTRANT})) And
           kact.cunite <> kacta.cunite:SCREEN-VALUE  
      Then Do:
         {affichemsg.i &Message = "Get-TradMsg (004811, 'L''unit� est diff�rente de l''unit� de r�f�rence de l''activit�.':T) + '|':U + wnom-onglet"}
         Apply "Entry":U to kacta.cunite.
         Return "Adm-Error":U.
      End.
      
      /* Gestion de la ligne de freinte pour les sortants (Art.R�f. uniquement) */
      /* Si il existe une ligne de freinte, alors : */
      /* 1/ il doit exister une ligne du m�me art. non ligne de freinte        */
      /*   (mais sans taux de freinte si Art.R�f. en flux tir� - i.e. sortant) */
      /* 2/ les unit�s doivent �tre les m�mes que pour la ligne du m�me art. non ligne de freinte */
      IF AVAILABLE kact AND AVAILABLE kactd THEN DO:
     
         IF kacta.tLigFr:SCREEN-VALUE = "YES":U THEN
            FOR FIRST bKacta FIELDS (cUnite cU2 TxFr)
                              WHERE bKacta.cSoc    = wcSoc
                                AND bKacta.cEtab   = wcEtab
                                AND bKacta.TypActa = w-TypActa
                                AND bKacta.cArt    = vcArt
                                AND bKacta.cAct    = kact.cAct
                                AND bKacta.Datef   = kactd.Datef /* i2-acta */
                                AND bKacta.tLigFr  = FALSE
                              NO-LOCK:     
                ASSIGN vTrouve      = TRUE
                       vcUnite      = bKacta.cUnite
                       vcUnite2     = bKacta.cu2
                       vtAnoFreinte = (bKacta.TxFr > 0 
                                       AND kacta.tLigFr:SCREEN-VALUE IN FRAME {&FRAME-NAME} = "YES":U).
            END.
         ELSE
            FOR FIRST bKacta FIELDS (cUnite cU2 Qte)
                              WHERE bKacta.cSoc    = wcSoc
                                AND bKacta.cEtab   = wcEtab
                                AND bKacta.TypActa = w-TypActa
                                AND bKacta.cArt    = vcArt
                                AND bKacta.cAct    = kact.cAct
                                AND bKacta.Datef   = kactd.Datef /* i2-acta */
                                AND bKacta.tLigFr  = TRUE
                              NO-LOCK:     
                ASSIGN vTrouve      = TRUE
                       vcUnite      = bKacta.cUnite
                       vcUnite2     = bKacta.cu2
                       vtAnoFreinte = (bKacta.Qte > 0
                                       AND DECIMAL(kacta.TxFr:SCREEN-VALUE) > 0 ).
           END.
          
         IF NOT vTrouve AND kacta.tLigFr:SCREEN-VALUE = "YES":U THEN DO:
            {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (024522, 'Pour d�finir une ligne de freinte sur un article donn�, il doit exister une autre ligne non ligne de freinte portant sur le m�me article (&1).':T), 
                                      vcArt)"}
            APPLY "Entry":U TO kacta.cUnite.
            RETURN "Adm-Error":U.
         END.
         IF (vcUnite <> kacta.cUnite:SCREEN-VALUE AND vcUnite <> "":U)
            OR (vcUnite2 <> kacta.cU2:SCREEN-VALUE AND vcUnite2 <> "":U) THEN DO:
            {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (005815, 'Donn�es incoh�rentes (&1).':T), 
                                                 SUBSTITUTE(Get-TradTxt (011620, 'L''unit� de freinte doit �tre &1 ou &2.':T), 
                                                            vcUnite, vcUnite2) )"}
            APPLY "Entry":U TO kacta.cUnite.
            RETURN "Adm-Error":U.
         END.
         IF vtAnoFreinte THEN DO:
            /* NLE 30/07/2013 M137702 : devient un simple avertissement */
            {affichemsg.i       &Message = "SUBSTITUTE(Get-TradTxt (016492, 'Param�trage erron� (&1).':T), Get-TradMsg (034307, 'Cet article est � la fois g�r� en taux de freinte et ligne de freinte.':T))"}
/*             APPLY "Entry":U TO kacta.cUnite. */
/*             RETURN "Adm-Error":U.            */
         END.
          
         /* NLE 10/03/2008 M041063 */
         B1:
         FOR EACH bKacta FIELDS (tligfr ni1)
                         WHERE bKacta.csoc    = kactd.csoc
                           AND bKacta.cetab   = kactd.cetab
                           AND bKacta.typacta = w-typacta
                           AND bKacta.cart    = vcart
                           AND bKacta.cact    = kactd.cact
                           AND bKacta.datef   = kactd.datef
                         NO-LOCK:
            IF bKacta.tligfr  = (kacta.tLigFr:SCREEN-VALUE = "YES":U) AND
               (Adm-New-Record OR bKacta.ni1 <> kacta.ni1) THEN DO:
               {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (025643, 'AVERTISSEMENT : Il y a plusieurs fois l''article &1 dans cette activit�.':T), vcart)"}
               LEAVE B1.
            END.
         END.
      END.
       
      If available kact and available kactd and article-de-reference () AND
        /* Si une op�ration est d�finie pour cette activit� ses informations suplantent celles de ce viewer */
         (kactd.lotq <> decimal(kacta.qte:SCREEN-VALUE)) AND
           /* Si la dur�e de l'activit� est d�finie par les op�rations 
            * ( param�tre ACTIVITE dur�e de l'activit� = 1, W-T-Duree = True )
            * ou si aucune dur�e de l'activit� n'est renseign�e dans les dates d'effet de l'activit� 
            * alors on r�plique la qte de l'article de r�f�rence d�clar�es au niveau 
            * des entrants/sortants dans celle d�clar�e au niveau des dates d'effet 
            * pour garder la coh�rence entre ces 2 qte pour l'article de r�f�rence */
         Not ( ( Kactd.lotde = 0 And Kactd.lotds = 0 And Kactd.lotdt = 0 ) Or W-T-Duree ) 
      Then Do:
         {affichemsg.i &Message = "Get-TradMsg (004814, 'La quantit� est diff�rente de la quantit�|exprim�e avec la date d''effet.':T) + '|':U + wnom-onglet"}
         Apply "Entry":U to kacta.qte.
         Return "Adm-Error":U.
      End.
   End. /* Fin If Test-Global */

   /*--- Unit�  ---*/
   If Test-Global Or Widget-Test = kacta.cunite:Handle in Frame {&Frame-Name} Then Do With Frame {&Frame-Name}:
      {unitecc.i kacta.cunite yes no  " " "1" " "}
      {&Run-unite-Test-Unite-Article} (Input  wcsoc,
                                       Input  vcArt,
                                       Input  kacta.cunite:Screen-Value,
                                       Output O-unite-retour,
                                       Output O-unite-Mess).
      If Not O-unite-retour Then Do:
         {affichemsg.i &Message="O-unite-Mess + '|':U + wnom-onglet"}
         Apply "Entry":u to kacta.cunite in Frame {&Frame-Name}.
         Return "Adm-Error":U.
      End.
      /* Gestion du format des quantit�s en fonction de l'unit� */
      Assign Kacta.Qte:Format = {&unite-Format-qte} (Input Kacta.Cunite:screen-value , Input "z,zzz,zz":U).
      /* Article avec deux unit�s */
      If Not Test-Global Then Run Calcul-quantite.
   End.

   If Test-Global 
      Or Widget-Test = kacta.Qte:Handle in Frame {&Frame-Name} Then Do:
      { Verideci.i "decimal(kacta.Qte:Screen-Value In Frame {&Frame-Name})"  "kacta.Cunite:Screen-Value In Frame {&Frame-Name}" kacta.Qte}
      /* Article avec deux unit�s */
      If Not Test-Global Then Run Calcul-quantite.
   End.

   /*--- 2�me unit�  ---*/    
   If W-T-2emeUnite Then Do With Frame {&Frame-Name}:
      If Test-Global Or Widget-Test = kacta.cu2:Handle
      Then Do:
         {unitecc.i kacta.cu2 yes no  " " "2" " "}
         {&Run-unite-Test-Unite-Article} (Input  wcsoc,
                                          Input  vcArt,
                                          Input  kacta.cu2:Screen-Value,
                                          Output O-unite-retour,
                                          Output O-unite-Mess).
         If Not O-unite-retour Then Do:
            {affichemsg.i &Message="O-unite-Mess + '|':U + wnom-onglet"}
            Apply "Entry":u to kacta.cu2 in Frame {&Frame-Name}.
            Return "Adm-Error":U.
         End.
         /* Gestion du format des quantit�s en fonction de l'unit� */
         if kacta.cu2:Screen-Value <> "":U Then
            Assign Kacta.Qte2:Format = {&unite-Format-qte} (Input Kacta.cu2:SCREEN-VALUE, Input "z,zzz,zz":U).
         else if Not Test-global Then 
            Assign Kacta.qte2:screen-value = "0":U.

         If kacta.cu2:screen-value <> "":U And kacta.cu2:screen-value = kacta.cunite:screen-value Then Do:
            {affichemsg.i &Message = "Get-TradMsg (004818, 'Les deux unit�s ne peuvent �tre les m�mes.':T) + '|':U + wnom-onglet"}
            Apply "Entry":u to kacta.cu2.
            Return "Adm-Error":U.
         End.

         If Test-Global And
            kacta.cu2:screen-value <> "" And 
            Decimal(kacta.qte2:screen-value) = 0 And 
            Decimal(kacta.qte:screen-value) <> 0 
         Then Do:
            {affichemsg.i &Message = "Get-TradMsg (004822, 'Vous devez renseigner la quantit� de la 2�me unit�.':T) + '|':U + wnom-onglet"}          
            Apply "Entry":u to kacta.qte2.
            Return "Adm-Error":U.
         End.
      End.

      If Test-Global Or Widget-Test = kacta.Qte2:Handle in Frame {&Frame-Name} Then Do:
         { Verideci.i "decimal(kacta.Qte2:Screen-Value)"  "kacta.Cu2:Screen-Value" kacta.Qte2 No}  

         If Test-Global AND Decimal(kacta.qte2:screen-value) <> 0 And kacta.cu2:screen-value = "" Then Do:
            {affichemsg.i &Message = "Get-TradMsg (004828, 'Vous ne pouvez saisir une quantit� pour la 2�me unit� sans saisir cette derni�re.':T) + '|':U + wnom-onglet"}
            Apply "Entry":u to kacta.cu2.
            Return "Adm-Error":U.
         End.
      End.

      /* Article avec deux unit�s */
      If Not Test-Global Then Do :
         If Decimal(kacta.qte2:Screen-Value) = 0 Then Run Calcul-quantite.
      End.
      Else Do:
         If Not kacta.cu2:Hidden Then w-unit2 = kacta.cu2:Screen-Value.
                                 Else w-unit2 = "":U.
         w-unit1 = kacta.cunite:Screen-Value.
      End.
   End.

   /*--- D�pot ---*/
   If Test-Global Or Widget-Test = {&Table}.cdepot:Handle Then Do :            
      {depotcc.i {&Table}.cdepot yes yes zdepot.ldepot} 
      If Available Buf-zdepot
      Then If Buf-zdepot.temp /* Le d�p�t est par emplacement */
            THEN Assign {&Table}.cemp:sensitive = Adm-Fields-Enabled
                        B_AID-cemp:sensitive    = {&Table}.cemp:sensitive.
            Else Assign {&Table}.cemp:screen-value = "":u
                        zemp.lemp:screen-value     = "":u
                        {&Table}.cemp:sensitive    = False
                        B_AID-cemp:sensitive       = {&Table}.cemp:sensitive.
   End.

   If Test-Global Or Widget-Test = {&Table}.cemp:Handle Then Do:
      {empcc.i {&Table}.cemp yes yes zemp.lemp 1 {&Table}.cdepot:screen-value wcetab '' NO}
   End.  

    /*--- op�ration informatis�e ---*/    
    /* << KL 20130910_0007 D1662 : Bloc d�plac� dans actaajv.w (Onglet Atelier) */
/*     If Test-Global Or Widget-Test = F-copinf:Handle in Frame {&Frame-Name} Then Do with frame {&Frame-name}: */
/*        If F-copinf:screen-value in Frame {&Frame-Name} <> "" Then do:                                        */
/*           {acto2cc.i F-copinf YES yes kacto.loper 1 kactd.cact string(kactd.datef)}                          */
/*        End.                                                                                                  */
/*     End.                                                                                                     */
    /* >> KL 20130910_0007 D1662 */
                                                                                    

    If Test-Global Or Widget-Test = {&Table}.txfr:Handle In Frame {&Frame-Name} Then Do With Frame {&Frame-Name} :
      If  Decimal({&Table}.txfr:Screen-Value) <> 0.0 Then Do:
         Assign {&Table}.tligfr:Sensitive = No.
         /*
         *If {&Table}.tligfr:Checked Then Do:
         *   {affichemsg.i &Message = "Get-TradMsg (021588, 'Une ligne avec un taux de freinte ne peux pas �tre une ligne de freinte.| Voulez-vous d�cocher le t�moin ''Ligne de freinte'' ?':T)"
         *                 &TypeBtn = {&MsgOuiNon}
         *                 &TypeMsg = {&MsgQuestion}
         *                 &BtnDef  = 2 }
         *   If ChoixMsg = 1 Then /* Oui */
         *      Assign {&Table}.tligfr:Checked = No.
         *   Else Do:
         *      Apply "Entry":u to kacta.txfr.
         *      Return "Adm-Error":U.
         *   End.
         *End.
         */
      End.
      Else Assign {&Table}.tligfr:Sensitive = Adm-Fields-Enabled.
    End.
    

    If Test-Global Or Widget-Test = {&Table}.tligfr:Handle In Frame {&Frame-Name} Then Do:
       Run Pr-Ctrl-Tligfr( Output ret ).
       If Not ret Then Do:
          Apply "Entry":u To kacta.tligfr In Frame {&Frame-Name}.
          Return "Adm-Error":U.
       End.
    End.

    If Test-Global And {&Table}.tqconst:Checked And {&Table}.tqtedif:Checked
       /* Gestion de la freinte pour les sortants (ARt.r�f. uniquement) */
       /* On autorise une qt� fixe et un mvt de stock auto par diff�rence */
       /* pour les sortants lorsqu'il s'agit d'une ligne de freinte */
       AND (kacta.tLigFr:SCREEN-VALUE IN FRAME {&FRAME-NAME} <> "YES":U OR w-typacta <> {&TYPACTA-SORTANT}) Then Do:
       {affichemsg.i &Message = "Get-TradMsg (020858, 'Un article ne peut pas �tre g�r� � la fois en quantit� constante et par quantit� par diff�rence.':T) + '|':U + wnom-onglet"}
       Apply "Entry":u To {&Table}.tqconst.
       Return "Adm-Error":U.
    End.
    
    /* L'article de r�f�rence g�n�rique doit �tre obligatoirement substitu� */
    If Test-Global And buf1-zart.tgene And Not {&Table}.toartgen:Checked AND 
       article-de-reference ()
    Then Do:
       {affichemsg.i &Message = "Get-TradMsg (020936, 'L''article de r�f�rence, s''il est g�n�rique, doit �tre obligatoire.':T) + '|':U + wnom-onglet"}
       Apply "Entry":u To {&Table}.toartgen.
       Return "Adm-Error":U.
    End.
    
    RUN P-Test-Saisie-2(INPUT Test-Global, INPUT Widget-Test, INPUT wnom-onglet).
    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE P-Test-Saisie-2 V-table-Win 
PROCEDURE P-Test-Saisie-2 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

  DEFINE INPUT PARAMETER Test-Global AS LOGICAL   NO-UNDO.
  Define Input Parameter Widget-Test As Widget-Handle No-Undo.
  DEFINE INPUT PARAMETER iNomOnglet  AS CHARACTER NO-UNDO.
  
  DEFINE VARIABLE vRet         AS LOGICAL   NO-UNDO.
  DEFINE VARIABLE vMsg         AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vtMotif      AS LOGICAL   NO-UNDO.
  DEFINE VARIABLE vHdlCriv2jv  AS HANDLE    NO-UNDO. /* Handle du viewer des crit�res */
  DEFINE VARIABLE vCle-Crifa   AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vCfmact      AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vDatef       AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vcAct        AS CHARACTER NO-UNDO.
  
  
  DEFINE BUFFER bKact FOR kact.

  Do With Frame {&Frame-Name}:

     /* CD DP2374 la valeur du crit�re est obligatoire d�s que l'article est coch� comme participant au rendement */
     IF Test-Global AND 
        {&TABLE}.trdtdyn:SENSITIVE AND
        {&TABLE}.trdtdyn:CHECKED   AND 
        {&TABLE}.valcridyn:SENSITIVE AND
        DECIMAL({&TABLE}.valcridyn:SCREEN-VALUE) <= 0 THEN DO:
         {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (012640, 'Cette zone est obligatoire (&1).':T), Get-TradTxt (034233, 'Valeur du crit�re de r�f�rence':T))"}
         Apply "Entry":U to  {&TABLE}.valcridyn.
         Return "Adm-Error":U.
     END.
     
     
     If Test-Global And {&Table}.tqconst:Checked And {&Table}.trdtdyn:CHECKED Then Do:
       /* Erreur si article qt� fixe et participe au rendement */
       {affichemsg.i &Message = "Get-TradMsg (034245, 'Un article ne peut pas �tre g�r� � la fois en quantit� constante et participer au rendement dynamique':T) + '|':U + iNomOnglet"}
       Apply "Entry":u To {&Table}.trdtdyn.
       Return "Adm-Error":U.
    End.
     
     /* contr�les sur valeur du crit�re teneur */
     If Test-Global 
      Or ({&TABLE}.valcridyn:SENSITIVE And Widget-Test = {&TABLE}.valcridyn:Handle) Then Do : 
         /* -------------------------------------- */
         /* Recherche du Handle de l'objet crit�re */
         /* -------------------------------------- */
         RUN Get-Link-One-Object IN adm-broker-hdl
              (INPUT  THIS-PROCEDURE,
               INPUT  "Criteres-Target":U, /* "Group-Assign-Target":U,*/
               OUTPUT vHdlCriv2jv).
         IF VALID-HANDLE(vHdlCriv2jv) Then 
            RUN Get-Tt-Criv IN vHdlCriv2jv (OUTPUT TABLE TT-criv).
         
         FOR FIRST bKact FIELDS (cfmact cact)
                         WHERE bKact.cSoc  = kactd.cSoc
                           AND bKact.cEtab = kactd.cEtab
                           AND bKact.cAct  = kactd.cAct
                         NO-LOCK:
            ASSIGN vCfmact = bKact.cfmact
                   vcAct   = bKact.cAct.                 
         END.
         
         ASSIGN vDatef = (IF kactd.Datef NE ? THEN STRING(kactd.Datef,'99/99/9999':U) ELSE "").
         Assign vCle-Crifa = (F-cArtvu:SCREEN-VALUE) + '�':U +
                               'RDTDYN':u       + '�':U +
                               vDatef            + '�':U +
                               (IF vCfmact <> "" THEN vCfmact + ',':U + vcAct ELSE '').
                               
         /* RUN Send-Info (THIS-PROCEDURE, "Info-Acte":U, OUTPUT vInfo-Acte). */ 
         
         {&Run-CriteresS-Controles} (INPUT  wcSoc,
                                     INPUT  wcEtab,
                                     INPUT  vCle-Crifa,
                                     INPUT  "":u,  /* vInfo-Acte */
                                     INPUT  F-cArtvu:SCREEN-VALUE,
                                     INPUT  F-CritTen:SCREEN-VALUE,
                                     INPUT  {&ZCRIFA-MSAISIE-UNIQUE},
                                     INPUT  "":U,
                                     INPUT  DECIMAL({&TABLE}.valcridyn:SCREEN-VALUE),  
                                     INPUT  ?,     /* TT-criv2.Valdmin */
                                     INPUT TABLE  TT-criv,
                                     INPUT TABLE  TT-klotq,     
                                     INPUT TABLE  TT-klotqc-mod, 
                                     INPUT TABLE  TT-klotqe,   
                                     OUTPUT vRet,
                                     OUTPUT vtMotif,
                                     OUTPUT vMsg).
         IF NOT vRet OR vMsg <> "":U THEN DO:
            {affichemsg.i &message = "vMsg"}
            APPLY "ENTRY":U TO Widget-Test.
            RETURN "ADM-ERROR":U.
         END.

      End.
   
     
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-Ctrl-Tligfr V-table-Win 
PROCEDURE Pr-Ctrl-Tligfr :
/*------------------------------------------------------------------------------
  Purpose:     Contr�le qu'une ligne coch�e "Ligne de freinte" pour un article 
               de r�f�rence n'est pas la seule ligne pour cette article
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
Define Output Parameter o-ret As Logical    No-Undo.

Define Buffer b-kacta       For kacta.
Define Buffer b-kact        For kact.

Define Variable wl-typacta  As Character    No-Undo.
Define Variable w-art-ref   As Logical      No-Undo.
Define Variable w-cart-ref  As Character    No-Undo.
Define Variable w-typflux   As Character    No-Undo.

   For First b-kact Fields (cart typflux)
                     Where b-kact.csoc  = kactd.csoc
                       And b-kact.cetab = kactd.cetab
                       And b-kact.cact  = kactd.cact
   No-Lock:
      Assign w-cart-ref = b-kact.cart
             w-typflux  = b-kact.typflux.
   End.
  
   /* Si le t�moin Ligne de freinte n'est pas coch�  ou bien, si l'article */
   /* de la ligne n'est pas l'art. de r�f. de l'activit�                   */
   /* -------------------------------------------------------------------- */
   If Not kacta.tligfr:Checked In Frame {&Frame-Name}
      Or w-cart-ref <> F-cartvu:Screen-Value In Frame {&Frame-Name} 
   Then Do: 
      /* Pas besoin de test suppl�mentaire : on sort sans erreur */ 
      Assign o-ret = True.
      Return.
   End.
   
   /* Soit la ligne est coch�e ligne de freinte,     */
   /* soit la ligne porte sur l'article de r�f�rence */
   Case w-typflux:
      When {&TYPFLUX-POUSSE} Then Assign wl-typacta = {&TYPACTA-ENTRANT}.
      When {&TYPFLUX-TIRE}   Then Assign wl-typacta = {&TYPACTA-SORTANT}.
   End Case.

   If wl-typacta <> w-typacta Then Do:
      /* Pas besoin de test suppl�mentaire : on sort sans erreur */
      Assign o-ret = True.
      Return.
   End.
   
   Assign w-art-ref = Adm-New-Record.
   For First b-kacta Fields (ni1)
                      Where b-kacta.csoc    = kactd.csoc
                        And b-kacta.cetab   = kactd.cetab
                        And b-kacta.cact    = kactd.cact
                        And b-kacta.datef   = kactd.datef
                        And b-kacta.typacta = w-typacta
                        And b-kacta.cart    = w-cart-ref
                     No-Lock By b-kacta.csoc
                             By b-kacta.cetab
                             By b-kacta.cact
                             By b-kacta.datef
                             By b-kacta.typacta
                             By b-kacta.nlig :
      If Not Adm-New-Record And Available kacta And b-kacta.ni1 = kacta.ni1 Then Do:
         {affichemsg.i &Message = "Get-TradMsg (021592, 'L''article de r�f�rence ne peut pas �tre une ligne de freinte.':T) + '|':U + Get-Label-Folder()"}
         Assign o-ret = False.
         Return.
      End.
      Assign w-art-ref = No.
   End.
   
   If w-art-ref Then Do:
      {affichemsg.i &Message = "Get-TradMsg (021592, 'L''article de r�f�rence ne peut pas �tre une ligne de freinte.':T) + '|':U + Get-Label-Folder()"}
      Assign o-ret = False.
      Return.
   End.
      
   Assign o-ret = True.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-Gestion-Freinte V-table-Win 
PROCEDURE Pr-Gestion-Freinte :
/*------------------------------------------------------------------------------
  Purpose:     En mode modification, affichage et activation des zones Taux de   
               freinte et Ligne de freinte en fonction du reste des entrants 
               (resp. sortants) de l'activit�
  Parameters:  <none>
  Notes:       Cr�ation MB D002845 (II) 16/11/07 
------------------------------------------------------------------------------*/
   DEFINE PARAMETER BUFFER bpKacta FOR kacta. /* buffer de la ligne courante */
   DEFINE INPUT PARAMETER iTypActa AS CHARACTER NO-UNDO.
   DEFINE INPUT PARAMETER icArt    AS CHARACTER NO-UNDO. /* article � l'�cran */
 
   DEFINE VARIABLE vtExistArt    AS LOGICAL   NO-UNDO. /* Existe-t-il d�j� un kacta non ligne de freinte 
                                                        * pour l'article courant dans l'activit� ? */
   DEFINE VARIABLE vcArtRef      AS CHARACTER NO-UNDO. /* Article de r�f�rence de l'activit� */
     
   DEFINE BUFFER bKact  FOR kact.   
   DEFINE BUFFER bKacta FOR kacta.  
/* -------------------------------------------------------------------------- */

   IF NOT AVAILABLE bpKacta THEN RETURN "":U.

   DO WITH FRAME {&FRAME-NAME}:

      IF Adm-New-Record THEN DO:
                /* Recherche si l'article courant existe comme entrant (resp. sortant) */
                /* de l'activit� (non ligne de freinte) */
         ASSIGN vtExistArt = (icArt <> "":U
                              AND CAN-FIND (FIRST bKacta WHERE bKacta.cSoc    = wcSoc
                                                           AND bKacta.cEtab   = wcEtab
                                                           AND bKacta.TypActa = w-TypActa
                                                           AND bKacta.cAct    = kactd.cAct
                                                           AND bKacta.cArt    = icArt
                                                           AND bKacta.Datef   = kactd.Datef /* i2-acta */
                                                           AND bKacta.tLigFr  = FALSE
                                                         NO-LOCK)).
      END.
      ELSE DO:
                /* Recherche si l'article courant existe comme entrant (resp. sortant) */
                /* de l'activit� (non ligne de freinte) */
         ASSIGN vtExistArt = (icArt <> "":U
                              AND CAN-FIND (FIRST bKacta WHERE bKacta.cSoc    = wcSoc
                                                           AND bKacta.cEtab   = wcEtab
                                                           AND bKacta.TypActa = w-TypActa
                                                           AND bKacta.cAct    = kactd.cAct
                                                           AND bKacta.cArt    = icArt
                                                           AND bKacta.Datef   = kactd.Datef /* i2-acta */
                                                           AND bKacta.tLigFr  = FALSE
                                                           AND ROWID(bKacta)  <> ROWID(bpKacta) 
                                                         NO-LOCK)).
      END.
      
      CASE iTypActa :
         /* Entrants : le taux de freinte n'est affich� que si l'article */
         /* n'appara�t pas d�j� en entr�e de l'activit�, et inversement, */
         /* le t�moin Ligne de freinte n'appara�t que si l'article       */
         /* appara�t d�j� en entr�e de l'activit�                        */
         /* Taux de freinte et Ligne de freinte INCOMPATIBLES pour un m�me entrant */
         WHEN {&TYPACTA-ENTRANT} THEN 
            ASSIGN kacta.TxFr:HIDDEN   = (vgtCacherFreinte /*OR vtExistArt*/ )
                   F-%:HIDDEN          = (vgtCacherFreinte /*OR vtExistArt*/ )
                   kacta.tLigFr:HIDDEN = (vgtCacherFreinte /*OR NOT vtExistArt*/ ). 
        
         /* Sortants (Art.R�f. uniquement) : le taux de freinte n'est affich�      */
         /* que si l'article de r�f. n'appara�t pas d�j� en sortie de l'activit�   */  
         /* et inversement, le t�moin Ligne de freinte n'appara�t que si           */
         /* l'article appara�t d�j� en sortie de l'activit�                        */
         /* Taux de freinte et Ligne de freinte INCOMPATIBLES pour un m�me sortant */
         WHEN {&TYPACTA-SORTANT} Then DO:
            /* Recherche de l'article de r�f�rence de l'activit� */
            FOR FIRST bKact FIELDS (cArt)
                            WHERE bKact.cSoc  = kactd.cSoc
                              AND bKact.cEtab = kactd.cEtab
                              AND bKact.cAct  = kactd.cAct
                            NO-LOCK:
               ASSIGN vcArtRef = bKact.cArt.                 
            END.
           
            IF F-cArtVu:SCREEN-VALUE = vcArtRef THEN
               ASSIGN kacta.TxFr:HIDDEN   = (vgtCacherFreinte OR vtExistArt)
                      F-%:HIDDEN          = (vgtCacherFreinte OR vtExistArt)
                      kacta.tLigFr:HIDDEN = (vgtCacherFreinte OR NOT vtExistArt).
            ELSE
               ASSIGN kacta.TxFr:SENSITIVE = NO
                      kacta.TxFr:HIDDEN    = YES
                      F-%:HIDDEN           = YES
                      kacta.tLigFr:HIDDEN  = (vgtCacherFreinte OR NOT vtExistArt).
         END.
         
         OTHERWISE
            ASSIGN kacta.TxFr:SENSITIVE = NO
                   kacta.TxFr:HIDDEN    = YES
                   F-%:HIDDEN           = YES
                   kacta.tLigFr:HIDDEN  = YES.
      END CASE.
      
      /* Quand on cache les zones de freinte, on r�initialise leurs valeurs */
      IF kacta.TxFr:HIDDEN THEN 
         ASSIGN kacta.TxFr:SCREEN-VALUE = STRING(0.0).
      
      IF kacta.tLigFr:HIDDEN THEN 
         ASSIGN kacta.tLigFr:CHECKED    = FALSE. 
      
      /* En mode modification, si les zones sont affich�es, */
      /* alors elles doivent �tre modifiables */
      IF Adm-Fields-Enabled THEN
         ASSIGN kacta.TxFr:SENSITIVE   = (NOT kacta.TxFr:HIDDEN)
                kacta.tLigFr:SENSITIVE = (NOT kacta.tLigFr:HIDDEN).
      
   END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-Maj-QteRef-Datef V-table-Win 
PROCEDURE Pr-Maj-QteRef-Datef :
/*------------------------------------------------------------------------------
  Purpose:     Proc�dure calculant, pour l'article de r�f�rence, la qt� de 
               r�f�rence de la date d'effet (� partir des qt�s de l'article de 
               r�f�rence)
               Appel�e par l'assign-statement et le delete-statement
  Parameters:  <none>
  Notes:       Cr�ation MB D002845 (II) 23/11/07
------------------------------------------------------------------------------*/
   DEFINE PARAMETER BUFFER bpKacta FOR kacta.
   DEFINE INPUT PARAMETER iTypFlux AS CHARACTER NO-UNDO.
   
   DEFINE VARIABLE vTypFlux AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vtArtRef AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vQteRef  AS DECIMAL   NO-UNDO.
   DEFINE BUFFER bKacta   FOR kacta.
   DEFINE BUFFER bufKactd FOR kactd.
/* -------------------------------------------------------------------------- */

   IF NOT AVAILABLE bpKacta THEN RETURN "":U.
   
   /* MAJ de la qt� de r�f�rence de l'activit� (kactd.lotq) � partir */
   /* de la qt� de l'activit� pour l'art. de r�f�rence (kacta.qte)   */
   
   /* MB D002845 15/10/07 - Gestion de la freinte pour les sortants (Art.R�f. uniquement) */
   /* Si on est en flux tir�, l'art. de r�f. est un sortant,    */
   /* la qt� de r�f. est alors la qt� de l'activit� pour l'art. */
   /* de r�f. � laquelle s'ajoute la qt� de freinte             */
   IF iTypFlux = {&TYPFLUX-TIRE} AND bpKacta.TypActa = {&TYPACTA-SORTANT} THEN
      FOR EACH bKacta FIELDS (Qte Txfr)
                       WHERE bKacta.cSoc    = wcSoc
                         AND bKacta.cEtab   = wcEtab
                         AND bKacta.TypActa = bpKacta.TypActa
                         AND bKacta.cArt    = bpKacta.cArt
                         AND bKacta.cAct    = bpKacta.cAct
                         AND bKacta.Datef   = bpKacta.Datef  /* i2-acta */
                       NO-LOCK:         
         ASSIGN vQteRef = vQteRef + (bKacta.Qte / (1 - (bKacta.TxFr / 100))). 
      END.
   /* Sinon, on est en flux pouss�, l'art. de r�f. est un entrant,      */
   /* la qt� de r�f. est alors la qt� de l'activit� pour l'art. de r�f. */
   ELSE 
      ASSIGN vQteRef = DECIMAL(kacta.Qte:SCREEN-VALUE IN FRAME {&FRAME-NAME}).
   
   IF kactd.lotq <> vQteRef /*decimal(kacta.qte:screen-value in frame {&FRAME-NAME})*/
   THEN DO:
       FIND FIRST bufKactd WHERE ROWID (bufKactd) = ROWID (kactd) 
                           EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
       IF AVAILABLE bufKactd THEN DO:
           ASSIGN bufKactd.Lotq = /*decimal( kacta.qte:Screen-Value In Frame {&FRAME-NAME} )*/
                                 vQteRef.
           VALIDATE bufKactd.
       END.
   END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info V-table-Win 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.

   Define Variable w-ni1    As Integer      No-Undo.
   Define Buffer b-kacta    For kacta.
   
   Case Name-Info :
      /* Ins�rer ici vos propres infos � envoyer */
      When "cle-com":U Then Do:
         If Adm-New-Record And Not Available kacta Then Do:
            Find Last b-kacta Where b-kacta.csoc    = wcsoc
                                And b-kacta.cetab   = wcetab
                                And b-kacta.cact    = kactd.cact
                                And b-kacta.datef   = kactd.datef
                                And b-kacta.typacta = w-typacta
                              No-Lock No-Error.
            If Available b-kacta Then 
                w-ni1 = b-kacta.ni1 + 1.
            Else 
                w-ni1 = 1.
            Valeur-Info = kactd.cact + ',':U
                        + String(kactd.datef, '99/99/9999':U) + ',':U
                        + w-typacta + ',':U
                        + String(w-ni1).
         End.
         Else
            If Available kacta Then
               Valeur-Info = kacta.cact + ',':U
                           + String(kacta.datef, '99/99/9999':U) + ',':U
                           + kacta.typacta + ',':U
                           + String(kacta.ni1).
            Else
               Valeur-Info = "":U.
      End.
      WHEN 'cart':U  THEN valeur-info = IF AVAILABLE kacta THEN kacta.cart ELSE "":U.
      WHEN 'datef':U THEN valeur-info = IF AVAILABLE kacta THEN String(kacta.datef, '99/99/9999':U) + ',':U ELSE "":U.
      Otherwise 
         /* Par d�faut, envoi de la Screen-Value du widget dont 
          * le nom est contenu dans Name-Info */
         Run Brokvif-Send-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kacta"}
  {src/adm/template/snd-list.i "kactd"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
  
      WHEN "RAZ-memCart" THEN vgMemCart = "":U. /* CD Q7432 20140910_0001 10/09/2014 pour bloquer le passage en mode cr�ation � l'ouverture */ 
  
     
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {admvif/template/vinclude.i "vstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE zzEnable-crges V-table-Win 
PROCEDURE zzEnable-crges :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Define Input Parameter i-replacement-curseur    As Logical  No-Undo.                 */
/*                                                                                      */
/* Define Variable w-crges AS CHARACTER No-Undo.                                        */
/* /* NB 02/12/2003 : on remplace 0 par 0.0 + coefcout est un d�cimal non un integer */ */
/* Do With Frame {&Frame-Name}:                                                         */
/*     Run Get-Valeur-Combo( cb-crges:Handle                                            */
/*                         , Output w-crges ).                                          */
/*     Case w-crges:                                                                    */
/*         When {&KACTA-CRGES-COEFFICIENT-REPARTITION} Then Do:                         */
/*             Assign kacta.coefcout:Sensitive = Adm-fields-enabled                     */
/*                    kacta.coefcout:Hidden    = No                                     */
/*                    f-cout-coef:Hidden       = Yes.                                   */
/*             If  Available kacta And kacta.coefcout <> 0.0                            */
/*             And Decimal(kacta.coefcout:Screen-Value) = 0.0 Then                      */
/*                 Display kacta.coefcout With Frame {&Frame-Name}.                     */
/*             If i-replacement-curseur Then                                            */
/*                Apply "Entry":U To kacta.coefcout In Frame {&Frame-Name}.             */
/*         End.                                                                         */
/*         /* SLe 22/12/2010 D000662 Distinction CTART pr�fix� et coeff de prix */      */
/*         When {&KACTA-CRGES-COUT-PREFIXE} Then Do:                                    */
/*             Assign kacta.coefcout:Sensitive     = No                                 */
/*                    kacta.coefcout:Hidden        = Yes                                */
/*                    kacta.coefcout:Screen-Value  = '0.0':U                            */
/*                    f-cout-coef:Hidden           = No                                 */
/*                    f-cout-coef:Screen-Value     = w-cout-prefixe.                    */
/*         End.                                                                         */
/*         When {&KACTA-CRGES-COEFFICIENT-DE-PRIX} Then Do:                             */
/*             Assign kacta.coefcout:Sensitive     = No                                 */
/*                    kacta.coefcout:Hidden        = Yes                                */
/*                    kacta.coefcout:Screen-Value  = '0.0':U                            */
/*                    f-cout-coef:Hidden           = No                                 */
/*                    f-cout-coef:Screen-Value     = w-cout-coef.                       */
/*         End.                                                                         */
/*         /* --- */                                                                    */
/*         Otherwise Do:                                                                */
/*             Assign kacta.coefcout:Sensitive     = No                                 */
/*                    kacta.coefcout:Hidden        = Yes                                */
/*                    kacta.coefcout:Screen-Value  = '0.0':U                            */
/*                    f-cout-coef:Hidden           = Yes.                               */
/*         End.                                                                         */
/*     End Case.                                                                        */
/* End.                                                                                 */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE zzPr-Test-Coefcout V-table-Win 
PROCEDURE zzPr-Test-Coefcout :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* Define Output Parameter o-ret   As Logical  No-Undo.                                                                                                                                                                                                    */
/*                                                                                                                                                                                                                                                         */
/* Define Variable w-crges As Character    No-Undo.                                                                                                                                                                                                        */
/*                                                                                                                                                                                                                                                         */
/*     If w-typacta = {&TYPACTA-SORTANT} Then Do:                                                                                                                                                                                                          */
/*        Run Get-Valeur-Combo( cb-crges:Handle In Frame {&Frame-Name}                                                                                                                                                                                     */
/*                            , Output w-crges ).                                                                                                                                                                                                          */
/*        If w-crges = {&KACTA-CRGES-COEFFICIENT-REPARTITION} Then Do:                                                                                                                                                                                     */
/*           If Decimal (kacta.coefcout:screen-value) > 100  Then Do:                                                                                                                                                                                      */
/*              {affichemsg.i &Message = "Get-TradMsg (004841, 'Le coefficient de r�partion des co�ts ne peut pas �tre sup�rieur � 100.':T) + '|':U + Get-Label-Folder()"}                                                                                 */
/*              Assign o-ret = No.                                                                                                                                                                                                                         */
/*              Return.                                                                                                                                                                                                                                    */
/*           End.                                                                                                                                                                                                                                          */
/*                                                                                                                                                                                                                                                         */
/*           /* NB 19/09/2003 */                                                                                                                                                                                                                           */
/*           If Decimal (kacta.coefcout:screen-value) = 0.0 Then Do:                                                                                                                                                                                       */
/*              /* On demande si l'utilisateur veut passer en co�t nul ou changer la valeur */                                                                                                                                                             */
/*              {affichemsg.i &Message = "Get-TradMsg (021496, 'Le coefficient de r�partition d''un sortant ne peut pas �tre nul.|Cela revient � utiliser la r�gle co�t nul.|Confirmez-vous le co�t nul de cet article ?':T) + '|':U + Get-Label-Folder()" */
/*                            &TypeBtn = {&MsgOuiNon}                                                                                                                                                                                                      */
/*                            &TypeMsg = {&MsgQuestion}                                                                                                                                                                                                    */
/*                            &BtnDef = 1                                                                                                                                                                                                                  */
/*              }                                                                                                                                                                                                                                          */
/*              If ChoixMsg = 1 /* Oui */ Then Do:                                                                                                                                                                                                         */
/*                 Run Set-Valeur-Combo( cb-crges:Handle In Frame {&Frame-Name}                                                                                                                                                                            */
/*                                     , {&KACTA-CRGES-COUT-NUL} ).                                                                                                                                                                                        */
/*              End.                                                                                                                                                                                                                                       */
/*              Else Do: /* Non */                                                                                                                                                                                                                         */
/*                 Assign o-ret = No.                                                                                                                                                                                                                      */
/*                 Return.                                                                                                                                                                                                                                 */
/*              End.                                                                                                                                                                                                                                       */
/*           End.                                                                                                                                                                                                                                          */
/*        End.                                                                                                                                                                                                                                             */
/*                                                                                                                                                                                                                                                         */
/*        Run enabled-fields.                                                                                                                                                                                                                              */
/*                                                                                                                                                                                                                                                         */
/*     End.                                                                                                                                                                                                                                                */
/*     Assign o-ret = True.                                                                                                                                                                                                                                */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK  _PROCEDURE CURRENT-WINDOW-layouts _LAYOUT-CASES
PROCEDURE CURRENT-WINDOW-layouts:
  DEFINE INPUT PARAMETER layout AS CHARACTER                     NO-UNDO.
  DEFINE VARIABLE lbl-hndl AS WIDGET-HANDLE                      NO-UNDO.
  DEFINE VARIABLE widg-pos AS DECIMAL                            NO-UNDO.

  /* Copy the name of the active layout into a variable accessible to   */
  /* the rest of this file.                                             */
  CURRENT-WINDOW-layout = layout.

  CASE layout:
    WHEN "Master Layout" THEN DO:
      ASSIGN
         B_AID-4:HIDDEN IN FRAME F-Main                    = yes
         B_AID-4:ROW IN FRAME F-Main                       = 4.05
         B_AID-4:HIDDEN IN FRAME F-Main                    = no
         B_AID-4:HIDDEN IN FRAME F-Main                    = no.

      ASSIGN
         B_AID-cemp:HIDDEN IN FRAME F-Main                 = yes
         B_AID-cemp:ROW IN FRAME F-Main                    = 5.1
         B_AID-cemp:HIDDEN IN FRAME F-Main                 = no
         B_AID-cemp:HIDDEN IN FRAME F-Main                 = no.

      ASSIGN
         kacta.cdepot:HIDDEN IN FRAME F-Main               = yes
         widg-pos = kacta.cdepot:ROW IN FRAME F-Main 
         kacta.cdepot:ROW IN FRAME F-Main                  = 4.05
         lbl-hndl = kacta.cdepot:SIDE-LABEL-HANDLE IN FRAME F-Main 
         lbl-hndl:ROW = lbl-hndl:ROW + kacta.cdepot:ROW IN FRAME F-Main  - widg-pos
         kacta.cdepot:WIDTH IN FRAME F-Main                = 19.2
         kacta.cdepot:HIDDEN IN FRAME F-Main               = no
         kacta.cdepot:HIDDEN IN FRAME F-Main               = no.

      ASSIGN
         kacta.cemp:HIDDEN IN FRAME F-Main                 = yes
         widg-pos = kacta.cemp:ROW IN FRAME F-Main 
         kacta.cemp:ROW IN FRAME F-Main                    = 5.1
         lbl-hndl = kacta.cemp:SIDE-LABEL-HANDLE IN FRAME F-Main 
         lbl-hndl:ROW = lbl-hndl:ROW + kacta.cemp:ROW IN FRAME F-Main  - widg-pos
         kacta.cemp:HIDDEN IN FRAME F-Main                 = no
         kacta.cemp:HIDDEN IN FRAME F-Main                 = no.

      ASSIGN
         F-CritTen:HIDDEN IN FRAME F-Main                  = yes
         F-CritTen:ROW IN FRAME F-Main                     = 6.52
         F-CritTen:HIDDEN IN FRAME F-Main                  = no.

      ASSIGN
         F-cuCriDyn:HIDDEN IN FRAME F-Main                 = yes
         F-cuCriDyn:ROW IN FRAME F-Main                    = 6.33
         F-cuCriDyn:HIDDEN IN FRAME F-Main                 = no
         F-cuCriDyn:HIDDEN IN FRAME F-Main                 = no.

      ASSIGN
         zdepot.ldepot:HIDDEN IN FRAME F-Main              = yes
         zdepot.ldepot:ROW IN FRAME F-Main                 = 4.05
         zdepot.ldepot:HIDDEN IN FRAME F-Main              = no
         zdepot.ldepot:HIDDEN IN FRAME F-Main              = no.

      ASSIGN
         zemp.lemp:HIDDEN IN FRAME F-Main                  = yes
         zemp.lemp:ROW IN FRAME F-Main                     = 5.1
         zemp.lemp:HIDDEN IN FRAME F-Main                  = no
         zemp.lemp:HIDDEN IN FRAME F-Main                  = no.

      ASSIGN
         kacta.tauto:HIDDEN IN FRAME F-Main                = no.

      ASSIGN
         kacta.teprod:HIDDEN IN FRAME F-Main               = no.

      ASSIGN
         kacta.tligfr:HIDDEN IN FRAME F-Main               = no.

      ASSIGN
         kacta.toartgen:HIDDEN IN FRAME F-Main             = no.

      ASSIGN
         kacta.tqconst:HIDDEN IN FRAME F-Main              = yes
         kacta.tqconst:HEIGHT IN FRAME F-Main              = .81
         kacta.tqconst:HIDDEN IN FRAME F-Main              = no.

      ASSIGN
         kacta.tqtedif:HIDDEN IN FRAME F-Main              = no.

      ASSIGN
         kacta.trdtdyn:HIDDEN IN FRAME F-Main              = yes
         kacta.trdtdyn:ROW IN FRAME F-Main                 = 6.48
         kacta.trdtdyn:HIDDEN IN FRAME F-Main              = no
         kacta.trdtdyn:HIDDEN IN FRAME F-Main              = no.

      ASSIGN
         kacta.trdtm:HIDDEN IN FRAME F-Main                = no.

      ASSIGN
         kacta.valcridyn:HIDDEN IN FRAME F-Main            = yes
         kacta.valcridyn:ROW IN FRAME F-Main               = 6.33
         kacta.valcridyn:HIDDEN IN FRAME F-Main            = no
         kacta.valcridyn:HIDDEN IN FRAME F-Main            = no.

    END.  /* Master Layout Layout Case */

    WHEN "cout":U THEN DO:
      ASSIGN
         B_AID-4:HIDDEN IN FRAME F-Main                    = yes
         B_AID-4:ROW IN FRAME F-Main                       = 5.1.

      ASSIGN
         B_AID-cemp:HIDDEN IN FRAME F-Main                 = yes
         B_AID-cemp:ROW IN FRAME F-Main                    = 6.14.

      ASSIGN
         kacta.cdepot:HIDDEN IN FRAME F-Main               = yes
         widg-pos = kacta.cdepot:ROW IN FRAME F-Main 
         kacta.cdepot:ROW IN FRAME F-Main                  = 5.1
         lbl-hndl = kacta.cdepot:SIDE-LABEL-HANDLE IN FRAME F-Main 
         lbl-hndl:ROW = lbl-hndl:ROW + kacta.cdepot:ROW IN FRAME F-Main  - widg-pos
         kacta.cdepot:WIDTH IN FRAME F-Main                = 8.

      ASSIGN
         kacta.cemp:HIDDEN IN FRAME F-Main                 = yes
         widg-pos = kacta.cemp:ROW IN FRAME F-Main 
         kacta.cemp:ROW IN FRAME F-Main                    = 6.14
         lbl-hndl = kacta.cemp:SIDE-LABEL-HANDLE IN FRAME F-Main 
         lbl-hndl:ROW = lbl-hndl:ROW + kacta.cemp:ROW IN FRAME F-Main  - widg-pos.

      ASSIGN
         F-CritTen:HIDDEN IN FRAME F-Main                  = yes
         F-CritTen:ROW IN FRAME F-Main                     = 7.57
         F-CritTen:HIDDEN IN FRAME F-Main                  = no.

      ASSIGN
         F-cuCriDyn:HIDDEN IN FRAME F-Main                 = yes
         F-cuCriDyn:ROW IN FRAME F-Main                    = 7.38.

      ASSIGN
         zdepot.ldepot:HIDDEN IN FRAME F-Main              = yes
         zdepot.ldepot:ROW IN FRAME F-Main                 = 5.1.

      ASSIGN
         zemp.lemp:HIDDEN IN FRAME F-Main                  = yes
         zemp.lemp:ROW IN FRAME F-Main                     = 6.14.

      ASSIGN
         kacta.tauto:HIDDEN IN FRAME F-Main                = yes.

      ASSIGN
         kacta.teprod:HIDDEN IN FRAME F-Main               = yes.

      ASSIGN
         kacta.tligfr:HIDDEN IN FRAME F-Main               = yes.

      ASSIGN
         kacta.toartgen:HIDDEN IN FRAME F-Main             = yes.

      ASSIGN
         kacta.tqconst:HIDDEN IN FRAME F-Main              = yes
         kacta.tqconst:HEIGHT IN FRAME F-Main              = 1.05
         kacta.tqconst:HIDDEN IN FRAME F-Main              = no.

      ASSIGN
         kacta.tqtedif:HIDDEN IN FRAME F-Main              = yes.

      ASSIGN
         kacta.trdtdyn:HIDDEN IN FRAME F-Main              = yes
         kacta.trdtdyn:ROW IN FRAME F-Main                 = 7.52.

      ASSIGN
         kacta.trdtm:HIDDEN IN FRAME F-Main                = yes.

      ASSIGN
         kacta.valcridyn:HIDDEN IN FRAME F-Main            = yes
         kacta.valcridyn:ROW IN FRAME F-Main               = 7.38.

    END.  /* cout Layout Case */

  END CASE.
END PROCEDURE.  /* CURRENT-WINDOW-layouts */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Article-de-reference V-table-Win 
FUNCTION Article-de-reference RETURNS LOGICAL PRIVATE
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  Retourne 
            - VRAI si le kacta courant est le kacta de l'article de r�f�rence, 
            - FAUX si le kacta courant est le kacta de l'article de r�f�rence Ligne de freinte
    Notes:  
------------------------------------------------------------------------------*/
   Define Buffer b2Kacta For kacta.

   /* Le kacta de l'article de r�f�rence est g�n�r� � la cr�ation de la date d'effet */
   If Not Adm-New-Record And         
      kact.cart = kacta.cart And
       ((kact.typflux = {&TYPFLUX-TIRE}   And kacta.typacta = {&TYPACTA-SORTANT}) Or
        (kact.typflux = {&TYPFLUX-POUSSE} And kacta.typacta = {&TYPACTA-ENTRANT})) 
   Then Do:
      /* SLe 30/08/2011 A004779 M103180 mais qd il n'y a pas de ligne de freinte, 
         l'art.ref est tout seul et donc pas available */
      /* R�cap' : on est l'art.ref. si on ne trouve pas une seule autre ligne de m�me article AVANT
       * celle sur laquelle on est positionn� (importance du tri des lignes)
       */
      RETURN NOT CAN-FIND (FIRST b2Kacta 
                         Where b2Kacta.csoc    = kacta.csoc
                           And b2Kacta.cetab   = kacta.cetab
                           And b2Kacta.typacta = kacta.typacta
                           And b2Kacta.cart    = kacta.cart
                           And b2Kacta.cact    = kacta.cact
                           And b2Kacta.datef   = kacta.datef
                           And b2Kacta.nlig    < kacta.nlig
                         NO-LOCK). 
      
      /* L'article de REF est le Premier ==> Nlig le plus faible */
      /* MB D002845 17/10/07 - Gestion de la freinte pour les sortants (Art.R�f. uniquement) */
      /* Pour les sortants, il ne faut pas tenir compte de nlig */  
      /* MB D002845 (II) 27/11/07 - finalement SI */ 
      /* Return Available b2Kacta and b2Kacta.nlig > kacta.nlig.  */
   /* Return Available b2Kacta and ( (b2Kacta.nlig > kacta.nlig AND b2Kacta.typacta = {&TYPACTA-ENTRANT}) 
                                      OR b2Kacta.typacta = {&TYPACTA-SORTANT}). */                          
   End. 
   Else RETURN FALSE.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


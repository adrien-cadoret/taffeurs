&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" V-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\actd3jv.w
 *
 * Description  : SmartViewer sur Activit� par date d'effet
 * 
 * Template     : VIEWER.W - SmartViewer basique 
 * Type Objet   : SmartViewer 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 05/11/2002 � 16:14 par NB
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 22/12/2014 - MB      : DP001776:Activit�s : dates d'effet "historiques" � ignorer
 *                              20140623_0008 (25599)|DP001776|Activit�s : dates d'effet "historiques" � ignorer|MB|22/12/14 09:36:11 
 *                               
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" V-table-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/viewerd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS V-table-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

/* Parameters Definitions ---                                           */

/* Variables globales */
{variable.i " "}
{liedoclib.i}

/* constantes de pre-compilation */
&GLOBAL-DEFINE table kactd
&GLOBAL-DEFINE ltable Activit� par date

/* Variable pour template viewer */
{admvif/template/vinclude.i "Definitions"}
{unitebibli.i}

/* Local Variable Definitions ---                                       */
define variable w-datef like kactd.datef no-undo.
Define Variable w-typart  Like zart.typart No-undo.

Define Variable W-T-Duree   As Logical        No-Undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" V-table-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kactd kact
&Scoped-define FIRST-EXTERNAL-TABLE kactd


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kactd, kact.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS kactd.datef 
&Scoped-define ENABLED-TABLES kactd
&Scoped-define FIRST-ENABLED-TABLE kactd
&Scoped-Define ENABLED-OBJECTS B_AIDAT- 
&Scoped-Define DISPLAYED-FIELDS kactd.datef 
&Scoped-define DISPLAYED-TABLES kactd
&Scoped-define FIRST-DISPLAYED-TABLE kactd


/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,L-Oblig,L-Field,L-Majuscule,List-6 */
&Scoped-define ADM-CREATE-FIELDS kactd.datef 
&Scoped-define L-Oblig kactd.datef 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Auto-Definitions" V-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wrdef.p */

/* ********** Preprocessor Definitions VIF ***********/
&Scoped-Define Trait-External-Tables ~{&TP1} kactd ~{&TP2} ~
                                     ~{&TP1} kact ~{&TP2} 

&Scoped-Define List-Champs-Auto kactd.datef
&If Defined(List-Champs-Auto) <> 0 &Then
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&List-Champs-Auto},{&User-List-Champs}
   &Else
      &Scoped-Define List-champs {&List-Champs-Auto}
   &Endif
&Else
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&User-List-Champs}
   &Endif
&Endif

&Scoped-Define FIELD-PAIRS~
 ~{&FP1}datef ~{&FP2}datef ~{&FP3}
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON B_AIDAT- 
     IMAGE-UP FILE "admvif/images/calendar.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/calendari.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     kactd.datef AT ROW 1.19 COL 23 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 13.2 BY 1
     B_AIDAT- AT ROW 1.19 COL 38.4
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: vif5_7.kactd,vif5_7.kact
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW V-table-Win ASSIGN
         HEIGHT             = 1.57
         WIDTH              = 101.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB V-table-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_viewe.i}
{src/adm/method/viewer.i}
{admvif/method/ap_viewe.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW V-table-Win
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit Custom                            */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       B_AIDAT-:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide|Tooltip=Calendar".

/* SETTINGS FOR FILL-IN kactd.datef IN FRAME F-Main
   1 3                                                                  */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" V-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME B_AIDAT-
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AIDAT- V-table-Win
ON CHOOSE OF B_AIDAT- IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-aide-date"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AIDAT- V-table-Win
ON ENTRY OF B_AIDAT- IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-aide-date"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK V-table-Win 


/* ***************************  Main Block  *************************** */
/*** Triggers Generaux Du viewer */
{admvif/template/vinclude.i "triggers"}

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         

{admvif/template/vinclude.i "main-block"}


  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available V-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kactd"}
  {src/adm/template/row-list.i "kact"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kactd"}
  {src/adm/template/row-find.i "kact"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Aide-Sur-Champ V-table-Win 
PROCEDURE Aide-Sur-Champ :
/*------------------------------------------------------------------------------
  Purpose:     Aide (liste de choix) Sur les Champs
  Parameters:  Handle du widget active
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Champ  As Widget-Handle No-Undo.
    Define Variable ChoixEffectue As Character No-Undo.

    Case Champ:Name :
 /*      When "lotdu":U Then Do :
 *           {&Run-prog} "admvif/objects/a-standa.w":U 
 *                       ("unitej":U,             /* Code fonction pour les droits */
 *                        "unitejb":U, "cfamu=tps":U,   /* Nom browser et attributs */
 *                        "":U, "":U,    /* Nom viewer et attributs */
 *                        Champ:Label,             /* Titre ecran d'aide */
 *                        no,                     /* Acces bouton nouveau */
 *                        Champ:Screen-Value,      /* Valeur de depart */
 *                        Output ChoixEffectue).
 *           If ChoixEffectue <> ? Then 
 *              Champ:Screen-Value = ChoixEffectue.
 *        End.
 * */
       Otherwise ChoixEffectue = ?.
    End Case.

    If ChoixEffectue <> ? Then 
       Apply "Tab":U To Champ.
    Else
       Apply "Entry":U To Champ.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI V-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record V-table-Win 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  Assign kactd.datef:Sensitive In Frame {&Frame-Name} = Adm-New-Record.
  Run Dispatch ('Adv-Clear-Modified':U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-documents V-table-Win 
PROCEDURE local-adv-documents :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       NLE 26/08/2005
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vl-list-type   AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vl-list-cle    AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vl-list-clelib AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vCact          AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vModifAuto     AS LOGICAL   NO-UNDO.
  DEFINE VARIABLE hdlSF          AS HANDLE    NO-UNDO.
  DEFINE VARIABLE vLig           AS INTEGER   NO-UNDO.
  DEFINE VARIABLE vCol           AS INTEGER   NO-UNDO.
  /* MB M139113 20130827_0010 */
  DEFINE VARIABLE vTitre         AS CHARACTER NO-UNDO.
  /* MB DP001776 */
  DEFINE VARIABLE vDatefEnCours  AS DATE      NO-UNDO.
  DEFINE BUFFER bKactd FOR kactd.
  
  /* Code placed here will execute PRIOR to standard behavior. */
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-documents':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  IF AVAILABLE {&TABLE} THEN DO:
     Run Get-Info ("CACT":u, "container-source":u).                           
     ASSIGN vCact = Return-Value. /* code activit� r�el, pas $NLE... */ 
     /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
     FOR LAST bKactd FIELDS (datef)
                     WHERE bKactd.csoc  = wcSoc
                       AND bKactd.cetab = wcEtab
                       AND bKactd.cact  = vcAct
                       AND bKactd.datef <= TODAY  
                     NO-LOCK:
        ASSIGN vDatefEnCours = bKactd.datef.
     END.
     /* Fin DP001776 */
     
     ASSIGN vl-list-type   = {&ZLIEDOC-TYPCLE-FAB}
            vl-list-cle    = {&TABLE}.cact + ':':U + STRING({&TABLE}.datef, '99/99/9999':U)     /* $NLE:10/01/2005 si en MAJ, ABATTAGE:10/01/2005 sinon */
            vl-list-clelib = vCact         + ':':U + STRING({&TABLE}.datef, '99/99/9999':U)     /* ABATTAGE:10/01/2005 */
            vTitre         = vl-list-clelib
            vModifAuto     = (SUBSTRING({&TABLE}.cact, 1, 1) = '$':U   /* modification de l'arborescence des documents associ�s autoris�e ? */
                              /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
                              AND {&TABLE}.datef >= vDatefEnCours). 
     /* MB M139113 20130827_00010 */
     /* Mauvaise gestion des documents li�s en copie d'activit� */      
     RUN Get-Info ("ModeFct":U, "container-source":U).
     IF RETURN-VALUE = "Copier":U THEN
        ASSIGN vl-list-clelib = vl-list-cle.
     
     /* vient de actjf.w */
     Run Get-Info ("HANDLE":u, "Container-Source":U).
     hdlSF = WIDGET-HANDLE(Return-Value).
     
     /* prend la position de la SF dans laquelle on est (actdjs), car celle des documents
      * sera affich�e aux m�mes coordonn�es, et comme elle est plus haute, elle
      * sera tronqu�e en bas. Donc on met en 1,1 actdjs, pour 
      * que la SF des documents soit affich�e aussi en 1,1, puis on remettra
      * actdjs � sa position initiale */
     RUN brokvif-GetPosition IN Adm-broker-hdl (hdlSF, OUTPUT vLig, OUTPUT vCol).
     RUN Set-position IN hdlSF (1, 1) NO-ERROR.
     
     /* sous fonction des documents */
     {liedoci.i &csoc        = wcsoc
                &cetab       = wcetab
                &lst-type    = vl-list-type
                &lst-cle     = vl-list-cle
                &lst-libelle = vl-list-clelib 
                &titre       = vTitre
                &modif-auto  = vModifAuto }
     
     /* pourquoi - 2 ? parce que la SF se repositionne trop bas sans qu'on sache pourquoi */
     RUN Set-position IN hdlSF (vLig - 2, vCol) NO-ERROR.
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement V-table-Win 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  Define Buffer b-kacta      For kacta.
  Define Buffer b-kacto      For kacto.
  Define Buffer b-kactod     For kactod.
  Define Buffer b-kcom       For kcom.
  Define Buffer b-kactartr   For kactartr. 
  DEFINE BUFFER b-Kactdpop   FOR Kactdpop.
  DEFINE BUFFER b-Kactapopd  FOR Kactapopd.
  Define Buffer bufKacta     For kacta.
  Define Buffer bufKacto     For kacto.
  Define Buffer bufKactod    For kactod.
  Define Buffer bufKcom      For kcom.
  Define Buffer bufKactartr  For kactartr.      
  DEFINE BUFFER bufKactdpop  FOR Kactdpop.
  DEFINE BUFFER bufKactapopd FOR Kactapopd.
  
  Define Variable w-cle-com   As Character    No-Undo.
  DEFINE VARIABLE vCleI       AS CHARACTER    NO-UNDO.   /* clef dossier de fab � copier (ancienne date d'effet) */
  DEFINE VARIABLE vCleO       AS CHARACTER    NO-UNDO.   /* clef dossier de fab r�sultant de la copie (nouvelle date d'effet) */
  DEFINE VARIABLE vCdospI     AS CHARACTER    NO-UNDO.
  DEFINE VARIABLE vCdospO     AS CHARACTER    NO-UNDO.
  DEFINE VARIABLE vCact       AS CHARACTER    NO-UNDO.

  /* Code placed here will execute PRIOR to standard behavior. */
  If Adm-New-Record THEN DO:
      Assign kactd.csoc   = wcsoc
           kactd.cetab  = wcetab
           kactd.cact   = kact.cact
           kactd.datmod = Today
           kactd.cuser  = wcuser. 
           
  END.


  /*--  En cr�ation, lorsque kact.typflux est renseign�,      --*/         
  /*-- on peut renseigner l'article de r�f�rence comme kacta  --*/

  /*-- Lorsque l'on a utilise le bouton copier, il faut dupliquer : --*/         
  /*--    - les entrants                                            --*/
  /*--    - les sortants                                            --*/
  /*--    - les articles de co�t                                    --*/
  /*--    - les operations                                          --*/
  /*--    - les articles de remplacement                            --*/
  /*--    - les liens vers les documents                            --*/
  /*--    - les liens Version d'activit� - Process op�ratoires      --*/
  /*--    - les liens Articles d'activit� - Process op�ratoires     --*/
 
  /* COPIE */
  If Adm-New-Record And Not Adm-Adding-Record Then Do:
  
    /*{&setcurs-wait}  
 
    /*-- Entrants, sortants et articles de co�t. --*/
    For Each b-kacta Where b-kacta.csoc  = wcsoc
                       And b-kacta.cetab = wcetab
                       And b-kacta.cact  = kactd.cact
                       And b-kacta.datef = w-datef
                     No-Lock :
      Create bufKacta.
      Assign bufKacta.datef = Date(kactd.datef:Screen-Value In Frame {&Frame-Name}).
      Buffer-Copy b-kacta Except Datef To bufKacta.
      Assign w-cle-com = b-kacta.cact                           + ',':U
                       + String(b-kacta.datef, '99/99/9999':U)  + ',':U
                       + b-kacta.typacta                        + ',':U
                       + String(b-kacta.ni1).
      Find First b-kcom Where b-kcom.csoc   = b-kacta.csoc
                          And b-kcom.cetab  = b-kacta.cetab
                          And b-kcom.typcle = {&TYPCLE-COM-ACT}
                          And b-kcom.cle    = w-cle-com
      No-Lock No-Error.
      If Available b-kcom Then Do:
        Create bufKcom.
        Buffer-Copy b-kcom Except cle To bufKcom
        Assign bufKcom.cle = bufKacta.cact                           + ',':U
                           + String(bufKacta.datef, '99/99/9999':U)  + ',':U
                           + bufKacta.typacta                        + ',':U
                           + String(bufKacta.ni1).
      End.
    End.
        
    /*-- Op�rations. --*/
    For Each b-kacto Where b-kacto.csoc  = wcsoc
                       And b-kacto.cetab = wcetab
                       And b-kacto.cact  = kactd.cact
                       And b-kacto.datef = w-datef
                     No-Lock :
            
      Create bufKacto.
      Assign bufKacto.datef = Date(kactd.datef:Screen-Value In Frame {&Frame-Name}).
      Buffer-Copy b-kacto Except Datef To bufKacto.     
      
    End.
            
    /*-- D�tail des op�rations. --*/
    For Each b-kactod Where b-kactod.csoc  = wcsoc
                        And b-kactod.cetab = wcetab
                        And b-kactod.cact  = kactd.cact
                        And b-kactod.datef = w-datef
                      No-Lock :
           
      Create bufKactod.          
      Assign bufKactod.date = Date(kactd.datef:Screen-Value In Frame {&Frame-Name}).
      Buffer-Copy b-kactod Except Datef To bufKactod.
    End.    
  
    /* -- Articles de remplacement --*/
    For Each b-kactartr Where b-kactartr.csoc  = wcsoc
                          And b-kactartr.cetab = wcetab
                          And b-kactartr.cact  = kactd.cact
                          And b-kactartr.datef = w-datef
                        No-Lock:
       Create bufKactartr.
       Assign bufKactartr.datef = Date(kactd.datef:Screen-Value In Frame {&Frame-Name}).
       Buffer-copy b-kactartr Except Datef To bufKactartr.
    End.

    /* Copie de ses liens vers documents */
    Run Get-Info ("CACT":u, "container-source":u).                           
    ASSIGN vCact   = RETURN-VALUE   /* code activit� r�el, pas $NLE... */ 
           vCleI   = {&TABLE}.cact + ':':U + STRING(w-datef, '99/99/9999':U)
           vCleO   = {&TABLE}.cact + ':':U + kactd.datef:Screen-Value In Frame {&Frame-Name}
           vCdospI = vCact + ':':U + STRING(w-datef, '99/99/9999':U)
           vCdospO = vCact + ':':U + kactd.datef:Screen-Value In Frame {&Frame-Name}.
    
    {&Run-LIEDOC-Copie}       ( INPUT wcsoc,
                                INPUT wcetab,
                                INPUT {&ZLIEDOC-TYPCLE-FAB},
                                INPUT vCleI,
                                INPUT vCleO,
                                INPUT YES).

    
    {&Run-LIEDOC-MAJ-cdosp}   ( INPUT wcsoc,
                                INPUT wcetab,
                                INPUT {&ZLIEDOC-TYPCLE-FAB},
                                INPUT vCleO,
                                INPUT vCdospI,
                                INPUT vCdospO).

    /*-- Liens Version d'activit� - Process op�ratoires --*/    
    FOR EACH b-Kactdpop WHERE b-Kactdpop.csoc  = wcsoc     
                          AND b-Kactdpop.cetab = wcetab    
                          AND b-Kactdpop.cact  = kactd.cact
                          AND b-Kactdpop.datef = w-datef   
                        NO-LOCK:
       CREATE bufKactdpop.
       ASSIGN bufKactdpop.datef = DATE(Kactd.datef:SCREEN-VALUE IN FRAME {&FRAME-NAME}).
       BUFFER-COPY b-Kactdpop EXCEPT datef TO bufKactdpop.
    END.
    
    /*-- Liens Articles d'activit� - Process op�ratoires --*/
    FOR EACH b-Kactapopd WHERE b-Kactapopd.csoc  = wcsoc     
                           AND b-Kactapopd.cetab = wcetab    
                           AND b-Kactapopd.cact  = kactd.cact
                           AND b-Kactapopd.datef = w-datef   
                         NO-LOCK:
       CREATE bufKactapopd.
       ASSIGN bufKactapopd.datef = DATE(Kactd.datef:SCREEN-VALUE IN FRAME {&FRAME-NAME}).
       BUFFER-COPY b-Kactapopd EXCEPT datef TO bufKactapopd.
    END.
    
    {&setcurs-nowait}      */
  end.    
   
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-copy-record V-table-Win 
PROCEDURE local-copy-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  w-datef = kactd.datef.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'copy-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Assign kactd.datef:Sensitive In Frame {&Frame-Name} = Adm-New-Record.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-statement V-table-Win 
PROCEDURE local-delete-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE BUFFER bKacta       FOR Kacta.
  DEFINE BUFFER bKacto       FOR Kacto.
  DEFINE BUFFER bKactod      FOR Kactod.
  Define Buffer bKactartr    For kactartr.
  DEFINE BUFFER bKactdpop    FOR Kactdpop.
  DEFINE BUFFER bKactapopd   FOR Kactapopd.
  DEFINE BUFFER bufKacta     FOR Kacta. 
  DEFINE BUFFER bufKcom      FOR Kcom.
  DEFINE BUFFER bufKacto     FOR Kacto.
  DEFINE BUFFER bufKactod    FOR Kactod.  
  DEFINE BUFFER bufKactartr  FOR Kactartr.
  DEFINE BUFFER bufKactdpop  FOR Kactdpop.
  DEFINE BUFFER bufKactapopd FOR Kactapopd.  
  DEFINE VARIABLE vNbEnregSup AS INTEGER   NO-UNDO. 

  /* Code placed here will execute PRIOR to standard behavior. */

  /*-- Suppression des entrants, sortants et articles de co�t. --*/
  
  {&setcurs-wait}  
  
  For Each bKacta FIELDS (csoc cetab cact datef typacta ni1)
                   Where bKacta.csoc  = wcsoc
                     And bKacta.cetab = wcetab
                     And bKacta.cact  = kactd.cact
                     And bKacta.datef = kactd.datef
                  NO-LOCK :
      Find First bufKcom Where bufKcom.csoc   = bKacta.csoc
                           And bufKcom.cetab  = bKacta.cetab
                           And bufKcom.typcle = {&TYPCLE-COM-ACT}
                           And bufKcom.cle    = bKacta.cact + ',':U
                                        + String(bKacta.datef, '99/99/9999':U) + ',':U
                                        + bKacta.typacta + ',':U
                                        + String(bKacta.ni1)
      Exclusive-Lock No-Error.
      If Available bufKcom Then Delete bufKcom.
      
      FIND FIRST bufKacta WHERE ROWID(bufKacta) = ROWID(bKacta)
                          EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      IF AVAILABLE bufKacta THEN       
         Delete bufKacta.
  End.
        
  /*-- Suppression des op�rations. --*/
  For Each bKacto FIELDS ({&EmptyFields})
                   Where bKacto.csoc  = wcsoc
                     And bKacto.cetab = wcetab
                     And bKacto.cact  = kactd.cact
                     And bKacto.datef = kactd.datef
                  NO-LOCK :          
     FIND FIRST bufKacto WHERE ROWID(bufKacto) = ROWID(bKacto)
                         EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF AVAILABLE bufKacto THEN
        Delete bufKacto.
  End.
        
  /*-- Suppression du d�tail des op�rations. --*/
  For Each bKactod FIELDS ({&EmptyFields})
                    Where bKactod.csoc  = wcsoc
                      And bKactod.cetab = wcetab
                      And bKactod.cact  = kactd.cact
                      And bKactod.datef = kactd.datef
                   NO-LOCK :          
     FIND FIRST bufKactod WHERE ROWID(bufKactod) = ROWID(bKactod)
                          EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF AVAILABLE bufKactod THEN
        Delete bufKactod.
  End.

  Find First bufKcom Where bufKcom.csoc   = {&Table}.csoc
                       And bufKcom.cetab  = {&Table}.cetab
                       And bufKcom.typcle = {&TYPCLE-COM-ACT}
                       And bufKcom.cle    = {&Table}.cact + ',':U
                                          + String({&Table}.datef, '99/99/9999':U)
  Exclusive-Lock No-Error.
  If Available bufKcom Then Delete bufKcom.

  /*-- Suppression des articles de remplacement --*/
  For Each bKactartr FIELDS({&EmptyFields})
                      Where bKactartr.csoc  = wcsoc
                        And bKactartr.cetab = wcetab
                        And bKactartr.cact  = kactd.cact
                        And bKactartr.datef = kactd.datef
                     NO-LOCK:
     FIND FIRST bufKactartr WHERE ROWID(bufKactartr) = ROWID(bKactartr)
                            EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
     IF AVAILABLE bufKactartr THEN
        Delete bufKactartr.
  End.

  /* Suppression de ses liens vers documents */
  {&Run-LIEDOC-Suppression} ( INPUT wcsoc,
                              INPUT wcetab,
                              INPUT {&ZLIEDOC-TYPCLE-FAB},
                              INPUT {&TABLE}.cact + ':':U + STRING({&TABLE}.datef, '99/99/9999':U),
                              OUTPUT vNbEnregSup).  /* AMA 22/02/2006 */


  /*-- Suppression des liens Version d'activit� - Process op�ratoires --*/  
  FOR EACH bKactdpop WHERE bKactdpop.csoc  = wcsoc      
                       AND bKactdpop.cetab = wcetab     
                       AND bKactdpop.cact  = kactd.cact 
                       AND bKactdpop.datef = kactd.datef
                     NO-LOCK:      
      FIND FIRST bufKactdpop WHERE ROWID(bufKactdpop) = ROWID(bKactdpop)
                             EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      IF AVAILABLE bufKactdpop THEN
         DELETE bufKactdpop.
  END.
  
  /*-- Suppression des liens Articles d'activit� - Process op�ratoires --*/
  FOR EACH bKactapopd WHERE bKactapopd.csoc  = wcsoc      
                        AND bKactapopd.cetab = wcetab     
                        AND bKactapopd.cact  = kactd.cact 
                        AND bKactapopd.datef = kactd.datef
                      NO-LOCK:      
      FIND FIRST bufKactapopd WHERE ROWID(bufKactapopd) = ROWID(bKactapopd)
                              EXCLUSIVE-LOCK NO-WAIT NO-ERROR.
      IF AVAILABLE bufKactapopd THEN
         DELETE bufKactapopd.
  END.
  
  {&setcurs-nowait}          

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields V-table-Win 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  
  Assign kactd.datef:Sensitive In Frame {&Frame-Name} = Adm-New-Record.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields V-table-Win 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Assign kactd.datef:Sensitive In Frame {&Frame-Name} = Adm-New-Record.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Test-Saisie V-table-Win 
PROCEDURE Local-Test-Saisie :
/*------------------------------------------------------------------------------
  Purpose:     Tester la saisie des variables du viewer
  Parameters:  - Test Global, ou Pour un widget 
               - Handle du widget � tester.
  Notes:       
------------------------------------------------------------------------------*/
   Define Input Parameter Test-Global As Logical       No-Undo.
   Define Input Parameter Widget-Test As Widget-Handle No-Undo.

   DEFINE VARIABLE vRet AS LOGICAL NO-UNDO.
   DEFINE VARIABLE vMsg AS CHARACTER NO-UNDO.
   /* MB DP001776 */
   DEFINE VARIABLE vDatefEnCours AS DATE      NO-UNDO.
   DEFINE BUFFER bKactd FOR kactd.

   /* Exemple de test : Test apres la saisie de cle, en creation, si l'enreg n'existe
   pas d�j� */
   If Test-Global 
       Or Widget-Test = kactd.datef:Handle in Frame {&Frame-Name} Then Do :
      If adm-new-record Then Do :
         Find kactd Where kactd.csoc    = wcsoc 
                      and kactd.cetab = wcetab
                      and kactd.cact  = kact.cact
                      And kactd.datef = Input Frame {&Frame-Name} kactd.datef
                    No-Lock No-Error.
         If Available kactd Then Do :
            {affichemsg.i &Message="SUBSTITUTE(Get-TradMsg (013125, 'Cette valeur existe d�j� (&1).':T), Get-TradTxt (000353, 'Date d''effet':T))"}
            Apply "Entry":U to kactd.datef in Frame {&Frame-Name}.
            Return "Adm-Error":U.
         End.
          
         /* Contr�le de la validit� de l'article de r�f�rence pour la nouvelle date */
         RUN artfabdfabcp.p ( INPUT  wcsoc,
                              INPUT  wcetab,
                              INPUT  kact.cart,
                              INPUT  date(kactd.datef:SCREEN-VALUE IN FRAME {&FRAME-NAME}),
                              OUTPUT vRet,
                              OUTPUT vMsg).
         IF vMsg <> "" THEN DO :
            {affichemsg.i &Message = "vMsg"}
         END.
         IF NOT vRet THEN DO :
            Apply "Entry":U to kactd.datef.
            Return "Adm-Error":U.
         END.
         
         /* MB DP001776 - Les dates d'effet "historiques" ne doivent plus �tre modifiables */
         /* On v�rifie que la date cr��e n'est pas dans le "pass�" */
         FOR LAST bKactd FIELDS (datef)
                          WHERE bKactd.csoc  = wcSoc
                            AND bKactd.cetab = wcEtab
                            AND bKactd.cact  = kact.cact
                            AND bKactd.datef <= TODAY  
                          NO-LOCK:
            ASSIGN vDatefEnCours = bKactd.datef.
         END.
         /* Comparaison de la date saisie avec la date d'effet en vigueur */
         IF DATE(kactd.datef:SCREEN-VALUE) < vDatefEnCours THEN DO:
            {affichemsg.i &Message = "SUBSTITUTE(Get-TradTxt (022493, 'Traitement impossible (&1).':T), 
                                    Get-TradMsg (035302, 'Cr�ation d''une date d''effet pass�e':T))"}
            APPLY "Entry":U TO kactd.datef.
            RETURN "Adm-Error":U.                      
         END.
         /* Fin DP001776 */
      End.
   End.
    
    Return.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info V-table-Win 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.
   
   DEFINE VARIABLE vDatef AS CHARACTER NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
   ASSIGN vDatef = IF kactd.datef:SCREEN-VALUE NE "" THEN STRING(DATE(kactd.datef:SCREEN-VALUE),'99/99/9999':U) ELSE "".

   Case Name-Info :
       /* Ins�rer ici vos propres infos � envoyer */
       When "cle-com":U Then
          If Adm-New-Record Then
             Valeur-Info = kact.cact + ',':U + vDatef.
          Else Do:
             If Available kactd Then
                Valeur-Info = kactd.cact + ',':U + String(kactd.datef, '99/99/9999':U).
             Else
                Valeur-Info = "":U.
          End.
          
       /* Pour les crit�res */
       When "Cle-Crit":U Then
          Assign Valeur-Info = wcsoc + "|":U +
                               wcetab + "|":U +
                               {&KCRIV-TYPCLE-ACT} + "|":U +
                               (If Available kact THEN kact.cact ELSE '') + ",":u + vDatef.

       When "Cle-Crifa":U Then Do:
          Assign Valeur-Info = (If Available kact THEN kact.cart ELSE "") + '�':U +
                               '':u       + '�':U +  /* stade encore inexistant */
                               vDatef         + '�':U +
                               (If Available kact THEN kact.cfmact + ',':U + kact.cact ELSE '').
       End.
       
       When "Init-Crifa":U Then
          Assign Valeur-Info = "Non":U + "|":U + (If Available kact THEN kact.cact ELSE "").

       Otherwise 
          /* Par d�faut, envoi de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Send-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.

END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records V-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kactd"}
  {src/adm/template/snd-list.i "kact"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed V-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {admvif/template/vinclude.i "vstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


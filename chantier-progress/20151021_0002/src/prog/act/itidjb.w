&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" B-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : act\itidjb.w
 *
 * Description  : SmartBrowser sur Itin�raire par date d'effet
 * 
 * Template     : BROWSER.W - SmartBrowser basique 
 * Type Objet   : SmartBrowser 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 01/08/1996 � 00:00 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 05/09/2012 - SLE     : Q005521:Coh�rence affichage Datef ACT, ITI, POP (DESC)
 *                              20120905_0001 (22212)|Q005521|Coh�rence affichage Datef ACT, ITI, POP (DESC)|SLE|05/09/12 14:53:23 
 *                               
 *  - Le 04/05/2009 - SLE     : A002234:Pb Itin�raire : blocage qd changement date d'effet
 *                              20090504_0010 (16060)|A002234|Pb Itin�raire : blocage qd changement date d'effet|SLE|04/05/09 12:40:31 
 *                               M056455
 *                              
 *                              
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" B-table-Win _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/browserd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS B-table-Win 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/



/* Parameters Definitions ---                                           */

/* Variables globales */
{variable.i " "}


/* constantes de pre-compilation */
&GLOBAL-DEFINE table kitid
&GLOBAL-DEFINE ltable Itin�raire par date d'effet

/* Variable utilis�es pour le template des browser */
{admvif/template/binclude.i "definitions"}

/* Local Variable Definitions ---                                       */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" B-table-Win _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartBrowser
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main
&Scoped-define BROWSE-NAME br_table

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kiti
&Scoped-define FIRST-EXTERNAL-TABLE kiti


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kiti.
/* Internal Tables (found by Frame, Query & Browse Queries)             */
&Scoped-define INTERNAL-TABLES kitid

/* Definitions for BROWSE br_table                                      */
&Scoped-define FIELDS-IN-QUERY-br_table kitid.datef 
&Scoped-define ENABLED-FIELDS-IN-QUERY-br_table 
&Scoped-define QUERY-STRING-br_table FOR EACH kitid OF kiti NO-LOCK ~
    BY kitid.datef DESCENDING
&Scoped-define OPEN-QUERY-br_table OPEN QUERY br_table FOR EACH kitid OF kiti NO-LOCK ~
    BY kitid.datef DESCENDING.
&Scoped-define TABLES-IN-QUERY-br_table kitid
&Scoped-define FIRST-TABLE-IN-QUERY-br_table kitid


/* Definitions for FRAME F-Main                                         */

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS br_table 

/* Custom List Definitions                                              */
/* List-1,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Browse-TTY" B-table-Win _INLINE
/* Actions: ? admvif/xftr/ettybr.w ? ? ? */
/********************** TAILLE DU BROWSE EN TTY **********/
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Complement-Display-Browse" B-table-Win _INLINE
/* Actions: ? admvif/xftr/ecompbr.w ? ? admvif/xftr/wcompbr.p */
/********************** COMPLEMENT DISPLAY BROWSE **********/
/* Automatique=Oui*/
/* Pas de compl�ment-display-browse */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-br_table 
       MENU-ITEM m_Recherche-Code LABEL "&Recherche par la date d'effet".


/* Definitions of the field level widgets                               */
/* Query definitions                                                    */
&ANALYZE-SUSPEND
DEFINE QUERY br_table FOR 
      kitid SCROLLING.
&ANALYZE-RESUME

/* Browse definitions                                                   */
DEFINE BROWSE br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _DISPLAY-FIELDS br_table B-table-Win _STRUCTURED
  QUERY br_table NO-LOCK DISPLAY
      kitid.datef COLUMN-LABEL "Date d'effet       ." FORMAT "99/99/9999":U
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME
    WITH NO-ASSIGN SEPARATORS SIZE 20.2 BY 8.19
         FONT 6.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     br_table AT ROW 1 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartBrowser
   External Tables: vif5_7.kiti
   Allow: Basic,Browse
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW B-table-Win ASSIGN
         HEIGHT             = 8.38
         WIDTH              = 20.8.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB B-table-Win 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_brows.i}
{src/adm/method/browser.i}
{admvif/method/ap_brows.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW B-table-Win
  NOT-VISIBLE,,RUN-PERSISTENT                                           */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
/* BROWSE-TAB br_table 1 F-Main */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       br_table:POPUP-MENU IN FRAME F-Main             = MENU POPUP-MENU-br_table:HANDLE.

/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK BROWSE br_table
/* Query rebuild information for BROWSE br_table
     _TblList          = "vif5_7.kitid OF vif5_7.kiti"
     _Options          = "NO-LOCK"
     _OrdList          = "vif5_7.kitid.datef|no"
     _FldNameList[1]   > vif5_7.kitid.datef
"kitid.datef" "Date d'effet       ." ? "date" ? ? ? ? ? ? no ? no no ? yes no no "U" "" "" "" "" "" "" 0 no 0 no no
     _Query            is NOT OPENED
*/  /* BROWSE br_table */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" B-table-Win _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define BROWSE-NAME br_table
&Scoped-define SELF-NAME br_table
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON MOUSE-SELECT-DBLCLICK OF br_table IN FRAME F-Main
DO:
  /* code standard pour le double-click, en cas de browse d'aide */
  {admvif/template/binclude.i "brsdbclick"}
  
   /* Pour faire comme si on appuyait sur le bouton activit� */
   Run new-state ("DBLCLICK-ITIDJS,Container-source":u).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-ENTRY OF br_table IN FRAME F-Main
DO:
  /* This code displays initial values for newly added or copied rows. */
  {admvif/template/binclude.i "brsentry"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON ROW-LEAVE OF br_table IN FRAME F-Main
DO:
    /* Do not disable this code or no updates will take place except
     by pressing the Save button on an Update SmartPanel. */
  {admvif/template/binclude.i "brsleave"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL br_table B-table-Win
ON VALUE-CHANGED OF br_table IN FRAME F-Main
DO:
  /* This ADM trigger code must be preserved in order to notify other
     objects when the browser's current row changes. */
  {admvif/template/binclude.i "brschnge"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Recherche-Code
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Recherche-Code B-table-Win
ON CHOOSE OF MENU-ITEM m_Recherche-Code /* Recherche par la date d'effet */
DO:
  /* Recherche standard par le code */
  RUN dispatch IN THIS-PROCEDURE ('Adv-Recherche-Code':U).
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK B-table-Win 


/* ***************************  Main Block  *************************** */
/*** Triggers Generaux ***/
{admvif/template/binclude.i "triggers"}

&IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
&ENDIF

/* Main-block standard browser */
{admvif/template/binclude.i "main-block"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available B-table-Win  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kiti"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kiti"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI B-table-Win  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-reopen-query B-table-Win 
PROCEDURE local-adv-reopen-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-reopen-query':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  if num-results("{&BROWSE-NAME}":U) = 0 
    then Run Set-Info('Datef-Available=no':U,'container-source':U).
    else Run Set-Info('Datef-Available=yes':U,'container-source':U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-open-query B-table-Win 
PROCEDURE local-open-query :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-reopen-query':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  If num-results("{&BROWSE-NAME}":U) = 0 Or 
     num-results("{&BROWSE-NAME}":U) = ? 
  Then Do :
     Run Set-Info('Datef-Available=no':U,'container-source':U).
     Run dispatch ('Row-Changed':U).    /* Pour avertir les record-target */
  End.
  Else Do :
     Run Set-Info('Datef-Available=yes':U,'container-source':U).
     Run Get-Info("F-datref":U, "Container-Source":U).
     Find Last kitid Where kitid.csoc = wcsoc 
                          and kitid.cetab = wcetab
                          and kitid.citi = kiti.citi
                          And kitid.datef <= date(Return-Value)
                        No-Lock No-Error.
     If Not Available kitid THEN DO :
      /* SLe 04/05/09 A002234 M056455 */
         Find Last kitid Where kitid.csoc  = wcsoc
                           and kitid.cetab = wcetab
                           and kitid.citi  = kiti.citi
                         No-Lock No-Error.
      /* --- */
         If Not Available kitid THEN DO : 
            Apply "End":U To Browse {&Browse-Name}.
         END.
     END.
     Run Reposition-Browse(Rowid(kitid)).
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Position-Record B-table-Win 
PROCEDURE Position-Record :
/*------------------------------------------------------------------------------
  Purpose:     Postionnement du browse en fonction d'un cle d'entree
  Parameters:  Cle d'entree (List Progress, avec comme separteur '|').
  Notes:       
------------------------------------------------------------------------------*/
   Define Input Parameter CleEntree As Character No-Undo.

   Find Last {&Table} Where {&Table}.csoc = wcsoc 
                         and {&Table}.cetab = wcetab
                         and {&Table}.citi = kiti.citi
                         And {&Table}.datef <= date(Entry(1,CleEntree,'|':U))
                      No-Lock No-Error.

   If Available {&Table} Then 
      Run Reposition-Browse (Rowid({&Table})).
   Else
      Apply "End":U To Browse {&Browse-Name}.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Reaffiche-Browse B-table-Win 
PROCEDURE Reaffiche-Browse :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Ret = Browse {&Browse-Name}:Refresh().
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info B-table-Win 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.
   
   Case Name-Info :
       /* Ins�rer ici vos propres infos � recevoir */
       When "Color-Browse":U Then 
          Browse {&Browse-Name}:Bgcolor = Integer(Valeur-Info).
       
       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Cle-Record B-table-Win 
PROCEDURE Send-Cle-Record :
/*------------------------------------------------------------------------------
  Purpose:     Renvoie la cle du record courant en une liste Progress
               separe par '|'
  Parameters:  <none>
  Notes:       Cette Procedure est surtout utilise quand le browse sert d'aide a
               la saisie.
------------------------------------------------------------------------------*/
     Define Output Parameter CleRecord As Character No-Undo.
     
     If Available {&Table} Then Do :
        CleRecord = string({&Table}.datef).
        Return.
     End.
     
     CleRecord = ?.
     Return.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records B-table-Win  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kiti"}
  {src/adm/template/snd-list.i "kitid"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed B-table-Win 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
     {admvif/template/binclude.i "bstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Traiter-Contexte B-table-Win 
PROCEDURE Traiter-Contexte :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  Define Output Parameter w-val As character No-undo.

  Run Get-Contexte In Adv-Outils-Hdl ("kiti.dtef":U, Output w-val).  

  If w-val <> "":U Then Do:
     Run Position-Record (INPUT w-val).
     Run Set-Contexte In Adv-Outils-Hdl (This-Procedure,"kiti.dtef":U, "":U).
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Include _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : fab\mnemobsfab.i
 *
 * Description  : Mn�moniques des Services FAB
 * 
 * Template     : include.i - Structured Include File 
 * Type Objet   : Include 
 * Compilable   : __COMPILE__NON 
 * Cr�� le      : 27/02/2009 � 09:42 par SLE
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 21/08/2015 - JD      : A_08326:Suppression des OF de pr�pa ou de transfert interd...
 *                              20150821_0002 (27539)|A_08326|Suppression des OF de pr�pa ou de transfert interdite|JD|21/08/15 11:41:01 
 *                               
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/


/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Include _INLINE
/* Actions: ? ? ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Include 
/*----------------------------------------------------------------------*/
/*          Definitions de l'include                                    */
/*----------------------------------------------------------------------*/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */



/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME



/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: Include
   Allow: 
   Frames: 0
   Add Fields to: Neither
   Other Settings: INCLUDE-ONLY
 */
&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Include ASSIGN
         HEIGHT             = 9.1
         WIDTH              = 48.6.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

 


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Include 


/* KOTA.TYPACTA  -   Type des articles des O.F. : entrant ou sortant */
&Global-Define TYPACTA-ENTRANT       'ENT':U
&Global-Define TYPACTA-SORTANT       'SOR':U
&Global-Define TYPACTA-OT            'OT':U
&Global-Define TYPACTA-OF            'OF':U
&Global-Define LCODE-TYPACTA-KOTA    ENT,SOR
&Global-Define LCLAIR-TYPACTA-KOTA   Entrant,Sortant
&Global-Define LCODE-TYPACTA-ECARTS  ENT,SOR,ART,RES
&Global-Define LCLAIR-TYPACTA-ECARTS Entrant,Sortant,Article de co�t,Ressource
&Global-Define LCODE-TYPACTA-SDP     ENT,SOR,ART,RES,OT,OF
&Global-Define LCLAIR-TYPACTA-SDP    Entrants,Sortants,Articles de co�t,Ressources,OT,OF

/* KOF.CETAT    -   Etat des Ordres de Fabrication */
&Global-Define CETAT-OF-SAISI         '10':U
&Global-Define INIT-CETAT-OF-SAISI    10
&Global-Define CETAT-OF-NON-CALCULE   '11':U
&Global-Define INIT-CETAT-OF-NON-CALCULE 11
&Global-Define CETAT-OF-CREE          '15':U
&Global-Define INIT-CETAT-OF-CREE     15
&Global-Define CETAT-OF-MODIFIE       '20':U
&Global-Define INIT-CETAT-OF-MODIFIE  20
&Global-Define CETAT-OF-CALCULE       '25':U
&Global-Define INIT-CETAT-OF-CALCULE  25
&Global-Define CETAT-OF-PLANIFIE      '30':U
&Global-Define INIT-CETAT-OF-PLANIFIE 30
&Global-Define CETAT-OF-LANCE         '35':U
&Global-Define INIT-CETAT-OF-LANCE    35
&Global-Define CETAT-OF-ENCOURS       '37':U
&Global-Define INIT-CETAT-OF-ENCOURS  37
&Global-Define CETAT-OF-BLOQUE        '40':U
&Global-Define INIT-CETAT-OF-BLOQUE   40
&Global-Define CETAT-OF-TERMINE       '40':U
&Global-Define INIT-CETAT-OF-TERMINE   40
&Global-Define CETAT-OF-CONSTATE      '45':U
&Global-Define INIT-CETAT-OF-CONSTATE 45
&Global-Define CETAT-OF-DECLOTURE     '47':U
&Global-Define INIT-CETAT-OF-DECLOTURE 47
&Global-Define CETAT-OF-CLOTURE       '50':U
&Global-Define INIT-CETAT-OF-CLOTURE  50
&Global-Define LCODE-CETAT-OF         10,11,15,20,25,30,35,37,40,45,47,50
&Global-Define LCLAIR-CETAT-OF        Saisi,Non calcul�,Cr��,� Modifier,Calcul�,Programm�,Lanc�,En-cours,Bloqu�,Constat�,D�cl�tur�,Cl�tur�
&Global-Define LCODE-CETAT-AFF-OF     10,11,15,20,25,30,35,37,40,45,47,50
&Global-Define LCLAIR-CETAT-AFF-OF    SAI,NC,CRE,MOD,CAL,PRG,LAN,ENC,BLO,CTT,DCL,CLO
/* Ne sert plus ??? */
&Global-Define LCODE-AFF-CETAT-OF  SAI,NC,CRE,MOD,CAL,PRG,LAN,ENC,BLO,CTT,DCL,CLO

/* Ev�nements sur les OF */
&Global-Define CEVT-OF-LANCEMENT        '35LAN':U
&Global-Define INIT-CEVT-OF-LANCEMENT   35LAN
&Global-Define CEVT-OF-MODIF-OP         '88MOP':U
&Global-Define INIT-CEVT-OF-MODIF-OP     88MOP
&Global-Define CEVT-OF-SUPPRESSION      '99SUP':U
&Global-Define INIT-CEVT-OF-SUPPRESSION 99SUP
&Global-Define LCODE-CEVT-OF            35LAN,99SUP
&Global-Define LCLAIR-CEVT-OF           Lancement,Suppression

/* KCRIV.TYPCLE  -   Type de cl� dans les valeurs de crit�res */
&Global-Define KCRIV-TYPCLE-ACT       'ACT':U
&Global-Define KCRIV-TYPCLE-OF        'OF':U
&Global-Define KCRIV-TYPCLE-OT        'OT':U
&Global-Define KCRIV-TYPCLE-OTA       'OTA':U
&Global-Define KCRIV-TYPCLE-ECH       'ECH':U   /* Echantillon d'un lot */
&Global-Define KCRIV-TYPCLE-OFOT      'OFT':U
/* MB P2374 - TYPCLE pour rendements dynamiques */
&Global-Define KCRIV-TYPCLE-ACT-RD    'ARD':U
&Global-Define KCRIV-TYPCLE-OT-RD     'ORT':U
&Global-Define KCRIV-TYPCLE-OTA-RD    'ORA':U

/* KOF.ORIG     -   Origine des Ordres de Fabrication */
&Global-Define ORIG-MAN     'MAN':U
&Global-Define ORIG-CBN     'CBN':U
&GLOBAL-DEFINE ORIG-PLA     'PLA':U
&GLOBAL-DEFINE ORIG-PDP     'PDP':U
&Global-Define ORIG-CLI     'CLI':U
&Global-Define ORIG-SEC     'SEC':U
&Global-Define ORIG-ZIMPORT 'IMP':U
&Global-Define ORIG-OF-ORIG 'ORI':U
&Global-Define ORIG-FOU     'FOU':U
&Global-Define ORIG-PREPA   'PRE':U
/*DP1960  PCS 28/02/2014 */
&Global-Define ORIG-CDE 'CDE':U
&Global-Define LCODE-ORIG   MAN,CBN,PLA,PDP,CLI,IMP,ORI,FOU,PRE,CDE
&Global-Define LCLAIR-ORIG  Manuel,Calcul des besoins,Plan,Planification,Client,Importation,OF origine,Fournisseur,Pr�paration de cde,Commande
&Global-Define LCODE-ORIG2  MAN,CBN,PLA,PDP,CLI,IMP,SEC,ORI,FOU,PRE,CDE
&Global-Define LCLAIR-ORIG2 Manuel,Calcul des besoins,Plan,Planification,Client,Importation,S�chage,OF origine,Fournisseur,Pr�paration de cde,Commande

/* KOF.CSIT - Type de gestion des ressources des op�rations */
&GLOBAL-DEFINE KOF-CSIT-GESTION-PROCESS 'POP':U /* APO D003489 */
&GLOBAL-DEFINE KOF-CSIT-GESTION-CCS     '':U
&GLOBAL-DEFINE LCODE-KOF-CSIT            ,POP
&GLOBAL-DEFINE LCLAIR-KOF-CSIT          Centre de charge sup�rieur, Process op�ratoire

/* KOF.COFUSE et KOT.COFUSE  -   Code contexte */
&Global-Define KOF-COFUSE-FAB       'FAB':U
&Global-Define KOF-COFUSE-PREPA     'PREPA':U
&Global-Define KOF-COFUSE-TRANSFERT 'TRANS':U
&Global-Define KOF-COFUSE-PLANIF    'PLANIF':U
&Global-Define KOF-COFUSE-MELANGE   'MEL':U
&Global-Define KOF-COFUSE-RANGEMENT 'RNG':U      /* OM : 08/06/2005 Utilisation rangement */
&Global-Define KOF-COFUSE-SOUS-TRAITANCE 'SST':U
&Global-Define LCODE-COFUSE         FAB,PREPA,TRANS,PLANIF,MEL,RNG,SST
&Global-Define LCLAIR-COFUSE        Fabrication,Pr�paration,Transfert,Planification,M�lange,Rangement, Sous-traitance
&Global-Define LCODE-COFUSE2        FAB,PREPA,TRANS,MEL,RNG,SST
&Global-Define LCLAIR-COFUSE2       Fabrication,Pr�paration,Transfert,M�lange,Rangement,Sous-traitance

/*JD Pour utilitaire SUP of */
&Global-Define LCODE-COFUSE-SUPOF FAB,PLANIF,MEL,RNG
&Global-Define LCLAIR-COFUSE-SUPOF Fabrication,Planification,M�lange,Rangement

/* S�lection O.F. - Ressource d'en-t�te CCS ou Ressource Primaire */
&Global-Define SELECTION-CRESOF-CCS      'CCS':U
&Global-Define SELECTION-CRESOF-RESPRIM  'RESPRIM':U
&Global-Define LCODE-SELECTION-CRESOF    CCS,RESPRIM
&Global-Define LCLAIR-SELECTION-CRESOF   CCS,Ress.Primaire

/*KOTAD.typor - origine des d�claration des kotad */
&Global-Define KOTAD-TYPOR-AUTO            'AUTO':U
&Global-Define KOTAD-TYPOR-MANUEL          'MANU':U
&Global-Define KOTAD-TYPOR-REMONTEEATELIER 'REAT':U
&Global-Define KOTAD-TYPOR-VENTILATION     'VENT':U
&Global-Define KOTAD-TYPOR-IMPORTATION     'IMPO':U
&Global-Define LCODE-KOTAD-TYPOR           AUTO,MANU,REAT,VENT,IMPO
&Global-Define LCLAIR-KOTAD-TYPOR          Automatisme,Manuelle,Remont�e atelier,Ventilation,Importation

/* OM : 24/03/2006 G7_1677 File d'attente */
/* XFILATT - Type de file d'attente */
&Global-Define XFILATT-TYPFILATT-ABA-PORC       'ABP':U
&Global-Define XFILATT-TYPFILATT-ABA-BOEUF      'ABB':U
&Global-Define LCODE-XFILATT-TYPFILATT  ABP,ABB
&Global-Define LCLAIR-XFILATT-TYPFILATT Abattage porc,Abattage boeuf

/* XFILATT - Type de cl� de l'entit� suivie */
&Global-Define XFILATT-TYPCLE-NSC       'NSC':U
&Global-Define XFILATT-TYPCLE-ARTLOT    'AL':U          /* NC DE00200 - 17/07/2009 */
&Global-Define LCODE-XFILATT-TYPCLE  NSC,AL             /* NC DE00200 - 17/07/2009 */
&Global-Define LCLAIR-XFILATT-TYPCLE NSC,Article-Lot    /* NC DE00200 - 17/07/2009 */

/* MB D002674 - Entit�s g�r�es par la pes�e fiscale porc (doit correspondre au typcle de l'entit�) */
&GLOBAL-DEFINE LCODE-XPFPO-ENTITE  NSC
&GLOBAL-DEFINE LCLAIR-XPFPO-ENTITE NSC
/* Fin MB */

/* XFILATTD - Etat de l'entit� */
&Global-Define XFILATTD-CETAT-PES    'PES':U
&Global-Define XFILATTD-CETAT-IMP    'IMP':U
&Global-Define XFILATTD-CETAT-ATB    'ATB':U
&Global-Define XFILATTD-CETAT-AIG    'AIG':U
&Global-Define XFILATTD-CETAT-ABA    'ABA':U
&Global-Define XFILATTD-CETAT-ARC    'ARC':U
&Global-Define LCODE-XFILATTD-CETAT  PES,IMP,ATB,AIG,ABA,ARC
&Global-Define LCLAIR-XFILATTD-CETAT Pes�,Imprim�,Attribu�,Aiguill�,Abattu,Archiv�
/* OM : 24/03/2006 G7_1677 Fin ajout pour File d'attente */

/* OM : 19/04/2006 G7_1684 Besoin technique */
/* KBESTECH - Besoin technique */
&Global-Define KBESTECH-TYPDES-CLIENT   'CLT':U
&Global-Define KBESTECH-TYPDES-INTERNE  'INT':U
&Global-Define LCODE-KBESTECH-TYPDES    CLT,INT
&Global-Define LCLAIR-KBESTECH-TYPDES   Client,Interne
/* OM : 19/04/2006 G7_1684 Fin */

/* OM : 01/03/2007 D002452 Exportation Normabev */
&Global-Define NORMABEV-ETAT-TRACE-ATE   'ATE':U
&Global-Define NORMABEV-ETAT-TRACE-ATA   'ATA':U
&Global-Define NORMABEV-ETAT-TRACE-ACQ   'ACQ':U
&Global-Define NORMABEV-ETAT-TRACE-ACD   'ACD':U
&Global-Define NORMABEV-ETAT-TRACE-ANO   'ANO':U
&Global-Define LCODE-NORMABEV-ETAT-TRACE    ATE,ATA,ACQ,ACD,ANO
&Global-Define LCLAIR-NORMABEV-ETAT-TRACE   A Envoyer,En attente d''acquittement,Acquitt�,Acquitt� avec doublon,En anomalie

/* Devrait �tre dans mnemogp2.i car PUR FAB, mais en attendant que tous les Atelier -> FAB */
/* SLe 05/03/09 */
/* Plans de rangement */
/* XPLANR.TYPLANR - Type de plan de rangement */
&Global-Define XPLANR-TYPLANR-ART    'ART':U
&Global-Define XPLANR-TYPLANR-ACT    'ACT':U
&Global-Define XPLANR-TYPLANR-ITI    'ITI':U
&Global-Define LCODE-XPLANR-TYPLANR  ART,ACT,ITI
&Global-Define LCLAIR-XPLANR-TYPLANR Article,Activit�,Itin�raire

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



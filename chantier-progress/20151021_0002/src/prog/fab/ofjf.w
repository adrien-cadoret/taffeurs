&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Tablejf _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : fab\ofjf.w
 *
 * Description  : Saisie des OF : SmartFonction sur OF
 * 
 * Template     : fonction.w - SmartFonction 
 * Type Objet   : SmartFonction 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 17/04/1997 � 15:47 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 11/10/2006 - IL      : D002214:Revue des champs de param�tres et utilitaires r�se...
 *                              20060922_0006 (5985)|D002214|Revue des champs de param�tres et utilitaires r�serv�s VIF|IL|11/10/06 09:44:45 
 *                               
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Tablejf _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Tablejf 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/


/* Constante pour sous-fonctions */

   /* La Saisie se fait-elle dans une sous-fonction  */
&Scoped-Define Saisie-Dans-Sous-fonction No
   /* Numero de page o� se trouve la sous-fonction de saisie de la table */
&Scoped-Define Page-Saisie-Table   0

/* Constantes propres � la fonction */
&Scoped-Define Nombre-Pages 5
&SCOPED-DEFINE W-nombrowse "OF":U

/* Variables globales */
{variable.i " "}
{ofttli.i}
{unitebibli.i}
{ofbibli.i}

{admvif/template/fonction.i "Definitions"}

&if DEFINED(UIB_is_Running) NE 0 &Then
 /* Attributs de depart pour test SmartFonction */
 Attributs-De-Depart = "".
&Endif

&Global-Define Adv-User-Attr-Globaux "origine,contexte":U

/* Local Variable Definitions ---                                       */
Define Variable W-Etatof        As Character No-Undo.
Define Variable W-Chrono        AS INTEGER   No-Undo.
Define Variable W-Ret           As Logical   No-Undo.
Define Variable W-Msg           As Character No-Undo.
Define Variable W-Datdeb        As Date      No-undo.
Define Variable W-Datfin        As Date      No-undo.

Define Variable Lance-Ot        As Logical   No-Undo.
Define Variable w-CacherFreinte As Logical   No-Undo.

Define Variable w-lib-titre     As Character No-Undo.

/* GF (27/04/2005) : G7_1298 : Tests sur USERADM ou VIF */
DEFINE VARIABLE vgTestUserVIF   AS LOGICAL   NO-UNDO.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" Tablejf _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartFonction
&Scoped-define DB-AWARE no

&Scoped-define ADM-CONTAINER FRAME

/* Name of first Frame and/or Browse and/or first Query                 */
&Scoped-define FRAME-NAME F-Main

/* Standard List Definitions                                            */
&Scoped-Define ENABLED-OBJECTS RECT-buttons 

/* Custom List Definitions                                              */
/* Btn-SF,List-2,List-3,List-4,List-5,List-6                            */

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Fo-Autorise-OFtech Tablejf 
FUNCTION Fo-Autorise-OFtech RETURNS LOGICAL
  ( I-Etat As Character,
    Buffer kof For kof )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Menu Definitions                                                     */
DEFINE MENU POPUP-MENU-Btn-Ecl 
       MENU-ITEM m_Eclatement   LABEL "Eclatement"    
       MENU-ITEM m_Nouvel_OF_technique LABEL "Nouvel OF technique"
       MENU-ITEM m_Liste        LABEL "Liste"         .


/* Definitions of handles for SmartObjects                              */
DEFINE VARIABLE h_com1jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_comcrjv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_crivjv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_folder AS HANDLE NO-UNDO.
DEFINE VARIABLE h_mvresls AS HANDLE NO-UNDO.
DEFINE VARIABLE h_of1jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_of2jv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_ofjb AS HANDLE NO-UNDO.
DEFINE VARIABLE h_ofjv AS HANDLE NO-UNDO.
DEFINE VARIABLE h_ofl1js AS HANDLE NO-UNDO.
DEFINE VARIABLE h_otjs AS HANDLE NO-UNDO.
DEFINE VARIABLE h_p-updsav AS HANDLE NO-UNDO.

/* Definitions of the field level widgets                               */
DEFINE BUTTON Btn-Ecl 
     LABEL "OF &techniques..." 
     SIZE 20 BY 1.05 TOOLTIP "Cr�ation d'OF techniques".

DEFINE BUTTON Btn-OT 
     LABEL "&O.T." 
     SIZE 20 BY 1.05 TOOLTIP "Ordres de Traitements".

DEFINE BUTTON Btn-ProgDeprog 
     LABEL "" 
     SIZE 20 BY 1.05.

DEFINE BUTTON Btn-res 
     LABEL "&Ressources" 
     SIZE 20 BY 1.05.

DEFINE BUTTON Btn-Stk 
     LABEL "&Stock" 
     SIZE 20 BY 1.05.

DEFINE RECTANGLE RECT-buttons
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 22 BY 5.76.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Btn-OT AT ROW 1.76 COL 137
     Btn-ProgDeprog AT ROW 2.81 COL 137
     Btn-Stk AT ROW 3.86 COL 137
     Btn-res AT ROW 4.91 COL 137
     Btn-Ecl AT ROW 5.95 COL 137
     RECT-buttons AT ROW 1.52 COL 136
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1
         SIZE 159.2 BY 22.05
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartFonction
   Allow: Basic,Browse,DB-Fields,Query,Smart
   Container Links: Page-Target,Record-Source,Record-Target,Navigation-Source,Navigation-Target,TableIO-Source,TableIO-Target
   Design Page: 10
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Tablejf ASSIGN
         HEIGHT             = 22.43
         WIDTH              = 160.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" Tablejf _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Tablejf 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_fonct.i}
{src/adm/method/containr.i}
{admvif/method/ap_fonct.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW Tablejf
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE                                                          */
ASSIGN 
       FRAME F-Main:HIDDEN           = TRUE.

/* SETTINGS FOR BUTTON Btn-Ecl IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       Btn-Ecl:HIDDEN IN FRAME F-Main           = TRUE
       Btn-Ecl:POPUP-MENU IN FRAME F-Main       = MENU POPUP-MENU-Btn-Ecl:HANDLE.

/* SETTINGS FOR BUTTON Btn-OT IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn-ProgDeprog IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn-res IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR BUTTON Btn-Stk IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = ""
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME F-Main
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Main Tablejf
ON HELP OF FRAME F-Main
DO:
  /* Gestion standard de l'aide en ligne */
  {admvif/template/tinclude.i "Help"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-Ecl
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-Ecl Tablejf
ON CHOOSE OF Btn-Ecl IN FRAME F-Main /* OF techniques... */
DO:

  Run Display-Popup-Menu in adm-broker-hdl (self:Popup-Menu).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-OT
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-OT Tablejf
ON CHOOSE OF Btn-OT IN FRAME F-Main /* O.T. */
DO:
  Run page-sousfct(9).
  Lance-Ot = Yes.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-ProgDeprog
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-ProgDeprog Tablejf
ON CHOOSE OF Btn-ProgDeprog IN FRAME F-Main
DO:

  /* PROGRAMMATION */
  
/* NLE 21/11/2002 */
/*  If Btn-ProgDeprog:Label In Frame {&Frame-Name} = Get-TradTxt (004643, '&Programmer':T) Then Do:*/
  If W-etatOf = {&CETAT-OF-CALCULE} Then Do:

    Run New-State('ProgrammerOF,Info1-Source':U).
    Run Get-Info ("chrono":u,"Info1-Source":u).
    If Return-Value = ? Then
       Return "Adm-Error":u.
    Assign W-chrono = integer(return-Value).
    Find Kof Where Kof.Csoc    = Wcsoc
               And Kof.Cetab   = Wcetab
               And Kof.prechro = {&PRECHRO-OF}
               And Kof.Chrono  = W-Chrono
              No-Lock No-Error.
    If Available kof And
       Kof.Cetat = {&CETAT-OF-PLANIFIE} Then do With Frame {&Frame-Name}:
   
       Assign Btn-ProgDeprog:Label = Get-TradTxt (004655, 'D�&programmer':T).
    End.   
                                           
  End. /* PROGRAMMATION */
  
  /* DEPROGRAMMATION */
  Else 
/* NLE 21/11/2002 */
/*  If Btn-ProgDeprog:Label In Frame {&Frame-Name} = Get-TradTxt (004655, 'D�&programmer':T) Then Do:*/
  If W-etatOf = {&CETAT-OF-PLANIFIE} Then Do:
  
    Run New-State('DeProgrammerOF,Info1-Source':U).
    Run Get-Info ("chrono":u,"Info1-Source":u).
    If Return-Value = ? Then Return "Adm-Error":u.

    Assign W-chrono = integer(return-Value).
    Find Kof Where Kof.Csoc    = Wcsoc
               And Kof.Cetab   = Wcetab
               And Kof.prechro = {&PRECHRO-OF}
               And Kof.Chrono  = W-Chrono
              No-Lock No-Error.
    If Available kof And
       Kof.Cetat = {&CETAT-OF-CALCULE} Then do With Frame {&Frame-Name}:
   
       Assign Btn-ProgDeprog:Label = Get-TradTxt (004643, '&Programmer':T).
    End.   

  End. /* DEPROGRAMMATION */

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-res
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-res Tablejf
ON CHOOSE OF Btn-res IN FRAME F-Main /* Ressources */
DO:
  If W-etatof = {&CETAT-OF-MODIFIE} Then Do:
    {affichemsg.i
        &Message="Get-TradMsg (004660, 'Pas de mouvements de ressources sur un OF � l''�tat Modifi� !':T)"
    }
  End.
  Else do:
/*     Run init-attribute(6).
 *    Run page-sousfct(6). */
     Run init-attribute(8).
     Run page-sousfct(8).
  End.

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Btn-Stk
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Btn-Stk Tablejf
ON CHOOSE OF Btn-Stk IN FRAME F-Main /* Stock */
DO:
  If W-etatof = {&CETAT-OF-MODIFIE} Then Do:
    {affichemsg.i
        &Message="Get-TradMsg (004663, 'Pas de mouvements de stock sur un OF � l''�tat Modifi� !':T)"
    }
  End.
  Else do:
  
      Run Get-Info ("Rowid-Of":u, "Info1-Source":u).
  
      Find first kof where Rowid(kof) = To-Rowid(Return-Value)
                         No-lock No-error.
  
      If NOT AVAILABLE kof THEN RETURN NO-APPLY.  

      Wstr = ''.
      IF kof.cetat > {&CETAT-OF-CALCULE} THEN
         Wstr = SUBSTITUTE ('|TYPRES=LIS�&1,&2,&3':U, {&TYPRES-RES}, {&TYPRES-FER}, {&TYPRES-PLA}).
  
      RUN Set-Contexte IN Adv-Outils-Hdl 
          (INPUT THIS-PROCEDURE:Handle,
           INPUT "CST":U,
           INPUT SUBSTITUTE ("PRECHRO=UNI�&1|CHRONO=UNI�&2|DATE=BOR�&3�&4&5|CTRL-DEPPrev=UNI�O":U,
                             kof.prechro, String(kof.chrono), String(kof.datdeb), String(kof.datfin), Wstr)).
  
      RUN Lance-Fonction IN Adv-Outils-Hdl ("CUMARTL":U).
/*    YG 13/07/04 
 *     Run init-attribute(7).
 *     Run page-sousfct(7).*/
  End.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Eclatement
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Eclatement Tablejf
ON CHOOSE OF MENU-ITEM m_Eclatement /* Eclatement */
DO:
    /* NLE 30/08/2004 */

    Define Buffer B-kof For kof.

    Define Variable w-NbOFtech          As Integer   No-Undo.
    Define Variable w-QteOFtechEff      As Decimal   No-Undo.
    Define Variable w-QteOFtech2Eff     As Decimal   No-Undo.
    Define Variable w-QteResteApresEff  As Decimal   No-Undo.
    Define Variable w-QteOFtechStk      As Decimal   No-Undo.
    Define Variable w-QteOFtech2Stk     As Decimal   No-Undo.
    Define Variable w-QteResteApresStk  As Decimal   No-Undo.
    Define Variable w-lotent            As Character No-Undo.
    Define Variable w-lotent-qte        As Decimal   No-Undo.
    Define Variable w-lotent-cu         As Character No-Undo.
    Define Variable w-lotent-cdepot     As Character No-Undo.
    Define Variable w-lotent-cemp       As Character No-Undo.

    Define Variable w-cuniteEff      AS CHARACTER No-Undo.
   
    Define Variable w-QteBatch    AS DECIMAL   No-Undo.
    Define Variable w-QteRest     AS DECIMAL   No-Undo.
    Define Variable w-cunite      AS CHARACTER No-Undo.
    Define Variable w-Ret         As Logical   No-Undo.

    Define Variable w-1erOFtech   As Integer   No-Undo.
    Define Variable w-DerOFtech   As Integer   No-Undo.

    RUN Notify ('adv-auto-update,Ecla-Target':U).   
    If Return-Value = 'Adm-Error':U Then Return No-Apply.

    RUN Get-Info ("RowId-OF":U, "Info1-Source":U).
    If Return-Value = ? Then Return No-Apply.
   
    Find B-kof Where RowId (B-kof) = To-RowId (Return-Value) No-Lock No-Error.
    If Not Available B-kof Then Return No-Apply.
       
    /* NLE 07/11/2003 */
    If w-CacherFreinte Then 
        RUN ofeclasd.w  (Input  B-kof.csoc,          /* sans freinte */
                         Input  B-kof.cetab,
                         Input  B-kof.prechro,
                         Input  B-kof.chrono,
                         Output w-QteOFtechEff,
                         Output w-QteOFtechStk,
                         Output w-NbOFtech,
                         Output w-QteOFtech2Eff,
                         Output w-QteOFtech2Stk,
                         Output w-QteResteApresEff,
                         Output w-QteResteApresStk,
                         Output w-lotent,
                         Output w-lotent-qte,
                         Output w-lotent-cu,
                         Output w-lotent-cdepot,
                         Output w-lotent-cemp,
                         Output w-Ret).

    Else
        RUN ofecla3sd.w (Input  B-kof.csoc,          /* avec freinte */
                         Input  B-kof.cetab,
                         Input  B-kof.prechro,
                         Input  B-kof.chrono,
                         Output w-QteOFtechEff,
                         Output w-QteOFtechStk,
                         Output w-NbOFtech,
                         Output w-QteOFtech2Eff,
                         Output w-QteOFtech2Stk,
                         Output w-QteResteApresEff,
                         Output w-QteResteApresStk,
                         Output w-lotent,
                         Output w-lotent-qte,
                         Output w-lotent-cu,
                         Output w-lotent-cdepot,
                         Output w-lotent-cemp,
                         Output w-Ret).

    If Not w-Ret Then Return No-Apply.
    
    If w-QteOFtechStk = 0 And
       w-NbOFtech     = 0 Then Do:
        {affichemsg.i       &Message = "Substitute(Get-TradMsg (022701, 'Confirmez-vous la mise � jour de la quantit� r�siduelle � &1 ?':T), 
                                       Left-Trim({&Unite-Affichage-Qte-Lg} (w-QteResteApresStk, B-kof.cunite, B-kof.cunite, 20)))"
                            &TypeBtn = {&MsgOuiNon}
                            &BtnDef  = 1}
    End.
    Else Do:
        {affichemsg.i       &Message = "Substitute(Get-TradMsg (004618, 'Confirmez-vous le lancement de l''�clatement de l''OF &1 en &2 OF techniques de &3&4,|avec une quantit� r�siduelle de &5, ainsi que la g�n�ration des �ventuelles affectations ?':T), 
                                                   String(B-kof.chrono), 
                                                   String(w-NbOFtech), 
                                                   Left-Trim({&Unite-Affichage-Qte-Lg} (w-QteOFtechStk, B-kof.cunite, B-kof.cunite, 20)), 
                                                   If w-QteOFtech2Stk = 0 Then '' 
                                                                           Else ' ':U + Substitute(Get-TradTxt (022689, '+ 1 OF technique de &1 &2':T), 
                                                                                           Left-Trim({&Unite-Affichage-Qte-Lg} (w-QteOFtech2Stk, B-kof.cunite, B-kof.cunite, 20))), 
                                                   Left-Trim({&Unite-Affichage-Qte-Lg} (w-QteResteApresStk, B-kof.cunite, B-kof.cunite, 20)))"
                      &TypeBtn = {&MsgOuiNon}
                      &BtnDef  = 1}
    End.

    If ChoixMsg <> 1 Then Return No-Apply.

    If w-QteOFtechStk = 0 And
       w-NbOFtech     = 0 Then 
        RUN Init-Box-Traitement In Adv-Outils-Hdl 
           (Substitute(Get-TradTxt (022702, 'Mise � jour du Reste � faire de l''OF &1 en cours...':T), String (B-kof.chrono)),
            "Type=Normal, longueur = 30":U,
            "":U, "":U, "":U).
    Else               
        RUN Init-Box-Traitement In Adv-Outils-Hdl 
           (Substitute(Get-TradTxt (004625, 'Cr�ation des OF techniques de l''OF &1 et des �ventuelles affectations en cours ...':T), String (B-kof.chrono)),
            "Type=Normal, longueur = 30":U,
            "":U, "":U, "":U).

    {&Run-Prog} ofeclajp.p (Input  B-kof.csoc,
                            Input  B-kof.cetab,
                            Input  B-kof.prechro,
                            Input  B-kof.chrono,
                            Input  w-NbOFtech,
                            Input  w-QteOFtechEff,
                            Input  w-QteOFtechStk,
                            Input  w-QteOFtech2Eff,
                            Input  w-QteOFtech2Stk,
                            Input  w-QteResteApresEff,
                            Input  w-QteResteApresStk,
                            Input  B-kof.cunite,
                            Input  wcuser,
                            Input  w-lotent,
                            Input  w-lotent-qte,
                            Input  w-lotent-cu,
                            Input  w-lotent-cdepot,
                            Input  w-lotent-cemp,
                            Output Table tt-kof,
                            Output w-Ret,
                            Output w-Msg).

    If Not w-ret Then RUN adm-display-fields IN h_comcrjv.
                 ELSE RUN new-state('refresh,refresh-Target':U).

    RUN Delete-Box-Traitement In Adv-Outils-Hdl.

    Assign w-1erOFtech = 0
           w-DerOFtech = 0.
    For First tt-kof Fields(chrono)
                     No-Lock
                     By tt-kof.csoc
                     By tt-kof.cetab
                     By tt-kof.prechro:
        w-1erOFtech = tt-kof.chrono.
    End.
    For Last tt-kof  Fields(chrono)
                     No-Lock
                     By tt-kof.csoc
                     By tt-kof.cetab
                     By tt-kof.prechro:
        w-DerOFtech = tt-kof.chrono.
    End.

    If w-QteOFtechStk = 0 And
       w-NbOFtech     = 0 Then Do:
        If Not w-Ret Then Do:
           {affichemsg.i       &Message = "Substitute(Get-TradMsg (005029, 'La mise � jour s''est mal pass�e.|&1':T), w-msg)"}
           RUN Set-Info ("RefreshBrowse=":U + String (RowId (B-kof)), "Info1-Source":U).
        End.
        Else Do: 
           /* D�clenchement d'un Open Query dans le browse */
           RUN Set-Info ("RefreshBrowse=":U + String (RowId (B-kof)), "Info1-Source":U).
           {affichemsg.i       &Message = "Substitute(Get-TradMsg (000725, 'Le traitement &1 s''est bien pass�.':T), '')"
                                          &TypeMsg={&MsgInformation}}
        End.
    End.
    Else Do:
        If Not w-Ret Then Do:
           {affichemsg.i &Message="Substitute(Get-TradMsg (004630, 'La cr�ation des OF techniques de l''OF &1 et/ou des �ventuelles affectations s''est mal pass�e (du &2 au &3).|&4':T), String (B-kof.chrono), String(w-1erOFtech), String(w-DerOFtech), w-Msg)"}
           RUN Set-Info ("RefreshBrowse=":U + String (RowId (B-kof)), "Info1-Source":U).
        End.
        Else Do: 
           /* D�clenchement d'un Open Query dans le browse */
           RUN Set-Info ("RefreshBrowse=":U + String (RowId (B-kof)), "Info1-Source":U).
           {affichemsg.i &Message="Substitute(Get-TradMsg (004638, 'La cr�ation des OF techniques de l''OF &1 et des �ventuelles affectations s''est bien pass�e (du &2 au &3).':T), String(B-kof.chrono), String(w-1erOFtech), String(w-DerOFtech))"
                         &TypeMsg={&MsgInformation}}
        End.
    End.

    /* NLE 03/06/2005 */
    RUN Droits-Btn (B-kof.cetat).   /* NLE 14/06/2005 */
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Liste
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Liste Tablejf
ON CHOOSE OF MENU-ITEM m_Liste /* Liste */
DO:
    /* NLE 30/08/2004 */
  
    Define Variable w-prechro    As Character    No-Undo.
    Define Variable w-chrono     As Integer      No-Undo.
    Define Variable w-reposition As Character    No-Undo.

    
    Run Get-Info ("Rowid-Of":u, "Info1-Source":u).
  
    Find first kof where Rowid(kof) = To-Rowid(Return-Value)
                   No-lock No-error.

    Case kof.typof :
        When {&TYPOF-OFORIG} Then 
            Assign w-prechro = kof.prechro
                   w-chrono  = kof.chrono.
                   
        When {&TYPOF-OFTECH} Then 
            Assign w-prechro = kof.prechror
                   w-chrono  = kof.chronor.
                   
        Otherwise Return.
    End.
        
        
/*    {&Run-prog} "admvif/objects/a-standa.w":U 
 *                 ("ofj":U,            /* code de la fonction */
 *                  "ofjb":U, "origine=OF-TECH,kof.prechror=":U + w-prechro + ",kof.chronor=":U + String(w-chrono), /* Nom browser et attributs */
 *                  "":U, "":U,          /* Nom viewer et attributs */
 *                  Substitute(w-lib-titre, String(w-chrono)),         /* Titre ecran d'aide */
 *                  no,                  /* Acces bouton nouveau */
 *                  If kof.typof = {&TYPOF-OFTECH} Then String(kof.chrono) Else '',  /* Valeur de depart */
 *                  Output wstr).*/


    {&Run-Prog} oftechla.w (Input  "",
                            Input  No,
                            Input  If kof.typof = {&TYPOF-OFTECH} Then String(kof.chrono) Else '',
                            Output w-reposition,
                        
                            Input  'OF-TECH':U,
                            Input  w-prechro,
                            Input  String(w-chrono),
                            Input  Substitute(w-lib-titre, String(w-chrono))).

    If w-reposition <> ? Then
        Run Set-Info("position-record=":u + w-reposition,"Info1-Source":u).

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME m_Nouvel_OF_technique
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL m_Nouvel_OF_technique Tablejf
ON CHOOSE OF MENU-ITEM m_Nouvel_OF_technique /* Nouvel OF technique */
DO:

  /* NLE 30/08/2004 */
  
  Run SetTitle(Get-TradTxt (022696, 'Nouvel OF technique':T)).
  Run New-State("NEW-OF-TECH,Ecla-Target").
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Tablejf 


{admvif/template/fonction.i "Triggers"}

{admvif/template/fonction.i "Main-Block"}

{admvif/template/fonction.i "Procedures-Internes"}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-create-objects Tablejf  _ADM-CREATE-OBJECTS
PROCEDURE adm-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Create handles for all SmartObjects used in this procedure.
               After SmartObjects are initialized, then SmartLinks are added.
  Parameters:  <none>
------------------------------------------------------------------------------*/
  DEFINE VARIABLE adm-current-page  AS INTEGER NO-UNDO.

  RUN get-attribute IN THIS-PROCEDURE ('Current-Page':U).
  ASSIGN adm-current-page = INTEGER(RETURN-VALUE).

  CASE adm-current-page: 

    WHEN 0 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'ofjb.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'cartd = ,
                     cartf = ,
                     cetat = ,
                     cres = ,
                     criteres = ,
                     csecteur = ,
                     datdeb = ,
                     datfin = ,
                     kof.chronor = ,
                     kof.prechror = ,
                     origine = ,
                     tout = ,
                     Appel-Aide = Non,
                     Btn-SF = Oui,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     Suppression-Message = ,
                     csce = ':U ,
             OUTPUT h_ofjb ).
       RUN set-position IN h_ofjb ( 1.52 , 19.00 ) NO-ERROR.
       RUN set-size IN h_ofjb ( 5.81 , 114.00 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/p-updsav.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'SmartPanelType = Save,
                     Edge-Pixels = 0,
                     SaveFunction = AddMultiple,
                     NameBtnAction = ,
                     Hide-Save = no,
                     Hide-Add = no,
                     Hide-Copy = no,
                     Hide-Delete = no,
                     Hide-cancel = no,
                     Hide-Print = yes,
                     Hide-Documents = Yes,
                     PrintFunction = Normale,
                     Layout-Options = "Master Layout,Standard Character",
                     Default-Layout = Master Layout,
                     OrientPanel = Horizontal':U ,
             OUTPUT h_p-updsav ).
       RUN set-position IN h_p-updsav ( 2.05 , 6.00 ) NO-ERROR.
       /* Size in UIB:  ( 1.86 , 10.00 ) */

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'admvif/objects/folder.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'FOLDER-TAB-TYPE = 2,
                     FOLDER-LABELS = ':U + 'G�n�ral|Suite|Commentaire|Anomalie' ,
             OUTPUT h_folder ).
       RUN set-position IN h_folder ( 8.33 , 1.00 ) NO-ERROR.
       RUN set-size IN h_folder ( 14.71 , 159.20 ) NO-ERROR.

       /* Links to SmartBrowser h_ofjb. */
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'info3':U , h_ofjb ).
       RUN add-link IN adm-broker-hdl ( h_ofjb , 'Info1':U , THIS-PROCEDURE ).

       /* Links to SmartFolder h_folder. */
       RUN add-link IN adm-broker-hdl ( h_folder , 'Page':U , THIS-PROCEDURE ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_ofjb ,
             Btn-OT:HANDLE IN FRAME F-Main , 'BEFORE':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_folder ,
             Btn-Ecl:HANDLE IN FRAME F-Main , 'AFTER':U ).
    END. /* Page 0 */
    WHEN 1 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'ofjv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Non,
                     Suppression-Reponse = Non,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     Suppresion-Message = ':U ,
             OUTPUT h_ofjv ).
       RUN set-position IN h_ofjv ( 10.38 , 8.00 ) NO-ERROR.
       RUN set-size IN h_ofjv ( 8.91 , 143.40 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'criv2jv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Layout-Options = "Master Layout,Standard Character",
                     aff-statut = {&INIT-ZCRI-AFF-STATUT-NON},
                     longueur = 717,
                     mode = ,
                     mode-statut = {&INIT-ZCRI-MODE-STATUT-CONSULT},
                     table = kcriv,
                     tqualite = OUI,
                     valeur-attendue = {&INIT-ZCRI-VALEUR-ATTENDUE-NON},
                     Hide-Links-With-Object = No,
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Default-Layout = Master Layout,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_crivjv ).
       RUN set-position IN h_crivjv ( 20.38 , 8.00 ) NO-ERROR.
       RUN set-size IN h_crivjv ( 1.19 , 6.20 ) NO-ERROR.

       /* Links to SmartViewer h_ofjv. */
       RUN add-link IN adm-broker-hdl ( h_ofjb , 'info5':U , h_ofjv ).
       RUN add-link IN adm-broker-hdl ( h_ofjb , 'Record':U , h_ofjv ).
       RUN add-link IN adm-broker-hdl ( h_p-updsav , 'TableIO':U , h_ofjv ).
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'Ecla':U , h_ofjv ).

       /* Links to SmartViewerSpe h_crivjv. */
       RUN add-link IN adm-broker-hdl ( h_ofjb , 'Record':U , h_crivjv ).
       RUN add-link IN adm-broker-hdl ( h_ofjv , 'criteres':U , h_crivjv ).
       RUN add-link IN adm-broker-hdl ( h_ofjv , 'group-assign':U , h_crivjv ).
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'heritage-crit':U , h_crivjv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_ofjv ,
             h_folder , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_crivjv ,
             h_ofjv , 'AFTER':U ).
    END. /* Page 1 */
    WHEN 2 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'of1jv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ':U ,
             OUTPUT h_of1jv ).
       RUN set-position IN h_of1jv ( 12.38 , 10.60 ) NO-ERROR.
       RUN set-size IN h_of1jv ( 6.48 , 143.00 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartViewer h_of1jv. */
       RUN add-link IN adm-broker-hdl ( h_ofjb , 'Record':U , h_of1jv ).
       RUN add-link IN adm-broker-hdl ( h_ofjv , 'group-assign':U , h_of1jv ).
       RUN add-link IN adm-broker-hdl ( h_ofjv , 'info6':U , h_of1jv ).
       RUN add-link IN adm-broker-hdl ( THIS-PROCEDURE , 'refresh':U , h_of1jv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_of1jv ,
             h_folder , 'AFTER':U ).
    END. /* Page 2 */
    WHEN 3 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'com1jv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'cetab = ,
                     csoc = ,
                     Titre = ,
                     typcle = {&INIT-COM-OF-COM},
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Layout-Options = "Master Layout,Standard Character",
                     Default-Layout = Master Layout,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no,
                     cle-int-ext = ,
                     Suppression-Message = ,
                     cle = ':U ,
             OUTPUT h_com1jv ).
       RUN set-position IN h_com1jv ( 11.10 , 32.40 ) NO-ERROR.
       RUN set-size IN h_com1jv ( 7.38 , 82.40 ) NO-ERROR.

       RUN init-object IN THIS-PROCEDURE (
             INPUT  'of2jv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     Suppression-Confirme = Oui,
                     Suppression-Reponse = Non,
                     Suppression-Message = ':U ,
             OUTPUT h_of2jv ).
       RUN set-position IN h_of2jv ( 19.00 , 48.40 ) NO-ERROR.
       RUN set-size IN h_of2jv ( 1.52 , 75.00 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartViewer h_com1jv. */
       RUN add-link IN adm-broker-hdl ( h_ofjb , 'Record':U , h_com1jv ).
       RUN add-link IN adm-broker-hdl ( h_ofjv , 'group-assign':U , h_com1jv ).
       RUN add-link IN adm-broker-hdl ( h_ofjv , 'info2':U , h_com1jv ).

       /* Links to SmartViewer h_of2jv. */
       RUN add-link IN adm-broker-hdl ( h_ofjb , 'Record':U , h_of2jv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_com1jv ,
             h_folder , 'AFTER':U ).
       RUN adjust-tab-order IN adm-broker-hdl ( h_of2jv ,
             h_com1jv , 'AFTER':U ).
    END. /* Page 3 */
    WHEN 4 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'comcrjv.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  't-Ano = Oui,
                     Titre = Anomalies d�tect�es,
                     Hide-Links-With-Object = No,
                     Initial-Lock = DEFAULT,
                     Dern-Champ-Go = No,
                     DISABLE-ON-INIT = no,
                     HIDE-ON-INIT = no':U ,
             OUTPUT h_comcrjv ).
       RUN set-position IN h_comcrjv ( 10.95 , 16.00 ) NO-ERROR.
       RUN set-size IN h_comcrjv ( 9.95 , 130.00 ) NO-ERROR.

       /* Initialize other pages that this page requires. */
       RUN init-pages IN THIS-PROCEDURE ('1':U) NO-ERROR.

       /* Links to SmartViewerSpe h_comcrjv. */
       RUN add-link IN adm-broker-hdl ( h_ofjb , 'info2':U , h_comcrjv ).
       RUN add-link IN adm-broker-hdl ( h_ofjb , 'Record':U , h_comcrjv ).
       RUN add-link IN adm-broker-hdl ( h_ofjv , 'group-assign':U , h_comcrjv ).
       RUN add-link IN adm-broker-hdl ( h_ofjv , 'info4':U , h_comcrjv ).

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_comcrjv ,
             h_folder , 'AFTER':U ).
    END. /* Page 4 */
    WHEN 8 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'mvresls.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'DirectOF = oui,
                     typlan = OF,
                     TypeClose = Normal':U ,
             OUTPUT h_mvresls ).
       RUN set-position IN h_mvresls ( 1.00 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_mvresls ,
             Btn-OT:HANDLE IN FRAME F-Main , 'BEFORE':U ).
    END. /* Page 8 */
    WHEN 9 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'otjs.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'TypeClose = Normal':U ,
             OUTPUT h_otjs ).
       RUN set-position IN h_otjs ( 1.00 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_otjs ,
             Btn-OT:HANDLE IN FRAME F-Main , 'BEFORE':U ).
    END. /* Page 9 */
    WHEN 10 THEN DO:
       RUN init-object IN THIS-PROCEDURE (
             INPUT  'ofl1js.w':U ,
             INPUT  FRAME F-Main:HANDLE ,
             INPUT  'origine = OF,
                     TypeClose = Normal':U ,
             OUTPUT h_ofl1js ).
       RUN set-position IN h_ofl1js ( 1.00 , 1.00 ) NO-ERROR.
       /* Size in UIB:  ( 2.00 , 8.40 ) */

       /* Adjust the tab order of the smart objects. */
       RUN adjust-tab-order IN adm-broker-hdl ( h_ofl1js ,
             Btn-OT:HANDLE IN FRAME F-Main , 'BEFORE':U ).
    END. /* Page 10 */

  END CASE.
  /* Select a Startup page. */
  IF adm-current-page eq 0 
  THEN RUN select-page IN THIS-PROCEDURE ( 1 ).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available Tablejf  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Tablejf  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Droits-Btn Tablejf 
PROCEDURE Droits-Btn :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
Define Input Parameter I-Etat       As Character     No-Undo.

Define Variable w-Btn-Sensitive     As Logical       No-Undo.
Define Variable w-crea-oftech-autor As Logical       No-Undo.
Define Variable w-au-moins-1        As Logical       No-Undo.
Define Variable whdl-MENU-ITEM      As Widget-Handle No-Undo.

/* NLE 19/12/2002 */
/*  w-Btn-Sensitive = I-Etat >= {&CETAT-OF-CALCULE} And*/
  w-Btn-Sensitive = I-Etat >= {&CETAT-OF-MODIFIE} And
                    I-Etat <> '':U And
                    I-Etat <> '?':U .

  Do With Frame {&FRAME-NAME}:
     Assign Btn-OT:sensitive           = w-Btn-Sensitive
            Btn-Stk:sensitive          = w-Btn-Sensitive
            Btn-Res:sensitive          = w-Btn-Sensitive
            Btn-ProgDeprog:sensitive   = Btn-ProgDeprog:Label <> '':u.

     /* NLE 30/08/2004 */
     Run Get-Info ("Rowid-Of":u, "Info1-Source":u).
     Find First kof Where Rowid(kof) = To-Rowid(Return-Value) No-lock No-error.

     If Available kof Then
         Assign w-au-moins-1             = No
                w-crea-oftech-autor      = w-Btn-Sensitive And Fo-Autorise-OFtech(I-Etat, Buffer kof)
                whdl-MENU-ITEM           = MENU-ITEM m_Eclatement:Handle In MENU POPUP-MENU-Btn-Ecl
                whdl-MENU-ITEM:Sensitive = w-crea-oftech-autor
                w-au-moins-1             = w-au-moins-1 Or whdl-MENU-ITEM:Sensitive
                whdl-MENU-ITEM           = MENU-ITEM m_Nouvel_OF_technique:Handle In MENU POPUP-MENU-Btn-Ecl
                whdl-MENU-ITEM:Sensitive = w-crea-oftech-autor
                w-au-moins-1             = w-au-moins-1 Or whdl-MENU-ITEM:Sensitive
                whdl-MENU-ITEM           = MENU-ITEM m_Liste:Handle In MENU POPUP-MENU-Btn-Ecl
                whdl-MENU-ITEM:Sensitive = w-Btn-Sensitive And 
                                           (kof.typof = {&TYPOF-OFORIG} Or 
                                            kof.typof = {&TYPOF-OFTECH}) 
                w-au-moins-1             = w-au-moins-1 Or whdl-MENU-ITEM:Sensitive
                Btn-Ecl:sensitive        = w-Btn-Sensitive And w-au-moins-1.
     Else
         Btn-Ecl:sensitive = False.
     /* NLE 30/08/2004 fin */
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable_UI Tablejf  _DEFAULT-ENABLE
PROCEDURE enable_UI :
/*------------------------------------------------------------------------------
  Purpose:     ENABLE the User Interface
  Parameters:  <none>
  Notes:       Here we display/view/enable the widgets in the
               user-interface.  In addition, OPEN all queries
               associated with each FRAME and BROWSE.
               These statements here are based on the "Other 
               Settings" section of the widget Property Sheets.
------------------------------------------------------------------------------*/
  ENABLE RECT-buttons 
      WITH FRAME F-Main.
  {&OPEN-BROWSERS-IN-QUERY-F-Main}
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE init-Attribute Tablejf 
PROCEDURE init-Attribute :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
Define input Parameter i-page As integer No-undo.

  Run Get-Info ("Rowid-Of":u, "Info1-Source":u).

  Find first kof where Rowid(kof) = To-Rowid(Return-Value)
                 No-lock No-error.

  If available kof then do:

     Case  i-page:
/*       When 6 then Do:
          If valid-handle(h_mvresls ) then 
             Run Set-Attribute-List In h_mvresls 
                     ("chrono-of=":U + String(kof.chrono) + 
                      ",datdeb=":U + String(kof.datdeb) +
                      ",datfin=":U + String(kof.datfin)).
       End.
*/
/*       YG 13/07/04 
 *        When 7 then Do:
 *           If valid-handle(h_cumartls ) then 
 *              Run Set-Attribute-List In h_cumartls 
 *                     ("chrono-of=":U + String(kof.chrono) + 
 *                      ",precho-of=":U + kof.prechro + 
 *                      ",datdeb=":U + String(kof.datdeb) +
 *                      ",datfin=":U + String(kof.datfin)).
 *        End.*/
       When 8 then Do:
          If valid-handle(h_mvresls ) then Do:
             Run Set-Attribute-List In h_mvresls 
                ("CleOF=":U + kof.csoc + '�':u + kof.cetab + '�':u + kof.prechro + '�':u + String(kof.chrono)).
             Run AppelOF In h_mvresls. /* Fait passer les infos au browse mvreslb */
          End.
       End.

     End Case.                                       
  End.


END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Init-BtnEcl Tablejf 
PROCEDURE Init-BtnEcl :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  Define Variable w-tDroitEcla As Logical No-Undo.
  
  w-tDroitEcla = False.
  
  If vgTestUserVIF Then
     w-tDroitEcla = True.
  Else Do:
/* IL 22/09/06 Activer syst�matiquement les OF techniques   */  
/*      {getparii.i &CPACON  = "'OFCRE3':U"                  */
/*                  &NOCHAMP = 7}                            */
/*      If vParam-Ok And vParam-Valeurs = "Yes":U  Then Do:  */
        {getparii.i &CPACON  = "'OFCRE3':U"
                    &NOCHAMP = 8}
        /* NB 26/10/2005 : ajout vParam-Valeurs = "":U */
        If Not vParam-Ok Or vParam-Valeurs = "":U OR LookUp (wcuser, vParam-Valeurs, ",":U) <> 0 Then
           w-tDroitEcla = True.         
/*      End. */
  End.

  If w-tDroitEcla Then Btn-Ecl:hidden In Frame {&Frame-Name} = False.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-adv-close Tablejf 
PROCEDURE local-adv-close :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  /*cdu 22/10/03*/
  Run New-State("Suppr-pyramide,Container-Target").
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'adv-close':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-create-objects Tablejf 
PROCEDURE local-create-objects :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'create-objects':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Run Get-Attribute ("Current-Page":U).
  Run init-attribute(Int(Return-Value)).
/*  
 *   Case  Int(Return-Value):
 *     When 6 then Do:
 *     
 *       Run Get-Info ("chrono","Info1-Source").
 *       Assign W-chrono = integer(return-Value).
 *       Run Get-Info ("dated","Info1-Source").
 *       Assign W-Datdeb = date(return-Value).
 *       Run Get-Info ("datef","Info1-Source").
 *       Assign W-Datfin = date(return-Value).
 * 
 *       Run Set-Attribute-List In h_mvrls ("chrono-of=":U + String(w-chrono) + 
 *                                          ",datdeb=":U + String(w-datdeb) +
 *                                          ",datfin=":U + String(w-datfin)).
 *     End.                                       
 *     When 7 then Do:
 *     
 *       Run Get-Info ("chrono","Info1-Source").
 *       Assign W-chrono = integer(return-Value).
 *       Run Get-Info ("dated","Info1-Source").
 *       Assign W-Datdeb = date(return-Value).
 *       Run Get-Info ("datef","Info1-Source").
 *       Assign W-Datfin = date(return-Value).
 * 
 *       Run Set-Attribute-List In h_cumartls ("chrono-of=":U + String(w-chrono) + 
 *                                             ",datdeb=":U + String(w-datdeb) +
 *                                             ",datfin=":U + String(w-datfin)).
 *     End.                                       
 *   End.*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize Tablejf 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  Run Get-Attribute ("UIB-mode":U).
  If Return-Value <> "Design":U Then Do With Frame {&Frame-Name}:

     /* GF (27/04/2005) : G7_1298 : Tests sur USERADM ou VIF */
     RUN Verifier-Famille IN Adv-Outils-Hdl (INPUT wcuser,
                                             INPUT NO,
                                             INPUT YES, /* VIF */
                                             OUTPUT vgTestUserVif).

     i = 5.

     Run Init-BtnEcl. 
     If Btn-Ecl:hidden Then i = 4.

     Run ctrl-acces-fct in adv-outils-hdl
        ("OFMVRESL":u,{&CTRL-ACCES-MODIFIER}).
     
     If Return-Value = "No":u then Do :
        Assign Btn-Res:visible          = False
               Btn-Res:Sensitive        = False.
        If i = 4 Then i = 3.
     End.

     Run ctrl-acces-fct in adv-outils-hdl
        ("CUMARTL":u,{&CTRL-ACCES-MODIFIER}).

     If Return-Value = "No":u then Do With Frame {&Frame-Name}:
        Assign Btn-Stk:visible          = False
               Btn-Stk:Sensitive        = False.
        If i = 3 then i = 2.
     End.

     Run ctrl-acces-fct in adv-outils-hdl
        ("OF1J":u,{&CTRL-ACCES-MODIFIER}).

     If Return-Value = "No":u then Do With Frame {&Frame-Name}:
        Assign Btn-ProgDeprog:visible          = False
               Btn-ProgDeprog:Sensitive        = False.
        If i = 2 then i = 1.
     End.

     If i <> 5 then do:
        Rect-buttons:Height-Chars = Btn-Ot:Height-Chars * i + 0.5.
     End.


     /* NLE 07/11/2003 */
     {getparii.i &CPACON="'ACTIVITE':U" 
                 &NOCHAMP=2
                 &CSOC=wcsoc
                 &CETAB=wcetab
     } 
     w-CacherFreinte = (vParam-Ok And vParam-Valeurs = 'Yes':u ).

     w-lib-titre = Get-TradTxt (022715, 'OF techniques de l''OF d''origine &1':T).
  End.

 
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
/*  Btn-Lien-Pere:Menu-Mouse In Frame {&Frame-Name} = 1.*/
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-Retour-Sous-Fonction Tablejf 
PROCEDURE local-Retour-Sous-Fonction :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'Retour-Sous-Fonction':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If Lance-Ot Then Do :
     Run New-State("Refresh-Data,Info3-Target":U).
     Lance-Ot = No.
  End.

/*  If Lance-Liens Then Do :
 *      Run Local-Display-Fields In h_oflienjv . 
 *      Lance-Liens = No.
 *   End.
 *   */
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info Tablejf 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
       /* Ins�rer ici vos propres infos � recevoir */
       When "Etat-OF":u Then Do:
            W-etatOf = valeur-Info.
            Case Valeur-Info :
              When {&CETAT-OF-CALCULE} Then
                Assign Btn-ProgDeprog:Label In Frame {&Frame-Name} = Get-TradTxt (004643, '&Programmer':T).
              When {&CETAT-OF-PLANIFIE} Then
                Assign Btn-ProgDeprog:Label In Frame {&Frame-Name} = Get-TradTxt (004655, 'D�&programmer':T).
              When {&CETAT-OF-MODIFIE} Then
                Assign Btn-ProgDeprog:Label In Frame {&Frame-Name} = "":u.

              Otherwise 
                Assign Btn-ProgDeprog:Label In Frame {&Frame-Name} = "":U.
            End.
            RUN Droits-Btn (W-etatOf).
       End.

       When "article:sensitive":U Then Do:  
          If Valeur-Info = "Yes":U Then
             Run Set-Attribute-List In h_crivjv ("Mode={&INIT-ZCRI-MODE-MODIF}":U).
          Else
             Run Set-Attribute-List In h_crivjv ("Mode={&INIT-ZCRI-MODE-CONSULT}":U).
       End.

       When "BoutonDefaut":U Then Do:
           If Valeur-Info = 'oui':u And Btn-OT:sensitive IN Frame {&Frame-Name} Then
                Apply "choose":u to Btn-OT IN Frame {&Frame-Name}.
       End.

       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info Tablejf 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.

   Case Name-Info :
       /* Ins�rer ici vos propres infos � envoyer */
       When "Rowid-Of":u Then do:
            Run Get-Info("Rowid-Of":U,"Info1-Source":U).
            Valeur-Info = Return-Value.
       End.  

       When "Nom-Browse":u Then 
            Valeur-Info = {&W-nombrowse}.   

       Otherwise 
          /* Par d�faut, envoi de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Send-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records Tablejf  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* SEND-RECORDS does nothing because there are no External
     Tables specified for this SmartFonction, and there are no
     tables specified in any contained Browse, Query, or Frame. */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed Tablejf 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE NO-UNDO.
  DEFINE INPUT PARAMETER p-state AS CHARACTER NO-UNDO.
  
  CASE p-state:
      /* Placez vos cases ici pour remplacez les states standard ou
         en mettre des nouveaux */

      /* NLE 30/08/2004 */
      When 'FIN-OF-TECH':U Then Run SetTitle (Get-TradTxt (022697, 'Saisie des OF':T)).

      /* Cases standard des fonction */
      {admvif/template/fonction.i "Fctstates"}
         
      /* Cases standard des container */
      {admvif/template/cinclude.i "cstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Traiter-Contexte Tablejf 
PROCEDURE Traiter-Contexte :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

      Run Traiter-Contexte In h_ofjb(Output wstr). /* Browse des OF */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE zz-Choose-btn-JsqEtatLance Tablejf 
PROCEDURE zz-Choose-btn-JsqEtatLance :
/*  /* NLE 21/11/2002 */
 *   Case W-etatOf:
 *       When {&CETAT-OF-NON-CALCULE} Or 
 *       When {&CETAT-OF-MODIFIE} Then 
 *           Run New-State('CalProgLanOF,info1-Source':U).
 *           
 *       When {&CETAT-OF-CALCULE} Then 
 *           Run New-State('ProgLanOF,info1-Source':U).
 *           
 *       When {&CETAT-OF-PLANIFIE} Then 
 *           Run New-State('LancerOF,info1-Source':U).
 *   End Case.          */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Fo-Autorise-OFtech Tablejf 
FUNCTION Fo-Autorise-OFtech RETURNS LOGICAL
  ( I-Etat As Character,
    Buffer kof For kof ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

  If I-Etat <> {&CETAT-OF-CALCULE}  And
     I-Etat <> {&CETAT-OF-PLANIFIE} And
     I-Etat <> {&CETAT-OF-LANCE}    Then
        Return False.

  If Available kof Then Do:
    If kof.typof  = {&TYPOF-OFTECH}         Or
       kof.cofuse = {&KOF-COFUSE-PREPA}     Or
       kof.cofuse = {&KOF-COFUSE-TRANSFERT} OR
/*       kof.lot    <> ''                     OR      NLE 10/02/2005 */    
       kof.lotent <> ''                     Then Return False.

    {&LANCE-ofbib}.

    /* Verif qu'il n'y a pas d'affectations */
    If {&ofbib-Possede-Affect-Matiere}   (kof.csoc, kof.cetab, kof.prechro, kof.chrono) Then Return False.

    /* Verif qu'il n'y a pas de d�clarations */
    If I-Etat = {&CETAT-OF-LANCE} Then
        If {&ofbib-Possede-Decl-GP}      (kof.csoc, kof.cetab, kof.prechro, kof.chrono) Or
           {&ofbib-Possede-Decl-Atelier} (kof.csoc, kof.cetab, kof.prechro, kof.chrono) Then Return False.
  End.
  Else
    Return False.

  Return True.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


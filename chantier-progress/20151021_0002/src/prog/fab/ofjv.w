&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : fab\ofjv.w
 *
 * Description  : Saisie d'OF : SmartViewer sur OF
 * 
 * Template     : viewer.w - SmartViewer basique 
 * Type Objet   : SmartViewer 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 16/10/2003 � 10:30 par NB
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 30/07/2015 - VH      : M_166907:Cr�ation OF: erreur appel veridec sur mauvais rapp...
 *                              20150722_0001 (27461)|M_166907|Cr�ation OF: erreur appel veridec sur mauvais rapport quantit�/unit�|VH|30/07/15 09:33:34 
 *                               
 *  - Le 02/01/2015 - NLE     : Q005956:G�rer au mieux la combinaison activit� type/crit�r...
 *                              20150102_0001 (26406)|Q005956|G�rer au mieux la combinaison activit� type/crit�res activit� (Itin�raire)|NLE|02/01/15 14:49:28 
 *                               
 *  - Le 30/12/2014 - CD      : :Pb cr�ation OF technique
 *                              20141230_0003 (26397)||Pb cr�ation OF technique|CD|30/12/14 11:49:02 
 *                               
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Tablejv _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/viewerd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Tablejv 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

/* Variables globales */
{variable.i " "}
{addfab/bstrs01l.i}
{unitebibli.i}
{actbibli.i}
{ofbibli.i}
{ofetatbibli.i}
{sitebibli.i &OPT = OUI}
{affect2bibli.i}
{stkbibli.i}
{speciflib.i}     /* MB 11/08/05 */

{infoactbibli.i}    /* NLE 12/11/2003 : TEMPORAIRE (cd init-info-itiact) */
{infoitibibli.i}    /* NLE 12/11/2003 : TEMPORAIRE (cd init-info-itiact) */ 
{infofambibli.i}    /* Information sur les familles elementaires */ /* OM : 20/03/2006 G7_1756 */
{fabindic01li.i}

{xxpdr01li.i}  /* CD D001232 30/05/2012 pas de remise en cause OF commenc� si PDR sur l'OF */ 
{cristabibli.i} /* << ALB 20130719_0003 DP1713 */

/* constantes de pre-compilation */
&GLOBAL-DEFINE table kof
&GLOBAL-DEFINE ltable

/* Variable pour template viewer */
{admvif/template/vinclude.i "Definitions"}

/* Local Variable Definitions ---                                       */
&SCOPED-DEFINE AUCUN-CTRL    0
&SCOPED-DEFINE CTRL-ALERTE   1
&SCOPED-DEFINE CTRL-BLOQUANT 2

Define Variable W-ModifLot   As logical   No-Undo.
Define Variable W-Ancres     As Character No-Undo.

/* Table Temporaire Crit�res de Lancement */
{crivi.i}

Define Variable w-mod-of       As logical   No-undo.
Define Variable w-FCT          As Character No-undo.
Define Variable vgOrigLanct    As Character No-Undo.

/* Sauvegarde des kof.citi et cact et crit. de lancement en cas de copie 
* pour savoir si on doit relancer la copie du standard en ex�cutable
* ou simplement copier les fichiers
*/
Define Variable w-SAvCOPY-chrono  As Integer      No-Undo.
Define Variable w-SAvCOPY-citi    As Character    No-Undo.
Define Variable w-SAvCOPY-cact    As Character    No-Undo.
Define Variable w-SavCOPY-LstCrit As   Character  No-Undo.

/* Pour pouvoir afficher les messages Warning non bloquant des assign-record, dans l'end-update */
Define Variable w-msgWARNING As Character No-Undo.

/* 
 *  Valeur utilisateur de l'option 'ARTICLE' 
 *  concernant l'Aide sur les articles
 */
 
Define Variable w-valOpt-AideArt As Logical No-Undo.

Define Variable w-RelanceGenereOF As   Logical No-Undo.

Define Variable W-Local-Ass-Record-Rowid   As Character     No-Undo .
Define Variable w-LstCrit-Avant-Modif      As Character     No-Undo.
Define Variable w-datedeb-Avant-Modif      As Character     No-Undo.
Define Variable w-datefin-Avant-Modif      As Character     No-Undo.

Define Variable w-cofuse       As Character     No-Undo.
Define Variable w-prechro      As Character     No-Undo.
Define Variable w-chrono-newOF As Integer       No-Undo.

Define Variable w-crn               As Integer       No-Undo.
Define Variable w-crt               As Character     No-Undo.
Define Variable w-texte-entete      As Character     No-Undo.
Define Variable w-rowid             As Rowid         No-Undo.

/* NLE 18/12/2002 */
Define Variable w-LstCrit-OLD       As Character     No-Undo.

/* SLE 04/08/03 Suite � l'ajout de -POUTIR, pour savoir si on table sur
 * le stade LANCENT TIRE ou POUSSE, il faut conna�tre le typflux d'apr�s la 
 * fabrication choisie et non plus syst�matiquement d'apr�s zartfab.typflux
 */
Define Variable w-typflux           As Character    No-Undo.
Define Variable w-CacherFreinte     As Logical      No-Undo.
Define Variable w-old-custk2        As Character    No-Undo.

Define Variable w-SAVcarts          As Character    No-Undo.

Define Variable w-lib-oforig        As Character    No-Undo.
Define Variable w-lib-oftech        As Character    No-Undo.
Define Variable w-lib-ofclas        As Character    No-Undo.

Define Variable w-oftech            As Logical      No-Undo.
Define Variable w-oforig-prechro    As Character    No-Undo.
Define Variable w-oforig-chrono     As Integer      No-Undo.
Define Variable w-lotent-OFPousse   As Logical      No-Undo.

Define Temp-Table Tt-crivVIDE No-Undo Like Tt-criv. /* utile pour l'appel d'aide sur lot */

Define Variable w-lotent            As Character    No-Undo.
Define Variable w-cdepot-lotent     As Character    No-Undo. 
Define Variable w-cemp-lotent       As Character    No-Undo. 
Define Variable w-ancien-lotent     As Character    No-Undo.

Define Variable w-ret               As Logical      No-Undo.
Define Variable w-msg               As Character    No-Undo.

Define Variable w-typof             As Character    No-Undo.
Define Variable w-typof-force       As Logical      No-Undo.
                                
Define Variable w-SAV-typof         As Character    No-Undo.
Define Variable w-SAV-carts         As Character    No-Undo.
Define Variable w-SAV-itiact        As Character    No-Undo.

DEFINE VARIABLE vgChronoOFActuel    AS INTEGER      NO-UNDO.

/* APO D003489 : Process op�ratoires */
DEFINE VARIABLE vgTypgestres      AS CHARACTER     NO-UNDO.
DEFINE VARIABLE vgtRechTypgestres AS LOGICAL       NO-UNDO.

/* Si cr�ation OF technique : lot entrant de l'OF origine */
/*DEFINE VARIABLE vgOfOrigLotEnt AS CHARACTER NO-UNDO.  /* MB D003368 28/04/08 */*/

/* Sauvegarde de la qt� de l'OF avant mise � jour */
DEFINE VARIABLE vgModifQte1    AS DECIMAL   NO-UNDO. /* MB D003368 31/07/08 */

/* Sauvegarde � la copie de l'info pour d�clencher un contr�le si modification */
DEFINE VARIABLE vgKofCresOrigine    AS CHARACTER   NO-UNDO. /* MB D003368 31/07/08 */

DEFINE VARIABLE vgLabelCresCCS AS CHARACTER NO-UNDO.
DEFINE VARIABLE vgLabelCresPOP AS CHARACTER NO-UNDO.
DEFINE VARIABLE vgEtape        AS INTEGER   NO-UNDO. /* ALB 20131024_0003 DP1253, =1 si action de Programmer l'of, =2 si action de lancer l'of */
DEFINE VARIABLE vgCactRefIti   AS CHARACTER NO-UNDO. /* ALB 20140911_0006 Q7437 Activit� de re�f�rence d'un itin�raire */

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" Tablejv _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kof
&Scoped-define FIRST-EXTERNAL-TABLE kof


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kof.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS kof.datdeb kof.datfin kof.lot kof.lotent ~
kof.cequip kof.cres 
&Scoped-define ENABLED-TABLES kof
&Scoped-define FIRST-ENABLED-TABLE kof
&Scoped-Define ENABLED-OBJECTS RECT-51 RECT-52 Tb-Datdeb F-heurdeb ~
Tb-Datfin F-Heurfin B_AID-3 B_AID-lotent F-Qtestk2 F-Custk2 B_AID-2 F-txfr ~
B_AID-5 B_AID-7 B_AID-Cpope F-% 
&Scoped-Define DISPLAYED-FIELDS kof.datdeb kof.datfin kof.prechro ~
kof.chrono kof.carts kof.lot kof.lotent kof.cequip kof.cres kof.datprod 
&Scoped-define DISPLAYED-TABLES kof
&Scoped-define FIRST-DISPLAYED-TABLE kof
&Scoped-Define DISPLAYED-OBJECTS Tb-Datdeb F-heurdeb Tb-Datfin F-Heurfin ~
FL-cetat F-Qtestk2 F-Custk2 F-txfr Rs-iti Fl-Itiact F-CresCCS F-CresPop F-% 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,L-Oblig,L-Field,L-Majuscule,List-6 */
&Scoped-define ADM-CREATE-FIELDS kof.carts Rs-iti Fl-Itiact 
&Scoped-define L-Oblig Tb-Datdeb kof.carts F-Qtestk2 
&Scoped-define L-Field F-heurdeb F-Heurfin kof.lot kof.lotent F-Qtestk2 ~
F-Custk2 F-txfr kof.cequip 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Auto-Definitions" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/wrdef.p */

/* ********** Preprocessor Definitions VIF ***********/
&Scoped-Define Trait-External-Tables ~{&TP1} kof ~{&TP2} 
&Scoped-Define List-Champs-Auto Tb-Datdeb,kof.datdeb,F-heurdeb,Tb-Datfin,kof.datfin,F-Heurfin,kof.prechro,kof.chrono,FL-cetat,kof.carts,kof.lot,kof.lotent,F-Qtestk2,F-Custk2,F-txfr,Rs-iti,Fl-Itiact,kof.cequip,F-CresCCS,F-CresPop,kof.cres,kof.datprod,F-%
&If Defined(List-Champs-Auto) <> 0 &Then
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&List-Champs-Auto},{&User-List-Champs}
   &Else
      &Scoped-Define List-champs {&List-Champs-Auto}
   &Endif
&Else
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&User-List-Champs}
   &Endif
&Endif

&Scoped-Define List-Combos Rs-iti
&Scoped-Define Clear-Combos 
&Scoped-Define Initialize-Combos ~
  Run Init-Cb-Rs In Adm-broker-hdl (Rs-iti:handle in Frame F-Main).
&Scoped-Define FIELD-PAIRS~
 ~{&FP1}datdeb ~{&FP2}datdeb ~{&FP3}~
 ~{&FP1}datfin ~{&FP2}datfin ~{&FP3}~
 ~{&FP1}lot ~{&FP2}lot ~{&FP3}~
 ~{&FP1}lotent ~{&FP2}lotent ~{&FP3}~
 ~{&FP1}cequip ~{&FP2}cequip ~{&FP3}~
 ~{&FP1}cres ~{&FP2}cres ~{&FP3}
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD F-StadeLanctev Tablejv 
FUNCTION F-StadeLanctev RETURNS CHARACTER
  (INPUT iTypflux AS CHARACTER)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD F-StadeProgev Tablejv 
FUNCTION F-StadeProgev RETURNS CHARACTER
  (INPUT iTypflux AS CHARACTER)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Fo-CleComp Tablejv 
FUNCTION Fo-CleComp RETURNS CHARACTER
  ( INPUT iCStade AS CHARACTER)  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Fo-Qte-Lotent-Prioritaire Tablejv 
FUNCTION Fo-Qte-Lotent-Prioritaire RETURNS LOGICAL
  ( Input i-cart As Character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Fo-QteReste Tablejv 
FUNCTION Fo-QteReste RETURNS DECIMAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Fo-Stade Tablejv 
FUNCTION Fo-Stade RETURNS CHARACTER
  (Input iTypflux As Character, Input iTypof As Character, Input iCetatOf As Character )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD GererParEquipe Tablejv 
FUNCTION GererParEquipe RETURNS LOGICAL PRIVATE
  ( Input iCsoc      As CHARACTER,
    Input iCetab     As CHARACTER,
    Input iCactref   As CHARACTER,  
    Input iTypOf     As CHARACTER,
    INPUT iPrechro   As CHARACTER,
    INPUT iChrono    As INTEGER,
    INPUT iNi1artref AS INTEGER
    )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Regeneration-Num-Lot Tablejv 
FUNCTION Regeneration-Num-Lot RETURNS LOGICAL
  ( /* parameter-definitions */ )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON B_AID-2 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-3 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-5 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-7 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-Cpope 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE BUTTON B_AID-lotent 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE VARIABLE F-% AS CHARACTER FORMAT "X(256)":U INITIAL "%" 
      VIEW-AS TEXT 
     SIZE 3.6 BY 1 NO-UNDO.

DEFINE VARIABLE F-CresCCS AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 26.8 BY 1 NO-UNDO.

DEFINE VARIABLE F-CresPop AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 18.8 BY 1 NO-UNDO.

DEFINE VARIABLE F-cunite AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 7.2 BY 1 NO-UNDO.

DEFINE VARIABLE f-cunite1 AS CHARACTER FORMAT "X(3)":U 
     VIEW-AS FILL-IN 
     SIZE 7.2 BY 1 NO-UNDO.

DEFINE VARIABLE F-CuReste AS CHARACTER FORMAT "X(3)":U 
     VIEW-AS FILL-IN 
     SIZE 7.2 BY 1 NO-UNDO.

DEFINE VARIABLE F-Custk2 AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 7.2 BY 1 NO-UNDO.

DEFINE VARIABLE F-heurdeb AS CHARACTER FORMAT "99:99":U INITIAL "0000" 
     LABEL "�" 
     VIEW-AS FILL-IN 
     SIZE 8.4 BY 1 NO-UNDO.

DEFINE VARIABLE F-Heurfin AS CHARACTER FORMAT "99:99":U INITIAL "0000" 
     LABEL "�" 
     VIEW-AS FILL-IN 
     SIZE 8.4 BY 1 NO-UNDO.

DEFINE VARIABLE F-lres AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 52.8 BY 1 NO-UNDO.

DEFINE VARIABLE f-qte1 AS DECIMAL FORMAT "-z,zzz,zz9.9999" INITIAL 0 
     LABEL "Qt� effective" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1
     FONT 8 NO-UNDO.

DEFINE VARIABLE F-QteReste AS DECIMAL FORMAT "-z,zzz,zz9.9999" INITIAL 0 
     LABEL "Qt� de stock r�siduelle" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1
     FONT 8 NO-UNDO.

DEFINE VARIABLE F-Qtestk AS DECIMAL FORMAT "-z,zzz,zz9.9999":U INITIAL 0 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1
     FONT 8 NO-UNDO.

DEFINE VARIABLE F-Qtestk2 AS DECIMAL FORMAT "-z,zzz,zz9.9999":U INITIAL 0 
     LABEL "Quantit�" 
     VIEW-AS FILL-IN 
     SIZE 23 BY 1
     FONT 8 NO-UNDO.

DEFINE VARIABLE F-txfr AS DECIMAL FORMAT "z9.99":U INITIAL 0 
     LABEL "Freinte" 
     VIEW-AS FILL-IN 
     SIZE 8.4 BY 1
     FONT 8 NO-UNDO.

DEFINE VARIABLE FL-cetat AS CHARACTER FORMAT "X(3)" 
     VIEW-AS FILL-IN 
     SIZE 6.6 BY 1 NO-UNDO.

DEFINE VARIABLE Fl-Itiact AS CHARACTER FORMAT "X(15)":U 
     VIEW-AS FILL-IN 
     SIZE 28.2 BY 1 NO-UNDO.

DEFINE VARIABLE Fl-Litiact AS CHARACTER FORMAT "X(256)":U 
     VIEW-AS FILL-IN 
     SIZE 52.8 BY 1 NO-UNDO.

DEFINE VARIABLE Rs-iti AS INTEGER INITIAL 1 
     VIEW-AS RADIO-SET HORIZONTAL
     RADIO-BUTTONS 
          "Itin�raire":T, 1,
"Activit�:":T, 2
     SIZE 24 BY 1.24 NO-UNDO.

DEFINE RECTANGLE RECT-51
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 143.4 BY 5.86.

DEFINE RECTANGLE RECT-52
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 143.4 BY 3.14.

DEFINE VARIABLE Tb-Datdeb AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.4 BY 1.05 NO-UNDO.

DEFINE VARIABLE Tb-Datfin AS LOGICAL INITIAL no 
     LABEL "" 
     VIEW-AS TOGGLE-BOX
     SIZE 2.4 BY .95 NO-UNDO.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     Tb-Datdeb AT ROW 1.52 COL 8
     kof.datdeb AT ROW 1.52 COL 19 COLON-ALIGNED
          LABEL "D�but le"
          VIEW-AS FILL-IN 
          SIZE 13.2 BY 1
     F-heurdeb AT ROW 1.52 COL 36 COLON-ALIGNED
     Tb-Datfin AT ROW 1.52 COL 57
     kof.datfin AT ROW 1.52 COL 65.2 COLON-ALIGNED
          LABEL "Fin le"
          VIEW-AS FILL-IN 
          SIZE 13.2 BY 1
     F-Heurfin AT ROW 1.52 COL 82 COLON-ALIGNED
     kof.prechro AT ROW 1.52 COL 113 COLON-ALIGNED
          LABEL "N� OF"
          VIEW-AS FILL-IN 
          SIZE 8.4 BY 1
     kof.chrono AT ROW 1.52 COL 122 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 11.8 BY 1 TOOLTIP "Ordre de Fabrication"
     FL-cetat AT ROW 1.52 COL 134 COLON-ALIGNED NO-LABEL
     kof.carts AT ROW 2.86 COL 11 COLON-ALIGNED
          LABEL "Article"
          VIEW-AS FILL-IN 
          SIZE 26 BY 1
     B_AID-3 AT ROW 2.86 COL 39
     zart.lart AT ROW 2.86 COL 41 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 50.8 BY 1
     kof.lot AT ROW 4.14 COL 11 COLON-ALIGNED
          LABEL "Lot sortant"
          VIEW-AS FILL-IN 
          SIZE 28 BY 1
     kof.lotent AT ROW 4.14 COL 54 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 28 BY 1
     B_AID-lotent AT ROW 4.14 COL 84.2
     F-QteReste AT ROW 4.14 COL 111 COLON-ALIGNED
     F-CuReste AT ROW 4.14 COL 134 COLON-ALIGNED NO-LABEL
     F-Qtestk2 AT ROW 5.48 COL 11 COLON-ALIGNED
     F-Custk2 AT ROW 5.48 COL 34 COLON-ALIGNED NO-LABEL
     B_AID-2 AT ROW 5.48 COL 43.4
     F-Qtestk AT ROW 5.48 COL 45 COLON-ALIGNED NO-LABEL
     F-cunite AT ROW 5.48 COL 68 COLON-ALIGNED NO-LABEL
     F-txfr AT ROW 5.48 COL 85 COLON-ALIGNED
     f-qte1 AT ROW 5.48 COL 111 COLON-ALIGNED
     f-cunite1 AT ROW 5.48 COL 134 COLON-ALIGNED NO-LABEL
     Rs-iti AT ROW 7.29 COL 7.2 NO-LABEL
     Fl-Itiact AT ROW 7.29 COL 30.2 COLON-ALIGNED NO-LABEL
     B_AID-5 AT ROW 7.29 COL 60.4
     Fl-Litiact AT ROW 7.29 COL 61.8 COLON-ALIGNED NO-LABEL
     kof.cequip AT ROW 7.29 COL 128.4 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 9.6 BY 1
     B_AID-7 AT ROW 7.29 COL 139.8
     F-CresCCS AT ROW 8.33 COL 5.4 NO-LABEL WIDGET-ID 12
     F-CresPop AT ROW 8.33 COL 11.4 COLON-ALIGNED NO-LABEL WIDGET-ID 10
     kof.cres AT ROW 8.33 COL 30.2 COLON-ALIGNED NO-LABEL WIDGET-ID 8
          VIEW-AS FILL-IN 
          SIZE 19.4 BY 1
     B_AID-Cpope AT ROW 8.33 COL 51.6
     F-lres AT ROW 8.33 COL 53 COLON-ALIGNED NO-LABEL WIDGET-ID 6
     kof.datprod AT ROW 8.33 COL 128.4 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 13 BY 1
     F-% AT ROW 5.48 COL 94 COLON-ALIGNED NO-LABEL
     RECT-51 AT ROW 1 COL 1
     RECT-52 AT ROW 6.76 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: vif5_7.kof
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Tablejv ASSIGN
         HEIGHT             = 8.91
         WIDTH              = 143.4.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Tablejv 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_viewe.i}
{src/adm/method/viewer.i}
{admvif/method/ap_viewe.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW Tablejv
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       B_AID-2:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-3:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-5:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-7:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-Cpope:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

ASSIGN 
       B_AID-lotent:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

/* SETTINGS FOR FILL-IN kof.carts IN FRAME F-Main
   NO-ENABLE 1 3 EXP-LABEL                                              */
/* SETTINGS FOR FILL-IN kof.cequip IN FRAME F-Main
   4                                                                    */
/* SETTINGS FOR FILL-IN kof.chrono IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN kof.datdeb IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN kof.datfin IN FRAME F-Main
   EXP-LABEL                                                            */
/* SETTINGS FOR FILL-IN kof.datprod IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       F-%:PRIVATE-DATA IN FRAME F-Main     = 
                "Init-Text=%".

/* SETTINGS FOR FILL-IN F-CresCCS IN FRAME F-Main
   NO-ENABLE ALIGN-L                                                    */
ASSIGN 
       F-CresCCS:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN F-CresPop IN FRAME F-Main
   NO-ENABLE                                                            */
ASSIGN 
       F-CresPop:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN F-cunite IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       F-cunite:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN f-cunite1 IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       f-cunite1:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN F-CuReste IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       F-CuReste:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN F-Custk2 IN FRAME F-Main
   4                                                                    */
/* SETTINGS FOR FILL-IN F-heurdeb IN FRAME F-Main
   4                                                                    */
/* SETTINGS FOR FILL-IN F-Heurfin IN FRAME F-Main
   4                                                                    */
/* SETTINGS FOR FILL-IN F-lres IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN f-qte1 IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       f-qte1:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN F-QteReste IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       F-QteReste:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN F-Qtestk IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
ASSIGN 
       F-Qtestk:HIDDEN IN FRAME F-Main           = TRUE.

/* SETTINGS FOR FILL-IN F-Qtestk2 IN FRAME F-Main
   3 4                                                                  */
/* SETTINGS FOR FILL-IN F-txfr IN FRAME F-Main
   4                                                                    */
/* SETTINGS FOR FILL-IN FL-cetat IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN Fl-Itiact IN FRAME F-Main
   NO-ENABLE 1                                                          */
/* SETTINGS FOR FILL-IN Fl-Litiact IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN zart.lart IN FRAME F-Main
   NO-DISPLAY NO-ENABLE EXP-LABEL                                       */
/* SETTINGS FOR FILL-IN kof.lot IN FRAME F-Main
   4 EXP-LABEL                                                          */
/* SETTINGS FOR FILL-IN kof.lotent IN FRAME F-Main
   4                                                                    */
/* SETTINGS FOR FILL-IN kof.prechro IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR RADIO-SET Rs-iti IN FRAME F-Main
   NO-ENABLE 1                                                          */
/* SETTINGS FOR TOGGLE-BOX Tb-Datdeb IN FRAME F-Main
   3                                                                    */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME B_AID-2
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-2 Tablejv
ON CHOOSE OF B_AID-2 IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-2 Tablejv
ON ENTRY OF B_AID-2 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-3
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-3 Tablejv
ON CHOOSE OF B_AID-3 IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-3 Tablejv
ON ENTRY OF B_AID-3 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-5
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-5 Tablejv
ON CHOOSE OF B_AID-5 IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-5 Tablejv
ON ENTRY OF B_AID-5 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-7
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-7 Tablejv
ON CHOOSE OF B_AID-7 IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-7 Tablejv
ON ENTRY OF B_AID-7 IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-Cpope
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-Cpope Tablejv
ON CHOOSE OF B_AID-Cpope IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-Cpope Tablejv
ON ENTRY OF B_AID-Cpope IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME B_AID-lotent
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-lotent Tablejv
ON CHOOSE OF B_AID-lotent IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID-lotent Tablejv
ON ENTRY OF B_AID-lotent IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}

END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME F-heurdeb
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-heurdeb Tablejv
ON LEAVE OF F-heurdeb IN FRAME F-Main /* � */
DO :
  /* Remplace les blancs par des 0 pou �viter un message d'erreur */
  {admvif/template/tinclude.i "leave-champ-heure"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME F-Heurfin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL F-Heurfin Tablejv
ON LEAVE OF F-Heurfin IN FRAME F-Main /* � */
DO :
  /* Remplace les blancs par des 0 pou �viter un message d'erreur */
  {admvif/template/tinclude.i "leave-champ-heure"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Rs-iti
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Rs-iti Tablejv
ON VALUE-CHANGED OF Rs-iti IN FRAME F-Main
DO:
  
  Apply "Entry":u to Fl-ItiAct in Frame {&Frame-Name}.
  
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Tb-Datdeb
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tb-Datdeb Tablejv
ON VALUE-CHANGED OF Tb-Datdeb IN FRAME F-Main
DO:
  If Tb-datdeb:Checked In Frame {&Frame-name} Then  Do:
    Assign Tb-datfin:Screen-Value In Frame {&Frame-name} = "NO":u.
    run Enable-datdeb(Yes).
  End.  
  Else Do:   
    Assign Tb-datfin:Screen-Value In Frame {&Frame-name} = "Yes":u.
    run Enable-datdeb(No).    
  End.  
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME Tb-Datfin
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL Tb-Datfin Tablejv
ON VALUE-CHANGED OF Tb-Datfin IN FRAME F-Main
DO:
  If Tb-datfin:Checked In Frame {&Frame-name} Then Do:
    Assign Tb-datdeb:Screen-Value In Frame {&Frame-name} = "NO":u.
    run Enable-datdeb(No).
  End.  
  Else Do:   
    Assign Tb-datdeb:Screen-Value In Frame {&Frame-name} = "Yes":u.
    run Enable-datdeb(Yes).
  End.
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Tablejv 


/* ***************************  Main Block  *************************** */
/*** Triggers Generaux Du viewer */
{admvif/template/vinclude.i "triggers"}

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         

{admvif/template/vinclude.i "main-block"}


  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available Tablejv  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kof"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kof"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Aide-Sur-Champ Tablejv 
PROCEDURE Aide-Sur-Champ :
/*------------------------------------------------------------------------------
  Purpose:     Aide (liste de choix) Sur les Champs
  Parameters:  Handle du widget active
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Champ  As Widget-Handle No-Undo.

    Define Variable ChoixEffectue As Character     No-Undo.
    Define Variable w-lstAttr As   Character       No-Undo. /* Attributs du browse 
                                                             d'aide des articles */
    Define Variable vCartent   As Character         No-Undo.
    Define Variable vCartsor   As Character         No-Undo.
    Define Variable w-ret       As Logical           No-Undo.
    Define Variable w-msg       As Character         No-Undo.
    Define Variable w-date      As Date              No-Undo.
    Define Variable w-heure     As Integer           No-Undo.
    DEFINE VARIABLE vLstact     AS CHARACTER         NO-UNDO.
    DEFINE VARIABLE vDate       AS CHARACTER NO-UNDO.
    Define Variable vCsecteur   As Character No-Undo.
    DEFINE VARIABLE vCarts      AS CHARACTER NO-UNDO.
    Define Variable L-Criteres  As Character No-Undo.
    
   Case Champ:Name :
      When "carts":U Then Do With Frame {&Frame-Name}:
         Assign vDate = (If Tb-Datdeb:Checked
                           Then Kof.Datdeb:Screen-Value
                           Else Kof.Datfin:Screen-Value).
                            
         /* 
          *  Si la valeur utilisateur de l'option 'ARTICLE' 
          *  concernant l'Aide sur les articles est � oui, on affiche 
          *  tous les articles. Sinon, on affiche seulement les articles 
          *  coh�rents pour l'aide
          */                    
         If NOT w-valOpt-AideArt THEN 
            ASSIGN w-lstAttr = Substitute('typart=&1,tfab=&2,dvalid=&3':U, 
                                          {&TYPART-MARCHANDISE},
                                          'oui':U, 
                                           vDate). 
                            
         {&Run-prog} "admvif/objects/a-standa.w":U 
                ("Artj":U,             /* code de la fonction */
                 "artjb":U, w-lstAttr,   
                                       /* Nom browser et attributs */
                 "Artjv":U, "":U,    /* Nom viewer et attributs */
                 Champ:Label,             /* Titre ecran d'aide */
                 No,                     /* Acces bouton nouveau */
                 Champ:Screen-Value,      /* Valeur de depart */
                 Output ChoixEffectue).
         If ChoixEffectue <> ? Then 
            ASSIGN Champ:Screen-Value = ChoixEffectue.
      End.
      
      When "cequip":U Then Do :
         {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("equipjf":U,             /* Code fonction pour les droits */
                       "equipjb":U, "":U,   /* Nom browser et attributs */
                       "equipjv":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no ,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
         If ChoixEffectue <> ? Then 
            Champ:Screen-Value = ChoixEffectue.
      End.
      
      When "Fl-ItiAct":U Then Do With Frame {&Frame-Name}:
         /* El�ments � prendre en compte dans la recherche */
         /* Article */
         Assign vCarts = kof.carts:screen-value.
         
         Run Pr-Rech-cartent-cartsor (Input  vCarts,
                                      Output vCartent,
                                      Output vCartsor,
                                      Output w-ret,
                                      Output w-msg).
         If w-ret And (vCartent <> '' Or vCartsor <> '') Then Do:
            {&Run-SITE2-Recherche-Character} (Input wcsoc,
                                              Input wcetab,
                                              Input vCarts,
                                              Input "",
                                              Input "ZARTFAB.TYPFLUXOF":U,
                                              Output w-typflux,
                                              Output w-ret,
                                              Output w-msg).
         End.
         Else Do:
            {affichemsg.i
               &Message="Substitute(Get-TradMsg (004715, 'Aucun article de fabrication n''est associ� � l''article &1.|(Fiche article de production - Planification).':T),vCarts)"
            }
            Return "Adm-Error":u.   
         End.

         /* Date en vigueur */   
         Assign vDate = (If Tb-Datdeb:Checked 
                           Then Kof.Datdeb:Screen-Value
                           Else Kof.Datfin:Screen-Value).
         /* Crit�res de lancement */                  
         Run Get-Info ("Liste-Criteres":U, "Criteres-Target":U).
         Assign L-Criteres = if Return-Value = ? Then "" Else Return-Value.
                                
         If Rs-Iti:Screen-Value = "1" Then Do:   /* ITINERAIRE */
            {&Run-prog} "admvif/objects/a-standa.w":U 
                        ("itijf":U,             /* Code fonction pour les droits */
                         "iti1jb":U, "cart=":U + vCarts + ",typflux=":u + w-typflux + ",date=":U + vDate + ", criteres=":U + L-criteres,   /* Nom browser et attributs */
                         "":U, "":U,    /* Nom viewer et attributs */
                         Champ:Label,             /* Titre ecran d'aide */
                         no,                     /* Acces bouton nouveau */
                         Champ:Screen-Value,      /* Valeur de depart */
                         Output ChoixEffectue).
         End.
         Else Do:   /* ACTIVITE */
            {&Run-prog} "admvif/objects/a-standa.w":U 
                        ("actjf":U,             /* Code fonction pour les droits */
                         "act1jb":U, "cart=":U + vCarts + ",typflux=":u + w-typflux + ",date=":U + vDate + ", criteres=":U + L-Criteres,   /* Nom browser et attributs */
                         "":U, "":U,    /* Nom viewer et attributs */
                         Champ:Label,             /* Titre ecran d'aide */
                         no,                     /* Acces bouton nouveau */
                         Champ:Screen-Value,      /* Valeur de depart */
                         Output ChoixEffectue).

         End.             
         If ChoixEffectue <> ? Then Do:
            Champ:Screen-Value = ChoixEffectue.
            Run Set-Info('itiact=':U + STRING (Rs-iti:screen-value = '1':U, 'OUI/NON':U) + '|':U + 
                                        FL-itiact:Screen-Value   + '|':U + 
                                        (If Tb-Datdeb:checked 
                                            Then String(kof.datdeb:Screen-Value)
                                            Else String(kof.datfin:Screen-Value)), 
                          'Group-assign-Target':U).
         End.
      END.
      
      When "cres":U Then Do with frame {&frame-name} :
         /* Initialisation de W-Csecteur 
          * On ne peut pas utiliser directement Init-Info-ItiAct
          * car elle modifie certaines screen-value comme par ex : cres*/
         ASSIGN i-actbib-csoc  = Wcsoc
                i-actbib-cetab = Wcetab
                i-actbib-cart  = kof.carts:screen-Value  
                i-actbib-ofcre = -1 /* on se base sur l'option "OFCRE":u */
                i-actbib-datef = Date (If Tb-Datdeb:checked 
                                         Then kof.datdeb:screen-Value
                                         Else kof.datfin:screen-Value).
            
         CASE Rs-iti:screen-value :
            When "1":u then Assign i-actbib-citi  = Fl-itiact:screen-value
                                   i-actbib-cact  = "":U.
            When "2":u then Assign i-actbib-citi  = "":U
                                   i-actbib-cact  = Fl-itiact:screen-value.
         END case.
         {&Run-actbib-Donnees-ItiAct}

         ASSIGN vCsecteur = (If not o-actbib-Retour 
                              Then '':u
                              Else o-actbib-csecteur).
         
         IF NOT vgtRechTypgestres THEN
            RUN P-Gestion-ressources.

         IF vgTypgestres = {&KOF-CSIT-GESTION-PROCESS} THEN DO:
            CASE Rs-iti:SCREEN-VALUE :
               WHEN '1':U THEN DO:
                  {&RUN-ACTBIB-PROCESSListeActivitesDateRech}
                                  (INPUT  wcsoc, wcetab, Fl-Itiact:SCREEN-VALUE,i-actbib-datef,
                                   OUTPUT vLstact).
               END.
               WHEN '2':U THEN
                  ASSIGN vLstact = Fl-Itiact:SCREEN-VALUE.
            END CASE.
            /* SLe 03/05/2013 M133569 La date de recherche est celle de l'O.F. et non celle de l'iti ou act */
            {&Run-prog} "admvif/objects/a-standa.w":U 
                         ("popej":U,                     /* Code fonction pour les droits */
                          "popeljb":U, SUBSTITUTE("cact=&1,DateRech=&2":U,
                                       vLstact,i-actbib-datef),   /* Nom browser et attributs */
                          "":U, "":U,                    /* Nom viewer et attributs */
                          vgLabelCresPOP,                   /* Titre ecran d'aide */
                          no,                            /* Acces bouton nouveau */
                          Champ:Screen-Value,            /* Valeur de depart */
                          Output ChoixEffectue).          
         END.
         ELSE DO:
             {&Run-prog} "admvif/objects/a-standa.w":U 
                         ("resj":U,                                   /* Code fonction pour les droits */
                          "reslb":U, "cnatres=,cresg=TOu,cresm=TOU,typres=":U
                                     + {&TYPRESS-CENTRE-SUPERIEUR}
                                     + ",csecteur=":u + vCsecteur,    /* Nom browser et attributs */
                          "":U, "":U,                                 /* Nom viewer et attributs */
                          vgLabelCresCCS,                                /* Titre ecran d'aide */
                          no,                                         /* Acces bouton nouveau */
                          Champ:Screen-Value,                         /* Valeur de depart */
                          Output ChoixEffectue).          
          END.
          If ChoixEffectue <> ? Then 
             Champ:Screen-Value = ChoixEffectue.
      End.
      
      When "f-custk2":U   Then Do :
          {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("artj":U,             /* Code fonction pour les droits */
                       "artulb":U, "att-wcart=":U + kof.carts:screen-value in frame {&Frame-name},   /* Nom browser et attributs */
                       "":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
          If ChoixEffectue <> ? Then 
             Assign w-old-custk2       = Champ:Screen-Value
                    Champ:Screen-Value = ChoixEffectue.
      End.
      
      When "lotent":U Then Do With Frame {&Frame-Name}:
         If Date(kof.datdeb:Screen-Value) = ? Then Do:
            {admvif/method/saiheure.i Assign w-heure F-heurfin yes}
         End.
         Else Do:
            {admvif/method/saiheure.i Assign w-heure F-heurdeb yes}
         End.
         w-date = If Date(kof.datdeb:Screen-Value) = ? Then Date(kof.datfin:Screen-Value) Else Date(kof.datdeb:Screen-Value).
           
         /* Les crit�res de lancements participent au choix du lot entrant, de mani�re stricte */
         RUN Criteres-de-Lancement.

         {cumvklai.i &VALEUR-IN = "Champ:Screen-Value + '|':u + '' + '|':u + ''"
                     &CART      = kof.carts:Screen-Value
                     &DATEF     = w-date
                     &HEUREF    = w-heure
                     &MODESTK   = "{&STOCK-PREVISIONNEL}"
                     &TT-CRIV   = tt-criv }

         If ChoixEffectue <> ? THEN
            Assign Champ:Screen-Value     = Entry(1,ChoixEffectue, "|":u)
                   w-lotent               = Champ:SCREEN-VALUE No-Error.
      End.          
      
      Otherwise ChoixEffectue = ?.
   End Case.

   If ChoixEffectue <> ? Then 
      Apply "Tab":U To Champ.
   Else
      Apply "Entry":U To Champ.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Controle-activite Tablejv 
PROCEDURE Controle-activite :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  Define Input  Parameter I-cart   As Character No-Undo.
  Define Output Parameter O-Cunite As Character No-Undo.
  Define Output Parameter O-ret    As Logical   No-Undo.
  Define Output Parameter O-Msg    As Character No-Undo.
  
  DEFINE VARIABLE vDateVigueur AS DATE      NO-UNDO. /* date de vigueur pour recherche datef de l'activite */
  
  For First Kact Fields (cact cart cunite)
                 Where Kact.Csoc  = Wcsoc
                   And Kact.Cetab = Wcetab
                   And Kact.Cact  = Fl-Itiact:screen-value in frame {&frame-name}
                   And Kact.Cetat = {&CETAT-ACTIF} 
                 No-Lock :

     /* Contr�le de l'article */
     If Kact.Cart <> I-Cart Then Do:
        Assign O-Ret = False
               O-Msg = '(15) ':U + Substitute(Get-TradMsg (004727, 'L''article de fabrication &1 et l''article de l''activit� &2 sont diff�rents.':T),I-cart,kact.cart).
        Return "".  
     End. 
        
     /* SLe 03/05/06
      * on v�rifie que les crit�res de l'activit� correspondent aux crit�res de lancement */
     {&Run-ACTBIB-Verif-Crit-Act} (Input  wcsoc,
                                   Input  wcetab,
                                   Input  kact.cact,
                                   Input  Table Tt-criv, /* crit�res */
                                   Output o-ACTBIB-Retour,
                                   Output o-ACTBIB-Mess).
     If Not o-ACTBIB-Retour Then Do:
        Assign O-Ret = False
               O-Msg = '(16) ':U + Substitute(Get-TradMsg (001378, 'L''activit� &1 ne correspond pas aux crit�res saisis.':T), kact.cact).
        Return "".  
     End.
              
     /* Contr�le que l'activit� a une version en vigueur */
     /* OM : 16/11/2006 Ctrl source: on ressort er IF du can-find car dans la config ORACLE-Progress V10 le if can-find se bannane */
     ASSIGN vDateVigueur = (If Tb-Datdeb:Checked In Frame {&Frame-Name} 
                               Then Date(Kof.Datdeb:Screen-Value In Frame {&Frame-Name})
                               Else date(Kof.Datfin:Screen-Value In Frame {&Frame-Name})).     
     If Not Can-Find(Last Kactd Where Kactd.Csoc   = Wcsoc
                                  And Kactd.Cetab  = Wcetab
                                  And Kactd.Cact   = Kact.Cact
                                  And Kactd.Datef <= vDateVigueur
                                No-Lock) 
     Then Do:
        Assign O-Ret = False
               O-Msg = '(18) ':U + Get-TradMsg (004728, 'Aucune date n''est en vigueur pour l''activit�.':T).
        Return "".
     End.   
     Assign O-Cunite = Kact.Cunite
            O-Ret    = True.
  End.
  If Not Available Kact Then Do:
     Assign O-Ret = False
            O-Msg = '(14) ':U + Substitute(Get-TradMsg (004724, 'Je ne connais pas l''activit� &1.':T),Fl-Itiact:screen-value in frame {&frame-name}).
     Return "".
  End. 
                                        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Controle-itineraire Tablejv 
PROCEDURE Controle-itineraire :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  Define Input  Parameter I-cart   As Character    No-Undo.
  Define Output Parameter O-Cunite As Character    No-Undo.
  Define Output Parameter O-ret    As   Logical    No-Undo.
  Define Output Parameter O-Msg    As   Character  No-Undo.
 
  DEFINE VARIABLE vDateVigueur AS DATE      NO-UNDO. /* date de vigueur pour recherche datef de l'activite */
  DEFINE VARIABLE vTrouve      AS LOGICAL   NO-UNDO. /* A t on trouv� l'enreg ? */
  DEFINE VARIABLE vKitiCart    AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vKitidDatef  AS DATE      NO-UNDO.
  
  DEFINE BUFFER bKiti   FOR kiti.
  DEFINE BUFFER bKitid  FOR kitid.
  DEFINE BUFFER bKitidd FOR kitidd.
  DEFINE BUFFER bKact   FOR kact.
  DEFINE BUFFER bKactd  FOR kactd.
  
  
  FOR FIRST bKiti FIELDS(cart)
            Where bKiti.Csoc  = Wcsoc
              And bKiti.Cetab = Wcetab
              And bKiti.Citi  = Fl-Itiact:screen-value in frame {&frame-name} 
              And bKiti.Cetat = {&CETAT-ACTIF}
            No-Lock:
     ASSIGN vTrouve   = TRUE
            vKitiCart = bKiti.cart.
  END.  
  If Not vTrouve Then Do:
     Assign O-Ret = False
            O-Msg = '(8) ':U + Substitute(Get-TradMsg (004744, 'Je ne connais pas l''itin�raire &1.':T),Fl-Itiact:screen-value in frame {&frame-name}).
     RETURN.      
  End. 
        
  /* Contr�le de l'article */
  If vKitiCart <> I-Cart Then Do:
     Assign O-Ret = False
            O-Msg = '(9) ':U + Substitute(Get-TradMsg (004745, 'L''article de fabrication &1 et l''article de l''itin�raire &2 sont diff�rents.':T),
                                  I-cart,vKitiCart).
     Return "".
  End. 
        
  /* Contr�le que l'itin�raire a une version en vigueur */
  /* OM : 16/11/2006 Ctrl source: on ressort er IF du can-find car dans la config ORACLE-Progress V10 le if can-find se bannane */
  ASSIGN vTrouve      = FALSE
         vDateVigueur = (If Tb-Datdeb:Checked In Frame {&Frame-Name} 
                                          Then Date(Kof.Datdeb:Screen-Value In Frame {&Frame-Name})
                                          Else Date(Kof.Datfin:Screen-Value In Frame {&Frame-Name})).
  FOR LAST bKitid FIELDS(datef)
                  Where bKitid.Csoc   = Wcsoc
                    And bKitid.Cetab  = Wcetab
                    And bKitid.Citi   = Fl-Itiact:screen-value in frame {&frame-name} 
                    And bKitid.Datef <= vDateVigueur
                  No-Lock:  
     ASSIGN vTrouve     = TRUE
            vKitidDatef = bKitid.datef.
  END.
  If Not vTrouve Then Do:
     Assign O-Ret = False
            O-Msg = '(12) ':U + Get-TradMsg (004750, 'Aucune date n''est en vigueur pour l''itin�raire.':T).
     RETURN.
  End.   
                                          
  /* Contr�le que l'itin�raire a au moins une activit� en vigueur */
  assign i = 0.
  For Each bKitidd FIELDS(csoc cetab cact tactype)
                   Where bKitidd.Csoc  = Wcsoc
                     And bKitidd.Cetab = Wcetab
                     And bKitidd.Citi  = Fl-Itiact:screen-value in frame {&frame-name} 
                     And bKitidd.Datef = vKitidDatef 
                   No-Lock,   
     First bKact Fields (cart cunite)
                 Where bKact.Csoc  = bKitidd.csoc     /* requete avec bKitidd.csoc pour assurer l'ordre des requete sous Oracle: d'abord kitidd puis sur kact etc */
                   And bKact.Cetab = bKitidd.cetab
                   AND bKact.Cact  = bKitidd.Cact                                          
                   And bKact.Cetat = {&CETAT-ACTIF}
                 No-Lock,
     Last bKactd FIELDS({&EmptyFields})
                 Where bKactd.Csoc   = bKitidd.csoc
                   And bKactd.Cetab  = bKitidd.cetab
                   And bKactd.cact   = bKitidd.Cact
                   And bKactd.Datef <= vDateVigueur
                 NO-LOCK:
                    
     IF NOT bKitidd.tactype THEN DO:      /* NLE 02/01/2015 Q005956 */
        /* SLe 03/05/06
         * on v�rifie que les crit�res de l'activit� correspondent aux crit�res de lancement */
        {&Run-ACTBIB-Verif-Crit-Act} (Input  wcsoc,
                                      Input  wcetab,
                                      Input  bKitidd.cact,
                                      Input  Table Tt-criv, /* crit�res */
                                      Output o-ACTBIB-Retour,
                                      Output o-ACTBIB-Mess).
        If Not o-ACTBIB-Retour Then Do:
           Assign O-Ret = False
                  O-Msg = '(17) ':U + Substitute(Get-TradMsg (001378, 'L''activit� &1 ne correspond pas aux crit�res saisis.':T), bKitidd.cact).
           Return "".  
        End.
     END.

    If bKact.Cart = I-Cart Then 
      Assign O-Cunite = bKact.Cunite. 
                 
    Assign i = i + 1.
  End. /* Fin for each bkitidd */                              

  If i = 0 Then Do:
     Assign O-Ret = False
            O-Msg = '(13) ':U + Get-TradMsg (004759, 'Aucune activit� de l''itin�raire n''a de date en vigueur.':T).
     Return "".
  End.
  O-Ret = True.     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CopieCommOTetOTA Tablejv 
PROCEDURE CopieCommOTetOTA :
/*------------------------------------------------------------------------------
  Purpose:     En cas de copie d'OF ou de nouvel OF technique, copie dans le nouvel OF
               les commentaires de niveau OT et OTA.
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE INPUT PARAMETER iCsoc       AS CHARACTER NO-UNDO.
   DEFINE INPUT PARAMETER iCetab      AS CHARACTER NO-UNDO.
   DEFINE INPUT PARAMETER iPrechro    AS CHARACTER NO-UNDO.
   DEFINE INPUT PARAMETER iChronoPere AS INTEGER   NO-UNDO.
   DEFINE INPUT PARAMETER iChronoFils AS INTEGER   NO-UNDO.
   
   DEFINE BUFFER bKot  FOR kot.
   DEFINE BUFFER bKota FOR kota.
   
   /* niveau OT */
   FOR EACH bKot FIELDS (ni1)
                 WHERE bKot.csoc    = iCsoc
                   AND bKot.cetab   = iCetab
                   AND bKot.prechro = iPrechro
                   AND bKot.chrono  = iChronoPere
                 NO-LOCK:
      Run CopieCommOTetOTA2
        (Input iCsoc,
         Input iCetab,
         INPUT {&TYPCLE-COM-OT},
         Input iPrechro + ",":U + String(iChronoPere) + ",":U + string(bKot.ni1),
         Input iPrechro + ",":U + String(iChronoFils) + ",":U + string(bKot.ni1) ).
      
      /* niveau OTA */
      FOR EACH bKota FIELDS (ni2)
                    WHERE bKota.csoc    = iCsoc
                      AND bKota.cetab   = iCetab
                      AND bKota.prechro = iPrechro
                      AND bKota.chrono  = iChronoPere
                      AND bKota.ni1     = bKot.ni1
                    NO-LOCK:
         Run CopieCommOTetOTA2
           (Input iCsoc,
            Input iCetab,
            INPUT {&TYPCLE-COM-OT}, /* m�me code qu'OT m�me si c'est des OTA qu'on traite */
            Input iPrechro + ",":U + String(iChronoPere) + ",":U + string(bKot.ni1) + ",":U + STRING(bKota.ni2),
            Input iPrechro + ",":U + String(iChronoFils) + ",":U + string(bKot.ni1) + ",":U + STRING(bKota.ni2) ).
      END.        
   END.        
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE CopieCommOTetOTA2 Tablejv 
PROCEDURE CopieCommOTetOTA2 :
/*------------------------------------------------------------------------------
  Purpose:     Copie les commentaires du niveau donn�.
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
  Define Input  Parameter iCsoc      AS CHARACTER   NO-UNDO.
  Define Input  Parameter iCetab     AS CHARACTER   NO-UNDO.
  Define Input  Parameter iTypclecom AS CHARACTER   NO-UNDO.
  Define Input  Parameter iCleOFPere As Character   No-Undo.
  Define Input  Parameter iCleOFFils As Character   No-Undo.

  DEFINE BUFFER bKcom FOR kcom.
  

  /* recherche du commentaire � copier */
  FOR First kcom FIELDS (ccom com)
                 where kcom.csoc   = Icsoc
                   And kcom.cetab  = Icetab
                   And kcom.typcle = Itypclecom
                   And kcom.cle    = IcleOFPere
                 No-Lock:
     /* copie du commentaire */
     Find First bKcom where bKcom.csoc   = iCsoc
                        And bKcom.cetab  = iCetab
                        And bKcom.typcle = iTypclecom
                        And bKcom.cle    = iCleOFFils
                      EXCLUSIVE-LOCK NO-WAIT No-Error.
   
     If Not Available bKcom Then DO: 
         Create bKcom.
     
         Assign bKcom.csoc   = iCsoc
                bKcom.cetab  = iCetab
                bKcom.typcle = iTypclecom
                bKcom.cle    = iCleOFFils.
     END.
     
     ASSIGN bKcom.ccom   = kcom.ccom
            bKcom.com    = kcom.com.
     
     Validate bKcom.
  END.
         
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Criteres-de-Lancement Tablejv 
PROCEDURE Criteres-de-Lancement :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Variable whdl-crit As Widget-Handle No-Undo.
    
   Run Get-Link-One-Object In Adm-Broker-Hdl
                 (Input  THIS-PROCEDURE,
                  Input  "Criteres-Target":u,
                  Output whdl-Crit).
   If Valid-Handle(whdl-crit) Then Run Get-Tt-criv In whdl-crit (Output Table Tt-criv).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Disable-Fields Tablejv 
PROCEDURE Disable-Fields :
/*------------------------------------------------------------------------------
  Purpose:    CD D001232 29/05/2012 dans le cas de la modif d'OF lanc�s et commenc�s,
              seuls la zone "quantit�" doit �tre saisissable (par d�faut, toutes zones saisissables)
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/

   IF w-mod-of AND w-FCT = "OFL":u THEN
      Run Set-Info("Disable-Prior=NO":U, "Group-Assign-Target":U).   
      
   IF w-mod-of    /* autorisation � la modification OF lanc� et commenc� */
      AND (Available kof And kof.cetat = {&CETAT-OF-LANCE})  /* OF lanc� */ 
      AND NOT adm-new-record                                 /* pas en cr�ation (cr�ation ou copie) */   
      AND Adm-Fields-Enabled 
   THEN DO:   
   
      {&LANCE-ofbib}.  
      IF ({&ofbib-Possede-Decl-GP}            (kof.csoc, kof.cetab, kof.prechro, kof.chrono)
           OR {&ofbib-Possede-Decl-Atelier}   (kof.csoc, kof.cetab, kof.prechro, kof.chrono)
           OR {&ofbib-Possede-Affect-Matiere} (kof.csoc, kof.cetab, kof.prechro, kof.chrono)) /* commenc� */   
      THEN DO WITH FRAME {&FRAME-NAME}:
         /* uniquement la zone "qt� = f-qtestk2 + unit� = F-custk2 d'actif */ 
         Assign Tb-Datdeb:Sensitive    = No
                Tb-Datfin:Sensitive    = No
                kof.datdeb:Sensitive   = No
                F-heurdeb:Sensitive    = No
                kof.datfin:Sensitive   = No
                F-heurfin:Sensitive    = No
                kof.lot:Sensitive      = No
                kof.lotent:Sensitive   = No
                F-txfr:Sensitive       = No
                Kof.cres:Sensitive     = No
                kof.cequip:Sensitive   = No
                B_AID-lotent:Sensitive = kof.lotent:Sensitive.
          
          /* La priorit� (2�me onglet) ne doit pas non plus �tre saisissable */ 
          Run Set-Info("Disable-Prior=YES":U, "Group-Assign-Target":U).
       
      END.
   END.
   /* CJ : 14/01/2013 M127116 en cr�ation d'OF le champ �quipe est saisissable */
   ELSE IF adm-new-record THEN
      ASSIGN kof.cequip:Sensitive   = Adm-Fields-Enabled.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Tablejv  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Dupliq-OF Tablejv 
PROCEDURE Dupliq-OF :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   define input parameter w-chrono As Integer  No-Undo.
   define buffer b-ot    for Kot.
   define buffer b-ota   for kota.
   define buffer b-oto   for koto.
   define buffer b-otod  for kotod.
   define buffer b-of    for kof.
   define buffer b-com   for kcom.
   define buffer b-kcriv for kcriv.
   Define Buffer b-zart  For zart.
   DEFINE VARIABLE w-reconduireLotS AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vCformus         AS CHARACTER NO-UNDO. 
   DEFINE VARIABLE w-heure          AS INTEGER NO-UNDO. /* NLE 03/06/2008 M045640 */

   /* Buffer en exclusive-lock */
   DEFINE BUFFER bufKot   FOR kot.
   DEFINE BUFFER bufKota  FOR kota.
   DEFINE BUFFER bufKoto  FOR koto.
   DEFINE BUFFER bufKotod FOR kotod.
   DEFINE BUFFER bufKcriv FOR kcriv.
  
   vgKofCresOrigine = "". /* TA000363 MMO - initialisation */

   /* OF */
   for each b-of where b-of.csoc    = wcsoc
                   and b-of.cetab   = wcetab
                   and b-of.prechro = w-prechro
                   and b-of.chrono  = w-chrono
                 no-lock :
                  
      Buffer-Copy b-of Except csoc cetab prechro chrono orig datcre traisdat carts cvs cart cv cformu 
                            cetat datmod cuser lot datdebr heurdebr datfinr heurfinr duree2 
                            citi cact cres cequip
                            cavanc cprop
                            prechroCR chronoCR
                To kof.                      
      Assign kof.qte[2]   = 0
             kof.qtefr[2] = 0
             kof.cavanc   = {&CAVANC-EAT}
             kof.cprop    = {&CPROP-TOU}
             kof.qte[3]   = 0
             kof.qtefr[3] = 0
             vCformus     = b-of.cformus. 

             
      vgKofCresOrigine = b-of.cres. /* TA000363 MMO */


      /* NLE 03/06/2008 M045640 */
      DO WITH FRAME {&FRAME-NAME}:
        If Tb-Datdeb:Checked In Frame {&Frame-Name} Then Do:
            {admvif/method/saiheure.i Assign w-heure F-heurdeb yes}
            ASSIGN kof.datdeb               = DATE(kof.datdeb:SCREEN-VALUE)
                   kof.heurdeb              = w-heure
                   kof.datfin               = DATE(kof.datdeb:SCREEN-VALUE)
                   kof.heurfin              = w-heure
                   kof.datfin:SCREEN-VALUE  = kof.datdeb:SCREEN-VALUE
                   F-heurfin:SCREEN-VALUE   = F-heurdeb:SCREEN-VALUE
                NO-ERROR.
        End.    
        Else Do:
            {admvif/method/saiheure.i Assign w-heure F-heurfin yes}
            ASSIGN kof.datdeb               = DATE(kof.datfin:SCREEN-VALUE)
                   kof.heurdeb              = w-heure
                   kof.datfin               = DATE(kof.datfin:SCREEN-VALUE)
                   kof.heurfin              = w-heure
                   kof.datdeb:SCREEN-VALUE  = kof.datfin:SCREEN-VALUE
                   F-heurdeb:SCREEN-VALUE   = F-heurfin:SCREEN-VALUE
                NO-ERROR.
        End.      
    END.
    
    If kof.typof = {&TYPOF-OFTECH} Then kof.orig = {&ORIG-OF-ORIG}.
  End.

  /* OT */
  for each b-ot where b-ot.csoc    = wcsoc
                  and b-ot.cetab   = wcetab
                  and b-ot.prechro = w-prechro
                  and b-ot.chrono  = w-chrono
                  no-lock :
    create bufKot. 
    assign bufKot.chrono = kof.chrono
           bufKot.cetat  = {&CETAT-OF-MODIFIE}.
    Buffer-Copy b-ot Except chrono cetat lot datdebr heurdebr datfinr heurfinr dureetr
                            tfab tcons tfabres tconsres cavanc cprop tdatdebr tdatfinr
                            datcre datmod cuser
                            prechroCR chronoCR  /* NLE 26/06/2014 M152029 */
                To bufKot.                      
    Assign bufKot.qte[2]   = 0
           bufKot.qtefr[2] = 0           
           bufKot.cavanc   = {&CAVANC-EAT}
           bufKot.cprop    = {&CPROP-TOU}
           bufKot.qte[3]   = 0
           bufKot.qtefr[3] = 0
           bufKot.Datcre   = Today
           bufKot.Datmod   = Today
           bufKot.Cuser    = wcuser.
           
    /* Les crit�res */
    For Each b-kcriv Where b-kcriv.csoc   = b-ot.csoc
                       And b-kcriv.cetab  = b-ot.cetab
                       And b-kcriv.typcle = {&KCRIV-TYPCLE-OT}
                       And b-kcriv.cle    = b-ot.prechro        + ",":U +
                                            String(b-ot.chrono) + ",":U +
                                            String(b-ot.ni1)
                     No-Lock:
       Create bufKcriv.
       Assign bufKcriv.csoc   = bufKot.csoc
              bufKcriv.cetab  = bufKot.cetab
              bufKcriv.typcle = {&KCRIV-TYPCLE-OT}
              bufKcriv.cle    = bufKot.prechro        + ",":U +
                             String(bufKot.chrono) + ",":U +
                             String(bufKot.ni1).
       Buffer-Copy b-kcriv Except csoc cetab typcle cle To bufKcriv.
       
      /* Cr�ation des enregistrements de crit�res pour faciliter la s�lection des OT */
      /* SLe 19/04/10 Q003401 M077285 */
      Create bufKcriv.
      Assign bufKcriv.csoc   = bufKot.csoc
             bufKcriv.cetab  = bufKot.cetab
             bufKcriv.Typcle = {&KCRIV-TYPCLE-OFOT}
             bufKcriv.cle    = bufKot.Prechro + ',':u + string(bufKot.chrono).
      /* SLe 24/06/2011 Bug Buffer A004725 M0100769 */
      Buffer-Copy b-kcriv Except csoc cetab typcle Cle To bufKcriv.
    End.
  end.

  /* Entrants/Sortants et leurs crit�res*/
  for each b-ota where b-ota.csoc    = wcsoc
                   and b-ota.cetab   = wcetab
                   and b-ota.prechro = w-prechro
                   and b-ota.chrono  = w-chrono
                   no-lock :
    create bufKota. 
    assign bufKota.chrono = kof.chrono.
    Buffer-Copy b-ota Except chrono cavanc cprop
                      To bufKota.                      
    Assign bufKota.qte[2]   = 0
           bufKota.qtefr[2] = 0
           bufKota.cavanc   = {&CAVANC-EAT}
           bufKota.cprop    = {&CPROP-TOU}
           bufKota.qte[3]   = 0
           bufKota.qtefr[3] = 0.

    /* NLE 17/03/2005 : selon OFDEC2, on vide le lot sortant (sauf si saisi) pour qu'il soit reg�n�r� */
    IF bufKota.typacta   = {&TYPACTA-SORTANT}    AND
       vCformus         <> {&KOF-CFORMUS-SAISIE} THEN DO:      /* NLE 19/12/2008 M054190 */
        w-reconduireLotS = YES.
        FOR FIRST b-zart FIELDS(cfam) 
                         WHERE b-zart.csoc = bufKota.csoc
                           AND b-zart.cart = bufKota.cart
                         NO-LOCK:
          {getparii.i &CPACON    = "'OFCRE2':U"
                      &NOCHAMP   = 10
                      &CSOC      = bufKota.csoc
                      &CETAB     = bufKota.cetab
                      &CLE       = b-zart.cfam
                      &RETVALDEF = Yes}
          w-reconduireLotS = (vParam-Valeurs = 'YES':U).
        END.
        IF NOT w-reconduireLotS THEN 
            ASSIGN bufKota.lotsor = "":U
/* NLE 27/03/2008 M042403                   kof.cformus = {&KOF-CFORMUS-SAISIE}  */    /* NLE 02/06/2005 : lots des kota plus homog�nes (s'ils l'ont jamais �t�) */
                   kof.lot     = "":U.                      /*                  donc ils ne peuvent plus �tre "AUTO", et le lot d'OF est vid� */
    END.
    
    /* Les crit�res */
    For Each b-kcriv Where b-kcriv.csoc   = b-ota.csoc
                       And b-kcriv.cetab  = b-ota.cetab
                       And b-kcriv.typcle = {&KCRIV-TYPCLE-OTA}
                       And b-kcriv.cle    = b-ota.prechro        + ",":U +
                                            String(b-ota.chrono) + ",":U +
                                            String(b-ota.ni1)    + ",":U +
                                            String(b-ota.ni2)
                     No-Lock:
       Create bufKcriv.
       Assign bufKcriv.csoc   = bufKota.csoc
              bufKcriv.cetab  = bufKota.cetab
              bufKcriv.typcle = {&KCRIV-TYPCLE-OTA}
              bufKcriv.cle    = bufKota.prechro        + ",":U +
                             String(bufKota.chrono) + ",":U +
                             String(bufKota.ni1)    + ",":U +
                             String(bufKota.ni2).
       Buffer-Copy b-kcriv Except csoc cetab typcle cle To bufKcriv.
       
    End.
  end.
       
  /* Op�rations */
  for each b-oto where b-oto.csoc    = wcsoc
                   and b-oto.cetab   = wcetab
                   and b-oto.prechro = w-prechro
                   and b-oto.chrono  = w-chrono
                   no-lock :
    create bufKoto. 
    assign bufKoto.chrono = kof.chrono.
    Buffer-Copy b-oto Except chrono datdebr heurdebr datfinr heurfinr duree2 To bufKoto.
  end.
       
  /* Ressources */
  for each b-otod where b-otod.csoc    = wcsoc
                    and b-otod.cetab   = wcetab
                    and b-otod.prechro = w-prechro
                    and b-otod.chrono  = w-chrono
                    no-lock :
    create bufKotod. 
    assign bufKotod.chrono = kof.chrono.
    Buffer-Copy b-otod Except chrono nbr duree2 To bufKotod.
  end.

  /* On l�che les locks sur kot, kota, koto, kotod */
  Release bufKot.
  Release bufKota.
  Release bufKoto.
  Release bufKotod.
  RELEASE bufKcriv.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE enable-datdeb Tablejv 
PROCEDURE enable-datdeb :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input Parameter actif As Logical No-Undo.

   If Adm-Fields-Enabled Then Do With Frame {&Frame-Name}:
      Assign kof.datdeb:sensitive = actif
             kof.datfin:sensitive = not actif
             F-heurdeb:sensitive  = actif
             F-heurfin:sensitive  = not actif.   
      If kof.datdeb:sensitive Then      
         Apply "Entry":U to Kof.Datdeb.
      Else If kof.datfin:sensitive Then      
         Apply "Entry":U to Kof.Datfin.        
   End.    
        
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Enable-Lot Tablejv 
PROCEDURE Enable-Lot :
/*------------------------------------------------------------------------------
  Purpose:     Lot sortant pas modifiable quand il vient des sortants.
  Parameters:  
  Notes:       modif NLE 02/06/2005, cf zz
------------------------------------------------------------------------------*/

   IF AVAILABLE kof AND 
      NOT (Adm-new-record AND Adm-adding-record)
   THEN
      Assign kof.lot:sensitive In Frame {&Frame-Name} = Adm-fields-enabled AND (kof.cformus = {&KOF-CFORMUS-SAISIE} OR kof.cformus = '').
   ELSE
      Assign kof.lot:sensitive In Frame {&Frame-Name} = Adm-fields-enabled.  
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Enable-Lotent Tablejv 
PROCEDURE Enable-Lotent :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter i-typflux As Character    No-Undo.
    Define Input Parameter i-typof   As Character    No-Undo.
    Define Input Parameter i-carts   As Character    No-Undo.

DEFINE VARIABLE vRet    AS LOGICAL   NO-UNDO. /* variable de retour */
DEFINE VARIABLE vMsg    AS CHARACTER NO-UNDO.
/* MB D003368 03/09/08 */
DEFINE VARIABLE vtOFTec AS LOGICAL   NO-UNDO. /* T�moin indiquant que l'OF d'origine en cours */
                                              /* de modification a d�j� des OFs techniques */
DEFINE BUFFER bKof FOR kof.
                                              
   Do With Frame {&Frame-Name}:
        Case i-typflux:
           When {&TYPFLUX-POUSSE} Then Do:
               If i-carts   = '' Then 
                    w-lotent-OFPousse = False.
               ELSE DO:
                  /* OM : 16/10/2006 D002275 Mise en place include de centralisation creation OF */
                  {&RUN-OFBIB-AUTORISE-LOTENT}(wcsoc, wcetab, i-typflux, i-Typof, i-carts,
                                               OUTPUT w-lotent-OFPousse, 
                                               OUTPUT vRet, OUTPUT vMsg).
                  IF NOT vRet THEN DO:
                     {affichemsg.i &message = "vMsg"}
                  END.               
               END.
/*             Run Autorise-Lotent-Pousse (i-typflux, i-typof, i-carts). */
/*             kof.lotent:hidden    = Not w-lotent-OFPousse.     */
/*             If kof.lotent:hidden Then                         */
/*                kof.lotent:sensitive     = False.              */
/*             Else                                              */
/*                kof.lotent:sensitive     = adm-fields-enabled. */

               ASSIGN vtOFTec = (AVAILABLE kof 
                                 AND CAN-FIND (FIRST bKof WHERE bKof.Orig     = {&ORIG-OF-ORIG}
                                                            AND bKof.cSocor   = wcSoc
                                                            AND bKof.cEtabor  = wcEtab
                                                            AND bKof.Prechror = {&PRECHRO-OF}
                                                            AND bKof.Chronor  = kof.chrono /* i13-of */
                                                          NO-LOCK))
                      kof.lotent:SENSITIVE = w-lotent-OFPousse 
                                              AND adm-fields-enabled
                                              /* MB D003368 02/05/08 */
                                              /* Ne pas rendre la zone modifiable en cr�ation d'OF  */
                                              /* technique avec OF d'origine portant un lot entrant */
                                              AND ((AVAILABLE kof AND kof.lotent = "":U AND i-TypOf = {&TYPOF-OFTECH})
                                                   OR i-Typof <> {&TYPOF-OFTECH})
                                              /* Ni en modification d'OF origine ayant des OFs techniques */
                                              /*AND NOT vtOFtec.*/
                                              AND NOT (NOT Adm-New-Record
                                                       AND i-TypOf = {&TYPOF-OFORIG} 
                                                       AND vtOFtec). 
           End.
    
           When {&TYPFLUX-TIRE}   Then 
               Assign /*kof.lotent:hidden        = True*/
                      kof.lotent:sensitive     = False
                      kof.lotent:Screen-Value  = ''.
           
           Otherwise 
               Assign /*kof.lotent:hidden        = True*/
                      kof.lotent:sensitive     = False
                      kof.lotent:Screen-Value  = ''.
        End Case.
    
        Assign /*B_AID-lotent:Hidden    = kof.lotent:Hidden*/
               B_AID-lotent:Sensitive = kof.lotent:Sensitive.
   End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Enabled-Fields Tablejv 
PROCEDURE Enabled-Fields :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  Define Variable w-temp As Logical No-Undo.

  Do with frame {&FRAME-NAME} :

     Assign w-temp = Adm-Fields-Enabled And 
                    (Adm-New-Record And Not adm-adding-record OR
                     Available kof And (kof.cetat = {&CETAT-OF-SAISI} Or 
                                        kof.cetat = {&CETAT-OF-NON-CALCULE}))
            Rs-Iti:Sensitive    = w-temp
            Fl-Itiact:Sensitive = w-temp.            
   
     /* JS 04/05/2004 : Correction du bug -> les cases � cocher restent enabled
                        lorsqu'on click sur "Modifier/Annuler"  */
     Assign Tb-Datdeb:Sensitive = Adm-Fields-Enabled
            Tb-Datfin:Sensitive = Adm-Fields-Enabled.
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Envoie-itiact Tablejv 
PROCEDURE Envoie-itiact :
/*------------------------------------------------------------------------------
  Purpose:     Envoie au viewer Suite l'itin�raire ou l'activit�
               pour gestion combo type de l'OF par rapport au param�tre
               OFCRE/gestion classique ou technique des OF
  Parameters:  
  Notes:       
------------------------------------------------------------------------------*/
    
    
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Infos-ActiviteReference Tablejv 
PROCEDURE Infos-ActiviteReference :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       Sous-proc�dure SLe 14/02/2014
------------------------------------------------------------------------------*/
   DEFINE OUTPUT PARAMETER oCactRef AS CHARACTER NO-UNDO.
   DEFINE OUTPUT PARAMETER oCartRef AS CHARACTER NO-UNDO.
   DEFINE OUTPUT PARAMETER oDatef   AS DATE      NO-UNDO.
   
   DEFINE VARIABLE vDateOF   AS DATE      NO-UNDO.
   DEFINE VARIABLE vDatefIti AS DATE      NO-UNDO. 
   DEFINE BUFFER bKact   FOR kact.  
   DEFINE BUFFER bKactd  FOR kactd. 
   DEFINE BUFFER bKitid  FOR kitid.
   DEFINE BUFFER bKitidd FOR kitidd.
   
   /* CHU 20100208_0007 - report correction AM068217 - r�cup�ration date de l'OF */   
   ASSIGN vDateOF = (If Tb-Datdeb:CHECKED IN FRAME {&FRAME-NAME}
                         Then Date(Kof.Datdeb:SCREEN-VALUE)
                         Else date(Kof.Datfin:SCREEN-VALUE)).   
   
   CASE Rs-Iti:SCREEN-VALUE:
      WHEN "1":U THEN DO: /* Itin�raire */
         FOR LAST bKitid FIELDS (datef)
                         WHERE bKitid.cSoc  = wcSoc
                           AND bKitid.cEtab = wcEtab
                           AND bKitid.cIti  = Fl-ItiAct:SCREEN-VALUE
                           AND bKitid.Datef <= vDateOF /* CHU 20100208_0007 - report correction AM068217 - r�cup�ration date de l'OF */
                         NO-LOCK:
            ASSIGN vDatefIti = bKitid.Datef.
         END.
         IF vDatefIti <> ? THEN DO:
           FOR LAST bKitidd FIELDS (cAct cart)
                            WHERE bKitidd.cSoc  = wcSoc
                              AND bKitidd.cEtab = wcEtab
                              AND bKitidd.cIti  = Fl-ItiAct:SCREEN-VALUE
                              AND bKitidd.Datef = vDatefIti
                            NO-LOCK:
               ASSIGN oCactRef = bKitidd.cAct
                      oCartRef = bKitidd.cArt.
            END.
            /* MB 20071221_0010 */
            /* Recherche de la date d'effet de l'activit� */ 
            FOR LAST bKactd FIELDS (Datef)
                            WHERE bKactd.cSoc   = wcSoc
                              AND bKactd.cEtab  = wcEtab
                              AND bKactd.cAct   = oCactRef
                              AND bKactd.Datef <= vDateOF /* SLe 19/04/10 Q003369 M076597 vDatefIti */
                            NO-LOCK:
               ASSIGN oDatef  = bKactd.Datef.
            END. /* Fin For Last bKactd */
         END.
      END. /* Fin Itin�raire */
           
      WHEN "2":U THEN DO: /* Activit� */
         FOR LAST bKactd FIELDS (datef)
                         WHERE bKactd.cSoc   = wcSoc
                           AND bKactd.cEtab  = wcEtab
                           AND bKactd.cAct   = Fl-ItiAct:SCREEN-VALUE
                           AND bKactd.Datef <= vDateOF /* CHU 20100208_0007 - report correction AM068217 - r�cup�ration date de l'OF */
                         NO-LOCK:
            ASSIGN oDatef   = bKactd.Datef.
         END. /* Fin For Last bKactd */
        
         ASSIGN oCactRef = Fl-ItiAct:SCREEN-VALUE.
         
         FOR FIRST bKact FIELDS (cart)
                         WHERE bKact.cSoc   = wcSoc
                           AND bKact.cEtab  = wcEtab
                           AND bKact.cAct   = Fl-ItiAct:SCREEN-VALUE
                         NO-LOCK:
           ASSIGN oCartRef = bKact.cArt. /* SLe 11/12/07 */
         END.
      END. /* Fin Activit� */
   END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE init-commentaire Tablejv 
PROCEDURE init-commentaire :
/*------------------------------------------------------------------------------
  Purpose: L'initialisation du commentaire se fait en fonction du param�tre OFCRE:
             - 00 : Aucune initialisation
             - 01 : Init par le commentaire de l'article de prod
             - 02 : Init par le commentaire de l'activit�
                  - Flux pouss� : commentaire de la premi�re activit�
                  - Flux tir�   : commentaire de la derni�re activit�
  Parameters:  Entr�e:
                - Signal "l'OF suit un itin�raire ou une Activit�" (1/2)
                - Itin�raire ou Activit�
  Notes:       /* OM : 16/10/2006 D002275 refonte car externalisation dans ofbib */               
------------------------------------------------------------------------------*/
  Define Input Parameter iTItiact As Character No-Undo.
  Define Input Parameter iCitiact  As Character No-Undo.

DEFINE VARIABLE vCcom AS CHARACTER NO-UNDO. /* code du commentaire */
DEFINE VARIABLE vCom  AS CHARACTER NO-UNDO. /* commentaire */

  {&RUN-OFBIB-INITCOMMENTAIRE}
                  (wcsoc, wcetab, 
                   IF iTItiact = "1":U /* iti */ THEN iCitiact ELSE "":U,
                   IF iTItiact = "1":U /* iti */ THEN "":U     ELSE iCitiact,
                   If Tb-Datdeb:checked in Frame {&Frame-Name}
                      Then date(kof.datdeb:Screen-Value in Frame {&Frame-Name})
                      Else date(kof.datfin:Screen-Value in Frame {&Frame-Name}),
                   kof.carts:Screen-Value in Frame {&Frame-Name},   
                   OUTPUT vCcom,
                   OUTPUT VCom).
  IF vCcom <> '' OR vCom <> '' THEN
     Run Set-Info("Renseigne-Com=":U + vCcom + '�':U + vCom, "Group-Assign-Target":U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE init-droit-Panel Tablejv 
PROCEDURE init-droit-Panel :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
  Define variable w-liste-bouton as character No-undo.
  Define variable w-Modif-date   as Logical   No-undo.
  DEFINE VARIABLE vDroitSuppr    AS LOGICAL   NO-UNDO.
  DEFINE VARIABLE vDroitAjout    AS LOGICAL   NO-UNDO.

  /* CJ A4981 M102636 - Tenir compte des droits administrateurs */
  IF w-FCT = "OFL":u THEN
    Run Ctrl-Acces-Fonuse ("OFL1J":U, wcsoc, wcuser, {&CTRL-ACCES-SUPPRIMER}, output vDroitSuppr).
  ELSE vDroitSuppr = YES.
  
  IF w-FCT = "OFL":u THEN
    Run Ctrl-Acces-Fonuse ("OFL1J":U, wcsoc, wcuser, {&CTRL-ACCES-AJOUTER}, output vDroitAjout).
  ELSE vDroitAjout = YES.
  
  /* SLe 18/05/06 On ne peut rien faire sur les OF de transfert ou pr�pa */
  CASE kof.cofuse:
     WHEN {&KOF-COFUSE-TRANSFERT} OR
     WHEN {&KOF-COFUSE-PREPA}     THEN
        ASSIGN w-liste-bouton = "". 
     OTHERWISE
        Case Kof.Cetat :
            When {&CETAT-OF-BLOQUE}    Or
            When {&CETAT-OF-TERMINE}   Or
            When {&CETAT-OF-DECLOTURE} Or 
            When {&CETAT-OF-CLOTURE} THEN DO:
               IF vDroitSuppr AND vDroitAjout THEN 
                  ASSIGN w-liste-bouton = "Nouveau-Supprimer-Copier":u.
               ELSE IF vDroitSuppr AND NOT vDroitAjout THEN
                  ASSIGN w-liste-bouton = "Supprimer":u.
               ELSE IF NOT vDroitSuppr AND vDroitAjout THEN
                  ASSIGN w-liste-bouton = "Nouveau-Copier":u.
               ELSE
                  ASSIGN w-liste-bouton = "":u.
               
               ASSIGN w-Modif-date   = No.
            END.
               

            When {&CETAT-OF-PLANIFIE} Or
            When {&CETAT-OF-LANCE} Then Do:
               {&LANCE-ofbib}. 
               If (w-mod-of And w-FCT <> "OFL":U)
               Or (w-mod-of And w-FCT = "OFL":U /* CD D001232 30/05/2012 */
                            AND ({&PDR-Get-cPlanr}(kof.csoc, kof.cetab, kof.prechro, kof.chrono) = "":U))
                            AND NOT {&ofbib-Possede-Affect-Matiere} (kof.csoc, kof.cetab, kof.prechro, kof.chrono)   /* Pas d'affectation mati�re  */ 
                          /*  And Not {&ofbib-Possede-Decl-GP}        (kof.csoc, kof.cetab, kof.prechro, kof.chrono)    /* Pas de d�claration en GP   */
                            And Not {&ofbib-Possede-Decl-Atelier}   (kof.csoc, kof.cetab, kof.prechro, kof.chrono)    /* Pas de d�claration Atelier */*/
               THEN DO:
                  IF vDroitSuppr AND vDroitAjout THEN 
                     ASSIGN w-liste-bouton = "Valider-Nouveau-Supprimer-Copier":u.
                  ELSE IF vDroitSuppr AND NOT vDroitAjout THEN
                     ASSIGN w-liste-bouton = "Valider-Supprimer":u.
                  ELSE IF NOT vDroitSuppr AND vDroitAjout THEN
                     ASSIGN w-liste-bouton = "Valider-Nouveau-Copier":u.
                  ELSE
                     ASSIGN w-liste-bouton = "Valider":u.
                  
                  ASSIGN w-Modif-date   = YES.
               END.
              
               ELSE DO:
                  IF vDroitSuppr AND vDroitAjout THEN 
                     ASSIGN w-liste-bouton = "Nouveau-Supprimer-Copier":u.
                  ELSE IF vDroitSuppr AND NOT vDroitAjout THEN
                     ASSIGN w-liste-bouton = "Supprimer":u.
                  ELSE IF NOT vDroitSuppr AND vDroitAjout THEN
                     ASSIGN w-liste-bouton = "Nouveau-Copier":u.
                  ELSE
                     ASSIGN w-liste-bouton = "":u.
                  
                  ASSIGN w-Modif-date   = NO.
               END.
                   
            End.
            OTHERWISE DO:
               IF vDroitSuppr AND vDroitAjout THEN 
                  ASSIGN w-liste-bouton = "Valider-Nouveau-Abandon-Supprimer-Copier":u.
               ELSE IF vDroitSuppr AND NOT vDroitAjout THEN
                  ASSIGN w-liste-bouton = "Valider-Abandon-Supprimer":u.
               ELSE IF NOT vDroitSuppr AND vDroitAjout THEN
                  ASSIGN w-liste-bouton = "Valider-Nouveau-Abandon-Copier":u.
               ELSE
                  ASSIGN w-liste-bouton = "Valider-Abandon":u.
               
               ASSIGN w-Modif-date   = YES.
            END.
               
        End Case.
  END CASE.
 
  Run New-State("ActiveOnly-":u + w-liste-bouton + ",Tableio-Source":U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE init-info-ItiAct Tablejv 
PROCEDURE init-info-ItiAct :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
Define Input Parameter i-ecrase-seult-si-vide  As Logical   No-Undo.

DEFINE VARIABLE vCequip  AS CHARACTER NO-UNDO.
DEFINE VARIABLE vDatprod AS DATE      NO-UNDO.
DEFINE VARIABLE vRet     AS LOGICAL   NO-UNDO.
DEFINE VARIABLE vHeure   AS INTEGER   NO-UNDO.

do with frame {&frame-name} :

   Assign i-actbib-csoc  = Wcsoc
          i-actbib-cetab = Wcetab
          i-actbib-cart  = kof.carts:screen-Value  /* SLE 05/02/03 Oubli de cette ligne ? */
          i-actbib-ofcre = -1. /* on se base sur l'option "OFCRE":u */
   If Tb-Datdeb:checked THEN DO:
      ASSIGN i-actbib-datef = DATE (kof.datdeb:SCREEN-VALUE).
      {admvif/method/saiheure.i Assign vHeure F-heurdeb yes}
   END.
   ELSE DO:
      ASSIGN i-actbib-datef = DATE (kof.datfin:SCREEN-VALUE).
      {admvif/method/saiheure.i Assign vHeure F-heurfin yes}
   END.
   Case Rs-iti:screen-value :
     When "1":u then Assign i-actbib-citi  = Fl-itiact:screen-value
                            i-actbib-cact  = "":U.
     When "2":u then Assign i-actbib-citi  = "":U
                            i-actbib-cact  = Fl-itiact:screen-value.
   End case.         
               
   {&Run-actbib-Donnees-ItiActBis}

   If not o-actbib-Retour Then Return.

   /* cr�ation de la zone avant de la valoriser */
   /* IF NOT vgtRechTypgestres THEN   SLe 21/04/10 Q003421 M077831 */
   /* Si on est l�, c'est qu'on veut recharger les infos de la fab. choisie */
      RUN P-Gestion-ressources.
      
   /* SLe 03/12/09 J'ai enrichie ACTBIB pour que DonneesItiAct g�re aussi les POP */
   Assign Kof.cres:Screen-Value  = (If i-ecrase-seult-si-vide And Kof.cres:Screen-Value <> '' Then Kof.cres:Screen-Value Else o-actbib-cres)
          F-cunite1:Screen-Value = o-actbib-cunite
          F-cunite:screen-value  = o-actbib-cunite.
   IF vgTypgestres = {&KOF-CSIT-GESTION-PROCESS} THEN DO :
      IF i-actbib-cact <> "":U THEN DO:
         /* SLe 16/03/2011 A004461 ajout de datef */
         {popecc.i Kof.cres YES YES F-lres 0 i-actbib-cact o-actbib-datef}
      END.
      ELSE DO:
         {popecc.i Kof.cres YES YES F-lres 1 '' ?}
      END.
      /* SLe 18/04/2012 Journ�e de prod d'apr�s le calendrier de la 1�re ressource �l�mentaire avec calendrier */
      RUN bscald03jp.p (INPUT  wcsoc,wcetab,i-actbib-datef,vHeure,
                        INPUT  i-actbib-citi,i-actbib-cact,o-actbib-cres,
                        OUTPUT vDatprod, OUTPUT vCequip, OUTPUT vRet).
      ASSIGN kof.datprod:SCREEN-VALUE = STRING (vDatprod).  
      /* KL 20130909_0004: D1708. Ajout d'un contr�le sur l'O.F afin de d�terminer si celui-ci doit �tre li� � une �quipe lors de sa cr�ation. */
      /* Ce contr�le s'applique uniquement au O.F de type Origine. 
       * Vu avec Sophie : on �crase l'�quipe � partir du moment o� il y en a une au calendrier et m�me s'il y a d�j� une �quipe dans la zone (�ventuellement saisie par un utilisateur) */                      
      IF vRet AND vCequip NE "" AND /*  NLE 13/02/2014 Q006782 */ 
         GererParEquipe(wcsoc, wcetab, o-actbib-cactref, w-typof, "", 0, 0) THEN
         ASSIGN kof.cequip:SCREEN-VALUE  = vCequip.
/*       ELSE                                     */
/*          ASSIGN kof.cequip:SCREEN-VALUE  = "". */
   END.
   ELSE DO:
      {rescc.i Kof.cres YES yes F-lres 1 ACT}
      /* SLe 18/04/2012 Journ�e de prod d'apr�s le calendrier de la 1�re ressource �l�mentaire avec calendrier */
      RUN bscald03jp.p (INPUT  wcsoc,wcetab,i-actbib-datef,vHeure,
                        INPUT  i-actbib-citi,i-actbib-cact,o-actbib-cres,
                        OUTPUT vDatprod, OUTPUT vCequip, OUTPUT vRet). 
      ASSIGN kof.datprod:SCREEN-VALUE = STRING (vDatprod).
       /* KL 20130909_0004: D1708. Ajout d'un contr�le sur l'O.F afin de d�terminer si celui-ci doit �tre li� � une �quipe lors de sa cr�ation. */
       /* Ce contr�le s'applique uniquement au O.F de type Origine. 
        * Vu avec Sophie : on �crase l'�quipe � partir du moment o� il y en a une au calendrier et m�me s'il y a d�j� une �quipe dans la zone (�ventuellement saisie par un utilisateur) */
      IF vRet AND vCequip NE "" AND /*  NLE 13/02/2014 Q006782 */ 
         GererParEquipe(wcsoc, wcetab, o-actbib-cactref, w-typof, "", 0, 0) THEN
         ASSIGN kof.cequip:SCREEN-VALUE  = vCequip.
/*       ELSE                                     */
/*          ASSIGN kof.cequip:SCREEN-VALUE  = "". */
   END.
   Assign wstr             = {&Unite-Format-Qte} (o-actbib-cunite, "z,zzz,zz":U)
          F-qte1:Format    = wstr
          F-qtestk:Format  = wstr.

   If F-custk2:Screen-value = '' Or
      Not F-custk2:Modified 
   Then
      Assign F-custk2:Screen-value   = o-actbib-cunite
             F-custk2:Modified       = False   /* SLe 02/08/04 */
             w-old-custk2            = o-actbib-cunite
             F-qtestk2:Format        = {&Unite-Format-Qte} (F-custk2:Screen-value, "z,zzz,zz":U).

   If Decimal(F-qtestk2:Screen-value) = 0 Or 
      Not F-qtestk2:Modified 
   Then
      Assign F-qtestk2:Screen-value  = String(o-actbib-qte)
             F-qtestk2:Modified      = False. 

   If Decimal(F-qte1:Screen-value) = 0 Or 
      Not F-qte1:Modified 
   Then
      Assign F-qte1:Screen-value = String(o-actbib-qte)
             F-qte1:Modified     = False.

   If Not w-CacherFreinte And
      (Decimal(F-txfr:Screen-value) = 0 Or 
       Not F-txfr:Modified) 
   Then
      Assign F-txfr:Screen-value = String(o-actbib-txfr)
             F-txfr:Modified     = FALSE. 

   /* NLE : TEMPORAIRE : */
   /* SI TIRE : ART REF EST UN SORTANT : PAS DE FREINTE POUR L'INSTANT */
   /* LE TYPFLUX N'EST PAS DISPO : ALLER LE CHERCHER TEMPORAIREMENT */
   /* UTILISATION DE infoactbiblp.p ET infoitibiblp.p : A ENLEVER DES Definitions QUAND GESTION FREINTE SORTANTS */   
   Case Rs-Iti:Screen-Value : 
      When "1":u Then Do: /* Itin�raire */
         {&Run-InfoIti-RechInfo} (wcsoc, wcetab, Fl-itiact:screen-value, "typflux":U, Output vRet, Output w-typflux). 
      End.
      When "2":u Then Do: /* Activit� */
         {&Run-InfoAct-RechInfo} (wcsoc, wcetab, Fl-itiact:screen-value, "typflux":U, Output vRet, Output w-typflux). 
      End.
   End Case.
   If Not w-CacherFreinte Then Do:
       Case w-typflux :
           When {&TYPFLUX-TIRE} Or
           When ''              Then 
               /* MB D002845 31/10/07 */
               /* La zone taux de freinte devient modifiable (dans Pr-Maj-Qte1)    */
               /* seulement s'il n'y a pas de ligne de freinte pour l'art. de r�f. */
            /* ASSIGN F-txfr:Screen-Value = String(0). */
              If F-txfr:Sensitive Then F-txfr:Sensitive = False.
           
           When {&TYPFLUX-POUSSE} Then Do:
               If Not F-txfr:Sensitive And 
                  F-qteStk2:Sensitive Then F-txfr:Sensitive = True.
           End.
       End Case.
   End.
   /* NLE : FIN TEMPORAIRE */
 
   vgCactRefIti = o-actbib-CactRef. /* ALB 20140911_0006 Q7437 */
   
   Run Set-Info('Init-Csst-With-Cact=':U + String(Fl-Itiact:Screen-Value),
                 'Group-assign-Target':U).
      
   Run Pr-MAJ-qtestk.
   Run Pr-MAJ-qte1.

   Run New-State ("Critere, Group-Assign-Target":U). /* NB 21/04/2006 */

End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LinkFeatureID-Initialize Tablejv 
PROCEDURE LinkFeatureID-Initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   {artcc.i {&TABLE}.carts NO NO}
   {unitecc.i    F-custk2  NO NO}
   {equipcc.i {&TABLE}.Cequip NO NO}
   { actcc.i Fl-itiact NO NO}
   
   /* Ca marche dans 90% des cas (soit tout POP, soit RES, peu de mixte) , valid� avec SLE */
   IF vgTypgestres = {&KOF-CSIT-GESTION-PROCESS} THEN DO:    
      {popecc.i Kof.cres NO NO}
   END.
   ELSE DO:
      {rescc.i Kof.cres NO NO}
   END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record Tablejv 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  Define Variable W-Date        As Date     No-Undo.
  Define Variable W-heur        As Integer  No-Undo.
  Define Variable W-Traisdat    As Logical  No-Undo Init Yes.
  
   /* Code placed here will execute PRIOR to standard behavior. */
   Assign w-lotent        = ''
          w-typof-force   = NO
          w-SAV-typof     = ''
          w-SAV-carts     = ''
          w-SAV-itiact    = ''
          vgtRechTypgestres = FALSE.
   DO WITH FRAME {&FRAME-NAME}:
      Assign Tb-Datdeb:HIDDEN = False
             Tb-Datfin:HIDDEN = False.

      /* Sauvegarde des dates heures calcul�es pour les afficher */
      /* par d�faut sur l'OF suivant */
      If w-CacherFreinte THEN
         Assign F-qte1:Hidden     = True
                F-cunite1:Hidden  = True.
   END.

   If Available Kof Then Do:

    If Kof.traisdat Then Do:
       Assign W-Date = Kof.Datfin
              W-Heur = Kof.Heurfin.
       If W-Heur = {&Duree-Jour-Seconde} then Assign w-date = w-date + 1 
                                                     W-Heur = 0.
       
    End.
    Else Do:
       Assign W-Date = Kof.Datdeb
              W-Heur = kof.Heurdeb. 
       If W-Heur = 0 then Assign w-date = w-date - 1 
                                 W-Heur = {&Duree-Jour-Seconde}.
               
    End.
    W-traisdat = Kof.Traisdat.
    
  End.

  Run New-State('Nouveau,Info4-Target':u).

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  If Return-Value <> "Adm-Error":U Then Do:
  
    /* Code placed here will execute AFTER standard behavior.    */
    assign Tb-Datdeb:Sensitive In Frame {&Frame-Name} = Adm-Fields-Enabled
           Tb-Datfin:Sensitive In Frame {&Frame-Name} = Adm-Fields-Enabled.
    
    If W-Traisdat 
      Then Assign Tb-Datdeb:Screen-Value In Frame {&Frame-Name} = "Yes":U
                  Tb-Datfin:Screen-Value In Frame {&Frame-Name} = "No":U.
      Else Assign Tb-Datfin:Screen-Value In Frame {&Frame-Name} = "Yes":U
                  Tb-Datdeb:Screen-Value In Frame {&Frame-Name} = "No":U.
 
    If Tb-Datdeb:Checked In Frame {&Frame-Name} Then Do:
      Assign Kof.Datdeb:Screen-Value In Frame {&FRAME-NAME} = String(W-Date).     
      {admvif/method/saiheure.i Display W-Heur F-Heurdeb yes }
      Assign F-heurfin:screen-value in frame {&frame-name} = "24:00":U.
    End.    
    Else Do:
      Assign Kof.Datfin:Screen-Value In Frame {&FRAME-NAME} = String(W-Date).
      {admvif/method/saiheure.i Display W-Heur F-Heurfin yes }
      {admvif/method/saiheure.i Display 0 F-Heurdeb Yes}                
    End.      

    If Tb-Datdeb:Checked In Frame {&Frame-Name}
    Then Run Enable-Datdeb(yes).
    Else Run Enable-Datdeb(no).

    Assign F-qte1:Format            In Frame {&Frame-Name} = "z,zzz,zz9.9999":U
           F-qtestk2:Format         In Frame {&Frame-Name} = "z,zzz,zz9.9999":U
           F-qtestk:Format          In Frame {&Frame-Name} = "z,zzz,zz9.9999":U
           kof.lot:SENSITIVE        In Frame {&Frame-Name} = YES
/*           kof.lot:Sensitive        In Frame {&Frame-Name} = False*/
           kof.lot:Screen-Value     In Frame {&Frame-Name} = ''
           /*kof.lotent:Hidden        In Frame {&Frame-Name} = True*/
           kof.lotent:Sensitive     In Frame {&Frame-Name} = False
           kof.lotent:Screen-Value  In Frame {&Frame-Name} = ''
           /*B_AID-lotent:Hidden      In Frame {&Frame-Name} = kof.lotent:Hidden    In Frame {&Frame-Name}*/
           B_AID-lotent:Sensitive   In Frame {&Frame-Name} = kof.lotent:Sensitive In Frame {&Frame-Name}
           F-txfr:SENSITIVE         In Frame {&Frame-Name} = FALSE. /* OM : 02/01/2007 Q001029 En creation tant que pas d'ITIACT alors pas d'acces au tx de freinte */


    Assign kof.prechro:label  = w-lib-OFclas
           F-qtereste:Hidden  = Yes
           F-cureste:Hidden   = Yes.
    
    /* Pour savoir si la saisie des crit�res doit �tre ouverte en consultation 
     *                                                      ou en modification
     */
/* NLE 04/11/2003 */
/*    Run Set-Info ("Article:Sensitive=":U + String(f-qte1:Sensitive In Frame {&Frame-Name}),
 *                   "Container-Source":U).    */
    Run Set-Info ("Article:Sensitive=":U + String(f-qtestk2:Sensitive In Frame {&Frame-Name}),
                  "Container-Source":U).    
                  

/* NB : 30/12/2002 c'est dans le local-test-saisie */
/*      /* OM : 23/10/2002 ajout pour renseigner le chrono avant l'appel � NUMBATCH */
 *       /* Recherche du chrono. */
 *       Run chronojp.p (Input  {&CHRONO-NORMAL},
 *                       Input  wcsoc,
 *                       Input  wcetab,
 *                       Input  w-prechro,
 *                       Output w-chrono-newOF).
 *                     
 *       Assign kof.chrono:screen-value = string(w-chrono-newOF).                  
 *       /* OM : 23/10/2002 fin ajout */ */
      
  End.           

  RUN P-Gestion-ressources.
  
  If W-fct = "OFL":u Then
     Run New-state("Add-record,container-source":u).



  If vgOrigLanct = "OF":u Then
     Run Set-Info("Etat-OF=?":u ,"container-source":u).

   /* SLe 02/08/04 */
   /* Remise a zero des flags de modification de saisie */
   Run Dispatch ('Adv-Clear-Modified':U).
   /* -- */
     
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-record Tablejv 
PROCEDURE local-assign-record :
Define Variable W-Ret          As Logical   No-Undo.
   Define Variable W-Ano          As Logical   No-Undo.
   Define Variable W-Msg          As Character No-Undo.
   Define Variable W-CalQte       As Logical   No-Undo.
   Define Variable W-ModifQtestk  As Decimal   No-Undo.
   Define Variable W-ModifTxfr    As Decimal   No-Undo.
   Define Variable w-lot          As Character No-Undo.
   Define Variable W-typsai       As Character No-Undo.
   Define Variable w-LstCrit      As Character No-Undo.
   Define Variable W-Qte          As Decimal   No-Undo.
   Define Variable w-oforig-typof As Character No-Undo.
   Define Variable w-oforig-cetat As Character No-Undo.
   Define Variable W-renumLot     As Logical   No-Undo Initial No.

   Define Buffer b-kof For kof.
   Define Buffer b-kot For kot.

   Assign w-crn        = 0
          w-crt        = "":U
          w-msgWARNING = "":U.

   If not adm-new-record 
   Then 
      /* M�morisation des anciennes valeur de OF pour savoir si l'on
       * doit relancer le calcul des quantit�s pr�vues */
        Assign /* W-ModifQte1   = Kof.qte[1] */ /* MB D003368 31/07/08 */
               /* w-modifQte1 devient globale car n�cessaire dans Local-End-update pour MaJ OF Origine */
               vgModifQte1   = Kof.qte[1]
               W-ModifQtestk = Kof.qtestk
               W-ModifTxfr   = Kof.txfr.
   /* MB D003368 31/07/08 */
   ELSE ASSIGN vgModifQte1   = 0.  /* R�initialisation */   


  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-record':U ) .
 
  If w-oftech Then 
      Assign kof.typof    = {&TYPOF-OFTECH}
             w-typof      = kof.typof
             kof.orig     = {&ORIG-OF-ORIG}
             kof.csocor   = kof.csoc
             kof.cetabor  = kof.cetab
             kof.prechror = w-oforig-prechro
             kof.chronor  = w-oforig-chrono.
             
/* MB D003368 21/04/08 - Gestion du lot entrant sur les OF d'origine */
/* If kof.typof = {&TYPOF-OFORIG} Then
 *   Assign kof.lotent                                     = ''
 *          kof.lotent:Screen-Value In Frame {&Frame-Name} = ''.
 */       

  Assign w-rowid        = Rowid(kof)
         w-texte-entete = Substitute(Get-TradTxt (018296, 'Calcul de l''OF n�&1':T),String(kof.chrono)).

  {&Run-Ofbib-Init-OF-CR} (Input kof.csoc,
                           Input kof.cetab,
                           Input kof.prechro,
                           Input kof.chrono,
                           Input wcuser).
  If Return-Value = "Adm-Error":u Then Return "Adm-Error":u.

  Assign W-Ano = True.

  Run Get-Info ('Liste-Criteres':U,'Criteres-Target':U).
  Assign w-LstCrit = Return-Value.

  Find first kot of kof No-Lock No-Error.
  w-RelanceGenereOF = Adm-new-record And 
                      (Adm-Adding-Record Or
                       w-SAvCOPY-citi <> kof.citi Or
                       w-SAvCOPY-cact <> kof.cact Or
                       w-SAvCOPY-LstCrit <> w-LstCrit) Or
                      Not Available kot .
  
  /****************************************************/
  /*  Par d�faut, on cr�e un OF � l'�tat Non calcul�  */
  /****************************************************/  
  If (vgOrigLanct = 'CBNOF':u OR vgOrigLanct = 'PDPOF':u Or 
     /* on va g�n�rer un Of NC quand on a fait nouveau et pas copier */
      (( vgOrigLanct = 'OF':u    Or 
         vgOrigLanct = 'OFL':u ) And w-RelanceGenereOF) ) 
     And
     ( Adm-New-Record Or
       kof.cetat = {&CETAT-OF-SAISI} Or
       kof.cetat = {&CETAT-OF-NON-CALCULE} Or
       kof.cetat = {&CETAT-OF-MODIFIE})
     And Not w-oftech
  Then Do With Frame {&Frame-Name}:  
     If Adm-New-Record Then Do:
        Assign kof.cetat   = {&CETAT-OF-NON-CALCULE}.
        If kof.datdeb = ? Then
           Assign kof.datdeb  = kof.datfin
                  kof.heurdeb = kof.heurfin
                  kof.tdatdebr = No
                  kof.datdebr  = kof.datdeb
                  kof.heurdebr = kof.heurdeb.
        Else If kof.datfin = ? Then
           Assign kof.datfin  = kof.datdeb
                  kof.heurfin = kof.heurdeb
                  kof.tdatfinr = No
                  kof.datfinr  = kof.datfin
                  kof.heurfinr = kof.heurfin.
     End.     
     /* Ajout� sinon si l'OF se retrouve � l'�tat MOD, son typflux est vide */
     kof.typflux = w-typflux.   
  End.
  Else DO WITH FRAME {&FRAME-NAME}:
     /*******************************************************/ 
     /* Cr�ation de l'OF � partir du standard.              */  
     /* En creation mais pas en copie sans modif de iti/act */
     /*******************************************************/ 
     /**********************************************************/
     /* BLOC EXECUTE EN MODIFICATION D'OF, COPIE OU NOUVEL OF  */
     /* TECHNIQUE SEULEMENT,                                   */
     /* EN CREATION, C'EST LE LOCAL-END-UPDATE QUI EST UTILISE */
     /**********************************************************/
     
     /* << DD 20140411_0012 Q006521 M137477 */
     /* Apr�s une modification, on doit g�rer � nouveau les dates debut / fin  de la m�me mani�re */
     /* qu'en cr�ation car elles sont utilis�es lors de la g�n�ration de lot automatique.         */
     IF      Tb-Datdeb:CHECKED    THEN ASSIGN kof.datfin   = kof.datdeb            
                                              kof.heurfin  = kof.heurdeb           
                                              kof.tdatfinr = NO                   
                                              kof.datfinr  = kof.datfin           
                                              kof.heurfinr = kof.heurfin.         
     ELSE IF Tb-Datfin:CHECKED    THEN ASSIGN kof.datdeb   = kof.datfin            
                                              kof.heurdeb  = kof.heurfin           
                                              kof.tdatdebr = NO                   
                                              kof.datdebr  = kof.datdeb           
                                              kof.heurdebr = kof.heurdeb.
     /* >> DD 20140411_0012 Q006521 M137477 */

     If w-RelanceGenereOF
     Then do:
       {&Run-OFBIB-Generation-OF} (Input  kof.csoc,kof.cetab,kof.prechro,kof.chrono,
                                   Input  Kof.Citi, 
                                   Input  Kof.Cact, 
                                   Output W-Ancres,
                                   Output w-ret, 
                                   Output w-Msg).

       If Not w-ret Then Do:
         Assign Adv-MsgErreur = w-msg + Adv-MsgErreur
                w-crn         = 0
                w-crt         = Adv-MsgErreur.
         Return "Adm-Error":U.
       End.
       Else If w-Msg <> "":U Then w-msgWARNING = w-msg.
     End. 
     ELSE DO:
       If w-LstCrit <> w-LstCrit-OLD Then Do:
         {&Run-OFBIB-Crit-Prev0} (kof.csoc,kof.cetab,kof.prechro,kof.chrono).
       End.
       /* CJ 20120105_0008 A005017 M095150 mise � jour de l'�quipe de l'OT */
       For Each b-kot Where b-kot.Csoc    = kof.csoc
                        And b-kot.Cetab   = kof.cetab
                        And b-kot.prechro = kof.prechro
                        And b-kot.Chrono  = kof.Chrono
                        AND b-kot.cequip <> kof.cequip
                      Exclusive-Lock  :
          Assign b-kot.cequip = kof.cequip.
          Validate b-kot. 
       End.
     END.
     
     /* en modif d'OF, pour un lot sortant g�n�r� (et non saisi), et si LOTAUTO dit qu'il faut le reg�n�rer... */
     If Not Adm-New-Record
     And kof.cformus = {&KOF-CFORMUS-AUTO}
     And Regeneration-Num-Lot ()
     Then
         Assign Kof.lot    = "":U
                W-renumLot = Yes.
   
     If Adm-New-Record
     Or (Not Adm-New-Record And W-ModifLot /*And kof.lot <> "":U   NLE 15/06/2005 G7_1403 */)
     Or W-renumLot
     Or kof.lotent <> ''
     Then Do:
     
        {&Run-OFBIB-Lancement-MultiLot}
                             (Input  kof.csoc,kof.cetab,kof.prechro,kof.chrono,
                              Input  wcuser,
                              Output w-lot,
                              Output W-typsai,   /* Saisie Manuelle/Automatique du num�ro lot */
                              Output W-Ret, 
                              Output W-Msg).
     
        If Not w-Ret Then Do:
           Assign Adv-MsgErreur = w-msg + Adv-MsgErreur
                  w-crn         = 0
                  w-crt         = Adv-MsgErreur.
           Return "Adm-Error":U.
        End.
        Else If w-Msg <> "":U Then w-msgWARNING = w-msg + '|':u + w-msgWARNING.
       
        /* ---------------------------------------------------- MB 10/06/05 */
        /* Mise en place du param�tre d�terminant le lot sortant de l'OF    */
        /* dans le cas d'un conflit des lots pr�vus pour les sortants       */
        /* g�r� directement dans OFBIB-LANCEMENT-MULTILOT                   */
        /* => soit remise � blanc, soit lot sortant pr�vu de l'art. de r�f. */
        /* ---------------------------------------------------------------- */  
/*      If Plusieurs-Lots ({&TYPACTA-SORTANT}) THEN w-lot = '':U. */
        /* Fin MB 10/06/05 */

        Assign kof.lot     = w-lot
               kof.cformus = W-typsai.    /* Saisie Manuelle/Automatique du num�ro lot */
     End.

     /**************************************************************/
     /* Substitution des ressources en modification (pas en copie) */ 
     /**************************************************************/ 
     /* SLe 29/10/09 Je ne sais plus pourquoi pas en copie ??? */
     IF NOT vgtRechTypgestres THEN
        RUN P-Gestion-ressources.          
     
     IF ((Adm-new-record AND NOT Adm-adding-record) OR /* SLe 29/10/09 Copie aussi */
         (Not adm-new-record OR w-oftech))    /* Modification ou OF technique */ 
     /*And W-Ancres <> "":U SLE 09/02/05 */
     And W-Ancres <> Kof.cres:SCREEN-VALUE  
     AND NOT (vgKofCresOrigine = kof.cres:SCREEN-VALUE AND Adm-new-record AND NOT Adm-adding-record) /* TA000363 MMO cas de copie o� W-Ancres est mal valoris�e pour la v�rification de modification */
     Then Do:     
        IF vgTypgestres = {&KOF-CSIT-GESTION-PROCESS} THEN DO: 
           RUN popsubstp.p(INPUT  "OF":U,
                           INPUT  kof.csoc, kof.cetab, kof.prechro, kof.chrono,
                           INPUT  0, /* ni1 */
                           INPUT  0, /* ni2 */         
                           INPUT  Kof.cres:SCREEN-VALUE,                         
                           OUTPUT W-Ret,
                           OUTPUT W-Msg). 
        END.
        ELSE DO:        
           {&Run-OFBIB-Substitution-Ressource} 
                                   (Input  "OF":U,
                                    Input  kof.csoc,kof.cetab,kof.prechro,kof.chrono,0,0,
                                    Input  W-Ancres, 
                                    Output W-Ret, 
                                    Output W-Msg).         
        END.        
        
        If Not w-ret Then Do:
           Assign Adv-MsgErreur = w-msg + Adv-MsgErreur
                  w-crn         = 0
                  w-crt         = Adv-MsgErreur.
           Return "Adm-Error":U.
        End.
        Else If w-Msg <> "":U Then w-msgWARNING = w-msg + '|':u + w-msgWARNING.
     End. 

     /***************************************************/ 
     /* Initialisation des codes �tat de l'OF et des OT */
     /***************************************************/
     If kof.cetat <= {&CETAT-OF-MODIFIE} Then Do:
        {&Run-OFETAT-Etat-MODIFIE} (Input kof.csoc,kof.cetab,kof.prechro,kof.chrono,
                                     Input wcuser,
                                     Output W-Ret,
                                     Output w-Msg). 
        If Not w-ret Then Do:
           Assign Adv-MsgErreur = w-msg + Adv-MsgErreur
                  w-crn         = 0
                  w-crt         = Adv-MsgErreur.
           Return "Adm-Error":U.
        End.
        Else If w-Msg <> "":U Then w-msgWARNING = w-msg + '|':u + w-msgWARNING.
     End.

     /****************************/
     /* Contr�le global de l'OF  */    
     /****************************/     
     {&Run-OFBIB-Controle-GlobalOF} (kof.csoc,kof.cetab,kof.prechro,kof.chrono,
                                     Input  wcuser,
                                     Output w-Ret,
                                     Output w-ano,
                                     Output w-msg).
                                   
     If Not w-ret Then Do:
        Assign Adv-MsgErreur = w-msg + Adv-MsgErreur
               w-crn         = 0
               w-crt         = Adv-MsgErreur.

        Return "Adm-Error":U.
     End.
     Else If Not w-ano Then w-msgWARNING = w-msg + '|':u + w-msgWARNING.     /* ATTENTION : Not w-ano = IL Y A UN WARNING */

     /* Doit-on calculer les quantit�s pr�vues ? */
     W-CalQte =  Adm-New-Record Or          /* A la cr�ation */
               w-RelanceGenereOF Or
              (Not Adm-New-Record And     /* Quand on a modifi� la quantit� de lancement */
              (vgModifQte1   <> Kof.qte[1] Or
               W-ModifQtestk <> Kof.qtestk Or
               W-ModifTxfr   <> Kof.txfr)). 

     Case w-typof :
        When '':U Then                         /* OF CLASSIQUE */
            w-qte = kof.qte[1].

        When {&TYPOF-OFORIG} Then              /* OF D'ORIGINE */
          /* CJ A004547 Si on ne test pas : en copie, on tient compte du reste � faire de l'O.F. d'origine copi� */
          If Not Adm-New-Record THEN DO:
            /* ancienne quantit� = 140 */
            /* nouvelle quantit� = 150 */
            /* rechercher kot (quantit� reste � faire) = 127 */
            /* QUANTITE A PASSER : nouvelle - ancienne + reste � faire : 150 - 140 + 127 = 137 */ 
            For First b-kot Fields (qte[1])
                            Where b-kot.csoc    = kof.csoc
                              And b-kot.cetab   = kof.cetab
                              And b-kot.prechro = kof.prechro
                              And b-kot.chrono  = kof.chrono
                              And b-kot.ni1     = kof.ni1artref
                            No-Lock:
                w-qte = Max(0, kof.qte[1] - vgModifQte1 + b-kot.qte[1]).
            End.
          END.
          ELSE /* si on cr�e un nouvel OF, il n'y a pas de quantit� d�j� faite */
            w-qte = Max(0, kof.qte[1] - vgModifQte1).
        /* Fin {&TYPOF-OFORIG} */
        
        /* MB D003368 31/07/08 */
        /* Bloc OF TECHNIQUE d�plac� dans le Local-End-Update (apr�s la g�n�ration de */
        /* l'OF technique) pour que les mvts pr�visionnels de stock de l'OF d'origine */
        /* ne soient pas supprim�s d'entr�e de jeu mais en fin de traitement          */
        /* (car la mise � jour des affectations, affect039pp.p, les utilisent)        */
                    
        Otherwise
            w-qte = kof.qte[1].
     END CASE.
     
     {&Run-OFBIB-Calculer-OF}
          ("OF":u,kof.csoc,kof.cetab, kof.prechro,kof.chrono,0,0,
           wcuser,
           W-calQte,
           (If w-RelanceGenereOF
               Then "Creation":U
               Else "Modification":U),
           w-qte,
           Output w-Ret,
           Output W-Msg).
    
    If Not w-ret then do :
       Assign Adv-MsgErreur = w-msg + Adv-MsgErreur
              w-crn         = 0
              w-crt         = Adv-MsgErreur.
       Return "Adm-Error":U.
    End.
    If w-Msg <> "":U Then w-msgWARNING = w-msg + '|':u + w-msgWARNING.

    If kof.cetat < {&CETAT-OF-CALCULE} then do :

      {&Run-OFETAT-Etat-CALCULER} (Input  kof.csoc, kof.cetab, kof.prechro, kof.chrono,
                                   Output W-Ret,
                                   Output W-Msg). 
      If Not w-ret Then Do:
        Assign Adv-MsgErreur = w-msg + Adv-MsgErreur
               w-crn         = 0
               w-crt         = Adv-MsgErreur.
        Return "Adm-Error":U.
      End. 
      If w-Msg <> "":U Then w-msgWARNING = w-msg + '|':u + w-msgWARNING.
    End.
    
    If kof.cetat = {&CETAT-OF-LANCE} then do :

      {&Run-OFETAT-Etat-ReLANCER} 
          (Input  Kof.csoc, Kof.cetab, Kof.prechro, Kof.chrono,
           Output o-ofbib-Retour,
           Output o-ofbib-Mess). 
      If Not o-ofbib-Retour Then Do:
        Assign Adv-MsgErreur = "(999) ":u + o-ofbib-Mess + "|":u + Adv-MsgErreur
               w-crn         = 0
               w-crt         = Adv-MsgErreur.
        Return "Adm-Error":U.
      End.
      Else If o-OFBIB-Mess <> "" Then w-msgWARNING = o-OFBIB-Mess + '|':u + w-msgWARNING.
    End.
    
    /* En "Nouvel OF technique" (et pas en modif d'OF technique), 
     * l'amener dans le m�me �tat que son OF d'origine
     * NB : w-oftech est vrai en "Nouvel OF technique" et 
     *      pas en modif d'OF technique, donc on peut l'utiliser */
    IF w-oftech THEN DO:
      If w-oforig-cetat = {&CETAT-OF-PLANIFIE} Then Do:
        {&Run-OFETAT-Etat-Programmer}
            (Input  kof.csoc, kof.cetab, kof.prechro, kof.Chrono, wcuser,
             Output w-Ret,
             Output w-Msg).
        IF w-ret THEN FL-Cetat:Screen-Value = Get-TradClair( 'CETAT-AFF-OF':U, {&CETAT-OF-PLANIFIE}, '{&LCODE-CETAT-AFF-OF}':U, '{&LCLAIR-CETAT-AFF-OF}':U).
      End.
      ELSE
      If w-oforig-cetat = {&CETAT-OF-LANCE} Then Do:
        {&Run-OFETAT-Etat-ProgLancer}
           (Input  kof.csoc, kof.cetab, kof.prechro, kof.Chrono, wcuser,
            Output w-Ret,
            Output w-Msg).
        IF w-ret THEN FL-Cetat:Screen-Value = Get-TradClair( 'CETAT-AFF-OF':U, {&CETAT-OF-LANCE}, '{&LCODE-CETAT-AFF-OF}':U, '{&LCLAIR-CETAT-AFF-OF}':U).
      END.
    END.
  End.

  /* Copie des commentaires OT et OTA vers le nouvel OF */
  /* (marche en copie d'OF et en nouvel OF technique) */
  IF Adm-new-record AND NOT Adm-adding-record THEN 
     RUN CopieCommOTetOTA (kof.csoc, kof.cetab, kof.prechro, vgChronoOFActuel, kof.chrono).
  
  Assign W-Local-Ass-Record-Rowid = String(Rowid(kof))
         w-chrono-newOF = 0.
        /* vgOfOrigLotEnt = "". /* MB D003368 01/08/08 */*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement Tablejv 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   Define Variable W-qte       As Decimal           No-Undo.
   Define Variable W-TxFr      As Decimal           No-Undo.
   Define Variable W-Lot       As Character         No-Undo.
   Define Variable w-cartiti   As Character         No-Undo.
   Define variable w-cle       As   Character       No-Undo.
   Define Variable w-ret       As   Logical         No-Undo.
   Define variable w-msg       As   Character       No-Undo.
   Define Variable w-LstCrit   As   Character       No-Undo.
   Define Variable vCartent    As   Character       No-Undo.
   Define Variable vCartsor    As   Character       No-Undo.
   Define Variable w-crgesOF   As   Character       No-Undo.
   DEFINE VARIABLE vCcal       AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vCequip     AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vTravail    AS LOGICAL   NO-UNDO.
   Define Variable W-datDeb    As Date              No-Undo.
   Define Variable W-datFin    As Date              No-Undo.
   DEFINE VARIABLE vDate       AS DATE              NO-UNDO.
   DEFINE VARIABLE vHeure      AS INTEGER           NO-UNDO.
   
   /* Code placed here will execute PRIOR to standard behavior. */

  If adm-new-record then do With Frame {&Frame-Name} :
     Assign Kof.Csoc      = Wcsoc
            Kof.Cetab     = Wcetab
            Kof.prechro   = w-prechro
            Kof.Orig      = {&ORIG-MAN}
            Kof.Datcre    = Today
            Kof.cetat     = {&CETAT-OF-SAISI}
            Kof.cofuse    = w-cofuse
            Kof.csit      = vgTypgestres.
            
     /* OM : 23/10/2002 modification li� a l'appel de NUMBATCH */
     /* Chronojp.p est dor�navant appeler dans local-add-record */
     /* et local-copy-record */
     
     Assign kof.chrono = w-chrono-newOF.

     Run Pr-Rech-cartent-cartsor (Input  kof.carts:screen-value,
                                  Output vCartent,
                                  Output vCartsor,
                                  Output w-ret,
                                  Output w-msg).
     /*****************************************************************/
     /* Substitution de l'article saisi par l'article de fabrication. */
     /*****************************************************************/     
     If Not w-ret Or (vCartent = '' And vCartsor = '') Then Do:
        Assign Adv-MsgErreur = Substitute(Get-TradMsg (004783, 'Aucun article de fabrication n''est associ� � l''article &1.':T),kof.carts:screen-value) + 
                               '|':U + Get-TradMsg (004784, '(Fiche article de production - Planification).':T)
               w-crn         = 0
               w-crt         = Adv-MsgErreur.
        Return "Adm-Error":U.
     End.

     Case w-typflux /* SLE 04/08/03 zartfab.typfluxOF*/:
        When {&TYPFLUX-POUSSE} Then w-cartiti = /*zartfab.cartent*/ vCartent.
        When {&TYPFLUX-TIRE}   Then w-cartiti = /*zartfab.cartsor*/ vCartsor.
        When {&TYPFLUX-AUCUN}  Then w-cartiti = "":U.
     End Case.
     
     Assign kof.cart = w-cartiti.
     
     /* Ancien emplacement de Dupliq-OF */  
  End.
  ELSE
     Assign W-Qte    = Kof.Qte[1]
            W-TxFr   = Kof.Txfr
            W-Lot    = kof.lot
            W-datDeb = Kof.datdeb
            W-datFin = Kof.datfin.        
  
  /* NB 23/04/2003 : on peut modifier l'activit� sur un OF NC */
  If Adm-Fields-Enabled And 
     (Adm-New-Record And Not adm-adding-record OR
     Available kof And (kof.cetat = {&CETAT-OF-SAISI} Or 
     kof.cetat = {&CETAT-OF-NON-CALCULE}))
  Then Do:
     Case Rs-Iti:Screen-Value : 
        When "1":u Then Assign Kof.Citi = Fl-itiact:screen-value
                               Kof.cact = "":U.
        When "2":u Then Assign Kof.Cact = Fl-itiact:screen-value
                               Kof.Citi = "":U.
     End Case.     
  End.
  
  /* D�placement du code SLE 07/05/03 (on ne connaissait pas encore kof.citi */
  /* Dans le cas d'une copie sans modif de citi ou cact ou crit. de lancement,
   * duplication des fichiers - ancien emplacement de Dupliq-OF */  
  If Adm-New-Record Then Do:
     Run Get-Info ('Liste-Criteres':U,'Criteres-Target':U).
     w-LstCrit = Return-Value.
     
     If Not Adm-Adding-Record And
        w-SAvCOPY-citi    = kof.citi And        
        w-SAvCOPY-cact    = kof.cact And
        w-SavCOPY-LstCrit = w-LstCrit 
     Then 
        Run Dupliq-OF (Input w-SAvCOPY-chrono).
  End.
  /* -- */
  
  /***************/
  /* Libell�s OF */
  /***************/
  {&Run-SITE2-Recherche-Character} (Input wcsoc,
                                    Input wcetab,
                                    Input kof.carts:screen-value,
                                    Input "",
                                    Input "ZARTFAB.CRGESOF":U,
                                    Output w-crgesOF,
                                    Output w-ret,
                                    Output w-msg).
   IF w-crgesOF = '' THEN w-crgesOF = 'F01':U.
   
   Case w-crgesOF:
      When 'F03':u Then Do:
        /*  Crit�res de lancement */
        /* SLe 29/08/2011 ne fonctionne pas en copie [modif. criv2jv.w]
        on ne peut pas utiliser "Liste-criteres comme F04 car pas m�me format de s�parateur */
        Run Get-Info ("F-Critere":U, "Criteres-Target":U).
        Assign w-cle = Return-Value.
      End.
      
      When 'F04':u Then Do:
        /*  Valeurs du 1er crit�re de lancement */
        Run Get-Info ("Liste-Criteres":U, "Criteres-Target":U).
        Assign w-cle = Return-Value.
      END.
      
      OTHERWISE w-cle = ''.
   END CASE.

   {&RUN-OFBIB-LIBELLESOF} (Input  kof.csoc,
                            Input  kof.cetab,
                            Input  kof.prechro,
                            Input  kof.chrono,
                            Input  kof.carts:screen-value, 
                            Input  kof.citi,
                            Input  kof.cact,
                            INPUT  w-cle,
                            Output kof.lof,
                            Output kof.rof).
   
  /* Raisonnement en date d�but ou en date fin. */
  Do With Frame {&Frame-Name}:
    /* JS 05/04/2004 : Passage saisie manuelle si n�cessaire ; NLE 02/06/2005 : sauf si en copie*/    
    If Kof.Lot <> Kof.Lot:Screen-Value
      AND NOT (adm-new-record AND NOT adm-adding-record)     
    Then Assign Kof.cformus = {&KOF-CFORMUS-SAISIE}.

    /* NLE 23/07/2013 QA006517 */
    /* Remise en cause de la datprod car la datdeb a pu �tre chang�e. */
    vDate = DATE (Kof.datdeb:SCREEN-VALUE).
    {admvif/method/saiheure.i Assign vHeure F-heurdeb yes}
    RUN bscald03jp.p (INPUT  Kof.csoc,Kof.cetab,vDate,vHeure,
                      INPUT  Kof.citi,Kof.cact,Kof.cres,
                      OUTPUT Kof.datprod, OUTPUT vCequip, OUTPUT w-Ret).
    Kof.datprod:SCREEN-VALUE = STRING (Kof.datprod).
    /* Contr�le sur l'O.F afin de d�terminer si celui-ci doit �tre li� � une �quipe lors de sa cr�ation. */
    /* Ce contr�le s'applique uniquement aux OF de type Origine. 
     * Vu avec Sophie : on �crase l'�quipe � partir du moment o� il y en a une au calendrier et m�me s'il y a d�j� une �quipe dans la zone (�ventuellement saisie par un utilisateur) */                      
    IF w-Ret AND vCequip NE "" AND /*  NLE 13/02/2014 Q006782 */ 
       GererParEquipe(kof.csoc, kof.cetab, "", kof.typof, kof.prechro, kof.chrono, kof.ni1artref) THEN
       ASSIGN kof.cequip = vCequip
              kof.cequip:SCREEN-VALUE = vCequip.
    
    Assign Kof.traisdat = Tb-Datdeb:Checked
           Kof.Lot      = Kof.Lot:Screen-Value
           Kof.Lotent   = Kof.Lotent:Screen-Value
           Kof.Datmod   = Today
           Kof.Cuser    = wcuser
           W-Ancres     = Kof.Cres
           Kof.Carts    = kof.carts:screen-value.
           
/* NLE 23/07/2013 QA006517 */
/*           kof.datprod  = DATE (kof.datprod:SCREEN-VALUE).*/
  End.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .

  If Return-Value <> "Adm-Error":U Then Do:
    Run Pr-MAJ-qtestk.
    Run Pr-MAJ-qte1.
 
    /* Code placed here will execute AFTER standard behavior.    */
    {admvif/method/saiheure.i Assign Kof.Heurdeb F-heurdeb yes}
    {admvif/method/saiheure.i Assign Kof.Heurfin F-heurfin yes}

    Assign kof.tdatdebr = No
           kof.datdebr  = kof.datdeb
           kof.heurdebr = kof.heurdeb
           kof.tdatfinr = No
           kof.datfinr  = kof.datfin
           kof.heurfinr = kof.heurfin
           Kof.qte[1]   = Decimal(F-qte1:Screen-Value)
           Kof.cunite   = F-cunite1:Screen-Value
           Kof.qtestk   = Decimal(F-qtestk:Screen-Value)
           Kof.qtestk2  = Decimal(F-qtestk2:Screen-Value)
           Kof.custk2   = F-custk2:Screen-Value
           Kof.txfr     = Decimal(F-txfr:Screen-Value)
           Kof.qtefr[1] = Kof.qtestk - Kof.qte[1]
           W-ModifLot   = (W-Lot <> Kof.Lot).
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-cancel-record Tablejv 
PROCEDURE local-cancel-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  Define Variable w-liste-bouton As Character No-undo.

  /* NLE 30/08/2004 */
  If w-oftech Then Do:
    Assign w-oforig-prechro = ''
           w-oforig-chrono  = 0
           w-oftech         = FALSE
           w-SAV-typof      = ''           
           w-SAV-carts      = ''
           w-SAV-itiact     = ''.
           
    Run New-state("FIN-OF-TECH,Container-Source":U).
  End.

  w-typof-force = No.

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'cancel-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

  If Return-Value <> "Adm-Error":u Then Do With  Frame {&Frame-Name}:

/*     F-TxFr:Sensitive = Adm-Fields-Enabled /* And Decimal({&Table}.txfr:screen-value) <> 0 */ . /* NB 16/10/2003 */*/
  
     /* JS 23/06/2004 :  */
     If Not Available Kof 
     Then Do:
        Assign Tb-Datdeb:Sensitive    = No
               Tb-Datdeb:hidden       = True
               Tb-Datfin:Sensitive    = No
               Tb-Datfin:hidden       = True
               kof.datdeb:Sensitive   = No
               F-heurdeb:Sensitive    = No
               kof.datfin:Sensitive   = No
               F-heurfin:Sensitive    = No
/*               kof.lot:Hidden         = Yes*/
               kof.lot:Sensitive      = No
               /*kof.lotent:Hidden      = Yes*/
               kof.lotent:Sensitive   = No
               F-Qtestk2:Sensitive    = No
               F-Custk2:Sensitive     = No
               F-txfr:Sensitive       = No
               Kof.cres:Sensitive     = No
               kof.cequip:Sensitive   = No
               /*B_AID-lotent:Hidden    = kof.lotent:Hidden*/
               B_AID-lotent:Sensitive = kof.lotent:Sensitive.
     
        Assign w-liste-bouton = "Nouveau":u.
        Run New-State("ActiveOnly-":u + w-liste-bouton + ",Tableio-Source":U).
        Run New-state("Cancel-record,Container-Source":U).
     End.
  End.

  If W-fct = "OFL":u Then 
     Run New-state("Cancel-record,container-source":u).

  /* NB 30/12/2002, on remet � z�ro le nouveau chrono d'OF en cas d'annulation */
  Assign w-chrono-newOF = 0.
        /* vgOfOrigLotEnt = "". /* MB D003368 01/08/08 */*/

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-copy-record Tablejv 
PROCEDURE local-copy-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  /* Code placed here will execute PRIOR to standard behavior. */

  Define Variable w-qtestk    As Decimal   No-Undo.
  DEFINE VARIABLE vtQteStk    AS LOGICAL   NO-UNDO.
  Define Variable w-qte       As Decimal   No-Undo.
  Define Buffer b-zart  For zart.                                /* NLE 17/03/2005 */
  Define Variable w-reconduireLotS As Logical      No-Undo.   /* NLE 17/03/2005 */

  If Available kof Then Do:
    If kof.typof = {&TYPOF-OFTECH} Then Do:
      Assign w-oftech         = True
             w-oforig-prechro = kof.prechror
             w-oforig-chrono  = kof.chronor.

      Run New-State("OFTECH,Group-assign-Target":U).
    End.
    Else If w-typof <> {&TYPOF-OFTECH} Then
      Assign w-typof = kof.typof.
      
    /* SLe 27/10/09 On conserve la m�me valeur que l'OF copi� pour vgTypgestres */
    /* Mais pour que les Fill soient bien g�r�s apr�s copie, repassons dans Gestion-Ressource */
    ASSIGN vgtRechTypgestres = FALSE.
  End.
             
  w-typof-force = No.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'copy-record':U ) .


  /* Code placed here will execute AFTER standard behavior.    */

  If Return-Value <> "Adm-Error":U 
  Then Do with Frame {&Frame-Name}:
     Assign kof.carts:Sensitive = No 
            Tb-Datdeb:Sensitive  = Adm-Fields-Enabled
            Tb-Datfin:Sensitive  = Adm-Fields-Enabled.
     If Available kof Then Do:
        run enable-datdeb(kof.traisdat). 

        /* Sauvegarde de l'iti ou act et criteres de lancement copi�(e) pour 
         * savoir si on doit relancer la copie du std en ex�cutable ou si 
         * la simple copie des fichiers suffit
         */
        Assign w-SAvCOPY-chrono = kof.chrono
               w-SAvCOPY-citi = kof.citi
               w-SAvCOPY-cact = kof.cact.        
        Run Get-Info ('Liste-Criteres':U,'Criteres-Target':U).
        w-SavCOPY-LstCrit = Return-Value.       
     End.

     If kof.datdeb:sensitive Then      
        Apply "Entry":U to Kof.Datdeb.
     Else If kof.datfin:sensitive Then      
        Apply "Entry":U to Kof.Datfin. 
     

     /* NLE 17/03/2005 : selon OFDEC2, on vide le lot sortant (sauf si saisi) pour qu'il soit reg�n�r� */

/*      /* Il faut remettre � blanc le n� de lot OF pour relancer la num�rotation auto */ */
/*      /* sauf si of technique */     /* NLE 10/02/2005 */                               */
/*      IF NOT w-oftech AND            /* NLE 10/02/2005 */                               */
/*         Assign kof.lot:Screen-Value    = "":U.                                         */

     IF kof.cformus <> {&KOF-CFORMUS-SAISIE} THEN DO:    /* NLE 19/12/2008 M054190 */
           w-reconduireLotS = YES.
           FOR FIRST b-zart FIELDS(cfam) 
                            WHERE b-zart.csoc = kof.csoc
                              AND b-zart.cart = kof.carts
                            NO-LOCK:
             {getparii.i &CPACON    = "'OFCRE2':U"
                         &NOCHAMP   = 10
                         &CSOC      = kof.csoc
                         &CETAB     = kof.cetab
                         &CLE       = b-zart.cfam
                         &RETVALDEF = Yes}
             w-reconduireLotS = (vParam-Valeurs = 'YES':U).
           END.
/*           IF NOT w-reconduireLotS THEN kof.lot:Screen-Value = "":U.*/
           IF NOT w-reconduireLotS THEN 
              ASSIGN /*kof.cformus            = {&KOF-CFORMUS-SAISIE}      /* NLE 02/06/2005 : lots des kota plus homog�nes (s'ils l'ont jamais �t�) */*/
                     kof.lot:SCREEN-VALUE   = "":U.                      /*                  donc ils ne peuvent plus �tre "AUTO", et le lot d'OF est vid� */
     END.

     /* NLE 17/03/2005 : fin */
    
    /*
     * Permet de rendre les criteres modifiables 
     */
     Run Set-Info ("Article:Sensitive=":U + String(f-qtestk2:Sensitive In Frame {&Frame-Name}),
                   "Container-Source":U).    

     /* NLE 24/09/2004 */       
     If w-oftech And Available kof Then Do With Frame {&Frame-Name}:
        If w-typof = {&TYPOF-OFTECH} THEN
           /* En flux pouss� la qt� pr�vue est la qt� � d�stocker */
           /* En flux tir� la qt� pr�vue est la qt� qui rentre en stock */
           ASSIGN vtQteStk = (kof.typflux = {&TYPFLUX-POUSSE})
                  w-qtestk = Fo-QteReste().  /* kot de reference : reste � faire */
        Else        /* OF classique */
           ASSIGN vtQteStk = TRUE
                  w-qtestk = kof.qtestk.
        IF vtQteStk THEN DO:
           ASSIGN F-qtestk:Screen-Value = String(w-qtestk).           
           /* qte1 = w-qte - txfr % */
           F-qte1:Screen-Value = String(w-qtestk * (1 - Decimal(F-txfr:Screen-Value) / 100)).        
        END.
        ELSE DO: /* Qte[1] et il faut calculer qtestk */
           RUN Pr-OFTECH-Qte-tire (INPUT-OUTPUT w-qtestk).
        END.
        
        /* en d�duire qtestk2 */
        If F-custk2:Screen-Value = F-cunite:Screen-Value Then
            w-qte = w-qtestk.
        Else Do:
            /* reconvertir qtestk2 en cunite1 au lieu de prendre qtestk car pb arrondis sinon */
            {&Run-Actbib-Conv-Act-Art} (Input  wcsoc, kof.carts:Screen-Value, 
                                        Input  kof.cunite, kof.lotq, kof.cu2, kof.lotq2,
                                        Input  w-qtestk, F-cunite:Screen-Value, F-custk2:Screen-Value,
                                        Output w-qte, Output w-ret, Output w-msg).
        End.
        F-qtestk2:Screen-Value = String(w-qte).              
     End.
     /* MB D002845 30/11/07 - SLe 14/02/2014 ELSE pour ne pas refaire 2 fois les choses */
     ELSE IF w-TypFlux = {&TYPFLUX-TIRE} THEN
        RUN Pr-MAJ-Qte1.
  End.
  If W-fct = "OFL":u Then
     Run New-state("Copy-record,container-source":u).

  If vgOrigLanct = "OF":u Then
     Run Set-Info("Etat-OF=?":u ,"container-source":u).

  /* NLE 24/02/2005 : pour mettre l'�tat � CALCULE, notamment en cas de copie d'un OF PRG ou LAN,
   *                  l'OF g�n�r� est CAL, mais restait affich� � PRG ou LAN, ce qui �tait confusing. */
  Assign FL-Cetat:Screen-Value = Get-TradClair( 'CETAT-AFF-OF':U, {&CETAT-OF-CALCULE}, '{&LCODE-CETAT-AFF-OF}':U, '{&LCLAIR-CETAT-AFF-OF}':U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-record Tablejv 
PROCEDURE local-delete-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE VARIABLE vCleOF AS CHARACTER NO-UNDO.
   
  /* Code placed here will execute PRIOR to standard behavior. */  
  /* SLE 13/11/02 tout le code de tests pr�alables est dans un include */
  {ofsupci.i}
   
  ASSIGN vCleOF = String(kof.csoc)    + "|":u + 
                  String(kof.cetab)   + "|":u + 
                  String(kof.prechro) + "|":u + 
                  String(kof.chrono)  + "|":u + 
                  String(kof.typof)
         w-typof-force = No.
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-record':U ) .
  IF RETURN-VALUE = 'Adm-Error':U THEN RETURN "ADM-ERROR":U. /* OM : 23/01/2006 evite 1 bug sur annulation de suppresion */  
  
  /* Code placed here will execute AFTER standard behavior.    */
  If W-fct = "OFL":u Then
     Run Set-Info ('supprimer-OF=':u + vCleOF,'Container-Source':U).
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-delete-statement Tablejv 
PROCEDURE local-delete-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
   Define Variable W-Ret   As Logical   No-Undo.
   Define Variable W-Msg   As Character No-Undo.
   Define Variable w-qte   As Decimal   No-Undo.
   Define Variable w-delta As Decimal   No-Undo.

   Define Buffer b-kof For kof.
   Define Buffer b-kot For kot.
 
  /* Code placed here will execute PRIOR to standard behavior. */
   
  {&setcurs-wait}  

  
  For Each B-kot FIELDS (ni1)
                 Where B-kot.csoc    = kof.csoc
                   and B-kot.cetab   = kof.cetab
                   and B-kot.prechro = kof.prechro
                   and B-kot.chrono  = kof.chrono
                 No-Lock :
    /* Indicateurs fab */
    {&RUN-FABIND01-GestionFabEvt}
         (INPUT {&FABEVT-SUP-OT},
          INPUT kof.csoc,
          INPUT kof.cetab,
          INPUT kof.prechro,
          INPUT kof.chrono,
          INPUT b-kot.ni1,
          INPUT 0).
  END.

   /* Suppression de l'Of et de ses 'pi�ces jointes' */  
  {&Run-OFBIB-SupprimeOF} (Input  kof.csoc,kof.cetab,kof.prechro,kof.chrono,
                           Input  Yes, /* Info Ent�te inclus */
                           Input  wcuser,     /* JS 25/05/2004 */
                           Output W-Ret,
                           Output w-msg). 

  If Not w-ret Then Do:
     Assign Adv-MsgErreur = w-msg + Adv-MsgErreur.
     {&setcurs-nowait}    
     Return "Adm-Error":U.
  End.
  
  {&setcurs-nowait}
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'delete-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-disable-fields Tablejv 
PROCEDURE local-disable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  
  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'disable-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Run Enabled-fields.
  
  /* CD D001232 29/05/2012 modification possible OF commenc�s */     
  RUN Disable-Fields.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields Tablejv 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
Define Variable vtExiste As Logical   No-Undo.
Define Variable vDatef   As Date      No-Undo.
Define Variable vRet     As Logical   No-Undo.
Define Variable vMsg     As Character No-Undo.
   
   /* Code placed here will execute PRIOR to standard behavior. */
   ASSIGN w-typof-force   = NO
          w-SAV-typof     = ''
          w-SAV-carts     = ''
          w-SAV-itiact    = ''
          w-ancien-lotent = ''
          vgtRechTypgestres = FALSE.

   If Available Kof Then Do With Frame {&Frame-Name}:
                 
      ASSIGN vgChronoOFActuel = kof.chrono
             w-typof          = kof.typof
             w-typof-force    = NO
             w-lotent         = kof.lotent
             w-ancien-lotent  = kof.lotent.

      Case w-typof:
         When {&TYPOF-OFORIG} Then 
            Assign kof.prechro:label = w-lib-oforig
                   F-QteReste:Format = {&Unite-Format-Qte} (kof.cunite, "z,zzz,zz":U)
                   F-QteReste:Screen-Value = String(Fo-QteReste())
                   F-CuReste:Screen-Value = kof.cunite
                   F-QteReste:Hidden = False
                   F-CuReste:Hidden  = False.                  
         When {&TYPOF-OFTECH} Then 
            Assign kof.prechro:label = w-lib-oftech
                   F-QteReste:Hidden = True
                   F-CuReste:Hidden  = True.
         Otherwise 
            Assign kof.prechro:label = w-lib-ofclas
                   F-QteReste:Hidden = True
                   F-CuReste:Hidden  = True.
      End Case.
    
      Assign FL-Cetat:Screen-Value = Get-TradClair( 'CETAT-AFF-OF':U, kof.cetat, '{&LCODE-CETAT-AFF-OF}':U, '{&LCLAIR-CETAT-AFF-OF}':U).

      Run init-droit-Panel.

      /* Corrige le pb d'activation des deux champs dates */
      Run Enable-Datdeb(Tb-Datdeb:Checked).

      ASSIGN vDatef = (If kof.traisdat
                         Then kof.Datdeb
                         Else kof.Datfin).

      {&Run-ACTBIB-Exist-Crit-Oblig-Fab} (Input  kof.csoc   ,
                                         Input  kof.cetab  ,
                                         Input  kof.citi   ,
                                         Input  kof.cact   ,
                                         Input  vDatef    ,
                                         Output vtExiste   ).
      If Return-Value <> "Adm-Error":U 
       Then vtExiste = Not vtExiste And f-qtestk2:Sensitive . 
       Else vtExiste = f-qtestk2:Sensitive .
    
      Run Set-Info ("Article:Sensitive=":U + String( vtExiste ),
                    "Container-Source":U).    

      /* JS 09/07/2004 : Correction bug "enable" date de fin avec case d�but coch�e */
      If kof.cetat = {&CETAT-OF-MODIFIE} or kof.cetat = {&CETAT-OF-CALCULE}
      or kof.cetat = {&CETAT-OF-PLANIFIE} or kof.cetat = {&CETAT-OF-LANCE}
      then    
         run enable-datdeb(kof.traisdat).

      if kof.traisdat
      then assign Tb-Datdeb:Screen-value = "Yes":U
                  Tb-Datfin:Screen-value = "No":U.
      else assign Tb-Datfin:Screen-value = "Yes":U
                  Tb-Datdeb:Screen-value = "No":U.
        
      Run Enable-lot.

      If kof.cact <> "":U
      Then
        assign rs-iti:screen-value    = "2":U
               Fl-itiact:Screen-Value = Kof.Cact.         
      else 
        assign rs-iti:screen-value    = "1":U
               Fl-itiact:Screen-Value = Kof.Citi.        
    
      Run Set-Info('itiact=':U + (If Rs-iti:screen-value = '1':U 
                                   Then 'OUI':U 
                                   Else 'NON':U)      + '|':U + 
                               FL-itiact:Screen-Value   + '|':U + 
                               (If Tb-Datdeb:checked 
                                   Then String(kof.datdeb:Screen-Value)
                                   Else String(kof.datfin:Screen-Value)), 
                 'Group-assign-Target':U).
                 
                 
                  
     
      {admvif/method/saiheure.i Display Kof.Heurdeb F-Heurdeb no }
      {admvif/method/saiheure.i Display Kof.Heurfin F-Heurfin no }
   End.
   Else 
      assign Tb-Datdeb:Sensitive = No
             Tb-Datfin:Sensitive = No.  

   /* Dispatch standard ADM method.                             */
   RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .
   
   If Return-Value <> "Adm-Error":U Then Do:
      /* Code placed here will execute AFTER standard behavior.    */
      {Artcc.i kof.carts No Yes Zart.Lart 2 {&TYPART-MARCHANDISE} }  
      Assign w-SAVcarts = kof.carts:Screen-Value.

      If rs-iti:screen-value in frame {&Frame-Name} = "1":U Then Do:   
         { iticc.i Fl-itiact No Yes Fl-litiact }
         If Available buf-kiti Then w-typflux = buf-kiti.typflux.
         { iticc.i Fl-itiact NO NO}
      End.
      Else Do:  
         { actcc.i Fl-itiact No Yes Fl-litiact }
         If Available buf-kact Then w-typflux = buf-kact.typflux.
         { actcc.i Fl-itiact NO NO}         
      End.

      If w-typflux = "" AND Available kof Then Do: 
         /* c'est que la fabrication n'est plus valide */
         {&Run-SITE2-Recherche-Character} (Input wcsoc,
                                           Input wcetab,
                                           Input kof.carts:Screen-Value,
                                           Input "",
                                           Input "ZARTFAB.TYPFLUXOF":U,
                                           Output w-typflux,
                                           Output vRet,
                                           Output vMsg).
      End.

      RUN P-Gestion-ressources.
      
      /* process op�ratoire*/
      IF vgTypgestres = {&KOF-CSIT-GESTION-PROCESS} THEN DO:
         IF AVAILABLE buf-kact THEN DO:
            /* SLe 16/03/2011 A004461 en connaissance de cause, je laisse avec TODAY (tant pis pour les O.F. dans le futur)
             * car p�nalit� pour rechercher la datef */
            {popecc.i Kof.cres NO YES F-lres 2 buf-kact.cact ? } 
         END.
         ELSE DO:
            {popecc.i Kof.cres NO YES F-lres 3 '' ? }
         END.
      END.
      ELSE DO:
        { rescc.i Kof.cres no yes F-lres 1 }
      END.

      If Available kof Then Do:       
         Assign wstr                   = {&Unite-Format-Qte} (kof.custk2, "z,zzz,zz":U)
              F-qtestk2:Format       = wstr
              wstr                   = {&Unite-Format-Qte} (kof.cunite, "z,zzz,zz":U)
              F-qtestk:Format        = wstr
              F-qte1:Format          = wstr
              F-qtestk2:Screen-Value = String(kof.qtestk2)
              F-custk2:Screen-Value  = kof.custk2
              w-old-custk2           = kof.custk2
              F-qtestk:Screen-Value  = String(kof.qtestk)
              F-cunite:Screen-Value  = kof.cunite
              F-qte1:Screen-Value    = String(kof.qte[1])
              F-cunite1:Screen-Value = kof.cunite
              F-txfr:Screen-Value    = String(kof.txfr)
              F-qtestk2:modified     = False.

         If w-CacherFreinte Then
            If F-custk2:Screen-Value = F-cunite1:Screen-Value Then
               Assign F-qte1:Hidden     = True
                      F-cunite1:Hidden  = True.
            Else
               Assign F-qte1:Hidden     = False
                      F-cunite1:Hidden  = False.
         Else Do:
            Case w-typflux :
               When {&TYPFLUX-TIRE} Or
               When '' Then Do:
/* SLe 18/01/2010 on g�re maintenant la freinte en tir�  F-txfr:Screen-Value = String(0). */
                  If F-txfr:Sensitive Then F-txfr:Sensitive = False.
               End.
               When {&TYPFLUX-POUSSE} Then Do:
                  If Not F-txfr:Sensitive And 
                     F-qteStk2:Sensitive  And
                     kof.cetat <= {&CETAT-OF-CALCULE} 
                  Then F-txfr:Sensitive = True.
               End.
            End Case.
         End.

         Run Pr-Gestion-Aff-qtestk.
      End.
      
      Run Enabled-fields.

      Run Enable-Lotent(w-typflux, w-typof, If Available kof Then kof.carts Else '').

      If w-typflux = {&TYPFLUX-POUSSE} And kof.lotent:SENSITIVE Then 
         ASSIGN kof.lotent:Screen-Value = kof.lotent.

      Run Get-Info ('Liste-Criteres':U,'Criteres-Target':U).

      If Available kof Then
         Assign w-datedeb-Avant-Modif = kof.datdeb:Screen-value
                w-datefin-Avant-Modif = kof.datfin:Screen-value .
               
      Run Dispatch("Adv-Clear-Modified":U).
   End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-enable-fields Tablejv 
PROCEDURE local-enable-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'enable-fields':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Run Enabled-fields.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-end-update Tablejv 
PROCEDURE local-end-update :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
Define Variable W-Com-Ano      As Character No-Undo.
Define Variable w-csoc         As Character No-Undo.
Define Variable w-cetab        As Character No-Undo.
Define Variable w-prechro      As Character No-Undo.
Define Variable w-chrono       As Integer   No-Undo.
Define Variable W-Ret          As Logical   No-Undo.
Define Variable W-plusieurs    As Logical   No-Undo.
Define Variable W-msg          As Character No-Undo.
DEFINE VARIABLE vCres          AS CHARACTER NO-UNDO.

DEFINE BUFFER bkot FOR kot.


  /* Code placed here will execute PRIOR to standard behavior. */
  If w-msgWARNING <> "":U  And
     w-msgWARNING <> "|":U Then Do:         /* NLE 04/11/2003 */
    {affichemsg.i  &Message="w-msgWARNING" }
  End.
  
  Find First kof where Rowid (kof) = To-Rowid (W-Local-Ass-Record-Rowid)
                 No-Lock No-Error.

  If Available kof And
     ( vgOrigLanct = 'OFL':u Or
       vgOrigLanct = 'OF':u )  And
     W-RelanceGenereOF   And
     ( kof.cetat = {&CETAT-OF-NON-CALCULE} Or
       kof.cetat = {&CETAT-OF-MODIFIE} )
  Then do:
    Assign w-csoc    = kof.csoc
           w-cetab   = kof.cetab
           w-prechro = kof.prechro
           w-chrono  = kof.chrono
           vCres     = Kof.cres:SCREEN-VALUE IN FRAME {&FRAME-NAME}. /* sauvegarde si saisie d'un process op�ratoire */
    
    /************************************************************/
    /* APPELE A LA CREATION DE L'OF SEULEMENT ; EN MODIF, EN    */
    /* COPIE, OU EN CAS DE NOUVEL OF TECHNIQUE, C'EST LE        */
    /* LOCAL-ASSIGN-RECORD QUI EST UTILISE (IL                  */
    /* NE LANCE QUE CE QUI EST NECESSAIRE)                      */
    /************************************************************/
    {&Run-OFBIB-Passage-Etat-Calcule} ( Input         kof.csoc,
                                        Input         kof.cetab,
                                        Input-Output  w-prechro,
                                        Input-Output  w-chrono,
                                        Input         wcuser,
                                        Output        O-OFBIB-retour,
                                        Output        O-OFBIB-Mess).

    /* ---------------------------------------------------- MB 10/06/05 */
    /* Mise en place du param�tre d�terminant le lot sortant de l'OF    */
    /* dans le cas d'un conflit des lots pr�vus pour les sortants       */
    /* g�r� directement dans OFBIB-LANCEMENT-MULTILOT                   */
    /* => soit remise � blanc, soit lot sortant pr�vu de l'art. de r�f. */
    /* ---------------------------------------------------------------- */ 

    
    /* APO : Process op�ratoire : restauration de la valeur de process op�ratoire saisie */
    IF kof.cres <> vCres THEN
       ASSIGN Kof.cres = vCres.
       
    Find First kof Where kof.csoc       = w-csoc
                     And kof.cetab      = w-cetab
                     And kof.prechro    = w-prechro
                     And kof.chrono     = w-chrono
    No-Lock No-Error.
    If Available kof Then Do:
        If Not O-OFBIB-retour Then do:
           Assign w-crn         = 0
                  w-crt         = O-OFBIB-Mess.
                  
           {&Run-OFETAT-Etat-MODIFIE} (Input kof.csoc,kof.cetab,kof.prechro,kof.chrono,
                                       Input wcuser,
                                       Output W-Ret,
                                       Output w-Msg). 
           If w-Msg <> "":U Then 
               Assign O-OFBIB-Mess = w-msg + '|':u + O-OFBIB-Mess
                      w-crt        = w-msg + '|':u + O-OFBIB-Mess.
        End.

        If O-OFBIB-Mess <> "":U Then Do:
           {affichemsg.i  &Message="O-OFBIB-Mess" }
        End.
    End.
  End.

  kof.lot:SCREEN-VALUE IN FRAME {&FRAME-NAME} = kof.lot.
  
  Run Suppr-et-Genere-Affectation.
  
  /* MB D003368 31/07/08 */
  /* Bloc OF TECHNIQUE d�plac� dans le Local-End-Update (apr�s la g�n�ration de */
  /* l'OF technique) pour que les mvts pr�visionnels de stock de l'OF d'origine */
  /* ne soient pas supprim�s d'entr�e de jeu mais en fin de traitement          */
  /* (car la mise � jour des affectations, affect039pp.p, les utilisent)        */
  IF w-typof = {&TYPOF-OFTECH} THEN DO:
     /* MB D003458 11/06/08 */
     /* Centralisation du code de mise � jour de l'OF d'origine */
     {&RUN-OFBIB-Maj-Of-Origine} (INPUT  kof.cSocOr,
                                  INPUT  kof.cEtabOr,
                                  INPUT  kof.Prechror,
                                  INPUT  kof.Chronor,
                                  INPUT  kof.Qte[1],
                                  INPUT  kof.cUnite,
                                  INPUT  vgModifQte1,
                                  INPUT  wcUser, 
                                  OUTPUT w-Ret,
                                  OUTPUT w-Msg).
  END.
  
  Assign w-ancien-lotent = kof.lotent.      /* NLE 04/01/2005 */
  
  If w-crt <> "" Then Do:
     /* ALB 20140409_0008 Q7043 Pour g�rer le cas de l'appel 104289, il ne faut pas enlever le no-undo de tt-var */
     /* mais plut�t reg�n�rer le tt-var si n'existe plus via la proc�dure Init-OF-CR */
     /* (pour + de d�tails, voir commentaire de Marie-nono au 08/09/2011 dans appel 104289) */
     {&Run-Ofbib-Init-OF-CR} (Input kof.csoc,
                              Input kof.cetab,
                              Input kof.prechro,
                              Input kof.chrono,
                              Input wcuser).
  
    {&Run-Ofbib-Anomalie-CR} (Input w-crn, Input w-crt, Input w-texte-entete, Input w-rowid, Input Yes).
    Assign w-crn = 0
           w-crt = "".
  End.
  
  {&Run-Ofbib-Fin-CR} (Input w-texte-entete, Input w-rowid, Input Yes).
  
  If vgOrigLanct = 'OF':u And 
     Available kof
  Then Run Set-Info ('AjoutTT=':u + kof.csoc    + '|':u + 
                                    kof.cetab   + '|':u + 
                                    kof.prechro + '|':u + 
                                    String (kof.chrono),'Record-Source':U).
  Else Run Set-Info ('RowidOf=':u + W-Local-Ass-Record-Rowid ,'Record-Source':U).     
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'end-update':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  If w-oftech Then Do With Frame {&Frame-Name}:
    Assign w-oforig-prechro = ''
           w-oforig-chrono  = 0
           w-oftech         = False.

    Case w-typof:
        When {&TYPOF-OFORIG} Then 
            Assign kof.prechro:label = w-lib-oforig
                   F-QteReste:Format = {&Unite-Format-Qte} (kof.cunite, "z,zzz,zz":U)
                   F-QteReste:Screen-Value = String(Fo-QteReste())
                   F-CuReste:Screen-Value = kof.cunite
                   F-QteReste:Hidden = False
                   F-CuReste:Hidden  = False.
        When {&TYPOF-OFTECH} Then 
            Assign kof.prechro:label = w-lib-oftech
                   F-QteReste:Hidden = True
                   F-CuReste:Hidden  = True.
        Otherwise 
            Assign kof.prechro:label = w-lib-ofclas
                   F-QteReste:Hidden = True
                   F-CuReste:Hidden  = True.
    End Case.

    Run New-state("FIN-OF-TECH,Container-Source":U).
  End.

  ASSIGN w-typof-force  = No.
  
  RUN Enable-Lot.


  For Each bkot  FIELDS (ni1)
                 Where bkot.csoc    = kof.csoc
                   and bkot.cetab   = kof.cetab
                   and bkot.prechro = kof.prechro
                   and bkot.chrono  = kof.chrono
                 No-Lock :
    /* Indicateurs fab */
    {&RUN-FABIND01-GestionFabEvt}
         (INPUT {&FABEVT-C-M-OT},
          INPUT kof.csoc,
          INPUT kof.cetab,
          INPUT kof.prechro,
          INPUT kof.chrono,
          INPUT bkot.ni1,
          INPUT 0).
  END.
  
  /* CD : raz variable m�mo vgKofcresorigine */
  /* car sinon : si on copie un OF d'origine puis on cr�� directement un OF technique (sans ressortir) */
  /* l'OF technique n'est pas bien calcul� car popsubstp pas relanc� */ 
  ASSIGN vgKofCresOrigine = "":U.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize Tablejv 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
    
  /* Code placed here will execute PRIOR to standard behavior. */
  Run Get-Attribute("FCT":U).
  Assign w-FCT = Return-value.

  Run Get-Attribute("origine":u).
  Assign vgOrigLanct = Return-Value.

  Assign w-lib-oforig  = Get-TradTxt (022693, 'OF d''origine':T)
         w-lib-oftech  = Get-TradTxt (022694, 'OF technique':T)
         w-lib-ofclas  = Get-TradTxt (002591, 'OF':T)
         {&Table}.prechro:Tooltip In Frame {&Frame-Name} = string(Get-TradClair('PRECHRO':u,"OF":U,"{&LCODE-PRECHRO}":u,"{&LCLAIR-PRECHRO}":u)).

  w-typof-force = No.

  /* Possibilit� de modifications jusqu'� l'�tat Lanc� */
  /* Param�tre par famille utilisateur */  
  If w-FCT = "OFPROG":U  /* Fonction  : Programmation des OF          */
  Or w-FCT = "OFLAN":U   /* Fonction  : Lancement des OF              */
  Or w-FCT = "OFL":U     /* Fonctions : Liste des OF - Liaison CBN/OF */
  Then  
     For First zuser Fields ( cuserf )
                     Where zuser.cuser = wcuser
                     No-Lock:
        {getparii.i &CPACON    = "'OFMODIF':U" 
                    &NOCHAMP   = 1
                    &CLE       = zuser.cuserf
                    &retvaldef = "No"}
        Assign w-mod-of = vParam-OK And (vParam-Valeurs = 'Yes':U).
     End.

  
  If vgOrigLanct = 'CBNOF':u OR vgOrigLanct = 'PDPOF':u Then
    Assign w-cofuse  = {&KOF-COFUSE-PLANIF}
           w-prechro = {&PRECHRO-CBNOF}.
  Else
    Assign w-cofuse  = {&KOF-COFUSE-FAB}
           w-prechro = {&PRECHRO-OF}.

  {getparii.i &CPACON="'ACTIVITE':U" 
              &NOCHAMP=2
              &CSOC=wcsoc
              &CETAB=wcetab
  } 
  w-CacherFreinte = (vParam-Ok And vParam-Valeurs = 'Yes':u ).

  Run Pr-Gestion-Aff-Freinte.
  
  ASSIGN vgtRechTypgestres = FALSE.
  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  Run Pr-Recup-valOpt-AideArt (Output w-valOpt-AideArt).  

  ASSIGN vgLabelCresCCS = Get-TradTxt (000360, 'Centre de charge sup�rieur':T)
         vgLabelCresPOP = Get-TradTxt (029909, 'Process op�ratoires':T)
         F-CresPop = vgLabelCresPOP + ':':U
         F-CresCCS = vgLabelCresCCS + ':':U
         F-CresPop:SCREEN-VALUE = F-CresPop
         F-CresCCS:SCREEN-VALUE = F-CresCcs.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-modify-record Tablejv 
PROCEDURE local-modify-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

   /* Code placed here will execute PRIOR to standard behavior. */
   Case Kof.Cetat :
      When {&CETAT-OF-PLANIFIE} Or
      When {&CETAT-OF-LANCE} Then Do:
         {&LANCE-ofbib}.
         /* CD D001232 29/05/12 modification of commenc� */  
         If Not w-mod-of 
           /* Or ({&ofbib-Possede-Decl-GP}        (kof.csoc, kof.cetab, kof.prechro, kof.chrono))   /* d�claration en GP   */
            Or ({&ofbib-Possede-Decl-Atelier}   (kof.csoc, kof.cetab, kof.prechro, kof.chrono))   /* d�claration Atelier */ */
            Or ({&ofbib-Possede-Affect-Matiere} (kof.csoc, kof.cetab, kof.prechro, kof.chrono))   /* affectation mati�re */ 
         Then Do:
            {affichemsg.i &Message = "Get-TradMsg (002431, 'Impossible de modifier l''OF':T)
                                   + '|':U
                                   + SUBSTITUTE(Get-TradMsg (006659, 'Il existe d�j� des d�clarations r�elles sur l''OF &1.':T), kof.chrono) "}.
            Return "Adm-Error":u.
         End.
         /*  CD D001232 30/05/2012 pas de modif d'un plan avec un plan de rangement */
         IF ({&PDR-Get-cPlanr}(kof.csoc, kof.cetab, kof.prechro, kof.chrono) <> "":U) THEN DO:
            {affichemsg.i &Message = "Get-TradMsg (002431, 'Impossible de modifier l''OF':T) "}.
            Return "Adm-Error":u.            
         END.
          
      End.
   End Case.

   /* Dispatch standard ADM method.                             */
   RUN dispatch IN THIS-PROCEDURE ( INPUT 'modify-record':U ) .

   /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Test-Saisie Tablejv 
PROCEDURE Local-Test-Saisie :
/*------------------------------------------------------------------------------
  Purpose:     Tester la saisie des variables du viewer
  Parameters:  - Test Global, ou Pour un widget 
               - Handle du widget � tester.
  Notes:       
------------------------------------------------------------------------------*/
    Define Input Parameter Test-Global As Logical       No-Undo.
    Define Input Parameter Widget-Test As Widget-Handle No-Undo.

    Define Variable w-ret       As Logical      No-Undo.
    Define Variable w-mess      As Character    No-Undo.
    Define Variable w-qte       As Decimal      No-Undo.
    DEFINE VARIABLE vDateOF     AS DATE         NO-UNDO.
    DEFINE VARIABLE vHeure       AS INTEGER   NO-UNDO.

DO WITH FRAME {&FRAME-NAME} :

   /*************************** DATES ******************************/    
   If Test-Global 
     Or Widget-Test = F-Heurdeb:Handle
   Then Do :
      {admvif/method/saiheure.i Test-Saisie Kof.heurdeb F-Heurdeb yes}
      If F-Heurdeb:Screen-Value = "24:00":u And 
         Tb-Datdeb:Checked
      Then Do:
         {affichemsg.i &Message="Get-TradMsg (004890, 'L''heure de d�but ne peut pas �tre 24H.':T)" }
         Apply "Entry":U to F-heurdeb.
         Return "Adm-Error":U.
      End.
   End.

   If Test-Global
      Or Widget-Test = F-Heurfin:Handle Then Do :
      {admvif/method/saiheure.i Test-Saisie Kof.heurfin F-Heurfin yes}
   End.

   If (Test-Global
       Or Widget-Test = Kof.Datdeb:Handle) And
      Tb-Datdeb:Checked And
      Date(Kof.Datdeb:Screen-Value) = ?
   Then Do:
      {affichemsg.i &Message="Get-TradMsg (004895, 'La date d�but n''existe pas.':T)" }
      Apply "Entry":U to Kof.DatDeb.
      Return "Adm-Error":U.
   End.

   If (Test-Global
       Or Widget-Test = Kof.Datfin:Handle) And
      Tb-Datfin:Checked And
      Date(Kof.Datfin:Screen-Value) = ?
   Then Do:
      {affichemsg.i  &Message="Get-TradMsg (004898, 'La date fin n''existe pas.':T)" }
      Apply "Entry":U to Kof.DatFin.
      Return "Adm-Error":U.
   End.

   /*************************** ARTICLE ******************************/
   If (Test-Global
       Or Widget-Test = kof.carts:Handle) And
       kof.carts:Screen-Value <> "":U
   Then Do:
      { Artcc.i kof.carts yes Yes Zart.Lart }

      w-typof-force = No.

      If Not Available Buf-Zart Then Do:
        {affichemsg.i  &Message="Get-TradMsg (004900, 'Cet article n''existe pas.':T)" }
        Apply "Entry":U to kof.carts.
        Return "Adm-Error":U.
      End.

      IF Tb-DatDeb:Checked THEN
         ASSIGN vDateOF = date(Kof.DatDeb:Screen-Value).
      IF Tb-DatFin:Checked THEN
         ASSIGN vDateOF = date(Kof.DatFin:Screen-Value).

      { veriart.i "{&ARTICLE-BLANC},{&ARTICLE-FABRIQUE},{&ARTICLE-NON-GENERIQUE},{&ARTICLE-DEPOT},{&ARTFAB-UTILISABLE}" 
                 kof.carts "buf-zart" vDateOF}

      /* ??? Contr�le non bloquant pour les articles dont la validit� est � venir
         + prise en compte des dates de fabrication en priorit� */

      If Not Test-Global Then Do:
         {&Run-SITE2-Recherche-Character} (Input wcsoc,
                                           Input wcetab,
                                           Input kof.carts:Screen-Value,
                                           Input "",
                                           Input "ZARTFAB.TYPFLUXOF":U,
                                           Output w-typflux, /* NLE 12/05/04 w-typfluxOF,*/
                                           Output w-ret,
                                           Output w-mess).
         
         /* Lancement de la saisie des crit�res */   
         Run New-State ("Critere, Group-Assign-Target":U).

         /* JS (31/10/2003) : Si l'article poss�de une activit� pr�f�rentielle -> Init. du commentaire */
         /* Initialisation de la fabrication la plus plausible */
         Run recherche-fabrication(yes).


         /* lot sortant visible seulement si article g�r� par lot */
         /* NLE 03/11/2004 : �a ne d�pend pas de l'article de r�f�rence de l'OF mais de tous ses sortants */
         Run Enable-lot.    /* NLE 08/11/2004 */
         Run Enable-Lotent(w-typflux, w-typof, kof.carts:Screen-Value).
         
         If Fl-Itiact:Screen-Value ne ""
         Then Run Init-commentaire(Input Rs-iti:Screen-Value, Input Fl-Itiact:Screen-Value).
      End.

   End.

   /*************************** QTESTK2 ******************************/
   If Test-Global
      Or Widget-Test = F-qtestk2:Handle 
   Then Do:
      If Decimal(F-qtestk2:Screen-Value) = 0 Then Do:
        {affichemsg.i &Message="Get-TradMsg (004910, 'La quantit� n''a pas �t� renseign�e.':T)" }
        Apply "Entry":U to F-qtestk2.
        Return "Adm-Error":U.
      End.
      /*MP166907 20150722_0001 VH/NLE: incoh�rence du test � priori sur mauvaise association zone quantit� / zone unit�
      Contexte notamment: suite � changement � la vol�e de la 1�re unit� visible � l'�cran (soit custk2)*/
     /* { Verideci.i "Decimal(F-qtestk2:Screen-Value)"  "f-cunite1:Screen-Value" F-qtestk2 }*/
      { Verideci.i "Decimal(F-qtestk2:Screen-Value)"  "f-custk2:Screen-Value" F-qtestk2 }

      Run Pr-MAJ-qtestk.
      Run Pr-MAJ-qte1.
      RUN disable-fields. /* CD D001232 29/05/2012 modification OF commenc�s ne pas rendre la freinte saissable */ 
   End.

   /*************************** CUSTK2 ******************************/
   If Test-Global
      Or Widget-Test = f-custk2:Handle
   Then Do :
      {unitecc.i    F-custk2    yes no  " " "1" " "}

      {&Run-unite-Test-Unite-Article}
            (Input  wcsoc
           , Input  kof.carts:screen-Value
           , Input  F-custk2:Screen-Value
           , Output O-unite-retour
           , Output O-unite-Mess).

      If Not O-unite-retour Then Do:
         {affichemsg.i &Message="O-unite-Mess + '|':U + Get-Label-Folder()"}
         Apply "Entry":u to F-custk2.
         Return "Adm-Error":U.
      End.

      /* NLE 21/11/2003 */
      /* Si la qtestk2 n'a pas �t� modifi�e par l'utilisateur, convertir sa valeur actuelle dans la nouvelle unit� */

      If Not F-qtestk2:modified Then Do:
         {&Run-Unite-Calcul-Qte-Opt}
                (wcsoc
               , kof.carts:Screen-Value
               , w-old-custk2
               , f-custk2:Screen-Value
               , Decimal( F-qtestk2:Screen-Value )
               , Output w-qte
               , Output w-ret
               , Output w-mess).

         If w-ret Then F-qtestk2:Screen-Value = String(w-qte).
      End.

      /* Gestion du format des quantit�s en fonction de l'unit� */
      /* ATTENTION, cette instruction touche le modified de qtesai qui n'est
      pas forc�ment modifi�. Pas de solution en vue. NB 21/10/2003 */
      If F-custk2:Screen-Value = ''
      Then Assign F-qtestk2:Format = "z,zzz,zz9.9999":U.
      Else Assign F-qtestk2:Format = {&unite-Format-qte} (Input F-custk2:screen-value ,
                                                          Input "z,zzz,zz":U).

      Run Pr-MAJ-qtestk.
      Run Pr-MAJ-qte1.
      RUN disable-fields. /* CD D001232 29/05/2012 modification OF commenc�s ne pas rendre la freinte saissable */ 
   End.

   /*************************** TXFR ******************************/    
   If Test-Global
      Or Widget-Test = F-txfr:Handle 
   Then Do:
       Run Pr-MAJ-qte1.
   End.

   RUN Test-Saisie-Suite (Test-Global, Widget-Test).
   IF RETURN-VALUE = 'adm-error':U THEN Return "Adm-Error":u.

   RUN Test-Saisie-Suite2 (Test-Global, Widget-Test).
   IF RETURN-VALUE = 'adm-error':U THEN Return "Adm-Error":u.
   /*************************** LOT SORTANT ******************************/    
   /* OM : 20/03/2006 G7_1756 Refonte */      
   /* lancer apres le test-saisie-suite car il faut les informations prechro/chrono 
      g�n�r�es en cr�ation qd test-global dans le test-saisie-suite */
   If (Test-Global Or
       Widget-Test = kof.Lot:Handle) THEN DO:       
       RUN Test-Saisie-lot-sortant(Test-Global, Widget-Test).
       IF RETURN-VALUE = 'adm-error':U THEN Return "Adm-Error":u.
   END.       
   /* OM : 20/03/2006 G7_1756 Fin */
   
   /* CJ : 14/01/2013 M127116 on ne recharge pas l'�quipe dans le test global sinon on �crase la valeur saisie par l'utilisateur */
/*    IF Test-Global THEN                                                                                            */
/*       /* SLe 18/04/2012 Journ�e de prod d'apr�s le calendrier de la 1�re ressource �l�mentaire avec calendrier */ */
/*       RUN Test-Saisie-RecalculDatprod.                                                                            */
/*                                                                                                                   */
/*    Return.                                                                                                        */
    
END.  /* Do With Frame} */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE P-Gestion-ressources Tablejv 
PROCEDURE P-Gestion-ressources :
/*------------------------------------------------------------------------------
  Purpose: Retourne la gestion des ressources de l'itin�raire ou de l'activit�    
  Parameters:  <none>
  Notes:     1) Renseignement de vgTypgestres et vgtRechTypgestres hors Copie OF
             2) Gestion HIDDEN / Sensitive des zones F-CresPop,F-CresCCS et Kof.cres
------------------------------------------------------------------------------*/
    DEFINE VARIABLE vTypcle  AS CHARACTER NO-UNDO.
    DEFINE VARIABLE vtAucune AS LOGICAL   NO-UNDO.
    DEFINE VARIABLE vRet     AS LOGICAL   NO-UNDO.
    DEFINE VARIABLE vDate    AS DATE      NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
   IF Rs-iti:SCREEN-VALUE = '1':U THEN 
      ASSIGN vTypcle = {&KTACT-TYPCLE-ITINERAIRE}.
   ELSE
      ASSIGN vTypcle = {&KTACT-TYPCLE-ACTIVITE}.

   Assign vDate = DATE (If Tb-Datdeb:Checked 
                     Then Kof.Datdeb:Screen-Value
                     Else Kof.Datfin:Screen-Value).
   /* En copie, on ne remet pas en cause vgTypgestres, c'est celui de l'OF copi� */
   IF NOT (Adm-New-Record AND NOT Adm-adding-record) THEN DO: 
      {&RUN-ACTBIB-PROCESSGestionRessourcesDateRech} (Input  wcsoc,
                                                Input  wcetab,
                                                INPUT  vTypcle,
                                                Input  Fl-Itiact:SCREEN-VALUE,
                                                INPUT  vDate,
                                                Output vgTypgestres,
                                                Output vtAucune). 
      /* Pour tenir compte de l'historique, si rien mais que l'OF existant d�j� avec un cres,
       * c'est qu'il s'agit d'un CCS
       */
      IF NOT Adm-New-Record AND vtAucune THEN
         ASSIGN vgTypgestres = {&KOF-CSIT-GESTION-CCS}.
      ASSIGN vgtRechTypgestres = YES.                                                                     
   END.

   IF vgTypgestres = {&KOF-CSIT-GESTION-PROCESS} THEN DO:   
      ASSIGN F-CresPop:HIDDEN  = NO
             F-CresCcs:HIDDEN  = YES.
      DISPLAY F-CresPop.
   END.
   ELSE DO:
      ASSIGN F-CresPop:HIDDEN  = YES
             F-CresCcs:HIDDEN  = NO.
      DISPLAY F-CresCCS.   
   END.   
   
   /* si l'iin�raire a plusieurs activit�s dont les ressources sont g�r�es par process op�ratoires */
   /* kof.cres ne doit pas �tre saisissable */   
   IF vgTypgestres = {&KOF-CSIT-GESTION-PROCESS} AND
      vTypcle = {&KTACT-TYPCLE-ITINERAIRE}  AND
      Fl-Itiact:SCREEN-VALUE <> ""
   THEN DO:   
      {&RUN-ACTBIB-PROCESSTestNbActivitesDateRech}(INPUT  wcsoc,
                                              INPUT  wcetab,
                                              INPUT  Fl-Itiact:SCREEN-VALUE,
                                              INPUT  vDate,
                                              OUTPUT vRet).
      /*VH TANO 963 - si en cours de saisie, on a chang� le code de l'itin�raire choisir
      alors il faut activer / d�sactiver la ressource et non pas seulement l'interdire quand ce n'est pas bon!
      Ex: ITI1, sans modification possible
      ITI2 avec.
      Si par l'aide sur champ, on change de ITI1 en ITI2 il faut pouvoir autoriser la modification
      si vret � no, alors non modifiable
      si vret � oui, alors modifiable seulement si autoris� par les champs
      
      le t�moin vret indique
      Yes: il n'existe pas ou il existe un seul process op�ratoire bas�e sur une ressource, alors
      la zone ne devrait pas �tre modifiable.
      No: il existe plus d'une donc on doit laisser  la main � l'op�rateur pour le choix
      */

      ASSIGN Kof.cres:SENSITIVE    = vRet AND adm-fields-enabled
             B_AID-Cpope:SENSITIVE = vRet AND adm-fields-enabled. 

/*       IF NOT vRet THEN                      */
/*          ASSIGN Kof.cres:SENSITIVE    = NO  */
/*                 B_AID-Cpope:SENSITIVE = NO. */
   END.
   ELSE    
      ASSIGN Kof.cres:SENSITIVE    = adm-fields-enabled
             B_AID-Cpope:SENSITIVE = adm-fields-enabled.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE P-Init-Lot-Sortant Tablejv 
PROCEDURE P-Init-Lot-Sortant :
/*------------------------------------------------------------------------------
  Purpose:     Proc. d'appel � prog. sp�cifique d'initialisation de la zone 
               Lot Sortant de l'OF en fonction d'un crit�re du cahier des charges
  Parameters:  <none>
  Notes:       Cr�ation MB 11/08/05
------------------------------------------------------------------------------*/
   DEFINE INPUT-OUTPUT PARAMETER ioLotSor AS CHARACTER NO-UNDO. /* OM : 23/03/2006 M4817 Bug mise a blanc du lot sur modif de critere   OUTPUT */          

   DEFINE VARIABLE vcCC AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vRet AS LOGICAL   NO-UNDO. 
   DEFINE VARIABLE vMsg AS CHARACTER NO-UNDO. 
/* -------------------------------------------------------------------------- */

   FIND FIRST tt-criv WHERE tt-criv.cSoc  = wcSoc
                        AND tt-criv.cEtab = wcEtab
                        AND tt-criv.cCri  = "$CC":U
                      NO-LOCK NO-ERROR.
   IF AVAILABLE tt-Criv THEN ASSIGN vcCC = tt-criv.ValCmin.
                     
   RUN VALUE({&SPECIF-FO-RECHPROGSPE} (INPUT "xxinitof00pp.p":U))
         (INPUT  wcSoc,
          INPUT  wcEtab,
          INPUT  vcCC,    /* peut �tre � blanc si $CC non param�tr� au stade ou saisi � blanc */   
          INPUT-OUTPUT ioLotSor, /* retourne "" si vRet est � NO */ /* OM : 23/03/2006 M4817 Bug mise a blanc du lot sur modif de critere OUTPUT */          
          OUTPUT vRet,
          OUTPUT vMsg).
   IF NOT vRet THEN DO:
      {affichemsg.i &Message = vMsg}
      /* juste pour info et on continue */      
   END.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-Gestion-Aff-Freinte Tablejv 
PROCEDURE Pr-Gestion-Aff-Freinte :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       /* NLE 04/11/2003 */
------------------------------------------------------------------------------*/

  Do With Frame {&Frame-Name} :
     If w-CacherFreinte Then
        Assign F-txfr:Hidden     = True
               F-%:Hidden        = True.
     Else
        Assign F-qtestk:Hidden   = False
               F-cunite:Hidden   = False
               F-qte1:Hidden     = False
               F-cunite1:Hidden  = False
               F-%:Screen-Value  = '%':U.
  End.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-Gestion-Aff-qtestk Tablejv 
PROCEDURE Pr-Gestion-Aff-qtestk :
/*------------------------------------------------------------------------------
  Purpose:     si l'unit� de stock est la m�me que l'unit� de lancement, cacher la zone qtestk/cunite
  Parameters:  <none>
  Notes:       NLE 04/11/2003
------------------------------------------------------------------------------*/

   If Not w-CacherFreinte Then Do With Frame {&Frame-Name}:

      If F-custk2:Screen-Value = F-cunite:Screen-Value Or 
               F-cunite:Screen-Value = '' /* Ajout NLE 06/01/2003 */
      Then Do:
         If Not F-qtestk:Hidden Then 
            Assign F-qtestk:Hidden = True
                   F-cunite:Hidden = True.
      End.
      Else Do:
         If F-qtestk:Hidden Then
            Assign F-qtestk:Hidden = False
                   F-cunite:Hidden = False.
      End.
   End.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-MAJ-qte1 Tablejv 
PROCEDURE Pr-MAJ-qte1 :
/*------------------------------------------------------------------------------
  Purpose:     Recalcule F-qte1 en fonction de F-qtestk2, F-custk2, F-cunite et F-txfr.
  Parameters:  <none>
  Notes:       PROCEDURE IDENTIQUE MOT POUR MOT DANS XCCREAOF01JV (b�mol de ##### pr�s)
------------------------------------------------------------------------------*/  
   Define Variable w-qte   As Decimal   No-Undo.
   Define Variable w-ret   As Logical   No-Undo.
   Define Variable w-msg   As Character No-Undo.
   DEFINE VARIABLE vDatef    AS DATE      NO-UNDO.
   DEFINE VARIABLE vcArtRef  AS CHARACTER NO-UNDO. 
   DEFINE VARIABLE vcActRef  AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vQteRef   AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vtFrFixe  AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vQteLigFr AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vTauxFr   AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vQte1     AS DECIMAL   NO-UNDO. 
   DEFINE BUFFER bKacta  FOR kacta.
    
Do With Frame {&Frame-Name}:
   If F-cunite:Screen-Value = '' Then 
      Assign F-cunite:Screen-Value  = F-custk2:Screen-Value
             F-cunite1:Screen-Value = F-custk2:Screen-Value
             wstr                   = {&Unite-Format-Qte} (F-custk2:Screen-Value, "z,zzz,zz":U)
             F-qte1:Format          = wstr
             F-qtestk:Format        = wstr.
            
   If F-custk2:Screen-Value = F-cunite:Screen-Value Then
      w-qte = Decimal(F-qtestk2:Screen-Value).
   Else Do:
      /* ##### */
      /* reconvertir qtestk2 en cunite1 au lieu de prendre qtestk car pb arrondis sinon */
      {&Run-Actbib-Conv-Act-Art} (Input  wcsoc, kof.carts:Screen-Value, 
                                  Input  o-actbib-cunite1, o-actbib-qte1, o-actbib-cunite2, o-actbib-qte2,
                                  Input  Decimal(F-qtestk2:Screen-Value), F-custk2:Screen-Value, F-cunite:Screen-Value,
                                  Output w-qte, Output w-ret, Output w-msg).
   End.
        
   /* MB D002845 19/10/07 - Gestion de la freinte pour les sortants (Art.R�f. uniquement) */
   ASSIGN vTauxFr = DECIMAL(F-txfr:SCREEN-VALUE).
   
   IF w-TypFlux = {&TYPFLUX-TIRE} THEN DO:
      RUN Infos-ActiviteReference (OUTPUT vcActRef, OUTPUT vcArtRef, OUTPUT vDatef).
           
      /* Recherche des qt�s de r�f�rence et de freinte de l'article de r�f�rence */
      IF vDatef <> ? AND vcActRef <> "":U THEN
         FOR EACH bKacta FIELDS (tLigFr tqConst Qte)                        
                         WHERE bKacta.cSoc    = wcSoc 
                           AND bKacta.cEtab   = wcEtab
                           AND bKacta.TypActa = {&TYPACTA-SORTANT}
                           AND bKacta.cArt    = vcArtRef /* SLe 11/12/07 kof.cArts:SCREEN-VALUE */
                           AND bKacta.cAct    = vcActRef
                           AND bKacta.Datef   = vDatef /* i2-acta */
                         NO-LOCK:
            IF bKacta.tLigFr THEN 
               ASSIGN vtFrFixe  = bKacta.tqConst
                      vQteLigFr = bKacta.Qte.
            ELSE 
               ASSIGN vQteRef = bKacta.Qte.          
         END. /* Fin For Each bKacta */  
           
      /* Si flux tir�, il faut aussi d�duire de w-qte la qt� de la ligne */
      /* de freinte du sortant (Art.r�f) s'il y en a une                 */
      /* qte1 = w-qte - (w-qte * txfr %) - QteLigFr */
      IF vtFrFixe THEN
         ASSIGN vQte1 = (w-qte - vQteLigFr) * (100 - vTauxFr) / 100.  
      ELSE 
         ASSIGN vQte1 = w-qte * (1 - (vQteLigFr / (vQteLigfr + (vQteRef * 100 / (100 - vTauxFr))))) * (1 - (vTauxFr / 100)).        
           
      ASSIGN F-qte1:SCREEN-VALUE = STRING(vQte1)
             /* En flux tir�, le taux de freinte ne doit �tre modifiable que s'il n'y a pas  */
             /* de ligne de freinte d�finie pour l'art. de r�f�rence au niveau de l'activit� */
             F-TxFr:SENSITIVE    = (vQteLigFr = 0).
           
   END. /* Fin If w-typflux = "TIR" */
   
   ELSE  /* Si flux pouss�, on ne change rien */
         /* qte1 = w-qte - (w-qte * txfr %) */
      ASSIGN F-qte1:Screen-Value = String(w-qte * (1 - vTauxFr / 100)).
        
   /* NLE 21/01/2004 */
   If w-CacherFreinte Then
      If F-custk2:Screen-Value = F-cunite1:Screen-Value Then
         Assign F-qte1:Hidden     = True
                F-cunite1:Hidden  = True.
      Else
         Assign F-qte1:Hidden     = False
                F-cunite1:Hidden  = False.
End.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-MAJ-qtestk Tablejv 
PROCEDURE Pr-MAJ-qtestk :
/*------------------------------------------------------------------------------
  Purpose:     Recalcule F-qtestk en fonction de F-qtestk2, F-custk2 et F-cunite1.
  Parameters:  <none>
  Notes:       /* NLE 04/11/2003 */
------------------------------------------------------------------------------*/
    
    Define Variable w-qte   As Decimal      No-Undo.
    Define Variable w-ret   As Logical      No-Undo.
    Define Variable w-msg   As Character    No-Undo.
    
    
    Do With Frame {&Frame-Name}:
        If F-custk2:Screen-Value = F-cunite1:Screen-Value Then
            F-qtestk:Screen-Value = F-qtestk2:Screen-Value.
        Else Do:
            {&Run-Actbib-Conv-Act-Art} (Input  wcsoc, kof.carts:Screen-Value, 
                                        Input  o-actbib-cunite1, o-actbib-qte1, o-actbib-cunite2, o-actbib-qte2,
                                        Input  Decimal(F-qtestk2:Screen-Value), F-custk2:Screen-Value, F-cunite1:Screen-Value,
                                        Output w-qte, Output w-ret, Output w-msg).
                                 
            F-qtestk:Screen-Value = String(w-qte).
        End.
    End.

    Run Pr-Gestion-Aff-qtestk.
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-OFTECH-Qte-tire Tablejv 
PROCEDURE Pr-OFTECH-Qte-tire PRIVATE :
/*------------------------------------------------------------------------------
  Purpose:     R�cup du reste � faire sur l'OF origine (kot.qtestk) 
            mais qui correspond � kof.qte[1] en flux pouss� et kof.qtestk en flux tir�
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE INPUT-OUTPUT PARAMETER ioQte AS DECIMAL   NO-UNDO.
   
   DEFINE VARIABLE vDatef    AS DATE      NO-UNDO.
   DEFINE VARIABLE vcArtRef  AS CHARACTER NO-UNDO. 
   DEFINE VARIABLE vcActRef  AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vQteRef   AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vtFrFixe  AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vQteLigFr AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vTauxFr   AS DECIMAL   NO-UNDO.
   DEFINE VARIABLE vQteStk   AS DECIMAL   NO-UNDO.
   DEFINE BUFFER bKacta  FOR kacta.
    
Do With Frame {&Frame-Name}:
   /* MB D002845 19/10/07 - Gestion de la freinte pour les sortants (Art.R�f. uniquement) */
   ASSIGN vTauxFr = DECIMAL(F-txfr:SCREEN-VALUE).
   
   RUN Infos-ActiviteReference (OUTPUT vcActRef, OUTPUT vcArtRef, OUTPUT vDatef).

   /* Recherche des qt�s de r�f�rence et de freinte de l'article de r�f�rence */
   IF vDatef <> ? AND vcActRef <> "":U THEN
      FOR EACH bKacta FIELDS (tLigFr tqConst Qte)                        
                      WHERE bKacta.cSoc    = wcSoc 
                        AND bKacta.cEtab   = wcEtab
                        AND bKacta.TypActa = {&TYPACTA-SORTANT}
                        AND bKacta.cArt    = vcArtRef /* SLe 11/12/07 kof.cArts:SCREEN-VALUE */
                        AND bKacta.cAct    = vcActRef
                        AND bKacta.Datef   = vDatef /* i2-acta */
                      NO-LOCK:
         IF bKacta.tLigFr THEN 
            ASSIGN vtFrFixe  = bKacta.tqConst
                   vQteLigFr = bKacta.Qte.
         ELSE 
            ASSIGN vQteRef = bKacta.Qte.          
      END. /* Fin For Each bKacta */  
        
   /* Si flux tir�, il faut aussi d�duire de w-qte la qt� de la ligne */
   /* de freinte du sortant (Art.r�f) s'il y en a une                 */
   /* qte1 = w-qte - (w-qte * txfr %) - QteLigFr */
   /* On applique ici la formule "� l'envers" de Pr-MAJ-qte1 */
   IF vtFrFixe THEN  
      ASSIGN vQteStk = (ioQte / (100 - vTauxFr) * 100) + vQteLigFr.
   ELSE 
      ASSIGN vQteStk = ioQte / ((1 - (vQteLigFr / (vQteLigfr + (vQteRef * 100 / (100 - vTauxFr))))) * (1 - (vTauxFr / 100))).  
      
   ASSIGN F-qtestk:SCREEN-VALUE = STRING(vQteStk)
          F-qte1:SCREEN-VALUE   = STRING(ioQte)
          ioQte = vQteStk
          /* En flux tir�, le taux de freinte ne doit �tre modifiable que s'il n'y a pas  */
          /* de ligne de freinte d�finie pour l'art. de r�f�rence au niveau de l'activit� */
          F-TxFr:SENSITIVE      = (vQteLigFr = 0).
        
End.
    

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-Rech-cartent-cartsor Tablejv 
PROCEDURE Pr-Rech-cartent-cartsor :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       NLE 07/11/2003
------------------------------------------------------------------------------*/

    Define Input  Parameter i-cart      As Character        No-Undo.
    Define Output Parameter o-cartent   As Character        No-Undo.
    Define Output Parameter o-cartsor   As Character        No-Undo.
    Define Output Parameter o-ret       As Logical          No-Undo.
    Define Output Parameter o-msg       As Character        No-Undo.
    

    {&Run-SITE2-Recherche-Character} (Input wcsoc,
                                      Input wcetab,
                                      Input i-cart,
                                      Input "",
                                      Input "ZARTFAB.CARTENT":U,
                                      Output o-cartent,
                                      Output o-ret,
                                      Output o-msg).
    If Not o-ret Then Return.

    {&Run-SITE2-Recherche-Character} (Input wcsoc,
                                      Input wcetab,
                                      Input i-cart,
                                      Input "",
                                      Input "ZARTFAB.CARTSOR":U,
                                      Output o-cartsor,
                                      Output o-ret,
                                      Output o-msg).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-Recup-ValOpt-AideArt Tablejv 
PROCEDURE Pr-Recup-ValOpt-AideArt :
/*------------------------------------------------------------------------------
  Purpose   : R�cup�ration de la valeur utilisateur du param�tre 'ARTICLE' 
              concernant l'Aide sur les articles     
  Parameters: o-valOpt-AideArt la valeur r�cup�r�e
  Notes:       
------------------------------------------------------------------------------*/
  Define Output Parameter o-valOpt-AideArt As Logical No-Undo.

  /*
   *  R�cup�ration de la valeur utilisateur de l'option 'ARTICLE' 
   *  concernant l'Aide sur les articles
   */

   {getparii.i &CPACON="'ARTICLE':U" 
               &NOCHAMP=2
   }
   o-valOpt-AideArt = (vParam-Ok And vParam-Valeurs = 'Yes':U).
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Qte-du-lot Tablejv 
PROCEDURE Qte-du-lot :
/*------------------------------------------------------------------------------
  Purpose:     Ram�ne la liste des depot-empl-lot pour le lot, et fait le cumul
  Parameters:  
  Notes:       NLE 04/01/2005
------------------------------------------------------------------------------*/
  DEFINE INPUT  PARAMETER i-csoc      AS CHARACTER NO-UNDO.
  DEFINE INPUT  PARAMETER i-cetab     AS CHARACTER NO-UNDO.
  DEFINE INPUT  PARAMETER i-cart      AS CHARACTER NO-UNDO.
  DEFINE INPUT  PARAMETER i-lot       AS CHARACTER NO-UNDO.
  DEFINE INPUT  PARAMETER i-date      AS DATE      NO-UNDO.
  DEFINE INPUT  PARAMETER i-heure     AS INTEGER   NO-UNDO.
  DEFINE INPUT  PARAMETER i-cunite    AS CHARACTER NO-UNDO.
  Define OUTPUT Parameter Table For Tt-kcumvk.
  DEFINE OUTPUT PARAMETER o-qte       AS DECIMAL   NO-UNDO.
  Define Output Parameter o-ret       As Logical   No-Undo.
  Define Output Parameter o-msg       As CHARACTER No-Undo.

  DEFINE VARIABLE         w-qte      AS DECIMAL    NO-UNDO.

  /* liste des kcumvk */
  {&Run-StkBib-Constitution-Tt-kcumvk-Dispo} (Input  i-csoc
                                            , Input  i-cetab
                                            , Input  i-cart
                                            , INPUT  i-lot
                                            , Input  "":U               /* depot */
                                            , Input  "":U               /* emplacement */
                                            , Input  i-date
                                            , Input  i-heure
                                            , Input  Table Tt-crivVIDE
                                            , Input  "":U               /* Tri */
                                            , Input  "":U               /* Tri */
                                            , Input  "":U               /* liste de statuts */
                                            , Input  TRUE               /* TRUE = seulement stocks positifs */
                                            , Output Table Tt-kcumvk
                                            , Output o-ret
                                            , Output o-Msg).

  /* cumul des qt�s dans l'unit� demand�e */
  o-qte = 0.
  FOR EACH tt-kcumvk:
    {&Run-UNITE-Conversion-qte-opt}
                       (Input  I-csoc,
                        Input  I-cart, 
                        Input  TT-kcumvk.qug,
                        Input  TT-kcumvk.cug,
                        Input  TT-kcumvk.qus,
                        Input  TT-kcumvk.cus,
                        Input  TT-kcumvk.qut,
                        Input  TT-kcumvk.cut,
                        Input  I-cunite,
                        Output w-qte,
                        Output o-Ret,
                        Output o-Msg).

    If Not o-Ret Then Return "":u.
    o-qte = o-qte + w-qte.    
  END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info Tablejv 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.

   Define Variable w-fill-in        As widget  No-Undo.
   DEFINE VARIABLE vLotSor AS CHARACTER NO-UNDO. /* MB 11/08/05 */
   
   Case Name-Info :
       /* Ins�rer ici vos propres infos � recevoir */
       When "Position-curseur":u Then Do with frame {&frame-name} :
         Case Valeur-Info :
           When "kof.datdeb":u  Then w-fill-in = kof.datdeb:handle.
           When "kof.heurdeb":u Then w-fill-in = F-heurdeb:handle.
           When "kof.datfin":u  Then w-fill-in = kof.datfin:handle.
           When "kof.heurfin":u Then w-fill-in = F-heurfin:handle.
           When "kof.carts":u   Then w-fill-in = kof.carts:handle.
           When "kof.qte[1]":u  Then w-fill-in = f-qte1:handle.
           When "kof.citi":u    Then w-fill-in = Fl-itiAct:handle.
           When "kof.cres":u    Then w-fill-in = Kof.cres:handle.
           When "kof.lot":u     Then w-fill-in = kof.lot:handle.
           When "kof.cequip":u  Then w-fill-in = kof.cequip:handle.
         End Case.
         If w-fill-in <> ? and w-fill-in:sensitive then 
            Apply "entry":u To w-fill-in.

       End.

       When "kof.typof":u Then Do:
         ASSIGN w-typof = Valeur-Info.

         Run Enable-lot.
         Run Enable-Lotent(w-typflux, w-typof, kof.carts:SCREEN-VALUE).

         ASSIGN w-SAV-typof  = w-typof
                w-SAV-carts  = kof.carts:SCREEN-VALUE IN FRAME {&FRAME-NAME}
                w-SAV-itiact = Fl-itiact:SCREEN-VALUE IN FRAME {&FRAME-NAME}.
         RUN disable-fields. /* CD D001232 31/05/2012 le lotent ne doit pas �tre accessible si OF commenc� */ 
       End.
       
       WHEN "ModifCrit":U THEN DO:  /* MB 11/08/05 - appel� au retour du viewer des crit�res */
          IF Valeur-Info = "oui":U THEN DO:
             RUN Criteres-De-Lancement. /* Remplit la table temporaire tt-criv */
             /* ------------------------------------------------------------------------ */
             /* Appel � prog. sp�cifique d'initialisation de la zone Lot Sortant de l'OF */
             /* ------------------------------------------------------------------------ */
             /* OM : 23/03/2006 M4817 Bug mise a blanc du lot sur modif de critere */
             ASSIGN vLotsor = kof.Lot:SCREEN-VALUE.
             RUN P-Init-Lot-Sortant (INPUT-OUTPUT vLotsor).
             ASSIGN kof.Lot:SCREEN-VALUE = vLotsor.
          END.          
       END. /* Fin MB */
       /* ALB 20131024_0003 DP1253, =1 si action de Programmer l'of, =2 si action de lancer l'of */
       WHEN "Etape":U
          THEN vgEtape = INTEGER(Valeur-Info) NO-ERROR.

       Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Recherche-Article-Lot-sortant Tablejv 
PROCEDURE Recherche-Article-Lot-sortant :
/*------------------------------------------------------------------------------
  Purpose:     Recherche dans tous les sortants de l'OF qui sont g�r�s par lot
               s'ils ont la m�me r�gle de cr�ation de lot.
               Si oui retourne le code article correspondant
               Par defaut, cas d'erreur, cas non g�r� le code retourn� est Blanc
  Parameters:  <none>
  Notes:       /* OM : 20/03/2006 G7_1756 creation */
------------------------------------------------------------------------------*/
DEFINE OUTPUT PARAMETER oCart  AS CHARACTER NO-UNDO. /* Article sortant servant de reference pour trouver la regle de calcul */
DEFINE OUTPUT PARAMETER oTCtrl AS INTEGER   NO-UNDO. /* Doit on effectuer le controle ? */
  
  Define Variable vCartref      AS Character No-Undo.
  Define Variable vCartrefCfam  As Character No-Undo.
  DEFINE VARIABLE vTypeGestConflit AS INTEGER   NO-UNDO. /* Type de gestion des conflits pour le lot sortant */
  DEFINE VARIABLE vDateVigueur  AS DATE      NO-UNDO.
  DEFINE VARIABLE vCfam         AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vCrges2       AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vCrges2REF    AS CHARACTER NO-UNDO.  
  Define Variable vRet          As Logical   No-Undo.
  Define Variable vMsg          As Character No-Undo.
  DEFINE VARIABLE vTrouve       AS LOGICAL   NO-UNDO. /* A t on trouv� l'enreg ? */
  DEFINE VARIABLE vDatef        AS DATE      NO-UNDO.
  DEFINE VARIABLE vCact         AS CHARACTER NO-UNDO.   /* MB A06256 23/07/13 */
  
  DEFINE BUFFER bKota  FOR kota.
  DEFINE BUFFER bKacta FOR kacta.
  DEFINE BUFFER bKactd FOR kactd.
  DEFINE BUFFER bKitid FOR kitid.      /* MB A06256 23/07/13 */
  DEFINE BUFFER bKitidd FOR kitidd.    /* MB A06256 23/07/13 */

  /**************************************************************************/
  /* --- Lecture param OFCRE2 champs 11 Gestion conflit sur lot sortant --- */
  /**************************************************************************/
  /* Que faire si il existe des conflits entre les sortants ? */
  {&Run-SITE2-Recherche-Character} (Input wcsoc,
                                    Input wcetab,
                                    Input kof.carts:SCREEN-VALUE IN FRAME {&FRAME-NAME},
                                    Input "",
                                    Input "ZARTFAB.CARTREF":U, /* Article de reference des fiches qualites de l'article saisi */
                                    Output vCartref,
                                    Output vRet,
                                    Output vMsg).
  IF NOT vRet THEN DO:
     {affichemsg.i &message="vMsg"} /* Rare donc pas plus de gestion que ca */
  END.
  
  {&Run-SITE2-Recherche-Character} (wcsoc,      /* Societe */
                                    wcetab,     /* Etablissement */
                                    vCartref,   /* Article de reference de l'article saisi */
                                    "", "ZART.CFAM":U,
                                    Output vCartrefCfam, Output vRet, Output vMsg).
  IF NOT vRet THEN DO:
     {affichemsg.i &message="vMsg"} /* Rare donc pas plus de gestion que ca */                                            
  END.

  /* Controle t on la structure du lot ? &RETVALDEF = TRUE */
  {getparii.i &CPACON    = "'OFCRE2':U"
              &NOCHAMP   = 12
              &CLE       = vCartrefCfam
              }
  ASSIGN oTCtrl = INTEGER (vParam-Valeurs).
  IF oTCtrl = {&AUCUN-CTRL} THEN RETURN.
  
  /* Par d�faut, la zone kof.lot correspond au lot unique parmi tous les sortants */
  ASSIGN vTypeGestConflit = 0.
  /* V�rifions qu'en flux tir�, il ne prend pas la signification lot de l'art.ref */
  IF w-Typflux = {&TYPFLUX-TIRE} THEN DO:
     {getparii.i &CPACON  = "'OFCRE2':U"
                 &NOCHAMP = 11
                 &CLE     = vCartrefCfam}
     If vParam-Ok AND INTEGER(vParam-Valeurs) = 1 THEN 
        ASSIGN vTypeGestConflit = 1.
  END.
  /* --- FIN Lecture param OFCRE2 --- */
  
  CASE vTypeGestConflit:
     /* Lot sortant = Lot pr�vu de l'article de reference */   
     WHEN 1 THEN DO: 
        ASSIGN oCart = vCartref.        
     END.
     
     /* lot sortant = blanc si conflit sur la regle de generation lot entre article sortant gere par lot */     
     WHEN 0 THEN DO :
        /* 2 articles sont en conflits si la regle de generation du lot est differente pour les 2 articles */
        /* Nota: la famille peut etre differente mais pas la regle */           
        ASSIGN vCartref = '':U.
        
        /* --- En modif, Structure OF genere --- */
        IF NOT ADM-NEW-RECORD AND /* Mode modification */
           AVAILABLE kof AND kof.cetat >= {&CETAT-OF-CALCULE} THEN DO:  /* Structure de l'OF genere */
      
           For Each bKota Fields (csoc cetab cart)
                         Where bKota.csoc    = kof.csoc 
                           And bKota.cetab   = kof.cetab
                           And bKota.prechro = kof.prechro
                           And bKota.chrono  = kof.chrono
                           And bKota.typacta = {&TYPACTA-SORTANT}
                           And bKota.ni1or   = 0
                           And bKota.ni2or   = 0
                         NO-LOCK,
              First zart Fields ({&EmptyFields})
                         Where zart.csoc = bKota.csoc
                           And zart.cart = bKota.cart
                           And zart.tlot = TRUE
                         No-Lock :
                                       
              /* Chercher la regle de numerotation du lot de la famille de l'article de reference de la fiche qualite 
                 de l'article port� par kota */
              {&Run-SITE2-Recherche-Character} (Input bKota.csoc,
                                                Input bKota.cetab,
                                                Input bKota.cart,
                                                Input '':U,
                                                Input "ZARTFAB.CARTREF":U,
                                                Output vCartref,
                                                Output vRet,
                                                Output vMsg).
              If Not vRet Or vCartref = '':U Then Next.
              
              {&Run-SITE2-Recherche-Character} (Input bKota.csoc,
                                                Input bKota.cetab,
                                                Input vCartref,
                                                Input '':U,
                                                "ZART.CFAM":U,
                                                Output vCfam, Output vRet, Output vMsg).
              If Not vRet Or vCfam = '':U Then Next.
   
              {&Run-InfoFam-RechInfo} (bKota.csoc, vCfam, "crges2":U, Output vRet, Output vCrges2).            
              If Not vRet Or vCrges2 = '':U Then Next.
   
              /* Sauvegarde de la regle pour comparaison ulterieure */
              If vCrges2REF = '':U THEN ASSIGN vCrges2REF = vCrges2
                                               oCart      = vCartref.
              
              /* Comparaison des 2 regles pour determiner un conflit */
              If vCrges2REF <> vCrges2 THEN
                 ASSIGN oCart = ''.                                                      
                   
           END. /* Fin For Each bKota */
                                                                                                      
        END. /* FIN En modif, Structure OF genere */
    /*********************************/
        /* --- Structure OF NON genere (En modif ou creation) --- */
        ELSE DO: /* Mode Creation, pas encore la structure de l'OF on travaille sur les kacta donc pas de prise en compte de certains �l�ments, ex: substitution */
           /* Si activite on gere Sinon on ne sait pas faire => il faudrait exclure les articles fantomes dans les itineraires */
           /* MB A06256 23/07/13 - pour les itin�raires, contr�le de l'activit� de r�f�rence */
        /* If Rs-Iti:Screen-Value IN FRAME {&FRAME-NAME} = "2":U Then Do: */
           ASSIGN vDateVigueur = (If Tb-Datdeb:CHECKED 
                                     Then Date(Kof.Datdeb:SCREEN-VALUE)
                                     Else date(Kof.Datfin:SCREEN-VALUE)). 
           /* MB A06256 23/07/13 */
           If Rs-Iti:Screen-Value IN FRAME {&FRAME-NAME} = "2":U Then Do:    
              /* ACTIVITE */
              ASSIGN vcAct = Fl-ItiAct:SCREEN-VALUE.                          
                                     
              FOR LAST bKactd FIELDS(datef)
                              WHERE bKactd.csoc   = wcsoc
                                and bKactd.cetab  = wcetab
                                and bKactd.cact   = vcAct  /* MB A06256 23/07/13 Fl-ItiAct:SCREEN-VALUE */
                                and bKactd.datef <= vDateVigueur /* TODAY SLe 19/04/10 Q003369 */
                           NO-LOCK:
                 ASSIGN vTrouve = TRUE
                        vDatef  = bKactd.datef.
              END.
           /* MB A05256 23/07/13 */
           END.
           ELSE DO:
              /* ITINERAIRE */
              FOR LAST bKitid FIELDS(datef)
                              WHERE bKitid.csoc   = wcsoc
                                and bKitid.cetab  = wcetab
                                and bKitid.citi   = Fl-ItiAct:SCREEN-VALUE
                                and bKitid.datef <= vDateVigueur /* TODAY SLe 19/04/10 Q003369 */
                              NO-LOCK:
                 B1:
                 FOR EACH bKitidd FIELDS(csoc cetab citi datef nlig cact)
                                  WHERE bKitidd.csoc   = wcsoc
                                    and bKitidd.cetab  = wcetab
                                    and bKitidd.citi   = Fl-ItiAct:SCREEN-VALUE
                                    and bKitidd.datef  = bKitid.datef
                                  NO-LOCK
                                  BY bKitidd.csoc
                                  BY bKitidd.cetab
                                  BY bKitidd.citi
                                  BY bKitidd.datef
                                  BY bKitidd.nlig:
                    ASSIGN vcAct = bKitidd.cact.

                    /* activit� de r�f�rence = */
                    /*     flux pouss� : 1�re activit� de l'itin�raire */
                    /*     flux tir�   : derni�re activit� de l'itin�raire */
                    IF w-Typflux = {&TYPFLUX-POUSSE} THEN LEAVE B1.
                 END.

                 IF vcAct <> "":U THEN DO:
                    FOR LAST bKactd FIELDS(datef)
                                    WHERE bKactd.csoc   = wcsoc
                                      and bKactd.cetab  = wcetab
                                      and bKactd.cact   = vCact
                                      and bKactd.datef <= bKitid.datef
                                    NO-LOCK:
                       ASSIGN vTrouve = TRUE
                              vDatef  = bKactd.datef.
                    END.
                 END.
              END. /* Fin FOR EACH bKitid */
           END. /* Fin ELSE */
           /* Fin MB A06256 */
              
           IF NOT vTrouve THEN DO:
              ASSIGN oTCtrl = {&AUCUN-CTRL}. /* Erreur rare, on passe sans faire de ctrl */
              RETURN.
           END.
           
           IF vcAct <> "":U THEN DO:   /* MB A06259 23/07/13 */
              FOR EACH bKacta Fields (csoc cetab cart)                        
                            Where bKacta.csoc    = wcsoc 
                              And bKacta.cetab   = wcetab
                              And bKacta.cact    = vcAct /* MB A06259 23/07/13 Fl-ItiAct:SCREEN-VALUE */
                              And bKacta.datef   = vDatef
                              And bKacta.typacta = {&TYPACTA-SORTANT}
                            NO-LOCK,
                 First zart Fields (tgene)
                            Where zart.csoc = bKacta.csoc
                              And zart.cart = bKacta.cart
                              And zart.tlot = TRUE
                            No-Lock :
                            
                 /* Si un article sortant est generique alors on ne connait pas son code definitif 
                    donc pas de regle de numerotation / On ne sait pas gerer */
                 IF zart.tgene THEN DO:
                    ASSIGN oTCtrl = {&AUCUN-CTRL}.
                    RETURN.                
                 END.
                 
                 /* Chercher la regle de numerotation du lot de la famille de l'article de reference de la fiche qualite 
                    de l'article port� par kota */
                 {&Run-SITE2-Recherche-Character} (Input bKacta.csoc,
                                                   Input bKacta.cetab,
                                                   Input bKacta.cart,
                                                   Input '':U,
                                                   Input "ZARTFAB.CARTREF":U,
                                                   Output vCartref,
                                                   Output vRet,
                                                   Output vMsg).
                 If Not vRet Or vCartref = '':U Then Next.
                 
                 {&Run-SITE2-Recherche-Character} (Input bKacta.csoc,
                                                   Input bKacta.cetab,
                                                   Input vCartref,
                                                   Input '':U,
                                                   "ZART.CFAM":U,
                                                   Output vCfam, Output vRet, Output vMsg).
                 If Not vRet Or vCfam = '':U Then Next.
      
                 {&Run-InfoFam-RechInfo} (bKacta.csoc, vCfam, "crges2":U, Output vRet, Output vCrges2).            
                 If Not vRet Or vCrges2 = '':U Then Next.                 
                 
                 /* Sauvegarde de la regle pour comparaison ulterieure */
                 If vCrges2REF = '':U THEN ASSIGN vCrges2REF = vCrges2
                                                  oCart      = vCartref.
                 
                 /* Comparaison des 2 regles pour determiner un conflit */
                 If vCrges2REF <> vCrges2 THEN DO:
                    ASSIGN oCart = ''.                    
                 END.        
              END. /* FIn for each kacta */
           END. /* Fin IF vcAct <> "" */
           
           /* MB A06259 23/07/13 */
/*         END. /* FIN If Rs-Iti:Screen-Value */
 *         ELSE DO:
 *            /* Pas de gestion de ctrl en creation pour les itineraires et les activites ayant pour sortant au moins un article generique */
 *            ASSIGN oTCtrl = {&AUCUN-CTRL}.
 *            RETURN.
 *         END. */
        
        END. /* Fin else */
  
     END. /* Fin when 0 */
    
  END CASE.  
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Recherche-Fabrication Tablejv 
PROCEDURE Recherche-Fabrication :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       Appel� dans le Test-Saisie de l'article (pas en Test-Global)
------------------------------------------------------------------------------*/
  Define Input Parameter i-ecrase-seult-si-vide  As Logical   No-Undo.

  Define Variable W-Citi As Character No-Undo.
  Define Variable W-Cact As Character No-Undo.
  Define Variable w-ret  As Logical   No-Undo.
  Define Variable w-mess As Character No-Undo.
  
  Run Criteres-de-Lancement. /* Remplit la table temporaire tt-criv */

  Do With Frame {&frame-Name} :

     If w-typflux = "" Then Return "":U.
 
     {&Run-ActBib-Rech-Fab}
                 (wcsoc, wcetab,
                  Input  Date (If Tb-Datdeb:checked 
                               Then kof.datdeb:screen-Value
                               Else kof.datfin:screen-Value), /* Date de Calcul Saisie        */
                  Input  kof.carts:screen-value,   /* Article de r�f�rence de l'Of */
                  Input  Table Tt-Criv,        /* Crit�res de Lancement */
                  Input  w-typflux,    /* Type de Flux  */ /* NLE 12/05/04 w-typfluxOF, */

                  Input  {&APPEL-PROD},        /* Contexte activit�     */
                  Input  yes,                  /* Demande des tables temporaires avec les iti ou act trouv�s */

                  Output w-citi,               /* Itin�raire */
                  Output w-cact,               /* Activit�   */
                  
                  Output Table Tt-RechITI,     /* Tt des itin�raires trouv�s */
                  Output Table Tt-RechACT,     /* Tt des activit�s trouv�es  */

                  Output w-ret,                /* Erreur     */
                  Output W-mess).              /* Message d'erreur */

     If w-ret Then do:
        /*
         * Affichage des libell�s +
         * Informations compl�mentaires de la fab
         */
        If w-citi <> "":U Then Do:
           Assign rs-iti:screen-value    = "1":u
                  Fl-ItiAct:screen-value = W-Citi. 
           { iticc.i Fl-itiact No Yes Fl-litiact }

           Run Set-Info('itiact=':U + (If Rs-iti:screen-value = '1':U 
                                          Then 'OUI':U 
                                          Else 'NON':U)      + '|':U + 
                                      FL-itiact:Screen-Value   + '|':U + 
                                      (If Tb-Datdeb:checked 
                                          Then String(kof.datdeb:Screen-Value)
                                          Else String(kof.datfin:Screen-Value)), 
                        'Group-assign-Target':U).
           
           Run init-info-ItiAct(i-ecrase-seult-si-vide).
           { iticc.i Fl-itiact NO NO}           
        End.
        Else If w-cact <> "":U Then Do:
           Assign rs-iti:screen-value    = "2":u
                  Fl-ItiAct:screen-value = W-Cact. 
           { actcc.i Fl-itiact No Yes Fl-litiact }
          
           Run Set-Info('itiact=':U + (If Rs-iti:screen-value = '1':U 
                                          Then 'OUI':U 
                                          Else 'NON':U)      + '|':U + 
                                      FL-itiact:Screen-Value   + '|':U + 
                                      (If Tb-Datdeb:checked 
                                          Then String(kof.datdeb:Screen-Value)
                                          Else String(kof.datfin:Screen-Value)), 
                        'Group-assign-Target':U).

           Run init-info-ItiAct(i-ecrase-seult-si-vide).
           { actcc.i Fl-itiact NO NO}           
        End.          
     End.    
     Else If Fl-ItiAct:screen-value = "":U Then Do:    
        /* 
         * Initialisation plus pr�cise pour les rs-iti si aucune 
         * fabrication pr�f�rentielle n'a �t� trouv�e 
         */
        i = 0.
        For Each Tt-RechITI :
           i = i + 1.
        End.
        Rs-iti:screen-value = (If i >= 1 Then "1":u Else "2":u).
     End.
     
  End.
  
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Send-Info Tablejv 
PROCEDURE Send-Info :
/*------------------------------------------------------------------------------
  Purpose:     Envoie une info � un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Output Parameter Valeur-Info        As Character No-Undo.

   Define Variable vStade         As Character No-Undo.
   Define Variable w-date          As Character No-Undo.
   Define Variable w-heure         As Character No-Undo.
   Define Variable w-prechro-local As Character No-Undo.
   Define Variable w-chrono-local  As Character No-Undo.
   Define Variable w-ret           As Logical   No-Undo.
   Define Variable w-msg           As Character No-Undo.
   Define Variable w-lot           As Character No-Undo.
   DEFINE VARIABLE vItiAct         AS CHARACTER NO-UNDO. /* MB P2374 04/07/13 */
   
   Case Name-Info :
      /* Ins�rer ici vos propres infos � envoyer */
      When "Cle-Com":U Then 
         Valeur-Info = If Available Kof And Not Adm-New-Record 
                       Then (Kof.prechro   + ",":u + String(Kof.Chrono)) 
                       Else (w-prechro + ",":u + String(w-chrono-newOF)).

      /* NLE 29/08/2002 */
      When "Cle-CR":u Then     
          Valeur-Info = If Available Kof 
                        Then (Kof.csoc + ",":U + Kof.cetab + ",":U + Kof.prechroCR + ",":U + String(Kof.ChronoCR)) 
                        Else ?.

      When "Cle-OF":U Then 
         If Available Kof And Not Adm-New-Record Then
            Valeur-Info = (Kof.csoc    + ",":u + 
                            Kof.cetab   + ",":u + 
                            Kof.prechro + ",":u + 
                            String(Kof.Chrono)).
         ELSE
            /* OM 23/10/2002  gestion du cas nouvel OF */
            Valeur-Info = wcsoc + "|":U + wcetab + "|":U + w-prechro + "|":U + String(w-chrono-newOF).

      When "Cle-Crit":U Then 
          Assign Valeur-Info = wcsoc + "|":U +
                               wcetab + "|":U +
                               {&KCRIV-TYPCLE-OF} + "|":U +
                               (If Available {&Table} And Not Adm-New-Record Then {&Table}.prechro + ",":U + String({&Table}.chrono)
                               Else If Adm-New-Record Then w-prechro + ",":U + String(w-chrono-newOF)
                                                      Else "NOSOURCE":U).
                               
      When "Cle-Crifa":U Then Do With Frame {&Frame-Name}:      
         If kof.carts:Screen-Value <> "":U Then Do:
            If Fl-Itiact:Screen-Value = "" Then Do:
                {&Run-SITE2-Recherche-Character} (Input wcsoc,
                                                  Input wcetab,
                                                  Input kof.carts:Screen-Value,
                                                  Input "",
                                                  Input "ZARTFAB.TYPFLUXOF":U,
                                                  Output w-typflux,
                                                  Output w-ret,
                                                  Output w-msg).
            End.

            /* Sinon, w-typflux est renseign� en test-saisie sur Fl-itiact */
/* << ALB 20130719_0003 DP1713 */           
/*             Case w-typflux:                                                                                 */
/*                When {&TYPFLUX-POUSSE}                                                                       */
/*                   Then If w-typof = {&TYPOF-OFTECH}                                                         */
/*                      THEN w-lanct = 'OFTECPOU':U.                                                           */
/*                      ELSE w-lanct = 'LANCTPOU':u.                                                           */
/*                                                                                                             */
/*                When {&TYPFLUX-TIRE}                                                                         */
/*                   Then If w-typof = {&TYPOF-OFTECH}                                                         */
/*                      THEN w-lanct = 'OFTECTIR':U.                                                           */
/*                      ELSE w-lanct = 'LANCTTIR':u.                                                           */
/*                /* SLE 02/11/04 */                                                                           */
/*                When {&TYPFLUX-POUTIR} THEN DO:                                                              */
/*                   {getparii.i &CPACON  = "'ARTICLE':U"                                                      */
/*                               &NOCHAMP = 1}                                                                 */
/*                   IF vParam-Ok AND vParam-Valeurs = 'T':U THEN                                              */
/*                      ASSIGN w-lanct   = (If w-typof NE {&TYPOF-OFTECH} THEN 'LANCTTIR':u ELSE 'OFTECTIR':U) */
/*                             w-typflux = {&TYPFLUX-TIRE}. /* SLe 18/01/08 */                                 */
/*                   ELSE                                                                                      */
/*                      ASSIGN w-lanct   = (If w-typof NE {&TYPOF-OFTECH} THEN 'LANCTPOU':u ELSE 'OFTECPOU':U) */
/*                             w-typflux = {&TYPFLUX-POUSSE}. /* SLe 18/01/08 */                               */
/*                END.                                                                                         */
/*                /* --- */                                                                                    */
/*                OtherWise    w-lanct = '':u.                                                                 */
/*             End Case.                                                                                       */
            vStade = Fo-Stade
                        (Input w-typflux, 
                         Input w-typof, 
                         Input (IF AVAILABLE {&TABLE} THEN {&TABLE}.cetat ELSE "")).                        
            /* >> ALB 20131024_0003 DP1253 */
         End.       
         If vStade = '':U Then Do:
            If kof.carts:Screen-Value <> "":U then  do:
             {affichemsg.i
               &Message="Substitute(Get-TradMsg (004954, 'Vous devez renseigner un type de flux pr�f�rentiel |pour l''article &1.|':T),kof.carts:Screen-Value) +
                         Get-TradMsg (004956, '(Informations de production de l''article, onglet Fabrication)|':T) +
                         Get-TradMsg (004959, 'Par d�faut, les recherches d''informations se basant sur cette information|consid�rent le flux en pouss�.':T)"
             }
            End.
            ASSIGN vStade = 'LANCTPOU':u
                   w-typflux = {&TYPFLUX-POUSSE}. /* SLe 18/01/08 */

         End.
         Assign Valeur-Info = kof.carts:Screen-Value + "�":U +
                              vStade + "�":U +
                              (If Tb-Datdeb:Checked
                                 Then kof.datdeb:Screen-Value
                                 Else kof.datfin:Screen-Value) + "�":U +
                              Fo-CleComp(vStade).  /* << ALB 20130719_0003 DP1713 */
      End.                        
      
      When "Init-Crifa":U Then Do:
         Assign Valeur-Info = /* String(kof.carts:Sensitive In Frame {&Frame-Name},"Oui/Non":U) + "|":U + */ 
                              "Oui":U + "|":U +    /* MB G7_1589 22/09/05 - Toujours OUI */
                              (If Available {&Table} And Not Adm-New-Record
                               Then {&Table}.prechro + ",":U + String({&Table}.chrono)
                               Else w-prechro + ",":U + String(w-chrono-newOF)).

      End.
      
      When "Info-Acte":U Then Do With Frame {&Frame-Name} :
         If Tb-Datdeb:Checked In Frame {&Frame-Name}
         Then Do:
            {admvif/method/saiheure.i Assign i F-heurdeb yes}
            Assign w-date = kof.datdeb:Screen-Value In Frame {&Frame-Name}
                   w-heure = String(i).
         End.
         Else Do:
            If F-heurfin:Screen-Value In Frame {&Frame-Name} <> "":U Then Do:
              {admvif/method/saiheure.i Assign i F-heurfin yes}
            End.
            Else i = 0.

            Assign w-date = kof.datfin:Screen-Value In Frame {&Frame-Name}
                   w-heure = String(i).
         End.
         
         /* OM 23/10/2002 Gestion de add et copy d'OF */
         If Adm-New-Record Or Not Available kof
         Then Assign w-prechro-local = w-prechro /* SLE 18/10/02 "":u */
                     w-chrono-local  = String(w-chrono-newOF)  /*"":u*/.
         Else Assign w-prechro-local = kof.prechro
                     w-chrono-local  = String ( kof.chrono ).
         /* OM 23/10/2002 */
   
         w-lot = kof.lotent:Screen-Value In Frame {&Frame-Name}.
         
         IF Rs-iti:SCREEN-VALUE = ? THEN 
            ASSIGN vItiAct = "":U. /* MB P2374 04/07/13 */
         
         Do With Frame {&Frame-Name}:
            Assign Valeur-Info =  "":U        + '|':u + /* cnatie    */
                                  "":U        + '|':u + /* ctie      */
                                  "":U        + '|':u + /* Qte 1    */
                                  "":U        + '|':u + /* U1       */
                                  "":U        + '|':u + /* Qte 2    */
                                  "":U        + '|':u + /* U2       */
                                  "":U        + '|':u + /* Qte 3    */
                                  "":U        + '|':u + /* U3       */
                                  w-date      + '|':U + /* Date mvt  */
                                  w-heure     + '|':U + /* Heure mvt */
                                  w-lot       + '|':U + /* Lot       */
                                  kof.carts:Screen-Value + '|':U + /* Article   */
                                  "":U        + '|':u + /* Transport.*/
                                  w-date      + '|':u + /* Date prev mvt  */
                                  w-heure     + '|':u + /* Heure prev mvt */
                                  w-prechro-local   + '|':u + /* kof.prechro */
                                  w-chrono-local    + '|':u + /* kof.chrono */
                                  ""          + '|':u + /* conditionnement */
                                  /* Elt n�19 - Cl� & infos de l'acte */
                                  wcsoc + '�':u + wcetab + '�':u + 
                                    w-prechro-local  + '�':u + w-chrono-local  + '���':u + 
                                    wcsoc + '�':u + wcetab + '�':u +                                    /* NLE 05/10/2004 */
                                    w-oforig-prechro + '�':u + String(w-oforig-chrono) + '�':u +        /* NLE 05/10/2004 */
                                    Kof.cres:Screen-Value  + '�':u + Fl-itiact:Screen-Value + '�':u +   /* NLE 05/10/2004 */
                                    kof.lot:Screen-Value + '�':u + kof.lotent:Screen-Value +            /* NLE 05/10/2004 */
                                    '�':u + w-typof +                                      /* SLE 17/03/2005 elt 15 de cleOF */
                                    '�':U + vItiAct +                    /* MB P2374 20130418_0002 elt 16 de CleOF : Rs-Iti */
                                    /****/
                                    '||':U + 
                                    '�':u + "kcriv":U + '�':u + {&KCRIV-TYPCLE-OF} + '�':u + String(w-oforig-prechro) + ",":U + String(w-oforig-chrono) + /* << ALB 20130719_0003 DP1713 Renvoyer la cl� origine pour initialiser avec les valeurs attendues */
                                    /****/
                                  Fill('|':U, 9).                  
         End.
       End.
      
      Otherwise 
          /* Par d�faut, envoi de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Send-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Output Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records Tablejv  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kof"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed Tablejv 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.
 
  DEFINE VARIABLE vCriHdl AS HANDLE    NO-UNDO.
  DEFINE VARIABLE vStade  AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vCfmact AS CHARACTER NO-UNDO.
  
  CASE p-state:
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */

      When "FinDisplay":U Then Do :
         Run Get-Info ("Liste-Criteres":U, "Criteres-Target":U).
         Assign w-LstCrit-OLD = Return-Value.
      End.
      When "NEW-OF-TECH":U Then Do:
        If Available kof Then Do With Frame {&Frame-Name}:
            Assign w-oforig-prechro  = kof.prechro
                   w-oforig-chrono   = kof.chrono
                   F-QteReste:Hidden = True
                   F-CuReste:Hidden  = True
                   w-typof           = {&TYPOF-OFTECH}
                   w-typof-force     = No
                   w-oftech          = True.
                /*   vgOfOrigLotEnt    = kof.lotent. /* MB D003368 28/04/08 */*/

            Run New-State("OFTECH,Group-assign-Target":U).
                   
            Run Dispatch ("copy-Record":U).

            Run Enable-Lotent(w-typflux, w-typof, kof.carts:Screen-Value).
            /* << ALB 20130719_0003 DP1713 Quand cr�ation d'un OF technique par grignotage, on lance les crit�res, car nouveaux stades pour les OF techniques */
            Run New-State ("Critere, Group-Assign-Target":U).
        End.
      End.
      WHEN "Recup-Criteres":U THEN DO:
         /* << ALB 20130719_0003 DP1713 Si les crit�res pr�sents � l'ent�te de l'of ne sont pas sur le stade Lancement, les rendre non saisissables */
         vStade = Fo-Stade
               (Input w-typflux,
                Input w-typof,
                Input (IF AVAILABLE {&TABLE} THEN {&TABLE}.cetat ELSE "")).
         IF vStade = "LANCTEVT":U OR vStade = "PROGEVT":U OR vStade = "LANCTEVP":U OR vStade = "PROGEVP":U THEN DO:  /* ALB 20141020_0004 Q007539 */
            Run Get-Info ("criHdl":u,'info3-Source':U).
            Assign vCriHdl = WIDGET-HANDLE(Return-Value).
            IF VALID-HANDLE(vCriHdl) THEN DO:
               Case Rs-Iti:Screen-Value :
                  When "1":u Then Do: /* Itin�raire */
                     {&Run-InfoAct-RechInfo} (wcsoc, wcetab, vgCactRefIti, "cfmact":U, Output w-ret, Output vCfmact).   /* ALB 20140911_0006 Q7437 */
                  End.
                  When "2":u Then Do: /* Activit� */
                     {&Run-InfoAct-RechInfo} (wcsoc, wcetab, Fl-itiact:screen-value, "cfmact":U, Output w-ret, Output vCfmact).
                  End.
               End Case.
               RUN get-Tt-criv IN vCriHdl (OUTPUT TABLE Tt-criv).
               FOR EACH Tt-criv:
                  IF NOT {&Stade-CritExistence}
                        (Input wcsoc,
                         Input wcetab,
                         Input kof.carts:Screen-Value,
                         Input vStade,
                         Input (IF Tb-Datdeb:CHECKED
                                   THEN DATE(kof.datdeb:SCREEN-VALUE)
                                   ELSE DATE(kof.datfin:SCREEN-VALUE)),
                         INPUT (vCfmact + ","),
                         Input Tt-criv.ccri)
                     THEN Tt-criv.tsaimodif = FALSE.
               END.
               RUN Set-Tt-criv IN vCriHdl (INPUT TABLE Tt-criv).
            END.
         END.

      END.
      /* << ALB 20130719_0003 DP1713 Derni�re �tape, mise � jour des crit�res qui viennent d'�tre saisis et r�initialisation de l'�tape */
      WHEN "ApCritere":U THEN do:
         RUN dispatch IN THIS-PROCEDURE ( INPUT 'update-record':U ) .
         /* On revient � l'�tape standard */
         vgEtape = 0.
      END.
      /* >> ALB 20131024_0003 DP1253 */
      {admvif/template/vinclude.i "vstates"}
  END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Suppr-et-Genere-Affectation Tablejv 
PROCEDURE Suppr-et-Genere-Affectation :
/*------------------------------------------------------------------------------
  Purpose:     G�n�re une ou plusieurs affectations du lot entrant sur l'OF
  Parameters:  
  Notes:       NLE 30/08/2004
------------------------------------------------------------------------------*/
    DEFINE VARIABLE vReste-A-Aff AS DECIMAL   NO-UNDO.
    DEFINE VARIABLE vDate        AS DATE      NO-UNDO.
    DEFINE VARIABLE vHeure       AS INTEGER   NO-UNDO.
    DEFINE VARIABLE vRet         AS LOGICAL   NO-UNDO.
    DEFINE VARIABLE vMsg         AS CHARACTER NO-UNDO.
    /* MB D003368 20/08/08 */
    DEFINE VARIABLE vQte-A-Aff AS DECIMAL   NO-UNDO.
                           
    DEFINE BUFFER b-kaffect FOR kaffect.
    /* MB D003368 20/08/08 */
    DEFINE BUFFER bKot      FOR kot.
/* -------------------------------------------------------------------------- */
   
   /* -------------------------------------------------- */
   /* Suppression des �ventuelles anciennes affectations */
   /* -------------------------------------------------- */
   If w-ancien-lotent <> '' Then
      For Each b-kaffect Where b-kaffect.typdes     = {&KAFFECT-TYP-FAB}
                           And b-kaffect.csocdes    = wcsoc
                           And b-kaffect.cetabdes   = wcetab
                           And b-kaffect.prechrodes = kof.prechro
                           And b-kaffect.chronodes  = kof.chrono
                           And b-kaffect.ni1des     = kof.ni1artref
                           And b-kaffect.ni2des     = kof.ni2artref
                           And b-kaffect.typor      = {&KAFFECT-TYP-STK}
                           And b-kaffect.csocor     = wcsoc
                           And b-kaffect.cetabor    = wcetab
                           And b-kaffect.cartor     = kof.carts
                           And b-kaffect.lotor      = w-ancien-lotent
                         No-Lock:

         {&Run-Affect-Sup-Affect}  ( Input  b-kaffect.typor,
                                     Input  b-kaffect.csocor,
                                     Input  b-kaffect.cetabor,
                                     Input  b-kaffect.cartor,
                                     Input  b-kaffect.lotor,
                                     Input  b-kaffect.prechror,
                                     Input  b-kaffect.chronor,
                                     Input  b-kaffect.ni1or,
                                     Input  b-kaffect.ni2or,
                                     Input  b-kaffect.nligdes,
                                     Input  b-kaffect.cart,
                                     Input  wcuser,
                                     Input  Yes,     /* majstk */
                                     Output w-ret,
                                     Output w-msg).
         If Not w-ret Then Do:
            {affichemsg.i       &Message = w-msg}
            RETURN "":U.
         End.
      End.

   /* -------------------------------------------------------------------------------- */
   /* On affecte 1 ou plusieurs depot-empl-lot entrant (origine) � un OF (destination) */
   /* -------------------------------------------------------------------------------- */
   IF kof.LotEnt NE "":U THEN DO:
      ASSIGN vDate = (IF kof.DatDeb = ? 
                         THEN kof.DatFin ELSE kof.DatDeb).
        
      IF kof.DatDeb = ? THEN DO:
         {admvif/method/saiheure.i ASSIGN vHeure F-HeurFin YES}
      END.
      ELSE DO:
         {admvif/method/saiheure.i ASSIGN vHeure F-HeurDeb YES}
      END.
      
      /* MB D003368 20/08/08 */
      /* Gestion du lot entrant sur les O.F. d'origine */
      /* En cas de modification d'un OF d'origine, la qt� � affecter ne doit pas   */
      /* �tre la qt� de l'OF mais la qt� r�siduelle de l'OF, port�e par kot.qte[1] */
      IF NOT Adm-New-Record AND w-typof = {&TYPOF-OFORIG} THEN 
         FOR FIRST bKot FIELDS (qte[1])
                        WHERE bKot.csoc    = kof.csoc
                          AND bKot.cetab   = kof.cetab
                          AND bKot.prechro = kof.prechro
                          AND bKot.chrono  = kof.chrono
                          AND bKot.ni1     = kof.ni1artref
                        NO-LOCK:
             ASSIGN vQte-A-Aff = bKot.qte[1].
         END.
      ELSE 
         ASSIGN vQte-A-Aff = kof.QteStk.
      /* Fin MB D003368 */
     
      {&Run-AFFECT-Genere-Affectation-OF}
                  (INPUT  wcSoc,
                   INPUT  wcEtab,
                   INPUT  wcUser,
                   INPUT  vDate,
                   INPUT  vHeure,
                   INPUT  kof.Prechro,
                   INPUT  kof.Chrono,
                   INPUT  kof.Ni1ArtRef,
                   INPUT  kof.Ni2ArtRef,
                   INPUT  kof.cArts,
                   INPUT  kof.LotEnt,
                   INPUT  vQte-A-Aff, /*kof.QteStk,*/ /* MB D003368 20/01/08 */
                   INPUT  kof.cUnite,
                   INPUT vgModifQte1, /* MB D003368 01/01/08 */
                   INPUT TABLE TT-CrivVide,
                   OUTPUT vReste-A-Aff,
                   OUTPUT vRet,
                   OUTPUT vMsg).       /* affect039pp.p */
      IF NOT vRet THEN DO:
         {affichemsg.i &Message = "vMsg"}
         RETURN "":U.
      END.
      
     {affichemsg.i  &Message = "Substitute(Get-TradMsg (022723, 'Affectation g�n�r�e du lot &1 sur l''OF &2 pour &3 &4.':T), 
                                 kof.LotEnt, kof.Chrono, /*kof.QteStk*/ vQte-A-Aff - vReste-A-Aff, kof.cUnite)"} 
   End. /* Fin If kof.lotEnt Ne "":U */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Test-Saisie-lot-sortant Tablejv 
PROCEDURE Test-Saisie-lot-sortant :
/*------------------------------------------------------------------------------
  Purpose:     Test sasie du lot sortant (Kof.lot)
  Parameters:  <none>
  Notes:       /* OM : 20/03/2006 G7_1756 Creation */
------------------------------------------------------------------------------*/
   Define Input Parameter Test-Global As Logical       No-Undo.
   Define Input Parameter Widget-Test As Widget-Handle No-Undo.

   Define Variable vCart        As Character    No-Undo.
   Define Variable vDate        As Character    No-Undo.
   Define Variable vHeure       As Character    No-Undo.
   DEFINE VARIABLE vLstInfosLot AS CHARACTER NO-UNDO.

   DEFINE VARIABLE vZartCfam   AS CHARACTER NO-UNDO. /* Famille elementaire de l'article de reference */
   DEFINE VARIABLE vZfamCrges2 AS CHARACTER NO-UNDO. /* Regle de numerotation de lot lie a la famille ci-dessus */
   DEFINE VARIABLE vRet        AS LOGICAL   NO-UNDO. /* variable de retour */
   DEFINE VARIABLE vMsg        AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vLot        AS CHARACTER NO-UNDO. /* Lot saisie */
   DEFINE VARIABLE vTCtrl      AS INTEGER   NO-UNDO. /* Doit on effectuer le controle ? */
   DEFINE BUFFER   bZrges      FOR Zrges.            /* Regle de gestion porte sur la regle de numeratation des lots de l'article */

   IF Test-global AND /* Controle qd test global car il faut toutes les informations (ex: PRechro/chrono) */
      kof.Lot:SCREEN-VALUE IN FRAME {&FRAME-NAME} <> "" THEN DO:  /* OM : 20/03/2006 G7_1756 un code lot a ete saisi ou genere ulterieurement (si pas de saisie, pas de test) */

      /* Controle du lot eventuellement saisi par rapport � la structure �nonc�e par la regle 
         de numerotation de lot renseign�e a la famille de l'article de reference */               
      RUN Recherche-Article-Lot-Sortant(OUTPUT vCart, OUTPUT vTCtrl).
      IF vTCtrl = {&AUCUN-CTRL} THEN RETURN. 
      
      Assign vLot = kof.Lot:SCREEN-VALUE IN FRAME {&FRAME-NAME}.
      
      IF vCart <> "" THEN DO:      
         {&Run-SITE2-Recherche-Character} (wcsoc,  /* Societe */
                                           wcetab, /* Etablissement */
                                           vCart,  /* Article (Attention, calcul dans le lcode pas tjrs kof.carts) */
                                           "", "ZART.CFAM":U,
                                           Output vZartCfam, Output vRet, Output vMsg).
         IF NOT vRet THEN DO:
            {affichemsg.i &message="vMsg"} /* Rare donc pas plus de gestion que ca */
         END.
         
         {&Run-InfoFam-RechInfo} (wcsoc, vZartCfam, "crges2":U, Output vRet, Output vZfamCrges2).            

         FOR FIRST bZrges FIELDS(crges pgm)
                          WHERE bZrges.crges = vZfamCrges2
                            AND bZrges.pgm   <> ""
                          NO-LOCK:
            /****************************************************/
            /*     Constitution de la liste des infos du lots   */
            /****************************************************/
            If Tb-Datdeb:Checked In Frame {&Frame-Name} Then
               Assign vDate  = Kof.Datdeb:Screen-Value
                      vHeure = F-Heurdeb:Screen-Value.
            Else
               Assign vDate  = Kof.Datfin:Screen-Value
                      vHeure = F-Heurfin:Screen-Value.
                     
            ASSIGN vLstInfosLot = wcsoc  + ",":u +
                                  wcetab + ",":u +
                                  vCart  + ",":u +  /* Article: parfois a blanc !!! */
                                  "":U   + ",":u +  /* Variante */
                                  "":U   + ",":u +  /* D�p�t    */
                                  kof.prechro:SCREEN-VALUE + "|":U + kof.chrono:SCREEN-VALUE + ",,,":u +  /* Renseigner en creation dans test-saisie-suite en test-global */
                                  vDate + '|':U + vHeure.
            /* ---- */

            /* Controle par la regle */
            Run Value(bZrges.pgm) ({&STKLOT-LOT-CONTROLE}, /* On souhaite controler la saisie par rapport a la regle de numerotation */
                                  Input        {&FCT-SAISIE-OF},
                                  Input        vLstInfosLot,
                                  Input-Output vLot,
                                  Output vRet).
         END. /* FOR FIRST bZrges */
         
      END. /* Fin si Code Article */
      ELSE
         ASSIGN vRet = FALSE.
      
      If NOT vRet THEN DO:
         {affichemsg.i &message="Get-TradMsg (004915, 'Num�ro du lot ne correspondant pas �|sa num�rotation automatique.':T)"}
         IF vTCtrl = {&CTRL-BLOQUANT} THEN DO:
            APPLY "ENTRY":U TO kof.lot.
            RETURN "Adm-Error":U.         
         END.
      END.
   End. /* Fin LOT Sortant */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Test-Saisie-Suite Tablejv 
PROCEDURE Test-Saisie-Suite :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input Parameter Test-Global As Logical       No-Undo.
   Define Input Parameter Widget-Test As Widget-Handle No-Undo.

   DEFINE VARIABLE vDatef   AS DATE      NO-UNDO.
   DEFINE VARIABLE vHeure   AS INTEGER   NO-UNDO.
   Define Variable vQte     AS DECIMAL   NO-UNDO.
   Define Variable vQte-old AS DECIMAL   NO-UNDO.   
   DEFINE VARIABLE vRet     AS LOGICAL   NO-UNDO.
   Define Variable vMsg     AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vQteDispo  AS DECIMAL   NO-UNDO. /* Qt� maxi pour l'OF */   
   DEFINE VARIABLE vQteOFOrig AS DECIMAL   NO-UNDO. /* Affectation restante du lot sur l'OF origine */
        
DO WITH FRAME {&FRAME-NAME} :

   /*************************** LOT ENTRANT ******************************/    
   If (Test-Global Or
       Widget-Test = kof.Lotent:Handle) AND
       kof.lotent:screen-value <> ''
   Then Do:
      Assign w-lotent = kof.lotent:SCREEN-VALUE
             vDatef  = If Date(kof.datdeb:Screen-Value) = ? Then Date(kof.datfin:Screen-Value) Else Date(kof.datdeb:Screen-Value).

      If Date(kof.datdeb:Screen-Value) = ? Then Do:
         {admvif/method/saiheure.i Assign vHeure F-heurfin yes}
      End.
      Else Do:
         {admvif/method/saiheure.i Assign vHeure F-heurdeb yes}
      End.

      /* Ram�ne la liste des depot-empl-lot pour le lot, et fait le cumul */
      RUN Qte-du-lot(INPUT  wcsoc,
                     INPUT  wcetab,
                     INPUT  kof.carts:SCREEN-VALUE,
                     INPUT  kof.lotent:SCREEN-VALUE,
                     INPUT  vDatef,
                     INPUT  vHeure,
                     INPUT  F-custk2:SCREEN-VALUE,
                     OUTPUT TABLE tt-kcumvk,
                     OUTPUT vQte,
                     OUTPUT vRet,
                     OUTPUT vMsg).
      If NOT vRet then do:
         {affichemsg.i &Message="vMsg" }
         Apply "Entry":U to kof.lotent.
         Return "Adm-Error":U.
      End.
      
      /* la quantit� du lot �crase-t-elle la qt� � lancer de l'OF ? */
      If NOT test-global AND Fo-Qte-Lotent-Prioritaire(kof.carts:screen-value) Then Do:
         Assign F-qtestk2:Format       = {&unite-Format-qte} (Input F-custk2:SCREEN-VALUE, Input "z,zzz,zz":U)
                F-qtestk2:Screen-Value = String(Max(0,vQte))
                F-custk2:Screen-Value  = F-custk2:SCREEN-VALUE.

         Run Local-Test-Saisie(No, F-qtestk2:Handle).
         If Return-Value = "Adm-Error":U Then 
            Apply "Entry":U to F-qtestk2.
         Else Do:
            Run Local-Test-Saisie(No, F-custk2:Handle).
            If Return-Value = "Adm-Error":U Then 
                  Apply "Entry":U to F-custk2.
         End.
      END.

      IF test-global THEN DO:
          
         Assign vQte-old = 0.
           
         /* Si on est en modification d'OF ou en cr�ation d'OF technique :         */ 
         /* recherche de la quantit� actuellement affect�e du lot sur l'OF afin de */
         /* la retrancher lors du test de quantit� suffisante restant sur le lot   */
         /* (ainsi que la qt� actuellement affect�e du lot � l'OF d'origine)       */
          
         IF (NOT Adm-new-record OR w-typof = {&TYPOF-OFTECH} /* MB D003368 21/08/08 */ ) 
             AND AVAILABLE kof 
         THEN DO:
            {&Run-AFFECT-Qte-Aff-OF-Lot}( INPUT  'LOT':U
                                          , INPUT  kof.csoc     
                                          , INPUT  kof.cetab    
                                          , INPUT  kof.carts:SCREEN-VALUE     
                                          , INPUT  kof.lotent:SCREEN-VALUE
                                            /* MB D003368 21/08/08 - Ajout chrono OF origine */
                                          , INPUT  (IF w-typof <> {&TYPOF-OFTECH}
                                                       THEN 0 
                                                       ELSE IF NOT Adm-New-record
                                                               THEN kof.Chronor 
                                                               ELSE w-oforig-chrono)
                                          , INPUT  'FAB':U
                                          , INPUT  kof.csoc
                                          , INPUT  kof.cetab
                                          , INPUT  kof.prechro
                                          , INPUT  kof.chrono
                                          , INPUT  kof.ni1artref
                                          , INPUT  kof.ni2artref
                                          , INPUT  F-custk2:SCREEN-VALUE
                                          , OUTPUT vQte-old
                                            /* MB D003368 21/08/08 - Ajout Qt� r�siduelle de l'OF origine */
                                          , OUTPUT vQteOFOrig  
                                          , OUTPUT vRet        
                                          , OUTPUT vMsg). /* affect037pp.p */
            IF NOT vRet THEN DO:
               {affichemsg.i &Message="vMsg" }
               Apply "Entry":U to kof.lotent.
               Return "Adm-Error":U.
            END.
         END.
      /*  /* MB D003368 01/08/08 */
         /* Si modification (ou copie) OF technique portant un lot entrant ou modification   */
         /* OF origine, alors initialisation du lot entrant de l'OF d'origine s'il y en a un */
         /* (car en cr�ation, il est initialis� dans le State-Changed "NEW-OF-TECH")         */
          IF vgOfOrigLotEnt = "":U THEN DO:
             CASE w-typof:
                WHEN {&TYPOF-OFTECH} THEN
                   ASSIGN vPrechro = kof.Prechror
                          vChrono  = kof.Chronor.
                WHEN {&TYPOF-OFORIG} THEN
                   IF NOT Adm-New-Record THEN 
                      ASSIGN vPrechro = kof.Prechro
                             vChrono  = kof.Chrono.
             END CASE.
             IF vChrono <> 0 THEN
                FOR FIRST bKof FIELDS (Lotent)
                                WHERE bKof.cSoc    = wcSoc
                                  AND bKof.cEtab   = wcEtab
                                  AND bKof.Prechro = vPrechro
                                  AND bKof.Chrono  = vChrono
                                NO-LOCK:  
                   ASSIGN vgOfOrigLotEnt = bKof.LotEnt.                 
                END.                   
          END.                   
        */  
         /* Si OF pas OF technique  */
         /*   et si qt� disponible en stock + ancienne qt� affect�e � l'OF < nouvelle qt� saisie */
         /* OU                      */
         /* Si OF technique         */
         /*   et    si cr�ation     */
         /*           et qt� disponible en stock + qt� du lot affect�e � l'OF d'origine < nouvelle qt� saisie */
         /*      ou si modification */
         /*           et qt� disponible en stock + ancienne qt� affect�e � l'OF + qt� du lot affect�e � l'OF d'origine < nouvelle qt� saisie */
         /* alors on bloque         */  
          
         ASSIGN vQteDispo = (IF w-Typof <> {&TYPOF-OFTECH} 
                                THEN vQte + vQte-old
                             ELSE IF Adm-New-Record
                                THEN vQte + vQteOFOrig
                             ELSE    vQte + vQte-old + vQteOFOrig).
          
         /* si quantit� disponible + ancienne quantit� < nouvelle quantit� alors KO */
       /* IF vQte + vQte-old < DECIMAL(F-qtestk2:SCREEN-VALUE) THEN DO: */
          
         IF vQteDispo < DECIMAL(F-qtestk2:SCREEN-VALUE) THEN DO:
          /* Fin MB D003368 */
            {affichemsg.i &Message = "Substitute(Get-TradMsg (022758, 'Il n''y a pas assez de quantit� sur le lot &1 pour l''affecter � cet OF (il reste &2 &3), ou bien il n''existe pas.':T), 
                                       kof.lotent:screen-value, 
                                       vQteDispo, F-custk2:screen-value )"}
            Apply "Entry":U to kof.lotent.
            Return "Adm-Error":u.
         End.
      END.
   End.
END.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Test-Saisie-Suite2 Tablejv 
PROCEDURE Test-Saisie-Suite2 :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input Parameter Test-Global As Logical       No-Undo.
   Define Input Parameter Widget-Test As Widget-Handle No-Undo.
   
   DEFINE VARIABLE vDatef        AS DATE      NO-UNDO.
   DEFINE VARIABLE vDateVigueur  AS DATE      NO-UNDO.
   Define Variable Wcartiti AS CHARACTER NO-UNDO.
   Define Variable vCunite  AS CHARACTER NO-UNDO.
   Define Variable vRet     As LOGICAL   No-Undo.
   Define Variable vCarts   AS CHARACTER NO-UNDO.
   Define Variable vMsg     AS CHARACTER NO-UNDO.
   Define Variable vCartent AS CHARACTER NO-UNDO.
   Define Variable vCartsor AS CHARACTER NO-UNDO.
   DEFINE BUFFER bKactd    FOR Kactd.
   DEFINE BUFFER bKactdpop FOR Kactdpop.
   
DO WITH FRAME {&FRAME-NAME} :
   /*************************** ITIACT ******************************/    
   ASSIGN vDateVigueur = (If Tb-Datdeb:CHECKED  
                   Then Date(Kof.Datdeb:SCREEN-VALUE)
                   Else date(Kof.Datfin:SCREEN-VALUE)). /* SLe 19/04/10 Q003369 */
   If Test-global OR Widget-Test = Fl-itiact:Handle THEN DO:           
        RUN P-Gestion-ressources.
        Run Set-Info('itiact=':U + (If Rs-iti:screen-value = '1':U 
                                       Then 'OUI':U 
                                       Else 'NON':U)      + '|':U + 
                                   FL-itiact:Screen-Value   + '|':U + 
                                   (If Tb-Datdeb:checked 
                                       Then String(kof.datdeb:Screen-Value)
                                       Else String(kof.datfin:Screen-Value)), 
                     'Group-assign-Target':U).                     
   END.
   
   RUN Criteres-De-Lancement. /* CHU 20100208_0015 - report correction AM066416 - 
   rafraichissement des crit�res avant les contr�les itin�raire et activit�, pour �viter un message d'erreur en cr�ation d'OF */

   /* Contr�le de l'itin�raire en fonction de la date, de l'article et de la formule. */
   If (Test-Global 
       Or Widget-Test = Fl-itiact:Handle) And
      (Fl-itiact:Sensitive Or 
       w-datedeb-Avant-Modif <> kof.datdeb:Screen-value Or
       w-datefin-Avant-Modif <> kof.datfin:Screen-value )
   Then Do:

      If Test-Global And
         Fl-itiact:Screen-Value = "":U 
      Then Do:
         {affichemsg.i &Message="Get-TradMsg (004920, 'La zone activit� ou itin�raire n''a pas �t� renseign�e.':T)" }
         Apply "Entry":U to Fl-itiact.
         Return "Adm-Error":U.
      End.   

      If kof.carts:Screen-Value = "":U Then Do:
         {affichemsg.i &Message="Get-TradMsg (004923, 'L''article n''a pas �t� renseign�.':T)" }
         Apply "Entry":U to kof.carts.
         Return "Adm-Error":U.
      End.

      vDatef = (If Tb-Datdeb:Checked
                 Then Date(kof.Datdeb:Screen-Value)
                 Else Date(kof.Datfin:Screen-Value)).

      Assign vCarts = kof.carts:Screen-Value.

      Run Pr-Rech-cartent-cartsor (Input  vCarts,
                                   Output vCartent,
                                   Output vCartsor,
                                   Output vRet,
                                   Output vMsg).
      If Not vRet Or (vCartent = '' And vCartsor = '') Then Do:
         {affichemsg.i &Message="Substitute(Get-TradMsg (004925, 'Aucun article de fabrication n''est associ� � l''article &1.':T), vCarts) +
                             '|':U + Get-TradTxt (004928, '(Fiche article de production - Planification).':T)" }
         Return "Adm-Error":u.   
      End. 
        
      If rs-iti:screen-value = "1":u Then Do:
      /* ITINERAIRE */
         { iticc.i Fl-Itiact Yes Yes Fl-litiact }

         If Available buf-kiti Then Do:
            w-typflux = buf-kiti.typflux.

            {&Run-ActBib-Test-Crit-Oblig-Fab}
                       (wcsoc, wcetab,
                        Input  buf-kiti.citi,
                        Input  "":U,
                        Input  vDatef,
                        Output vRet,
                        Output wstr).
                        
            If Not vRet Then Do:
               {affichemsg.i &Message="Substitute(Get-TradMsg (004935, 'L''itin�raire &1 comporte des crit�res obligatoires|que vous n''avez pas saisi.':T),kof.carts:Screen-Value)" }
               Return "Adm-Error":U.
            End.
         End.

         If Not Test-Global And Available buf-Kiti Then
            run init-info-ITIACT(no).

         If Test-Global Then Do:
            Case w-typflux:
               When {&TYPFLUX-POUSSE} Then wcartiti = vCartent.
               When {&TYPFLUX-TIRE}   Then wcartiti = vCartsor.
               When {&TYPFLUX-AUCUN}  Then wcartiti = "":U.
            End Case.

            Run controle-itineraire (Input Wcartiti, Output vCunite,
                                     Output vRet, Output vMsg).
            If Not vRet Then Do:
               {affichemsg.i  &Message="vMsg" }
               Return "Adm-Error":U.
            End.
         End.
         Else 
            Run Enable-Lotent(w-typflux, w-typof, kof.carts:Screen-Value).

         If Available buf-kiti Then
            Run Init-commentaire(Input "1":U,buf-kiti.citi).
      End. /*itin�raire */
      
      Else Do:
      /* ACTIVITE */
         { actcc.i Fl-Itiact Yes Yes Fl-litiact }
          
         If Available buf-kact Then Do:
            w-typflux = buf-kact.typflux. 
            
            {&Run-ActBib-Test-Crit-Oblig-Fab}
                    (Input wcsoc, wcetab,
                     Input  "":U,
                     Input  buf-kact.cact,
                     Input  vDatef,
                     Output vRet,
                     Output wstr).                     
            If Not vRet Then Do:
               {affichemsg.i
                  &Message="Substitute(Get-TradMsg (004942, 'L''activit� &1 comporte des crit�res obligatoires|que vous n''avez pas saisi.':T),kof.carts:Screen-Value)" }
               Return "Adm-Error":U.
            End.
            
            IF Fl-litiact:SCREEN-VALUE <> "" AND
               Kof.cres:SCREEN-VALUE   = ""  AND 
               NOT Adm-new-record 
            THEN DO:
               /* s'il s'agit d'une activit� qui poss�de un process op�ratoire par d�faut, */
               /* l'OF sera cr�� par d�faut avec les op�rations de celui-ci */
               FOR LAST bKactd FIELDS(csoc cetab cact datef)
                                WHERE bKactd.csoc  = wcsoc
                                  AND bKactd.cetab = wcetab
                                  AND bKactd.cact  = buf-kact.cact
                                  AND bKactd.datef <= vDateVigueur /* TODAY SLe 19/04/10 Q003369 */
                               NO-LOCK,
                   {&EachOne} bKactdpop FIELDS (cpope)
                                         WHERE bKactdpop.csoc    = bKactd.csoc 
                                           AND bKactdpop.cetab   = bKactd.cetab         
                                           AND bKactdpop.cact    = bKactd.cact 
                                           AND bKactdpop.datef   = bKactd.datef
                                           AND bKactdpop.tdefaut = YES
                                        NO-LOCK:                     
                     ASSIGN Kof.cres:SCREEN-VALUE = bKactdpop.cpope.
               END.
            END.
         End.
         
         If Not Test-Global And Available buf-Kact Then 
            run init-info-ITIACT(no).

         If Test-Global Then Do:
            Case w-typflux:
               When {&TYPFLUX-POUSSE} Then wcartiti = vCartent.
               When {&TYPFLUX-TIRE}   Then wcartiti = vCartsor.
               When {&TYPFLUX-AUCUN}  Then wcartiti = "":U.
            End Case.

            Run controle-activite (Input Wcartiti, Output vCunite,
                                   Output vRet, Output vMsg).
            If Not vRet Then Do:
               {affichemsg.i  &Message="vMsg" }
               Return "Adm-Error":U.
            End.            
         End.     
         Else
             Run Enable-Lotent(w-typflux, w-typof, kof.carts:Screen-Value).

         If Available buf-kact Then
             Run Init-commentaire(Input "2":U, Input buf-kact.cact).
      End.  /* activit� */
      
      /* NB 22/09/2005 : Ajout du contr�le sur le contexte prod */
      IF NOT CAN-FIND( FIRST kactctxu WHERE kactctxu.csoc  = wcsoc
                                        AND kactctxu.cetab = wcetab
                                        AND kactctxu.typcle = (IF rs-iti:screen-value = "1":u
                                                                  THEN {&KACTCTXU-TYPCLE-ITINERAIRE}
                                                                  ELSE {&KACTCTXU-TYPCLE-ACTIVITE})
                                        AND kactctxu.cle = Fl-Itiact:SCREEN-VALUE
                                        AND kactctxu.cactctx = {&APPEL-PROD}
                                        AND kactctxu.tuse = TRUE )
      THEN DO:
         {affichemsg.i       &Message = "Substitute(Get-TradMsg (023789, 'L''activit� ou itin�raire &1 n''est pas utilis� dans le contexte production.':T), Fl-Itiact:SCREEN-VALUE)"}
         Apply "Entry":U to Fl-itiact.
         Return "Adm-Error":U.
      END.
      /* Fin NB 22/09/2005 */
   End. /* Test sur F1-ItiAct */

   /*********************************************************/    
    If Test-Global 
       Or Widget-Test = Kof.cequip:Handle Then Do :
       {equipcc.i Kof.Cequip yes no}
    End.
       
    If (Test-Global Or Widget-Test = Kof.cres:Handle)      
    Then Do :       
       /* d�termination de la gestion des ressources de l'itin�raire */
       IF NOT vgtRechTypgestres THEN
          RUN P-Gestion-ressources.          

       IF vgTypgestres = {&KOF-CSIT-GESTION-PROCESS} THEN DO:                              
          IF Kof.cres:SCREEN-VALUE <> "" THEN
             IF Rs-iti:screen-value = '1':U THEN DO:       
                /* Lien Process - Activit�s de l'itin�raire */
                {&RUN-ACTBIB-PROCESSTestSaisieItinerairesDateRech} (INPUT  wcsoc,
                                                               INPUT  wcetab,
                                                               INPUT  Fl-Itiact:SCREEN-VALUE,
                                                               INPUT  Kof.cres:SCREEN-VALUE,
                                                               INPUT  vDateVigueur,
                                                               OUTPUT vRet).
                IF NOT vRet THEN DO:                
                   {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (029399, 'Le process op�ratoire &1 n''est li� � aucune activit� de l''itin�raire':T), Kof.cres:SCREEN-VALUE)"}
                   Apply "Entry":U to Kof.cres.
                   Return "Adm-Error":u.
                END.   
             END.   
             ELSE          
                /* le process saisi doit �tre li� � la derni�re date d'effet de l'activit� de l'OF */       
                FOR LAST bKactd FIELDS (csoc cetab cact datef)
                                 WHERE bKactd.csoc  = wcsoc                 
                                   AND bkactd.cetab = wcetab                
                                   AND bKactd.cact  = Fl-Itiact:SCREEN-VALUE
                                   AND bKactd.datef  <= vDateVigueur /* TODAY SLe 19/04/10 Q003369 */
                                 NO-LOCK:  
                   ASSIGN vDatef = bKactd.datef.
                   IF NOT CAN-FIND(FIRST bKactdpop WHERE bKactdpop.csoc  = bKactd.csoc 
                                                     AND bKactdpop.cetab = bkactd.cetab
                                                     AND bKactdpop.cact  = bKactd.cact 
                                                     AND bKactdpop.datef = bkactd.datef
                                                     AND bKactdpop.cpope = Kof.cres:SCREEN-VALUE
                                                   NO-LOCK) THEN DO:                                                                                        
                      {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (019885, 'Cette valeur n''existe pas (&1 &2).':T), 
                                                             Get-TradTxt (030120, 'process op�ratoire':T) + ' ' + Kof.cres:SCREEN-VALUE,
                                                             '- ':U + Get-TradTxt (000345, 'Activit�':T) + ' ' + Fl-Itiact:SCREEN-VALUE)"}
                      Apply "Entry":U to Kof.cres.
                      Return "Adm-Error":u.
                   END.      
                END.        

          /* affichage du libell� du process */
          IF AVAILABLE buf-Kact THEN DO:
            /* sLe 16/03/2011 A004461 datef */
            {popecc.i Kof.cres NO YES F-lres 4 buf-kact.cact vDatef} /* Process op�ratoires */
          END.
          ELSE DO:
            {popecc.i Kof.cres NO YES F-lres 5 '' ?} /* Process op�ratoires */
          END.
       END.
       ELSE DO:
         {rescc.i Kof.cres yes yes F-lres 0 ACT {&TYPRESS-CENTRE-SUPERIEUR}}
       END.
    End.

    /* Il provient du local-add et local-copy */
    If Test-Global And Adm-New-Record And w-chrono-newOF = 0 Then Do:
      Run chronojp.p (Input  {&CHRONO-NORMAL},
                      Input  wcsoc,
                      Input  wcetab,
                      Input  w-prechro,
                      Output w-chrono-newOF).
      Assign kof.prechro:screen-value = w-prechro
             kof.chrono:screen-value  = string(w-chrono-newOF).
    End.

    If Test-Global And F-custk2:Screen-Value = '' Then Do:
       {affichemsg.i       &Message = "Substitute(Get-TradMsg (012640, 'Cette zone est obligatoire (&1).':T), Get-TradTxt (000112, 'Unit�':T))"}
       Apply "Entry":U to F-custk2.
       Return "Adm-Error":u.
    End.
END.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE zzTest-Saisie-RecalculDatprod Tablejv 
PROCEDURE zzTest-Saisie-RecalculDatprod :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       zz 14/01/2013 M127116
------------------------------------------------------------------------------*/
   /*
   DEFINE VARIABLE vHeure   AS INTEGER   NO-UNDO.
   DEFINE VARIABLE vDatprod AS DATE      NO-UNDO.
   DEFINE VARIABLE vCequip  AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vRet     AS LOGICAL   NO-UNDO.

DO WITH FRAME {&FRAME-NAME}:
   /* SLe 18/04/2012 Journ�e de prod d'apr�s le calendrier de la 1�re ressource �l�mentaire avec calendrier */
   If Tb-Datdeb:checked THEN DO:
      ASSIGN i-actbib-datef = DATE (kof.datdeb:SCREEN-VALUE).
      {admvif/method/saiheure.i Assign vHeure F-heurdeb yes}
   END.
   ELSE DO:
      ASSIGN i-actbib-datef = DATE (kof.datfin:SCREEN-VALUE).
      {admvif/method/saiheure.i Assign vHeure F-heurfin yes}
   END.
   Case Rs-iti:screen-value :
     When "1":u then Assign i-actbib-citi  = Fl-itiact:screen-value
                            i-actbib-cact  = "":U.
     When "2":u then Assign i-actbib-citi  = "":U
                            i-actbib-cact  = Fl-itiact:screen-value.
   End case.
   RUN bscald03jp.p (INPUT  wcsoc,wcetab,i-actbib-datef,vHeure,
                     INPUT  i-actbib-citi,i-actbib-cact,kof.cres:SCREEN-VALUE,
                     OUTPUT vDatprod, OUTPUT vCequip, OUTPUT vRet).
   ASSIGN kof.datprod:SCREEN-VALUE = STRING (vDatprod).         
    /* KL 20130909_0004: D1708. Ajout d'un contr�le sur l'O.F afin de d�terminer si celui-ci doit �tre li� � une �quipe lors de sa cr�ation. */
    /* Ce contr�le s'applique uniquement au O.F de type Origine. */    
   IF vRet AND vCequip NE ""  AND GererParEquipe(wcsoc, wcetab, i-actbib-citi, i-actbib-cact, w-typof, i-actbib-datef) THEN
      /* Si l'�quipe est renseign�e au niveau des calendriers, on l'impose */
      ASSIGN kof.cequip:SCREEN-VALUE = vCequip.
   ELSE 
      ASSIGN kof.cequip:SCREEN-VALUE  = "".

END.
*/
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION F-StadeLanctev Tablejv 
FUNCTION F-StadeLanctev RETURNS CHARACTER
  (INPUT iTypflux AS CHARACTER) :
/*------------------------------------------------------------------------------
  Purpose:  ALB 20141020_0004 Q007539
    Notes:  
------------------------------------------------------------------------------*/

  RETURN (IF iTypflux = {&TYPFLUX-POUSSE} THEN 'LANCTEVP':U ELSE 'LANCTEVT':U).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION F-StadeProgev Tablejv 
FUNCTION F-StadeProgev RETURNS CHARACTER
  (INPUT iTypflux AS CHARACTER) :
/*------------------------------------------------------------------------------
  Purpose:   ALB 20141020_0004 Q007539
    Notes:  
------------------------------------------------------------------------------*/

  RETURN (IF iTypflux = {&TYPFLUX-POUSSE} THEN 'PROGEVP':U ELSE 'PROGEVT':U).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Fo-CleComp Tablejv 
FUNCTION Fo-CleComp RETURNS CHARACTER
  ( INPUT iCStade AS CHARACTER) :
/*------------------------------------------------------------------------------
  Purpose:   << ALB 20130719_0003 DP1713 Retourne la cl� compl�mentaire du stade
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vCfmact   AS CHARACTER NO-UNDO.       
  DEFINE VARIABLE vCleComp  AS CHARACTER NO-UNDO.
  
  /* Pour les stades LANCTTIR et LANCTPOU, le param�trage ne se fait pas par activit�, or si on envoie une cl� compl�mentaire 
  dans ce cas l�, il y a des effets de bord sur la gestion des crit�res. */
  CASE iCStade:
     WHEN "LANCTTIR":U OR
     WHEN "LANCTPOU":U THEN vCleComp = "":U. /* CBA 20140325_0004 DP1253 : ajout gestion Et Par activit� pour stades cr�ation O.F tir� et pouss� */ /* ALB 20140530_0001 Q7202 Retour arri�re */
     WHEN "OFTECTIR":U OR
     WHEN "OFTECPOU":U OR 
     WHEN "PROGEVT":U  OR 
     WHEN "LANCTEVT":U OR 
     WHEN "PROGEVP":U  OR 
     WHEN "LANCTEVP":U THEN DO WITH FRAME {&FRAME-NAME}: /* ALB 20141020_0004 Q007539 */
        Case Rs-Iti:Screen-Value : 
           When "1":u Then Do: /* Itin�raire */
              {&Run-InfoAct-RechInfo} (wcsoc, wcetab, vgCactRefIti, "cfmact":U, Output w-ret, Output vCfmact).    /* ALB 20140911_0006 Q7437 */
           End.
           When "2":u Then Do: /* Activit� */
              {&Run-InfoAct-RechInfo} (wcsoc, wcetab, Fl-itiact:screen-value, "cfmact":U, Output w-ret, Output vCfmact). 
           End.
        End Case.          
        vCleComp = vCfmact + ",":U.
     END.
  END CASE.
  
  RETURN vCleComp. 

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Fo-Qte-Lotent-Prioritaire Tablejv 
FUNCTION Fo-Qte-Lotent-Prioritaire RETURNS LOGICAL
  ( Input i-cart As Character ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
  Define Variable w-cfam As Character No-Undo.

  {&Run-SITE2-Recherche-Character} (Input  wcsoc
                                  , Input  wcetab
                                  , Input  i-cart
                                  , Input  "":U
                                  , Input  "zart.cfam":U
                                  , Output w-cfam
                                  , Output ret
                                  , Output wstr).
  If Not ret Then Return False.

  {getparii.i &CPACON    = "'OFCRE2':U"
              &NOCHAMP   = 9
              &CSOC      = wcsoc
              &CETAB     = wcetab
              &CLE       = w-cfam
              &RETVALDEF = Yes}

  RETURN (vParam-Valeurs = '1':U).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Fo-QteReste Tablejv 
FUNCTION Fo-QteReste RETURNS DECIMAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  NLE 30/08/2004
    Notes:  Pour OF d'origine, renvoie Reste � faire de l'OF (sur l'OT de r�f�rence)
------------------------------------------------------------------------------*/

    Define Buffer b-kot For kot.
    DEFINE VARIABLE vQteReste AS DECIMAL NO-UNDO INITIAL 0.0.

    If Available kof THEN DO:
       For First b-kot Fields (ni1 qtestk qte cunite)
                       Where b-kot.csoc    = kof.csoc
                         And b-kot.cetab   = kof.cetab
                         And b-kot.prechro = kof.prechro
                         And b-kot.chrono  = kof.chrono
                         And b-kot.ni1     = kof.ni1artref
                       No-Lock:
          /* << DD 20130415_0012 M131596 M131597 */   
          CASE kof.typflux:
             WHEN {&TYPFLUX-POUSSE} THEN DO:
                /* En flux pouss� la qt� pr�vue est la qt� � d�stocker */
                ASSIGN vQteReste = b-kot.qtestk.
             END.
             WHEN {&TYPFLUX-TIRE} THEN DO:
                /* En flux tir� la qt� pr�vue est la qt� qui rentre en stock */
               ASSIGN vQteReste = b-kot.qte[1].
             END.
          END CASE.
          /*Return b-kot.qtestk.*/
          /* >> DD 20130415_0012 M131596 M131597 */
       End.
    END.
    
    RETURN vQteReste.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Fo-Stade Tablejv 
FUNCTION Fo-Stade RETURNS CHARACTER
  (Input iTypflux As Character, Input iTypof As Character, Input iCetatOf As Character ) :
/*------------------------------------------------------------------------------
  Purpose:  ALB 20130719_0003 DP1713 Recherche du stade
    Notes:  
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vStade       AS CHARACTER NO-UNDO.
  DEFINE VARIABLE vtLanctExist AS LOGICAL   NO-UNDO.
  DEFINE VARIABLE vtProgExist  AS LOGICAL   NO-UNDO.
  DEFINE VARIABLE vCfmact      AS CHARACTER NO-UNDO.
  
  /* Est-ce qu'il y a au moins un crit�re sur le stade lancement qui a �t� param�tr� */
  DO WITH FRAME {&FRAME-NAME}:
     Case Rs-Iti:Screen-Value : 
        When "1":u Then Do: /* Itin�raire */
           {&Run-InfoAct-RechInfo} (wcsoc, wcetab, vgCactRefIti, "cfmact":U, Output w-ret, Output vCfmact).  /* ALB 20140911_0006 Q7437 */          
        End.
        When "2":u Then Do: /* Activit� */
           {&Run-InfoAct-RechInfo} (wcsoc, wcetab, Fl-itiact:screen-value, "cfmact":U, Output w-ret, Output vCfmact). 
        End.
     End Case.            
     vtLanctExist = {&Stade-CritExistence}
                        (Input wcsoc,
                         Input wcetab,
                         INPUT kof.carts:SCREEN-VALUE,
                         Input F-StadeLanctev(iTypflux), /* ALB 20141020_0004 Q007539 */
                         (IF Tb-Datdeb:CHECKED 
                             THEN DATE(kof.datdeb:SCREEN-VALUE) 
                             ELSE DATE(kof.datfin:SCREEN-VALUE)),
                         INPUT (vCfmact + ","),
                         INPUT ""  /* Liste des crit�res */
                        ).
     vtProgExist = {&Stade-CritExistence}
                        (Input wcsoc,
                         Input wcetab,
                         INPUT kof.carts:SCREEN-VALUE,
                         Input F-StadeProgev(iTypflux),  /* ALB 20141020_0004 Q007539 */
                         (IF Tb-Datdeb:CHECKED 
                             THEN DATE(kof.datdeb:SCREEN-VALUE) 
                             ELSE DATE(kof.datfin:SCREEN-VALUE)),
                         INPUT (vCfmact + ","),
                         INPUT ""  /* Liste des crit�res */
                        ).  
                        
  END.  

  /* Recherche stade suivant plusieurs crit�res:                                                                                   */
  /* Stade programmation si:                                                                                                       */
  /*    - Il y a au moins un crit�re param�tr� � ce stade                                                                          */
  /*    - On n'est pas en cours de cr�ation de l'of                                                                                */
  /*    - L'utilisateur vient de r�aliser l'action de "programmation" (vgEtape = 1) ou l'of est � soit l'�tat Calcul� ou Programm� */
  /* Stade lancement si:                                                                                                           */
  /*    - Il y a au moins un crit�re param�tr� � ce stade                                                                          */
  /*    - On n'est pas en cours de cr�ation de l'of                                                                                */
  /*    - L'utilisateur vient de r�aliser l'action de "lancement" (vgEtape = 2) ou l'of est � l'�tat lanc� (ou +)                  */  
  /* Si aucune de ces conditions ne sont remplis, on prend les stades correspondant � une cr�ation d'OF                            */                        
  IF NOT Adm-New-Record AND vgEtape NE 2 AND ((iCetatOf >= {&CETAT-OF-CALCULE} AND iCetatOf < {&CETAT-OF-LANCE}) OR vgEtape = 1) AND vtProgExist
     THEN vStade = F-StadeProgev(iTypflux).  /* ALB 20141020_0004 Q007539 */
     ELSE IF NOT Adm-New-Record AND (iCetatOf > {&CETAT-OF-PLANIFIE} OR vgEtape = 2) AND vtLanctExist
        THEN vStade = F-StadeLanctev(iTypflux). /* ALB 20141020_0004 Q007539 */
        ELSE        
           Case iTypflux:
              When {&TYPFLUX-POUSSE} 
                  Then If iTypof = {&TYPOF-OFTECH} 
                     THEN vStade = 'OFTECPOU':U.
                     ELSE vStade = 'LANCTPOU':u.
                     
               When {&TYPFLUX-TIRE}   
                  Then If iTypof = {&TYPOF-OFTECH} 
                     THEN vStade = 'OFTECTIR':U.
                     ELSE vStade = 'LANCTTIR':u.
               /* SLE 02/11/04 */
               When {&TYPFLUX-POUTIR} THEN DO:  
                  {getparii.i &CPACON  = "'ARTICLE':U"
                              &NOCHAMP = 1}
                  IF vParam-Ok AND vParam-Valeurs = 'T':U THEN
                     ASSIGN vStade   = (If iTypof NE {&TYPOF-OFTECH} THEN 'LANCTTIR':u ELSE 'OFTECTIR':U)
                            iTypflux = {&TYPFLUX-TIRE}. /* SLe 18/01/08 */
                  ELSE
                     ASSIGN vStade   = (If iTypof NE {&TYPOF-OFTECH} THEN 'LANCTPOU':u ELSE 'OFTECPOU':U)
                            iTypflux = {&TYPFLUX-POUSSE}. /* SLe 18/01/08 */
               END.
               /* --- */
               OtherWise vStade = '':u.
            End Case.
  
  RETURN vStade.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION GererParEquipe Tablejv 
FUNCTION GererParEquipe RETURNS LOGICAL PRIVATE
  ( Input iCsoc      As CHARACTER,
    Input iCetab     As CHARACTER,
    Input iCactref   As CHARACTER,  
    Input iTypOf     As CHARACTER,
    INPUT iPrechro   As CHARACTER,
    INPUT iChrono    As INTEGER,
    INPUT iNi1artref AS INTEGER
    ) :
/*------------------------------------------------------------------------------
  Purpose: Fonction permettant de d�terminer si un O.F d'origine doit �tre li� � une �quipe.
           1) Si le param�tre iTypOf est diff�rent de {&TYPOF-OFORIG}, la valeur True est automatiquement retourn�e.
           2) Sinon, on r�cup�re le champ 12 du param�tre OFCRE sur la famille d'activit� de l'O.F:
            - Si la valeur est � 'Oui', l'O.F sera li� � une �quipe.
            - Si la valeur est � 'Non', l'O.F ne sera pas li� � une �quipe.
  Parameters : Entr�e: - iCsoc: Soci�t� courant
                       - iCetab: Etablissement courant
                       - iCact: Activit� de l'article de r�f�rence de l'O.F
                       - iTypOf: Type de l'O.F (Origine ou classique)
               Sortie: - Boolean indiquant si l'O.F doit �tre li� � une �quipe.
    Notes:  
------------------------------------------------------------------------------*/
DEFINE VARIABLE vRet    AS LOGICAL   NO-UNDO.
DEFINE VARIABLE vCfmact AS CHARACTER NO-UNDO.

DEFINE BUFFER bKot FOR kot.

   /* Recherche du param�tre UNIQUEMENT si l'O.F est d'origine. */
   IF iTypOf = {&TYPOF-OFORIG} THEN DO:
      /* Chargement de la famille activit� */              
      IF iCactref = '' THEN DO:
         FOR FIRST bKot FIELDS (cfmact)
                        WHERE bKot.csoc    = iCsoc
                          AND bKot.cetab   = iCetab
                          AND bKot.prechro = iPrechro
                          AND bKot.chrono  = iChrono
                          AND bKot.ni1     = iNi1artref
                        NO-LOCK:
            ASSIGN vCfmact = bkot.cfmact
                   vRet    = YES.
         END.
      END.
      ELSE DO:
         {&Run-InfoAct-RechInfo} (iCsoc, iCetab, iCactref, "cfmact":U, Output vRet, Output vCfmact). 
      END.
 
      /* Si la famille est trouv�, on charge le champ 12 du param�tre 'OFCRE'. */
      IF vRet THEN DO:
         {getparii.i 
           &CPACON    = "'OFCRE':U" 
           &NOCHAMP   = 12
           &CSOC      = iCsoc
           &CETAB     = iCetab
           &CLE       = vCfmact
           &RETVALDEF = YES
         }
         RETURN LOGICAL(vParam-Valeurs).
      END.
      /* Cas o� la famille de l'activit� n'est pas retouv�e. */
      RETURN FALSE.
   END.
   /* L'O.F est classique, dans ce cas, il est forc�ment li� � une �quipe. */
   RETURN TRUE.

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Regeneration-Num-Lot Tablejv 
FUNCTION Regeneration-Num-Lot RETURNS LOGICAL
  ( /* parameter-definitions */ ) :
/*------------------------------------------------------------------------------
  Purpose:  V�rifie si le param�trage de l'article est ok pour re-g�n�rer un num�ro de lot.
    Notes:  
------------------------------------------------------------------------------*/
  Define Variable w-cfam As Character No-Undo.

  {&Run-SITE2-Recherche-Character} (Input  wcsoc
                                  , Input  wcetab
                                  , Input  kof.carts
                                  , Input  "":U
                                  , Input  "zart.cfam":U
                                  , Output w-cfam
                                  , Output ret
                                  , Output wstr).
  If Not ret Then Return False.

  {getparii.i &CPACON  = "'LOTAUTO':u"
              &NOCHAMP = 12
              &CLE     = w-cfam
              &CSOC    = wcsoc}

  Return (vParam-Ok And vParam-Valeurs = 'Yes':U).

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


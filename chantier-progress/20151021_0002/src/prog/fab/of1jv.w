&ANALYZE-SUSPEND _VERSION-NUMBER UIB_v8r12 GUI ADM1
&ANALYZE-RESUME
/* Connected Databases 
          vif5_7           ORACLE
*/
&Scoped-define WINDOW-NAME CURRENT-WINDOW
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Description-Programme" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/compro-w.p */

/*-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
 * Application  : DVIF4 - VIF5_7                          
 * Programme    : fab\of1jv.w
 *
 * Description  : Saisie d'OF : SmartViewer suite sur OF
 * 
 * Template     : viewer.w - SmartViewer basique 
 * Type Objet   : SmartViewer 
 * Compilable   : __COMPILE__WIN 
 * Cr�� le      : 17/04/1997 � 13:49 par CL
 *
 * HISTORIQUE DES MODIFICATIONS :
 *  - Le 17/10/2012 - SLE     : Q005768:Priorit� : si ZARTPROD n'existe pas, on n'applique...
 *                              20121017_0003 (22335)|Q005768|Priorit� : si ZARTPROD n'existe pas, on n'applique pas la priorit� par d�faut, 9|SLE|17/10/12 12:07:54 
 *                               
 *
 * ATTRIBUTS :
 *
 *-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-*/
&GLOBAL-DEFINE PRODLIV-PROGRAMME G.P.

&Global-Define Adv-User-Attribute-List "":U

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Initial-Section" Tablejv _INLINE
/* Actions: ? ? ? ? ? */
/* Create an unnamed pool to store all the widgets created 
     by this procedure. This is a good default which assures
     that this procedure's triggers and internal procedures 
     will execute in this procedure's storage, and that proper
     cleanup will occur on deletion of the procedure. */
CREATE WIDGET-POOL.

&SCOPED-DEFINE adm-attribute-dlg admvif/support/viewerd.w

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _DEFINITIONS Tablejv 
/*----------------------------------------------------------------------*/
/*          Definitions du programme.                                   */
/*----------------------------------------------------------------------*/

/* Variables globales */
{variable.i " "}
{infoactbibli.i " "}

/* constantes de pre-compilation */
&GLOBAL-DEFINE table  kof
&GLOBAL-DEFINE ltable

&Scoped-define ENABLED-TABLES kof

/* Car une valeur vide n'est pas consid�r�e comme une valeur dans une combo... */
&GLOBAL-DEFINE CLASSIQUE   'CLASSIQUE':U

/* Variable pour template viewer */
{admvif/template/vinclude.i "Definitions"}

/* Local Variable Definitions ---                                       */
Define Variable vgLib-ofclass   As Character No-Undo.
Define Variable vgLib-oforig    As Character No-Undo.
Define Variable vgLib-oftech    As Character No-Undo.
DEFINE VARIABLE vgLcode-orig    AS CHARACTER NO-UNDO.
DEFINE VARIABLE vgLclair-orig   AS CHARACTER NO-UNDO.
DEFINE VARIABLE vgLcode-cnatie  AS CHARACTER NO-UNDO.
DEFINE VARIABLE vgLclair-cnatie AS CHARACTER NO-UNDO.

Define Variable vgLst-cod           As Character    No-Undo.
Define Variable vgLst-lib           As Character    No-Undo.

/* SLe 02/06/09 Q002616 */
DEFINE VARIABLE vgSAVcleInit AS CHARACTER NO-UNDO.

DEFINE VARIABLE vgSAVCart    AS CHARACTER NO-UNDO.

Define Variable w-cact  As Character No-Undo.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Outils Proc�dures" Tablejv _INLINE
/* Actions: ? admvif/xftr/eoutils.w ? ? ? */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-PREPROCESSOR-BLOCK 

/* ********************  Preprocessor Definitions  ******************** */

&Scoped-define PROCEDURE-TYPE SmartViewer
&Scoped-define DB-AWARE no

&Scoped-define ADM-SUPPORTED-LINKS Record-Source,Record-Target,TableIO-Target

/* Name of designated FRAME-NAME and/or first browse and/or first query */
&Scoped-define FRAME-NAME F-Main

/* External Tables                                                      */
&Scoped-define EXTERNAL-TABLES kof
&Scoped-define FIRST-EXTERNAL-TABLE kof


/* Need to scope the external tables to this procedure                  */
DEFINE QUERY external_tables FOR kof.
/* Standard List Definitions                                            */
&Scoped-Define ENABLED-FIELDS kof.prior kof.tsstrait kof.csst 
&Scoped-define ENABLED-TABLES kof
&Scoped-define FIRST-ENABLED-TABLE kof
&Scoped-Define ENABLED-OBJECTS RECT-45 RECT-47 CB-typof B_AID- 
&Scoped-Define DISPLAYED-FIELDS kof.csocor kof.cetabor kof.prechror ~
kof.chronor kof.ni1or kof.ni2or kof.ctieor kof.prior kof.tsstrait kof.lof ~
kof.rof kof.csst ksst.lsst 
&Scoped-define DISPLAYED-TABLES kof ksst
&Scoped-define FIRST-DISPLAYED-TABLE kof
&Scoped-define SECOND-DISPLAYED-TABLE ksst
&Scoped-Define DISPLAYED-OBJECTS CB-typof 

/* Custom List Definitions                                              */
/* ADM-CREATE-FIELDS,ADM-ASSIGN-FIELDS,L-Oblig,L-Field,L-Majuscule,List-6 */
&Scoped-define L-Oblig kof.csst 
&Scoped-define L-Field CB-typof 

/* _UIB-PREPROCESSOR-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Auto-Definitions" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/wrdef.p */

/* ********** Preprocessor Definitions VIF ***********/
&Scoped-Define Trait-External-Tables ~{&TP1} kof ~{&TP2} 
&Scoped-Define List-Champs-Auto kof.csocor,kof.cetabor,kof.prechror,kof.chronor,kof.ni1or,kof.ni2or,kof.ctieor,CB-typof,kof.prior,kof.tsstrait,kof.lof,kof.rof,kof.csst,ksst.lsst
&If Defined(List-Champs-Auto) <> 0 &Then
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&List-Champs-Auto},{&User-List-Champs}
   &Else
      &Scoped-Define List-champs {&List-Champs-Auto}
   &Endif
&Else
   &If Defined(User-List-champs) <> 0 &Then
      &Scoped-Define List-champs {&User-List-Champs}
   &Endif
&Endif

&Scoped-Define List-Combos CB-typof
&Scoped-Define Clear-Combos Assign CB-typof:screen-value in Frame F-Main = CB-typof:Entry(1)
&Scoped-Define Initialize-Combos ~
  Run Init-Cb-Rs In Adm-broker-hdl (CB-typof:handle in Frame F-Main).
&Scoped-Define Special-Pairs  ~
       ~{&SP1}tsstrait~{&SP2}tsstrait~{&SP3}tsstrait~{&SP4}
&Scoped-Define FIELD-PAIRS~
 ~{&FP1}prior ~{&FP2}prior ~{&FP3}~
 ~{&FP1}csst ~{&FP2}csst ~{&FP3}
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Prototypes ********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Csst Tablejv 
FUNCTION Csst RETURNS CHARACTER
  ( INPUT iCact AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Priorite Tablejv 
FUNCTION Priorite RETURNS CHARACTER PRIVATE
  ( INPUT iCart AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION-FORWARD Tsstrait Tablejv 
FUNCTION Tsstrait RETURNS LOGICAL
  ( INPUT iCact AS CHARACTER )  FORWARD.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* ***********************  Control Definitions  ********************** */


/* Definitions of the field level widgets                               */
DEFINE BUTTON B_AID- 
     IMAGE-UP FILE "admvif/images/combo.bmp":U
     IMAGE-INSENSITIVE FILE "admvif/images/combos.bmp":U
     LABEL "?" 
     SIZE 3.6 BY 1.05.

DEFINE VARIABLE CB-typof AS CHARACTER FORMAT "X(30)":U 
     LABEL "Type de l'OF" 
     VIEW-AS COMBO-BOX INNER-LINES 3
     DROP-DOWN-LIST
     SIZE 26 BY 1 NO-UNDO.

DEFINE VARIABLE F-cnatieor AS CHARACTER FORMAT "X(12)":U 
     LABEL "Tiers" 
     VIEW-AS FILL-IN 
     SIZE 22.8 BY 1 TOOLTIP "Type de tiers � l'origine de l'OF" NO-UNDO.

DEFINE VARIABLE F-ltie AS CHARACTER FORMAT "X(30)":U 
     VIEW-AS FILL-IN 
     SIZE 49.2 BY 1 TOOLTIP "Libell� du tiers" NO-UNDO.

DEFINE VARIABLE F-orig AS CHARACTER FORMAT "X(30)":U 
     LABEL "Origine" 
     VIEW-AS FILL-IN 
     SIZE 55.2 BY 1 TOOLTIP "Origine" NO-UNDO.

DEFINE RECTANGLE RECT-45
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 143 BY 3.14.

DEFINE RECTANGLE RECT-47
     EDGE-PIXELS 2 GRAPHIC-EDGE  NO-FILL   
     SIZE 143 BY 3.14.


/* ************************  Frame Definitions  *********************** */

DEFINE FRAME F-Main
     F-orig AT ROW 1.91 COL 3.8
     kof.csocor AT ROW 2.95 COL 9.6 COLON-ALIGNED
          LABEL "Acte"
          VIEW-AS FILL-IN 
          SIZE 4.8 BY 1 TOOLTIP "Soci�t� d'origine"
     kof.cetabor AT ROW 2.95 COL 16.6 NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 4.8 BY 1 TOOLTIP "Etablissement d'origine"
     kof.prechror AT ROW 2.95 COL 19.6 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 7.2 BY 1 TOOLTIP "Pr�fixe du chrono de l'acte d'origine"
     kof.chronor AT ROW 2.95 COL 27 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 11 BY 1 TOOLTIP "Num�ro de l'acte d'origine"
     kof.ni1or AT ROW 2.95 COL 38.2 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 5 BY 1 TOOLTIP "Ligne de l'acte d'origine"
     kof.ni2or AT ROW 2.95 COL 43.4 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 5 BY 1
     F-cnatieor AT ROW 2.95 COL 55.2 COLON-ALIGNED
     kof.ctieor AT ROW 2.95 COL 78 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 14 BY 1 TOOLTIP "Tiers"
     F-ltie AT ROW 2.95 COL 91.6 COLON-ALIGNED NO-LABEL
     CB-typof AT ROW 4.76 COL 21 COLON-ALIGNED
     kof.prior AT ROW 4.76 COL 86 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 12 BY 1
     kof.tsstrait AT ROW 4.76 COL 107
          VIEW-AS TOGGLE-BOX
          SIZE 18.6 BY .81
     kof.lof AT ROW 6 COL 21 COLON-ALIGNED
          VIEW-AS FILL-IN 
          SIZE 32 BY 1
     kof.rof AT ROW 6 COL 53.8 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 14 BY 1
     kof.csst AT ROW 6.14 COL 105.8 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 12 BY 1
     B_AID- AT ROW 6.14 COL 120
     ksst.lsst AT ROW 6.14 COL 122 COLON-ALIGNED NO-LABEL
          VIEW-AS FILL-IN 
          SIZE 19.6 BY 1
     " Origine" VIEW-AS TEXT
          SIZE 11.4 BY .62 AT ROW 1 COL 3.4
          FONT 15
     RECT-45 AT ROW 1.29 COL 1
     RECT-47 AT ROW 4.33 COL 1
    WITH 1 DOWN NO-BOX KEEP-TAB-ORDER OVERLAY 
         SIDE-LABELS NO-UNDERLINE THREE-D 
         AT COL 1 ROW 1 SCROLLABLE 
         FONT 6.


/* *********************** Procedure Settings ************************ */

&ANALYZE-SUSPEND _PROCEDURE-SETTINGS
/* Settings for THIS-PROCEDURE
   Type: SmartViewer
   External Tables: vif5_7.kof
   Allow: Basic,DB-Fields
   Frames: 1
   Add Fields to: EXTERNAL-TABLES
   Other Settings: PERSISTENT-ONLY COMPILE
 */

/* This procedure should always be RUN PERSISTENT.  Report the error,  */
/* then cleanup and return.                                            */
IF NOT THIS-PROCEDURE:PERSISTENT THEN DO:
  MESSAGE "{&FILE-NAME} should only be RUN PERSISTENT.":U
          VIEW-AS ALERT-BOX ERROR BUTTONS OK.
  RETURN.
END.

&ANALYZE-RESUME _END-PROCEDURE-SETTINGS

/* *************************  Create Window  ************************** */

&ANALYZE-SUSPEND _CREATE-WINDOW
/* DESIGN Window definition (used by the UIB) 
  CREATE WINDOW Tablejv ASSIGN
         HEIGHT             = 6.48
         WIDTH              = 143.
/* END WINDOW DEFINITION */
                                                                        */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _INCLUDED-LIB Tablejv 
/* ************************* Included-Libraries *********************** */

{admvif/method/av_viewe.i}
{src/adm/method/viewer.i}
{admvif/method/ap_viewe.i}

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME




/* ***********  Runtime Attributes and AppBuilder Settings  *********** */

&ANALYZE-SUSPEND _RUN-TIME-ATTRIBUTES
/* SETTINGS FOR WINDOW Tablejv
  VISIBLE,,RUN-PERSISTENT                                               */
/* SETTINGS FOR FRAME F-Main
   NOT-VISIBLE FRAME-NAME Size-to-Fit                                   */
ASSIGN 
       FRAME F-Main:SCROLLABLE       = FALSE
       FRAME F-Main:HIDDEN           = TRUE.

ASSIGN 
       B_AID-:PRIVATE-DATA IN FRAME F-Main     = 
                "Fonction=Combo-Aide".

/* SETTINGS FOR COMBO-BOX CB-typof IN FRAME F-Main
   4                                                                    */
/* SETTINGS FOR FILL-IN kof.cetabor IN FRAME F-Main
   NO-ENABLE ALIGN-L EXP-LABEL                                          */
/* SETTINGS FOR FILL-IN kof.chronor IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN kof.csocor IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN kof.csst IN FRAME F-Main
   3                                                                    */
/* SETTINGS FOR FILL-IN kof.ctieor IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN F-cnatieor IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN F-ltie IN FRAME F-Main
   NO-DISPLAY NO-ENABLE                                                 */
/* SETTINGS FOR FILL-IN F-orig IN FRAME F-Main
   NO-DISPLAY NO-ENABLE ALIGN-L                                         */
/* SETTINGS FOR FILL-IN kof.lof IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN ksst.lsst IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN kof.ni1or IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN kof.ni2or IN FRAME F-Main
   NO-ENABLE                                                            */
/* SETTINGS FOR FILL-IN kof.prechror IN FRAME F-Main
   NO-ENABLE EXP-LABEL                                                  */
/* SETTINGS FOR FILL-IN kof.rof IN FRAME F-Main
   NO-ENABLE                                                            */
/* _RUN-TIME-ATTRIBUTES-END */
&ANALYZE-RESUME


/* Setting information for Queries and Browse Widgets fields            */

&ANALYZE-SUSPEND _QUERY-BLOCK FRAME F-Main
/* Query rebuild information for FRAME F-Main
     _Options          = "NO-LOCK"
     _Query            is NOT OPENED
*/  /* FRAME F-Main */
&ANALYZE-RESUME

 

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _XFTR "Taille-Frame" Tablejv _INLINE
/* Actions: ? ? ? ? admvif/xftr/wtaille.p */
/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME



/* ************************  Control Triggers  ************************ */

&Scoped-define SELF-NAME B_AID-
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID- Tablejv
ON CHOOSE OF B_AID- IN FRAME F-Main /* ? */
DO :
  /* Appliquer la r�gle standard du choose sur le bouton */
  {admvif/template/tinclude.i "choose-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL B_AID- Tablejv
ON ENTRY OF B_AID- IN FRAME F-Main /* ? */
DO:
  /* Appliquer la r�gle standard d'entr�e sur le bouton */
  {admvif/template/tinclude.i "entry-combo-aide"}
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME CB-typof
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL CB-typof Tablejv
ON VALUE-CHANGED OF CB-typof IN FRAME F-Main /* Type de l'OF */
DO:
/*     Run Modif-typof. */
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&Scoped-define SELF-NAME kof.tsstrait
&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CONTROL kof.tsstrait Tablejv
ON VALUE-CHANGED OF kof.tsstrait IN FRAME F-Main /* Sous-traitance */
DO:
  If kof.tsstrait:Checked In Frame {&Frame-name} Then  Do:
  MESSAGE "checked".
    ASSIGN kof.csst:SENSITIVE = YES.
    ASSIGN kof.csst:HIDDEN = NO. 
    ASSIGN B_AID-:SENSITIVE = YES.
    ASSIGN kof.csst:SCREEN-VALUE = "".
    ASSIGN ksst.lsst:SCREEN-VALUE = "".
  End.  
  Else Do:   
    MESSAGE "unchecked".
    ASSIGN kof.csst:SENSITIVE = NO.
    ASSIGN B_AID-:SENSITIVE = NO.
    ASSIGN kof.csst:SCREEN-VALUE = "$Interne". 
    ASSIGN kof.csst:HIDDEN = YES.  
    ASSIGN ksst.lsst:SCREEN-VALUE = "".

  End.  
    
END.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


&UNDEFINE SELF-NAME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _CUSTOM _MAIN-BLOCK Tablejv 


/* ***************************  Main Block  *************************** */
/*** Triggers Generaux Du viewer */
{admvif/template/vinclude.i "triggers"}

  &IF DEFINED(UIB_IS_RUNNING) <> 0 &THEN          
    RUN dispatch IN THIS-PROCEDURE ('initialize':U).        
  &ENDIF         

{admvif/template/vinclude.i "main-block"}
  /************************ INTERNAL PROCEDURES ********************/

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME


/* **********************  Internal Procedures  *********************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE adm-row-available Tablejv  _ADM-ROW-AVAILABLE
PROCEDURE adm-row-available :
/*------------------------------------------------------------------------------
  Purpose:     Dispatched to this procedure when the Record-
               Source has a new row available.  This procedure
               tries to get the new row (or foriegn keys) from
               the Record-Source and process it.
  Parameters:  <none>
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.             */
  {src/adm/template/row-head.i}

  /* Create a list of all the tables that we need to get.            */
  {src/adm/template/row-list.i "kof"}

  /* Get the record ROWID's from the RECORD-SOURCE.                  */
  {src/adm/template/row-get.i}

  /* FIND each record specified by the RECORD-SOURCE.                */
  {src/adm/template/row-find.i "kof"}

  /* Process the newly available records (i.e. display fields,
     open queries, and/or pass records on to any RECORD-TARGETS).    */
  {src/adm/template/row-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Aide-Sur-Champ Tablejv 
PROCEDURE Aide-Sur-Champ :
/*------------------------------------------------------------------------------
  Purpose:     Aide (liste de choix) Sur les Champs
  Parameters:  Handle du widget active
  Notes:       
------------------------------------------------------------------------------*/
Define Input Parameter Champ  As Widget-Handle No-Undo.

    Define Variable ChoixEffectue As Character     No-Undo.
    
    Run Get-Info("Fl-Itiact","Group-Assign-Source").
    ASSIGN w-cact = RETURN-VALUE.
    
   Case Champ:Name :
      
      When "csst":U Then Do :
         {&Run-prog} "admvif/objects/a-standa.w":U 
                      ("equipjf":U,             /* Code fonction pour les droits */
                       "ofactsstjb":U, "att-wcact=":U + w-cact,   /* Nom browser et attributs */
                       "equipjv":U, "":U,    /* Nom viewer et attributs */
                       Champ:Label,             /* Titre ecran d'aide */
                       no ,                     /* Acces bouton nouveau */
                       Champ:Screen-Value,      /* Valeur de depart */
                       Output ChoixEffectue).
         If ChoixEffectue <> ? Then 
            Champ:Screen-Value = ChoixEffectue.
      End.
      
      Otherwise ChoixEffectue = ?.
   End Case.

   If ChoixEffectue <> ? Then 
      Apply "Tab":U To Champ.
   Else
      Apply "Entry":U To Champ.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE disable_UI Tablejv  _DEFAULT-DISABLE
PROCEDURE disable_UI :
/*------------------------------------------------------------------------------
  Purpose:     DISABLE the User Interface
  Parameters:  <none>
  Notes:       Here we clean-up the user-interface by deleting
               dynamic widgets we have created and/or hide 
               frames.  This procedure is usually called when
               we are ready to "clean-up" after running.
------------------------------------------------------------------------------*/
  /* Hide all frames. */
  HIDE FRAME F-Main.
  IF THIS-PROCEDURE:PERSISTENT THEN DELETE PROCEDURE THIS-PROCEDURE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE LinkFeatureID-Initialize Tablejv 
PROCEDURE LinkFeatureID-Initialize :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   { csstcc.i kof.csst NO NO }

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-add-record Tablejv 
PROCEDURE local-add-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'add-record':U ) .

  

  /* Code placed here will execute AFTER standard behavior.    */
  RUN Pr-Disable-Champs.
  CB-typof:Sensitive In Frame {&Frame-Name} = True.
  ASSIGN vgSAVcleInit = ""
         vgSAVCart    = "".

  Assign vgLst-cod = {&CLASSIQUE}    + ",":U
                   + {&TYPOF-OFORIG}
         vgLst-lib = vgLib-ofclass  + ",":U
                   + vgLib-oforig.
  {admvif/method/chglstval.i &Hdl="cb-typof:Handle In Frame {&Frame-Name}"
                             &Mnemoid="'':U"
                             &Liste-Valeur="vgLst-cod"
                             &Liste-Libell="vgLst-lib"}
  Run Modif-typof.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-assign-statement Tablejv 
PROCEDURE local-assign-statement :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  DEFINE VARIABLE vTypof AS CHARACTER NO-UNDO.
  
  /* Code placed here will execute PRIOR to standard behavior. */
  Run Get-Valeur-Combo(CB-typof:handle In Frame {&Frame-Name}, Output vtypof).
  ASSIGN kof.typof = (If vtypof = {&CLASSIQUE} THEN '' ELSE vtypof).

  
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'assign-statement':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  
  /* OM : 18/05/2006 G7_1671 Saisie dans la liste des OFs */
  /* NOTA: la gestion du xpilof.prior est assur�e par l'appel dans ofjv.w 
   * � xxpilot01jp.p */
  
  Assign vgLst-cod = {&CLASSIQUE}    + ",":U
                   + {&TYPOF-OFORIG} + ",":U
                   + {&TYPOF-OFTECH}
         vgLst-lib = vgLib-ofclass  + ",":U
                   + vgLib-oforig   + ",":U
                   + vgLib-oftech.
  {admvif/method/chglstval.i &Hdl="cb-typof:Handle In Frame {&Frame-Name}"
                             &Mnemoid="'':U"
                             &Liste-Valeur="vgLst-cod"
                             &Liste-Libell="vgLst-lib"}
  Run Set-Valeur-Combo (cb-typof:Handle In Frame {&Frame-Name}, Input vtypof).
  Run Modif-typof.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-cancel-record Tablejv 
PROCEDURE local-cancel-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'cancel-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
   ASSIGN vgSAVcleInit = ""
          vgSAVCart    = "".
  Assign vgLst-cod = {&CLASSIQUE}    + ",":U
                   + {&TYPOF-OFORIG} + ",":U
                   + {&TYPOF-OFTECH}
         vgLst-lib = vgLib-ofclass  + ",":U
                   + vgLib-oforig   + ",":U
                   + vgLib-oftech.
  {admvif/method/chglstval.i &Hdl="cb-typof:Handle In Frame {&Frame-Name}"
                             &Mnemoid="'':U"
                             &Liste-Valeur="vgLst-cod"
                             &Liste-Libell="vgLst-lib"}

  Run Modif-typof.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-copy-record Tablejv 
PROCEDURE local-copy-record :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'copy-record':U ) .

  /* Code placed here will execute AFTER standard behavior.    */
  RUN Pr-Disable-Champs.
  ASSIGN CB-typof:Sensitive In Frame {&Frame-Name} = False
         F-orig:SCREEN-VALUE       = Entry(Lookup({&ORIG-MAN}, vgLcode-orig), vgLclair-orig)
         kof.csocor:SCREEN-VALUE   = ""
         kof.cetabor:SCREEN-VALUE  = ""
         kof.prechror:SCREEN-VALUE = ""
         kof.chronor:SCREEN-VALUE  = ""
         kof.ni1or:SCREEN-VALUE    = ""
         kof.ni2or:SCREEN-VALUE    = ""
         F-cnatieor:SCREEN-VALUE   = ""
         kof.ctieor:SCREEN-VALUE   = ""
         F-ltie:SCREEN-VALUE    = "".

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-display-fields Tablejv 
PROCEDURE local-display-fields :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/
  
  /* Code placed here will execute PRIOR to standard behavior. */
  RUN Pr-Disable-Champs.
    
  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'display-fields':U ) .



  /* Code placed here will execute AFTER standard behavior.    */
  Do With Frame {&Frame-Name}:
  { csstcc.i kof.csst YES Yes ksst.lsst}
    ASSIGN vgSAVcleInit = ""
           vgSAVCart    = "".
    If Available kof Then Do:
       ASSIGN F-orig:SCREEN-VALUE     = Entry(Lookup(kof.orig, vgLcode-orig), vgLclair-orig)
              F-cnatieor:SCREEN-VALUE = Entry(Lookup(kof.cnatieor, vgLcode-cnatie), vgLclair-cnatie).
              
       CB-typof:Sensitive = False.
       If kof.typof = '' Then
          Run Set-Valeur-Combo(CB-typof:handle, {&CLASSIQUE}).
       Else
          Run Set-Valeur-Combo(CB-typof:handle, kof.typof).

       Run Modif-typof.
       
       ASSIGN vgSAVCart = kof.carts.
    End.
    Else If Adm-New-Record Then
       ASSIGN CB-typof:Sensitive = True.
  End.
  
  If Available kof THEN
   CASE kof.cnatie:
      WHEN '' THEN ASSIGN F-ltie:SCREEN-VALUE = "".
      WHEN {&cnatie-FRS} THEN DO:
         {tiecc.i kof.ctieor  no yes F-ltie 1 "F-cnatieor:screen-value in frame {&frame-Name}"}         
      END.
      WHEN {&cnatie-CLT} THEN DO:
         {clietcc.i kof.ctieor no yes F-ltie 2 {&VCLIET-CSNATIE-CLT}}         
      END.
   END CASE.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE local-initialize Tablejv 
PROCEDURE local-initialize :
/*------------------------------------------------------------------------------
  Purpose:     Override standard ADM method
  Notes:       
------------------------------------------------------------------------------*/

  /* Code placed here will execute PRIOR to standard behavior. */
  Assign vgLib-ofclass = Get-TradTxt (022774, 'OF classique':T)
         vgLib-oforig  = Get-TradTxt (022693, 'OF d''origine':T)
         vgLib-oftech  = Get-TradTxt (022694, 'OF technique':T)
         
         vgLcode-orig    = ',':u + Get-TradLCode("ORIG2":U,  "{&LCODE-ORIG2}":U)
         vgLclair-orig   = ',':u + Get-TradLClair("ORIG2":U, "{&LCLAIR-ORIG2}":U)
         vgLcode-cnatie  = ',':u + Get-TradLCode("CNATIE":U, "{&LCODE-CNATIE}":U)
         vgLclair-cnatie = ',':u + Get-TradLClair("CNATIE":U,"{&LCLAIR-CNATIE}":U).

  Assign vgLst-cod = {&CLASSIQUE}    + ",":U
                   + {&TYPOF-OFORIG} + ",":U
                   + {&TYPOF-OFTECH}
         vgLst-lib = vgLib-ofclass  + ",":U
                   + vgLib-oforig   + ",":U
                   + vgLib-oftech.
  {admvif/method/chglstval.i &Hdl="cb-typof:Handle In Frame {&Frame-Name}"
                             &Mnemoid="'':U"
                             &Liste-Valeur="vgLst-cod"
                             &Liste-Libell="vgLst-lib"}
   ASSIGN vgSAVcleInit = ""
          vgSAVCart    = "".
  Run Modif-typof.
  
  Run Get-Info("Fl-Itiact","Group-Assign-Source").
    ASSIGN w-cact = RETURN-VALUE.

  /* Dispatch standard ADM method.                             */
  RUN dispatch IN THIS-PROCEDURE ( INPUT 'initialize':U ) .

  /* Code placed here will execute AFTER standard behavior.    */

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Local-Test-Saisie Tablejv 
PROCEDURE Local-Test-Saisie :
/*------------------------------------------------------------------------------
  Purpose:     Tester la saisie des variables du viewer
  Parameters:  - Test Global, ou Pour un widget 
               - Handle du widget � tester.
  Notes:       
------------------------------------------------------------------------------*/
DEFINE INPUT PARAMETER iTest-Global AS LOGICAL   NO-UNDO.
DEFINE INPUT PARAMETER iWidget-Test AS HANDLE    NO-UNDO.

/* Exemple de test : Test apres la saisie de cle, en creation, si l'enreg n'existe pas d�j� */
 /*IF iTest-Global OR                                                                                                          
    iWidget-Test = kof.csst:HANDLE IN FRAME {&FRAME-NAME} THEN DO:                                                         
    IF adm-new-record AND                                                                                                    
       CAN-FIND(FIRST kactsst WHERE kactsst.csoc = wcsoc
                            AND     kactsst.cetab = wcetab
                            AND     kactsst.cact = w-cact                                                                           
                            AND kactsst.csst = INPUT FRAME {&FRAME-NAME} kof.csst NO-LOCK) THEN DO:                            
       {affichemsg.i &Message = "SUBSTITUTE(Get-TradMsg (013125, 'Cette valeur existe d�j� (&1).':T), iWidget-Test:label )"} 
       APPLY "ENTRY":U TO kof.csst IN FRAME {&FRAME-NAME}.                                                                  
       RETURN "ADM-ERROR":U.                                                                                                 
    END.                                                                                                                  
 END. */                                                                                                                

/* Autre Exemple : Test libelle complementaire */
 IF iTest-Global OR                                                    
    iWidget-Test = kof.csst:HANDLE IN FRAME {&FRAME-NAME} THEN DO: 
     {csstcc.i kof.csst YES YES ksst.lsst}                     
 END.                                                              

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Modif-csst Tablejv 
PROCEDURE Modif-csst :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
/* YES si l'activit� li�e � l'OF est de sous-traitance, NO sinon*/ 
DEFINE VARIABLE kactsstrait AS LOGICAL NO-UNDO.
/* Le nouveau sous-traitant de l'OF */
DEFINE VARIABLE kofcsst AS CHARACTER NO-UNDO.

DEFINE BUFFER bKactsst FOR kactsst.

/* Initialise les 2 variables ci-dessus. */
FIND FIRST bKactsst WHERE kactsst.csoc = wcsoc
                     AND kactsst.cetab = wcetab
                     AND kactsst.cact = w-cact 
 USE-INDEX i02-sstact.
IF AVAILABLE bKactsst THEN DO:
   kactsstrait = YES.
   kofcsst = kactsst.csst.     
END.
  
 ELSE DO:
    kactsstrait = NO.
    kofcsst = "". 
 END.
 
 MESSAGE "kactsstrait  " + string(kactsstrait). 
  MESSAGE "kofcsst  " + string(kofcsst).

IF(kactsstrait = YES) THEN Do With Frame {&Frame-Name}:

   kof.csst = kofcsst.
   IF(kofcsst <> "$Interne") THEN
   assign kof.tsstrait = YES.
    ELSE 
   /*assign kof.tsstrait:SCREEN-VALUE = "No":U.*/
      assign kof.tsstrait = NO.
END.
ELSE 
   assign kof.tsstrait = NO.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Modif-typof Tablejv 
PROCEDURE Modif-typof :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   DEFINE VARIABLE vTypof AS CHARACTER NO-UNDO.
    
    Run Get-Valeur-Combo(CB-typof:handle In Frame {&Frame-Name}, Output vTypof).

    If vTypof = {&CLASSIQUE} Then vTypof = {&TYPOF-CLASSIQUE}.

    Run Set-Info("kof.typof=":U + vTypof, "group-assign-source":U).

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Param-Typof Tablejv 
PROCEDURE Param-Typof :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       Appel� dans le Receive-Info
------------------------------------------------------------------------------*/
   DEFINE INPUT  PARAMETER iCact        AS CHARACTER NO-UNDO.
   DEFINE OUTPUT PARAMETER oParam-typof AS CHARACTER NO-UNDO.

   DEFINE VARIABLE vRet    AS LOGICAL   NO-UNDO.
   DEFINE VARIABLE vCfmact AS CHARACTER NO-UNDO.
   
   {&Run-InfoAct-RechInfo} (wcsoc, wcetab, iCact, "cfmact":U, Output vRet, Output vCfmact). 
    
   {getparii.i &CPACON    = "'OFCRE':U" 
               &NOCHAMP   = 8 
               &CSOC      = wcsoc
               &CETAB     = wcetab
               &CLE       = vCfmact
               &OUT-valeurs = oParam-typof
               &RETVALDEF = "Yes"
   }
    
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Pr-Disable-Champs Tablejv 
PROCEDURE Pr-Disable-Champs :
/*------------------------------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   DO WITH FRAME {&FRAME-NAME}:
      ASSIGN F-orig:SENSITIVE     = FALSE
             F-cnatieor:SENSITIVE = FALSE.
   END.
   
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE Receive-Info Tablejv 
PROCEDURE Receive-Info :
/*------------------------------------------------------------------------------
  Purpose:     Recoit une info provenant d'un objet
  Parameters:  <none>
  Notes:       
------------------------------------------------------------------------------*/
   Define Input  Parameter p-issuer-hdl       As Handle    No-Undo.
   Define Input  Parameter Name-Info          As Character No-Undo.
   Define Input  Parameter Valeur-Info        As Character No-Undo.

   Define VARIABLE w-param-typof     As Character No-Undo.
   Define Variable w-param-typof-sav As Character No-Undo.
   Define Variable w-titi            As Logical   No-Undo.
   Define Variable w-itiact          As Character No-Undo.
   Define Variable w-date            As Date      No-Undo.

   Define Buffer bKitid    For kitid.
   Define Buffer bKitidd   For kitidd.
   DEFINE VARIABLE vCarts  AS CHARACTER NO-UNDO.
   DEFINE VARIABLE vKact AS CHARACTER NO-UNDO.
   
   Case Name-Info :
      /* Ins�rer ici vos propres infos � recevoir */
      When 'itiact':U Then Do With Frame {&Frame-Name}:
      
         /* SLe 18/05/2012 Initialisation de la priorit� via l'article */
         RUN Get-Info ("kof.carts":u,"Group-Assign-Source":u).
         ASSIGN vCarts = RETURN-VALUE.
         IF vCarts NE vgSAVCart THEN DO:
            ASSIGN kof.prior:SCREEN-VALUE = Priorite (vCarts)
                   vgSAVCart = vCarts.
         END.
         
         /* ACA 15/12/2015 Initialisation de la check box Sous-Traitance et du Sous-Traitant via l'activit� */ 
         /*IF adm-new-record THEN DO:
         MESSAGE "new ou modif".
                  Run Get-Info("Fl-Itiact":u,"Group-Assign-Source":u).
                 
         ASSIGN vKact = RETURN-VALUE.
         ASSIGN kof.tsstrait:CHECKED = Tsstrait (vKact).
         ASSIGN kof.csst:SCREEN-VALUE = Csst (vKact).
         Run Get-Info("Cle-OF":u,"Group-Assign-Source":u).
         MESSAGE STRING(RETURN-VALUE).
         END.*/


         /* SLe 02/06/09 Q002616 il ne faut faire l'init du param�tre qu'une seule fois,
          * sinon on �crase le for�age manuel de l'utilisateur
          */
         IF Valeur-Info = vgSAVcleInit THEN RETURN "".
         
         ASSIGN vgSAVcleInit = Valeur-Info.
         If Num-Entries(Valeur-Info, '|') = 3 Then Do:
          
            If Adm-New-Record    And
               Adm-Adding-Record 
            Then Do:   /* en cr�ation seulement , pas en copie ni en modif */
             
               Assign w-titi   = (Entry(1, Valeur-Info, '|':U) = 'OUI':U)
                      w-itiact =  Entry(2, Valeur-Info, '|':U)
                      w-date   =  Date(Entry(3, Valeur-Info, '|':U)) No-Error.
   
               If w-titi Then Do:
                  /* ITINERAIRE */
                  ASSIGN w-param-typof-sav = ''.
                  B1:
                  For Last bKitid Fields (datef)
                                  Where bKitid.csoc     = wcsoc
                                    And bKitid.cetab    = wcetab
                                    And bKitid.citi     = w-itiact
                                    And bKitid.datef   <= w-date
                                  No-Lock,
                     Each bKitidd Fields (cact)
                                   Where bKitidd.csoc     = wcsoc
                                     And bKitidd.cetab    = wcetab
                                     And bKitidd.citi     = w-itiact
                                     And bKitidd.datef    = bKitid.datef
                                   No-Lock:
    
                     Run Param-Typof (bKitidd.cact, Output w-param-typof).
                     If w-param-typof-sav = '' Then 
                        ASSIGN w-param-typof-sav = w-param-typof.
                     Else If w-param-typof <> w-param-typof-sav Then Do:
                        Run Set-Valeur-Combo(CB-typof:handle, {&CLASSIQUE}).
                        Run Modif-typof.
                        CB-typof:Sensitive = False.
                        {affichemsg.i &Message = "Get-TradMsg (022779, 'Attention, les activit�s de l''itin�raire ne sont pas param�tr�es de la m�me fa�on|concernant le type de gestion de l''OF (classique ou technique), dans le param�tre OFCRE.':T)"}
                        Return.
                     End.
                  End.
               End.
               Else 
                  /* ACTIVITE */
                  Run Param-Typof (w-itiact, Output w-param-typof).

               Case w-param-typof:
                  When 'OF CLASSIQUES':U Then Do:
                     Run Set-Valeur-Combo(CB-typof:handle, {&CLASSIQUE}).
                     Run Modif-typof.
                     CB-typof:Sensitive = False.
                  End.
                  When 'OF ORIG. + TECH.':U  Then Do:
                     Run Set-Valeur-Combo(CB-typof:handle, {&TYPOF-OFORIG}).
                     Run Modif-typof.
                     CB-typof:Sensitive = True.
                  End.
                  Otherwise Do:
                     Run Set-Valeur-Combo(CB-typof:handle, {&CLASSIQUE}).
                     Run Modif-typof.
                     CB-typof:Sensitive = False.
                  End.
               End Case.
            End.
         End.
         Else Do:
            Run Set-Valeur-Combo(CB-typof:handle, {&CLASSIQUE}).
            Run Modif-typof.
            CB-typof:Sensitive = False.
         End.
      End.
       
      /* CD D001232 30/05/2012 OF commenc� modifiable */  
      WHEN "Disable-Prior":U THEN
         ASSIGN kof.prior:SENSITIVE = (Valeur-Info = "NO":u).
      
      WHEN "Init-Csst-With-Cact":U THEN DO:
         
         DEFINE VARIABLE cact AS CHARACTER NO-UNDO.
         ASSIGN cact = STRING(Valeur-Info).
         ASSIGN kof.csst:SCREEN-VALUE = Csst (cact). 
      END.
      
      Otherwise 
          /* Par d�faut, valorisation de la Screen-Value du widget dont 
           * le nom est contenu dans Name-Info */
          Run Brokvif-Receive-Info In Adm-Broker-Hdl
                (This-Procedure, p-issuer-hdl, Name-Info, Valeur-Info).
   End Case.

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE send-records Tablejv  _ADM-SEND-RECORDS
PROCEDURE send-records :
/*------------------------------------------------------------------------------
  Purpose:     Send record ROWID's for all tables used by
               this file.
  Parameters:  see template/snd-head.i
------------------------------------------------------------------------------*/

  /* Define variables needed by this internal procedure.               */
  {src/adm/template/snd-head.i}

  /* For each requested table, put it's ROWID in the output list.      */
  {src/adm/template/snd-list.i "kof"}

  /* Deal with any unexpected table requests before closing.           */
  {src/adm/template/snd-end.i}

END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _PROCEDURE state-changed Tablejv 
PROCEDURE state-changed :
/* -----------------------------------------------------------
  Purpose:     
  Parameters:  <none>
  Notes:       
-------------------------------------------------------------*/
  DEFINE INPUT PARAMETER p-issuer-hdl AS HANDLE    NO-UNDO.
  DEFINE INPUT PARAMETER p-state      AS CHARACTER NO-UNDO.

  CASE p-state:
      When "OFTECH":U Then Do:
        Run Set-Valeur-Combo(CB-typof:handle In Frame {&Frame-Name}, {&TYPOF-OFTECH}).
        Run Modif-typof.
      End.
      
      WHEN 'refresh':U THEN do:
          Run Dispatch ("display-fields":U).
      END.
        
      /* Object instance CASEs can go here to replace standard behavior
         or add new cases. */
      {admvif/template/vinclude.i "vstates"}
  END CASE.
END PROCEDURE.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

/* ************************  Function Implementations ***************** */

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Csst Tablejv 
FUNCTION Csst RETURNS CHARACTER
  ( INPUT iCact AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/

   DEFINE BUFFER bKactsst FOR kactsst.
   
   DEFINE VARIABLE first-csst AS CHARACTER NO-UNDO.
   
   DEFINE VARIABLE isSStAct AS LOGICAL INITIAL NO.
   
   DEFINE VARIABLE interne-present AS LOGICAL INITIAL NO.
   
   DEFINE VARIABLE interne-prioritaire AS LOGICAL INITIAL NO.
   
   DEFINE VARIABLE nb-csst AS INTEGER INITIAL 0 NO-UNDO.
   
   
   FOR EACH bKactsst 
                       WHERE bKactsst.csoc = wcsoc
                         AND bKactsst.cetab = wcetab
                         AND bKactsst.cact  = iCact
                       USE-INDEX i02-sstact NO-LOCK:
                       
      IF(bKactsst.csst = "$Interne" AND nb-csst = 0) THEN Do With Frame {&Frame-Name}:
            interne-present = YES.
            interne-prioritaire = YES.
            first-csst = bKactsst.csst.          
      END.
      
      IF(bKactsst.csst = "$Interne" AND nb-csst <> 0) THEN Do With Frame {&Frame-Name}:
            interne-present = YES.
      END.
      
      IF(bKactsst.csst <> "$Interne" AND nb-csst = 0) THEN Do With Frame {&Frame-Name}:
            first-csst = bKactsst.csst.
      END.
      
      nb-csst = nb-csst + 1.
      
   END.
   
   IF(nb-csst > 0) THEN DO: 
      isSStAct = YES.
      MESSAGE "isSStAct " + STRING(isSStAct).
      MESSAGE "interne-present " + STRING(interne-present).
      MESSAGE "interne-prioritaire " + STRING(interne-prioritaire).
      MESSAGE "first-csst " + STRING(first-csst).
   END.
   ELSE DO:
      MESSAGE "isSStAct " + STRING(isSStAct).
      MESSAGE "interne-present " + STRING(interne-present).
      MESSAGE "interne-prioritaire " + STRING(interne-prioritaire).
      MESSAGE "first-csst " + STRING(first-csst).
   END.
      
       
   
   
   Case isSStAct : 
      WHEN YES Then Do With Frame {&Frame-Name}: /* si c'est une activit� de sous-traitance */
         
         /* Griser l'option sous-traitance si $Interne appartient � la liste des sous-traitants de l'activit� */
         
         IF(interne-present AND nb-csst = 1) THEN DO:
             MESSAGE "il n'y a que $Interne".
             ASSIGN kof.tsstrait:CHECKED = NO.
             ASSIGN kof.tsstrait:SENSITIVE = NO.
             ASSIGN kof.csst:SENSITIVE = NO.
             MESSAGE "Sous-traitant indiqu� " + first-csst.
             RETURN first-csst.
         END.
         
         IF(interne-present AND interne-prioritaire AND nb-csst > 1) THEN DO:
             MESSAGE "il y a Interne Prioritaire et plusieurs autres sous-traitants".
             ASSIGN kof.tsstrait:CHECKED = NO.
             ASSIGN kof.tsstrait:SENSITIVE = YES.
             ASSIGN kof.csst:SENSITIVE = NO.
             MESSAGE "Sous-traitant indiqu� " + first-csst.
             RETURN first-csst.
         END.
         
         IF(interne-present AND NOT interne-prioritaire) THEN DO:
             MESSAGE "il y a Interne Non Prioritaire et plusieurs autres sous-traitants".
             ASSIGN kof.tsstrait:CHECKED = YES.
             ASSIGN kof.tsstrait:SENSITIVE = YES.
             ASSIGN kof.csst:SENSITIVE = YES.
             MESSAGE "Sous-traitant indiqu� " + first-csst.
             RETURN first-csst.
         END.
         
         IF(NOT interne-present) THEN DO:
            MESSAGE "il n'y a pas Interne mais au moins un sous-traitant".
            ASSIGN kof.tsstrait:CHECKED = YES .
            ASSIGN kof.tsstrait:SENSITIVE = NO.
            ASSIGN kof.csst:SENSITIVE = YES.
            MESSAGE "Sous-traitant indiqu� " + first-csst.
            RETURN first-csst.
         END.
               
      End.
      
      WHEN NO Then Do With Frame {&Frame-Name}: /* si ce n'est pas une activit� de sous-traitance */
      
         ASSIGN kof.tsstrait:CHECKED = NO.
         ASSIGN kof.tsstrait:SENSITIVE = NO.
         ASSIGN kof.csst:SENSITIVE = NO.
         RETURN "".
      End.
   End Case.


END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Priorite Tablejv 
FUNCTION Priorite RETURNS CHARACTER PRIVATE
  ( INPUT iCart AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/
   DEFINE BUFFER bZartprod FOR zartprod.

   FOR FIRST bZartprod FIELDS (prior)
                       WHERE bZartprod.csoc = wcsoc
                         AND bZartprod.cetab = wcetab
                         AND bZartprod.cart  = iCart
                       NO-LOCK:
      RETURN STRING (bZartprod.prior).
   END.
   RETURN "999":u.   /* Function return value. */

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

&ANALYZE-SUSPEND _UIB-CODE-BLOCK _FUNCTION Tsstrait Tablejv 
FUNCTION Tsstrait RETURNS LOGICAL
  ( INPUT iCact AS CHARACTER ) :
/*------------------------------------------------------------------------------
  Purpose:  
    Notes:  
------------------------------------------------------------------------------*/  
   DEFINE BUFFER bKactsst FOR kactsst.

   FIND FIRST bKactsst /*FIELDS (csst)*/ WHERE bKactsst.csoc = wcsoc
                         AND bKactsst.cetab = wcetab
                         AND bKactsst.cact  = iCact USE-INDEX i02-sstact NO-LOCK NO-ERROR.
      IF AVAILABLE bKactsst THEN DO:
         IF(bKactsst.csst = "$Interne") THEN
            RETURN NO.
        
         ELSE
            RETURN YES.
      END.
      ELSE
         RETURN NO.
   
   
  /* DEFINE BUFFER bKact FOR kact.

   FOR FIRST bKact FIELDS (tsstrait)
                       WHERE bKact.csoc = wcsoc
                         AND bKact.cetab = wcetab
                         AND bKact.cact  = iCact
                       NO-LOCK:
      RETURN bKact.tsstrait.
   END.
   RETURN NO.   /* Function return value. */*/

END FUNCTION.

/* _UIB-CODE-BLOCK-END */
&ANALYZE-RESUME

